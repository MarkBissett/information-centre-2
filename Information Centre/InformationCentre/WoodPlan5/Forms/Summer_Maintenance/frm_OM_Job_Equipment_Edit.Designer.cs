namespace WoodPlan5
{
    partial class frm_OM_Job_Equipment_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Job_Equipment_Edit));
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling25 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling22 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling23 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLinkedDocuments = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.JobExpectedStartDateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp06187OMJobEquipmentEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.OwnerTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OwnerNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobSubTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteLocationYTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteLocationXTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SitePostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EndDateTimeDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.StartDateTimeDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.PurchaseOrderIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobSubTypeDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobTypeDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameContractDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SellValueTotalSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostValueTotalSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellValueVatSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostValueVatSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellValueExVatSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostValueExVatSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellPerUnitVatRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostPerUnitVatRateSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellPerUnitExVatSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostPerUnitExVatSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SellUnitDescriptorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06101OMWorkUnitTypesPicklistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SellUnitsUsedSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CostUnitDescriptorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CostUnitsUsedSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EquipmentNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.EquipmentUsedIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.JobTypeJobSubTypeDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.EquipmentIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForClientContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSitePostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteLocationX = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteLocationY = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPurchaseOrderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobSubTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobSubTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEquipmentID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEquipmentUsedID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForJobTypeJobSubTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layGrpAddress = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCostUnitsUsed = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostUnitDescriptorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostPerUnitExVat = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostPerUnitVatRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostValueExVat = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.ItemForCostValueVat = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostValueTotal = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSellUnitsUsed = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellUnitDescriptorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellPerUnitExVat = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellPerUnitVatRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellValueExVat = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.ItemForSellValueVat = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellValueTotal = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientNameContractDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForStartDateTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEndDateTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOwnerName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOwnerType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEquipmentName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForJobExpectedStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp06101_OM_Work_Unit_Types_PicklistTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06101_OM_Work_Unit_Types_PicklistTableAdapter();
            this.sp06187_OM_Job_Equipment_EditTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06187_OM_Job_Equipment_EditTableAdapter();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.JobExpectedStartDateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06187OMJobEquipmentEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteLocationYTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteLocationXTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SitePostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateTimeDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateTimeDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateTimeDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateTimeDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameContractDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellValueTotalSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostValueTotalSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellValueVatSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostValueVatSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellValueExVatSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostValueExVatSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellPerUnitVatRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostPerUnitVatRateSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellPerUnitExVatSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostPerUnitExVatSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellUnitDescriptorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06101OMWorkUnitTypesPicklistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellUnitsUsedSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostUnitDescriptorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostUnitsUsedSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentUsedIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeJobSubTypeDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSitePostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteLocationX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteLocationY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPurchaseOrderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentUsedID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeJobSubTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostUnitsUsed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostUnitDescriptorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostPerUnitExVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostPerUnitVatRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostValueExVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostValueVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostValueTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellUnitsUsed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellUnitDescriptorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellPerUnitExVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellPerUnitVatRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellValueExVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellValueVat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellValueTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientNameContractDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobExpectedStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 614);
            this.barDockControlBottom.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 588);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(686, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 588);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn1.Width = 53;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colID1.Width = 53;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiLinkedDocuments});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLinkedDocuments, true)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem5.Image")));
            toolTipTitleItem5.Text = "Save Button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bbiFormSave.SuperTip = superToolTip5;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem6.Image")));
            toolTipTitleItem6.Text = "Cancel Button - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.bbiFormCancel.SuperTip = superToolTip6;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiLinkedDocuments
            // 
            this.bbiLinkedDocuments.Caption = "Linked Documents";
            this.bbiLinkedDocuments.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLinkedDocuments.Glyph")));
            this.bbiLinkedDocuments.Id = 15;
            this.bbiLinkedDocuments.Name = "bbiLinkedDocuments";
            toolTipTitleItem8.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem8.Image")));
            toolTipTitleItem8.Text = "Linked Documents - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to open the Linked Documents Manager, filtered on the current data entry" +
    " record.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bbiLinkedDocuments.SuperTip = superToolTip8;
            this.bbiLinkedDocuments.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLinkedDocuments_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Form Mode - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.barStaticItemFormMode.SuperTip = superToolTip7;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(686, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 614);
            this.barDockControl2.Size = new System.Drawing.Size(686, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 588);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(686, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 588);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.JobExpectedStartDateTextEdit);
            this.dataLayoutControl1.Controls.Add(this.OwnerTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.OwnerNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientContractIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteContractIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobSubTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteLocationYTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteLocationXTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SitePostcodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EndDateTimeDateEdit);
            this.dataLayoutControl1.Controls.Add(this.StartDateTimeDateEdit);
            this.dataLayoutControl1.Controls.Add(this.PurchaseOrderIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobSubTypeDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobTypeDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameContractDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SellValueTotalSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostValueTotalSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellValueVatSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostValueVatSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellValueExVatSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostValueExVatSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellPerUnitVatRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostPerUnitVatRateSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellPerUnitExVatSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostPerUnitExVatSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SellUnitDescriptorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.SellUnitsUsedSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CostUnitDescriptorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.CostUnitsUsedSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EquipmentNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.EquipmentUsedIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.JobTypeJobSubTypeDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.EquipmentIDTextEdit);
            this.dataLayoutControl1.DataSource = this.sp06187OMJobEquipmentEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientContractID,
            this.ItemForClientID,
            this.ItemForClientName,
            this.ItemForJobID,
            this.ItemForVisitID,
            this.ItemForSitePostcode,
            this.ItemForSiteLocationX,
            this.ItemForSiteLocationY,
            this.ItemForSiteID,
            this.ItemForSiteContractID,
            this.ItemForPurchaseOrderID,
            this.ItemForJobTypeID,
            this.ItemForJobTypeDescription,
            this.ItemForJobSubTypeID,
            this.ItemForJobSubTypeDescription,
            this.ItemForEquipmentID,
            this.ItemForEquipmentUsedID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1234, 162, 301, 422);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(686, 588);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // JobExpectedStartDateTextEdit
            // 
            this.JobExpectedStartDateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "JobExpectedStartDate", true));
            this.JobExpectedStartDateTextEdit.Location = new System.Drawing.Point(117, 131);
            this.JobExpectedStartDateTextEdit.MenuManager = this.barManager1;
            this.JobExpectedStartDateTextEdit.Name = "JobExpectedStartDateTextEdit";
            this.JobExpectedStartDateTextEdit.Properties.Mask.EditMask = "g";
            this.JobExpectedStartDateTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.JobExpectedStartDateTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.JobExpectedStartDateTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobExpectedStartDateTextEdit, true);
            this.JobExpectedStartDateTextEdit.Size = new System.Drawing.Size(191, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobExpectedStartDateTextEdit, optionsSpelling25);
            this.JobExpectedStartDateTextEdit.StyleController = this.dataLayoutControl1;
            this.JobExpectedStartDateTextEdit.TabIndex = 70;
            // 
            // sp06187OMJobEquipmentEditBindingSource
            // 
            this.sp06187OMJobEquipmentEditBindingSource.DataMember = "sp06187_OM_Job_Equipment_Edit";
            this.sp06187OMJobEquipmentEditBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // OwnerTypeTextEdit
            // 
            this.OwnerTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "OwnerType", true));
            this.OwnerTypeTextEdit.Location = new System.Drawing.Point(417, 179);
            this.OwnerTypeTextEdit.MenuManager = this.barManager1;
            this.OwnerTypeTextEdit.Name = "OwnerTypeTextEdit";
            this.OwnerTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.OwnerTypeTextEdit, true);
            this.OwnerTypeTextEdit.Size = new System.Drawing.Size(240, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.OwnerTypeTextEdit, optionsSpelling1);
            this.OwnerTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.OwnerTypeTextEdit.TabIndex = 69;
            // 
            // OwnerNameTextEdit
            // 
            this.OwnerNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "OwnerName", true));
            this.OwnerNameTextEdit.Location = new System.Drawing.Point(117, 179);
            this.OwnerNameTextEdit.MenuManager = this.barManager1;
            this.OwnerNameTextEdit.Name = "OwnerNameTextEdit";
            this.OwnerNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.OwnerNameTextEdit, true);
            this.OwnerNameTextEdit.Size = new System.Drawing.Size(191, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.OwnerNameTextEdit, optionsSpelling2);
            this.OwnerNameTextEdit.StyleController = this.dataLayoutControl1;
            this.OwnerNameTextEdit.TabIndex = 68;
            // 
            // ClientContractIDTextEdit
            // 
            this.ClientContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "ClientContractID", true));
            this.ClientContractIDTextEdit.Location = new System.Drawing.Point(117, 157);
            this.ClientContractIDTextEdit.MenuManager = this.barManager1;
            this.ClientContractIDTextEdit.Name = "ClientContractIDTextEdit";
            this.ClientContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientContractIDTextEdit, true);
            this.ClientContractIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientContractIDTextEdit, optionsSpelling3);
            this.ClientContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientContractIDTextEdit.TabIndex = 67;
            this.ClientContractIDTextEdit.TabStop = false;
            // 
            // SiteContractIDTextEdit
            // 
            this.SiteContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "SiteContractID", true));
            this.SiteContractIDTextEdit.Location = new System.Drawing.Point(117, 301);
            this.SiteContractIDTextEdit.MenuManager = this.barManager1;
            this.SiteContractIDTextEdit.Name = "SiteContractIDTextEdit";
            this.SiteContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteContractIDTextEdit, true);
            this.SiteContractIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteContractIDTextEdit, optionsSpelling4);
            this.SiteContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteContractIDTextEdit.TabIndex = 66;
            this.SiteContractIDTextEdit.TabStop = false;
            // 
            // JobTypeIDTextEdit
            // 
            this.JobTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "JobTypeID", true));
            this.JobTypeIDTextEdit.Location = new System.Drawing.Point(117, 229);
            this.JobTypeIDTextEdit.MenuManager = this.barManager1;
            this.JobTypeIDTextEdit.Name = "JobTypeIDTextEdit";
            this.JobTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobTypeIDTextEdit, true);
            this.JobTypeIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobTypeIDTextEdit, optionsSpelling5);
            this.JobTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.JobTypeIDTextEdit.TabIndex = 65;
            this.JobTypeIDTextEdit.TabStop = false;
            // 
            // JobSubTypeIDTextEdit
            // 
            this.JobSubTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "JobSubTypeID", true));
            this.JobSubTypeIDTextEdit.Location = new System.Drawing.Point(117, 214);
            this.JobSubTypeIDTextEdit.MenuManager = this.barManager1;
            this.JobSubTypeIDTextEdit.Name = "JobSubTypeIDTextEdit";
            this.JobSubTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobSubTypeIDTextEdit, true);
            this.JobSubTypeIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobSubTypeIDTextEdit, optionsSpelling6);
            this.JobSubTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.JobSubTypeIDTextEdit.TabIndex = 64;
            this.JobSubTypeIDTextEdit.TabStop = false;
            // 
            // SiteLocationYTextEdit
            // 
            this.SiteLocationYTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "SiteLocationY", true));
            this.SiteLocationYTextEdit.Location = new System.Drawing.Point(441, 253);
            this.SiteLocationYTextEdit.MenuManager = this.barManager1;
            this.SiteLocationYTextEdit.Name = "SiteLocationYTextEdit";
            this.SiteLocationYTextEdit.Properties.Mask.EditMask = "n8";
            this.SiteLocationYTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.SiteLocationYTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SiteLocationYTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteLocationYTextEdit, true);
            this.SiteLocationYTextEdit.Size = new System.Drawing.Size(216, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteLocationYTextEdit, optionsSpelling7);
            this.SiteLocationYTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteLocationYTextEdit.TabIndex = 63;
            this.SiteLocationYTextEdit.TabStop = false;
            // 
            // SiteLocationXTextEdit
            // 
            this.SiteLocationXTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "SiteLocationX", true));
            this.SiteLocationXTextEdit.Location = new System.Drawing.Point(441, 253);
            this.SiteLocationXTextEdit.MenuManager = this.barManager1;
            this.SiteLocationXTextEdit.Name = "SiteLocationXTextEdit";
            this.SiteLocationXTextEdit.Properties.Mask.EditMask = "n8";
            this.SiteLocationXTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.SiteLocationXTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SiteLocationXTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteLocationXTextEdit, true);
            this.SiteLocationXTextEdit.Size = new System.Drawing.Size(216, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteLocationXTextEdit, optionsSpelling8);
            this.SiteLocationXTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteLocationXTextEdit.TabIndex = 62;
            this.SiteLocationXTextEdit.TabStop = false;
            // 
            // SitePostcodeTextEdit
            // 
            this.SitePostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "SitePostcode", true));
            this.SitePostcodeTextEdit.Location = new System.Drawing.Point(441, 253);
            this.SitePostcodeTextEdit.MenuManager = this.barManager1;
            this.SitePostcodeTextEdit.Name = "SitePostcodeTextEdit";
            this.SitePostcodeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SitePostcodeTextEdit, true);
            this.SitePostcodeTextEdit.Size = new System.Drawing.Size(216, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SitePostcodeTextEdit, optionsSpelling9);
            this.SitePostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.SitePostcodeTextEdit.TabIndex = 61;
            this.SitePostcodeTextEdit.TabStop = false;
            // 
            // EndDateTimeDateEdit
            // 
            this.EndDateTimeDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "EndDateTime", true));
            this.EndDateTimeDateEdit.EditValue = null;
            this.EndDateTimeDateEdit.Location = new System.Drawing.Point(117, 227);
            this.EndDateTimeDateEdit.MenuManager = this.barManager1;
            this.EndDateTimeDateEdit.Name = "EndDateTimeDateEdit";
            this.EndDateTimeDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.EndDateTimeDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndDateTimeDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndDateTimeDateEdit.Properties.Mask.EditMask = "g";
            this.EndDateTimeDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EndDateTimeDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.EndDateTimeDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.EndDateTimeDateEdit.Size = new System.Drawing.Size(191, 20);
            this.EndDateTimeDateEdit.StyleController = this.dataLayoutControl1;
            this.EndDateTimeDateEdit.TabIndex = 15;
            // 
            // StartDateTimeDateEdit
            // 
            this.StartDateTimeDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "StartDateTime", true));
            this.StartDateTimeDateEdit.EditValue = null;
            this.StartDateTimeDateEdit.Location = new System.Drawing.Point(117, 203);
            this.StartDateTimeDateEdit.MenuManager = this.barManager1;
            this.StartDateTimeDateEdit.Name = "StartDateTimeDateEdit";
            this.StartDateTimeDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.StartDateTimeDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartDateTimeDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartDateTimeDateEdit.Properties.Mask.EditMask = "g";
            this.StartDateTimeDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.StartDateTimeDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.StartDateTimeDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.StartDateTimeDateEdit.Size = new System.Drawing.Size(191, 20);
            this.StartDateTimeDateEdit.StyleController = this.dataLayoutControl1;
            this.StartDateTimeDateEdit.TabIndex = 14;
            // 
            // PurchaseOrderIDTextEdit
            // 
            this.PurchaseOrderIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "PurchaseOrderID", true));
            this.PurchaseOrderIDTextEdit.Location = new System.Drawing.Point(117, 277);
            this.PurchaseOrderIDTextEdit.MenuManager = this.barManager1;
            this.PurchaseOrderIDTextEdit.Name = "PurchaseOrderIDTextEdit";
            this.PurchaseOrderIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PurchaseOrderIDTextEdit, true);
            this.PurchaseOrderIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PurchaseOrderIDTextEdit, optionsSpelling10);
            this.PurchaseOrderIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PurchaseOrderIDTextEdit.TabIndex = 60;
            this.PurchaseOrderIDTextEdit.TabStop = false;
            // 
            // JobSubTypeDescriptionTextEdit
            // 
            this.JobSubTypeDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "JobSubTypeDescription", true));
            this.JobSubTypeDescriptionTextEdit.Location = new System.Drawing.Point(117, 214);
            this.JobSubTypeDescriptionTextEdit.MenuManager = this.barManager1;
            this.JobSubTypeDescriptionTextEdit.Name = "JobSubTypeDescriptionTextEdit";
            this.JobSubTypeDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobSubTypeDescriptionTextEdit, true);
            this.JobSubTypeDescriptionTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobSubTypeDescriptionTextEdit, optionsSpelling11);
            this.JobSubTypeDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.JobSubTypeDescriptionTextEdit.TabIndex = 59;
            this.JobSubTypeDescriptionTextEdit.TabStop = false;
            // 
            // JobTypeDescriptionTextEdit
            // 
            this.JobTypeDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "JobTypeDescription", true));
            this.JobTypeDescriptionTextEdit.Location = new System.Drawing.Point(117, 214);
            this.JobTypeDescriptionTextEdit.MenuManager = this.barManager1;
            this.JobTypeDescriptionTextEdit.Name = "JobTypeDescriptionTextEdit";
            this.JobTypeDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobTypeDescriptionTextEdit, true);
            this.JobTypeDescriptionTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobTypeDescriptionTextEdit, optionsSpelling12);
            this.JobTypeDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.JobTypeDescriptionTextEdit.TabIndex = 58;
            this.JobTypeDescriptionTextEdit.TabStop = false;
            // 
            // ClientNameTextEdit
            // 
            this.ClientNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "ClientName", true));
            this.ClientNameTextEdit.Location = new System.Drawing.Point(117, 157);
            this.ClientNameTextEdit.MenuManager = this.barManager1;
            this.ClientNameTextEdit.Name = "ClientNameTextEdit";
            this.ClientNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameTextEdit, true);
            this.ClientNameTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameTextEdit, optionsSpelling13);
            this.ClientNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameTextEdit.TabIndex = 57;
            this.ClientNameTextEdit.TabStop = false;
            // 
            // VisitNumberTextEdit
            // 
            this.VisitNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "VisitNumber", true));
            this.VisitNumberTextEdit.Location = new System.Drawing.Point(117, 83);
            this.VisitNumberTextEdit.MenuManager = this.barManager1;
            this.VisitNumberTextEdit.Name = "VisitNumberTextEdit";
            this.VisitNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitNumberTextEdit, true);
            this.VisitNumberTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitNumberTextEdit, optionsSpelling14);
            this.VisitNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitNumberTextEdit.TabIndex = 56;
            this.VisitNumberTextEdit.TabStop = false;
            // 
            // SiteNameTextEdit
            // 
            this.SiteNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "SiteName", true));
            this.SiteNameTextEdit.Location = new System.Drawing.Point(117, 59);
            this.SiteNameTextEdit.MenuManager = this.barManager1;
            this.SiteNameTextEdit.Name = "SiteNameTextEdit";
            this.SiteNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteNameTextEdit, true);
            this.SiteNameTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteNameTextEdit, optionsSpelling15);
            this.SiteNameTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteNameTextEdit.TabIndex = 55;
            this.SiteNameTextEdit.TabStop = false;
            // 
            // ClientNameContractDescriptionTextEdit
            // 
            this.ClientNameContractDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "ClientNameContractDescription", true));
            this.ClientNameContractDescriptionTextEdit.Location = new System.Drawing.Point(117, 35);
            this.ClientNameContractDescriptionTextEdit.MenuManager = this.barManager1;
            this.ClientNameContractDescriptionTextEdit.Name = "ClientNameContractDescriptionTextEdit";
            this.ClientNameContractDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameContractDescriptionTextEdit, true);
            this.ClientNameContractDescriptionTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameContractDescriptionTextEdit, optionsSpelling16);
            this.ClientNameContractDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameContractDescriptionTextEdit.TabIndex = 54;
            this.ClientNameContractDescriptionTextEdit.TabStop = false;
            // 
            // VisitIDTextEdit
            // 
            this.VisitIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "VisitID", true));
            this.VisitIDTextEdit.Location = new System.Drawing.Point(441, 253);
            this.VisitIDTextEdit.MenuManager = this.barManager1;
            this.VisitIDTextEdit.Name = "VisitIDTextEdit";
            this.VisitIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitIDTextEdit, true);
            this.VisitIDTextEdit.Size = new System.Drawing.Size(216, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitIDTextEdit, optionsSpelling17);
            this.VisitIDTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitIDTextEdit.TabIndex = 53;
            // 
            // SiteIDTextEdit
            // 
            this.SiteIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "SiteID", true));
            this.SiteIDTextEdit.Location = new System.Drawing.Point(117, 325);
            this.SiteIDTextEdit.MenuManager = this.barManager1;
            this.SiteIDTextEdit.Name = "SiteIDTextEdit";
            this.SiteIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteIDTextEdit, true);
            this.SiteIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteIDTextEdit, optionsSpelling18);
            this.SiteIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteIDTextEdit.TabIndex = 52;
            // 
            // ClientIDTextEdit
            // 
            this.ClientIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "ClientID", true));
            this.ClientIDTextEdit.Location = new System.Drawing.Point(117, 157);
            this.ClientIDTextEdit.MenuManager = this.barManager1;
            this.ClientIDTextEdit.Name = "ClientIDTextEdit";
            this.ClientIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientIDTextEdit, true);
            this.ClientIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientIDTextEdit, optionsSpelling19);
            this.ClientIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDTextEdit.TabIndex = 51;
            // 
            // SellValueTotalSpinEdit
            // 
            this.SellValueTotalSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "SellValueTotal", true));
            this.SellValueTotalSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellValueTotalSpinEdit.Location = new System.Drawing.Point(459, 511);
            this.SellValueTotalSpinEdit.MenuManager = this.barManager1;
            this.SellValueTotalSpinEdit.Name = "SellValueTotalSpinEdit";
            this.SellValueTotalSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellValueTotalSpinEdit.Properties.Mask.EditMask = "c";
            this.SellValueTotalSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellValueTotalSpinEdit.Properties.ReadOnly = true;
            this.SellValueTotalSpinEdit.Size = new System.Drawing.Size(162, 20);
            this.SellValueTotalSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellValueTotalSpinEdit.TabIndex = 50;
            this.SellValueTotalSpinEdit.TabStop = false;
            // 
            // CostValueTotalSpinEdit
            // 
            this.CostValueTotalSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "CostValueTotal", true));
            this.CostValueTotalSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostValueTotalSpinEdit.Location = new System.Drawing.Point(153, 511);
            this.CostValueTotalSpinEdit.MenuManager = this.barManager1;
            this.CostValueTotalSpinEdit.Name = "CostValueTotalSpinEdit";
            this.CostValueTotalSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostValueTotalSpinEdit.Properties.Mask.EditMask = "c";
            this.CostValueTotalSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostValueTotalSpinEdit.Properties.ReadOnly = true;
            this.CostValueTotalSpinEdit.Size = new System.Drawing.Size(167, 20);
            this.CostValueTotalSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostValueTotalSpinEdit.TabIndex = 49;
            this.CostValueTotalSpinEdit.TabStop = false;
            // 
            // SellValueVatSpinEdit
            // 
            this.SellValueVatSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "SellValueVat", true));
            this.SellValueVatSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellValueVatSpinEdit.Location = new System.Drawing.Point(459, 487);
            this.SellValueVatSpinEdit.MenuManager = this.barManager1;
            this.SellValueVatSpinEdit.Name = "SellValueVatSpinEdit";
            this.SellValueVatSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellValueVatSpinEdit.Properties.Mask.EditMask = "c";
            this.SellValueVatSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellValueVatSpinEdit.Properties.ReadOnly = true;
            this.SellValueVatSpinEdit.Size = new System.Drawing.Size(162, 20);
            this.SellValueVatSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellValueVatSpinEdit.TabIndex = 49;
            this.SellValueVatSpinEdit.TabStop = false;
            // 
            // CostValueVatSpinEdit
            // 
            this.CostValueVatSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "CostValueVat", true));
            this.CostValueVatSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostValueVatSpinEdit.Location = new System.Drawing.Point(153, 487);
            this.CostValueVatSpinEdit.MenuManager = this.barManager1;
            this.CostValueVatSpinEdit.Name = "CostValueVatSpinEdit";
            this.CostValueVatSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostValueVatSpinEdit.Properties.Mask.EditMask = "c";
            this.CostValueVatSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostValueVatSpinEdit.Properties.ReadOnly = true;
            this.CostValueVatSpinEdit.Size = new System.Drawing.Size(167, 20);
            this.CostValueVatSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostValueVatSpinEdit.TabIndex = 48;
            this.CostValueVatSpinEdit.TabStop = false;
            // 
            // SellValueExVatSpinEdit
            // 
            this.SellValueExVatSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "SellValueExVat", true));
            this.SellValueExVatSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellValueExVatSpinEdit.Location = new System.Drawing.Point(459, 463);
            this.SellValueExVatSpinEdit.MenuManager = this.barManager1;
            this.SellValueExVatSpinEdit.Name = "SellValueExVatSpinEdit";
            this.SellValueExVatSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellValueExVatSpinEdit.Properties.Mask.EditMask = "c";
            this.SellValueExVatSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellValueExVatSpinEdit.Properties.ReadOnly = true;
            this.SellValueExVatSpinEdit.Size = new System.Drawing.Size(162, 20);
            this.SellValueExVatSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellValueExVatSpinEdit.TabIndex = 48;
            this.SellValueExVatSpinEdit.TabStop = false;
            // 
            // CostValueExVatSpinEdit
            // 
            this.CostValueExVatSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "CostValueExVat", true));
            this.CostValueExVatSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostValueExVatSpinEdit.Location = new System.Drawing.Point(153, 463);
            this.CostValueExVatSpinEdit.MenuManager = this.barManager1;
            this.CostValueExVatSpinEdit.Name = "CostValueExVatSpinEdit";
            this.CostValueExVatSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostValueExVatSpinEdit.Properties.Mask.EditMask = "c";
            this.CostValueExVatSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostValueExVatSpinEdit.Properties.ReadOnly = true;
            this.CostValueExVatSpinEdit.Size = new System.Drawing.Size(167, 20);
            this.CostValueExVatSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostValueExVatSpinEdit.TabIndex = 47;
            this.CostValueExVatSpinEdit.TabStop = false;
            // 
            // SellPerUnitVatRateSpinEdit
            // 
            this.SellPerUnitVatRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "SellPerUnitVatRate", true));
            this.SellPerUnitVatRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellPerUnitVatRateSpinEdit.Location = new System.Drawing.Point(459, 437);
            this.SellPerUnitVatRateSpinEdit.MenuManager = this.barManager1;
            this.SellPerUnitVatRateSpinEdit.Name = "SellPerUnitVatRateSpinEdit";
            this.SellPerUnitVatRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellPerUnitVatRateSpinEdit.Properties.Mask.EditMask = "P";
            this.SellPerUnitVatRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellPerUnitVatRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.SellPerUnitVatRateSpinEdit.Size = new System.Drawing.Size(162, 20);
            this.SellPerUnitVatRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellPerUnitVatRateSpinEdit.TabIndex = 48;
            this.SellPerUnitVatRateSpinEdit.EditValueChanged += new System.EventHandler(this.SellPerUnitVatRateSpinEdit_EditValueChanged);
            this.SellPerUnitVatRateSpinEdit.Validated += new System.EventHandler(this.SellPerUnitVatRateSpinEdit_Validated);
            // 
            // CostPerUnitVatRateSpinEdit
            // 
            this.CostPerUnitVatRateSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "CostPerUnitVatRate", true));
            this.CostPerUnitVatRateSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostPerUnitVatRateSpinEdit.Location = new System.Drawing.Point(153, 437);
            this.CostPerUnitVatRateSpinEdit.MenuManager = this.barManager1;
            this.CostPerUnitVatRateSpinEdit.Name = "CostPerUnitVatRateSpinEdit";
            this.CostPerUnitVatRateSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostPerUnitVatRateSpinEdit.Properties.Mask.EditMask = "P";
            this.CostPerUnitVatRateSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostPerUnitVatRateSpinEdit.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.CostPerUnitVatRateSpinEdit.Size = new System.Drawing.Size(167, 20);
            this.CostPerUnitVatRateSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostPerUnitVatRateSpinEdit.TabIndex = 47;
            this.CostPerUnitVatRateSpinEdit.EditValueChanged += new System.EventHandler(this.CostPerUnitVatRateSpinEdit_EditValueChanged);
            this.CostPerUnitVatRateSpinEdit.Validated += new System.EventHandler(this.CostPerUnitVatRateSpinEdit_Validated);
            // 
            // SellPerUnitExVatSpinEdit
            // 
            this.SellPerUnitExVatSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "SellPerUnitExVat", true));
            this.SellPerUnitExVatSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellPerUnitExVatSpinEdit.Location = new System.Drawing.Point(459, 413);
            this.SellPerUnitExVatSpinEdit.MenuManager = this.barManager1;
            this.SellPerUnitExVatSpinEdit.Name = "SellPerUnitExVatSpinEdit";
            this.SellPerUnitExVatSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellPerUnitExVatSpinEdit.Properties.Mask.EditMask = "c";
            this.SellPerUnitExVatSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellPerUnitExVatSpinEdit.Size = new System.Drawing.Size(162, 20);
            this.SellPerUnitExVatSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellPerUnitExVatSpinEdit.TabIndex = 47;
            this.SellPerUnitExVatSpinEdit.EditValueChanged += new System.EventHandler(this.SellPerUnitExVatSpinEdit_EditValueChanged);
            this.SellPerUnitExVatSpinEdit.Validated += new System.EventHandler(this.SellPerUnitExVatSpinEdit_Validated);
            // 
            // CostPerUnitExVatSpinEdit
            // 
            this.CostPerUnitExVatSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "CostPerUnitExVat", true));
            this.CostPerUnitExVatSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostPerUnitExVatSpinEdit.Location = new System.Drawing.Point(153, 413);
            this.CostPerUnitExVatSpinEdit.MenuManager = this.barManager1;
            this.CostPerUnitExVatSpinEdit.Name = "CostPerUnitExVatSpinEdit";
            this.CostPerUnitExVatSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostPerUnitExVatSpinEdit.Properties.Mask.EditMask = "c";
            this.CostPerUnitExVatSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostPerUnitExVatSpinEdit.Size = new System.Drawing.Size(167, 20);
            this.CostPerUnitExVatSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostPerUnitExVatSpinEdit.TabIndex = 46;
            this.CostPerUnitExVatSpinEdit.EditValueChanged += new System.EventHandler(this.CostPerUnitExVatSpinEdit_EditValueChanged);
            this.CostPerUnitExVatSpinEdit.Validated += new System.EventHandler(this.CostPerUnitExVatSpinEdit_Validated);
            // 
            // SellUnitDescriptorIDGridLookUpEdit
            // 
            this.SellUnitDescriptorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "SellUnitDescriptorID", true));
            this.SellUnitDescriptorIDGridLookUpEdit.Location = new System.Drawing.Point(459, 389);
            this.SellUnitDescriptorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SellUnitDescriptorIDGridLookUpEdit.Name = "SellUnitDescriptorIDGridLookUpEdit";
            this.SellUnitDescriptorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellUnitDescriptorIDGridLookUpEdit.Properties.DataSource = this.sp06101OMWorkUnitTypesPicklistBindingSource;
            this.SellUnitDescriptorIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.SellUnitDescriptorIDGridLookUpEdit.Properties.NullText = "";
            this.SellUnitDescriptorIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.SellUnitDescriptorIDGridLookUpEdit.Properties.View = this.gridView1;
            this.SellUnitDescriptorIDGridLookUpEdit.Size = new System.Drawing.Size(162, 20);
            this.SellUnitDescriptorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SellUnitDescriptorIDGridLookUpEdit.TabIndex = 47;
            // 
            // sp06101OMWorkUnitTypesPicklistBindingSource
            // 
            this.sp06101OMWorkUnitTypesPicklistBindingSource.DataMember = "sp06101_OM_Work_Unit_Types_Picklist";
            this.sp06101OMWorkUnitTypesPicklistBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.gridColumn1;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Unit Descriptor";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // SellUnitsUsedSpinEdit
            // 
            this.SellUnitsUsedSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "SellUnitsUsed", true));
            this.SellUnitsUsedSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SellUnitsUsedSpinEdit.Location = new System.Drawing.Point(459, 365);
            this.SellUnitsUsedSpinEdit.MenuManager = this.barManager1;
            this.SellUnitsUsedSpinEdit.Name = "SellUnitsUsedSpinEdit";
            this.SellUnitsUsedSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SellUnitsUsedSpinEdit.Properties.Mask.EditMask = "n2";
            this.SellUnitsUsedSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SellUnitsUsedSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            131072});
            this.SellUnitsUsedSpinEdit.Size = new System.Drawing.Size(162, 20);
            this.SellUnitsUsedSpinEdit.StyleController = this.dataLayoutControl1;
            this.SellUnitsUsedSpinEdit.TabIndex = 46;
            this.SellUnitsUsedSpinEdit.EditValueChanged += new System.EventHandler(this.SellUnitsUsedSpinEdit_EditValueChanged);
            this.SellUnitsUsedSpinEdit.Validated += new System.EventHandler(this.SellUnitsUsedSpinEdit_Validated);
            // 
            // CostUnitDescriptorIDGridLookUpEdit
            // 
            this.CostUnitDescriptorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "CostUnitDescriptorID", true));
            this.CostUnitDescriptorIDGridLookUpEdit.Location = new System.Drawing.Point(153, 389);
            this.CostUnitDescriptorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.CostUnitDescriptorIDGridLookUpEdit.Name = "CostUnitDescriptorIDGridLookUpEdit";
            this.CostUnitDescriptorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostUnitDescriptorIDGridLookUpEdit.Properties.DataSource = this.sp06101OMWorkUnitTypesPicklistBindingSource;
            this.CostUnitDescriptorIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.CostUnitDescriptorIDGridLookUpEdit.Properties.NullText = "";
            this.CostUnitDescriptorIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.CostUnitDescriptorIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.CostUnitDescriptorIDGridLookUpEdit.Size = new System.Drawing.Size(167, 20);
            this.CostUnitDescriptorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.CostUnitDescriptorIDGridLookUpEdit.TabIndex = 46;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.colID1;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Unit Descriptor";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "RecordOrder";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // CostUnitsUsedSpinEdit
            // 
            this.CostUnitsUsedSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "CostUnitsUsed", true));
            this.CostUnitsUsedSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CostUnitsUsedSpinEdit.Location = new System.Drawing.Point(153, 365);
            this.CostUnitsUsedSpinEdit.MenuManager = this.barManager1;
            this.CostUnitsUsedSpinEdit.Name = "CostUnitsUsedSpinEdit";
            this.CostUnitsUsedSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostUnitsUsedSpinEdit.Properties.Mask.EditMask = "n2";
            this.CostUnitsUsedSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CostUnitsUsedSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            131072});
            this.CostUnitsUsedSpinEdit.Size = new System.Drawing.Size(167, 20);
            this.CostUnitsUsedSpinEdit.StyleController = this.dataLayoutControl1;
            this.CostUnitsUsedSpinEdit.TabIndex = 45;
            this.CostUnitsUsedSpinEdit.EditValueChanged += new System.EventHandler(this.CostUnitsUsedSpinEdit_EditValueChanged);
            this.CostUnitsUsedSpinEdit.Validated += new System.EventHandler(this.CostUnitsUsedSpinEdit_Validated);
            // 
            // EquipmentNameButtonEdit
            // 
            this.EquipmentNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "EquipmentName", true));
            this.EquipmentNameButtonEdit.EditValue = "";
            this.EquipmentNameButtonEdit.Location = new System.Drawing.Point(117, 155);
            this.EquipmentNameButtonEdit.MenuManager = this.barManager1;
            this.EquipmentNameButtonEdit.Name = "EquipmentNameButtonEdit";
            this.EquipmentNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click me to open the Select Equipment screen", "choose", null, true)});
            this.EquipmentNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.EquipmentNameButtonEdit.Size = new System.Drawing.Size(540, 20);
            this.EquipmentNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.EquipmentNameButtonEdit.TabIndex = 13;
            this.EquipmentNameButtonEdit.TabStop = false;
            this.EquipmentNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.EquipmentNameButtonEdit_ButtonClick);
            this.EquipmentNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.EquipmentNameButtonEdit_Validating);
            // 
            // EquipmentUsedIDTextEdit
            // 
            this.EquipmentUsedIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "EquipmentUsedID", true));
            this.EquipmentUsedIDTextEdit.Location = new System.Drawing.Point(117, 237);
            this.EquipmentUsedIDTextEdit.MenuManager = this.barManager1;
            this.EquipmentUsedIDTextEdit.Name = "EquipmentUsedIDTextEdit";
            this.EquipmentUsedIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EquipmentUsedIDTextEdit, true);
            this.EquipmentUsedIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EquipmentUsedIDTextEdit, optionsSpelling20);
            this.EquipmentUsedIDTextEdit.StyleController = this.dataLayoutControl1;
            this.EquipmentUsedIDTextEdit.TabIndex = 42;
            // 
            // JobIDTextEdit
            // 
            this.JobIDTextEdit.Location = new System.Drawing.Point(117, 157);
            this.JobIDTextEdit.MenuManager = this.barManager1;
            this.JobIDTextEdit.Name = "JobIDTextEdit";
            this.JobIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobIDTextEdit, true);
            this.JobIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobIDTextEdit, optionsSpelling21);
            this.JobIDTextEdit.StyleController = this.dataLayoutControl1;
            this.JobIDTextEdit.TabIndex = 44;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 331);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(597, 222);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling22);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 35;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp06187OMJobEquipmentEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(117, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // JobTypeJobSubTypeDescriptionButtonEdit
            // 
            this.JobTypeJobSubTypeDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "JobTypeJobSubTypeDescription", true));
            this.JobTypeJobSubTypeDescriptionButtonEdit.EditValue = "";
            this.JobTypeJobSubTypeDescriptionButtonEdit.Location = new System.Drawing.Point(117, 107);
            this.JobTypeJobSubTypeDescriptionButtonEdit.MenuManager = this.barManager1;
            this.JobTypeJobSubTypeDescriptionButtonEdit.Name = "JobTypeJobSubTypeDescriptionButtonEdit";
            this.JobTypeJobSubTypeDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to open the Select Job screen", "choose", null, true)});
            this.JobTypeJobSubTypeDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.JobTypeJobSubTypeDescriptionButtonEdit.Size = new System.Drawing.Size(540, 20);
            this.JobTypeJobSubTypeDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.JobTypeJobSubTypeDescriptionButtonEdit.TabIndex = 6;
            this.JobTypeJobSubTypeDescriptionButtonEdit.TabStop = false;
            this.JobTypeJobSubTypeDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.JobTypeJobSubTypeDescriptionButtonEdit_ButtonClick);
            this.JobTypeJobSubTypeDescriptionButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.JobTypeJobSubTypeDescriptionButtonEdit_Validating);
            // 
            // EquipmentIDTextEdit
            // 
            this.EquipmentIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06187OMJobEquipmentEditBindingSource, "EquipmentID", true));
            this.EquipmentIDTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.EquipmentIDTextEdit.Location = new System.Drawing.Point(117, 261);
            this.EquipmentIDTextEdit.MenuManager = this.barManager1;
            this.EquipmentIDTextEdit.Name = "EquipmentIDTextEdit";
            this.EquipmentIDTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.EquipmentIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.EquipmentIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EquipmentIDTextEdit, true);
            this.EquipmentIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EquipmentIDTextEdit, optionsSpelling23);
            this.EquipmentIDTextEdit.StyleController = this.dataLayoutControl1;
            this.EquipmentIDTextEdit.TabIndex = 27;
            // 
            // ItemForClientContractID
            // 
            this.ItemForClientContractID.Control = this.ClientContractIDTextEdit;
            this.ItemForClientContractID.CustomizationFormText = "Client Contract ID:";
            this.ItemForClientContractID.Location = new System.Drawing.Point(0, 225);
            this.ItemForClientContractID.Name = "ItemForClientContractID";
            this.ItemForClientContractID.Size = new System.Drawing.Size(649, 24);
            this.ItemForClientContractID.Text = "Client Contract ID:";
            this.ItemForClientContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientIDTextEdit;
            this.ItemForClientID.CustomizationFormText = "Client ID:";
            this.ItemForClientID.Location = new System.Drawing.Point(0, 225);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(649, 24);
            this.ItemForClientID.Text = "Client ID:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.Control = this.ClientNameTextEdit;
            this.ItemForClientName.CustomizationFormText = "Client Name:";
            this.ItemForClientName.Location = new System.Drawing.Point(0, 225);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(649, 24);
            this.ItemForClientName.Text = "Client Name:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobID
            // 
            this.ItemForJobID.Control = this.JobIDTextEdit;
            this.ItemForJobID.CustomizationFormText = "Job ID:";
            this.ItemForJobID.Location = new System.Drawing.Point(0, 225);
            this.ItemForJobID.Name = "ItemForJobID";
            this.ItemForJobID.Size = new System.Drawing.Size(649, 24);
            this.ItemForJobID.Text = "Job ID:";
            this.ItemForJobID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForVisitID
            // 
            this.ItemForVisitID.Control = this.VisitIDTextEdit;
            this.ItemForVisitID.CustomizationFormText = "Visit ID:";
            this.ItemForVisitID.Location = new System.Drawing.Point(324, 321);
            this.ItemForVisitID.Name = "ItemForVisitID";
            this.ItemForVisitID.Size = new System.Drawing.Size(325, 24);
            this.ItemForVisitID.Text = "Visit ID:";
            this.ItemForVisitID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSitePostcode
            // 
            this.ItemForSitePostcode.Control = this.SitePostcodeTextEdit;
            this.ItemForSitePostcode.CustomizationFormText = "Site Postcode:";
            this.ItemForSitePostcode.Location = new System.Drawing.Point(324, 321);
            this.ItemForSitePostcode.Name = "ItemForSitePostcode";
            this.ItemForSitePostcode.Size = new System.Drawing.Size(325, 48);
            this.ItemForSitePostcode.Text = "Site Postcode:";
            this.ItemForSitePostcode.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteLocationX
            // 
            this.ItemForSiteLocationX.Control = this.SiteLocationXTextEdit;
            this.ItemForSiteLocationX.CustomizationFormText = "Site Latitude:";
            this.ItemForSiteLocationX.Location = new System.Drawing.Point(324, 321);
            this.ItemForSiteLocationX.Name = "ItemForSiteLocationX";
            this.ItemForSiteLocationX.Size = new System.Drawing.Size(325, 72);
            this.ItemForSiteLocationX.Text = "Site Latitude:";
            this.ItemForSiteLocationX.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteLocationY
            // 
            this.ItemForSiteLocationY.Control = this.SiteLocationYTextEdit;
            this.ItemForSiteLocationY.CustomizationFormText = "Site Longitude:";
            this.ItemForSiteLocationY.Location = new System.Drawing.Point(324, 321);
            this.ItemForSiteLocationY.Name = "ItemForSiteLocationY";
            this.ItemForSiteLocationY.Size = new System.Drawing.Size(325, 96);
            this.ItemForSiteLocationY.Text = "Site Longitude:";
            this.ItemForSiteLocationY.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteID
            // 
            this.ItemForSiteID.Control = this.SiteIDTextEdit;
            this.ItemForSiteID.CustomizationFormText = "Site ID:";
            this.ItemForSiteID.Location = new System.Drawing.Point(0, 393);
            this.ItemForSiteID.Name = "ItemForSiteID";
            this.ItemForSiteID.Size = new System.Drawing.Size(649, 24);
            this.ItemForSiteID.Text = "Site ID:";
            this.ItemForSiteID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteContractID
            // 
            this.ItemForSiteContractID.Control = this.SiteContractIDTextEdit;
            this.ItemForSiteContractID.CustomizationFormText = "Site Contract ID:";
            this.ItemForSiteContractID.Location = new System.Drawing.Point(0, 369);
            this.ItemForSiteContractID.Name = "ItemForSiteContractID";
            this.ItemForSiteContractID.Size = new System.Drawing.Size(649, 24);
            this.ItemForSiteContractID.Text = "Site Contract ID:";
            this.ItemForSiteContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForPurchaseOrderID
            // 
            this.ItemForPurchaseOrderID.Control = this.PurchaseOrderIDTextEdit;
            this.ItemForPurchaseOrderID.CustomizationFormText = "Purchase Order ID:";
            this.ItemForPurchaseOrderID.Location = new System.Drawing.Point(0, 345);
            this.ItemForPurchaseOrderID.Name = "ItemForPurchaseOrderID";
            this.ItemForPurchaseOrderID.Size = new System.Drawing.Size(649, 24);
            this.ItemForPurchaseOrderID.Text = "Purchase Order ID:";
            this.ItemForPurchaseOrderID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobTypeID
            // 
            this.ItemForJobTypeID.Control = this.JobTypeIDTextEdit;
            this.ItemForJobTypeID.CustomizationFormText = "Job Type ID:";
            this.ItemForJobTypeID.Location = new System.Drawing.Point(0, 297);
            this.ItemForJobTypeID.Name = "ItemForJobTypeID";
            this.ItemForJobTypeID.Size = new System.Drawing.Size(649, 24);
            this.ItemForJobTypeID.Text = "Job Type ID:";
            this.ItemForJobTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobTypeDescription
            // 
            this.ItemForJobTypeDescription.Control = this.JobTypeDescriptionTextEdit;
            this.ItemForJobTypeDescription.CustomizationFormText = "Job Type:";
            this.ItemForJobTypeDescription.Location = new System.Drawing.Point(0, 273);
            this.ItemForJobTypeDescription.Name = "ItemForJobTypeDescription";
            this.ItemForJobTypeDescription.Size = new System.Drawing.Size(649, 24);
            this.ItemForJobTypeDescription.Text = "Job Type:";
            this.ItemForJobTypeDescription.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobSubTypeID
            // 
            this.ItemForJobSubTypeID.Control = this.JobSubTypeIDTextEdit;
            this.ItemForJobSubTypeID.CustomizationFormText = "Job Sub-Type ID:";
            this.ItemForJobSubTypeID.Location = new System.Drawing.Point(0, 249);
            this.ItemForJobSubTypeID.Name = "ItemForJobSubTypeID";
            this.ItemForJobSubTypeID.Size = new System.Drawing.Size(649, 24);
            this.ItemForJobSubTypeID.Text = "Job Sub-Type ID:";
            this.ItemForJobSubTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobSubTypeDescription
            // 
            this.ItemForJobSubTypeDescription.Control = this.JobSubTypeDescriptionTextEdit;
            this.ItemForJobSubTypeDescription.CustomizationFormText = "Job Sub-Type:";
            this.ItemForJobSubTypeDescription.Location = new System.Drawing.Point(0, 225);
            this.ItemForJobSubTypeDescription.Name = "ItemForJobSubTypeDescription";
            this.ItemForJobSubTypeDescription.Size = new System.Drawing.Size(649, 24);
            this.ItemForJobSubTypeDescription.Text = "Job Sub-Type:";
            this.ItemForJobSubTypeDescription.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForEquipmentID
            // 
            this.ItemForEquipmentID.Control = this.EquipmentIDTextEdit;
            this.ItemForEquipmentID.CustomizationFormText = "Equipment ID:";
            this.ItemForEquipmentID.Location = new System.Drawing.Point(0, 249);
            this.ItemForEquipmentID.Name = "ItemForEquipmentID";
            this.ItemForEquipmentID.Size = new System.Drawing.Size(649, 24);
            this.ItemForEquipmentID.Text = "Equipment ID:";
            this.ItemForEquipmentID.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForEquipmentUsedID
            // 
            this.ItemForEquipmentUsedID.Control = this.EquipmentUsedIDTextEdit;
            this.ItemForEquipmentUsedID.CustomizationFormText = "Equipment Used ID:";
            this.ItemForEquipmentUsedID.Location = new System.Drawing.Point(0, 225);
            this.ItemForEquipmentUsedID.Name = "ItemForEquipmentUsedID";
            this.ItemForEquipmentUsedID.Size = new System.Drawing.Size(649, 24);
            this.ItemForEquipmentUsedID.Text = "Equipment Used ID:";
            this.ItemForEquipmentUsedID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(669, 599);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForJobTypeJobSubTypeDescription,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem4,
            this.layoutControlGroup6,
            this.ItemForClientNameContractDescription,
            this.ItemForSiteName,
            this.ItemForVisitNumber,
            this.emptySpaceItem6,
            this.ItemForStartDateTime,
            this.ItemForEndDateTime,
            this.ItemForOwnerName,
            this.ItemForOwnerType,
            this.ItemForEquipmentName,
            this.emptySpaceItem7,
            this.ItemForJobExpectedStartDate});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(649, 569);
            // 
            // ItemForJobTypeJobSubTypeDescription
            // 
            this.ItemForJobTypeJobSubTypeDescription.AllowHide = false;
            this.ItemForJobTypeJobSubTypeDescription.Control = this.JobTypeJobSubTypeDescriptionButtonEdit;
            this.ItemForJobTypeJobSubTypeDescription.CustomizationFormText = "Job Description:";
            this.ItemForJobTypeJobSubTypeDescription.Location = new System.Drawing.Point(0, 95);
            this.ItemForJobTypeJobSubTypeDescription.Name = "ItemForJobTypeJobSubTypeDescription";
            this.ItemForJobTypeJobSubTypeDescription.Size = new System.Drawing.Size(649, 24);
            this.ItemForJobTypeJobSubTypeDescription.Text = "Job Description:";
            this.ItemForJobTypeJobSubTypeDescription.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(105, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(105, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(105, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(282, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(367, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(105, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 239);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(649, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 249);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(649, 320);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layGrpAddress;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(625, 274);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layGrpAddress,
            this.layoutControlGroup4});
            // 
            // layGrpAddress
            // 
            this.layGrpAddress.CustomizationFormText = "Address";
            this.layGrpAddress.ExpandButtonVisible = true;
            this.layGrpAddress.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layGrpAddress.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup7,
            this.splitterItem1,
            this.emptySpaceItem5});
            this.layGrpAddress.Location = new System.Drawing.Point(0, 0);
            this.layGrpAddress.Name = "layGrpAddress";
            this.layGrpAddress.Size = new System.Drawing.Size(601, 226);
            this.layGrpAddress.Text = "Details";
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Costs";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCostUnitsUsed,
            this.ItemForCostUnitDescriptorID,
            this.ItemForCostPerUnitExVat,
            this.ItemForCostPerUnitVatRate,
            this.ItemForCostValueExVat,
            this.simpleSeparator1,
            this.ItemForCostValueVat,
            this.ItemForCostValueTotal});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(300, 216);
            this.layoutControlGroup5.Text = "Costs";
            // 
            // ItemForCostUnitsUsed
            // 
            this.ItemForCostUnitsUsed.Control = this.CostUnitsUsedSpinEdit;
            this.ItemForCostUnitsUsed.CustomizationFormText = "Cost Units Used:";
            this.ItemForCostUnitsUsed.Location = new System.Drawing.Point(0, 0);
            this.ItemForCostUnitsUsed.Name = "ItemForCostUnitsUsed";
            this.ItemForCostUnitsUsed.Size = new System.Drawing.Size(276, 24);
            this.ItemForCostUnitsUsed.Text = "Units Used:";
            this.ItemForCostUnitsUsed.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForCostUnitDescriptorID
            // 
            this.ItemForCostUnitDescriptorID.Control = this.CostUnitDescriptorIDGridLookUpEdit;
            this.ItemForCostUnitDescriptorID.CustomizationFormText = "Cost Unit Descriptor:";
            this.ItemForCostUnitDescriptorID.Location = new System.Drawing.Point(0, 24);
            this.ItemForCostUnitDescriptorID.Name = "ItemForCostUnitDescriptorID";
            this.ItemForCostUnitDescriptorID.Size = new System.Drawing.Size(276, 24);
            this.ItemForCostUnitDescriptorID.Text = "Unit Descriptor:";
            this.ItemForCostUnitDescriptorID.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForCostPerUnitExVat
            // 
            this.ItemForCostPerUnitExVat.Control = this.CostPerUnitExVatSpinEdit;
            this.ItemForCostPerUnitExVat.CustomizationFormText = "Cost Per Unit Ex VAT:";
            this.ItemForCostPerUnitExVat.Location = new System.Drawing.Point(0, 48);
            this.ItemForCostPerUnitExVat.Name = "ItemForCostPerUnitExVat";
            this.ItemForCostPerUnitExVat.Size = new System.Drawing.Size(276, 24);
            this.ItemForCostPerUnitExVat.Text = "Unit Ex VAT:";
            this.ItemForCostPerUnitExVat.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForCostPerUnitVatRate
            // 
            this.ItemForCostPerUnitVatRate.Control = this.CostPerUnitVatRateSpinEdit;
            this.ItemForCostPerUnitVatRate.CustomizationFormText = "Cost Per Unit VAT Rate:";
            this.ItemForCostPerUnitVatRate.Location = new System.Drawing.Point(0, 72);
            this.ItemForCostPerUnitVatRate.Name = "ItemForCostPerUnitVatRate";
            this.ItemForCostPerUnitVatRate.Size = new System.Drawing.Size(276, 24);
            this.ItemForCostPerUnitVatRate.Text = "Unit VAT Rate:";
            this.ItemForCostPerUnitVatRate.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForCostValueExVat
            // 
            this.ItemForCostValueExVat.Control = this.CostValueExVatSpinEdit;
            this.ItemForCostValueExVat.CustomizationFormText = "Cost Value Ex VAT:";
            this.ItemForCostValueExVat.Location = new System.Drawing.Point(0, 98);
            this.ItemForCostValueExVat.Name = "ItemForCostValueExVat";
            this.ItemForCostValueExVat.Size = new System.Drawing.Size(276, 24);
            this.ItemForCostValueExVat.Text = "Value Ex VAT:";
            this.ItemForCostValueExVat.TextSize = new System.Drawing.Size(102, 13);
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 96);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(276, 2);
            // 
            // ItemForCostValueVat
            // 
            this.ItemForCostValueVat.Control = this.CostValueVatSpinEdit;
            this.ItemForCostValueVat.CustomizationFormText = "Cost Value VAT:";
            this.ItemForCostValueVat.Location = new System.Drawing.Point(0, 122);
            this.ItemForCostValueVat.Name = "ItemForCostValueVat";
            this.ItemForCostValueVat.Size = new System.Drawing.Size(276, 24);
            this.ItemForCostValueVat.Text = "Value VAT:";
            this.ItemForCostValueVat.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForCostValueTotal
            // 
            this.ItemForCostValueTotal.Control = this.CostValueTotalSpinEdit;
            this.ItemForCostValueTotal.CustomizationFormText = "Cost Value Total:";
            this.ItemForCostValueTotal.Location = new System.Drawing.Point(0, 146);
            this.ItemForCostValueTotal.Name = "ItemForCostValueTotal";
            this.ItemForCostValueTotal.Size = new System.Drawing.Size(276, 24);
            this.ItemForCostValueTotal.Text = "Value Total:";
            this.ItemForCostValueTotal.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Sell";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSellUnitsUsed,
            this.ItemForSellUnitDescriptorID,
            this.ItemForSellPerUnitExVat,
            this.ItemForSellPerUnitVatRate,
            this.ItemForSellValueExVat,
            this.simpleSeparator2,
            this.ItemForSellValueVat,
            this.ItemForSellValueTotal});
            this.layoutControlGroup7.Location = new System.Drawing.Point(306, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(295, 216);
            this.layoutControlGroup7.Text = "Sell";
            // 
            // ItemForSellUnitsUsed
            // 
            this.ItemForSellUnitsUsed.Control = this.SellUnitsUsedSpinEdit;
            this.ItemForSellUnitsUsed.CustomizationFormText = "Sell Units Used";
            this.ItemForSellUnitsUsed.Location = new System.Drawing.Point(0, 0);
            this.ItemForSellUnitsUsed.Name = "ItemForSellUnitsUsed";
            this.ItemForSellUnitsUsed.Size = new System.Drawing.Size(271, 24);
            this.ItemForSellUnitsUsed.Text = "Units Used";
            this.ItemForSellUnitsUsed.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSellUnitDescriptorID
            // 
            this.ItemForSellUnitDescriptorID.Control = this.SellUnitDescriptorIDGridLookUpEdit;
            this.ItemForSellUnitDescriptorID.CustomizationFormText = "Sell Unit Descriptor:";
            this.ItemForSellUnitDescriptorID.Location = new System.Drawing.Point(0, 24);
            this.ItemForSellUnitDescriptorID.Name = "ItemForSellUnitDescriptorID";
            this.ItemForSellUnitDescriptorID.Size = new System.Drawing.Size(271, 24);
            this.ItemForSellUnitDescriptorID.Text = "Unit Description:";
            this.ItemForSellUnitDescriptorID.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSellPerUnitExVat
            // 
            this.ItemForSellPerUnitExVat.Control = this.SellPerUnitExVatSpinEdit;
            this.ItemForSellPerUnitExVat.CustomizationFormText = "Sell PerUnit Ex VAT:";
            this.ItemForSellPerUnitExVat.Location = new System.Drawing.Point(0, 48);
            this.ItemForSellPerUnitExVat.Name = "ItemForSellPerUnitExVat";
            this.ItemForSellPerUnitExVat.Size = new System.Drawing.Size(271, 24);
            this.ItemForSellPerUnitExVat.Text = "Unit Ex VAT:";
            this.ItemForSellPerUnitExVat.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSellPerUnitVatRate
            // 
            this.ItemForSellPerUnitVatRate.Control = this.SellPerUnitVatRateSpinEdit;
            this.ItemForSellPerUnitVatRate.CustomizationFormText = "Sell Per Unit VAT Rate:";
            this.ItemForSellPerUnitVatRate.Location = new System.Drawing.Point(0, 72);
            this.ItemForSellPerUnitVatRate.Name = "ItemForSellPerUnitVatRate";
            this.ItemForSellPerUnitVatRate.Size = new System.Drawing.Size(271, 24);
            this.ItemForSellPerUnitVatRate.Text = "Unit VAT Rate:";
            this.ItemForSellPerUnitVatRate.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSellValueExVat
            // 
            this.ItemForSellValueExVat.Control = this.SellValueExVatSpinEdit;
            this.ItemForSellValueExVat.CustomizationFormText = "Sell Value Ex VAT:";
            this.ItemForSellValueExVat.Location = new System.Drawing.Point(0, 98);
            this.ItemForSellValueExVat.Name = "ItemForSellValueExVat";
            this.ItemForSellValueExVat.Size = new System.Drawing.Size(271, 24);
            this.ItemForSellValueExVat.Text = "Value Ex VAT:";
            this.ItemForSellValueExVat.TextSize = new System.Drawing.Size(102, 13);
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.CustomizationFormText = "simpleSeparator2";
            this.simpleSeparator2.Location = new System.Drawing.Point(0, 96);
            this.simpleSeparator2.Name = "simpleSeparator2";
            this.simpleSeparator2.Size = new System.Drawing.Size(271, 2);
            // 
            // ItemForSellValueVat
            // 
            this.ItemForSellValueVat.Control = this.SellValueVatSpinEdit;
            this.ItemForSellValueVat.CustomizationFormText = "Sell Value VAT:";
            this.ItemForSellValueVat.Location = new System.Drawing.Point(0, 122);
            this.ItemForSellValueVat.Name = "ItemForSellValueVat";
            this.ItemForSellValueVat.Size = new System.Drawing.Size(271, 24);
            this.ItemForSellValueVat.Text = "Value VAT:";
            this.ItemForSellValueVat.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSellValueTotal
            // 
            this.ItemForSellValueTotal.Control = this.SellValueTotalSpinEdit;
            this.ItemForSellValueTotal.CustomizationFormText = "Sell Value Total:";
            this.ItemForSellValueTotal.Location = new System.Drawing.Point(0, 146);
            this.ItemForSellValueTotal.Name = "ItemForSellValueTotal";
            this.ItemForSellValueTotal.Size = new System.Drawing.Size(271, 24);
            this.ItemForSellValueTotal.Text = "Value Total:";
            this.ItemForSellValueTotal.TextSize = new System.Drawing.Size(102, 13);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(300, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(6, 216);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 216);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(601, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImage")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(601, 226);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Comments:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(601, 226);
            this.ItemForRemarks.Text = "Comments:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // ItemForClientNameContractDescription
            // 
            this.ItemForClientNameContractDescription.Control = this.ClientNameContractDescriptionTextEdit;
            this.ItemForClientNameContractDescription.CustomizationFormText = "Client \\ Contract:";
            this.ItemForClientNameContractDescription.Location = new System.Drawing.Point(0, 23);
            this.ItemForClientNameContractDescription.Name = "ItemForClientNameContractDescription";
            this.ItemForClientNameContractDescription.Size = new System.Drawing.Size(649, 24);
            this.ItemForClientNameContractDescription.Text = "Client \\ Contract:";
            this.ItemForClientNameContractDescription.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSiteName
            // 
            this.ItemForSiteName.Control = this.SiteNameTextEdit;
            this.ItemForSiteName.CustomizationFormText = "Site Name:";
            this.ItemForSiteName.Location = new System.Drawing.Point(0, 47);
            this.ItemForSiteName.Name = "ItemForSiteName";
            this.ItemForSiteName.Size = new System.Drawing.Size(649, 24);
            this.ItemForSiteName.Text = "Site Name:";
            this.ItemForSiteName.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForVisitNumber
            // 
            this.ItemForVisitNumber.Control = this.VisitNumberTextEdit;
            this.ItemForVisitNumber.CustomizationFormText = "Visit Number:";
            this.ItemForVisitNumber.Location = new System.Drawing.Point(0, 71);
            this.ItemForVisitNumber.Name = "ItemForVisitNumber";
            this.ItemForVisitNumber.Size = new System.Drawing.Size(649, 24);
            this.ItemForVisitNumber.Text = "Visit Number:";
            this.ItemForVisitNumber.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(300, 191);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(349, 48);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForStartDateTime
            // 
            this.ItemForStartDateTime.Control = this.StartDateTimeDateEdit;
            this.ItemForStartDateTime.CustomizationFormText = "Start Date:";
            this.ItemForStartDateTime.Location = new System.Drawing.Point(0, 191);
            this.ItemForStartDateTime.Name = "ItemForStartDateTime";
            this.ItemForStartDateTime.Size = new System.Drawing.Size(300, 24);
            this.ItemForStartDateTime.Text = "Start Date:";
            this.ItemForStartDateTime.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForEndDateTime
            // 
            this.ItemForEndDateTime.Control = this.EndDateTimeDateEdit;
            this.ItemForEndDateTime.CustomizationFormText = "End Date:";
            this.ItemForEndDateTime.Location = new System.Drawing.Point(0, 215);
            this.ItemForEndDateTime.Name = "ItemForEndDateTime";
            this.ItemForEndDateTime.Size = new System.Drawing.Size(300, 24);
            this.ItemForEndDateTime.Text = "End Date:";
            this.ItemForEndDateTime.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForOwnerName
            // 
            this.ItemForOwnerName.Control = this.OwnerNameTextEdit;
            this.ItemForOwnerName.CustomizationFormText = "Equipment Owner:";
            this.ItemForOwnerName.Location = new System.Drawing.Point(0, 167);
            this.ItemForOwnerName.MaxSize = new System.Drawing.Size(300, 24);
            this.ItemForOwnerName.MinSize = new System.Drawing.Size(300, 24);
            this.ItemForOwnerName.Name = "ItemForOwnerName";
            this.ItemForOwnerName.Size = new System.Drawing.Size(300, 24);
            this.ItemForOwnerName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForOwnerName.Text = "Equipment Owner:";
            this.ItemForOwnerName.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForOwnerType
            // 
            this.ItemForOwnerType.Control = this.OwnerTypeTextEdit;
            this.ItemForOwnerType.CustomizationFormText = "Owner Type:";
            this.ItemForOwnerType.Location = new System.Drawing.Point(300, 167);
            this.ItemForOwnerType.Name = "ItemForOwnerType";
            this.ItemForOwnerType.Size = new System.Drawing.Size(349, 24);
            this.ItemForOwnerType.Text = "Owner Type:";
            this.ItemForOwnerType.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForEquipmentName
            // 
            this.ItemForEquipmentName.Control = this.EquipmentNameButtonEdit;
            this.ItemForEquipmentName.CustomizationFormText = "Equipment Name:";
            this.ItemForEquipmentName.Location = new System.Drawing.Point(0, 143);
            this.ItemForEquipmentName.Name = "ItemForEquipmentName";
            this.ItemForEquipmentName.Size = new System.Drawing.Size(649, 24);
            this.ItemForEquipmentName.Text = "Equipment Name:";
            this.ItemForEquipmentName.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(300, 119);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(349, 24);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForJobExpectedStartDate
            // 
            this.ItemForJobExpectedStartDate.Control = this.JobExpectedStartDateTextEdit;
            this.ItemForJobExpectedStartDate.CustomizationFormText = "Job Expected Start Date:";
            this.ItemForJobExpectedStartDate.Location = new System.Drawing.Point(0, 119);
            this.ItemForJobExpectedStartDate.MaxSize = new System.Drawing.Size(300, 24);
            this.ItemForJobExpectedStartDate.MinSize = new System.Drawing.Size(300, 24);
            this.ItemForJobExpectedStartDate.Name = "ItemForJobExpectedStartDate";
            this.ItemForJobExpectedStartDate.Size = new System.Drawing.Size(300, 24);
            this.ItemForJobExpectedStartDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForJobExpectedStartDate.Text = "Expected Start Date:";
            this.ItemForJobExpectedStartDate.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 569);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(649, 10);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(649, 10);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06101_OM_Work_Unit_Types_PicklistTableAdapter
            // 
            this.sp06101_OM_Work_Unit_Types_PicklistTableAdapter.ClearBeforeFill = true;
            // 
            // sp06187_OM_Job_Equipment_EditTableAdapter
            // 
            this.sp06187_OM_Job_Equipment_EditTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            // 
            // frm_OM_Job_Equipment_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(686, 644);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Job_Equipment_Edit";
            this.Text = "Edit Job Equipment";
            this.Activated += new System.EventHandler(this.frm_OM_Job_Equipment_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Job_Equipment_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Job_Equipment_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.JobExpectedStartDateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06187OMJobEquipmentEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteLocationYTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteLocationXTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SitePostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateTimeDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateTimeDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateTimeDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateTimeDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PurchaseOrderIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameContractDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellValueTotalSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostValueTotalSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellValueVatSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostValueVatSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellValueExVatSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostValueExVatSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellPerUnitVatRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostPerUnitVatRateSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellPerUnitExVatSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostPerUnitExVatSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellUnitDescriptorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06101OMWorkUnitTypesPicklistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SellUnitsUsedSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostUnitDescriptorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostUnitsUsedSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentUsedIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeJobSubTypeDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSitePostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteLocationX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteLocationY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPurchaseOrderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentUsedID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeJobSubTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostUnitsUsed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostUnitDescriptorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostPerUnitExVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostPerUnitVatRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostValueExVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostValueVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostValueTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellUnitsUsed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellUnitDescriptorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellPerUnitExVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellPerUnitVatRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellValueExVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellValueVat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellValueTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientNameContractDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobExpectedStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobTypeJobSubTypeDescription;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.XtraEditors.ButtonEdit JobTypeJobSubTypeDescriptionButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentID;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit EquipmentIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpAddress;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DevExpress.XtraEditors.TextEdit JobIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobID;
        private DevExpress.XtraEditors.TextEdit EquipmentUsedIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentUsedID;
        private DevExpress.XtraEditors.ButtonEdit EquipmentNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentName;
        private DataSet_OM_Job dataSet_OM_Job;
        private DevExpress.XtraEditors.SpinEdit CostUnitsUsedSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostUnitsUsed;
        private DevExpress.XtraEditors.GridLookUpEdit CostUnitDescriptorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostUnitDescriptorID;
        private System.Windows.Forms.BindingSource sp06101OMWorkUnitTypesPicklistBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DataSet_OM_ContractTableAdapters.sp06101_OM_Work_Unit_Types_PicklistTableAdapter sp06101_OM_Work_Unit_Types_PicklistTableAdapter;
        private DevExpress.XtraEditors.SpinEdit SellUnitsUsedSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellUnitsUsed;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraEditors.GridLookUpEdit SellUnitDescriptorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellUnitDescriptorID;
        private DevExpress.XtraEditors.SpinEdit CostPerUnitExVatSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostPerUnitExVat;
        private DevExpress.XtraEditors.SpinEdit SellPerUnitExVatSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellPerUnitExVat;
        private DevExpress.XtraEditors.SpinEdit SellPerUnitVatRateSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CostPerUnitVatRateSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostPerUnitVatRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellPerUnitVatRate;
        private DevExpress.XtraEditors.SpinEdit CostValueExVatSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostValueExVat;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraEditors.SpinEdit SellValueExVatSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellValueExVat;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraEditors.SpinEdit CostValueVatSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostValueVat;
        private DevExpress.XtraEditors.SpinEdit SellValueVatSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellValueVat;
        private DevExpress.XtraEditors.SpinEdit CostValueTotalSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostValueTotal;
        private DevExpress.XtraEditors.SpinEdit SellValueTotalSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellValueTotal;
        private DevExpress.XtraEditors.TextEdit SiteIDTextEdit;
        private DevExpress.XtraEditors.TextEdit ClientIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteID;
        private DevExpress.XtraEditors.TextEdit VisitIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitID;
        private DevExpress.XtraEditors.TextEdit ClientNameContractDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientNameContractDescription;
        private DevExpress.XtraEditors.TextEdit SiteNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteName;
        private DevExpress.XtraEditors.TextEdit VisitNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitNumber;
        private DevExpress.XtraEditors.TextEdit ClientNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraEditors.TextEdit JobTypeDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobTypeDescription;
        private DevExpress.XtraEditors.TextEdit JobSubTypeDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobSubTypeDescription;
        private DevExpress.XtraEditors.TextEdit PurchaseOrderIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPurchaseOrderID;
        private DevExpress.XtraEditors.DateEdit EndDateTimeDateEdit;
        private DevExpress.XtraEditors.DateEdit StartDateTimeDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartDateTime;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndDateTime;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.TextEdit SiteLocationXTextEdit;
        private DevExpress.XtraEditors.TextEdit SitePostcodeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSitePostcode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteLocationX;
        private DevExpress.XtraEditors.TextEdit SiteLocationYTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteLocationY;
        private DevExpress.XtraEditors.TextEdit JobTypeIDTextEdit;
        private DevExpress.XtraEditors.TextEdit JobSubTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobSubTypeID;
        private DevExpress.XtraEditors.TextEdit ClientContractIDTextEdit;
        private DevExpress.XtraEditors.TextEdit SiteContractIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientContractID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteContractID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraBars.BarButtonItem bbiLinkedDocuments;
        private System.Windows.Forms.BindingSource sp06187OMJobEquipmentEditBindingSource;
        private DataSet_OM_JobTableAdapters.sp06187_OM_Job_Equipment_EditTableAdapter sp06187_OM_Job_Equipment_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit OwnerNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnerName;
        private DevExpress.XtraEditors.TextEdit OwnerTypeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnerType;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.TextEdit JobExpectedStartDateTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobExpectedStartDate;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

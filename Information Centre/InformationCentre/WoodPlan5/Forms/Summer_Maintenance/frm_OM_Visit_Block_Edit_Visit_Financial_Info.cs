﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraEditors;

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Visit_Block_Edit_Visit_Financial_Info : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string strMaterialDescription = null;

        public int intSelectedVisitCount = 0;
        public int? intCostCalculationLevel = null;
        public int? intSellCalculationLevel = null;
        public int intClearSelfBillingInvoice = 0;
        public decimal? decLabourCost = null;
        public decimal? decMaterialCost = null;
        public decimal? decEquipmentCost = null;
        public decimal? decTotalCost = null;
        public decimal? decLabourSell = null;
        public decimal? decMaterialSell = null;
        public decimal? decEquipmentSell = null;
        public decimal? decTotalSell = null;

        #endregion

        public frm_OM_Visit_Block_Edit_Visit_Financial_Info()
        {
            InitializeComponent();
        }

        private void frm_OM_Visit_Block_Edit_Visit_Financial_Info_Load(object sender, EventArgs e)
        {
            this.FormID = 500295;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //
            
            strConnectionString = this.GlobalSettings.ConnectionString;

            labelControlVisitCount.Text = "Visits Selected: " + intSelectedVisitCount.ToString();

            try
            {
                sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter.Fill(dataSet_OM_Visit.sp06134_OM_Job_Calculation_Level_Descriptors, 0);
            }
            catch (Exception) { }

            gridLookUpEditApplyCostCalculationLevel.EditValue = null;
            gridLookUpEditApplySellCalculationLevel.EditValue = null;
            spinEditLabourCost.EditValue = null;
            spinEditMaterialCost.EditValue = null;
            spinEditEquipmentCost.EditValue = null;
            spinEditTotalCost.EditValue = null;
            spinEditLabourSell.EditValue = null;
            spinEditMaterialSell.EditValue = null;
            spinEditEquipmentSell.EditValue = null;
            spinEditTotalSell.EditValue = null;
            checkEditClearSelfBillingInvoice.Checked = false;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }
       
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (gridLookUpEditApplyCostCalculationLevel.EditValue != null) intCostCalculationLevel = Convert.ToInt32(gridLookUpEditApplyCostCalculationLevel.EditValue);
            if (gridLookUpEditApplySellCalculationLevel.EditValue != null) intSellCalculationLevel = Convert.ToInt32(gridLookUpEditApplySellCalculationLevel.EditValue);

            if (spinEditLabourCost.EditValue != null) decLabourCost = Convert.ToDecimal(spinEditLabourCost.EditValue);
            if (spinEditMaterialCost.EditValue != null) decMaterialCost = Convert.ToDecimal(spinEditMaterialCost.EditValue);
            if (spinEditEquipmentCost.EditValue != null) decEquipmentCost = Convert.ToDecimal(spinEditEquipmentCost.EditValue);
            if (spinEditTotalCost.EditValue != null) decTotalCost = Convert.ToDecimal(spinEditTotalCost.EditValue);
            if (spinEditLabourSell.EditValue != null) decLabourSell = Convert.ToDecimal(spinEditLabourSell.EditValue);
            if (spinEditMaterialSell.EditValue != null) decMaterialSell = Convert.ToDecimal(spinEditMaterialSell.EditValue);
            if (spinEditEquipmentSell.EditValue != null) decEquipmentSell = Convert.ToDecimal(spinEditEquipmentSell.EditValue);
            if (spinEditTotalSell.EditValue != null) decTotalSell = Convert.ToDecimal(spinEditTotalSell.EditValue);
            intClearSelfBillingInvoice = (checkEditClearSelfBillingInvoice.Checked ? 1 : 0);
            
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }


        #region Editors

        private void gridLookUpEditApplyCostCalculationLevel_EditValueChanged(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            int? intValue = (string.IsNullOrEmpty(glue.EditValue.ToString()) ? -1 : Convert.ToInt32(glue.EditValue));
            //if (intValue != 0)
            //{
            //    spinEditLabourCost.EditValue = null;
            //    spinEditMaterialCost.EditValue = null;
            //    spinEditEquipmentCost.EditValue = null;
            //    spinEditTotalCost.EditValue = null;
            //}
            //spinEditLabourCost.Properties.ReadOnly = (intValue == 0 ? false : true);
            //spinEditMaterialCost.Properties.ReadOnly = (intValue == 0 ? false : true);
            //spinEditEquipmentCost.Properties.ReadOnly = (intValue == 0 ? false : true);
        }

        private void gridLookUpEditApplySellCalculationLevel_EditValueChanged(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            int? intValue = (string.IsNullOrEmpty(glue.EditValue.ToString()) ? -1 : Convert.ToInt32(glue.EditValue));
            //if (intValue != 0)
            //{
            //    spinEditLabourSell.EditValue = null;
            //    spinEditMaterialSell.EditValue = null;
            //    spinEditEquipmentSell.EditValue = null;
            //    spinEditTotalSell.EditValue = null;
            //}
            //spinEditLabourSell.Properties.ReadOnly = (intValue == 0 ? false : true);
            //spinEditMaterialSell.Properties.ReadOnly = (intValue == 0 ? false : true);
            //spinEditEquipmentSell.Properties.ReadOnly = (intValue == 0 ? false : true);
        }

        #endregion


    }
}

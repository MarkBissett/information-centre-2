namespace WoodPlan5
{
    partial class frm_OM_Points_Team_Redemption_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Points_Team_Redemption_Manager));
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.repositoryItemCheckEditShowActiveEmployeesOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Teams = new WoodPlan5.DataSet_Teams();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPointsRedemptionHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBSCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRedemptionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactMethodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalPointsOnOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalYearPoints = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYTDPoints = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPointsBalance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVatRegistration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemTextEditLatLong = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp06800OMTeamRedemptionListItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPointsRedemptionListID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberOfPoints = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPointsRatio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlDetails = new DevExpress.XtraGrid.GridControl();
            this.sp06804OMTeamPointsRedemptionDetailItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewDetails = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPointsRedemptionDetailID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPointsRedemptionHeaderID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBSCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPointsRedemptionListID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderLine = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitNumberOfPoints = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLineTotalPoints = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLineTotalPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLineVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurchaseOrderNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.sp06800_OM_Team_RedemptionListItemTableAdapter = new WoodPlan5.DataSet_TeamsTableAdapters.sp06800_OM_Team_RedemptionListItemTableAdapter();
            this.sp06806OMTeamListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp06806_OM_Team_ListTableAdapter = new WoodPlan5.DataSet_TeamsTableAdapters.sp06806_OM_Team_ListTableAdapter();
            this.sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter = new WoodPlan5.DataSet_TeamsTableAdapters.sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter();
            this.sp06804_OM_Team_PointsRedemptionDetailItemTableAdapter = new WoodPlan5.DataSet_TeamsTableAdapters.sp06804_OM_Team_PointsRedemptionDetailItemTableAdapter();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiInvoice = new DevExpress.XtraBars.BarButtonItem();
            this.skinBarSubItem1 = new DevExpress.XtraBars.SkinBarSubItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveEmployeesOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Teams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06800OMTeamRedemptionListItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06804OMTeamPointsRedemptionDetailItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sp06806OMTeamListBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1678, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 649);
            this.barDockControlBottom.Size = new System.Drawing.Size(1678, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 623);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1678, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 623);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.bbiRefresh,
            this.skinBarSubItem1,
            this.bbiInvoice});
            this.barManager1.MaxItemId = 37;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemCheckEditShowActiveEmployeesOnly});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("find_16x16.png", "images/find/find_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/find/find_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "find_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "refresh_16x16.png");
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // repositoryItemCheckEditShowActiveEmployeesOnly
            // 
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AutoHeight = false;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Caption = "";
            this.repositoryItemCheckEditShowActiveEmployeesOnly.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Name = "repositoryItemCheckEditShowActiveEmployeesOnly";
            this.repositoryItemCheckEditShowActiveEmployeesOnly.ValueChecked = 1;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.ValueUnchecked = 0;
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Reload Data", "reload")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditLatLong});
            this.gridControl1.Size = new System.Drawing.Size(1669, 443);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView4});
            // 
            // sp06802OMTeamPointsRedemptionHeaderItemBindingSource
            // 
            this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource.DataMember = "sp06802_OM_Team_PointsRedemptionHeaderItem";
            this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource.DataSource = this.dataSet_Teams;
            // 
            // dataSet_Teams
            // 
            this.dataSet_Teams.DataSetName = "DataSet_Teams";
            this.dataSet_Teams.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPointsRedemptionHeaderID,
            this.colBSCode,
            this.colintContractorID,
            this.colTeamName,
            this.colRedemptionDate,
            this.colContactMethodID,
            this.colContactMethod,
            this.colTotalPointsOnOrder,
            this.colTotalYearPoints,
            this.colTotalPrice,
            this.colTotalVAT,
            this.colMode1,
            this.colRecordID1,
            this.colYTDPoints,
            this.colPointsBalance,
            this.colAddress1,
            this.colAddress2,
            this.colAddress3,
            this.colPostCode,
            this.colVatRegistration});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.FindDelay = 2000;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colPointsRedemptionHeaderID
            // 
            this.colPointsRedemptionHeaderID.FieldName = "PointsRedemptionHeaderID";
            this.colPointsRedemptionHeaderID.Name = "colPointsRedemptionHeaderID";
            this.colPointsRedemptionHeaderID.OptionsColumn.AllowEdit = false;
            this.colPointsRedemptionHeaderID.OptionsColumn.AllowFocus = false;
            this.colPointsRedemptionHeaderID.OptionsColumn.ReadOnly = true;
            this.colPointsRedemptionHeaderID.OptionsColumn.ShowInCustomizationForm = false;
            this.colPointsRedemptionHeaderID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colBSCode
            // 
            this.colBSCode.FieldName = "BSCode";
            this.colBSCode.Name = "colBSCode";
            this.colBSCode.OptionsColumn.AllowEdit = false;
            this.colBSCode.OptionsColumn.AllowFocus = false;
            this.colBSCode.OptionsColumn.ReadOnly = true;
            this.colBSCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colBSCode.Visible = true;
            this.colBSCode.VisibleIndex = 0;
            this.colBSCode.Width = 114;
            // 
            // colintContractorID
            // 
            this.colintContractorID.FieldName = "intContractorID";
            this.colintContractorID.Name = "colintContractorID";
            this.colintContractorID.OptionsColumn.AllowEdit = false;
            this.colintContractorID.OptionsColumn.AllowFocus = false;
            this.colintContractorID.OptionsColumn.ReadOnly = true;
            this.colintContractorID.OptionsColumn.ShowInCustomizationForm = false;
            this.colintContractorID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colTeamName
            // 
            this.colTeamName.FieldName = "TeamName";
            this.colTeamName.Name = "colTeamName";
            this.colTeamName.OptionsColumn.AllowEdit = false;
            this.colTeamName.OptionsColumn.AllowFocus = false;
            this.colTeamName.OptionsColumn.ReadOnly = true;
            this.colTeamName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTeamName.Visible = true;
            this.colTeamName.VisibleIndex = 1;
            this.colTeamName.Width = 309;
            // 
            // colRedemptionDate
            // 
            this.colRedemptionDate.FieldName = "RedemptionDate";
            this.colRedemptionDate.Name = "colRedemptionDate";
            this.colRedemptionDate.OptionsColumn.AllowEdit = false;
            this.colRedemptionDate.OptionsColumn.AllowFocus = false;
            this.colRedemptionDate.OptionsColumn.ReadOnly = true;
            this.colRedemptionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colRedemptionDate.Visible = true;
            this.colRedemptionDate.VisibleIndex = 2;
            this.colRedemptionDate.Width = 102;
            // 
            // colContactMethodID
            // 
            this.colContactMethodID.FieldName = "ContactMethodID";
            this.colContactMethodID.Name = "colContactMethodID";
            this.colContactMethodID.OptionsColumn.AllowEdit = false;
            this.colContactMethodID.OptionsColumn.AllowFocus = false;
            this.colContactMethodID.OptionsColumn.ReadOnly = true;
            this.colContactMethodID.OptionsColumn.ShowInCustomizationForm = false;
            this.colContactMethodID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colContactMethod
            // 
            this.colContactMethod.FieldName = "ContactMethod";
            this.colContactMethod.Name = "colContactMethod";
            this.colContactMethod.OptionsColumn.AllowEdit = false;
            this.colContactMethod.OptionsColumn.AllowFocus = false;
            this.colContactMethod.OptionsColumn.ReadOnly = true;
            this.colContactMethod.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colContactMethod.Visible = true;
            this.colContactMethod.VisibleIndex = 3;
            this.colContactMethod.Width = 211;
            // 
            // colTotalPointsOnOrder
            // 
            this.colTotalPointsOnOrder.FieldName = "TotalPointsOnOrder";
            this.colTotalPointsOnOrder.Name = "colTotalPointsOnOrder";
            this.colTotalPointsOnOrder.OptionsColumn.AllowEdit = false;
            this.colTotalPointsOnOrder.OptionsColumn.AllowFocus = false;
            this.colTotalPointsOnOrder.OptionsColumn.ReadOnly = true;
            this.colTotalPointsOnOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTotalPointsOnOrder.Visible = true;
            this.colTotalPointsOnOrder.VisibleIndex = 4;
            this.colTotalPointsOnOrder.Width = 217;
            // 
            // colTotalYearPoints
            // 
            this.colTotalYearPoints.FieldName = "TotalYearPoints";
            this.colTotalYearPoints.Name = "colTotalYearPoints";
            this.colTotalYearPoints.OptionsColumn.AllowEdit = false;
            this.colTotalYearPoints.OptionsColumn.AllowFocus = false;
            this.colTotalYearPoints.OptionsColumn.ReadOnly = true;
            this.colTotalYearPoints.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTotalYearPoints.Visible = true;
            this.colTotalYearPoints.VisibleIndex = 5;
            this.colTotalYearPoints.Width = 166;
            // 
            // colTotalPrice
            // 
            this.colTotalPrice.DisplayFormat.FormatString = "c2";
            this.colTotalPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalPrice.FieldName = "TotalPrice";
            this.colTotalPrice.Name = "colTotalPrice";
            this.colTotalPrice.OptionsColumn.AllowEdit = false;
            this.colTotalPrice.OptionsColumn.AllowFocus = false;
            this.colTotalPrice.OptionsColumn.ReadOnly = true;
            this.colTotalPrice.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTotalPrice.Visible = true;
            this.colTotalPrice.VisibleIndex = 6;
            this.colTotalPrice.Width = 151;
            // 
            // colTotalVAT
            // 
            this.colTotalVAT.DisplayFormat.FormatString = "c2";
            this.colTotalVAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTotalVAT.FieldName = "TotalVAT";
            this.colTotalVAT.Name = "colTotalVAT";
            this.colTotalVAT.OptionsColumn.AllowEdit = false;
            this.colTotalVAT.OptionsColumn.AllowFocus = false;
            this.colTotalVAT.OptionsColumn.ReadOnly = true;
            this.colTotalVAT.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTotalVAT.Visible = true;
            this.colTotalVAT.VisibleIndex = 7;
            this.colTotalVAT.Width = 125;
            // 
            // colMode1
            // 
            this.colMode1.FieldName = "Mode";
            this.colMode1.Name = "colMode1";
            this.colMode1.OptionsColumn.AllowEdit = false;
            this.colMode1.OptionsColumn.AllowFocus = false;
            this.colMode1.OptionsColumn.ReadOnly = true;
            this.colMode1.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colRecordID1
            // 
            this.colRecordID1.FieldName = "RecordID";
            this.colRecordID1.Name = "colRecordID1";
            this.colRecordID1.OptionsColumn.AllowEdit = false;
            this.colRecordID1.OptionsColumn.AllowFocus = false;
            this.colRecordID1.OptionsColumn.ReadOnly = true;
            this.colRecordID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colYTDPoints
            // 
            this.colYTDPoints.FieldName = "YTDPoints";
            this.colYTDPoints.Name = "colYTDPoints";
            this.colYTDPoints.OptionsColumn.AllowEdit = false;
            this.colYTDPoints.OptionsColumn.AllowFocus = false;
            this.colYTDPoints.OptionsColumn.ReadOnly = true;
            this.colYTDPoints.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colYTDPoints.Visible = true;
            this.colYTDPoints.VisibleIndex = 8;
            this.colYTDPoints.Width = 130;
            // 
            // colPointsBalance
            // 
            this.colPointsBalance.FieldName = "PointsBalance";
            this.colPointsBalance.Name = "colPointsBalance";
            this.colPointsBalance.OptionsColumn.AllowEdit = false;
            this.colPointsBalance.OptionsColumn.AllowFocus = false;
            this.colPointsBalance.OptionsColumn.ReadOnly = true;
            this.colPointsBalance.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPointsBalance.Visible = true;
            this.colPointsBalance.VisibleIndex = 9;
            this.colPointsBalance.Width = 117;
            // 
            // colAddress1
            // 
            this.colAddress1.FieldName = "Address1";
            this.colAddress1.Name = "colAddress1";
            this.colAddress1.OptionsColumn.AllowEdit = false;
            this.colAddress1.OptionsColumn.AllowFocus = false;
            this.colAddress1.OptionsColumn.ReadOnly = true;
            this.colAddress1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colAddress2
            // 
            this.colAddress2.FieldName = "Address2";
            this.colAddress2.Name = "colAddress2";
            this.colAddress2.OptionsColumn.AllowEdit = false;
            this.colAddress2.OptionsColumn.AllowFocus = false;
            this.colAddress2.OptionsColumn.ReadOnly = true;
            this.colAddress2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colAddress3
            // 
            this.colAddress3.FieldName = "Address3";
            this.colAddress3.Name = "colAddress3";
            this.colAddress3.OptionsColumn.AllowEdit = false;
            this.colAddress3.OptionsColumn.AllowFocus = false;
            this.colAddress3.OptionsColumn.ReadOnly = true;
            this.colAddress3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colPostCode
            // 
            this.colPostCode.FieldName = "PostCode";
            this.colPostCode.Name = "colPostCode";
            this.colPostCode.OptionsColumn.AllowEdit = false;
            this.colPostCode.OptionsColumn.AllowFocus = false;
            this.colPostCode.OptionsColumn.ReadOnly = true;
            this.colPostCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colVatRegistration
            // 
            this.colVatRegistration.FieldName = "VatRegistration";
            this.colVatRegistration.Name = "colVatRegistration";
            this.colVatRegistration.OptionsColumn.AllowEdit = false;
            this.colVatRegistration.OptionsColumn.AllowFocus = false;
            this.colVatRegistration.OptionsColumn.ReadOnly = true;
            this.colVatRegistration.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // repositoryItemTextEditLatLong
            // 
            this.repositoryItemTextEditLatLong.AutoHeight = false;
            this.repositoryItemTextEditLatLong.Mask.EditMask = "n8";
            this.repositoryItemTextEditLatLong.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditLatLong.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditLatLong.Name = "repositoryItemTextEditLatLong";
            // 
            // gridView4
            // 
            this.gridView4.GridControl = this.gridControl1;
            this.gridView4.Name = "gridView4";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp06800OMTeamRedemptionListItemBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Reload Data", "reload")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemCheckEdit1});
            this.gridControl2.Size = new System.Drawing.Size(1673, 592);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp06800OMTeamRedemptionListItemBindingSource
            // 
            this.sp06800OMTeamRedemptionListItemBindingSource.DataMember = "sp06800_OM_Team_RedemptionListItem";
            this.sp06800OMTeamRedemptionListItemBindingSource.DataSource = this.dataSet_Teams;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPointsRedemptionListID,
            this.colItemTypeID,
            this.colItemType,
            this.colReferenceCode,
            this.colItemDescription,
            this.colNumberOfPoints,
            this.colPointsRatio,
            this.colMode,
            this.colRecordID});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsFind.FindDelay = 2000;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colPointsRedemptionListID
            // 
            this.colPointsRedemptionListID.FieldName = "PointsRedemptionListID";
            this.colPointsRedemptionListID.Name = "colPointsRedemptionListID";
            this.colPointsRedemptionListID.OptionsColumn.AllowEdit = false;
            this.colPointsRedemptionListID.OptionsColumn.AllowFocus = false;
            this.colPointsRedemptionListID.OptionsColumn.ReadOnly = true;
            this.colPointsRedemptionListID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPointsRedemptionListID.Width = 141;
            // 
            // colItemTypeID
            // 
            this.colItemTypeID.FieldName = "ItemTypeID";
            this.colItemTypeID.Name = "colItemTypeID";
            this.colItemTypeID.OptionsColumn.AllowEdit = false;
            this.colItemTypeID.OptionsColumn.AllowFocus = false;
            this.colItemTypeID.OptionsColumn.ReadOnly = true;
            this.colItemTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colItemType
            // 
            this.colItemType.FieldName = "ItemType";
            this.colItemType.Name = "colItemType";
            this.colItemType.OptionsColumn.AllowEdit = false;
            this.colItemType.OptionsColumn.AllowFocus = false;
            this.colItemType.OptionsColumn.ReadOnly = true;
            this.colItemType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colItemType.Visible = true;
            this.colItemType.VisibleIndex = 0;
            // 
            // colReferenceCode
            // 
            this.colReferenceCode.FieldName = "ReferenceCode";
            this.colReferenceCode.Name = "colReferenceCode";
            this.colReferenceCode.OptionsColumn.AllowEdit = false;
            this.colReferenceCode.OptionsColumn.AllowFocus = false;
            this.colReferenceCode.OptionsColumn.ReadOnly = true;
            this.colReferenceCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colReferenceCode.Visible = true;
            this.colReferenceCode.VisibleIndex = 1;
            this.colReferenceCode.Width = 158;
            // 
            // colItemDescription
            // 
            this.colItemDescription.FieldName = "ItemDescription";
            this.colItemDescription.Name = "colItemDescription";
            this.colItemDescription.OptionsColumn.AllowEdit = false;
            this.colItemDescription.OptionsColumn.AllowFocus = false;
            this.colItemDescription.OptionsColumn.ReadOnly = true;
            this.colItemDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colItemDescription.Visible = true;
            this.colItemDescription.VisibleIndex = 2;
            this.colItemDescription.Width = 537;
            // 
            // colNumberOfPoints
            // 
            this.colNumberOfPoints.FieldName = "NumberOfPoints";
            this.colNumberOfPoints.Name = "colNumberOfPoints";
            this.colNumberOfPoints.OptionsColumn.AllowEdit = false;
            this.colNumberOfPoints.OptionsColumn.AllowFocus = false;
            this.colNumberOfPoints.OptionsColumn.ReadOnly = true;
            this.colNumberOfPoints.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colNumberOfPoints.Visible = true;
            this.colNumberOfPoints.VisibleIndex = 3;
            this.colNumberOfPoints.Width = 135;
            // 
            // colPointsRatio
            // 
            this.colPointsRatio.FieldName = "PointsRatio";
            this.colPointsRatio.Name = "colPointsRatio";
            this.colPointsRatio.OptionsColumn.AllowEdit = false;
            this.colPointsRatio.OptionsColumn.AllowFocus = false;
            this.colPointsRatio.OptionsColumn.ReadOnly = true;
            this.colPointsRatio.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPointsRatio.Visible = true;
            this.colPointsRatio.VisibleIndex = 4;
            this.colPointsRatio.Width = 290;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            this.colMode.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            this.colRecordID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 26);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1678, 623);
            this.xtraTabControl1.TabIndex = 5;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1673, 597);
            this.xtraTabPage1.Text = "Team Points Redemption";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer2);
            this.splitContainerControl1.Size = new System.Drawing.Size(1673, 597);
            this.splitContainerControl1.SplitterPosition = 144;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1669, 443);
            this.gridSplitContainer1.TabIndex = 2;
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControlDetails;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControlDetails);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1669, 140);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControlDetails
            // 
            this.gridControlDetails.DataSource = this.sp06804OMTeamPointsRedemptionDetailItemBindingSource;
            this.gridControlDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlDetails.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlDetails.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlDetails.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlDetails.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlDetails.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlDetails.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlDetails.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlDetails.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlDetails.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlDetails.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlDetails.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlDetails.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Ad New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Reload Data", "reload")});
            this.gridControlDetails.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.gridControlDetails.Location = new System.Drawing.Point(0, 0);
            this.gridControlDetails.MainView = this.gridViewDetails;
            this.gridControlDetails.MenuManager = this.barManager1;
            this.gridControlDetails.Name = "gridControlDetails";
            this.gridControlDetails.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6});
            this.gridControlDetails.Size = new System.Drawing.Size(1669, 140);
            this.gridControlDetails.TabIndex = 3;
            this.gridControlDetails.UseEmbeddedNavigator = true;
            this.gridControlDetails.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDetails});
            // 
            // sp06804OMTeamPointsRedemptionDetailItemBindingSource
            // 
            this.sp06804OMTeamPointsRedemptionDetailItemBindingSource.DataMember = "sp06804_OM_Team_PointsRedemptionDetailItem";
            this.sp06804OMTeamPointsRedemptionDetailItemBindingSource.DataSource = this.dataSet_Teams;
            // 
            // gridViewDetails
            // 
            this.gridViewDetails.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPointsRedemptionDetailID,
            this.colPointsRedemptionHeaderID1,
            this.colBSCode1,
            this.colTeamName1,
            this.colPointsRedemptionListID1,
            this.colItemTypeID1,
            this.colItemType1,
            this.colItemDescription1,
            this.colOrderQuantity,
            this.colOrderLine,
            this.colUnitNumberOfPoints,
            this.colUnitPrice,
            this.colUnitVAT,
            this.colLineTotalPoints,
            this.colLineTotalPrice,
            this.colLineVAT,
            this.colPurchaseOrderNumber,
            this.colMode2,
            this.colRecordID2,
            this.colComments});
            this.gridViewDetails.GridControl = this.gridControlDetails;
            this.gridViewDetails.Name = "gridViewDetails";
            this.gridViewDetails.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridViewDetails.OptionsFilter.AllowFilterEditor = false;
            this.gridViewDetails.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridViewDetails.OptionsFilter.AllowMRUFilterList = false;
            this.gridViewDetails.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridViewDetails.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridViewDetails.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridViewDetails.OptionsFind.FindDelay = 2000;
            this.gridViewDetails.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewDetails.OptionsLayout.StoreAppearance = true;
            this.gridViewDetails.OptionsLayout.StoreFormatRules = true;
            this.gridViewDetails.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewDetails.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewDetails.OptionsSelection.MultiSelect = true;
            this.gridViewDetails.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewDetails.OptionsView.ColumnAutoWidth = false;
            this.gridViewDetails.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewDetails.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewDetails.OptionsView.ShowGroupPanel = false;
            this.gridViewDetails.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewDetails.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewDetails.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewDetails.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewDetails.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewDetails.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewDetails.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.gridViewDetails.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.gridViewDetails.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colPointsRedemptionDetailID
            // 
            this.colPointsRedemptionDetailID.FieldName = "PointsRedemptionDetailID";
            this.colPointsRedemptionDetailID.Name = "colPointsRedemptionDetailID";
            this.colPointsRedemptionDetailID.OptionsColumn.AllowEdit = false;
            this.colPointsRedemptionDetailID.OptionsColumn.AllowFocus = false;
            this.colPointsRedemptionDetailID.OptionsColumn.ReadOnly = true;
            this.colPointsRedemptionDetailID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colPointsRedemptionHeaderID1
            // 
            this.colPointsRedemptionHeaderID1.FieldName = "PointsRedemptionHeaderID";
            this.colPointsRedemptionHeaderID1.Name = "colPointsRedemptionHeaderID1";
            this.colPointsRedemptionHeaderID1.OptionsColumn.AllowEdit = false;
            this.colPointsRedemptionHeaderID1.OptionsColumn.AllowFocus = false;
            this.colPointsRedemptionHeaderID1.OptionsColumn.ReadOnly = true;
            this.colPointsRedemptionHeaderID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colBSCode1
            // 
            this.colBSCode1.FieldName = "BSCode";
            this.colBSCode1.Name = "colBSCode1";
            this.colBSCode1.OptionsColumn.AllowEdit = false;
            this.colBSCode1.OptionsColumn.AllowFocus = false;
            this.colBSCode1.OptionsColumn.ReadOnly = true;
            this.colBSCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colBSCode1.Visible = true;
            this.colBSCode1.VisibleIndex = 0;
            this.colBSCode1.Width = 68;
            // 
            // colTeamName1
            // 
            this.colTeamName1.FieldName = "TeamName";
            this.colTeamName1.Name = "colTeamName1";
            this.colTeamName1.OptionsColumn.AllowEdit = false;
            this.colTeamName1.OptionsColumn.AllowFocus = false;
            this.colTeamName1.OptionsColumn.ReadOnly = true;
            this.colTeamName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colTeamName1.Visible = true;
            this.colTeamName1.VisibleIndex = 1;
            this.colTeamName1.Width = 161;
            // 
            // colPointsRedemptionListID1
            // 
            this.colPointsRedemptionListID1.FieldName = "PointsRedemptionListID";
            this.colPointsRedemptionListID1.Name = "colPointsRedemptionListID1";
            this.colPointsRedemptionListID1.OptionsColumn.AllowEdit = false;
            this.colPointsRedemptionListID1.OptionsColumn.AllowFocus = false;
            this.colPointsRedemptionListID1.OptionsColumn.ReadOnly = true;
            this.colPointsRedemptionListID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPointsRedemptionListID1.Width = 141;
            // 
            // colItemTypeID1
            // 
            this.colItemTypeID1.FieldName = "ItemTypeID";
            this.colItemTypeID1.Name = "colItemTypeID1";
            this.colItemTypeID1.OptionsColumn.AllowEdit = false;
            this.colItemTypeID1.OptionsColumn.AllowFocus = false;
            this.colItemTypeID1.OptionsColumn.ReadOnly = true;
            this.colItemTypeID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colItemType1
            // 
            this.colItemType1.FieldName = "ItemType";
            this.colItemType1.Name = "colItemType1";
            this.colItemType1.OptionsColumn.AllowEdit = false;
            this.colItemType1.OptionsColumn.AllowFocus = false;
            this.colItemType1.OptionsColumn.ReadOnly = true;
            this.colItemType1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colItemType1.Visible = true;
            this.colItemType1.VisibleIndex = 2;
            this.colItemType1.Width = 98;
            // 
            // colItemDescription1
            // 
            this.colItemDescription1.FieldName = "ItemDescription";
            this.colItemDescription1.Name = "colItemDescription1";
            this.colItemDescription1.OptionsColumn.AllowEdit = false;
            this.colItemDescription1.OptionsColumn.AllowFocus = false;
            this.colItemDescription1.OptionsColumn.ReadOnly = true;
            this.colItemDescription1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colItemDescription1.Visible = true;
            this.colItemDescription1.VisibleIndex = 3;
            this.colItemDescription1.Width = 241;
            // 
            // colOrderQuantity
            // 
            this.colOrderQuantity.FieldName = "OrderQuantity";
            this.colOrderQuantity.Name = "colOrderQuantity";
            this.colOrderQuantity.OptionsColumn.AllowEdit = false;
            this.colOrderQuantity.OptionsColumn.AllowFocus = false;
            this.colOrderQuantity.OptionsColumn.ReadOnly = true;
            this.colOrderQuantity.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOrderQuantity.Visible = true;
            this.colOrderQuantity.VisibleIndex = 4;
            this.colOrderQuantity.Width = 92;
            // 
            // colOrderLine
            // 
            this.colOrderLine.FieldName = "OrderLine";
            this.colOrderLine.Name = "colOrderLine";
            this.colOrderLine.OptionsColumn.AllowEdit = false;
            this.colOrderLine.OptionsColumn.AllowFocus = false;
            this.colOrderLine.OptionsColumn.ReadOnly = true;
            this.colOrderLine.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOrderLine.Visible = true;
            this.colOrderLine.VisibleIndex = 5;
            this.colOrderLine.Width = 58;
            // 
            // colUnitNumberOfPoints
            // 
            this.colUnitNumberOfPoints.FieldName = "UnitNumberOfPoints";
            this.colUnitNumberOfPoints.Name = "colUnitNumberOfPoints";
            this.colUnitNumberOfPoints.OptionsColumn.AllowEdit = false;
            this.colUnitNumberOfPoints.OptionsColumn.AllowFocus = false;
            this.colUnitNumberOfPoints.OptionsColumn.ReadOnly = true;
            this.colUnitNumberOfPoints.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colUnitNumberOfPoints.Visible = true;
            this.colUnitNumberOfPoints.VisibleIndex = 6;
            this.colUnitNumberOfPoints.Width = 125;
            // 
            // colUnitPrice
            // 
            this.colUnitPrice.DisplayFormat.FormatString = "c2";
            this.colUnitPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colUnitPrice.FieldName = "UnitPrice";
            this.colUnitPrice.Name = "colUnitPrice";
            this.colUnitPrice.OptionsColumn.AllowEdit = false;
            this.colUnitPrice.OptionsColumn.AllowFocus = false;
            this.colUnitPrice.OptionsColumn.ReadOnly = true;
            this.colUnitPrice.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colUnitPrice.Visible = true;
            this.colUnitPrice.VisibleIndex = 7;
            this.colUnitPrice.Width = 61;
            // 
            // colUnitVAT
            // 
            this.colUnitVAT.DisplayFormat.FormatString = "c2";
            this.colUnitVAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colUnitVAT.FieldName = "UnitVAT";
            this.colUnitVAT.Name = "colUnitVAT";
            this.colUnitVAT.OptionsColumn.AllowEdit = false;
            this.colUnitVAT.OptionsColumn.AllowFocus = false;
            this.colUnitVAT.OptionsColumn.ReadOnly = true;
            this.colUnitVAT.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colUnitVAT.Visible = true;
            this.colUnitVAT.VisibleIndex = 8;
            this.colUnitVAT.Width = 76;
            // 
            // colLineTotalPoints
            // 
            this.colLineTotalPoints.FieldName = "LineTotalPoints";
            this.colLineTotalPoints.Name = "colLineTotalPoints";
            this.colLineTotalPoints.OptionsColumn.AllowEdit = false;
            this.colLineTotalPoints.OptionsColumn.AllowFocus = false;
            this.colLineTotalPoints.OptionsColumn.ReadOnly = true;
            this.colLineTotalPoints.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colLineTotalPoints.Visible = true;
            this.colLineTotalPoints.VisibleIndex = 9;
            this.colLineTotalPoints.Width = 97;
            // 
            // colLineTotalPrice
            // 
            this.colLineTotalPrice.DisplayFormat.FormatString = "c2";
            this.colLineTotalPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colLineTotalPrice.FieldName = "LineTotalPrice";
            this.colLineTotalPrice.Name = "colLineTotalPrice";
            this.colLineTotalPrice.OptionsColumn.AllowEdit = false;
            this.colLineTotalPrice.OptionsColumn.AllowFocus = false;
            this.colLineTotalPrice.OptionsColumn.ReadOnly = true;
            this.colLineTotalPrice.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colLineTotalPrice.Visible = true;
            this.colLineTotalPrice.VisibleIndex = 10;
            this.colLineTotalPrice.Width = 91;
            // 
            // colLineVAT
            // 
            this.colLineVAT.DisplayFormat.FormatString = "c2";
            this.colLineVAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colLineVAT.FieldName = "LineVAT";
            this.colLineVAT.Name = "colLineVAT";
            this.colLineVAT.OptionsColumn.AllowEdit = false;
            this.colLineVAT.OptionsColumn.AllowFocus = false;
            this.colLineVAT.OptionsColumn.ReadOnly = true;
            this.colLineVAT.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colLineVAT.Visible = true;
            this.colLineVAT.VisibleIndex = 11;
            this.colLineVAT.Width = 71;
            // 
            // colPurchaseOrderNumber
            // 
            this.colPurchaseOrderNumber.FieldName = "PurchaseOrderNumber";
            this.colPurchaseOrderNumber.Name = "colPurchaseOrderNumber";
            this.colPurchaseOrderNumber.OptionsColumn.AllowEdit = false;
            this.colPurchaseOrderNumber.OptionsColumn.AllowFocus = false;
            this.colPurchaseOrderNumber.OptionsColumn.ReadOnly = true;
            this.colPurchaseOrderNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colPurchaseOrderNumber.Visible = true;
            this.colPurchaseOrderNumber.VisibleIndex = 12;
            this.colPurchaseOrderNumber.Width = 134;
            // 
            // colMode2
            // 
            this.colMode2.FieldName = "Mode";
            this.colMode2.Name = "colMode2";
            this.colMode2.OptionsColumn.AllowEdit = false;
            this.colMode2.OptionsColumn.AllowFocus = false;
            this.colMode2.OptionsColumn.ReadOnly = true;
            this.colMode2.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colRecordID2
            // 
            this.colRecordID2.FieldName = "RecordID";
            this.colRecordID2.Name = "colRecordID2";
            this.colRecordID2.OptionsColumn.AllowEdit = false;
            this.colRecordID2.OptionsColumn.AllowFocus = false;
            this.colRecordID2.OptionsColumn.ReadOnly = true;
            this.colRecordID2.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colComments
            // 
            this.colComments.FieldName = "Comments";
            this.colComments.Name = "colComments";
            this.colComments.OptionsColumn.AllowEdit = false;
            this.colComments.OptionsColumn.AllowFocus = false;
            this.colComments.OptionsColumn.ReadOnly = true;
            this.colComments.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colComments.Visible = true;
            this.colComments.VisibleIndex = 13;
            this.colComments.Width = 333;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1673, 592);
            this.xtraTabPage2.Text = "Points Redemption";
            // 
            // sp06800_OM_Team_RedemptionListItemTableAdapter
            // 
            this.sp06800_OM_Team_RedemptionListItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp06806OMTeamListBindingSource
            // 
            this.sp06806OMTeamListBindingSource.DataMember = "sp06806_OM_Team_List";
            this.sp06806OMTeamListBindingSource.DataSource = this.dataSet_Teams;
            // 
            // sp06806_OM_Team_ListTableAdapter
            // 
            this.sp06806_OM_Team_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter
            // 
            this.sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp06804_OM_Team_PointsRedemptionDetailItemTableAdapter
            // 
            this.sp06804_OM_Team_PointsRedemptionDetailItemTableAdapter.ClearBeforeFill = true;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Refresh";
            this.barButtonItem1.Id = 33;
            this.barButtonItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.Image")));
            this.barButtonItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.LargeImage")));
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.Text = "Screen Functions";
            // 
            // bar2
            // 
            this.bar2.BarName = "Function Bar";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, this.bbiInvoice, true)});
            this.bar2.OptionsBar.AllowRename = true;
            this.bar2.Text = "Function Bar";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 34;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.LargeImage")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bbiInvoice
            // 
            this.bbiInvoice.Caption = "Create Invoice";
            this.bbiInvoice.Enabled = false;
            this.bbiInvoice.Id = 36;
            this.bbiInvoice.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiInvoice.ImageOptions.Image")));
            this.bbiInvoice.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiInvoice.ImageOptions.LargeImage")));
            this.bbiInvoice.Name = "bbiInvoice";
            this.bbiInvoice.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // skinBarSubItem1
            // 
            this.skinBarSubItem1.Caption = "skinBarSubItem1";
            this.skinBarSubItem1.Id = 35;
            this.skinBarSubItem1.Name = "skinBarSubItem1";
            // 
            // frm_OM_Points_Team_Redemption_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1678, 649);
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Points_Team_Redemption_Manager";
            this.Text = "Points Redemption Manager";
            this.Activated += new System.EventHandler(this.frm_OM_Points_Team_Redemption_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Points_Team_Redemption_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Points_Team_Redemption_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveEmployeesOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Teams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06800OMTeamRedemptionListItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06804OMTeamPointsRedemptionDetailItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sp06806OMTeamListBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditShowActiveEmployeesOnly;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private System.Windows.Forms.BindingSource sp06800OMTeamRedemptionListItemBindingSource;
        private DataSet_Teams dataSet_Teams;
        private DevExpress.XtraGrid.Columns.GridColumn colPointsRedemptionListID;
        private DevExpress.XtraGrid.Columns.GridColumn colItemTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colItemType;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceCode;
        private DevExpress.XtraGrid.Columns.GridColumn colItemDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberOfPoints;
        private DevExpress.XtraGrid.Columns.GridColumn colPointsRatio;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DataSet_TeamsTableAdapters.sp06800_OM_Team_RedemptionListItemTableAdapter sp06800_OM_Team_RedemptionListItemTableAdapter;
        private System.Windows.Forms.BindingSource sp06806OMTeamListBindingSource;
        private DataSet_TeamsTableAdapters.sp06806_OM_Team_ListTableAdapter sp06806_OM_Team_ListTableAdapter;
        private System.Windows.Forms.BindingSource sp06802OMTeamPointsRedemptionHeaderItemBindingSource;
        private DataSet_TeamsTableAdapters.sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colPointsRedemptionHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colBSCode;
        private DevExpress.XtraGrid.Columns.GridColumn colintContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamName;
        private DevExpress.XtraGrid.Columns.GridColumn colRedemptionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMethodID;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalPointsOnOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalYearPoints;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colMode1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID1;
        private DevExpress.XtraGrid.Columns.GridColumn colYTDPoints;
        private DevExpress.XtraGrid.Columns.GridColumn colPointsBalance;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress3;
        private DevExpress.XtraGrid.Columns.GridColumn colPostCode;
        private DevExpress.XtraGrid.Columns.GridColumn colVatRegistration;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditLatLong;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridControl gridControlDetails;
        private System.Windows.Forms.BindingSource sp06804OMTeamPointsRedemptionDetailItemBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDetails;
        private DevExpress.XtraGrid.Columns.GridColumn colPointsRedemptionDetailID;
        private DevExpress.XtraGrid.Columns.GridColumn colPointsRedemptionHeaderID1;
        private DevExpress.XtraGrid.Columns.GridColumn colBSCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamName1;
        private DevExpress.XtraGrid.Columns.GridColumn colPointsRedemptionListID1;
        private DevExpress.XtraGrid.Columns.GridColumn colItemTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colItemType1;
        private DevExpress.XtraGrid.Columns.GridColumn colItemDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderLine;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitNumberOfPoints;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colLineTotalPoints;
        private DevExpress.XtraGrid.Columns.GridColumn colLineTotalPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colLineVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colPurchaseOrderNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colMode2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID2;
        private DevExpress.XtraGrid.Columns.GridColumn colComments;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DataSet_TeamsTableAdapters.sp06804_OM_Team_PointsRedemptionDetailItemTableAdapter sp06804_OM_Team_PointsRedemptionDetailItemTableAdapter;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarButtonItem bbiInvoice;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem1;
    }
}

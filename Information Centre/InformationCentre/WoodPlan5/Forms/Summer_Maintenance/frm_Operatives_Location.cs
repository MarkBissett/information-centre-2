﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraMap;
using DevExpress.Utils.Menu;  // Required to disable Column Chooser on Grids //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using LocusEffects;

namespace WoodPlan5
{
    public partial class frm_Operatives_Location : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection3;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;
        private BeaconLocusEffect m_customBeaconLocusEffect1 = null;

        string i_str_MapType = "Bing Hybrid";
        private DateTime i_dtStart = DateTime.MinValue;
        private DateTime i_dtEnd = DateTime.MaxValue;
        private TimeSpan i_tStart = new TimeSpan(5, 0, 0);
        private TimeSpan i_tEnd = new TimeSpan(22, 0, 0);

        double latitude;
        double longitude;
        private bool BestFitZoomInProcess = false;
        int intPointsToZoomToCount = 0;
        int intContractorIDToZoomTo = 0;

        private string i_str_selected_ping_type_ids = "";
        private string i_str_selected_ping_types = "";

        VectorItemsLayer mapObjectsLayer { get { return (VectorItemsLayer)mapControl1.Layers[1]; } }

        private ArrayList arraylistFloatingPanels = new ArrayList();  // Holds which panels were floating when form was deactivated so the panels can be hidden then re-shown on form activate //

        #endregion

        public frm_Operatives_Location()
        {
            InitializeComponent();
        }

        private void frm_Operatives_Location_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 7003;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            sp06016_Get_Teams_From_LocationsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06017_Get_Operative_LocationsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06126_Get_Operative_PingsTableAdapter.Connection.ConnectionString = strConnectionString;

            sp06125_OM_Operative_Location_Ping_Type_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06125_OM_Operative_Location_Ping_Type_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06125_OM_Operative_Location_Ping_Type_Filter);
            gridControl3.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;

            dateEditStart.DateTime = DateTime.Today;
            dateEditEnd.DateTime = DateTime.Today;
            StartTimeTimeEdit.EditValue = i_tStart;
            EndTimeTimeEdit.EditValue = i_tEnd;

            RefreshGridViewState1 = new RefreshGridState(gridView1, "ContractorID");
            
            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 65; // Make wider to allow for caption in header //
            selection1.CheckMarkColumn.Caption = "     Select";  // Spaces at start so room for checkbox in front //
            selection1.CheckMarkColumn.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
            selection1.CheckMarkColumn.OptionsColumn.ShowCaption = true;
            selection1.CheckMarkColumn.ToolTip = "Controls which People that Pings are Loaded for.";
            selection1.editColumnHeader.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            selection1.editColumnHeader.Caption = "";
            selection1.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            selection1.CheckMarkColumn.VisibleIndex = 0;

            // Add record selection checkboxes to popup grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 65; // Make wider to allow for caption in header //
            selection2.CheckMarkColumn.Caption = "     Visible";  // Spaces at start so room for checkbox in front //
            selection2.CheckMarkColumn.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
            selection2.CheckMarkColumn.OptionsColumn.ShowCaption = true;
            selection2.CheckMarkColumn.ToolTip = "Controls which Pings are Loaded into the map.";
            selection2.editColumnHeader.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            selection2.editColumnHeader.Caption = "";
            selection2.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            selection2.CheckMarkColumn.VisibleIndex = 0;

            InformationLayer infoLayer = new InformationLayer();
            mapControl1.Layers.Add(infoLayer);
            BingSearchDataProvider searchProvider = new BingSearchDataProvider();
            infoLayer.DataProvider = searchProvider;
            searchProvider.BingKey = "As_rq12T3oFpxWh52cGqK5yhqcHov7wkd4orPObzZFgo2N92CqPDc1xO_uokdUdT";
            searchProvider.SearchOptions.DistanceUnit = DistanceMeasureUnit.Mile;
            searchProvider.SearchOptions.SearchRadius = 200;
            searchProvider.SearchOptions.ResultsCount = 5;

            // Prepare LocusEffects and add custom effect //
            locusEffectsProvider1 = new LocusEffectsProvider();
            locusEffectsProvider1.Initialize();
            locusEffectsProvider1.FramesPerSecond = 30;
            m_customBeaconLocusEffect1 = new BeaconLocusEffect();
            m_customBeaconLocusEffect1.Name = "CustomBeacon1";
            m_customBeaconLocusEffect1.InitialSize = new Size(150, 150);
            m_customBeaconLocusEffect1.AnimationTime = 1500;
            m_customBeaconLocusEffect1.LeadInTime = 0;
            m_customBeaconLocusEffect1.LeadOutTime = 0;
            m_customBeaconLocusEffect1.AnimationStartColor = Color.Red;
            m_customBeaconLocusEffect1.AnimationEndColor = Color.Red;
            m_customBeaconLocusEffect1.AnimationOuterColor = Color.Salmon;
            m_customBeaconLocusEffect1.RingWidth = 6; // 6;
            m_customBeaconLocusEffect1.OuterRingWidth = 3;// 3;
            m_customBeaconLocusEffect1.BodyFadeOut = true;
            m_customBeaconLocusEffect1.Style = BeaconEffectStyles.Shrink;//.HeartBeat;
            locusEffectsProvider1.AddLocusEffect(m_customBeaconLocusEffect1);

            mapObjectsLayer.DataLoaded += VectorItemsLayer_DataLoaded;  // Add listener event for Vector Layer Finished Loading Map Objects //
            mapControl1.RightClickDrag = false;  // Prevent dragging with the right mouse button //
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            VectorItemsLayer pointLayer = this.mapControl1.Layers[1] as VectorItemsLayer;
            pointLayer.ViewportChanged += new ViewportChangedEventHandler(pointLayer_ViewportChanged);
            
            LoadLastSavedUserScreenSettings();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();

            Application.DoEvents();  // Allow Form time to repaint itself [required for next line to work correctly] //
            bciLegend.Checked = true;  // Show Legend //
        }
        
        private void frm_Operatives_Location_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
            
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in arraylistFloatingPanels)
            {
                dp.Show();
            }
        }

        private void frm_Operatives_Location_Deactivate(object sender, EventArgs e)
        {
            arraylistFloatingPanels = new ArrayList();
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in dockManager1.Panels)
            {
                if (dp.Dock == DevExpress.XtraBars.Docking.DockingStyle.Float && dp.Visibility == DevExpress.XtraBars.Docking.DockVisibility.Visible)
                {
                    arraylistFloatingPanels.Add(dp);
                    dp.Hide();
                }
            }

        }

        private void frm_Operatives_Location_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "PingTypeFilter", i_str_selected_ping_type_ids);
                default_screen_settings.SaveDefaultScreenSettings();
            }
            mapObjectsLayer.DataLoaded -= VectorItemsLayer_DataLoaded;  // Remove Event //
        }


        void pointLayer_ViewportChanged(object sender, ViewportChangedEventArgs e)
        {
            /*VectorItemsLayer pointLayer = (VectorItemsLayer)sender;
            if (BestFitZoomInProcess)
            {
                int p_count = 0;
                for (int i = 0; i < pointLayer.Data.Count; i++)
                {
                    GeoPoint geoPoint = ((MapCustomElement)pointLayer.Data.GetItem(i)).Location;
                    if (Convert.ToInt32(pointLayer.Data.GetItem(i).Attributes["ContractorID"].Value) == intContractorIDToZoomTo)
                    {
                        if (geoPoint.Latitude < e.TopLeft.Latitude && geoPoint.Latitude > e.BottomRight.Latitude && geoPoint.Longitude > e.TopLeft.Longitude && geoPoint.Longitude < e.BottomRight.Longitude) p_count++;
                    }
                }
                if (p_count == intPointsToZoomToCount) BestFitZoomInProcess = false;
            }*/
        }

        public void LoadLastSavedUserScreenSettings()
        {
            barEditItemDateRange.EditValue = PopupContainerEdit1_Get_Selected();
            barEditItemMapType.EditValue = PopupContainerEdit2_Get_Selected();
            
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                int intFoundRow = 0;
                string strFilter = default_screen_settings.RetrieveSetting("PingTypeFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    Array array = strFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView view = (GridView)gridControl3.MainView;
                    view.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in array)
                    {
                        if (strElement == "") break;
                        intFoundRow = view.LocateByValue(0, view.Columns["PingTypeText"], strElement);
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            view.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    view.EndUpdate();
                    barEditItemPingType.EditValue = PopupContainerEdit3_Get_Selected();
                }
            }
       }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Teams Available - Adjust any filters";
                    break;
                case "gridView2":
                    message = "No Pings Available - Select one or more Teams to view linked Pings";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    view = (GridView)gridControl2.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region Date Range Filter Panel

        private void btnDateRangeFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditDateRangeFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            string strValue = "";
            if (dateEditStart.DateTime == DateTime.MinValue)
            {
                i_dtStart = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtStart = dateEditStart.DateTime;
            }
            if (dateEditEnd.DateTime == DateTime.MinValue)
            {
                i_dtEnd = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtEnd = dateEditEnd.DateTime;
            }
            if (dateEditStart.DateTime == DateTime.MinValue && dateEditEnd.DateTime == DateTime.MinValue)
            {
                strValue = "No Date Range Filter";
            }
            else
            {
                strValue = "Date: " + (i_dtStart == new DateTime(1900, 1, 1) ? "No Start" : i_dtStart.ToString("dd/MM/yyyy")) + " - " + (i_dtEnd == new DateTime(1900, 1, 1) ? "No End" : i_dtEnd.ToString("dd/MM/yyyy"));
            }

            i_tStart = StartTimeTimeEdit.Time.TimeOfDay;
            i_tEnd = EndTimeTimeEdit.Time.TimeOfDay;
            strValue += ", Time: " + i_tStart.ToString() + " - " + i_tEnd.ToString();

            return strValue;
        }


        #endregion


        #region Ping Type Panel

        private void btnPingTypeOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditPingType_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit3_Get_Selected();
        }

        private string PopupContainerEdit3_Get_Selected()
        {
            i_str_selected_ping_type_ids = "";    // Reset any prior values first //
            i_str_selected_ping_types = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl3.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_ping_type_ids = "";
                return "No Ping Type Filter";

            }
            else if (selection3.SelectedCount <= 0)
            {
                i_str_selected_ping_type_ids = "";
                return "No Ping Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_ping_type_ids += Convert.ToString(view.GetRowCellValue(i, "PingTypeText")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_ping_types = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_ping_types += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_ping_types;
        }

        #endregion


        #region Map Type

        private void btnOKMapType_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditMapType_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit2_Get_Selected();
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            string strTempType = "";
            if (checkEdit1.Checked)
            {
                strTempType = "Bing Hybrid";
                if (strTempType != i_str_MapType)
                {
                    BingMapDataProvider bingProvider = new BingMapDataProvider();
                    ImageTilesLayer layer = (ImageTilesLayer)mapControl1.Layers[0];
                    layer.DataProvider = bingProvider;
                    bingProvider.BingKey = "As_rq12T3oFpxWh52cGqK5yhqcHov7wkd4orPObzZFgo2N92CqPDc1xO_uokdUdT";
                    bingProvider.Kind = BingMapKind.Hybrid;
                    i_str_MapType = strTempType;
                }
            }
            else if (checkEdit2.Checked)
            {
                strTempType = "Bing Satellite";
                if (strTempType != i_str_MapType)
                {
                    BingMapDataProvider bingProvider = new BingMapDataProvider();
                    ImageTilesLayer layer = (ImageTilesLayer)mapControl1.Layers[0];
                    layer.DataProvider = bingProvider;
                    bingProvider.BingKey = "As_rq12T3oFpxWh52cGqK5yhqcHov7wkd4orPObzZFgo2N92CqPDc1xO_uokdUdT";
                    bingProvider.Kind = BingMapKind.Area;
                    i_str_MapType = strTempType;
                }
            }
            else if (checkEdit3.Checked)
            {
                strTempType = "Bing Road";
                if (strTempType != i_str_MapType)
                {
                    BingMapDataProvider bingProvider = new BingMapDataProvider();
                    ImageTilesLayer layer = (ImageTilesLayer)mapControl1.Layers[0];
                    layer.DataProvider = bingProvider;
                    bingProvider.BingKey = "As_rq12T3oFpxWh52cGqK5yhqcHov7wkd4orPObzZFgo2N92CqPDc1xO_uokdUdT";
                    bingProvider.Kind = BingMapKind.Road;
                    i_str_MapType = strTempType;
                }
            }
            else
            {
                strTempType = "Open Street Map";
                if (strTempType != i_str_MapType)
                {
                    OpenStreetMapDataProvider provider = new OpenStreetMapDataProvider();
                    ImageTilesLayer layer = (ImageTilesLayer)mapControl1.Layers[0];
                    layer.DataProvider = provider;
                    i_str_MapType = strTempType;
                }
            }
            return i_str_MapType;
        }

        #endregion


        #region GridControl1

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
        }

        public override void PostViewClick(object sender, EventArgs e, int row)
        {
            GridView view = (GridView)sender;
            if (view.GridControl.MainView.Name != "gridView1") return;  // Only fire for appropriate view //
            if (row == GridControl.InvalidRowHandle) return;
            btnLoadPings.Enabled = selection1.SelectedCount > 0;
        }

        #endregion


        #region GridControl2

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 2;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bbiViewObjectOnMap.Enabled = (intCount == 1 ? true : false);

                pmMapItemMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region Map Control

        private void mapControl1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            MapControl map = sender as MapControl;
            MapHitInfo hitInfo = map.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left)
            {
                return;
            }
            if (e.Button == MouseButtons.Right)
            {
                pmMapControl.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void bbiMapPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            WaitDialogForm loadingForm = new WaitDialogForm("Loading Preview...", "Print Preview");
            loadingForm.Show();

            frmPrintPreview fPrintPreview = new frmPrintPreview();
            fPrintPreview.loadingForm = loadingForm;
            fPrintPreview.objObject = this;
            fPrintPreview.MdiParent = this.MdiParent;
            fPrintPreview.GlobalSettings = this.GlobalSettings;
            fPrintPreview.StaffID = GlobalSettings.UserID;
            fPrintPreview.Show();
            
            // Invoke Post Open event on Base form //
            MethodInfo method = typeof(frmBase).GetMethod("PostOpen");
            if (method != null) method.Invoke(fPrintPreview, new object[] { null });
        }

        void VectorItemsLayer_DataLoaded(object sender, DataLoadedEventArgs e)
        {
            if (this.splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                this.splashScreenManager.CloseWaitForm();
            }
        }

        #endregion


        private void bbiRefreshTeams_ItemClick(object sender, ItemClickEventArgs e)
        {
            Load_Teams();
        }

        private void Load_Teams()
        {
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();
            this.RefreshGridViewState1.SaveViewInfo();
            gridControl1.BeginUpdate();
            try
            {
                sp06016_Get_Teams_From_LocationsTableAdapter.Fill(dataSet_GC_Summer_Core.sp06016_Get_Teams_From_Locations, i_dtStart, i_dtEnd, i_tStart, i_tEnd, i_str_selected_ping_type_ids);
                this.RefreshGridViewState1.LoadViewInfo();
            }
            catch (Exception ex)
            {
                gridControl1.EndUpdate();
                if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the Operatives...\n\nError: " + ex.Message, "Load Operatives", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            gridControl1.EndUpdate();
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
            bsiMapObjectCount.Caption = "Map Object Count: 0";
            this.dataSet_GC_Summer_Core.sp06126_Get_Operative_Pings.Clear();
            this.dataSet_GC_Summer_Core.sp06017_Get_Operative_Locations.Clear();
            selection1.ClearSelection();
            selection2.ClearSelection();
        }

        private void btnLoadPings_Click(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl1.MainView;
            if (view.DataRowCount <= 0) return;
            if (selection1.SelectedCount <= 0) return;

            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();
            string strSelectedIDs = "";
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                {
                    strSelectedIDs += Convert.ToString(view.GetRowCellValue(i, view.Columns["ContractorID"])) + ',';
                }
            }

            gridControl2.MainView.BeginUpdate();
            if (string.IsNullOrWhiteSpace(strSelectedIDs))
            {
                this.dataSet_GC_Summer_Core.sp06126_Get_Operative_Pings.Clear();
                this.dataSet_GC_Summer_Core.sp06017_Get_Operative_Locations.Clear();
                bsiMapObjectCount.Caption = "Map Object Count: 0";
            }
            else
            {
                try
                {
                    sp06126_Get_Operative_PingsTableAdapter.Fill(dataSet_GC_Summer_Core.sp06126_Get_Operative_Pings, i_dtStart, i_dtEnd, i_tStart, i_tEnd, strSelectedIDs, i_str_selected_ping_type_ids);
                    this.dataSet_GC_Summer_Core.sp06017_Get_Operative_Locations.Clear();
                    bsiMapObjectCount.Caption = "Map Object Count: 0";
                }
                catch (Exception ex) { }
            }
            gridControl2.MainView.EndUpdate();
            view = (GridView)gridControl2.MainView;
            view.ExpandAllGroups();
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
            selection2.ClearSelection();
        }

        private void btnRefreshMap_Click(object sender, EventArgs e)
        {
            int intProcessedCount = 0;
            GridView view = (GridView)gridControl2.MainView;
            if (selection2.SelectedCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more pings to render on the map before proceeding.", "Refresh Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            this.splashScreenManager.ShowWaitForm();
            this.splashScreenManager.SetWaitFormDescription("Loading Objects into Map...");

            string strPingIDs = "";
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                {
                    intProcessedCount++;
                    strPingIDs += Convert.ToString(view.GetRowCellValue(i, "OperativeLocationID")) + ",";
                    if (intProcessedCount >= selection2.SelectedCount) break;
                }
            }

            try
            {
                VectorItemsLayer layer = (VectorItemsLayer)mapControl1.Layers[1];
                mapControl1.SuspendRender();
                //layer.Items.BeginUpdate();
                sp06017_Get_Operative_LocationsTableAdapter.Fill(dataSet_GC_Summer_Core.sp06017_Get_Operative_Locations, strPingIDs);
                //layer.Items.EndUpdate();
                mapControl1.ResumeRender();
                bsiMapObjectCount.Caption = "Map Object Count: " + dataSet_GC_Summer_Core.sp06017_Get_Operative_Locations.Rows.Count.ToString();
            }
            catch (Exception ex)
            {
                mapControl1.ResumeRender();
                if (this.splashScreenManager.IsSplashFormVisible)
                {
                    this.splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                    this.splashScreenManager.CloseWaitForm();
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the map...\n\nError: " + ex.Message, "Refresh Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Close of Loading Message done from VectorItemsLayer_DataLoaded event //
        }

        private void bbiViewObjectOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (i_int_FocusedGrid == 1)
            {
                /*GridView view = (GridView)gridControl1.MainView;
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                if (intCount != 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select only one Team to Zoom the map to before proceeding.", "View Object On Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                intContractorIDToZoomTo = Convert.ToInt32(view.GetFocusedRowCellValue("ContractorID"));
                intPointsToZoomToCount = 0;

                VectorItemsLayer pointLayer = mapControl1.Layers[1] as VectorItemsLayer;
                if (pointLayer.Items.Count > 0)
                {
                    double Lat = 0;
                    double Long = 0;
                    int i = 0;
                    for (i = 0; i < pointLayer.Items.Count; i++)
                    {
                        if (Convert.ToInt32(pointLayer.Items[i].Attributes["ContractorID"].Value) == intContractorIDToZoomTo)
                        {
                            Lat = Lat + ((MapCustomElement)pointLayer.Items[i]).Location.Latitude;
                            Long = Long + ((MapCustomElement)pointLayer.Items[i]).Location.Longitude;
                            intPointsToZoomToCount++;
                        }
                    }

                    if (intPointsToZoomToCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to find selected Map Object. Click the Load Selected Pings into Map button before trying again.", "View Object On Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    GeoPoint centerPoint = new GeoPoint(Lat / intPointsToZoomToCount, Long / intPointsToZoomToCount);
                    mapControl1.CenterPoint = centerPoint;

                    BestFitZoomInProcess = true;
                    i = 20;
                    while (i > 0 && BestFitZoomInProcess)
                    {
                        mapControl1.ZoomLevel = i;
                        i--;
                    }
                    mapControl1.Refresh();
                }*/
            }
            else  // Grid 2 //
            {
                GridView view = (GridView)gridControl2.MainView;
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                if (intCount != 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select only one Ping to Zoom the map to before proceeding.", "View Object On Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                intContractorIDToZoomTo = Convert.ToInt32(view.GetFocusedRowCellValue("OperativeLocationID"));
                intPointsToZoomToCount = 0;

                VectorItemsLayer pointLayer = mapControl1.Layers[1] as VectorItemsLayer;
                if (pointLayer.Data.Count > 0)
                {
                    double Lat = 0;
                    double Long = 0;
                    int i = 0;
                    for (i = 0; i < pointLayer.Data.Count; i++)
                    {
                        if (Convert.ToInt32(pointLayer.Data.GetItem(i).Attributes["OperativeLocationID"].Value) == intContractorIDToZoomTo)
                        {
                            Lat = Lat + ((GeoPoint)((MapCustomElement)pointLayer.Data.GetItem(i)).Location).Latitude;
                            Long = Long + ((GeoPoint)((MapCustomElement)pointLayer.Data.GetItem(i)).Location).Longitude;
                            intPointsToZoomToCount++;
                        }
                    }

                    if (intPointsToZoomToCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to find selected Map Object. Click the Load Selected Pings into Map button before trying again.", "View Object On Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    GeoPoint centerPoint = new GeoPoint(Lat / intPointsToZoomToCount, Long / intPointsToZoomToCount);

                    // Do both items in one step //
                    BeginInvoke(new MethodInvoker(() =>
                    {
                        mapControl1.ZoomLevel = 16;
                        mapControl1.CenterPoint = centerPoint;
                    }));
                    
                    mapControl1.Refresh();
                    Point location = mapControl1.PointToScreen(Point.Empty);

                    //MapPoint mp = pointLayer.GeoToScreenPoint(mapControl1.CenterPoint);
                    MapPoint mp = mapControl1.CoordPointToScreenPoint(mapControl1.CenterPoint);
                    System.Drawing.Point screenPoint = new Point(location.X + Convert.ToInt32(mp.X), location.Y + Convert.ToInt32(mp.Y));
                    locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customBeaconLocusEffect1.Name);

                }
            }
        }

        private void bbiExportToExcel_ItemClick(object sender, ItemClickEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            string strFilename = "Export_" + String.Format("{0:yyyy-MM-dd_mm-ss}", DateTime.Now);
            sfd.FileName = strFilename;
            sfd.Filter = "Xlsx-Files (*.xlsx)|*.xlsx";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    gridControl2.ExportToXlsx(sfd.FileName);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Data Exported Sucessfully", "Export Pings", MessageBoxButtons.OK);
                    System.Diagnostics.Process.Start(sfd.FileName);
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("There was a problem exporting Pings. Please try again.If the problem persists, please contact Technical Support.", "Export Pings", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    throw;
                }
            }
        }


        #region Legend

        private void bciLegend_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (bciLegend.Checked)
            {
                if (dockPanelLegend.Visibility == DevExpress.XtraBars.Docking.DockVisibility.Hidden)
                {
                    dockPanelLegend.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
                    if (dockPanelLegend.Dock == DockingStyle.Float)
                    {
                        // Important note: Forms StartPosition needs to be set to Manual in the Form's properties //
                        dockPanelLegend.FloatLocation = new System.Drawing.Point((PointToScreen(mapControl1.Location).X) + mapControl1.Width - (dockPanelLegend.Width + 9), PointToScreen(mapControl1.Location).Y + 57);
                        //dockPanelLegend.FloatLocation = new System.Drawing.Point((PointToScreen(mapControl1.Location).X) + 9, PointToScreen(mapControl1.Location).Y + 9);
                    }
                }
            }
            else
            {
                if (dockPanelLegend.Visibility != DevExpress.XtraBars.Docking.DockVisibility.Hidden) dockPanelLegend.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            }

        }

        private void dockPanelLegend_ClosingPanel(object sender, DockPanelCancelEventArgs e)
        {
            if (bciLegend.Checked) bciLegend.Checked = false;
        }

        private void dockPanelLegend_DockChanged(object sender, EventArgs e)
        {
            // Make semi transparent when undocked //
            DockPanel panel = sender as DockPanel;
            if (panel.FloatForm != null)
            {
                panel.FloatForm.AllowTransparency = true;
                panel.FloatForm.Opacity = 0.9;
            }
        }

        #endregion








    }
}

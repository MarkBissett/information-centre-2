namespace WoodPlan5
{
    partial class frm_OM_Select_Job_Collection_Template_Item_Rule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Select_Job_Collection_Template_Item_Rule));
            this.repositoryItemCheckEditAllowSiteSelection = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp06172OMJobCollectionTemplateHeadersSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobCollectionTemplateHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedItemCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp06173OMJobCollectionTemplateItemsSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobCollectionTemplateItemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobCollectionTemplateHeaderID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colHeaderDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp06332OMJobCollectionTemplateItemRulesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobCollectionTemplateItemRuleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobCollectionTemplateItemID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colJobCollectionTemplateHeaderID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderRecordOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedVisitCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.sp06172_OM_Job_Collection_Template_Headers_SelectTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06172_OM_Job_Collection_Template_Headers_SelectTableAdapter();
            this.sp06173_OM_Job_Collection_Template_Items_SelectTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06173_OM_Job_Collection_Template_Items_SelectTableAdapter();
            this.sp06332_OM_Job_Collection_Template_Item_RulesTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06332_OM_Job_Collection_Template_Item_RulesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditAllowSiteSelection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06172OMJobCollectionTemplateHeadersSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06173OMJobCollectionTemplateItemsSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06332OMJobCollectionTemplateItemRulesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(645, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 566);
            this.barDockControlBottom.Size = new System.Drawing.Size(645, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 540);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(645, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 540);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.MaxItemId = 35;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // repositoryItemCheckEditAllowSiteSelection
            // 
            this.repositoryItemCheckEditAllowSiteSelection.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditAllowSiteSelection.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditAllowSiteSelection.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditAllowSiteSelection.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditAllowSiteSelection.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditAllowSiteSelection.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditAllowSiteSelection.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditAllowSiteSelection.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditAllowSiteSelection.AutoHeight = false;
            this.repositoryItemCheckEditAllowSiteSelection.Caption = "Allow Site Selection:";
            this.repositoryItemCheckEditAllowSiteSelection.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemCheckEditAllowSiteSelection.LookAndFeel.SkinName = "Blue";
            this.repositoryItemCheckEditAllowSiteSelection.Name = "repositoryItemCheckEditAllowSiteSelection";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 26);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Job Collection Template Headers ";
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Size = new System.Drawing.Size(645, 505);
            this.splitContainerControl1.SplitterPosition = 278;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            this.gridControl1.DataSource = this.sp06172OMJobCollectionTemplateHeadersSelectBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.gridControl1.Size = new System.Drawing.Size(620, 218);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp06172OMJobCollectionTemplateHeadersSelectBindingSource
            // 
            this.sp06172OMJobCollectionTemplateHeadersSelectBindingSource.DataMember = "sp06172_OM_Job_Collection_Template_Headers_Select";
            this.sp06172OMJobCollectionTemplateHeadersSelectBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobCollectionTemplateHeaderID,
            this.colDescription,
            this.colRecordOrder,
            this.colRemarks,
            this.colLinkedItemCount,
            this.colClientID,
            this.colClientName});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.FindDelay = 2000;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colJobCollectionTemplateHeaderID
            // 
            this.colJobCollectionTemplateHeaderID.Caption = "Job Collection Template Header ID";
            this.colJobCollectionTemplateHeaderID.FieldName = "JobCollectionTemplateHeaderID";
            this.colJobCollectionTemplateHeaderID.Name = "colJobCollectionTemplateHeaderID";
            this.colJobCollectionTemplateHeaderID.OptionsColumn.AllowEdit = false;
            this.colJobCollectionTemplateHeaderID.OptionsColumn.AllowFocus = false;
            this.colJobCollectionTemplateHeaderID.OptionsColumn.ReadOnly = true;
            this.colJobCollectionTemplateHeaderID.Width = 186;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 266;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Record Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            this.colRecordOrder.Width = 99;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 2;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colLinkedItemCount
            // 
            this.colLinkedItemCount.Caption = "Linked Items";
            this.colLinkedItemCount.FieldName = "LinkedItemCount";
            this.colLinkedItemCount.Name = "colLinkedItemCount";
            this.colLinkedItemCount.OptionsColumn.AllowEdit = false;
            this.colLinkedItemCount.OptionsColumn.AllowFocus = false;
            this.colLinkedItemCount.OptionsColumn.ReadOnly = true;
            this.colLinkedItemCount.Visible = true;
            this.colLinkedItemCount.VisibleIndex = 3;
            this.colLinkedItemCount.Width = 81;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            this.colClientID.Width = 60;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 1;
            this.colClientName.Width = 150;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControl2);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Template Items ";
            this.splitContainerControl2.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel2.Controls.Add(this.gridControl3);
            this.splitContainerControl2.Panel2.ShowCaption = true;
            this.splitContainerControl2.Panel2.Text = "Item Rules";
            this.splitContainerControl2.Size = new System.Drawing.Size(641, 274);
            this.splitContainerControl2.SplitterPosition = 142;
            this.splitContainerControl2.TabIndex = 1;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp06173OMJobCollectionTemplateItemsSelectBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.gridControl2.Size = new System.Drawing.Size(616, 139);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp06173OMJobCollectionTemplateItemsSelectBindingSource
            // 
            this.sp06173OMJobCollectionTemplateItemsSelectBindingSource.DataMember = "sp06173_OM_Job_Collection_Template_Items_Select";
            this.sp06173OMJobCollectionTemplateItemsSelectBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobCollectionTemplateItemID,
            this.colJobCollectionTemplateHeaderID1,
            this.colJobSubTypeID,
            this.colRemarks1,
            this.colHeaderDescription,
            this.colJobSubTypeDescription,
            this.colHeaderRecordOrder});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsFind.FindDelay = 2000;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobSubTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colJobCollectionTemplateItemID
            // 
            this.colJobCollectionTemplateItemID.Caption = "Job Collection Template Item ID";
            this.colJobCollectionTemplateItemID.FieldName = "JobCollectionTemplateItemID";
            this.colJobCollectionTemplateItemID.Name = "colJobCollectionTemplateItemID";
            this.colJobCollectionTemplateItemID.OptionsColumn.AllowEdit = false;
            this.colJobCollectionTemplateItemID.OptionsColumn.AllowFocus = false;
            this.colJobCollectionTemplateItemID.OptionsColumn.ReadOnly = true;
            this.colJobCollectionTemplateItemID.Width = 173;
            // 
            // colJobCollectionTemplateHeaderID1
            // 
            this.colJobCollectionTemplateHeaderID1.Caption = "Job Collection Template Header ID";
            this.colJobCollectionTemplateHeaderID1.FieldName = "JobCollectionTemplateHeaderID";
            this.colJobCollectionTemplateHeaderID1.Name = "colJobCollectionTemplateHeaderID1";
            this.colJobCollectionTemplateHeaderID1.OptionsColumn.AllowEdit = false;
            this.colJobCollectionTemplateHeaderID1.OptionsColumn.AllowFocus = false;
            this.colJobCollectionTemplateHeaderID1.OptionsColumn.ReadOnly = true;
            this.colJobCollectionTemplateHeaderID1.Width = 186;
            // 
            // colJobSubTypeID
            // 
            this.colJobSubTypeID.Caption = "Job Sub Type ID";
            this.colJobSubTypeID.FieldName = "JobSubTypeID";
            this.colJobSubTypeID.Name = "colJobSubTypeID";
            this.colJobSubTypeID.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeID.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeID.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeID.Width = 100;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 1;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colHeaderDescription
            // 
            this.colHeaderDescription.Caption = "Header Description";
            this.colHeaderDescription.FieldName = "HeaderDescription";
            this.colHeaderDescription.Name = "colHeaderDescription";
            this.colHeaderDescription.OptionsColumn.AllowEdit = false;
            this.colHeaderDescription.OptionsColumn.AllowFocus = false;
            this.colHeaderDescription.OptionsColumn.ReadOnly = true;
            this.colHeaderDescription.Width = 274;
            // 
            // colJobSubTypeDescription
            // 
            this.colJobSubTypeDescription.Caption = "Job Sub-Type";
            this.colJobSubTypeDescription.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription.Name = "colJobSubTypeDescription";
            this.colJobSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription.Visible = true;
            this.colJobSubTypeDescription.VisibleIndex = 0;
            this.colJobSubTypeDescription.Width = 275;
            // 
            // colHeaderRecordOrder
            // 
            this.colHeaderRecordOrder.Caption = "Header Order";
            this.colHeaderRecordOrder.FieldName = "HeaderRecordOrder";
            this.colHeaderRecordOrder.Name = "colHeaderRecordOrder";
            this.colHeaderRecordOrder.OptionsColumn.AllowEdit = false;
            this.colHeaderRecordOrder.OptionsColumn.AllowFocus = false;
            this.colHeaderRecordOrder.OptionsColumn.ReadOnly = true;
            this.colHeaderRecordOrder.Width = 87;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp06332OMJobCollectionTemplateItemRulesBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3});
            this.gridControl3.Size = new System.Drawing.Size(616, 123);
            this.gridControl3.TabIndex = 9;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp06332OMJobCollectionTemplateItemRulesBindingSource
            // 
            this.sp06332OMJobCollectionTemplateItemRulesBindingSource.DataMember = "sp06332_OM_Job_Collection_Template_Item_Rules";
            this.sp06332OMJobCollectionTemplateItemRulesBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobCollectionTemplateItemRuleID,
            this.colJobCollectionTemplateItemID1,
            this.colDescription1,
            this.colRecordOrder1,
            this.colRemarks2,
            this.colJobCollectionTemplateHeaderID2,
            this.colJobSubTypeID1,
            this.colHeaderDescription1,
            this.colJobSubTypeDescription1,
            this.colHeaderRecordOrder1,
            this.colLinkedVisitCount});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView3.OptionsFilter.AllowFilterEditor = false;
            this.gridView3.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView3.OptionsFilter.AllowMRUFilterList = false;
            this.gridView3.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView3.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView3.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView3.OptionsFind.FindDelay = 2000;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsLayout.StoreFormatRules = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colHeaderRecordOrder1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colJobCollectionTemplateItemRuleID
            // 
            this.colJobCollectionTemplateItemRuleID.Caption = "Job Collection Template Item Rule ID";
            this.colJobCollectionTemplateItemRuleID.FieldName = "JobCollectionTemplateItemRuleID";
            this.colJobCollectionTemplateItemRuleID.Name = "colJobCollectionTemplateItemRuleID";
            this.colJobCollectionTemplateItemRuleID.OptionsColumn.AllowEdit = false;
            this.colJobCollectionTemplateItemRuleID.OptionsColumn.AllowFocus = false;
            this.colJobCollectionTemplateItemRuleID.OptionsColumn.ReadOnly = true;
            this.colJobCollectionTemplateItemRuleID.Width = 195;
            // 
            // colJobCollectionTemplateItemID1
            // 
            this.colJobCollectionTemplateItemID1.Caption = "Job Collection Template Item ID";
            this.colJobCollectionTemplateItemID1.FieldName = "JobCollectionTemplateItemID";
            this.colJobCollectionTemplateItemID1.Name = "colJobCollectionTemplateItemID1";
            this.colJobCollectionTemplateItemID1.OptionsColumn.AllowEdit = false;
            this.colJobCollectionTemplateItemID1.OptionsColumn.AllowFocus = false;
            this.colJobCollectionTemplateItemID1.OptionsColumn.ReadOnly = true;
            this.colJobCollectionTemplateItemID1.Width = 171;
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Rule Description";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 203;
            // 
            // colRecordOrder1
            // 
            this.colRecordOrder1.Caption = "Rule Order";
            this.colRecordOrder1.FieldName = "RecordOrder";
            this.colRecordOrder1.Name = "colRecordOrder1";
            this.colRecordOrder1.OptionsColumn.AllowEdit = false;
            this.colRecordOrder1.OptionsColumn.AllowFocus = false;
            this.colRecordOrder1.OptionsColumn.ReadOnly = true;
            this.colRecordOrder1.Visible = true;
            this.colRecordOrder1.VisibleIndex = 1;
            this.colRecordOrder1.Width = 84;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 3;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colJobCollectionTemplateHeaderID2
            // 
            this.colJobCollectionTemplateHeaderID2.Caption = "Job Collection Template Header ID";
            this.colJobCollectionTemplateHeaderID2.FieldName = "JobCollectionTemplateHeaderID";
            this.colJobCollectionTemplateHeaderID2.Name = "colJobCollectionTemplateHeaderID2";
            this.colJobCollectionTemplateHeaderID2.OptionsColumn.AllowEdit = false;
            this.colJobCollectionTemplateHeaderID2.OptionsColumn.AllowFocus = false;
            this.colJobCollectionTemplateHeaderID2.OptionsColumn.ReadOnly = true;
            this.colJobCollectionTemplateHeaderID2.Width = 184;
            // 
            // colJobSubTypeID1
            // 
            this.colJobSubTypeID1.Caption = "Job Sub Type ID";
            this.colJobSubTypeID1.FieldName = "JobSubTypeID";
            this.colJobSubTypeID1.Name = "colJobSubTypeID1";
            this.colJobSubTypeID1.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeID1.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeID1.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeID1.Width = 100;
            // 
            // colHeaderDescription1
            // 
            this.colHeaderDescription1.Caption = "Template Header";
            this.colHeaderDescription1.FieldName = "HeaderDescription";
            this.colHeaderDescription1.Name = "colHeaderDescription1";
            this.colHeaderDescription1.OptionsColumn.AllowEdit = false;
            this.colHeaderDescription1.OptionsColumn.AllowFocus = false;
            this.colHeaderDescription1.OptionsColumn.ReadOnly = true;
            this.colHeaderDescription1.Width = 224;
            // 
            // colJobSubTypeDescription1
            // 
            this.colJobSubTypeDescription1.Caption = "Job Sub-Type";
            this.colJobSubTypeDescription1.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription1.Name = "colJobSubTypeDescription1";
            this.colJobSubTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription1.Width = 211;
            // 
            // colHeaderRecordOrder1
            // 
            this.colHeaderRecordOrder1.Caption = "Template Order";
            this.colHeaderRecordOrder1.FieldName = "HeaderRecordOrder";
            this.colHeaderRecordOrder1.Name = "colHeaderRecordOrder1";
            this.colHeaderRecordOrder1.OptionsColumn.AllowEdit = false;
            this.colHeaderRecordOrder1.OptionsColumn.AllowFocus = false;
            this.colHeaderRecordOrder1.OptionsColumn.ReadOnly = true;
            this.colHeaderRecordOrder1.Width = 107;
            // 
            // colLinkedVisitCount
            // 
            this.colLinkedVisitCount.Caption = "Linked Visits";
            this.colLinkedVisitCount.FieldName = "LinkedVisitCount";
            this.colLinkedVisitCount.Name = "colLinkedVisitCount";
            this.colLinkedVisitCount.OptionsColumn.AllowEdit = false;
            this.colLinkedVisitCount.OptionsColumn.AllowFocus = false;
            this.colLinkedVisitCount.OptionsColumn.ReadOnly = true;
            this.colLinkedVisitCount.Visible = true;
            this.colLinkedVisitCount.VisibleIndex = 2;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(564, 537);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(483, 537);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // sp06172_OM_Job_Collection_Template_Headers_SelectTableAdapter
            // 
            this.sp06172_OM_Job_Collection_Template_Headers_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // sp06173_OM_Job_Collection_Template_Items_SelectTableAdapter
            // 
            this.sp06173_OM_Job_Collection_Template_Items_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // sp06332_OM_Job_Collection_Template_Item_RulesTableAdapter
            // 
            this.sp06332_OM_Job_Collection_Template_Item_RulesTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Select_Job_Collection_Template_Item_Rule
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(645, 566);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_OM_Select_Job_Collection_Template_Item_Rule";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Job Collection Template Item Rule";
            this.Load += new System.EventHandler(this.frm_OM_Select_Job_Collection_Template_Item_Rule_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditAllowSiteSelection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06172OMJobCollectionTemplateHeadersSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06173OMJobCollectionTemplateItemsSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06332OMJobCollectionTemplateItemRulesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditAllowSiteSelection;
        private DataSet_OM_Job dataSet_OM_Job;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        public DevExpress.XtraGrid.GridControl gridControl2;
        private System.Windows.Forms.BindingSource sp06172OMJobCollectionTemplateHeadersSelectBindingSource;
        private DataSet_OM_JobTableAdapters.sp06172_OM_Job_Collection_Template_Headers_SelectTableAdapter sp06172_OM_Job_Collection_Template_Headers_SelectTableAdapter;
        private System.Windows.Forms.BindingSource sp06173OMJobCollectionTemplateItemsSelectBindingSource;
        private DataSet_OM_JobTableAdapters.sp06173_OM_Job_Collection_Template_Items_SelectTableAdapter sp06173_OM_Job_Collection_Template_Items_SelectTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCollectionTemplateHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedItemCount;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCollectionTemplateItemID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCollectionTemplateHeaderID1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderRecordOrder;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        public DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.BindingSource sp06332OMJobCollectionTemplateItemRulesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCollectionTemplateItemRuleID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCollectionTemplateItemID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCollectionTemplateHeaderID2;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderRecordOrder1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedVisitCount;
        private DataSet_OM_JobTableAdapters.sp06332_OM_Job_Collection_Template_Item_RulesTableAdapter sp06332_OM_Job_Collection_Template_Item_RulesTableAdapter;
    }
}

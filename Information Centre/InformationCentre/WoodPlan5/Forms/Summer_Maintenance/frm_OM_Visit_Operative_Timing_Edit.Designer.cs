namespace WoodPlan5
{
    partial class frm_OM_Visit_Operative_Timing_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Visit_Operative_Timing_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLinkedDocuments = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.LinkedToPersonTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp06306OMVisitOperativeTimingEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.LinkedToPersonTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.TeamNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OperativeNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OperativeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitOperativeTimingIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameContractDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.HoursUsedSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.LinkedToParentDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForVisitID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitOperativeTimingID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOperativeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToPersonTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLinkedToParentDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTeamName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOperativeName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHoursUsed = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToPersonType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientNameContractDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp06306_OM_Visit_Operative_Timing_EditTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06306_OM_Visit_Operative_Timing_EditTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06306OMVisitOperativeTimingEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OperativeNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OperativeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitOperativeTimingIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameContractDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HoursUsedSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToParentDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitOperativeTimingID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOperativeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToParentDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOperativeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHoursUsed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientNameContractDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 614);
            this.barDockControlBottom.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 588);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(686, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 588);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiLinkedDocuments});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLinkedDocuments, true)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiLinkedDocuments
            // 
            this.bbiLinkedDocuments.Caption = "Linked Documents";
            this.bbiLinkedDocuments.Enabled = false;
            this.bbiLinkedDocuments.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLinkedDocuments.Glyph")));
            this.bbiLinkedDocuments.Id = 15;
            this.bbiLinkedDocuments.Name = "bbiLinkedDocuments";
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Linked Documents - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to open the Linked Documents Manager, filtered on the current data entry" +
    " record.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiLinkedDocuments.SuperTip = superToolTip3;
            this.bbiLinkedDocuments.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiLinkedDocuments.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLinkedDocuments_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Form Mode - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.barStaticItemFormMode.SuperTip = superToolTip4;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(686, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 614);
            this.barDockControl2.Size = new System.Drawing.Size(686, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 588);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(686, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 588);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.LinkedToPersonTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToPersonTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.TeamNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.OperativeNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.OperativeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitOperativeTimingIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientContractIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteContractIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameContractDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.HoursUsedSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.LinkedToParentDescriptionButtonEdit);
            this.dataLayoutControl1.DataSource = this.sp06306OMVisitOperativeTimingEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForVisitID,
            this.ItemForSiteID,
            this.ItemForSiteContractID,
            this.ItemForClientName,
            this.ItemForClientID,
            this.ItemForClientContractID,
            this.ItemForVisitNumber,
            this.ItemForVisitOperativeTimingID,
            this.ItemForOperativeID,
            this.ItemForLinkedToPersonTypeID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1234, 162, 301, 422);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(686, 588);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // LinkedToPersonTypeTextEdit
            // 
            this.LinkedToPersonTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06306OMVisitOperativeTimingEditBindingSource, "LinkedToPersonType", true));
            this.LinkedToPersonTypeTextEdit.Location = new System.Drawing.Point(110, 152);
            this.LinkedToPersonTypeTextEdit.MenuManager = this.barManager1;
            this.LinkedToPersonTypeTextEdit.Name = "LinkedToPersonTypeTextEdit";
            this.LinkedToPersonTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToPersonTypeTextEdit, true);
            this.LinkedToPersonTypeTextEdit.Size = new System.Drawing.Size(552, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToPersonTypeTextEdit, optionsSpelling1);
            this.LinkedToPersonTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToPersonTypeTextEdit.TabIndex = 74;
            // 
            // sp06306OMVisitOperativeTimingEditBindingSource
            // 
            this.sp06306OMVisitOperativeTimingEditBindingSource.DataMember = "sp06306_OM_Visit_Operative_Timing_Edit";
            this.sp06306OMVisitOperativeTimingEditBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // LinkedToPersonTypeIDTextEdit
            // 
            this.LinkedToPersonTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("ReadOnly", this.sp06306OMVisitOperativeTimingEditBindingSource, "LinkedToPersonTypeID", true));
            this.LinkedToPersonTypeIDTextEdit.Location = new System.Drawing.Point(153, 371);
            this.LinkedToPersonTypeIDTextEdit.MenuManager = this.barManager1;
            this.LinkedToPersonTypeIDTextEdit.Name = "LinkedToPersonTypeIDTextEdit";
            this.LinkedToPersonTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToPersonTypeIDTextEdit, true);
            this.LinkedToPersonTypeIDTextEdit.Size = new System.Drawing.Size(509, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToPersonTypeIDTextEdit, optionsSpelling2);
            this.LinkedToPersonTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToPersonTypeIDTextEdit.TabIndex = 73;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06306OMVisitOperativeTimingEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(110, 248);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.RemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(552, 143);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling3);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 72;
            // 
            // TeamNameTextEdit
            // 
            this.TeamNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06306OMVisitOperativeTimingEditBindingSource, "TeamName", true));
            this.TeamNameTextEdit.Location = new System.Drawing.Point(110, 176);
            this.TeamNameTextEdit.MenuManager = this.barManager1;
            this.TeamNameTextEdit.Name = "TeamNameTextEdit";
            this.TeamNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TeamNameTextEdit, true);
            this.TeamNameTextEdit.Size = new System.Drawing.Size(552, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TeamNameTextEdit, optionsSpelling4);
            this.TeamNameTextEdit.StyleController = this.dataLayoutControl1;
            this.TeamNameTextEdit.TabIndex = 71;
            // 
            // OperativeNameTextEdit
            // 
            this.OperativeNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06306OMVisitOperativeTimingEditBindingSource, "OperativeName", true));
            this.OperativeNameTextEdit.Location = new System.Drawing.Point(110, 200);
            this.OperativeNameTextEdit.MenuManager = this.barManager1;
            this.OperativeNameTextEdit.Name = "OperativeNameTextEdit";
            this.OperativeNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.OperativeNameTextEdit, true);
            this.OperativeNameTextEdit.Size = new System.Drawing.Size(552, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.OperativeNameTextEdit, optionsSpelling5);
            this.OperativeNameTextEdit.StyleController = this.dataLayoutControl1;
            this.OperativeNameTextEdit.TabIndex = 70;
            // 
            // OperativeIDTextEdit
            // 
            this.OperativeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06306OMVisitOperativeTimingEditBindingSource, "OperativeID", true));
            this.OperativeIDTextEdit.Location = new System.Drawing.Point(122, 294);
            this.OperativeIDTextEdit.MenuManager = this.barManager1;
            this.OperativeIDTextEdit.Name = "OperativeIDTextEdit";
            this.OperativeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.OperativeIDTextEdit, true);
            this.OperativeIDTextEdit.Size = new System.Drawing.Size(528, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.OperativeIDTextEdit, optionsSpelling6);
            this.OperativeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.OperativeIDTextEdit.TabIndex = 69;
            // 
            // VisitOperativeTimingIDTextEdit
            // 
            this.VisitOperativeTimingIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06306OMVisitOperativeTimingEditBindingSource, "VisitOperativeTimingID", true));
            this.VisitOperativeTimingIDTextEdit.Location = new System.Drawing.Point(136, 556);
            this.VisitOperativeTimingIDTextEdit.MenuManager = this.barManager1;
            this.VisitOperativeTimingIDTextEdit.Name = "VisitOperativeTimingIDTextEdit";
            this.VisitOperativeTimingIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitOperativeTimingIDTextEdit, true);
            this.VisitOperativeTimingIDTextEdit.Size = new System.Drawing.Size(538, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitOperativeTimingIDTextEdit, optionsSpelling7);
            this.VisitOperativeTimingIDTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitOperativeTimingIDTextEdit.TabIndex = 68;
            // 
            // ClientContractIDTextEdit
            // 
            this.ClientContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06306OMVisitOperativeTimingEditBindingSource, "ClientContractID", true));
            this.ClientContractIDTextEdit.Location = new System.Drawing.Point(117, 342);
            this.ClientContractIDTextEdit.MenuManager = this.barManager1;
            this.ClientContractIDTextEdit.Name = "ClientContractIDTextEdit";
            this.ClientContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientContractIDTextEdit, true);
            this.ClientContractIDTextEdit.Size = new System.Drawing.Size(557, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientContractIDTextEdit, optionsSpelling8);
            this.ClientContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientContractIDTextEdit.TabIndex = 67;
            this.ClientContractIDTextEdit.TabStop = false;
            // 
            // SiteContractIDTextEdit
            // 
            this.SiteContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06306OMVisitOperativeTimingEditBindingSource, "SiteContractID", true));
            this.SiteContractIDTextEdit.Location = new System.Drawing.Point(142, 462);
            this.SiteContractIDTextEdit.MenuManager = this.barManager1;
            this.SiteContractIDTextEdit.Name = "SiteContractIDTextEdit";
            this.SiteContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteContractIDTextEdit, true);
            this.SiteContractIDTextEdit.Size = new System.Drawing.Size(532, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteContractIDTextEdit, optionsSpelling9);
            this.SiteContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteContractIDTextEdit.TabIndex = 66;
            this.SiteContractIDTextEdit.TabStop = false;
            // 
            // ClientNameTextEdit
            // 
            this.ClientNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06306OMVisitOperativeTimingEditBindingSource, "ClientName", true));
            this.ClientNameTextEdit.Location = new System.Drawing.Point(117, 390);
            this.ClientNameTextEdit.MenuManager = this.barManager1;
            this.ClientNameTextEdit.Name = "ClientNameTextEdit";
            this.ClientNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameTextEdit, true);
            this.ClientNameTextEdit.Size = new System.Drawing.Size(557, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameTextEdit, optionsSpelling10);
            this.ClientNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameTextEdit.TabIndex = 57;
            this.ClientNameTextEdit.TabStop = false;
            // 
            // VisitNumberTextEdit
            // 
            this.VisitNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06306OMVisitOperativeTimingEditBindingSource, "VisitNumber", true));
            this.VisitNumberTextEdit.Location = new System.Drawing.Point(117, 83);
            this.VisitNumberTextEdit.MenuManager = this.barManager1;
            this.VisitNumberTextEdit.Name = "VisitNumberTextEdit";
            this.VisitNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitNumberTextEdit, true);
            this.VisitNumberTextEdit.Size = new System.Drawing.Size(557, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitNumberTextEdit, optionsSpelling11);
            this.VisitNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitNumberTextEdit.TabIndex = 56;
            this.VisitNumberTextEdit.TabStop = false;
            // 
            // SiteNameTextEdit
            // 
            this.SiteNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06306OMVisitOperativeTimingEditBindingSource, "SiteName", true));
            this.SiteNameTextEdit.Location = new System.Drawing.Point(98, 59);
            this.SiteNameTextEdit.MenuManager = this.barManager1;
            this.SiteNameTextEdit.Name = "SiteNameTextEdit";
            this.SiteNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteNameTextEdit, true);
            this.SiteNameTextEdit.Size = new System.Drawing.Size(576, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteNameTextEdit, optionsSpelling12);
            this.SiteNameTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteNameTextEdit.TabIndex = 55;
            this.SiteNameTextEdit.TabStop = false;
            // 
            // ClientNameContractDescriptionTextEdit
            // 
            this.ClientNameContractDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06306OMVisitOperativeTimingEditBindingSource, "ClientNameContractDescription", true));
            this.ClientNameContractDescriptionTextEdit.Location = new System.Drawing.Point(98, 35);
            this.ClientNameContractDescriptionTextEdit.MenuManager = this.barManager1;
            this.ClientNameContractDescriptionTextEdit.Name = "ClientNameContractDescriptionTextEdit";
            this.ClientNameContractDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameContractDescriptionTextEdit, true);
            this.ClientNameContractDescriptionTextEdit.Size = new System.Drawing.Size(576, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameContractDescriptionTextEdit, optionsSpelling13);
            this.ClientNameContractDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameContractDescriptionTextEdit.TabIndex = 54;
            this.ClientNameContractDescriptionTextEdit.TabStop = false;
            // 
            // VisitIDTextEdit
            // 
            this.VisitIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06306OMVisitOperativeTimingEditBindingSource, "VisitID", true));
            this.VisitIDTextEdit.Location = new System.Drawing.Point(142, 510);
            this.VisitIDTextEdit.MenuManager = this.barManager1;
            this.VisitIDTextEdit.Name = "VisitIDTextEdit";
            this.VisitIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitIDTextEdit, true);
            this.VisitIDTextEdit.Size = new System.Drawing.Size(532, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitIDTextEdit, optionsSpelling14);
            this.VisitIDTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitIDTextEdit.TabIndex = 53;
            // 
            // SiteIDTextEdit
            // 
            this.SiteIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06306OMVisitOperativeTimingEditBindingSource, "SiteID", true));
            this.SiteIDTextEdit.Location = new System.Drawing.Point(142, 486);
            this.SiteIDTextEdit.MenuManager = this.barManager1;
            this.SiteIDTextEdit.Name = "SiteIDTextEdit";
            this.SiteIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteIDTextEdit, true);
            this.SiteIDTextEdit.Size = new System.Drawing.Size(532, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteIDTextEdit, optionsSpelling15);
            this.SiteIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteIDTextEdit.TabIndex = 52;
            // 
            // ClientIDTextEdit
            // 
            this.ClientIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06306OMVisitOperativeTimingEditBindingSource, "ClientID", true));
            this.ClientIDTextEdit.Location = new System.Drawing.Point(117, 366);
            this.ClientIDTextEdit.MenuManager = this.barManager1;
            this.ClientIDTextEdit.Name = "ClientIDTextEdit";
            this.ClientIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientIDTextEdit, true);
            this.ClientIDTextEdit.Size = new System.Drawing.Size(557, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientIDTextEdit, optionsSpelling16);
            this.ClientIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDTextEdit.TabIndex = 51;
            // 
            // HoursUsedSpinEdit
            // 
            this.HoursUsedSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06306OMVisitOperativeTimingEditBindingSource, "HoursUsed", true));
            this.HoursUsedSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.HoursUsedSpinEdit.Location = new System.Drawing.Point(110, 224);
            this.HoursUsedSpinEdit.MenuManager = this.barManager1;
            this.HoursUsedSpinEdit.Name = "HoursUsedSpinEdit";
            this.HoursUsedSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HoursUsedSpinEdit.Properties.Mask.EditMask = "######0.00 Hours";
            this.HoursUsedSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.HoursUsedSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            131072});
            this.HoursUsedSpinEdit.Properties.ReadOnly = true;
            this.HoursUsedSpinEdit.Size = new System.Drawing.Size(122, 20);
            this.HoursUsedSpinEdit.StyleController = this.dataLayoutControl1;
            this.HoursUsedSpinEdit.TabIndex = 45;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp06306OMVisitOperativeTimingEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(98, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // LinkedToParentDescriptionButtonEdit
            // 
            this.LinkedToParentDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06306OMVisitOperativeTimingEditBindingSource, "LinkedToParentDescription", true));
            this.LinkedToParentDescriptionButtonEdit.EditValue = "";
            this.LinkedToParentDescriptionButtonEdit.Location = new System.Drawing.Point(98, 83);
            this.LinkedToParentDescriptionButtonEdit.MenuManager = this.barManager1;
            this.LinkedToParentDescriptionButtonEdit.Name = "LinkedToParentDescriptionButtonEdit";
            this.LinkedToParentDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to open the Select Job screen", "choose", null, true)});
            this.LinkedToParentDescriptionButtonEdit.Properties.ReadOnly = true;
            this.LinkedToParentDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.LinkedToParentDescriptionButtonEdit.Size = new System.Drawing.Size(576, 20);
            this.LinkedToParentDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToParentDescriptionButtonEdit.TabIndex = 6;
            this.LinkedToParentDescriptionButtonEdit.TabStop = false;
            this.LinkedToParentDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LinkedToParentDescriptionButtonEdit_ButtonClick);
            this.LinkedToParentDescriptionButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.LinkedToParentDescriptionButtonEdit_Validating);
            // 
            // ItemForVisitID
            // 
            this.ItemForVisitID.Control = this.VisitIDTextEdit;
            this.ItemForVisitID.CustomizationFormText = "Visit ID:";
            this.ItemForVisitID.Location = new System.Drawing.Point(0, 168);
            this.ItemForVisitID.Name = "ItemForVisitID";
            this.ItemForVisitID.Size = new System.Drawing.Size(666, 24);
            this.ItemForVisitID.Text = "Visit ID:";
            this.ItemForVisitID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteID
            // 
            this.ItemForSiteID.Control = this.SiteIDTextEdit;
            this.ItemForSiteID.CustomizationFormText = "Site ID:";
            this.ItemForSiteID.Location = new System.Drawing.Point(0, 144);
            this.ItemForSiteID.Name = "ItemForSiteID";
            this.ItemForSiteID.Size = new System.Drawing.Size(666, 24);
            this.ItemForSiteID.Text = "Site ID:";
            this.ItemForSiteID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteContractID
            // 
            this.ItemForSiteContractID.Control = this.SiteContractIDTextEdit;
            this.ItemForSiteContractID.CustomizationFormText = "Site Contract ID:";
            this.ItemForSiteContractID.Location = new System.Drawing.Point(0, 120);
            this.ItemForSiteContractID.Name = "ItemForSiteContractID";
            this.ItemForSiteContractID.Size = new System.Drawing.Size(666, 24);
            this.ItemForSiteContractID.Text = "Site Contract ID:";
            this.ItemForSiteContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.Control = this.ClientNameTextEdit;
            this.ItemForClientName.CustomizationFormText = "Client Name:";
            this.ItemForClientName.Location = new System.Drawing.Point(0, 48);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(666, 24);
            this.ItemForClientName.Text = "Client Name:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientIDTextEdit;
            this.ItemForClientID.CustomizationFormText = "Client ID:";
            this.ItemForClientID.Location = new System.Drawing.Point(0, 24);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(666, 24);
            this.ItemForClientID.Text = "Client ID:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientContractID
            // 
            this.ItemForClientContractID.Control = this.ClientContractIDTextEdit;
            this.ItemForClientContractID.CustomizationFormText = "Client Contract ID:";
            this.ItemForClientContractID.Location = new System.Drawing.Point(0, 0);
            this.ItemForClientContractID.Name = "ItemForClientContractID";
            this.ItemForClientContractID.Size = new System.Drawing.Size(666, 24);
            this.ItemForClientContractID.Text = "Client Contract ID:";
            this.ItemForClientContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForVisitNumber
            // 
            this.ItemForVisitNumber.Control = this.VisitNumberTextEdit;
            this.ItemForVisitNumber.CustomizationFormText = "Visit Number:";
            this.ItemForVisitNumber.Location = new System.Drawing.Point(0, 71);
            this.ItemForVisitNumber.Name = "ItemForVisitNumber";
            this.ItemForVisitNumber.Size = new System.Drawing.Size(666, 24);
            this.ItemForVisitNumber.Text = "Visit Number:";
            this.ItemForVisitNumber.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForVisitOperativeTimingID
            // 
            this.ItemForVisitOperativeTimingID.Control = this.VisitOperativeTimingIDTextEdit;
            this.ItemForVisitOperativeTimingID.Location = new System.Drawing.Point(0, 214);
            this.ItemForVisitOperativeTimingID.Name = "ItemForVisitOperativeTimingID";
            this.ItemForVisitOperativeTimingID.Size = new System.Drawing.Size(666, 24);
            this.ItemForVisitOperativeTimingID.Text = "Visit Operative Timing ID:";
            this.ItemForVisitOperativeTimingID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForOperativeID
            // 
            this.ItemForOperativeID.Control = this.OperativeIDTextEdit;
            this.ItemForOperativeID.Location = new System.Drawing.Point(0, 106);
            this.ItemForOperativeID.Name = "ItemForOperativeID";
            this.ItemForOperativeID.Size = new System.Drawing.Size(618, 24);
            this.ItemForOperativeID.Text = "Operative ID:";
            this.ItemForOperativeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLinkedToPersonTypeID
            // 
            this.ItemForLinkedToPersonTypeID.Control = this.LinkedToPersonTypeIDTextEdit;
            this.ItemForLinkedToPersonTypeID.Location = new System.Drawing.Point(0, 219);
            this.ItemForLinkedToPersonTypeID.Name = "ItemForLinkedToPersonTypeID";
            this.ItemForLinkedToPersonTypeID.Size = new System.Drawing.Size(642, 24);
            this.ItemForLinkedToPersonTypeID.Text = "Linked To Person Type ID:";
            this.ItemForLinkedToPersonTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(686, 588);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLinkedToParentDescription,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.layoutControlGroup6,
            this.ItemForClientNameContractDescription,
            this.ItemForSiteName,
            this.emptySpaceItem6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(666, 395);
            // 
            // ItemForLinkedToParentDescription
            // 
            this.ItemForLinkedToParentDescription.AllowHide = false;
            this.ItemForLinkedToParentDescription.Control = this.LinkedToParentDescriptionButtonEdit;
            this.ItemForLinkedToParentDescription.CustomizationFormText = "Linked To:";
            this.ItemForLinkedToParentDescription.Location = new System.Drawing.Point(0, 71);
            this.ItemForLinkedToParentDescription.Name = "ItemForLinkedToParentDescription";
            this.ItemForLinkedToParentDescription.Size = new System.Drawing.Size(666, 24);
            this.ItemForLinkedToParentDescription.Text = "Linked To:";
            this.ItemForLinkedToParentDescription.TextSize = new System.Drawing.Size(83, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(86, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(86, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(86, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(263, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(403, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(86, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTeamName,
            this.ItemForOperativeName,
            this.ItemForHoursUsed,
            this.emptySpaceItem4,
            this.ItemForRemarks,
            this.ItemForLinkedToPersonType});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 106);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(666, 289);
            this.layoutControlGroup6.Text = "Details";
            // 
            // ItemForTeamName
            // 
            this.ItemForTeamName.Control = this.TeamNameTextEdit;
            this.ItemForTeamName.Location = new System.Drawing.Point(0, 24);
            this.ItemForTeamName.Name = "ItemForTeamName";
            this.ItemForTeamName.Size = new System.Drawing.Size(642, 24);
            this.ItemForTeamName.Text = "Team Name:";
            this.ItemForTeamName.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForOperativeName
            // 
            this.ItemForOperativeName.Control = this.OperativeNameTextEdit;
            this.ItemForOperativeName.Location = new System.Drawing.Point(0, 48);
            this.ItemForOperativeName.Name = "ItemForOperativeName";
            this.ItemForOperativeName.Size = new System.Drawing.Size(642, 24);
            this.ItemForOperativeName.Text = "Operative Name:";
            this.ItemForOperativeName.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForHoursUsed
            // 
            this.ItemForHoursUsed.Control = this.HoursUsedSpinEdit;
            this.ItemForHoursUsed.CustomizationFormText = "Hours Used:";
            this.ItemForHoursUsed.Location = new System.Drawing.Point(0, 72);
            this.ItemForHoursUsed.MaxSize = new System.Drawing.Size(212, 24);
            this.ItemForHoursUsed.MinSize = new System.Drawing.Size(212, 24);
            this.ItemForHoursUsed.Name = "ItemForHoursUsed";
            this.ItemForHoursUsed.Size = new System.Drawing.Size(212, 24);
            this.ItemForHoursUsed.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForHoursUsed.Text = "Hours Used:";
            this.ItemForHoursUsed.TextSize = new System.Drawing.Size(83, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(212, 72);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(430, 24);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 96);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(642, 147);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForLinkedToPersonType
            // 
            this.ItemForLinkedToPersonType.Control = this.LinkedToPersonTypeTextEdit;
            this.ItemForLinkedToPersonType.Location = new System.Drawing.Point(0, 0);
            this.ItemForLinkedToPersonType.Name = "ItemForLinkedToPersonType";
            this.ItemForLinkedToPersonType.Size = new System.Drawing.Size(642, 24);
            this.ItemForLinkedToPersonType.Text = "Labour Type:";
            this.ItemForLinkedToPersonType.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForClientNameContractDescription
            // 
            this.ItemForClientNameContractDescription.Control = this.ClientNameContractDescriptionTextEdit;
            this.ItemForClientNameContractDescription.CustomizationFormText = "Client \\ Contract:";
            this.ItemForClientNameContractDescription.Location = new System.Drawing.Point(0, 23);
            this.ItemForClientNameContractDescription.Name = "ItemForClientNameContractDescription";
            this.ItemForClientNameContractDescription.Size = new System.Drawing.Size(666, 24);
            this.ItemForClientNameContractDescription.Text = "Client \\ Contract:";
            this.ItemForClientNameContractDescription.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForSiteName
            // 
            this.ItemForSiteName.Control = this.SiteNameTextEdit;
            this.ItemForSiteName.CustomizationFormText = "Site Name:";
            this.ItemForSiteName.Location = new System.Drawing.Point(0, 47);
            this.ItemForSiteName.Name = "ItemForSiteName";
            this.ItemForSiteName.Size = new System.Drawing.Size(666, 24);
            this.ItemForSiteName.Text = "Site Name:";
            this.ItemForSiteName.TextSize = new System.Drawing.Size(83, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 95);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 11);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 11);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(666, 11);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 395);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(666, 173);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(666, 173);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06306_OM_Visit_Operative_Timing_EditTableAdapter
            // 
            this.sp06306_OM_Visit_Operative_Timing_EditTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Visit_Operative_Timing_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(686, 644);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Visit_Operative_Timing_Edit";
            this.Text = "Edit Visit Operative Timing";
            this.Activated += new System.EventHandler(this.frm_OM_Visit_Operative_Timing_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Visit_Operative_Timing_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Visit_Operative_Timing_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06306OMVisitOperativeTimingEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OperativeNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OperativeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitOperativeTimingIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameContractDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HoursUsedSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToParentDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitOperativeTimingID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOperativeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToParentDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOperativeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHoursUsed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientNameContractDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToParentDescription;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.ButtonEdit LinkedToParentDescriptionButtonEdit;
        private DevExpress.XtraEditors.SpinEdit HoursUsedSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHoursUsed;
        private DevExpress.XtraEditors.TextEdit SiteIDTextEdit;
        private DevExpress.XtraEditors.TextEdit ClientIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteID;
        private DevExpress.XtraEditors.TextEdit VisitIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitID;
        private DevExpress.XtraEditors.TextEdit ClientNameContractDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientNameContractDescription;
        private DevExpress.XtraEditors.TextEdit SiteNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteName;
        private DevExpress.XtraEditors.TextEdit VisitNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitNumber;
        private DevExpress.XtraEditors.TextEdit ClientNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.TextEdit ClientContractIDTextEdit;
        private DevExpress.XtraEditors.TextEdit SiteContractIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientContractID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteContractID;
        private DevExpress.XtraBars.BarButtonItem bbiLinkedDocuments;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private DevExpress.XtraEditors.TextEdit VisitOperativeTimingIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitOperativeTimingID;
        private System.Windows.Forms.BindingSource sp06306OMVisitOperativeTimingEditBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06306_OM_Visit_Operative_Timing_EditTableAdapter sp06306_OM_Visit_Operative_Timing_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit OperativeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOperativeID;
        private DevExpress.XtraEditors.TextEdit TeamNameTextEdit;
        private DevExpress.XtraEditors.TextEdit OperativeNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOperativeName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTeamName;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit LinkedToPersonTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToPersonTypeID;
        private DevExpress.XtraEditors.TextEdit LinkedToPersonTypeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToPersonType;
    }
}

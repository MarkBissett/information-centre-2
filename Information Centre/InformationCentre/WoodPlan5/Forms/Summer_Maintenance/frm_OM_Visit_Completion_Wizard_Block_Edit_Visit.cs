﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;
using DevExpress.XtraEditors;
using System.IO;

namespace WoodPlan5
{
    public partial class frm_OM_Visit_Completion_Wizard_Block_Edit_Visit : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        
        public string strSignaturePath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        public DateTime? dtVisitStartDate = null;
        public DateTime? dtVisitEndDate = null;
        public int? intVisitStatusID = null;
        public double? dStartLatitude = null;
        public double? dStartLongitude = null;
        public double? dFinishLatitude = null;
        public double? dFinishLongitude = null;
        public string strClientSignaturePath = null;
        public int? intNoOnetoSign = null;
        public string strManagerName = null;
        public string strManagerNotes = null;
        public int? intIssueFound = null;
        public int? intIssueTypeID = null;
        public int? intRework = null;
        public int? intExtraWorkRequired = null;
        public int? intExtraWorkTypeID = null;
        public string strExtraWorkComments = null;
        public string strCompletionSheetNumber = null;
        public string strRemarks = null;

        #endregion

        public frm_OM_Visit_Completion_Wizard_Block_Edit_Visit()
        {
            InitializeComponent();
        }

        private void frm_OM_Visit_Completion_Wizard_Block_Edit_Visit_Load(object sender, EventArgs e)
        {
            this.FormID = 409;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //
            
            strConnectionString = this.GlobalSettings.ConnectionString;
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strSignaturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_PictureFilesFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Client Signatures (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Client Signature Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            try
            {
                sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_Blank);

                sp06251_OM_Issue_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06251_OM_Issue_Types_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06251_OM_Issue_Types_With_Blank, 0);

                sp06267_OM_Extra_Work_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06267_OM_Extra_Work_Types_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06267_OM_Extra_Work_Types_With_Blank, 0);
            }
            catch (Exception) { }

            dateEditStartDate.EditValue = null;
            dateEditEndDate.EditValue = null;
            gridLookUpEditVisitStatusID.EditValue = null;
            StartLatitudeTextEdit.EditValue = null;
            StartLongitudeTextEdit.EditValue = null;
            FinishLatitudeTextEdit.EditValue = null;
            FinishLongitudeTextEdit.EditValue = null;
            ClientSignaturePathButtonEdit.EditValue = null;
            NoOnetoSignCheckEdit.EditValue = null;
            ManagerNameTextEdit.EditValue = null;
            ManagerNotesMemoEdit.EditValue = null;
            IssueFoundCheckEdit.EditValue = null;
            IssueTypeIDGridLookUpEdit.EditValue = null;
            checkEditRework.EditValue = null;
            ExtraWorkRequiredCheckEdit.EditValue = null;
            gridLookUpEditExtraWorkTypeID.EditValue = null;
            ExtraWorkCommentsMemoEdit.EditValue = null;
            CompletionSheetNumberTextEdit.EditValue = null;
            RemarksMemoEdit.EditValue = null;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }
       
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (dateEditStartDate.EditValue != null) dtVisitStartDate = Convert.ToDateTime(dateEditStartDate.EditValue);
            if (dateEditEndDate.EditValue != null) dtVisitEndDate = Convert.ToDateTime(dateEditEndDate.EditValue);
            if (gridLookUpEditVisitStatusID.EditValue != null) intVisitStatusID = Convert.ToInt32(gridLookUpEditVisitStatusID.EditValue);
            if (StartLatitudeTextEdit.EditValue != null) dStartLatitude = Convert.ToDouble(StartLatitudeTextEdit.EditValue);
            if (StartLongitudeTextEdit.EditValue != null) dStartLongitude = Convert.ToDouble(StartLongitudeTextEdit.EditValue);
            if (FinishLatitudeTextEdit.EditValue != null) dFinishLatitude = Convert.ToDouble(FinishLatitudeTextEdit.EditValue);
            if (FinishLongitudeTextEdit.EditValue != null) dFinishLongitude = Convert.ToDouble(FinishLongitudeTextEdit.EditValue);
            if (ClientSignaturePathButtonEdit.EditValue != null) strClientSignaturePath = ClientSignaturePathButtonEdit.EditValue.ToString();
            if (NoOnetoSignCheckEdit.EditValue != null) intNoOnetoSign = Convert.ToInt32(NoOnetoSignCheckEdit.EditValue);
            if (ManagerNameTextEdit.EditValue != null) strManagerName = ManagerNameTextEdit.EditValue.ToString();
            if (ManagerNotesMemoEdit.EditValue != null) strManagerNotes = ManagerNotesMemoEdit.EditValue.ToString();
            if (IssueFoundCheckEdit.EditValue != null) intIssueFound = Convert.ToInt32(IssueFoundCheckEdit.EditValue);
            if (IssueTypeIDGridLookUpEdit.EditValue != null) intIssueTypeID = Convert.ToInt32(IssueTypeIDGridLookUpEdit.EditValue);
            if (checkEditRework.EditValue != null) intRework = Convert.ToInt32(checkEditRework.EditValue);
            if (ExtraWorkRequiredCheckEdit.EditValue != null) intExtraWorkRequired = Convert.ToInt32(ExtraWorkRequiredCheckEdit.EditValue);
            if (gridLookUpEditExtraWorkTypeID.EditValue != null) intExtraWorkTypeID = Convert.ToInt32(gridLookUpEditExtraWorkTypeID.EditValue);

            if (ExtraWorkCommentsMemoEdit.EditValue != null) strExtraWorkComments = ExtraWorkCommentsMemoEdit.EditValue.ToString();
            if (CompletionSheetNumberTextEdit.EditValue != null) strCompletionSheetNumber = CompletionSheetNumberTextEdit.EditValue.ToString();
            if (RemarksMemoEdit.EditValue != null) strRemarks = RemarksMemoEdit.EditValue.ToString();
          
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void ClientSignaturePathButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            string strFirstPartOfPathWithBackslash = strSignaturePath + "\\";
            switch (e.Button.Tag.ToString())
            {
                case "select file":
                    {
                        if (strFormMode == "view") return;
                        using (OpenFileDialog dlg = new OpenFileDialog())
                        {
                            dlg.DefaultExt = "tab";
                            dlg.Filter = "";
                            if (strSignaturePath != "") dlg.InitialDirectory = strFirstPartOfPathWithBackslash;
                            dlg.Multiselect = false;
                            dlg.ShowDialog();
                            if (dlg.FileNames.Length > 0)
                            {
                                string strTempFileName = "";
                                string strTempResult = "";
                                //string strExtension = "";
                                foreach (string filename in dlg.FileNames)
                                {
                                    if (strFirstPartOfPathWithBackslash != "")
                                    {
                                        if (!filename.ToLower().StartsWith(strFirstPartOfPathWithBackslash.ToLower()))
                                        {
                                            DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files from under the default folder [as specified in the System Configuration Screen - Default Client Signatures Folder] or one of it's sub-folders.\n\nFile Selection Aborted!", "Select File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            break;
                                        }
                                    }
                                    strTempFileName = filename.Substring(strFirstPartOfPathWithBackslash.Length);
                                    if (strTempResult == "")
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += strTempFileName;
                                    }
                                    else
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += "\r\n" + strTempFileName;
                                    }
                                }
                                ClientSignaturePathButtonEdit.Text = "\\" + Path.GetFileName(strTempFileName); ;
                                NoOnetoSignCheckEdit.Checked = false;
                            }
                        }
                    }
                    break;
            }
        }

        private void NoOnetoSignCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                // Clear Signature and Manager Name //
                ClientSignaturePathButtonEdit.EditValue = "";
                ManagerNameTextEdit.EditValue = "";
            }
        }





    }
}

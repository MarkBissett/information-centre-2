﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Xml;
using System.Data.SqlClient;
using System.Net.Mail;

namespace WoodPlan5
{
    public partial class frm_OM_Points_Team_Redemption_Details_Edit: BaseObjects.frmBase
    {
        public frm_OM_Points_Team_Redemption_Details_Edit()
        {
            InitializeComponent();            
        }

        private void frm_OM_Points_Team_Redemption_Details_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 703002;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            // connect adapters //
            sp06808_OM_Team_RedemptionItemTypeTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06800_OM_Team_RedemptionListItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06804_OM_Team_PointsRedemptionDetailItemTableAdapter.Connection.ConnectionString = strConnectionString;
            
            editFormDataLayoutControlGroup.BeginUpdate();
            switch (strFormMode.ToLower())
            {
                case "add":                   
                    addNewRow(FormMode.add);
                    break;
                case "blockadd":
                    addNewRow(FormMode.blockadd);
                    break;
                case "blockedit":
                    addNewRow(FormMode.blockedit);
                    this.dataSet_Teams.sp06804_OM_Team_PointsRedemptionDetailItem.Rows[0].AcceptChanges(); 
                    break;
                case "edit":
                case "view":
                    try
                    {
                        loadData();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    break;
            }
            editFormDataLayoutControlGroup.EndUpdate();
            try
            {
                PopulatePickList();
                DataRowView currentRateRow = (DataRowView)sp06810OMTeamRedemptionVATRateBindingSource.Current;
                if (currentRateRow != null)
                {
                    dblVatRate = Convert.ToDouble(dataSet_Teams.sp06810_OM_Team_RedemptionVATRate[0].VatRate);
                }
                DataRowView currentRow = (DataRowView)sp06800OMTeamRedemptionListItemBindingSource.Current;
                if (currentRow != null)
                {
                    intPointsRatio = dataSet_Teams.sp06800_OM_Team_RedemptionListItem[0].PointsRatio;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
            if (strFormMode == "view")  // Disable all controls //
            {
               editFormDataLayoutControlGroup.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            PostOpen(null);
        }

        #region Instance Variables
        
        bool forceFormClose = false;
        int intPointsRatio = 1;
        double dblVatRate = 0.20;
        public FormMode formMode;
        public string strTeamRedemptionRecordIDs = "";
        private bool ibool_FormEditingCancelled = false;
        public string strGCReference;
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        bool bool_FormLoading = true;
        public bool editorDataChanged = false;
        public enum FormMode { add, edit, view, delete, blockadd, blockedit };
        public enum SentenceCase { Upper , Lower, Title, AsIs }

        #endregion

        #region Validate Method

        private bool validateLookupEdit(LookUpEdit lookupEdit)
        {
            bool valid = true;
            if (bool_FormLoading)
                return valid;

            string ErrorText = "Please Select Value.";

            if (lookupEdit.Properties.GetDisplayText(lookupEdit.EditValue) == "" ? false : true)
            {
                ErrorText = "Please Select " + lookupEdit.Tag + ".";
            }
            if (lookupEdit.Properties.GetDisplayText(lookupEdit.EditValue) == "" || Convert.ToInt32(lookupEdit.EditValue) < 1)
            {
                dxErrorProvider.SetError(lookupEdit, ErrorText);
                valid = false;  // Show stop icon as field is invalid //
                return valid;
            }
            else
            {
                dxErrorProvider.SetError(lookupEdit, "");
                return valid;
            }
        }
        
        private bool validateSpinEdit(SpinEdit spinEdit)
        {
            bool valid = true;
            if (bool_FormLoading)
                return valid;
            string ErrorText = "Please Enter a Value.";
            if (spinEdit.EditValue == DBNull.Value)
            {
                if (spinEdit.Tag != null)
                {
                    ErrorText = "Please Enter a valid " + spinEdit.Tag.ToString() + ".";
                    valid = false;
                    dxErrorProvider.SetError(spinEdit, ErrorText);
                }
            }
            if (valid)
            {
                dxErrorProvider.SetError(spinEdit, "");
            }
            return valid;
        }
        private bool validateTextBox(TextEdit txtBox, SentenceCase SentenceCase)
        {
            if (bool_FormLoading)
                return true;

            bool valid = true;
            string ErrorText = "Please Enter a Value.";

            if (txtBox.EditValue.ToString() == "")
            {
                valid = false;
            }
            
            if (txtBox.Text != "")
            {
                switch (SentenceCase)
                {
                    case SentenceCase.Upper:
                        txtBox.EditValue = txtBox.Text.ToUpper();
                        break;
                    case SentenceCase.Lower:
                        txtBox.EditValue = txtBox.Text.ToLower();
                        break;
                    case SentenceCase.Title:
                        txtBox.EditValue = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtBox.Text);
                        break;
                }
            }

            dxErrorProvider.SetError(txtBox, ErrorText);
            if (valid)
            {
                dxErrorProvider.SetError(txtBox, "");
            }
            return valid;
        }
        private bool validateDateEdit(DateEdit dateEdit)
        {
            bool valid = true;
            if (bool_FormLoading)
                return valid;
            string ErrorText = "Please Enter a Valid Date.";
            if (dateEdit.EditValue == DBNull.Value)
            {
                if (dateEdit.Tag != null)
                {
                    ErrorText = "Please Enter a valid " + dateEdit.Tag.ToString() + ".";
                    valid = false;
                    dxErrorProvider.SetError(dateEdit, ErrorText);
                }
            }
            if (valid)
            {
                dxErrorProvider.SetError(dateEdit, "");
            }
            return valid;
        }
        #endregion

        #region Editor Events
        private void SpinEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateSpinEdit((SpinEdit)sender))
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }
        private void textBox_Validating(object sender, CancelEventArgs e)
        {
            if (validateTextBox((TextEdit)sender, SentenceCase.Title))
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }
        private void lookupedit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }
        private void PointsRedemptionListIDLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (PointsRedemptionListIDLookUpEdit.Properties.GetDisplayText(PointsRedemptionListIDLookUpEdit.EditValue) != "")
            {
                calculatedFields();
            }
        }

        private void OrderQuantitySpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (OrderQuantitySpinEdit.Properties.GetDisplayText(OrderQuantitySpinEdit.EditValue) != "")
            {
                calculatedFields();
            }
        }
        private void calculatedFields()
        {
            ItemTypeIDLookUpEdit.EditValue = PointsRedemptionListIDLookUpEdit.Properties.GetDataSourceValue("ItemTypeID", PointsRedemptionListIDLookUpEdit.ItemIndex);
            var NumberOfPoints = PointsRedemptionListIDLookUpEdit.Properties.GetDataSourceValue("NumberOfPoints", PointsRedemptionListIDLookUpEdit.ItemIndex);
            var dcVatRate = Convert.ToDecimal(dblVatRate);
            var intQuantity = Convert.ToInt32(OrderQuantitySpinEdit.EditValue.ToString());
            UnitNumberOfPointsSpinEdit.EditValue = NumberOfPoints;
            UnitPriceSpinEdit.EditValue = Convert.ToDecimal(NumberOfPoints) / intPointsRatio;
            UnitVATSpinEdit.EditValue = (Convert.ToDecimal(NumberOfPoints) / intPointsRatio) * dcVatRate;
            LineTotalPointsSpinEdit.EditValue = Convert.ToInt32(NumberOfPoints) * intQuantity;
            LineTotalPriceSpinEdit.EditValue = (Convert.ToDecimal(NumberOfPoints) / intPointsRatio) * intQuantity;
            LineVATSpinEdit.EditValue = (Convert.ToDecimal(NumberOfPoints) / intPointsRatio) * intQuantity * dcVatRate;
        }
        #endregion

        #region Unique Form Functions

        public bool IsValid(string emailaddress)
        {
            if (emailaddress == "")
                return false;
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        private void IdentifyChangedBindingSource(DataTable dt)
        {

            switch (dt.TableName)
            {
                case "sp06804_OM_Team_PointsRedemptionDetailItem":
                    editorDataChanged = true;
                    break;
            }
        }

        private void EndEdit()
        {
            sp06804OMTeamPointsRedemptionDetailItemBindingSource.EndEdit();
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {

            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //         
            EndEdit();
            // this.BindingContext[equipmentDataLayoutControl.DataSource, equipmentDataLayoutControl.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            this.Validate();
            this.ValidateChildren();
            bool shouldReturn;
            string result = SaveChangesExtracted(out shouldReturn);
            if (shouldReturn)
                return result;
                     

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            EndEdit();

            CheckChangedTable(this.dataSet_Teams.sp06804_OM_Team_PointsRedemptionDetailItem);

            try
            {
                // Insert and Update queries defined in Table Adapter //
                if (editorDataChanged)
                {
                    this.sp06804_OM_Team_PointsRedemptionDetailItemTableAdapter.Update(dataSet_Teams);
                    editorDataChanged = false;
                }
            }
            catch (Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            finally
            {
                if (!editorDataChanged)
                    forceFormClose = true;
            }
            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //      
            switch (strCaller)
            {
                case "frm_OM_Points_Team_Redemption_Manager":
                    if (formMode == FormMode.add)
                    {                        
                        string strNewID = "";
                        DataRowView currentRow = (DataRowView)sp06804OMTeamPointsRedemptionDetailItemBindingSource.Current;
                        if (currentRow != null)
                        {
                            strRecordIDs = this.dataSet_Teams.sp06804_OM_Team_PointsRedemptionDetailItem[0].PointsRedemptionDetailID + ",";
                            strTeamRedemptionRecordIDs = this.dataSet_Teams.sp06804_OM_Team_PointsRedemptionDetailItem[0].PointsRedemptionHeaderID + ",";
                        }

                        strNewID = strRecordIDs;
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_OM_Points_Team_Redemption_Manager).UpdateFormRefreshStatus(3, strTeamRedemptionRecordIDs,"", strNewID);
                            //(Application.OpenForms[strCaller] as frm_OM_Points_Team_Redemption_Manager).frmActivated();
                        }
                    }
                    else if (formMode == FormMode.edit || formMode == FormMode.blockedit)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_OM_Points_Team_Redemption_Manager).UpdateFormRefreshStatus(3, strTeamRedemptionRecordIDs, "",strRecordIDs);
                            //(Application.OpenForms[strCaller] as frm_OM_Points_Team_Redemption_Manager).frmActivated();
                        }
                    }
                    else
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_OM_Points_Team_Redemption_Manager).UpdateFormRefreshStatus(3, "", "", strRecordIDs);
                            //(Application.OpenForms[strCaller] as frm_OM_Points_Team_Redemption_Manager).frmActivated();
                        }
                    }
                    break;
                default:
                    break;
       
            }
            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
            
        }

        private void PopulatePickList()
        {
            sp06808_OM_Team_RedemptionItemTypeTableAdapter.Fill(dataSet_Teams.sp06808_OM_Team_RedemptionItemType);
            sp06800_OM_Team_RedemptionListItemTableAdapter.Fill(dataSet_Teams.sp06800_OM_Team_RedemptionListItem, "", "");          
            sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter.Fill(dataSet_Teams.sp06802_OM_Team_PointsRedemptionHeaderItem, "", "");
            sp06810_OM_Team_RedemptionVATRateTableAdapter.Fill(dataSet_Teams.sp06810_OM_Team_RedemptionVATRate);
        }
        
        private void addNewRow(FormMode mode)
        {
            try
            {
                DataRow drNewRow = dataSet_Teams.sp06804_OM_Team_PointsRedemptionDetailItem.NewRow();
                drNewRow["Mode"] = (mode.ToString()).ToLower();
                drNewRow["PurchaseOrderNumber"] = "None";
                drNewRow["OrderQuantity"] = 1;
                drNewRow["PointsRedemptionHeaderID"] = (splitStrRecords(strTeamRedemptionRecordIDs))[0];

                this.dataSet_Teams.sp06804_OM_Team_PointsRedemptionDetailItem.Rows.Add(drNewRow);               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void loadData()
        {
            editFormLayoutControlGroup.BeginUpdate();
            sp06804_OM_Team_PointsRedemptionDetailItemTableAdapter.Fill(this.dataSet_Teams.sp06804_OM_Team_PointsRedemptionDetailItem, strRecordIDs, strFormMode);
            editFormLayoutControlGroup.EndUpdate();
        }

        #endregion

        #region Standard Form Functions

        private string[] splitStrRecords(string records)
        {
            char[] delimiters = new char[] { ',', ';' };
            string[] parts;
            if (records == "")
            {
                parts = strRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                parts = records.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            }
            return parts;
        }

        private void ClearErrors(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is LookUpEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is TextEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is DateEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is ColorPickEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is SpinEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is ContainerControl)
                    this.ClearErrors(item.Controls);
                if (item2 is DataLayoutControl)
                    this.ClearErrors(item.Controls);

            }
        }

        private string[] splitStrRecords()
        {
            char[] delimiters = new char[] { ',' };
            string[] parts = strRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            return parts;
        }

        private string SaveChangesExtracted(out bool shouldReturn)
        {
            shouldReturn = false;

            if (dxErrorProvider.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider, editFormDataLayoutControlGroup);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors +
                "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                shouldReturn = true;
                return "Error";
            }

            return String.Empty;
        }

        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
                if (item2 is DataLayoutControl) this.Attach_EditValueChanged_To_Children(item.Controls);

            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
        }

        private Boolean SetChangesPendingLabel()
        {
            EndEdit();

            DataSet dsChanges = this.dataSet_Teams.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;

                if (strFormMode.ToLower() == "add")
                {
                    bbiFormSave.Enabled = true;
                }
                return false;
            }


        }

        private string CheckForPendingSave()
        {
            EndEdit();

            string strMessage = "";
            string strMessage1 = CheckTablePendingSave(this.dataSet_Teams.sp06804_OM_Team_PointsRedemptionDetailItem, " on the "+ this.Text + " Form ");


            if (strMessage1 != "")
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (strMessage1 != "") 
                    strMessage += strMessage1;
            }
            return strMessage;
        }

        private string CheckTablePendingSave(DataTable dt, string formArea)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)" + formArea + "\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)" + formArea + "\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)" + formArea + "\n";
            }
            return strMessage;
        }

        private void CheckChangedTable(DataTable dt)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                IdentifyChangedBindingSource(dt);
            }
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            
            if (this.dataSet_Teams.sp06804_OM_Team_PointsRedemptionDetailItem.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                if (splashScreenManager.IsSplashFormVisible)
                {
                    splashScreenManager.CloseWaitForm();
                }
                XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Waste Types Accepted", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible)
            {
                splashScreenManager.CloseWaitForm();
            }
        }
                
        #endregion

        #region Compulsory Implementation 
        
        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode.ToLower())
            {
                case "add":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Adding";
                        bsiFormMode.ImageIndex = 0;  // Add //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.Appearance.ForeColor = color;
                        editFormDataNavigator.Visible = false;
                        
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Block Adding";
                        bsiFormMode.ImageIndex = 0;  // Add //
                        bsiFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        editFormDataNavigator.Visible = false;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Editing";
                        //barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        ////barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                       // txtEquipment_GC_Reference.Focus();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Block Editing";
                        //barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        bsiFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        //barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        editFormDataNavigator.Visible = false;
                        //lueSupplier.Focus();
                        //txtEquipment_GC_Reference.Properties.ReadOnly = true;
                        //lueEquipment_Category.Properties.ReadOnly = true;
                        //txtManufacturer_ID.Properties.ReadOnly = true;
                        //lueMake.Properties.ReadOnly = true;
                        //lueModel.Properties.ReadOnly = true;
                        //deDepreciation_Start_Date.Properties.ReadOnly = true;
                        //txtEx_Asset_ID.Properties.ReadOnly = true;
                        //txtSage_Asset_Ref.Properties.ReadOnly = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Viewing";
                        bsiFormMode.ImageIndex = 4;  // View //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        //barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
               // equipmentDataLayoutControl.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
        
            bbiSave.Enabled = false;
            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        #endregion

        #region Form Events

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) 
                this.Close();
        }

        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private void frm_OM_Points_Team_Redemption_Details_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            if (forceFormClose) return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

        private void frm_OM_Points_Team_Redemption_Details_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }

          //  IdentifyChangedBindingSource();
        }

        #endregion

        #region Data Navigator
                      
        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //Populate picklist Data
            PopulatePickList();
        }

    }
}

﻿namespace WoodPlan5
{
    partial class frm_OM_Site_Contract_Wizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Site_Contract_Wizard));
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.Animation.Transition transition1 = new DevExpress.Utils.Animation.Transition();
            this.colActive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn123 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn93 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn147 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageWelcome = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.btnWelcomeNext = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.xtraTabPageStep1 = new DevExpress.XtraTab.XtraTabPage();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.btnDateRangeOK = new DevExpress.XtraEditors.SimpleButton();
            this.standaloneBarDockControl3 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.btnStep1Previous = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep1Next = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp06086OMSiteContractWizardClientContractsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDirectorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colYearlyPercentageIncrease = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCheckRPIDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastClientPaymentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPreviousContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDirector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientYearCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageStep2 = new DevExpress.XtraTab.XtraTabPage();
            this.checkEditCreateGenericJobRates = new DevExpress.XtraEditors.CheckEdit();
            this.labelControlSelectedSiteCount = new DevExpress.XtraEditors.LabelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp06089OMSitesForClientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActiveSiteContractCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.btnStep2Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep2Previous = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPageStep3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp06090OMSiteContractEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colEndDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colContractValue1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colLastClientPaymentDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colYearlyPercentageIncrease1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colDummySiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteInstructions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientBillingTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditClientBillingTypeID = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06355OMClientBillingTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit4View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultScheduleTemplateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditDefaultScheduleTemplateID = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06141OMVisitTemplateHeadersWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.repositoryItemGridLookUpEdit3View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn124 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn125 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysSeparationPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnStep3Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep3Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.btnSplitValueOverSites = new DevExpress.XtraEditors.SimpleButton();
            this.CurrentContractValueTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.pictureEdit10 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageStep4 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.CopyClientContractYearsSimpleButton = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddYearsBtn = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp06092OMSiteContractYearEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientContractYearID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit50CharsMax = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colClientValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSiteName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearlyPercentageIncrease2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditPercentage2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.spinEditContractYears = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForYearsGrid = new DevExpress.XtraLayout.LayoutControlItem();
            this.ITemForAddYearsToGridButton = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCopyClientContractYears = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.btnStep4Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep4Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit7 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageStep5 = new DevExpress.XtraTab.XtraTabPage();
            this.btnStep5Next = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedFromClientContractYearID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedFromClientContractYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp06095OMSiteContractYearBillingProfileEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteContractProfileID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractYearID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceDateDue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colInvoiceDateActual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedBillAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colActualBillAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBilledByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedToParent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedFromClientContractProfileID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedFromClientContractProfile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.btnStep5Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit8 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageStep6 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl9 = new DevExpress.XtraGrid.GridControl();
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationX1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationY1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyEquipmentCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDummyLabourCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyMaterialCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyPersonResponsibilityCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.standaloneBarDockControl4 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl10 = new DevExpress.XtraGrid.GridControl();
            this.sp06098OMSiteContractLabourCostEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteContractLabourCostID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditUnitDescriptor = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06101OMWorkUnitTypesPicklistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostPerUnitExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditMoney = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colCostPerUnitVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditPercent = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colSellPerUnitExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellPerUnitVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCISPercentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCISValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcodeSiteDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditMiles = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colLatLongSiteDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditChooseContractor = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colLinkedToPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditLabourType = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colLinkedToPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnStep6Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep6Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit13 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageStep7 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl11 = new DevExpress.XtraGrid.GridControl();
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyEquipmentCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDummyLabourCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyMaterialCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyPersonResponsibilityCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl12 = new DevExpress.XtraGrid.GridControl();
            this.sp06103OMSiteContractEquipmentCostEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView12 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostUnitDescriptorID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditCostUnitDescriptors = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditCurrency3 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn64 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditPercentage3 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn65 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn66 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn71 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn72 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn73 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn74 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn75 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn76 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditChooseEquipment = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colOwnerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.standaloneBarDockControl5 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.btnStep7Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep7Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit11 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageStep8 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl5 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl13 = new DevExpress.XtraGrid.GridControl();
            this.gridView13 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn67 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn68 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn69 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn70 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn77 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn78 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn79 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn80 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn81 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn82 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn83 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn84 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn85 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn86 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn87 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn88 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyEquipmentCount2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDummyLabourCount2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyMaterialCount2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyPersonResponsibilityCount2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl14 = new DevExpress.XtraGrid.GridControl();
            this.sp06106OMSiteContractMaterialCostEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView14 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn89 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn90 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn91 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostUnitDescriptor3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06108OMUnitDescriptorsPicklistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView16 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn94 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn95 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn96 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn97 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn98 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn99 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn100 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn101 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn102 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn103 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn104 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn105 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn106 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditChooseMaterial = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.standaloneBarDockControl6 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.btnStep8Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep8Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit12 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageStep9 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl6 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl15 = new DevExpress.XtraGrid.GridControl();
            this.gridView15 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn92 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn107 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn108 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn109 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn110 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn111 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn112 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn113 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn114 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn115 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn116 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn117 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn118 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn119 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn120 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn121 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn122 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyEquipmentCount3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDummyLabourCount3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyMaterialCount3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyPersonResponsibilityCount3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.standaloneBarDockControl2 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPersonResponsibilityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibleForRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibleForRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibilityTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibilityType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditPersonResponsibilityType = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditChoosePerson = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colPersonTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnStep9Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep9Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit9 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageStep10 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl7 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl17 = new DevExpress.XtraGrid.GridControl();
            this.gridView17 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn126 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn127 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn128 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn129 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn130 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn131 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn132 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn133 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn134 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit16 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn135 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn136 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn137 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn138 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn139 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn140 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn141 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn142 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn143 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit16 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn144 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn145 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn146 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyJobRateCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl18 = new DevExpress.XtraGrid.GridControl();
            this.sp06346OMSiteContractJobRateEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView18 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn148 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn159 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit17 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn160 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn161 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDate18 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn162 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn163 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractJobRateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditJobSubType = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colFromDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colToDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colClientSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditVisitCategoryID = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06250OMVisitCategoriesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView19 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn149 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn150 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.standaloneBarDockControl7 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit14 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.btnStep10Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep10Previous = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPageFinish = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.btnFinish = new DevExpress.XtraEditors.SimpleButton();
            this.btnFinishPrevious = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemButtonEditChooseStaff = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.sp06063OMContractTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiAddProfile = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCopyClientContractYearBillingProfile = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddProfileFromTemplate = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEditTemplates = new DevExpress.XtraBars.BarButtonItem();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiAddPersonResponsibility = new DevExpress.XtraBars.BarButtonItem();
            this.bbiBlockAddPersonResponsibility = new DevExpress.XtraBars.BarButtonItem();
            this.barStep1 = new DevExpress.XtraBars.Bar();
            this.barEditItemDateRange = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateRange = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barEditItemActive = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.bbiRefreshClientContracts = new DevExpress.XtraBars.BarButtonItem();
            this.sp06086_OM_Site_Contract_Wizard_Client_ContractsTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06086_OM_Site_Contract_Wizard_Client_ContractsTableAdapter();
            this.sp06089_OM_Sites_For_ClientTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06089_OM_Sites_For_ClientTableAdapter();
            this.sp06090_OM_Site_Contract_EditTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06090_OM_Site_Contract_EditTableAdapter();
            this.sp06092_OM_Site_Contract_Year_EditTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06092_OM_Site_Contract_Year_EditTableAdapter();
            this.sp06095_OM_Site_Contract_Year_Billing_Profile_EditTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06095_OM_Site_Contract_Year_Billing_Profile_EditTableAdapter();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.bbiAddLabourCost = new DevExpress.XtraBars.BarButtonItem();
            this.bbiBlockAddLabourCost = new DevExpress.XtraBars.BarButtonItem();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.bbiAddEquipmentRate = new DevExpress.XtraBars.BarButtonItem();
            this.bbiBlockAddEquipmentRate = new DevExpress.XtraBars.BarButtonItem();
            this.bar5 = new DevExpress.XtraBars.Bar();
            this.bbiAddMaterialRate = new DevExpress.XtraBars.BarButtonItem();
            this.bbiBlockAddMaterialRate = new DevExpress.XtraBars.BarButtonItem();
            this.sp06098_OM_Site_Contract_Labour_Cost_EditTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06098_OM_Site_Contract_Labour_Cost_EditTableAdapter();
            this.sp06101_OM_Work_Unit_Types_PicklistTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06101_OM_Work_Unit_Types_PicklistTableAdapter();
            this.sp06103_OM_Site_Contract_Equipment_Cost_EditTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06103_OM_Site_Contract_Equipment_Cost_EditTableAdapter();
            this.sp06106_OM_Site_Contract_Material_Cost_EditTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06106_OM_Site_Contract_Material_Cost_EditTableAdapter();
            this.sp06108_OM_Unit_Descriptors_PicklistTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06108_OM_Unit_Descriptors_PicklistTableAdapter();
            this.bsiRecordTicking = new DevExpress.XtraBars.BarSubItem();
            this.bbiTick = new DevExpress.XtraBars.BarButtonItem();
            this.bbiUntick = new DevExpress.XtraBars.BarButtonItem();
            this.bar6 = new DevExpress.XtraBars.Bar();
            this.bbiAddJobRate = new DevExpress.XtraBars.BarButtonItem();
            this.bbiBlockAddJobRate = new DevExpress.XtraBars.BarButtonItem();
            this.transitionManager1 = new DevExpress.Utils.Animation.TransitionManager(this.components);
            this.sp06346_OM_Site_Contract_Job_Rate_EditTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06346_OM_Site_Contract_Job_Rate_EditTableAdapter();
            this.sp06355_OM_Client_Billing_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06355_OM_Client_Billing_Types_With_BlankTableAdapter();
            this.sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter();
            this.timerWelcomePage = new System.Windows.Forms.Timer(this.components);
            this.sp06250_OM_Visit_Categories_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06250_OM_Visit_Categories_With_BlankTableAdapter();
            this.checkEditSkipEquipment = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditSkipMaterials = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageWelcome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.xtraTabPageStep1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06086OMSiteContractWizardClientContractsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).BeginInit();
            this.xtraTabPageStep2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditCreateGenericJobRates.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06089OMSitesForClientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            this.xtraTabPageStep3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06090OMSiteContractEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditClientBillingTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06355OMClientBillingTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit4View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditDefaultScheduleTemplateID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06141OMVisitTemplateHeadersWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit3View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentContractValueTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit10.Properties)).BeginInit();
            this.xtraTabPageStep4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06092OMSiteContractYearEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit50CharsMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercentage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditContractYears.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForYearsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITemForAddYearsToGridButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCopyClientContractYears)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).BeginInit();
            this.xtraTabPageStep5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06095OMSiteContractYearBillingProfileEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).BeginInit();
            this.xtraTabPageStep6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06098OMSiteContractLabourCostEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditUnitDescriptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06101OMWorkUnitTypesPicklistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditMiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditLabourType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit13.Properties)).BeginInit();
            this.xtraTabPageStep7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06103OMSiteContractEquipmentCostEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditCostUnitDescriptors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCurrency3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercentage3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseEquipment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit11.Properties)).BeginInit();
            this.xtraTabPageStep8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).BeginInit();
            this.splitContainerControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06106OMSiteContractMaterialCostEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06108OMUnitDescriptorsPicklistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseMaterial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit12.Properties)).BeginInit();
            this.xtraTabPageStep9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).BeginInit();
            this.splitContainerControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditPersonResponsibilityType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChoosePerson)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit9.Properties)).BeginInit();
            this.xtraTabPageStep10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl7)).BeginInit();
            this.splitContainerControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06346OMSiteContractJobRateEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditJobSubType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditVisitCategoryID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06250OMVisitCategoriesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit14.Properties)).BeginInit();
            this.xtraTabPageFinish.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseStaff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06063OMContractTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSkipEquipment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSkipMaterials.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiRecordTicking, true)});
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(982, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Size = new System.Drawing.Size(982, 0);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(982, 0);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2,
            this.barStep1,
            this.bar3,
            this.bar4,
            this.bar5,
            this.bar6});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl3);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl4);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl5);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl6);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl2);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl7);
            this.barManager1.Images = this.imageCollection1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiAddProfile,
            this.bbiAddProfileFromTemplate,
            this.bbiEditTemplates,
            this.bbiAddPersonResponsibility,
            this.barEditItemDateRange,
            this.bbiRefreshClientContracts,
            this.barEditItemActive,
            this.bbiCopyClientContractYearBillingProfile,
            this.bbiAddLabourCost,
            this.bbiAddEquipmentRate,
            this.bbiAddMaterialRate,
            this.bbiBlockAddLabourCost,
            this.bbiBlockAddEquipmentRate,
            this.bbiBlockAddMaterialRate,
            this.bbiBlockAddPersonResponsibility,
            this.bsiRecordTicking,
            this.bbiTick,
            this.bbiUntick,
            this.bbiAddJobRate,
            this.bbiBlockAddJobRate});
            this.barManager1.MaxItemId = 57;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditDateRange,
            this.repositoryItemCheckEdit4});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActive1
            // 
            this.colActive1.Caption = "Active";
            this.colActive1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive1.FieldName = "Active";
            this.colActive1.Name = "colActive1";
            this.colActive1.OptionsColumn.AllowEdit = false;
            this.colActive1.OptionsColumn.AllowFocus = false;
            this.colActive1.OptionsColumn.ReadOnly = true;
            this.colActive1.Visible = true;
            this.colActive1.VisibleIndex = 4;
            this.colActive1.Width = 51;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridColumn123
            // 
            this.gridColumn123.Caption = "ID";
            this.gridColumn123.FieldName = "ID";
            this.gridColumn123.Name = "gridColumn123";
            this.gridColumn123.OptionsColumn.AllowEdit = false;
            this.gridColumn123.OptionsColumn.AllowFocus = false;
            this.gridColumn123.OptionsColumn.ReadOnly = true;
            this.gridColumn123.Width = 53;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "ID";
            this.gridColumn38.FieldName = "ID";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Width = 53;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.Width = 53;
            // 
            // gridColumn93
            // 
            this.gridColumn93.Caption = "ID";
            this.gridColumn93.FieldName = "ID";
            this.gridColumn93.Name = "gridColumn93";
            this.gridColumn93.OptionsColumn.AllowEdit = false;
            this.gridColumn93.OptionsColumn.AllowFocus = false;
            this.gridColumn93.OptionsColumn.ReadOnly = true;
            this.gridColumn93.Width = 53;
            // 
            // gridColumn147
            // 
            this.gridColumn147.Caption = "ID";
            this.gridColumn147.FieldName = "ID";
            this.gridColumn147.Name = "gridColumn147";
            this.gridColumn147.OptionsColumn.AllowEdit = false;
            this.gridColumn147.OptionsColumn.AllowFocus = false;
            this.gridColumn147.OptionsColumn.ReadOnly = true;
            this.gridColumn147.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn147.Width = 53;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
            this.xtraTabControl1.Size = new System.Drawing.Size(986, 541);
            this.xtraTabControl1.TabIndex = 6;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageWelcome,
            this.xtraTabPageStep1,
            this.xtraTabPageStep2,
            this.xtraTabPageStep3,
            this.xtraTabPageStep4,
            this.xtraTabPageStep5,
            this.xtraTabPageStep6,
            this.xtraTabPageStep7,
            this.xtraTabPageStep8,
            this.xtraTabPageStep9,
            this.xtraTabPageStep10,
            this.xtraTabPageFinish});
            this.xtraTabControl1.Tag = "";
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            this.xtraTabControl1.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.xtraTabControl1_SelectedPageChanging);
            // 
            // xtraTabPageWelcome
            // 
            this.xtraTabPageWelcome.Controls.Add(this.panelControl1);
            this.xtraTabPageWelcome.Controls.Add(this.btnWelcomeNext);
            this.xtraTabPageWelcome.Controls.Add(this.labelControl2);
            this.xtraTabPageWelcome.Controls.Add(this.pictureEdit1);
            this.xtraTabPageWelcome.Name = "xtraTabPageWelcome";
            this.xtraTabPageWelcome.Size = new System.Drawing.Size(959, 536);
            this.xtraTabPageWelcome.Tag = "0";
            this.xtraTabPageWelcome.Text = "Welcome";
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.pictureEdit4);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Location = new System.Drawing.Point(211, 6);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(742, 48);
            this.panelControl1.TabIndex = 17;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit4.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit4.Location = new System.Drawing.Point(698, 4);
            this.pictureEdit4.MenuManager = this.barManager1;
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Properties.ReadOnly = true;
            this.pictureEdit4.Properties.ShowMenu = false;
            this.pictureEdit4.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit4.TabIndex = 16;
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(5, 1);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(423, 29);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "Welcome to the <b>Site Contract Wizard</b>";
            // 
            // labelControl9
            // 
            this.labelControl9.AllowHtmlString = true;
            this.labelControl9.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl9.Appearance.Options.UseBackColor = true;
            this.labelControl9.Location = new System.Drawing.Point(9, 31);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(320, 13);
            this.labelControl9.TabIndex = 15;
            this.labelControl9.Text = "This wizard will allow you to create one or more new Site Contracts\r\n";
            // 
            // btnWelcomeNext
            // 
            this.btnWelcomeNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWelcomeNext.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnWelcomeNext.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnWelcomeNext.Location = new System.Drawing.Point(865, 500);
            this.btnWelcomeNext.Name = "btnWelcomeNext";
            this.btnWelcomeNext.Size = new System.Drawing.Size(88, 30);
            this.btnWelcomeNext.TabIndex = 7;
            this.btnWelcomeNext.Text = "Next";
            this.btnWelcomeNext.Click += new System.EventHandler(this.btnWelcomeNext_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.AllowHtmlString = true;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(220, 104);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(220, 25);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Click <b>Next</b> To Continue";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = global::WoodPlan5.Properties.Resources.wizard_image;
            this.pictureEdit1.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(205, 536);
            this.pictureEdit1.TabIndex = 4;
            // 
            // xtraTabPageStep1
            // 
            this.xtraTabPageStep1.Controls.Add(this.popupContainerControlDateRange);
            this.xtraTabPageStep1.Controls.Add(this.standaloneBarDockControl3);
            this.xtraTabPageStep1.Controls.Add(this.panelControl3);
            this.xtraTabPageStep1.Controls.Add(this.btnStep1Previous);
            this.xtraTabPageStep1.Controls.Add(this.btnStep1Next);
            this.xtraTabPageStep1.Controls.Add(this.gridControl1);
            this.xtraTabPageStep1.Name = "xtraTabPageStep1";
            this.xtraTabPageStep1.Size = new System.Drawing.Size(959, 536);
            this.xtraTabPageStep1.Tag = "1";
            this.xtraTabPageStep1.Text = "Step 1";
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.groupControl2);
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeOK);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(34, 222);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(199, 107);
            this.popupContainerControlDateRange.TabIndex = 18;
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.labelControl17);
            this.groupControl2.Controls.Add(this.labelControl18);
            this.groupControl2.Controls.Add(this.dateEditFromDate);
            this.groupControl2.Controls.Add(this.dateEditToDate);
            this.groupControl2.Location = new System.Drawing.Point(3, 5);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(193, 76);
            this.groupControl2.TabIndex = 20;
            this.groupControl2.Text = "Date Range  [Contract Start Date]";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(6, 53);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(16, 13);
            this.labelControl17.TabIndex = 13;
            this.labelControl17.Text = "To:";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(6, 27);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(28, 13);
            this.labelControl18.TabIndex = 12;
            this.labelControl18.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(148, 20);
            this.dateEditFromDate.TabIndex = 10;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(148, 20);
            this.dateEditToDate.TabIndex = 11;
            // 
            // btnDateRangeOK
            // 
            this.btnDateRangeOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeOK.Location = new System.Drawing.Point(3, 84);
            this.btnDateRangeOK.Name = "btnDateRangeOK";
            this.btnDateRangeOK.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDateRangeOK.Size = new System.Drawing.Size(50, 20);
            this.btnDateRangeOK.TabIndex = 19;
            this.btnDateRangeOK.Text = "OK";
            this.btnDateRangeOK.Click += new System.EventHandler(this.btnDateRangeOK_Click);
            // 
            // standaloneBarDockControl3
            // 
            this.standaloneBarDockControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl3.CausesValidation = false;
            this.standaloneBarDockControl3.Location = new System.Drawing.Point(7, 62);
            this.standaloneBarDockControl3.Manager = this.barManager1;
            this.standaloneBarDockControl3.Name = "standaloneBarDockControl3";
            this.standaloneBarDockControl3.Size = new System.Drawing.Size(946, 26);
            this.standaloneBarDockControl3.Text = "standaloneBarDockControl3";
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.Controls.Add(this.labelControl3);
            this.panelControl3.Controls.Add(this.pictureEdit3);
            this.panelControl3.Controls.Add(this.labelControl5);
            this.panelControl3.Location = new System.Drawing.Point(7, 6);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(946, 48);
            this.panelControl3.TabIndex = 10;
            // 
            // labelControl3
            // 
            this.labelControl3.AllowHtmlString = true;
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseBackColor = true;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(5, 5);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(387, 16);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "<b>Step 1:</b> Select the Client Contract to create the Site Contract(s) for";
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit3.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit3.Location = new System.Drawing.Point(902, 4);
            this.pictureEdit3.MenuManager = this.barManager1;
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.ReadOnly = true;
            this.pictureEdit3.Properties.ShowMenu = false;
            this.pictureEdit3.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit3.TabIndex = 8;
            // 
            // labelControl5
            // 
            this.labelControl5.AllowHtmlString = true;
            this.labelControl5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl5.Appearance.Options.UseBackColor = true;
            this.labelControl5.Location = new System.Drawing.Point(57, 29);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(460, 13);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Select the client contract to add the site contract(s) to by <b>clicking on it</b" +
    ">. Once done click Next.";
            // 
            // btnStep1Previous
            // 
            this.btnStep1Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep1Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep1Previous.ImageOptions.ImageIndex = 0;
            this.btnStep1Previous.Location = new System.Drawing.Point(771, 500);
            this.btnStep1Previous.Name = "btnStep1Previous";
            this.btnStep1Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep1Previous.TabIndex = 9;
            this.btnStep1Previous.Text = "Previous";
            this.btnStep1Previous.Click += new System.EventHandler(this.btnStep1Previous_Click);
            // 
            // btnStep1Next
            // 
            this.btnStep1Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep1Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep1Next.ImageOptions.ImageIndex = 1;
            this.btnStep1Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep1Next.Location = new System.Drawing.Point(865, 500);
            this.btnStep1Next.Name = "btnStep1Next";
            this.btnStep1Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep1Next.TabIndex = 8;
            this.btnStep1Next.Text = "Next";
            this.btnStep1Next.Click += new System.EventHandler(this.btnStep1Next_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp06086OMSiteContractWizardClientContractsBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Reload Data", "reload")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(7, 86);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime3,
            this.repositoryItemTextEditCurrency3,
            this.repositoryItemTextEditPercentage3,
            this.repositoryItemTextEditInteger});
            this.gridControl1.Size = new System.Drawing.Size(946, 408);
            this.gridControl1.TabIndex = 7;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp06086OMSiteContractWizardClientContractsBindingSource
            // 
            this.sp06086OMSiteContractWizardClientContractsBindingSource.DataMember = "sp06086_OM_Site_Contract_Wizard_Client_Contracts";
            this.sp06086OMSiteContractWizardClientContractsBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 6);
            this.imageCollection1.Images.SetKeyName(6, "refresh_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.BlockEdit_16x16, "BlockEdit_16x16", typeof(global::WoodPlan5.Properties.Resources), 7);
            this.imageCollection1.Images.SetKeyName(7, "BlockEdit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.BlockAdd_16x16, "BlockAdd_16x16", typeof(global::WoodPlan5.Properties.Resources), 8);
            this.imageCollection1.Images.SetKeyName(8, "BlockAdd_16x16");
            this.imageCollection1.InsertGalleryImage("copy_16x16.png", "images/edit/copy_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/copy_16x16.png"), 9);
            this.imageCollection1.Images.SetKeyName(9, "copy_16x16.png");
            this.imageCollection1.InsertGalleryImage("addgroupheader_16x16.png", "images/reports/addgroupheader_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/reports/addgroupheader_16x16.png"), 10);
            this.imageCollection1.Images.SetKeyName(10, "addgroupheader_16x16.png");
            this.imageCollection1.InsertGalleryImage("groupheader_16x16.png", "images/reports/groupheader_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/reports/groupheader_16x16.png"), 11);
            this.imageCollection1.Images.SetKeyName(11, "groupheader_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientContractID1,
            this.colClientID,
            this.colGCCompanyID,
            this.colContractTypeID,
            this.colContractStatusID,
            this.colContractDirectorID,
            this.colStartDate1,
            this.colEndDate1,
            this.colActive1,
            this.colSectorTypeID,
            this.colContractValue,
            this.colYearlyPercentageIncrease,
            this.colCheckRPIDate,
            this.colLastClientPaymentDate,
            this.colLinkedToPreviousContractID,
            this.colFinanceClientCode,
            this.colBillingCentreCodeID,
            this.colRemarks,
            this.colClientName,
            this.colGCCompanyName,
            this.colContractType,
            this.colContractStatus,
            this.colContractDirector,
            this.colSectorType,
            this.colClientYearCount,
            this.colContractDescription,
            this.colReactive});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActive1;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.FindDelay = 2000;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colClientContractID1
            // 
            this.colClientContractID1.Caption = "Client Contract ID";
            this.colClientContractID1.FieldName = "ClientContractID";
            this.colClientContractID1.Name = "colClientContractID1";
            this.colClientContractID1.OptionsColumn.AllowEdit = false;
            this.colClientContractID1.OptionsColumn.AllowFocus = false;
            this.colClientContractID1.OptionsColumn.ReadOnly = true;
            this.colClientContractID1.Width = 107;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colGCCompanyID
            // 
            this.colGCCompanyID.Caption = "GC Company ID";
            this.colGCCompanyID.FieldName = "GCCompanyID";
            this.colGCCompanyID.Name = "colGCCompanyID";
            this.colGCCompanyID.OptionsColumn.AllowEdit = false;
            this.colGCCompanyID.OptionsColumn.AllowFocus = false;
            this.colGCCompanyID.OptionsColumn.ReadOnly = true;
            this.colGCCompanyID.Width = 97;
            // 
            // colContractTypeID
            // 
            this.colContractTypeID.Caption = "Contract Type ID";
            this.colContractTypeID.FieldName = "ContractTypeID";
            this.colContractTypeID.Name = "colContractTypeID";
            this.colContractTypeID.OptionsColumn.AllowEdit = false;
            this.colContractTypeID.OptionsColumn.AllowFocus = false;
            this.colContractTypeID.OptionsColumn.ReadOnly = true;
            this.colContractTypeID.Width = 104;
            // 
            // colContractStatusID
            // 
            this.colContractStatusID.Caption = "Contract Status ID";
            this.colContractStatusID.FieldName = "ContractStatusID";
            this.colContractStatusID.Name = "colContractStatusID";
            this.colContractStatusID.OptionsColumn.AllowEdit = false;
            this.colContractStatusID.OptionsColumn.AllowFocus = false;
            this.colContractStatusID.OptionsColumn.ReadOnly = true;
            this.colContractStatusID.Width = 111;
            // 
            // colContractDirectorID
            // 
            this.colContractDirectorID.Caption = "Contract Director ID";
            this.colContractDirectorID.FieldName = "ContractDirectorID";
            this.colContractDirectorID.Name = "colContractDirectorID";
            this.colContractDirectorID.OptionsColumn.AllowEdit = false;
            this.colContractDirectorID.OptionsColumn.AllowFocus = false;
            this.colContractDirectorID.OptionsColumn.ReadOnly = true;
            this.colContractDirectorID.Width = 118;
            // 
            // colStartDate1
            // 
            this.colStartDate1.Caption = "Start Date";
            this.colStartDate1.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colStartDate1.FieldName = "StartDate";
            this.colStartDate1.Name = "colStartDate1";
            this.colStartDate1.OptionsColumn.AllowEdit = false;
            this.colStartDate1.OptionsColumn.AllowFocus = false;
            this.colStartDate1.OptionsColumn.ReadOnly = true;
            this.colStartDate1.Visible = true;
            this.colStartDate1.VisibleIndex = 2;
            this.colStartDate1.Width = 100;
            // 
            // repositoryItemTextEditDateTime3
            // 
            this.repositoryItemTextEditDateTime3.AutoHeight = false;
            this.repositoryItemTextEditDateTime3.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime3.Name = "repositoryItemTextEditDateTime3";
            // 
            // colEndDate1
            // 
            this.colEndDate1.Caption = "End Date";
            this.colEndDate1.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colEndDate1.FieldName = "EndDate";
            this.colEndDate1.Name = "colEndDate1";
            this.colEndDate1.OptionsColumn.AllowEdit = false;
            this.colEndDate1.OptionsColumn.AllowFocus = false;
            this.colEndDate1.OptionsColumn.ReadOnly = true;
            this.colEndDate1.Visible = true;
            this.colEndDate1.VisibleIndex = 3;
            this.colEndDate1.Width = 100;
            // 
            // colSectorTypeID
            // 
            this.colSectorTypeID.Caption = "Sector Type ID";
            this.colSectorTypeID.FieldName = "SectorTypeID";
            this.colSectorTypeID.Name = "colSectorTypeID";
            this.colSectorTypeID.OptionsColumn.AllowEdit = false;
            this.colSectorTypeID.OptionsColumn.AllowFocus = false;
            this.colSectorTypeID.OptionsColumn.ReadOnly = true;
            this.colSectorTypeID.Width = 93;
            // 
            // colContractValue
            // 
            this.colContractValue.Caption = "Contract Value";
            this.colContractValue.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colContractValue.FieldName = "ContractValue";
            this.colContractValue.Name = "colContractValue";
            this.colContractValue.OptionsColumn.AllowEdit = false;
            this.colContractValue.OptionsColumn.AllowFocus = false;
            this.colContractValue.OptionsColumn.ReadOnly = true;
            this.colContractValue.Visible = true;
            this.colContractValue.VisibleIndex = 10;
            this.colContractValue.Width = 92;
            // 
            // repositoryItemTextEditCurrency3
            // 
            this.repositoryItemTextEditCurrency3.AutoHeight = false;
            this.repositoryItemTextEditCurrency3.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency3.Name = "repositoryItemTextEditCurrency3";
            // 
            // colYearlyPercentageIncrease
            // 
            this.colYearlyPercentageIncrease.Caption = "Yearly % Increase";
            this.colYearlyPercentageIncrease.ColumnEdit = this.repositoryItemTextEditPercentage3;
            this.colYearlyPercentageIncrease.FieldName = "YearlyPercentageIncrease";
            this.colYearlyPercentageIncrease.Name = "colYearlyPercentageIncrease";
            this.colYearlyPercentageIncrease.OptionsColumn.AllowEdit = false;
            this.colYearlyPercentageIncrease.OptionsColumn.AllowFocus = false;
            this.colYearlyPercentageIncrease.OptionsColumn.ReadOnly = true;
            this.colYearlyPercentageIncrease.Visible = true;
            this.colYearlyPercentageIncrease.VisibleIndex = 11;
            this.colYearlyPercentageIncrease.Width = 110;
            // 
            // repositoryItemTextEditPercentage3
            // 
            this.repositoryItemTextEditPercentage3.AutoHeight = false;
            this.repositoryItemTextEditPercentage3.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage3.Name = "repositoryItemTextEditPercentage3";
            // 
            // colCheckRPIDate
            // 
            this.colCheckRPIDate.Caption = "Check RPI Date";
            this.colCheckRPIDate.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colCheckRPIDate.FieldName = "CheckRPIDate";
            this.colCheckRPIDate.Name = "colCheckRPIDate";
            this.colCheckRPIDate.OptionsColumn.AllowEdit = false;
            this.colCheckRPIDate.OptionsColumn.AllowFocus = false;
            this.colCheckRPIDate.OptionsColumn.ReadOnly = true;
            this.colCheckRPIDate.Width = 96;
            // 
            // colLastClientPaymentDate
            // 
            this.colLastClientPaymentDate.Caption = "Last Client Payment Date";
            this.colLastClientPaymentDate.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colLastClientPaymentDate.FieldName = "LastClientPaymentDate";
            this.colLastClientPaymentDate.Name = "colLastClientPaymentDate";
            this.colLastClientPaymentDate.OptionsColumn.AllowEdit = false;
            this.colLastClientPaymentDate.OptionsColumn.AllowFocus = false;
            this.colLastClientPaymentDate.OptionsColumn.ReadOnly = true;
            this.colLastClientPaymentDate.Width = 142;
            // 
            // colLinkedToPreviousContractID
            // 
            this.colLinkedToPreviousContractID.Caption = "Linked To Previous Contract ID";
            this.colLinkedToPreviousContractID.FieldName = "LinkedToPreviousContractID";
            this.colLinkedToPreviousContractID.Name = "colLinkedToPreviousContractID";
            this.colLinkedToPreviousContractID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPreviousContractID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPreviousContractID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPreviousContractID.Width = 169;
            // 
            // colFinanceClientCode
            // 
            this.colFinanceClientCode.Caption = "Finance Client Code";
            this.colFinanceClientCode.FieldName = "FinanceClientCode";
            this.colFinanceClientCode.Name = "colFinanceClientCode";
            this.colFinanceClientCode.OptionsColumn.AllowEdit = false;
            this.colFinanceClientCode.OptionsColumn.AllowFocus = false;
            this.colFinanceClientCode.OptionsColumn.ReadOnly = true;
            this.colFinanceClientCode.Width = 116;
            // 
            // colBillingCentreCodeID
            // 
            this.colBillingCentreCodeID.Caption = "Billing Centre Code ID";
            this.colBillingCentreCodeID.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID.Name = "colBillingCentreCodeID";
            this.colBillingCentreCodeID.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID.Width = 125;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.AllowEdit = false;
            this.colRemarks.OptionsColumn.AllowFocus = false;
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 13;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 195;
            // 
            // colGCCompanyName
            // 
            this.colGCCompanyName.Caption = "Gc Company Name";
            this.colGCCompanyName.FieldName = "GCCompanyName";
            this.colGCCompanyName.Name = "colGCCompanyName";
            this.colGCCompanyName.OptionsColumn.AllowEdit = false;
            this.colGCCompanyName.OptionsColumn.AllowFocus = false;
            this.colGCCompanyName.OptionsColumn.ReadOnly = true;
            this.colGCCompanyName.Width = 111;
            // 
            // colContractType
            // 
            this.colContractType.Caption = "Contract Type";
            this.colContractType.FieldName = "ContractType";
            this.colContractType.Name = "colContractType";
            this.colContractType.OptionsColumn.AllowEdit = false;
            this.colContractType.OptionsColumn.AllowFocus = false;
            this.colContractType.OptionsColumn.ReadOnly = true;
            this.colContractType.Visible = true;
            this.colContractType.VisibleIndex = 6;
            this.colContractType.Width = 147;
            // 
            // colContractStatus
            // 
            this.colContractStatus.Caption = "Contract Status";
            this.colContractStatus.FieldName = "ContractStatus";
            this.colContractStatus.Name = "colContractStatus";
            this.colContractStatus.OptionsColumn.AllowEdit = false;
            this.colContractStatus.OptionsColumn.AllowFocus = false;
            this.colContractStatus.OptionsColumn.ReadOnly = true;
            this.colContractStatus.Visible = true;
            this.colContractStatus.VisibleIndex = 8;
            this.colContractStatus.Width = 136;
            // 
            // colContractDirector
            // 
            this.colContractDirector.Caption = "Contract Director";
            this.colContractDirector.FieldName = "ContractDirector";
            this.colContractDirector.Name = "colContractDirector";
            this.colContractDirector.OptionsColumn.AllowEdit = false;
            this.colContractDirector.OptionsColumn.AllowFocus = false;
            this.colContractDirector.OptionsColumn.ReadOnly = true;
            this.colContractDirector.Visible = true;
            this.colContractDirector.VisibleIndex = 9;
            this.colContractDirector.Width = 120;
            // 
            // colSectorType
            // 
            this.colSectorType.Caption = "Sector Type";
            this.colSectorType.FieldName = "SectorType";
            this.colSectorType.Name = "colSectorType";
            this.colSectorType.OptionsColumn.AllowEdit = false;
            this.colSectorType.OptionsColumn.AllowFocus = false;
            this.colSectorType.OptionsColumn.ReadOnly = true;
            this.colSectorType.Visible = true;
            this.colSectorType.VisibleIndex = 7;
            this.colSectorType.Width = 146;
            // 
            // colClientYearCount
            // 
            this.colClientYearCount.Caption = "Linked Client Contract Years";
            this.colClientYearCount.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colClientYearCount.FieldName = "ClientYearCount";
            this.colClientYearCount.Name = "colClientYearCount";
            this.colClientYearCount.OptionsColumn.AllowEdit = false;
            this.colClientYearCount.OptionsColumn.AllowFocus = false;
            this.colClientYearCount.OptionsColumn.ReadOnly = true;
            this.colClientYearCount.Visible = true;
            this.colClientYearCount.VisibleIndex = 12;
            this.colClientYearCount.Width = 156;
            // 
            // repositoryItemTextEditInteger
            // 
            this.repositoryItemTextEditInteger.AutoHeight = false;
            this.repositoryItemTextEditInteger.Mask.EditMask = "f0";
            this.repositoryItemTextEditInteger.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger.Name = "repositoryItemTextEditInteger";
            // 
            // colContractDescription
            // 
            this.colContractDescription.Caption = "Contract Description";
            this.colContractDescription.FieldName = "ContractDescription";
            this.colContractDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colContractDescription.Name = "colContractDescription";
            this.colContractDescription.OptionsColumn.AllowEdit = false;
            this.colContractDescription.OptionsColumn.AllowFocus = false;
            this.colContractDescription.OptionsColumn.ReadOnly = true;
            this.colContractDescription.Visible = true;
            this.colContractDescription.VisibleIndex = 1;
            this.colContractDescription.Width = 285;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 5;
            this.colReactive.Width = 63;
            // 
            // xtraTabPageStep2
            // 
            this.xtraTabPageStep2.Controls.Add(this.checkEditSkipMaterials);
            this.xtraTabPageStep2.Controls.Add(this.checkEditSkipEquipment);
            this.xtraTabPageStep2.Controls.Add(this.checkEditCreateGenericJobRates);
            this.xtraTabPageStep2.Controls.Add(this.labelControlSelectedSiteCount);
            this.xtraTabPageStep2.Controls.Add(this.gridControl2);
            this.xtraTabPageStep2.Controls.Add(this.panelControl4);
            this.xtraTabPageStep2.Controls.Add(this.btnStep2Next);
            this.xtraTabPageStep2.Controls.Add(this.btnStep2Previous);
            this.xtraTabPageStep2.Name = "xtraTabPageStep2";
            this.xtraTabPageStep2.Size = new System.Drawing.Size(959, 536);
            this.xtraTabPageStep2.Tag = "2";
            this.xtraTabPageStep2.Text = "Step 2";
            // 
            // checkEditCreateGenericJobRates
            // 
            this.checkEditCreateGenericJobRates.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditCreateGenericJobRates.EditValue = true;
            this.checkEditCreateGenericJobRates.Location = new System.Drawing.Point(465, 505);
            this.checkEditCreateGenericJobRates.MenuManager = this.barManager1;
            this.checkEditCreateGenericJobRates.Name = "checkEditCreateGenericJobRates";
            this.checkEditCreateGenericJobRates.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.checkEditCreateGenericJobRates.Properties.Appearance.Options.UseFont = true;
            this.checkEditCreateGenericJobRates.Properties.Caption = "Create Generic Job Rate for Each Selected Site:";
            this.checkEditCreateGenericJobRates.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditCreateGenericJobRates.Size = new System.Drawing.Size(287, 19);
            this.checkEditCreateGenericJobRates.TabIndex = 24;
            // 
            // labelControlSelectedSiteCount
            // 
            this.labelControlSelectedSiteCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControlSelectedSiteCount.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_16x16;
            this.labelControlSelectedSiteCount.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlSelectedSiteCount.Appearance.Options.UseImage = true;
            this.labelControlSelectedSiteCount.Appearance.Options.UseImageAlign = true;
            this.labelControlSelectedSiteCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlSelectedSiteCount.Location = new System.Drawing.Point(7, 496);
            this.labelControlSelectedSiteCount.Name = "labelControlSelectedSiteCount";
            this.labelControlSelectedSiteCount.Size = new System.Drawing.Size(159, 17);
            this.labelControlSelectedSiteCount.TabIndex = 23;
            this.labelControlSelectedSiteCount.Text = "       0 Selected Sites";
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp06089OMSitesForClientBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(7, 62);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6});
            this.gridControl2.Size = new System.Drawing.Size(946, 432);
            this.gridControl2.TabIndex = 22;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp06089OMSitesForClientBindingSource
            // 
            this.sp06089OMSitesForClientBindingSource.DataMember = "sp06089_OM_Sites_For_Client";
            this.sp06089OMSitesForClientBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteID,
            this.colClientID2,
            this.colSiteTypeID,
            this.colSiteName,
            this.colSiteAddressLine1,
            this.colSiteAddressLine2,
            this.colSiteAddressLine3,
            this.colSiteAddressLine4,
            this.colSiteAddressLine5,
            this.colSitePostcode,
            this.colRemarks4,
            this.colLocationX,
            this.colLocationY,
            this.colSiteType,
            this.colActiveSiteContractCount});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.FindDelay = 2000;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.Visible = true;
            this.colSiteID.VisibleIndex = 0;
            this.colSiteID.Width = 51;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            this.colClientID2.Width = 62;
            // 
            // colSiteTypeID
            // 
            this.colSiteTypeID.Caption = "Site Type ID";
            this.colSiteTypeID.FieldName = "SiteTypeID";
            this.colSiteTypeID.Name = "colSiteTypeID";
            this.colSiteTypeID.OptionsColumn.AllowEdit = false;
            this.colSiteTypeID.OptionsColumn.AllowFocus = false;
            this.colSiteTypeID.OptionsColumn.ReadOnly = true;
            this.colSiteTypeID.Width = 80;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 1;
            this.colSiteName.Width = 200;
            // 
            // colSiteAddressLine1
            // 
            this.colSiteAddressLine1.Caption = "Address Line 1";
            this.colSiteAddressLine1.FieldName = "SiteAddressLine1";
            this.colSiteAddressLine1.Name = "colSiteAddressLine1";
            this.colSiteAddressLine1.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine1.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine1.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine1.Visible = true;
            this.colSiteAddressLine1.VisibleIndex = 4;
            this.colSiteAddressLine1.Width = 120;
            // 
            // colSiteAddressLine2
            // 
            this.colSiteAddressLine2.Caption = "Address Line 2";
            this.colSiteAddressLine2.FieldName = "SiteAddressLine2";
            this.colSiteAddressLine2.Name = "colSiteAddressLine2";
            this.colSiteAddressLine2.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine2.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine2.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine2.Visible = true;
            this.colSiteAddressLine2.VisibleIndex = 5;
            this.colSiteAddressLine2.Width = 120;
            // 
            // colSiteAddressLine3
            // 
            this.colSiteAddressLine3.Caption = "Address Line 3";
            this.colSiteAddressLine3.FieldName = "SiteAddressLine3";
            this.colSiteAddressLine3.Name = "colSiteAddressLine3";
            this.colSiteAddressLine3.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine3.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine3.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine3.Visible = true;
            this.colSiteAddressLine3.VisibleIndex = 6;
            this.colSiteAddressLine3.Width = 120;
            // 
            // colSiteAddressLine4
            // 
            this.colSiteAddressLine4.Caption = "Address Line 4";
            this.colSiteAddressLine4.FieldName = "SiteAddressLine4";
            this.colSiteAddressLine4.Name = "colSiteAddressLine4";
            this.colSiteAddressLine4.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine4.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine4.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine4.Visible = true;
            this.colSiteAddressLine4.VisibleIndex = 7;
            this.colSiteAddressLine4.Width = 120;
            // 
            // colSiteAddressLine5
            // 
            this.colSiteAddressLine5.Caption = "Address Line 5";
            this.colSiteAddressLine5.FieldName = "SiteAddressLine5";
            this.colSiteAddressLine5.Name = "colSiteAddressLine5";
            this.colSiteAddressLine5.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine5.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine5.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine5.Visible = true;
            this.colSiteAddressLine5.VisibleIndex = 8;
            this.colSiteAddressLine5.Width = 120;
            // 
            // colSitePostcode
            // 
            this.colSitePostcode.Caption = "Postcode";
            this.colSitePostcode.FieldName = "SitePostcode";
            this.colSitePostcode.Name = "colSitePostcode";
            this.colSitePostcode.OptionsColumn.AllowEdit = false;
            this.colSitePostcode.OptionsColumn.AllowFocus = false;
            this.colSitePostcode.OptionsColumn.ReadOnly = true;
            this.colSitePostcode.Visible = true;
            this.colSitePostcode.VisibleIndex = 9;
            this.colSitePostcode.Width = 65;
            // 
            // colRemarks4
            // 
            this.colRemarks4.Caption = "Remarks";
            this.colRemarks4.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colRemarks4.FieldName = "Remarks";
            this.colRemarks4.Name = "colRemarks4";
            this.colRemarks4.OptionsColumn.AllowEdit = false;
            this.colRemarks4.OptionsColumn.AllowFocus = false;
            this.colRemarks4.OptionsColumn.ReadOnly = true;
            this.colRemarks4.Visible = true;
            this.colRemarks4.VisibleIndex = 10;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // colLocationX
            // 
            this.colLocationX.Caption = "Location X";
            this.colLocationX.FieldName = "LocationX";
            this.colLocationX.Name = "colLocationX";
            this.colLocationX.OptionsColumn.AllowEdit = false;
            this.colLocationX.OptionsColumn.AllowFocus = false;
            this.colLocationX.OptionsColumn.ReadOnly = true;
            // 
            // colLocationY
            // 
            this.colLocationY.Caption = "Location Y";
            this.colLocationY.FieldName = "LocationY";
            this.colLocationY.Name = "colLocationY";
            this.colLocationY.OptionsColumn.AllowEdit = false;
            this.colLocationY.OptionsColumn.AllowFocus = false;
            this.colLocationY.OptionsColumn.ReadOnly = true;
            // 
            // colSiteType
            // 
            this.colSiteType.Caption = "Site Type";
            this.colSiteType.FieldName = "SiteType";
            this.colSiteType.Name = "colSiteType";
            this.colSiteType.OptionsColumn.AllowEdit = false;
            this.colSiteType.OptionsColumn.AllowFocus = false;
            this.colSiteType.OptionsColumn.ReadOnly = true;
            this.colSiteType.Visible = true;
            this.colSiteType.VisibleIndex = 3;
            this.colSiteType.Width = 126;
            // 
            // colActiveSiteContractCount
            // 
            this.colActiveSiteContractCount.Caption = "Active Contracts";
            this.colActiveSiteContractCount.FieldName = "ActiveSiteContractCount";
            this.colActiveSiteContractCount.Name = "colActiveSiteContractCount";
            this.colActiveSiteContractCount.OptionsColumn.AllowEdit = false;
            this.colActiveSiteContractCount.OptionsColumn.AllowFocus = false;
            this.colActiveSiteContractCount.OptionsColumn.ReadOnly = true;
            this.colActiveSiteContractCount.Visible = true;
            this.colActiveSiteContractCount.VisibleIndex = 2;
            this.colActiveSiteContractCount.Width = 99;
            // 
            // panelControl4
            // 
            this.panelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl4.Controls.Add(this.pictureEdit5);
            this.panelControl4.Controls.Add(this.labelControl7);
            this.panelControl4.Controls.Add(this.labelControl8);
            this.panelControl4.Location = new System.Drawing.Point(7, 6);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(946, 48);
            this.panelControl4.TabIndex = 15;
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit5.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit5.Location = new System.Drawing.Point(902, 4);
            this.pictureEdit5.MenuManager = this.barManager1;
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Properties.ReadOnly = true;
            this.pictureEdit5.Properties.ShowMenu = false;
            this.pictureEdit5.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit5.TabIndex = 9;
            // 
            // labelControl7
            // 
            this.labelControl7.AllowHtmlString = true;
            this.labelControl7.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseBackColor = true;
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(5, 5);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(261, 16);
            this.labelControl7.TabIndex = 6;
            this.labelControl7.Text = "<b>Step 2:</b> Select Site(s) to create Contracts for";
            // 
            // labelControl8
            // 
            this.labelControl8.AllowHtmlString = true;
            this.labelControl8.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl8.Appearance.Options.UseBackColor = true;
            this.labelControl8.Location = new System.Drawing.Point(57, 29);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(446, 13);
            this.labelControl8.TabIndex = 7;
            this.labelControl8.Text = "Select one or more Sites to create Site contracts for by <b>ticking</b> them. Cli" +
    "ck Next when done.";
            // 
            // btnStep2Next
            // 
            this.btnStep2Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep2Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep2Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep2Next.Location = new System.Drawing.Point(865, 500);
            this.btnStep2Next.Name = "btnStep2Next";
            this.btnStep2Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep2Next.TabIndex = 0;
            this.btnStep2Next.Text = "Next";
            this.btnStep2Next.Click += new System.EventHandler(this.btnStep2Next_Click);
            // 
            // btnStep2Previous
            // 
            this.btnStep2Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep2Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep2Previous.Location = new System.Drawing.Point(771, 500);
            this.btnStep2Previous.Name = "btnStep2Previous";
            this.btnStep2Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep2Previous.TabIndex = 10;
            this.btnStep2Previous.Text = "Previous";
            this.btnStep2Previous.Click += new System.EventHandler(this.btnStep2Previous_Click);
            // 
            // xtraTabPageStep3
            // 
            this.xtraTabPageStep3.Controls.Add(this.gridControl3);
            this.xtraTabPageStep3.Controls.Add(this.btnStep3Next);
            this.xtraTabPageStep3.Controls.Add(this.btnStep3Previous);
            this.xtraTabPageStep3.Controls.Add(this.panelControl8);
            this.xtraTabPageStep3.Name = "xtraTabPageStep3";
            this.xtraTabPageStep3.Size = new System.Drawing.Size(959, 536);
            this.xtraTabPageStep3.Tag = "3";
            this.xtraTabPageStep3.Text = "Step 3";
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp06090OMSiteContractEditBindingSource;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Block Edit Selected Records", "block_edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(7, 62);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit3,
            this.repositoryItemCheckEdit5,
            this.repositoryItemSpinEdit2,
            this.repositoryItemMemoExEdit7,
            this.repositoryItemSpinEditPercentage,
            this.repositoryItemGridLookUpEditClientBillingTypeID,
            this.repositoryItemGridLookUpEditDefaultScheduleTemplateID});
            this.gridControl3.Size = new System.Drawing.Size(946, 432);
            this.gridControl3.TabIndex = 22;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp06090OMSiteContractEditBindingSource
            // 
            this.sp06090OMSiteContractEditBindingSource.DataMember = "sp06090_OM_Site_Contract_Edit";
            this.sp06090OMSiteContractEditBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteContractID,
            this.colClientContractID2,
            this.colSiteID1,
            this.colStartDate2,
            this.colEndDate2,
            this.colActive2,
            this.colContractValue1,
            this.colLastClientPaymentDate1,
            this.colRemarks5,
            this.colYearlyPercentageIncrease1,
            this.colDummySiteContractID,
            this.colLinkedToParent2,
            this.colClientName2,
            this.colSiteName1,
            this.colSiteInstructions,
            this.colClientBillingTypeID,
            this.colSiteCategory,
            this.colDefaultScheduleTemplateID,
            this.colDaysSeparationPercent});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView3.OptionsFilter.AllowFilterEditor = false;
            this.gridView3.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView3.OptionsFilter.AllowMRUFilterList = false;
            this.gridView3.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView3.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView3.OptionsFind.FindDelay = 2000;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsLayout.StoreFormatRules = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView3.OptionsView.ShowFooter = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colSiteContractID
            // 
            this.colSiteContractID.Caption = "Site Contract ID";
            this.colSiteContractID.FieldName = "SiteContractID";
            this.colSiteContractID.Name = "colSiteContractID";
            this.colSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSiteContractID.Width = 98;
            // 
            // colClientContractID2
            // 
            this.colClientContractID2.Caption = "Client Contract ID";
            this.colClientContractID2.FieldName = "ClientContractID";
            this.colClientContractID2.Name = "colClientContractID2";
            this.colClientContractID2.OptionsColumn.AllowEdit = false;
            this.colClientContractID2.OptionsColumn.AllowFocus = false;
            this.colClientContractID2.OptionsColumn.ReadOnly = true;
            this.colClientContractID2.Width = 107;
            // 
            // colSiteID1
            // 
            this.colSiteID1.Caption = "Site ID";
            this.colSiteID1.FieldName = "SiteID";
            this.colSiteID1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteID1.Name = "colSiteID1";
            this.colSiteID1.OptionsColumn.AllowEdit = false;
            this.colSiteID1.OptionsColumn.AllowFocus = false;
            this.colSiteID1.OptionsColumn.ReadOnly = true;
            this.colSiteID1.Visible = true;
            this.colSiteID1.VisibleIndex = 0;
            this.colSiteID1.Width = 51;
            // 
            // colStartDate2
            // 
            this.colStartDate2.Caption = "Start Date";
            this.colStartDate2.ColumnEdit = this.repositoryItemDateEdit3;
            this.colStartDate2.FieldName = "StartDate";
            this.colStartDate2.Name = "colStartDate2";
            this.colStartDate2.Visible = true;
            this.colStartDate2.VisibleIndex = 2;
            this.colStartDate2.Width = 120;
            // 
            // repositoryItemDateEdit3
            // 
            this.repositoryItemDateEdit3.AutoHeight = false;
            this.repositoryItemDateEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit3.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit3.Mask.EditMask = "g";
            this.repositoryItemDateEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit3.Name = "repositoryItemDateEdit3";
            // 
            // colEndDate2
            // 
            this.colEndDate2.Caption = "End Date";
            this.colEndDate2.ColumnEdit = this.repositoryItemDateEdit3;
            this.colEndDate2.FieldName = "EndDate";
            this.colEndDate2.Name = "colEndDate2";
            this.colEndDate2.Visible = true;
            this.colEndDate2.VisibleIndex = 3;
            this.colEndDate2.Width = 120;
            // 
            // colActive2
            // 
            this.colActive2.Caption = "Active";
            this.colActive2.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colActive2.FieldName = "Active";
            this.colActive2.Name = "colActive2";
            this.colActive2.Visible = true;
            this.colActive2.VisibleIndex = 4;
            this.colActive2.Width = 51;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Caption = "Check";
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.ValueChecked = 1;
            this.repositoryItemCheckEdit5.ValueUnchecked = 0;
            // 
            // colContractValue1
            // 
            this.colContractValue1.Caption = "Contract Value";
            this.colContractValue1.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colContractValue1.FieldName = "ContractValue";
            this.colContractValue1.Name = "colContractValue1";
            this.colContractValue1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ContractValue", "{0:c2}")});
            this.colContractValue1.Visible = true;
            this.colContractValue1.VisibleIndex = 6;
            this.colContractValue1.Width = 98;
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit2.Mask.EditMask = "c";
            this.repositoryItemSpinEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // colLastClientPaymentDate1
            // 
            this.colLastClientPaymentDate1.Caption = "Last Client Payment Date";
            this.colLastClientPaymentDate1.FieldName = "LastClientPaymentDate";
            this.colLastClientPaymentDate1.Name = "colLastClientPaymentDate1";
            this.colLastClientPaymentDate1.OptionsColumn.ShowInCustomizationForm = false;
            this.colLastClientPaymentDate1.Width = 142;
            // 
            // colRemarks5
            // 
            this.colRemarks5.Caption = "Remarks";
            this.colRemarks5.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.colRemarks5.FieldName = "Remarks";
            this.colRemarks5.Name = "colRemarks5";
            this.colRemarks5.Visible = true;
            this.colRemarks5.VisibleIndex = 12;
            this.colRemarks5.Width = 142;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // colYearlyPercentageIncrease1
            // 
            this.colYearlyPercentageIncrease1.Caption = "Yearly % Increase";
            this.colYearlyPercentageIncrease1.ColumnEdit = this.repositoryItemSpinEditPercentage;
            this.colYearlyPercentageIncrease1.FieldName = "YearlyPercentageIncrease";
            this.colYearlyPercentageIncrease1.Name = "colYearlyPercentageIncrease1";
            this.colYearlyPercentageIncrease1.Visible = true;
            this.colYearlyPercentageIncrease1.VisibleIndex = 7;
            this.colYearlyPercentageIncrease1.Width = 110;
            // 
            // repositoryItemSpinEditPercentage
            // 
            this.repositoryItemSpinEditPercentage.AutoHeight = false;
            this.repositoryItemSpinEditPercentage.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditPercentage.Mask.EditMask = "P";
            this.repositoryItemSpinEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditPercentage.Name = "repositoryItemSpinEditPercentage";
            // 
            // colDummySiteContractID
            // 
            this.colDummySiteContractID.Caption = "Dummy Site Contract ID";
            this.colDummySiteContractID.FieldName = "DummySiteContractID";
            this.colDummySiteContractID.Name = "colDummySiteContractID";
            this.colDummySiteContractID.OptionsColumn.ShowInCustomizationForm = false;
            this.colDummySiteContractID.Width = 136;
            // 
            // colLinkedToParent2
            // 
            this.colLinkedToParent2.Caption = "Linked To Parent";
            this.colLinkedToParent2.FieldName = "LinkedToParent";
            this.colLinkedToParent2.Name = "colLinkedToParent2";
            this.colLinkedToParent2.Width = 101;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.Width = 172;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name  [Read Only]";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Visible = true;
            this.colSiteName1.VisibleIndex = 1;
            this.colSiteName1.Width = 234;
            // 
            // colSiteInstructions
            // 
            this.colSiteInstructions.Caption = "Site Instructions";
            this.colSiteInstructions.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.colSiteInstructions.FieldName = "SiteInstructions";
            this.colSiteInstructions.Name = "colSiteInstructions";
            this.colSiteInstructions.Visible = true;
            this.colSiteInstructions.VisibleIndex = 10;
            this.colSiteInstructions.Width = 142;
            // 
            // colClientBillingTypeID
            // 
            this.colClientBillingTypeID.Caption = "Billing Type";
            this.colClientBillingTypeID.ColumnEdit = this.repositoryItemGridLookUpEditClientBillingTypeID;
            this.colClientBillingTypeID.FieldName = "ClientBillingTypeID";
            this.colClientBillingTypeID.Name = "colClientBillingTypeID";
            this.colClientBillingTypeID.Visible = true;
            this.colClientBillingTypeID.VisibleIndex = 5;
            this.colClientBillingTypeID.Width = 134;
            // 
            // repositoryItemGridLookUpEditClientBillingTypeID
            // 
            this.repositoryItemGridLookUpEditClientBillingTypeID.AutoHeight = false;
            this.repositoryItemGridLookUpEditClientBillingTypeID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditClientBillingTypeID.DataSource = this.sp06355OMClientBillingTypesWithBlankBindingSource;
            this.repositoryItemGridLookUpEditClientBillingTypeID.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditClientBillingTypeID.Name = "repositoryItemGridLookUpEditClientBillingTypeID";
            this.repositoryItemGridLookUpEditClientBillingTypeID.PopupView = this.repositoryItemGridLookUpEdit4View;
            this.repositoryItemGridLookUpEditClientBillingTypeID.ValueMember = "ID";
            // 
            // sp06355OMClientBillingTypesWithBlankBindingSource
            // 
            this.sp06355OMClientBillingTypesWithBlankBindingSource.DataMember = "sp06355_OM_Client_Billing_Types_With_Blank";
            this.sp06355OMClientBillingTypesWithBlankBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // repositoryItemGridLookUpEdit4View
            // 
            this.repositoryItemGridLookUpEdit4View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription1,
            this.colID,
            this.colRecordOrder});
            this.repositoryItemGridLookUpEdit4View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit4View.Name = "repositoryItemGridLookUpEdit4View";
            this.repositoryItemGridLookUpEdit4View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit4View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit4View.OptionsLayout.StoreFormatRules = true;
            this.repositoryItemGridLookUpEdit4View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit4View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit4View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit4View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit4View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit4View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit4View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit4View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Billing Type";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 199;
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            this.colID.Width = 31;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // colSiteCategory
            // 
            this.colSiteCategory.Caption = "Site Category";
            this.colSiteCategory.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.colSiteCategory.FieldName = "SiteCategory";
            this.colSiteCategory.Name = "colSiteCategory";
            this.colSiteCategory.Visible = true;
            this.colSiteCategory.VisibleIndex = 8;
            this.colSiteCategory.Width = 142;
            // 
            // colDefaultScheduleTemplateID
            // 
            this.colDefaultScheduleTemplateID.Caption = "Default Schedule Template";
            this.colDefaultScheduleTemplateID.ColumnEdit = this.repositoryItemGridLookUpEditDefaultScheduleTemplateID;
            this.colDefaultScheduleTemplateID.FieldName = "DefaultScheduleTemplateID";
            this.colDefaultScheduleTemplateID.Name = "colDefaultScheduleTemplateID";
            this.colDefaultScheduleTemplateID.Visible = true;
            this.colDefaultScheduleTemplateID.VisibleIndex = 9;
            this.colDefaultScheduleTemplateID.Width = 186;
            // 
            // repositoryItemGridLookUpEditDefaultScheduleTemplateID
            // 
            this.repositoryItemGridLookUpEditDefaultScheduleTemplateID.AutoHeight = false;
            this.repositoryItemGridLookUpEditDefaultScheduleTemplateID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditDefaultScheduleTemplateID.DataSource = this.sp06141OMVisitTemplateHeadersWithBlankBindingSource;
            this.repositoryItemGridLookUpEditDefaultScheduleTemplateID.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditDefaultScheduleTemplateID.Name = "repositoryItemGridLookUpEditDefaultScheduleTemplateID";
            this.repositoryItemGridLookUpEditDefaultScheduleTemplateID.PopupView = this.repositoryItemGridLookUpEdit3View;
            this.repositoryItemGridLookUpEditDefaultScheduleTemplateID.ValueMember = "ID";
            // 
            // sp06141OMVisitTemplateHeadersWithBlankBindingSource
            // 
            this.sp06141OMVisitTemplateHeadersWithBlankBindingSource.DataMember = "sp06141_OM_Visit_Template_Headers_With_Blank";
            this.sp06141OMVisitTemplateHeadersWithBlankBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // repositoryItemGridLookUpEdit3View
            // 
            this.repositoryItemGridLookUpEdit3View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn123,
            this.gridColumn124,
            this.gridColumn125});
            this.repositoryItemGridLookUpEdit3View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn123;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.repositoryItemGridLookUpEdit3View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.repositoryItemGridLookUpEdit3View.Name = "repositoryItemGridLookUpEdit3View";
            this.repositoryItemGridLookUpEdit3View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit3View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit3View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit3View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit3View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit3View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit3View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit3View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit3View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit3View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit3View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn125, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn124
            // 
            this.gridColumn124.Caption = "Schedule Template";
            this.gridColumn124.FieldName = "Description";
            this.gridColumn124.Name = "gridColumn124";
            this.gridColumn124.OptionsColumn.AllowEdit = false;
            this.gridColumn124.OptionsColumn.AllowFocus = false;
            this.gridColumn124.OptionsColumn.ReadOnly = true;
            this.gridColumn124.Visible = true;
            this.gridColumn124.VisibleIndex = 0;
            this.gridColumn124.Width = 260;
            // 
            // gridColumn125
            // 
            this.gridColumn125.Caption = "Order";
            this.gridColumn125.FieldName = "RecordOrder";
            this.gridColumn125.Name = "gridColumn125";
            this.gridColumn125.OptionsColumn.AllowEdit = false;
            this.gridColumn125.OptionsColumn.AllowFocus = false;
            this.gridColumn125.OptionsColumn.ReadOnly = true;
            // 
            // colDaysSeparationPercent
            // 
            this.colDaysSeparationPercent.Caption = "Visit Days Separation %";
            this.colDaysSeparationPercent.ColumnEdit = this.repositoryItemSpinEditPercentage;
            this.colDaysSeparationPercent.FieldName = "DaysSeparationPercent";
            this.colDaysSeparationPercent.Name = "colDaysSeparationPercent";
            this.colDaysSeparationPercent.Visible = true;
            this.colDaysSeparationPercent.VisibleIndex = 11;
            this.colDaysSeparationPercent.Width = 134;
            // 
            // btnStep3Next
            // 
            this.btnStep3Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep3Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep3Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep3Next.Location = new System.Drawing.Point(865, 500);
            this.btnStep3Next.Name = "btnStep3Next";
            this.btnStep3Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep3Next.TabIndex = 17;
            this.btnStep3Next.Text = "Next";
            this.btnStep3Next.Click += new System.EventHandler(this.btnStep3Next_Click);
            // 
            // btnStep3Previous
            // 
            this.btnStep3Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep3Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep3Previous.Location = new System.Drawing.Point(771, 500);
            this.btnStep3Previous.Name = "btnStep3Previous";
            this.btnStep3Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep3Previous.TabIndex = 18;
            this.btnStep3Previous.Text = "Previous";
            this.btnStep3Previous.Click += new System.EventHandler(this.btnStep3Previous_Click);
            // 
            // panelControl8
            // 
            this.panelControl8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl8.Controls.Add(this.btnSplitValueOverSites);
            this.panelControl8.Controls.Add(this.CurrentContractValueTextEdit);
            this.panelControl8.Controls.Add(this.pictureEdit10);
            this.panelControl8.Controls.Add(this.labelControl19);
            this.panelControl8.Controls.Add(this.labelControl20);
            this.panelControl8.Location = new System.Drawing.Point(7, 6);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(946, 48);
            this.panelControl8.TabIndex = 16;
            // 
            // btnSplitValueOverSites
            // 
            this.btnSplitValueOverSites.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSplitValueOverSites.ImageOptions.Image")));
            this.btnSplitValueOverSites.Location = new System.Drawing.Point(672, 22);
            this.btnSplitValueOverSites.Name = "btnSplitValueOverSites";
            this.btnSplitValueOverSites.Size = new System.Drawing.Size(102, 23);
            toolTipTitleItem15.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem15.Appearance.Options.UseImage = true;
            toolTipTitleItem15.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem15.Text = "Split Value Over Sites - Infromation";
            toolTipItem15.LeftIndent = 6;
            toolTipItem15.Text = "Click to split the Contract Value evenly accross all sites on the contract.";
            superToolTip15.Items.Add(toolTipTitleItem15);
            superToolTip15.Items.Add(toolTipItem15);
            this.btnSplitValueOverSites.SuperTip = superToolTip15;
            this.btnSplitValueOverSites.TabIndex = 11;
            this.btnSplitValueOverSites.Text = "Split Over Sites";
            this.btnSplitValueOverSites.Click += new System.EventHandler(this.btnSplitValueOverSites_Click);
            // 
            // CurrentContractValueTextEdit
            // 
            this.CurrentContractValueTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.CurrentContractValueTextEdit.Location = new System.Drawing.Point(583, 25);
            this.CurrentContractValueTextEdit.MenuManager = this.barManager1;
            this.CurrentContractValueTextEdit.Name = "CurrentContractValueTextEdit";
            this.CurrentContractValueTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.CurrentContractValueTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CurrentContractValueTextEdit.Properties.Mask.EditMask = "c";
            this.CurrentContractValueTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.CurrentContractValueTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CurrentContractValueTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CurrentContractValueTextEdit, true);
            this.CurrentContractValueTextEdit.Size = new System.Drawing.Size(83, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CurrentContractValueTextEdit, optionsSpelling1);
            this.CurrentContractValueTextEdit.TabIndex = 10;
            // 
            // pictureEdit10
            // 
            this.pictureEdit10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit10.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit10.Location = new System.Drawing.Point(902, 4);
            this.pictureEdit10.MenuManager = this.barManager1;
            this.pictureEdit10.Name = "pictureEdit10";
            this.pictureEdit10.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit10.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit10.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit10.Properties.ReadOnly = true;
            this.pictureEdit10.Properties.ShowMenu = false;
            this.pictureEdit10.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit10.TabIndex = 9;
            // 
            // labelControl19
            // 
            this.labelControl19.AllowHtmlString = true;
            this.labelControl19.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Appearance.Options.UseBackColor = true;
            this.labelControl19.Appearance.Options.UseFont = true;
            this.labelControl19.Location = new System.Drawing.Point(5, 5);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(190, 16);
            this.labelControl19.TabIndex = 6;
            this.labelControl19.Text = "<b>Step 3:</b> Set Site Contract Values";
            // 
            // labelControl20
            // 
            this.labelControl20.AllowHtmlString = true;
            this.labelControl20.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl20.Appearance.Options.UseBackColor = true;
            this.labelControl20.Location = new System.Drawing.Point(57, 29);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(522, 13);
            this.labelControl20.TabIndex = 7;
            this.labelControl20.Text = "Complete the data entry boxes for each selected site. Click Next when done.  <b>S" +
    "elected Contract - Value:</b>";
            // 
            // xtraTabPageStep4
            // 
            this.xtraTabPageStep4.Controls.Add(this.splitContainerControl2);
            this.xtraTabPageStep4.Controls.Add(this.btnStep4Next);
            this.xtraTabPageStep4.Controls.Add(this.btnStep4Previous);
            this.xtraTabPageStep4.Controls.Add(this.panelControl5);
            this.xtraTabPageStep4.Name = "xtraTabPageStep4";
            this.xtraTabPageStep4.Size = new System.Drawing.Size(959, 536);
            this.xtraTabPageStep4.Tag = "4";
            this.xtraTabPageStep4.Text = "Step 4";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl2.Location = new System.Drawing.Point(7, 62);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControl4);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Sites  [Read Only]";
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.Controls.Add(this.layoutControl1);
            this.splitContainerControl2.Panel2.ShowCaption = true;
            this.splitContainerControl2.Panel2.Text = "Site Contract Years - Linked to Selected Sites";
            this.splitContainerControl2.Size = new System.Drawing.Size(947, 432);
            this.splitContainerControl2.SplitterPosition = 373;
            this.splitContainerControl2.TabIndex = 20;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp06090OMSiteContractEditBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit6,
            this.repositoryItemMemoExEdit8,
            this.repositoryItemTextEditDateTime4,
            this.repositoryItemTextEditCurrency4,
            this.repositoryItemTextEditPercentage4});
            this.gridControl4.Size = new System.Drawing.Size(369, 408);
            this.gridControl4.TabIndex = 23;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView4.OptionsFilter.AllowFilterEditor = false;
            this.gridView4.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView4.OptionsFilter.AllowMRUFilterList = false;
            this.gridView4.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView4.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView4.OptionsFind.FindDelay = 2000;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsLayout.StoreFormatRules = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn25, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Site Contract ID";
            this.gridColumn1.FieldName = "SiteContractID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 98;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Client Contract ID";
            this.gridColumn2.FieldName = "ClientContractID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 107;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Site ID";
            this.gridColumn3.FieldName = "SiteID";
            this.gridColumn3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 51;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Start Date";
            this.gridColumn4.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.gridColumn4.FieldName = "StartDate";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 120;
            // 
            // repositoryItemTextEditDateTime4
            // 
            this.repositoryItemTextEditDateTime4.AutoHeight = false;
            this.repositoryItemTextEditDateTime4.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime4.Name = "repositoryItemTextEditDateTime4";
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "End Date";
            this.gridColumn5.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.gridColumn5.FieldName = "EndDate";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 120;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Active";
            this.gridColumn6.ColumnEdit = this.repositoryItemCheckEdit6;
            this.gridColumn6.FieldName = "Active";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            this.gridColumn6.Width = 51;
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Caption = "Check";
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            this.repositoryItemCheckEdit6.ValueChecked = 1;
            this.repositoryItemCheckEdit6.ValueUnchecked = 0;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Contract Value";
            this.gridColumn7.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn7.FieldName = "ContractValue";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ContractValue", "{0:c2}")});
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 2;
            this.gridColumn7.Width = 92;
            // 
            // repositoryItemTextEditCurrency4
            // 
            this.repositoryItemTextEditCurrency4.AutoHeight = false;
            this.repositoryItemTextEditCurrency4.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency4.Name = "repositoryItemTextEditCurrency4";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Last Client Payment Date";
            this.gridColumn8.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.gridColumn8.FieldName = "LastClientPaymentDate";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn8.Width = 142;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Remarks";
            this.gridColumn9.ColumnEdit = this.repositoryItemMemoExEdit8;
            this.gridColumn9.FieldName = "Remarks";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 7;
            this.gridColumn9.Width = 142;
            // 
            // repositoryItemMemoExEdit8
            // 
            this.repositoryItemMemoExEdit8.AutoHeight = false;
            this.repositoryItemMemoExEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit8.Name = "repositoryItemMemoExEdit8";
            this.repositoryItemMemoExEdit8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit8.ShowIcon = false;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Yearly % Increase";
            this.gridColumn21.ColumnEdit = this.repositoryItemTextEditPercentage4;
            this.gridColumn21.FieldName = "YearlyPercentageIncrease";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 6;
            this.gridColumn21.Width = 110;
            // 
            // repositoryItemTextEditPercentage4
            // 
            this.repositoryItemTextEditPercentage4.AutoHeight = false;
            this.repositoryItemTextEditPercentage4.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage4.Name = "repositoryItemTextEditPercentage4";
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Dummy Site Contract ID";
            this.gridColumn22.FieldName = "DummySiteContractID";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn22.Width = 136;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Linked To Parent";
            this.gridColumn23.FieldName = "LinkedToParent";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Width = 101;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Client Name";
            this.gridColumn24.FieldName = "ClientName";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Width = 172;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Site Name";
            this.gridColumn25.FieldName = "SiteName";
            this.gridColumn25.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 1;
            this.gridColumn25.Width = 234;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.CopyClientContractYearsSimpleButton);
            this.layoutControl1.Controls.Add(this.btnAddYearsBtn);
            this.layoutControl1.Controls.Add(this.gridControl5);
            this.layoutControl1.Controls.Add(this.spinEditContractYears);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(534, 136, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup4;
            this.layoutControl1.Size = new System.Drawing.Size(564, 408);
            this.layoutControl1.TabIndex = 19;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // CopyClientContractYearsSimpleButton
            // 
            this.CopyClientContractYearsSimpleButton.ImageOptions.ImageIndex = 9;
            this.CopyClientContractYearsSimpleButton.ImageOptions.ImageList = this.imageCollection1;
            this.CopyClientContractYearsSimpleButton.Location = new System.Drawing.Point(259, 2);
            this.CopyClientContractYearsSimpleButton.Name = "CopyClientContractYearsSimpleButton";
            this.CopyClientContractYearsSimpleButton.Size = new System.Drawing.Size(160, 22);
            this.CopyClientContractYearsSimpleButton.StyleController = this.layoutControl1;
            this.CopyClientContractYearsSimpleButton.TabIndex = 23;
            this.CopyClientContractYearsSimpleButton.Text = "Copy Client Contract Years";
            this.CopyClientContractYearsSimpleButton.Click += new System.EventHandler(this.CopyClientContractYearsSimpleButton_Click);
            // 
            // btnAddYearsBtn
            // 
            this.btnAddYearsBtn.ImageOptions.ImageIndex = 0;
            this.btnAddYearsBtn.ImageOptions.ImageList = this.imageCollection1;
            this.btnAddYearsBtn.Location = new System.Drawing.Point(139, 2);
            this.btnAddYearsBtn.Name = "btnAddYearsBtn";
            this.btnAddYearsBtn.Size = new System.Drawing.Size(116, 22);
            this.btnAddYearsBtn.StyleController = this.layoutControl1;
            this.btnAddYearsBtn.TabIndex = 22;
            this.btnAddYearsBtn.Text = "Add Years To Grid";
            this.btnAddYearsBtn.Click += new System.EventHandler(this.btnAddYearsBtn_Click);
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp06092OMSiteContractYearEditBindingSource;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Block Edit Selected Records", "block_edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl5.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl5_EmbeddedNavigator_ButtonClick);
            this.gridControl5.Location = new System.Drawing.Point(2, 28);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemSpinEdit1,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEdit50CharsMax,
            this.repositoryItemSpinEditPercentage2});
            this.gridControl5.Size = new System.Drawing.Size(560, 378);
            this.gridControl5.TabIndex = 21;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp06092OMSiteContractYearEditBindingSource
            // 
            this.sp06092OMSiteContractYearEditBindingSource.DataMember = "sp06092_OM_Site_Contract_Year_Edit";
            this.sp06092OMSiteContractYearEditBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientContractYearID,
            this.colClientContractID,
            this.colYearDescription,
            this.colStartDate,
            this.colEndDate,
            this.colActive,
            this.colClientValue,
            this.colRemarks1,
            this.colSiteName2,
            this.colYearlyPercentageIncrease2});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupCount = 1;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView5.OptionsFilter.AllowFilterEditor = false;
            this.gridView5.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView5.OptionsFilter.AllowMRUFilterList = false;
            this.gridView5.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView5.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView5.OptionsFind.FindDelay = 2000;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsLayout.StoreFormatRules = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView5.OptionsView.ShowFooter = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colClientContractYearID
            // 
            this.colClientContractYearID.Caption = "Site Contract Year ID";
            this.colClientContractYearID.FieldName = "SiteContractYearID";
            this.colClientContractYearID.Name = "colClientContractYearID";
            this.colClientContractYearID.OptionsColumn.AllowEdit = false;
            this.colClientContractYearID.OptionsColumn.AllowFocus = false;
            this.colClientContractYearID.OptionsFilter.AllowAutoFilter = false;
            this.colClientContractYearID.OptionsFilter.AllowFilter = false;
            this.colClientContractYearID.Width = 132;
            // 
            // colClientContractID
            // 
            this.colClientContractID.Caption = "Site Contract ID";
            this.colClientContractID.FieldName = "SiteContractID";
            this.colClientContractID.Name = "colClientContractID";
            this.colClientContractID.OptionsColumn.AllowEdit = false;
            this.colClientContractID.OptionsColumn.AllowFocus = false;
            this.colClientContractID.OptionsFilter.AllowAutoFilter = false;
            this.colClientContractID.OptionsFilter.AllowFilter = false;
            this.colClientContractID.Width = 107;
            // 
            // colYearDescription
            // 
            this.colYearDescription.Caption = "Year Description";
            this.colYearDescription.ColumnEdit = this.repositoryItemTextEdit50CharsMax;
            this.colYearDescription.FieldName = "YearDescription";
            this.colYearDescription.Name = "colYearDescription";
            this.colYearDescription.OptionsFilter.AllowAutoFilter = false;
            this.colYearDescription.OptionsFilter.AllowFilter = false;
            this.colYearDescription.Visible = true;
            this.colYearDescription.VisibleIndex = 2;
            this.colYearDescription.Width = 183;
            // 
            // repositoryItemTextEdit50CharsMax
            // 
            this.repositoryItemTextEdit50CharsMax.AutoHeight = false;
            this.repositoryItemTextEdit50CharsMax.MaxLength = 50;
            this.repositoryItemTextEdit50CharsMax.Name = "repositoryItemTextEdit50CharsMax";
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsFilter.AllowAutoFilter = false;
            this.colStartDate.OptionsFilter.AllowFilter = false;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 0;
            this.colStartDate.Width = 130;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Mask.EditMask = "g";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "End Date";
            this.colEndDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsFilter.AllowAutoFilter = false;
            this.colEndDate.OptionsFilter.AllowFilter = false;
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 1;
            this.colEndDate.Width = 113;
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsFilter.AllowAutoFilter = false;
            this.colActive.OptionsFilter.AllowFilter = false;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 5;
            this.colActive.Width = 52;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colClientValue
            // 
            this.colClientValue.Caption = "Client Value";
            this.colClientValue.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colClientValue.FieldName = "ClientValue";
            this.colClientValue.Name = "colClientValue";
            this.colClientValue.OptionsFilter.AllowAutoFilter = false;
            this.colClientValue.OptionsFilter.AllowFilter = false;
            this.colClientValue.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ClientValue", "{0:c2}")});
            this.colClientValue.Visible = true;
            this.colClientValue.VisibleIndex = 4;
            this.colClientValue.Width = 97;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.Mask.EditMask = "c";
            this.repositoryItemSpinEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsFilter.AllowAutoFilter = false;
            this.colRemarks1.OptionsFilter.AllowFilter = false;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 6;
            this.colRemarks1.Width = 105;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colSiteName2
            // 
            this.colSiteName2.Caption = "Site Name";
            this.colSiteName2.FieldName = "SiteName";
            this.colSiteName2.Name = "colSiteName2";
            this.colSiteName2.OptionsColumn.AllowEdit = false;
            this.colSiteName2.OptionsColumn.AllowFocus = false;
            this.colSiteName2.OptionsColumn.ReadOnly = true;
            this.colSiteName2.OptionsFilter.AllowAutoFilter = false;
            this.colSiteName2.OptionsFilter.AllowFilter = false;
            this.colSiteName2.Visible = true;
            this.colSiteName2.VisibleIndex = 7;
            this.colSiteName2.Width = 231;
            // 
            // colYearlyPercentageIncrease2
            // 
            this.colYearlyPercentageIncrease2.Caption = "Yearly % Increase";
            this.colYearlyPercentageIncrease2.ColumnEdit = this.repositoryItemSpinEditPercentage2;
            this.colYearlyPercentageIncrease2.FieldName = "YearlyPercentageIncrease";
            this.colYearlyPercentageIncrease2.Name = "colYearlyPercentageIncrease2";
            this.colYearlyPercentageIncrease2.OptionsFilter.AllowAutoFilter = false;
            this.colYearlyPercentageIncrease2.OptionsFilter.AllowFilter = false;
            this.colYearlyPercentageIncrease2.Visible = true;
            this.colYearlyPercentageIncrease2.VisibleIndex = 3;
            this.colYearlyPercentageIncrease2.Width = 100;
            // 
            // repositoryItemSpinEditPercentage2
            // 
            this.repositoryItemSpinEditPercentage2.AutoHeight = false;
            this.repositoryItemSpinEditPercentage2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditPercentage2.Mask.EditMask = "P";
            this.repositoryItemSpinEditPercentage2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditPercentage2.Name = "repositoryItemSpinEditPercentage2";
            // 
            // spinEditContractYears
            // 
            this.spinEditContractYears.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditContractYears.Location = new System.Drawing.Point(81, 2);
            this.spinEditContractYears.MenuManager = this.barManager1;
            this.spinEditContractYears.Name = "spinEditContractYears";
            this.spinEditContractYears.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditContractYears.Properties.IsFloatValue = false;
            this.spinEditContractYears.Properties.Mask.EditMask = "N00";
            this.spinEditContractYears.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditContractYears.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.spinEditContractYears.Size = new System.Drawing.Size(54, 20);
            this.spinEditContractYears.StyleController = this.layoutControl1;
            this.spinEditContractYears.TabIndex = 20;
            this.spinEditContractYears.EditValueChanged += new System.EventHandler(this.spinEditContractYears_EditValueChanged);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Root";
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.ItemForYearsGrid,
            this.ITemForAddYearsToGridButton,
            this.ItemForCopyClientContractYears,
            this.emptySpaceItem1});
            this.layoutControlGroup4.Name = "Root";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(564, 408);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.spinEditContractYears;
            this.layoutControlItem4.CustomizationFormText = "Contract Years:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(137, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(137, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(137, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "Contract Years:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(76, 13);
            // 
            // ItemForYearsGrid
            // 
            this.ItemForYearsGrid.Control = this.gridControl5;
            this.ItemForYearsGrid.CustomizationFormText = "Years Grid:";
            this.ItemForYearsGrid.Location = new System.Drawing.Point(0, 26);
            this.ItemForYearsGrid.Name = "ItemForYearsGrid";
            this.ItemForYearsGrid.Size = new System.Drawing.Size(564, 382);
            this.ItemForYearsGrid.Text = "Years Grid:";
            this.ItemForYearsGrid.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForYearsGrid.TextVisible = false;
            // 
            // ITemForAddYearsToGridButton
            // 
            this.ITemForAddYearsToGridButton.Control = this.btnAddYearsBtn;
            this.ITemForAddYearsToGridButton.CustomizationFormText = "Add Years To Grid Button";
            this.ITemForAddYearsToGridButton.Location = new System.Drawing.Point(137, 0);
            this.ITemForAddYearsToGridButton.MaxSize = new System.Drawing.Size(120, 26);
            this.ITemForAddYearsToGridButton.MinSize = new System.Drawing.Size(120, 26);
            this.ITemForAddYearsToGridButton.Name = "ITemForAddYearsToGridButton";
            this.ITemForAddYearsToGridButton.Size = new System.Drawing.Size(120, 26);
            this.ITemForAddYearsToGridButton.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ITemForAddYearsToGridButton.Text = "Add Years To Grid Button";
            this.ITemForAddYearsToGridButton.TextSize = new System.Drawing.Size(0, 0);
            this.ITemForAddYearsToGridButton.TextVisible = false;
            // 
            // ItemForCopyClientContractYears
            // 
            this.ItemForCopyClientContractYears.Control = this.CopyClientContractYearsSimpleButton;
            this.ItemForCopyClientContractYears.CustomizationFormText = "Copy Client Contract Years Button";
            this.ItemForCopyClientContractYears.Location = new System.Drawing.Point(257, 0);
            this.ItemForCopyClientContractYears.MaxSize = new System.Drawing.Size(164, 0);
            this.ItemForCopyClientContractYears.MinSize = new System.Drawing.Size(164, 26);
            this.ItemForCopyClientContractYears.Name = "ItemForCopyClientContractYears";
            this.ItemForCopyClientContractYears.Size = new System.Drawing.Size(164, 26);
            this.ItemForCopyClientContractYears.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCopyClientContractYears.Text = "Copy Client Contract Years Button";
            this.ItemForCopyClientContractYears.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForCopyClientContractYears.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(421, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(143, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // btnStep4Next
            // 
            this.btnStep4Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep4Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep4Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep4Next.Location = new System.Drawing.Point(865, 500);
            this.btnStep4Next.Name = "btnStep4Next";
            this.btnStep4Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep4Next.TabIndex = 17;
            this.btnStep4Next.Text = "Next";
            this.btnStep4Next.Click += new System.EventHandler(this.btnStep4Next_Click);
            // 
            // btnStep4Previous
            // 
            this.btnStep4Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep4Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep4Previous.Location = new System.Drawing.Point(771, 500);
            this.btnStep4Previous.Name = "btnStep4Previous";
            this.btnStep4Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep4Previous.TabIndex = 18;
            this.btnStep4Previous.Text = "Previous";
            this.btnStep4Previous.Click += new System.EventHandler(this.btnStep4Previous_Click);
            // 
            // panelControl5
            // 
            this.panelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl5.Controls.Add(this.pictureEdit7);
            this.panelControl5.Controls.Add(this.labelControl4);
            this.panelControl5.Controls.Add(this.labelControl6);
            this.panelControl5.Location = new System.Drawing.Point(7, 6);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(946, 48);
            this.panelControl5.TabIndex = 16;
            // 
            // pictureEdit7
            // 
            this.pictureEdit7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit7.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit7.Location = new System.Drawing.Point(902, 4);
            this.pictureEdit7.MenuManager = this.barManager1;
            this.pictureEdit7.Name = "pictureEdit7";
            this.pictureEdit7.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit7.Properties.ReadOnly = true;
            this.pictureEdit7.Properties.ShowMenu = false;
            this.pictureEdit7.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit7.TabIndex = 9;
            // 
            // labelControl4
            // 
            this.labelControl4.AllowHtmlString = true;
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseBackColor = true;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(5, 5);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(282, 16);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "<b>Step 4:</b> Create Site Contract Year(s) <b>[Optional]</b>\r\n\r\n";
            // 
            // labelControl6
            // 
            this.labelControl6.AllowHtmlString = true;
            this.labelControl6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl6.Appearance.Options.UseBackColor = true;
            this.labelControl6.Location = new System.Drawing.Point(57, 29);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(640, 13);
            this.labelControl6.TabIndex = 7;
            this.labelControl6.Text = "Select each site then enter the number of <b>Contract Years</b> the contract span" +
    "s and click <b>Add Years To Grid</b>. Click Next when done.";
            // 
            // xtraTabPageStep5
            // 
            this.xtraTabPageStep5.Controls.Add(this.btnStep5Next);
            this.xtraTabPageStep5.Controls.Add(this.splitContainerControl1);
            this.xtraTabPageStep5.Controls.Add(this.btnStep5Previous);
            this.xtraTabPageStep5.Controls.Add(this.panelControl6);
            this.xtraTabPageStep5.Name = "xtraTabPageStep5";
            this.xtraTabPageStep5.Size = new System.Drawing.Size(959, 536);
            this.xtraTabPageStep5.Tag = "5";
            this.xtraTabPageStep5.Text = "Step 5";
            // 
            // btnStep5Next
            // 
            this.btnStep5Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep5Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep5Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep5Next.Location = new System.Drawing.Point(865, 500);
            this.btnStep5Next.Name = "btnStep5Next";
            this.btnStep5Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep5Next.TabIndex = 19;
            this.btnStep5Next.Text = "Next";
            this.btnStep5Next.Click += new System.EventHandler(this.btnStep5Next_Click);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.Location = new System.Drawing.Point(7, 62);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl6);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Site Contract Year(s)  [Read Only]";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl7);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Billing Profile";
            this.splitContainerControl1.Size = new System.Drawing.Size(947, 432);
            this.splitContainerControl1.SplitterPosition = 361;
            this.splitContainerControl1.TabIndex = 22;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.sp06092OMSiteContractYearEditBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3,
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditDateTime});
            this.gridControl6.Size = new System.Drawing.Size(357, 408);
            this.gridControl6.TabIndex = 21;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.colCreatedFromClientContractYearID,
            this.colCreatedFromClientContractYear});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.GroupCount = 1;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowFilterEditor = false;
            this.gridView6.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView6.OptionsFilter.AllowMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView6.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView6.OptionsFind.FindDelay = 2000;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsLayout.StoreFormatRules = true;
            this.gridView6.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn18, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn13, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Site Contract Year ID";
            this.gridColumn10.FieldName = "SiteContractYearID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn10.OptionsFilter.AllowFilter = false;
            this.gridColumn10.Width = 132;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Site Contract ID";
            this.gridColumn11.FieldName = "SiteContractID";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn11.OptionsFilter.AllowFilter = false;
            this.gridColumn11.Width = 107;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Year Description";
            this.gridColumn12.FieldName = "YearDescription";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn12.OptionsFilter.AllowFilter = false;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 3;
            this.gridColumn12.Width = 154;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Start Date";
            this.gridColumn13.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.gridColumn13.FieldName = "StartDate";
            this.gridColumn13.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn13.OptionsFilter.AllowFilter = false;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 0;
            this.gridColumn13.Width = 130;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "End Date";
            this.gridColumn14.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.gridColumn14.FieldName = "EndDate";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn14.OptionsFilter.AllowFilter = false;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 1;
            this.gridColumn14.Width = 100;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Active";
            this.gridColumn15.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn15.FieldName = "Active";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn15.OptionsFilter.AllowFilter = false;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 4;
            this.gridColumn15.Width = 52;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Client Value";
            this.gridColumn16.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.gridColumn16.FieldName = "ClientValue";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn16.OptionsFilter.AllowFilter = false;
            this.gridColumn16.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ClientValue", "{0:c2}")});
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 2;
            this.gridColumn16.Width = 81;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Remarks";
            this.gridColumn17.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.gridColumn17.FieldName = "Remarks";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn17.OptionsFilter.AllowFilter = false;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 5;
            this.gridColumn17.Width = 86;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Site Name";
            this.gridColumn18.FieldName = "SiteName";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn18.OptionsFilter.AllowFilter = false;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 6;
            this.gridColumn18.Width = 231;
            // 
            // colCreatedFromClientContractYearID
            // 
            this.colCreatedFromClientContractYearID.Caption = "Created From Client Contract Year ID";
            this.colCreatedFromClientContractYearID.FieldName = "CreatedFromClientContractYearID";
            this.colCreatedFromClientContractYearID.Name = "colCreatedFromClientContractYearID";
            this.colCreatedFromClientContractYearID.OptionsColumn.AllowEdit = false;
            this.colCreatedFromClientContractYearID.OptionsColumn.AllowFocus = false;
            this.colCreatedFromClientContractYearID.OptionsColumn.ReadOnly = true;
            this.colCreatedFromClientContractYearID.Width = 201;
            // 
            // colCreatedFromClientContractYear
            // 
            this.colCreatedFromClientContractYear.Caption = "Created From Client Contract Year";
            this.colCreatedFromClientContractYear.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colCreatedFromClientContractYear.FieldName = "CreatedFromClientContractYear";
            this.colCreatedFromClientContractYear.Name = "colCreatedFromClientContractYear";
            this.colCreatedFromClientContractYear.OptionsColumn.AllowEdit = false;
            this.colCreatedFromClientContractYear.OptionsColumn.AllowFocus = false;
            this.colCreatedFromClientContractYear.OptionsColumn.ReadOnly = true;
            this.colCreatedFromClientContractYear.Visible = true;
            this.colCreatedFromClientContractYear.VisibleIndex = 6;
            this.colCreatedFromClientContractYear.Width = 187;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(577, 26);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // gridControl7
            // 
            this.gridControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl7.DataSource = this.sp06095OMSiteContractYearBillingProfileEditBindingSource;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl7.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Block Edit Selected Records", "block edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl7.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl7_EmbeddedNavigator_ButtonClick);
            this.gridControl7.Location = new System.Drawing.Point(0, 25);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemDateEdit2,
            this.repositoryItemSpinEditCurrency,
            this.repositoryItemCheckEdit7});
            this.gridControl7.Size = new System.Drawing.Size(576, 383);
            this.gridControl7.TabIndex = 22;
            this.gridControl7.UseEmbeddedNavigator = true;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp06095OMSiteContractYearBillingProfileEditBindingSource
            // 
            this.sp06095OMSiteContractYearBillingProfileEditBindingSource.DataMember = "sp06095_OM_Site_Contract_Year_Billing_Profile_Edit";
            this.sp06095OMSiteContractYearBillingProfileEditBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteContractProfileID,
            this.colSiteContractYearID,
            this.colInvoiceDateDue,
            this.colInvoiceDateActual,
            this.colEstimatedBillAmount,
            this.colActualBillAmount,
            this.colBilledByPersonID,
            this.colRemarks2,
            this.colLinkedToParent,
            this.colSiteContractID1,
            this.colSiteName3,
            this.colCreatedFromClientContractProfileID,
            this.colCreatedFromClientContractProfile});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.GroupCount = 2;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView7.OptionsFilter.AllowFilterEditor = false;
            this.gridView7.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView7.OptionsFilter.AllowMRUFilterList = false;
            this.gridView7.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView7.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView7.OptionsFind.FindDelay = 2000;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsLayout.StoreFormatRules = true;
            this.gridView7.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.MultiSelect = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView7.OptionsView.ShowFooter = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInvoiceDateDue, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView7.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView7.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView7_MouseUp);
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            // 
            // colSiteContractProfileID
            // 
            this.colSiteContractProfileID.Caption = "Site Contract Profile ID";
            this.colSiteContractProfileID.FieldName = "SiteContractProfileID";
            this.colSiteContractProfileID.Name = "colSiteContractProfileID";
            this.colSiteContractProfileID.OptionsColumn.AllowEdit = false;
            this.colSiteContractProfileID.OptionsColumn.AllowFocus = false;
            this.colSiteContractProfileID.OptionsColumn.ReadOnly = true;
            this.colSiteContractProfileID.Width = 153;
            // 
            // colSiteContractYearID
            // 
            this.colSiteContractYearID.Caption = "Site Contract Year ID";
            this.colSiteContractYearID.FieldName = "SiteContractYearID";
            this.colSiteContractYearID.Name = "colSiteContractYearID";
            this.colSiteContractYearID.OptionsColumn.AllowEdit = false;
            this.colSiteContractYearID.OptionsColumn.AllowFocus = false;
            this.colSiteContractYearID.OptionsColumn.ReadOnly = true;
            this.colSiteContractYearID.Width = 123;
            // 
            // colInvoiceDateDue
            // 
            this.colInvoiceDateDue.Caption = "Invoice Due Date";
            this.colInvoiceDateDue.ColumnEdit = this.repositoryItemDateEdit2;
            this.colInvoiceDateDue.FieldName = "InvoiceDateDue";
            this.colInvoiceDateDue.Name = "colInvoiceDateDue";
            this.colInvoiceDateDue.Visible = true;
            this.colInvoiceDateDue.VisibleIndex = 0;
            this.colInvoiceDateDue.Width = 154;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.Mask.EditMask = "g";
            this.repositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // colInvoiceDateActual
            // 
            this.colInvoiceDateActual.Caption = "Actual Invoice Date";
            this.colInvoiceDateActual.ColumnEdit = this.repositoryItemDateEdit2;
            this.colInvoiceDateActual.FieldName = "InvoiceDateActual";
            this.colInvoiceDateActual.Name = "colInvoiceDateActual";
            this.colInvoiceDateActual.Visible = true;
            this.colInvoiceDateActual.VisibleIndex = 2;
            this.colInvoiceDateActual.Width = 115;
            // 
            // colEstimatedBillAmount
            // 
            this.colEstimatedBillAmount.Caption = "Estimated Bill Amount";
            this.colEstimatedBillAmount.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colEstimatedBillAmount.FieldName = "EstimatedBillAmount";
            this.colEstimatedBillAmount.Name = "colEstimatedBillAmount";
            this.colEstimatedBillAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "EstimatedBillAmount", "{0:c2}")});
            this.colEstimatedBillAmount.Visible = true;
            this.colEstimatedBillAmount.VisibleIndex = 1;
            this.colEstimatedBillAmount.Width = 123;
            // 
            // repositoryItemSpinEditCurrency
            // 
            this.repositoryItemSpinEditCurrency.AutoHeight = false;
            this.repositoryItemSpinEditCurrency.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditCurrency.Mask.EditMask = "c";
            this.repositoryItemSpinEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditCurrency.Name = "repositoryItemSpinEditCurrency";
            // 
            // colActualBillAmount
            // 
            this.colActualBillAmount.Caption = "Actual Bill Amount";
            this.colActualBillAmount.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colActualBillAmount.FieldName = "ActualBillAmount";
            this.colActualBillAmount.Name = "colActualBillAmount";
            this.colActualBillAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ActualBillAmount", "{0:c2}")});
            this.colActualBillAmount.Visible = true;
            this.colActualBillAmount.VisibleIndex = 3;
            this.colActualBillAmount.Width = 106;
            // 
            // colBilledByPersonID
            // 
            this.colBilledByPersonID.Caption = "Billed By Person ID";
            this.colBilledByPersonID.FieldName = "BilledByPersonID";
            this.colBilledByPersonID.Name = "colBilledByPersonID";
            this.colBilledByPersonID.OptionsColumn.AllowEdit = false;
            this.colBilledByPersonID.OptionsColumn.AllowFocus = false;
            this.colBilledByPersonID.OptionsColumn.ReadOnly = true;
            this.colBilledByPersonID.Width = 110;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 4;
            this.colRemarks2.Width = 92;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colLinkedToParent
            // 
            this.colLinkedToParent.Caption = "Site Year";
            this.colLinkedToParent.FieldName = "LinkedToParent";
            this.colLinkedToParent.Name = "colLinkedToParent";
            this.colLinkedToParent.Visible = true;
            this.colLinkedToParent.VisibleIndex = 5;
            this.colLinkedToParent.Width = 327;
            // 
            // colSiteContractID1
            // 
            this.colSiteContractID1.Caption = "Site Contract ID";
            this.colSiteContractID1.FieldName = "SiteContractID";
            this.colSiteContractID1.Name = "colSiteContractID1";
            this.colSiteContractID1.OptionsColumn.AllowEdit = false;
            this.colSiteContractID1.OptionsColumn.AllowFocus = false;
            this.colSiteContractID1.OptionsColumn.ReadOnly = true;
            this.colSiteContractID1.Width = 98;
            // 
            // colSiteName3
            // 
            this.colSiteName3.Caption = "Site Name";
            this.colSiteName3.FieldName = "SiteName";
            this.colSiteName3.Name = "colSiteName3";
            this.colSiteName3.OptionsColumn.AllowEdit = false;
            this.colSiteName3.OptionsColumn.AllowFocus = false;
            this.colSiteName3.OptionsColumn.ReadOnly = true;
            this.colSiteName3.Visible = true;
            this.colSiteName3.VisibleIndex = 7;
            this.colSiteName3.Width = 259;
            // 
            // colCreatedFromClientContractProfileID
            // 
            this.colCreatedFromClientContractProfileID.Caption = "Created From Client Contract Profile ID";
            this.colCreatedFromClientContractProfileID.FieldName = "CreatedFromClientContractProfileID";
            this.colCreatedFromClientContractProfileID.Name = "colCreatedFromClientContractProfileID";
            this.colCreatedFromClientContractProfileID.OptionsColumn.AllowEdit = false;
            this.colCreatedFromClientContractProfileID.OptionsColumn.AllowFocus = false;
            this.colCreatedFromClientContractProfileID.OptionsColumn.ReadOnly = true;
            this.colCreatedFromClientContractProfileID.Width = 209;
            // 
            // colCreatedFromClientContractProfile
            // 
            this.colCreatedFromClientContractProfile.Caption = "Created From Client Contract Profile";
            this.colCreatedFromClientContractProfile.ColumnEdit = this.repositoryItemCheckEdit7;
            this.colCreatedFromClientContractProfile.FieldName = "CreatedFromClientContractProfile";
            this.colCreatedFromClientContractProfile.Name = "colCreatedFromClientContractProfile";
            this.colCreatedFromClientContractProfile.OptionsColumn.AllowEdit = false;
            this.colCreatedFromClientContractProfile.OptionsColumn.AllowFocus = false;
            this.colCreatedFromClientContractProfile.OptionsColumn.ReadOnly = true;
            this.colCreatedFromClientContractProfile.Visible = true;
            this.colCreatedFromClientContractProfile.VisibleIndex = 5;
            this.colCreatedFromClientContractProfile.Width = 195;
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Caption = "Check";
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            this.repositoryItemCheckEdit7.ValueChecked = 1;
            this.repositoryItemCheckEdit7.ValueUnchecked = 0;
            // 
            // btnStep5Previous
            // 
            this.btnStep5Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep5Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep5Previous.Location = new System.Drawing.Point(771, 500);
            this.btnStep5Previous.Name = "btnStep5Previous";
            this.btnStep5Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep5Previous.TabIndex = 20;
            this.btnStep5Previous.Text = "Previous";
            this.btnStep5Previous.Click += new System.EventHandler(this.btnStep5Previous_Click);
            // 
            // panelControl6
            // 
            this.panelControl6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl6.Controls.Add(this.pictureEdit8);
            this.panelControl6.Controls.Add(this.labelControl13);
            this.panelControl6.Controls.Add(this.labelControl14);
            this.panelControl6.Location = new System.Drawing.Point(7, 6);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(946, 48);
            this.panelControl6.TabIndex = 17;
            // 
            // pictureEdit8
            // 
            this.pictureEdit8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit8.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit8.Location = new System.Drawing.Point(902, 4);
            this.pictureEdit8.MenuManager = this.barManager1;
            this.pictureEdit8.Name = "pictureEdit8";
            this.pictureEdit8.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit8.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit8.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit8.Properties.ReadOnly = true;
            this.pictureEdit8.Properties.ShowMenu = false;
            this.pictureEdit8.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit8.TabIndex = 9;
            // 
            // labelControl13
            // 
            this.labelControl13.AllowHtmlString = true;
            this.labelControl13.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.Options.UseBackColor = true;
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Location = new System.Drawing.Point(5, 5);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(251, 16);
            this.labelControl13.TabIndex = 6;
            this.labelControl13.Text = "<b>Step 5:</b> Create Billing Profile(s) <b>[Optional]</b>\r\n\r\n";
            // 
            // labelControl14
            // 
            this.labelControl14.AllowHtmlString = true;
            this.labelControl14.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl14.Appearance.Options.UseBackColor = true;
            this.labelControl14.Location = new System.Drawing.Point(57, 29);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(617, 13);
            this.labelControl14.TabIndex = 7;
            this.labelControl14.Text = "Select each Site Contract Year by clicking on it then set up the Billing Profile " +
    "in the in the Billing Profile grid. Click Next when done.";
            // 
            // xtraTabPageStep6
            // 
            this.xtraTabPageStep6.Controls.Add(this.splitContainerControl3);
            this.xtraTabPageStep6.Controls.Add(this.btnStep6Next);
            this.xtraTabPageStep6.Controls.Add(this.btnStep6Previous);
            this.xtraTabPageStep6.Controls.Add(this.panelControl11);
            this.xtraTabPageStep6.Name = "xtraTabPageStep6";
            this.xtraTabPageStep6.Size = new System.Drawing.Size(959, 536);
            this.xtraTabPageStep6.Tag = "6";
            this.xtraTabPageStep6.Text = "Step 6";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl3.Location = new System.Drawing.Point(7, 63);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl3.Panel1.Controls.Add(this.gridControl9);
            this.splitContainerControl3.Panel1.ShowCaption = true;
            this.splitContainerControl3.Panel1.Text = "Sites  [Read Only]";
            this.splitContainerControl3.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl3.Panel2.Controls.Add(this.standaloneBarDockControl4);
            this.splitContainerControl3.Panel2.Controls.Add(this.gridControl10);
            this.splitContainerControl3.Panel2.ShowCaption = true;
            this.splitContainerControl3.Panel2.Text = "Site Preferred Labour - Linked to Selected Sites";
            this.splitContainerControl3.Size = new System.Drawing.Size(946, 431);
            this.splitContainerControl3.SplitterPosition = 349;
            this.splitContainerControl3.TabIndex = 26;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // gridControl9
            // 
            this.gridControl9.DataSource = this.sp06090OMSiteContractEditBindingSource;
            this.gridControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl9.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl9.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl9.Location = new System.Drawing.Point(0, 0);
            this.gridControl9.MainView = this.gridView9;
            this.gridControl9.MenuManager = this.barManager1;
            this.gridControl9.Name = "gridControl9";
            this.gridControl9.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit8,
            this.repositoryItemMemoExEdit10,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit3,
            this.repositoryItemTextEditInteger2});
            this.gridControl9.Size = new System.Drawing.Size(345, 407);
            this.gridControl9.TabIndex = 24;
            this.gridControl9.UseEmbeddedNavigator = true;
            this.gridControl9.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView9});
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.colSitePostcode1,
            this.colLocationX1,
            this.colLocationY1,
            this.colDummyEquipmentCount,
            this.colDummyLabourCount,
            this.colDummyMaterialCount,
            this.colDummyPersonResponsibilityCount});
            this.gridView9.GridControl = this.gridControl9;
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView9.OptionsFilter.AllowFilterEditor = false;
            this.gridView9.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView9.OptionsFilter.AllowMRUFilterList = false;
            this.gridView9.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView9.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView9.OptionsFind.FindDelay = 2000;
            this.gridView9.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView9.OptionsLayout.StoreAppearance = true;
            this.gridView9.OptionsLayout.StoreFormatRules = true;
            this.gridView9.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView9.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView9.OptionsSelection.MultiSelect = true;
            this.gridView9.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView9.OptionsView.ColumnAutoWidth = false;
            this.gridView9.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView9.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            this.gridView9.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn37, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView9.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView9_CustomDrawCell);
            this.gridView9.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView9.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView9.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView9.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView9.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView9_MouseUp);
            this.gridView9.GotFocus += new System.EventHandler(this.gridView9_GotFocus);
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Site Contract ID";
            this.gridColumn19.FieldName = "SiteContractID";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Width = 98;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Client Contract ID";
            this.gridColumn20.FieldName = "ClientContractID";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Width = 107;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Site ID";
            this.gridColumn26.FieldName = "SiteID";
            this.gridColumn26.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 0;
            this.gridColumn26.Width = 51;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Start Date";
            this.gridColumn27.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn27.FieldName = "StartDate";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 4;
            this.gridColumn27.Width = 120;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "g";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "End Date";
            this.gridColumn28.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn28.FieldName = "EndDate";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 5;
            this.gridColumn28.Width = 120;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Active";
            this.gridColumn29.ColumnEdit = this.repositoryItemCheckEdit8;
            this.gridColumn29.FieldName = "Active";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 6;
            this.gridColumn29.Width = 51;
            // 
            // repositoryItemCheckEdit8
            // 
            this.repositoryItemCheckEdit8.AutoHeight = false;
            this.repositoryItemCheckEdit8.Caption = "Check";
            this.repositoryItemCheckEdit8.Name = "repositoryItemCheckEdit8";
            this.repositoryItemCheckEdit8.ValueChecked = 1;
            this.repositoryItemCheckEdit8.ValueUnchecked = 0;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Contract Value";
            this.gridColumn30.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn30.FieldName = "ContractValue";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ContractValue", "{0:c2}")});
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 3;
            this.gridColumn30.Width = 92;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "c";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Last Client Payment Date";
            this.gridColumn31.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn31.FieldName = "LastClientPaymentDate";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn31.Width = 142;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Remarks";
            this.gridColumn32.ColumnEdit = this.repositoryItemMemoExEdit10;
            this.gridColumn32.FieldName = "Remarks";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 9;
            // 
            // repositoryItemMemoExEdit10
            // 
            this.repositoryItemMemoExEdit10.AutoHeight = false;
            this.repositoryItemMemoExEdit10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit10.Name = "repositoryItemMemoExEdit10";
            this.repositoryItemMemoExEdit10.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit10.ShowIcon = false;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Yearly % Increase";
            this.gridColumn33.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColumn33.FieldName = "YearlyPercentageIncrease";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 7;
            this.gridColumn33.Width = 110;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "P";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Dummy Site Contract ID";
            this.gridColumn34.FieldName = "DummySiteContractID";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn34.Width = 136;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Linked To Parent";
            this.gridColumn35.FieldName = "LinkedToParent";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Width = 101;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Client Name";
            this.gridColumn36.FieldName = "ClientName";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Width = 172;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Site Name";
            this.gridColumn37.FieldName = "SiteName";
            this.gridColumn37.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 1;
            this.gridColumn37.Width = 234;
            // 
            // colSitePostcode1
            // 
            this.colSitePostcode1.Caption = "Site Postcode";
            this.colSitePostcode1.FieldName = "SitePostcode";
            this.colSitePostcode1.Name = "colSitePostcode1";
            this.colSitePostcode1.OptionsColumn.AllowEdit = false;
            this.colSitePostcode1.OptionsColumn.AllowFocus = false;
            this.colSitePostcode1.OptionsColumn.ReadOnly = true;
            this.colSitePostcode1.Visible = true;
            this.colSitePostcode1.VisibleIndex = 2;
            this.colSitePostcode1.Width = 86;
            // 
            // colLocationX1
            // 
            this.colLocationX1.Caption = "Site Latitude";
            this.colLocationX1.FieldName = "LocationX";
            this.colLocationX1.Name = "colLocationX1";
            this.colLocationX1.OptionsColumn.AllowEdit = false;
            this.colLocationX1.OptionsColumn.AllowFocus = false;
            this.colLocationX1.OptionsColumn.ReadOnly = true;
            this.colLocationX1.Width = 81;
            // 
            // colLocationY1
            // 
            this.colLocationY1.Caption = "Site Longitude";
            this.colLocationY1.FieldName = "LocationY";
            this.colLocationY1.Name = "colLocationY1";
            this.colLocationY1.OptionsColumn.AllowEdit = false;
            this.colLocationY1.OptionsColumn.AllowFocus = false;
            this.colLocationY1.OptionsColumn.ReadOnly = true;
            this.colLocationY1.Width = 89;
            // 
            // colDummyEquipmentCount
            // 
            this.colDummyEquipmentCount.Caption = "Equipment Count";
            this.colDummyEquipmentCount.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colDummyEquipmentCount.FieldName = "DummyEquipmentCount";
            this.colDummyEquipmentCount.Name = "colDummyEquipmentCount";
            this.colDummyEquipmentCount.OptionsColumn.AllowEdit = false;
            this.colDummyEquipmentCount.OptionsColumn.AllowFocus = false;
            this.colDummyEquipmentCount.OptionsColumn.ReadOnly = true;
            this.colDummyEquipmentCount.Width = 103;
            // 
            // repositoryItemTextEditInteger2
            // 
            this.repositoryItemTextEditInteger2.AutoHeight = false;
            this.repositoryItemTextEditInteger2.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger2.Name = "repositoryItemTextEditInteger2";
            // 
            // colDummyLabourCount
            // 
            this.colDummyLabourCount.Caption = "Labour Count";
            this.colDummyLabourCount.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colDummyLabourCount.FieldName = "DummyLabourCount";
            this.colDummyLabourCount.Name = "colDummyLabourCount";
            this.colDummyLabourCount.OptionsColumn.AllowEdit = false;
            this.colDummyLabourCount.OptionsColumn.AllowFocus = false;
            this.colDummyLabourCount.OptionsColumn.ReadOnly = true;
            this.colDummyLabourCount.Visible = true;
            this.colDummyLabourCount.VisibleIndex = 8;
            this.colDummyLabourCount.Width = 86;
            // 
            // colDummyMaterialCount
            // 
            this.colDummyMaterialCount.Caption = "Material Count";
            this.colDummyMaterialCount.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colDummyMaterialCount.FieldName = "DummyMaterialCount";
            this.colDummyMaterialCount.Name = "colDummyMaterialCount";
            this.colDummyMaterialCount.OptionsColumn.AllowEdit = false;
            this.colDummyMaterialCount.OptionsColumn.AllowFocus = false;
            this.colDummyMaterialCount.OptionsColumn.ReadOnly = true;
            this.colDummyMaterialCount.Width = 91;
            // 
            // colDummyPersonResponsibilityCount
            // 
            this.colDummyPersonResponsibilityCount.Caption = "Responsibility Count";
            this.colDummyPersonResponsibilityCount.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colDummyPersonResponsibilityCount.FieldName = "DummyPersonResponsibilityCount";
            this.colDummyPersonResponsibilityCount.Name = "colDummyPersonResponsibilityCount";
            this.colDummyPersonResponsibilityCount.OptionsColumn.AllowEdit = false;
            this.colDummyPersonResponsibilityCount.OptionsColumn.AllowFocus = false;
            this.colDummyPersonResponsibilityCount.OptionsColumn.ReadOnly = true;
            this.colDummyPersonResponsibilityCount.Width = 118;
            // 
            // standaloneBarDockControl4
            // 
            this.standaloneBarDockControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl4.CausesValidation = false;
            this.standaloneBarDockControl4.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl4.Manager = this.barManager1;
            this.standaloneBarDockControl4.Name = "standaloneBarDockControl4";
            this.standaloneBarDockControl4.Size = new System.Drawing.Size(586, 25);
            this.standaloneBarDockControl4.Text = "standaloneBarDockControl4";
            // 
            // gridControl10
            // 
            this.gridControl10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl10.DataSource = this.sp06098OMSiteContractLabourCostEditBindingSource;
            this.gridControl10.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl10.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl10.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Block Edit Selected Records", "block edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl10.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl10_EmbeddedNavigator_ButtonClick);
            this.gridControl10.Location = new System.Drawing.Point(0, 25);
            this.gridControl10.MainView = this.gridView10;
            this.gridControl10.MenuManager = this.barManager1;
            this.gridControl10.Name = "gridControl10";
            this.gridControl10.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemGridLookUpEditUnitDescriptor,
            this.repositoryItemSpinEditMoney,
            this.repositoryItemSpinEditPercent,
            this.repositoryItemSpinEditMiles,
            this.repositoryItemMemoExEdit9,
            this.repositoryItemButtonEditChooseContractor,
            this.repositoryItemButtonEditLabourType});
            this.gridControl10.Size = new System.Drawing.Size(587, 382);
            this.gridControl10.TabIndex = 24;
            this.gridControl10.UseEmbeddedNavigator = true;
            this.gridControl10.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView10});
            // 
            // sp06098OMSiteContractLabourCostEditBindingSource
            // 
            this.sp06098OMSiteContractLabourCostEditBindingSource.DataMember = "sp06098_OM_Site_Contract_Labour_Cost_Edit";
            this.sp06098OMSiteContractLabourCostEditBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteContractLabourCostID,
            this.colSiteContractID2,
            this.colContractorID,
            this.colCostUnitDescriptorID,
            this.colSellUnitDescriptorID,
            this.colCostPerUnitExVat,
            this.colCostPerUnitVatRate,
            this.colSellPerUnitExVat,
            this.colSellPerUnitVatRate,
            this.colCISPercentage,
            this.colCISValue,
            this.colPostcodeSiteDistance,
            this.colLatLongSiteDistance,
            this.colRemarks6,
            this.colClientName3,
            this.colContractStartDate1,
            this.colContractEndDate1,
            this.colSiteName4,
            this.colContractorName,
            this.colLinkedToPersonType,
            this.colLinkedToPersonTypeID});
            this.gridView10.GridControl = this.gridControl10;
            this.gridView10.GroupCount = 1;
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView10.OptionsFilter.AllowFilterEditor = false;
            this.gridView10.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView10.OptionsFilter.AllowMRUFilterList = false;
            this.gridView10.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView10.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView10.OptionsFind.FindDelay = 2000;
            this.gridView10.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView10.OptionsLayout.StoreAppearance = true;
            this.gridView10.OptionsLayout.StoreFormatRules = true;
            this.gridView10.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView10.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView10.OptionsSelection.MultiSelect = true;
            this.gridView10.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView10.OptionsView.ColumnAutoWidth = false;
            this.gridView10.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView10.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            this.gridView10.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName4, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToPersonType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView10.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView10.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView10.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView10.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView10_MouseUp);
            this.gridView10.GotFocus += new System.EventHandler(this.gridView10_GotFocus);
            // 
            // colSiteContractLabourCostID
            // 
            this.colSiteContractLabourCostID.Caption = "Site Contract Labour Cost ID";
            this.colSiteContractLabourCostID.FieldName = "SiteContractLabourCostID";
            this.colSiteContractLabourCostID.Name = "colSiteContractLabourCostID";
            this.colSiteContractLabourCostID.OptionsColumn.AllowEdit = false;
            this.colSiteContractLabourCostID.OptionsColumn.AllowFocus = false;
            this.colSiteContractLabourCostID.OptionsColumn.ReadOnly = true;
            this.colSiteContractLabourCostID.Width = 159;
            // 
            // colSiteContractID2
            // 
            this.colSiteContractID2.Caption = "Site Contract ID";
            this.colSiteContractID2.FieldName = "SiteContractID";
            this.colSiteContractID2.Name = "colSiteContractID2";
            this.colSiteContractID2.OptionsColumn.AllowEdit = false;
            this.colSiteContractID2.OptionsColumn.AllowFocus = false;
            this.colSiteContractID2.OptionsColumn.ReadOnly = true;
            this.colSiteContractID2.Width = 98;
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            // 
            // colCostUnitDescriptorID
            // 
            this.colCostUnitDescriptorID.Caption = "Cost Unit Descriptor";
            this.colCostUnitDescriptorID.ColumnEdit = this.repositoryItemGridLookUpEditUnitDescriptor;
            this.colCostUnitDescriptorID.FieldName = "CostUnitDescriptorID";
            this.colCostUnitDescriptorID.Name = "colCostUnitDescriptorID";
            this.colCostUnitDescriptorID.Visible = true;
            this.colCostUnitDescriptorID.VisibleIndex = 2;
            this.colCostUnitDescriptorID.Width = 117;
            // 
            // repositoryItemGridLookUpEditUnitDescriptor
            // 
            this.repositoryItemGridLookUpEditUnitDescriptor.AutoHeight = false;
            this.repositoryItemGridLookUpEditUnitDescriptor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditUnitDescriptor.DataSource = this.sp06101OMWorkUnitTypesPicklistBindingSource;
            this.repositoryItemGridLookUpEditUnitDescriptor.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditUnitDescriptor.Name = "repositoryItemGridLookUpEditUnitDescriptor";
            this.repositoryItemGridLookUpEditUnitDescriptor.NullText = "";
            this.repositoryItemGridLookUpEditUnitDescriptor.PopupView = this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor;
            this.repositoryItemGridLookUpEditUnitDescriptor.ValueMember = "ID";
            // 
            // sp06101OMWorkUnitTypesPicklistBindingSource
            // 
            this.sp06101OMWorkUnitTypesPicklistBindingSource.DataMember = "sp06101_OM_Work_Unit_Types_Picklist";
            this.sp06101OMWorkUnitTypesPicklistBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // repositoryItemGridLookUpEditGridViewCostUnitDescriptor
            // 
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40});
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn38;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.Name = "repositoryItemGridLookUpEditGridViewCostUnitDescriptor";
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn40, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Gender";
            this.gridColumn39.FieldName = "Description";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 0;
            this.gridColumn39.Width = 220;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Unit Descriptor";
            this.gridColumn40.FieldName = "RecordOrder";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            // 
            // colSellUnitDescriptorID
            // 
            this.colSellUnitDescriptorID.Caption = "Sell Unit Descriptor";
            this.colSellUnitDescriptorID.ColumnEdit = this.repositoryItemGridLookUpEditUnitDescriptor;
            this.colSellUnitDescriptorID.FieldName = "SellUnitDescriptorID";
            this.colSellUnitDescriptorID.Name = "colSellUnitDescriptorID";
            this.colSellUnitDescriptorID.Visible = true;
            this.colSellUnitDescriptorID.VisibleIndex = 5;
            this.colSellUnitDescriptorID.Width = 111;
            // 
            // colCostPerUnitExVat
            // 
            this.colCostPerUnitExVat.Caption = "Cost Per Unit Ex VAT";
            this.colCostPerUnitExVat.ColumnEdit = this.repositoryItemSpinEditMoney;
            this.colCostPerUnitExVat.FieldName = "CostPerUnitExVat";
            this.colCostPerUnitExVat.Name = "colCostPerUnitExVat";
            this.colCostPerUnitExVat.Visible = true;
            this.colCostPerUnitExVat.VisibleIndex = 3;
            this.colCostPerUnitExVat.Width = 121;
            // 
            // repositoryItemSpinEditMoney
            // 
            this.repositoryItemSpinEditMoney.AutoHeight = false;
            this.repositoryItemSpinEditMoney.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditMoney.Mask.EditMask = "c";
            this.repositoryItemSpinEditMoney.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditMoney.Name = "repositoryItemSpinEditMoney";
            // 
            // colCostPerUnitVatRate
            // 
            this.colCostPerUnitVatRate.Caption = "Cost Per Unit VAT Rate";
            this.colCostPerUnitVatRate.ColumnEdit = this.repositoryItemSpinEditPercent;
            this.colCostPerUnitVatRate.FieldName = "CostPerUnitVatRate";
            this.colCostPerUnitVatRate.Name = "colCostPerUnitVatRate";
            this.colCostPerUnitVatRate.Visible = true;
            this.colCostPerUnitVatRate.VisibleIndex = 4;
            this.colCostPerUnitVatRate.Width = 132;
            // 
            // repositoryItemSpinEditPercent
            // 
            this.repositoryItemSpinEditPercent.AutoHeight = false;
            this.repositoryItemSpinEditPercent.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditPercent.Mask.EditMask = "P";
            this.repositoryItemSpinEditPercent.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditPercent.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            131072});
            this.repositoryItemSpinEditPercent.Name = "repositoryItemSpinEditPercent";
            // 
            // colSellPerUnitExVat
            // 
            this.colSellPerUnitExVat.Caption = "Sell Per Unit Ex VAT";
            this.colSellPerUnitExVat.ColumnEdit = this.repositoryItemSpinEditMoney;
            this.colSellPerUnitExVat.FieldName = "SellPerUnitExVat";
            this.colSellPerUnitExVat.Name = "colSellPerUnitExVat";
            this.colSellPerUnitExVat.Visible = true;
            this.colSellPerUnitExVat.VisibleIndex = 6;
            this.colSellPerUnitExVat.Width = 115;
            // 
            // colSellPerUnitVatRate
            // 
            this.colSellPerUnitVatRate.Caption = "Sell Per Unit VAT Rate";
            this.colSellPerUnitVatRate.ColumnEdit = this.repositoryItemSpinEditPercent;
            this.colSellPerUnitVatRate.FieldName = "SellPerUnitVatRate";
            this.colSellPerUnitVatRate.Name = "colSellPerUnitVatRate";
            this.colSellPerUnitVatRate.Visible = true;
            this.colSellPerUnitVatRate.VisibleIndex = 7;
            this.colSellPerUnitVatRate.Width = 126;
            // 
            // colCISPercentage
            // 
            this.colCISPercentage.Caption = "CIS %";
            this.colCISPercentage.ColumnEdit = this.repositoryItemSpinEditPercent;
            this.colCISPercentage.FieldName = "CISPercentage";
            this.colCISPercentage.Name = "colCISPercentage";
            this.colCISPercentage.Visible = true;
            this.colCISPercentage.VisibleIndex = 8;
            // 
            // colCISValue
            // 
            this.colCISValue.Caption = "CIS Value";
            this.colCISValue.ColumnEdit = this.repositoryItemSpinEditMoney;
            this.colCISValue.FieldName = "CISValue";
            this.colCISValue.Name = "colCISValue";
            this.colCISValue.Visible = true;
            this.colCISValue.VisibleIndex = 9;
            this.colCISValue.Width = 81;
            // 
            // colPostcodeSiteDistance
            // 
            this.colPostcodeSiteDistance.Caption = "Distance From Site Postcode";
            this.colPostcodeSiteDistance.ColumnEdit = this.repositoryItemSpinEditMiles;
            this.colPostcodeSiteDistance.FieldName = "PostcodeSiteDistance";
            this.colPostcodeSiteDistance.Name = "colPostcodeSiteDistance";
            this.colPostcodeSiteDistance.Visible = true;
            this.colPostcodeSiteDistance.VisibleIndex = 10;
            this.colPostcodeSiteDistance.Width = 157;
            // 
            // repositoryItemSpinEditMiles
            // 
            this.repositoryItemSpinEditMiles.AutoHeight = false;
            this.repositoryItemSpinEditMiles.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditMiles.Mask.EditMask = "########0.00 Miles";
            this.repositoryItemSpinEditMiles.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditMiles.Name = "repositoryItemSpinEditMiles";
            // 
            // colLatLongSiteDistance
            // 
            this.colLatLongSiteDistance.Caption = "Distance From Site Lat\\Long";
            this.colLatLongSiteDistance.ColumnEdit = this.repositoryItemSpinEditMiles;
            this.colLatLongSiteDistance.FieldName = "LatLongSiteDistance";
            this.colLatLongSiteDistance.Name = "colLatLongSiteDistance";
            this.colLatLongSiteDistance.Visible = true;
            this.colLatLongSiteDistance.VisibleIndex = 11;
            this.colLatLongSiteDistance.Width = 155;
            // 
            // colRemarks6
            // 
            this.colRemarks6.Caption = "Remarks";
            this.colRemarks6.ColumnEdit = this.repositoryItemMemoExEdit9;
            this.colRemarks6.FieldName = "Remarks";
            this.colRemarks6.Name = "colRemarks6";
            this.colRemarks6.Visible = true;
            this.colRemarks6.VisibleIndex = 12;
            // 
            // repositoryItemMemoExEdit9
            // 
            this.repositoryItemMemoExEdit9.AutoHeight = false;
            this.repositoryItemMemoExEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit9.Name = "repositoryItemMemoExEdit9";
            this.repositoryItemMemoExEdit9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit9.ShowIcon = false;
            // 
            // colClientName3
            // 
            this.colClientName3.Caption = "Client Name";
            this.colClientName3.FieldName = "ClientName";
            this.colClientName3.Name = "colClientName3";
            this.colClientName3.OptionsColumn.AllowEdit = false;
            this.colClientName3.OptionsColumn.AllowFocus = false;
            this.colClientName3.OptionsColumn.ReadOnly = true;
            this.colClientName3.Width = 166;
            // 
            // colContractStartDate1
            // 
            this.colContractStartDate1.Caption = "Contract Start";
            this.colContractStartDate1.FieldName = "ContractStartDate";
            this.colContractStartDate1.Name = "colContractStartDate1";
            this.colContractStartDate1.OptionsColumn.AllowEdit = false;
            this.colContractStartDate1.OptionsColumn.AllowFocus = false;
            this.colContractStartDate1.OptionsColumn.ReadOnly = true;
            this.colContractStartDate1.Width = 90;
            // 
            // colContractEndDate1
            // 
            this.colContractEndDate1.Caption = "Contract End";
            this.colContractEndDate1.FieldName = "ContractEndDate";
            this.colContractEndDate1.Name = "colContractEndDate1";
            this.colContractEndDate1.OptionsColumn.AllowEdit = false;
            this.colContractEndDate1.OptionsColumn.AllowFocus = false;
            this.colContractEndDate1.OptionsColumn.ReadOnly = true;
            this.colContractEndDate1.Width = 84;
            // 
            // colSiteName4
            // 
            this.colSiteName4.Caption = "Site Name";
            this.colSiteName4.FieldName = "SiteName";
            this.colSiteName4.Name = "colSiteName4";
            this.colSiteName4.OptionsColumn.AllowEdit = false;
            this.colSiteName4.OptionsColumn.AllowFocus = false;
            this.colSiteName4.OptionsColumn.ReadOnly = true;
            this.colSiteName4.Width = 191;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Labour Name";
            this.colContractorName.ColumnEdit = this.repositoryItemButtonEditChooseContractor;
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 1;
            this.colContractorName.Width = 176;
            // 
            // repositoryItemButtonEditChooseContractor
            // 
            this.repositoryItemButtonEditChooseContractor.AutoHeight = false;
            this.repositoryItemButtonEditChooseContractor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to Open the Select Contractor \\ Staff Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditChooseContractor.Name = "repositoryItemButtonEditChooseContractor";
            this.repositoryItemButtonEditChooseContractor.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditChooseContractor.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditChooseContractor_ButtonClick);
            // 
            // colLinkedToPersonType
            // 
            this.colLinkedToPersonType.Caption = "Labour Type";
            this.colLinkedToPersonType.ColumnEdit = this.repositoryItemButtonEditLabourType;
            this.colLinkedToPersonType.FieldName = "LinkedToPersonType";
            this.colLinkedToPersonType.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colLinkedToPersonType.Name = "colLinkedToPersonType";
            this.colLinkedToPersonType.Visible = true;
            this.colLinkedToPersonType.VisibleIndex = 0;
            this.colLinkedToPersonType.Width = 124;
            // 
            // repositoryItemButtonEditLabourType
            // 
            this.repositoryItemButtonEditLabourType.AutoHeight = false;
            this.repositoryItemButtonEditLabourType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to Open the Select Person Type screen.", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditLabourType.Name = "repositoryItemButtonEditLabourType";
            this.repositoryItemButtonEditLabourType.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditLabourType.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditLabourType_ButtonClick);
            // 
            // colLinkedToPersonTypeID
            // 
            this.colLinkedToPersonTypeID.Caption = "Labour Type ID";
            this.colLinkedToPersonTypeID.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.Name = "colLinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID.Width = 93;
            // 
            // btnStep6Next
            // 
            this.btnStep6Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep6Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep6Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep6Next.Location = new System.Drawing.Point(865, 500);
            this.btnStep6Next.Name = "btnStep6Next";
            this.btnStep6Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep6Next.TabIndex = 19;
            this.btnStep6Next.Text = "Next";
            this.btnStep6Next.Click += new System.EventHandler(this.btnStep6Next_Click);
            // 
            // btnStep6Previous
            // 
            this.btnStep6Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep6Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep6Previous.Location = new System.Drawing.Point(771, 500);
            this.btnStep6Previous.Name = "btnStep6Previous";
            this.btnStep6Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep6Previous.TabIndex = 20;
            this.btnStep6Previous.Text = "Previous";
            this.btnStep6Previous.Click += new System.EventHandler(this.btnStep6Previous_Click);
            // 
            // panelControl11
            // 
            this.panelControl11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl11.Controls.Add(this.pictureEdit13);
            this.panelControl11.Controls.Add(this.labelControl25);
            this.panelControl11.Controls.Add(this.labelControl26);
            this.panelControl11.Location = new System.Drawing.Point(7, 6);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(946, 48);
            this.panelControl11.TabIndex = 18;
            // 
            // pictureEdit13
            // 
            this.pictureEdit13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit13.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit13.Location = new System.Drawing.Point(902, 4);
            this.pictureEdit13.MenuManager = this.barManager1;
            this.pictureEdit13.Name = "pictureEdit13";
            this.pictureEdit13.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit13.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit13.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit13.Properties.ReadOnly = true;
            this.pictureEdit13.Properties.ShowMenu = false;
            this.pictureEdit13.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit13.TabIndex = 9;
            // 
            // labelControl25
            // 
            this.labelControl25.AllowHtmlString = true;
            this.labelControl25.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.Appearance.Options.UseBackColor = true;
            this.labelControl25.Appearance.Options.UseFont = true;
            this.labelControl25.Location = new System.Drawing.Point(5, 5);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(364, 16);
            this.labelControl25.TabIndex = 6;
            this.labelControl25.Text = "<b>Step 6:</b> Create Preferred Labour and Default Rates <b>[Optional]</b>\r\n\r\n";
            // 
            // labelControl26
            // 
            this.labelControl26.AllowHtmlString = true;
            this.labelControl26.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl26.Appearance.Options.UseBackColor = true;
            this.labelControl26.Location = new System.Drawing.Point(57, 29);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(383, 13);
            this.labelControl26.TabIndex = 7;
            this.labelControl26.Text = "Add labour to the Labour Grid by clicking the Add button. Click Next when done.";
            // 
            // xtraTabPageStep7
            // 
            this.xtraTabPageStep7.Controls.Add(this.splitContainerControl4);
            this.xtraTabPageStep7.Controls.Add(this.btnStep7Next);
            this.xtraTabPageStep7.Controls.Add(this.btnStep7Previous);
            this.xtraTabPageStep7.Controls.Add(this.panelControl9);
            this.xtraTabPageStep7.Name = "xtraTabPageStep7";
            this.xtraTabPageStep7.Size = new System.Drawing.Size(959, 536);
            this.xtraTabPageStep7.Tag = "7";
            this.xtraTabPageStep7.Text = "Step 7";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl4.Location = new System.Drawing.Point(7, 63);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl4.Panel1.Controls.Add(this.gridControl11);
            this.splitContainerControl4.Panel1.ShowCaption = true;
            this.splitContainerControl4.Panel1.Text = "Sites  [Read Only]";
            this.splitContainerControl4.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl4.Panel2.Controls.Add(this.gridControl12);
            this.splitContainerControl4.Panel2.Controls.Add(this.standaloneBarDockControl5);
            this.splitContainerControl4.Panel2.ShowCaption = true;
            this.splitContainerControl4.Panel2.Text = "Site Default Equipment Rates - Linked to Selected Sites";
            this.splitContainerControl4.Size = new System.Drawing.Size(946, 431);
            this.splitContainerControl4.SplitterPosition = 349;
            this.splitContainerControl4.TabIndex = 22;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // gridControl11
            // 
            this.gridControl11.DataSource = this.sp06090OMSiteContractEditBindingSource;
            this.gridControl11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl11.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl11.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl11.Location = new System.Drawing.Point(0, 0);
            this.gridControl11.MainView = this.gridView11;
            this.gridControl11.MenuManager = this.barManager1;
            this.gridControl11.Name = "gridControl11";
            this.gridControl11.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit9,
            this.repositoryItemMemoExEdit11,
            this.repositoryItemTextEdit4,
            this.repositoryItemTextEdit5,
            this.repositoryItemTextEdit6,
            this.repositoryItemTextEditInteger3});
            this.gridControl11.Size = new System.Drawing.Size(345, 407);
            this.gridControl11.TabIndex = 25;
            this.gridControl11.UseEmbeddedNavigator = true;
            this.gridControl11.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView11});
            // 
            // gridView11
            // 
            this.gridView11.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn41,
            this.gridColumn42,
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48,
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51,
            this.gridColumn52,
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn55,
            this.gridColumn56,
            this.gridColumn57,
            this.colDummyEquipmentCount1,
            this.colDummyLabourCount1,
            this.colDummyMaterialCount1,
            this.colDummyPersonResponsibilityCount1});
            this.gridView11.GridControl = this.gridControl11;
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView11.OptionsFilter.AllowFilterEditor = false;
            this.gridView11.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView11.OptionsFilter.AllowMRUFilterList = false;
            this.gridView11.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView11.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView11.OptionsFind.FindDelay = 2000;
            this.gridView11.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView11.OptionsLayout.StoreAppearance = true;
            this.gridView11.OptionsLayout.StoreFormatRules = true;
            this.gridView11.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView11.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView11.OptionsSelection.MultiSelect = true;
            this.gridView11.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView11.OptionsView.ColumnAutoWidth = false;
            this.gridView11.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView11.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            this.gridView11.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn54, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView11.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView11_CustomDrawCell);
            this.gridView11.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView11.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView11.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView11.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView11.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView11.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView11_MouseUp);
            this.gridView11.GotFocus += new System.EventHandler(this.gridView11_GotFocus);
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Site Contract ID";
            this.gridColumn41.FieldName = "SiteContractID";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Width = 98;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Client Contract ID";
            this.gridColumn42.FieldName = "ClientContractID";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Width = 107;
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Site ID";
            this.gridColumn43.FieldName = "SiteID";
            this.gridColumn43.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsColumn.AllowFocus = false;
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 0;
            this.gridColumn43.Width = 51;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Start Date";
            this.gridColumn44.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn44.FieldName = "StartDate";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.AllowFocus = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            this.gridColumn44.Visible = true;
            this.gridColumn44.VisibleIndex = 4;
            this.gridColumn44.Width = 120;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "g";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "End Date";
            this.gridColumn45.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn45.FieldName = "EndDate";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.AllowFocus = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.Visible = true;
            this.gridColumn45.VisibleIndex = 5;
            this.gridColumn45.Width = 120;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Active";
            this.gridColumn46.ColumnEdit = this.repositoryItemCheckEdit9;
            this.gridColumn46.FieldName = "Active";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.Visible = true;
            this.gridColumn46.VisibleIndex = 6;
            this.gridColumn46.Width = 51;
            // 
            // repositoryItemCheckEdit9
            // 
            this.repositoryItemCheckEdit9.AutoHeight = false;
            this.repositoryItemCheckEdit9.Caption = "Check";
            this.repositoryItemCheckEdit9.Name = "repositoryItemCheckEdit9";
            this.repositoryItemCheckEdit9.ValueChecked = 1;
            this.repositoryItemCheckEdit9.ValueUnchecked = 0;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Contract Value";
            this.gridColumn47.ColumnEdit = this.repositoryItemTextEdit5;
            this.gridColumn47.FieldName = "ContractValue";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.AllowEdit = false;
            this.gridColumn47.OptionsColumn.AllowFocus = false;
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ContractValue", "{0:c2}")});
            this.gridColumn47.Visible = true;
            this.gridColumn47.VisibleIndex = 3;
            this.gridColumn47.Width = 92;
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.Mask.EditMask = "c";
            this.repositoryItemTextEdit5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Last Client Payment Date";
            this.gridColumn48.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn48.FieldName = "LastClientPaymentDate";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsColumn.AllowFocus = false;
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn48.Width = 142;
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Remarks";
            this.gridColumn49.ColumnEdit = this.repositoryItemMemoExEdit11;
            this.gridColumn49.FieldName = "Remarks";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.Visible = true;
            this.gridColumn49.VisibleIndex = 9;
            // 
            // repositoryItemMemoExEdit11
            // 
            this.repositoryItemMemoExEdit11.AutoHeight = false;
            this.repositoryItemMemoExEdit11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit11.Name = "repositoryItemMemoExEdit11";
            this.repositoryItemMemoExEdit11.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit11.ShowIcon = false;
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "Yearly % Increase";
            this.gridColumn50.ColumnEdit = this.repositoryItemTextEdit6;
            this.gridColumn50.FieldName = "YearlyPercentageIncrease";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.AllowEdit = false;
            this.gridColumn50.OptionsColumn.AllowFocus = false;
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            this.gridColumn50.Visible = true;
            this.gridColumn50.VisibleIndex = 7;
            this.gridColumn50.Width = 110;
            // 
            // repositoryItemTextEdit6
            // 
            this.repositoryItemTextEdit6.AutoHeight = false;
            this.repositoryItemTextEdit6.Mask.EditMask = "P";
            this.repositoryItemTextEdit6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit6.Name = "repositoryItemTextEdit6";
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "Dummy Site Contract ID";
            this.gridColumn51.FieldName = "DummySiteContractID";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.OptionsColumn.AllowFocus = false;
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn51.Width = 136;
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Linked To Parent";
            this.gridColumn52.FieldName = "LinkedToParent";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.AllowEdit = false;
            this.gridColumn52.OptionsColumn.AllowFocus = false;
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            this.gridColumn52.Width = 101;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "Client Name";
            this.gridColumn53.FieldName = "ClientName";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.AllowEdit = false;
            this.gridColumn53.OptionsColumn.AllowFocus = false;
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.Width = 172;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Site Name";
            this.gridColumn54.FieldName = "SiteName";
            this.gridColumn54.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.AllowEdit = false;
            this.gridColumn54.OptionsColumn.AllowFocus = false;
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            this.gridColumn54.Visible = true;
            this.gridColumn54.VisibleIndex = 1;
            this.gridColumn54.Width = 234;
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "Site Postcode";
            this.gridColumn55.FieldName = "SitePostcode";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.AllowEdit = false;
            this.gridColumn55.OptionsColumn.AllowFocus = false;
            this.gridColumn55.OptionsColumn.ReadOnly = true;
            this.gridColumn55.Visible = true;
            this.gridColumn55.VisibleIndex = 2;
            this.gridColumn55.Width = 86;
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "Site Latitude";
            this.gridColumn56.FieldName = "LocationX";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.OptionsColumn.AllowEdit = false;
            this.gridColumn56.OptionsColumn.AllowFocus = false;
            this.gridColumn56.OptionsColumn.ReadOnly = true;
            this.gridColumn56.Width = 81;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Site Longitude";
            this.gridColumn57.FieldName = "LocationY";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.AllowEdit = false;
            this.gridColumn57.OptionsColumn.AllowFocus = false;
            this.gridColumn57.OptionsColumn.ReadOnly = true;
            this.gridColumn57.Width = 89;
            // 
            // colDummyEquipmentCount1
            // 
            this.colDummyEquipmentCount1.Caption = "Equipment Count";
            this.colDummyEquipmentCount1.ColumnEdit = this.repositoryItemTextEditInteger3;
            this.colDummyEquipmentCount1.FieldName = "DummyEquipmentCount";
            this.colDummyEquipmentCount1.Name = "colDummyEquipmentCount1";
            this.colDummyEquipmentCount1.OptionsColumn.AllowEdit = false;
            this.colDummyEquipmentCount1.OptionsColumn.AllowFocus = false;
            this.colDummyEquipmentCount1.OptionsColumn.ReadOnly = true;
            this.colDummyEquipmentCount1.Visible = true;
            this.colDummyEquipmentCount1.VisibleIndex = 8;
            this.colDummyEquipmentCount1.Width = 103;
            // 
            // repositoryItemTextEditInteger3
            // 
            this.repositoryItemTextEditInteger3.AutoHeight = false;
            this.repositoryItemTextEditInteger3.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger3.Name = "repositoryItemTextEditInteger3";
            // 
            // colDummyLabourCount1
            // 
            this.colDummyLabourCount1.Caption = "Labour Count";
            this.colDummyLabourCount1.ColumnEdit = this.repositoryItemTextEditInteger3;
            this.colDummyLabourCount1.FieldName = "DummyLabourCount";
            this.colDummyLabourCount1.Name = "colDummyLabourCount1";
            this.colDummyLabourCount1.OptionsColumn.AllowEdit = false;
            this.colDummyLabourCount1.OptionsColumn.AllowFocus = false;
            this.colDummyLabourCount1.OptionsColumn.ReadOnly = true;
            this.colDummyLabourCount1.Width = 86;
            // 
            // colDummyMaterialCount1
            // 
            this.colDummyMaterialCount1.Caption = "Material Count";
            this.colDummyMaterialCount1.ColumnEdit = this.repositoryItemTextEditInteger3;
            this.colDummyMaterialCount1.FieldName = "DummyMaterialCount";
            this.colDummyMaterialCount1.Name = "colDummyMaterialCount1";
            this.colDummyMaterialCount1.OptionsColumn.AllowEdit = false;
            this.colDummyMaterialCount1.OptionsColumn.AllowFocus = false;
            this.colDummyMaterialCount1.OptionsColumn.ReadOnly = true;
            this.colDummyMaterialCount1.Width = 91;
            // 
            // colDummyPersonResponsibilityCount1
            // 
            this.colDummyPersonResponsibilityCount1.Caption = "Responsibility Count";
            this.colDummyPersonResponsibilityCount1.ColumnEdit = this.repositoryItemTextEditInteger3;
            this.colDummyPersonResponsibilityCount1.FieldName = "DummyPersonResponsibilityCount";
            this.colDummyPersonResponsibilityCount1.Name = "colDummyPersonResponsibilityCount1";
            this.colDummyPersonResponsibilityCount1.OptionsColumn.AllowEdit = false;
            this.colDummyPersonResponsibilityCount1.OptionsColumn.AllowFocus = false;
            this.colDummyPersonResponsibilityCount1.OptionsColumn.ReadOnly = true;
            this.colDummyPersonResponsibilityCount1.Width = 118;
            // 
            // gridControl12
            // 
            this.gridControl12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl12.DataSource = this.sp06103OMSiteContractEquipmentCostEditBindingSource;
            this.gridControl12.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl12.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl12.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Block Edit Selected Records", "block edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl12.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl12_EmbeddedNavigator_ButtonClick);
            this.gridControl12.Location = new System.Drawing.Point(0, 25);
            this.gridControl12.MainView = this.gridView12;
            this.gridControl12.MenuManager = this.barManager1;
            this.gridControl12.Name = "gridControl12";
            this.gridControl12.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit12,
            this.repositoryItemSpinEditCurrency3,
            this.repositoryItemGridLookUpEditCostUnitDescriptors,
            this.repositoryItemSpinEditPercentage3,
            this.repositoryItemButtonEditChooseEquipment});
            this.gridControl12.Size = new System.Drawing.Size(587, 382);
            this.gridControl12.TabIndex = 23;
            this.gridControl12.UseEmbeddedNavigator = true;
            this.gridControl12.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView12});
            // 
            // sp06103OMSiteContractEquipmentCostEditBindingSource
            // 
            this.sp06103OMSiteContractEquipmentCostEditBindingSource.DataMember = "sp06103_OM_Site_Contract_Equipment_Cost_Edit";
            this.sp06103OMSiteContractEquipmentCostEditBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView12
            // 
            this.gridView12.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn58,
            this.gridColumn59,
            this.gridColumn60,
            this.colCostUnitDescriptorID2,
            this.gridColumn62,
            this.gridColumn63,
            this.gridColumn64,
            this.gridColumn65,
            this.gridColumn66,
            this.gridColumn71,
            this.gridColumn72,
            this.gridColumn73,
            this.gridColumn74,
            this.gridColumn75,
            this.gridColumn76,
            this.colOwnerName,
            this.colOwnerType});
            this.gridView12.GridControl = this.gridControl12;
            this.gridView12.GroupCount = 1;
            this.gridView12.Name = "gridView12";
            this.gridView12.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView12.OptionsFilter.AllowFilterEditor = false;
            this.gridView12.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView12.OptionsFilter.AllowMRUFilterList = false;
            this.gridView12.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView12.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView12.OptionsFind.FindDelay = 2000;
            this.gridView12.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView12.OptionsLayout.StoreAppearance = true;
            this.gridView12.OptionsLayout.StoreFormatRules = true;
            this.gridView12.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView12.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView12.OptionsSelection.MultiSelect = true;
            this.gridView12.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView12.OptionsView.ColumnAutoWidth = false;
            this.gridView12.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView12.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView12.OptionsView.ShowGroupPanel = false;
            this.gridView12.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn75, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn76, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView12.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView12.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView12.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView12.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView12_MouseUp);
            this.gridView12.GotFocus += new System.EventHandler(this.gridView12_GotFocus);
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Site Contract Equipment Cost ID";
            this.gridColumn58.FieldName = "SiteContractEquipmentCostID";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.AllowEdit = false;
            this.gridColumn58.OptionsColumn.AllowFocus = false;
            this.gridColumn58.OptionsColumn.ReadOnly = true;
            this.gridColumn58.Width = 159;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "Site Contract ID";
            this.gridColumn59.FieldName = "SiteContractID";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.AllowEdit = false;
            this.gridColumn59.OptionsColumn.AllowFocus = false;
            this.gridColumn59.OptionsColumn.ReadOnly = true;
            this.gridColumn59.Width = 98;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "Equipment ID";
            this.gridColumn60.FieldName = "EquipmentID";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.AllowEdit = false;
            this.gridColumn60.OptionsColumn.AllowFocus = false;
            this.gridColumn60.OptionsColumn.ReadOnly = true;
            // 
            // colCostUnitDescriptorID2
            // 
            this.colCostUnitDescriptorID2.Caption = "Cost Unit Descriptor";
            this.colCostUnitDescriptorID2.ColumnEdit = this.repositoryItemGridLookUpEditCostUnitDescriptors;
            this.colCostUnitDescriptorID2.FieldName = "CostUnitDescriptorID";
            this.colCostUnitDescriptorID2.Name = "colCostUnitDescriptorID2";
            this.colCostUnitDescriptorID2.Visible = true;
            this.colCostUnitDescriptorID2.VisibleIndex = 3;
            this.colCostUnitDescriptorID2.Width = 117;
            // 
            // repositoryItemGridLookUpEditCostUnitDescriptors
            // 
            this.repositoryItemGridLookUpEditCostUnitDescriptors.AutoHeight = false;
            this.repositoryItemGridLookUpEditCostUnitDescriptors.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditCostUnitDescriptors.DataSource = this.sp06101OMWorkUnitTypesPicklistBindingSource;
            this.repositoryItemGridLookUpEditCostUnitDescriptors.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditCostUnitDescriptors.Name = "repositoryItemGridLookUpEditCostUnitDescriptors";
            this.repositoryItemGridLookUpEditCostUnitDescriptors.PopupView = this.repositoryItemGridLookUpEdit2View;
            this.repositoryItemGridLookUpEditCostUnitDescriptors.ValueMember = "ID";
            // 
            // repositoryItemGridLookUpEdit2View
            // 
            this.repositoryItemGridLookUpEdit2View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.repositoryItemGridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.colID1;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.repositoryItemGridLookUpEdit2View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.repositoryItemGridLookUpEdit2View.Name = "repositoryItemGridLookUpEdit2View";
            this.repositoryItemGridLookUpEdit2View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit2View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit2View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit2View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Unit Descriptor";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "RecordOrder";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "Sell Unit Descriptor";
            this.gridColumn62.ColumnEdit = this.repositoryItemGridLookUpEditCostUnitDescriptors;
            this.gridColumn62.FieldName = "SellUnitDescriptorID";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.Visible = true;
            this.gridColumn62.VisibleIndex = 6;
            this.gridColumn62.Width = 111;
            // 
            // gridColumn63
            // 
            this.gridColumn63.Caption = "Cost Per Unit Ex VAT";
            this.gridColumn63.ColumnEdit = this.repositoryItemSpinEditCurrency3;
            this.gridColumn63.FieldName = "CostPerUnitExVat";
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.Visible = true;
            this.gridColumn63.VisibleIndex = 4;
            this.gridColumn63.Width = 121;
            // 
            // repositoryItemSpinEditCurrency3
            // 
            this.repositoryItemSpinEditCurrency3.AutoHeight = false;
            this.repositoryItemSpinEditCurrency3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditCurrency3.Mask.EditMask = "c";
            this.repositoryItemSpinEditCurrency3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditCurrency3.Name = "repositoryItemSpinEditCurrency3";
            // 
            // gridColumn64
            // 
            this.gridColumn64.Caption = "Cost Per Unit VAT Rate";
            this.gridColumn64.ColumnEdit = this.repositoryItemSpinEditPercentage3;
            this.gridColumn64.FieldName = "CostPerUnitVatRate";
            this.gridColumn64.Name = "gridColumn64";
            this.gridColumn64.Visible = true;
            this.gridColumn64.VisibleIndex = 5;
            this.gridColumn64.Width = 132;
            // 
            // repositoryItemSpinEditPercentage3
            // 
            this.repositoryItemSpinEditPercentage3.AutoHeight = false;
            this.repositoryItemSpinEditPercentage3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditPercentage3.Mask.EditMask = "P";
            this.repositoryItemSpinEditPercentage3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditPercentage3.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            131072});
            this.repositoryItemSpinEditPercentage3.Name = "repositoryItemSpinEditPercentage3";
            // 
            // gridColumn65
            // 
            this.gridColumn65.Caption = "Sell Per Unit Ex VAT";
            this.gridColumn65.ColumnEdit = this.repositoryItemSpinEditCurrency3;
            this.gridColumn65.FieldName = "SellPerUnitExVat";
            this.gridColumn65.Name = "gridColumn65";
            this.gridColumn65.Visible = true;
            this.gridColumn65.VisibleIndex = 7;
            this.gridColumn65.Width = 115;
            // 
            // gridColumn66
            // 
            this.gridColumn66.Caption = "Sell Per Unit VAT Rate";
            this.gridColumn66.ColumnEdit = this.repositoryItemSpinEditPercentage3;
            this.gridColumn66.FieldName = "SellPerUnitVatRate";
            this.gridColumn66.Name = "gridColumn66";
            this.gridColumn66.Visible = true;
            this.gridColumn66.VisibleIndex = 8;
            this.gridColumn66.Width = 126;
            // 
            // gridColumn71
            // 
            this.gridColumn71.Caption = "Remarks";
            this.gridColumn71.ColumnEdit = this.repositoryItemMemoExEdit12;
            this.gridColumn71.FieldName = "Remarks";
            this.gridColumn71.Name = "gridColumn71";
            this.gridColumn71.Visible = true;
            this.gridColumn71.VisibleIndex = 9;
            // 
            // repositoryItemMemoExEdit12
            // 
            this.repositoryItemMemoExEdit12.AutoHeight = false;
            this.repositoryItemMemoExEdit12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit12.Name = "repositoryItemMemoExEdit12";
            this.repositoryItemMemoExEdit12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit12.ShowIcon = false;
            // 
            // gridColumn72
            // 
            this.gridColumn72.Caption = "Client Name";
            this.gridColumn72.FieldName = "ClientName";
            this.gridColumn72.Name = "gridColumn72";
            this.gridColumn72.OptionsColumn.AllowEdit = false;
            this.gridColumn72.OptionsColumn.AllowFocus = false;
            this.gridColumn72.OptionsColumn.ReadOnly = true;
            this.gridColumn72.Width = 166;
            // 
            // gridColumn73
            // 
            this.gridColumn73.Caption = "Contract Start";
            this.gridColumn73.FieldName = "ContractStartDate";
            this.gridColumn73.Name = "gridColumn73";
            this.gridColumn73.OptionsColumn.AllowEdit = false;
            this.gridColumn73.OptionsColumn.AllowFocus = false;
            this.gridColumn73.OptionsColumn.ReadOnly = true;
            this.gridColumn73.Width = 90;
            // 
            // gridColumn74
            // 
            this.gridColumn74.Caption = "Contract End";
            this.gridColumn74.FieldName = "ContractEndDate";
            this.gridColumn74.Name = "gridColumn74";
            this.gridColumn74.OptionsColumn.AllowEdit = false;
            this.gridColumn74.OptionsColumn.AllowFocus = false;
            this.gridColumn74.OptionsColumn.ReadOnly = true;
            this.gridColumn74.Width = 84;
            // 
            // gridColumn75
            // 
            this.gridColumn75.Caption = "Site Name";
            this.gridColumn75.FieldName = "SiteName";
            this.gridColumn75.Name = "gridColumn75";
            this.gridColumn75.OptionsColumn.AllowEdit = false;
            this.gridColumn75.OptionsColumn.AllowFocus = false;
            this.gridColumn75.OptionsColumn.ReadOnly = true;
            this.gridColumn75.Width = 191;
            // 
            // gridColumn76
            // 
            this.gridColumn76.Caption = "Equipment Type";
            this.gridColumn76.ColumnEdit = this.repositoryItemButtonEditChooseEquipment;
            this.gridColumn76.FieldName = "EquipmentType";
            this.gridColumn76.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn76.Name = "gridColumn76";
            this.gridColumn76.Visible = true;
            this.gridColumn76.VisibleIndex = 0;
            this.gridColumn76.Width = 176;
            // 
            // repositoryItemButtonEditChooseEquipment
            // 
            this.repositoryItemButtonEditChooseEquipment.AutoHeight = false;
            this.repositoryItemButtonEditChooseEquipment.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click me to open the Select Equipment Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditChooseEquipment.Name = "repositoryItemButtonEditChooseEquipment";
            this.repositoryItemButtonEditChooseEquipment.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditChooseEquipment_ButtonClick);
            // 
            // colOwnerName
            // 
            this.colOwnerName.Caption = "Owner";
            this.colOwnerName.FieldName = "OwnerName";
            this.colOwnerName.Name = "colOwnerName";
            this.colOwnerName.OptionsColumn.AllowEdit = false;
            this.colOwnerName.OptionsColumn.AllowFocus = false;
            this.colOwnerName.OptionsColumn.ReadOnly = true;
            this.colOwnerName.Visible = true;
            this.colOwnerName.VisibleIndex = 1;
            this.colOwnerName.Width = 117;
            // 
            // colOwnerType
            // 
            this.colOwnerType.Caption = "Owner Type";
            this.colOwnerType.FieldName = "OwnerType";
            this.colOwnerType.Name = "colOwnerType";
            this.colOwnerType.OptionsColumn.AllowEdit = false;
            this.colOwnerType.OptionsColumn.AllowFocus = false;
            this.colOwnerType.OptionsColumn.ReadOnly = true;
            this.colOwnerType.Visible = true;
            this.colOwnerType.VisibleIndex = 2;
            this.colOwnerType.Width = 80;
            // 
            // standaloneBarDockControl5
            // 
            this.standaloneBarDockControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl5.CausesValidation = false;
            this.standaloneBarDockControl5.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl5.Manager = this.barManager1;
            this.standaloneBarDockControl5.Name = "standaloneBarDockControl5";
            this.standaloneBarDockControl5.Size = new System.Drawing.Size(587, 25);
            this.standaloneBarDockControl5.Text = "standaloneBarDockControl5";
            // 
            // btnStep7Next
            // 
            this.btnStep7Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep7Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep7Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep7Next.Location = new System.Drawing.Point(865, 500);
            this.btnStep7Next.Name = "btnStep7Next";
            this.btnStep7Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep7Next.TabIndex = 20;
            this.btnStep7Next.Text = "Next";
            this.btnStep7Next.Click += new System.EventHandler(this.btnStep7Next_Click);
            // 
            // btnStep7Previous
            // 
            this.btnStep7Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep7Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep7Previous.Location = new System.Drawing.Point(771, 500);
            this.btnStep7Previous.Name = "btnStep7Previous";
            this.btnStep7Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep7Previous.TabIndex = 21;
            this.btnStep7Previous.Text = "Previous";
            this.btnStep7Previous.Click += new System.EventHandler(this.btnStep7Previous_Click);
            // 
            // panelControl9
            // 
            this.panelControl9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl9.Controls.Add(this.pictureEdit11);
            this.panelControl9.Controls.Add(this.labelControl21);
            this.panelControl9.Controls.Add(this.labelControl22);
            this.panelControl9.Location = new System.Drawing.Point(7, 6);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(946, 48);
            this.panelControl9.TabIndex = 19;
            // 
            // pictureEdit11
            // 
            this.pictureEdit11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit11.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit11.Location = new System.Drawing.Point(902, 4);
            this.pictureEdit11.MenuManager = this.barManager1;
            this.pictureEdit11.Name = "pictureEdit11";
            this.pictureEdit11.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit11.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit11.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit11.Properties.ReadOnly = true;
            this.pictureEdit11.Properties.ShowMenu = false;
            this.pictureEdit11.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit11.TabIndex = 9;
            // 
            // labelControl21
            // 
            this.labelControl21.AllowHtmlString = true;
            this.labelControl21.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl21.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Appearance.Options.UseBackColor = true;
            this.labelControl21.Appearance.Options.UseFont = true;
            this.labelControl21.Location = new System.Drawing.Point(5, 5);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(302, 16);
            this.labelControl21.TabIndex = 6;
            this.labelControl21.Text = "<b>Step 7:</b> Create Default Equipment Rates <b>[Optional]</b>";
            // 
            // labelControl22
            // 
            this.labelControl22.AllowHtmlString = true;
            this.labelControl22.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl22.Appearance.Options.UseBackColor = true;
            this.labelControl22.Location = new System.Drawing.Point(57, 29);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(420, 13);
            this.labelControl22.TabIndex = 7;
            this.labelControl22.Text = "Add equipment to the Equipment Grid by clicking the Add button. Click Next when d" +
    "one.";
            // 
            // xtraTabPageStep8
            // 
            this.xtraTabPageStep8.Controls.Add(this.splitContainerControl5);
            this.xtraTabPageStep8.Controls.Add(this.btnStep8Next);
            this.xtraTabPageStep8.Controls.Add(this.btnStep8Previous);
            this.xtraTabPageStep8.Controls.Add(this.panelControl10);
            this.xtraTabPageStep8.Name = "xtraTabPageStep8";
            this.xtraTabPageStep8.Size = new System.Drawing.Size(959, 536);
            this.xtraTabPageStep8.Tag = "8";
            this.xtraTabPageStep8.Text = "Step 8";
            // 
            // splitContainerControl5
            // 
            this.splitContainerControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl5.Location = new System.Drawing.Point(7, 63);
            this.splitContainerControl5.Name = "splitContainerControl5";
            this.splitContainerControl5.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl5.Panel1.Controls.Add(this.gridControl13);
            this.splitContainerControl5.Panel1.ShowCaption = true;
            this.splitContainerControl5.Panel1.Text = "Sites  [Read Only]";
            this.splitContainerControl5.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl5.Panel2.Controls.Add(this.gridControl14);
            this.splitContainerControl5.Panel2.Controls.Add(this.standaloneBarDockControl6);
            this.splitContainerControl5.Panel2.ShowCaption = true;
            this.splitContainerControl5.Panel2.Text = "Site Default Material Rates - Linked to Selected Sites";
            this.splitContainerControl5.Size = new System.Drawing.Size(946, 431);
            this.splitContainerControl5.SplitterPosition = 349;
            this.splitContainerControl5.TabIndex = 22;
            this.splitContainerControl5.Text = "splitContainerControl5";
            // 
            // gridControl13
            // 
            this.gridControl13.DataSource = this.sp06090OMSiteContractEditBindingSource;
            this.gridControl13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl13.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl13.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl13.Location = new System.Drawing.Point(0, 0);
            this.gridControl13.MainView = this.gridView13;
            this.gridControl13.MenuManager = this.barManager1;
            this.gridControl13.Name = "gridControl13";
            this.gridControl13.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit10,
            this.repositoryItemMemoExEdit13,
            this.repositoryItemTextEdit7,
            this.repositoryItemTextEdit8,
            this.repositoryItemTextEdit9,
            this.repositoryItemTextEditInteger4});
            this.gridControl13.Size = new System.Drawing.Size(345, 407);
            this.gridControl13.TabIndex = 26;
            this.gridControl13.UseEmbeddedNavigator = true;
            this.gridControl13.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView13});
            // 
            // gridView13
            // 
            this.gridView13.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn61,
            this.gridColumn67,
            this.gridColumn68,
            this.gridColumn69,
            this.gridColumn70,
            this.gridColumn77,
            this.gridColumn78,
            this.gridColumn79,
            this.gridColumn80,
            this.gridColumn81,
            this.gridColumn82,
            this.gridColumn83,
            this.gridColumn84,
            this.gridColumn85,
            this.gridColumn86,
            this.gridColumn87,
            this.gridColumn88,
            this.colDummyEquipmentCount2,
            this.colDummyLabourCount2,
            this.colDummyMaterialCount2,
            this.colDummyPersonResponsibilityCount2});
            this.gridView13.GridControl = this.gridControl13;
            this.gridView13.Name = "gridView13";
            this.gridView13.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView13.OptionsFilter.AllowFilterEditor = false;
            this.gridView13.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView13.OptionsFilter.AllowMRUFilterList = false;
            this.gridView13.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView13.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView13.OptionsFind.FindDelay = 2000;
            this.gridView13.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView13.OptionsLayout.StoreAppearance = true;
            this.gridView13.OptionsLayout.StoreFormatRules = true;
            this.gridView13.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView13.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView13.OptionsSelection.MultiSelect = true;
            this.gridView13.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView13.OptionsView.ColumnAutoWidth = false;
            this.gridView13.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView13.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView13.OptionsView.ShowGroupPanel = false;
            this.gridView13.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn85, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView13.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView13_CustomDrawCell);
            this.gridView13.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView13.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView13.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView13.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView13.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView13.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView13_MouseUp);
            this.gridView13.GotFocus += new System.EventHandler(this.gridView13_GotFocus);
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "Site Contract ID";
            this.gridColumn61.FieldName = "SiteContractID";
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.OptionsColumn.AllowEdit = false;
            this.gridColumn61.OptionsColumn.AllowFocus = false;
            this.gridColumn61.OptionsColumn.ReadOnly = true;
            this.gridColumn61.Width = 98;
            // 
            // gridColumn67
            // 
            this.gridColumn67.Caption = "Client Contract ID";
            this.gridColumn67.FieldName = "ClientContractID";
            this.gridColumn67.Name = "gridColumn67";
            this.gridColumn67.OptionsColumn.AllowEdit = false;
            this.gridColumn67.OptionsColumn.AllowFocus = false;
            this.gridColumn67.OptionsColumn.ReadOnly = true;
            this.gridColumn67.Width = 107;
            // 
            // gridColumn68
            // 
            this.gridColumn68.Caption = "Site ID";
            this.gridColumn68.FieldName = "SiteID";
            this.gridColumn68.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn68.Name = "gridColumn68";
            this.gridColumn68.OptionsColumn.AllowEdit = false;
            this.gridColumn68.OptionsColumn.AllowFocus = false;
            this.gridColumn68.OptionsColumn.ReadOnly = true;
            this.gridColumn68.Visible = true;
            this.gridColumn68.VisibleIndex = 0;
            this.gridColumn68.Width = 51;
            // 
            // gridColumn69
            // 
            this.gridColumn69.Caption = "Start Date";
            this.gridColumn69.ColumnEdit = this.repositoryItemTextEdit7;
            this.gridColumn69.FieldName = "StartDate";
            this.gridColumn69.Name = "gridColumn69";
            this.gridColumn69.OptionsColumn.AllowEdit = false;
            this.gridColumn69.OptionsColumn.AllowFocus = false;
            this.gridColumn69.OptionsColumn.ReadOnly = true;
            this.gridColumn69.Visible = true;
            this.gridColumn69.VisibleIndex = 4;
            this.gridColumn69.Width = 120;
            // 
            // repositoryItemTextEdit7
            // 
            this.repositoryItemTextEdit7.AutoHeight = false;
            this.repositoryItemTextEdit7.Mask.EditMask = "g";
            this.repositoryItemTextEdit7.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit7.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit7.Name = "repositoryItemTextEdit7";
            // 
            // gridColumn70
            // 
            this.gridColumn70.Caption = "End Date";
            this.gridColumn70.ColumnEdit = this.repositoryItemTextEdit7;
            this.gridColumn70.FieldName = "EndDate";
            this.gridColumn70.Name = "gridColumn70";
            this.gridColumn70.OptionsColumn.AllowEdit = false;
            this.gridColumn70.OptionsColumn.AllowFocus = false;
            this.gridColumn70.OptionsColumn.ReadOnly = true;
            this.gridColumn70.Visible = true;
            this.gridColumn70.VisibleIndex = 5;
            this.gridColumn70.Width = 120;
            // 
            // gridColumn77
            // 
            this.gridColumn77.Caption = "Active";
            this.gridColumn77.ColumnEdit = this.repositoryItemCheckEdit10;
            this.gridColumn77.FieldName = "Active";
            this.gridColumn77.Name = "gridColumn77";
            this.gridColumn77.OptionsColumn.AllowEdit = false;
            this.gridColumn77.OptionsColumn.AllowFocus = false;
            this.gridColumn77.OptionsColumn.ReadOnly = true;
            this.gridColumn77.Visible = true;
            this.gridColumn77.VisibleIndex = 6;
            this.gridColumn77.Width = 51;
            // 
            // repositoryItemCheckEdit10
            // 
            this.repositoryItemCheckEdit10.AutoHeight = false;
            this.repositoryItemCheckEdit10.Caption = "Check";
            this.repositoryItemCheckEdit10.Name = "repositoryItemCheckEdit10";
            this.repositoryItemCheckEdit10.ValueChecked = 1;
            this.repositoryItemCheckEdit10.ValueUnchecked = 0;
            // 
            // gridColumn78
            // 
            this.gridColumn78.Caption = "Contract Value";
            this.gridColumn78.ColumnEdit = this.repositoryItemTextEdit8;
            this.gridColumn78.FieldName = "ContractValue";
            this.gridColumn78.Name = "gridColumn78";
            this.gridColumn78.OptionsColumn.AllowEdit = false;
            this.gridColumn78.OptionsColumn.AllowFocus = false;
            this.gridColumn78.OptionsColumn.ReadOnly = true;
            this.gridColumn78.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ContractValue", "{0:c2}")});
            this.gridColumn78.Visible = true;
            this.gridColumn78.VisibleIndex = 3;
            this.gridColumn78.Width = 92;
            // 
            // repositoryItemTextEdit8
            // 
            this.repositoryItemTextEdit8.AutoHeight = false;
            this.repositoryItemTextEdit8.Mask.EditMask = "c";
            this.repositoryItemTextEdit8.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit8.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit8.Name = "repositoryItemTextEdit8";
            // 
            // gridColumn79
            // 
            this.gridColumn79.Caption = "Last Client Payment Date";
            this.gridColumn79.ColumnEdit = this.repositoryItemTextEdit7;
            this.gridColumn79.FieldName = "LastClientPaymentDate";
            this.gridColumn79.Name = "gridColumn79";
            this.gridColumn79.OptionsColumn.AllowEdit = false;
            this.gridColumn79.OptionsColumn.AllowFocus = false;
            this.gridColumn79.OptionsColumn.ReadOnly = true;
            this.gridColumn79.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn79.Width = 142;
            // 
            // gridColumn80
            // 
            this.gridColumn80.Caption = "Remarks";
            this.gridColumn80.ColumnEdit = this.repositoryItemMemoExEdit13;
            this.gridColumn80.FieldName = "Remarks";
            this.gridColumn80.Name = "gridColumn80";
            this.gridColumn80.OptionsColumn.ReadOnly = true;
            this.gridColumn80.Visible = true;
            this.gridColumn80.VisibleIndex = 9;
            // 
            // repositoryItemMemoExEdit13
            // 
            this.repositoryItemMemoExEdit13.AutoHeight = false;
            this.repositoryItemMemoExEdit13.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit13.Name = "repositoryItemMemoExEdit13";
            this.repositoryItemMemoExEdit13.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit13.ShowIcon = false;
            // 
            // gridColumn81
            // 
            this.gridColumn81.Caption = "Yearly % Increase";
            this.gridColumn81.ColumnEdit = this.repositoryItemTextEdit9;
            this.gridColumn81.FieldName = "YearlyPercentageIncrease";
            this.gridColumn81.Name = "gridColumn81";
            this.gridColumn81.OptionsColumn.AllowEdit = false;
            this.gridColumn81.OptionsColumn.AllowFocus = false;
            this.gridColumn81.OptionsColumn.ReadOnly = true;
            this.gridColumn81.Visible = true;
            this.gridColumn81.VisibleIndex = 7;
            this.gridColumn81.Width = 110;
            // 
            // repositoryItemTextEdit9
            // 
            this.repositoryItemTextEdit9.AutoHeight = false;
            this.repositoryItemTextEdit9.Mask.EditMask = "P";
            this.repositoryItemTextEdit9.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit9.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit9.Name = "repositoryItemTextEdit9";
            // 
            // gridColumn82
            // 
            this.gridColumn82.Caption = "Dummy Site Contract ID";
            this.gridColumn82.FieldName = "DummySiteContractID";
            this.gridColumn82.Name = "gridColumn82";
            this.gridColumn82.OptionsColumn.AllowEdit = false;
            this.gridColumn82.OptionsColumn.AllowFocus = false;
            this.gridColumn82.OptionsColumn.ReadOnly = true;
            this.gridColumn82.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn82.Width = 136;
            // 
            // gridColumn83
            // 
            this.gridColumn83.Caption = "Linked To Parent";
            this.gridColumn83.FieldName = "LinkedToParent";
            this.gridColumn83.Name = "gridColumn83";
            this.gridColumn83.OptionsColumn.AllowEdit = false;
            this.gridColumn83.OptionsColumn.AllowFocus = false;
            this.gridColumn83.OptionsColumn.ReadOnly = true;
            this.gridColumn83.Width = 101;
            // 
            // gridColumn84
            // 
            this.gridColumn84.Caption = "Client Name";
            this.gridColumn84.FieldName = "ClientName";
            this.gridColumn84.Name = "gridColumn84";
            this.gridColumn84.OptionsColumn.AllowEdit = false;
            this.gridColumn84.OptionsColumn.AllowFocus = false;
            this.gridColumn84.OptionsColumn.ReadOnly = true;
            this.gridColumn84.Width = 172;
            // 
            // gridColumn85
            // 
            this.gridColumn85.Caption = "Site Name";
            this.gridColumn85.FieldName = "SiteName";
            this.gridColumn85.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn85.Name = "gridColumn85";
            this.gridColumn85.OptionsColumn.AllowEdit = false;
            this.gridColumn85.OptionsColumn.AllowFocus = false;
            this.gridColumn85.OptionsColumn.ReadOnly = true;
            this.gridColumn85.Visible = true;
            this.gridColumn85.VisibleIndex = 1;
            this.gridColumn85.Width = 234;
            // 
            // gridColumn86
            // 
            this.gridColumn86.Caption = "Site Postcode";
            this.gridColumn86.FieldName = "SitePostcode";
            this.gridColumn86.Name = "gridColumn86";
            this.gridColumn86.OptionsColumn.AllowEdit = false;
            this.gridColumn86.OptionsColumn.AllowFocus = false;
            this.gridColumn86.OptionsColumn.ReadOnly = true;
            this.gridColumn86.Visible = true;
            this.gridColumn86.VisibleIndex = 2;
            this.gridColumn86.Width = 86;
            // 
            // gridColumn87
            // 
            this.gridColumn87.Caption = "Site Latitude";
            this.gridColumn87.FieldName = "LocationX";
            this.gridColumn87.Name = "gridColumn87";
            this.gridColumn87.OptionsColumn.AllowEdit = false;
            this.gridColumn87.OptionsColumn.AllowFocus = false;
            this.gridColumn87.OptionsColumn.ReadOnly = true;
            this.gridColumn87.Width = 81;
            // 
            // gridColumn88
            // 
            this.gridColumn88.Caption = "Site Longitude";
            this.gridColumn88.FieldName = "LocationY";
            this.gridColumn88.Name = "gridColumn88";
            this.gridColumn88.OptionsColumn.AllowEdit = false;
            this.gridColumn88.OptionsColumn.AllowFocus = false;
            this.gridColumn88.OptionsColumn.ReadOnly = true;
            this.gridColumn88.Width = 89;
            // 
            // colDummyEquipmentCount2
            // 
            this.colDummyEquipmentCount2.Caption = "Equipment Count";
            this.colDummyEquipmentCount2.ColumnEdit = this.repositoryItemTextEditInteger4;
            this.colDummyEquipmentCount2.FieldName = "DummyEquipmentCount";
            this.colDummyEquipmentCount2.Name = "colDummyEquipmentCount2";
            this.colDummyEquipmentCount2.OptionsColumn.AllowEdit = false;
            this.colDummyEquipmentCount2.OptionsColumn.AllowFocus = false;
            this.colDummyEquipmentCount2.OptionsColumn.ReadOnly = true;
            this.colDummyEquipmentCount2.Width = 103;
            // 
            // repositoryItemTextEditInteger4
            // 
            this.repositoryItemTextEditInteger4.AutoHeight = false;
            this.repositoryItemTextEditInteger4.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger4.Name = "repositoryItemTextEditInteger4";
            // 
            // colDummyLabourCount2
            // 
            this.colDummyLabourCount2.Caption = "Labour Count";
            this.colDummyLabourCount2.ColumnEdit = this.repositoryItemTextEditInteger4;
            this.colDummyLabourCount2.FieldName = "DummyLabourCount";
            this.colDummyLabourCount2.Name = "colDummyLabourCount2";
            this.colDummyLabourCount2.OptionsColumn.AllowEdit = false;
            this.colDummyLabourCount2.OptionsColumn.AllowFocus = false;
            this.colDummyLabourCount2.OptionsColumn.ReadOnly = true;
            this.colDummyLabourCount2.Width = 86;
            // 
            // colDummyMaterialCount2
            // 
            this.colDummyMaterialCount2.Caption = "Material Count";
            this.colDummyMaterialCount2.ColumnEdit = this.repositoryItemTextEditInteger4;
            this.colDummyMaterialCount2.FieldName = "DummyMaterialCount";
            this.colDummyMaterialCount2.Name = "colDummyMaterialCount2";
            this.colDummyMaterialCount2.OptionsColumn.AllowEdit = false;
            this.colDummyMaterialCount2.OptionsColumn.AllowFocus = false;
            this.colDummyMaterialCount2.OptionsColumn.ReadOnly = true;
            this.colDummyMaterialCount2.Visible = true;
            this.colDummyMaterialCount2.VisibleIndex = 8;
            this.colDummyMaterialCount2.Width = 91;
            // 
            // colDummyPersonResponsibilityCount2
            // 
            this.colDummyPersonResponsibilityCount2.Caption = "Responsibility Count";
            this.colDummyPersonResponsibilityCount2.ColumnEdit = this.repositoryItemTextEditInteger4;
            this.colDummyPersonResponsibilityCount2.FieldName = "DummyPersonResponsibilityCount";
            this.colDummyPersonResponsibilityCount2.Name = "colDummyPersonResponsibilityCount2";
            this.colDummyPersonResponsibilityCount2.OptionsColumn.AllowEdit = false;
            this.colDummyPersonResponsibilityCount2.OptionsColumn.AllowFocus = false;
            this.colDummyPersonResponsibilityCount2.OptionsColumn.ReadOnly = true;
            this.colDummyPersonResponsibilityCount2.Width = 118;
            // 
            // gridControl14
            // 
            this.gridControl14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl14.DataSource = this.sp06106OMSiteContractMaterialCostEditBindingSource;
            this.gridControl14.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl14.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl14.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Block Edit Selected Records", "block edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl14.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl14_EmbeddedNavigator_ButtonClick);
            this.gridControl14.Location = new System.Drawing.Point(0, 25);
            this.gridControl14.MainView = this.gridView14;
            this.gridControl14.MenuManager = this.barManager1;
            this.gridControl14.Name = "gridControl14";
            this.gridControl14.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit14,
            this.repositoryItemSpinEdit3,
            this.repositoryItemGridLookUpEdit2,
            this.repositoryItemSpinEdit4,
            this.repositoryItemButtonEditChooseMaterial});
            this.gridControl14.Size = new System.Drawing.Size(587, 382);
            this.gridControl14.TabIndex = 24;
            this.gridControl14.UseEmbeddedNavigator = true;
            this.gridControl14.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView14});
            // 
            // sp06106OMSiteContractMaterialCostEditBindingSource
            // 
            this.sp06106OMSiteContractMaterialCostEditBindingSource.DataMember = "sp06106_OM_Site_Contract_Material_Cost_Edit";
            this.sp06106OMSiteContractMaterialCostEditBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView14
            // 
            this.gridView14.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn89,
            this.gridColumn90,
            this.gridColumn91,
            this.colCostUnitDescriptor3,
            this.gridColumn96,
            this.gridColumn97,
            this.gridColumn98,
            this.gridColumn99,
            this.gridColumn100,
            this.gridColumn101,
            this.gridColumn102,
            this.gridColumn103,
            this.gridColumn104,
            this.gridColumn105,
            this.gridColumn106});
            this.gridView14.GridControl = this.gridControl14;
            this.gridView14.GroupCount = 1;
            this.gridView14.Name = "gridView14";
            this.gridView14.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView14.OptionsFilter.AllowFilterEditor = false;
            this.gridView14.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView14.OptionsFilter.AllowMRUFilterList = false;
            this.gridView14.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView14.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView14.OptionsFind.FindDelay = 2000;
            this.gridView14.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView14.OptionsLayout.StoreAppearance = true;
            this.gridView14.OptionsLayout.StoreFormatRules = true;
            this.gridView14.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView14.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView14.OptionsSelection.MultiSelect = true;
            this.gridView14.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView14.OptionsView.ColumnAutoWidth = false;
            this.gridView14.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView14.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView14.OptionsView.ShowGroupPanel = false;
            this.gridView14.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn105, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn106, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView14.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView14.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView14.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView14.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView14.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView14_MouseUp);
            this.gridView14.GotFocus += new System.EventHandler(this.gridView14_GotFocus);
            // 
            // gridColumn89
            // 
            this.gridColumn89.Caption = "Site Contract Material Cost ID";
            this.gridColumn89.FieldName = "SiteContractMaterialCostID";
            this.gridColumn89.Name = "gridColumn89";
            this.gridColumn89.OptionsColumn.AllowEdit = false;
            this.gridColumn89.OptionsColumn.AllowFocus = false;
            this.gridColumn89.OptionsColumn.ReadOnly = true;
            this.gridColumn89.Width = 159;
            // 
            // gridColumn90
            // 
            this.gridColumn90.Caption = "Site Contract ID";
            this.gridColumn90.FieldName = "SiteContractID";
            this.gridColumn90.Name = "gridColumn90";
            this.gridColumn90.OptionsColumn.AllowEdit = false;
            this.gridColumn90.OptionsColumn.AllowFocus = false;
            this.gridColumn90.OptionsColumn.ReadOnly = true;
            this.gridColumn90.Width = 98;
            // 
            // gridColumn91
            // 
            this.gridColumn91.Caption = "Material ID";
            this.gridColumn91.FieldName = "MaterialID";
            this.gridColumn91.Name = "gridColumn91";
            this.gridColumn91.OptionsColumn.AllowEdit = false;
            this.gridColumn91.OptionsColumn.AllowFocus = false;
            this.gridColumn91.OptionsColumn.ReadOnly = true;
            // 
            // colCostUnitDescriptor3
            // 
            this.colCostUnitDescriptor3.Caption = "Cost Unit Descriptor";
            this.colCostUnitDescriptor3.ColumnEdit = this.repositoryItemGridLookUpEdit2;
            this.colCostUnitDescriptor3.FieldName = "CostUnitDescriptorID";
            this.colCostUnitDescriptor3.Name = "colCostUnitDescriptor3";
            this.colCostUnitDescriptor3.Visible = true;
            this.colCostUnitDescriptor3.VisibleIndex = 1;
            this.colCostUnitDescriptor3.Width = 117;
            // 
            // repositoryItemGridLookUpEdit2
            // 
            this.repositoryItemGridLookUpEdit2.AutoHeight = false;
            this.repositoryItemGridLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit2.DataSource = this.sp06108OMUnitDescriptorsPicklistBindingSource;
            this.repositoryItemGridLookUpEdit2.DisplayMember = "Description";
            this.repositoryItemGridLookUpEdit2.Name = "repositoryItemGridLookUpEdit2";
            this.repositoryItemGridLookUpEdit2.PopupView = this.gridView16;
            this.repositoryItemGridLookUpEdit2.ValueMember = "ID";
            // 
            // sp06108OMUnitDescriptorsPicklistBindingSource
            // 
            this.sp06108OMUnitDescriptorsPicklistBindingSource.DataMember = "sp06108_OM_Unit_Descriptors_Picklist";
            this.sp06108OMUnitDescriptorsPicklistBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView16
            // 
            this.gridView16.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn93,
            this.gridColumn94,
            this.gridColumn95});
            this.gridView16.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.gridColumn93;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridView16.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridView16.Name = "gridView16";
            this.gridView16.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView16.OptionsLayout.StoreAppearance = true;
            this.gridView16.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView16.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView16.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView16.OptionsView.ColumnAutoWidth = false;
            this.gridView16.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView16.OptionsView.ShowGroupPanel = false;
            this.gridView16.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView16.OptionsView.ShowIndicator = false;
            this.gridView16.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn95, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn94
            // 
            this.gridColumn94.Caption = "Unit Descriptor";
            this.gridColumn94.FieldName = "Description";
            this.gridColumn94.Name = "gridColumn94";
            this.gridColumn94.OptionsColumn.AllowEdit = false;
            this.gridColumn94.OptionsColumn.AllowFocus = false;
            this.gridColumn94.OptionsColumn.ReadOnly = true;
            this.gridColumn94.Visible = true;
            this.gridColumn94.VisibleIndex = 0;
            this.gridColumn94.Width = 220;
            // 
            // gridColumn95
            // 
            this.gridColumn95.Caption = "Order";
            this.gridColumn95.FieldName = "RecordOrder";
            this.gridColumn95.Name = "gridColumn95";
            this.gridColumn95.OptionsColumn.AllowEdit = false;
            this.gridColumn95.OptionsColumn.AllowFocus = false;
            this.gridColumn95.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn96
            // 
            this.gridColumn96.Caption = "Sell Unit Descriptor";
            this.gridColumn96.ColumnEdit = this.repositoryItemGridLookUpEdit2;
            this.gridColumn96.FieldName = "SellUnitDescriptorID";
            this.gridColumn96.Name = "gridColumn96";
            this.gridColumn96.Visible = true;
            this.gridColumn96.VisibleIndex = 4;
            this.gridColumn96.Width = 111;
            // 
            // gridColumn97
            // 
            this.gridColumn97.Caption = "Cost Per Unit Ex VAT";
            this.gridColumn97.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn97.FieldName = "CostPerUnitExVat";
            this.gridColumn97.Name = "gridColumn97";
            this.gridColumn97.Visible = true;
            this.gridColumn97.VisibleIndex = 2;
            this.gridColumn97.Width = 121;
            // 
            // repositoryItemSpinEdit3
            // 
            this.repositoryItemSpinEdit3.AutoHeight = false;
            this.repositoryItemSpinEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit3.Mask.EditMask = "c";
            this.repositoryItemSpinEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit3.Name = "repositoryItemSpinEdit3";
            // 
            // gridColumn98
            // 
            this.gridColumn98.Caption = "Cost Per Unit VAT Rate";
            this.gridColumn98.ColumnEdit = this.repositoryItemSpinEdit4;
            this.gridColumn98.FieldName = "CostPerUnitVatRate";
            this.gridColumn98.Name = "gridColumn98";
            this.gridColumn98.Visible = true;
            this.gridColumn98.VisibleIndex = 3;
            this.gridColumn98.Width = 132;
            // 
            // repositoryItemSpinEdit4
            // 
            this.repositoryItemSpinEdit4.AutoHeight = false;
            this.repositoryItemSpinEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit4.Mask.EditMask = "P";
            this.repositoryItemSpinEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit4.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            131072});
            this.repositoryItemSpinEdit4.Name = "repositoryItemSpinEdit4";
            // 
            // gridColumn99
            // 
            this.gridColumn99.Caption = "Sell Per Unit Ex VAT";
            this.gridColumn99.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn99.FieldName = "SellPerUnitExVat";
            this.gridColumn99.Name = "gridColumn99";
            this.gridColumn99.Visible = true;
            this.gridColumn99.VisibleIndex = 5;
            this.gridColumn99.Width = 115;
            // 
            // gridColumn100
            // 
            this.gridColumn100.Caption = "Sell Per Unit VAT Rate";
            this.gridColumn100.ColumnEdit = this.repositoryItemSpinEdit4;
            this.gridColumn100.FieldName = "SellPerUnitVatRate";
            this.gridColumn100.Name = "gridColumn100";
            this.gridColumn100.Visible = true;
            this.gridColumn100.VisibleIndex = 6;
            this.gridColumn100.Width = 126;
            // 
            // gridColumn101
            // 
            this.gridColumn101.Caption = "Remarks";
            this.gridColumn101.ColumnEdit = this.repositoryItemMemoExEdit14;
            this.gridColumn101.FieldName = "Remarks";
            this.gridColumn101.Name = "gridColumn101";
            this.gridColumn101.Visible = true;
            this.gridColumn101.VisibleIndex = 7;
            // 
            // repositoryItemMemoExEdit14
            // 
            this.repositoryItemMemoExEdit14.AutoHeight = false;
            this.repositoryItemMemoExEdit14.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit14.Name = "repositoryItemMemoExEdit14";
            this.repositoryItemMemoExEdit14.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit14.ShowIcon = false;
            // 
            // gridColumn102
            // 
            this.gridColumn102.Caption = "Client Name";
            this.gridColumn102.FieldName = "ClientName";
            this.gridColumn102.Name = "gridColumn102";
            this.gridColumn102.OptionsColumn.AllowEdit = false;
            this.gridColumn102.OptionsColumn.AllowFocus = false;
            this.gridColumn102.OptionsColumn.ReadOnly = true;
            this.gridColumn102.Width = 166;
            // 
            // gridColumn103
            // 
            this.gridColumn103.Caption = "Contract Start";
            this.gridColumn103.FieldName = "ContractStartDate";
            this.gridColumn103.Name = "gridColumn103";
            this.gridColumn103.OptionsColumn.AllowEdit = false;
            this.gridColumn103.OptionsColumn.AllowFocus = false;
            this.gridColumn103.OptionsColumn.ReadOnly = true;
            this.gridColumn103.Width = 90;
            // 
            // gridColumn104
            // 
            this.gridColumn104.Caption = "Contract End";
            this.gridColumn104.FieldName = "ContractEndDate";
            this.gridColumn104.Name = "gridColumn104";
            this.gridColumn104.OptionsColumn.AllowEdit = false;
            this.gridColumn104.OptionsColumn.AllowFocus = false;
            this.gridColumn104.OptionsColumn.ReadOnly = true;
            this.gridColumn104.Width = 84;
            // 
            // gridColumn105
            // 
            this.gridColumn105.Caption = "Site Name";
            this.gridColumn105.FieldName = "SiteName";
            this.gridColumn105.Name = "gridColumn105";
            this.gridColumn105.OptionsColumn.AllowEdit = false;
            this.gridColumn105.OptionsColumn.AllowFocus = false;
            this.gridColumn105.OptionsColumn.ReadOnly = true;
            this.gridColumn105.Width = 191;
            // 
            // gridColumn106
            // 
            this.gridColumn106.Caption = "Material Description";
            this.gridColumn106.ColumnEdit = this.repositoryItemButtonEditChooseMaterial;
            this.gridColumn106.FieldName = "MaterialDescription";
            this.gridColumn106.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn106.Name = "gridColumn106";
            this.gridColumn106.Visible = true;
            this.gridColumn106.VisibleIndex = 0;
            this.gridColumn106.Width = 176;
            // 
            // repositoryItemButtonEditChooseMaterial
            // 
            this.repositoryItemButtonEditChooseMaterial.AutoHeight = false;
            this.repositoryItemButtonEditChooseMaterial.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click me to open the Select Equipment Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditChooseMaterial.Name = "repositoryItemButtonEditChooseMaterial";
            this.repositoryItemButtonEditChooseMaterial.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditChooseMaterial_ButtonClick);
            // 
            // standaloneBarDockControl6
            // 
            this.standaloneBarDockControl6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl6.CausesValidation = false;
            this.standaloneBarDockControl6.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl6.Manager = this.barManager1;
            this.standaloneBarDockControl6.Name = "standaloneBarDockControl6";
            this.standaloneBarDockControl6.Size = new System.Drawing.Size(587, 25);
            this.standaloneBarDockControl6.Text = "standaloneBarDockControl6";
            // 
            // btnStep8Next
            // 
            this.btnStep8Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep8Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep8Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep8Next.Location = new System.Drawing.Point(865, 500);
            this.btnStep8Next.Name = "btnStep8Next";
            this.btnStep8Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep8Next.TabIndex = 20;
            this.btnStep8Next.Text = "Next";
            this.btnStep8Next.Click += new System.EventHandler(this.btnStep8Next_Click);
            // 
            // btnStep8Previous
            // 
            this.btnStep8Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep8Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep8Previous.Location = new System.Drawing.Point(771, 500);
            this.btnStep8Previous.Name = "btnStep8Previous";
            this.btnStep8Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep8Previous.TabIndex = 21;
            this.btnStep8Previous.Text = "Previous";
            this.btnStep8Previous.Click += new System.EventHandler(this.btnStep8Previous_Click);
            // 
            // panelControl10
            // 
            this.panelControl10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl10.Controls.Add(this.pictureEdit12);
            this.panelControl10.Controls.Add(this.labelControl23);
            this.panelControl10.Controls.Add(this.labelControl24);
            this.panelControl10.Location = new System.Drawing.Point(7, 6);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(946, 48);
            this.panelControl10.TabIndex = 19;
            // 
            // pictureEdit12
            // 
            this.pictureEdit12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit12.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit12.Location = new System.Drawing.Point(902, 4);
            this.pictureEdit12.MenuManager = this.barManager1;
            this.pictureEdit12.Name = "pictureEdit12";
            this.pictureEdit12.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit12.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit12.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit12.Properties.ReadOnly = true;
            this.pictureEdit12.Properties.ShowMenu = false;
            this.pictureEdit12.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit12.TabIndex = 9;
            // 
            // labelControl23
            // 
            this.labelControl23.AllowHtmlString = true;
            this.labelControl23.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Appearance.Options.UseBackColor = true;
            this.labelControl23.Appearance.Options.UseFont = true;
            this.labelControl23.Location = new System.Drawing.Point(5, 5);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(288, 16);
            this.labelControl23.TabIndex = 6;
            this.labelControl23.Text = "<b>Step 8:</b> Create Default Material Rates <b>[Optional]</b>";
            // 
            // labelControl24
            // 
            this.labelControl24.AllowHtmlString = true;
            this.labelControl24.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl24.Appearance.Options.UseBackColor = true;
            this.labelControl24.Location = new System.Drawing.Point(57, 29);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(406, 13);
            this.labelControl24.TabIndex = 7;
            this.labelControl24.Text = "Add materials to the Materials Grid by clicking the Add button. Click Next when d" +
    "one.\r\n";
            // 
            // xtraTabPageStep9
            // 
            this.xtraTabPageStep9.Controls.Add(this.splitContainerControl6);
            this.xtraTabPageStep9.Controls.Add(this.btnStep9Next);
            this.xtraTabPageStep9.Controls.Add(this.btnStep9Previous);
            this.xtraTabPageStep9.Controls.Add(this.panelControl7);
            this.xtraTabPageStep9.Name = "xtraTabPageStep9";
            this.xtraTabPageStep9.Size = new System.Drawing.Size(959, 536);
            this.xtraTabPageStep9.Tag = "9";
            this.xtraTabPageStep9.Text = "Step 9";
            // 
            // splitContainerControl6
            // 
            this.splitContainerControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl6.Location = new System.Drawing.Point(7, 63);
            this.splitContainerControl6.Name = "splitContainerControl6";
            this.splitContainerControl6.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl6.Panel1.Controls.Add(this.gridControl15);
            this.splitContainerControl6.Panel1.ShowCaption = true;
            this.splitContainerControl6.Panel1.Text = "Sites  [Read Only]";
            this.splitContainerControl6.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl6.Panel2.Controls.Add(this.standaloneBarDockControl2);
            this.splitContainerControl6.Panel2.Controls.Add(this.gridControl8);
            this.splitContainerControl6.Panel2.ShowCaption = true;
            this.splitContainerControl6.Panel2.Text = "Person Responsibilities - Linked to Selected Sites";
            this.splitContainerControl6.Size = new System.Drawing.Size(946, 431);
            this.splitContainerControl6.SplitterPosition = 349;
            this.splitContainerControl6.TabIndex = 24;
            // 
            // gridControl15
            // 
            this.gridControl15.DataSource = this.sp06090OMSiteContractEditBindingSource;
            this.gridControl15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl15.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl15.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl15.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl15.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl15.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl15.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl15.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl15.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl15.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl15.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl15.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl15.Location = new System.Drawing.Point(0, 0);
            this.gridControl15.MainView = this.gridView15;
            this.gridControl15.MenuManager = this.barManager1;
            this.gridControl15.Name = "gridControl15";
            this.gridControl15.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit11,
            this.repositoryItemMemoExEdit15,
            this.repositoryItemTextEdit10,
            this.repositoryItemTextEdit11,
            this.repositoryItemTextEdit12,
            this.repositoryItemTextEditInteger5});
            this.gridControl15.Size = new System.Drawing.Size(345, 407);
            this.gridControl15.TabIndex = 27;
            this.gridControl15.UseEmbeddedNavigator = true;
            this.gridControl15.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView15});
            // 
            // gridView15
            // 
            this.gridView15.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn92,
            this.gridColumn107,
            this.gridColumn108,
            this.gridColumn109,
            this.gridColumn110,
            this.gridColumn111,
            this.gridColumn112,
            this.gridColumn113,
            this.gridColumn114,
            this.gridColumn115,
            this.gridColumn116,
            this.gridColumn117,
            this.gridColumn118,
            this.gridColumn119,
            this.gridColumn120,
            this.gridColumn121,
            this.gridColumn122,
            this.colDummyEquipmentCount3,
            this.colDummyLabourCount3,
            this.colDummyMaterialCount3,
            this.colDummyPersonResponsibilityCount3});
            this.gridView15.GridControl = this.gridControl15;
            this.gridView15.Name = "gridView15";
            this.gridView15.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView15.OptionsFilter.AllowFilterEditor = false;
            this.gridView15.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView15.OptionsFilter.AllowMRUFilterList = false;
            this.gridView15.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView15.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView15.OptionsFind.FindDelay = 2000;
            this.gridView15.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView15.OptionsLayout.StoreAppearance = true;
            this.gridView15.OptionsLayout.StoreFormatRules = true;
            this.gridView15.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView15.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView15.OptionsSelection.MultiSelect = true;
            this.gridView15.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView15.OptionsView.ColumnAutoWidth = false;
            this.gridView15.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView15.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView15.OptionsView.ShowGroupPanel = false;
            this.gridView15.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn119, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView15.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView15_CustomDrawCell);
            this.gridView15.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView15.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView15.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView15.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView15.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView15.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView15.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView15_MouseUp);
            this.gridView15.GotFocus += new System.EventHandler(this.gridView15_GotFocus);
            // 
            // gridColumn92
            // 
            this.gridColumn92.Caption = "Site Contract ID";
            this.gridColumn92.FieldName = "SiteContractID";
            this.gridColumn92.Name = "gridColumn92";
            this.gridColumn92.OptionsColumn.AllowEdit = false;
            this.gridColumn92.OptionsColumn.AllowFocus = false;
            this.gridColumn92.OptionsColumn.ReadOnly = true;
            this.gridColumn92.Width = 98;
            // 
            // gridColumn107
            // 
            this.gridColumn107.Caption = "Client Contract ID";
            this.gridColumn107.FieldName = "ClientContractID";
            this.gridColumn107.Name = "gridColumn107";
            this.gridColumn107.OptionsColumn.AllowEdit = false;
            this.gridColumn107.OptionsColumn.AllowFocus = false;
            this.gridColumn107.OptionsColumn.ReadOnly = true;
            this.gridColumn107.Width = 107;
            // 
            // gridColumn108
            // 
            this.gridColumn108.Caption = "Site ID";
            this.gridColumn108.FieldName = "SiteID";
            this.gridColumn108.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn108.Name = "gridColumn108";
            this.gridColumn108.OptionsColumn.AllowEdit = false;
            this.gridColumn108.OptionsColumn.AllowFocus = false;
            this.gridColumn108.OptionsColumn.ReadOnly = true;
            this.gridColumn108.Visible = true;
            this.gridColumn108.VisibleIndex = 0;
            this.gridColumn108.Width = 51;
            // 
            // gridColumn109
            // 
            this.gridColumn109.Caption = "Start Date";
            this.gridColumn109.ColumnEdit = this.repositoryItemTextEdit10;
            this.gridColumn109.FieldName = "StartDate";
            this.gridColumn109.Name = "gridColumn109";
            this.gridColumn109.OptionsColumn.AllowEdit = false;
            this.gridColumn109.OptionsColumn.AllowFocus = false;
            this.gridColumn109.OptionsColumn.ReadOnly = true;
            this.gridColumn109.Visible = true;
            this.gridColumn109.VisibleIndex = 4;
            this.gridColumn109.Width = 120;
            // 
            // repositoryItemTextEdit10
            // 
            this.repositoryItemTextEdit10.AutoHeight = false;
            this.repositoryItemTextEdit10.Mask.EditMask = "g";
            this.repositoryItemTextEdit10.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit10.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit10.Name = "repositoryItemTextEdit10";
            // 
            // gridColumn110
            // 
            this.gridColumn110.Caption = "End Date";
            this.gridColumn110.ColumnEdit = this.repositoryItemTextEdit10;
            this.gridColumn110.FieldName = "EndDate";
            this.gridColumn110.Name = "gridColumn110";
            this.gridColumn110.OptionsColumn.AllowEdit = false;
            this.gridColumn110.OptionsColumn.AllowFocus = false;
            this.gridColumn110.OptionsColumn.ReadOnly = true;
            this.gridColumn110.Visible = true;
            this.gridColumn110.VisibleIndex = 5;
            this.gridColumn110.Width = 120;
            // 
            // gridColumn111
            // 
            this.gridColumn111.Caption = "Active";
            this.gridColumn111.ColumnEdit = this.repositoryItemCheckEdit11;
            this.gridColumn111.FieldName = "Active";
            this.gridColumn111.Name = "gridColumn111";
            this.gridColumn111.OptionsColumn.AllowEdit = false;
            this.gridColumn111.OptionsColumn.AllowFocus = false;
            this.gridColumn111.OptionsColumn.ReadOnly = true;
            this.gridColumn111.Visible = true;
            this.gridColumn111.VisibleIndex = 6;
            this.gridColumn111.Width = 51;
            // 
            // repositoryItemCheckEdit11
            // 
            this.repositoryItemCheckEdit11.AutoHeight = false;
            this.repositoryItemCheckEdit11.Caption = "Check";
            this.repositoryItemCheckEdit11.Name = "repositoryItemCheckEdit11";
            this.repositoryItemCheckEdit11.ValueChecked = 1;
            this.repositoryItemCheckEdit11.ValueUnchecked = 0;
            // 
            // gridColumn112
            // 
            this.gridColumn112.Caption = "Contract Value";
            this.gridColumn112.ColumnEdit = this.repositoryItemTextEdit11;
            this.gridColumn112.FieldName = "ContractValue";
            this.gridColumn112.Name = "gridColumn112";
            this.gridColumn112.OptionsColumn.AllowEdit = false;
            this.gridColumn112.OptionsColumn.AllowFocus = false;
            this.gridColumn112.OptionsColumn.ReadOnly = true;
            this.gridColumn112.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ContractValue", "{0:c2}")});
            this.gridColumn112.Visible = true;
            this.gridColumn112.VisibleIndex = 3;
            this.gridColumn112.Width = 92;
            // 
            // repositoryItemTextEdit11
            // 
            this.repositoryItemTextEdit11.AutoHeight = false;
            this.repositoryItemTextEdit11.Mask.EditMask = "c";
            this.repositoryItemTextEdit11.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit11.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit11.Name = "repositoryItemTextEdit11";
            // 
            // gridColumn113
            // 
            this.gridColumn113.Caption = "Last Client Payment Date";
            this.gridColumn113.ColumnEdit = this.repositoryItemTextEdit10;
            this.gridColumn113.FieldName = "LastClientPaymentDate";
            this.gridColumn113.Name = "gridColumn113";
            this.gridColumn113.OptionsColumn.AllowEdit = false;
            this.gridColumn113.OptionsColumn.AllowFocus = false;
            this.gridColumn113.OptionsColumn.ReadOnly = true;
            this.gridColumn113.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn113.Width = 142;
            // 
            // gridColumn114
            // 
            this.gridColumn114.Caption = "Remarks";
            this.gridColumn114.ColumnEdit = this.repositoryItemMemoExEdit15;
            this.gridColumn114.FieldName = "Remarks";
            this.gridColumn114.Name = "gridColumn114";
            this.gridColumn114.OptionsColumn.ReadOnly = true;
            this.gridColumn114.Visible = true;
            this.gridColumn114.VisibleIndex = 9;
            // 
            // repositoryItemMemoExEdit15
            // 
            this.repositoryItemMemoExEdit15.AutoHeight = false;
            this.repositoryItemMemoExEdit15.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit15.Name = "repositoryItemMemoExEdit15";
            this.repositoryItemMemoExEdit15.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit15.ShowIcon = false;
            // 
            // gridColumn115
            // 
            this.gridColumn115.Caption = "Yearly % Increase";
            this.gridColumn115.ColumnEdit = this.repositoryItemTextEdit12;
            this.gridColumn115.FieldName = "YearlyPercentageIncrease";
            this.gridColumn115.Name = "gridColumn115";
            this.gridColumn115.OptionsColumn.AllowEdit = false;
            this.gridColumn115.OptionsColumn.AllowFocus = false;
            this.gridColumn115.OptionsColumn.ReadOnly = true;
            this.gridColumn115.Visible = true;
            this.gridColumn115.VisibleIndex = 7;
            this.gridColumn115.Width = 110;
            // 
            // repositoryItemTextEdit12
            // 
            this.repositoryItemTextEdit12.AutoHeight = false;
            this.repositoryItemTextEdit12.Mask.EditMask = "P";
            this.repositoryItemTextEdit12.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit12.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit12.Name = "repositoryItemTextEdit12";
            // 
            // gridColumn116
            // 
            this.gridColumn116.Caption = "Dummy Site Contract ID";
            this.gridColumn116.FieldName = "DummySiteContractID";
            this.gridColumn116.Name = "gridColumn116";
            this.gridColumn116.OptionsColumn.AllowEdit = false;
            this.gridColumn116.OptionsColumn.AllowFocus = false;
            this.gridColumn116.OptionsColumn.ReadOnly = true;
            this.gridColumn116.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn116.Width = 136;
            // 
            // gridColumn117
            // 
            this.gridColumn117.Caption = "Linked To Parent";
            this.gridColumn117.FieldName = "LinkedToParent";
            this.gridColumn117.Name = "gridColumn117";
            this.gridColumn117.OptionsColumn.AllowEdit = false;
            this.gridColumn117.OptionsColumn.AllowFocus = false;
            this.gridColumn117.OptionsColumn.ReadOnly = true;
            this.gridColumn117.Width = 101;
            // 
            // gridColumn118
            // 
            this.gridColumn118.Caption = "Client Name";
            this.gridColumn118.FieldName = "ClientName";
            this.gridColumn118.Name = "gridColumn118";
            this.gridColumn118.OptionsColumn.AllowEdit = false;
            this.gridColumn118.OptionsColumn.AllowFocus = false;
            this.gridColumn118.OptionsColumn.ReadOnly = true;
            this.gridColumn118.Width = 172;
            // 
            // gridColumn119
            // 
            this.gridColumn119.Caption = "Site Name";
            this.gridColumn119.FieldName = "SiteName";
            this.gridColumn119.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn119.Name = "gridColumn119";
            this.gridColumn119.OptionsColumn.AllowEdit = false;
            this.gridColumn119.OptionsColumn.AllowFocus = false;
            this.gridColumn119.OptionsColumn.ReadOnly = true;
            this.gridColumn119.Visible = true;
            this.gridColumn119.VisibleIndex = 1;
            this.gridColumn119.Width = 234;
            // 
            // gridColumn120
            // 
            this.gridColumn120.Caption = "Site Postcode";
            this.gridColumn120.FieldName = "SitePostcode";
            this.gridColumn120.Name = "gridColumn120";
            this.gridColumn120.OptionsColumn.AllowEdit = false;
            this.gridColumn120.OptionsColumn.AllowFocus = false;
            this.gridColumn120.OptionsColumn.ReadOnly = true;
            this.gridColumn120.Visible = true;
            this.gridColumn120.VisibleIndex = 2;
            this.gridColumn120.Width = 86;
            // 
            // gridColumn121
            // 
            this.gridColumn121.Caption = "Site Latitude";
            this.gridColumn121.FieldName = "LocationX";
            this.gridColumn121.Name = "gridColumn121";
            this.gridColumn121.OptionsColumn.AllowEdit = false;
            this.gridColumn121.OptionsColumn.AllowFocus = false;
            this.gridColumn121.OptionsColumn.ReadOnly = true;
            this.gridColumn121.Width = 81;
            // 
            // gridColumn122
            // 
            this.gridColumn122.Caption = "Site Longitude";
            this.gridColumn122.FieldName = "LocationY";
            this.gridColumn122.Name = "gridColumn122";
            this.gridColumn122.OptionsColumn.AllowEdit = false;
            this.gridColumn122.OptionsColumn.AllowFocus = false;
            this.gridColumn122.OptionsColumn.ReadOnly = true;
            this.gridColumn122.Width = 89;
            // 
            // colDummyEquipmentCount3
            // 
            this.colDummyEquipmentCount3.Caption = "Equipment Count";
            this.colDummyEquipmentCount3.ColumnEdit = this.repositoryItemTextEditInteger5;
            this.colDummyEquipmentCount3.FieldName = "DummyEquipmentCount";
            this.colDummyEquipmentCount3.Name = "colDummyEquipmentCount3";
            this.colDummyEquipmentCount3.OptionsColumn.AllowEdit = false;
            this.colDummyEquipmentCount3.OptionsColumn.AllowFocus = false;
            this.colDummyEquipmentCount3.OptionsColumn.ReadOnly = true;
            this.colDummyEquipmentCount3.Width = 103;
            // 
            // repositoryItemTextEditInteger5
            // 
            this.repositoryItemTextEditInteger5.AutoHeight = false;
            this.repositoryItemTextEditInteger5.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger5.Name = "repositoryItemTextEditInteger5";
            // 
            // colDummyLabourCount3
            // 
            this.colDummyLabourCount3.Caption = "Labour Count";
            this.colDummyLabourCount3.ColumnEdit = this.repositoryItemTextEditInteger5;
            this.colDummyLabourCount3.FieldName = "DummyLabourCount";
            this.colDummyLabourCount3.Name = "colDummyLabourCount3";
            this.colDummyLabourCount3.OptionsColumn.AllowEdit = false;
            this.colDummyLabourCount3.OptionsColumn.AllowFocus = false;
            this.colDummyLabourCount3.OptionsColumn.ReadOnly = true;
            this.colDummyLabourCount3.Width = 86;
            // 
            // colDummyMaterialCount3
            // 
            this.colDummyMaterialCount3.Caption = "Material Count";
            this.colDummyMaterialCount3.ColumnEdit = this.repositoryItemTextEditInteger5;
            this.colDummyMaterialCount3.FieldName = "DummyMaterialCount";
            this.colDummyMaterialCount3.Name = "colDummyMaterialCount3";
            this.colDummyMaterialCount3.OptionsColumn.AllowEdit = false;
            this.colDummyMaterialCount3.OptionsColumn.AllowFocus = false;
            this.colDummyMaterialCount3.OptionsColumn.ReadOnly = true;
            this.colDummyMaterialCount3.Width = 91;
            // 
            // colDummyPersonResponsibilityCount3
            // 
            this.colDummyPersonResponsibilityCount3.Caption = "Responsibility Count";
            this.colDummyPersonResponsibilityCount3.ColumnEdit = this.repositoryItemTextEditInteger5;
            this.colDummyPersonResponsibilityCount3.FieldName = "DummyPersonResponsibilityCount";
            this.colDummyPersonResponsibilityCount3.Name = "colDummyPersonResponsibilityCount3";
            this.colDummyPersonResponsibilityCount3.OptionsColumn.AllowEdit = false;
            this.colDummyPersonResponsibilityCount3.OptionsColumn.AllowFocus = false;
            this.colDummyPersonResponsibilityCount3.OptionsColumn.ReadOnly = true;
            this.colDummyPersonResponsibilityCount3.Visible = true;
            this.colDummyPersonResponsibilityCount3.VisibleIndex = 8;
            this.colDummyPersonResponsibilityCount3.Width = 118;
            // 
            // standaloneBarDockControl2
            // 
            this.standaloneBarDockControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl2.CausesValidation = false;
            this.standaloneBarDockControl2.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl2.Manager = this.barManager1;
            this.standaloneBarDockControl2.Name = "standaloneBarDockControl2";
            this.standaloneBarDockControl2.Size = new System.Drawing.Size(587, 25);
            this.standaloneBarDockControl2.Text = "standaloneBarDockControl2";
            // 
            // gridControl8
            // 
            this.gridControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl8.DataSource = this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl8.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Block Edit Selected Records", "block edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl8.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl8_EmbeddedNavigator_ButtonClick);
            this.gridControl8.Location = new System.Drawing.Point(0, 25);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.MenuManager = this.barManager1;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEditChoosePerson,
            this.repositoryItemButtonEditPersonResponsibilityType});
            this.gridControl8.Size = new System.Drawing.Size(587, 382);
            this.gridControl8.TabIndex = 23;
            this.gridControl8.UseEmbeddedNavigator = true;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // sp06081OMClientContractLinkedResponsibilitiesEditBindingSource
            // 
            this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.DataMember = "sp06081_OM_Client_Contract_Linked_Responsibilities_Edit";
            this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPersonResponsibilityID,
            this.colStaffID,
            this.colResponsibleForRecordID,
            this.colResponsibleForRecordTypeID,
            this.colResponsibilityTypeID,
            this.colRemarks3,
            this.colClientID1,
            this.colClientName1,
            this.colContractStartDate,
            this.colContractEndDate,
            this.colLinkedToParent1,
            this.colResponsibilityType,
            this.colStaffName,
            this.colPersonTypeDescription,
            this.colPersonTypeID});
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.GroupCount = 1;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView8.OptionsFilter.AllowFilterEditor = false;
            this.gridView8.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView8.OptionsFilter.AllowMRUFilterList = false;
            this.gridView8.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView8.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView8.OptionsFind.FindDelay = 2000;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsLayout.StoreFormatRules = true;
            this.gridView8.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.MultiSelect = true;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView8.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView8.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView8.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView8_MouseUp);
            this.gridView8.GotFocus += new System.EventHandler(this.gridView8_GotFocus);
            // 
            // colPersonResponsibilityID
            // 
            this.colPersonResponsibilityID.Caption = "Person Responsibility ID";
            this.colPersonResponsibilityID.FieldName = "PersonResponsibilityID";
            this.colPersonResponsibilityID.Name = "colPersonResponsibilityID";
            this.colPersonResponsibilityID.OptionsColumn.AllowEdit = false;
            this.colPersonResponsibilityID.OptionsColumn.AllowFocus = false;
            this.colPersonResponsibilityID.OptionsColumn.ReadOnly = true;
            this.colPersonResponsibilityID.OptionsFilter.AllowFilter = false;
            this.colPersonResponsibilityID.Width = 126;
            // 
            // colStaffID
            // 
            this.colStaffID.Caption = "Staff ID";
            this.colStaffID.FieldName = "StaffID";
            this.colStaffID.Name = "colStaffID";
            this.colStaffID.OptionsColumn.AllowEdit = false;
            this.colStaffID.OptionsColumn.AllowFocus = false;
            this.colStaffID.OptionsColumn.ReadOnly = true;
            this.colStaffID.OptionsFilter.AllowFilter = false;
            this.colStaffID.Width = 49;
            // 
            // colResponsibleForRecordID
            // 
            this.colResponsibleForRecordID.Caption = "Responsible For Record ID";
            this.colResponsibleForRecordID.FieldName = "ResponsibleForRecordID";
            this.colResponsibleForRecordID.Name = "colResponsibleForRecordID";
            this.colResponsibleForRecordID.OptionsColumn.AllowEdit = false;
            this.colResponsibleForRecordID.OptionsColumn.AllowFocus = false;
            this.colResponsibleForRecordID.OptionsColumn.ReadOnly = true;
            this.colResponsibleForRecordID.OptionsFilter.AllowFilter = false;
            this.colResponsibleForRecordID.Width = 138;
            // 
            // colResponsibleForRecordTypeID
            // 
            this.colResponsibleForRecordTypeID.Caption = "Responsible For Record Type ID";
            this.colResponsibleForRecordTypeID.FieldName = "ResponsibleForRecordTypeID";
            this.colResponsibleForRecordTypeID.Name = "colResponsibleForRecordTypeID";
            this.colResponsibleForRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colResponsibleForRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colResponsibleForRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colResponsibleForRecordTypeID.OptionsFilter.AllowFilter = false;
            this.colResponsibleForRecordTypeID.Width = 165;
            // 
            // colResponsibilityTypeID
            // 
            this.colResponsibilityTypeID.Caption = "Responsibility Type ID";
            this.colResponsibilityTypeID.FieldName = "ResponsibilityTypeID";
            this.colResponsibilityTypeID.Name = "colResponsibilityTypeID";
            this.colResponsibilityTypeID.OptionsFilter.AllowFilter = false;
            this.colResponsibilityTypeID.Width = 171;
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.OptionsFilter.AllowFilter = false;
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 3;
            this.colRemarks3.Width = 173;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            this.colClientID1.OptionsFilter.AllowFilter = false;
            this.colClientID1.Width = 54;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Site Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.OptionsFilter.AllowFilter = false;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 4;
            this.colClientName1.Width = 317;
            // 
            // colContractStartDate
            // 
            this.colContractStartDate.Caption = "Contract Start Date";
            this.colContractStartDate.FieldName = "ContractStartDate";
            this.colContractStartDate.Name = "colContractStartDate";
            this.colContractStartDate.OptionsColumn.AllowEdit = false;
            this.colContractStartDate.OptionsColumn.AllowFocus = false;
            this.colContractStartDate.OptionsColumn.ReadOnly = true;
            this.colContractStartDate.OptionsFilter.AllowFilter = false;
            this.colContractStartDate.Width = 106;
            // 
            // colContractEndDate
            // 
            this.colContractEndDate.Caption = "Contract End Date";
            this.colContractEndDate.FieldName = "ContractEndDate";
            this.colContractEndDate.Name = "colContractEndDate";
            this.colContractEndDate.OptionsColumn.AllowEdit = false;
            this.colContractEndDate.OptionsColumn.AllowFocus = false;
            this.colContractEndDate.OptionsColumn.ReadOnly = true;
            this.colContractEndDate.OptionsFilter.AllowFilter = false;
            this.colContractEndDate.Width = 100;
            // 
            // colLinkedToParent1
            // 
            this.colLinkedToParent1.Caption = "Linked To Parent";
            this.colLinkedToParent1.FieldName = "LinkedToParent";
            this.colLinkedToParent1.Name = "colLinkedToParent1";
            this.colLinkedToParent1.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent1.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent1.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent1.OptionsFilter.AllowFilter = false;
            this.colLinkedToParent1.Width = 438;
            // 
            // colResponsibilityType
            // 
            this.colResponsibilityType.Caption = "Responsibility Type";
            this.colResponsibilityType.ColumnEdit = this.repositoryItemButtonEditPersonResponsibilityType;
            this.colResponsibilityType.FieldName = "ResponsibilityType";
            this.colResponsibilityType.Name = "colResponsibilityType";
            this.colResponsibilityType.OptionsFilter.AllowFilter = false;
            this.colResponsibilityType.Visible = true;
            this.colResponsibilityType.VisibleIndex = 2;
            this.colResponsibilityType.Width = 197;
            // 
            // repositoryItemButtonEditPersonResponsibilityType
            // 
            this.repositoryItemButtonEditPersonResponsibilityType.AutoHeight = false;
            this.repositoryItemButtonEditPersonResponsibilityType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Click me to open the Select Person Responsibility Type screen.", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditPersonResponsibilityType.Name = "repositoryItemButtonEditPersonResponsibilityType";
            this.repositoryItemButtonEditPersonResponsibilityType.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditPersonResponsibilityType.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditPersonResponsibilityType_ButtonClick);
            // 
            // colStaffName
            // 
            this.colStaffName.Caption = "Staff Name";
            this.colStaffName.ColumnEdit = this.repositoryItemButtonEditChoosePerson;
            this.colStaffName.FieldName = "StaffName";
            this.colStaffName.Name = "colStaffName";
            this.colStaffName.OptionsFilter.AllowFilter = false;
            this.colStaffName.Visible = true;
            this.colStaffName.VisibleIndex = 1;
            this.colStaffName.Width = 244;
            // 
            // repositoryItemButtonEditChoosePerson
            // 
            this.repositoryItemButtonEditChoosePerson.AutoHeight = false;
            this.repositoryItemButtonEditChoosePerson.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Click me to open Select Staff screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditChoosePerson.Name = "repositoryItemButtonEditChoosePerson";
            this.repositoryItemButtonEditChoosePerson.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditChoosePerson.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditChoosePerson_ButtonClick);
            // 
            // colPersonTypeDescription
            // 
            this.colPersonTypeDescription.Caption = "Person Type";
            this.colPersonTypeDescription.FieldName = "PersonTypeDescription";
            this.colPersonTypeDescription.Name = "colPersonTypeDescription";
            this.colPersonTypeDescription.OptionsColumn.AllowEdit = false;
            this.colPersonTypeDescription.OptionsColumn.AllowFocus = false;
            this.colPersonTypeDescription.OptionsColumn.ReadOnly = true;
            this.colPersonTypeDescription.Visible = true;
            this.colPersonTypeDescription.VisibleIndex = 0;
            this.colPersonTypeDescription.Width = 105;
            // 
            // colPersonTypeID
            // 
            this.colPersonTypeID.Caption = "Person Type ID";
            this.colPersonTypeID.FieldName = "PersonTypeID";
            this.colPersonTypeID.Name = "colPersonTypeID";
            this.colPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colPersonTypeID.Width = 93;
            // 
            // btnStep9Next
            // 
            this.btnStep9Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep9Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep9Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep9Next.Location = new System.Drawing.Point(865, 500);
            this.btnStep9Next.Name = "btnStep9Next";
            this.btnStep9Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep9Next.TabIndex = 21;
            this.btnStep9Next.Text = "Next";
            this.btnStep9Next.Click += new System.EventHandler(this.btnStep9Next_Click);
            // 
            // btnStep9Previous
            // 
            this.btnStep9Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep9Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep9Previous.Location = new System.Drawing.Point(771, 500);
            this.btnStep9Previous.Name = "btnStep9Previous";
            this.btnStep9Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep9Previous.TabIndex = 22;
            this.btnStep9Previous.Text = "Previous";
            this.btnStep9Previous.Click += new System.EventHandler(this.btnStep9Previous_Click);
            // 
            // panelControl7
            // 
            this.panelControl7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl7.Controls.Add(this.pictureEdit9);
            this.panelControl7.Controls.Add(this.labelControl15);
            this.panelControl7.Controls.Add(this.labelControl16);
            this.panelControl7.Location = new System.Drawing.Point(7, 6);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(946, 48);
            this.panelControl7.TabIndex = 18;
            // 
            // pictureEdit9
            // 
            this.pictureEdit9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit9.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit9.Location = new System.Drawing.Point(902, 4);
            this.pictureEdit9.MenuManager = this.barManager1;
            this.pictureEdit9.Name = "pictureEdit9";
            this.pictureEdit9.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit9.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit9.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit9.Properties.ReadOnly = true;
            this.pictureEdit9.Properties.ShowMenu = false;
            this.pictureEdit9.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit9.TabIndex = 9;
            // 
            // labelControl15
            // 
            this.labelControl15.AllowHtmlString = true;
            this.labelControl15.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.Options.UseBackColor = true;
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Location = new System.Drawing.Point(5, 5);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(292, 16);
            this.labelControl15.TabIndex = 6;
            this.labelControl15.Text = "<b>Step 9:</b> Create Person Responsibilities <b>[Optional]</b>";
            // 
            // labelControl16
            // 
            this.labelControl16.AllowHtmlString = true;
            this.labelControl16.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl16.Appearance.Options.UseBackColor = true;
            this.labelControl16.Location = new System.Drawing.Point(57, 29);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(358, 13);
            this.labelControl16.TabIndex = 7;
            this.labelControl16.Text = "Add one or more Person Responsibilities to the grid. Click Next when done.";
            // 
            // xtraTabPageStep10
            // 
            this.xtraTabPageStep10.Controls.Add(this.splitContainerControl7);
            this.xtraTabPageStep10.Controls.Add(this.panelControl12);
            this.xtraTabPageStep10.Controls.Add(this.btnStep10Next);
            this.xtraTabPageStep10.Controls.Add(this.btnStep10Previous);
            this.xtraTabPageStep10.Name = "xtraTabPageStep10";
            this.xtraTabPageStep10.Size = new System.Drawing.Size(959, 536);
            this.xtraTabPageStep10.Tag = "10";
            this.xtraTabPageStep10.Text = "Step 10";
            // 
            // splitContainerControl7
            // 
            this.splitContainerControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl7.Location = new System.Drawing.Point(7, 63);
            this.splitContainerControl7.Name = "splitContainerControl7";
            this.splitContainerControl7.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl7.Panel1.Controls.Add(this.gridControl17);
            this.splitContainerControl7.Panel1.ShowCaption = true;
            this.splitContainerControl7.Panel1.Text = "Sites  [Read Only]";
            this.splitContainerControl7.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl7.Panel2.Controls.Add(this.gridControl18);
            this.splitContainerControl7.Panel2.Controls.Add(this.standaloneBarDockControl7);
            this.splitContainerControl7.Panel2.ShowCaption = true;
            this.splitContainerControl7.Panel2.Text = "Job Rates - Linked to Selected Sites";
            this.splitContainerControl7.Size = new System.Drawing.Size(946, 431);
            this.splitContainerControl7.SplitterPosition = 349;
            this.splitContainerControl7.TabIndex = 26;
            this.splitContainerControl7.Text = "splitContainerControl7";
            // 
            // gridControl17
            // 
            this.gridControl17.DataSource = this.sp06090OMSiteContractEditBindingSource;
            this.gridControl17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl17.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl17.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl17.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl17.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl17.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl17.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl17.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl17.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl17.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl17.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl17.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl17.Location = new System.Drawing.Point(0, 0);
            this.gridControl17.MainView = this.gridView17;
            this.gridControl17.MenuManager = this.barManager1;
            this.gridControl17.Name = "gridControl17";
            this.gridControl17.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit12,
            this.repositoryItemMemoExEdit16,
            this.repositoryItemTextEdit13,
            this.repositoryItemTextEdit14,
            this.repositoryItemTextEdit15,
            this.repositoryItemTextEdit16});
            this.gridControl17.Size = new System.Drawing.Size(345, 407);
            this.gridControl17.TabIndex = 28;
            this.gridControl17.UseEmbeddedNavigator = true;
            this.gridControl17.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView17});
            // 
            // gridView17
            // 
            this.gridView17.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn126,
            this.gridColumn127,
            this.gridColumn128,
            this.gridColumn129,
            this.gridColumn130,
            this.gridColumn131,
            this.gridColumn132,
            this.gridColumn133,
            this.gridColumn134,
            this.gridColumn135,
            this.gridColumn136,
            this.gridColumn137,
            this.gridColumn138,
            this.gridColumn139,
            this.gridColumn140,
            this.gridColumn141,
            this.gridColumn142,
            this.gridColumn143,
            this.gridColumn144,
            this.gridColumn145,
            this.gridColumn146,
            this.colDummyJobRateCount});
            this.gridView17.GridControl = this.gridControl17;
            this.gridView17.Name = "gridView17";
            this.gridView17.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView17.OptionsFilter.AllowFilterEditor = false;
            this.gridView17.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView17.OptionsFilter.AllowMRUFilterList = false;
            this.gridView17.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView17.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView17.OptionsFind.FindDelay = 2000;
            this.gridView17.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView17.OptionsLayout.StoreAppearance = true;
            this.gridView17.OptionsLayout.StoreFormatRules = true;
            this.gridView17.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView17.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView17.OptionsSelection.MultiSelect = true;
            this.gridView17.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView17.OptionsView.ColumnAutoWidth = false;
            this.gridView17.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView17.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView17.OptionsView.ShowGroupPanel = false;
            this.gridView17.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn139, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView17.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView17_CustomDrawCell);
            this.gridView17.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView17.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView17.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView17.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView17.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView17.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView17.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView17_MouseUp);
            this.gridView17.GotFocus += new System.EventHandler(this.gridView17_GotFocus);
            // 
            // gridColumn126
            // 
            this.gridColumn126.Caption = "Site Contract ID";
            this.gridColumn126.FieldName = "SiteContractID";
            this.gridColumn126.Name = "gridColumn126";
            this.gridColumn126.OptionsColumn.AllowEdit = false;
            this.gridColumn126.OptionsColumn.AllowFocus = false;
            this.gridColumn126.OptionsColumn.ReadOnly = true;
            this.gridColumn126.Width = 98;
            // 
            // gridColumn127
            // 
            this.gridColumn127.Caption = "Client Contract ID";
            this.gridColumn127.FieldName = "ClientContractID";
            this.gridColumn127.Name = "gridColumn127";
            this.gridColumn127.OptionsColumn.AllowEdit = false;
            this.gridColumn127.OptionsColumn.AllowFocus = false;
            this.gridColumn127.OptionsColumn.ReadOnly = true;
            this.gridColumn127.Width = 107;
            // 
            // gridColumn128
            // 
            this.gridColumn128.Caption = "Site ID";
            this.gridColumn128.FieldName = "SiteID";
            this.gridColumn128.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn128.Name = "gridColumn128";
            this.gridColumn128.OptionsColumn.AllowEdit = false;
            this.gridColumn128.OptionsColumn.AllowFocus = false;
            this.gridColumn128.OptionsColumn.ReadOnly = true;
            this.gridColumn128.Visible = true;
            this.gridColumn128.VisibleIndex = 0;
            this.gridColumn128.Width = 51;
            // 
            // gridColumn129
            // 
            this.gridColumn129.Caption = "Start Date";
            this.gridColumn129.ColumnEdit = this.repositoryItemTextEdit13;
            this.gridColumn129.FieldName = "StartDate";
            this.gridColumn129.Name = "gridColumn129";
            this.gridColumn129.OptionsColumn.AllowEdit = false;
            this.gridColumn129.OptionsColumn.AllowFocus = false;
            this.gridColumn129.OptionsColumn.ReadOnly = true;
            this.gridColumn129.Visible = true;
            this.gridColumn129.VisibleIndex = 4;
            this.gridColumn129.Width = 120;
            // 
            // repositoryItemTextEdit13
            // 
            this.repositoryItemTextEdit13.AutoHeight = false;
            this.repositoryItemTextEdit13.Mask.EditMask = "g";
            this.repositoryItemTextEdit13.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit13.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit13.Name = "repositoryItemTextEdit13";
            // 
            // gridColumn130
            // 
            this.gridColumn130.Caption = "End Date";
            this.gridColumn130.ColumnEdit = this.repositoryItemTextEdit13;
            this.gridColumn130.FieldName = "EndDate";
            this.gridColumn130.Name = "gridColumn130";
            this.gridColumn130.OptionsColumn.AllowEdit = false;
            this.gridColumn130.OptionsColumn.AllowFocus = false;
            this.gridColumn130.OptionsColumn.ReadOnly = true;
            this.gridColumn130.Visible = true;
            this.gridColumn130.VisibleIndex = 5;
            this.gridColumn130.Width = 120;
            // 
            // gridColumn131
            // 
            this.gridColumn131.Caption = "Active";
            this.gridColumn131.ColumnEdit = this.repositoryItemCheckEdit12;
            this.gridColumn131.FieldName = "Active";
            this.gridColumn131.Name = "gridColumn131";
            this.gridColumn131.OptionsColumn.AllowEdit = false;
            this.gridColumn131.OptionsColumn.AllowFocus = false;
            this.gridColumn131.OptionsColumn.ReadOnly = true;
            this.gridColumn131.Visible = true;
            this.gridColumn131.VisibleIndex = 6;
            this.gridColumn131.Width = 51;
            // 
            // repositoryItemCheckEdit12
            // 
            this.repositoryItemCheckEdit12.AutoHeight = false;
            this.repositoryItemCheckEdit12.Caption = "Check";
            this.repositoryItemCheckEdit12.Name = "repositoryItemCheckEdit12";
            this.repositoryItemCheckEdit12.ValueChecked = 1;
            this.repositoryItemCheckEdit12.ValueUnchecked = 0;
            // 
            // gridColumn132
            // 
            this.gridColumn132.Caption = "Contract Value";
            this.gridColumn132.ColumnEdit = this.repositoryItemTextEdit14;
            this.gridColumn132.FieldName = "ContractValue";
            this.gridColumn132.Name = "gridColumn132";
            this.gridColumn132.OptionsColumn.AllowEdit = false;
            this.gridColumn132.OptionsColumn.AllowFocus = false;
            this.gridColumn132.OptionsColumn.ReadOnly = true;
            this.gridColumn132.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ContractValue", "{0:c2}")});
            this.gridColumn132.Visible = true;
            this.gridColumn132.VisibleIndex = 3;
            this.gridColumn132.Width = 92;
            // 
            // repositoryItemTextEdit14
            // 
            this.repositoryItemTextEdit14.AutoHeight = false;
            this.repositoryItemTextEdit14.Mask.EditMask = "c";
            this.repositoryItemTextEdit14.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit14.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit14.Name = "repositoryItemTextEdit14";
            // 
            // gridColumn133
            // 
            this.gridColumn133.Caption = "Last Client Payment Date";
            this.gridColumn133.ColumnEdit = this.repositoryItemTextEdit13;
            this.gridColumn133.FieldName = "LastClientPaymentDate";
            this.gridColumn133.Name = "gridColumn133";
            this.gridColumn133.OptionsColumn.AllowEdit = false;
            this.gridColumn133.OptionsColumn.AllowFocus = false;
            this.gridColumn133.OptionsColumn.ReadOnly = true;
            this.gridColumn133.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn133.Width = 142;
            // 
            // gridColumn134
            // 
            this.gridColumn134.Caption = "Remarks";
            this.gridColumn134.ColumnEdit = this.repositoryItemMemoExEdit16;
            this.gridColumn134.FieldName = "Remarks";
            this.gridColumn134.Name = "gridColumn134";
            this.gridColumn134.OptionsColumn.ReadOnly = true;
            this.gridColumn134.Visible = true;
            this.gridColumn134.VisibleIndex = 9;
            // 
            // repositoryItemMemoExEdit16
            // 
            this.repositoryItemMemoExEdit16.AutoHeight = false;
            this.repositoryItemMemoExEdit16.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit16.Name = "repositoryItemMemoExEdit16";
            this.repositoryItemMemoExEdit16.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit16.ShowIcon = false;
            // 
            // gridColumn135
            // 
            this.gridColumn135.Caption = "Yearly % Increase";
            this.gridColumn135.ColumnEdit = this.repositoryItemTextEdit15;
            this.gridColumn135.FieldName = "YearlyPercentageIncrease";
            this.gridColumn135.Name = "gridColumn135";
            this.gridColumn135.OptionsColumn.AllowEdit = false;
            this.gridColumn135.OptionsColumn.AllowFocus = false;
            this.gridColumn135.OptionsColumn.ReadOnly = true;
            this.gridColumn135.Visible = true;
            this.gridColumn135.VisibleIndex = 7;
            this.gridColumn135.Width = 110;
            // 
            // repositoryItemTextEdit15
            // 
            this.repositoryItemTextEdit15.AutoHeight = false;
            this.repositoryItemTextEdit15.Mask.EditMask = "P";
            this.repositoryItemTextEdit15.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit15.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit15.Name = "repositoryItemTextEdit15";
            // 
            // gridColumn136
            // 
            this.gridColumn136.Caption = "Dummy Site Contract ID";
            this.gridColumn136.FieldName = "DummySiteContractID";
            this.gridColumn136.Name = "gridColumn136";
            this.gridColumn136.OptionsColumn.AllowEdit = false;
            this.gridColumn136.OptionsColumn.AllowFocus = false;
            this.gridColumn136.OptionsColumn.ReadOnly = true;
            this.gridColumn136.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn136.Width = 136;
            // 
            // gridColumn137
            // 
            this.gridColumn137.Caption = "Linked To Parent";
            this.gridColumn137.FieldName = "LinkedToParent";
            this.gridColumn137.Name = "gridColumn137";
            this.gridColumn137.OptionsColumn.AllowEdit = false;
            this.gridColumn137.OptionsColumn.AllowFocus = false;
            this.gridColumn137.OptionsColumn.ReadOnly = true;
            this.gridColumn137.Width = 101;
            // 
            // gridColumn138
            // 
            this.gridColumn138.Caption = "Client Name";
            this.gridColumn138.FieldName = "ClientName";
            this.gridColumn138.Name = "gridColumn138";
            this.gridColumn138.OptionsColumn.AllowEdit = false;
            this.gridColumn138.OptionsColumn.AllowFocus = false;
            this.gridColumn138.OptionsColumn.ReadOnly = true;
            this.gridColumn138.Width = 172;
            // 
            // gridColumn139
            // 
            this.gridColumn139.Caption = "Site Name";
            this.gridColumn139.FieldName = "SiteName";
            this.gridColumn139.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn139.Name = "gridColumn139";
            this.gridColumn139.OptionsColumn.AllowEdit = false;
            this.gridColumn139.OptionsColumn.AllowFocus = false;
            this.gridColumn139.OptionsColumn.ReadOnly = true;
            this.gridColumn139.Visible = true;
            this.gridColumn139.VisibleIndex = 1;
            this.gridColumn139.Width = 234;
            // 
            // gridColumn140
            // 
            this.gridColumn140.Caption = "Site Postcode";
            this.gridColumn140.FieldName = "SitePostcode";
            this.gridColumn140.Name = "gridColumn140";
            this.gridColumn140.OptionsColumn.AllowEdit = false;
            this.gridColumn140.OptionsColumn.AllowFocus = false;
            this.gridColumn140.OptionsColumn.ReadOnly = true;
            this.gridColumn140.Visible = true;
            this.gridColumn140.VisibleIndex = 2;
            this.gridColumn140.Width = 86;
            // 
            // gridColumn141
            // 
            this.gridColumn141.Caption = "Site Latitude";
            this.gridColumn141.FieldName = "LocationX";
            this.gridColumn141.Name = "gridColumn141";
            this.gridColumn141.OptionsColumn.AllowEdit = false;
            this.gridColumn141.OptionsColumn.AllowFocus = false;
            this.gridColumn141.OptionsColumn.ReadOnly = true;
            this.gridColumn141.Width = 81;
            // 
            // gridColumn142
            // 
            this.gridColumn142.Caption = "Site Longitude";
            this.gridColumn142.FieldName = "LocationY";
            this.gridColumn142.Name = "gridColumn142";
            this.gridColumn142.OptionsColumn.AllowEdit = false;
            this.gridColumn142.OptionsColumn.AllowFocus = false;
            this.gridColumn142.OptionsColumn.ReadOnly = true;
            this.gridColumn142.Width = 89;
            // 
            // gridColumn143
            // 
            this.gridColumn143.Caption = "Equipment Count";
            this.gridColumn143.ColumnEdit = this.repositoryItemTextEdit16;
            this.gridColumn143.FieldName = "DummyEquipmentCount";
            this.gridColumn143.Name = "gridColumn143";
            this.gridColumn143.OptionsColumn.AllowEdit = false;
            this.gridColumn143.OptionsColumn.AllowFocus = false;
            this.gridColumn143.OptionsColumn.ReadOnly = true;
            this.gridColumn143.Width = 103;
            // 
            // repositoryItemTextEdit16
            // 
            this.repositoryItemTextEdit16.AutoHeight = false;
            this.repositoryItemTextEdit16.Mask.EditMask = "n0";
            this.repositoryItemTextEdit16.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit16.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit16.Name = "repositoryItemTextEdit16";
            // 
            // gridColumn144
            // 
            this.gridColumn144.Caption = "Labour Count";
            this.gridColumn144.ColumnEdit = this.repositoryItemTextEdit16;
            this.gridColumn144.FieldName = "DummyLabourCount";
            this.gridColumn144.Name = "gridColumn144";
            this.gridColumn144.OptionsColumn.AllowEdit = false;
            this.gridColumn144.OptionsColumn.AllowFocus = false;
            this.gridColumn144.OptionsColumn.ReadOnly = true;
            this.gridColumn144.Width = 86;
            // 
            // gridColumn145
            // 
            this.gridColumn145.Caption = "Material Count";
            this.gridColumn145.ColumnEdit = this.repositoryItemTextEdit16;
            this.gridColumn145.FieldName = "DummyMaterialCount";
            this.gridColumn145.Name = "gridColumn145";
            this.gridColumn145.OptionsColumn.AllowEdit = false;
            this.gridColumn145.OptionsColumn.AllowFocus = false;
            this.gridColumn145.OptionsColumn.ReadOnly = true;
            this.gridColumn145.Width = 91;
            // 
            // gridColumn146
            // 
            this.gridColumn146.Caption = "Responsibility Count";
            this.gridColumn146.ColumnEdit = this.repositoryItemTextEdit16;
            this.gridColumn146.FieldName = "DummyPersonResponsibilityCount";
            this.gridColumn146.Name = "gridColumn146";
            this.gridColumn146.OptionsColumn.AllowEdit = false;
            this.gridColumn146.OptionsColumn.AllowFocus = false;
            this.gridColumn146.OptionsColumn.ReadOnly = true;
            this.gridColumn146.Width = 118;
            // 
            // colDummyJobRateCount
            // 
            this.colDummyJobRateCount.Caption = "Job Rate Count";
            this.colDummyJobRateCount.ColumnEdit = this.repositoryItemTextEdit16;
            this.colDummyJobRateCount.FieldName = "DummyJobRateCount";
            this.colDummyJobRateCount.Name = "colDummyJobRateCount";
            this.colDummyJobRateCount.OptionsColumn.AllowEdit = false;
            this.colDummyJobRateCount.OptionsColumn.AllowFocus = false;
            this.colDummyJobRateCount.OptionsColumn.ReadOnly = true;
            this.colDummyJobRateCount.Visible = true;
            this.colDummyJobRateCount.VisibleIndex = 8;
            this.colDummyJobRateCount.Width = 94;
            // 
            // gridControl18
            // 
            this.gridControl18.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl18.DataSource = this.sp06346OMSiteContractJobRateEditBindingSource;
            this.gridControl18.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl18.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl18.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl18.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl18.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl18.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl18.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl18.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl18.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl18.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl18.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl18.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Block Edit Selected Records", "block edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl18.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl18_EmbeddedNavigator_ButtonClick);
            this.gridControl18.Location = new System.Drawing.Point(0, 25);
            this.gridControl18.MainView = this.gridView18;
            this.gridControl18.MenuManager = this.barManager1;
            this.gridControl18.Name = "gridControl18";
            this.gridControl18.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit17,
            this.repositoryItemSpinEdit5,
            this.repositoryItemButtonEditJobSubType,
            this.repositoryItemDateEdit4,
            this.repositoryItemTextEditDate18,
            this.repositoryItemGridLookUpEditVisitCategoryID});
            this.gridControl18.Size = new System.Drawing.Size(587, 382);
            this.gridControl18.TabIndex = 24;
            this.gridControl18.UseEmbeddedNavigator = true;
            this.gridControl18.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView18});
            // 
            // sp06346OMSiteContractJobRateEditBindingSource
            // 
            this.sp06346OMSiteContractJobRateEditBindingSource.DataMember = "sp06346_OM_Site_Contract_Job_Rate_Edit";
            this.sp06346OMSiteContractJobRateEditBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView18
            // 
            this.gridView18.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn148,
            this.gridColumn159,
            this.gridColumn160,
            this.gridColumn161,
            this.gridColumn162,
            this.gridColumn163,
            this.colSiteContractJobRateID,
            this.colJobTypeID,
            this.colJobType,
            this.colJobSubTypeID,
            this.colJobSubType,
            this.colFromDate,
            this.colToDate,
            this.colTeamCost,
            this.colClientSell,
            this.colVisitCategoryID});
            this.gridView18.GridControl = this.gridControl18;
            this.gridView18.GroupCount = 1;
            this.gridView18.Name = "gridView18";
            this.gridView18.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView18.OptionsFilter.AllowFilterEditor = false;
            this.gridView18.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView18.OptionsFilter.AllowMRUFilterList = false;
            this.gridView18.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView18.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView18.OptionsFind.FindDelay = 2000;
            this.gridView18.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView18.OptionsLayout.StoreAppearance = true;
            this.gridView18.OptionsLayout.StoreFormatRules = true;
            this.gridView18.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView18.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView18.OptionsSelection.MultiSelect = true;
            this.gridView18.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView18.OptionsView.ColumnAutoWidth = false;
            this.gridView18.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView18.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView18.OptionsView.ShowGroupPanel = false;
            this.gridView18.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn163, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobSubType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFromDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView18.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView18.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView18.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView18.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView18_ShowingEditor);
            this.gridView18.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView18.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView18_MouseUp);
            this.gridView18.GotFocus += new System.EventHandler(this.gridView18_GotFocus);
            this.gridView18.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView18_ValidatingEditor);
            // 
            // gridColumn148
            // 
            this.gridColumn148.Caption = "Site Contract ID";
            this.gridColumn148.FieldName = "SiteContractID";
            this.gridColumn148.Name = "gridColumn148";
            this.gridColumn148.OptionsColumn.AllowEdit = false;
            this.gridColumn148.OptionsColumn.AllowFocus = false;
            this.gridColumn148.OptionsColumn.ReadOnly = true;
            this.gridColumn148.Width = 98;
            // 
            // gridColumn159
            // 
            this.gridColumn159.Caption = "Remarks";
            this.gridColumn159.ColumnEdit = this.repositoryItemMemoExEdit17;
            this.gridColumn159.FieldName = "Remarks";
            this.gridColumn159.Name = "gridColumn159";
            this.gridColumn159.Visible = true;
            this.gridColumn159.VisibleIndex = 7;
            // 
            // repositoryItemMemoExEdit17
            // 
            this.repositoryItemMemoExEdit17.AutoHeight = false;
            this.repositoryItemMemoExEdit17.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit17.Name = "repositoryItemMemoExEdit17";
            this.repositoryItemMemoExEdit17.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit17.ShowIcon = false;
            // 
            // gridColumn160
            // 
            this.gridColumn160.Caption = "Client Name";
            this.gridColumn160.FieldName = "ClientName";
            this.gridColumn160.Name = "gridColumn160";
            this.gridColumn160.OptionsColumn.AllowEdit = false;
            this.gridColumn160.OptionsColumn.AllowFocus = false;
            this.gridColumn160.OptionsColumn.ReadOnly = true;
            this.gridColumn160.Width = 166;
            // 
            // gridColumn161
            // 
            this.gridColumn161.Caption = "Contract Start";
            this.gridColumn161.ColumnEdit = this.repositoryItemTextEditDate18;
            this.gridColumn161.FieldName = "ContractStartDate";
            this.gridColumn161.Name = "gridColumn161";
            this.gridColumn161.OptionsColumn.AllowEdit = false;
            this.gridColumn161.OptionsColumn.AllowFocus = false;
            this.gridColumn161.OptionsColumn.ReadOnly = true;
            this.gridColumn161.Width = 90;
            // 
            // repositoryItemTextEditDate18
            // 
            this.repositoryItemTextEditDate18.AutoHeight = false;
            this.repositoryItemTextEditDate18.Mask.EditMask = "d";
            this.repositoryItemTextEditDate18.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDate18.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDate18.Name = "repositoryItemTextEditDate18";
            // 
            // gridColumn162
            // 
            this.gridColumn162.Caption = "Contract End";
            this.gridColumn162.ColumnEdit = this.repositoryItemTextEditDate18;
            this.gridColumn162.FieldName = "ContractEndDate";
            this.gridColumn162.Name = "gridColumn162";
            this.gridColumn162.OptionsColumn.AllowEdit = false;
            this.gridColumn162.OptionsColumn.AllowFocus = false;
            this.gridColumn162.OptionsColumn.ReadOnly = true;
            this.gridColumn162.Width = 84;
            // 
            // gridColumn163
            // 
            this.gridColumn163.Caption = "Site Name";
            this.gridColumn163.FieldName = "SiteName";
            this.gridColumn163.Name = "gridColumn163";
            this.gridColumn163.OptionsColumn.AllowEdit = false;
            this.gridColumn163.OptionsColumn.AllowFocus = false;
            this.gridColumn163.OptionsColumn.ReadOnly = true;
            this.gridColumn163.Width = 191;
            // 
            // colSiteContractJobRateID
            // 
            this.colSiteContractJobRateID.Caption = "Site Contract Job Rate ID";
            this.colSiteContractJobRateID.FieldName = "SiteContractJobRateID";
            this.colSiteContractJobRateID.Name = "colSiteContractJobRateID";
            this.colSiteContractJobRateID.OptionsColumn.AllowEdit = false;
            this.colSiteContractJobRateID.OptionsColumn.AllowFocus = false;
            this.colSiteContractJobRateID.OptionsColumn.ReadOnly = true;
            this.colSiteContractJobRateID.Width = 142;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            this.colJobTypeID.Width = 77;
            // 
            // colJobType
            // 
            this.colJobType.Caption = "Job Type";
            this.colJobType.FieldName = "JobType";
            this.colJobType.Name = "colJobType";
            this.colJobType.OptionsColumn.AllowEdit = false;
            this.colJobType.OptionsColumn.AllowFocus = false;
            this.colJobType.OptionsColumn.ReadOnly = true;
            this.colJobType.Visible = true;
            this.colJobType.VisibleIndex = 0;
            this.colJobType.Width = 143;
            // 
            // colJobSubTypeID
            // 
            this.colJobSubTypeID.Caption = "Job Sub-Type ID";
            this.colJobSubTypeID.FieldName = "JobSubTypeID";
            this.colJobSubTypeID.Name = "colJobSubTypeID";
            this.colJobSubTypeID.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeID.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeID.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeID.Width = 99;
            // 
            // colJobSubType
            // 
            this.colJobSubType.Caption = "Job Sub-Type";
            this.colJobSubType.ColumnEdit = this.repositoryItemButtonEditJobSubType;
            this.colJobSubType.FieldName = "JobSubType";
            this.colJobSubType.Name = "colJobSubType";
            this.colJobSubType.Visible = true;
            this.colJobSubType.VisibleIndex = 1;
            this.colJobSubType.Width = 262;
            // 
            // repositoryItemButtonEditJobSubType
            // 
            this.repositoryItemButtonEditJobSubType.AutoHeight = false;
            this.repositoryItemButtonEditJobSubType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Click me to open the Select Job Type \\ Sub-Type Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Clear", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Click to Clear the Job Type [set the rate to Generic]", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditJobSubType.Name = "repositoryItemButtonEditJobSubType";
            this.repositoryItemButtonEditJobSubType.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditJobSubType.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditJobSubType_ButtonClick);
            // 
            // colFromDate
            // 
            this.colFromDate.Caption = "Valid From";
            this.colFromDate.ColumnEdit = this.repositoryItemDateEdit4;
            this.colFromDate.FieldName = "FromDate";
            this.colFromDate.Name = "colFromDate";
            this.colFromDate.Visible = true;
            this.colFromDate.VisibleIndex = 3;
            this.colFromDate.Width = 80;
            // 
            // repositoryItemDateEdit4
            // 
            this.repositoryItemDateEdit4.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemDateEdit4.AutoHeight = false;
            this.repositoryItemDateEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit4.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit4.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.repositoryItemDateEdit4.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.repositoryItemDateEdit4.Name = "repositoryItemDateEdit4";
            // 
            // colToDate
            // 
            this.colToDate.Caption = "Valid To";
            this.colToDate.ColumnEdit = this.repositoryItemDateEdit4;
            this.colToDate.FieldName = "ToDate";
            this.colToDate.Name = "colToDate";
            this.colToDate.Visible = true;
            this.colToDate.VisibleIndex = 4;
            this.colToDate.Width = 80;
            // 
            // colTeamCost
            // 
            this.colTeamCost.Caption = "Team Cost";
            this.colTeamCost.ColumnEdit = this.repositoryItemSpinEdit5;
            this.colTeamCost.FieldName = "TeamCost";
            this.colTeamCost.Name = "colTeamCost";
            this.colTeamCost.Visible = true;
            this.colTeamCost.VisibleIndex = 5;
            this.colTeamCost.Width = 80;
            // 
            // repositoryItemSpinEdit5
            // 
            this.repositoryItemSpinEdit5.AutoHeight = false;
            this.repositoryItemSpinEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit5.Mask.EditMask = "c";
            this.repositoryItemSpinEdit5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit5.Name = "repositoryItemSpinEdit5";
            // 
            // colClientSell
            // 
            this.colClientSell.Caption = "Client Sell";
            this.colClientSell.ColumnEdit = this.repositoryItemSpinEdit5;
            this.colClientSell.FieldName = "ClientSell";
            this.colClientSell.Name = "colClientSell";
            this.colClientSell.Visible = true;
            this.colClientSell.VisibleIndex = 6;
            this.colClientSell.Width = 80;
            // 
            // colVisitCategoryID
            // 
            this.colVisitCategoryID.Caption = "Visit Category";
            this.colVisitCategoryID.ColumnEdit = this.repositoryItemGridLookUpEditVisitCategoryID;
            this.colVisitCategoryID.FieldName = "VisitCategoryID";
            this.colVisitCategoryID.Name = "colVisitCategoryID";
            this.colVisitCategoryID.Visible = true;
            this.colVisitCategoryID.VisibleIndex = 2;
            this.colVisitCategoryID.Width = 125;
            // 
            // repositoryItemGridLookUpEditVisitCategoryID
            // 
            this.repositoryItemGridLookUpEditVisitCategoryID.AutoHeight = false;
            this.repositoryItemGridLookUpEditVisitCategoryID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditVisitCategoryID.DataSource = this.sp06250OMVisitCategoriesWithBlankBindingSource;
            this.repositoryItemGridLookUpEditVisitCategoryID.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditVisitCategoryID.Name = "repositoryItemGridLookUpEditVisitCategoryID";
            this.repositoryItemGridLookUpEditVisitCategoryID.NullText = "";
            this.repositoryItemGridLookUpEditVisitCategoryID.PopupView = this.gridView19;
            this.repositoryItemGridLookUpEditVisitCategoryID.ValueMember = "ID";
            // 
            // sp06250OMVisitCategoriesWithBlankBindingSource
            // 
            this.sp06250OMVisitCategoriesWithBlankBindingSource.DataMember = "sp06250_OM_Visit_Categories_With_Blank";
            this.sp06250OMVisitCategoriesWithBlankBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView19
            // 
            this.gridView19.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn147,
            this.gridColumn149,
            this.gridColumn150});
            this.gridView19.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.gridColumn147;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView19.FormatRules.Add(gridFormatRule2);
            this.gridView19.HorzScrollStep = 3;
            this.gridView19.Name = "gridView19";
            this.gridView19.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView19.OptionsLayout.StoreAppearance = true;
            this.gridView19.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView19.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView19.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView19.OptionsView.ColumnAutoWidth = false;
            this.gridView19.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView19.OptionsView.ShowGroupPanel = false;
            this.gridView19.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView19.OptionsView.ShowIndicator = false;
            this.gridView19.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn150, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn149
            // 
            this.gridColumn149.Caption = "Visit Category";
            this.gridColumn149.FieldName = "Description";
            this.gridColumn149.Name = "gridColumn149";
            this.gridColumn149.OptionsColumn.AllowEdit = false;
            this.gridColumn149.OptionsColumn.AllowFocus = false;
            this.gridColumn149.OptionsColumn.ReadOnly = true;
            this.gridColumn149.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn149.Visible = true;
            this.gridColumn149.VisibleIndex = 0;
            this.gridColumn149.Width = 220;
            // 
            // gridColumn150
            // 
            this.gridColumn150.Caption = "Order";
            this.gridColumn150.FieldName = "RecordOrder";
            this.gridColumn150.Name = "gridColumn150";
            this.gridColumn150.OptionsColumn.AllowEdit = false;
            this.gridColumn150.OptionsColumn.AllowFocus = false;
            this.gridColumn150.OptionsColumn.ReadOnly = true;
            this.gridColumn150.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // standaloneBarDockControl7
            // 
            this.standaloneBarDockControl7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl7.CausesValidation = false;
            this.standaloneBarDockControl7.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl7.Manager = this.barManager1;
            this.standaloneBarDockControl7.Name = "standaloneBarDockControl7";
            this.standaloneBarDockControl7.Size = new System.Drawing.Size(587, 25);
            this.standaloneBarDockControl7.Text = "standaloneBarDockControl7";
            // 
            // panelControl12
            // 
            this.panelControl12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl12.Controls.Add(this.pictureEdit14);
            this.panelControl12.Controls.Add(this.labelControl27);
            this.panelControl12.Controls.Add(this.labelControl28);
            this.panelControl12.Location = new System.Drawing.Point(7, 6);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(946, 48);
            this.panelControl12.TabIndex = 25;
            // 
            // pictureEdit14
            // 
            this.pictureEdit14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit14.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit14.Location = new System.Drawing.Point(902, 4);
            this.pictureEdit14.MenuManager = this.barManager1;
            this.pictureEdit14.Name = "pictureEdit14";
            this.pictureEdit14.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit14.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit14.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit14.Properties.ReadOnly = true;
            this.pictureEdit14.Properties.ShowMenu = false;
            this.pictureEdit14.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit14.TabIndex = 9;
            // 
            // labelControl27
            // 
            this.labelControl27.AllowHtmlString = true;
            this.labelControl27.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl27.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl27.Appearance.Options.UseBackColor = true;
            this.labelControl27.Appearance.Options.UseFont = true;
            this.labelControl27.Location = new System.Drawing.Point(5, 5);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(251, 16);
            this.labelControl27.TabIndex = 6;
            this.labelControl27.Text = "<b>Step 10:</b> Create Site Job Rates <b>[Optional]</b>";
            // 
            // labelControl28
            // 
            this.labelControl28.AllowHtmlString = true;
            this.labelControl28.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl28.Appearance.Options.UseBackColor = true;
            this.labelControl28.Location = new System.Drawing.Point(57, 29);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(298, 13);
            this.labelControl28.TabIndex = 7;
            this.labelControl28.Text = "Add one or more Job Rates to the grid. Click Next when done.";
            // 
            // btnStep10Next
            // 
            this.btnStep10Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep10Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep10Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep10Next.Location = new System.Drawing.Point(865, 500);
            this.btnStep10Next.Name = "btnStep10Next";
            this.btnStep10Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep10Next.TabIndex = 23;
            this.btnStep10Next.Text = "Next";
            this.btnStep10Next.Click += new System.EventHandler(this.btnStep10Next_Click);
            // 
            // btnStep10Previous
            // 
            this.btnStep10Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep10Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep10Previous.Location = new System.Drawing.Point(771, 500);
            this.btnStep10Previous.Name = "btnStep10Previous";
            this.btnStep10Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep10Previous.TabIndex = 24;
            this.btnStep10Previous.Text = "Previous";
            this.btnStep10Previous.Click += new System.EventHandler(this.btnStep10Previous_Click);
            // 
            // xtraTabPageFinish
            // 
            this.xtraTabPageFinish.Controls.Add(this.panelControl2);
            this.xtraTabPageFinish.Controls.Add(this.groupControl1);
            this.xtraTabPageFinish.Controls.Add(this.labelControl10);
            this.xtraTabPageFinish.Controls.Add(this.btnFinish);
            this.xtraTabPageFinish.Controls.Add(this.btnFinishPrevious);
            this.xtraTabPageFinish.Controls.Add(this.pictureEdit2);
            this.xtraTabPageFinish.Name = "xtraTabPageFinish";
            this.xtraTabPageFinish.Size = new System.Drawing.Size(959, 536);
            this.xtraTabPageFinish.Tag = "99";
            this.xtraTabPageFinish.Text = "Finished";
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.pictureEdit6);
            this.panelControl2.Controls.Add(this.labelControl11);
            this.panelControl2.Controls.Add(this.labelControl12);
            this.panelControl2.Location = new System.Drawing.Point(211, 6);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(742, 48);
            this.panelControl2.TabIndex = 18;
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit6.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit6.Location = new System.Drawing.Point(698, 4);
            this.pictureEdit6.MenuManager = this.barManager1;
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit6.Properties.ReadOnly = true;
            this.pictureEdit6.Properties.ShowMenu = false;
            this.pictureEdit6.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit6.TabIndex = 19;
            // 
            // labelControl11
            // 
            this.labelControl11.AllowHtmlString = true;
            this.labelControl11.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl11.Appearance.Options.UseBackColor = true;
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(5, 1);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(240, 29);
            this.labelControl11.TabIndex = 5;
            this.labelControl11.Text = "Completing the Wizard";
            // 
            // labelControl12
            // 
            this.labelControl12.AllowHtmlString = true;
            this.labelControl12.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl12.Appearance.Options.UseBackColor = true;
            this.labelControl12.Location = new System.Drawing.Point(9, 31);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(547, 13);
            this.labelControl12.TabIndex = 15;
            this.labelControl12.Text = "You have successfully completed the Wizard. On clicking <b>Finish</b>, one or mor" +
    "e site contract records will be created.";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.checkEdit2);
            this.groupControl1.Controls.Add(this.checkEdit3);
            this.groupControl1.Controls.Add(this.checkEdit1);
            this.groupControl1.Location = new System.Drawing.Point(211, 155);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(742, 104);
            this.groupControl1.TabIndex = 16;
            this.groupControl1.Text = "Available Choices:";
            // 
            // checkEdit2
            // 
            this.checkEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit2.Location = new System.Drawing.Point(6, 51);
            this.checkEdit2.MenuManager = this.barManager1;
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit2.Properties.Caption = "Create the Site Contract(s) then <b>open</b> the <b>Visit Wizard</b> to create Vi" +
    "sits and Jobs.";
            this.checkEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit2.Properties.RadioGroupIndex = 1;
            this.checkEdit2.Size = new System.Drawing.Size(730, 19);
            this.checkEdit2.TabIndex = 4;
            this.checkEdit2.TabStop = false;
            // 
            // checkEdit3
            // 
            this.checkEdit3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit3.Location = new System.Drawing.Point(6, 76);
            this.checkEdit3.MenuManager = this.barManager1;
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit3.Properties.Caption = "Create the Site Contract(s) then <b>open</b> the <b>Site Contract Wizard</b> to c" +
    "reate further Site Contracts.";
            this.checkEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit3.Properties.RadioGroupIndex = 1;
            this.checkEdit3.Size = new System.Drawing.Size(730, 19);
            this.checkEdit3.TabIndex = 2;
            this.checkEdit3.TabStop = false;
            // 
            // checkEdit1
            // 
            this.checkEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit1.EditValue = true;
            this.checkEdit1.Location = new System.Drawing.Point(6, 26);
            this.checkEdit1.MenuManager = this.barManager1;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit1.Properties.Caption = "Create the Site Contract(s) then <b>return</b> to the <b>Site Contract Manager</b" +
    ">.";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit1.Properties.RadioGroupIndex = 1;
            this.checkEdit1.Size = new System.Drawing.Size(730, 19);
            this.checkEdit1.TabIndex = 0;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(213, 132);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(154, 13);
            this.labelControl10.TabIndex = 15;
            this.labelControl10.Text = "What would you like to do next?";
            // 
            // btnFinish
            // 
            this.btnFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinish.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnFinish.Appearance.Options.UseFont = true;
            this.btnFinish.ImageOptions.Image = global::WoodPlan5.Properties.Resources.apply_32x32;
            this.btnFinish.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnFinish.Location = new System.Drawing.Point(865, 500);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(88, 30);
            this.btnFinish.TabIndex = 12;
            this.btnFinish.Text = "Finish";
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnFinishPrevious
            // 
            this.btnFinishPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinishPrevious.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnFinishPrevious.Location = new System.Drawing.Point(771, 500);
            this.btnFinishPrevious.Name = "btnFinishPrevious";
            this.btnFinishPrevious.Size = new System.Drawing.Size(88, 30);
            this.btnFinishPrevious.TabIndex = 11;
            this.btnFinishPrevious.Text = "Previous";
            this.btnFinishPrevious.Click += new System.EventHandler(this.btnFinishPrevious_Click);
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureEdit2.EditValue = global::WoodPlan5.Properties.Resources.wizard_finish1;
            this.pictureEdit2.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit2.MenuManager = this.barManager1;
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.ShowMenu = false;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit2.Size = new System.Drawing.Size(205, 536);
            this.pictureEdit2.TabIndex = 5;
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.DisplayMember = "Description";
            this.repositoryItemGridLookUpEdit1.LookAndFeel.SkinName = "Blue";
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.NullText = "";
            this.repositoryItemGridLookUpEdit1.ValueMember = "ID";
            // 
            // repositoryItemButtonEditChooseStaff
            // 
            this.repositoryItemButtonEditChooseStaff.AutoHeight = false;
            this.repositoryItemButtonEditChooseStaff.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "Click me to open the Select Staff screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditChooseStaff.Name = "repositoryItemButtonEditChooseStaff";
            this.repositoryItemButtonEditChooseStaff.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // sp06063OMContractTypesWithBlankBindingSource
            // 
            this.sp06063OMContractTypesWithBlankBindingSource.DataMember = "sp06063_OM_Contract_Types_With_Blank";
            this.sp06063OMContractTypesWithBlankBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(890, 333);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddProfile),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCopyClientContractYearBillingProfile, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddProfileFromTemplate, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEditTemplates, true)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Custom 2";
            // 
            // bbiAddProfile
            // 
            this.bbiAddProfile.Caption = "Add";
            this.bbiAddProfile.Id = 37;
            this.bbiAddProfile.ImageOptions.ImageIndex = 0;
            this.bbiAddProfile.Name = "bbiAddProfile";
            this.bbiAddProfile.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Add Billing Profile - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to add a new Billing Profile record.\r\n\r\n<b>Note:</b> This button is only" +
    " enabled when one or more Contract Years are selected.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiAddProfile.SuperTip = superToolTip1;
            this.bbiAddProfile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddProfile_ItemClick);
            // 
            // bbiCopyClientContractYearBillingProfile
            // 
            this.bbiCopyClientContractYearBillingProfile.Caption = "Copy Client Contract Profile";
            this.bbiCopyClientContractYearBillingProfile.Id = 44;
            this.bbiCopyClientContractYearBillingProfile.ImageOptions.ImageIndex = 9;
            this.bbiCopyClientContractYearBillingProfile.Name = "bbiCopyClientContractYearBillingProfile";
            this.bbiCopyClientContractYearBillingProfile.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Copy Client Contract Profile - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = resources.GetString("toolTipItem2.Text");
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiCopyClientContractYearBillingProfile.SuperTip = superToolTip2;
            this.bbiCopyClientContractYearBillingProfile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCopyClientContractYearBillingProfile_ItemClick);
            // 
            // bbiAddProfileFromTemplate
            // 
            this.bbiAddProfileFromTemplate.Caption = "Add From Template";
            this.bbiAddProfileFromTemplate.Id = 38;
            this.bbiAddProfileFromTemplate.ImageOptions.ImageIndex = 10;
            this.bbiAddProfileFromTemplate.Name = "bbiAddProfileFromTemplate";
            this.bbiAddProfileFromTemplate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Add From Template - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to add one or more new Billing Profile records from a pre-defined templa" +
    "te.\r\n\r\n<b>Note:</b> This button is only enabled when one or more Contract Years " +
    "are selected.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiAddProfileFromTemplate.SuperTip = superToolTip3;
            this.bbiAddProfileFromTemplate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddProfileFromTemplate_ItemClick);
            // 
            // bbiEditTemplates
            // 
            this.bbiEditTemplates.Caption = "Template Manager";
            this.bbiEditTemplates.Id = 39;
            this.bbiEditTemplates.ImageOptions.ImageIndex = 11;
            this.bbiEditTemplates.Name = "bbiEditTemplates";
            this.bbiEditTemplates.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Template Manager - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to open the Template Manager screen.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiEditTemplates.SuperTip = superToolTip4;
            this.bbiEditTemplates.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEditTemplates_ItemClick);
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter
            // 
            this.sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter.ClearBeforeFill = true;
            // 
            // bar2
            // 
            this.bar2.BarName = "Person Responsibilities";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar2.FloatLocation = new System.Drawing.Point(448, 269);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddPersonResponsibility),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockAddPersonResponsibility, true)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DisableClose = true;
            this.bar2.OptionsBar.DisableCustomization = true;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.StandaloneBarDockControl = this.standaloneBarDockControl2;
            this.bar2.Text = "Custom 3";
            // 
            // bbiAddPersonResponsibility
            // 
            this.bbiAddPersonResponsibility.Caption = "Add";
            this.bbiAddPersonResponsibility.Id = 40;
            this.bbiAddPersonResponsibility.ImageOptions.ImageIndex = 0;
            this.bbiAddPersonResponsibility.Name = "bbiAddPersonResponsibility";
            this.bbiAddPersonResponsibility.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Add Person Responsibility - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to add a new Person Responsibility record.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bbiAddPersonResponsibility.SuperTip = superToolTip5;
            this.bbiAddPersonResponsibility.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddPersonResponsibility_ItemClick);
            // 
            // bbiBlockAddPersonResponsibility
            // 
            this.bbiBlockAddPersonResponsibility.Caption = "Block Add";
            this.bbiBlockAddPersonResponsibility.Id = 51;
            this.bbiBlockAddPersonResponsibility.ImageOptions.ImageIndex = 8;
            this.bbiBlockAddPersonResponsibility.Name = "bbiBlockAddPersonResponsibility";
            this.bbiBlockAddPersonResponsibility.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Block Add Person Responsibilities - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to block add multiple new Person Responsibility records.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.bbiBlockAddPersonResponsibility.SuperTip = superToolTip6;
            this.bbiBlockAddPersonResponsibility.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBlockAddPersonResponsibility_ItemClick);
            // 
            // barStep1
            // 
            this.barStep1.BarName = "Custom 4";
            this.barStep1.DockCol = 0;
            this.barStep1.DockRow = 0;
            this.barStep1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.barStep1.FloatLocation = new System.Drawing.Point(480, 247);
            this.barStep1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemDateRange),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemActive),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefreshClientContracts, true)});
            this.barStep1.OptionsBar.AllowQuickCustomization = false;
            this.barStep1.OptionsBar.DrawDragBorder = false;
            this.barStep1.OptionsBar.UseWholeRow = true;
            this.barStep1.StandaloneBarDockControl = this.standaloneBarDockControl3;
            this.barStep1.Text = "Custom 4";
            // 
            // barEditItemDateRange
            // 
            this.barEditItemDateRange.Caption = "Date Range:";
            this.barEditItemDateRange.Edit = this.repositoryItemPopupContainerEditDateRange;
            this.barEditItemDateRange.EditValue = "No Date Range Filter";
            this.barEditItemDateRange.EditWidth = 238;
            this.barEditItemDateRange.Id = 41;
            this.barEditItemDateRange.Name = "barEditItemDateRange";
            this.barEditItemDateRange.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEditDateRange
            // 
            this.repositoryItemPopupContainerEditDateRange.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateRange.Name = "repositoryItemPopupContainerEditDateRange";
            this.repositoryItemPopupContainerEditDateRange.PopupControl = this.popupContainerControlDateRange;
            this.repositoryItemPopupContainerEditDateRange.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateRange_QueryResultValue);
            // 
            // barEditItemActive
            // 
            this.barEditItemActive.Caption = "Active:";
            this.barEditItemActive.Edit = this.repositoryItemCheckEdit4;
            this.barEditItemActive.EditValue = 1;
            this.barEditItemActive.EditWidth = 20;
            this.barEditItemActive.Id = 43;
            this.barEditItemActive.Name = "barEditItemActive";
            this.barEditItemActive.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "";
            this.repositoryItemCheckEdit4.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // bbiRefreshClientContracts
            // 
            this.bbiRefreshClientContracts.Caption = "Load Client Contracts";
            this.bbiRefreshClientContracts.Id = 42;
            this.bbiRefreshClientContracts.ImageOptions.ImageIndex = 6;
            this.bbiRefreshClientContracts.Name = "bbiRefreshClientContracts";
            this.bbiRefreshClientContracts.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefreshClientContracts.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefreshClientContracts_ItemClick);
            // 
            // sp06086_OM_Site_Contract_Wizard_Client_ContractsTableAdapter
            // 
            this.sp06086_OM_Site_Contract_Wizard_Client_ContractsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06089_OM_Sites_For_ClientTableAdapter
            // 
            this.sp06089_OM_Sites_For_ClientTableAdapter.ClearBeforeFill = true;
            // 
            // sp06090_OM_Site_Contract_EditTableAdapter
            // 
            this.sp06090_OM_Site_Contract_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp06092_OM_Site_Contract_Year_EditTableAdapter
            // 
            this.sp06092_OM_Site_Contract_Year_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp06095_OM_Site_Contract_Year_Billing_Profile_EditTableAdapter
            // 
            this.sp06095_OM_Site_Contract_Year_Billing_Profile_EditTableAdapter.ClearBeforeFill = true;
            // 
            // bar3
            // 
            this.bar3.BarName = "Custom 5";
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar3.FloatLocation = new System.Drawing.Point(602, 328);
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddLabourCost),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockAddLabourCost, true)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DisableClose = true;
            this.bar3.OptionsBar.DisableCustomization = true;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.StandaloneBarDockControl = this.standaloneBarDockControl4;
            this.bar3.Text = "Custom 5";
            // 
            // bbiAddLabourCost
            // 
            this.bbiAddLabourCost.Caption = "Add";
            this.bbiAddLabourCost.Id = 45;
            this.bbiAddLabourCost.ImageOptions.ImageIndex = 0;
            this.bbiAddLabourCost.Name = "bbiAddLabourCost";
            this.bbiAddLabourCost.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Add Preferred Labour Rate - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to add a new Default Preferred Labour record.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.bbiAddLabourCost.SuperTip = superToolTip7;
            this.bbiAddLabourCost.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddLabourCost_ItemClick);
            // 
            // bbiBlockAddLabourCost
            // 
            this.bbiBlockAddLabourCost.Caption = "Block Add";
            this.bbiBlockAddLabourCost.Id = 48;
            this.bbiBlockAddLabourCost.ImageOptions.ImageIndex = 8;
            this.bbiBlockAddLabourCost.Name = "bbiBlockAddLabourCost";
            this.bbiBlockAddLabourCost.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Text = "Block Add Preferred Labour Rate - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to block add multiple new Default Preferred Labour records.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bbiBlockAddLabourCost.SuperTip = superToolTip8;
            this.bbiBlockAddLabourCost.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBlockAddLabourCost_ItemClick);
            // 
            // bar4
            // 
            this.bar4.BarName = "Custom 6";
            this.bar4.DockCol = 0;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar4.FloatLocation = new System.Drawing.Point(564, 289);
            this.bar4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddEquipmentRate),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockAddEquipmentRate, true)});
            this.bar4.OptionsBar.AllowQuickCustomization = false;
            this.bar4.OptionsBar.DisableClose = true;
            this.bar4.OptionsBar.DisableCustomization = true;
            this.bar4.OptionsBar.DrawDragBorder = false;
            this.bar4.OptionsBar.UseWholeRow = true;
            this.bar4.StandaloneBarDockControl = this.standaloneBarDockControl5;
            this.bar4.Text = "Custom 6";
            // 
            // bbiAddEquipmentRate
            // 
            this.bbiAddEquipmentRate.Caption = "Add";
            this.bbiAddEquipmentRate.Id = 46;
            this.bbiAddEquipmentRate.ImageOptions.ImageIndex = 0;
            this.bbiAddEquipmentRate.Name = "bbiAddEquipmentRate";
            this.bbiAddEquipmentRate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem9.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Text = "Add Equipment Rate - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Click me to add a new Equipment Rate record.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.bbiAddEquipmentRate.SuperTip = superToolTip9;
            this.bbiAddEquipmentRate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddEquipmentCost_ItemClick);
            // 
            // bbiBlockAddEquipmentRate
            // 
            this.bbiBlockAddEquipmentRate.Caption = "Block Add";
            this.bbiBlockAddEquipmentRate.Id = 49;
            this.bbiBlockAddEquipmentRate.ImageOptions.ImageIndex = 8;
            this.bbiBlockAddEquipmentRate.Name = "bbiBlockAddEquipmentRate";
            this.bbiBlockAddEquipmentRate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem10.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Text = "Block Add Default Equipment Rates - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Click me to block add multiple new Default Equipment Rate records.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.bbiBlockAddEquipmentRate.SuperTip = superToolTip10;
            this.bbiBlockAddEquipmentRate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBlockAddEquipmentCost_ItemClick);
            // 
            // bar5
            // 
            this.bar5.BarName = "Custom 7";
            this.bar5.DockCol = 0;
            this.bar5.DockRow = 0;
            this.bar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar5.FloatLocation = new System.Drawing.Point(539, 290);
            this.bar5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddMaterialRate),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockAddMaterialRate, true)});
            this.bar5.OptionsBar.AllowQuickCustomization = false;
            this.bar5.OptionsBar.DisableClose = true;
            this.bar5.OptionsBar.DisableCustomization = true;
            this.bar5.OptionsBar.DrawDragBorder = false;
            this.bar5.OptionsBar.UseWholeRow = true;
            this.bar5.StandaloneBarDockControl = this.standaloneBarDockControl6;
            this.bar5.Text = "Custom 7";
            // 
            // bbiAddMaterialRate
            // 
            this.bbiAddMaterialRate.Caption = "Add";
            this.bbiAddMaterialRate.Id = 47;
            this.bbiAddMaterialRate.ImageOptions.ImageIndex = 0;
            this.bbiAddMaterialRate.Name = "bbiAddMaterialRate";
            this.bbiAddMaterialRate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem11.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Appearance.Options.UseImage = true;
            toolTipTitleItem11.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Text = "Add Material Rate - Information";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Click me to add a new Material Rate record.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.bbiAddMaterialRate.SuperTip = superToolTip11;
            this.bbiAddMaterialRate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddMaterialCost_ItemClick);
            // 
            // bbiBlockAddMaterialRate
            // 
            this.bbiBlockAddMaterialRate.Caption = "Block Add";
            this.bbiBlockAddMaterialRate.Id = 50;
            this.bbiBlockAddMaterialRate.ImageOptions.ImageIndex = 8;
            this.bbiBlockAddMaterialRate.Name = "bbiBlockAddMaterialRate";
            this.bbiBlockAddMaterialRate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem12.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem12.Appearance.Options.UseImage = true;
            toolTipTitleItem12.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem12.Text = "Block Add Default Material Rates - Information";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Click me to block add multiple new Default Material Rate records.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            this.bbiBlockAddMaterialRate.SuperTip = superToolTip12;
            this.bbiBlockAddMaterialRate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBlockAddMaterialCost_ItemClick);
            // 
            // sp06098_OM_Site_Contract_Labour_Cost_EditTableAdapter
            // 
            this.sp06098_OM_Site_Contract_Labour_Cost_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp06101_OM_Work_Unit_Types_PicklistTableAdapter
            // 
            this.sp06101_OM_Work_Unit_Types_PicklistTableAdapter.ClearBeforeFill = true;
            // 
            // sp06103_OM_Site_Contract_Equipment_Cost_EditTableAdapter
            // 
            this.sp06103_OM_Site_Contract_Equipment_Cost_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp06106_OM_Site_Contract_Material_Cost_EditTableAdapter
            // 
            this.sp06106_OM_Site_Contract_Material_Cost_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp06108_OM_Unit_Descriptors_PicklistTableAdapter
            // 
            this.sp06108_OM_Unit_Descriptors_PicklistTableAdapter.ClearBeforeFill = true;
            // 
            // bsiRecordTicking
            // 
            this.bsiRecordTicking.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiRecordTicking.Caption = "Record <b>Ticking</b>";
            this.bsiRecordTicking.Id = 52;
            this.bsiRecordTicking.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiRecordTicking.ImageOptions.Image")));
            this.bsiRecordTicking.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTick),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUntick)});
            this.bsiRecordTicking.Name = "bsiRecordTicking";
            // 
            // bbiTick
            // 
            this.bbiTick.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiTick.Border = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.bbiTick.Caption = "<b>Tick</b> Selected Records";
            this.bbiTick.Id = 53;
            this.bbiTick.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiTick.ImageOptions.Image")));
            this.bbiTick.Name = "bbiTick";
            this.bbiTick.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTick_ItemClick);
            // 
            // bbiUntick
            // 
            this.bbiUntick.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiUntick.Caption = "<b>Untick</b> Selected Records";
            this.bbiUntick.Id = 54;
            this.bbiUntick.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUntick.ImageOptions.Image")));
            this.bbiUntick.Name = "bbiUntick";
            this.bbiUntick.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUntick_ItemClick);
            // 
            // bar6
            // 
            this.bar6.BarName = "Custom 8";
            this.bar6.DockCol = 0;
            this.bar6.DockRow = 0;
            this.bar6.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar6.FloatLocation = new System.Drawing.Point(824, 292);
            this.bar6.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddJobRate),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockAddJobRate, true)});
            this.bar6.OptionsBar.AllowQuickCustomization = false;
            this.bar6.OptionsBar.DisableClose = true;
            this.bar6.OptionsBar.DisableCustomization = true;
            this.bar6.OptionsBar.DrawDragBorder = false;
            this.bar6.OptionsBar.UseWholeRow = true;
            this.bar6.StandaloneBarDockControl = this.standaloneBarDockControl7;
            this.bar6.Text = "Custom 8";
            // 
            // bbiAddJobRate
            // 
            this.bbiAddJobRate.Caption = "Add";
            this.bbiAddJobRate.Id = 55;
            this.bbiAddJobRate.ImageOptions.ImageIndex = 0;
            this.bbiAddJobRate.Name = "bbiAddJobRate";
            this.bbiAddJobRate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem13.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem13.Appearance.Options.UseImage = true;
            toolTipTitleItem13.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem13.Text = "Add Job Rate - Information";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "Click me to add a new Job Rate record.";
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            this.bbiAddJobRate.SuperTip = superToolTip13;
            this.bbiAddJobRate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddJobRate_ItemClick);
            // 
            // bbiBlockAddJobRate
            // 
            this.bbiBlockAddJobRate.Caption = "Block Add";
            this.bbiBlockAddJobRate.Id = 56;
            this.bbiBlockAddJobRate.ImageOptions.ImageIndex = 8;
            this.bbiBlockAddJobRate.Name = "bbiBlockAddJobRate";
            this.bbiBlockAddJobRate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem14.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem14.Appearance.Options.UseImage = true;
            toolTipTitleItem14.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem14.Text = "Block Add Job Rates - Information";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Click me to block add multiple new Job Rate records.";
            superToolTip14.Items.Add(toolTipTitleItem14);
            superToolTip14.Items.Add(toolTipItem14);
            this.bbiBlockAddJobRate.SuperTip = superToolTip14;
            this.bbiBlockAddJobRate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBlockAddJobRate_ItemClick);
            // 
            // transitionManager1
            // 
            this.transitionManager1.FrameInterval = 5000;
            transition1.Control = this.xtraTabControl1;
            transition1.EasingMode = DevExpress.Data.Utils.EasingMode.EaseInOut;
            transition1.WaitingIndicatorProperties.Caption = "Loading...";
            this.transitionManager1.Transitions.Add(transition1);
            // 
            // sp06346_OM_Site_Contract_Job_Rate_EditTableAdapter
            // 
            this.sp06346_OM_Site_Contract_Job_Rate_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp06355_OM_Client_Billing_Types_With_BlankTableAdapter
            // 
            this.sp06355_OM_Client_Billing_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter
            // 
            this.sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // timerWelcomePage
            // 
            this.timerWelcomePage.Interval = 2000;
            this.timerWelcomePage.Tick += new System.EventHandler(this.timerWelcomePage_Tick);
            // 
            // sp06250_OM_Visit_Categories_With_BlankTableAdapter
            // 
            this.sp06250_OM_Visit_Categories_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // checkEditSkipEquipment
            // 
            this.checkEditSkipEquipment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditSkipEquipment.EditValue = true;
            this.checkEditSkipEquipment.Location = new System.Drawing.Point(186, 505);
            this.checkEditSkipEquipment.MenuManager = this.barManager1;
            this.checkEditSkipEquipment.Name = "checkEditSkipEquipment";
            this.checkEditSkipEquipment.Properties.Caption = "Skip Equipment Step:";
            this.checkEditSkipEquipment.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditSkipEquipment.Size = new System.Drawing.Size(122, 19);
            this.checkEditSkipEquipment.TabIndex = 25;
            // 
            // checkEditSkipMaterials
            // 
            this.checkEditSkipMaterials.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditSkipMaterials.EditValue = true;
            this.checkEditSkipMaterials.Location = new System.Drawing.Point(317, 505);
            this.checkEditSkipMaterials.MenuManager = this.barManager1;
            this.checkEditSkipMaterials.Name = "checkEditSkipMaterials";
            this.checkEditSkipMaterials.Properties.Caption = "Skip Materials Step:";
            this.checkEditSkipMaterials.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditSkipMaterials.Size = new System.Drawing.Size(115, 19);
            this.checkEditSkipMaterials.TabIndex = 26;
            // 
            // frm_OM_Site_Contract_Wizard
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(982, 537);
            this.Controls.Add(this.xtraTabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Site_Contract_Wizard";
            this.Text = "Site Contract Wizard - Operations";
            this.Activated += new System.EventHandler(this.frm_OM_Site_Contract_Wizard_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Site_Contract_Wizard_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Site_Contract_Wizard_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageWelcome.ResumeLayout(false);
            this.xtraTabPageWelcome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.xtraTabPageStep1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06086OMSiteContractWizardClientContractsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).EndInit();
            this.xtraTabPageStep2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditCreateGenericJobRates.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06089OMSitesForClientBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            this.xtraTabPageStep3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06090OMSiteContractEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditClientBillingTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06355OMClientBillingTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit4View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditDefaultScheduleTemplateID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06141OMVisitTemplateHeadersWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit3View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.panelControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentContractValueTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit10.Properties)).EndInit();
            this.xtraTabPageStep4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06092OMSiteContractYearEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit50CharsMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercentage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditContractYears.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForYearsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITemForAddYearsToGridButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCopyClientContractYears)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).EndInit();
            this.xtraTabPageStep5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06095OMSiteContractYearBillingProfileEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).EndInit();
            this.xtraTabPageStep6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06098OMSiteContractLabourCostEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditUnitDescriptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06101OMWorkUnitTypesPicklistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditMiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditLabourType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            this.panelControl11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit13.Properties)).EndInit();
            this.xtraTabPageStep7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06103OMSiteContractEquipmentCostEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditCostUnitDescriptors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCurrency3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercentage3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseEquipment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.panelControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit11.Properties)).EndInit();
            this.xtraTabPageStep8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).EndInit();
            this.splitContainerControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06106OMSiteContractMaterialCostEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06108OMUnitDescriptorsPicklistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseMaterial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            this.panelControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit12.Properties)).EndInit();
            this.xtraTabPageStep9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).EndInit();
            this.splitContainerControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditPersonResponsibilityType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChoosePerson)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.panelControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit9.Properties)).EndInit();
            this.xtraTabPageStep10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl7)).EndInit();
            this.splitContainerControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06346OMSiteContractJobRateEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditJobSubType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditVisitCategoryID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06250OMVisitCategoriesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            this.panelControl12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit14.Properties)).EndInit();
            this.xtraTabPageFinish.ResumeLayout(false);
            this.xtraTabPageFinish.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseStaff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06063OMContractTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSkipEquipment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSkipMaterials.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageWelcome;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep1;
        private DevExpress.XtraEditors.SimpleButton btnWelcomeNext;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageFinish;
        private DevExpress.XtraEditors.SimpleButton btnStep1Previous;
        private DevExpress.XtraEditors.SimpleButton btnStep1Next;
        private DevExpress.XtraEditors.SimpleButton btnStep2Next;
        private DevExpress.XtraEditors.SimpleButton btnStep2Previous;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.SimpleButton btnFinish;
        private DevExpress.XtraEditors.SimpleButton btnFinishPrevious;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private System.Windows.Forms.BindingSource sp06063OMContractTypesWithBlankBindingSource;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep4;
        private DevExpress.XtraEditors.SimpleButton btnStep4Next;
        private DevExpress.XtraEditors.SimpleButton btnStep4Previous;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit7;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.SpinEdit spinEditContractYears;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForYearsGrid;
        private DevExpress.XtraEditors.SimpleButton btnAddYearsBtn;
        private DevExpress.XtraLayout.LayoutControlItem ITemForAddYearsToGridButton;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractYearID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colYearDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit50CharsMax;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep5;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.PictureEdit pictureEdit8;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep9;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PictureEdit pictureEdit9;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.SimpleButton btnStep5Next;
        private DevExpress.XtraEditors.SimpleButton btnStep5Previous;
        private DevExpress.XtraEditors.SimpleButton btnStep9Next;
        private DevExpress.XtraEditors.SimpleButton btnStep9Previous;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractProfileID;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceDateDue;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceDateActual;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedBillAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colActualBillAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colBilledByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiAddProfile;
        private DevExpress.XtraBars.BarButtonItem bbiAddProfileFromTemplate;
        private DevExpress.XtraBars.BarButtonItem bbiEditTemplates;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditCurrency;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private System.Windows.Forms.BindingSource sp06081OMClientContractLinkedResponsibilitiesEditBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonResponsibilityID;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibleForRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibleForRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibilityTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colContractEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent1;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibilityType;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffName;
        private DataSet_OM_ContractTableAdapters.sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl2;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiAddPersonResponsibility;
        private DevExpress.XtraBars.Bar barStep1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl3;
        private DevExpress.XtraBars.BarEditItem barEditItemDateRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateRange;
        private DevExpress.XtraBars.BarButtonItem bbiRefreshClientContracts;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeOK;
        private DevExpress.XtraBars.BarEditItem barEditItemActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private System.Windows.Forms.BindingSource sp06086OMSiteContractWizardClientContractsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colGCCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDirectorID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime3;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colActive1;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency3;
        private DevExpress.XtraGrid.Columns.GridColumn colYearlyPercentageIncrease;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage3;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckRPIDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLastClientPaymentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPreviousContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colGCCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colContractType;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDirector;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorType;
        private DataSet_OM_ContractTableAdapters.sp06086_OM_Site_Contract_Wizard_Client_ContractsTableAdapter sp06086_OM_Site_Contract_Wizard_Client_ContractsTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private System.Windows.Forms.BindingSource sp06089OMSitesForClientBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks4;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteType;
        private DataSet_OM_ContractTableAdapters.sp06089_OM_Sites_For_ClientTableAdapter sp06089_OM_Sites_For_ClientTableAdapter;
        private DevExpress.XtraEditors.LabelControl labelControlSelectedSiteCount;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep3;
        private DevExpress.XtraEditors.SimpleButton btnStep3Next;
        private DevExpress.XtraEditors.SimpleButton btnStep3Previous;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.PictureEdit pictureEdit10;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep7;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.PictureEdit pictureEdit11;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep8;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.PictureEdit pictureEdit12;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep6;
        private DevExpress.XtraEditors.SimpleButton btnStep6Next;
        private DevExpress.XtraEditors.SimpleButton btnStep6Previous;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.PictureEdit pictureEdit13;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.SimpleButton btnStep7Next;
        private DevExpress.XtraEditors.SimpleButton btnStep7Previous;
        private DevExpress.XtraEditors.SimpleButton btnStep8Next;
        private DevExpress.XtraEditors.SimpleButton btnStep8Previous;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private System.Windows.Forms.BindingSource sp06090OMSiteContractEditBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID1;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colActive2;
        private DevExpress.XtraGrid.Columns.GridColumn colContractValue1;
        private DevExpress.XtraGrid.Columns.GridColumn colLastClientPaymentDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks5;
        private DevExpress.XtraGrid.Columns.GridColumn colYearlyPercentageIncrease1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colDummySiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DataSet_OM_ContractTableAdapters.sp06090_OM_Site_Contract_EditTableAdapter sp06090_OM_Site_Contract_EditTableAdapter;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditChooseStaff;
        private System.Windows.Forms.BindingSource sp06092OMSiteContractYearEditBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06092_OM_Site_Contract_Year_EditTableAdapter sp06092_OM_Site_Contract_Year_EditTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName2;
        private DevExpress.XtraEditors.SimpleButton CopyClientContractYearsSimpleButton;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCopyClientContractYears;
        private DevExpress.XtraGrid.Columns.GridColumn colClientYearCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.TextEdit CurrentContractValueTextEdit;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private System.Windows.Forms.BindingSource sp06095OMSiteContractYearBillingProfileEditBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06095_OM_Site_Contract_Year_Billing_Profile_EditTableAdapter sp06095_OM_Site_Contract_Year_Billing_Profile_EditTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName3;
        private DevExpress.XtraBars.BarButtonItem bbiCopyClientContractYearBillingProfile;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromClientContractYearID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromClientContractProfileID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromClientContractYear;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromClientContractProfile;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractYearID;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarButtonItem bbiAddLabourCost;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl4;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraBars.BarButtonItem bbiAddEquipmentRate;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl5;
        private DevExpress.XtraBars.Bar bar5;
        private DevExpress.XtraBars.BarButtonItem bbiAddMaterialRate;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl6;
        private DevExpress.XtraGrid.GridControl gridControl10;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private System.Windows.Forms.BindingSource sp06098OMSiteContractLabourCostEditBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractLabourCostID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID2;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSellUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnitExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnitVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnitExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnitVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colCISPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colCISValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcodeSiteDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colLatLongSiteDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks6;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName3;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName4;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DataSet_OM_ContractTableAdapters.sp06098_OM_Site_Contract_Labour_Cost_EditTableAdapter sp06098_OM_Site_Contract_Labour_Cost_EditTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditUnitDescriptor;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEditGridViewCostUnitDescriptor;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditMoney;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditPercent;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditMiles;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditChooseContractor;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraGrid.GridControl gridControl9;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private System.Windows.Forms.BindingSource sp06101OMWorkUnitTypesPicklistBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06101_OM_Work_Unit_Types_PicklistTableAdapter sp06101_OM_Work_Unit_Types_PicklistTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationX1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationY1;
        private DevExpress.XtraBars.BarButtonItem bbiBlockAddLabourCost;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraGrid.GridControl gridControl11;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.GridControl gridControl12;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView12;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditCurrency3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit12;
        private DevExpress.XtraBars.BarButtonItem bbiBlockAddEquipmentRate;
        private System.Windows.Forms.BindingSource sp06103OMSiteContractEquipmentCostEditBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06103_OM_Site_Contract_Equipment_Cost_EditTableAdapter sp06103_OM_Site_Contract_Equipment_Cost_EditTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitDescriptorID2;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditCostUnitDescriptors;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit2View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn64;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn65;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn66;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn71;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn72;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn73;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn74;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn75;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn76;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerName;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerType;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditPercentage3;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditChooseEquipment;
        private DevExpress.XtraBars.BarButtonItem bbiBlockAddMaterialRate;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl5;
        private DevExpress.XtraGrid.GridControl gridControl13;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn67;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn68;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn69;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn70;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn77;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn78;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn79;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn80;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn81;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn82;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn83;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn84;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn85;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn86;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn87;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn88;
        private DevExpress.XtraGrid.GridControl gridControl14;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn89;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn90;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn91;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitDescriptor3;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn93;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn94;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn95;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn96;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn97;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn98;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn99;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn100;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn101;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn102;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn103;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn104;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn105;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn106;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditChooseMaterial;
        private System.Windows.Forms.BindingSource sp06106OMSiteContractMaterialCostEditBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06106_OM_Site_Contract_Material_Cost_EditTableAdapter sp06106_OM_Site_Contract_Material_Cost_EditTableAdapter;
        private System.Windows.Forms.BindingSource sp06108OMUnitDescriptorsPicklistBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06108_OM_Unit_Descriptors_PicklistTableAdapter sp06108_OM_Unit_Descriptors_PicklistTableAdapter;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl6;
        private DevExpress.XtraGrid.GridControl gridControl15;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn92;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn107;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn108;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn109;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn110;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn111;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn112;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn113;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn114;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn115;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn116;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn117;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn118;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn119;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn120;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn121;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn122;
        private DevExpress.XtraBars.BarButtonItem bbiBlockAddPersonResponsibility;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditChoosePerson;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraGrid.Columns.GridColumn colYearlyPercentageIncrease2;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditPercentage2;
        private DevExpress.XtraEditors.SimpleButton btnSplitValueOverSites;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteInstructions;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyEquipmentCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger2;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyLabourCount;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyMaterialCount;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyEquipmentCount1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger3;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyLabourCount1;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyMaterialCount1;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyEquipmentCount2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger4;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyLabourCount2;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyMaterialCount2;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyPersonResponsibilityCount;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyPersonResponsibilityCount1;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyPersonResponsibilityCount2;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyEquipmentCount3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger5;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyLabourCount3;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyMaterialCount3;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyPersonResponsibilityCount3;
        private DevExpress.XtraBars.BarSubItem bsiRecordTicking;
        private DevExpress.XtraBars.BarButtonItem bbiTick;
        private DevExpress.XtraBars.BarButtonItem bbiUntick;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditLabourType;
        private DevExpress.XtraGrid.Columns.GridColumn colActiveSiteContractCount;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep10;
        private DevExpress.XtraEditors.SimpleButton btnStep10Next;
        private DevExpress.XtraEditors.SimpleButton btnStep10Previous;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.PictureEdit pictureEdit14;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl7;
        private DevExpress.XtraGrid.GridControl gridControl17;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn126;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn127;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn128;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn129;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn130;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn131;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn132;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn133;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn134;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn135;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn136;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn137;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn138;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn139;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn140;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn141;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn142;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn143;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn144;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn145;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn146;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyJobRateCount;
        private DevExpress.XtraBars.Bar bar6;
        private DevExpress.XtraBars.BarButtonItem bbiAddJobRate;
        private DevExpress.XtraBars.BarButtonItem bbiBlockAddJobRate;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl7;
        private DevExpress.Utils.Animation.TransitionManager transitionManager1;
        private DevExpress.XtraGrid.GridControl gridControl18;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn148;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn159;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn160;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn161;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn162;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn163;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditJobSubType;
        private System.Windows.Forms.BindingSource sp06346OMSiteContractJobRateEditBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractJobRateID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobType;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubType;
        private DevExpress.XtraGrid.Columns.GridColumn colFromDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colToDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamCost;
        private DevExpress.XtraGrid.Columns.GridColumn colClientSell;
        private DataSet_OM_ContractTableAdapters.sp06346_OM_Site_Contract_Job_Rate_EditTableAdapter sp06346_OM_Site_Contract_Job_Rate_EditTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDate18;
        private DevExpress.XtraEditors.CheckEdit checkEditCreateGenericJobRates;
        private DevExpress.XtraGrid.Columns.GridColumn colClientBillingTypeID;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditClientBillingTypeID;
        private System.Windows.Forms.BindingSource sp06355OMClientBillingTypesWithBlankBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit4View;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DataSet_OM_ContractTableAdapters.sp06355_OM_Client_Billing_Types_With_BlankTableAdapter sp06355_OM_Client_Billing_Types_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCategory;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditPersonResponsibilityType;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultScheduleTemplateID;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditDefaultScheduleTemplateID;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit3View;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private System.Windows.Forms.BindingSource sp06141OMVisitTemplateHeadersWithBlankBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn123;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn124;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn125;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysSeparationPercent;
        private System.Windows.Forms.Timer timerWelcomePage;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCategoryID;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditVisitCategoryID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView19;
        private System.Windows.Forms.BindingSource sp06250OMVisitCategoriesWithBlankBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn147;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn149;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn150;
        private DataSet_OM_VisitTableAdapters.sp06250_OM_Visit_Categories_With_BlankTableAdapter sp06250_OM_Visit_Categories_With_BlankTableAdapter;
        private DevExpress.XtraEditors.CheckEdit checkEditSkipMaterials;
        private DevExpress.XtraEditors.CheckEdit checkEditSkipEquipment;
    }
}

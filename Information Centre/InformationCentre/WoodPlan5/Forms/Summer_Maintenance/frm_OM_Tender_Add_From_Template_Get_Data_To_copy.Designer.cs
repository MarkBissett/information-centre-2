﻿namespace WoodPlan5
{
    partial class frm_OM_Tender_Add_From_Template_Get_Data_To_copy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Tender_Add_From_Template_Get_Data_To_copy));
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bsiInformation = new DevExpress.XtraBars.BarStaticItem();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditJobData = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditSiteData = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditClientData = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditLabourData = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditJobData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSiteData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditClientData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditLabourData.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(374, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 193);
            this.barDockControlBottom.Size = new System.Drawing.Size(374, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 167);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(374, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 167);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Images = this.imageCollection1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bsiInformation});
            this.barManager1.MaxItemId = 32;
            this.barManager1.StatusBar = this.bar1;
            // 
            // btnOK
            // 
            this.btnOK.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.ImageIndex = 1;
            this.btnOK.ImageList = this.imageCollection1;
            this.btnOK.Location = new System.Drawing.Point(203, 165);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(78, 22);
            this.btnOK.TabIndex = 13;
            this.btnOK.Text = "<b>OK</b>";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Info_16x16.png");
            this.imageCollection1.InsertGalleryImage("apply_16x16.png", "images/actions/apply_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "apply_16x16.png");
            this.imageCollection1.InsertGalleryImage("cancel_16x16.png", "images/actions/cancel_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/cancel_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "cancel_16x16.png");
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.ImageIndex = 2;
            this.btnCancel.ImageList = this.imageCollection1;
            this.btnCancel.Location = new System.Drawing.Point(287, 164);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(78, 22);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiInformation)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 3";
            // 
            // bsiInformation
            // 
            this.bsiInformation.Caption = "Tick the Data to be Copied";
            this.bsiInformation.Id = 30;
            this.bsiInformation.ImageIndex = 0;
            this.bsiInformation.Name = "bsiInformation";
            this.bsiInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // groupControl4
            // 
            this.groupControl4.AllowHtmlText = true;
            this.groupControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl4.Controls.Add(this.checkEditLabourData);
            this.groupControl4.Controls.Add(this.checkEditJobData);
            this.groupControl4.Controls.Add(this.checkEditSiteData);
            this.groupControl4.Controls.Add(this.checkEditClientData);
            this.groupControl4.Location = new System.Drawing.Point(9, 34);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(356, 124);
            this.groupControl4.TabIndex = 22;
            this.groupControl4.Text = "Data To <b>Copy</b> from Tender: ";
            // 
            // checkEditJobData
            // 
            this.checkEditJobData.EditValue = true;
            this.checkEditJobData.Location = new System.Drawing.Point(5, 74);
            this.checkEditJobData.MenuManager = this.barManager1;
            this.checkEditJobData.Name = "checkEditJobData";
            this.checkEditJobData.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditJobData.Properties.Caption = "<b>Job</b> Data";
            this.checkEditJobData.Size = new System.Drawing.Size(142, 19);
            this.checkEditJobData.TabIndex = 23;
            // 
            // checkEditSiteData
            // 
            this.checkEditSiteData.EditValue = true;
            this.checkEditSiteData.Location = new System.Drawing.Point(21, 51);
            this.checkEditSiteData.MenuManager = this.barManager1;
            this.checkEditSiteData.Name = "checkEditSiteData";
            this.checkEditSiteData.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditSiteData.Properties.Caption = "<b>Site</b> Data";
            this.checkEditSiteData.Size = new System.Drawing.Size(126, 19);
            this.checkEditSiteData.TabIndex = 6;
            // 
            // checkEditClientData
            // 
            this.checkEditClientData.EditValue = true;
            this.checkEditClientData.Location = new System.Drawing.Point(5, 28);
            this.checkEditClientData.MenuManager = this.barManager1;
            this.checkEditClientData.Name = "checkEditClientData";
            this.checkEditClientData.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditClientData.Properties.Caption = "<b>Client</b> Data";
            this.checkEditClientData.Size = new System.Drawing.Size(142, 19);
            this.checkEditClientData.TabIndex = 5;
            this.checkEditClientData.CheckedChanged += new System.EventHandler(this.checkEditClientData_CheckedChanged);
            // 
            // checkEditLabourData
            // 
            this.checkEditLabourData.EditValue = true;
            this.checkEditLabourData.Location = new System.Drawing.Point(5, 97);
            this.checkEditLabourData.MenuManager = this.barManager1;
            this.checkEditLabourData.Name = "checkEditLabourData";
            this.checkEditLabourData.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditLabourData.Properties.Caption = "<b>Proposed Labour</b> Data";
            this.checkEditLabourData.Size = new System.Drawing.Size(142, 19);
            this.checkEditLabourData.TabIndex = 24;
            // 
            // frm_OM_Tender_Add_From_Template_Get_Data_To_copy
            // 
            this.ClientSize = new System.Drawing.Size(374, 223);
            this.ControlBox = false;
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OM_Tender_Add_From_Template_Get_Data_To_copy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Tender From Template - Specify Data To Copy";
            this.Load += new System.EventHandler(this.frm_OM_Tender_Add_From_Template_Get_Data_To_copy_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.groupControl4, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditJobData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSiteData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditClientData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditLabourData.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem bsiInformation;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.CheckEdit checkEditSiteData;
        private DevExpress.XtraEditors.CheckEdit checkEditClientData;
        private DevExpress.XtraEditors.CheckEdit checkEditJobData;
        private DevExpress.XtraEditors.CheckEdit checkEditLabourData;
    }
}

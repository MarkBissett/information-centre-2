﻿namespace WoodPlan5
{
    partial class frm_OM_Site_Contract_Labour_Reassign
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Site_Contract_Labour_Reassign));
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemButtonEditChooseStaff = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridControl10 = new DevExpress.XtraGrid.GridControl();
            this.sp06292OMSiteContractLabourReassignListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteContractLabourCostID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditUnitDescriptor = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06101OMWorkUnitTypesPicklistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostPerUnitExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditMoney = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colCostPerUnitVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditPercent = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colSellPerUnitExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellPerUnitVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCISPercentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCISValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcodeSiteDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditMiles = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colLatLongSiteDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditChooseContractor = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colOriginalContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginalContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginalLinkedToPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginalLinkedToPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditChoosePersonType = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colLinkedToPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit13 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.sp06063OMContractTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bsiSaltUsed = new DevExpress.XtraBars.BarSubItem();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.sp06101_OM_Work_Unit_Types_PicklistTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06101_OM_Work_Unit_Types_PicklistTableAdapter();
            this.bsiRecordTicking = new DevExpress.XtraBars.BarSubItem();
            this.bbiTick = new DevExpress.XtraBars.BarButtonItem();
            this.bbiUntick = new DevExpress.XtraBars.BarButtonItem();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.CancelButton = new DevExpress.XtraEditors.SimpleButton();
            this.sp06292_OM_Site_Contract_Labour_Reassign_ListTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06292_OM_Site_Contract_Labour_Reassign_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseStaff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06292OMSiteContractLabourReassignListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditUnitDescriptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06101OMWorkUnitTypesPicklistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditMiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChoosePersonType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06063OMContractTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiRecordTicking, true)});
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(986, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 466);
            this.barDockControlBottom.Size = new System.Drawing.Size(986, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 466);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(986, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 466);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bsiSaltUsed,
            this.bsiRecordTicking,
            this.bbiTick,
            this.bbiUntick});
            this.barManager1.MaxItemId = 55;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit4});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "ID";
            this.gridColumn38.FieldName = "ID";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Width = 53;
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.DisplayMember = "Description";
            this.repositoryItemGridLookUpEdit1.LookAndFeel.SkinName = "Blue";
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.NullText = "";
            this.repositoryItemGridLookUpEdit1.ValueMember = "ID";
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // repositoryItemButtonEditChooseStaff
            // 
            this.repositoryItemButtonEditChooseStaff.AutoHeight = false;
            this.repositoryItemButtonEditChooseStaff.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to open the Select Staff screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditChooseStaff.Name = "repositoryItemButtonEditChooseStaff";
            this.repositoryItemButtonEditChooseStaff.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 6);
            this.imageCollection1.Images.SetKeyName(6, "refresh_16x16.png");
            this.imageCollection1.Images.SetKeyName(7, "BlockEdit_16x16.png");
            // 
            // gridControl10
            // 
            this.gridControl10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl10.DataSource = this.sp06292OMSiteContractLabourReassignListBindingSource;
            this.gridControl10.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl10.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl10.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Block Edit Selected Records", "block edit")});
            this.gridControl10.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl10_EmbeddedNavigator_ButtonClick);
            this.gridControl10.Location = new System.Drawing.Point(6, 54);
            this.gridControl10.MainView = this.gridView10;
            this.gridControl10.MenuManager = this.barManager1;
            this.gridControl10.Name = "gridControl10";
            this.gridControl10.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemGridLookUpEditUnitDescriptor,
            this.repositoryItemSpinEditMoney,
            this.repositoryItemSpinEditPercent,
            this.repositoryItemSpinEditMiles,
            this.repositoryItemMemoExEdit9,
            this.repositoryItemButtonEditChooseContractor,
            this.repositoryItemButtonEditChoosePersonType});
            this.gridControl10.Size = new System.Drawing.Size(974, 407);
            this.gridControl10.TabIndex = 24;
            this.gridControl10.UseEmbeddedNavigator = true;
            this.gridControl10.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView10});
            // 
            // sp06292OMSiteContractLabourReassignListBindingSource
            // 
            this.sp06292OMSiteContractLabourReassignListBindingSource.DataMember = "sp06292_OM_Site_Contract_Labour_Reassign_List";
            this.sp06292OMSiteContractLabourReassignListBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteContractLabourCostID,
            this.colSiteContractID2,
            this.colContractorID,
            this.colCostUnitDescriptorID,
            this.colSellUnitDescriptorID,
            this.colCostPerUnitExVat,
            this.colCostPerUnitVatRate,
            this.colSellPerUnitExVat,
            this.colSellPerUnitVatRate,
            this.colCISPercentage,
            this.colCISValue,
            this.colPostcodeSiteDistance,
            this.colLatLongSiteDistance,
            this.colRemarks6,
            this.colClientName3,
            this.colContractStartDate1,
            this.colContractEndDate1,
            this.colSiteName4,
            this.colContractorName,
            this.colOriginalContractorID,
            this.colOriginalContractorName,
            this.colOriginalLinkedToPersonType,
            this.colOriginalLinkedToPersonTypeID,
            this.colLinkedToPersonType,
            this.colLinkedToPersonTypeID});
            this.gridView10.GridControl = this.gridControl10;
            this.gridView10.GroupCount = 1;
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView10.OptionsFilter.AllowFilterEditor = false;
            this.gridView10.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView10.OptionsFilter.AllowMRUFilterList = false;
            this.gridView10.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView10.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView10.OptionsFind.FindDelay = 2000;
            this.gridView10.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView10.OptionsLayout.StoreAppearance = true;
            this.gridView10.OptionsLayout.StoreFormatRules = true;
            this.gridView10.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView10.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView10.OptionsSelection.MultiSelect = true;
            this.gridView10.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView10.OptionsView.ColumnAutoWidth = false;
            this.gridView10.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView10.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            this.gridView10.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName4, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOriginalLinkedToPersonType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOriginalContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView10.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView10_CustomDrawCell);
            this.gridView10.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView10_PopupMenuShowing);
            this.gridView10.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView10_SelectionChanged);
            this.gridView10.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView10_CustomDrawEmptyForeground);
            this.gridView10.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView10_CustomFilterDialog);
            this.gridView10.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView10_FilterEditorCreated);
            this.gridView10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView10_MouseUp);
            this.gridView10.GotFocus += new System.EventHandler(this.gridView10_GotFocus);
            // 
            // colSiteContractLabourCostID
            // 
            this.colSiteContractLabourCostID.Caption = "Site Contract Labour Cost ID";
            this.colSiteContractLabourCostID.FieldName = "SiteContractLabourCostID";
            this.colSiteContractLabourCostID.Name = "colSiteContractLabourCostID";
            this.colSiteContractLabourCostID.OptionsColumn.AllowEdit = false;
            this.colSiteContractLabourCostID.OptionsColumn.AllowFocus = false;
            this.colSiteContractLabourCostID.OptionsColumn.ReadOnly = true;
            this.colSiteContractLabourCostID.Width = 159;
            // 
            // colSiteContractID2
            // 
            this.colSiteContractID2.Caption = "Site Contract ID";
            this.colSiteContractID2.FieldName = "SiteContractID";
            this.colSiteContractID2.Name = "colSiteContractID2";
            this.colSiteContractID2.OptionsColumn.AllowEdit = false;
            this.colSiteContractID2.OptionsColumn.AllowFocus = false;
            this.colSiteContractID2.OptionsColumn.ReadOnly = true;
            this.colSiteContractID2.Width = 98;
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            // 
            // colCostUnitDescriptorID
            // 
            this.colCostUnitDescriptorID.Caption = "Cost Unit Descriptor";
            this.colCostUnitDescriptorID.ColumnEdit = this.repositoryItemGridLookUpEditUnitDescriptor;
            this.colCostUnitDescriptorID.FieldName = "CostUnitDescriptorID";
            this.colCostUnitDescriptorID.Name = "colCostUnitDescriptorID";
            this.colCostUnitDescriptorID.Visible = true;
            this.colCostUnitDescriptorID.VisibleIndex = 4;
            this.colCostUnitDescriptorID.Width = 117;
            // 
            // repositoryItemGridLookUpEditUnitDescriptor
            // 
            this.repositoryItemGridLookUpEditUnitDescriptor.AutoHeight = false;
            this.repositoryItemGridLookUpEditUnitDescriptor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditUnitDescriptor.DataSource = this.sp06101OMWorkUnitTypesPicklistBindingSource;
            this.repositoryItemGridLookUpEditUnitDescriptor.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditUnitDescriptor.Name = "repositoryItemGridLookUpEditUnitDescriptor";
            this.repositoryItemGridLookUpEditUnitDescriptor.NullText = "";
            this.repositoryItemGridLookUpEditUnitDescriptor.PopupView = this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor;
            this.repositoryItemGridLookUpEditUnitDescriptor.ValueMember = "ID";
            // 
            // sp06101OMWorkUnitTypesPicklistBindingSource
            // 
            this.sp06101OMWorkUnitTypesPicklistBindingSource.DataMember = "sp06101_OM_Work_Unit_Types_Picklist";
            this.sp06101OMWorkUnitTypesPicklistBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // repositoryItemGridLookUpEditGridViewCostUnitDescriptor
            // 
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40});
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn38;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.Name = "repositoryItemGridLookUpEditGridViewCostUnitDescriptor";
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn40, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Gender";
            this.gridColumn39.FieldName = "Description";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 0;
            this.gridColumn39.Width = 220;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Unit Descriptor";
            this.gridColumn40.FieldName = "RecordOrder";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            // 
            // colSellUnitDescriptorID
            // 
            this.colSellUnitDescriptorID.Caption = "Sell Unit Descriptor";
            this.colSellUnitDescriptorID.ColumnEdit = this.repositoryItemGridLookUpEditUnitDescriptor;
            this.colSellUnitDescriptorID.FieldName = "SellUnitDescriptorID";
            this.colSellUnitDescriptorID.Name = "colSellUnitDescriptorID";
            this.colSellUnitDescriptorID.Visible = true;
            this.colSellUnitDescriptorID.VisibleIndex = 7;
            this.colSellUnitDescriptorID.Width = 111;
            // 
            // colCostPerUnitExVat
            // 
            this.colCostPerUnitExVat.Caption = "Cost Per Unit Ex VAT";
            this.colCostPerUnitExVat.ColumnEdit = this.repositoryItemSpinEditMoney;
            this.colCostPerUnitExVat.FieldName = "CostPerUnitExVat";
            this.colCostPerUnitExVat.Name = "colCostPerUnitExVat";
            this.colCostPerUnitExVat.Visible = true;
            this.colCostPerUnitExVat.VisibleIndex = 5;
            this.colCostPerUnitExVat.Width = 121;
            // 
            // repositoryItemSpinEditMoney
            // 
            this.repositoryItemSpinEditMoney.AutoHeight = false;
            this.repositoryItemSpinEditMoney.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditMoney.Mask.EditMask = "c";
            this.repositoryItemSpinEditMoney.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditMoney.Name = "repositoryItemSpinEditMoney";
            // 
            // colCostPerUnitVatRate
            // 
            this.colCostPerUnitVatRate.Caption = "Cost Per Unit VAT Rate";
            this.colCostPerUnitVatRate.ColumnEdit = this.repositoryItemSpinEditPercent;
            this.colCostPerUnitVatRate.FieldName = "CostPerUnitVatRate";
            this.colCostPerUnitVatRate.Name = "colCostPerUnitVatRate";
            this.colCostPerUnitVatRate.Visible = true;
            this.colCostPerUnitVatRate.VisibleIndex = 6;
            this.colCostPerUnitVatRate.Width = 132;
            // 
            // repositoryItemSpinEditPercent
            // 
            this.repositoryItemSpinEditPercent.AutoHeight = false;
            this.repositoryItemSpinEditPercent.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditPercent.Mask.EditMask = "P";
            this.repositoryItemSpinEditPercent.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditPercent.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            131072});
            this.repositoryItemSpinEditPercent.Name = "repositoryItemSpinEditPercent";
            // 
            // colSellPerUnitExVat
            // 
            this.colSellPerUnitExVat.Caption = "Sell Per Unit Ex VAT";
            this.colSellPerUnitExVat.ColumnEdit = this.repositoryItemSpinEditMoney;
            this.colSellPerUnitExVat.FieldName = "SellPerUnitExVat";
            this.colSellPerUnitExVat.Name = "colSellPerUnitExVat";
            this.colSellPerUnitExVat.Visible = true;
            this.colSellPerUnitExVat.VisibleIndex = 8;
            this.colSellPerUnitExVat.Width = 115;
            // 
            // colSellPerUnitVatRate
            // 
            this.colSellPerUnitVatRate.Caption = "Sell Per Unit VAT Rate";
            this.colSellPerUnitVatRate.ColumnEdit = this.repositoryItemSpinEditPercent;
            this.colSellPerUnitVatRate.FieldName = "SellPerUnitVatRate";
            this.colSellPerUnitVatRate.Name = "colSellPerUnitVatRate";
            this.colSellPerUnitVatRate.Visible = true;
            this.colSellPerUnitVatRate.VisibleIndex = 9;
            this.colSellPerUnitVatRate.Width = 126;
            // 
            // colCISPercentage
            // 
            this.colCISPercentage.Caption = "CIS %";
            this.colCISPercentage.ColumnEdit = this.repositoryItemSpinEditPercent;
            this.colCISPercentage.FieldName = "CISPercentage";
            this.colCISPercentage.Name = "colCISPercentage";
            this.colCISPercentage.Visible = true;
            this.colCISPercentage.VisibleIndex = 10;
            // 
            // colCISValue
            // 
            this.colCISValue.Caption = "CIS Value";
            this.colCISValue.ColumnEdit = this.repositoryItemSpinEditMoney;
            this.colCISValue.FieldName = "CISValue";
            this.colCISValue.Name = "colCISValue";
            this.colCISValue.Visible = true;
            this.colCISValue.VisibleIndex = 11;
            this.colCISValue.Width = 81;
            // 
            // colPostcodeSiteDistance
            // 
            this.colPostcodeSiteDistance.Caption = "Distance From Site Postcode";
            this.colPostcodeSiteDistance.ColumnEdit = this.repositoryItemSpinEditMiles;
            this.colPostcodeSiteDistance.FieldName = "PostcodeSiteDistance";
            this.colPostcodeSiteDistance.Name = "colPostcodeSiteDistance";
            this.colPostcodeSiteDistance.Visible = true;
            this.colPostcodeSiteDistance.VisibleIndex = 12;
            this.colPostcodeSiteDistance.Width = 157;
            // 
            // repositoryItemSpinEditMiles
            // 
            this.repositoryItemSpinEditMiles.AutoHeight = false;
            this.repositoryItemSpinEditMiles.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditMiles.Mask.EditMask = "########0.00 Miles";
            this.repositoryItemSpinEditMiles.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditMiles.Name = "repositoryItemSpinEditMiles";
            // 
            // colLatLongSiteDistance
            // 
            this.colLatLongSiteDistance.Caption = "Distance From Site Lat\\Long";
            this.colLatLongSiteDistance.ColumnEdit = this.repositoryItemSpinEditMiles;
            this.colLatLongSiteDistance.FieldName = "LatLongSiteDistance";
            this.colLatLongSiteDistance.Name = "colLatLongSiteDistance";
            this.colLatLongSiteDistance.Visible = true;
            this.colLatLongSiteDistance.VisibleIndex = 13;
            this.colLatLongSiteDistance.Width = 155;
            // 
            // colRemarks6
            // 
            this.colRemarks6.Caption = "Remarks";
            this.colRemarks6.ColumnEdit = this.repositoryItemMemoExEdit9;
            this.colRemarks6.FieldName = "Remarks";
            this.colRemarks6.Name = "colRemarks6";
            this.colRemarks6.Visible = true;
            this.colRemarks6.VisibleIndex = 14;
            // 
            // repositoryItemMemoExEdit9
            // 
            this.repositoryItemMemoExEdit9.AutoHeight = false;
            this.repositoryItemMemoExEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit9.Name = "repositoryItemMemoExEdit9";
            this.repositoryItemMemoExEdit9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit9.ShowIcon = false;
            // 
            // colClientName3
            // 
            this.colClientName3.Caption = "Client Name";
            this.colClientName3.FieldName = "ClientName";
            this.colClientName3.Name = "colClientName3";
            this.colClientName3.OptionsColumn.AllowEdit = false;
            this.colClientName3.OptionsColumn.AllowFocus = false;
            this.colClientName3.OptionsColumn.ReadOnly = true;
            this.colClientName3.Width = 166;
            // 
            // colContractStartDate1
            // 
            this.colContractStartDate1.Caption = "Contract Start";
            this.colContractStartDate1.FieldName = "ContractStartDate";
            this.colContractStartDate1.Name = "colContractStartDate1";
            this.colContractStartDate1.OptionsColumn.AllowEdit = false;
            this.colContractStartDate1.OptionsColumn.AllowFocus = false;
            this.colContractStartDate1.OptionsColumn.ReadOnly = true;
            this.colContractStartDate1.Width = 90;
            // 
            // colContractEndDate1
            // 
            this.colContractEndDate1.Caption = "Contract End";
            this.colContractEndDate1.FieldName = "ContractEndDate";
            this.colContractEndDate1.Name = "colContractEndDate1";
            this.colContractEndDate1.OptionsColumn.AllowEdit = false;
            this.colContractEndDate1.OptionsColumn.AllowFocus = false;
            this.colContractEndDate1.OptionsColumn.ReadOnly = true;
            this.colContractEndDate1.Width = 84;
            // 
            // colSiteName4
            // 
            this.colSiteName4.Caption = "Site Name";
            this.colSiteName4.FieldName = "SiteName";
            this.colSiteName4.Name = "colSiteName4";
            this.colSiteName4.OptionsColumn.AllowEdit = false;
            this.colSiteName4.OptionsColumn.AllowFocus = false;
            this.colSiteName4.OptionsColumn.ReadOnly = true;
            this.colSiteName4.Width = 191;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "New Labour Name";
            this.colContractorName.ColumnEdit = this.repositoryItemButtonEditChooseContractor;
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 3;
            this.colContractorName.Width = 176;
            // 
            // repositoryItemButtonEditChooseContractor
            // 
            this.repositoryItemButtonEditChooseContractor.AutoHeight = false;
            this.repositoryItemButtonEditChooseContractor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to Open the Select Contractor \\ Staff Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditChooseContractor.Name = "repositoryItemButtonEditChooseContractor";
            this.repositoryItemButtonEditChooseContractor.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditChooseContractor.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditChooseContractor_ButtonClick);
            // 
            // colOriginalContractorID
            // 
            this.colOriginalContractorID.Caption = "Original Contractor ID";
            this.colOriginalContractorID.FieldName = "OriginalContractorID";
            this.colOriginalContractorID.Name = "colOriginalContractorID";
            this.colOriginalContractorID.OptionsColumn.AllowEdit = false;
            this.colOriginalContractorID.OptionsColumn.AllowFocus = false;
            this.colOriginalContractorID.OptionsColumn.ReadOnly = true;
            this.colOriginalContractorID.Width = 124;
            // 
            // colOriginalContractorName
            // 
            this.colOriginalContractorName.Caption = "Original Labour Name";
            this.colOriginalContractorName.FieldName = "OriginalContractorName";
            this.colOriginalContractorName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colOriginalContractorName.Name = "colOriginalContractorName";
            this.colOriginalContractorName.OptionsColumn.AllowEdit = false;
            this.colOriginalContractorName.OptionsColumn.AllowFocus = false;
            this.colOriginalContractorName.OptionsColumn.ReadOnly = true;
            this.colOriginalContractorName.Visible = true;
            this.colOriginalContractorName.VisibleIndex = 1;
            this.colOriginalContractorName.Width = 153;
            // 
            // colOriginalLinkedToPersonType
            // 
            this.colOriginalLinkedToPersonType.Caption = "Original Person Type ";
            this.colOriginalLinkedToPersonType.FieldName = "OriginalLinkedToPersonType";
            this.colOriginalLinkedToPersonType.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colOriginalLinkedToPersonType.Name = "colOriginalLinkedToPersonType";
            this.colOriginalLinkedToPersonType.OptionsColumn.AllowEdit = false;
            this.colOriginalLinkedToPersonType.OptionsColumn.AllowFocus = false;
            this.colOriginalLinkedToPersonType.OptionsColumn.ReadOnly = true;
            this.colOriginalLinkedToPersonType.Visible = true;
            this.colOriginalLinkedToPersonType.VisibleIndex = 0;
            this.colOriginalLinkedToPersonType.Width = 134;
            // 
            // colOriginalLinkedToPersonTypeID
            // 
            this.colOriginalLinkedToPersonTypeID.Caption = "Original Person Type ID";
            this.colOriginalLinkedToPersonTypeID.FieldName = "OriginalLinkedToPersonTypeID";
            this.colOriginalLinkedToPersonTypeID.Name = "colOriginalLinkedToPersonTypeID";
            this.colOriginalLinkedToPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colOriginalLinkedToPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colOriginalLinkedToPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colOriginalLinkedToPersonTypeID.Width = 132;
            // 
            // colLinkedToPersonType
            // 
            this.colLinkedToPersonType.Caption = "New Person Type";
            this.colLinkedToPersonType.ColumnEdit = this.repositoryItemButtonEditChoosePersonType;
            this.colLinkedToPersonType.FieldName = "LinkedToPersonType";
            this.colLinkedToPersonType.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colLinkedToPersonType.Name = "colLinkedToPersonType";
            this.colLinkedToPersonType.Visible = true;
            this.colLinkedToPersonType.VisibleIndex = 2;
            this.colLinkedToPersonType.Width = 154;
            // 
            // repositoryItemButtonEditChoosePersonType
            // 
            this.repositoryItemButtonEditChoosePersonType.AutoHeight = false;
            this.repositoryItemButtonEditChoosePersonType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click me to Opent the Select Person Type screen.", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditChoosePersonType.Name = "repositoryItemButtonEditChoosePersonType";
            this.repositoryItemButtonEditChoosePersonType.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditChoosePersonType.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditChoosePersonType_ButtonClick);
            // 
            // colLinkedToPersonTypeID
            // 
            this.colLinkedToPersonTypeID.Caption = "New Linked Person Type ID";
            this.colLinkedToPersonTypeID.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.Name = "colLinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID.Width = 150;
            // 
            // panelControl11
            // 
            this.panelControl11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl11.Controls.Add(this.pictureEdit13);
            this.panelControl11.Controls.Add(this.labelControl26);
            this.panelControl11.Location = new System.Drawing.Point(6, 6);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(839, 41);
            this.panelControl11.TabIndex = 18;
            // 
            // pictureEdit13
            // 
            this.pictureEdit13.EditValue = ((object)(resources.GetObject("pictureEdit13.EditValue")));
            this.pictureEdit13.Location = new System.Drawing.Point(0, 4);
            this.pictureEdit13.MenuManager = this.barManager1;
            this.pictureEdit13.Name = "pictureEdit13";
            this.pictureEdit13.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit13.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit13.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit13.Properties.ReadOnly = true;
            this.pictureEdit13.Properties.ShowMenu = false;
            this.pictureEdit13.Size = new System.Drawing.Size(34, 33);
            this.pictureEdit13.TabIndex = 9;
            // 
            // labelControl26
            // 
            this.labelControl26.AllowHtmlString = true;
            this.labelControl26.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl26.Appearance.Options.UseBackColor = true;
            this.labelControl26.Location = new System.Drawing.Point(37, 7);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(796, 26);
            this.labelControl26.TabIndex = 7;
            this.labelControl26.Text = resources.GetString("labelControl26.Text");
            // 
            // sp06063OMContractTypesWithBlankBindingSource
            // 
            this.sp06063OMContractTypesWithBlankBindingSource.DataMember = "sp06063_OM_Contract_Types_With_Blank";
            this.sp06063OMContractTypesWithBlankBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // bsiSaltUsed
            // 
            this.bsiSaltUsed.Caption = "Salt Used";
            this.bsiSaltUsed.Id = 30;
            this.bsiSaltUsed.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiSaltUsed.ImageOptions.Image")));
            this.bsiSaltUsed.Name = "bsiSaltUsed";
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "";
            this.repositoryItemCheckEdit4.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // sp06101_OM_Work_Unit_Types_PicklistTableAdapter
            // 
            this.sp06101_OM_Work_Unit_Types_PicklistTableAdapter.ClearBeforeFill = true;
            // 
            // bsiRecordTicking
            // 
            this.bsiRecordTicking.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiRecordTicking.Caption = "Record <b>Ticking</b>";
            this.bsiRecordTicking.Id = 52;
            this.bsiRecordTicking.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiRecordTicking.ImageOptions.Image")));
            this.bsiRecordTicking.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTick),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUntick)});
            this.bsiRecordTicking.Name = "bsiRecordTicking";
            // 
            // bbiTick
            // 
            this.bbiTick.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiTick.Border = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.bbiTick.Caption = "<b>Tick</b> Selected Records";
            this.bbiTick.Id = 53;
            this.bbiTick.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiTick.ImageOptions.Image")));
            this.bbiTick.Name = "bbiTick";
            // 
            // bbiUntick
            // 
            this.bbiUntick.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiUntick.Caption = "<b>Untick</b> Selected Records";
            this.bbiUntick.Id = 54;
            this.bbiUntick.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUntick.ImageOptions.Image")));
            this.bbiUntick.Name = "bbiUntick";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.Location = new System.Drawing.Point(852, 6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(60, 23);
            this.btnSave.TabIndex = 25;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("CancelButton.ImageOptions.Image")));
            this.CancelButton.Location = new System.Drawing.Point(916, 6);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(63, 23);
            this.CancelButton.TabIndex = 26;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // sp06292_OM_Site_Contract_Labour_Reassign_ListTableAdapter
            // 
            this.sp06292_OM_Site_Contract_Labour_Reassign_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Site_Contract_Labour_Reassign
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(986, 466);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.gridControl10);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.panelControl11);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Site_Contract_Labour_Reassign";
            this.Text = "Reassign Preferred Site Labour and Update Outstanding Jobs";
            this.Activated += new System.EventHandler(this.frm_OM_Site_Contract_Labour_Reassign_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Site_Contract_Labour_Reassign_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Site_Contract_Labour_Reassign_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.panelControl11, 0);
            this.Controls.SetChildIndex(this.btnSave, 0);
            this.Controls.SetChildIndex(this.gridControl10, 0);
            this.Controls.SetChildIndex(this.CancelButton, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseStaff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06292OMSiteContractLabourReassignListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditUnitDescriptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06101OMWorkUnitTypesPicklistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditGridViewCostUnitDescriptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditMiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChoosePersonType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            this.panelControl11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06063OMContractTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.BarSubItem bsiSaltUsed;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private System.Windows.Forms.BindingSource sp06063OMContractTypesWithBlankBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.PictureEdit pictureEdit13;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditChooseStaff;
        private DevExpress.XtraGrid.GridControl gridControl10;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractLabourCostID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID2;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSellUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnitExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnitVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnitExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnitVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colCISPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colCISValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcodeSiteDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colLatLongSiteDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks6;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName3;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName4;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditUnitDescriptor;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEditGridViewCostUnitDescriptor;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditMoney;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditPercent;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditMiles;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditChooseContractor;
        private System.Windows.Forms.BindingSource sp06101OMWorkUnitTypesPicklistBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06101_OM_Work_Unit_Types_PicklistTableAdapter sp06101_OM_Work_Unit_Types_PicklistTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraBars.BarSubItem bsiRecordTicking;
        private DevExpress.XtraBars.BarButtonItem bbiTick;
        private DevExpress.XtraBars.BarButtonItem bbiUntick;
        private DevExpress.XtraEditors.SimpleButton CancelButton;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private System.Windows.Forms.BindingSource sp06292OMSiteContractLabourReassignListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalContractorName;
        private DataSet_OM_ContractTableAdapters.sp06292_OM_Site_Contract_Labour_Reassign_ListTableAdapter sp06292_OM_Site_Contract_Labour_Reassign_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalLinkedToPersonType;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginalLinkedToPersonTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditChoosePersonType;
    }
}

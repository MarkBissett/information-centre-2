﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;
using DevExpress.XtraEditors;
using System.IO;

namespace WoodPlan5
{
    public partial class frm_OM_Visit_Completion_Wizard_Block_Edit_Job : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        
        public string strSignaturePath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        public DateTime? dtActualStartDate = null;
        public DateTime? dtActualEndDate = null;
        public decimal? decActualDurationUnits = null;
        public int? intActualDurationUnitsDescriptionID = null;
        public int? intJobStatusID = null;
        public int? intCancelledReasonID = null;
        public double? dStartLatitude = null;
        public double? dStartLongitude = null;
        public double? dFinishLatitude = null;
        public double? dFinishLongitude = null;
        public string strRemarks = null;
        public int? intJobNoLongerRequired = null;
        public int? intNoWorkRequired = null;
        public int? intManuallyCompleted = null;
        public int? intDoNotPayContractor = null;
        public int? intDoNotInvoiceClient = null;
        public int? intRework = null;

        #endregion

        public frm_OM_Visit_Completion_Wizard_Block_Edit_Job()
        {
            InitializeComponent();
        }

        private void frm_OM_Visit_Completion_Wizard_Block_Edit_Job_Load(object sender, EventArgs e)
        {
            this.FormID = 410;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //
            
            strConnectionString = this.GlobalSettings.ConnectionString;
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strSignaturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_PictureFilesFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Client Signatures (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Client Signature Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            try
            {
                sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter.Fill(dataSet_OM_Job.sp06177_OM_Job_Duration_Descriptors_With_Blank, 1);
                
                sp06418_OM_Visit_Completion_Wizard_Job_StatusesTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06418_OM_Visit_Completion_Wizard_Job_StatusesTableAdapter.Fill(dataSet_OM_Job.sp06418_OM_Visit_Completion_Wizard_Job_Statuses, 0);

                sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter.Fill(dataSet_OM_Job.sp06268_OM_Cancelled_Reasons_With_Blank, 1);
            }
            catch (Exception) { }

            dateEditActualStartDate.EditValue = null;
            dateEditActualEndDate.EditValue = null;
            ActualDurationUnitsSpinEdit.EditValue = null;
            ActualDurationUnitsDescriptionIDGridLookUpEdit.EditValue = null;
            gridLookUpEditJobStatusID.EditValue = null;
            CancelledReasonIDGridLookUpEdit.EditValue = null;
            StartLatitudeTextEdit.EditValue = null;
            StartLongitudeTextEdit.EditValue = null;
            FinishLatitudeTextEdit.EditValue = null;
            FinishLongitudeTextEdit.EditValue = null;
            RemarksMemoEdit.EditValue = null;
            JobNoLongerRequiredCheckEdit.EditValue = null;
            NoWorkRequiredCheckEdit.EditValue = null;
            ManuallyCompletedCheckEdit.EditValue = null;
            DoNotPayContractorCheckEdit.EditValue = null;
            DoNotInvoiceClientCheckEdit.EditValue = null;
            ReworkCheckEdit.EditValue = null;

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }
       
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (dateEditActualStartDate.EditValue != null) dtActualStartDate = Convert.ToDateTime(dateEditActualStartDate.EditValue);
            if (dateEditActualEndDate.EditValue != null) dtActualEndDate = Convert.ToDateTime(dateEditActualEndDate.EditValue);
            if (ActualDurationUnitsSpinEdit.EditValue != null) decActualDurationUnits = Convert.ToDecimal(ActualDurationUnitsSpinEdit.EditValue);
            if (ActualDurationUnitsDescriptionIDGridLookUpEdit.EditValue != null) intActualDurationUnitsDescriptionID = Convert.ToInt32(ActualDurationUnitsDescriptionIDGridLookUpEdit.EditValue);
            if (gridLookUpEditJobStatusID.EditValue != null) intJobStatusID = Convert.ToInt32(gridLookUpEditJobStatusID.EditValue);
            if (CancelledReasonIDGridLookUpEdit.EditValue != null) intCancelledReasonID = Convert.ToInt32(CancelledReasonIDGridLookUpEdit.EditValue);
            if (StartLatitudeTextEdit.EditValue != null) dStartLatitude = Convert.ToDouble(StartLatitudeTextEdit.EditValue);
            if (StartLongitudeTextEdit.EditValue != null) dStartLongitude = Convert.ToDouble(StartLongitudeTextEdit.EditValue);
            if (FinishLatitudeTextEdit.EditValue != null) dFinishLatitude = Convert.ToDouble(FinishLatitudeTextEdit.EditValue);
            if (FinishLongitudeTextEdit.EditValue != null) dFinishLongitude = Convert.ToDouble(FinishLongitudeTextEdit.EditValue);
            if (RemarksMemoEdit.EditValue != null) strRemarks = RemarksMemoEdit.EditValue.ToString();
            if (JobNoLongerRequiredCheckEdit.EditValue != null) intJobNoLongerRequired = Convert.ToInt32(JobNoLongerRequiredCheckEdit.EditValue);
            if (NoWorkRequiredCheckEdit.EditValue != null) intNoWorkRequired = Convert.ToInt32(NoWorkRequiredCheckEdit.EditValue);
            if (ManuallyCompletedCheckEdit.EditValue != null) intManuallyCompleted = Convert.ToInt32(ManuallyCompletedCheckEdit.EditValue);
            if (DoNotPayContractorCheckEdit.EditValue != null) intDoNotPayContractor = Convert.ToInt32(DoNotPayContractorCheckEdit.EditValue);
            if (DoNotInvoiceClientCheckEdit.EditValue != null) intDoNotInvoiceClient = Convert.ToInt32(DoNotInvoiceClientCheckEdit.EditValue);
            if (ReworkCheckEdit.EditValue != null) intRework = Convert.ToInt32(ReworkCheckEdit.EditValue);
          
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void dateEditActualStartDate_EditValueChanged(object sender, EventArgs e)
        {
            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            DateEdit de = (DateEdit)sender;
            try { dtStart = de.DateTime; }
            catch (Exception) { }

            try { dtEnd = Convert.ToDateTime(dateEditActualEndDate.DateTime); }
            catch (Exception) { }

            try { decUnits = Convert.ToDecimal(ActualDurationUnitsSpinEdit.EditValue); }
            catch (Exception) { }

            try { intUnitDescriptorID = Convert.ToInt32(ActualDurationUnitsDescriptionIDGridLookUpEdit.EditValue); }
            catch (Exception) { }

            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, "ActualStartDate");
        }

        private void dateEditActualEndDate_EditValueChanged(object sender, EventArgs e)
        {
            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            try { dtStart = Convert.ToDateTime(dateEditActualStartDate.DateTime); }
            catch (Exception) { }

            DateEdit de = (DateEdit)sender;
            try { dtEnd = de.DateTime; }
            catch (Exception) { }

            try { decUnits = Convert.ToDecimal(ActualDurationUnitsSpinEdit.EditValue); }
            catch (Exception) { }

            try { intUnitDescriptorID = Convert.ToInt32(ActualDurationUnitsDescriptionIDGridLookUpEdit.EditValue); }
            catch (Exception) { }

            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, "ActualEndDate");
        }

        private void ActualDurationUnitsSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            try { dtStart = Convert.ToDateTime(dateEditActualStartDate.DateTime); }
            catch (Exception) { }

            try { dtEnd = Convert.ToDateTime(dateEditActualEndDate.DateTime); }
            catch (Exception) { }

            SpinEdit se = (SpinEdit)sender;
            try { decUnits = Convert.ToDecimal(se.Value); }
            catch (Exception) { }

            try { intUnitDescriptorID = Convert.ToInt32(ActualDurationUnitsDescriptionIDGridLookUpEdit.EditValue); }
            catch (Exception) { }

            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, "ActualDurationUnits");
        }

        private void ActualDurationUnitsDescriptionIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            try { dtStart = Convert.ToDateTime(dateEditActualStartDate.DateTime); }
            catch (Exception) { }

            try { dtEnd = Convert.ToDateTime(dateEditActualEndDate.DateTime); }
            catch (Exception) { }

            try { decUnits = Convert.ToDecimal(ActualDurationUnitsSpinEdit.EditValue); }
            catch (Exception) { }

            GridLookUpEdit glue = (GridLookUpEdit)sender;
            try { intUnitDescriptorID = Convert.ToInt32(glue.EditValue); }
            catch (Exception) { }

            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, "ActualDurationUnitsDescriptionID");
        }

        private void CalculateDates(DateTime? StartDate, decimal Units, int UnitDescriptorID, DateTime? EndDate, string ChangedColumnName)
        {
            if (StartDate == DateTime.MinValue && Units == 0 && UnitDescriptorID == 0 && EndDate == DateTime.MinValue) return;
            switch (ChangedColumnName)
            {
                case "ActualStartDate":
                    {
                        if (StartDate <= DateTime.MinValue) return;
                        if (Units == 0 && UnitDescriptorID == 0 && EndDate == DateTime.MinValue) return;

                        if (Units > (decimal)0.00 && UnitDescriptorID > 0)  // Calculate End Date //
                        {
                            switch (UnitDescriptorID)
                            {
                                case 1:  // Hours //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddHours((double)Units);
                                    }
                                    break;
                                case 2:  // Days //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddDays((double)Units);
                                    }
                                    break;
                                case 3:  // Weeks //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddDays((double)Units * (double)7);
                                    }
                                    break;
                                case 4:  // Months //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddMonths((int)Units);
                                    }
                                    break;
                                case 5:  // Years //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddYears((int)Units);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            dateEditActualEndDate.DateTime =  (EndDate == null ? DateTime.MinValue : (DateTime)EndDate);
                        }
                        else if (EndDate != null)  // Calculate Duration //
                        {
                            // Calculate the interval between the two dates.
                            DateDiff dateDiff = DateDiff.Difference((DateTime)StartDate, (DateTime)EndDate);

                            if (dateDiff.Years > 0 && (dateDiff.Months == 0 && dateDiff.Weeks == 0 && dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                Units = (decimal)dateDiff.Years;
                                UnitDescriptorID = 5;
                            }
                            else if (dateDiff.Months > 0 && (dateDiff.Weeks == 0 && dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                Units = (decimal)dateDiff.Months + ((decimal)dateDiff.Years * (decimal)12);
                                UnitDescriptorID = 4;
                            }
                            else if (dateDiff.Weeks > 0 && (dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalDays / (decimal)7;
                                UnitDescriptorID = 3;
                            }
                            else if (dateDiff.Days > 0 && (dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalDays;
                                UnitDescriptorID = 2;
                                if (Units % 7 == 0) // Check if remainder is 0 if yes then number is devisable for weeks so convert to weeks //
                                {
                                    Units = (decimal)ts.TotalDays / (decimal)7;
                                    UnitDescriptorID = 3;
                                }
                            }
                            else if (dateDiff.Hours > 0 && (dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalHours;
                                UnitDescriptorID = 1;
                            }
                            else if (dateDiff.Minutes > 0)
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalMinutes / (decimal)60;
                                UnitDescriptorID = 1;
                            }
                            ActualDurationUnitsSpinEdit.EditValue =  (Units <= (decimal)0.00 ? (decimal)0.00 : Units);
                            ActualDurationUnitsDescriptionIDGridLookUpEdit.EditValue = UnitDescriptorID;
                        }
                    }
                    break;
                case "ActualEndDate":
                    {
                        if (EndDate <= DateTime.MinValue) return;
                        if (Units > (decimal)0.00 && UnitDescriptorID > 0)  // Calculate End Date //
                        {
                            switch (UnitDescriptorID)
                            {
                                case 1:  // Hours //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddHours((double)Units * (double)-1);
                                    }
                                    break;
                                case 2:  // Days //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddDays((double)Units * (double)-1);
                                    }
                                    break;
                                case 3:  // Weeks //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddDays(((double)Units * (double)7) * (double)-1);
                                    }
                                    break;
                                case 4:  // Months //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddMonths((int)Units * -1);
                                    }
                                    break;
                                case 5:  // Years //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddYears((int)Units * -1);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            dateEditActualStartDate.DateTime = (StartDate == null ? DateTime.MinValue : (DateTime)StartDate);
                        }
                        else if (StartDate != null)  // Calculate Duration //
                        {
                            // Calculate the interval between the two dates.
                            DateDiff dateDiff = DateDiff.Difference((DateTime)StartDate, (DateTime)EndDate);

                            if (dateDiff.Years > 0 && (dateDiff.Months == 0 && dateDiff.Weeks == 0 && dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                Units = (decimal)dateDiff.Years;
                                UnitDescriptorID = 5;
                            }
                            else if (dateDiff.Months > 0 && (dateDiff.Weeks == 0 && dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                Units = (decimal)dateDiff.Months + ((decimal)dateDiff.Years * (decimal)12);
                                UnitDescriptorID = 4;
                            }
                            else if (dateDiff.Weeks > 0 && (dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalDays / (decimal)7;
                                UnitDescriptorID = 3;
                            }
                            else if (dateDiff.Days > 0 && (dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalDays;
                                UnitDescriptorID = 2;
                                if (Units % 7 == 0) // Check if remainder is 0 if yes then number is devisable for weeks so convert to weeks //
                                {
                                    Units = (decimal)ts.TotalDays / (decimal)7;
                                    UnitDescriptorID = 3;
                                }
                            }
                            else if (dateDiff.Hours > 0 && (dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalHours;
                                UnitDescriptorID = 1;
                            }
                            else if (dateDiff.Minutes > 0)
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalMinutes / (decimal)60;
                                UnitDescriptorID = 1;
                            }
                            ActualDurationUnitsSpinEdit.EditValue = (Units <= (decimal)0.00 ? (decimal)0.00 : Units);
                            ActualDurationUnitsDescriptionIDGridLookUpEdit.EditValue = UnitDescriptorID;
                        }
                    }
                    break;
                case "ActualDurationUnits":
                case "ActualDurationUnitsDescriptionID":
                    {
                        if (Units > (decimal)0.00 && UnitDescriptorID > 0 && StartDate != null)  // Calculate End Date //
                        {
                            switch (UnitDescriptorID)
                            {
                                case 1:  // Hours //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddHours((double)Units);
                                    }
                                    break;
                                case 2:  // Days //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddDays((double)Units);
                                    }
                                    break;
                                case 3:  // Weeks //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddDays((double)Units * (double)7);
                                    }
                                    break;
                                case 4:  // Months //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddMonths((int)Units);
                                    }
                                    break;
                                case 5:  // Years //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddYears((int)Units);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            dateEditActualEndDate.DateTime = (EndDate == null ? DateTime.MinValue : (DateTime)EndDate);
                        }
                        else if (Units > (decimal)0.00 && UnitDescriptorID > 0 && EndDate != null)  // Calculate Start Date //
                        {
                            switch (UnitDescriptorID)
                            {
                                case 1:  // Hours //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddHours((double)Units * (double)-1);
                                    }
                                    break;
                                case 2:  // Days //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddDays((double)Units * (double)-1);
                                    }
                                    break;
                                case 3:  // Weeks //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddDays(((double)Units * (double)7) * (double)-1);
                                    }
                                    break;
                                case 4:  // Months //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddMonths((int)Units * -1);
                                    }
                                    break;
                                case 5:  // Years //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddYears((int)Units * -1);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            dateEditActualStartDate.DateTime = (StartDate == null ? DateTime.MinValue : (DateTime)StartDate);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridLookUpEditJobStatusID_EditValueChanged(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            try 
            { 
                int intJobStatusID = Convert.ToInt32(glue.EditValue);
                if (intJobStatusID == 90 || intJobStatusID == 100)  // Aborted or Cancelled so clear duration //
                {
                    dateEditActualStartDate.EditValue = null;
                    dateEditActualEndDate.EditValue = null;
                    ActualDurationUnitsSpinEdit.EditValue = null;
                    ActualDurationUnitsDescriptionIDGridLookUpEdit.EditValue = null;
                }
            }
            catch (Exception) { }
        }





    }
}

﻿namespace WoodPlan5
{
    partial class frm_OM_Points_Team_Redemption_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Points_Team_Redemption_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.BSCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Teams = new WoodPlan5.DataSet_Teams();
            this.intContractorIDLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.sp06806OMTeamListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.RedemptionDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ContactMethodIDLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.sp06807OMTeamContactMethodBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForBSCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRedemptionDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintContractorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContactMethodID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter = new WoodPlan5.DataSet_TeamsTableAdapters.sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter();
            this.sp06806_OM_Team_ListTableAdapter = new WoodPlan5.DataSet_TeamsTableAdapters.sp06806_OM_Team_ListTableAdapter();
            this.sp06807_OM_Team_ContactMethodTableAdapter = new WoodPlan5.DataSet_TeamsTableAdapters.sp06807_OM_Team_ContactMethodTableAdapter();
            this.sp06809OMTeamRedemptionDuplicateSearchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp06809_OM_Team_RedemptionDuplicateSearchTableAdapter = new WoodPlan5.DataSet_TeamsTableAdapters.sp06809_OM_Team_RedemptionDuplicateSearchTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BSCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Teams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intContractorIDLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06806OMTeamListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RedemptionDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RedemptionDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactMethodIDLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06807OMTeamContactMethodBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBSCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRedemptionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintContractorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactMethodID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06809OMTeamRedemptionDuplicateSearchBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1049, 31);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 520);
            this.barDockControlBottom.Size = new System.Drawing.Size(1049, 27);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 31);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 489);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1049, 31);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 489);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation,
            this.bbiRefresh});
            this.barManager1.MaxItemId = 37;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Id = 36;
            this.bbiRefresh.Name = "bbiRefresh";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlGroup1});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(1049, 489);
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(382, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(243, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.Location = new System.Drawing.Point(394, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(239, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.BSCodeTextEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.intContractorIDLookUpEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.RedemptionDateDateEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ContactMethodIDLookUpEdit);
            this.editFormDataLayoutControlGroup.DataSource = this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 31);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1238, 228, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(1049, 489);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // BSCodeTextEdit
            // 
            this.BSCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource, "BSCode", true));
            this.BSCodeTextEdit.Location = new System.Drawing.Point(98, 35);
            this.BSCodeTextEdit.MenuManager = this.barManager1;
            this.BSCodeTextEdit.Name = "BSCodeTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.BSCodeTextEdit, true);
            this.BSCodeTextEdit.Size = new System.Drawing.Size(424, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.BSCodeTextEdit, optionsSpelling1);
            this.BSCodeTextEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.BSCodeTextEdit.TabIndex = 129;
            this.BSCodeTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.textBox_Validating);
            // 
            // sp06802OMTeamPointsRedemptionHeaderItemBindingSource
            // 
            this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource.DataMember = "sp06802_OM_Team_PointsRedemptionHeaderItem";
            this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource.DataSource = this.dataSet_Teams;
            // 
            // dataSet_Teams
            // 
            this.dataSet_Teams.DataSetName = "DataSet_Teams";
            this.dataSet_Teams.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // intContractorIDLookUpEdit
            // 
            this.intContractorIDLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource, "intContractorID", true));
            this.intContractorIDLookUpEdit.Location = new System.Drawing.Point(612, 35);
            this.intContractorIDLookUpEdit.MenuManager = this.barManager1;
            this.intContractorIDLookUpEdit.Name = "intContractorIDLookUpEdit";
            this.intContractorIDLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.intContractorIDLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.intContractorIDLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.intContractorIDLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("intContractorID", "int Contractor ID", 104, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TeamName", "Team Name", 66, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Isactive", "Is Active", 48, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far)});
            this.intContractorIDLookUpEdit.Properties.DataSource = this.sp06806OMTeamListBindingSource;
            this.intContractorIDLookUpEdit.Properties.DisplayMember = "TeamName";
            this.intContractorIDLookUpEdit.Properties.NullText = "";
            this.intContractorIDLookUpEdit.Properties.ValueMember = "intContractorID";
            this.intContractorIDLookUpEdit.Size = new System.Drawing.Size(425, 20);
            this.intContractorIDLookUpEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.intContractorIDLookUpEdit.TabIndex = 130;
            this.intContractorIDLookUpEdit.Tag = "Team Name";
            this.intContractorIDLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.lookupedit_Validating);
            // 
            // sp06806OMTeamListBindingSource
            // 
            this.sp06806OMTeamListBindingSource.DataMember = "sp06806_OM_Team_List";
            this.sp06806OMTeamListBindingSource.DataSource = this.dataSet_Teams;
            // 
            // RedemptionDateDateEdit
            // 
            this.RedemptionDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource, "RedemptionDate", true));
            this.RedemptionDateDateEdit.EditValue = null;
            this.RedemptionDateDateEdit.Location = new System.Drawing.Point(98, 59);
            this.RedemptionDateDateEdit.MenuManager = this.barManager1;
            this.RedemptionDateDateEdit.Name = "RedemptionDateDateEdit";
            this.RedemptionDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RedemptionDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RedemptionDateDateEdit.Size = new System.Drawing.Size(424, 20);
            this.RedemptionDateDateEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.RedemptionDateDateEdit.TabIndex = 131;
            this.RedemptionDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.dateEdit_Validating);
            // 
            // ContactMethodIDLookUpEdit
            // 
            this.ContactMethodIDLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource, "ContactMethodID", true));
            this.ContactMethodIDLookUpEdit.Location = new System.Drawing.Point(612, 59);
            this.ContactMethodIDLookUpEdit.MenuManager = this.barManager1;
            this.ContactMethodIDLookUpEdit.Name = "ContactMethodIDLookUpEdit";
            this.ContactMethodIDLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ContactMethodIDLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ContactMethodIDLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ContactMethodIDLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "Code", 35, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ContactMethod", "Contact Method", 87, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("intOrder", "int Order", 53, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.ContactMethodIDLookUpEdit.Properties.DataSource = this.sp06807OMTeamContactMethodBindingSource;
            this.ContactMethodIDLookUpEdit.Properties.DisplayMember = "ContactMethod";
            this.ContactMethodIDLookUpEdit.Properties.NullText = "";
            this.ContactMethodIDLookUpEdit.Properties.ValueMember = "ContactMethodID";
            this.ContactMethodIDLookUpEdit.Size = new System.Drawing.Size(425, 20);
            this.ContactMethodIDLookUpEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.ContactMethodIDLookUpEdit.TabIndex = 132;
            this.ContactMethodIDLookUpEdit.Tag = "Contact Method";
            this.ContactMethodIDLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.lookupedit_Validating);
            // 
            // sp06807OMTeamContactMethodBindingSource
            // 
            this.sp06807OMTeamContactMethodBindingSource.DataMember = "sp06807_OM_Team_ContactMethod";
            this.sp06807OMTeamContactMethodBindingSource.DataSource = this.dataSet_Teams;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(382, 23);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(625, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(404, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForBSCode,
            this.ItemForRedemptionDate,
            this.ItemForintContractorID,
            this.ItemForContactMethodID,
            this.emptySpaceItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1029, 446);
            // 
            // ItemForBSCode
            // 
            this.ItemForBSCode.Control = this.BSCodeTextEdit;
            this.ItemForBSCode.Location = new System.Drawing.Point(0, 0);
            this.ItemForBSCode.Name = "ItemForBSCode";
            this.ItemForBSCode.Size = new System.Drawing.Size(514, 24);
            this.ItemForBSCode.Text = "BS Code";
            this.ItemForBSCode.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForRedemptionDate
            // 
            this.ItemForRedemptionDate.Control = this.RedemptionDateDateEdit;
            this.ItemForRedemptionDate.Location = new System.Drawing.Point(0, 24);
            this.ItemForRedemptionDate.Name = "ItemForRedemptionDate";
            this.ItemForRedemptionDate.Size = new System.Drawing.Size(514, 24);
            this.ItemForRedemptionDate.Text = "Redemption Date";
            this.ItemForRedemptionDate.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForintContractorID
            // 
            this.ItemForintContractorID.Control = this.intContractorIDLookUpEdit;
            this.ItemForintContractorID.Location = new System.Drawing.Point(514, 0);
            this.ItemForintContractorID.Name = "ItemForintContractorID";
            this.ItemForintContractorID.Size = new System.Drawing.Size(515, 24);
            this.ItemForintContractorID.Text = "Team Name";
            this.ItemForintContractorID.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForContactMethodID
            // 
            this.ItemForContactMethodID.Control = this.ContactMethodIDLookUpEdit;
            this.ItemForContactMethodID.Location = new System.Drawing.Point(514, 24);
            this.ItemForContactMethodID.Name = "ItemForContactMethodID";
            this.ItemForContactMethodID.Size = new System.Drawing.Size(515, 24);
            this.ItemForContactMethodID.Text = "Contact Method";
            this.ItemForContactMethodID.TextSize = new System.Drawing.Size(83, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(1029, 398);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter
            // 
            this.sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp06806_OM_Team_ListTableAdapter
            // 
            this.sp06806_OM_Team_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp06807_OM_Team_ContactMethodTableAdapter
            // 
            this.sp06807_OM_Team_ContactMethodTableAdapter.ClearBeforeFill = true;
            // 
            // sp06809OMTeamRedemptionDuplicateSearchBindingSource
            // 
            this.sp06809OMTeamRedemptionDuplicateSearchBindingSource.DataMember = "sp06809_OM_Team_RedemptionDuplicateSearch";
            this.sp06809OMTeamRedemptionDuplicateSearchBindingSource.DataSource = this.dataSet_Teams;
            // 
            // sp06809_OM_Team_RedemptionDuplicateSearchTableAdapter
            // 
            this.sp06809_OM_Team_RedemptionDuplicateSearchTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Points_Team_Redemption_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1049, 547);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1065, 586);
            this.Name = "frm_OM_Points_Team_Redemption_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Team Redemption Points";
            this.Activated += new System.EventHandler(this.frm_OM_Points_Team_Redemption_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Points_Team_Redemption_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Points_Team_Redemption_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BSCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06802OMTeamPointsRedemptionHeaderItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Teams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intContractorIDLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06806OMTeamListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RedemptionDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RedemptionDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactMethodIDLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06807OMTeamContactMethodBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBSCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRedemptionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintContractorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactMethodID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06809OMTeamRedemptionDuplicateSearchBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit BSCodeTextEdit;
        private System.Windows.Forms.BindingSource sp06802OMTeamPointsRedemptionHeaderItemBindingSource;
        private DataSet_Teams dataSet_Teams;
        private DevExpress.XtraEditors.LookUpEdit intContractorIDLookUpEdit;
        private DevExpress.XtraEditors.DateEdit RedemptionDateDateEdit;
        private DevExpress.XtraEditors.LookUpEdit ContactMethodIDLookUpEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBSCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRedemptionDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintContractorID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactMethodID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DataSet_TeamsTableAdapters.sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter sp06802_OM_Team_PointsRedemptionHeaderItemTableAdapter;
        private System.Windows.Forms.BindingSource sp06806OMTeamListBindingSource;
        private DataSet_TeamsTableAdapters.sp06806_OM_Team_ListTableAdapter sp06806_OM_Team_ListTableAdapter;
        private System.Windows.Forms.BindingSource sp06807OMTeamContactMethodBindingSource;
        private DataSet_TeamsTableAdapters.sp06807_OM_Team_ContactMethodTableAdapter sp06807_OM_Team_ContactMethodTableAdapter;
        private System.Windows.Forms.BindingSource sp06809OMTeamRedemptionDuplicateSearchBindingSource;
        private DataSet_TeamsTableAdapters.sp06809_OM_Team_RedemptionDuplicateSearchTableAdapter sp06809_OM_Team_RedemptionDuplicateSearchTableAdapter;
    }
}

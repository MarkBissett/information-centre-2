namespace WoodPlan5
{
    partial class frm_OM_Select_Job_Material
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Select_Job_Material));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colActive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp06066OMSelectClientContractBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDatetime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDirector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp07194UTPermissionDocumentSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_WorkOrder = new WoodPlan5.DataSet_UT_WorkOrder();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnDateRangeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.beiDateFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.sp07194_UT_Permission_Document_SelectTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07194_UT_Permission_Document_SelectTableAdapter();
            this.sp06066_OM_Select_Client_ContractTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06066_OM_Select_Client_ContractTableAdapter();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp06117OMSiteContractsForClientContractBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractValue1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colYearlyPercentageIncrease = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLastClientPaymentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp06147OMVisitsForSiteContractBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVisitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpectedEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCalculationLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVisitSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditLatLong = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colStartLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCostCalculationLevelDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFriendlyVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellCalculationLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellCalculationLevelDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp06209OMJobsForVisitBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditLatLong4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colJobTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colJobNoLongerRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequiresAccessPermit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessPermitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceSystemPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateTime4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpectedEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric2DP4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpectedDurationUnitsDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScheduleSentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnitsDescriptionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateClientInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPayContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRouteOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartLatitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartLongitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLatitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLongitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedDurationUnitsDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnitsDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaximumDaysFromLastVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumDaysFromLastVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysLeeway = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colImagesFolderOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp06212OMMaterialsForJobsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMaterialUsedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterialID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostUnitsUsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCostUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellUnitsUsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSellUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostPerUnitExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCostPerUnitVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSellPerUnitExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellPerUnitVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostValueExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostValueVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostValueTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellValueExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellValueVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellValueTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterialName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurchaseOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp06117_OM_Site_Contracts_For_Client_ContractTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06117_OM_Site_Contracts_For_Client_ContractTableAdapter();
            this.sp06147_OM_Visits_For_Site_ContractTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06147_OM_Visits_For_Site_ContractTableAdapter();
            this.sp06209_OM_Jobs_For_VisitTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06209_OM_Jobs_For_VisitTableAdapter();
            this.sp06212_OM_Materials_For_JobsTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06212_OM_Materials_For_JobsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06066OMSelectClientContractBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDatetime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07194UTPermissionDocumentSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06117OMSiteContractsForClientContractBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06147OMVisitsForSiteContractBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06209OMJobsForVisitBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateTime4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06212OMMaterialsForJobsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(649, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 704);
            this.barDockControlBottom.Size = new System.Drawing.Size(649, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 678);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(649, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 678);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.beiDateFilter,
            this.bbiRefresh});
            this.barManager1.MaxItemId = 32;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 4;
            this.colActive.Width = 51;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colActive1
            // 
            this.colActive1.Caption = "Active";
            this.colActive1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colActive1.FieldName = "Active";
            this.colActive1.Name = "colActive1";
            this.colActive1.OptionsColumn.AllowEdit = false;
            this.colActive1.OptionsColumn.AllowFocus = false;
            this.colActive1.OptionsColumn.ReadOnly = true;
            this.colActive1.Visible = true;
            this.colActive1.VisibleIndex = 4;
            this.colActive1.Width = 51;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp06066OMSelectClientContractBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditDatetime,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditCurrency});
            this.gridControl1.Size = new System.Drawing.Size(624, 140);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp06066OMSelectClientContractBindingSource
            // 
            this.sp06066OMSelectClientContractBindingSource.DataMember = "sp06066_OM_Select_Client_Contract";
            this.sp06066OMSelectClientContractBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientContractID,
            this.colClientID,
            this.colStartDate,
            this.colEndDate,
            this.colActive,
            this.colSectorTypeID,
            this.colContractValue,
            this.colRemarks,
            this.colClientName,
            this.colGCCompanyName,
            this.colContractType,
            this.colContractStatus,
            this.colContractDirector,
            this.colSectorType,
            this.colContractDescription,
            this.colReactive});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActive;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.FindDelay = 2000;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colClientContractID
            // 
            this.colClientContractID.Caption = "Client Contract ID";
            this.colClientContractID.FieldName = "ClientContractID";
            this.colClientContractID.Name = "colClientContractID";
            this.colClientContractID.OptionsColumn.AllowEdit = false;
            this.colClientContractID.OptionsColumn.AllowFocus = false;
            this.colClientContractID.OptionsColumn.ReadOnly = true;
            this.colClientContractID.Width = 107;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.ColumnEdit = this.repositoryItemTextEditDatetime;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 2;
            this.colStartDate.Width = 100;
            // 
            // repositoryItemTextEditDatetime
            // 
            this.repositoryItemTextEditDatetime.AutoHeight = false;
            this.repositoryItemTextEditDatetime.Mask.EditMask = "g";
            this.repositoryItemTextEditDatetime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDatetime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDatetime.Name = "repositoryItemTextEditDatetime";
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "End Date";
            this.colEndDate.ColumnEdit = this.repositoryItemTextEditDatetime;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 3;
            this.colEndDate.Width = 100;
            // 
            // colSectorTypeID
            // 
            this.colSectorTypeID.Caption = "Sector Type ID";
            this.colSectorTypeID.FieldName = "SectorTypeID";
            this.colSectorTypeID.Name = "colSectorTypeID";
            this.colSectorTypeID.OptionsColumn.AllowEdit = false;
            this.colSectorTypeID.OptionsColumn.AllowFocus = false;
            this.colSectorTypeID.OptionsColumn.ReadOnly = true;
            this.colSectorTypeID.Width = 93;
            // 
            // colContractValue
            // 
            this.colContractValue.Caption = "Contract Value";
            this.colContractValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colContractValue.FieldName = "ContractValue";
            this.colContractValue.Name = "colContractValue";
            this.colContractValue.OptionsColumn.AllowEdit = false;
            this.colContractValue.OptionsColumn.AllowFocus = false;
            this.colContractValue.OptionsColumn.ReadOnly = true;
            this.colContractValue.Visible = true;
            this.colContractValue.VisibleIndex = 11;
            this.colContractValue.Width = 92;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 12;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 181;
            // 
            // colGCCompanyName
            // 
            this.colGCCompanyName.Caption = "GC Company Name";
            this.colGCCompanyName.FieldName = "GCCompanyName";
            this.colGCCompanyName.Name = "colGCCompanyName";
            this.colGCCompanyName.OptionsColumn.AllowEdit = false;
            this.colGCCompanyName.OptionsColumn.AllowFocus = false;
            this.colGCCompanyName.OptionsColumn.ReadOnly = true;
            this.colGCCompanyName.Visible = true;
            this.colGCCompanyName.VisibleIndex = 6;
            this.colGCCompanyName.Width = 113;
            // 
            // colContractType
            // 
            this.colContractType.Caption = "Contract Type";
            this.colContractType.FieldName = "ContractType";
            this.colContractType.Name = "colContractType";
            this.colContractType.OptionsColumn.AllowEdit = false;
            this.colContractType.OptionsColumn.AllowFocus = false;
            this.colContractType.OptionsColumn.ReadOnly = true;
            this.colContractType.Visible = true;
            this.colContractType.VisibleIndex = 7;
            this.colContractType.Width = 105;
            // 
            // colContractStatus
            // 
            this.colContractStatus.Caption = "Contract Status";
            this.colContractStatus.FieldName = "ContractStatus";
            this.colContractStatus.Name = "colContractStatus";
            this.colContractStatus.OptionsColumn.AllowEdit = false;
            this.colContractStatus.OptionsColumn.AllowFocus = false;
            this.colContractStatus.OptionsColumn.ReadOnly = true;
            this.colContractStatus.Visible = true;
            this.colContractStatus.VisibleIndex = 9;
            this.colContractStatus.Width = 118;
            // 
            // colContractDirector
            // 
            this.colContractDirector.Caption = "Contract Director";
            this.colContractDirector.FieldName = "ContractDirector";
            this.colContractDirector.Name = "colContractDirector";
            this.colContractDirector.OptionsColumn.AllowEdit = false;
            this.colContractDirector.OptionsColumn.AllowFocus = false;
            this.colContractDirector.OptionsColumn.ReadOnly = true;
            this.colContractDirector.Visible = true;
            this.colContractDirector.VisibleIndex = 10;
            this.colContractDirector.Width = 117;
            // 
            // colSectorType
            // 
            this.colSectorType.Caption = "Sector Type";
            this.colSectorType.FieldName = "SectorType";
            this.colSectorType.Name = "colSectorType";
            this.colSectorType.OptionsColumn.AllowEdit = false;
            this.colSectorType.OptionsColumn.AllowFocus = false;
            this.colSectorType.OptionsColumn.ReadOnly = true;
            this.colSectorType.Visible = true;
            this.colSectorType.VisibleIndex = 8;
            this.colSectorType.Width = 79;
            // 
            // colContractDescription
            // 
            this.colContractDescription.Caption = "Contract Description";
            this.colContractDescription.FieldName = "ContractDescription";
            this.colContractDescription.Name = "colContractDescription";
            this.colContractDescription.OptionsColumn.AllowEdit = false;
            this.colContractDescription.OptionsColumn.AllowFocus = false;
            this.colContractDescription.OptionsColumn.ReadOnly = true;
            this.colContractDescription.Visible = true;
            this.colContractDescription.VisibleIndex = 1;
            this.colContractDescription.Width = 285;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 5;
            this.colReactive.Width = 63;
            // 
            // sp07194UTPermissionDocumentSelectBindingSource
            // 
            this.sp07194UTPermissionDocumentSelectBindingSource.DataMember = "sp07194_UT_Permission_Document_Select";
            this.sp07194UTPermissionDocumentSelectBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // dataSet_UT_WorkOrder
            // 
            this.dataSet_UT_WorkOrder.DataSetName = "DataSet_UT_WorkOrder";
            this.dataSet_UT_WorkOrder.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(481, 676);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(562, 676);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeFilterOK);
            this.popupContainerControlDateRange.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(112, 29);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(180, 109);
            this.popupContainerControlDateRange.TabIndex = 5;
            // 
            // btnDateRangeFilterOK
            // 
            this.btnDateRangeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeFilterOK.Location = new System.Drawing.Point(3, 83);
            this.btnDateRangeFilterOK.Name = "btnDateRangeFilterOK";
            this.btnDateRangeFilterOK.Size = new System.Drawing.Size(34, 23);
            this.btnDateRangeFilterOK.TabIndex = 3;
            this.btnDateRangeFilterOK.Text = "OK";
            this.btnDateRangeFilterOK.Click += new System.EventHandler(this.btnDateRangeFilterOK_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.dateEditToDate);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFromDate);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(174, 76);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Date Range";
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Size = new System.Drawing.Size(129, 20);
            this.dateEditToDate.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Size = new System.Drawing.Size(129, 20);
            this.dateEditFromDate.TabIndex = 0;
            // 
            // beiDateFilter
            // 
            this.beiDateFilter.Caption = "No Date Filter:";
            this.beiDateFilter.Description = "Date Filter:";
            this.beiDateFilter.Edit = this.repositoryItemPopupContainerEdit1;
            this.beiDateFilter.EditValue = "No Date Filter";
            this.beiDateFilter.EditWidth = 168;
            this.beiDateFilter.Id = 30;
            this.beiDateFilter.Name = "beiDateFilter";
            this.beiDateFilter.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            this.repositoryItemPopupContainerEdit1.PopupControl = this.popupContainerControlDateRange;
            this.repositoryItemPopupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateRange_QueryResultValue);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh Data";
            this.bbiRefresh.Id = 31;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 1;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiDateFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.Text = "Filter";
            // 
            // sp07194_UT_Permission_Document_SelectTableAdapter
            // 
            this.sp07194_UT_Permission_Document_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // sp06066_OM_Select_Client_ContractTableAdapter
            // 
            this.sp06066_OM_Select_Client_ContractTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 26);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlDateRange);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Client Contracts";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(649, 644);
            this.splitContainerControl1.SplitterPosition = 143;
            this.splitContainerControl1.TabIndex = 7;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControl2);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Linked Site Contracts";
            this.splitContainerControl2.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel2.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl2.Size = new System.Drawing.Size(649, 495);
            this.splitContainerControl2.SplitterPosition = 137;
            this.splitContainerControl2.TabIndex = 6;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp06117OMSiteContractsForClientContractBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemTextEditPercentage});
            this.gridControl2.Size = new System.Drawing.Size(624, 134);
            this.gridControl2.TabIndex = 5;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp06117OMSiteContractsForClientContractBindingSource
            // 
            this.sp06117OMSiteContractsForClientContractBindingSource.DataMember = "sp06117_OM_Site_Contracts_For_Client_Contract";
            this.sp06117OMSiteContractsForClientContractBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteContractID,
            this.colClientContractID1,
            this.colSiteID,
            this.colStartDate1,
            this.colEndDate1,
            this.colActive1,
            this.colContractValue1,
            this.colYearlyPercentageIncrease,
            this.colLastClientPaymentDate,
            this.colRemarks1,
            this.colClientID1,
            this.colClientName1,
            this.colSiteName,
            this.colLinkedToParent});
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.colActive1;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView2.FormatRules.Add(gridFormatRule2);
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsFind.FindDelay = 2000;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colSiteContractID
            // 
            this.colSiteContractID.Caption = "Site Contract ID";
            this.colSiteContractID.FieldName = "SiteContractID";
            this.colSiteContractID.Name = "colSiteContractID";
            this.colSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSiteContractID.Width = 98;
            // 
            // colClientContractID1
            // 
            this.colClientContractID1.Caption = "Client Contract ID";
            this.colClientContractID1.FieldName = "ClientContractID";
            this.colClientContractID1.Name = "colClientContractID1";
            this.colClientContractID1.OptionsColumn.AllowEdit = false;
            this.colClientContractID1.OptionsColumn.AllowFocus = false;
            this.colClientContractID1.OptionsColumn.ReadOnly = true;
            this.colClientContractID1.Width = 107;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.Visible = true;
            this.colSiteID.VisibleIndex = 0;
            this.colSiteID.Width = 51;
            // 
            // colStartDate1
            // 
            this.colStartDate1.Caption = "Start Date";
            this.colStartDate1.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colStartDate1.FieldName = "StartDate";
            this.colStartDate1.Name = "colStartDate1";
            this.colStartDate1.OptionsColumn.AllowEdit = false;
            this.colStartDate1.OptionsColumn.AllowFocus = false;
            this.colStartDate1.OptionsColumn.ReadOnly = true;
            this.colStartDate1.Visible = true;
            this.colStartDate1.VisibleIndex = 2;
            this.colStartDate1.Width = 100;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colEndDate1
            // 
            this.colEndDate1.Caption = "End Date";
            this.colEndDate1.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colEndDate1.FieldName = "EndDate";
            this.colEndDate1.Name = "colEndDate1";
            this.colEndDate1.OptionsColumn.AllowEdit = false;
            this.colEndDate1.OptionsColumn.AllowFocus = false;
            this.colEndDate1.OptionsColumn.ReadOnly = true;
            this.colEndDate1.Visible = true;
            this.colEndDate1.VisibleIndex = 3;
            this.colEndDate1.Width = 100;
            // 
            // colContractValue1
            // 
            this.colContractValue1.Caption = "Contract Value";
            this.colContractValue1.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colContractValue1.FieldName = "ContractValue";
            this.colContractValue1.Name = "colContractValue1";
            this.colContractValue1.OptionsColumn.AllowEdit = false;
            this.colContractValue1.OptionsColumn.AllowFocus = false;
            this.colContractValue1.OptionsColumn.ReadOnly = true;
            this.colContractValue1.Visible = true;
            this.colContractValue1.VisibleIndex = 5;
            this.colContractValue1.Width = 92;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // colYearlyPercentageIncrease
            // 
            this.colYearlyPercentageIncrease.Caption = "Yearly % Increase";
            this.colYearlyPercentageIncrease.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colYearlyPercentageIncrease.FieldName = "YearlyPercentageIncrease";
            this.colYearlyPercentageIncrease.Name = "colYearlyPercentageIncrease";
            this.colYearlyPercentageIncrease.OptionsColumn.AllowEdit = false;
            this.colYearlyPercentageIncrease.OptionsColumn.AllowFocus = false;
            this.colYearlyPercentageIncrease.OptionsColumn.ReadOnly = true;
            this.colYearlyPercentageIncrease.Visible = true;
            this.colYearlyPercentageIncrease.VisibleIndex = 6;
            this.colYearlyPercentageIncrease.Width = 110;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // colLastClientPaymentDate
            // 
            this.colLastClientPaymentDate.Caption = "Last Client Payment";
            this.colLastClientPaymentDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colLastClientPaymentDate.FieldName = "LastClientPaymentDate";
            this.colLastClientPaymentDate.Name = "colLastClientPaymentDate";
            this.colLastClientPaymentDate.OptionsColumn.AllowEdit = false;
            this.colLastClientPaymentDate.OptionsColumn.AllowFocus = false;
            this.colLastClientPaymentDate.OptionsColumn.ReadOnly = true;
            this.colLastClientPaymentDate.Width = 116;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 7;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Width = 237;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 1;
            this.colSiteName.Width = 211;
            // 
            // colLinkedToParent
            // 
            this.colLinkedToParent.Caption = "Linked To Contract";
            this.colLinkedToParent.FieldName = "LinkedToParent";
            this.colLinkedToParent.Name = "colLinkedToParent";
            this.colLinkedToParent.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent.Width = 369;
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Horizontal = false;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl3.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl3.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl3.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl3.Panel1.Controls.Add(this.gridControl3);
            this.splitContainerControl3.Panel1.ShowCaption = true;
            this.splitContainerControl3.Panel1.Text = "Linked Visits ";
            this.splitContainerControl3.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl3.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl3.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl3.Panel2.Controls.Add(this.splitContainerControl4);
            this.splitContainerControl3.Size = new System.Drawing.Size(649, 352);
            this.splitContainerControl3.SplitterPosition = 118;
            this.splitContainerControl3.TabIndex = 7;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp06147OMVisitsForSiteContractBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEditDateTime3,
            this.repositoryItemTextEditMoney3,
            this.repositoryItemTextEditLatLong});
            this.gridControl3.Size = new System.Drawing.Size(624, 115);
            this.gridControl3.TabIndex = 6;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp06147OMVisitsForSiteContractBindingSource
            // 
            this.sp06147OMVisitsForSiteContractBindingSource.DataMember = "sp06147_OM_Visits_For_Site_Contract";
            this.sp06147OMVisitsForSiteContractBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVisitID,
            this.colSiteContractID1,
            this.colVisitNumber,
            this.colCreatedByStaffID,
            this.colExpectedStartDate,
            this.colExpectedEndDate,
            this.colStartDate2,
            this.colEndDate2,
            this.colCostCalculationLevel,
            this.colVisitCost,
            this.colVisitSell,
            this.colStartLatitude,
            this.colStartLongitude,
            this.colFinishLatitude,
            this.colFinishLongitude,
            this.colRemarks2,
            this.colCostCalculationLevelDescription,
            this.colCreatedByStaffName,
            this.colFriendlyVisitNumber,
            this.colSellCalculationLevel,
            this.colSellCalculationLevelDescription});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsFind.FindDelay = 2000;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsLayout.StoreFormatRules = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colVisitNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colVisitID
            // 
            this.colVisitID.Caption = "Visit ID";
            this.colVisitID.FieldName = "VisitID";
            this.colVisitID.Name = "colVisitID";
            this.colVisitID.OptionsColumn.AllowEdit = false;
            this.colVisitID.OptionsColumn.AllowFocus = false;
            this.colVisitID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteContractID1
            // 
            this.colSiteContractID1.Caption = "Site Contract ID";
            this.colSiteContractID1.FieldName = "SiteContractID";
            this.colSiteContractID1.Name = "colSiteContractID1";
            this.colSiteContractID1.OptionsColumn.AllowEdit = false;
            this.colSiteContractID1.OptionsColumn.AllowFocus = false;
            this.colSiteContractID1.OptionsColumn.ReadOnly = true;
            this.colSiteContractID1.Width = 98;
            // 
            // colVisitNumber
            // 
            this.colVisitNumber.Caption = "Visit Number";
            this.colVisitNumber.FieldName = "VisitNumber";
            this.colVisitNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colVisitNumber.Name = "colVisitNumber";
            this.colVisitNumber.OptionsColumn.AllowEdit = false;
            this.colVisitNumber.OptionsColumn.AllowFocus = false;
            this.colVisitNumber.OptionsColumn.ReadOnly = true;
            this.colVisitNumber.Visible = true;
            this.colVisitNumber.VisibleIndex = 0;
            this.colVisitNumber.Width = 93;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 116;
            // 
            // colExpectedStartDate
            // 
            this.colExpectedStartDate.Caption = "Expected Start Date";
            this.colExpectedStartDate.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colExpectedStartDate.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate.Name = "colExpectedStartDate";
            this.colExpectedStartDate.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate.Visible = true;
            this.colExpectedStartDate.VisibleIndex = 2;
            this.colExpectedStartDate.Width = 119;
            // 
            // repositoryItemTextEditDateTime3
            // 
            this.repositoryItemTextEditDateTime3.AutoHeight = false;
            this.repositoryItemTextEditDateTime3.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime3.Name = "repositoryItemTextEditDateTime3";
            // 
            // colExpectedEndDate
            // 
            this.colExpectedEndDate.Caption = "Expected End Date";
            this.colExpectedEndDate.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colExpectedEndDate.FieldName = "ExpectedEndDate";
            this.colExpectedEndDate.Name = "colExpectedEndDate";
            this.colExpectedEndDate.OptionsColumn.AllowEdit = false;
            this.colExpectedEndDate.OptionsColumn.AllowFocus = false;
            this.colExpectedEndDate.OptionsColumn.ReadOnly = true;
            this.colExpectedEndDate.Visible = true;
            this.colExpectedEndDate.VisibleIndex = 3;
            this.colExpectedEndDate.Width = 113;
            // 
            // colStartDate2
            // 
            this.colStartDate2.Caption = "Start Date";
            this.colStartDate2.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colStartDate2.FieldName = "StartDate";
            this.colStartDate2.Name = "colStartDate2";
            this.colStartDate2.OptionsColumn.AllowEdit = false;
            this.colStartDate2.OptionsColumn.AllowFocus = false;
            this.colStartDate2.OptionsColumn.ReadOnly = true;
            this.colStartDate2.Visible = true;
            this.colStartDate2.VisibleIndex = 4;
            this.colStartDate2.Width = 100;
            // 
            // colEndDate2
            // 
            this.colEndDate2.Caption = "End Date";
            this.colEndDate2.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colEndDate2.FieldName = "EndDate";
            this.colEndDate2.Name = "colEndDate2";
            this.colEndDate2.OptionsColumn.AllowEdit = false;
            this.colEndDate2.OptionsColumn.AllowFocus = false;
            this.colEndDate2.OptionsColumn.ReadOnly = true;
            this.colEndDate2.Visible = true;
            this.colEndDate2.VisibleIndex = 5;
            this.colEndDate2.Width = 100;
            // 
            // colCostCalculationLevel
            // 
            this.colCostCalculationLevel.Caption = "Cost Calculation ID";
            this.colCostCalculationLevel.FieldName = "CostCalculationLevel";
            this.colCostCalculationLevel.Name = "colCostCalculationLevel";
            this.colCostCalculationLevel.OptionsColumn.AllowEdit = false;
            this.colCostCalculationLevel.OptionsColumn.AllowFocus = false;
            this.colCostCalculationLevel.OptionsColumn.ReadOnly = true;
            this.colCostCalculationLevel.Width = 140;
            // 
            // colVisitCost
            // 
            this.colVisitCost.Caption = "Visit Cost";
            this.colVisitCost.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.colVisitCost.FieldName = "VisitCost";
            this.colVisitCost.Name = "colVisitCost";
            this.colVisitCost.OptionsColumn.AllowEdit = false;
            this.colVisitCost.OptionsColumn.AllowFocus = false;
            this.colVisitCost.OptionsColumn.ReadOnly = true;
            this.colVisitCost.Visible = true;
            this.colVisitCost.VisibleIndex = 8;
            // 
            // repositoryItemTextEditMoney3
            // 
            this.repositoryItemTextEditMoney3.AutoHeight = false;
            this.repositoryItemTextEditMoney3.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney3.Name = "repositoryItemTextEditMoney3";
            // 
            // colVisitSell
            // 
            this.colVisitSell.Caption = "Visit Sell";
            this.colVisitSell.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.colVisitSell.FieldName = "VisitSell";
            this.colVisitSell.Name = "colVisitSell";
            this.colVisitSell.OptionsColumn.AllowEdit = false;
            this.colVisitSell.OptionsColumn.AllowFocus = false;
            this.colVisitSell.OptionsColumn.ReadOnly = true;
            this.colVisitSell.Visible = true;
            this.colVisitSell.VisibleIndex = 9;
            // 
            // colStartLatitude
            // 
            this.colStartLatitude.Caption = "Start Latitude";
            this.colStartLatitude.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colStartLatitude.FieldName = "StartLatitude";
            this.colStartLatitude.Name = "colStartLatitude";
            this.colStartLatitude.OptionsColumn.AllowEdit = false;
            this.colStartLatitude.OptionsColumn.AllowFocus = false;
            this.colStartLatitude.OptionsColumn.ReadOnly = true;
            this.colStartLatitude.Visible = true;
            this.colStartLatitude.VisibleIndex = 11;
            this.colStartLatitude.Width = 87;
            // 
            // repositoryItemTextEditLatLong
            // 
            this.repositoryItemTextEditLatLong.AutoHeight = false;
            this.repositoryItemTextEditLatLong.Mask.EditMask = "n8";
            this.repositoryItemTextEditLatLong.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditLatLong.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditLatLong.Name = "repositoryItemTextEditLatLong";
            // 
            // colStartLongitude
            // 
            this.colStartLongitude.Caption = "Start Longitude";
            this.colStartLongitude.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colStartLongitude.FieldName = "StartLongitude";
            this.colStartLongitude.Name = "colStartLongitude";
            this.colStartLongitude.OptionsColumn.AllowEdit = false;
            this.colStartLongitude.OptionsColumn.AllowFocus = false;
            this.colStartLongitude.OptionsColumn.ReadOnly = true;
            this.colStartLongitude.Visible = true;
            this.colStartLongitude.VisibleIndex = 12;
            this.colStartLongitude.Width = 95;
            // 
            // colFinishLatitude
            // 
            this.colFinishLatitude.Caption = "Finish Latitude";
            this.colFinishLatitude.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colFinishLatitude.FieldName = "FinishLatitude";
            this.colFinishLatitude.Name = "colFinishLatitude";
            this.colFinishLatitude.OptionsColumn.AllowEdit = false;
            this.colFinishLatitude.OptionsColumn.AllowFocus = false;
            this.colFinishLatitude.OptionsColumn.ReadOnly = true;
            this.colFinishLatitude.Visible = true;
            this.colFinishLatitude.VisibleIndex = 13;
            this.colFinishLatitude.Width = 90;
            // 
            // colFinishLongitude
            // 
            this.colFinishLongitude.Caption = "Finish Longitude";
            this.colFinishLongitude.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colFinishLongitude.FieldName = "FinishLongitude";
            this.colFinishLongitude.Name = "colFinishLongitude";
            this.colFinishLongitude.OptionsColumn.AllowEdit = false;
            this.colFinishLongitude.OptionsColumn.AllowFocus = false;
            this.colFinishLongitude.OptionsColumn.ReadOnly = true;
            this.colFinishLongitude.Visible = true;
            this.colFinishLongitude.VisibleIndex = 14;
            this.colFinishLongitude.Width = 98;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 15;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colCostCalculationLevelDescription
            // 
            this.colCostCalculationLevelDescription.Caption = "Cost Calculation";
            this.colCostCalculationLevelDescription.FieldName = "CostCalculationLevelDescription";
            this.colCostCalculationLevelDescription.Name = "colCostCalculationLevelDescription";
            this.colCostCalculationLevelDescription.OptionsColumn.AllowEdit = false;
            this.colCostCalculationLevelDescription.OptionsColumn.AllowFocus = false;
            this.colCostCalculationLevelDescription.OptionsColumn.ReadOnly = true;
            this.colCostCalculationLevelDescription.Visible = true;
            this.colCostCalculationLevelDescription.VisibleIndex = 6;
            this.colCostCalculationLevelDescription.Width = 100;
            // 
            // colCreatedByStaffName
            // 
            this.colCreatedByStaffName.Caption = "Created By";
            this.colCreatedByStaffName.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName.Name = "colCreatedByStaffName";
            this.colCreatedByStaffName.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName.Visible = true;
            this.colCreatedByStaffName.VisibleIndex = 10;
            this.colCreatedByStaffName.Width = 151;
            // 
            // colFriendlyVisitNumber
            // 
            this.colFriendlyVisitNumber.Caption = "Friendly Visit #";
            this.colFriendlyVisitNumber.FieldName = "FriendlyVisitNumber";
            this.colFriendlyVisitNumber.Name = "colFriendlyVisitNumber";
            this.colFriendlyVisitNumber.OptionsColumn.AllowEdit = false;
            this.colFriendlyVisitNumber.OptionsColumn.AllowFocus = false;
            this.colFriendlyVisitNumber.OptionsColumn.ReadOnly = true;
            this.colFriendlyVisitNumber.Visible = true;
            this.colFriendlyVisitNumber.VisibleIndex = 1;
            this.colFriendlyVisitNumber.Width = 90;
            // 
            // colSellCalculationLevel
            // 
            this.colSellCalculationLevel.Caption = "Sell Calculation ID";
            this.colSellCalculationLevel.FieldName = "SellCalculationLevel";
            this.colSellCalculationLevel.Name = "colSellCalculationLevel";
            this.colSellCalculationLevel.OptionsColumn.AllowEdit = false;
            this.colSellCalculationLevel.OptionsColumn.AllowFocus = false;
            this.colSellCalculationLevel.OptionsColumn.ReadOnly = true;
            this.colSellCalculationLevel.Width = 104;
            // 
            // colSellCalculationLevelDescription
            // 
            this.colSellCalculationLevelDescription.Caption = "Sell Calculation";
            this.colSellCalculationLevelDescription.FieldName = "SellCalculationLevelDescription";
            this.colSellCalculationLevelDescription.Name = "colSellCalculationLevelDescription";
            this.colSellCalculationLevelDescription.OptionsColumn.AllowEdit = false;
            this.colSellCalculationLevelDescription.OptionsColumn.AllowFocus = false;
            this.colSellCalculationLevelDescription.OptionsColumn.ReadOnly = true;
            this.colSellCalculationLevelDescription.Visible = true;
            this.colSellCalculationLevelDescription.VisibleIndex = 7;
            this.colSellCalculationLevelDescription.Width = 100;
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl4.Horizontal = false;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl4.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl4.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl4.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl4.Panel1.Controls.Add(this.gridControl4);
            this.splitContainerControl4.Panel1.ShowCaption = true;
            this.splitContainerControl4.Panel1.Text = "Linked Jobs";
            this.splitContainerControl4.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl4.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl4.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl4.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl4.Panel2.Controls.Add(this.gridControl5);
            this.splitContainerControl4.Panel2.ShowCaption = true;
            this.splitContainerControl4.Panel2.Text = "Linked Materials ";
            this.splitContainerControl4.Size = new System.Drawing.Size(649, 228);
            this.splitContainerControl4.SplitterPosition = 111;
            this.splitContainerControl4.TabIndex = 8;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp06209OMJobsForVisitBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemDateTime4,
            this.repositoryItemTextEditCurrency4,
            this.repositoryItemTextEditLatLong4,
            this.repositoryItemTextEditInteger4,
            this.repositoryItemCheckEdit4,
            this.repositoryItemTextEditNumeric2DP4});
            this.gridControl4.Size = new System.Drawing.Size(624, 108);
            this.gridControl4.TabIndex = 7;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp06209OMJobsForVisitBindingSource
            // 
            this.sp06209OMJobsForVisitBindingSource.DataMember = "sp06209_OM_Jobs_For_Visit";
            this.sp06209OMJobsForVisitBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobID,
            this.colClientID2,
            this.colClientContractID2,
            this.colSiteID1,
            this.colSiteContractID2,
            this.colVisitID1,
            this.colJobTypeID,
            this.colJobSubTypeID,
            this.colSitePostcode,
            this.colLocationX,
            this.colLocationY,
            this.colClientName2,
            this.colSiteName1,
            this.colVisitNumber1,
            this.colJobTypeDescription,
            this.colJobSubTypeDescription,
            this.colJobStatusID,
            this.colCreatedByStaffID1,
            this.colReactive1,
            this.colJobNoLongerRequired,
            this.colRequiresAccessPermit,
            this.colAccessPermitID,
            this.colClientPONumber,
            this.colClientPOID,
            this.colFinanceSystemPONumber,
            this.colExpectedStartDate1,
            this.colExpectedEndDate1,
            this.colExpectedDurationUnits,
            this.colExpectedDurationUnitsDescriptorID,
            this.colScheduleSentDate,
            this.colActualStartDate,
            this.colActualEndDate,
            this.colActualDurationUnits,
            this.colActualDurationUnitsDescriptionID,
            this.colClientInvoiceID,
            this.colDateClientInvoiced,
            this.colSelfBillingInvoiceID,
            this.colDoNotPayContractor,
            this.colDoNotInvoiceClient,
            this.colRemarks3,
            this.colRouteOrder,
            this.colStartLatitude1,
            this.colStartLongitude1,
            this.colFinishLatitude1,
            this.colFinishLongitude1,
            this.colJobStatusDescription,
            this.colExpectedDurationUnitsDescriptor,
            this.colActualDurationUnitsDescriptor,
            this.colTenderID,
            this.colTenderJobID,
            this.colMaximumDaysFromLastVisit,
            this.colMinimumDaysFromLastVisit,
            this.colDaysLeeway,
            this.colBillingCentreCodeID,
            this.colImagesFolderOM});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsFind.FindDelay = 2000;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsLayout.StoreFormatRules = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colExpectedStartDate1, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colExpectedEndDate1, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colJobID
            // 
            this.colJobID.Caption = "Job ID";
            this.colJobID.FieldName = "JobID";
            this.colJobID.Name = "colJobID";
            this.colJobID.OptionsColumn.AllowEdit = false;
            this.colJobID.OptionsColumn.AllowFocus = false;
            this.colJobID.OptionsColumn.ReadOnly = true;
            this.colJobID.Width = 52;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            this.colClientID2.Width = 62;
            // 
            // colClientContractID2
            // 
            this.colClientContractID2.Caption = "Client Contract ID";
            this.colClientContractID2.FieldName = "ClientContractID";
            this.colClientContractID2.Name = "colClientContractID2";
            this.colClientContractID2.OptionsColumn.AllowEdit = false;
            this.colClientContractID2.OptionsColumn.AllowFocus = false;
            this.colClientContractID2.OptionsColumn.ReadOnly = true;
            this.colClientContractID2.Width = 107;
            // 
            // colSiteID1
            // 
            this.colSiteID1.Caption = "Site ID";
            this.colSiteID1.FieldName = "SiteID";
            this.colSiteID1.Name = "colSiteID1";
            this.colSiteID1.OptionsColumn.AllowEdit = false;
            this.colSiteID1.OptionsColumn.AllowFocus = false;
            this.colSiteID1.OptionsColumn.ReadOnly = true;
            this.colSiteID1.Width = 53;
            // 
            // colSiteContractID2
            // 
            this.colSiteContractID2.Caption = "Ste Contract ID";
            this.colSiteContractID2.FieldName = "SiteContractID";
            this.colSiteContractID2.Name = "colSiteContractID2";
            this.colSiteContractID2.OptionsColumn.AllowEdit = false;
            this.colSiteContractID2.OptionsColumn.AllowFocus = false;
            this.colSiteContractID2.OptionsColumn.ReadOnly = true;
            this.colSiteContractID2.Width = 96;
            // 
            // colVisitID1
            // 
            this.colVisitID1.Caption = "Visit ID";
            this.colVisitID1.FieldName = "VisitID";
            this.colVisitID1.Name = "colVisitID1";
            this.colVisitID1.OptionsColumn.AllowEdit = false;
            this.colVisitID1.OptionsColumn.AllowFocus = false;
            this.colVisitID1.OptionsColumn.ReadOnly = true;
            this.colVisitID1.Width = 54;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            this.colJobTypeID.Width = 79;
            // 
            // colJobSubTypeID
            // 
            this.colJobSubTypeID.Caption = "Job Sub-Type ID";
            this.colJobSubTypeID.FieldName = "JobSubTypeID";
            this.colJobSubTypeID.Name = "colJobSubTypeID";
            this.colJobSubTypeID.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeID.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeID.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeID.Width = 101;
            // 
            // colSitePostcode
            // 
            this.colSitePostcode.Caption = "Site Postcode";
            this.colSitePostcode.FieldName = "SitePostcode";
            this.colSitePostcode.Name = "colSitePostcode";
            this.colSitePostcode.OptionsColumn.AllowEdit = false;
            this.colSitePostcode.OptionsColumn.AllowFocus = false;
            this.colSitePostcode.OptionsColumn.ReadOnly = true;
            this.colSitePostcode.Width = 86;
            // 
            // colLocationX
            // 
            this.colLocationX.Caption = "Site Latitude";
            this.colLocationX.ColumnEdit = this.repositoryItemTextEditLatLong4;
            this.colLocationX.FieldName = "LocationX";
            this.colLocationX.Name = "colLocationX";
            this.colLocationX.OptionsColumn.AllowEdit = false;
            this.colLocationX.OptionsColumn.AllowFocus = false;
            this.colLocationX.OptionsColumn.ReadOnly = true;
            this.colLocationX.Width = 81;
            // 
            // repositoryItemTextEditLatLong4
            // 
            this.repositoryItemTextEditLatLong4.AutoHeight = false;
            this.repositoryItemTextEditLatLong4.Mask.EditMask = "n8";
            this.repositoryItemTextEditLatLong4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditLatLong4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditLatLong4.Name = "repositoryItemTextEditLatLong4";
            // 
            // colLocationY
            // 
            this.colLocationY.Caption = "Site Longitude";
            this.colLocationY.ColumnEdit = this.repositoryItemTextEditLatLong4;
            this.colLocationY.FieldName = "LocationY";
            this.colLocationY.Name = "colLocationY";
            this.colLocationY.OptionsColumn.AllowEdit = false;
            this.colLocationY.OptionsColumn.AllowFocus = false;
            this.colLocationY.OptionsColumn.ReadOnly = true;
            this.colLocationY.Width = 89;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 171;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Width = 180;
            // 
            // colVisitNumber1
            // 
            this.colVisitNumber1.Caption = "Visit #";
            this.colVisitNumber1.ColumnEdit = this.repositoryItemTextEditInteger4;
            this.colVisitNumber1.FieldName = "VisitNumber";
            this.colVisitNumber1.Name = "colVisitNumber1";
            this.colVisitNumber1.OptionsColumn.AllowEdit = false;
            this.colVisitNumber1.OptionsColumn.AllowFocus = false;
            this.colVisitNumber1.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemTextEditInteger4
            // 
            this.repositoryItemTextEditInteger4.AutoHeight = false;
            this.repositoryItemTextEditInteger4.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger4.Name = "repositoryItemTextEditInteger4";
            // 
            // colJobTypeDescription
            // 
            this.colJobTypeDescription.Caption = "Job Type Description";
            this.colJobTypeDescription.FieldName = "JobTypeDescription";
            this.colJobTypeDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colJobTypeDescription.Name = "colJobTypeDescription";
            this.colJobTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription.Visible = true;
            this.colJobTypeDescription.VisibleIndex = 0;
            this.colJobTypeDescription.Width = 121;
            // 
            // colJobSubTypeDescription
            // 
            this.colJobSubTypeDescription.Caption = "Sub-Type Description";
            this.colJobSubTypeDescription.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colJobSubTypeDescription.Name = "colJobSubTypeDescription";
            this.colJobSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription.Visible = true;
            this.colJobSubTypeDescription.VisibleIndex = 1;
            this.colJobSubTypeDescription.Width = 123;
            // 
            // colJobStatusID
            // 
            this.colJobStatusID.Caption = "Job Status ID";
            this.colJobStatusID.FieldName = "JobStatusID";
            this.colJobStatusID.Name = "colJobStatusID";
            this.colJobStatusID.OptionsColumn.AllowEdit = false;
            this.colJobStatusID.OptionsColumn.AllowFocus = false;
            this.colJobStatusID.OptionsColumn.ReadOnly = true;
            this.colJobStatusID.Width = 86;
            // 
            // colCreatedByStaffID1
            // 
            this.colCreatedByStaffID1.Caption = "Created By Staff ID";
            this.colCreatedByStaffID1.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID1.Name = "colCreatedByStaffID1";
            this.colCreatedByStaffID1.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID1.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID1.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID1.Width = 119;
            // 
            // colReactive1
            // 
            this.colReactive1.Caption = "Reactive";
            this.colReactive1.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colReactive1.FieldName = "Reactive";
            this.colReactive1.Name = "colReactive1";
            this.colReactive1.OptionsColumn.AllowEdit = false;
            this.colReactive1.OptionsColumn.AllowFocus = false;
            this.colReactive1.OptionsColumn.ReadOnly = true;
            this.colReactive1.Visible = true;
            this.colReactive1.VisibleIndex = 2;
            this.colReactive1.Width = 63;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // colJobNoLongerRequired
            // 
            this.colJobNoLongerRequired.Caption = "No Longer Required";
            this.colJobNoLongerRequired.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colJobNoLongerRequired.FieldName = "JobNoLongerRequired";
            this.colJobNoLongerRequired.Name = "colJobNoLongerRequired";
            this.colJobNoLongerRequired.OptionsColumn.AllowEdit = false;
            this.colJobNoLongerRequired.OptionsColumn.AllowFocus = false;
            this.colJobNoLongerRequired.OptionsColumn.ReadOnly = true;
            this.colJobNoLongerRequired.Visible = true;
            this.colJobNoLongerRequired.VisibleIndex = 14;
            this.colJobNoLongerRequired.Width = 116;
            // 
            // colRequiresAccessPermit
            // 
            this.colRequiresAccessPermit.Caption = "Requires Access Permit";
            this.colRequiresAccessPermit.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colRequiresAccessPermit.FieldName = "RequiresAccessPermit";
            this.colRequiresAccessPermit.Name = "colRequiresAccessPermit";
            this.colRequiresAccessPermit.OptionsColumn.AllowEdit = false;
            this.colRequiresAccessPermit.OptionsColumn.AllowFocus = false;
            this.colRequiresAccessPermit.OptionsColumn.ReadOnly = true;
            this.colRequiresAccessPermit.Visible = true;
            this.colRequiresAccessPermit.VisibleIndex = 13;
            this.colRequiresAccessPermit.Width = 132;
            // 
            // colAccessPermitID
            // 
            this.colAccessPermitID.Caption = "Access Permit ID";
            this.colAccessPermitID.FieldName = "AccessPermitID";
            this.colAccessPermitID.Name = "colAccessPermitID";
            this.colAccessPermitID.OptionsColumn.AllowEdit = false;
            this.colAccessPermitID.OptionsColumn.AllowFocus = false;
            this.colAccessPermitID.OptionsColumn.ReadOnly = true;
            this.colAccessPermitID.Width = 101;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "Client P.O. #";
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.OptionsColumn.AllowEdit = false;
            this.colClientPONumber.OptionsColumn.AllowFocus = false;
            this.colClientPONumber.OptionsColumn.ReadOnly = true;
            this.colClientPONumber.Width = 84;
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "Client P.O. ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            this.colClientPOID.Width = 87;
            // 
            // colFinanceSystemPONumber
            // 
            this.colFinanceSystemPONumber.Caption = "Finance System #";
            this.colFinanceSystemPONumber.FieldName = "FinanceSystemPONumber";
            this.colFinanceSystemPONumber.Name = "colFinanceSystemPONumber";
            this.colFinanceSystemPONumber.OptionsColumn.AllowEdit = false;
            this.colFinanceSystemPONumber.OptionsColumn.AllowFocus = false;
            this.colFinanceSystemPONumber.OptionsColumn.ReadOnly = true;
            this.colFinanceSystemPONumber.Width = 107;
            // 
            // colExpectedStartDate1
            // 
            this.colExpectedStartDate1.Caption = "Expected Start";
            this.colExpectedStartDate1.ColumnEdit = this.repositoryItemDateTime4;
            this.colExpectedStartDate1.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate1.Name = "colExpectedStartDate1";
            this.colExpectedStartDate1.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate1.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate1.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate1.Visible = true;
            this.colExpectedStartDate1.VisibleIndex = 4;
            this.colExpectedStartDate1.Width = 106;
            // 
            // repositoryItemDateTime4
            // 
            this.repositoryItemDateTime4.AutoHeight = false;
            this.repositoryItemDateTime4.Mask.EditMask = "g";
            this.repositoryItemDateTime4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemDateTime4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateTime4.Name = "repositoryItemDateTime4";
            // 
            // colExpectedEndDate1
            // 
            this.colExpectedEndDate1.Caption = "Expected End";
            this.colExpectedEndDate1.ColumnEdit = this.repositoryItemDateTime4;
            this.colExpectedEndDate1.FieldName = "ExpectedEndDate";
            this.colExpectedEndDate1.Name = "colExpectedEndDate1";
            this.colExpectedEndDate1.OptionsColumn.AllowEdit = false;
            this.colExpectedEndDate1.OptionsColumn.AllowFocus = false;
            this.colExpectedEndDate1.OptionsColumn.ReadOnly = true;
            this.colExpectedEndDate1.Visible = true;
            this.colExpectedEndDate1.VisibleIndex = 5;
            this.colExpectedEndDate1.Width = 100;
            // 
            // colExpectedDurationUnits
            // 
            this.colExpectedDurationUnits.Caption = "Expected Duration";
            this.colExpectedDurationUnits.ColumnEdit = this.repositoryItemTextEditNumeric2DP4;
            this.colExpectedDurationUnits.FieldName = "ExpectedDurationUnits";
            this.colExpectedDurationUnits.Name = "colExpectedDurationUnits";
            this.colExpectedDurationUnits.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnits.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnits.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnits.Visible = true;
            this.colExpectedDurationUnits.VisibleIndex = 6;
            this.colExpectedDurationUnits.Width = 110;
            // 
            // repositoryItemTextEditNumeric2DP4
            // 
            this.repositoryItemTextEditNumeric2DP4.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP4.Mask.EditMask = "f2";
            this.repositoryItemTextEditNumeric2DP4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP4.Name = "repositoryItemTextEditNumeric2DP4";
            // 
            // colExpectedDurationUnitsDescriptorID
            // 
            this.colExpectedDurationUnitsDescriptorID.Caption = "Expected Duration Descriptor ID";
            this.colExpectedDurationUnitsDescriptorID.FieldName = "ExpectedDurationUnitsDescriptorID";
            this.colExpectedDurationUnitsDescriptorID.Name = "colExpectedDurationUnitsDescriptorID";
            this.colExpectedDurationUnitsDescriptorID.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnitsDescriptorID.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnitsDescriptorID.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnitsDescriptorID.Width = 176;
            // 
            // colScheduleSentDate
            // 
            this.colScheduleSentDate.Caption = "Schedule Sent";
            this.colScheduleSentDate.ColumnEdit = this.repositoryItemDateTime4;
            this.colScheduleSentDate.FieldName = "ScheduleSentDate";
            this.colScheduleSentDate.Name = "colScheduleSentDate";
            this.colScheduleSentDate.OptionsColumn.AllowEdit = false;
            this.colScheduleSentDate.OptionsColumn.AllowFocus = false;
            this.colScheduleSentDate.OptionsColumn.ReadOnly = true;
            this.colScheduleSentDate.Visible = true;
            this.colScheduleSentDate.VisibleIndex = 8;
            this.colScheduleSentDate.Width = 100;
            // 
            // colActualStartDate
            // 
            this.colActualStartDate.Caption = "Actual Start";
            this.colActualStartDate.ColumnEdit = this.repositoryItemDateTime4;
            this.colActualStartDate.FieldName = "ActualStartDate";
            this.colActualStartDate.Name = "colActualStartDate";
            this.colActualStartDate.OptionsColumn.AllowEdit = false;
            this.colActualStartDate.OptionsColumn.AllowFocus = false;
            this.colActualStartDate.OptionsColumn.ReadOnly = true;
            this.colActualStartDate.Visible = true;
            this.colActualStartDate.VisibleIndex = 9;
            this.colActualStartDate.Width = 100;
            // 
            // colActualEndDate
            // 
            this.colActualEndDate.Caption = "Actual End";
            this.colActualEndDate.ColumnEdit = this.repositoryItemDateTime4;
            this.colActualEndDate.FieldName = "ActualEndDate";
            this.colActualEndDate.Name = "colActualEndDate";
            this.colActualEndDate.OptionsColumn.AllowEdit = false;
            this.colActualEndDate.OptionsColumn.AllowFocus = false;
            this.colActualEndDate.OptionsColumn.ReadOnly = true;
            this.colActualEndDate.Visible = true;
            this.colActualEndDate.VisibleIndex = 10;
            this.colActualEndDate.Width = 100;
            // 
            // colActualDurationUnits
            // 
            this.colActualDurationUnits.Caption = "Actual Duration";
            this.colActualDurationUnits.ColumnEdit = this.repositoryItemTextEditNumeric2DP4;
            this.colActualDurationUnits.FieldName = "ActualDurationUnits";
            this.colActualDurationUnits.Name = "colActualDurationUnits";
            this.colActualDurationUnits.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnits.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnits.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnits.Visible = true;
            this.colActualDurationUnits.VisibleIndex = 11;
            this.colActualDurationUnits.Width = 95;
            // 
            // colActualDurationUnitsDescriptionID
            // 
            this.colActualDurationUnitsDescriptionID.Caption = "Actual Duration Descriptor ID";
            this.colActualDurationUnitsDescriptionID.FieldName = "ActualDurationUnitsDescriptionID";
            this.colActualDurationUnitsDescriptionID.Name = "colActualDurationUnitsDescriptionID";
            this.colActualDurationUnitsDescriptionID.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnitsDescriptionID.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnitsDescriptionID.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnitsDescriptionID.Width = 161;
            // 
            // colClientInvoiceID
            // 
            this.colClientInvoiceID.Caption = "Client Invoice ID";
            this.colClientInvoiceID.FieldName = "ClientInvoiceID";
            this.colClientInvoiceID.Name = "colClientInvoiceID";
            this.colClientInvoiceID.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceID.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceID.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceID.Width = 100;
            // 
            // colDateClientInvoiced
            // 
            this.colDateClientInvoiced.Caption = "Date Client Invoiced";
            this.colDateClientInvoiced.ColumnEdit = this.repositoryItemDateTime4;
            this.colDateClientInvoiced.FieldName = "DateClientInvoiced";
            this.colDateClientInvoiced.Name = "colDateClientInvoiced";
            this.colDateClientInvoiced.OptionsColumn.AllowEdit = false;
            this.colDateClientInvoiced.OptionsColumn.AllowFocus = false;
            this.colDateClientInvoiced.OptionsColumn.ReadOnly = true;
            this.colDateClientInvoiced.Visible = true;
            this.colDateClientInvoiced.VisibleIndex = 15;
            this.colDateClientInvoiced.Width = 118;
            // 
            // colSelfBillingInvoiceID
            // 
            this.colSelfBillingInvoiceID.Caption = "Self-Billing Invoice ID";
            this.colSelfBillingInvoiceID.FieldName = "SelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.Name = "colSelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceID.Width = 121;
            // 
            // colDoNotPayContractor
            // 
            this.colDoNotPayContractor.Caption = "Don\'t Pay Contractor";
            this.colDoNotPayContractor.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colDoNotPayContractor.FieldName = "DoNotPayContractor";
            this.colDoNotPayContractor.Name = "colDoNotPayContractor";
            this.colDoNotPayContractor.OptionsColumn.AllowEdit = false;
            this.colDoNotPayContractor.OptionsColumn.AllowFocus = false;
            this.colDoNotPayContractor.OptionsColumn.ReadOnly = true;
            this.colDoNotPayContractor.Visible = true;
            this.colDoNotPayContractor.VisibleIndex = 16;
            this.colDoNotPayContractor.Width = 122;
            // 
            // colDoNotInvoiceClient
            // 
            this.colDoNotInvoiceClient.Caption = "Don\'t Invoice Client";
            this.colDoNotInvoiceClient.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colDoNotInvoiceClient.FieldName = "DoNotInvoiceClient";
            this.colDoNotInvoiceClient.Name = "colDoNotInvoiceClient";
            this.colDoNotInvoiceClient.OptionsColumn.AllowEdit = false;
            this.colDoNotInvoiceClient.OptionsColumn.AllowFocus = false;
            this.colDoNotInvoiceClient.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClient.Visible = true;
            this.colDoNotInvoiceClient.VisibleIndex = 17;
            this.colDoNotInvoiceClient.Width = 114;
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.OptionsColumn.ReadOnly = true;
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 18;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colRouteOrder
            // 
            this.colRouteOrder.Caption = "Route Order";
            this.colRouteOrder.ColumnEdit = this.repositoryItemTextEditInteger4;
            this.colRouteOrder.FieldName = "RouteOrder";
            this.colRouteOrder.Name = "colRouteOrder";
            this.colRouteOrder.OptionsColumn.AllowEdit = false;
            this.colRouteOrder.OptionsColumn.AllowFocus = false;
            this.colRouteOrder.OptionsColumn.ReadOnly = true;
            this.colRouteOrder.Width = 81;
            // 
            // colStartLatitude1
            // 
            this.colStartLatitude1.Caption = "Start Latitude";
            this.colStartLatitude1.ColumnEdit = this.repositoryItemTextEditLatLong4;
            this.colStartLatitude1.FieldName = "StartLatitude";
            this.colStartLatitude1.Name = "colStartLatitude1";
            this.colStartLatitude1.OptionsColumn.AllowEdit = false;
            this.colStartLatitude1.OptionsColumn.AllowFocus = false;
            this.colStartLatitude1.OptionsColumn.ReadOnly = true;
            this.colStartLatitude1.Visible = true;
            this.colStartLatitude1.VisibleIndex = 19;
            this.colStartLatitude1.Width = 87;
            // 
            // colStartLongitude1
            // 
            this.colStartLongitude1.Caption = "Start Longitude";
            this.colStartLongitude1.ColumnEdit = this.repositoryItemTextEditLatLong4;
            this.colStartLongitude1.FieldName = "StartLongitude";
            this.colStartLongitude1.Name = "colStartLongitude1";
            this.colStartLongitude1.OptionsColumn.AllowEdit = false;
            this.colStartLongitude1.OptionsColumn.AllowFocus = false;
            this.colStartLongitude1.OptionsColumn.ReadOnly = true;
            this.colStartLongitude1.Visible = true;
            this.colStartLongitude1.VisibleIndex = 20;
            this.colStartLongitude1.Width = 95;
            // 
            // colFinishLatitude1
            // 
            this.colFinishLatitude1.Caption = "Finish Latitude";
            this.colFinishLatitude1.ColumnEdit = this.repositoryItemTextEditLatLong4;
            this.colFinishLatitude1.FieldName = "FinishLatitude";
            this.colFinishLatitude1.Name = "colFinishLatitude1";
            this.colFinishLatitude1.OptionsColumn.AllowEdit = false;
            this.colFinishLatitude1.OptionsColumn.AllowFocus = false;
            this.colFinishLatitude1.OptionsColumn.ReadOnly = true;
            this.colFinishLatitude1.Visible = true;
            this.colFinishLatitude1.VisibleIndex = 21;
            this.colFinishLatitude1.Width = 90;
            // 
            // colFinishLongitude1
            // 
            this.colFinishLongitude1.Caption = "Finish Longitude";
            this.colFinishLongitude1.ColumnEdit = this.repositoryItemTextEditLatLong4;
            this.colFinishLongitude1.FieldName = "FinishLongitude";
            this.colFinishLongitude1.Name = "colFinishLongitude1";
            this.colFinishLongitude1.OptionsColumn.AllowEdit = false;
            this.colFinishLongitude1.OptionsColumn.AllowFocus = false;
            this.colFinishLongitude1.OptionsColumn.ReadOnly = true;
            this.colFinishLongitude1.Visible = true;
            this.colFinishLongitude1.VisibleIndex = 22;
            this.colFinishLongitude1.Width = 98;
            // 
            // colJobStatusDescription
            // 
            this.colJobStatusDescription.Caption = "Job Status";
            this.colJobStatusDescription.FieldName = "JobStatusDescription";
            this.colJobStatusDescription.Name = "colJobStatusDescription";
            this.colJobStatusDescription.OptionsColumn.AllowEdit = false;
            this.colJobStatusDescription.OptionsColumn.AllowFocus = false;
            this.colJobStatusDescription.OptionsColumn.ReadOnly = true;
            this.colJobStatusDescription.Visible = true;
            this.colJobStatusDescription.VisibleIndex = 3;
            this.colJobStatusDescription.Width = 124;
            // 
            // colExpectedDurationUnitsDescriptor
            // 
            this.colExpectedDurationUnitsDescriptor.Caption = "Expected Duration Descriptor";
            this.colExpectedDurationUnitsDescriptor.FieldName = "ExpectedDurationUnitsDescriptor";
            this.colExpectedDurationUnitsDescriptor.Name = "colExpectedDurationUnitsDescriptor";
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnitsDescriptor.Visible = true;
            this.colExpectedDurationUnitsDescriptor.VisibleIndex = 7;
            this.colExpectedDurationUnitsDescriptor.Width = 162;
            // 
            // colActualDurationUnitsDescriptor
            // 
            this.colActualDurationUnitsDescriptor.Caption = "Actual Duration Descriptor";
            this.colActualDurationUnitsDescriptor.FieldName = "ActualDurationUnitsDescriptor";
            this.colActualDurationUnitsDescriptor.Name = "colActualDurationUnitsDescriptor";
            this.colActualDurationUnitsDescriptor.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnitsDescriptor.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnitsDescriptor.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnitsDescriptor.Visible = true;
            this.colActualDurationUnitsDescriptor.VisibleIndex = 12;
            this.colActualDurationUnitsDescriptor.Width = 147;
            // 
            // colTenderID
            // 
            this.colTenderID.Caption = "Tender ID";
            this.colTenderID.FieldName = "TenderID";
            this.colTenderID.Name = "colTenderID";
            this.colTenderID.OptionsColumn.AllowEdit = false;
            this.colTenderID.OptionsColumn.AllowFocus = false;
            this.colTenderID.OptionsColumn.ReadOnly = true;
            // 
            // colTenderJobID
            // 
            this.colTenderJobID.Caption = "Tender Job ID";
            this.colTenderJobID.FieldName = "TenderJobID";
            this.colTenderJobID.Name = "colTenderJobID";
            this.colTenderJobID.OptionsColumn.AllowEdit = false;
            this.colTenderJobID.OptionsColumn.AllowFocus = false;
            this.colTenderJobID.OptionsColumn.ReadOnly = true;
            this.colTenderJobID.Width = 89;
            // 
            // colMaximumDaysFromLastVisit
            // 
            this.colMaximumDaysFromLastVisit.Caption = "Max Days From Last Visit";
            this.colMaximumDaysFromLastVisit.ColumnEdit = this.repositoryItemTextEditInteger4;
            this.colMaximumDaysFromLastVisit.FieldName = "MaximumDaysFromLastVisit";
            this.colMaximumDaysFromLastVisit.Name = "colMaximumDaysFromLastVisit";
            this.colMaximumDaysFromLastVisit.OptionsColumn.AllowEdit = false;
            this.colMaximumDaysFromLastVisit.OptionsColumn.AllowFocus = false;
            this.colMaximumDaysFromLastVisit.OptionsColumn.ReadOnly = true;
            this.colMaximumDaysFromLastVisit.Visible = true;
            this.colMaximumDaysFromLastVisit.VisibleIndex = 23;
            this.colMaximumDaysFromLastVisit.Width = 140;
            // 
            // colMinimumDaysFromLastVisit
            // 
            this.colMinimumDaysFromLastVisit.Caption = "Min Days From Last Visit";
            this.colMinimumDaysFromLastVisit.ColumnEdit = this.repositoryItemTextEditInteger4;
            this.colMinimumDaysFromLastVisit.FieldName = "MinimumDaysFromLastVisit";
            this.colMinimumDaysFromLastVisit.Name = "colMinimumDaysFromLastVisit";
            this.colMinimumDaysFromLastVisit.OptionsColumn.AllowEdit = false;
            this.colMinimumDaysFromLastVisit.OptionsColumn.AllowFocus = false;
            this.colMinimumDaysFromLastVisit.OptionsColumn.ReadOnly = true;
            this.colMinimumDaysFromLastVisit.Visible = true;
            this.colMinimumDaysFromLastVisit.VisibleIndex = 24;
            this.colMinimumDaysFromLastVisit.Width = 136;
            // 
            // colDaysLeeway
            // 
            this.colDaysLeeway.Caption = "Days Leeway";
            this.colDaysLeeway.ColumnEdit = this.repositoryItemTextEditInteger4;
            this.colDaysLeeway.FieldName = "DaysLeeway";
            this.colDaysLeeway.Name = "colDaysLeeway";
            this.colDaysLeeway.OptionsColumn.AllowEdit = false;
            this.colDaysLeeway.OptionsColumn.AllowFocus = false;
            this.colDaysLeeway.OptionsColumn.ReadOnly = true;
            this.colDaysLeeway.Visible = true;
            this.colDaysLeeway.VisibleIndex = 25;
            this.colDaysLeeway.Width = 85;
            // 
            // colBillingCentreCodeID
            // 
            this.colBillingCentreCodeID.Caption = "Billing Centre Code ID";
            this.colBillingCentreCodeID.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID.Name = "colBillingCentreCodeID";
            this.colBillingCentreCodeID.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID.Width = 125;
            // 
            // colImagesFolderOM
            // 
            this.colImagesFolderOM.Caption = "Images Folder";
            this.colImagesFolderOM.FieldName = "ImagesFolderOM";
            this.colImagesFolderOM.Name = "colImagesFolderOM";
            this.colImagesFolderOM.OptionsColumn.AllowEdit = false;
            this.colImagesFolderOM.OptionsColumn.AllowFocus = false;
            this.colImagesFolderOM.Width = 87;
            // 
            // repositoryItemTextEditCurrency4
            // 
            this.repositoryItemTextEditCurrency4.AutoHeight = false;
            this.repositoryItemTextEditCurrency4.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency4.Name = "repositoryItemTextEditCurrency4";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp06212OMMaterialsForJobsBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemTextEditDateTime5,
            this.repositoryItemTextEditCurrency5,
            this.repositoryItemTextEdit2DP5,
            this.repositoryItemTextEditPercentage5});
            this.gridControl5.Size = new System.Drawing.Size(624, 108);
            this.gridControl5.TabIndex = 8;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp06212OMMaterialsForJobsBindingSource
            // 
            this.sp06212OMMaterialsForJobsBindingSource.DataMember = "sp06212_OM_Materials_For_Jobs";
            this.sp06212OMMaterialsForJobsBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMaterialUsedID,
            this.colMaterialID,
            this.colJobID1,
            this.colCostUnitsUsed,
            this.colCostUnitDescriptorID,
            this.colSellUnitsUsed,
            this.colSellUnitDescriptorID,
            this.colCostPerUnitExVat,
            this.colCostPerUnitVatRate,
            this.colSellPerUnitExVat,
            this.colSellPerUnitVatRate,
            this.colCostValueExVat,
            this.colCostValueVat,
            this.colCostValueTotal,
            this.colSellValueExVat,
            this.colSellValueVat,
            this.colSellValueTotal,
            this.colRemarks4,
            this.colClientID3,
            this.colSiteID2,
            this.colSiteName2,
            this.colClientName3,
            this.colJobSubTypeDescription1,
            this.colJobTypeDescription1,
            this.colExpectedStartDate2,
            this.colVisitNumber2,
            this.colFullDescription,
            this.colMaterialName,
            this.colCostUnitDescriptor,
            this.colSellUnitDescriptor,
            this.colPurchaseOrderID,
            this.colVisitID2});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsFind.FindDelay = 2000;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsLayout.StoreFormatRules = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colMaterialName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colMaterialUsedID
            // 
            this.colMaterialUsedID.Caption = "Material Used ID";
            this.colMaterialUsedID.FieldName = "MaterialUsedID";
            this.colMaterialUsedID.Name = "colMaterialUsedID";
            this.colMaterialUsedID.OptionsColumn.AllowEdit = false;
            this.colMaterialUsedID.OptionsColumn.AllowFocus = false;
            this.colMaterialUsedID.OptionsColumn.ReadOnly = true;
            this.colMaterialUsedID.Width = 95;
            // 
            // colMaterialID
            // 
            this.colMaterialID.Caption = "Material ID";
            this.colMaterialID.FieldName = "MaterialID";
            this.colMaterialID.Name = "colMaterialID";
            this.colMaterialID.OptionsColumn.AllowEdit = false;
            this.colMaterialID.OptionsColumn.AllowFocus = false;
            this.colMaterialID.OptionsColumn.ReadOnly = true;
            this.colMaterialID.Width = 87;
            // 
            // colJobID1
            // 
            this.colJobID1.Caption = "Job ID";
            this.colJobID1.FieldName = "JobID";
            this.colJobID1.Name = "colJobID1";
            this.colJobID1.OptionsColumn.AllowEdit = false;
            this.colJobID1.OptionsColumn.AllowFocus = false;
            this.colJobID1.OptionsColumn.ReadOnly = true;
            // 
            // colCostUnitsUsed
            // 
            this.colCostUnitsUsed.Caption = "Cost Units Used";
            this.colCostUnitsUsed.ColumnEdit = this.repositoryItemTextEditDateTime5;
            this.colCostUnitsUsed.FieldName = "CostUnitsUsed";
            this.colCostUnitsUsed.Name = "colCostUnitsUsed";
            this.colCostUnitsUsed.OptionsColumn.AllowEdit = false;
            this.colCostUnitsUsed.OptionsColumn.AllowFocus = false;
            this.colCostUnitsUsed.OptionsColumn.ReadOnly = true;
            this.colCostUnitsUsed.Visible = true;
            this.colCostUnitsUsed.VisibleIndex = 1;
            this.colCostUnitsUsed.Width = 97;
            // 
            // repositoryItemTextEditDateTime5
            // 
            this.repositoryItemTextEditDateTime5.AutoHeight = false;
            this.repositoryItemTextEditDateTime5.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime5.Name = "repositoryItemTextEditDateTime5";
            // 
            // colCostUnitDescriptorID
            // 
            this.colCostUnitDescriptorID.Caption = "Cost Unit Descriptor ID";
            this.colCostUnitDescriptorID.FieldName = "CostUnitDescriptorID";
            this.colCostUnitDescriptorID.Name = "colCostUnitDescriptorID";
            this.colCostUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colCostUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colCostUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colCostUnitDescriptorID.Width = 131;
            // 
            // colSellUnitsUsed
            // 
            this.colSellUnitsUsed.Caption = "Sell Units Used";
            this.colSellUnitsUsed.ColumnEdit = this.repositoryItemTextEdit2DP5;
            this.colSellUnitsUsed.FieldName = "SellUnitsUsed";
            this.colSellUnitsUsed.Name = "colSellUnitsUsed";
            this.colSellUnitsUsed.OptionsColumn.AllowEdit = false;
            this.colSellUnitsUsed.OptionsColumn.AllowFocus = false;
            this.colSellUnitsUsed.OptionsColumn.ReadOnly = true;
            this.colSellUnitsUsed.Visible = true;
            this.colSellUnitsUsed.VisibleIndex = 8;
            this.colSellUnitsUsed.Width = 91;
            // 
            // repositoryItemTextEdit2DP5
            // 
            this.repositoryItemTextEdit2DP5.AutoHeight = false;
            this.repositoryItemTextEdit2DP5.Mask.EditMask = "n0";
            this.repositoryItemTextEdit2DP5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP5.Name = "repositoryItemTextEdit2DP5";
            // 
            // colSellUnitDescriptorID
            // 
            this.colSellUnitDescriptorID.Caption = "Sell Unit Descriptor ID";
            this.colSellUnitDescriptorID.FieldName = "SellUnitDescriptorID";
            this.colSellUnitDescriptorID.Name = "colSellUnitDescriptorID";
            this.colSellUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colSellUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colSellUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colSellUnitDescriptorID.Width = 125;
            // 
            // colCostPerUnitExVat
            // 
            this.colCostPerUnitExVat.Caption = "Unit Cost Ex VAT";
            this.colCostPerUnitExVat.ColumnEdit = this.repositoryItemTextEditCurrency5;
            this.colCostPerUnitExVat.FieldName = "CostPerUnitExVat";
            this.colCostPerUnitExVat.Name = "colCostPerUnitExVat";
            this.colCostPerUnitExVat.OptionsColumn.AllowEdit = false;
            this.colCostPerUnitExVat.OptionsColumn.AllowFocus = false;
            this.colCostPerUnitExVat.OptionsColumn.ReadOnly = true;
            this.colCostPerUnitExVat.Visible = true;
            this.colCostPerUnitExVat.VisibleIndex = 3;
            this.colCostPerUnitExVat.Width = 102;
            // 
            // repositoryItemTextEditCurrency5
            // 
            this.repositoryItemTextEditCurrency5.AutoHeight = false;
            this.repositoryItemTextEditCurrency5.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency5.Name = "repositoryItemTextEditCurrency5";
            // 
            // colCostPerUnitVatRate
            // 
            this.colCostPerUnitVatRate.Caption = "Unit Cost VAT Rate";
            this.colCostPerUnitVatRate.ColumnEdit = this.repositoryItemTextEditPercentage5;
            this.colCostPerUnitVatRate.FieldName = "CostPerUnitVatRate";
            this.colCostPerUnitVatRate.Name = "colCostPerUnitVatRate";
            this.colCostPerUnitVatRate.OptionsColumn.AllowEdit = false;
            this.colCostPerUnitVatRate.OptionsColumn.AllowFocus = false;
            this.colCostPerUnitVatRate.OptionsColumn.ReadOnly = true;
            this.colCostPerUnitVatRate.Visible = true;
            this.colCostPerUnitVatRate.VisibleIndex = 4;
            this.colCostPerUnitVatRate.Width = 113;
            // 
            // repositoryItemTextEditPercentage5
            // 
            this.repositoryItemTextEditPercentage5.AutoHeight = false;
            this.repositoryItemTextEditPercentage5.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage5.Name = "repositoryItemTextEditPercentage5";
            // 
            // colSellPerUnitExVat
            // 
            this.colSellPerUnitExVat.Caption = "Unit Sell Ex VAT";
            this.colSellPerUnitExVat.ColumnEdit = this.repositoryItemTextEditCurrency5;
            this.colSellPerUnitExVat.FieldName = "SellPerUnitExVat";
            this.colSellPerUnitExVat.Name = "colSellPerUnitExVat";
            this.colSellPerUnitExVat.OptionsColumn.AllowEdit = false;
            this.colSellPerUnitExVat.OptionsColumn.AllowFocus = false;
            this.colSellPerUnitExVat.OptionsColumn.ReadOnly = true;
            this.colSellPerUnitExVat.Visible = true;
            this.colSellPerUnitExVat.VisibleIndex = 10;
            this.colSellPerUnitExVat.Width = 96;
            // 
            // colSellPerUnitVatRate
            // 
            this.colSellPerUnitVatRate.Caption = "Unit Sell VAT Rate";
            this.colSellPerUnitVatRate.ColumnEdit = this.repositoryItemTextEditPercentage5;
            this.colSellPerUnitVatRate.FieldName = "SellPerUnitVatRate";
            this.colSellPerUnitVatRate.Name = "colSellPerUnitVatRate";
            this.colSellPerUnitVatRate.OptionsColumn.AllowEdit = false;
            this.colSellPerUnitVatRate.OptionsColumn.AllowFocus = false;
            this.colSellPerUnitVatRate.OptionsColumn.ReadOnly = true;
            this.colSellPerUnitVatRate.Visible = true;
            this.colSellPerUnitVatRate.VisibleIndex = 11;
            this.colSellPerUnitVatRate.Width = 107;
            // 
            // colCostValueExVat
            // 
            this.colCostValueExVat.Caption = "Cost Ex. VAT";
            this.colCostValueExVat.ColumnEdit = this.repositoryItemTextEditCurrency5;
            this.colCostValueExVat.FieldName = "CostValueExVat";
            this.colCostValueExVat.Name = "colCostValueExVat";
            this.colCostValueExVat.OptionsColumn.AllowEdit = false;
            this.colCostValueExVat.OptionsColumn.AllowFocus = false;
            this.colCostValueExVat.OptionsColumn.ReadOnly = true;
            this.colCostValueExVat.Visible = true;
            this.colCostValueExVat.VisibleIndex = 5;
            this.colCostValueExVat.Width = 84;
            // 
            // colCostValueVat
            // 
            this.colCostValueVat.Caption = "Cost VAT";
            this.colCostValueVat.ColumnEdit = this.repositoryItemTextEditCurrency5;
            this.colCostValueVat.FieldName = "CostValueVat";
            this.colCostValueVat.Name = "colCostValueVat";
            this.colCostValueVat.OptionsColumn.AllowEdit = false;
            this.colCostValueVat.OptionsColumn.AllowFocus = false;
            this.colCostValueVat.OptionsColumn.ReadOnly = true;
            this.colCostValueVat.Visible = true;
            this.colCostValueVat.VisibleIndex = 6;
            // 
            // colCostValueTotal
            // 
            this.colCostValueTotal.Caption = "Cost Total";
            this.colCostValueTotal.ColumnEdit = this.repositoryItemTextEditCurrency5;
            this.colCostValueTotal.FieldName = "CostValueTotal";
            this.colCostValueTotal.Name = "colCostValueTotal";
            this.colCostValueTotal.OptionsColumn.AllowEdit = false;
            this.colCostValueTotal.OptionsColumn.AllowFocus = false;
            this.colCostValueTotal.OptionsColumn.ReadOnly = true;
            this.colCostValueTotal.Visible = true;
            this.colCostValueTotal.VisibleIndex = 7;
            // 
            // colSellValueExVat
            // 
            this.colSellValueExVat.Caption = "Sell Ex. VAT";
            this.colSellValueExVat.ColumnEdit = this.repositoryItemTextEditCurrency5;
            this.colSellValueExVat.FieldName = "SellValueExVat";
            this.colSellValueExVat.Name = "colSellValueExVat";
            this.colSellValueExVat.OptionsColumn.AllowEdit = false;
            this.colSellValueExVat.OptionsColumn.AllowFocus = false;
            this.colSellValueExVat.OptionsColumn.ReadOnly = true;
            this.colSellValueExVat.Visible = true;
            this.colSellValueExVat.VisibleIndex = 12;
            this.colSellValueExVat.Width = 78;
            // 
            // colSellValueVat
            // 
            this.colSellValueVat.Caption = "Sell VAT";
            this.colSellValueVat.ColumnEdit = this.repositoryItemTextEditCurrency5;
            this.colSellValueVat.FieldName = "SellValueVat";
            this.colSellValueVat.Name = "colSellValueVat";
            this.colSellValueVat.OptionsColumn.AllowEdit = false;
            this.colSellValueVat.OptionsColumn.AllowFocus = false;
            this.colSellValueVat.OptionsColumn.ReadOnly = true;
            this.colSellValueVat.Visible = true;
            this.colSellValueVat.VisibleIndex = 13;
            // 
            // colSellValueTotal
            // 
            this.colSellValueTotal.Caption = "Sell Total";
            this.colSellValueTotal.ColumnEdit = this.repositoryItemTextEditCurrency5;
            this.colSellValueTotal.FieldName = "SellValueTotal";
            this.colSellValueTotal.Name = "colSellValueTotal";
            this.colSellValueTotal.OptionsColumn.AllowEdit = false;
            this.colSellValueTotal.OptionsColumn.AllowFocus = false;
            this.colSellValueTotal.OptionsColumn.ReadOnly = true;
            this.colSellValueTotal.Visible = true;
            this.colSellValueTotal.VisibleIndex = 14;
            // 
            // colRemarks4
            // 
            this.colRemarks4.Caption = "Remarks";
            this.colRemarks4.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRemarks4.FieldName = "Remarks";
            this.colRemarks4.Name = "colRemarks4";
            this.colRemarks4.OptionsColumn.ReadOnly = true;
            this.colRemarks4.Visible = true;
            this.colRemarks4.VisibleIndex = 15;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colClientID3
            // 
            this.colClientID3.Caption = "Client ID";
            this.colClientID3.FieldName = "ClientID";
            this.colClientID3.Name = "colClientID3";
            this.colClientID3.OptionsColumn.AllowEdit = false;
            this.colClientID3.OptionsColumn.AllowFocus = false;
            this.colClientID3.OptionsColumn.ReadOnly = true;
            // 
            // colSiteID2
            // 
            this.colSiteID2.Caption = "Site ID";
            this.colSiteID2.FieldName = "SiteID";
            this.colSiteID2.Name = "colSiteID2";
            this.colSiteID2.OptionsColumn.AllowEdit = false;
            this.colSiteID2.OptionsColumn.AllowFocus = false;
            this.colSiteID2.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName2
            // 
            this.colSiteName2.Caption = "Site Name";
            this.colSiteName2.FieldName = "SiteName";
            this.colSiteName2.Name = "colSiteName2";
            this.colSiteName2.OptionsColumn.AllowEdit = false;
            this.colSiteName2.OptionsColumn.AllowFocus = false;
            this.colSiteName2.OptionsColumn.ReadOnly = true;
            this.colSiteName2.Width = 196;
            // 
            // colClientName3
            // 
            this.colClientName3.Caption = "Client Name";
            this.colClientName3.FieldName = "ClientName";
            this.colClientName3.Name = "colClientName3";
            this.colClientName3.OptionsColumn.AllowEdit = false;
            this.colClientName3.OptionsColumn.AllowFocus = false;
            this.colClientName3.OptionsColumn.ReadOnly = true;
            this.colClientName3.Width = 199;
            // 
            // colJobSubTypeDescription1
            // 
            this.colJobSubTypeDescription1.Caption = "Job Sub-Type";
            this.colJobSubTypeDescription1.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription1.Name = "colJobSubTypeDescription1";
            this.colJobSubTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription1.Width = 142;
            // 
            // colJobTypeDescription1
            // 
            this.colJobTypeDescription1.Caption = "Job Type";
            this.colJobTypeDescription1.FieldName = "JobTypeDescription";
            this.colJobTypeDescription1.Name = "colJobTypeDescription1";
            this.colJobTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription1.Width = 147;
            // 
            // colExpectedStartDate2
            // 
            this.colExpectedStartDate2.Caption = "Expected Start Date";
            this.colExpectedStartDate2.ColumnEdit = this.repositoryItemTextEditDateTime5;
            this.colExpectedStartDate2.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate2.Name = "colExpectedStartDate2";
            this.colExpectedStartDate2.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate2.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate2.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate2.Width = 119;
            // 
            // colVisitNumber2
            // 
            this.colVisitNumber2.Caption = "Visit #";
            this.colVisitNumber2.FieldName = "VisitNumber";
            this.colVisitNumber2.Name = "colVisitNumber2";
            this.colVisitNumber2.OptionsColumn.AllowEdit = false;
            this.colVisitNumber2.OptionsColumn.AllowFocus = false;
            this.colVisitNumber2.OptionsColumn.ReadOnly = true;
            this.colVisitNumber2.Visible = true;
            this.colVisitNumber2.VisibleIndex = 16;
            // 
            // colFullDescription
            // 
            this.colFullDescription.Caption = "Full Description";
            this.colFullDescription.FieldName = "FullDescription";
            this.colFullDescription.Name = "colFullDescription";
            this.colFullDescription.OptionsColumn.AllowEdit = false;
            this.colFullDescription.OptionsColumn.AllowFocus = false;
            this.colFullDescription.OptionsColumn.ReadOnly = true;
            this.colFullDescription.Width = 210;
            // 
            // colMaterialName
            // 
            this.colMaterialName.Caption = "Material Type";
            this.colMaterialName.FieldName = "MaterialName";
            this.colMaterialName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colMaterialName.Name = "colMaterialName";
            this.colMaterialName.OptionsColumn.AllowEdit = false;
            this.colMaterialName.OptionsColumn.AllowFocus = false;
            this.colMaterialName.OptionsColumn.ReadOnly = true;
            this.colMaterialName.Visible = true;
            this.colMaterialName.VisibleIndex = 0;
            this.colMaterialName.Width = 147;
            // 
            // colCostUnitDescriptor
            // 
            this.colCostUnitDescriptor.Caption = "Cost Unit Descriptor";
            this.colCostUnitDescriptor.FieldName = "CostUnitDescriptor";
            this.colCostUnitDescriptor.Name = "colCostUnitDescriptor";
            this.colCostUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colCostUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colCostUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colCostUnitDescriptor.Visible = true;
            this.colCostUnitDescriptor.VisibleIndex = 2;
            this.colCostUnitDescriptor.Width = 117;
            // 
            // colSellUnitDescriptor
            // 
            this.colSellUnitDescriptor.Caption = "Sell Unit Descriptor";
            this.colSellUnitDescriptor.FieldName = "SellUnitDescriptor";
            this.colSellUnitDescriptor.Name = "colSellUnitDescriptor";
            this.colSellUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colSellUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colSellUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colSellUnitDescriptor.Visible = true;
            this.colSellUnitDescriptor.VisibleIndex = 9;
            this.colSellUnitDescriptor.Width = 111;
            // 
            // colPurchaseOrderID
            // 
            this.colPurchaseOrderID.Caption = "P.O. ID";
            this.colPurchaseOrderID.FieldName = "PurchaseOrderID";
            this.colPurchaseOrderID.Name = "colPurchaseOrderID";
            this.colPurchaseOrderID.OptionsColumn.AllowEdit = false;
            this.colPurchaseOrderID.OptionsColumn.AllowFocus = false;
            this.colPurchaseOrderID.OptionsColumn.ReadOnly = true;
            this.colPurchaseOrderID.Visible = true;
            this.colPurchaseOrderID.VisibleIndex = 17;
            // 
            // colVisitID2
            // 
            this.colVisitID2.Caption = "Visit ID";
            this.colVisitID2.FieldName = "VisitID";
            this.colVisitID2.Name = "colVisitID2";
            this.colVisitID2.OptionsColumn.AllowEdit = false;
            this.colVisitID2.OptionsColumn.AllowFocus = false;
            this.colVisitID2.OptionsColumn.ReadOnly = true;
            // 
            // sp06117_OM_Site_Contracts_For_Client_ContractTableAdapter
            // 
            this.sp06117_OM_Site_Contracts_For_Client_ContractTableAdapter.ClearBeforeFill = true;
            // 
            // sp06147_OM_Visits_For_Site_ContractTableAdapter
            // 
            this.sp06147_OM_Visits_For_Site_ContractTableAdapter.ClearBeforeFill = true;
            // 
            // sp06209_OM_Jobs_For_VisitTableAdapter
            // 
            this.sp06209_OM_Jobs_For_VisitTableAdapter.ClearBeforeFill = true;
            // 
            // sp06212_OM_Materials_For_JobsTableAdapter
            // 
            this.sp06212_OM_Materials_For_JobsTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Select_Job_Material
            // 
            this.ClientSize = new System.Drawing.Size(649, 704);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_OM_Select_Job_Material";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Job Material";
            this.Load += new System.EventHandler(this.frm_OM_Select_Job_Material_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06066OMSelectClientContractBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDatetime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07194UTPermissionDocumentSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06117OMSiteContractsForClientContractBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06147OMVisitsForSiteContractBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06209OMJobsForVisitBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateTime4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06212OMMaterialsForJobsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDatetime;
        private DevExpress.XtraBars.BarEditItem beiDateFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeFilterOK;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraBars.Bar bar1;
        private System.Windows.Forms.BindingSource sp07194UTPermissionDocumentSelectBindingSource;
        private DataSet_UT_WorkOrder dataSet_UT_WorkOrder;
        private DataSet_UT_WorkOrderTableAdapters.sp07194_UT_Permission_Document_SelectTableAdapter sp07194_UT_Permission_Document_SelectTableAdapter;
        private System.Windows.Forms.BindingSource sp06066OMSelectClientContractBindingSource;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DataSet_OM_ContractTableAdapters.sp06066_OM_Select_Client_ContractTableAdapter sp06066_OM_Select_Client_ContractTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colGCCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colContractType;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDirector;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorType;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private System.Windows.Forms.BindingSource sp06117OMSiteContractsForClientContractBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colActive1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractValue1;
        private DevExpress.XtraGrid.Columns.GridColumn colYearlyPercentageIncrease;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colLastClientPaymentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent;
        private DataSet_OM_ContractTableAdapters.sp06117_OM_Site_Contracts_For_Client_ContractTableAdapter sp06117_OM_Site_Contracts_For_Client_ContractTableAdapter;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private System.Windows.Forms.BindingSource sp06147OMVisitsForSiteContractBindingSource;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private DataSet_OM_VisitTableAdapters.sp06147_OM_Visits_For_Site_ContractTableAdapter sp06147_OM_Visits_For_Site_ContractTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCalculationLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCost;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitSell;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCalculationLevelDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditLatLong;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemDateTime4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditLatLong4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private System.Windows.Forms.BindingSource sp06209OMJobsForVisitBindingSource;
        private DataSet_OM_Job dataSet_OM_Job;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID2;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID1;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobNoLongerRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colRequiresAccessPermit;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessPermitID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceSystemPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnitsDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colScheduleSentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnitsDescriptionID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateClientInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPayContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClient;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colRouteOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLatitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLongitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLatitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLongitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnitsDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnitsDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderID;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colMaximumDaysFromLastVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumDaysFromLastVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysLeeway;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID;
        private DataSet_OM_JobTableAdapters.sp06209_OM_Jobs_For_VisitTableAdapter sp06209_OM_Jobs_For_VisitTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP4;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage5;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency5;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterialUsedID;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterialID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitsUsed;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSellUnitsUsed;
        private DevExpress.XtraGrid.Columns.GridColumn colSellUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnitExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnitVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnitExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnitVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostValueExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colCostValueVat;
        private DevExpress.XtraGrid.Columns.GridColumn colCostValueTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colSellValueExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colSellValueVat;
        private DevExpress.XtraGrid.Columns.GridColumn colSellValueTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks4;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName3;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber2;
        private DevExpress.XtraGrid.Columns.GridColumn colFullDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterialName;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colSellUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colPurchaseOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID2;
        private System.Windows.Forms.BindingSource sp06212OMMaterialsForJobsBindingSource;
        private DataSet_OM_JobTableAdapters.sp06212_OM_Materials_For_JobsTableAdapter sp06212_OM_Materials_For_JobsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colFriendlyVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSellCalculationLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colSellCalculationLevelDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colImagesFolderOM;
    }
}

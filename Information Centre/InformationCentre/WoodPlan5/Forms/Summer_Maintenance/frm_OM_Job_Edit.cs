using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.Utils;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using WoodPlan5.Properties;
using BaseObjects;
using WoodPlan5.Classes.Operations;

namespace WoodPlan5
{
    public partial class frm_OM_Job_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;

        public string strLinkedToRecordDesc1 = "";
        public string strLinkedToRecordDesc2 = "";
        public string strLinkedToRecordDesc3 = "";
        public string strLinkedToRecordDesc4 = "";
        public int intLinkedToRecordID1 = 0;
        public int intLinkedToRecordID2 = 0;
        public int intLinkedToRecordID3 = 0;
        public int intLinkedToRecordID4 = 0;
        public int intLinkedToRecordID5 = 0;
        public int intLinkedToRecordID6 = 0;
        public double dbSiteLatitude = (double)0.00;
        public double dbSiteLongitude = (double)0.00;
        public int _PassedInClientPOID = 0;
        public string _PassedInClientPONumber = "";
        public int _VisitCostCalculationLevelID = 0;
        public string _VisitCostCalculationLevel = "";
        public int _VisitSellCalculationLevelID = 0;
        public string _VisitSellCalculationLevel = "";

        public bool _AtLeastOneSelfBillingInvoice = false;
        public bool _AtLeastOneClientInvoice = false;

        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        bool iBool_AllowAdd = true;
        bool iBool_AllowEdit = true;
        bool iBool_AllowDelete = true;

        public RefreshGridState RefreshGridViewStateLabour;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateEquipment;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateMaterials;  // Used by Grid View State Facilities //
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        private Utils.enmFocusedGrid _enmFocusedGrid = Utils.enmFocusedGrid.Labour;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        string i_str_AddedRecordIDsLabour = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDsEquipment = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDsMaterials = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        private decimal _decDefaultVatRate = (decimal)0.00;

        //private enmFocusedGrids _enmFocusedGrid = enmFocusedGrids.enmSanctionProgress;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        #endregion
        
        public frm_OM_Job_Edit()
        {
            InitializeComponent();
        }

        private void frm_OM_Job_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 500176;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp06208_OM_JOb_Statuses_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06432_OM_Job_Suspension_Reasons_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            try
            {
                sp06208_OM_JOb_Statuses_ListTableAdapter.Fill(dataSet_OM_Job.sp06208_OM_JOb_Statuses_List, 1);
                sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter.Fill(dataSet_OM_Job.sp06177_OM_Job_Duration_Descriptors_With_Blank, 1);
                sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter.Fill(dataSet_OM_Job.sp06268_OM_Cancelled_Reasons_With_Blank, 1);
                sp06432_OM_Job_Suspension_Reasons_With_BlankTableAdapter.Fill(dataSet_OM_Job.sp06432_OM_Job_Suspension_Reasons_With_Blank);
            }
            catch (Exception) { }

            try
            {
                var GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                _decDefaultVatRate = Convert.ToDecimal(GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingDefaultVatRate"));
            }
            catch (Exception)
            {
                _decDefaultVatRate = (decimal)0.00;
            }


            sp06033_OM_Job_Manager_Linked_labourTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06034_OM_Job_Manager_Linked_EquipmentTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06035_OM_Job_Manager_Linked_MaterialsTableAdapter.Connection.ConnectionString = strConnectionString;

            RefreshGridViewStateLabour = new RefreshGridState(gridViewLabour, "LabourUsedID");
            RefreshGridViewStateEquipment = new RefreshGridState(gridViewEquipment, "EquipmentUsedID");
            RefreshGridViewStateMaterials = new RefreshGridState(gridViewMaterials, "MaterialUsedID");

            LoadLastSavedUserScreenSettings();

            // Populate Main Dataset //               
            sp06174_OM_Job_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {                      
                        var drNewRow =(DataSet_OM_Job.sp06174_OM_Job_EditRow)dataSet_OM_Job.sp06174_OM_Job_Edit.NewRow();
                        drNewRow.strMode = strFormMode;
                        drNewRow.strRecordIDs = strRecordIDs;
                        drNewRow.ClientID = intLinkedToRecordID1;
                        drNewRow.ClientContractID = intLinkedToRecordID2;
                        drNewRow.SiteID = intLinkedToRecordID3;
                        drNewRow.SiteContractID = intLinkedToRecordID4;
                        drNewRow.VisitID = intLinkedToRecordID5; 
                        if (intLinkedToRecordID6 > 0) drNewRow.VisitNumber = intLinkedToRecordID6;
                        drNewRow.VisitCostCalculationLevelID = _VisitCostCalculationLevelID;
                        drNewRow.VisitSellCalculationLevel = _VisitSellCalculationLevel;
                        drNewRow.VisitCostCalculationLevelID = _VisitCostCalculationLevelID;
                        drNewRow.VisitSellCalculationLevel = _VisitSellCalculationLevel;

                        drNewRow.ClientName = strLinkedToRecordDesc1;
                        drNewRow.ContractDescription = strLinkedToRecordDesc2;
                        drNewRow.SiteName = strLinkedToRecordDesc3;
                        drNewRow.ClientNameContractDescription = (string.IsNullOrWhiteSpace(strLinkedToRecordDesc2) ? "" : strLinkedToRecordDesc1 + " [" + strLinkedToRecordDesc2 + "]");
                        drNewRow.JobStatusID = 20;  // Job Creation Stated - Ready To Send //
                        drNewRow.JobTypeID = 0;
                        drNewRow.JobSubTypeID = 0;
                        drNewRow.CreatedByStaffID = GlobalSettings.UserID;
                        drNewRow.CreatedByStaffName = GlobalSettings.UserSurname + ": " + GlobalSettings.UserForename;
                        drNewRow.Reactive = 0;
                        drNewRow.JobNoLongerRequired = 0;
                        drNewRow.RequiresAccessPermit = 0;
                        drNewRow.AccessPermitID = 0;
                        drNewRow.ExpectedDurationUnits = (decimal)1.00;
                        drNewRow.ExpectedDurationUnitsDescriptorID = 1;  // Hours //
                        drNewRow.ExpectedStartDate = DateTime.Now.AddHours(1.00);
                        drNewRow.ExpectedEndDate = drNewRow.ExpectedStartDate.AddHours(1.00);
                        drNewRow.ActualDurationUnits = (decimal)0.00;
                        drNewRow.ActualDurationUnitsDescriptionID = 0;  // None //
                        drNewRow.CostTotalMaterialExVAT = (decimal)0.00;
                        drNewRow.CostTotalMaterialVATRate = _decDefaultVatRate;
                        drNewRow.CostTotalMaterialVAT = (decimal)0.00;
                        drNewRow.CostTotalMaterialCost = (decimal)0.00;
                        drNewRow.SellTotalMaterialExVAT = (decimal)0.00;
                        drNewRow.SellTotalMaterialVATRate = _decDefaultVatRate;
                        drNewRow.SellTotalMaterialVAT = (decimal)0.00;
                        drNewRow.SellTotalMaterialCost = (decimal)0.00;
                        drNewRow.CostTotalEquipmentExVAT = (decimal)0.00;
                        drNewRow.CostTotalEquipmentVATRate = _decDefaultVatRate;
                        drNewRow.CostTotalEquipmentVAT = (decimal)0.00;
                        drNewRow.CostTotalEquipmentCost = (decimal)0.00;
                        drNewRow.SellTotalEquipmentExVAT = (decimal)0.00;
                        drNewRow.SellTotalEquipmentVATRate = _decDefaultVatRate;
                        drNewRow.SellTotalEquipmentVAT = (decimal)0.00;
                        drNewRow.SellTotalEquipmentCost = (decimal)0.00;
                        drNewRow.CostTotalLabourExVAT = (decimal)0.00;
                        drNewRow.CostTotalLabourVATRate = _decDefaultVatRate;
                        drNewRow.CostTotalLabourVAT = (decimal)0.00;
                        drNewRow.CostTotalLabourCost = (decimal)0.00;
                        drNewRow.SellTotalLabourExVAT = (decimal)0.00;
                        drNewRow.SellTotalLabourVATRate = _decDefaultVatRate;
                        drNewRow.SellTotalLabourVAT = (decimal)0.00;
                        drNewRow.SellTotalLabourCost = (decimal)0.00;
                        drNewRow.CostTotalCostExVAT = (decimal)0.00;
                        drNewRow.CostTotalCostVAT = (decimal)0.00;
                        drNewRow.CostTotalCost = (decimal)0.00;
                        drNewRow.SellTotalCostExVAT = (decimal)0.00;
                        drNewRow.SellTotalCostVAT = (decimal)0.00;
                        drNewRow.SellTotalCost = (decimal)0.00;
                        drNewRow.ClientInvoiceID = 0;
                        drNewRow.SelfBillingInvoiceID = 0;
                        drNewRow.DoNotPayContractor = 0;
                        drNewRow.DoNotInvoiceClient = 0;
                        drNewRow.StartLatitude = (double)0.00;
                        drNewRow.StartLongitude = (double)0.00;
                        drNewRow.FinishLatitude = (double)0.00;
                        drNewRow.FinishLongitude = (double)0.00;
                        drNewRow.TenderID = 0;
                        drNewRow.TenderJobID = 0;
                        drNewRow.MinimumDaysFromLastVisit = 0;
                        drNewRow.MaximumDaysFromLastVisit = 0;
                        drNewRow.DaysLeeway = 0;
                        drNewRow.BillingCentreCodeID = 0;
                        drNewRow.LocationX = dbSiteLatitude;
                        drNewRow.LocationY = dbSiteLongitude;
                        drNewRow.SitePostcode = strLinkedToRecordDesc4;
                        drNewRow.Rework = 0;
                        drNewRow.ReworkOriginalJobID = 0;
                        drNewRow.CancelledReasonID = 0;
                        drNewRow.NoWorkRequired = 0;
                        drNewRow.ManuallyCompleted = 0;
                        drNewRow.VisitCostCalculationLevelID = _VisitCostCalculationLevelID;
                        drNewRow.VisitCostCalculationLevel = _VisitCostCalculationLevel;

                        drNewRow.VisitSellCalculationLevelID = _VisitSellCalculationLevelID;
                        drNewRow.VisitSellCalculationLevel = _VisitSellCalculationLevel;

                        drNewRow.DisplayOrder = 0;
                        drNewRow.Mandatory = 0;
                        drNewRow.SuspendedReasonID = 0;

                        if (_PassedInClientPOID > 0)
                        {
                            drNewRow["ClientPOID"] = _PassedInClientPOID;
                            drNewRow["ClientPONumber"] = _PassedInClientPONumber = "";
                        }
                        else
                        {
                            // Update default Client PO # //
                            int intRecordTypeID = 2;
                            int intDefaultPOID = 0;
                            string strDefaultPONumber = "";
                            SqlConnection SQlConn = new SqlConnection(strConnectionString);
                            SqlCommand cmd = null;
                            cmd = new SqlCommand("sp06328_OM_Default_Client_PO_Number_For_Record", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@RecordID", intLinkedToRecordID5));
                            cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordTypeID));
                            SqlDataAdapter sdaPO = new SqlDataAdapter(cmd);
                            DataSet dsPO = new DataSet("NewDataSet");
                            sdaPO.Fill(dsPO, "Table");
                            foreach (DataRow dr in dsPO.Tables[0].Rows)
                            {
                                intDefaultPOID = Convert.ToInt32(dr["ClientPurchaseOrderID"]);
                                strDefaultPONumber = dr["PONumber"].ToString();
                                break;
                            }
                            if (intDefaultPOID <= 0)  // No Visit specific Client PO ID so try for Site Contract specific //
                            {
                                intRecordTypeID = 1;
                                cmd = null;
                                cmd = new SqlCommand("sp06328_OM_Default_Client_PO_Number_For_Record", SQlConn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@RecordID", intLinkedToRecordID4));
                                cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordTypeID));
                                sdaPO = new SqlDataAdapter(cmd);
                                dsPO = new DataSet("NewDataSet");
                                sdaPO.Fill(dsPO, "Table");
                                foreach (DataRow dr in dsPO.Tables[0].Rows)
                                {
                                    intDefaultPOID = Convert.ToInt32(dr["ClientPurchaseOrderID"]);
                                    strDefaultPONumber = dr["PONumber"].ToString();
                                    break;
                                }
                            }
                            if (intDefaultPOID <= 0)  // No Site Contract specific Client PO ID so try for Client Contract specific //
                            {
                                intRecordTypeID = 0;
                                cmd = null;
                                cmd = new SqlCommand("sp06328_OM_Default_Client_PO_Number_For_Record", SQlConn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@RecordID", intLinkedToRecordID2));
                                cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordTypeID));
                                sdaPO = new SqlDataAdapter(cmd);
                                dsPO = new DataSet("NewDataSet");
                                sdaPO.Fill(dsPO, "Table");
                                foreach (DataRow dr in dsPO.Tables[0].Rows)
                                {
                                    intDefaultPOID = Convert.ToInt32(dr["ClientPurchaseOrderID"]);
                                    strDefaultPONumber = dr["PONumber"].ToString();
                                    break;
                                }
                            }
                            drNewRow["ClientPOID"] = intDefaultPOID;
                            drNewRow["ClientPONumber"] = strDefaultPONumber;
                        }
                        dataSet_OM_Job.sp06174_OM_Job_Edit.Rows.Add(drNewRow);
                        gridControlLabour.Enabled = false;
                        gridControlEquipment.Enabled = false;
                        gridControlMaterials.Enabled = false;

                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        var drNewRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)dataSet_OM_Job.sp06174_OM_Job_Edit.Newsp06174_OM_Job_EditRow();
                        drNewRow.strMode = "blockedit";
                        drNewRow.strRecordIDs = strRecordIDs;
                        dataSet_OM_Job.sp06174_OM_Job_Edit.Addsp06174_OM_Job_EditRow(drNewRow);
                        drNewRow.AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp06174_OM_Job_EditTableAdapter.Fill(this.dataSet_OM_Job.sp06174_OM_Job_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception ex)
                    {
                    }                 
                    if (strFormMode == "view")
                    {
                        iBool_AllowAdd = false;
                        iBool_AllowEdit = false;
                        iBool_AllowDelete = false;
                    }
                    break;
            }

            if (!(this.strFormMode == "blockadd" || this.strFormMode == "blockedit"))
            {
                Load_Linked_Labour();
                Load_Linked_Equipment();
                Load_Linked_Materials();
            }

            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void Attach_EditValueChanged_To_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_OM_Job.sp06174_OM_Job_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Job", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        public void UpdateFormRefreshStatus(int status, Utils.enmFocusedGrid grid, string NewIDs)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            switch (grid)
            {
                case Utils.enmFocusedGrid.Labour:
                    i_str_AddedRecordIDsLabour = NewIDs;
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    i_str_AddedRecordIDsEquipment = NewIDs;
                    break;
                case Utils.enmFocusedGrid.Materials:
                    i_str_AddedRecordIDsMaterials = NewIDs;
                    break;
            }
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        VisitNumberButtonEdit.Focus();

                        VisitNumberButtonEdit.Properties.ReadOnly = false;
                        VisitNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        JobStatusIDGridLookUpEdit.Focus();

                        VisitNumberButtonEdit.Properties.ReadOnly = true;
                        VisitNumberButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        JobStatusIDGridLookUpEdit.Focus();

                        VisitNumberButtonEdit.Properties.ReadOnly = false;
                        VisitNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        JobStatusIDGridLookUpEdit.Focus();

                        VisitNumberButtonEdit.Properties.ReadOnly = true;
                        VisitNumberButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            Set_Field_Enabled_Status();

            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DefaultBoolean.True;
            }
            ibool_ignoreValidation = true;
            ibool_FormStillLoading = false;
        }

        private void SetEditorButtons()
        {
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_OM_Job.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                btnSave.Enabled = bbiSave.Enabled;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                btnSave.Enabled = bbiSave.Enabled;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControlLabour.MainView;
            intRowHandles = view.GetSelectedRows();

            int intID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp06174OMJobEditBindingSource.Current;
            if (currentRow != null)
            {
                intID = (currentRow["JobID"] == null ? 0 : Convert.ToInt32(currentRow["JobID"]));
            }
            bbiLinkedDocuments.Enabled = intID > 0;  // Set status of button //
            bbiViewOnMap.Enabled = intID > 0 && (strFormMode != "blockedit" || strFormMode != "blockadd");  // Set status of button //

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Labour:
                    {
                        view = (GridView)gridControlLabour.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowAdd && strFormMode != "blockedit" && intID > 0)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;
                        }
                        else
                        {
                            bsiAdd.Enabled = false;
                            bbiSingleAdd.Enabled = false;
                        }
                        if (iBool_AllowEdit && strFormMode != "blockedit" && intRowHandles.Length > 0 && intID > 0)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2) bbiBlockEdit.Enabled = true;
                        }
                        else
                        {
                            bsiEdit.Enabled = false;
                            bbiSingleEdit.Enabled = false;
                            bbiBlockEdit.Enabled = false;
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                        else
                        {
                            bbiDelete.Enabled = false;
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        view = (GridView)gridControlEquipment.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowAdd && strFormMode != "blockedit" && intID > 0)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;
                        }
                        else
                        {
                            bsiAdd.Enabled = false;
                            bbiSingleAdd.Enabled = false;
                        }
                        if (iBool_AllowEdit && strFormMode != "blockedit" && intRowHandles.Length > 0 && intID > 0)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2) bbiBlockEdit.Enabled = true;
                        }
                        else
                        {
                            bsiEdit.Enabled = false;
                            bbiSingleEdit.Enabled = false;
                            bbiBlockEdit.Enabled = false;
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                        else
                        {
                            bbiDelete.Enabled = false;
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        view = (GridView)gridControlMaterials.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowAdd && strFormMode != "blockedit" && intID > 0)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;
                        }
                        else
                        {
                            bsiAdd.Enabled = false;
                            bbiSingleAdd.Enabled = false;
                        }
                        if (iBool_AllowEdit && strFormMode != "blockedit" && intRowHandles.Length > 0 && intID > 0)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2) bbiBlockEdit.Enabled = true;
                        }
                        else
                        {
                            bsiEdit.Enabled = false;
                            bbiSingleEdit.Enabled = false;
                            bbiBlockEdit.Enabled = false;
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                        else
                        {
                            bbiDelete.Enabled = false;
                        }
                    }
                    break;
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            view = (GridView)gridControlLabour.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd && intID > 0 && strFormMode != "blockedit");
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 && strFormMode != "blockedit");
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = (intRowHandles.Length == 1);
            gridControlLabour.Enabled = (iBool_AllowAdd || iBool_AllowEdit) && (strFormMode == "add" || strFormMode == "edit");

            view = (GridView)gridControlEquipment.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlEquipment.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd && intID > 0 && strFormMode != "blockedit");
            gridControlEquipment.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 && strFormMode != "blockedit");
            gridControlEquipment.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlEquipment.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlEquipment.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = (intRowHandles.Length == 1);
            gridControlEquipment.Enabled = (iBool_AllowAdd || iBool_AllowEdit) && (strFormMode == "add" || strFormMode == "edit");
            
            view = (GridView)gridControlMaterials.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlMaterials.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd && intID > 0 && strFormMode != "blockedit");
            gridControlMaterials.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 && strFormMode != "blockedit");
            gridControlMaterials.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlMaterials.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlMaterials.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = (intRowHandles.Length == 1);
            gridControlMaterials.Enabled = (iBool_AllowAdd || iBool_AllowEdit) && (strFormMode == "add" || strFormMode == "edit");
        }


        private void frm_OM_Job_Edit_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDsLabour))
                {
                    Load_Linked_Labour();
                }
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDsEquipment))
                {
                    Load_Linked_Equipment();
                }
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDsMaterials))
                {
                    Load_Linked_Materials();
                }
            }
            SetMenuStatus();
        }

        private void frm_OM_Job_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;

            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
            }

            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

            }
        }

        private void Load_Linked_Labour()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            DataRowView currentRow = (DataRowView)sp06174OMJobEditBindingSource.Current;
            if (currentRow == null) return;
            int intJobID = (string.IsNullOrEmpty(currentRow["JobID"].ToString()) ? 0 : Convert.ToInt32(currentRow["JobID"]));
            if (intJobID <= 0) return;

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControlLabour.MainView;
            view.BeginUpdate();
            RefreshGridViewStateLabour.SaveViewInfo();  // Store expanded groups and selected rows //
            if (string.IsNullOrWhiteSpace(strRecordIDs) || strFormMode == "blockadd" || strFormMode == "blockedit")
            {
                this.dataSet_OM_Job.sp06033_OM_Job_Manager_Linked_labour.Clear();
            }
            else
            {
                sp06033_OM_Job_Manager_Linked_labourTableAdapter.Fill(dataSet_OM_Job.sp06033_OM_Job_Manager_Linked_labour, strRecordIDs);
                RefreshGridViewStateLabour.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            view.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsLabour != "")
            {
                strArray = i_str_AddedRecordIDsLabour.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LabourUsedID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsLabour = "";
            }
        }

        private void Load_Linked_Equipment()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            DataRowView currentRow = (DataRowView)sp06174OMJobEditBindingSource.Current;
            if (currentRow == null) return;
            int intJobID = (string.IsNullOrEmpty(currentRow["JobID"].ToString()) ? 0 : Convert.ToInt32(currentRow["JobID"]));
            if (intJobID <= 0) return;

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControlEquipment.MainView;
            view.BeginUpdate();
            RefreshGridViewStateEquipment.SaveViewInfo();  // Store expanded groups and selected rows //
            if (string.IsNullOrWhiteSpace(strRecordIDs) || strFormMode == "blockadd" || strFormMode == "blockedit")
            {
                this.dataSet_OM_Job.sp06034_OM_Job_Manager_Linked_Equipment.Clear();
            }
            else
            {
                sp06034_OM_Job_Manager_Linked_EquipmentTableAdapter.Fill(dataSet_OM_Job.sp06034_OM_Job_Manager_Linked_Equipment, strRecordIDs);
                RefreshGridViewStateEquipment.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            view.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsEquipment != "")
            {
                strArray = i_str_AddedRecordIDsEquipment.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["EquipmentUsedID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsEquipment = "";
            }
        }

        private void Load_Linked_Materials()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            DataRowView currentRow = (DataRowView)sp06174OMJobEditBindingSource.Current;
            if (currentRow == null) return;
            int intJobID = (string.IsNullOrEmpty(currentRow["JobID"].ToString()) ? 0 : Convert.ToInt32(currentRow["JobID"]));
            if (intJobID <= 0) return;

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControlMaterials.MainView;
            view.BeginUpdate();
            RefreshGridViewStateMaterials.SaveViewInfo();  // Store expanded groups and selected rows //
            if (string.IsNullOrWhiteSpace(strRecordIDs) || strFormMode == "blockadd" || strFormMode == "blockedit")
            {
                this.dataSet_OM_Job.sp06035_OM_Job_Manager_Linked_Materials.Clear();
            }
            else
            {
                sp06035_OM_Job_Manager_Linked_MaterialsTableAdapter.Fill(dataSet_OM_Job.sp06035_OM_Job_Manager_Linked_Materials, strRecordIDs);
                RefreshGridViewStateMaterials.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            view.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsMaterials != "")
            {
                strArray = i_str_AddedRecordIDsMaterials.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["MaterialUsedID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsMaterials = "";
            }
        }

        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp06174OMJobEditBindingSource.EndEdit();
            try
            {
                this.sp06174_OM_Job_EditTableAdapter.Update(dataSet_OM_Job);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            if (strFormMode == "add" || strFormMode == "edit")
            {
                gridControlLabour.Enabled = true;  // Child Grid now enabled //
                gridControlEquipment.Enabled = true;  // Child Grid now enabled //
                gridControlMaterials.Enabled = true;  // Child Grid now enabled //
                FilterGrids();
            }

            GridView view = (GridView)gridControlLabour.MainView;
            view.PostEditor();

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp06174OMJobEditBindingSource.Current;
                if (currentRow != null)
                {
                    strNewIDs = Convert.ToInt32(currentRow["JobID"]) + ";";
                    strRecordIDs = Convert.ToInt32(currentRow["JobID"]) + ",";  // This is required so the linked child records can be loaded into the grid //
                }
                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
                if (currentRow != null)
                {
                    currentRow["strMode"] = "blockedit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Values returned from Update SP //
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_OM_Job_Manager")
                    {
                        var fParentForm = (frm_OM_Job_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Jobs, strNewIDs);
                    }
                    if (frmChild.Name == "frm_OM_Visit_Manager")
                    {
                        var fParentForm = (frm_OM_Visit_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(2, Utils.enmFocusedGrid.Jobs, strNewIDs);
                    }
                    if (frmChild.Name == "frm_OM_Tender_Manager")
                    {
                        var fParentForm = (frm_OM_Tender_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(2, Utils.enmFocusedGrid.Visit, strNewIDs);  // Refresh Visit Grid [has job totals] (no job grid on screen) //
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_OM_Job.sp06174_OM_Job_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_OM_Job.sp06174_OM_Job_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0) //|| intAccessIssuesNew > 0 || intAccessIssuesModified > 0 || intAccessIssuesDeleted > 0 || intEnvironmentalIssuesNew > 0 || intEnvironmentalIssuesModified > 0 || intEnvironmentalIssuesDeleted > 0 || intSiteHazardsNew > 0 || intSiteHazardsModified > 0 || intSiteHazardsDeleted > 0 || intElecticalHazardsNew > 0 || intElecticalHazardsModified > 0 || intElecticalHazardsDeleted > 0
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (!(this.strFormMode == "blockadd" || this.strFormMode == "blockedit"))
                {
                    FilterGrids();
                    GridView view = (GridView)gridControlLabour.MainView;
                    view.ExpandAllGroups();
                    Set_Field_Enabled_Status();
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridViewLabour":
                    message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Labour NOT Shown When Block Adding and Block Editing" : "No Linked Labour Available - Click the Add button to Create Linked Labour");
                    break;
                case "gridViewEquipment":
                    message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Equipment NOT Shown When Block Adding and Block Editing" : "No Linked Equipment Available - Click the Add button to Create Linked Equipment");
                    break;
                case "gridViewMaterials":
                    message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Materials NOT Shown When Block Adding and Block Editing" : "No Linked Materials Available - Click the Add button to Create Linked Materials");
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        #region GridControl Labour

        private void gridControlLabour_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControlLabour.Focus();
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Linked_Labour();
                        FilterGrids();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlLabour.MainView;
                        int intRecordType = 22;  // Labour //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "LabourUsedID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "FullDescription").ToString() + ", Contractor: " + view.GetRowCellValue(view.FocusedRowHandle, "ContractorName").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                   break;
                default:
                    break;
            }
        }

        private void gridViewLabour_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewLabour_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Labour;

            SetMenuStatus();
        }

        private void gridViewLabour_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Labour;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridControl Equipment

        private void gridControlEquipment_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControlLabour.Focus();
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Linked_Equipment();
                        FilterGrids();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlEquipment.MainView;
                        int intRecordType = 23;  // Equipment //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "EquipmentUsedID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "FullDescription").ToString() + ", Equipment: " + view.GetRowCellValue(view.FocusedRowHandle, "EquipmentType").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewEquipment_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewEquipment_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Equipment;

            SetMenuStatus();
        }

        private void gridViewEquipment_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Equipment;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridControl Materials

        private void gridControlMaterials_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControlLabour.Focus();
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Linked_Equipment();
                        FilterGrids();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlMaterials.MainView;
                        int intRecordType = 24;  // Materials //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "MaterialUsedID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "FullDescription").ToString() + ", Material: " + view.GetRowCellValue(view.FocusedRowHandle, "MaterialName").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewMaterials_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewMaterials_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Materials;

            SetMenuStatus();
        }

        private void gridViewMaterials_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Materials;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region Editors

        private void VisitNumberButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                int intClientID = (string.IsNullOrEmpty(currentRow.ClientID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientID));
                int intClientContractID = (string.IsNullOrEmpty(currentRow.ClientContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientContractID));
                int intSiteID = (string.IsNullOrEmpty(currentRow.SiteID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteID));
                int intSiteContractID = (string.IsNullOrEmpty(currentRow.SiteContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteContractID));
                int intVisitID = (string.IsNullOrEmpty(currentRow.VisitID.ToString()) ? 0 : Convert.ToInt32(currentRow.VisitID));
                var fChildForm = new frm_OM_Select_Visit();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalClientContractID = intClientContractID;
                fChildForm.intOriginalSiteContractID = intSiteContractID;
                fChildForm.intOriginalVisitID = intVisitID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.ClientID = fChildForm.intSelectedClientID;
                    currentRow.ClientName = fChildForm.strSelectedClientName;
                    currentRow.ClientContractID = fChildForm.intSelectedClientContractID;
                    currentRow.ContractDescription = fChildForm.strSelectedContractDescription;
                    currentRow.SiteID = fChildForm.intSelectedSiteID;
                    currentRow.SiteName = fChildForm.strSelectedSiteName;
                    currentRow.SiteContractID = fChildForm.intSelectedSiteContractID;
                    currentRow.VisitID = fChildForm.intSelectedVisitID;
                    currentRow.VisitNumber = fChildForm.intSelectedVisitNumber;
                    currentRow.VisitCostCalculationLevelID = fChildForm.intCostCalculationLevelID;
                    currentRow.VisitCostCalculationLevel = fChildForm.strCostCalculationLevel;
                    currentRow.VisitSellCalculationLevelID = fChildForm.intSellCalculationLevelID;
                    currentRow.VisitSellCalculationLevel = fChildForm.strSellCalculationLevel;

                    if (fChildForm.intSelectedClientPOID > 0)
                    {
                        currentRow.ClientPOID = fChildForm.intSelectedClientPOID;
                        currentRow.ClientPONumber = fChildForm.strSelectedClientPoNumber;
                    }
                    else
                    {
                        // Update default Client PO # //
                        int intRecordTypeID = 2;
                        int intDefaultPOID = 0;
                        string strDefaultPONumber = "";
                        try
                        {
                            SqlConnection SQlConn = new SqlConnection(strConnectionString);
                            SqlCommand cmd = null;
                            cmd = new SqlCommand("sp06328_OM_Default_Client_PO_Number_For_Record", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@RecordID", intVisitID));
                            cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordTypeID));
                            SqlDataAdapter sdaPO = new SqlDataAdapter(cmd);
                            DataSet dsPO = new DataSet("NewDataSet");
                            sdaPO.Fill(dsPO, "Table");
                            foreach (DataRow dr in dsPO.Tables[0].Rows)
                            {
                                intDefaultPOID = Convert.ToInt32(dr["ClientPurchaseOrderID"]);
                                strDefaultPONumber = dr["PONumber"].ToString();
                                break;
                            }
                            if (intDefaultPOID <= 0)  // No Visit specific Client PO ID so try for Site Contract specific //
                            {
                                intRecordTypeID = 1;
                                cmd = null;
                                cmd = new SqlCommand("sp06328_OM_Default_Client_PO_Number_For_Record", SQlConn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@RecordID", intSiteContractID));
                                cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordTypeID));
                                sdaPO = new SqlDataAdapter(cmd);
                                dsPO = new DataSet("NewDataSet");
                                sdaPO.Fill(dsPO, "Table");
                                foreach (DataRow dr in dsPO.Tables[0].Rows)
                                {
                                    intDefaultPOID = Convert.ToInt32(dr["ClientPurchaseOrderID"]);
                                    strDefaultPONumber = dr["PONumber"].ToString();
                                    break;
                                }
                            }
                            if (intDefaultPOID <= 0)  // No Site Contract specific Client PO ID so try for Client Contract specific //
                            {
                                intRecordTypeID = 0;
                                cmd = null;
                                cmd = new SqlCommand("sp06328_OM_Default_Client_PO_Number_For_Record", SQlConn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@RecordID", intClientContractID));
                                cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordTypeID));
                                sdaPO = new SqlDataAdapter(cmd);
                                dsPO = new DataSet("NewDataSet");
                                sdaPO.Fill(dsPO, "Table");
                                foreach (DataRow dr in dsPO.Tables[0].Rows)
                                {
                                    intDefaultPOID = Convert.ToInt32(dr["ClientPurchaseOrderID"]);
                                    strDefaultPONumber = dr["PONumber"].ToString();
                                    break;
                                }
                            }
                        }
                        catch (Exception) { }
                        currentRow.ClientPOID = intDefaultPOID;
                        currentRow.ClientPONumber = strDefaultPONumber;
                    }
                    sp06174OMJobEditBindingSource.EndEdit();

                    if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
                    Set_Team_Cost(false);
                    Set_Client_Sell(false);
                }
            }
        }
        private void VisitNumberButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if ((this.strFormMode == "edit" || this.strFormMode == "add") && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(VisitNumberButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(VisitNumberButtonEdit, "");
            }
        }

        private void JobSubTypeDescriptionButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
                var fChildForm = new frm_OM_Select_Job_Sub_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm._Mode = "single";
                fChildForm.intOperationManagerJobsOnly = 1;

                if (currentRow == null) return;
                if (strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    fChildForm.intOriginalParentID = 0;
                    fChildForm.intOriginalChildID = 0;
                }
                else
                {
                    fChildForm.intOriginalParentID = (string.IsNullOrWhiteSpace(currentRow.JobTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.JobTypeID));
                    fChildForm.intOriginalChildID = (string.IsNullOrWhiteSpace(currentRow.JobSubTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.JobSubTypeID));
                }

                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.JobSubTypeID = fChildForm.intSelectedChildID;
                    currentRow.JobTypeID = fChildForm.intSelectedParentID;
                    currentRow.JobSubTypeDescription = fChildForm.strSelectedChildDescriptions;
                    currentRow.JobTypeDescription = fChildForm.strSelectedParentDescriptions;
                    currentRow.JobTypeJobSubTypeDescription = fChildForm.strSelectedParentDescriptions + " - " + fChildForm.strSelectedChildDescriptions;
                    sp06174OMJobEditBindingSource.EndEdit();

                    if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
                    Set_Team_Cost(false);
                    Set_Client_Sell(false);
                }
            }
        }
        private void JobSubTypeDescriptionButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if ((this.strFormMode == "edit" || this.strFormMode == "add") && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(JobSubTypeDescriptionButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(JobSubTypeDescriptionButtonEdit, "");
            }
        }

        private void JobStatusIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if ((this.strFormMode == "add" || this.strFormMode == "edit") && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(JobStatusIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(JobStatusIDGridLookUpEdit, "");
            }
        }

        private void ExpectedStartDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ExpectedStartDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit dt = (DateEdit)sender;
            if (this.strFormMode == "blockedit" || this.strFormMode == "view") return;
            if (string.IsNullOrEmpty(dt.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(ExpectedStartDateDateEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ExpectedStartDateDateEdit, "");
            }
        }
        private void ExpectedStartDateDateEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;

            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            DateEdit de = (DateEdit)sender;
            try { dtStart = de.DateTime; }
            catch (Exception) { }

            try { dtEnd = currentRow.ExpectedEndDate; }
            catch (Exception) { }

            try { decUnits = currentRow.ExpectedDurationUnits; }
            catch (Exception) { }

            try { intUnitDescriptorID = currentRow.ExpectedDurationUnitsDescriptorID; }
            catch (Exception) { }
            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, "ExpectedStartDate");

            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            Set_Team_Cost(false);
            Set_Client_Sell(false);
        }

        private void ExpectedDurationUnitsSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ExpectedDurationUnitsSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;

            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            try { dtStart = currentRow.ExpectedStartDate; }
            catch (Exception) { }

            try { dtEnd = currentRow.ExpectedEndDate; }
            catch (Exception) { }

            SpinEdit se = (SpinEdit)sender;
            try { decUnits = Convert.ToDecimal(se.Value); }
            catch (Exception) { }

            try { intUnitDescriptorID = currentRow.ExpectedDurationUnitsDescriptorID; }
            catch (Exception) { }

            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, "ExpectedDurationUnits");
        }

        private void ExpectedDurationUnitsDescriptorIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ExpectedDurationUnitsDescriptorIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;

            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            try { dtStart = currentRow.ExpectedStartDate; }
            catch (Exception) { }

            try { dtEnd = currentRow.ExpectedEndDate; }
            catch (Exception) { }

            try { decUnits = currentRow.ExpectedDurationUnits; }
            catch (Exception) { }

            GridLookUpEdit glue = (GridLookUpEdit)sender;
            try { intUnitDescriptorID = Convert.ToInt32(glue.EditValue); }
            catch (Exception) { }

            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, "ExpectedDurationUnitsDescriptorID");
        }

        private void ExpectedEndDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ExpectedEndDateDateEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;

            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            DateEdit de = (DateEdit)sender;
            try { dtEnd = de.DateTime; }
            catch (Exception) { }

            try { dtStart = currentRow.ExpectedStartDate; }
            catch (Exception) { }

            try { decUnits = currentRow.ExpectedDurationUnits; }
            catch (Exception) { }

            try { intUnitDescriptorID = currentRow.ExpectedDurationUnitsDescriptorID; }
            catch (Exception) { }

            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, "ExpectedEndDate");
        }

        private void ActualStartDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ActualStartDateDateEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;

            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            DateEdit de = (DateEdit)sender;
            try { dtStart = de.DateTime; }
            catch (Exception) { }

            try { dtEnd = currentRow.ActualEndDate; }
            catch (Exception) { }

            try { decUnits = currentRow.ActualDurationUnits; }
            catch (Exception) { }

            try { intUnitDescriptorID = currentRow.ActualDurationUnitsDescriptionID; }
            catch (Exception) { }

            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, "ActualStartDate");
            
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            Set_Team_Cost(false);
            Set_Client_Sell(false);
        }

        private void ActualDurationUnitsSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ActualDurationUnitsSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;

            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            try { dtStart = currentRow.ActualStartDate; }
            catch (Exception) { }

            try { dtEnd = currentRow.ActualEndDate; }
            catch (Exception) { }

            SpinEdit se = (SpinEdit)sender;
            try { decUnits = Convert.ToDecimal(se.Value); }
            catch (Exception) { }

            try { intUnitDescriptorID = currentRow.ActualDurationUnitsDescriptionID; }
            catch (Exception) { }

            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, "ActualDurationUnits");
        }

        private void ActualDurationUnitsDescriptionIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ActualDurationUnitsDescriptionIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;

            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            try { dtStart = currentRow.ActualStartDate; }
            catch (Exception) { }

            try { dtEnd = currentRow.ActualEndDate; }
            catch (Exception) { }

            try { decUnits = currentRow.ActualDurationUnits; }
            catch (Exception) { }

            GridLookUpEdit glue = (GridLookUpEdit)sender;
            try { intUnitDescriptorID = Convert.ToInt32(glue.EditValue); }
            catch (Exception) { }

            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, "ActualDurationUnitsDescriptionID");
        }

        private void ActualEndDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ActualEndDateDateEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;

            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            DateEdit de = (DateEdit)sender;
            try { dtEnd = de.DateTime; }
            catch (Exception) { }

            try { dtStart = currentRow.ActualStartDate; }
            catch (Exception) { }

            try { decUnits = currentRow.ActualDurationUnits; }
            catch (Exception) { }

            try { intUnitDescriptorID = currentRow.ActualDurationUnitsDescriptionID; }
            catch (Exception) { }

            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, "ActualEndDate");
        }

        private void CalculateDates(DateTime? StartDate, decimal Units, int UnitDescriptorID, DateTime? EndDate, string ChangedColumnName)
        {
            if (strFormMode == "view") return;

            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;

            switch (ChangedColumnName)
            {
                case "ExpectedStartDate":
                case "ActualStartDate":
                    {
                        if (StartDate <= DateTime.MinValue) return;
                        if (Units > (decimal)0.00 && UnitDescriptorID > 0)  // Calculate End Date //
                        {
                            switch (UnitDescriptorID)
                            {
                                case 1:  // Hours //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddHours((double)Units);
                                    }
                                    break;
                                case 2:  // Days //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddDays((double)Units);
                                    }
                                    break;
                                case 3:  // Weeks //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddDays((double)Units * (double)7);
                                    }
                                    break;
                                case 4:  // Months //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddMonths((int)Units);
                                    }
                                    break;
                                case 5:  // Years //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddYears((int)Units);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            if (ChangedColumnName == "ExpectedStartDate")
                            {
                                currentRow.ExpectedEndDate = (EndDate == null ? DateTime.MinValue : (DateTime)EndDate);
                            }
                            else  // colActualStartDate //
                            {
                                currentRow.ActualEndDate = (EndDate == null ? DateTime.MinValue : (DateTime)EndDate);
                            }
                        }
                        else if (EndDate != null)  // Calculate Duration //
                        {
                            // Calculate the interval between the two dates.
                            DateDiff dateDiff = DateDiff.Difference((DateTime)StartDate, (DateTime)EndDate);

                            if (dateDiff.Years > 0 && (dateDiff.Months == 0 && dateDiff.Weeks == 0 && dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                Units = (decimal)dateDiff.Years;
                                UnitDescriptorID = 5;
                            }
                            else if (dateDiff.Months > 0 && (dateDiff.Weeks == 0 && dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                Units = (decimal)dateDiff.Months + ((decimal)dateDiff.Years * (decimal)12);
                                UnitDescriptorID = 4;
                            }
                            else if (dateDiff.Weeks > 0 && (dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalDays / (decimal)7;
                                UnitDescriptorID = 3;
                            }
                            else if (dateDiff.Days > 0 && (dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalDays;
                                UnitDescriptorID = 2;
                                if (Units % 7 == 0) // Check if remainder is 0 if yes then number is devisable for weeks so convert to weeks //
                                {
                                    Units = (decimal)ts.TotalDays / (decimal)7;
                                    UnitDescriptorID = 3;
                                }
                            }
                            else if (dateDiff.Hours > 0 && (dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalHours;
                                UnitDescriptorID = 1;
                            }
                            else if (dateDiff.Minutes > 0)
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalMinutes / (decimal)60;
                                UnitDescriptorID = 1;
                            }
                            if (ChangedColumnName == "ExpectedStartDate")
                            {
                                currentRow.ExpectedDurationUnits = (Units <= (decimal)0.00 ? (decimal)0.00 : Units);
                                currentRow.ExpectedDurationUnitsDescriptorID = UnitDescriptorID;
                            }
                            else  // colActualStartDate //
                            {
                                currentRow.ActualDurationUnits = (Units <= (decimal)0.00 ? (decimal)0.00 : Units);
                                currentRow.ActualDurationUnitsDescriptionID = UnitDescriptorID;
                            }
                        }
                        sp06174OMJobEditBindingSource.EndEdit();
                    }
                    break;
                case "ExpectedEndDate":
                case "ActualEndDate":
                    {
                        if (EndDate <= DateTime.MinValue) return;
                        if (Units > (decimal)0.00 && UnitDescriptorID > 0)  // Calculate End Date //
                        {
                            switch (UnitDescriptorID)
                            {
                                case 1:  // Hours //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddHours((double)Units * (double)-1);
                                    }
                                    break;
                                case 2:  // Days //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddDays((double)Units * (double)-1);
                                    }
                                    break;
                                case 3:  // Weeks //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddDays(((double)Units * (double)7) * (double)-1);
                                    }
                                    break;
                                case 4:  // Months //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddMonths((int)Units * -1);
                                    }
                                    break;
                                case 5:  // Years //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddYears((int)Units * -1);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            if (ChangedColumnName == "ExpectedEndDate")
                            {
                                currentRow.ExpectedStartDate = (StartDate == null ? DateTime.MinValue : (DateTime)StartDate);
                            }
                            else  // colActualEndDate //
                            {
                                currentRow.ActualStartDate = (StartDate == null ? DateTime.MinValue : (DateTime)StartDate);
                            }
                        }
                        else if (StartDate != null)  // Calculate Duration //
                        {
                            // Calculate the interval between the two dates.
                            DateDiff dateDiff = DateDiff.Difference((DateTime)StartDate, (DateTime)EndDate);

                            if (dateDiff.Years > 0 && (dateDiff.Months == 0 && dateDiff.Weeks == 0 && dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                Units = (decimal)dateDiff.Years;
                                UnitDescriptorID = 5;
                            }
                            else if (dateDiff.Months > 0 && (dateDiff.Weeks == 0 && dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                Units = (decimal)dateDiff.Months + ((decimal)dateDiff.Years * (decimal)12);
                                UnitDescriptorID = 4;
                            }
                            else if (dateDiff.Weeks > 0 && (dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalDays / (decimal)7;
                                UnitDescriptorID = 3;
                            }
                            else if (dateDiff.Days > 0 && (dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalDays;
                                UnitDescriptorID = 2;
                                if (Units % 7 == 0) // Check if remainder is 0 if yes then number is devisable for weeks so convert to weeks //
                                {
                                    Units = (decimal)ts.TotalDays / (decimal)7;
                                    UnitDescriptorID = 3;
                                }
                            }
                            else if (dateDiff.Hours > 0 && (dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalHours;
                                UnitDescriptorID = 1;
                            }
                            else if (dateDiff.Minutes > 0)
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalMinutes / (decimal)60;
                                UnitDescriptorID = 1;
                            }
                            if (ChangedColumnName == "ExpectedEndDate")
                            {
                                currentRow.ExpectedDurationUnits = (Units <= (decimal)0.00 ? (decimal)0.00 : Units);
                                currentRow.ExpectedDurationUnitsDescriptorID = UnitDescriptorID;
                            }
                            else  // colActualEndDate //
                            {
                                currentRow.ActualDurationUnits = (Units <= (decimal)0.00 ? (decimal)0.00 : Units);
                                currentRow.ActualDurationUnitsDescriptionID = UnitDescriptorID;
                            }
                        }
                        sp06174OMJobEditBindingSource.EndEdit();
                    }
                    break;
                case "ExpectedDurationUnits":
                case "ExpectedDurationUnitsDescriptorID":
                case "ActualDurationUnits":
                case "ActualDurationUnitsDescriptionID":
                    {
                        if (Units > (decimal)0.00 && UnitDescriptorID > 0 && StartDate != null)  // Calculate End Date //
                        {
                            switch (UnitDescriptorID)
                            {
                                case 1:  // Hours //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddHours((double)Units);
                                    }
                                    break;
                                case 2:  // Days //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddDays((double)Units);
                                    }
                                    break;
                                case 3:  // Weeks //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddDays((double)Units * (double)7);
                                    }
                                    break;
                                case 4:  // Months //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddMonths((int)Units);
                                    }
                                    break;
                                case 5:  // Years //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddYears((int)Units);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            if (ChangedColumnName == "ExpectedDurationUnits" || ChangedColumnName == "ExpectedDurationUnitsDescriptorID")
                            {
                                currentRow.ExpectedEndDate = (EndDate == null ? DateTime.MinValue : (DateTime)EndDate);
                            }
                            else  // colActualDurationUnits or colActualDurationUnitsDescriptionID //
                            {
                                currentRow.ActualEndDate = (EndDate == null ? DateTime.MinValue : (DateTime)EndDate);
                            }
                        }
                        else if (Units > (decimal)0.00 && UnitDescriptorID > 0 && EndDate != null)  // Calculate Start Date //
                        {
                            switch (UnitDescriptorID)
                            {
                                case 1:  // Hours //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddHours((double)Units * (double)-1);
                                    }
                                    break;
                                case 2:  // Days //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddDays((double)Units * (double)-1);
                                    }
                                    break;
                                case 3:  // Weeks //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddDays(((double)Units * (double)7) * (double)-1);
                                    }
                                    break;
                                case 4:  // Months //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddMonths((int)Units * -1);
                                    }
                                    break;
                                case 5:  // Years //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddYears((int)Units * -1);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            if (ChangedColumnName == "ExpectedDurationUnits" || ChangedColumnName == "ExpectedDurationUnitsDescriptorID")
                            {
                                currentRow.ExpectedStartDate = (StartDate == null ? DateTime.MinValue : (DateTime)StartDate);
                            }
                            else  // colActualDurationUnits or colActualDurationUnitsDescriptionID //
                            {
                                currentRow.ActualStartDate = (StartDate == null ? DateTime.MinValue : (DateTime)StartDate);
                            }
                        }
                        sp06174OMJobEditBindingSource.EndEdit();
                    }
                    break;
                default:
                    break;
            }
        }

        private void ClientPONumberButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                if (strFormMode == "blockedit" || strFormMode == "blockadd")
                {
                    var fChildForm = new frm_OM_Select_Client_PO();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strPassedInParentIDs = "";
                    fChildForm.intPassedInChildID = 0;
                    fChildForm.intFilterSummerMaintenanceClient = 1;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        currentRow.ClientPOID = fChildForm.intSelectedChildID;
                        currentRow.ClientPONumber = fChildForm.strSelectedChildDescriptions;
                        sp06174OMJobEditBindingSource.EndEdit();
                    }
                }
                else
                {

                    int intClientContractID = 0;
                    try { intClientContractID = (string.IsNullOrEmpty(currentRow.ClientContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientContractID)); }
                    catch (Exception) { }

                    int intSiteContractID = 0;
                    try { intSiteContractID = (string.IsNullOrEmpty(currentRow.SiteContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteContractID)); }
                    catch (Exception) { }

                    int intClientPOID = 0;
                    try { intClientPOID = (string.IsNullOrEmpty(currentRow.ClientPOID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientPOID)); }
                    catch (Exception) { }

                    int intVisitID = 0;
                    try { intVisitID = (string.IsNullOrEmpty(currentRow.VisitID.ToString()) ? 0 : Convert.ToInt32(currentRow.VisitID)); }
                    catch (Exception) { }

                    var fChildForm = new frm_OM_Select_Client_PO_Multi_Page();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.boolPage1Enabled = true;
                    fChildForm.boolPage2Enabled = true;
                    fChildForm.boolPage3Enabled = true;
                    fChildForm.intActivePage = 3;
                    fChildForm._PassedInClientContractIDs = intClientContractID.ToString() + ",";
                    fChildForm._PassedInSiteContractIDs = intSiteContractID.ToString() + ",";
                    fChildForm._PassedInVisitIDs = intVisitID.ToString() + ",";
                    fChildForm.intOriginalPOID = intClientPOID;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        currentRow.ClientPOID = fChildForm.intSelectedPOID;
                        currentRow.ClientPONumber = fChildForm.strSelectedPONumbers;
                        sp06174OMJobEditBindingSource.EndEdit();
                    }
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                currentRow.ClientPOID = 0;
                currentRow.ClientPONumber = "";
                sp06174OMJobEditBindingSource.EndEdit();
            }
        }


        #region Financial Fields

        private void CostTotalLabourExVATSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void CostTotalLabourExVATSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = (string.IsNullOrWhiteSpace(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value));
            Calculate_Labour_Cost("CostTotalLabourExVAT", decValue);
            Calculate_Total_Cost();
        }

        private void CostTotalLabourVATRateSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void CostTotalLabourVATRateSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = (string.IsNullOrWhiteSpace(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value));
            Calculate_Labour_Cost("CostTotalLabourVATRate", decValue);
            Calculate_Total_Cost();
        }

        private void Calculate_Labour_Cost(string strCurrentColumn, decimal decValue)
        {
            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
            decimal decCostTotalExVAT = (decimal)0.00;
            decimal decCostTotalVATRate = (decimal)0.00;
            decimal decCostTotalVAT = (decimal)0.00;
            decimal decCostTotalCost = (decimal)0.00;
            decCostTotalExVAT = (strCurrentColumn == "CostTotalLabourExVAT" ? decValue : (string.IsNullOrWhiteSpace(currentRow["CostTotalLabourExVAT"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.CostTotalLabourExVAT)));
            decCostTotalVATRate = (strCurrentColumn == "CostTotalLabourVATRate" ? decValue : (string.IsNullOrWhiteSpace(currentRow["CostTotalLabourVATRate"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.CostTotalLabourVATRate)));
            switch (strCurrentColumn)
            {
                case "CostTotalLabourExVAT":
                    {
                        if (decCostTotalVATRate != (decimal)0.00)
                        {
                            decCostTotalVAT = (decCostTotalExVAT * (decCostTotalVATRate / 100));
                            decCostTotalCost = (decCostTotalExVAT + decCostTotalVAT);
                        }
                        else
                        {
                            decCostTotalVAT = (decimal)0.00;
                            decCostTotalCost = (decCostTotalExVAT + decCostTotalVAT);
                        }
                        currentRow.CostTotalLabourVAT = decCostTotalVAT;
                        currentRow.CostTotalLabourCost = decCostTotalCost;
                        sp06174OMJobEditBindingSource.EndEdit();
                    }
                    break;
                case "CostTotalLabourVATRate":
                    {
                        if (decCostTotalExVAT != (decimal)0.00)
                        {
                            decCostTotalVAT = (decCostTotalExVAT * (decCostTotalVATRate / 100));
                            decCostTotalCost = (decCostTotalExVAT + decCostTotalVAT);
                        }
                        else
                        {
                            decCostTotalVAT = (decimal)0.00;
                            decCostTotalCost = (decimal)0.00;
                        }
                        currentRow.CostTotalLabourVAT = decCostTotalVAT;
                        currentRow.CostTotalLabourCost = decCostTotalCost;
                        sp06174OMJobEditBindingSource.EndEdit();
                    }
                    break;
                default:
                    break;
            }
        }


        private void CostTotalEquipmentExVATSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void CostTotalEquipmentExVATSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = (string.IsNullOrWhiteSpace(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value));
            Calculate_Equipment_Cost("CostTotalEquipmentExVAT", decValue);
            Calculate_Total_Cost();
        }

        private void CostTotalEquipmentVATRateSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void CostTotalEquipmentVATRateSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = (string.IsNullOrWhiteSpace(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value));
            Calculate_Equipment_Cost("CostTotalEquipmentVATRate", decValue);
            Calculate_Total_Cost();
        }

        private void Calculate_Equipment_Cost(string strCurrentColumn, decimal decValue)
        {
            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
            decimal decCostTotalExVAT = (decimal)0.00;
            decimal decCostTotalVATRate = (decimal)0.00;
            decimal decCostTotalVAT = (decimal)0.00;
            decimal decCostTotalCost = (decimal)0.00;
            decCostTotalExVAT = (strCurrentColumn == "CostTotalEquipmentExVAT" ? decValue : (string.IsNullOrWhiteSpace(currentRow["CostTotalEquipmentExVAT"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.CostTotalEquipmentExVAT)));
            decCostTotalVATRate = (strCurrentColumn == "CostTotalEquipmentVATRate" ? decValue : (string.IsNullOrWhiteSpace(currentRow["CostTotalEquipmentVATRate"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.CostTotalEquipmentVATRate)));
            switch (strCurrentColumn)
            {
                case "CostTotalEquipmentExVAT":
                    {
                        if (decCostTotalVATRate != (decimal)0.00)
                        {
                            decCostTotalVAT = (decCostTotalExVAT * (decCostTotalVATRate / 100));
                            decCostTotalCost = (decCostTotalExVAT + decCostTotalVAT);
                        }
                        else
                        {
                            decCostTotalVAT = (decimal)0.00;
                            decCostTotalCost = (decCostTotalExVAT + decCostTotalVAT);
                        }
                        currentRow.CostTotalEquipmentVAT = decCostTotalVAT;
                        currentRow.CostTotalEquipmentCost = decCostTotalCost;
                        sp06174OMJobEditBindingSource.EndEdit();
                    }
                    break;
                case "CostTotalEquipmentVATRate":
                    {
                        if (decCostTotalExVAT != (decimal)0.00)
                        {
                            decCostTotalVAT = (decCostTotalExVAT * (decCostTotalVATRate / 100));
                            decCostTotalCost = (decCostTotalExVAT + decCostTotalVAT);
                        }
                        else
                        {
                            decCostTotalVAT = (decimal)0.00;
                            decCostTotalCost = (decimal)0.00;
                        }
                        currentRow.CostTotalEquipmentVAT = decCostTotalVAT;
                        currentRow.CostTotalEquipmentCost = decCostTotalCost;
                        sp06174OMJobEditBindingSource.EndEdit();
                    }
                    break;
                default:
                    break;
            }
        }


        private void CostTotalMaterialExVATSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void CostTotalMaterialExVATSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = (string.IsNullOrWhiteSpace(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value));
            Calculate_Material_Cost("CostTotalMaterialExVAT", decValue);
            Calculate_Total_Cost();
        }

        private void CostTotalMaterialVATRateSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void CostTotalMaterialVATRateSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = (string.IsNullOrWhiteSpace(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value));
            Calculate_Material_Cost("CostTotalMaterialVATRate", decValue);
            Calculate_Total_Cost();
        }

        private void Calculate_Material_Cost(string strCurrentColumn, decimal decValue)
        {
            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
            decimal decCostTotalExVAT = (decimal)0.00;
            decimal decCostTotalVATRate = (decimal)0.00;
            decimal decCostTotalVAT = (decimal)0.00;
            decimal decCostTotalCost = (decimal)0.00;
            decCostTotalExVAT = (strCurrentColumn == "CostTotalMaterialExVAT" ? decValue : (string.IsNullOrWhiteSpace(currentRow["CostTotalMaterialExVAT"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.CostTotalMaterialExVAT)));
            decCostTotalVATRate = (strCurrentColumn == "CostTotalMaterialVATRate" ? decValue : (string.IsNullOrWhiteSpace(currentRow["CostTotalMaterialVATRate"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.CostTotalMaterialVATRate)));
            switch (strCurrentColumn)
            {
                case "CostTotalMaterialExVAT":
                    {
                        if (decCostTotalVATRate != (decimal)0.00)
                        {
                            decCostTotalVAT = (decCostTotalExVAT * (decCostTotalVATRate / 100));
                            decCostTotalCost = (decCostTotalExVAT + decCostTotalVAT);
                        }
                        else
                        {
                            decCostTotalVAT = (decimal)0.00;
                            decCostTotalCost = (decCostTotalExVAT + decCostTotalVAT);
                        }
                        currentRow.CostTotalMaterialVAT = decCostTotalVAT;
                        currentRow.CostTotalMaterialCost = decCostTotalCost;
                        sp06174OMJobEditBindingSource.EndEdit();
                    }
                    break;
                case "CostTotalMaterialVATRate":
                    {
                        if (decCostTotalExVAT != (decimal)0.00)
                        {
                            decCostTotalVAT = (decCostTotalExVAT * (decCostTotalVATRate / 100));
                            decCostTotalCost = (decCostTotalExVAT + decCostTotalVAT);
                        }
                        else
                        {
                            decCostTotalVAT = (decimal)0.00;
                            decCostTotalCost = (decimal)0.00;
                        }
                        currentRow.CostTotalMaterialVAT = decCostTotalVAT;
                        currentRow.CostTotalMaterialCost = decCostTotalCost;
                        sp06174OMJobEditBindingSource.EndEdit();
                    }
                    break;
                default:
                    break;
            }
        }

        private void Calculate_Total_Cost()
        {
            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
            decimal decCostTotalLabourExVAT = (string.IsNullOrWhiteSpace(currentRow["CostTotalLabourExVAT"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.CostTotalLabourExVAT));
            decimal decCostTotalEquipmentExVAT = (string.IsNullOrWhiteSpace(currentRow["CostTotalEquipmentExVAT"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.CostTotalEquipmentExVAT));
            decimal decCostTotalMaterialExVAT = (string.IsNullOrWhiteSpace(currentRow["CostTotalMaterialExVAT"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.CostTotalMaterialExVAT));
            currentRow.CostTotalCostExVAT = (decCostTotalLabourExVAT + decCostTotalEquipmentExVAT + decCostTotalMaterialExVAT);

            decimal decCostTotalLabourVAT = (string.IsNullOrWhiteSpace(currentRow["CostTotalLabourVAT"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.CostTotalLabourVAT));
            decimal decCostTotalEquipmentVAT = (string.IsNullOrWhiteSpace(currentRow["CostTotalEquipmentVAT"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.CostTotalEquipmentVAT));
            decimal decCostTotalMaterialVAT = (string.IsNullOrWhiteSpace(currentRow["CostTotalMaterialVAT"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.CostTotalMaterialVAT));
            currentRow.CostTotalCostVAT = (decCostTotalLabourVAT + decCostTotalEquipmentVAT + decCostTotalMaterialVAT);

            currentRow.CostTotalCost = (currentRow.CostTotalCostExVAT + currentRow.CostTotalCostVAT);
            sp06174OMJobEditBindingSource.EndEdit();
        }


        // ------------- SELL -------------- //

        private void SellTotalLabourExVATSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void SellTotalLabourExVATSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = (string.IsNullOrWhiteSpace(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value));
            Calculate_Labour_Sell("SellTotalLabourExVAT", decValue);
            Calculate_Total_Sell();
        }

        private void SellTotalLabourVATRateSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void SellTotalLabourVATRateSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = (string.IsNullOrWhiteSpace(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value));
            Calculate_Labour_Sell("SellTotalLabourVATRate", decValue);
            Calculate_Total_Sell();
        }

        private void Calculate_Labour_Sell(string strCurrentColumn, decimal decValue)
        {
            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
            decimal decSellTotalExVAT = (decimal)0.00;
            decimal decSellTotalVATRate = (decimal)0.00;
            decimal decSellTotalVAT = (decimal)0.00;
            decimal decSellTotalCost = (decimal)0.00;
            decSellTotalExVAT = (strCurrentColumn == "SellTotalLabourExVAT" ? decValue : (string.IsNullOrWhiteSpace(currentRow["SellTotalLabourExVAT"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.SellTotalLabourExVAT)));
            decSellTotalVATRate = (strCurrentColumn == "SellTotalLabourVATRate" ? decValue : (string.IsNullOrWhiteSpace(currentRow["SellTotalLabourVATRate"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.SellTotalLabourVATRate)));
            switch (strCurrentColumn)
            {
                case "SellTotalLabourExVAT":
                    {
                        if (decSellTotalVATRate != (decimal)0.00)
                        {
                            decSellTotalVAT = (decSellTotalExVAT * (decSellTotalVATRate / 100));
                            decSellTotalCost = (decSellTotalExVAT + decSellTotalVAT);
                        }
                        else
                        {
                            decSellTotalVAT = (decimal)0.00;
                            decSellTotalCost = (decSellTotalExVAT + decSellTotalVAT);
                        }
                        currentRow.SellTotalLabourVAT = decSellTotalVAT;
                        currentRow.SellTotalLabourCost = decSellTotalCost;
                        sp06174OMJobEditBindingSource.EndEdit();
                    }
                    break;
                case "SellTotalLabourVATRate":
                    {
                        if (decSellTotalExVAT != (decimal)0.00)
                        {
                            decSellTotalVAT = (decSellTotalExVAT * (decSellTotalVATRate / 100));
                            decSellTotalCost = (decSellTotalExVAT + decSellTotalVAT);
                        }
                        else
                        {
                            decSellTotalVAT = (decimal)0.00;
                            decSellTotalCost = (decimal)0.00;
                        }
                        currentRow.SellTotalLabourVAT = decSellTotalVAT;
                        currentRow.SellTotalLabourCost = decSellTotalCost;
                        sp06174OMJobEditBindingSource.EndEdit();
                    }
                    break;
                default:
                    break;
            }
        }


        private void SellTotalEquipmentExVATSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void SellTotalEquipmentExVATSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = (string.IsNullOrWhiteSpace(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value));
            Calculate_Equipment_Sell("SellTotalEquipmentExVAT", decValue);
            Calculate_Total_Sell();
        }

        private void SellTotalEquipmentVATRateSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void SellTotalEquipmentVATRateSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = (string.IsNullOrWhiteSpace(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value));
            Calculate_Equipment_Sell("SellTotalEquipmentVATRate", decValue);
            Calculate_Total_Sell();
        }

        private void Calculate_Equipment_Sell(string strCurrentColumn, decimal decValue)
        {
            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
            decimal decSellTotalExVAT = (decimal)0.00;
            decimal decSellTotalVATRate = (decimal)0.00;
            decimal decSellTotalVAT = (decimal)0.00;
            decimal decSellTotalCost = (decimal)0.00;
            decSellTotalExVAT = (strCurrentColumn == "SellTotalEquipmentExVAT" ? decValue : (string.IsNullOrWhiteSpace(currentRow["SellTotalEquipmentExVAT"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.SellTotalEquipmentExVAT)));
            decSellTotalVATRate = (strCurrentColumn == "SellTotalEquipmentVATRate" ? decValue : (string.IsNullOrWhiteSpace(currentRow["SellTotalEquipmentVATRate"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.SellTotalEquipmentVATRate)));
            switch (strCurrentColumn)
            {
                case "SellTotalEquipmentExVAT":
                    {
                        if (decSellTotalVATRate != (decimal)0.00)
                        {
                            decSellTotalVAT = (decSellTotalExVAT * (decSellTotalVATRate / 100));
                            decSellTotalCost = (decSellTotalExVAT + decSellTotalVAT);
                        }
                        else
                        {
                            decSellTotalVAT = (decimal)0.00;
                            decSellTotalCost = (decSellTotalExVAT + decSellTotalVAT);
                        }
                        currentRow.SellTotalEquipmentVAT = decSellTotalVAT;
                        currentRow.SellTotalEquipmentCost = decSellTotalCost;
                        sp06174OMJobEditBindingSource.EndEdit();
                    }
                    break;
                case "SellTotalEquipmentVATRate":
                    {
                        if (decSellTotalExVAT != (decimal)0.00)
                        {
                            decSellTotalVAT = (decSellTotalExVAT * (decSellTotalVATRate / 100));
                            decSellTotalCost = (decSellTotalExVAT + decSellTotalVAT);
                        }
                        else
                        {
                            decSellTotalVAT = (decimal)0.00;
                            decSellTotalCost = (decimal)0.00;
                        }
                        currentRow.SellTotalEquipmentVAT = decSellTotalVAT;
                        currentRow.SellTotalEquipmentCost = decSellTotalCost;
                        sp06174OMJobEditBindingSource.EndEdit();
                    }
                    break;
                default:
                    break;
            }
        }


        private void SellTotalMaterialExVATSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void SellTotalMaterialExVATSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = (string.IsNullOrWhiteSpace(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value));
            Calculate_Material_Sell("SellTotalMaterialExVAT", decValue);
            Calculate_Total_Sell();
        }

        private void SellTotalMaterialVATRateSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void SellTotalMaterialVATRateSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = (string.IsNullOrWhiteSpace(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value));
            Calculate_Material_Sell("SellTotalMaterialVATRate", decValue);
            Calculate_Total_Sell();
        }

        private void Calculate_Material_Sell(string strCurrentColumn, decimal decValue)
        {
            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
            decimal decSellTotalExVAT = (decimal)0.00;
            decimal decSellTotalVATRate = (decimal)0.00;
            decimal decSellTotalVAT = (decimal)0.00;
            decimal decSellTotalCost = (decimal)0.00;
            decSellTotalExVAT = (strCurrentColumn == "SellTotalMaterialExVAT" ? decValue : (string.IsNullOrWhiteSpace(currentRow["SellTotalMaterialExVAT"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.SellTotalMaterialExVAT)));
            decSellTotalVATRate = (strCurrentColumn == "SellTotalMaterialVATRate" ? decValue : (string.IsNullOrWhiteSpace(currentRow["SellTotalMaterialVATRate"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.SellTotalMaterialVATRate)));
            switch (strCurrentColumn)
            {
                case "SellTotalMaterialExVAT":
                    {
                        if (decSellTotalVATRate != (decimal)0.00)
                        {
                            decSellTotalVAT = (decSellTotalExVAT * (decSellTotalVATRate / 100));
                            decSellTotalCost = (decSellTotalExVAT + decSellTotalVAT);
                        }
                        else
                        {
                            decSellTotalVAT = (decimal)0.00;
                            decSellTotalCost = (decSellTotalExVAT + decSellTotalVAT);
                        }
                        currentRow.SellTotalMaterialVAT = decSellTotalVAT;
                        currentRow.SellTotalMaterialCost = decSellTotalCost;
                        sp06174OMJobEditBindingSource.EndEdit();
                    }
                    break;
                case "SellTotalMaterialVATRate":
                    {
                        if (decSellTotalExVAT != (decimal)0.00)
                        {
                            decSellTotalVAT = (decSellTotalExVAT * (decSellTotalVATRate / 100));
                            decSellTotalCost = (decSellTotalExVAT + decSellTotalVAT);
                        }
                        else
                        {
                            decSellTotalVAT = (decimal)0.00;
                            decSellTotalCost = (decimal)0.00;
                        }
                        currentRow.SellTotalMaterialVAT = decSellTotalVAT;
                        currentRow.SellTotalMaterialCost = decSellTotalCost;
                        sp06174OMJobEditBindingSource.EndEdit();
                    }
                    break;
                default:
                    break;
            }
        }

        private void Calculate_Total_Sell()
        {
            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
            decimal decSellTotalLabourExVAT = (string.IsNullOrWhiteSpace(currentRow["SellTotalLabourExVAT"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.SellTotalLabourExVAT));
            decimal decSellTotalEquipmentExVAT = (string.IsNullOrWhiteSpace(currentRow["SellTotalEquipmentExVAT"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.SellTotalEquipmentExVAT));
            decimal decSellTotalMaterialExVAT = (string.IsNullOrWhiteSpace(currentRow["SellTotalMaterialExVAT"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.SellTotalMaterialExVAT));
            currentRow.SellTotalCostExVAT = (decSellTotalLabourExVAT + decSellTotalEquipmentExVAT + decSellTotalMaterialExVAT);

            decimal decSellTotalLabourVAT = (string.IsNullOrWhiteSpace(currentRow["SellTotalLabourVAT"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.SellTotalLabourVAT));
            decimal decSellTotalEquipmentVAT = (string.IsNullOrWhiteSpace(currentRow["SellTotalEquipmentVAT"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.SellTotalEquipmentVAT));
            decimal decSellTotalMaterialVAT = (string.IsNullOrWhiteSpace(currentRow["SellTotalMaterialVAT"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.SellTotalMaterialVAT));
            currentRow.SellTotalCostVAT = (decSellTotalLabourVAT + decSellTotalEquipmentVAT + decSellTotalMaterialVAT);

            currentRow.SellTotalCost = (currentRow.SellTotalCostExVAT + currentRow.SellTotalCostVAT);
            sp06174OMJobEditBindingSource.EndEdit();
        }

        #endregion


        private void CalculateCostButton_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("<color=red>Are you sure you wish to re-calculate the Job Costs?</color>", "Re-Calculate Job Costs", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) != DialogResult.Yes) return;
            Set_Team_Cost(true);
        }

        private void CalculateSellButton_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("<color=red>Are you sure you wish to re-calculate the Job Sell Price?</color>", "Re-Calculate Job Sell Price", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) != DialogResult.Yes) return;
            Set_Client_Sell(true);
        }

        private void Set_Team_Cost(bool showErrors)
        {
            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit") return;
            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            int intSelfBillingInvoiceID = 0;
            try
            {
                intSelfBillingInvoiceID = (string.IsNullOrEmpty(currentRow.SelfBillingInvoiceID.ToString()) ? 0 : Convert.ToInt32(currentRow.SelfBillingInvoiceID));
            }
            catch (Exception) { }
            if (intSelfBillingInvoiceID > 0) return;  // Can't change rates since Team Self-Billing Invoiced //
            
            int intVisitID = 0;
            try
            {
                intVisitID = (string.IsNullOrEmpty(currentRow.VisitID.ToString()) ? 0 : Convert.ToInt32(currentRow.VisitID));
            }
            catch (Exception) { }
            int intJobSubTypeID = 0;
            try
            {
                intJobSubTypeID = (string.IsNullOrEmpty(currentRow.JobSubTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.JobSubTypeID));
            }
            catch (Exception) { }

            if (intVisitID <= 0 || intJobSubTypeID <= 0)
            {
                if (showErrors)
                {
                    XtraMessageBox.Show("Select both the Visit and Job Sub-Type first before clicking the Re-Calculate Cost Price button.", "Re-Calculate Cost Price", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                currentRow.CostTotalLabourExVAT = (decimal)0.00;
                sp06174OMJobEditBindingSource.EndEdit();
                Calculate_Labour_Cost("CostTotalLabourExVAT", (decimal)0.00);
                Calculate_Total_Cost();
                return;
            }
            decimal decCurrentValue = (string.IsNullOrEmpty(currentRow.CostTotalLabourExVAT.ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.CostTotalLabourExVAT));
            decimal decValue = (decimal)0.00;

            int intCostCalculationLevelID = (string.IsNullOrEmpty(currentRow.VisitCostCalculationLevelID.ToString()) ? 0 : Convert.ToInt32(currentRow.VisitCostCalculationLevelID));
            switch (intCostCalculationLevelID)
            {
                case 0:  // Manual //
                    {
                        XtraMessageBox.Show("This job is linked to a Manually Calculated Cost Visit - Enter the required value in the Labour box.", "Re-Calculate Cost Price", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 1:  // Generic Visit Rate //
                    {
                        currentRow.CostTotalLabourExVAT = decValue;
                        currentRow.CostTotalMaterialExVAT = decValue;
                        currentRow.CostTotalLabourExVAT = decValue;
                        sp06174OMJobEditBindingSource.EndEdit();
                        Calculate_Labour_Cost("CostTotalLabourExVAT", decValue);
                        Calculate_Labour_Cost("CostTotalMaterialExVAT", decValue);
                        Calculate_Labour_Cost("CostTotalEquipmentExVAT", decValue);
                        Calculate_Total_Cost();
                    }
                    break;
                case 2:  // Job Specific Rate //
                    {
                        DateTime dtStartDate = DateTime.MinValue;
                        try
                        {
                            dtStartDate = Convert.ToDateTime(currentRow.ActualStartDate);
                        }
                        catch (Exception) { }
                        if (dtStartDate == DateTime.MinValue)
                        {
                            try
                            {
                                dtStartDate = Convert.ToDateTime(currentRow.ExpectedStartDate);
                            }
                            catch (Exception) { }
                        }
                        using (var GetValue = new DataSet_OM_JobTableAdapters.QueriesTableAdapter())
                        {
                            GetValue.ChangeConnectionString(strConnectionString);
                            try
                            {
                                decValue = Convert.ToDecimal(GetValue.sp06351_OM_Get_Job_Value(intVisitID, intJobSubTypeID, "teamcost", dtStartDate));
                                if (decValue != decCurrentValue)
                                {
                                    currentRow.CostTotalLabourExVAT = decValue;
                                    sp06174OMJobEditBindingSource.EndEdit();
                                    Calculate_Labour_Cost("CostTotalLabourExVAT", decValue);
                                    Calculate_Total_Cost();
                                }
                            }
                            catch (Exception) { }
                        }
                    }
                    break;
                case 3:  // Time \ Materials //
                    {
                        try
                        {
                            int intJobID = (string.IsNullOrEmpty(currentRow.JobID.ToString()) ? 0 : Convert.ToInt32(currentRow.JobID));
                            if (intJobID <= 0)
                            {
                                if (showErrors)
                                {
                                    XtraMessageBox.Show("Save the Job first before clicking the Re-Calculate Cost Price button.", "Re-Calculate Cost Price", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    return;
                                }
                            }
                            SqlDataAdapter sda = new SqlDataAdapter();
                            DataSet ds = new DataSet("NewDataSet");
                            SqlConnection SQlConn = new SqlConnection(strConnectionString);
                            SqlCommand cmd = null;
                            cmd = new SqlCommand("sp06369_OM_Get_Job_Time_And_Material_Totals", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@JobID", intJobID));
                            cmd.Parameters.Add(new SqlParameter("@CostType", "teamcost"));
                            sda = new SqlDataAdapter(cmd);
                            sda.Fill(ds, "Table");
                            if (ds.Tables[0].Rows.Count <= 0) return;
                            DataRow dr = ds.Tables[0].Rows[0];

                            currentRow.CostTotalLabourExVAT = Convert.ToDecimal(dr["LabourCostExVat"]);
                            currentRow.CostTotalLabourVAT = Convert.ToDecimal(dr["LabourCostVat"]);
                            currentRow.CostTotalLabourCost = Convert.ToDecimal(dr["LabourCostTotal"]);

                            currentRow.CostTotalMaterialExVAT = Convert.ToDecimal(dr["MaterialCostExVat"]);
                            currentRow.CostTotalMaterialVAT = Convert.ToDecimal(dr["MaterialCostVat"]);
                            currentRow.CostTotalMaterialCost = Convert.ToDecimal(dr["MaterialCostTotal"]);

                            currentRow.CostTotalEquipmentExVAT = Convert.ToDecimal(dr["EquipmentCostExVat"]);
                            currentRow.CostTotalEquipmentVAT = Convert.ToDecimal(dr["EquipmentCostVat"]);
                            currentRow.CostTotalEquipmentCost = Convert.ToDecimal(dr["EquipmentCostTotal"]);

                            currentRow.CostTotalCostExVAT = Convert.ToDecimal(dr["TotalCostExVat"]);
                            currentRow.CostTotalCostVAT = Convert.ToDecimal(dr["TotalCostVat"]);
                            currentRow.CostTotalCost = Convert.ToDecimal(dr["TotalCostTotal"]);
                            
                            sp06174OMJobEditBindingSource.EndEdit();
                        }
                        catch (Exception) { }
                    }
                    break;
                default:
                    break;
            }
        }

        private void Set_Client_Sell(bool showErrors)
        {
            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit") return;
            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            int intClientInvoiceID = 0;
            try
            {
                intClientInvoiceID = (string.IsNullOrEmpty(currentRow.ClientInvoiceID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientInvoiceID));
            }
            catch (Exception) { }
            if (intClientInvoiceID > 0) return;  // Can't change rates since Client Invoiced //

            int intVisitID = 0;
            try
            {
                intVisitID = (string.IsNullOrEmpty(currentRow.VisitID.ToString()) ? 0 : Convert.ToInt32(currentRow.VisitID));
            }
            catch (Exception) { }
            int intJobSubTypeID = 0;
            try
            {
                intJobSubTypeID = (string.IsNullOrEmpty(currentRow.JobSubTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.JobSubTypeID));
            }
            catch (Exception) { }
            
            if (intVisitID <= 0 || intJobSubTypeID <= 0)
            {
                if (showErrors)
                {
                    XtraMessageBox.Show("Select both the Visit and Job Sub-Type first before clicking the Re-Calculate Sell Price button.", "Re-Calculate Sell Price", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                currentRow.SellTotalLabourExVAT = (decimal)0.00;
                sp06174OMJobEditBindingSource.EndEdit();
                Calculate_Labour_Sell("SellTotalLabourExVAT", (decimal)0.00);
                Calculate_Total_Sell();
                return;
            }
            decimal decCurrentValue = (string.IsNullOrEmpty(currentRow.SellTotalLabourExVAT.ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.SellTotalLabourExVAT));
            decimal decValue = (decimal)0.00;

            int intCostCalculationLevelID = (string.IsNullOrEmpty(currentRow.VisitCostCalculationLevelID.ToString()) ? 0 : Convert.ToInt32(currentRow.VisitCostCalculationLevelID));
            switch (intCostCalculationLevelID)
            {
                case 0:  // Manual //
                    {
                        XtraMessageBox.Show("This job is linked to a Manually Calculated Sell Visit - Enter the required value in the Labour box.", "Re-Calculate Sell Price", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 1:  // Generic Visit Rate //
                    {
                        currentRow.SellTotalLabourExVAT = decValue;
                        currentRow.SellTotalMaterialExVAT = decValue;
                        currentRow.SellTotalLabourExVAT = decValue;
                        sp06174OMJobEditBindingSource.EndEdit();
                        Calculate_Labour_Sell("SellTotalLabourExVAT", decValue);
                        Calculate_Labour_Sell("SellTotalMaterialExVAT", decValue);
                        Calculate_Labour_Sell("SellTotalEquipmentExVAT", decValue);
                        Calculate_Total_Sell();
                    }
                    break;
                case 2:  // Job Specific Rate //
                    {
                        DateTime dtStartDate = DateTime.MinValue;
                        try
                        {
                            dtStartDate = Convert.ToDateTime(currentRow.ActualStartDate);
                        }
                        catch (Exception) { }
                        if (dtStartDate == DateTime.MinValue)
                        {
                            try
                            {
                                dtStartDate = Convert.ToDateTime(currentRow.ExpectedStartDate);
                            }
                            catch (Exception) { }
                        }
                        using (var GetValue = new DataSet_OM_JobTableAdapters.QueriesTableAdapter())
                        {
                            GetValue.ChangeConnectionString(strConnectionString);
                            try
                            {
                                decValue = Convert.ToDecimal(GetValue.sp06351_OM_Get_Job_Value(intVisitID, intJobSubTypeID, "clientsell", dtStartDate));
                                if (decValue != decCurrentValue)
                                {
                                    currentRow.SellTotalLabourExVAT = decValue;
                                    sp06174OMJobEditBindingSource.EndEdit();
                                    Calculate_Labour_Sell("SellTotalLabourExVAT", decValue);
                                    Calculate_Total_Sell();
                                }
                            }
                            catch (Exception) { }
                        }
                    }
                    break;
                case 3:  // Time \ Materials //
                    {
                        try
                        {
                            int intJobID = (string.IsNullOrEmpty(currentRow.JobID.ToString()) ? 0 : Convert.ToInt32(currentRow.JobID));
                            if (intJobID <= 0)
                            {
                                if (showErrors)
                                {
                                    XtraMessageBox.Show("Save the Job first before clicking the Re-Calculate Sell Price button.", "Re-Calculate Sell Price", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    return;
                                }
                            }
                            SqlDataAdapter sda = new SqlDataAdapter();
                            DataSet ds = new DataSet("NewDataSet");
                            SqlConnection SQlConn = new SqlConnection(strConnectionString);
                            SqlCommand cmd = null;
                            cmd = new SqlCommand("sp06369_OM_Get_Job_Time_And_Material_Totals", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@JobID", intJobID));
                            cmd.Parameters.Add(new SqlParameter("@CostType", "clientsell"));
                            sda = new SqlDataAdapter(cmd);
                            sda.Fill(ds, "Table");
                            if (ds.Tables[0].Rows.Count <= 0) return;
                            DataRow dr = ds.Tables[0].Rows[0];
                            
                            currentRow.SellTotalLabourExVAT = Convert.ToDecimal(dr["LabourCostExVat"]);
                            currentRow.SellTotalLabourVAT = Convert.ToDecimal(dr["LabourCostVat"]);
                            currentRow.SellTotalLabourCost = Convert.ToDecimal(dr["LabourCostTotal"]);

                            currentRow.SellTotalMaterialExVAT = Convert.ToDecimal(dr["MaterialCostExVat"]);
                            currentRow.SellTotalMaterialVAT = Convert.ToDecimal(dr["MaterialCostVat"]);
                            currentRow.SellTotalMaterialCost = Convert.ToDecimal(dr["MaterialCostTotal"]);

                            currentRow.SellTotalEquipmentExVAT = Convert.ToDecimal(dr["EquipmentCostExVat"]);
                            currentRow.SellTotalEquipmentVAT = Convert.ToDecimal(dr["EquipmentCostVat"]);
                            currentRow.SellTotalEquipmentCost = Convert.ToDecimal(dr["EquipmentCostTotal"]);

                            currentRow.SellTotalCostExVAT = Convert.ToDecimal(dr["TotalCostExVat"]);
                            currentRow.SellTotalCostVAT = Convert.ToDecimal(dr["TotalCostVat"]);
                            currentRow.SellTotalCost = Convert.ToDecimal(dr["TotalCostTotal"]);
                            
                            sp06174OMJobEditBindingSource.EndEdit();
                        }
                        catch (Exception) { }
                    }
                    break;
                default:
                    break;
            }
        }

        private void Set_Field_Enabled_Status()
        {
            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit")
            {
                CostTotalLabourExVATSpinEdit.Properties.ReadOnly = true;
                CalculateCostButton.Enabled = false;
                CostTotalLabourVATRateSpinEdit.Properties.ReadOnly = true;
                CostTotalMaterialExVATSpinEdit.Properties.ReadOnly = true;
                CostTotalMaterialVATRateSpinEdit.Properties.ReadOnly = true;
                CostTotalEquipmentExVATSpinEdit.Properties.ReadOnly = true;
                CostTotalEquipmentVATRateSpinEdit.Properties.ReadOnly = true;

                SellTotalLabourExVATSpinEdit.Properties.ReadOnly = true;
                CalculateSellButton.Enabled = false;
                SellTotalLabourVATRateSpinEdit.Properties.ReadOnly = true;
                SellTotalMaterialExVATSpinEdit.Properties.ReadOnly = true;
                SellTotalMaterialVATRateSpinEdit.Properties.ReadOnly = true;
                SellTotalEquipmentExVATSpinEdit.Properties.ReadOnly = true;
                SellTotalEquipmentVATRateSpinEdit.Properties.ReadOnly = true;

                VisitNumberButtonEdit.Properties.ReadOnly = true;
                VisitNumberButtonEdit.Properties.Buttons[0].Enabled = false;

                if (strFormMode == "blockedit" && (_AtLeastOneSelfBillingInvoice || _AtLeastOneClientInvoice))
                {
                    JobSubTypeDescriptionButtonEdit.Properties.ReadOnly = true;
                    JobSubTypeDescriptionButtonEdit.Properties.Buttons[0].Enabled = false;
                    ExpectedStartDateDateEdit.Properties.ReadOnly = true;
                    ExpectedEndDateDateEdit.Properties.ReadOnly = true;
                    ActualStartDateDateEdit.Properties.ReadOnly = true;
                    ActualEndDateDateEdit.Properties.ReadOnly = true;
                }
                return;
            }
            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
            if (currentRow == null) return;
            int intCostCalculationLevel = 0;
            int intSelfBillingInvoiceID = 0;
            try
            {
                intCostCalculationLevel = (string.IsNullOrEmpty(currentRow.VisitCostCalculationLevelID.ToString()) ? 0 : Convert.ToInt32(currentRow.VisitCostCalculationLevelID));
                intSelfBillingInvoiceID = (string.IsNullOrEmpty(currentRow.SelfBillingInvoiceID.ToString()) ? 0 : Convert.ToInt32(currentRow.SelfBillingInvoiceID));
            }
            catch (Exception) { }
            CostTotalLabourExVATSpinEdit.Properties.ReadOnly = (intCostCalculationLevel == 0 && intSelfBillingInvoiceID == 0 ? false : true);
            CalculateCostButton.Enabled = (intCostCalculationLevel > 0 && intSelfBillingInvoiceID == 0 ? true : false);
            CostTotalLabourVATRateSpinEdit.Properties.ReadOnly = (intCostCalculationLevel == 0 && intSelfBillingInvoiceID == 0 ? false : true);
            CostTotalMaterialExVATSpinEdit.Properties.ReadOnly = (intCostCalculationLevel == 0 && intSelfBillingInvoiceID == 0 ? false : true);
            CostTotalMaterialVATRateSpinEdit.Properties.ReadOnly = (intCostCalculationLevel == 0 && intSelfBillingInvoiceID == 0 ? false : true);
            CostTotalEquipmentExVATSpinEdit.Properties.ReadOnly = (intCostCalculationLevel == 0 && intSelfBillingInvoiceID == 0 ? false : true);
            CostTotalEquipmentVATRateSpinEdit.Properties.ReadOnly = (intCostCalculationLevel == 0 && intSelfBillingInvoiceID == 0 ? false : true);

            int intSellCalculationLevel = 0;
            int intClientInvoiceID = 0;
            try
            {
                intSellCalculationLevel = (string.IsNullOrEmpty(currentRow.VisitSellCalculationLevelID.ToString()) ? 0 : Convert.ToInt32(currentRow.VisitSellCalculationLevelID));
                intClientInvoiceID = (string.IsNullOrEmpty(currentRow.ClientInvoiceID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientInvoiceID));
            }
            catch (Exception) { }
            SellTotalLabourExVATSpinEdit.Properties.ReadOnly = (intSellCalculationLevel == 0 && intClientInvoiceID == 0 ? false : true);
            CalculateSellButton.Enabled = (intSellCalculationLevel > 0 && intClientInvoiceID == 0 ? true : false);
            SellTotalLabourVATRateSpinEdit.Properties.ReadOnly = (intSellCalculationLevel == 0 && intClientInvoiceID == 0 ? false : true);
            SellTotalMaterialExVATSpinEdit.Properties.ReadOnly = (intSellCalculationLevel == 0 && intClientInvoiceID == 0 ? false : true);
            SellTotalMaterialVATRateSpinEdit.Properties.ReadOnly = (intSellCalculationLevel == 0 && intClientInvoiceID == 0 ? false : true);
            SellTotalEquipmentExVATSpinEdit.Properties.ReadOnly = (intSellCalculationLevel == 0 && intClientInvoiceID == 0 ? false : true);
            SellTotalEquipmentVATRateSpinEdit.Properties.ReadOnly = (intSellCalculationLevel == 0 && intClientInvoiceID == 0 ? false : true);

            VisitNumberButtonEdit.Properties.ReadOnly = (intSelfBillingInvoiceID > 0 || intClientInvoiceID > 0 ? true : false);
            VisitNumberButtonEdit.Properties.Buttons[0].Enabled = (intSelfBillingInvoiceID > 0 || intClientInvoiceID > 0 ? false : true);

            JobSubTypeDescriptionButtonEdit.Properties.ReadOnly = (intSelfBillingInvoiceID > 0 || intClientInvoiceID > 0 ? true : false);
            JobSubTypeDescriptionButtonEdit.Properties.Buttons[0].Enabled = (intSelfBillingInvoiceID > 0 || intClientInvoiceID > 0 ? false : true);

            ExpectedStartDateDateEdit.Properties.ReadOnly = (intSelfBillingInvoiceID > 0 || intClientInvoiceID > 0 ? true : false);
            ExpectedEndDateDateEdit.Properties.ReadOnly = (intSelfBillingInvoiceID > 0 || intClientInvoiceID > 0 ? true : false);
            ActualStartDateDateEdit.Properties.ReadOnly = (intSelfBillingInvoiceID > 0 || intClientInvoiceID > 0 ? true : false);
            ActualEndDateDateEdit.Properties.ReadOnly = (intSelfBillingInvoiceID > 0 || intClientInvoiceID > 0 ? true : false);

        }

        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }


        private void Add_Record()
        {
            if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            var rowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var row = (DataSet_OM_Job.sp06174_OM_Job_EditRow)rowView.Row;
            if (rowView == null) return;
            int intJobID = (string.IsNullOrWhiteSpace(rowView["JobID"].ToString()) ? 0 : Convert.ToInt32(rowView["JobID"]));
            if (intJobID <= 0)
            {
                XtraMessageBox.Show("Please Save the Job record before attempting to add linked records.", "Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Labour:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();
                        RefreshGridViewStateLabour.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_OM_Job_Labour_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm._ClientID = row.ClientID;
                        fChildForm._ClientContractID = row.ClientContractID;
                        fChildForm._SiteID = row.SiteID;
                        fChildForm._SiteContractID = row.SiteContractID;
                        fChildForm._VisitID = row.VisitID;
                        fChildForm._VisitNumber = row.VisitNumber;
                        fChildForm._JobID = row.JobID;
                        fChildForm._JobTypeID = row.JobTypeID;
                        fChildForm._JobSubTypeID = row.JobSubTypeID;
                        fChildForm._ClientName = row.ClientName;
                        fChildForm._ContractDescription = row.ContractDescription;
                        fChildForm._SiteName = row.SiteName;
                        fChildForm._JobTypeDescription = row.JobTypeDescription;
                        fChildForm._JobSubTypeDescription = row.JobSubTypeDescription;
                        fChildForm._SiteLatitude = row.LocationX;
                        fChildForm._SiteLongitude = row.LocationY;
                        fChildForm._SitePostcode = row.SitePostcode;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlEquipment.MainView;
                        view.PostEditor();
                        RefreshGridViewStateEquipment.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_OM_Job_Equipment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm._ClientID = row.ClientID;
                        fChildForm._ClientContractID = row.ClientContractID;
                        fChildForm._SiteID = row.SiteID;
                        fChildForm._SiteContractID = row.SiteContractID;
                        fChildForm._VisitID = row.VisitID;
                        fChildForm._VisitNumber = row.VisitNumber;
                        fChildForm._JobID = row.JobID;
                        fChildForm._JobTypeID = row.JobTypeID;
                        fChildForm._JobSubTypeID = row.JobSubTypeID;
                        fChildForm._ClientName = row.ClientName;
                        fChildForm._ContractDescription = row.ContractDescription;
                        fChildForm._SiteName = row.SiteName;
                        fChildForm._JobTypeDescription = row.JobTypeDescription;
                        fChildForm._JobSubTypeDescription = row.JobSubTypeDescription;
                        fChildForm._SiteLatitude = row.LocationX;
                        fChildForm._SiteLongitude = row.LocationY;
                        fChildForm._SitePostcode = row.SitePostcode;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlMaterials.MainView;
                        view.PostEditor();
                        RefreshGridViewStateMaterials.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_OM_Job_Material_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm._ClientID = row.ClientID;
                        fChildForm._ClientContractID = row.ClientContractID;
                        fChildForm._SiteID = row.SiteID;
                        fChildForm._SiteContractID = row.SiteContractID;
                        fChildForm._VisitID = row.VisitID;
                        fChildForm._VisitNumber = row.VisitNumber;
                        fChildForm._JobID = row.JobID;
                        fChildForm._JobTypeID = row.JobTypeID;
                        fChildForm._JobSubTypeID = row.JobSubTypeID;
                        fChildForm._ClientName = row.ClientName;
                        fChildForm._ContractDescription = row.ContractDescription;
                        fChildForm._SiteName = row.SiteName;
                        fChildForm._JobTypeDescription = row.JobTypeDescription;
                        fChildForm._JobSubTypeDescription = row.JobSubTypeDescription;
                        fChildForm._SiteLatitude = row.LocationX;
                        fChildForm._SiteLongitude = row.LocationY;
                        fChildForm._SitePostcode = row.SitePostcode;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit_Record()
        {
            if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            GridView view = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Labour:
                    {
                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LabourUsedID")) + ',';
                        }
                        RefreshGridViewStateLabour.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Job_Labour_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        view = (GridView)gridControlEquipment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EquipmentUsedID")) + ',';
                        }
                        RefreshGridViewStateEquipment.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Job_Equipment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        view = (GridView)gridControlMaterials.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "MaterialUsedID")) + ',';
                        }
                        RefreshGridViewStateMaterials.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Job_Material_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            GridView view = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Labour:
                    {
                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LabourUsedID")) + ',';
                        }
                        RefreshGridViewStateLabour.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Job_Labour_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        view = (GridView)gridControlEquipment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EquipmentUsedID")) + ',';
                        }
                        RefreshGridViewStateEquipment.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Job_Equipment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        view = (GridView)gridControlMaterials.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "MaterialUsedID")) + ',';
                        }
                        RefreshGridViewStateMaterials.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Job_Material_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Labour:
                    {
                        view = (GridView)gridControlLabour.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Labour Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Labour Record" : Convert.ToString(intRowHandles.Length) + " Labour Records") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Labour Record" : "these Labour Records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "LabourUsedID")) + ",";
                            }

                            RefreshGridViewStateLabour.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job_labour_used", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Linked_Labour();
                            FilterGrids();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        view = (GridView)gridControlEquipment.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Equipment Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Equipment Record" : Convert.ToString(intRowHandles.Length) + " Equipment Records") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Equipment Record" : "these Equipment Records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "EquipmentUsedID")) + ",";
                            }

                            RefreshGridViewStateEquipment.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job_equipment_used", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Linked_Equipment();
                            FilterGrids();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        view = (GridView)gridControlMaterials.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Material Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Material Record" : Convert.ToString(intRowHandles.Length) + " Material Records") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Material Record" : "these Material Records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "MaterialUsedID")) + ",";
                            }

                            RefreshGridViewStateMaterials.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job_equipment_used", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Linked_Materials();
                            FilterGrids();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            //switch (i_int_FocusedGrid)
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Labour:
                    {
                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LabourUsedID")) + ',';
                        }
                        RefreshGridViewStateLabour.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Job_Labour_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        view = (GridView)gridControlEquipment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EquipmentUsedID")) + ',';
                        }
                        RefreshGridViewStateEquipment.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Job_Equipment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        view = (GridView)gridControlMaterials.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "MaterialUsedID")) + ',';
                        }
                        RefreshGridViewStateMaterials.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Job_Material_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }


        private void FilterGrids()
        {
            string strID = "";
            DataRowView currentRow = (DataRowView)sp06174OMJobEditBindingSource.Current;
            if (currentRow != null)
            {
                strID = (currentRow["JobID"] == null ? "0" : currentRow["JobID"].ToString());
            }

            GridView view = (GridView)gridControlLabour.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[JobID] = " + Convert.ToString(strID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();

            view = (GridView)gridControlEquipment.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[JobID] = " + Convert.ToString(strID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
            
            view = (GridView)gridControlMaterials.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[JobID] = " + Convert.ToString(strID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }


        private void bbiLinkedDocuments_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "blockedit" || strFormMode == "blockadd") return;
            DataRowView currentRow = (DataRowView)sp06174OMJobEditBindingSource.Current;
            if (currentRow == null) return;
            int intRecordID = (string.IsNullOrEmpty(currentRow["JobID"].ToString()) ? 0 : Convert.ToInt32(currentRow["JobID"]));
            if (intRecordID <= 0) return;

            // Drilldown to Linked Document Manager //
            int intRecordType = 21;  // Job //
            int intRecordSubType = 0;  // Not Used //
            string strRecordDescription = currentRow["ClientName"].ToString() + ", Contract: " + currentRow["ContractDescription"].ToString() + ", Site: " + currentRow["SiteName"].ToString() + ", Visit #: " + currentRow["VisitNumber"].ToString() + ", Job: " + currentRow["JobTypeDescription"].ToString() + " - " + currentRow["JobSubTypeDescription"].ToString();
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription); 
        }

        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp06049_OM_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "om_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);

        }

        private void bbiViewOnMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "blockedit" || strFormMode == "blockadd") return;
            DataRowView currentRow = (DataRowView)sp06174OMJobEditBindingSource.Current;
            if (currentRow == null) return;
            int intRecordID = (string.IsNullOrEmpty(currentRow["JobID"].ToString()) ? 0 : Convert.ToInt32(currentRow["JobID"]));
            if (intRecordID <= 0)
            {
                XtraMessageBox.Show("Please Save the Job before attempting to show it's start and end locations on the map.", "View Jobs on Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedIDs = intRecordID.ToString() + ",";
            string strClientIDs = currentRow["ClientID"].ToString() +  ",";
            string strSiteIDs = currentRow["SiteID"].ToString() + ",";
            string strVisitIDs = currentRow["VisitID"].ToString() + ","; 
            string strSelectedFullSiteDescriptions = "Client: " + currentRow["ClientName"].ToString() + ", Site: " + currentRow["SiteName"].ToString() + " | ";  // Pipe used as deimiter //

            var fChildForm = new frm_OM_Visit_Mapping_View();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_PassedInJobIDs = strSelectedIDs;
            fChildForm.i_str_PassedInVisitIDs = strVisitIDs;
            fChildForm.i_str_PassedInSiteIDs = strSiteIDs;
            fChildForm.i_str_PassedInClientIDs = strClientIDs;
            fChildForm.i_str_selected_Site_descriptions = strSelectedFullSiteDescriptions;
            fChildForm.i_bool_LoadVisitsInMapOnStart = false;
            fChildForm.i_bool_LoadSitesInMapOnStart = false;
            fChildForm.i_bool_LoadJobsInMapOnStart = true;
            fChildForm.strFormMode = "view";
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }


    }
}


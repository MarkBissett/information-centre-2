﻿namespace WoodPlan5
{
    partial class frm_Operatives_Location
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Operatives_Location));
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping1 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping2 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping3 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping4 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraMap.MapItemAttributeMapping mapItemAttributeMapping5 = new DevExpress.XtraMap.MapItemAttributeMapping();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            this.colDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.imageTilesLayer1 = new DevExpress.XtraMap.ImageTilesLayer();
            this.bingMapDataProvider1 = new DevExpress.XtraMap.BingMapDataProvider();
            this.vectorItemsLayer1 = new DevExpress.XtraMap.VectorItemsLayer();
            this.listSourceDataAdapter1 = new DevExpress.XtraMap.ListSourceDataAdapter();
            this.sp06017GetOperativeLocationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Summer_Core = new WoodPlan5.DataSet_GC_Summer_Core();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp06016GetTeamsFromLocationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobileTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTextNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsGritter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsWoodPlanVisible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsSnowClearer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsUtilityArbTeam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlMapType = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControlMapType = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.btnOKMapType = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlDateRangeFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.EndTimeTimeEdit = new BaseObjects.TimeSpanEdit();
            this.StartTimeTimeEdit = new BaseObjects.TimeSpanEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnDateRangeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.mapControl1 = new BaseObjects.ExtendedMapControl();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barEditItemDateRange = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateRangeFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barEditItemPingType = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditPingType = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlPingTypes = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp06125OMOperativeLocationPingTypeFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPingTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPingTypeText = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.btnPingTypeOK = new DevExpress.XtraEditors.SimpleButton();
            this.bbiRefreshTeams = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemMapType = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditMapType = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.repositoryItemRadioGroup1 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.sp06016_Get_Teams_From_LocationsTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06016_Get_Teams_From_LocationsTableAdapter();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanelLegend = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit7 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.btnLoadPings = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp06126GetOperativePingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOperativeLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWhereaboutsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRecorded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobileTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisabled1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colInternalContractor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTextNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsGritter1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsWoodPlanVisible1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsSnowClearer1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsUtilityArbTeam1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colThematicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNearestPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccuracy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colOS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPingType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPingTypeFriendly = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colToken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnRefreshMap = new DevExpress.XtraEditors.SimpleButton();
            this.sp06017_Get_Operative_LocationsTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06017_Get_Operative_LocationsTableAdapter();
            this.pmMapItemMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiViewObjectOnMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExportToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bciLegend = new DevExpress.XtraBars.BarCheckItem();
            this.bsiMapObjectCount = new DevExpress.XtraBars.BarStaticItem();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp06125_OM_Operative_Location_Ping_Type_FilterTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06125_OM_Operative_Location_Ping_Type_FilterTableAdapter();
            this.sp06126_Get_Operative_PingsTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06126_Get_Operative_PingsTableAdapter();
            this.bbiMapPrintPreview = new DevExpress.XtraBars.BarButtonItem();
            this.pmMapControl = new DevExpress.XtraBars.PopupMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06017GetOperativeLocationsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Summer_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06016GetTeamsFromLocationsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlMapType)).BeginInit();
            this.popupContainerControlMapType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlMapType)).BeginInit();
            this.groupControlMapType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRangeFilter)).BeginInit();
            this.popupContainerControlDateRangeFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EndTimeTimeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartTimeTimeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRangeFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditPingType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlPingTypes)).BeginInit();
            this.popupContainerControlPingTypes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06125OMOperativeLocationPingTypeFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditMapType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanelLegend.SuspendLayout();
            this.dockPanel2_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06126GetOperativePingsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmMapItemMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmMapControl)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(950, 34);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 503);
            this.barDockControlBottom.Size = new System.Drawing.Size(950, 34);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 34);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 469);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(950, 34);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 469);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2});
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barEditItemMapType,
            this.bbiViewObjectOnMap,
            this.bsiMapObjectCount,
            this.barEditItemDateRange,
            this.barEditItemPingType,
            this.bbiRefreshTeams,
            this.bbiExportToExcel,
            this.bbiMapPrintPreview,
            this.bciLegend});
            this.barManager1.MaxItemId = 42;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemRadioGroup1,
            this.repositoryItemPopupContainerEditMapType,
            this.repositoryItemPopupContainerEditDateRangeFilter,
            this.repositoryItemPopupContainerEditPingType});
            this.barManager1.StatusBar = this.bar2;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colDisabled
            // 
            this.colDisabled.Caption = "Disabled";
            this.colDisabled.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDisabled.FieldName = "Disabled";
            this.colDisabled.Name = "colDisabled";
            this.colDisabled.OptionsColumn.AllowEdit = false;
            this.colDisabled.OptionsColumn.AllowFocus = false;
            this.colDisabled.OptionsColumn.ReadOnly = true;
            this.colDisabled.Visible = true;
            this.colDisabled.VisibleIndex = 10;
            this.colDisabled.Width = 65;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            this.imageTilesLayer1.DataProvider = this.bingMapDataProvider1;
            this.bingMapDataProvider1.BingKey = "As_rq12T3oFpxWh52cGqK5yhqcHov7wkd4orPObzZFgo2N92CqPDc1xO_uokdUdT";
            this.vectorItemsLayer1.Data = this.listSourceDataAdapter1;
            this.vectorItemsLayer1.ItemStyle.TextColor = System.Drawing.Color.Blue;
            this.vectorItemsLayer1.ToolTipPattern = "<b>Team:</b> {Name}\r\n<b>Time:</b>  {DateRecorded}\r\n<b>Ping Type:</b> {PingTypeFri" +
    "endly}";
            mapItemAttributeMapping1.Member = "Name";
            mapItemAttributeMapping1.Name = "Name";
            mapItemAttributeMapping1.ValueType = DevExpress.XtraMap.FieldValueType.String;
            mapItemAttributeMapping2.Member = "DateRecorded";
            mapItemAttributeMapping2.Name = "DateRecorded";
            mapItemAttributeMapping2.ValueType = DevExpress.XtraMap.FieldValueType.DateTime;
            mapItemAttributeMapping3.Member = "ContractorID";
            mapItemAttributeMapping3.Name = "ContractorID";
            mapItemAttributeMapping3.ValueType = DevExpress.XtraMap.FieldValueType.Integer;
            mapItemAttributeMapping4.Member = "OperativeLocationID";
            mapItemAttributeMapping4.Name = "OperativeLocationID";
            mapItemAttributeMapping4.ValueType = DevExpress.XtraMap.FieldValueType.Integer;
            mapItemAttributeMapping5.Member = "PingTypeFriendly";
            mapItemAttributeMapping5.Name = "PingTypeFriendly";
            mapItemAttributeMapping5.ValueType = DevExpress.XtraMap.FieldValueType.String;
            this.listSourceDataAdapter1.AttributeMappings.Add(mapItemAttributeMapping1);
            this.listSourceDataAdapter1.AttributeMappings.Add(mapItemAttributeMapping2);
            this.listSourceDataAdapter1.AttributeMappings.Add(mapItemAttributeMapping3);
            this.listSourceDataAdapter1.AttributeMappings.Add(mapItemAttributeMapping4);
            this.listSourceDataAdapter1.AttributeMappings.Add(mapItemAttributeMapping5);
            this.listSourceDataAdapter1.DataSource = this.sp06017GetOperativeLocationsBindingSource;
            this.listSourceDataAdapter1.Mappings.ImageIndex = "ThematicValue";
            this.listSourceDataAdapter1.Mappings.Latitude = "Latitude";
            this.listSourceDataAdapter1.Mappings.Longitude = "Longitude";
            this.listSourceDataAdapter1.Mappings.Text = "Name";
            // 
            // sp06017GetOperativeLocationsBindingSource
            // 
            this.sp06017GetOperativeLocationsBindingSource.DataMember = "sp06017_Get_Operative_Locations";
            this.sp06017GetOperativeLocationsBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // dataSet_GC_Summer_Core
            // 
            this.dataSet_GC_Summer_Core.DataSetName = "DataSet_GC_Summer_Core";
            this.dataSet_GC_Summer_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp06016GetTeamsFromLocationsBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(371, 200);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp06016GetTeamsFromLocationsBindingSource
            // 
            this.sp06016GetTeamsFromLocationsBindingSource.DataMember = "sp06016_Get_Teams_From_Locations";
            this.sp06016GetTeamsFromLocationsBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colContractorID,
            this.colName,
            this.colPostcode,
            this.colTelephone1,
            this.colTelephone2,
            this.colMobileTelephone,
            this.colDisabled,
            this.colInternalContractor,
            this.colTextNumber,
            this.colLatitude,
            this.colLongitude,
            this.colIsGritter,
            this.colIsWoodPlanVisible,
            this.colIsSnowClearer,
            this.colIsUtilityArbTeam,
            this.colLocationCount});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colDisabled;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 1;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.FindDelay = 2000;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            this.colContractorID.Width = 88;
            // 
            // colName
            // 
            this.colName.Caption = "Name";
            this.colName.FieldName = "Name";
            this.colName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.AllowFocus = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 163;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Visible = true;
            this.colPostcode.VisibleIndex = 2;
            this.colPostcode.Width = 65;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Width = 81;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Width = 81;
            // 
            // colMobileTelephone
            // 
            this.colMobileTelephone.Caption = "Mobile Tel";
            this.colMobileTelephone.FieldName = "MobileTelephone";
            this.colMobileTelephone.Name = "colMobileTelephone";
            this.colMobileTelephone.OptionsColumn.AllowEdit = false;
            this.colMobileTelephone.OptionsColumn.AllowFocus = false;
            this.colMobileTelephone.OptionsColumn.ReadOnly = true;
            this.colMobileTelephone.Visible = true;
            this.colMobileTelephone.VisibleIndex = 1;
            this.colMobileTelephone.Width = 74;
            // 
            // colInternalContractor
            // 
            this.colInternalContractor.Caption = "Internal";
            this.colInternalContractor.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colInternalContractor.FieldName = "InternalContractor";
            this.colInternalContractor.Name = "colInternalContractor";
            this.colInternalContractor.OptionsColumn.AllowEdit = false;
            this.colInternalContractor.OptionsColumn.AllowFocus = false;
            this.colInternalContractor.OptionsColumn.ReadOnly = true;
            this.colInternalContractor.Visible = true;
            this.colInternalContractor.VisibleIndex = 3;
            this.colInternalContractor.Width = 64;
            // 
            // colTextNumber
            // 
            this.colTextNumber.Caption = "Text #";
            this.colTextNumber.FieldName = "TextNumber";
            this.colTextNumber.Name = "colTextNumber";
            this.colTextNumber.OptionsColumn.AllowEdit = false;
            this.colTextNumber.OptionsColumn.AllowFocus = false;
            this.colTextNumber.OptionsColumn.ReadOnly = true;
            this.colTextNumber.Visible = true;
            this.colTextNumber.VisibleIndex = 4;
            this.colTextNumber.Width = 59;
            // 
            // colLatitude
            // 
            this.colLatitude.Caption = "Latitude";
            this.colLatitude.FieldName = "Latitude";
            this.colLatitude.Name = "colLatitude";
            this.colLatitude.OptionsColumn.AllowEdit = false;
            this.colLatitude.OptionsColumn.AllowFocus = false;
            this.colLatitude.OptionsColumn.ReadOnly = true;
            this.colLatitude.Width = 87;
            // 
            // colLongitude
            // 
            this.colLongitude.Caption = "Longitude";
            this.colLongitude.FieldName = "Longitude";
            this.colLongitude.Name = "colLongitude";
            this.colLongitude.OptionsColumn.AllowEdit = false;
            this.colLongitude.OptionsColumn.AllowFocus = false;
            this.colLongitude.OptionsColumn.ReadOnly = true;
            this.colLongitude.Width = 87;
            // 
            // colIsGritter
            // 
            this.colIsGritter.Caption = "Gritter";
            this.colIsGritter.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsGritter.FieldName = "IsGritter";
            this.colIsGritter.Name = "colIsGritter";
            this.colIsGritter.OptionsColumn.AllowEdit = false;
            this.colIsGritter.OptionsColumn.AllowFocus = false;
            this.colIsGritter.OptionsColumn.ReadOnly = true;
            this.colIsGritter.Visible = true;
            this.colIsGritter.VisibleIndex = 7;
            this.colIsGritter.Width = 52;
            // 
            // colIsWoodPlanVisible
            // 
            this.colIsWoodPlanVisible.Caption = "WoodPlan";
            this.colIsWoodPlanVisible.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsWoodPlanVisible.FieldName = "IsWoodPlanVisible";
            this.colIsWoodPlanVisible.Name = "colIsWoodPlanVisible";
            this.colIsWoodPlanVisible.OptionsColumn.AllowEdit = false;
            this.colIsWoodPlanVisible.OptionsColumn.AllowFocus = false;
            this.colIsWoodPlanVisible.OptionsColumn.ReadOnly = true;
            this.colIsWoodPlanVisible.Visible = true;
            this.colIsWoodPlanVisible.VisibleIndex = 6;
            this.colIsWoodPlanVisible.Width = 69;
            // 
            // colIsSnowClearer
            // 
            this.colIsSnowClearer.Caption = "Snow Clearer";
            this.colIsSnowClearer.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsSnowClearer.FieldName = "IsSnowClearer";
            this.colIsSnowClearer.Name = "colIsSnowClearer";
            this.colIsSnowClearer.OptionsColumn.AllowEdit = false;
            this.colIsSnowClearer.OptionsColumn.AllowFocus = false;
            this.colIsSnowClearer.OptionsColumn.ReadOnly = true;
            this.colIsSnowClearer.Visible = true;
            this.colIsSnowClearer.VisibleIndex = 8;
            this.colIsSnowClearer.Width = 85;
            // 
            // colIsUtilityArbTeam
            // 
            this.colIsUtilityArbTeam.Caption = "Utility ARB";
            this.colIsUtilityArbTeam.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsUtilityArbTeam.FieldName = "IsUtilityArbTeam";
            this.colIsUtilityArbTeam.Name = "colIsUtilityArbTeam";
            this.colIsUtilityArbTeam.OptionsColumn.AllowEdit = false;
            this.colIsUtilityArbTeam.OptionsColumn.AllowFocus = false;
            this.colIsUtilityArbTeam.OptionsColumn.ReadOnly = true;
            this.colIsUtilityArbTeam.Visible = true;
            this.colIsUtilityArbTeam.VisibleIndex = 5;
            this.colIsUtilityArbTeam.Width = 71;
            // 
            // colLocationCount
            // 
            this.colLocationCount.Caption = "Locations";
            this.colLocationCount.FieldName = "LocationCount";
            this.colLocationCount.Name = "colLocationCount";
            this.colLocationCount.OptionsColumn.AllowEdit = false;
            this.colLocationCount.OptionsColumn.AllowFocus = false;
            this.colLocationCount.OptionsColumn.ReadOnly = true;
            this.colLocationCount.Visible = true;
            this.colLocationCount.VisibleIndex = 9;
            this.colLocationCount.Width = 66;
            // 
            // popupContainerControlMapType
            // 
            this.popupContainerControlMapType.Controls.Add(this.groupControlMapType);
            this.popupContainerControlMapType.Controls.Add(this.btnOKMapType);
            this.popupContainerControlMapType.Location = new System.Drawing.Point(708, 48);
            this.popupContainerControlMapType.Name = "popupContainerControlMapType";
            this.popupContainerControlMapType.Size = new System.Drawing.Size(129, 157);
            this.popupContainerControlMapType.TabIndex = 9;
            // 
            // groupControlMapType
            // 
            this.groupControlMapType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControlMapType.Controls.Add(this.checkEdit4);
            this.groupControlMapType.Controls.Add(this.checkEdit3);
            this.groupControlMapType.Controls.Add(this.checkEdit2);
            this.groupControlMapType.Controls.Add(this.checkEdit1);
            this.groupControlMapType.Location = new System.Drawing.Point(3, 4);
            this.groupControlMapType.Name = "groupControlMapType";
            this.groupControlMapType.Size = new System.Drawing.Size(123, 123);
            this.groupControlMapType.TabIndex = 3;
            this.groupControlMapType.Text = "Map Type";
            // 
            // checkEdit4
            // 
            this.checkEdit4.Location = new System.Drawing.Point(5, 99);
            this.checkEdit4.MenuManager = this.barManager1;
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "Open Street Map";
            this.checkEdit4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit4.Properties.RadioGroupIndex = 1;
            this.checkEdit4.Size = new System.Drawing.Size(110, 19);
            this.checkEdit4.TabIndex = 3;
            this.checkEdit4.TabStop = false;
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(5, 74);
            this.checkEdit3.MenuManager = this.barManager1;
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "Bing Road";
            this.checkEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit3.Properties.RadioGroupIndex = 1;
            this.checkEdit3.Size = new System.Drawing.Size(110, 19);
            this.checkEdit3.TabIndex = 2;
            this.checkEdit3.TabStop = false;
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(5, 49);
            this.checkEdit2.MenuManager = this.barManager1;
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "Bing Satellite";
            this.checkEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit2.Properties.RadioGroupIndex = 1;
            this.checkEdit2.Size = new System.Drawing.Size(110, 19);
            this.checkEdit2.TabIndex = 1;
            this.checkEdit2.TabStop = false;
            // 
            // checkEdit1
            // 
            this.checkEdit1.EditValue = true;
            this.checkEdit1.Location = new System.Drawing.Point(5, 24);
            this.checkEdit1.MenuManager = this.barManager1;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Bing Hybrid";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit1.Properties.RadioGroupIndex = 1;
            this.checkEdit1.Size = new System.Drawing.Size(110, 19);
            this.checkEdit1.TabIndex = 0;
            // 
            // btnOKMapType
            // 
            this.btnOKMapType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOKMapType.Location = new System.Drawing.Point(3, 131);
            this.btnOKMapType.Name = "btnOKMapType";
            this.btnOKMapType.Size = new System.Drawing.Size(38, 23);
            this.btnOKMapType.TabIndex = 2;
            this.btnOKMapType.Text = "OK";
            this.btnOKMapType.Click += new System.EventHandler(this.btnOKMapType_Click);
            // 
            // popupContainerControlDateRangeFilter
            // 
            this.popupContainerControlDateRangeFilter.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRangeFilter.Controls.Add(this.btnDateRangeFilterOK);
            this.popupContainerControlDateRangeFilter.Location = new System.Drawing.Point(384, 48);
            this.popupContainerControlDateRangeFilter.Name = "popupContainerControlDateRangeFilter";
            this.popupContainerControlDateRangeFilter.Size = new System.Drawing.Size(318, 117);
            this.popupContainerControlDateRangeFilter.TabIndex = 8;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.EndTimeTimeEdit);
            this.groupControl1.Controls.Add(this.StartTimeTimeEdit);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.dateEditEnd);
            this.groupControl1.Controls.Add(this.dateEditStart);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(312, 83);
            this.groupControl1.TabIndex = 3;
            this.groupControl1.Text = "Date Range Filter";
            // 
            // EndTimeTimeEdit
            // 
            this.EndTimeTimeEdit.EditValue = System.TimeSpan.Parse("00:00:00");
            this.EndTimeTimeEdit.Location = new System.Drawing.Point(215, 55);
            this.EndTimeTimeEdit.MenuManager = this.barManager1;
            this.EndTimeTimeEdit.Name = "EndTimeTimeEdit";
            this.EndTimeTimeEdit.Properties.AllowSecondInput = false;
            this.EndTimeTimeEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.EndTimeTimeEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.EndTimeTimeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndTimeTimeEdit.Properties.DisplayFormat.FormatString = "HH:mm";
            this.EndTimeTimeEdit.Properties.EditFormat.FormatString = "HH:mm";
            this.EndTimeTimeEdit.Properties.Mask.EditMask = "HH:mm";
            this.EndTimeTimeEdit.Properties.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.DoubleClick;
            this.EndTimeTimeEdit.Properties.Tag = "";
            this.EndTimeTimeEdit.Size = new System.Drawing.Size(90, 20);
            this.EndTimeTimeEdit.TabIndex = 24;
            // 
            // StartTimeTimeEdit
            // 
            this.StartTimeTimeEdit.EditValue = System.TimeSpan.Parse("00:00:00");
            this.StartTimeTimeEdit.Location = new System.Drawing.Point(65, 55);
            this.StartTimeTimeEdit.MenuManager = this.barManager1;
            this.StartTimeTimeEdit.Name = "StartTimeTimeEdit";
            this.StartTimeTimeEdit.Properties.AllowSecondInput = false;
            this.StartTimeTimeEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.StartTimeTimeEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.StartTimeTimeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartTimeTimeEdit.Properties.DisplayFormat.FormatString = "HH:mm";
            this.StartTimeTimeEdit.Properties.EditFormat.FormatString = "HH:mm";
            this.StartTimeTimeEdit.Properties.Mask.EditMask = "HH:mm";
            this.StartTimeTimeEdit.Properties.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.DoubleClick;
            this.StartTimeTimeEdit.Properties.Tag = "";
            this.StartTimeTimeEdit.Size = new System.Drawing.Size(90, 20);
            this.StartTimeTimeEdit.TabIndex = 23;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(161, 58);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(47, 13);
            this.labelControl4.TabIndex = 5;
            this.labelControl4.Text = "End Time:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(8, 58);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(53, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Start Time:";
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(215, 25);
            this.dateEditEnd.MenuManager = this.barManager1;
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditEnd.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditEnd.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditEnd.Size = new System.Drawing.Size(90, 20);
            this.dateEditEnd.TabIndex = 3;
            // 
            // dateEditStart
            // 
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(65, 25);
            this.dateEditStart.MenuManager = this.barManager1;
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditStart.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditStart.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditStart.Size = new System.Drawing.Size(90, 20);
            this.dateEditStart.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(161, 28);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(48, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "End Date:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(7, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(54, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Start Date:";
            // 
            // btnDateRangeFilterOK
            // 
            this.btnDateRangeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeFilterOK.Location = new System.Drawing.Point(3, 91);
            this.btnDateRangeFilterOK.Name = "btnDateRangeFilterOK";
            this.btnDateRangeFilterOK.Size = new System.Drawing.Size(38, 23);
            this.btnDateRangeFilterOK.TabIndex = 2;
            this.btnDateRangeFilterOK.Text = "OK";
            this.btnDateRangeFilterOK.Click += new System.EventHandler(this.btnDateRangeFilterOK_Click);
            // 
            // mapControl1
            // 
            this.mapControl1.CenterPoint = new DevExpress.XtraMap.GeoPoint(51.63060168D, 0.41083011D);
            this.mapControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapControl1.ImageList = this.imageCollection2;
            this.mapControl1.Layers.Add(this.imageTilesLayer1);
            this.mapControl1.Layers.Add(this.vectorItemsLayer1);
            this.mapControl1.Location = new System.Drawing.Point(378, 34);
            this.mapControl1.Name = "mapControl1";
            this.mapControl1.Size = new System.Drawing.Size(572, 469);
            this.mapControl1.TabIndex = 0;
            this.mapControl1.ToolTipController = this.toolTipController1;
            this.mapControl1.ZoomLevel = 12D;
            this.mapControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.mapControl1_MouseUp);
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageSize = new System.Drawing.Size(32, 37);
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            this.imageCollection2.Images.SetKeyName(0, "small_red_0.png");
            this.imageCollection2.Images.SetKeyName(1, "small_green_0.png");
            this.imageCollection2.Images.SetKeyName(2, "small_orange_0.png");
            this.imageCollection2.Images.SetKeyName(3, "small_light_blue_0.png");
            // 
            // toolTipController1
            // 
            this.toolTipController1.AllowHtmlText = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(425, 134);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemDateRange),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemPingType),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefreshTeams)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // barEditItemDateRange
            // 
            this.barEditItemDateRange.Caption = "Date Range Filter";
            this.barEditItemDateRange.Edit = this.repositoryItemPopupContainerEditDateRangeFilter;
            this.barEditItemDateRange.EditWidth = 232;
            this.barEditItemDateRange.Id = 36;
            this.barEditItemDateRange.Name = "barEditItemDateRange";
            this.barEditItemDateRange.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEditDateRangeFilter
            // 
            this.repositoryItemPopupContainerEditDateRangeFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateRangeFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateRangeFilter.Name = "repositoryItemPopupContainerEditDateRangeFilter";
            this.repositoryItemPopupContainerEditDateRangeFilter.PopupControl = this.popupContainerControlDateRangeFilter;
            this.repositoryItemPopupContainerEditDateRangeFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditDateRangeFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateRangeFilter_QueryResultValue);
            // 
            // barEditItemPingType
            // 
            this.barEditItemPingType.Caption = "Ping Type:";
            this.barEditItemPingType.Edit = this.repositoryItemPopupContainerEditPingType;
            this.barEditItemPingType.EditValue = "No Ping Type Filter";
            this.barEditItemPingType.EditWidth = 147;
            this.barEditItemPingType.Id = 37;
            this.barEditItemPingType.Name = "barEditItemPingType";
            this.barEditItemPingType.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEditPingType
            // 
            this.repositoryItemPopupContainerEditPingType.AutoHeight = false;
            this.repositoryItemPopupContainerEditPingType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditPingType.Name = "repositoryItemPopupContainerEditPingType";
            this.repositoryItemPopupContainerEditPingType.PopupControl = this.popupContainerControlPingTypes;
            this.repositoryItemPopupContainerEditPingType.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditPingType.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditPingType_QueryResultValue);
            // 
            // popupContainerControlPingTypes
            // 
            this.popupContainerControlPingTypes.Controls.Add(this.gridControl3);
            this.popupContainerControlPingTypes.Controls.Add(this.btnPingTypeOK);
            this.popupContainerControlPingTypes.Location = new System.Drawing.Point(387, 179);
            this.popupContainerControlPingTypes.Name = "popupContainerControlPingTypes";
            this.popupContainerControlPingTypes.Size = new System.Drawing.Size(148, 145);
            this.popupContainerControlPingTypes.TabIndex = 11;
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp06125OMOperativeLocationPingTypeFilterBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(2, 2);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.gridControl3.Size = new System.Drawing.Size(144, 114);
            this.gridControl3.TabIndex = 4;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp06125OMOperativeLocationPingTypeFilterBindingSource
            // 
            this.sp06125OMOperativeLocationPingTypeFilterBindingSource.DataMember = "sp06125_OM_Operative_Location_Ping_Type_Filter";
            this.sp06125OMOperativeLocationPingTypeFilterBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPingTypeID,
            this.colPingTypeText,
            this.colDescription,
            this.colRecordOrder});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colPingTypeID
            // 
            this.colPingTypeID.Caption = "Ping Type ID";
            this.colPingTypeID.FieldName = "PingTypeID";
            this.colPingTypeID.Name = "colPingTypeID";
            this.colPingTypeID.OptionsColumn.AllowEdit = false;
            this.colPingTypeID.OptionsColumn.AllowFocus = false;
            this.colPingTypeID.OptionsColumn.ReadOnly = true;
            this.colPingTypeID.Width = 82;
            // 
            // colPingTypeText
            // 
            this.colPingTypeText.Caption = "Ping Type Text";
            this.colPingTypeText.FieldName = "PingTypeText";
            this.colPingTypeText.Name = "colPingTypeText";
            this.colPingTypeText.OptionsColumn.AllowEdit = false;
            this.colPingTypeText.OptionsColumn.AllowFocus = false;
            this.colPingTypeText.OptionsColumn.ReadOnly = true;
            this.colPingTypeText.Width = 93;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Ping Type";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 103;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // btnPingTypeOK
            // 
            this.btnPingTypeOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPingTypeOK.Location = new System.Drawing.Point(3, 119);
            this.btnPingTypeOK.Name = "btnPingTypeOK";
            this.btnPingTypeOK.Size = new System.Drawing.Size(38, 23);
            this.btnPingTypeOK.TabIndex = 2;
            this.btnPingTypeOK.Text = "OK";
            this.btnPingTypeOK.Click += new System.EventHandler(this.btnPingTypeOK_Click);
            // 
            // bbiRefreshTeams
            // 
            this.bbiRefreshTeams.Caption = "Refresh Teams";
            this.bbiRefreshTeams.Id = 38;
            this.bbiRefreshTeams.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefreshTeams.ImageOptions.Image")));
            this.bbiRefreshTeams.Name = "bbiRefreshTeams";
            this.bbiRefreshTeams.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefreshTeams.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefreshTeams_ItemClick);
            // 
            // barEditItemMapType
            // 
            this.barEditItemMapType.Caption = "Map Type";
            this.barEditItemMapType.Edit = this.repositoryItemPopupContainerEditMapType;
            this.barEditItemMapType.EditValue = "Bing Hybrid";
            this.barEditItemMapType.EditWidth = 105;
            this.barEditItemMapType.Id = 33;
            this.barEditItemMapType.Name = "barEditItemMapType";
            // 
            // repositoryItemPopupContainerEditMapType
            // 
            this.repositoryItemPopupContainerEditMapType.AutoHeight = false;
            this.repositoryItemPopupContainerEditMapType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditMapType.Name = "repositoryItemPopupContainerEditMapType";
            this.repositoryItemPopupContainerEditMapType.PopupControl = this.popupContainerControlMapType;
            this.repositoryItemPopupContainerEditMapType.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditMapType_QueryResultValue);
            // 
            // repositoryItemRadioGroup1
            // 
            this.repositoryItemRadioGroup1.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Bing Hybid"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Bing Satellite"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Bing Road"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Open Street Map")});
            this.repositoryItemRadioGroup1.Name = "repositoryItemRadioGroup1";
            // 
            // sp06016_Get_Teams_From_LocationsTableAdapter
            // 
            this.sp06016_Get_Teams_From_LocationsTableAdapter.ClearBeforeFill = true;
            // 
            // dockManager1
            // 
            this.dockManager1.DockingOptions.HideImmediatelyOnAutoHide = true;
            this.dockManager1.DockModeVS2005FadeFramesCount = 1;
            this.dockManager1.DockModeVS2005FadeSpeed = 1;
            this.dockManager1.Form = this;
            this.dockManager1.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelLegend});
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanelLegend
            // 
            this.dockPanelLegend.Controls.Add(this.dockPanel2_Container);
            this.dockPanelLegend.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float;
            this.dockPanelLegend.FloatLocation = new System.Drawing.Point(1309, 186);
            this.dockPanelLegend.FloatSize = new System.Drawing.Size(107, 175);
            this.dockPanelLegend.ID = new System.Guid("1c195edf-82db-4106-a76a-5bb0ce5550c1");
            this.dockPanelLegend.Location = new System.Drawing.Point(-32768, -32768);
            this.dockPanelLegend.Name = "dockPanelLegend";
            this.dockPanelLegend.Options.ShowCloseButton = false;
            this.dockPanelLegend.Options.ShowMaximizeButton = false;
            this.dockPanelLegend.OriginalSize = new System.Drawing.Size(109, 200);
            this.dockPanelLegend.SavedIndex = 1;
            this.dockPanelLegend.Size = new System.Drawing.Size(107, 175);
            this.dockPanelLegend.Text = "Legend";
            this.dockPanelLegend.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this.dockPanelLegend.ClosingPanel += new DevExpress.XtraBars.Docking.DockPanelCancelEventHandler(this.dockPanelLegend_ClosingPanel);
            this.dockPanelLegend.DockChanged += new System.EventHandler(this.dockPanelLegend_DockChanged);
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Controls.Add(this.groupControl3);
            this.dockPanel2_Container.Location = new System.Drawing.Point(2, 28);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(103, 145);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.pictureEdit1);
            this.groupControl3.Controls.Add(this.labelControl5);
            this.groupControl3.Controls.Add(this.labelControl6);
            this.groupControl3.Controls.Add(this.pictureEdit4);
            this.groupControl3.Controls.Add(this.labelControl7);
            this.groupControl3.Controls.Add(this.pictureEdit6);
            this.groupControl3.Controls.Add(this.pictureEdit7);
            this.groupControl3.Controls.Add(this.labelControl8);
            this.groupControl3.Location = new System.Drawing.Point(1, 1);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(101, 143);
            this.groupControl3.TabIndex = 17;
            this.groupControl3.Text = "Ping Type";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(71, 111);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Size = new System.Drawing.Size(24, 28);
            this.pictureEdit1.TabIndex = 12;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(7, 117);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(48, 13);
            this.labelControl5.TabIndex = 6;
            this.labelControl5.Text = "Unknown:";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(7, 30);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(41, 13);
            this.labelControl6.TabIndex = 6;
            this.labelControl6.Text = "Off Site:";
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.EditValue = ((object)(resources.GetObject("pictureEdit4.EditValue")));
            this.pictureEdit4.Location = new System.Drawing.Point(71, 82);
            this.pictureEdit4.MenuManager = this.barManager1;
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Size = new System.Drawing.Size(24, 28);
            this.pictureEdit4.TabIndex = 14;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(7, 88);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(32, 13);
            this.labelControl7.TabIndex = 4;
            this.labelControl7.Text = "Other:";
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.EditValue = ((object)(resources.GetObject("pictureEdit6.EditValue")));
            this.pictureEdit6.Location = new System.Drawing.Point(71, 24);
            this.pictureEdit6.MenuManager = this.barManager1;
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit6.Size = new System.Drawing.Size(24, 28);
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Visits Not Started - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "This icon is only used for visits which have not been started. \r\n\r\nThe positionin" +
    "g on the map is taken from the parent Site\'s Latitude and Longitude.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.pictureEdit6.SuperTip = superToolTip2;
            this.pictureEdit6.TabIndex = 12;
            // 
            // pictureEdit7
            // 
            this.pictureEdit7.EditValue = ((object)(resources.GetObject("pictureEdit7.EditValue")));
            this.pictureEdit7.Location = new System.Drawing.Point(71, 53);
            this.pictureEdit7.MenuManager = this.barManager1;
            this.pictureEdit7.Name = "pictureEdit7";
            this.pictureEdit7.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit7.Size = new System.Drawing.Size(24, 28);
            this.pictureEdit7.TabIndex = 13;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(7, 59);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(39, 13);
            this.labelControl8.TabIndex = 2;
            this.labelControl8.Text = "On Site:";
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("23bf8cf4-eb0d-4e72-9053-e9fd8ce4a0d6");
            this.dockPanel1.Location = new System.Drawing.Point(0, 34);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(378, 200);
            this.dockPanel1.Size = new System.Drawing.Size(378, 469);
            this.dockPanel1.Text = "Teams";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.splitContainerControl1);
            this.dockPanel1_Container.Controls.Add(this.btnRefreshMap);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(371, 437);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.btnLoadPings);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl2);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Linked Team Pings";
            this.splitContainerControl1.Size = new System.Drawing.Size(371, 412);
            this.splitContainerControl1.SplitterPosition = 224;
            this.splitContainerControl1.TabIndex = 3;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // btnLoadPings
            // 
            this.btnLoadPings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLoadPings.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnLoadPings.Appearance.Options.UseFont = true;
            this.btnLoadPings.Enabled = false;
            this.btnLoadPings.Location = new System.Drawing.Point(0, 201);
            this.btnLoadPings.Name = "btnLoadPings";
            this.btnLoadPings.Size = new System.Drawing.Size(192, 23);
            this.btnLoadPings.TabIndex = 4;
            this.btnLoadPings.Text = "Load Selected Team Pings";
            this.btnLoadPings.Click += new System.EventHandler(this.btnLoadPings_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp06126GetOperativePingsBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEdit1});
            this.gridControl2.Size = new System.Drawing.Size(346, 179);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp06126GetOperativePingsBindingSource
            // 
            this.sp06126GetOperativePingsBindingSource.DataMember = "sp06126_Get_Operative_Pings";
            this.sp06126GetOperativePingsBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOperativeLocationID,
            this.colWhereaboutsID,
            this.colContractorID1,
            this.colLatitude1,
            this.colLongitude1,
            this.colDateRecorded,
            this.colName1,
            this.colPostcode1,
            this.colTelephone11,
            this.colTelephone21,
            this.colMobileTelephone1,
            this.colDisabled1,
            this.colInternalContractor1,
            this.colTextNumber1,
            this.colIsGritter1,
            this.colIsWoodPlanVisible1,
            this.colIsSnowClearer1,
            this.colIsUtilityArbTeam1,
            this.colThematicValue,
            this.colNearestPostcode,
            this.colAccuracy,
            this.colOS,
            this.colPingType,
            this.colPingTypeFriendly,
            this.colToken});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.FindDelay = 2000;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateRecorded, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colOperativeLocationID
            // 
            this.colOperativeLocationID.Caption = "Operative Location ID";
            this.colOperativeLocationID.FieldName = "OperativeLocationID";
            this.colOperativeLocationID.Name = "colOperativeLocationID";
            this.colOperativeLocationID.OptionsColumn.AllowEdit = false;
            this.colOperativeLocationID.OptionsColumn.AllowFocus = false;
            this.colOperativeLocationID.OptionsColumn.ReadOnly = true;
            this.colOperativeLocationID.Width = 126;
            // 
            // colWhereaboutsID
            // 
            this.colWhereaboutsID.Caption = "Whereabouts ID";
            this.colWhereaboutsID.FieldName = "WhereaboutsID";
            this.colWhereaboutsID.Name = "colWhereaboutsID";
            this.colWhereaboutsID.OptionsColumn.AllowEdit = false;
            this.colWhereaboutsID.OptionsColumn.AllowFocus = false;
            this.colWhereaboutsID.OptionsColumn.ReadOnly = true;
            this.colWhereaboutsID.Width = 100;
            // 
            // colContractorID1
            // 
            this.colContractorID1.Caption = "Contractor ID";
            this.colContractorID1.FieldName = "ContractorID";
            this.colContractorID1.Name = "colContractorID1";
            this.colContractorID1.OptionsColumn.AllowEdit = false;
            this.colContractorID1.OptionsColumn.AllowFocus = false;
            this.colContractorID1.OptionsColumn.ReadOnly = true;
            this.colContractorID1.Width = 87;
            // 
            // colLatitude1
            // 
            this.colLatitude1.Caption = "Latitude";
            this.colLatitude1.FieldName = "Latitude";
            this.colLatitude1.Name = "colLatitude1";
            this.colLatitude1.OptionsColumn.AllowEdit = false;
            this.colLatitude1.OptionsColumn.AllowFocus = false;
            this.colLatitude1.OptionsColumn.ReadOnly = true;
            this.colLatitude1.Visible = true;
            this.colLatitude1.VisibleIndex = 2;
            // 
            // colLongitude1
            // 
            this.colLongitude1.Caption = "Longitude";
            this.colLongitude1.FieldName = "Longitude";
            this.colLongitude1.Name = "colLongitude1";
            this.colLongitude1.OptionsColumn.AllowEdit = false;
            this.colLongitude1.OptionsColumn.AllowFocus = false;
            this.colLongitude1.OptionsColumn.ReadOnly = true;
            this.colLongitude1.Visible = true;
            this.colLongitude1.VisibleIndex = 3;
            // 
            // colDateRecorded
            // 
            this.colDateRecorded.Caption = "Date Recorded";
            this.colDateRecorded.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateRecorded.FieldName = "DateRecorded";
            this.colDateRecorded.Name = "colDateRecorded";
            this.colDateRecorded.OptionsColumn.AllowEdit = false;
            this.colDateRecorded.OptionsColumn.AllowFocus = false;
            this.colDateRecorded.OptionsColumn.ReadOnly = true;
            this.colDateRecorded.Visible = true;
            this.colDateRecorded.VisibleIndex = 0;
            this.colDateRecorded.Width = 106;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colName1
            // 
            this.colName1.Caption = "Name";
            this.colName1.FieldName = "Name";
            this.colName1.Name = "colName1";
            this.colName1.OptionsColumn.AllowEdit = false;
            this.colName1.OptionsColumn.AllowFocus = false;
            this.colName1.OptionsColumn.ReadOnly = true;
            this.colName1.Visible = true;
            this.colName1.VisibleIndex = 4;
            this.colName1.Width = 196;
            // 
            // colPostcode1
            // 
            this.colPostcode1.Caption = "Postcode";
            this.colPostcode1.FieldName = "Postcode";
            this.colPostcode1.Name = "colPostcode1";
            this.colPostcode1.OptionsColumn.AllowEdit = false;
            this.colPostcode1.OptionsColumn.AllowFocus = false;
            this.colPostcode1.OptionsColumn.ReadOnly = true;
            // 
            // colTelephone11
            // 
            this.colTelephone11.Caption = "Telephone 1";
            this.colTelephone11.FieldName = "Telephone1";
            this.colTelephone11.Name = "colTelephone11";
            this.colTelephone11.OptionsColumn.AllowEdit = false;
            this.colTelephone11.OptionsColumn.AllowFocus = false;
            this.colTelephone11.OptionsColumn.ReadOnly = true;
            this.colTelephone11.Width = 80;
            // 
            // colTelephone21
            // 
            this.colTelephone21.Caption = "Telephone 2";
            this.colTelephone21.FieldName = "Telephone2";
            this.colTelephone21.Name = "colTelephone21";
            this.colTelephone21.OptionsColumn.AllowEdit = false;
            this.colTelephone21.OptionsColumn.AllowFocus = false;
            this.colTelephone21.OptionsColumn.ReadOnly = true;
            this.colTelephone21.Width = 80;
            // 
            // colMobileTelephone1
            // 
            this.colMobileTelephone1.Caption = "Mobile Tel";
            this.colMobileTelephone1.FieldName = "MobileTelephone";
            this.colMobileTelephone1.Name = "colMobileTelephone1";
            this.colMobileTelephone1.OptionsColumn.AllowEdit = false;
            this.colMobileTelephone1.OptionsColumn.AllowFocus = false;
            this.colMobileTelephone1.OptionsColumn.ReadOnly = true;
            // 
            // colDisabled1
            // 
            this.colDisabled1.Caption = "Disabled";
            this.colDisabled1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDisabled1.FieldName = "Disabled";
            this.colDisabled1.Name = "colDisabled1";
            this.colDisabled1.OptionsColumn.AllowEdit = false;
            this.colDisabled1.OptionsColumn.AllowFocus = false;
            this.colDisabled1.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colInternalContractor1
            // 
            this.colInternalContractor1.Caption = "Internal Contractor";
            this.colInternalContractor1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colInternalContractor1.FieldName = "InternalContractor";
            this.colInternalContractor1.Name = "colInternalContractor1";
            this.colInternalContractor1.OptionsColumn.AllowEdit = false;
            this.colInternalContractor1.OptionsColumn.AllowFocus = false;
            this.colInternalContractor1.OptionsColumn.ReadOnly = true;
            this.colInternalContractor1.Width = 114;
            // 
            // colTextNumber1
            // 
            this.colTextNumber1.Caption = "Text Number";
            this.colTextNumber1.FieldName = "TextNumber";
            this.colTextNumber1.Name = "colTextNumber1";
            this.colTextNumber1.OptionsColumn.AllowEdit = false;
            this.colTextNumber1.OptionsColumn.AllowFocus = false;
            this.colTextNumber1.OptionsColumn.ReadOnly = true;
            this.colTextNumber1.Width = 83;
            // 
            // colIsGritter1
            // 
            this.colIsGritter1.Caption = "Is Gritter";
            this.colIsGritter1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsGritter1.FieldName = "IsGritter";
            this.colIsGritter1.Name = "colIsGritter1";
            this.colIsGritter1.OptionsColumn.AllowEdit = false;
            this.colIsGritter1.OptionsColumn.AllowFocus = false;
            this.colIsGritter1.OptionsColumn.ReadOnly = true;
            // 
            // colIsWoodPlanVisible1
            // 
            this.colIsWoodPlanVisible1.Caption = "WoodPlan";
            this.colIsWoodPlanVisible1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsWoodPlanVisible1.FieldName = "IsWoodPlanVisible";
            this.colIsWoodPlanVisible1.Name = "colIsWoodPlanVisible1";
            this.colIsWoodPlanVisible1.OptionsColumn.AllowEdit = false;
            this.colIsWoodPlanVisible1.OptionsColumn.AllowFocus = false;
            this.colIsWoodPlanVisible1.OptionsColumn.ReadOnly = true;
            // 
            // colIsSnowClearer1
            // 
            this.colIsSnowClearer1.Caption = "Snow Clearer";
            this.colIsSnowClearer1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsSnowClearer1.FieldName = "IsSnowClearer";
            this.colIsSnowClearer1.Name = "colIsSnowClearer1";
            this.colIsSnowClearer1.OptionsColumn.AllowEdit = false;
            this.colIsSnowClearer1.OptionsColumn.AllowFocus = false;
            this.colIsSnowClearer1.OptionsColumn.ReadOnly = true;
            this.colIsSnowClearer1.Width = 85;
            // 
            // colIsUtilityArbTeam1
            // 
            this.colIsUtilityArbTeam1.Caption = "Utility ARB";
            this.colIsUtilityArbTeam1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsUtilityArbTeam1.FieldName = "IsUtilityArbTeam";
            this.colIsUtilityArbTeam1.Name = "colIsUtilityArbTeam1";
            this.colIsUtilityArbTeam1.OptionsColumn.AllowEdit = false;
            this.colIsUtilityArbTeam1.OptionsColumn.AllowFocus = false;
            this.colIsUtilityArbTeam1.OptionsColumn.ReadOnly = true;
            // 
            // colThematicValue
            // 
            this.colThematicValue.Caption = "Thematic Value";
            this.colThematicValue.FieldName = "ThematicValue";
            this.colThematicValue.Name = "colThematicValue";
            this.colThematicValue.OptionsColumn.AllowEdit = false;
            this.colThematicValue.OptionsColumn.AllowFocus = false;
            this.colThematicValue.OptionsColumn.ReadOnly = true;
            this.colThematicValue.Width = 93;
            // 
            // colNearestPostcode
            // 
            this.colNearestPostcode.Caption = "Nearest Postcode";
            this.colNearestPostcode.FieldName = "NearestPostcode";
            this.colNearestPostcode.Name = "colNearestPostcode";
            this.colNearestPostcode.OptionsColumn.AllowEdit = false;
            this.colNearestPostcode.OptionsColumn.AllowFocus = false;
            this.colNearestPostcode.OptionsColumn.ReadOnly = true;
            this.colNearestPostcode.Visible = true;
            this.colNearestPostcode.VisibleIndex = 4;
            this.colNearestPostcode.Width = 106;
            // 
            // colAccuracy
            // 
            this.colAccuracy.Caption = "Accuracy";
            this.colAccuracy.ColumnEdit = this.repositoryItemTextEdit1;
            this.colAccuracy.FieldName = "Accuracy";
            this.colAccuracy.Name = "colAccuracy";
            this.colAccuracy.OptionsColumn.AllowEdit = false;
            this.colAccuracy.OptionsColumn.AllowFocus = false;
            this.colAccuracy.OptionsColumn.ReadOnly = true;
            this.colAccuracy.Visible = true;
            this.colAccuracy.VisibleIndex = 5;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "f";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colOS
            // 
            this.colOS.Caption = "OS";
            this.colOS.FieldName = "OS";
            this.colOS.Name = "colOS";
            this.colOS.OptionsColumn.AllowEdit = false;
            this.colOS.OptionsColumn.AllowFocus = false;
            this.colOS.OptionsColumn.ReadOnly = true;
            this.colOS.Visible = true;
            this.colOS.VisibleIndex = 6;
            // 
            // colPingType
            // 
            this.colPingType.Caption = "Ping Type Code";
            this.colPingType.FieldName = "PingType";
            this.colPingType.Name = "colPingType";
            this.colPingType.OptionsColumn.AllowEdit = false;
            this.colPingType.OptionsColumn.AllowFocus = false;
            this.colPingType.OptionsColumn.ReadOnly = true;
            this.colPingType.Width = 96;
            // 
            // colPingTypeFriendly
            // 
            this.colPingTypeFriendly.Caption = "Ping Type";
            this.colPingTypeFriendly.FieldName = "PingTypeFriendly";
            this.colPingTypeFriendly.Name = "colPingTypeFriendly";
            this.colPingTypeFriendly.OptionsColumn.AllowEdit = false;
            this.colPingTypeFriendly.OptionsColumn.AllowFocus = false;
            this.colPingTypeFriendly.OptionsColumn.ReadOnly = true;
            this.colPingTypeFriendly.Visible = true;
            this.colPingTypeFriendly.VisibleIndex = 1;
            this.colPingTypeFriendly.Width = 68;
            // 
            // colToken
            // 
            this.colToken.Caption = "Token";
            this.colToken.FieldName = "Token";
            this.colToken.Name = "colToken";
            this.colToken.OptionsColumn.AllowEdit = false;
            this.colToken.OptionsColumn.AllowFocus = false;
            this.colToken.OptionsColumn.ReadOnly = true;
            this.colToken.Visible = true;
            this.colToken.VisibleIndex = 7;
            this.colToken.Width = 170;
            // 
            // btnRefreshMap
            // 
            this.btnRefreshMap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRefreshMap.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnRefreshMap.Appearance.Options.UseFont = true;
            this.btnRefreshMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRefreshMap.ImageOptions.Image")));
            this.btnRefreshMap.Location = new System.Drawing.Point(0, 414);
            this.btnRefreshMap.Name = "btnRefreshMap";
            this.btnRefreshMap.Size = new System.Drawing.Size(192, 23);
            this.btnRefreshMap.TabIndex = 1;
            this.btnRefreshMap.Text = "Load Selected Pings into Map";
            this.btnRefreshMap.Click += new System.EventHandler(this.btnRefreshMap_Click);
            // 
            // sp06017_Get_Operative_LocationsTableAdapter
            // 
            this.sp06017_Get_Operative_LocationsTableAdapter.ClearBeforeFill = true;
            // 
            // pmMapItemMenu
            // 
            this.pmMapItemMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewObjectOnMap),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExportToExcel, true)});
            this.pmMapItemMenu.Manager = this.barManager1;
            this.pmMapItemMenu.Name = "pmMapItemMenu";
            // 
            // bbiViewObjectOnMap
            // 
            this.bbiViewObjectOnMap.Caption = "View Object On Map";
            this.bbiViewObjectOnMap.Id = 34;
            this.bbiViewObjectOnMap.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ScaleLarge;
            this.bbiViewObjectOnMap.Name = "bbiViewObjectOnMap";
            this.bbiViewObjectOnMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewObjectOnMap_ItemClick);
            // 
            // bbiExportToExcel
            // 
            this.bbiExportToExcel.Caption = "Export To Excel";
            this.bbiExportToExcel.Id = 39;
            this.bbiExportToExcel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiExportToExcel.ImageOptions.Image")));
            this.bbiExportToExcel.Name = "bbiExportToExcel";
            this.bbiExportToExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExportToExcel_ItemClick);
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 3";
            this.bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemMapType),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciLegend, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiMapObjectCount, true)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Custom 3";
            // 
            // bciLegend
            // 
            this.bciLegend.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.bciLegend.Caption = "Legend";
            this.bciLegend.Id = 41;
            this.bciLegend.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciLegend.ImageOptions.Image")));
            this.bciLegend.Name = "bciLegend";
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Legend - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to Show \\ Hide the Map Legend.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bciLegend.SuperTip = superToolTip1;
            this.bciLegend.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciLegend_CheckedChanged);
            // 
            // bsiMapObjectCount
            // 
            this.bsiMapObjectCount.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.bsiMapObjectCount.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.bsiMapObjectCount.Caption = "Map Object Count: 0";
            this.bsiMapObjectCount.Id = 35;
            this.bsiMapObjectCount.Name = "bsiMapObjectCount";
            this.bsiMapObjectCount.Size = new System.Drawing.Size(150, 0);
            this.bsiMapObjectCount.Width = 150;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // sp06125_OM_Operative_Location_Ping_Type_FilterTableAdapter
            // 
            this.sp06125_OM_Operative_Location_Ping_Type_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06126_Get_Operative_PingsTableAdapter
            // 
            this.sp06126_Get_Operative_PingsTableAdapter.ClearBeforeFill = true;
            // 
            // bbiMapPrintPreview
            // 
            this.bbiMapPrintPreview.Caption = "Print Preview";
            this.bbiMapPrintPreview.Id = 40;
            this.bbiMapPrintPreview.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiMapPrintPreview.ImageOptions.Image")));
            this.bbiMapPrintPreview.Name = "bbiMapPrintPreview";
            this.bbiMapPrintPreview.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiMapPrintPreview_ItemClick);
            // 
            // pmMapControl
            // 
            this.pmMapControl.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiMapPrintPreview)});
            this.pmMapControl.Manager = this.barManager1;
            this.pmMapControl.MenuCaption = "Map Menu";
            this.pmMapControl.Name = "pmMapControl";
            this.pmMapControl.ShowCaption = true;
            // 
            // frm_Operatives_Location
            // 
            this.ClientSize = new System.Drawing.Size(950, 537);
            this.Controls.Add(this.popupContainerControlPingTypes);
            this.Controls.Add(this.popupContainerControlMapType);
            this.Controls.Add(this.popupContainerControlDateRangeFilter);
            this.Controls.Add(this.mapControl1);
            this.Controls.Add(this.dockPanel1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Operatives_Location";
            this.Text = "Operatives Location";
            this.Activated += new System.EventHandler(this.frm_Operatives_Location_Activated);
            this.Deactivate += new System.EventHandler(this.frm_Operatives_Location_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Operatives_Location_FormClosing);
            this.Load += new System.EventHandler(this.frm_Operatives_Location_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dockPanel1, 0);
            this.Controls.SetChildIndex(this.mapControl1, 0);
            this.Controls.SetChildIndex(this.popupContainerControlDateRangeFilter, 0);
            this.Controls.SetChildIndex(this.popupContainerControlMapType, 0);
            this.Controls.SetChildIndex(this.popupContainerControlPingTypes, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06017GetOperativeLocationsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Summer_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06016GetTeamsFromLocationsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlMapType)).EndInit();
            this.popupContainerControlMapType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlMapType)).EndInit();
            this.groupControlMapType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRangeFilter)).EndInit();
            this.popupContainerControlDateRangeFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EndTimeTimeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartTimeTimeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRangeFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditPingType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlPingTypes)).EndInit();
            this.popupContainerControlPingTypes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06125OMOperativeLocationPingTypeFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditMapType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanelLegend.ResumeLayout(false);
            this.dockPanel2_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06126GetOperativePingsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmMapItemMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmMapControl)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BaseObjects.ExtendedMapControl mapControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup1;
        private DevExpress.XtraBars.BarEditItem barEditItemMapType;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditMapType;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRangeFilter;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeFilterOK;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private BaseObjects.TimeSpanEdit EndTimeTimeEdit;
        private BaseObjects.TimeSpanEdit StartTimeTimeEdit;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlMapType;
        private DevExpress.XtraEditors.GroupControl groupControlMapType;
        private DevExpress.XtraEditors.SimpleButton btnOKMapType;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private System.Windows.Forms.BindingSource sp06016GetTeamsFromLocationsBindingSource;
        private DataSet_GC_Summer_Core dataSet_GC_Summer_Core;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colMobileTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colTextNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colIsGritter;
        private DevExpress.XtraGrid.Columns.GridColumn colIsWoodPlanVisible;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSnowClearer;
        private DevExpress.XtraGrid.Columns.GridColumn colIsUtilityArbTeam;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationCount;
        private DataSet_GC_Summer_CoreTableAdapters.sp06016_Get_Teams_From_LocationsTableAdapter sp06016_Get_Teams_From_LocationsTableAdapter;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private System.Windows.Forms.BindingSource sp06017GetOperativeLocationsBindingSource;
        private DataSet_GC_Summer_CoreTableAdapters.sp06017_Get_Operative_LocationsTableAdapter sp06017_Get_Operative_LocationsTableAdapter;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraBars.PopupMenu pmMapItemMenu;
        private DevExpress.XtraBars.BarButtonItem bbiViewObjectOnMap;
        private DevExpress.XtraEditors.SimpleButton btnRefreshMap;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarStaticItem bsiMapObjectCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colOperativeLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colWhereaboutsID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID1;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRecorded;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colName1;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone11;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone21;
        private DevExpress.XtraGrid.Columns.GridColumn colMobileTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalContractor1;
        private DevExpress.XtraGrid.Columns.GridColumn colTextNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colIsGritter1;
        private DevExpress.XtraGrid.Columns.GridColumn colIsWoodPlanVisible1;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSnowClearer1;
        private DevExpress.XtraGrid.Columns.GridColumn colIsUtilityArbTeam1;
        private DevExpress.XtraGrid.Columns.GridColumn colThematicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colNearestPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colAccuracy;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colOS;
        private DevExpress.XtraGrid.Columns.GridColumn colPingType;
        private DevExpress.XtraGrid.Columns.GridColumn colPingTypeFriendly;
        private DevExpress.XtraGrid.Columns.GridColumn colToken;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraBars.BarEditItem barEditItemDateRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateRangeFilter;
        private DevExpress.XtraBars.BarEditItem barEditItemPingType;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditPingType;
        private DevExpress.XtraBars.BarButtonItem bbiRefreshTeams;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlPingTypes;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.SimpleButton btnPingTypeOK;
        private System.Windows.Forms.BindingSource sp06125OMOperativeLocationPingTypeFilterBindingSource;
        private DataSet_GC_Summer_CoreTableAdapters.sp06125_OM_Operative_Location_Ping_Type_FilterTableAdapter sp06125_OM_Operative_Location_Ping_Type_FilterTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colPingTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPingTypeText;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraEditors.SimpleButton btnLoadPings;
        private System.Windows.Forms.BindingSource sp06126GetOperativePingsBindingSource;
        private DataSet_GC_Summer_CoreTableAdapters.sp06126_Get_Operative_PingsTableAdapter sp06126_Get_Operative_PingsTableAdapter;
        private DevExpress.XtraBars.BarButtonItem bbiExportToExcel;
        private DevExpress.XtraBars.BarButtonItem bbiMapPrintPreview;
        private DevExpress.XtraBars.PopupMenu pmMapControl;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.XtraBars.BarCheckItem bciLegend;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelLegend;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraEditors.PictureEdit pictureEdit7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraMap.ImageTilesLayer imageTilesLayer1;
        private DevExpress.XtraMap.BingMapDataProvider bingMapDataProvider1;
        private DevExpress.XtraMap.VectorItemsLayer vectorItemsLayer1;
        private DevExpress.XtraMap.ListSourceDataAdapter listSourceDataAdapter1;
    }
}

﻿namespace WoodPlan5
{
    partial class frm_OM_Visit_Wizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule9 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue9 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule10 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue10 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition5 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Visit_Wizard));
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule11 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue11 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule12 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue12 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule6 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue6 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition6 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraScheduler.TimeRuler timeRuler3 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraScheduler.TimeRuler timeRuler4 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.Animation.Transition transition2 = new DevExpress.Utils.Animation.Transition();
            this.colActive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colActive3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colOnHold = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn129 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn126 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colOnHold1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageWelcome = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.btnWelcomeNext = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.xtraTabPageStep1 = new DevExpress.XtraTab.XtraTabPage();
            this.labelControlSelectedClientCount = new DevExpress.XtraEditors.LabelControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.btnDateRangeOK = new DevExpress.XtraEditors.SimpleButton();
            this.standaloneBarDockControl3 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.btnStep1Previous = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep1Next = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp06131OMVisitWizardClientContractsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDirectorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colYearlyPercentageIncrease = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCheckRPIDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastClientPaymentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPreviousContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDirector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientYearCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageStep2 = new DevExpress.XtraTab.XtraTabPage();
            this.labelControlSelectedSiteCount = new DevExpress.XtraEditors.LabelControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp06132OMVisitWizardSiteContractsForClientContractBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteContractID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractValue2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colYearlyPercentageIncrease3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLastClientPaymentDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVisitNumberMax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultScheduleTemplateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditDefaultScheduleTemplateID = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06141OMVisitTemplateHeadersWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldReasonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractDaysSeparationPercent2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.btnStep2Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep2Previous = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPageStep3 = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.splitContainerControl7 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp06142OMVisitTemplateHeadersSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTemplateHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedItemCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl18 = new DevExpress.XtraGrid.GridControl();
            this.sp06143OMVisitTemplateItemsSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView18 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTemplateItemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTemplateHeaderID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitsFromStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitsFromStartDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCalculationLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit16 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colUnitsFromStartDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCalculationLevelDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCategoryID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDurationUnitsDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDurationUnitsDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellCalculationLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellCalculationLevelDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControlOccurrences = new DevExpress.XtraEditors.LabelControl();
            this.spinEditOccurrenceCount = new DevExpress.XtraEditors.SpinEdit();
            this.radioGroupEndType = new DevExpress.XtraEditors.RadioGroup();
            this.dateEditRecurrenceEnd = new DevExpress.XtraEditors.DateEdit();
            this.yearlyRecurrenceControl1 = new DevExpress.XtraScheduler.UI.YearlyRecurrenceControl();
            this.weeklyRecurrenceControl1 = new DevExpress.XtraScheduler.UI.WeeklyRecurrenceControl();
            this.monthlyRecurrenceControl1 = new DevExpress.XtraScheduler.UI.MonthlyRecurrenceControl();
            this.dailyRecurrenceControl1 = new DevExpress.XtraScheduler.UI.DailyRecurrenceControl();
            this.radioGroupRecurrence = new DevExpress.XtraEditors.RadioGroup();
            this.labelControlDefaultScheduleTemplateUnavailable = new DevExpress.XtraEditors.LabelControl();
            this.labelControlNumberOfVisits = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditSellCalculationLevel = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06134OMJobCalculationLevelDescriptorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControlSellCalculationLevel = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditCostCalculationLevel = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView17 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn130 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn131 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControlCostCalculationLevel = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupNumberOfVisitsMethod = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditGapBetweenVisitsDescriptor = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06133OMVisitGapUnitDescriptorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn127 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn128 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.spinEditGapBetweenVisitsUnits = new DevExpress.XtraEditors.SpinEdit();
            this.labelControlGapBetweenVisits = new DevExpress.XtraEditors.LabelControl();
            this.spinEditNumberOfVisits = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.btnStep3Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep3Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.SelectedSiteContractCountTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.pictureEdit10 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageStep4 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonApplyDate1 = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditApplyDate1 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp06144OMVisitWizardSiteContractStartDatesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn132 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldReasonID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldReason1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractDaysSeparationPercent1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnStep4Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep4Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit7 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageStep5 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.btnApplyVisitNumber = new DevExpress.XtraEditors.SimpleButton();
            this.VisitNumberSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.VisitTimeEdit = new DevExpress.XtraEditors.TimeSpanEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.VisitDescriptorComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.VisitDurationSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.btnApplyVisitDuration = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.btnCalculateVisitDaysSeparation = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.VisitDaysSeparationSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.btnApplyVisitDaysSeparation = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage5GridView = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp06145OMVisitEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colstrMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRecordIDs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditVisitNumber = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colExpectedEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCalculationLevel1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditCostCalculationLevel = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVisitSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colStartLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditFloat = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colStartLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalendarLabel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabelColourID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVisitCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditVisitCategory = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06250OMVisitCategoriesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditVisitTypes = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06308OMVisitTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditClientPONumber = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colSellCalculationLevel1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedFromVisitTemplateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysSeparation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditDaysSeparation = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colSiteContractDaysSeparationPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage5Calendar = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.dateNavigator1 = new DevExpress.XtraScheduler.DateNavigator();
            this.schedulerControl1 = new DevExpress.XtraScheduler.SchedulerControl();
            this.schedulerStorage1 = new DevExpress.XtraScheduler.SchedulerStorage(this.components);
            this.resourcesCheckedListBoxControl1 = new DevExpress.XtraScheduler.UI.ResourcesCheckedListBoxControl();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.btnStep5Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep5Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit8 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageFinish = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.btnFinish = new DevExpress.XtraEditors.SimpleButton();
            this.btnFinishPrevious = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemButtonEditChooseStaff = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.barStep1 = new DevExpress.XtraBars.Bar();
            this.barEditItemDateRange = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateRange = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barEditItemActive = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.bbiRefreshClientContracts = new DevExpress.XtraBars.BarButtonItem();
            this.sp06131_OM_Visit_Wizard_Client_ContractsTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06131_OM_Visit_Wizard_Client_ContractsTableAdapter();
            this.sp06132_OM_Visit_Wizard_Site_Contracts_For_Client_ContractTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06132_OM_Visit_Wizard_Site_Contracts_For_Client_ContractTableAdapter();
            this.sp06133_OM_Visit_Gap_Unit_DescriptorsTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06133_OM_Visit_Gap_Unit_DescriptorsTableAdapter();
            this.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter();
            this.sp06142_OM_Visit_Template_Headers_SelectTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06142_OM_Visit_Template_Headers_SelectTableAdapter();
            this.sp06143_OM_Visit_Template_Items_SelectTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06143_OM_Visit_Template_Items_SelectTableAdapter();
            this.sp06145_OM_Visit_EditTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06145_OM_Visit_EditTableAdapter();
            this.schedulerBarController1 = new DevExpress.XtraScheduler.UI.SchedulerBarController(this.components);
            this.printPreviewItem1 = new DevExpress.XtraScheduler.UI.PrintPreviewItem();
            this.printItem1 = new DevExpress.XtraScheduler.UI.PrintItem();
            this.printPageSetupItem1 = new DevExpress.XtraScheduler.UI.PrintPageSetupItem();
            this.navigateViewBackwardItem1 = new DevExpress.XtraScheduler.UI.NavigateViewBackwardItem();
            this.navigateViewForwardItem1 = new DevExpress.XtraScheduler.UI.NavigateViewForwardItem();
            this.gotoTodayItem1 = new DevExpress.XtraScheduler.UI.GotoTodayItem();
            this.viewZoomInItem1 = new DevExpress.XtraScheduler.UI.ViewZoomInItem();
            this.viewZoomOutItem1 = new DevExpress.XtraScheduler.UI.ViewZoomOutItem();
            this.switchToDayViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToDayViewItem();
            this.switchToWorkWeekViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToWorkWeekViewItem();
            this.switchToWeekViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToWeekViewItem();
            this.switchToMonthViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToMonthViewItem();
            this.switchToTimelineViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToTimelineViewItem();
            this.switchToGanttViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToGanttViewItem();
            this.groupByNoneItem1 = new DevExpress.XtraScheduler.UI.GroupByNoneItem();
            this.groupByDateItem1 = new DevExpress.XtraScheduler.UI.GroupByDateItem();
            this.groupByResourceItem1 = new DevExpress.XtraScheduler.UI.GroupByResourceItem();
            this.switchTimeScalesItem1 = new DevExpress.XtraScheduler.UI.SwitchTimeScalesItem();
            this.changeScaleWidthItem1 = new DevExpress.XtraScheduler.UI.ChangeScaleWidthItem();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.switchTimeScalesCaptionItem1 = new DevExpress.XtraScheduler.UI.SwitchTimeScalesCaptionItem();
            this.switchCompressWeekendItem1 = new DevExpress.XtraScheduler.UI.SwitchCompressWeekendItem();
            this.switchShowWorkTimeOnlyItem1 = new DevExpress.XtraScheduler.UI.SwitchShowWorkTimeOnlyItem();
            this.switchCellsAutoHeightItem1 = new DevExpress.XtraScheduler.UI.SwitchCellsAutoHeightItem();
            this.changeSnapToCellsUIItem1 = new DevExpress.XtraScheduler.UI.ChangeSnapToCellsUIItem();
            this.editOccurrenceUICommandItem1 = new DevExpress.XtraScheduler.UI.EditOccurrenceUICommandItem();
            this.editSeriesUICommandItem1 = new DevExpress.XtraScheduler.UI.EditSeriesUICommandItem();
            this.deleteOccurrenceItem1 = new DevExpress.XtraScheduler.UI.DeleteOccurrenceItem();
            this.deleteSeriesItem1 = new DevExpress.XtraScheduler.UI.DeleteSeriesItem();
            this.printBar1 = new DevExpress.XtraScheduler.UI.PrintBar();
            this.navigatorBar1 = new DevExpress.XtraScheduler.UI.NavigatorBar();
            this.arrangeBar1 = new DevExpress.XtraScheduler.UI.ArrangeBar();
            this.groupByBar1 = new DevExpress.XtraScheduler.UI.GroupByBar();
            this.timeScaleBar1 = new DevExpress.XtraScheduler.UI.TimeScaleBar();
            this.layoutBar1 = new DevExpress.XtraScheduler.UI.LayoutBar();
            this.repositoryItemDuration1 = new DevExpress.XtraScheduler.UI.RepositoryItemDuration();
            this.sp06250_OM_Visit_Categories_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06250_OM_Visit_Categories_With_BlankTableAdapter();
            this.bsiRecordTicking = new DevExpress.XtraBars.BarSubItem();
            this.bbiTick = new DevExpress.XtraBars.BarButtonItem();
            this.bbiUntick = new DevExpress.XtraBars.BarButtonItem();
            this.sp06144_OM_Visit_Wizard_Site_Contract_Start_DatesTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06144_OM_Visit_Wizard_Site_Contract_Start_DatesTableAdapter();
            this.sp06308_OM_Visit_TypesTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06308_OM_Visit_TypesTableAdapter();
            this.transitionManager1 = new DevExpress.Utils.Animation.TransitionManager(this.components);
            this.sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter();
            this.timerWelcomePage = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageWelcome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.xtraTabPageStep1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06131OMVisitWizardClientContractsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).BeginInit();
            this.xtraTabPageStep2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06132OMVisitWizardSiteContractsForClientContractBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditDefaultScheduleTemplateID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06141OMVisitTemplateHeadersWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            this.xtraTabPageStep3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl7)).BeginInit();
            this.splitContainerControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06142OMVisitTemplateHeadersSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06143OMVisitTemplateItemsSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditOccurrenceCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupEndType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditRecurrenceEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditRecurrenceEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupRecurrence.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSellCalculationLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06134OMJobCalculationLevelDescriptorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditCostCalculationLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupNumberOfVisitsMethod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditGapBetweenVisitsDescriptor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06133OMVisitGapUnitDescriptorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditGapBetweenVisitsUnits.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditNumberOfVisits.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedSiteContractCountTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit10.Properties)).BeginInit();
            this.xtraTabPageStep4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditApplyDate1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditApplyDate1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06144OMVisitWizardSiteContractStartDatesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).BeginInit();
            this.xtraTabPageStep5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VisitTimeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitDescriptorComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitDurationSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VisitDaysSeparationSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPage5GridView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06145OMVisitEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditVisitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditCostCalculationLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditFloat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditVisitCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06250OMVisitCategoriesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditVisitTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06308OMVisitTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditClientPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditDaysSeparation)).BeginInit();
            this.xtraTabPage5Calendar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resourcesCheckedListBoxControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).BeginInit();
            this.xtraTabPageFinish.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseStaff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerBarController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiRecordTicking, true)});
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1234, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Size = new System.Drawing.Size(1234, 0);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1234, 0);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStep1,
            this.printBar1,
            this.navigatorBar1,
            this.arrangeBar1,
            this.groupByBar1,
            this.timeScaleBar1,
            this.layoutBar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl3);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Images = this.imageCollection1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barEditItemDateRange,
            this.bbiRefreshClientContracts,
            this.barEditItemActive,
            this.printPreviewItem1,
            this.printItem1,
            this.printPageSetupItem1,
            this.navigateViewBackwardItem1,
            this.navigateViewForwardItem1,
            this.gotoTodayItem1,
            this.viewZoomInItem1,
            this.viewZoomOutItem1,
            this.switchToDayViewItem1,
            this.switchToWorkWeekViewItem1,
            this.switchToWeekViewItem1,
            this.switchToMonthViewItem1,
            this.switchToTimelineViewItem1,
            this.switchToGanttViewItem1,
            this.groupByNoneItem1,
            this.groupByDateItem1,
            this.groupByResourceItem1,
            this.switchTimeScalesItem1,
            this.changeScaleWidthItem1,
            this.switchTimeScalesCaptionItem1,
            this.switchCompressWeekendItem1,
            this.switchShowWorkTimeOnlyItem1,
            this.switchCellsAutoHeightItem1,
            this.changeSnapToCellsUIItem1,
            this.editOccurrenceUICommandItem1,
            this.editSeriesUICommandItem1,
            this.deleteOccurrenceItem1,
            this.deleteSeriesItem1,
            this.bsiRecordTicking,
            this.bbiTick,
            this.bbiUntick});
            this.barManager1.MaxItemId = 94;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditDateRange,
            this.repositoryItemCheckEdit4,
            this.repositoryItemSpinEdit1,
            this.repositoryItemDuration1});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActive1
            // 
            this.colActive1.Caption = "Active";
            this.colActive1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive1.FieldName = "Active";
            this.colActive1.Name = "colActive1";
            this.colActive1.OptionsColumn.AllowEdit = false;
            this.colActive1.OptionsColumn.AllowFocus = false;
            this.colActive1.OptionsColumn.ReadOnly = true;
            this.colActive1.Visible = true;
            this.colActive1.VisibleIndex = 4;
            this.colActive1.Width = 51;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colActive3
            // 
            this.colActive3.Caption = "Active";
            this.colActive3.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colActive3.FieldName = "Active";
            this.colActive3.Name = "colActive3";
            this.colActive3.OptionsColumn.AllowEdit = false;
            this.colActive3.OptionsColumn.AllowFocus = false;
            this.colActive3.OptionsColumn.ReadOnly = true;
            this.colActive3.Visible = true;
            this.colActive3.VisibleIndex = 4;
            this.colActive3.Width = 51;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.ValueChecked = 1;
            this.repositoryItemCheckEdit5.ValueUnchecked = 0;
            // 
            // colOnHold
            // 
            this.colOnHold.Caption = "On-Hold";
            this.colOnHold.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colOnHold.FieldName = "OnHold";
            this.colOnHold.Name = "colOnHold";
            this.colOnHold.OptionsColumn.AllowEdit = false;
            this.colOnHold.OptionsColumn.AllowFocus = false;
            this.colOnHold.OptionsColumn.ReadOnly = true;
            this.colOnHold.Visible = true;
            this.colOnHold.VisibleIndex = 11;
            this.colOnHold.Width = 58;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "ID";
            this.gridColumn16.FieldName = "ID";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Width = 53;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "ID";
            this.gridColumn10.FieldName = "ID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 53;
            // 
            // gridColumn129
            // 
            this.gridColumn129.Caption = "ID";
            this.gridColumn129.FieldName = "ID";
            this.gridColumn129.Name = "gridColumn129";
            this.gridColumn129.OptionsColumn.AllowEdit = false;
            this.gridColumn129.OptionsColumn.AllowFocus = false;
            this.gridColumn129.OptionsColumn.ReadOnly = true;
            this.gridColumn129.Width = 53;
            // 
            // gridColumn126
            // 
            this.gridColumn126.Caption = "ID";
            this.gridColumn126.FieldName = "ID";
            this.gridColumn126.Name = "gridColumn126";
            this.gridColumn126.OptionsColumn.AllowEdit = false;
            this.gridColumn126.OptionsColumn.AllowFocus = false;
            this.gridColumn126.OptionsColumn.ReadOnly = true;
            this.gridColumn126.Width = 53;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Active";
            this.gridColumn6.ColumnEdit = this.repositoryItemCheckEdit6;
            this.gridColumn6.FieldName = "Active";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            this.gridColumn6.Width = 51;
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Caption = "Check";
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            this.repositoryItemCheckEdit6.ValueChecked = 1;
            this.repositoryItemCheckEdit6.ValueUnchecked = 0;
            // 
            // colOnHold1
            // 
            this.colOnHold1.Caption = "On-Hold";
            this.colOnHold1.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colOnHold1.FieldName = "OnHold";
            this.colOnHold1.Name = "colOnHold1";
            this.colOnHold1.OptionsColumn.AllowEdit = false;
            this.colOnHold1.OptionsColumn.AllowFocus = false;
            this.colOnHold1.OptionsColumn.ReadOnly = true;
            this.colOnHold1.Visible = true;
            this.colOnHold1.VisibleIndex = 7;
            this.colOnHold1.Width = 58;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.Width = 53;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "ID";
            this.gridColumn13.FieldName = "ID";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 53;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, -1);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
            this.xtraTabControl1.Size = new System.Drawing.Size(1238, 542);
            this.xtraTabControl1.TabIndex = 6;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageWelcome,
            this.xtraTabPageStep1,
            this.xtraTabPageStep2,
            this.xtraTabPageStep3,
            this.xtraTabPageStep4,
            this.xtraTabPageStep5,
            this.xtraTabPageFinish});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            this.xtraTabControl1.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.xtraTabControl1_SelectedPageChanging);
            // 
            // xtraTabPageWelcome
            // 
            this.xtraTabPageWelcome.Controls.Add(this.panelControl1);
            this.xtraTabPageWelcome.Controls.Add(this.btnWelcomeNext);
            this.xtraTabPageWelcome.Controls.Add(this.labelControl2);
            this.xtraTabPageWelcome.Controls.Add(this.pictureEdit1);
            this.xtraTabPageWelcome.Name = "xtraTabPageWelcome";
            this.xtraTabPageWelcome.Size = new System.Drawing.Size(1211, 537);
            this.xtraTabPageWelcome.Tag = "0";
            this.xtraTabPageWelcome.Text = "Welcome";
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.pictureEdit4);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Location = new System.Drawing.Point(211, 6);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(994, 48);
            this.panelControl1.TabIndex = 17;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit4.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit4.Location = new System.Drawing.Point(950, 4);
            this.pictureEdit4.MenuManager = this.barManager1;
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Properties.ReadOnly = true;
            this.pictureEdit4.Properties.ShowMenu = false;
            this.pictureEdit4.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit4.TabIndex = 16;
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(5, 1);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(319, 29);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "Welcome to the <b>Visit Wizard</b>";
            // 
            // labelControl9
            // 
            this.labelControl9.AllowHtmlString = true;
            this.labelControl9.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl9.Appearance.Options.UseBackColor = true;
            this.labelControl9.Location = new System.Drawing.Point(9, 31);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(425, 13);
            this.labelControl9.TabIndex = 15;
            this.labelControl9.Text = "This wizard will allow you to create one or more new Visits for one or more Site " +
    "Contracts\r\n";
            // 
            // btnWelcomeNext
            // 
            this.btnWelcomeNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWelcomeNext.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnWelcomeNext.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnWelcomeNext.Location = new System.Drawing.Point(1117, 501);
            this.btnWelcomeNext.Name = "btnWelcomeNext";
            this.btnWelcomeNext.Size = new System.Drawing.Size(88, 30);
            this.btnWelcomeNext.TabIndex = 7;
            this.btnWelcomeNext.Text = "Next";
            this.btnWelcomeNext.Click += new System.EventHandler(this.btnWelcomeNext_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.AllowHtmlString = true;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(220, 104);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(220, 25);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Click <b>Next</b> To Continue";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = global::WoodPlan5.Properties.Resources.wizard_image;
            this.pictureEdit1.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(205, 537);
            this.pictureEdit1.TabIndex = 4;
            // 
            // xtraTabPageStep1
            // 
            this.xtraTabPageStep1.Controls.Add(this.labelControlSelectedClientCount);
            this.xtraTabPageStep1.Controls.Add(this.popupContainerControlDateRange);
            this.xtraTabPageStep1.Controls.Add(this.standaloneBarDockControl3);
            this.xtraTabPageStep1.Controls.Add(this.panelControl3);
            this.xtraTabPageStep1.Controls.Add(this.btnStep1Previous);
            this.xtraTabPageStep1.Controls.Add(this.btnStep1Next);
            this.xtraTabPageStep1.Controls.Add(this.gridControl1);
            this.xtraTabPageStep1.Name = "xtraTabPageStep1";
            this.xtraTabPageStep1.Size = new System.Drawing.Size(1211, 537);
            this.xtraTabPageStep1.Tag = "1";
            this.xtraTabPageStep1.Text = "Step 1";
            // 
            // labelControlSelectedClientCount
            // 
            this.labelControlSelectedClientCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControlSelectedClientCount.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlSelectedClientCount.Appearance.ImageIndex = 10;
            this.labelControlSelectedClientCount.Appearance.ImageList = this.imageCollection1;
            this.labelControlSelectedClientCount.Appearance.Options.UseImageAlign = true;
            this.labelControlSelectedClientCount.Appearance.Options.UseImageIndex = true;
            this.labelControlSelectedClientCount.Appearance.Options.UseImageList = true;
            this.labelControlSelectedClientCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlSelectedClientCount.Location = new System.Drawing.Point(7, 503);
            this.labelControlSelectedClientCount.Name = "labelControlSelectedClientCount";
            this.labelControlSelectedClientCount.Size = new System.Drawing.Size(188, 17);
            this.labelControlSelectedClientCount.TabIndex = 24;
            this.labelControlSelectedClientCount.Text = "       0 Selected Client Contract(s)";
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 6);
            this.imageCollection1.Images.SetKeyName(6, "refresh_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.BlockEdit_16x16, "BlockEdit_16x16", typeof(global::WoodPlan5.Properties.Resources), 7);
            this.imageCollection1.Images.SetKeyName(7, "BlockEdit_16x16");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 8);
            this.imageCollection1.Images.SetKeyName(8, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("groupheader_16x16.png", "images/reports/groupheader_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/reports/groupheader_16x16.png"), 9);
            this.imageCollection1.Images.SetKeyName(9, "groupheader_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 10);
            this.imageCollection1.Images.SetKeyName(10, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Calculate_16x16, "Calculate_16x16", typeof(global::WoodPlan5.Properties.Resources), 11);
            this.imageCollection1.Images.SetKeyName(11, "Calculate_16x16");
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.groupControl2);
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeOK);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(34, 222);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(199, 107);
            this.popupContainerControlDateRange.TabIndex = 18;
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.labelControl17);
            this.groupControl2.Controls.Add(this.labelControl18);
            this.groupControl2.Controls.Add(this.dateEditFromDate);
            this.groupControl2.Controls.Add(this.dateEditToDate);
            this.groupControl2.Location = new System.Drawing.Point(3, 5);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(193, 76);
            this.groupControl2.TabIndex = 20;
            this.groupControl2.Text = "Date Range  [Contract Start Date]";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(6, 53);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(16, 13);
            this.labelControl17.TabIndex = 13;
            this.labelControl17.Text = "To:";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(6, 27);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(28, 13);
            this.labelControl18.TabIndex = 12;
            this.labelControl18.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(148, 20);
            this.dateEditFromDate.TabIndex = 10;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(148, 20);
            this.dateEditToDate.TabIndex = 11;
            // 
            // btnDateRangeOK
            // 
            this.btnDateRangeOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeOK.Location = new System.Drawing.Point(3, 84);
            this.btnDateRangeOK.Name = "btnDateRangeOK";
            this.btnDateRangeOK.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDateRangeOK.Size = new System.Drawing.Size(50, 20);
            this.btnDateRangeOK.TabIndex = 19;
            this.btnDateRangeOK.Text = "OK";
            this.btnDateRangeOK.Click += new System.EventHandler(this.btnDateRangeOK_Click);
            // 
            // standaloneBarDockControl3
            // 
            this.standaloneBarDockControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl3.CausesValidation = false;
            this.standaloneBarDockControl3.Location = new System.Drawing.Point(7, 62);
            this.standaloneBarDockControl3.Manager = this.barManager1;
            this.standaloneBarDockControl3.Name = "standaloneBarDockControl3";
            this.standaloneBarDockControl3.Size = new System.Drawing.Size(1198, 26);
            this.standaloneBarDockControl3.Text = "standaloneBarDockControl3";
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.Controls.Add(this.labelControl3);
            this.panelControl3.Controls.Add(this.pictureEdit3);
            this.panelControl3.Controls.Add(this.labelControl5);
            this.panelControl3.Location = new System.Drawing.Point(7, 6);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1198, 48);
            this.panelControl3.TabIndex = 10;
            // 
            // labelControl3
            // 
            this.labelControl3.AllowHtmlString = true;
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseBackColor = true;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(5, 5);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(353, 16);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "<b>Step 1:</b> Select the Client Contract(s) to create the Visit(s) for";
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit3.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit3.Location = new System.Drawing.Point(1154, 4);
            this.pictureEdit3.MenuManager = this.barManager1;
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.ReadOnly = true;
            this.pictureEdit3.Properties.ShowMenu = false;
            this.pictureEdit3.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit3.TabIndex = 8;
            // 
            // labelControl5
            // 
            this.labelControl5.AllowHtmlString = true;
            this.labelControl5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl5.Appearance.Options.UseBackColor = true;
            this.labelControl5.Location = new System.Drawing.Point(57, 29);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(398, 13);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Select the client contract to add the visit(s) to by <b>ticking it</b>. Once done" +
    " click Next.";
            // 
            // btnStep1Previous
            // 
            this.btnStep1Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep1Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep1Previous.Location = new System.Drawing.Point(1023, 501);
            this.btnStep1Previous.Name = "btnStep1Previous";
            this.btnStep1Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep1Previous.TabIndex = 9;
            this.btnStep1Previous.Text = "Previous";
            this.btnStep1Previous.Click += new System.EventHandler(this.btnStep1Previous_Click);
            // 
            // btnStep1Next
            // 
            this.btnStep1Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep1Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep1Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep1Next.Location = new System.Drawing.Point(1117, 501);
            this.btnStep1Next.Name = "btnStep1Next";
            this.btnStep1Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep1Next.TabIndex = 8;
            this.btnStep1Next.Text = "Next";
            this.btnStep1Next.Click += new System.EventHandler(this.btnStep1Next_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp06131OMVisitWizardClientContractsBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Reload Data", "reload")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(7, 86);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime3,
            this.repositoryItemTextEditCurrency3,
            this.repositoryItemTextEditPercentage3,
            this.repositoryItemTextEditInteger});
            this.gridControl1.Size = new System.Drawing.Size(1198, 409);
            this.gridControl1.TabIndex = 7;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp06131OMVisitWizardClientContractsBindingSource
            // 
            this.sp06131OMVisitWizardClientContractsBindingSource.DataMember = "sp06131_OM_Visit_Wizard_Client_Contracts";
            this.sp06131OMVisitWizardClientContractsBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientContractID1,
            this.colClientID,
            this.colGCCompanyID,
            this.colContractTypeID,
            this.colContractStatusID,
            this.colContractDirectorID,
            this.colStartDate1,
            this.colEndDate1,
            this.colActive1,
            this.colSectorTypeID,
            this.colContractValue,
            this.colYearlyPercentageIncrease,
            this.colCheckRPIDate,
            this.colLastClientPaymentDate,
            this.colLinkedToPreviousContractID,
            this.colFinanceClientCode,
            this.colBillingCentreCodeID,
            this.colRemarks,
            this.colClientName,
            this.colGCCompanyName,
            this.colContractType,
            this.colContractStatus,
            this.colContractDirector,
            this.colSectorType,
            this.colClientYearCount,
            this.colContractDescription,
            this.colReactive});
            gridFormatRule9.ApplyToRow = true;
            gridFormatRule9.Column = this.colActive1;
            gridFormatRule9.Name = "Format0";
            formatConditionRuleValue9.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue9.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue9.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue9.Value1 = 0;
            gridFormatRule9.Rule = formatConditionRuleValue9;
            this.gridView1.FormatRules.Add(gridFormatRule9);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.FindDelay = 2000;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colClientContractID1
            // 
            this.colClientContractID1.Caption = "Client Contract ID";
            this.colClientContractID1.FieldName = "ClientContractID";
            this.colClientContractID1.Name = "colClientContractID1";
            this.colClientContractID1.OptionsColumn.AllowEdit = false;
            this.colClientContractID1.OptionsColumn.AllowFocus = false;
            this.colClientContractID1.OptionsColumn.ReadOnly = true;
            this.colClientContractID1.Width = 107;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colGCCompanyID
            // 
            this.colGCCompanyID.Caption = "GC Company ID";
            this.colGCCompanyID.FieldName = "GCCompanyID";
            this.colGCCompanyID.Name = "colGCCompanyID";
            this.colGCCompanyID.OptionsColumn.AllowEdit = false;
            this.colGCCompanyID.OptionsColumn.AllowFocus = false;
            this.colGCCompanyID.OptionsColumn.ReadOnly = true;
            this.colGCCompanyID.Width = 97;
            // 
            // colContractTypeID
            // 
            this.colContractTypeID.Caption = "Contract Type ID";
            this.colContractTypeID.FieldName = "ContractTypeID";
            this.colContractTypeID.Name = "colContractTypeID";
            this.colContractTypeID.OptionsColumn.AllowEdit = false;
            this.colContractTypeID.OptionsColumn.AllowFocus = false;
            this.colContractTypeID.OptionsColumn.ReadOnly = true;
            this.colContractTypeID.Width = 104;
            // 
            // colContractStatusID
            // 
            this.colContractStatusID.Caption = "Contract Status ID";
            this.colContractStatusID.FieldName = "ContractStatusID";
            this.colContractStatusID.Name = "colContractStatusID";
            this.colContractStatusID.OptionsColumn.AllowEdit = false;
            this.colContractStatusID.OptionsColumn.AllowFocus = false;
            this.colContractStatusID.OptionsColumn.ReadOnly = true;
            this.colContractStatusID.Width = 111;
            // 
            // colContractDirectorID
            // 
            this.colContractDirectorID.Caption = "Contract Director ID";
            this.colContractDirectorID.FieldName = "ContractDirectorID";
            this.colContractDirectorID.Name = "colContractDirectorID";
            this.colContractDirectorID.OptionsColumn.AllowEdit = false;
            this.colContractDirectorID.OptionsColumn.AllowFocus = false;
            this.colContractDirectorID.OptionsColumn.ReadOnly = true;
            this.colContractDirectorID.Width = 118;
            // 
            // colStartDate1
            // 
            this.colStartDate1.Caption = "Start Date";
            this.colStartDate1.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colStartDate1.FieldName = "StartDate";
            this.colStartDate1.Name = "colStartDate1";
            this.colStartDate1.OptionsColumn.AllowEdit = false;
            this.colStartDate1.OptionsColumn.AllowFocus = false;
            this.colStartDate1.OptionsColumn.ReadOnly = true;
            this.colStartDate1.Visible = true;
            this.colStartDate1.VisibleIndex = 2;
            this.colStartDate1.Width = 100;
            // 
            // repositoryItemTextEditDateTime3
            // 
            this.repositoryItemTextEditDateTime3.AutoHeight = false;
            this.repositoryItemTextEditDateTime3.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime3.Name = "repositoryItemTextEditDateTime3";
            // 
            // colEndDate1
            // 
            this.colEndDate1.Caption = "End Date";
            this.colEndDate1.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colEndDate1.FieldName = "EndDate";
            this.colEndDate1.Name = "colEndDate1";
            this.colEndDate1.OptionsColumn.AllowEdit = false;
            this.colEndDate1.OptionsColumn.AllowFocus = false;
            this.colEndDate1.OptionsColumn.ReadOnly = true;
            this.colEndDate1.Visible = true;
            this.colEndDate1.VisibleIndex = 3;
            this.colEndDate1.Width = 100;
            // 
            // colSectorTypeID
            // 
            this.colSectorTypeID.Caption = "Sector Type ID";
            this.colSectorTypeID.FieldName = "SectorTypeID";
            this.colSectorTypeID.Name = "colSectorTypeID";
            this.colSectorTypeID.OptionsColumn.AllowEdit = false;
            this.colSectorTypeID.OptionsColumn.AllowFocus = false;
            this.colSectorTypeID.OptionsColumn.ReadOnly = true;
            this.colSectorTypeID.Width = 93;
            // 
            // colContractValue
            // 
            this.colContractValue.Caption = "Contract Value";
            this.colContractValue.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colContractValue.FieldName = "ContractValue";
            this.colContractValue.Name = "colContractValue";
            this.colContractValue.OptionsColumn.AllowEdit = false;
            this.colContractValue.OptionsColumn.AllowFocus = false;
            this.colContractValue.OptionsColumn.ReadOnly = true;
            this.colContractValue.Visible = true;
            this.colContractValue.VisibleIndex = 10;
            this.colContractValue.Width = 92;
            // 
            // repositoryItemTextEditCurrency3
            // 
            this.repositoryItemTextEditCurrency3.AutoHeight = false;
            this.repositoryItemTextEditCurrency3.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency3.Name = "repositoryItemTextEditCurrency3";
            // 
            // colYearlyPercentageIncrease
            // 
            this.colYearlyPercentageIncrease.Caption = "Yearly % Increase";
            this.colYearlyPercentageIncrease.ColumnEdit = this.repositoryItemTextEditPercentage3;
            this.colYearlyPercentageIncrease.FieldName = "YearlyPercentageIncrease";
            this.colYearlyPercentageIncrease.Name = "colYearlyPercentageIncrease";
            this.colYearlyPercentageIncrease.OptionsColumn.AllowEdit = false;
            this.colYearlyPercentageIncrease.OptionsColumn.AllowFocus = false;
            this.colYearlyPercentageIncrease.OptionsColumn.ReadOnly = true;
            this.colYearlyPercentageIncrease.Visible = true;
            this.colYearlyPercentageIncrease.VisibleIndex = 11;
            this.colYearlyPercentageIncrease.Width = 110;
            // 
            // repositoryItemTextEditPercentage3
            // 
            this.repositoryItemTextEditPercentage3.AutoHeight = false;
            this.repositoryItemTextEditPercentage3.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage3.Name = "repositoryItemTextEditPercentage3";
            // 
            // colCheckRPIDate
            // 
            this.colCheckRPIDate.Caption = "Check RPI Date";
            this.colCheckRPIDate.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colCheckRPIDate.FieldName = "CheckRPIDate";
            this.colCheckRPIDate.Name = "colCheckRPIDate";
            this.colCheckRPIDate.OptionsColumn.AllowEdit = false;
            this.colCheckRPIDate.OptionsColumn.AllowFocus = false;
            this.colCheckRPIDate.OptionsColumn.ReadOnly = true;
            this.colCheckRPIDate.Width = 96;
            // 
            // colLastClientPaymentDate
            // 
            this.colLastClientPaymentDate.Caption = "Last Client Payment Date";
            this.colLastClientPaymentDate.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colLastClientPaymentDate.FieldName = "LastClientPaymentDate";
            this.colLastClientPaymentDate.Name = "colLastClientPaymentDate";
            this.colLastClientPaymentDate.OptionsColumn.AllowEdit = false;
            this.colLastClientPaymentDate.OptionsColumn.AllowFocus = false;
            this.colLastClientPaymentDate.OptionsColumn.ReadOnly = true;
            this.colLastClientPaymentDate.Width = 142;
            // 
            // colLinkedToPreviousContractID
            // 
            this.colLinkedToPreviousContractID.Caption = "Linked To Previous Contract ID";
            this.colLinkedToPreviousContractID.FieldName = "LinkedToPreviousContractID";
            this.colLinkedToPreviousContractID.Name = "colLinkedToPreviousContractID";
            this.colLinkedToPreviousContractID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPreviousContractID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPreviousContractID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPreviousContractID.Width = 169;
            // 
            // colFinanceClientCode
            // 
            this.colFinanceClientCode.Caption = "Finance Client Code";
            this.colFinanceClientCode.FieldName = "FinanceClientCode";
            this.colFinanceClientCode.Name = "colFinanceClientCode";
            this.colFinanceClientCode.OptionsColumn.AllowEdit = false;
            this.colFinanceClientCode.OptionsColumn.AllowFocus = false;
            this.colFinanceClientCode.OptionsColumn.ReadOnly = true;
            this.colFinanceClientCode.Width = 116;
            // 
            // colBillingCentreCodeID
            // 
            this.colBillingCentreCodeID.Caption = "Billing Centre Code ID";
            this.colBillingCentreCodeID.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID.Name = "colBillingCentreCodeID";
            this.colBillingCentreCodeID.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID.Width = 125;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.AllowEdit = false;
            this.colRemarks.OptionsColumn.AllowFocus = false;
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 13;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 195;
            // 
            // colGCCompanyName
            // 
            this.colGCCompanyName.Caption = "Gc Company Name";
            this.colGCCompanyName.FieldName = "GCCompanyName";
            this.colGCCompanyName.Name = "colGCCompanyName";
            this.colGCCompanyName.OptionsColumn.AllowEdit = false;
            this.colGCCompanyName.OptionsColumn.AllowFocus = false;
            this.colGCCompanyName.OptionsColumn.ReadOnly = true;
            this.colGCCompanyName.Width = 111;
            // 
            // colContractType
            // 
            this.colContractType.Caption = "Contract Type";
            this.colContractType.FieldName = "ContractType";
            this.colContractType.Name = "colContractType";
            this.colContractType.OptionsColumn.AllowEdit = false;
            this.colContractType.OptionsColumn.AllowFocus = false;
            this.colContractType.OptionsColumn.ReadOnly = true;
            this.colContractType.Visible = true;
            this.colContractType.VisibleIndex = 6;
            this.colContractType.Width = 147;
            // 
            // colContractStatus
            // 
            this.colContractStatus.Caption = "Contract Status";
            this.colContractStatus.FieldName = "ContractStatus";
            this.colContractStatus.Name = "colContractStatus";
            this.colContractStatus.OptionsColumn.AllowEdit = false;
            this.colContractStatus.OptionsColumn.AllowFocus = false;
            this.colContractStatus.OptionsColumn.ReadOnly = true;
            this.colContractStatus.Visible = true;
            this.colContractStatus.VisibleIndex = 8;
            this.colContractStatus.Width = 136;
            // 
            // colContractDirector
            // 
            this.colContractDirector.Caption = "Contract Director";
            this.colContractDirector.FieldName = "ContractDirector";
            this.colContractDirector.Name = "colContractDirector";
            this.colContractDirector.OptionsColumn.AllowEdit = false;
            this.colContractDirector.OptionsColumn.AllowFocus = false;
            this.colContractDirector.OptionsColumn.ReadOnly = true;
            this.colContractDirector.Visible = true;
            this.colContractDirector.VisibleIndex = 9;
            this.colContractDirector.Width = 120;
            // 
            // colSectorType
            // 
            this.colSectorType.Caption = "Sector Type";
            this.colSectorType.FieldName = "SectorType";
            this.colSectorType.Name = "colSectorType";
            this.colSectorType.OptionsColumn.AllowEdit = false;
            this.colSectorType.OptionsColumn.AllowFocus = false;
            this.colSectorType.OptionsColumn.ReadOnly = true;
            this.colSectorType.Visible = true;
            this.colSectorType.VisibleIndex = 7;
            this.colSectorType.Width = 146;
            // 
            // colClientYearCount
            // 
            this.colClientYearCount.Caption = "Linked Site Contracts";
            this.colClientYearCount.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colClientYearCount.FieldName = "SiteContractCount";
            this.colClientYearCount.Name = "colClientYearCount";
            this.colClientYearCount.OptionsColumn.AllowEdit = false;
            this.colClientYearCount.OptionsColumn.AllowFocus = false;
            this.colClientYearCount.OptionsColumn.ReadOnly = true;
            this.colClientYearCount.Visible = true;
            this.colClientYearCount.VisibleIndex = 12;
            this.colClientYearCount.Width = 122;
            // 
            // repositoryItemTextEditInteger
            // 
            this.repositoryItemTextEditInteger.AutoHeight = false;
            this.repositoryItemTextEditInteger.Mask.EditMask = "f0";
            this.repositoryItemTextEditInteger.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger.Name = "repositoryItemTextEditInteger";
            // 
            // colContractDescription
            // 
            this.colContractDescription.Caption = "Contract Description";
            this.colContractDescription.FieldName = "ContractDescription";
            this.colContractDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colContractDescription.Name = "colContractDescription";
            this.colContractDescription.OptionsColumn.AllowEdit = false;
            this.colContractDescription.OptionsColumn.AllowFocus = false;
            this.colContractDescription.OptionsColumn.ReadOnly = true;
            this.colContractDescription.Visible = true;
            this.colContractDescription.VisibleIndex = 1;
            this.colContractDescription.Width = 285;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 5;
            this.colReactive.Width = 63;
            // 
            // xtraTabPageStep2
            // 
            this.xtraTabPageStep2.Controls.Add(this.labelControlSelectedSiteCount);
            this.xtraTabPageStep2.Controls.Add(this.gridControl2);
            this.xtraTabPageStep2.Controls.Add(this.panelControl4);
            this.xtraTabPageStep2.Controls.Add(this.btnStep2Next);
            this.xtraTabPageStep2.Controls.Add(this.btnStep2Previous);
            this.xtraTabPageStep2.Name = "xtraTabPageStep2";
            this.xtraTabPageStep2.Size = new System.Drawing.Size(1211, 537);
            this.xtraTabPageStep2.Tag = "2";
            this.xtraTabPageStep2.Text = "Step 2";
            // 
            // labelControlSelectedSiteCount
            // 
            this.labelControlSelectedSiteCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControlSelectedSiteCount.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlSelectedSiteCount.Appearance.ImageIndex = 10;
            this.labelControlSelectedSiteCount.Appearance.ImageList = this.imageCollection1;
            this.labelControlSelectedSiteCount.Appearance.Options.UseImageAlign = true;
            this.labelControlSelectedSiteCount.Appearance.Options.UseImageIndex = true;
            this.labelControlSelectedSiteCount.Appearance.Options.UseImageList = true;
            this.labelControlSelectedSiteCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlSelectedSiteCount.Location = new System.Drawing.Point(7, 503);
            this.labelControlSelectedSiteCount.Name = "labelControlSelectedSiteCount";
            this.labelControlSelectedSiteCount.Size = new System.Drawing.Size(188, 17);
            this.labelControlSelectedSiteCount.TabIndex = 23;
            this.labelControlSelectedSiteCount.Text = "       0 Selected Site Contract(s)";
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp06132OMVisitWizardSiteContractsForClientContractBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(7, 62);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemTextEditPercentage2,
            this.repositoryItemTextEditInteger2,
            this.repositoryItemGridLookUpEditDefaultScheduleTemplateID,
            this.repositoryItemCheckEdit5});
            this.gridControl2.Size = new System.Drawing.Size(1198, 433);
            this.gridControl2.TabIndex = 22;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp06132OMVisitWizardSiteContractsForClientContractBindingSource
            // 
            this.sp06132OMVisitWizardSiteContractsForClientContractBindingSource.DataMember = "sp06132_OM_Visit_Wizard_Site_Contracts_For_Client_Contract";
            this.sp06132OMVisitWizardSiteContractsForClientContractBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteContractID3,
            this.colClientContractID3,
            this.colSiteID,
            this.colStartDate3,
            this.colEndDate3,
            this.colActive3,
            this.colContractValue2,
            this.colYearlyPercentageIncrease3,
            this.colLastClientPaymentDate2,
            this.colRemarks4,
            this.colClientID2,
            this.colSiteName,
            this.colLinkedToParent3,
            this.colVisitCount,
            this.colVisitNumberMax,
            this.colDefaultScheduleTemplateID,
            this.colOnHold,
            this.colOnHoldReasonID,
            this.colOnHoldReason,
            this.colOnHoldStartDate,
            this.colOnHoldEndDate,
            this.colSiteCategory,
            this.colSiteContractDaysSeparationPercent2,
            this.colServiceLevel});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActive3;
            gridFormatRule1.Name = "FormatActive";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            gridFormatRule10.ApplyToRow = true;
            gridFormatRule10.Column = this.colOnHold;
            gridFormatRule10.Name = "FormatOnHold";
            formatConditionRuleValue10.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue10.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue10.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue10.Value1 = 1;
            gridFormatRule10.Rule = formatConditionRuleValue10;
            this.gridView2.FormatRules.Add(gridFormatRule1);
            this.gridView2.FormatRules.Add(gridFormatRule10);
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.FindDelay = 2000;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colSiteContractID3
            // 
            this.colSiteContractID3.Caption = "Site Contract ID";
            this.colSiteContractID3.FieldName = "SiteContractID";
            this.colSiteContractID3.Name = "colSiteContractID3";
            this.colSiteContractID3.OptionsColumn.AllowEdit = false;
            this.colSiteContractID3.OptionsColumn.AllowFocus = false;
            this.colSiteContractID3.OptionsColumn.ReadOnly = true;
            this.colSiteContractID3.Width = 98;
            // 
            // colClientContractID3
            // 
            this.colClientContractID3.Caption = "Client Contract ID";
            this.colClientContractID3.FieldName = "ClientContractID";
            this.colClientContractID3.Name = "colClientContractID3";
            this.colClientContractID3.OptionsColumn.AllowEdit = false;
            this.colClientContractID3.OptionsColumn.AllowFocus = false;
            this.colClientContractID3.OptionsColumn.ReadOnly = true;
            this.colClientContractID3.Width = 107;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.Visible = true;
            this.colSiteID.VisibleIndex = 0;
            this.colSiteID.Width = 51;
            // 
            // colStartDate3
            // 
            this.colStartDate3.Caption = "Start Date";
            this.colStartDate3.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colStartDate3.FieldName = "StartDate";
            this.colStartDate3.Name = "colStartDate3";
            this.colStartDate3.OptionsColumn.AllowEdit = false;
            this.colStartDate3.OptionsColumn.AllowFocus = false;
            this.colStartDate3.OptionsColumn.ReadOnly = true;
            this.colStartDate3.Visible = true;
            this.colStartDate3.VisibleIndex = 2;
            this.colStartDate3.Width = 100;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colEndDate3
            // 
            this.colEndDate3.Caption = "End Date";
            this.colEndDate3.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colEndDate3.FieldName = "EndDate";
            this.colEndDate3.Name = "colEndDate3";
            this.colEndDate3.OptionsColumn.AllowEdit = false;
            this.colEndDate3.OptionsColumn.AllowFocus = false;
            this.colEndDate3.OptionsColumn.ReadOnly = true;
            this.colEndDate3.Visible = true;
            this.colEndDate3.VisibleIndex = 3;
            this.colEndDate3.Width = 100;
            // 
            // colContractValue2
            // 
            this.colContractValue2.Caption = "Value";
            this.colContractValue2.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colContractValue2.FieldName = "ContractValue";
            this.colContractValue2.Name = "colContractValue2";
            this.colContractValue2.OptionsColumn.AllowEdit = false;
            this.colContractValue2.OptionsColumn.AllowFocus = false;
            this.colContractValue2.OptionsColumn.ReadOnly = true;
            this.colContractValue2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ContractValue", "{0:c}")});
            this.colContractValue2.Visible = true;
            this.colContractValue2.VisibleIndex = 7;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // colYearlyPercentageIncrease3
            // 
            this.colYearlyPercentageIncrease3.Caption = "Yearly % Increase";
            this.colYearlyPercentageIncrease3.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colYearlyPercentageIncrease3.FieldName = "YearlyPercentageIncrease";
            this.colYearlyPercentageIncrease3.Name = "colYearlyPercentageIncrease3";
            this.colYearlyPercentageIncrease3.OptionsColumn.AllowEdit = false;
            this.colYearlyPercentageIncrease3.OptionsColumn.AllowFocus = false;
            this.colYearlyPercentageIncrease3.OptionsColumn.ReadOnly = true;
            this.colYearlyPercentageIncrease3.Visible = true;
            this.colYearlyPercentageIncrease3.VisibleIndex = 8;
            this.colYearlyPercentageIncrease3.Width = 110;
            // 
            // repositoryItemTextEditPercentage2
            // 
            this.repositoryItemTextEditPercentage2.AutoHeight = false;
            this.repositoryItemTextEditPercentage2.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage2.Name = "repositoryItemTextEditPercentage2";
            // 
            // colLastClientPaymentDate2
            // 
            this.colLastClientPaymentDate2.Caption = "Last Client Payment";
            this.colLastClientPaymentDate2.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colLastClientPaymentDate2.FieldName = "LastClientPaymentDate";
            this.colLastClientPaymentDate2.Name = "colLastClientPaymentDate2";
            this.colLastClientPaymentDate2.OptionsColumn.AllowEdit = false;
            this.colLastClientPaymentDate2.OptionsColumn.AllowFocus = false;
            this.colLastClientPaymentDate2.OptionsColumn.ReadOnly = true;
            this.colLastClientPaymentDate2.Width = 116;
            // 
            // colRemarks4
            // 
            this.colRemarks4.Caption = "Remarks";
            this.colRemarks4.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colRemarks4.FieldName = "Remarks";
            this.colRemarks4.Name = "colRemarks4";
            this.colRemarks4.OptionsColumn.ReadOnly = true;
            this.colRemarks4.Visible = true;
            this.colRemarks4.VisibleIndex = 15;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 1;
            this.colSiteName.Width = 255;
            // 
            // colLinkedToParent3
            // 
            this.colLinkedToParent3.Caption = "Linked To Contract";
            this.colLinkedToParent3.FieldName = "LinkedToParent";
            this.colLinkedToParent3.Name = "colLinkedToParent3";
            this.colLinkedToParent3.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent3.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent3.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent3.Visible = true;
            this.colLinkedToParent3.VisibleIndex = 9;
            this.colLinkedToParent3.Width = 273;
            // 
            // colVisitCount
            // 
            this.colVisitCount.Caption = "Existing Visit Count";
            this.colVisitCount.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colVisitCount.FieldName = "VisitCount";
            this.colVisitCount.Name = "colVisitCount";
            this.colVisitCount.OptionsColumn.AllowEdit = false;
            this.colVisitCount.OptionsColumn.AllowFocus = false;
            this.colVisitCount.OptionsColumn.ReadOnly = true;
            this.colVisitCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitCount", "{0:n0}")});
            this.colVisitCount.Visible = true;
            this.colVisitCount.VisibleIndex = 16;
            this.colVisitCount.Width = 112;
            // 
            // repositoryItemTextEditInteger2
            // 
            this.repositoryItemTextEditInteger2.AutoHeight = false;
            this.repositoryItemTextEditInteger2.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger2.Name = "repositoryItemTextEditInteger2";
            // 
            // colVisitNumberMax
            // 
            this.colVisitNumberMax.Caption = "Current Max Visit #";
            this.colVisitNumberMax.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colVisitNumberMax.FieldName = "VisitNumberMax";
            this.colVisitNumberMax.Name = "colVisitNumberMax";
            this.colVisitNumberMax.OptionsColumn.AllowEdit = false;
            this.colVisitNumberMax.OptionsColumn.AllowFocus = false;
            this.colVisitNumberMax.OptionsColumn.ReadOnly = true;
            this.colVisitNumberMax.Visible = true;
            this.colVisitNumberMax.VisibleIndex = 17;
            this.colVisitNumberMax.Width = 114;
            // 
            // colDefaultScheduleTemplateID
            // 
            this.colDefaultScheduleTemplateID.Caption = "Default Schedule Template";
            this.colDefaultScheduleTemplateID.ColumnEdit = this.repositoryItemGridLookUpEditDefaultScheduleTemplateID;
            this.colDefaultScheduleTemplateID.FieldName = "DefaultScheduleTemplateID";
            this.colDefaultScheduleTemplateID.Name = "colDefaultScheduleTemplateID";
            this.colDefaultScheduleTemplateID.Visible = true;
            this.colDefaultScheduleTemplateID.VisibleIndex = 9;
            this.colDefaultScheduleTemplateID.Width = 228;
            // 
            // repositoryItemGridLookUpEditDefaultScheduleTemplateID
            // 
            this.repositoryItemGridLookUpEditDefaultScheduleTemplateID.AutoHeight = false;
            this.repositoryItemGridLookUpEditDefaultScheduleTemplateID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditDefaultScheduleTemplateID.DataSource = this.sp06141OMVisitTemplateHeadersWithBlankBindingSource;
            this.repositoryItemGridLookUpEditDefaultScheduleTemplateID.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditDefaultScheduleTemplateID.Name = "repositoryItemGridLookUpEditDefaultScheduleTemplateID";
            this.repositoryItemGridLookUpEditDefaultScheduleTemplateID.PopupView = this.gridView9;
            this.repositoryItemGridLookUpEditDefaultScheduleTemplateID.ValueMember = "ID";
            // 
            // sp06141OMVisitTemplateHeadersWithBlankBindingSource
            // 
            this.sp06141OMVisitTemplateHeadersWithBlankBindingSource.DataMember = "sp06141_OM_Visit_Template_Headers_With_Blank";
            this.sp06141OMVisitTemplateHeadersWithBlankBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18});
            this.gridView9.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.gridColumn16;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridView9.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView9.OptionsLayout.StoreAppearance = true;
            this.gridView9.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView9.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView9.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView9.OptionsView.ColumnAutoWidth = false;
            this.gridView9.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            this.gridView9.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView9.OptionsView.ShowIndicator = false;
            this.gridView9.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn18, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Schedule Template";
            this.gridColumn17.FieldName = "Description";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 0;
            this.gridColumn17.Width = 220;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Order";
            this.gridColumn18.FieldName = "RecordOrder";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            // 
            // colOnHoldReasonID
            // 
            this.colOnHoldReasonID.Caption = "On-Hold Reason ID";
            this.colOnHoldReasonID.FieldName = "OnHoldReasonID";
            this.colOnHoldReasonID.Name = "colOnHoldReasonID";
            this.colOnHoldReasonID.OptionsColumn.AllowEdit = false;
            this.colOnHoldReasonID.OptionsColumn.AllowFocus = false;
            this.colOnHoldReasonID.OptionsColumn.ReadOnly = true;
            this.colOnHoldReasonID.Width = 111;
            // 
            // colOnHoldReason
            // 
            this.colOnHoldReason.Caption = "On-Hold Reason";
            this.colOnHoldReason.FieldName = "OnHoldReason";
            this.colOnHoldReason.Name = "colOnHoldReason";
            this.colOnHoldReason.OptionsColumn.AllowEdit = false;
            this.colOnHoldReason.OptionsColumn.AllowFocus = false;
            this.colOnHoldReason.OptionsColumn.ReadOnly = true;
            this.colOnHoldReason.Visible = true;
            this.colOnHoldReason.VisibleIndex = 12;
            this.colOnHoldReason.Width = 161;
            // 
            // colOnHoldStartDate
            // 
            this.colOnHoldStartDate.Caption = "On-Hold Start";
            this.colOnHoldStartDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colOnHoldStartDate.FieldName = "OnHoldStartDate";
            this.colOnHoldStartDate.Name = "colOnHoldStartDate";
            this.colOnHoldStartDate.OptionsColumn.AllowEdit = false;
            this.colOnHoldStartDate.OptionsColumn.AllowFocus = false;
            this.colOnHoldStartDate.OptionsColumn.ReadOnly = true;
            this.colOnHoldStartDate.Visible = true;
            this.colOnHoldStartDate.VisibleIndex = 13;
            this.colOnHoldStartDate.Width = 100;
            // 
            // colOnHoldEndDate
            // 
            this.colOnHoldEndDate.Caption = "On-Hold End";
            this.colOnHoldEndDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colOnHoldEndDate.FieldName = "OnHoldEndDate";
            this.colOnHoldEndDate.Name = "colOnHoldEndDate";
            this.colOnHoldEndDate.OptionsColumn.AllowEdit = false;
            this.colOnHoldEndDate.OptionsColumn.AllowFocus = false;
            this.colOnHoldEndDate.OptionsColumn.ReadOnly = true;
            this.colOnHoldEndDate.Visible = true;
            this.colOnHoldEndDate.VisibleIndex = 14;
            this.colOnHoldEndDate.Width = 100;
            // 
            // colSiteCategory
            // 
            this.colSiteCategory.Caption = "Site Category";
            this.colSiteCategory.FieldName = "SiteCategory";
            this.colSiteCategory.Name = "colSiteCategory";
            this.colSiteCategory.OptionsColumn.AllowEdit = false;
            this.colSiteCategory.OptionsColumn.AllowFocus = false;
            this.colSiteCategory.OptionsColumn.ReadOnly = true;
            this.colSiteCategory.Visible = true;
            this.colSiteCategory.VisibleIndex = 5;
            this.colSiteCategory.Width = 118;
            // 
            // colSiteContractDaysSeparationPercent2
            // 
            this.colSiteContractDaysSeparationPercent2.Caption = "Vist Days Separation %";
            this.colSiteContractDaysSeparationPercent2.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colSiteContractDaysSeparationPercent2.FieldName = "SiteContractDaysSeparationPercent";
            this.colSiteContractDaysSeparationPercent2.Name = "colSiteContractDaysSeparationPercent2";
            this.colSiteContractDaysSeparationPercent2.OptionsColumn.AllowEdit = false;
            this.colSiteContractDaysSeparationPercent2.OptionsColumn.AllowFocus = false;
            this.colSiteContractDaysSeparationPercent2.OptionsColumn.ReadOnly = true;
            this.colSiteContractDaysSeparationPercent2.Visible = true;
            this.colSiteContractDaysSeparationPercent2.VisibleIndex = 10;
            this.colSiteContractDaysSeparationPercent2.Width = 132;
            // 
            // colServiceLevel
            // 
            this.colServiceLevel.Caption = "Service Level";
            this.colServiceLevel.FieldName = "ServiceLevel";
            this.colServiceLevel.Name = "colServiceLevel";
            this.colServiceLevel.OptionsColumn.AllowEdit = false;
            this.colServiceLevel.OptionsColumn.AllowFocus = false;
            this.colServiceLevel.OptionsColumn.ReadOnly = true;
            this.colServiceLevel.Visible = true;
            this.colServiceLevel.VisibleIndex = 6;
            this.colServiceLevel.Width = 117;
            // 
            // panelControl4
            // 
            this.panelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl4.Controls.Add(this.pictureEdit5);
            this.panelControl4.Controls.Add(this.labelControl7);
            this.panelControl4.Controls.Add(this.labelControl8);
            this.panelControl4.Location = new System.Drawing.Point(7, 6);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1198, 48);
            this.panelControl4.TabIndex = 15;
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit5.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit5.Location = new System.Drawing.Point(1154, 4);
            this.pictureEdit5.MenuManager = this.barManager1;
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Properties.ReadOnly = true;
            this.pictureEdit5.Properties.ShowMenu = false;
            this.pictureEdit5.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit5.TabIndex = 9;
            // 
            // labelControl7
            // 
            this.labelControl7.AllowHtmlString = true;
            this.labelControl7.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseBackColor = true;
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(5, 5);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(321, 16);
            this.labelControl7.TabIndex = 6;
            this.labelControl7.Text = "<b>Step 2:</b> Select the Site Contract(s) to create Visit(s) for";
            // 
            // labelControl8
            // 
            this.labelControl8.AllowHtmlString = true;
            this.labelControl8.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl8.Appearance.Options.UseBackColor = true;
            this.labelControl8.Location = new System.Drawing.Point(57, 29);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(449, 13);
            this.labelControl8.TabIndex = 7;
            this.labelControl8.Text = "Select one or more Site Contracts to create Visits for by <b>ticking</b> them. Cl" +
    "ick Next when done.";
            // 
            // btnStep2Next
            // 
            this.btnStep2Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep2Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep2Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep2Next.Location = new System.Drawing.Point(1117, 501);
            this.btnStep2Next.Name = "btnStep2Next";
            this.btnStep2Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep2Next.TabIndex = 0;
            this.btnStep2Next.Text = "Next";
            this.btnStep2Next.Click += new System.EventHandler(this.btnStep2Next_Click);
            // 
            // btnStep2Previous
            // 
            this.btnStep2Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep2Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep2Previous.Location = new System.Drawing.Point(1023, 501);
            this.btnStep2Previous.Name = "btnStep2Previous";
            this.btnStep2Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep2Previous.TabIndex = 10;
            this.btnStep2Previous.Text = "Previous";
            this.btnStep2Previous.Click += new System.EventHandler(this.btnStep2Previous_Click);
            // 
            // xtraTabPageStep3
            // 
            this.xtraTabPageStep3.Controls.Add(this.groupControl3);
            this.xtraTabPageStep3.Controls.Add(this.btnStep3Next);
            this.xtraTabPageStep3.Controls.Add(this.btnStep3Previous);
            this.xtraTabPageStep3.Controls.Add(this.panelControl8);
            this.xtraTabPageStep3.Name = "xtraTabPageStep3";
            this.xtraTabPageStep3.Size = new System.Drawing.Size(1211, 537);
            this.xtraTabPageStep3.Tag = "3";
            this.xtraTabPageStep3.Text = "Step 3";
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.splitContainerControl7);
            this.groupControl3.Controls.Add(this.labelControlOccurrences);
            this.groupControl3.Controls.Add(this.spinEditOccurrenceCount);
            this.groupControl3.Controls.Add(this.radioGroupEndType);
            this.groupControl3.Controls.Add(this.dateEditRecurrenceEnd);
            this.groupControl3.Controls.Add(this.yearlyRecurrenceControl1);
            this.groupControl3.Controls.Add(this.weeklyRecurrenceControl1);
            this.groupControl3.Controls.Add(this.monthlyRecurrenceControl1);
            this.groupControl3.Controls.Add(this.dailyRecurrenceControl1);
            this.groupControl3.Controls.Add(this.radioGroupRecurrence);
            this.groupControl3.Controls.Add(this.labelControlDefaultScheduleTemplateUnavailable);
            this.groupControl3.Controls.Add(this.labelControlNumberOfVisits);
            this.groupControl3.Controls.Add(this.gridLookUpEditSellCalculationLevel);
            this.groupControl3.Controls.Add(this.labelControlSellCalculationLevel);
            this.groupControl3.Controls.Add(this.gridLookUpEditCostCalculationLevel);
            this.groupControl3.Controls.Add(this.labelControlCostCalculationLevel);
            this.groupControl3.Controls.Add(this.radioGroupNumberOfVisitsMethod);
            this.groupControl3.Controls.Add(this.gridLookUpEditGapBetweenVisitsDescriptor);
            this.groupControl3.Controls.Add(this.spinEditGapBetweenVisitsUnits);
            this.groupControl3.Controls.Add(this.labelControlGapBetweenVisits);
            this.groupControl3.Controls.Add(this.spinEditNumberOfVisits);
            this.groupControl3.Controls.Add(this.labelControl27);
            this.groupControl3.Location = new System.Drawing.Point(7, 62);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(1198, 433);
            this.groupControl3.TabIndex = 23;
            this.groupControl3.Text = "How Many Visits Do You Wish To Create?";
            // 
            // splitContainerControl7
            // 
            this.splitContainerControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl7.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl7.Location = new System.Drawing.Point(241, 82);
            this.splitContainerControl7.Name = "splitContainerControl7";
            this.splitContainerControl7.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl7.Panel1.Controls.Add(this.gridControl3);
            this.splitContainerControl7.Panel1.ShowCaption = true;
            this.splitContainerControl7.Panel1.Text = "Template Headers  -  Select One";
            this.splitContainerControl7.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl7.Panel2.Controls.Add(this.gridControl18);
            this.splitContainerControl7.Panel2.ShowCaption = true;
            this.splitContainerControl7.Panel2.Text = "Template Items Linked to Selected Header [Read Only]";
            this.splitContainerControl7.Size = new System.Drawing.Size(948, 347);
            this.splitContainerControl7.SplitterPosition = 440;
            this.splitContainerControl7.TabIndex = 29;
            this.splitContainerControl7.Text = "splitContainerControl7";
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp06142OMVisitTemplateHeadersSelectBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 9, true, true, "Template Manager", "template_manager"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Refresh Data", "reload")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(436, 323);
            this.gridControl3.TabIndex = 22;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp06142OMVisitTemplateHeadersSelectBindingSource
            // 
            this.sp06142OMVisitTemplateHeadersSelectBindingSource.DataMember = "sp06142_OM_Visit_Template_Headers_Select";
            this.sp06142OMVisitTemplateHeadersSelectBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTemplateHeaderID,
            this.colDescription1,
            this.colOrderID,
            this.colRemarks5,
            this.colLinkedItemCount});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView3.OptionsFilter.AllowFilterEditor = false;
            this.gridView3.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView3.OptionsFilter.AllowMRUFilterList = false;
            this.gridView3.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView3.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView3.OptionsFind.FindDelay = 2000;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsLayout.StoreFormatRules = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView3.OptionsView.ShowFooter = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrderID, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colTemplateHeaderID
            // 
            this.colTemplateHeaderID.Caption = "Template Header ID";
            this.colTemplateHeaderID.FieldName = "TemplateHeaderID";
            this.colTemplateHeaderID.Name = "colTemplateHeaderID";
            this.colTemplateHeaderID.OptionsColumn.AllowEdit = false;
            this.colTemplateHeaderID.OptionsColumn.AllowFocus = false;
            this.colTemplateHeaderID.OptionsColumn.ReadOnly = true;
            this.colTemplateHeaderID.Width = 117;
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Description";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 236;
            // 
            // colOrderID
            // 
            this.colOrderID.Caption = "Order";
            this.colOrderID.FieldName = "OrderID";
            this.colOrderID.Name = "colOrderID";
            this.colOrderID.OptionsColumn.AllowEdit = false;
            this.colOrderID.OptionsColumn.AllowFocus = false;
            this.colOrderID.OptionsColumn.ReadOnly = true;
            this.colOrderID.Width = 62;
            // 
            // colRemarks5
            // 
            this.colRemarks5.Caption = "Remarks";
            this.colRemarks5.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.colRemarks5.FieldName = "Remarks";
            this.colRemarks5.Name = "colRemarks5";
            this.colRemarks5.OptionsColumn.ReadOnly = true;
            this.colRemarks5.Visible = true;
            this.colRemarks5.VisibleIndex = 1;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.LookAndFeel.SkinName = "Blue";
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // colLinkedItemCount
            // 
            this.colLinkedItemCount.Caption = "Linked Items";
            this.colLinkedItemCount.FieldName = "LinkedItemCount";
            this.colLinkedItemCount.Name = "colLinkedItemCount";
            this.colLinkedItemCount.OptionsColumn.AllowEdit = false;
            this.colLinkedItemCount.OptionsColumn.AllowFocus = false;
            this.colLinkedItemCount.OptionsColumn.ReadOnly = true;
            this.colLinkedItemCount.Visible = true;
            this.colLinkedItemCount.VisibleIndex = 2;
            this.colLinkedItemCount.Width = 81;
            // 
            // gridControl18
            // 
            this.gridControl18.DataSource = this.sp06143OMVisitTemplateItemsSelectBindingSource;
            this.gridControl18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl18.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl18.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl18.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl18.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl18.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl18.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl18.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl18.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl18.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl18.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl18.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl18.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Refresh Data", "reload")});
            this.gridControl18.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl18_EmbeddedNavigator_ButtonClick);
            this.gridControl18.Location = new System.Drawing.Point(0, 0);
            this.gridControl18.MainView = this.gridView18;
            this.gridControl18.MenuManager = this.barManager1;
            this.gridControl18.Name = "gridControl18";
            this.gridControl18.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit16,
            this.repositoryItemTextEdit2DP});
            this.gridControl18.Size = new System.Drawing.Size(498, 323);
            this.gridControl18.TabIndex = 23;
            this.gridControl18.UseEmbeddedNavigator = true;
            this.gridControl18.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView18});
            // 
            // sp06143OMVisitTemplateItemsSelectBindingSource
            // 
            this.sp06143OMVisitTemplateItemsSelectBindingSource.DataMember = "sp06143_OM_Visit_Template_Items_Select";
            this.sp06143OMVisitTemplateItemsSelectBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView18
            // 
            this.gridView18.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTemplateItemID,
            this.colTemplateHeaderID1,
            this.colUnitsFromStart,
            this.colUnitsFromStartDescriptorID,
            this.colCostCalculationLevel,
            this.colOrderID1,
            this.colRemarks7,
            this.colUnitsFromStartDescriptor,
            this.colHeaderDescription,
            this.colCostCalculationLevelDescriptor,
            this.colVisitCategory,
            this.colVisitCategoryID1,
            this.colDurationUnits,
            this.colDurationUnitsDescriptor,
            this.colDurationUnitsDescriptorID,
            this.colSellCalculationLevel,
            this.colSellCalculationLevelDescriptor});
            this.gridView18.GridControl = this.gridControl18;
            this.gridView18.Name = "gridView18";
            this.gridView18.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView18.OptionsFilter.AllowFilterEditor = false;
            this.gridView18.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView18.OptionsFilter.AllowMRUFilterList = false;
            this.gridView18.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView18.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView18.OptionsFind.FindDelay = 2000;
            this.gridView18.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView18.OptionsLayout.StoreAppearance = true;
            this.gridView18.OptionsLayout.StoreFormatRules = true;
            this.gridView18.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView18.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView18.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView18.OptionsView.ColumnAutoWidth = false;
            this.gridView18.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView18.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView18.OptionsView.ShowFooter = true;
            this.gridView18.OptionsView.ShowGroupPanel = false;
            this.gridView18.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrderID1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView18.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView18.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView18.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView18.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView18.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView18.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            this.gridView18.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView18_MouseUp);
            this.gridView18.GotFocus += new System.EventHandler(this.gridView18_GotFocus);
            // 
            // colTemplateItemID
            // 
            this.colTemplateItemID.Caption = "Template Item ID";
            this.colTemplateItemID.FieldName = "TemplateItemID";
            this.colTemplateItemID.Name = "colTemplateItemID";
            this.colTemplateItemID.OptionsColumn.AllowEdit = false;
            this.colTemplateItemID.OptionsColumn.AllowFocus = false;
            this.colTemplateItemID.OptionsColumn.ReadOnly = true;
            this.colTemplateItemID.Width = 104;
            // 
            // colTemplateHeaderID1
            // 
            this.colTemplateHeaderID1.Caption = "Template Header ID";
            this.colTemplateHeaderID1.FieldName = "TemplateHeaderID";
            this.colTemplateHeaderID1.Name = "colTemplateHeaderID1";
            this.colTemplateHeaderID1.OptionsColumn.AllowEdit = false;
            this.colTemplateHeaderID1.OptionsColumn.AllowFocus = false;
            this.colTemplateHeaderID1.OptionsColumn.ReadOnly = true;
            this.colTemplateHeaderID1.Width = 117;
            // 
            // colUnitsFromStart
            // 
            this.colUnitsFromStart.Caption = "Time From First Visit";
            this.colUnitsFromStart.FieldName = "UnitsFromStart";
            this.colUnitsFromStart.Name = "colUnitsFromStart";
            this.colUnitsFromStart.OptionsColumn.AllowEdit = false;
            this.colUnitsFromStart.OptionsColumn.AllowFocus = false;
            this.colUnitsFromStart.OptionsColumn.ReadOnly = true;
            this.colUnitsFromStart.Visible = true;
            this.colUnitsFromStart.VisibleIndex = 0;
            this.colUnitsFromStart.Width = 116;
            // 
            // colUnitsFromStartDescriptorID
            // 
            this.colUnitsFromStartDescriptorID.Caption = "Time Descriptor ID";
            this.colUnitsFromStartDescriptorID.FieldName = "UnitsFromStartDescriptorID";
            this.colUnitsFromStartDescriptorID.Name = "colUnitsFromStartDescriptorID";
            this.colUnitsFromStartDescriptorID.OptionsColumn.AllowEdit = false;
            this.colUnitsFromStartDescriptorID.OptionsColumn.AllowFocus = false;
            this.colUnitsFromStartDescriptorID.OptionsColumn.ReadOnly = true;
            this.colUnitsFromStartDescriptorID.Width = 109;
            // 
            // colCostCalculationLevel
            // 
            this.colCostCalculationLevel.Caption = "Cost Calculation ID";
            this.colCostCalculationLevel.FieldName = "CostCalculationLevel";
            this.colCostCalculationLevel.Name = "colCostCalculationLevel";
            this.colCostCalculationLevel.OptionsColumn.AllowEdit = false;
            this.colCostCalculationLevel.OptionsColumn.AllowFocus = false;
            this.colCostCalculationLevel.OptionsColumn.ReadOnly = true;
            this.colCostCalculationLevel.Width = 140;
            // 
            // colOrderID1
            // 
            this.colOrderID1.Caption = "Order";
            this.colOrderID1.FieldName = "OrderID";
            this.colOrderID1.Name = "colOrderID1";
            this.colOrderID1.OptionsColumn.AllowEdit = false;
            this.colOrderID1.OptionsColumn.AllowFocus = false;
            this.colOrderID1.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks7
            // 
            this.colRemarks7.Caption = "Remarks";
            this.colRemarks7.ColumnEdit = this.repositoryItemMemoExEdit16;
            this.colRemarks7.FieldName = "Remarks";
            this.colRemarks7.Name = "colRemarks7";
            this.colRemarks7.OptionsColumn.ReadOnly = true;
            this.colRemarks7.Visible = true;
            this.colRemarks7.VisibleIndex = 7;
            this.colRemarks7.Width = 110;
            // 
            // repositoryItemMemoExEdit16
            // 
            this.repositoryItemMemoExEdit16.AutoHeight = false;
            this.repositoryItemMemoExEdit16.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit16.Name = "repositoryItemMemoExEdit16";
            this.repositoryItemMemoExEdit16.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit16.ShowIcon = false;
            // 
            // colUnitsFromStartDescriptor
            // 
            this.colUnitsFromStartDescriptor.Caption = "Time Descriptor";
            this.colUnitsFromStartDescriptor.FieldName = "UnitsFromStartDescriptor";
            this.colUnitsFromStartDescriptor.Name = "colUnitsFromStartDescriptor";
            this.colUnitsFromStartDescriptor.OptionsColumn.AllowEdit = false;
            this.colUnitsFromStartDescriptor.OptionsColumn.AllowFocus = false;
            this.colUnitsFromStartDescriptor.OptionsColumn.ReadOnly = true;
            this.colUnitsFromStartDescriptor.Visible = true;
            this.colUnitsFromStartDescriptor.VisibleIndex = 1;
            this.colUnitsFromStartDescriptor.Width = 95;
            // 
            // colHeaderDescription
            // 
            this.colHeaderDescription.Caption = "Linked To Header";
            this.colHeaderDescription.FieldName = "HeaderDescription";
            this.colHeaderDescription.Name = "colHeaderDescription";
            this.colHeaderDescription.OptionsColumn.AllowEdit = false;
            this.colHeaderDescription.OptionsColumn.AllowFocus = false;
            this.colHeaderDescription.OptionsColumn.ReadOnly = true;
            this.colHeaderDescription.Width = 104;
            // 
            // colCostCalculationLevelDescriptor
            // 
            this.colCostCalculationLevelDescriptor.Caption = "Cost Calculation";
            this.colCostCalculationLevelDescriptor.FieldName = "CostCalculationLevelDescriptor";
            this.colCostCalculationLevelDescriptor.Name = "colCostCalculationLevelDescriptor";
            this.colCostCalculationLevelDescriptor.OptionsColumn.AllowEdit = false;
            this.colCostCalculationLevelDescriptor.OptionsColumn.AllowFocus = false;
            this.colCostCalculationLevelDescriptor.OptionsColumn.ReadOnly = true;
            this.colCostCalculationLevelDescriptor.Visible = true;
            this.colCostCalculationLevelDescriptor.VisibleIndex = 3;
            this.colCostCalculationLevelDescriptor.Width = 100;
            // 
            // colVisitCategory
            // 
            this.colVisitCategory.Caption = "Visit Category";
            this.colVisitCategory.FieldName = "VisitCategory";
            this.colVisitCategory.Name = "colVisitCategory";
            this.colVisitCategory.OptionsColumn.AllowEdit = false;
            this.colVisitCategory.OptionsColumn.AllowFocus = false;
            this.colVisitCategory.OptionsColumn.ReadOnly = true;
            this.colVisitCategory.Visible = true;
            this.colVisitCategory.VisibleIndex = 2;
            this.colVisitCategory.Width = 136;
            // 
            // colVisitCategoryID1
            // 
            this.colVisitCategoryID1.Caption = "Visit Category ID";
            this.colVisitCategoryID1.FieldName = "VisitCategoryID";
            this.colVisitCategoryID1.Name = "colVisitCategoryID1";
            this.colVisitCategoryID1.OptionsColumn.AllowEdit = false;
            this.colVisitCategoryID1.OptionsColumn.AllowFocus = false;
            this.colVisitCategoryID1.OptionsColumn.ReadOnly = true;
            this.colVisitCategoryID1.Width = 102;
            // 
            // colDurationUnits
            // 
            this.colDurationUnits.Caption = "Duration Units";
            this.colDurationUnits.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colDurationUnits.FieldName = "DurationUnits";
            this.colDurationUnits.Name = "colDurationUnits";
            this.colDurationUnits.OptionsColumn.AllowEdit = false;
            this.colDurationUnits.OptionsColumn.AllowFocus = false;
            this.colDurationUnits.OptionsColumn.ReadOnly = true;
            this.colDurationUnits.Visible = true;
            this.colDurationUnits.VisibleIndex = 5;
            this.colDurationUnits.Width = 89;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colDurationUnitsDescriptor
            // 
            this.colDurationUnitsDescriptor.Caption = "Units Descriptor";
            this.colDurationUnitsDescriptor.FieldName = "DurationUnitsDescriptor";
            this.colDurationUnitsDescriptor.Name = "colDurationUnitsDescriptor";
            this.colDurationUnitsDescriptor.OptionsColumn.AllowEdit = false;
            this.colDurationUnitsDescriptor.OptionsColumn.AllowFocus = false;
            this.colDurationUnitsDescriptor.OptionsColumn.ReadOnly = true;
            this.colDurationUnitsDescriptor.Visible = true;
            this.colDurationUnitsDescriptor.VisibleIndex = 6;
            this.colDurationUnitsDescriptor.Width = 95;
            // 
            // colDurationUnitsDescriptorID
            // 
            this.colDurationUnitsDescriptorID.Caption = "Units Descriptor ID";
            this.colDurationUnitsDescriptorID.FieldName = "DurationUnitsDescriptorID";
            this.colDurationUnitsDescriptorID.Name = "colDurationUnitsDescriptorID";
            this.colDurationUnitsDescriptorID.OptionsColumn.AllowEdit = false;
            this.colDurationUnitsDescriptorID.OptionsColumn.AllowFocus = false;
            this.colDurationUnitsDescriptorID.OptionsColumn.ReadOnly = true;
            this.colDurationUnitsDescriptorID.Width = 109;
            // 
            // colSellCalculationLevel
            // 
            this.colSellCalculationLevel.Caption = "Sell Calculation ID";
            this.colSellCalculationLevel.FieldName = "SellCalculationLevel";
            this.colSellCalculationLevel.Name = "colSellCalculationLevel";
            this.colSellCalculationLevel.OptionsColumn.AllowEdit = false;
            this.colSellCalculationLevel.OptionsColumn.AllowFocus = false;
            this.colSellCalculationLevel.OptionsColumn.ReadOnly = true;
            this.colSellCalculationLevel.Width = 104;
            // 
            // colSellCalculationLevelDescriptor
            // 
            this.colSellCalculationLevelDescriptor.Caption = "Sell Calculation";
            this.colSellCalculationLevelDescriptor.FieldName = "SellCalculationLevelDescriptor";
            this.colSellCalculationLevelDescriptor.Name = "colSellCalculationLevelDescriptor";
            this.colSellCalculationLevelDescriptor.OptionsColumn.AllowEdit = false;
            this.colSellCalculationLevelDescriptor.OptionsColumn.AllowFocus = false;
            this.colSellCalculationLevelDescriptor.OptionsColumn.ReadOnly = true;
            this.colSellCalculationLevelDescriptor.Visible = true;
            this.colSellCalculationLevelDescriptor.VisibleIndex = 4;
            this.colSellCalculationLevelDescriptor.Width = 100;
            // 
            // labelControlOccurrences
            // 
            this.labelControlOccurrences.Location = new System.Drawing.Point(424, 224);
            this.labelControlOccurrences.Name = "labelControlOccurrences";
            this.labelControlOccurrences.Size = new System.Drawing.Size(58, 13);
            this.labelControlOccurrences.TabIndex = 40;
            this.labelControlOccurrences.Text = "occurrences";
            this.labelControlOccurrences.Visible = false;
            // 
            // spinEditOccurrenceCount
            // 
            this.spinEditOccurrenceCount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditOccurrenceCount.Enabled = false;
            this.spinEditOccurrenceCount.Location = new System.Drawing.Point(367, 222);
            this.spinEditOccurrenceCount.Name = "spinEditOccurrenceCount";
            this.spinEditOccurrenceCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditOccurrenceCount.Properties.Mask.EditMask = "n0";
            this.spinEditOccurrenceCount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditOccurrenceCount.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.spinEditOccurrenceCount.Size = new System.Drawing.Size(51, 20);
            this.spinEditOccurrenceCount.TabIndex = 39;
            this.spinEditOccurrenceCount.Visible = false;
            // 
            // radioGroupEndType
            // 
            this.radioGroupEndType.EditValue = "EndDate";
            this.radioGroupEndType.Enabled = false;
            this.radioGroupEndType.Location = new System.Drawing.Point(278, 215);
            this.radioGroupEndType.Name = "radioGroupEndType";
            this.radioGroupEndType.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.radioGroupEndType.Properties.Appearance.Options.UseBackColor = true;
            this.radioGroupEndType.Properties.Columns = 1;
            this.radioGroupEndType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Occurrences", "End After"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("EndDate", "End Date")});
            this.radioGroupEndType.Size = new System.Drawing.Size(83, 58);
            this.radioGroupEndType.TabIndex = 38;
            this.radioGroupEndType.Visible = false;
            this.radioGroupEndType.EditValueChanged += new System.EventHandler(this.radioGroupEndType_EditValueChanged);
            // 
            // dateEditRecurrenceEnd
            // 
            this.dateEditRecurrenceEnd.EditValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateEditRecurrenceEnd.Enabled = false;
            this.dateEditRecurrenceEnd.Location = new System.Drawing.Point(367, 248);
            this.dateEditRecurrenceEnd.MenuManager = this.barManager1;
            this.dateEditRecurrenceEnd.Name = "dateEditRecurrenceEnd";
            this.dateEditRecurrenceEnd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditRecurrenceEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditRecurrenceEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditRecurrenceEnd.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditRecurrenceEnd.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateEditRecurrenceEnd.Size = new System.Drawing.Size(100, 20);
            this.dateEditRecurrenceEnd.TabIndex = 37;
            this.dateEditRecurrenceEnd.Visible = false;
            // 
            // yearlyRecurrenceControl1
            // 
            this.yearlyRecurrenceControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.yearlyRecurrenceControl1.Appearance.Options.UseBackColor = true;
            this.yearlyRecurrenceControl1.Location = new System.Drawing.Point(367, 113);
            this.yearlyRecurrenceControl1.Name = "yearlyRecurrenceControl1";
            this.yearlyRecurrenceControl1.Size = new System.Drawing.Size(388, 96);
            this.yearlyRecurrenceControl1.TabIndex = 36;
            this.yearlyRecurrenceControl1.Visible = false;
            // 
            // weeklyRecurrenceControl1
            // 
            this.weeklyRecurrenceControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.weeklyRecurrenceControl1.Appearance.Options.UseBackColor = true;
            this.weeklyRecurrenceControl1.Location = new System.Drawing.Point(367, 113);
            this.weeklyRecurrenceControl1.Name = "weeklyRecurrenceControl1";
            this.weeklyRecurrenceControl1.Size = new System.Drawing.Size(388, 96);
            this.weeklyRecurrenceControl1.TabIndex = 35;
            this.weeklyRecurrenceControl1.Visible = false;
            // 
            // monthlyRecurrenceControl1
            // 
            this.monthlyRecurrenceControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.monthlyRecurrenceControl1.Appearance.Options.UseBackColor = true;
            this.monthlyRecurrenceControl1.Location = new System.Drawing.Point(367, 113);
            this.monthlyRecurrenceControl1.Name = "monthlyRecurrenceControl1";
            this.monthlyRecurrenceControl1.Size = new System.Drawing.Size(388, 96);
            this.monthlyRecurrenceControl1.TabIndex = 34;
            this.monthlyRecurrenceControl1.Visible = false;
            // 
            // dailyRecurrenceControl1
            // 
            this.dailyRecurrenceControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.dailyRecurrenceControl1.Appearance.Options.UseBackColor = true;
            this.dailyRecurrenceControl1.Location = new System.Drawing.Point(367, 113);
            this.dailyRecurrenceControl1.Name = "dailyRecurrenceControl1";
            this.dailyRecurrenceControl1.Size = new System.Drawing.Size(388, 96);
            this.dailyRecurrenceControl1.TabIndex = 33;
            this.dailyRecurrenceControl1.Visible = false;
            // 
            // radioGroupRecurrence
            // 
            this.radioGroupRecurrence.EditValue = "Weekly";
            this.radioGroupRecurrence.Enabled = false;
            this.radioGroupRecurrence.Location = new System.Drawing.Point(278, 99);
            this.radioGroupRecurrence.Name = "radioGroupRecurrence";
            this.radioGroupRecurrence.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.radioGroupRecurrence.Properties.Appearance.Options.UseBackColor = true;
            this.radioGroupRecurrence.Properties.Columns = 1;
            this.radioGroupRecurrence.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Daily", "Daily"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Weekly", "Weekly"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Monthly", "Monthly"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Yearly", "Yearly")});
            this.radioGroupRecurrence.Size = new System.Drawing.Size(83, 110);
            this.radioGroupRecurrence.TabIndex = 32;
            this.radioGroupRecurrence.Visible = false;
            this.radioGroupRecurrence.EditValueChanged += new System.EventHandler(this.radioGroupRecurrence_EditValueChanged);
            // 
            // labelControlDefaultScheduleTemplateUnavailable
            // 
            this.labelControlDefaultScheduleTemplateUnavailable.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlDefaultScheduleTemplateUnavailable.Appearance.ImageIndex = 10;
            this.labelControlDefaultScheduleTemplateUnavailable.Appearance.ImageList = this.imageCollection1;
            this.labelControlDefaultScheduleTemplateUnavailable.Appearance.Options.UseImageAlign = true;
            this.labelControlDefaultScheduleTemplateUnavailable.Appearance.Options.UseImageIndex = true;
            this.labelControlDefaultScheduleTemplateUnavailable.Appearance.Options.UseImageList = true;
            this.labelControlDefaultScheduleTemplateUnavailable.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControlDefaultScheduleTemplateUnavailable.Location = new System.Drawing.Point(278, 30);
            this.labelControlDefaultScheduleTemplateUnavailable.Name = "labelControlDefaultScheduleTemplateUnavailable";
            this.labelControlDefaultScheduleTemplateUnavailable.Size = new System.Drawing.Size(634, 20);
            this.labelControlDefaultScheduleTemplateUnavailable.TabIndex = 31;
            this.labelControlDefaultScheduleTemplateUnavailable.Text = "One or more selected Site Contracts have no Default Schedule Template available -" +
    " Use Default Schedule Template Unavailable.";
            this.labelControlDefaultScheduleTemplateUnavailable.Visible = false;
            // 
            // labelControlNumberOfVisits
            // 
            this.labelControlNumberOfVisits.Location = new System.Drawing.Point(245, 59);
            this.labelControlNumberOfVisits.Name = "labelControlNumberOfVisits";
            this.labelControlNumberOfVisits.Size = new System.Drawing.Size(81, 13);
            this.labelControlNumberOfVisits.TabIndex = 24;
            this.labelControlNumberOfVisits.Text = "Number of Visits:";
            // 
            // gridLookUpEditSellCalculationLevel
            // 
            this.gridLookUpEditSellCalculationLevel.EditValue = 1;
            this.gridLookUpEditSellCalculationLevel.Location = new System.Drawing.Point(990, 56);
            this.gridLookUpEditSellCalculationLevel.MenuManager = this.barManager1;
            this.gridLookUpEditSellCalculationLevel.Name = "gridLookUpEditSellCalculationLevel";
            this.gridLookUpEditSellCalculationLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditSellCalculationLevel.Properties.DataSource = this.sp06134OMJobCalculationLevelDescriptorsBindingSource;
            this.gridLookUpEditSellCalculationLevel.Properties.DisplayMember = "Description";
            this.gridLookUpEditSellCalculationLevel.Properties.NullText = "";
            this.gridLookUpEditSellCalculationLevel.Properties.PopupView = this.gridView8;
            this.gridLookUpEditSellCalculationLevel.Properties.ValueMember = "ID";
            this.gridLookUpEditSellCalculationLevel.Size = new System.Drawing.Size(133, 20);
            this.gridLookUpEditSellCalculationLevel.TabIndex = 30;
            // 
            // sp06134OMJobCalculationLevelDescriptorsBindingSource
            // 
            this.sp06134OMJobCalculationLevelDescriptorsBindingSource.DataMember = "sp06134_OM_Job_Calculation_Level_Descriptors";
            this.sp06134OMJobCalculationLevelDescriptorsBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.gridColumn10;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = -1;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView8.FormatRules.Add(gridFormatRule2);
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView8.OptionsView.ShowIndicator = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Cost Calculation Level";
            this.gridColumn11.FieldName = "Description";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            this.gridColumn11.Width = 220;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Order";
            this.gridColumn12.FieldName = "RecordOrder";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            // 
            // labelControlSellCalculationLevel
            // 
            this.labelControlSellCalculationLevel.Location = new System.Drawing.Point(909, 59);
            this.labelControlSellCalculationLevel.Name = "labelControlSellCalculationLevel";
            this.labelControlSellCalculationLevel.Size = new System.Drawing.Size(75, 13);
            this.labelControlSellCalculationLevel.TabIndex = 30;
            this.labelControlSellCalculationLevel.Text = "Sell Calculation:";
            // 
            // gridLookUpEditCostCalculationLevel
            // 
            this.gridLookUpEditCostCalculationLevel.EditValue = 1;
            this.gridLookUpEditCostCalculationLevel.Location = new System.Drawing.Point(757, 56);
            this.gridLookUpEditCostCalculationLevel.MenuManager = this.barManager1;
            this.gridLookUpEditCostCalculationLevel.Name = "gridLookUpEditCostCalculationLevel";
            this.gridLookUpEditCostCalculationLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditCostCalculationLevel.Properties.DataSource = this.sp06134OMJobCalculationLevelDescriptorsBindingSource;
            this.gridLookUpEditCostCalculationLevel.Properties.DisplayMember = "Description";
            this.gridLookUpEditCostCalculationLevel.Properties.NullText = "";
            this.gridLookUpEditCostCalculationLevel.Properties.PopupView = this.gridView17;
            this.gridLookUpEditCostCalculationLevel.Properties.ValueMember = "ID";
            this.gridLookUpEditCostCalculationLevel.Size = new System.Drawing.Size(133, 20);
            this.gridLookUpEditCostCalculationLevel.TabIndex = 29;
            // 
            // gridView17
            // 
            this.gridView17.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn129,
            this.gridColumn130,
            this.gridColumn131});
            this.gridView17.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Column = this.gridColumn129;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue3.Value1 = -1;
            gridFormatRule3.Rule = formatConditionRuleValue3;
            this.gridView17.FormatRules.Add(gridFormatRule3);
            this.gridView17.Name = "gridView17";
            this.gridView17.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView17.OptionsLayout.StoreAppearance = true;
            this.gridView17.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView17.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView17.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView17.OptionsView.ColumnAutoWidth = false;
            this.gridView17.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView17.OptionsView.ShowGroupPanel = false;
            this.gridView17.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView17.OptionsView.ShowIndicator = false;
            this.gridView17.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn131, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn130
            // 
            this.gridColumn130.Caption = "Cost Calculation Level";
            this.gridColumn130.FieldName = "Description";
            this.gridColumn130.Name = "gridColumn130";
            this.gridColumn130.OptionsColumn.AllowEdit = false;
            this.gridColumn130.OptionsColumn.AllowFocus = false;
            this.gridColumn130.OptionsColumn.ReadOnly = true;
            this.gridColumn130.Visible = true;
            this.gridColumn130.VisibleIndex = 0;
            this.gridColumn130.Width = 220;
            // 
            // gridColumn131
            // 
            this.gridColumn131.Caption = "Order";
            this.gridColumn131.FieldName = "RecordOrder";
            this.gridColumn131.Name = "gridColumn131";
            this.gridColumn131.OptionsColumn.AllowEdit = false;
            this.gridColumn131.OptionsColumn.AllowFocus = false;
            this.gridColumn131.OptionsColumn.ReadOnly = true;
            // 
            // labelControlCostCalculationLevel
            // 
            this.labelControlCostCalculationLevel.Location = new System.Drawing.Point(671, 59);
            this.labelControlCostCalculationLevel.Name = "labelControlCostCalculationLevel";
            this.labelControlCostCalculationLevel.Size = new System.Drawing.Size(81, 13);
            this.labelControlCostCalculationLevel.TabIndex = 28;
            this.labelControlCostCalculationLevel.Text = "Cost Calculation:";
            // 
            // radioGroupNumberOfVisitsMethod
            // 
            this.radioGroupNumberOfVisitsMethod.EditValue = 0;
            this.radioGroupNumberOfVisitsMethod.Location = new System.Drawing.Point(95, 24);
            this.radioGroupNumberOfVisitsMethod.MenuManager = this.barManager1;
            this.radioGroupNumberOfVisitsMethod.Name = "radioGroupNumberOfVisitsMethod";
            this.radioGroupNumberOfVisitsMethod.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.radioGroupNumberOfVisitsMethod.Properties.Appearance.Options.UseBackColor = true;
            this.radioGroupNumberOfVisitsMethod.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.radioGroupNumberOfVisitsMethod.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Use Default Schedule Template"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Enter Number of Visits"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Choose From Template"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Recurrence")});
            this.radioGroupNumberOfVisitsMethod.Size = new System.Drawing.Size(177, 108);
            this.radioGroupNumberOfVisitsMethod.TabIndex = 28;
            this.radioGroupNumberOfVisitsMethod.EditValueChanged += new System.EventHandler(this.radioGroupNumberOfVisitsMethod_EditValueChanged);
            // 
            // gridLookUpEditGapBetweenVisitsDescriptor
            // 
            this.gridLookUpEditGapBetweenVisitsDescriptor.EditValue = 2;
            this.gridLookUpEditGapBetweenVisitsDescriptor.Location = new System.Drawing.Point(568, 56);
            this.gridLookUpEditGapBetweenVisitsDescriptor.MenuManager = this.barManager1;
            this.gridLookUpEditGapBetweenVisitsDescriptor.Name = "gridLookUpEditGapBetweenVisitsDescriptor";
            this.gridLookUpEditGapBetweenVisitsDescriptor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditGapBetweenVisitsDescriptor.Properties.DataSource = this.sp06133OMVisitGapUnitDescriptorsBindingSource;
            this.gridLookUpEditGapBetweenVisitsDescriptor.Properties.DisplayMember = "Description";
            this.gridLookUpEditGapBetweenVisitsDescriptor.Properties.NullText = "";
            this.gridLookUpEditGapBetweenVisitsDescriptor.Properties.PopupView = this.gridLookUpEdit1View;
            this.gridLookUpEditGapBetweenVisitsDescriptor.Properties.ValueMember = "ID";
            this.gridLookUpEditGapBetweenVisitsDescriptor.Size = new System.Drawing.Size(81, 20);
            this.gridLookUpEditGapBetweenVisitsDescriptor.TabIndex = 27;
            // 
            // sp06133OMVisitGapUnitDescriptorsBindingSource
            // 
            this.sp06133OMVisitGapUnitDescriptorsBindingSource.DataMember = "sp06133_OM_Visit_Gap_Unit_Descriptors";
            this.sp06133OMVisitGapUnitDescriptorsBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn126,
            this.gridColumn127,
            this.gridColumn128});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition5.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition5.Appearance.Options.UseForeColor = true;
            styleFormatCondition5.ApplyToRow = true;
            styleFormatCondition5.Column = this.gridColumn126;
            styleFormatCondition5.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition5.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition5});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn128, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn127
            // 
            this.gridColumn127.Caption = "Gap Descriptor";
            this.gridColumn127.FieldName = "Description";
            this.gridColumn127.Name = "gridColumn127";
            this.gridColumn127.OptionsColumn.AllowEdit = false;
            this.gridColumn127.OptionsColumn.AllowFocus = false;
            this.gridColumn127.OptionsColumn.ReadOnly = true;
            this.gridColumn127.Visible = true;
            this.gridColumn127.VisibleIndex = 0;
            this.gridColumn127.Width = 220;
            // 
            // gridColumn128
            // 
            this.gridColumn128.Caption = "Order";
            this.gridColumn128.FieldName = "RecordOrder";
            this.gridColumn128.Name = "gridColumn128";
            this.gridColumn128.OptionsColumn.AllowEdit = false;
            this.gridColumn128.OptionsColumn.AllowFocus = false;
            this.gridColumn128.OptionsColumn.ReadOnly = true;
            // 
            // spinEditGapBetweenVisitsUnits
            // 
            this.spinEditGapBetweenVisitsUnits.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditGapBetweenVisitsUnits.Location = new System.Drawing.Point(510, 56);
            this.spinEditGapBetweenVisitsUnits.MenuManager = this.barManager1;
            this.spinEditGapBetweenVisitsUnits.Name = "spinEditGapBetweenVisitsUnits";
            this.spinEditGapBetweenVisitsUnits.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditGapBetweenVisitsUnits.Properties.IsFloatValue = false;
            this.spinEditGapBetweenVisitsUnits.Properties.Mask.EditMask = "n0";
            this.spinEditGapBetweenVisitsUnits.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditGapBetweenVisitsUnits.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.spinEditGapBetweenVisitsUnits.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditGapBetweenVisitsUnits.Size = new System.Drawing.Size(53, 20);
            this.spinEditGapBetweenVisitsUnits.TabIndex = 26;
            // 
            // labelControlGapBetweenVisits
            // 
            this.labelControlGapBetweenVisits.Location = new System.Drawing.Point(409, 59);
            this.labelControlGapBetweenVisits.Name = "labelControlGapBetweenVisits";
            this.labelControlGapBetweenVisits.Size = new System.Drawing.Size(95, 13);
            this.labelControlGapBetweenVisits.TabIndex = 25;
            this.labelControlGapBetweenVisits.Text = "Gap Between Visits:";
            // 
            // spinEditNumberOfVisits
            // 
            this.spinEditNumberOfVisits.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditNumberOfVisits.Location = new System.Drawing.Point(332, 56);
            this.spinEditNumberOfVisits.MenuManager = this.barManager1;
            this.spinEditNumberOfVisits.Name = "spinEditNumberOfVisits";
            this.spinEditNumberOfVisits.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditNumberOfVisits.Properties.IsFloatValue = false;
            this.spinEditNumberOfVisits.Properties.Mask.EditMask = "n0";
            this.spinEditNumberOfVisits.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditNumberOfVisits.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.spinEditNumberOfVisits.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditNumberOfVisits.Size = new System.Drawing.Size(53, 20);
            this.spinEditNumberOfVisits.TabIndex = 23;
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(14, 33);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(79, 13);
            this.labelControl27.TabIndex = 0;
            this.labelControl27.Text = "Choose Method:";
            // 
            // btnStep3Next
            // 
            this.btnStep3Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep3Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep3Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep3Next.Location = new System.Drawing.Point(1117, 501);
            this.btnStep3Next.Name = "btnStep3Next";
            this.btnStep3Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep3Next.TabIndex = 17;
            this.btnStep3Next.Text = "Next";
            this.btnStep3Next.Click += new System.EventHandler(this.btnStep3Next_Click);
            // 
            // btnStep3Previous
            // 
            this.btnStep3Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep3Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep3Previous.Location = new System.Drawing.Point(1023, 501);
            this.btnStep3Previous.Name = "btnStep3Previous";
            this.btnStep3Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep3Previous.TabIndex = 18;
            this.btnStep3Previous.Text = "Previous";
            this.btnStep3Previous.Click += new System.EventHandler(this.btnStep3Previous_Click);
            // 
            // panelControl8
            // 
            this.panelControl8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl8.Controls.Add(this.SelectedSiteContractCountTextEdit);
            this.panelControl8.Controls.Add(this.pictureEdit10);
            this.panelControl8.Controls.Add(this.labelControl19);
            this.panelControl8.Controls.Add(this.labelControl20);
            this.panelControl8.Location = new System.Drawing.Point(7, 6);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(1198, 48);
            this.panelControl8.TabIndex = 16;
            // 
            // SelectedSiteContractCountTextEdit
            // 
            this.SelectedSiteContractCountTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.SelectedSiteContractCountTextEdit.Location = new System.Drawing.Point(680, 25);
            this.SelectedSiteContractCountTextEdit.MenuManager = this.barManager1;
            this.SelectedSiteContractCountTextEdit.Name = "SelectedSiteContractCountTextEdit";
            this.SelectedSiteContractCountTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.SelectedSiteContractCountTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SelectedSiteContractCountTextEdit.Properties.Mask.EditMask = "n0";
            this.SelectedSiteContractCountTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.SelectedSiteContractCountTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SelectedSiteContractCountTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SelectedSiteContractCountTextEdit, true);
            this.SelectedSiteContractCountTextEdit.Size = new System.Drawing.Size(48, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SelectedSiteContractCountTextEdit, optionsSpelling2);
            this.SelectedSiteContractCountTextEdit.TabIndex = 10;
            // 
            // pictureEdit10
            // 
            this.pictureEdit10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit10.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit10.Location = new System.Drawing.Point(1154, 4);
            this.pictureEdit10.MenuManager = this.barManager1;
            this.pictureEdit10.Name = "pictureEdit10";
            this.pictureEdit10.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit10.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit10.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit10.Properties.ReadOnly = true;
            this.pictureEdit10.Properties.ShowMenu = false;
            this.pictureEdit10.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit10.TabIndex = 9;
            // 
            // labelControl19
            // 
            this.labelControl19.AllowHtmlString = true;
            this.labelControl19.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Appearance.Options.UseBackColor = true;
            this.labelControl19.Appearance.Options.UseFont = true;
            this.labelControl19.Location = new System.Drawing.Point(5, 5);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(190, 16);
            this.labelControl19.TabIndex = 6;
            this.labelControl19.Text = "<b>Step 3:</b> Specify Number of Visits\r\n";
            // 
            // labelControl20
            // 
            this.labelControl20.AllowHtmlString = true;
            this.labelControl20.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl20.Appearance.Options.UseBackColor = true;
            this.labelControl20.Location = new System.Drawing.Point(57, 29);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(617, 13);
            this.labelControl20.TabIndex = 7;
            this.labelControl20.Text = "Select \\ Enter the number of visits to create for each selected Site Contract. Cl" +
    "ick Next when done.  <b>Selected Site Contracts:</b>";
            // 
            // xtraTabPageStep4
            // 
            this.xtraTabPageStep4.Controls.Add(this.panelControl12);
            this.xtraTabPageStep4.Controls.Add(this.gridControl4);
            this.xtraTabPageStep4.Controls.Add(this.btnStep4Next);
            this.xtraTabPageStep4.Controls.Add(this.btnStep4Previous);
            this.xtraTabPageStep4.Controls.Add(this.panelControl5);
            this.xtraTabPageStep4.Name = "xtraTabPageStep4";
            this.xtraTabPageStep4.Size = new System.Drawing.Size(1211, 537);
            this.xtraTabPageStep4.Tag = "4";
            this.xtraTabPageStep4.Text = "Step 4";
            // 
            // panelControl12
            // 
            this.panelControl12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelControl12.Controls.Add(this.simpleButtonApplyDate1);
            this.panelControl12.Controls.Add(this.dateEditApplyDate1);
            this.panelControl12.Controls.Add(this.labelControl28);
            this.panelControl12.Location = new System.Drawing.Point(7, 501);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(348, 30);
            this.panelControl12.TabIndex = 24;
            // 
            // simpleButtonApplyDate1
            // 
            this.simpleButtonApplyDate1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonApplyDate1.ImageOptions.Image")));
            this.simpleButtonApplyDate1.Location = new System.Drawing.Point(285, 4);
            this.simpleButtonApplyDate1.Name = "simpleButtonApplyDate1";
            this.simpleButtonApplyDate1.Size = new System.Drawing.Size(59, 22);
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Apply Visit Start Dates To Selected Site Contracts - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to apply the selected Date to the <b>selected</b> Site Contracts.\r\n\r\nOnl" +
    "y selected Site Contracts will be effected. All others will be left as is.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.simpleButtonApplyDate1.SuperTip = superToolTip6;
            this.simpleButtonApplyDate1.TabIndex = 2;
            this.simpleButtonApplyDate1.Text = "Apply";
            this.simpleButtonApplyDate1.Click += new System.EventHandler(this.simpleButtonApplyDate1_Click);
            // 
            // dateEditApplyDate1
            // 
            this.dateEditApplyDate1.EditValue = null;
            this.dateEditApplyDate1.Location = new System.Drawing.Point(170, 5);
            this.dateEditApplyDate1.MenuManager = this.barManager1;
            this.dateEditApplyDate1.Name = "dateEditApplyDate1";
            this.dateEditApplyDate1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditApplyDate1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditApplyDate1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditApplyDate1.Properties.Mask.EditMask = "g";
            this.dateEditApplyDate1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditApplyDate1.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditApplyDate1.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateEditApplyDate1.Size = new System.Drawing.Size(111, 20);
            this.dateEditApplyDate1.TabIndex = 1;
            // 
            // labelControl28
            // 
            this.labelControl28.Location = new System.Drawing.Point(6, 8);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(159, 13);
            this.labelControl28.TabIndex = 0;
            this.labelControl28.Text = "Set Selected Visit Start Dates To:";
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.sp06144OMVisitWizardSiteContractStartDatesBindingSource;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.Location = new System.Drawing.Point(7, 62);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit6,
            this.repositoryItemMemoExEdit8,
            this.repositoryItemTextEditDateTime4,
            this.repositoryItemTextEditCurrency4,
            this.repositoryItemTextEditPercentage4,
            this.repositoryItemTextEditInteger4,
            this.repositoryItemDateEdit2});
            this.gridControl4.Size = new System.Drawing.Size(1198, 433);
            this.gridControl4.TabIndex = 23;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp06144OMVisitWizardSiteContractStartDatesBindingSource
            // 
            this.sp06144OMVisitWizardSiteContractStartDatesBindingSource.DataMember = "sp06144_OM_Visit_Wizard_Site_Contract_Start_Dates";
            this.sp06144OMVisitWizardSiteContractStartDatesBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn132,
            this.colVisitStartDate,
            this.colClientName2,
            this.colContractDescription2,
            this.colOnHold1,
            this.colOnHoldReasonID1,
            this.colOnHoldReason1,
            this.colOnHoldStartDate1,
            this.colOnHoldEndDate1,
            this.colSiteContractDaysSeparationPercent1});
            gridFormatRule11.ApplyToRow = true;
            gridFormatRule11.Column = this.gridColumn6;
            gridFormatRule11.Name = "FormatActive";
            formatConditionRuleValue11.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue11.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue11.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue11.Value1 = 0;
            gridFormatRule11.Rule = formatConditionRuleValue11;
            gridFormatRule12.ApplyToRow = true;
            gridFormatRule12.Column = this.colOnHold1;
            gridFormatRule12.Name = "FormatOnHold";
            formatConditionRuleValue12.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue12.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue12.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue12.Value1 = 1;
            gridFormatRule12.Rule = formatConditionRuleValue12;
            this.gridView4.FormatRules.Add(gridFormatRule11);
            this.gridView4.FormatRules.Add(gridFormatRule12);
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 2;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsFind.AlwaysVisible = true;
            this.gridView4.OptionsFind.FindDelay = 2000;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsLayout.StoreFormatRules = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractDescription2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn23, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colVisitStartDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Site Contract ID";
            this.gridColumn1.FieldName = "SiteContractID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 98;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Client Contract ID";
            this.gridColumn2.FieldName = "ClientContractID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 107;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Site ID";
            this.gridColumn3.FieldName = "SiteID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 90;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Contract Start Date";
            this.gridColumn4.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.gridColumn4.FieldName = "StartDate";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 116;
            // 
            // repositoryItemTextEditDateTime4
            // 
            this.repositoryItemTextEditDateTime4.AutoHeight = false;
            this.repositoryItemTextEditDateTime4.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime4.Name = "repositoryItemTextEditDateTime4";
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Contract End Date";
            this.gridColumn5.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.gridColumn5.FieldName = "EndDate";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 110;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Value";
            this.gridColumn7.ColumnEdit = this.repositoryItemTextEditCurrency4;
            this.gridColumn7.FieldName = "ContractValue";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ContractValue", "{0:c}")});
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            // 
            // repositoryItemTextEditCurrency4
            // 
            this.repositoryItemTextEditCurrency4.AutoHeight = false;
            this.repositoryItemTextEditCurrency4.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency4.Name = "repositoryItemTextEditCurrency4";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Yearly % Increase";
            this.gridColumn8.ColumnEdit = this.repositoryItemTextEditPercentage4;
            this.gridColumn8.FieldName = "YearlyPercentageIncrease";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Width = 110;
            // 
            // repositoryItemTextEditPercentage4
            // 
            this.repositoryItemTextEditPercentage4.AutoHeight = false;
            this.repositoryItemTextEditPercentage4.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage4.Name = "repositoryItemTextEditPercentage4";
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Last Client Payment";
            this.gridColumn9.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.gridColumn9.FieldName = "LastClientPaymentDate";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Width = 116;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Remarks";
            this.gridColumn21.ColumnEdit = this.repositoryItemMemoExEdit8;
            this.gridColumn21.FieldName = "Remarks";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 11;
            // 
            // repositoryItemMemoExEdit8
            // 
            this.repositoryItemMemoExEdit8.AutoHeight = false;
            this.repositoryItemMemoExEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit8.Name = "repositoryItemMemoExEdit8";
            this.repositoryItemMemoExEdit8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit8.ShowIcon = false;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Client ID";
            this.gridColumn22.FieldName = "ClientID";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Site Name";
            this.gridColumn23.FieldName = "SiteName";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 1;
            this.gridColumn23.Width = 255;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Linked To Contract";
            this.gridColumn24.FieldName = "LinkedToParent";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Width = 273;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Existing Visit Count";
            this.gridColumn25.ColumnEdit = this.repositoryItemTextEditInteger4;
            this.gridColumn25.FieldName = "VisitCount";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitCount", "{0:n0}")});
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 12;
            this.gridColumn25.Width = 112;
            // 
            // repositoryItemTextEditInteger4
            // 
            this.repositoryItemTextEditInteger4.AutoHeight = false;
            this.repositoryItemTextEditInteger4.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger4.Name = "repositoryItemTextEditInteger4";
            // 
            // gridColumn132
            // 
            this.gridColumn132.Caption = "Current Max Visit #";
            this.gridColumn132.FieldName = "VisitNumberMax";
            this.gridColumn132.Name = "gridColumn132";
            this.gridColumn132.OptionsColumn.AllowEdit = false;
            this.gridColumn132.OptionsColumn.AllowFocus = false;
            this.gridColumn132.OptionsColumn.ReadOnly = true;
            this.gridColumn132.Visible = true;
            this.gridColumn132.VisibleIndex = 13;
            this.gridColumn132.Width = 114;
            // 
            // colVisitStartDate
            // 
            this.colVisitStartDate.Caption = "Visit Start Date";
            this.colVisitStartDate.ColumnEdit = this.repositoryItemDateEdit2;
            this.colVisitStartDate.FieldName = "VisitStartDate";
            this.colVisitStartDate.Name = "colVisitStartDate";
            this.colVisitStartDate.Visible = true;
            this.colVisitStartDate.VisibleIndex = 2;
            this.colVisitStartDate.Width = 120;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.Mask.EditMask = "g";
            this.repositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Visible = true;
            this.colClientName2.VisibleIndex = 10;
            this.colClientName2.Width = 181;
            // 
            // colContractDescription2
            // 
            this.colContractDescription2.Caption = "Contract Description";
            this.colContractDescription2.FieldName = "ContractDescription";
            this.colContractDescription2.Name = "colContractDescription2";
            this.colContractDescription2.OptionsColumn.AllowEdit = false;
            this.colContractDescription2.OptionsColumn.AllowFocus = false;
            this.colContractDescription2.OptionsColumn.ReadOnly = true;
            this.colContractDescription2.Visible = true;
            this.colContractDescription2.VisibleIndex = 14;
            this.colContractDescription2.Width = 288;
            // 
            // colOnHoldReasonID1
            // 
            this.colOnHoldReasonID1.Caption = "On-Hold Reason ID";
            this.colOnHoldReasonID1.FieldName = "OnHoldReasonID";
            this.colOnHoldReasonID1.Name = "colOnHoldReasonID1";
            this.colOnHoldReasonID1.OptionsColumn.AllowEdit = false;
            this.colOnHoldReasonID1.OptionsColumn.AllowFocus = false;
            this.colOnHoldReasonID1.OptionsColumn.ReadOnly = true;
            this.colOnHoldReasonID1.Width = 111;
            // 
            // colOnHoldReason1
            // 
            this.colOnHoldReason1.Caption = "On-Hold Reason";
            this.colOnHoldReason1.FieldName = "OnHoldReason";
            this.colOnHoldReason1.Name = "colOnHoldReason1";
            this.colOnHoldReason1.OptionsColumn.AllowEdit = false;
            this.colOnHoldReason1.OptionsColumn.AllowFocus = false;
            this.colOnHoldReason1.OptionsColumn.ReadOnly = true;
            this.colOnHoldReason1.Visible = true;
            this.colOnHoldReason1.VisibleIndex = 8;
            this.colOnHoldReason1.Width = 174;
            // 
            // colOnHoldStartDate1
            // 
            this.colOnHoldStartDate1.Caption = "On-Hold Start";
            this.colOnHoldStartDate1.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colOnHoldStartDate1.FieldName = "OnHoldStartDate";
            this.colOnHoldStartDate1.Name = "colOnHoldStartDate1";
            this.colOnHoldStartDate1.OptionsColumn.AllowEdit = false;
            this.colOnHoldStartDate1.OptionsColumn.AllowFocus = false;
            this.colOnHoldStartDate1.OptionsColumn.ReadOnly = true;
            this.colOnHoldStartDate1.Visible = true;
            this.colOnHoldStartDate1.VisibleIndex = 9;
            this.colOnHoldStartDate1.Width = 100;
            // 
            // colOnHoldEndDate1
            // 
            this.colOnHoldEndDate1.Caption = "On-Hold End";
            this.colOnHoldEndDate1.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colOnHoldEndDate1.FieldName = "OnHoldEndDate";
            this.colOnHoldEndDate1.Name = "colOnHoldEndDate1";
            this.colOnHoldEndDate1.OptionsColumn.AllowEdit = false;
            this.colOnHoldEndDate1.OptionsColumn.AllowFocus = false;
            this.colOnHoldEndDate1.OptionsColumn.ReadOnly = true;
            this.colOnHoldEndDate1.Visible = true;
            this.colOnHoldEndDate1.VisibleIndex = 10;
            this.colOnHoldEndDate1.Width = 100;
            // 
            // colSiteContractDaysSeparationPercent1
            // 
            this.colSiteContractDaysSeparationPercent1.Caption = "Visit Days Separation %";
            this.colSiteContractDaysSeparationPercent1.ColumnEdit = this.repositoryItemTextEditPercentage4;
            this.colSiteContractDaysSeparationPercent1.FieldName = "SiteContractDaysSeparationPercent";
            this.colSiteContractDaysSeparationPercent1.Name = "colSiteContractDaysSeparationPercent1";
            this.colSiteContractDaysSeparationPercent1.OptionsColumn.AllowEdit = false;
            this.colSiteContractDaysSeparationPercent1.OptionsColumn.AllowFocus = false;
            this.colSiteContractDaysSeparationPercent1.OptionsColumn.ReadOnly = true;
            this.colSiteContractDaysSeparationPercent1.Width = 134;
            // 
            // btnStep4Next
            // 
            this.btnStep4Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep4Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep4Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep4Next.Location = new System.Drawing.Point(1117, 501);
            this.btnStep4Next.Name = "btnStep4Next";
            this.btnStep4Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep4Next.TabIndex = 17;
            this.btnStep4Next.Text = "Next";
            this.btnStep4Next.Click += new System.EventHandler(this.btnStep4Next_Click);
            // 
            // btnStep4Previous
            // 
            this.btnStep4Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep4Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep4Previous.Location = new System.Drawing.Point(1023, 501);
            this.btnStep4Previous.Name = "btnStep4Previous";
            this.btnStep4Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep4Previous.TabIndex = 18;
            this.btnStep4Previous.Text = "Previous";
            this.btnStep4Previous.Click += new System.EventHandler(this.btnStep4Previous_Click);
            // 
            // panelControl5
            // 
            this.panelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl5.Controls.Add(this.pictureEdit7);
            this.panelControl5.Controls.Add(this.labelControl4);
            this.panelControl5.Controls.Add(this.labelControl6);
            this.panelControl5.Location = new System.Drawing.Point(7, 6);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1198, 48);
            this.panelControl5.TabIndex = 16;
            // 
            // pictureEdit7
            // 
            this.pictureEdit7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit7.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit7.Location = new System.Drawing.Point(1154, 4);
            this.pictureEdit7.MenuManager = this.barManager1;
            this.pictureEdit7.Name = "pictureEdit7";
            this.pictureEdit7.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit7.Properties.ReadOnly = true;
            this.pictureEdit7.Properties.ShowMenu = false;
            this.pictureEdit7.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit7.TabIndex = 9;
            // 
            // labelControl4
            // 
            this.labelControl4.AllowHtmlString = true;
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseBackColor = true;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(5, 5);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(166, 16);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "<b>Step 4:</b> Set Visit Start Dates";
            // 
            // labelControl6
            // 
            this.labelControl6.AllowHtmlString = true;
            this.labelControl6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl6.Appearance.Options.UseBackColor = true;
            this.labelControl6.Location = new System.Drawing.Point(57, 29);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(393, 13);
            this.labelControl6.TabIndex = 7;
            this.labelControl6.Text = "Select each site then enter the start date for the first visit. Click Next when d" +
    "one.";
            // 
            // xtraTabPageStep5
            // 
            this.xtraTabPageStep5.Controls.Add(this.panelControl10);
            this.xtraTabPageStep5.Controls.Add(this.panelControl9);
            this.xtraTabPageStep5.Controls.Add(this.panelControl7);
            this.xtraTabPageStep5.Controls.Add(this.xtraTabControl2);
            this.xtraTabPageStep5.Controls.Add(this.btnStep5Next);
            this.xtraTabPageStep5.Controls.Add(this.btnStep5Previous);
            this.xtraTabPageStep5.Controls.Add(this.panelControl6);
            this.xtraTabPageStep5.Name = "xtraTabPageStep5";
            this.xtraTabPageStep5.Size = new System.Drawing.Size(1211, 537);
            this.xtraTabPageStep5.Tag = "5";
            this.xtraTabPageStep5.Text = "Step 5";
            // 
            // panelControl10
            // 
            this.panelControl10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelControl10.Controls.Add(this.btnApplyVisitNumber);
            this.panelControl10.Controls.Add(this.VisitNumberSpinEdit);
            this.panelControl10.Controls.Add(this.labelControl22);
            this.panelControl10.Location = new System.Drawing.Point(12, 501);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(174, 30);
            this.panelControl10.TabIndex = 30;
            // 
            // btnApplyVisitNumber
            // 
            this.btnApplyVisitNumber.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnApplyVisitNumber.ImageOptions.Image")));
            this.btnApplyVisitNumber.Location = new System.Drawing.Point(111, 4);
            this.btnApplyVisitNumber.Name = "btnApplyVisitNumber";
            this.btnApplyVisitNumber.Size = new System.Drawing.Size(59, 22);
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Apply Visit Days Separation - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to apply the entered Visit Days Separation to the <b>selected</b> Visits" +
    ".\r\n\r\nOnly selected Visits will be effected. All others will be left as is.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.btnApplyVisitNumber.SuperTip = superToolTip7;
            this.btnApplyVisitNumber.TabIndex = 30;
            this.btnApplyVisitNumber.Text = "Apply";
            this.btnApplyVisitNumber.Click += new System.EventHandler(this.btnApplyVisitNumber_Click);
            // 
            // VisitNumberSpinEdit
            // 
            this.VisitNumberSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.VisitNumberSpinEdit.Location = new System.Drawing.Point(45, 5);
            this.VisitNumberSpinEdit.MenuManager = this.barManager1;
            this.VisitNumberSpinEdit.Name = "VisitNumberSpinEdit";
            this.VisitNumberSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitNumberSpinEdit.Properties.IsFloatValue = false;
            this.VisitNumberSpinEdit.Properties.Mask.EditMask = "n0";
            this.VisitNumberSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.VisitNumberSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.VisitNumberSpinEdit.Size = new System.Drawing.Size(62, 20);
            this.VisitNumberSpinEdit.TabIndex = 31;
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(5, 8);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(34, 13);
            this.labelControl22.TabIndex = 32;
            this.labelControl22.Text = "Visit #:";
            // 
            // panelControl9
            // 
            this.panelControl9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelControl9.Controls.Add(this.VisitTimeEdit);
            this.panelControl9.Controls.Add(this.labelControl21);
            this.panelControl9.Controls.Add(this.VisitDescriptorComboBoxEdit);
            this.panelControl9.Controls.Add(this.labelControl16);
            this.panelControl9.Controls.Add(this.VisitDurationSpinEdit);
            this.panelControl9.Controls.Add(this.btnApplyVisitDuration);
            this.panelControl9.Location = new System.Drawing.Point(551, 501);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(419, 30);
            this.panelControl9.TabIndex = 29;
            // 
            // VisitTimeEdit
            // 
            this.VisitTimeEdit.EditValue = System.TimeSpan.Parse("00:00:00");
            this.VisitTimeEdit.Location = new System.Drawing.Point(303, 6);
            this.VisitTimeEdit.MenuManager = this.barManager1;
            this.VisitTimeEdit.Name = "VisitTimeEdit";
            this.VisitTimeEdit.Properties.AllowEditDays = false;
            this.VisitTimeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitTimeEdit.Properties.Mask.EditMask = "HH:mm";
            this.VisitTimeEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.VisitTimeEdit.Properties.MaxDays = 0;
            this.VisitTimeEdit.Properties.MaxMilliseconds = 0;
            this.VisitTimeEdit.Properties.MaxSeconds = 0;
            this.VisitTimeEdit.Size = new System.Drawing.Size(49, 20);
            this.VisitTimeEdit.TabIndex = 46;
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(272, 9);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(26, 13);
            this.labelControl21.TabIndex = 33;
            this.labelControl21.Text = "Time:";
            // 
            // VisitDescriptorComboBoxEdit
            // 
            this.VisitDescriptorComboBoxEdit.EditValue = "Days";
            this.VisitDescriptorComboBoxEdit.Location = new System.Drawing.Point(162, 5);
            this.VisitDescriptorComboBoxEdit.MenuManager = this.barManager1;
            this.VisitDescriptorComboBoxEdit.Name = "VisitDescriptorComboBoxEdit";
            this.VisitDescriptorComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitDescriptorComboBoxEdit.Properties.Items.AddRange(new object[] {
            "Minutes",
            "Hours",
            "Days",
            "Weeks",
            "Months",
            "Years"});
            this.VisitDescriptorComboBoxEdit.Size = new System.Drawing.Size(100, 20);
            this.VisitDescriptorComboBoxEdit.TabIndex = 32;
            this.VisitDescriptorComboBoxEdit.EditValueChanged += new System.EventHandler(this.VisitDescriptorComboBoxEdit_EditValueChanged);
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(5, 8);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(67, 13);
            this.labelControl16.TabIndex = 31;
            this.labelControl16.Text = "Visit Duration:";
            // 
            // VisitDurationSpinEdit
            // 
            this.VisitDurationSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.VisitDurationSpinEdit.Location = new System.Drawing.Point(78, 5);
            this.VisitDurationSpinEdit.MenuManager = this.barManager1;
            this.VisitDurationSpinEdit.Name = "VisitDurationSpinEdit";
            this.VisitDurationSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitDurationSpinEdit.Properties.IsFloatValue = false;
            this.VisitDurationSpinEdit.Properties.Mask.EditMask = "n0";
            this.VisitDurationSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.VisitDurationSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.VisitDurationSpinEdit.Size = new System.Drawing.Size(80, 20);
            this.VisitDurationSpinEdit.TabIndex = 30;
            // 
            // btnApplyVisitDuration
            // 
            this.btnApplyVisitDuration.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnApplyVisitDuration.ImageOptions.Image")));
            this.btnApplyVisitDuration.Location = new System.Drawing.Point(356, 4);
            this.btnApplyVisitDuration.Name = "btnApplyVisitDuration";
            this.btnApplyVisitDuration.Size = new System.Drawing.Size(59, 22);
            superToolTip8.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Text = "Apply Visit Duration - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to apply the entered Visit Duration to the <b>selected</b> Visits.\r\n\r\nOn" +
    "ly selected Visits will be effected. All others will be left as is.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.btnApplyVisitDuration.SuperTip = superToolTip8;
            this.btnApplyVisitDuration.TabIndex = 29;
            this.btnApplyVisitDuration.Text = "Apply";
            this.btnApplyVisitDuration.Click += new System.EventHandler(this.btnApplyVisitDuration_Click);
            // 
            // panelControl7
            // 
            this.panelControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelControl7.Controls.Add(this.btnCalculateVisitDaysSeparation);
            this.panelControl7.Controls.Add(this.labelControl15);
            this.panelControl7.Controls.Add(this.VisitDaysSeparationSpinEdit);
            this.panelControl7.Controls.Add(this.btnApplyVisitDaysSeparation);
            this.panelControl7.Location = new System.Drawing.Point(199, 501);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(339, 30);
            this.panelControl7.TabIndex = 28;
            // 
            // btnCalculateVisitDaysSeparation
            // 
            this.btnCalculateVisitDaysSeparation.ImageOptions.ImageIndex = 11;
            this.btnCalculateVisitDaysSeparation.ImageOptions.ImageList = this.imageCollection1;
            this.btnCalculateVisitDaysSeparation.Location = new System.Drawing.Point(261, 4);
            this.btnCalculateVisitDaysSeparation.Name = "btnCalculateVisitDaysSeparation";
            this.btnCalculateVisitDaysSeparation.Size = new System.Drawing.Size(74, 22);
            superToolTip9.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem9.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Text = "Calculate Visit Days Separation - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = resources.GetString("toolTipItem9.Text");
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.btnCalculateVisitDaysSeparation.SuperTip = superToolTip9;
            this.btnCalculateVisitDaysSeparation.TabIndex = 32;
            this.btnCalculateVisitDaysSeparation.Text = "Calculate";
            this.btnCalculateVisitDaysSeparation.Click += new System.EventHandler(this.btnCalculateVisitDaysSeparation_Click);
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(5, 8);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(105, 13);
            this.labelControl15.TabIndex = 31;
            this.labelControl15.Text = "Visit Days Separation:";
            // 
            // VisitDaysSeparationSpinEdit
            // 
            this.VisitDaysSeparationSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.VisitDaysSeparationSpinEdit.Location = new System.Drawing.Point(114, 5);
            this.VisitDaysSeparationSpinEdit.MenuManager = this.barManager1;
            this.VisitDaysSeparationSpinEdit.Name = "VisitDaysSeparationSpinEdit";
            this.VisitDaysSeparationSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitDaysSeparationSpinEdit.Properties.IsFloatValue = false;
            this.VisitDaysSeparationSpinEdit.Properties.Mask.EditMask = "n0";
            this.VisitDaysSeparationSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.VisitDaysSeparationSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.VisitDaysSeparationSpinEdit.Size = new System.Drawing.Size(80, 20);
            this.VisitDaysSeparationSpinEdit.TabIndex = 30;
            // 
            // btnApplyVisitDaysSeparation
            // 
            this.btnApplyVisitDaysSeparation.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnApplyVisitDaysSeparation.ImageOptions.Image")));
            this.btnApplyVisitDaysSeparation.Location = new System.Drawing.Point(198, 4);
            this.btnApplyVisitDaysSeparation.Name = "btnApplyVisitDaysSeparation";
            this.btnApplyVisitDaysSeparation.Size = new System.Drawing.Size(59, 22);
            superToolTip10.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem10.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Text = "Apply Visit Number - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Click me to apply the entered Visit Number to the <b>selected</b> Visits.\r\n\r\nOnly" +
    " selected Visits will be effected. All others will be left as is.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.btnApplyVisitDaysSeparation.SuperTip = superToolTip10;
            this.btnApplyVisitDaysSeparation.TabIndex = 29;
            this.btnApplyVisitDaysSeparation.Text = "Apply";
            this.btnApplyVisitDaysSeparation.Click += new System.EventHandler(this.btnApplyVisitDaysSeparation_Click);
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl2.Location = new System.Drawing.Point(7, 61);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage5GridView;
            this.xtraTabControl2.Size = new System.Drawing.Size(1200, 436);
            this.xtraTabControl2.TabIndex = 27;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage5GridView,
            this.xtraTabPage5Calendar});
            this.xtraTabControl2.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl2_SelectedPageChanged);
            // 
            // xtraTabPage5GridView
            // 
            this.xtraTabPage5GridView.Controls.Add(this.gridControl6);
            this.xtraTabPage5GridView.Name = "xtraTabPage5GridView";
            this.xtraTabPage5GridView.Size = new System.Drawing.Size(1195, 410);
            this.xtraTabPage5GridView.Text = "Visits to Create - Grid View";
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.sp06145OMVisitEditBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditFloat,
            this.repositoryItemGridLookUpEditCostCalculationLevel,
            this.repositoryItemTextEdit1,
            this.repositoryItemGridLookUpEditVisitCategory,
            this.repositoryItemGridLookUpEditVisitTypes,
            this.repositoryItemButtonEditClientPONumber,
            this.repositoryItemSpinEditVisitNumber,
            this.repositoryItemDateEdit1,
            this.repositoryItemSpinEditDaysSeparation});
            this.gridControl6.Size = new System.Drawing.Size(1195, 410);
            this.gridControl6.TabIndex = 21;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp06145OMVisitEditBindingSource
            // 
            this.sp06145OMVisitEditBindingSource.DataMember = "sp06145_OM_Visit_Edit";
            this.sp06145OMVisitEditBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colstrMode,
            this.colstrRecordIDs,
            this.colVisitID,
            this.colSiteContractID,
            this.colVisitNumber,
            this.colCreatedByStaffID,
            this.colExpectedStartDate,
            this.colExpectedEndDate,
            this.colStartDate,
            this.colEndDate,
            this.colCostCalculationLevel1,
            this.colVisitCost,
            this.colVisitSell,
            this.colRemarks1,
            this.colStartLatitude,
            this.colStartLongitude,
            this.colFinishLatitude,
            this.colFinishLongitude,
            this.colClientContractID,
            this.colClientName1,
            this.colContractDescription1,
            this.colSiteName1,
            this.colCreatedByStaffName,
            this.colCalendarLabel,
            this.colLabelColourID,
            this.colWorkNumber,
            this.colVisitCategoryID,
            this.colVisitStatusID,
            this.colVisitTypeID,
            this.colClientPOID,
            this.colClientPONumber,
            this.colSellCalculationLevel1,
            this.colCreatedFromVisitTemplateID,
            this.colDaysSeparation,
            this.colSiteContractDaysSeparationPercent});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.GroupCount = 3;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowFilterEditor = false;
            this.gridView6.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView6.OptionsFilter.AllowMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView6.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView6.OptionsFind.FindDelay = 2000;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsLayout.StoreFormatRules = true;
            this.gridView6.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractDescription1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colExpectedStartDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // colstrMode
            // 
            this.colstrMode.FieldName = "strMode";
            this.colstrMode.Name = "colstrMode";
            this.colstrMode.OptionsColumn.AllowEdit = false;
            this.colstrMode.OptionsColumn.AllowFocus = false;
            this.colstrMode.OptionsColumn.ReadOnly = true;
            this.colstrMode.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colstrRecordIDs
            // 
            this.colstrRecordIDs.FieldName = "strRecordIDs";
            this.colstrRecordIDs.Name = "colstrRecordIDs";
            this.colstrRecordIDs.OptionsColumn.AllowEdit = false;
            this.colstrRecordIDs.OptionsColumn.AllowFocus = false;
            this.colstrRecordIDs.OptionsColumn.ReadOnly = true;
            this.colstrRecordIDs.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colVisitID
            // 
            this.colVisitID.Caption = "Visit ID";
            this.colVisitID.FieldName = "VisitID";
            this.colVisitID.Name = "colVisitID";
            this.colVisitID.OptionsColumn.AllowEdit = false;
            this.colVisitID.OptionsColumn.AllowFocus = false;
            this.colVisitID.OptionsColumn.ReadOnly = true;
            this.colVisitID.Width = 54;
            // 
            // colSiteContractID
            // 
            this.colSiteContractID.Caption = "Site Contract ID";
            this.colSiteContractID.FieldName = "SiteContractID";
            this.colSiteContractID.Name = "colSiteContractID";
            this.colSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSiteContractID.Width = 98;
            // 
            // colVisitNumber
            // 
            this.colVisitNumber.Caption = "Visit #";
            this.colVisitNumber.ColumnEdit = this.repositoryItemSpinEditVisitNumber;
            this.colVisitNumber.FieldName = "VisitNumber";
            this.colVisitNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colVisitNumber.Name = "colVisitNumber";
            this.colVisitNumber.Visible = true;
            this.colVisitNumber.VisibleIndex = 0;
            this.colVisitNumber.Width = 94;
            // 
            // repositoryItemSpinEditVisitNumber
            // 
            this.repositoryItemSpinEditVisitNumber.AutoHeight = false;
            this.repositoryItemSpinEditVisitNumber.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditVisitNumber.IsFloatValue = false;
            this.repositoryItemSpinEditVisitNumber.Mask.EditMask = "n0";
            this.repositoryItemSpinEditVisitNumber.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditVisitNumber.Name = "repositoryItemSpinEditVisitNumber";
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "CreatedBy Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 113;
            // 
            // colExpectedStartDate
            // 
            this.colExpectedStartDate.Caption = "Expected Start Date";
            this.colExpectedStartDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colExpectedStartDate.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate.Name = "colExpectedStartDate";
            this.colExpectedStartDate.Visible = true;
            this.colExpectedStartDate.VisibleIndex = 1;
            this.colExpectedStartDate.Width = 130;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Mask.EditMask = "G";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemDateEdit1_EditValueChanged);
            this.repositoryItemDateEdit1.Validating += new System.ComponentModel.CancelEventHandler(this.repositoryItemDateEdit1_Validating);
            // 
            // colExpectedEndDate
            // 
            this.colExpectedEndDate.Caption = "Expected End Date";
            this.colExpectedEndDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colExpectedEndDate.FieldName = "ExpectedEndDate";
            this.colExpectedEndDate.Name = "colExpectedEndDate";
            this.colExpectedEndDate.Visible = true;
            this.colExpectedEndDate.VisibleIndex = 2;
            this.colExpectedEndDate.Width = 130;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Actual Start Date";
            this.colStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.Width = 104;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "Actual End Date";
            this.colEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.Width = 98;
            // 
            // colCostCalculationLevel1
            // 
            this.colCostCalculationLevel1.Caption = "Cost Calculation";
            this.colCostCalculationLevel1.ColumnEdit = this.repositoryItemGridLookUpEditCostCalculationLevel;
            this.colCostCalculationLevel1.FieldName = "CostCalculationLevel";
            this.colCostCalculationLevel1.Name = "colCostCalculationLevel1";
            this.colCostCalculationLevel1.Visible = true;
            this.colCostCalculationLevel1.VisibleIndex = 7;
            this.colCostCalculationLevel1.Width = 100;
            // 
            // repositoryItemGridLookUpEditCostCalculationLevel
            // 
            this.repositoryItemGridLookUpEditCostCalculationLevel.AutoHeight = false;
            this.repositoryItemGridLookUpEditCostCalculationLevel.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditCostCalculationLevel.DataSource = this.sp06134OMJobCalculationLevelDescriptorsBindingSource;
            this.repositoryItemGridLookUpEditCostCalculationLevel.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditCostCalculationLevel.Name = "repositoryItemGridLookUpEditCostCalculationLevel";
            this.repositoryItemGridLookUpEditCostCalculationLevel.NullText = "";
            this.repositoryItemGridLookUpEditCostCalculationLevel.PopupView = this.repositoryItemGridLookUpEdit2View;
            this.repositoryItemGridLookUpEditCostCalculationLevel.ValueMember = "ID";
            // 
            // repositoryItemGridLookUpEdit2View
            // 
            this.repositoryItemGridLookUpEdit2View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.repositoryItemGridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule6.ApplyToRow = true;
            gridFormatRule6.Column = this.colID1;
            gridFormatRule6.Name = "Format0";
            formatConditionRuleValue6.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue6.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue6.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue6.Value1 = -1;
            gridFormatRule6.Rule = formatConditionRuleValue6;
            this.repositoryItemGridLookUpEdit2View.FormatRules.Add(gridFormatRule6);
            this.repositoryItemGridLookUpEdit2View.Name = "repositoryItemGridLookUpEdit2View";
            this.repositoryItemGridLookUpEdit2View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit2View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit2View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit2View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Cost Calculation Level";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "RecordOrder";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // colVisitCost
            // 
            this.colVisitCost.Caption = "Visit Cost";
            this.colVisitCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colVisitCost.FieldName = "VisitCost";
            this.colVisitCost.Name = "colVisitCost";
            this.colVisitCost.OptionsColumn.AllowEdit = false;
            this.colVisitCost.OptionsColumn.AllowFocus = false;
            this.colVisitCost.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colVisitSell
            // 
            this.colVisitSell.Caption = "Visit Sell";
            this.colVisitSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colVisitSell.FieldName = "VisitSell";
            this.colVisitSell.Name = "colVisitSell";
            this.colVisitSell.OptionsColumn.AllowEdit = false;
            this.colVisitSell.OptionsColumn.AllowFocus = false;
            this.colVisitSell.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 10;
            this.colRemarks1.Width = 78;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colStartLatitude
            // 
            this.colStartLatitude.Caption = "Start Latitude";
            this.colStartLatitude.ColumnEdit = this.repositoryItemTextEditFloat;
            this.colStartLatitude.FieldName = "StartLatitude";
            this.colStartLatitude.Name = "colStartLatitude";
            this.colStartLatitude.OptionsColumn.AllowEdit = false;
            this.colStartLatitude.OptionsColumn.AllowFocus = false;
            this.colStartLatitude.OptionsColumn.ReadOnly = true;
            this.colStartLatitude.Width = 87;
            // 
            // repositoryItemTextEditFloat
            // 
            this.repositoryItemTextEditFloat.AutoHeight = false;
            this.repositoryItemTextEditFloat.Mask.EditMask = "n8";
            this.repositoryItemTextEditFloat.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditFloat.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditFloat.Name = "repositoryItemTextEditFloat";
            // 
            // colStartLongitude
            // 
            this.colStartLongitude.Caption = "Start Longitude";
            this.colStartLongitude.ColumnEdit = this.repositoryItemTextEditFloat;
            this.colStartLongitude.FieldName = "StartLongitude";
            this.colStartLongitude.Name = "colStartLongitude";
            this.colStartLongitude.OptionsColumn.AllowEdit = false;
            this.colStartLongitude.OptionsColumn.AllowFocus = false;
            this.colStartLongitude.OptionsColumn.ReadOnly = true;
            this.colStartLongitude.Width = 95;
            // 
            // colFinishLatitude
            // 
            this.colFinishLatitude.Caption = "Finish Latitude";
            this.colFinishLatitude.ColumnEdit = this.repositoryItemTextEditFloat;
            this.colFinishLatitude.FieldName = "FinishLatitude";
            this.colFinishLatitude.Name = "colFinishLatitude";
            this.colFinishLatitude.OptionsColumn.AllowEdit = false;
            this.colFinishLatitude.OptionsColumn.AllowFocus = false;
            this.colFinishLatitude.OptionsColumn.ReadOnly = true;
            this.colFinishLatitude.Width = 90;
            // 
            // colFinishLongitude
            // 
            this.colFinishLongitude.Caption = "Finish Longitude";
            this.colFinishLongitude.ColumnEdit = this.repositoryItemTextEditFloat;
            this.colFinishLongitude.FieldName = "FinishLongitude";
            this.colFinishLongitude.Name = "colFinishLongitude";
            this.colFinishLongitude.OptionsColumn.AllowEdit = false;
            this.colFinishLongitude.OptionsColumn.AllowFocus = false;
            this.colFinishLongitude.OptionsColumn.ReadOnly = true;
            this.colFinishLongitude.Width = 98;
            // 
            // colClientContractID
            // 
            this.colClientContractID.Caption = "Client Contract ID";
            this.colClientContractID.FieldName = "ClientContractID";
            this.colClientContractID.Name = "colClientContractID";
            this.colClientContractID.OptionsColumn.AllowEdit = false;
            this.colClientContractID.OptionsColumn.AllowFocus = false;
            this.colClientContractID.OptionsColumn.ReadOnly = true;
            this.colClientContractID.Width = 107;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 10;
            this.colClientName1.Width = 262;
            // 
            // colContractDescription1
            // 
            this.colContractDescription1.Caption = "Contract Description";
            this.colContractDescription1.FieldName = "ContractDescription";
            this.colContractDescription1.Name = "colContractDescription1";
            this.colContractDescription1.OptionsColumn.AllowEdit = false;
            this.colContractDescription1.OptionsColumn.AllowFocus = false;
            this.colContractDescription1.OptionsColumn.ReadOnly = true;
            this.colContractDescription1.Visible = true;
            this.colContractDescription1.VisibleIndex = 11;
            this.colContractDescription1.Width = 302;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Visible = true;
            this.colSiteName1.VisibleIndex = 12;
            this.colSiteName1.Width = 226;
            // 
            // colCreatedByStaffName
            // 
            this.colCreatedByStaffName.Caption = "Created By Person";
            this.colCreatedByStaffName.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName.Name = "colCreatedByStaffName";
            this.colCreatedByStaffName.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName.Width = 111;
            // 
            // colCalendarLabel
            // 
            this.colCalendarLabel.Caption = "Calendar Label";
            this.colCalendarLabel.FieldName = "CalendarLabel";
            this.colCalendarLabel.Name = "colCalendarLabel";
            this.colCalendarLabel.OptionsColumn.AllowEdit = false;
            this.colCalendarLabel.OptionsColumn.AllowFocus = false;
            this.colCalendarLabel.OptionsColumn.ReadOnly = true;
            this.colCalendarLabel.Width = 92;
            // 
            // colLabelColourID
            // 
            this.colLabelColourID.Caption = "Calendar Label Colour ID";
            this.colLabelColourID.FieldName = "LabelColourID";
            this.colLabelColourID.Name = "colLabelColourID";
            this.colLabelColourID.OptionsColumn.AllowEdit = false;
            this.colLabelColourID.OptionsColumn.AllowFocus = false;
            this.colLabelColourID.OptionsColumn.ReadOnly = true;
            this.colLabelColourID.Width = 146;
            // 
            // colWorkNumber
            // 
            this.colWorkNumber.Caption = "Work Number";
            this.colWorkNumber.ColumnEdit = this.repositoryItemTextEdit1;
            this.colWorkNumber.FieldName = "WorkNumber";
            this.colWorkNumber.Name = "colWorkNumber";
            this.colWorkNumber.Visible = true;
            this.colWorkNumber.VisibleIndex = 6;
            this.colWorkNumber.Width = 133;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 50;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colVisitCategoryID
            // 
            this.colVisitCategoryID.Caption = "Visit Category";
            this.colVisitCategoryID.ColumnEdit = this.repositoryItemGridLookUpEditVisitCategory;
            this.colVisitCategoryID.FieldName = "VisitCategoryID";
            this.colVisitCategoryID.Name = "colVisitCategoryID";
            this.colVisitCategoryID.Visible = true;
            this.colVisitCategoryID.VisibleIndex = 4;
            this.colVisitCategoryID.Width = 137;
            // 
            // repositoryItemGridLookUpEditVisitCategory
            // 
            this.repositoryItemGridLookUpEditVisitCategory.AutoHeight = false;
            this.repositoryItemGridLookUpEditVisitCategory.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditVisitCategory.DataSource = this.sp06250OMVisitCategoriesWithBlankBindingSource;
            this.repositoryItemGridLookUpEditVisitCategory.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditVisitCategory.Name = "repositoryItemGridLookUpEditVisitCategory";
            this.repositoryItemGridLookUpEditVisitCategory.NullText = "";
            this.repositoryItemGridLookUpEditVisitCategory.PopupView = this.gridView7;
            this.repositoryItemGridLookUpEditVisitCategory.ValueMember = "ID";
            // 
            // sp06250OMVisitCategoriesWithBlankBindingSource
            // 
            this.sp06250OMVisitCategoriesWithBlankBindingSource.DataMember = "sp06250_OM_Visit_Categories_With_Blank";
            this.sp06250OMVisitCategoriesWithBlankBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15});
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition6.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition6.Appearance.Options.UseForeColor = true;
            styleFormatCondition6.ApplyToRow = true;
            styleFormatCondition6.Column = this.gridColumn13;
            styleFormatCondition6.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition6.Value1 = 0;
            this.gridView7.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition6});
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn15, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Visit Category";
            this.gridColumn14.FieldName = "Description";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 0;
            this.gridColumn14.Width = 220;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Order";
            this.gridColumn15.FieldName = "RecordOrder";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            // 
            // colVisitStatusID
            // 
            this.colVisitStatusID.Caption = "Visit Status";
            this.colVisitStatusID.FieldName = "VisitStatusID";
            this.colVisitStatusID.Name = "colVisitStatusID";
            this.colVisitStatusID.OptionsColumn.AllowEdit = false;
            this.colVisitStatusID.OptionsColumn.AllowFocus = false;
            this.colVisitStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colVisitTypeID
            // 
            this.colVisitTypeID.Caption = "Visit Type";
            this.colVisitTypeID.ColumnEdit = this.repositoryItemGridLookUpEditVisitTypes;
            this.colVisitTypeID.FieldName = "VisitTypeID";
            this.colVisitTypeID.Name = "colVisitTypeID";
            this.colVisitTypeID.Visible = true;
            this.colVisitTypeID.VisibleIndex = 5;
            this.colVisitTypeID.Width = 128;
            // 
            // repositoryItemGridLookUpEditVisitTypes
            // 
            this.repositoryItemGridLookUpEditVisitTypes.AutoHeight = false;
            this.repositoryItemGridLookUpEditVisitTypes.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditVisitTypes.DataSource = this.sp06308OMVisitTypesBindingSource;
            this.repositoryItemGridLookUpEditVisitTypes.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditVisitTypes.Name = "repositoryItemGridLookUpEditVisitTypes";
            this.repositoryItemGridLookUpEditVisitTypes.NullText = "";
            this.repositoryItemGridLookUpEditVisitTypes.PopupView = this.gridView5;
            this.repositoryItemGridLookUpEditVisitTypes.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2});
            this.repositoryItemGridLookUpEditVisitTypes.ValueMember = "ID";
            // 
            // sp06308OMVisitTypesBindingSource
            // 
            this.sp06308OMVisitTypesBindingSource.DataMember = "sp06308_OM_Visit_Types";
            this.sp06308OMVisitTypesBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActive,
            this.colDescription2,
            this.colID,
            this.colOrder1});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsLayout.StoreFormatRules = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 50;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Visit Type";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 202;
            // 
            // colID
            // 
            this.colID.Caption = "Visit Type ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.Width = 80;
            // 
            // colOrder1
            // 
            this.colOrder1.Caption = "Order";
            this.colOrder1.FieldName = "Order";
            this.colOrder1.Name = "colOrder1";
            this.colOrder1.Width = 61;
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "Client PO ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "Client PO #";
            this.colClientPONumber.ColumnEdit = this.repositoryItemButtonEditClientPONumber;
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.Visible = true;
            this.colClientPONumber.VisibleIndex = 9;
            this.colClientPONumber.Width = 178;
            // 
            // repositoryItemButtonEditClientPONumber
            // 
            this.repositoryItemButtonEditClientPONumber.AutoHeight = false;
            this.repositoryItemButtonEditClientPONumber.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Click me to open the Select Client Purchase Order screen.", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close, "clear", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Click me to Clear the selected Client Purchase Order.", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditClientPONumber.Name = "repositoryItemButtonEditClientPONumber";
            this.repositoryItemButtonEditClientPONumber.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditClientPONumber.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditClientPONumber_ButtonClick);
            // 
            // colSellCalculationLevel1
            // 
            this.colSellCalculationLevel1.Caption = "Sell Calculation";
            this.colSellCalculationLevel1.ColumnEdit = this.repositoryItemGridLookUpEditCostCalculationLevel;
            this.colSellCalculationLevel1.FieldName = "SellCalculationLevel";
            this.colSellCalculationLevel1.Name = "colSellCalculationLevel1";
            this.colSellCalculationLevel1.Visible = true;
            this.colSellCalculationLevel1.VisibleIndex = 8;
            this.colSellCalculationLevel1.Width = 100;
            // 
            // colCreatedFromVisitTemplateID
            // 
            this.colCreatedFromVisitTemplateID.Caption = "Created From Visit Template ID";
            this.colCreatedFromVisitTemplateID.FieldName = "CreatedFromVisitTemplateID";
            this.colCreatedFromVisitTemplateID.Name = "colCreatedFromVisitTemplateID";
            this.colCreatedFromVisitTemplateID.OptionsColumn.AllowEdit = false;
            this.colCreatedFromVisitTemplateID.OptionsColumn.AllowFocus = false;
            this.colCreatedFromVisitTemplateID.OptionsColumn.ReadOnly = true;
            this.colCreatedFromVisitTemplateID.Width = 168;
            // 
            // colDaysSeparation
            // 
            this.colDaysSeparation.Caption = "Visit Days Separation";
            this.colDaysSeparation.ColumnEdit = this.repositoryItemSpinEditDaysSeparation;
            this.colDaysSeparation.FieldName = "DaysSeparation";
            this.colDaysSeparation.Name = "colDaysSeparation";
            this.colDaysSeparation.Visible = true;
            this.colDaysSeparation.VisibleIndex = 3;
            this.colDaysSeparation.Width = 120;
            // 
            // repositoryItemSpinEditDaysSeparation
            // 
            this.repositoryItemSpinEditDaysSeparation.AutoHeight = false;
            editorButtonImageOptions7.Image = global::WoodPlan5.Properties.Resources.Calculate_16x16;
            this.repositoryItemSpinEditDaysSeparation.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Calculate", -1, false, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Calculate Visit Days Separation", "calculate", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemSpinEditDaysSeparation.IsFloatValue = false;
            this.repositoryItemSpinEditDaysSeparation.Mask.EditMask = "n0";
            this.repositoryItemSpinEditDaysSeparation.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditDaysSeparation.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.repositoryItemSpinEditDaysSeparation.Name = "repositoryItemSpinEditDaysSeparation";
            this.repositoryItemSpinEditDaysSeparation.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemSpinEditDaysSeparation_ButtonClick);
            // 
            // colSiteContractDaysSeparationPercent
            // 
            this.colSiteContractDaysSeparationPercent.Caption = "Site Contract Days Separation %";
            this.colSiteContractDaysSeparationPercent.FieldName = "SiteContractDaysSeparationPercent";
            this.colSiteContractDaysSeparationPercent.Name = "colSiteContractDaysSeparationPercent";
            this.colSiteContractDaysSeparationPercent.OptionsColumn.AllowEdit = false;
            this.colSiteContractDaysSeparationPercent.OptionsColumn.AllowFocus = false;
            this.colSiteContractDaysSeparationPercent.OptionsColumn.ReadOnly = true;
            this.colSiteContractDaysSeparationPercent.Width = 178;
            // 
            // xtraTabPage5Calendar
            // 
            this.xtraTabPage5Calendar.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage5Calendar.Controls.Add(this.standaloneBarDockControl1);
            this.xtraTabPage5Calendar.Name = "xtraTabPage5Calendar";
            this.xtraTabPage5Calendar.Size = new System.Drawing.Size(1195, 410);
            this.xtraTabPage5Calendar.Text = "Visits to Create - Calendar View";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 27);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.schedulerControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1194, 381);
            this.splitContainerControl1.SplitterPosition = 187;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.dateNavigator1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.resourcesCheckedListBoxControl1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(187, 381);
            this.splitContainerControl2.SplitterPosition = 187;
            this.splitContainerControl2.TabIndex = 1;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // dateNavigator1
            // 
            this.dateNavigator1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateNavigator1.CellPadding = new System.Windows.Forms.Padding(2);
            this.dateNavigator1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateNavigator1.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.dateNavigator1.Location = new System.Drawing.Point(0, 0);
            this.dateNavigator1.Name = "dateNavigator1";
            this.dateNavigator1.SchedulerControl = this.schedulerControl1;
            this.dateNavigator1.Size = new System.Drawing.Size(187, 188);
            this.dateNavigator1.TabIndex = 0;
            // 
            // schedulerControl1
            // 
            this.schedulerControl1.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Month;
            this.schedulerControl1.DataStorage = this.schedulerStorage1;
            this.schedulerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.schedulerControl1.GroupType = DevExpress.XtraScheduler.SchedulerGroupType.Resource;
            this.schedulerControl1.Location = new System.Drawing.Point(0, 0);
            this.schedulerControl1.MenuManager = this.barManager1;
            this.schedulerControl1.Name = "schedulerControl1";
            this.schedulerControl1.OptionsCustomization.AllowAppointmentCopy = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl1.OptionsCustomization.AllowAppointmentCreate = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl1.OptionsCustomization.AllowAppointmentDragBetweenResources = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl1.OptionsCustomization.AllowDisplayAppointmentDependencyForm = DevExpress.XtraScheduler.AllowDisplayAppointmentDependencyForm.Never;
            this.schedulerControl1.OptionsCustomization.AllowDisplayAppointmentForm = DevExpress.XtraScheduler.AllowDisplayAppointmentForm.Never;
            this.schedulerControl1.OptionsCustomization.AllowInplaceEditor = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl1.Size = new System.Drawing.Size(1001, 381);
            this.schedulerControl1.Start = new System.DateTime(2014, 9, 29, 0, 0, 0, 0);
            this.schedulerControl1.TabIndex = 0;
            this.schedulerControl1.Text = "schedulerControl1";
            this.schedulerControl1.Views.DayView.TimeRulers.Add(timeRuler3);
            this.schedulerControl1.Views.WorkWeekView.TimeRulers.Add(timeRuler4);
            this.schedulerControl1.SelectionChanged += new System.EventHandler(this.schedulerControl1_SelectionChanged);
            this.schedulerControl1.PopupMenuShowing += new DevExpress.XtraScheduler.PopupMenuShowingEventHandler(this.schedulerControl1_PopupMenuShowing);
            // 
            // schedulerStorage1
            // 
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("SiteName", "SiteName"));
            this.schedulerStorage1.Appointments.DataSource = this.sp06145OMVisitEditBindingSource;
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))), "None", "&None");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(194)))), ((int)(((byte)(190))))), "Visit 1", "Visit 1");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(213)))), ((int)(((byte)(255))))), "Visit 2", "Visit 2");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(244)))), ((int)(((byte)(156))))), "Visit 3", "Visit 3");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(228)))), ((int)(((byte)(199))))), "Visit 4", "Visit 4");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(206)))), ((int)(((byte)(147))))), "Visit 5", "Visit 5");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(244)))), ((int)(((byte)(255))))), "Visit 6", "Visit 6");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(219)))), ((int)(((byte)(152))))), "Visit 7", "Visit 7");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(207)))), ((int)(((byte)(233))))), "Visit 8", "Visit 8");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(233)))), ((int)(((byte)(223))))), "Visit 9", "Visit 9");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(247)))), ((int)(((byte)(165))))), "Visit 10", "Visit 10");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255))))), "Visit 11", "Visit 11");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255))))), "Visit 12", "Visit 12");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))), "Visit 13", "3Visit 12");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192))))), "Visit 14", "Visit 14");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192))))), "Visit 15", "Visit 15");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192))))), "Visit 16", "Visit 16");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))), "Visit 17", "Visit 17");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))), "Visit 18", "Visit 18");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0))))), "Visit 19", "Visit 19");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0))))), "Visit 20", "Visit 20");
            this.schedulerStorage1.Appointments.Mappings.AppointmentId = "VisitID";
            this.schedulerStorage1.Appointments.Mappings.Description = "ContractDescription";
            this.schedulerStorage1.Appointments.Mappings.End = "ExpectedEndDate";
            this.schedulerStorage1.Appointments.Mappings.Label = "LabelColourID";
            this.schedulerStorage1.Appointments.Mappings.Location = "CalendarLabel";
            this.schedulerStorage1.Appointments.Mappings.ResourceId = "SiteContractID";
            this.schedulerStorage1.Appointments.Mappings.Start = "ExpectedStartDate";
            this.schedulerStorage1.Resources.CustomFieldMappings.Add(new DevExpress.XtraScheduler.ResourceCustomFieldMapping("ContractDescription", "ContractDescription"));
            this.schedulerStorage1.Resources.Mappings.Caption = "SiteName";
            this.schedulerStorage1.Resources.Mappings.Id = "SiteContractID";
            this.schedulerStorage1.Resources.Mappings.ParentId = "ClientContractID";
            this.schedulerStorage1.AppointmentDeleting += new DevExpress.XtraScheduler.PersistentObjectCancelEventHandler(this.schedulerStorage1_AppointmentDeleting);
            this.schedulerStorage1.AppointmentsDeleted += new DevExpress.XtraScheduler.PersistentObjectsEventHandler(this.schedulerStorage1_AppointmentsDeleted);
            // 
            // resourcesCheckedListBoxControl1
            // 
            this.resourcesCheckedListBoxControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resourcesCheckedListBoxControl1.Location = new System.Drawing.Point(0, 0);
            this.resourcesCheckedListBoxControl1.Name = "resourcesCheckedListBoxControl1";
            this.resourcesCheckedListBoxControl1.SchedulerControl = this.schedulerControl1;
            this.resourcesCheckedListBoxControl1.Size = new System.Drawing.Size(187, 187);
            this.resourcesCheckedListBoxControl1.TabIndex = 0;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(1195, 26);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // btnStep5Next
            // 
            this.btnStep5Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep5Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep5Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep5Next.Location = new System.Drawing.Point(1117, 501);
            this.btnStep5Next.Name = "btnStep5Next";
            this.btnStep5Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep5Next.TabIndex = 19;
            this.btnStep5Next.Text = "Next";
            this.btnStep5Next.Click += new System.EventHandler(this.btnStep5Next_Click);
            // 
            // btnStep5Previous
            // 
            this.btnStep5Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep5Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep5Previous.Location = new System.Drawing.Point(1023, 501);
            this.btnStep5Previous.Name = "btnStep5Previous";
            this.btnStep5Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep5Previous.TabIndex = 20;
            this.btnStep5Previous.Text = "Previous";
            this.btnStep5Previous.Click += new System.EventHandler(this.btnStep5Previous_Click);
            // 
            // panelControl6
            // 
            this.panelControl6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl6.Controls.Add(this.pictureEdit8);
            this.panelControl6.Controls.Add(this.labelControl13);
            this.panelControl6.Controls.Add(this.labelControl14);
            this.panelControl6.Location = new System.Drawing.Point(7, 6);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(1198, 48);
            this.panelControl6.TabIndex = 17;
            // 
            // pictureEdit8
            // 
            this.pictureEdit8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit8.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit8.Location = new System.Drawing.Point(1154, 4);
            this.pictureEdit8.MenuManager = this.barManager1;
            this.pictureEdit8.Name = "pictureEdit8";
            this.pictureEdit8.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit8.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit8.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit8.Properties.ReadOnly = true;
            this.pictureEdit8.Properties.ShowMenu = false;
            this.pictureEdit8.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit8.TabIndex = 9;
            // 
            // labelControl13
            // 
            this.labelControl13.AllowHtmlString = true;
            this.labelControl13.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.Options.UseBackColor = true;
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Location = new System.Drawing.Point(5, 5);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(197, 16);
            this.labelControl13.TabIndex = 6;
            this.labelControl13.Text = "<b>Step 5:</b> Complete the Visit Details";
            // 
            // labelControl14
            // 
            this.labelControl14.AllowHtmlString = true;
            this.labelControl14.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl14.Appearance.Options.UseBackColor = true;
            this.labelControl14.Location = new System.Drawing.Point(57, 29);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(284, 13);
            this.labelControl14.TabIndex = 7;
            this.labelControl14.Text = "Enter Visit Details for each Site Visit. Click Next when Done.";
            // 
            // xtraTabPageFinish
            // 
            this.xtraTabPageFinish.Controls.Add(this.panelControl2);
            this.xtraTabPageFinish.Controls.Add(this.groupControl1);
            this.xtraTabPageFinish.Controls.Add(this.labelControl10);
            this.xtraTabPageFinish.Controls.Add(this.btnFinish);
            this.xtraTabPageFinish.Controls.Add(this.btnFinishPrevious);
            this.xtraTabPageFinish.Controls.Add(this.pictureEdit2);
            this.xtraTabPageFinish.Name = "xtraTabPageFinish";
            this.xtraTabPageFinish.Size = new System.Drawing.Size(1211, 537);
            this.xtraTabPageFinish.Tag = "99";
            this.xtraTabPageFinish.Text = "Finished";
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.pictureEdit6);
            this.panelControl2.Controls.Add(this.labelControl11);
            this.panelControl2.Controls.Add(this.labelControl12);
            this.panelControl2.Location = new System.Drawing.Point(211, 6);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(994, 48);
            this.panelControl2.TabIndex = 18;
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit6.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit6.Location = new System.Drawing.Point(950, 4);
            this.pictureEdit6.MenuManager = this.barManager1;
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit6.Properties.ReadOnly = true;
            this.pictureEdit6.Properties.ShowMenu = false;
            this.pictureEdit6.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit6.TabIndex = 19;
            // 
            // labelControl11
            // 
            this.labelControl11.AllowHtmlString = true;
            this.labelControl11.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl11.Appearance.Options.UseBackColor = true;
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(5, 1);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(240, 29);
            this.labelControl11.TabIndex = 5;
            this.labelControl11.Text = "Completing the Wizard";
            // 
            // labelControl12
            // 
            this.labelControl12.AllowHtmlString = true;
            this.labelControl12.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl12.Appearance.Options.UseBackColor = true;
            this.labelControl12.Location = new System.Drawing.Point(9, 31);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(506, 13);
            this.labelControl12.TabIndex = 15;
            this.labelControl12.Text = "You have successfully completed the Wizard. On clicking <b>Finish</b>, one or mor" +
    "e visit records will be created.";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.checkEdit2);
            this.groupControl1.Controls.Add(this.checkEdit3);
            this.groupControl1.Controls.Add(this.checkEdit1);
            this.groupControl1.Location = new System.Drawing.Point(211, 155);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(994, 104);
            this.groupControl1.TabIndex = 16;
            this.groupControl1.Text = "Available Choices:";
            // 
            // checkEdit2
            // 
            this.checkEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit2.Location = new System.Drawing.Point(6, 51);
            this.checkEdit2.MenuManager = this.barManager1;
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit2.Properties.Caption = "Create the Visit(s) then <b>open</b> the <b>Job Wizard</b> to create Jobs.";
            this.checkEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit2.Properties.RadioGroupIndex = 1;
            this.checkEdit2.Size = new System.Drawing.Size(982, 19);
            this.checkEdit2.TabIndex = 4;
            this.checkEdit2.TabStop = false;
            // 
            // checkEdit3
            // 
            this.checkEdit3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit3.Location = new System.Drawing.Point(6, 76);
            this.checkEdit3.MenuManager = this.barManager1;
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit3.Properties.Caption = "Create the Visit(s) then <b>open</b> the <b>Visit Wizard</b> to create further Vi" +
    "sits";
            this.checkEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit3.Properties.RadioGroupIndex = 1;
            this.checkEdit3.Size = new System.Drawing.Size(982, 19);
            this.checkEdit3.TabIndex = 2;
            this.checkEdit3.TabStop = false;
            // 
            // checkEdit1
            // 
            this.checkEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit1.EditValue = true;
            this.checkEdit1.Location = new System.Drawing.Point(6, 26);
            this.checkEdit1.MenuManager = this.barManager1;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit1.Properties.Caption = "Create the Visit(s) then <b>return</b> to the parent<b> Manager</b>.";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit1.Properties.RadioGroupIndex = 1;
            this.checkEdit1.Size = new System.Drawing.Size(982, 19);
            this.checkEdit1.TabIndex = 0;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(213, 132);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(154, 13);
            this.labelControl10.TabIndex = 15;
            this.labelControl10.Text = "What would you like to do next?";
            // 
            // btnFinish
            // 
            this.btnFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinish.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnFinish.Appearance.Options.UseFont = true;
            this.btnFinish.ImageOptions.Image = global::WoodPlan5.Properties.Resources.apply_32x32;
            this.btnFinish.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnFinish.Location = new System.Drawing.Point(1117, 501);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(88, 30);
            this.btnFinish.TabIndex = 12;
            this.btnFinish.Text = "Finish";
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnFinishPrevious
            // 
            this.btnFinishPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinishPrevious.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnFinishPrevious.Location = new System.Drawing.Point(1023, 501);
            this.btnFinishPrevious.Name = "btnFinishPrevious";
            this.btnFinishPrevious.Size = new System.Drawing.Size(88, 30);
            this.btnFinishPrevious.TabIndex = 11;
            this.btnFinishPrevious.Text = "Previous";
            this.btnFinishPrevious.Click += new System.EventHandler(this.btnFinishPrevious_Click);
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureEdit2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit2.EditValue = global::WoodPlan5.Properties.Resources.wizard_finish1;
            this.pictureEdit2.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit2.MenuManager = this.barManager1;
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.ShowMenu = false;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit2.Size = new System.Drawing.Size(205, 537);
            this.pictureEdit2.TabIndex = 5;
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.DisplayMember = "Description";
            this.repositoryItemGridLookUpEdit1.LookAndFeel.SkinName = "Blue";
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.NullText = "";
            this.repositoryItemGridLookUpEdit1.ValueMember = "ID";
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // repositoryItemButtonEditChooseStaff
            // 
            this.repositoryItemButtonEditChooseStaff.AutoHeight = false;
            this.repositoryItemButtonEditChooseStaff.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Click me to open the Select Staff screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditChooseStaff.Name = "repositoryItemButtonEditChooseStaff";
            this.repositoryItemButtonEditChooseStaff.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // barStep1
            // 
            this.barStep1.BarName = "Custom 4";
            this.barStep1.DockCol = 0;
            this.barStep1.DockRow = 0;
            this.barStep1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.barStep1.FloatLocation = new System.Drawing.Point(480, 247);
            this.barStep1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemDateRange),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemActive),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefreshClientContracts, true)});
            this.barStep1.OptionsBar.AllowQuickCustomization = false;
            this.barStep1.OptionsBar.DrawDragBorder = false;
            this.barStep1.OptionsBar.UseWholeRow = true;
            this.barStep1.StandaloneBarDockControl = this.standaloneBarDockControl3;
            this.barStep1.Text = "Custom 4";
            // 
            // barEditItemDateRange
            // 
            this.barEditItemDateRange.Caption = "Date Range:";
            this.barEditItemDateRange.Edit = this.repositoryItemPopupContainerEditDateRange;
            this.barEditItemDateRange.EditValue = "No Date Range Filter";
            this.barEditItemDateRange.EditWidth = 238;
            this.barEditItemDateRange.Id = 41;
            this.barEditItemDateRange.Name = "barEditItemDateRange";
            this.barEditItemDateRange.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEditDateRange
            // 
            this.repositoryItemPopupContainerEditDateRange.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateRange.Name = "repositoryItemPopupContainerEditDateRange";
            this.repositoryItemPopupContainerEditDateRange.PopupControl = this.popupContainerControlDateRange;
            this.repositoryItemPopupContainerEditDateRange.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateRange_QueryResultValue);
            // 
            // barEditItemActive
            // 
            this.barEditItemActive.Caption = "Active:";
            this.barEditItemActive.Edit = this.repositoryItemCheckEdit4;
            this.barEditItemActive.EditValue = 1;
            this.barEditItemActive.EditWidth = 20;
            this.barEditItemActive.Id = 43;
            this.barEditItemActive.Name = "barEditItemActive";
            this.barEditItemActive.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "";
            this.repositoryItemCheckEdit4.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // bbiRefreshClientContracts
            // 
            this.bbiRefreshClientContracts.Caption = "Load Client Contracts";
            this.bbiRefreshClientContracts.Id = 42;
            this.bbiRefreshClientContracts.ImageOptions.ImageIndex = 6;
            this.bbiRefreshClientContracts.Name = "bbiRefreshClientContracts";
            this.bbiRefreshClientContracts.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefreshClientContracts.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefreshClientContracts_ItemClick);
            // 
            // sp06131_OM_Visit_Wizard_Client_ContractsTableAdapter
            // 
            this.sp06131_OM_Visit_Wizard_Client_ContractsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06132_OM_Visit_Wizard_Site_Contracts_For_Client_ContractTableAdapter
            // 
            this.sp06132_OM_Visit_Wizard_Site_Contracts_For_Client_ContractTableAdapter.ClearBeforeFill = true;
            // 
            // sp06133_OM_Visit_Gap_Unit_DescriptorsTableAdapter
            // 
            this.sp06133_OM_Visit_Gap_Unit_DescriptorsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter
            // 
            this.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06142_OM_Visit_Template_Headers_SelectTableAdapter
            // 
            this.sp06142_OM_Visit_Template_Headers_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // sp06143_OM_Visit_Template_Items_SelectTableAdapter
            // 
            this.sp06143_OM_Visit_Template_Items_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // sp06145_OM_Visit_EditTableAdapter
            // 
            this.sp06145_OM_Visit_EditTableAdapter.ClearBeforeFill = true;
            // 
            // schedulerBarController1
            // 
            this.schedulerBarController1.BarItems.Add(this.printPreviewItem1);
            this.schedulerBarController1.BarItems.Add(this.printItem1);
            this.schedulerBarController1.BarItems.Add(this.printPageSetupItem1);
            this.schedulerBarController1.BarItems.Add(this.navigateViewBackwardItem1);
            this.schedulerBarController1.BarItems.Add(this.navigateViewForwardItem1);
            this.schedulerBarController1.BarItems.Add(this.gotoTodayItem1);
            this.schedulerBarController1.BarItems.Add(this.viewZoomInItem1);
            this.schedulerBarController1.BarItems.Add(this.viewZoomOutItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToDayViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToWorkWeekViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToWeekViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToMonthViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToTimelineViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToGanttViewItem1);
            this.schedulerBarController1.BarItems.Add(this.groupByNoneItem1);
            this.schedulerBarController1.BarItems.Add(this.groupByDateItem1);
            this.schedulerBarController1.BarItems.Add(this.groupByResourceItem1);
            this.schedulerBarController1.BarItems.Add(this.switchTimeScalesItem1);
            this.schedulerBarController1.BarItems.Add(this.changeScaleWidthItem1);
            this.schedulerBarController1.BarItems.Add(this.switchTimeScalesCaptionItem1);
            this.schedulerBarController1.BarItems.Add(this.switchCompressWeekendItem1);
            this.schedulerBarController1.BarItems.Add(this.switchShowWorkTimeOnlyItem1);
            this.schedulerBarController1.BarItems.Add(this.switchCellsAutoHeightItem1);
            this.schedulerBarController1.BarItems.Add(this.changeSnapToCellsUIItem1);
            this.schedulerBarController1.BarItems.Add(this.editOccurrenceUICommandItem1);
            this.schedulerBarController1.BarItems.Add(this.editSeriesUICommandItem1);
            this.schedulerBarController1.BarItems.Add(this.deleteOccurrenceItem1);
            this.schedulerBarController1.BarItems.Add(this.deleteSeriesItem1);
            this.schedulerBarController1.Control = this.schedulerControl1;
            // 
            // printPreviewItem1
            // 
            this.printPreviewItem1.Id = 63;
            this.printPreviewItem1.Name = "printPreviewItem1";
            // 
            // printItem1
            // 
            this.printItem1.Id = 64;
            this.printItem1.Name = "printItem1";
            // 
            // printPageSetupItem1
            // 
            this.printPageSetupItem1.Id = 65;
            this.printPageSetupItem1.Name = "printPageSetupItem1";
            // 
            // navigateViewBackwardItem1
            // 
            this.navigateViewBackwardItem1.Id = 66;
            this.navigateViewBackwardItem1.Name = "navigateViewBackwardItem1";
            // 
            // navigateViewForwardItem1
            // 
            this.navigateViewForwardItem1.Id = 67;
            this.navigateViewForwardItem1.Name = "navigateViewForwardItem1";
            // 
            // gotoTodayItem1
            // 
            this.gotoTodayItem1.Id = 68;
            this.gotoTodayItem1.Name = "gotoTodayItem1";
            // 
            // viewZoomInItem1
            // 
            this.viewZoomInItem1.Id = 69;
            this.viewZoomInItem1.Name = "viewZoomInItem1";
            // 
            // viewZoomOutItem1
            // 
            this.viewZoomOutItem1.Id = 70;
            this.viewZoomOutItem1.Name = "viewZoomOutItem1";
            // 
            // switchToDayViewItem1
            // 
            this.switchToDayViewItem1.Id = 71;
            this.switchToDayViewItem1.Name = "switchToDayViewItem1";
            // 
            // switchToWorkWeekViewItem1
            // 
            this.switchToWorkWeekViewItem1.Id = 72;
            this.switchToWorkWeekViewItem1.Name = "switchToWorkWeekViewItem1";
            // 
            // switchToWeekViewItem1
            // 
            this.switchToWeekViewItem1.Id = 73;
            this.switchToWeekViewItem1.Name = "switchToWeekViewItem1";
            // 
            // switchToMonthViewItem1
            // 
            this.switchToMonthViewItem1.Id = 74;
            this.switchToMonthViewItem1.Name = "switchToMonthViewItem1";
            // 
            // switchToTimelineViewItem1
            // 
            this.switchToTimelineViewItem1.Id = 75;
            this.switchToTimelineViewItem1.Name = "switchToTimelineViewItem1";
            // 
            // switchToGanttViewItem1
            // 
            this.switchToGanttViewItem1.Id = 76;
            this.switchToGanttViewItem1.Name = "switchToGanttViewItem1";
            // 
            // groupByNoneItem1
            // 
            this.groupByNoneItem1.Id = 77;
            this.groupByNoneItem1.Name = "groupByNoneItem1";
            // 
            // groupByDateItem1
            // 
            this.groupByDateItem1.Id = 78;
            this.groupByDateItem1.Name = "groupByDateItem1";
            // 
            // groupByResourceItem1
            // 
            this.groupByResourceItem1.Id = 79;
            this.groupByResourceItem1.Name = "groupByResourceItem1";
            // 
            // switchTimeScalesItem1
            // 
            this.switchTimeScalesItem1.Id = 80;
            this.switchTimeScalesItem1.Name = "switchTimeScalesItem1";
            // 
            // changeScaleWidthItem1
            // 
            this.changeScaleWidthItem1.Edit = this.repositoryItemSpinEdit1;
            this.changeScaleWidthItem1.Id = 81;
            this.changeScaleWidthItem1.Name = "changeScaleWidthItem1";
            this.changeScaleWidthItem1.UseCommandCaption = true;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // switchTimeScalesCaptionItem1
            // 
            this.switchTimeScalesCaptionItem1.Id = 82;
            this.switchTimeScalesCaptionItem1.Name = "switchTimeScalesCaptionItem1";
            // 
            // switchCompressWeekendItem1
            // 
            this.switchCompressWeekendItem1.Id = 83;
            this.switchCompressWeekendItem1.Name = "switchCompressWeekendItem1";
            // 
            // switchShowWorkTimeOnlyItem1
            // 
            this.switchShowWorkTimeOnlyItem1.Id = 84;
            this.switchShowWorkTimeOnlyItem1.Name = "switchShowWorkTimeOnlyItem1";
            // 
            // switchCellsAutoHeightItem1
            // 
            this.switchCellsAutoHeightItem1.Id = 85;
            this.switchCellsAutoHeightItem1.Name = "switchCellsAutoHeightItem1";
            // 
            // changeSnapToCellsUIItem1
            // 
            this.changeSnapToCellsUIItem1.Id = 86;
            this.changeSnapToCellsUIItem1.Name = "changeSnapToCellsUIItem1";
            // 
            // editOccurrenceUICommandItem1
            // 
            this.editOccurrenceUICommandItem1.Id = 87;
            this.editOccurrenceUICommandItem1.Name = "editOccurrenceUICommandItem1";
            // 
            // editSeriesUICommandItem1
            // 
            this.editSeriesUICommandItem1.Id = 88;
            this.editSeriesUICommandItem1.Name = "editSeriesUICommandItem1";
            // 
            // deleteOccurrenceItem1
            // 
            this.deleteOccurrenceItem1.Id = 89;
            this.deleteOccurrenceItem1.Name = "deleteOccurrenceItem1";
            // 
            // deleteSeriesItem1
            // 
            this.deleteSeriesItem1.Id = 90;
            this.deleteSeriesItem1.Name = "deleteSeriesItem1";
            // 
            // printBar1
            // 
            this.printBar1.BarName = "";
            this.printBar1.Control = this.schedulerControl1;
            this.printBar1.DockCol = 0;
            this.printBar1.DockRow = 0;
            this.printBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.printBar1.FloatLocation = new System.Drawing.Point(992, 254);
            this.printBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.printItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPageSetupItem1)});
            this.printBar1.OptionsBar.DisableClose = true;
            this.printBar1.OptionsBar.DrawDragBorder = false;
            this.printBar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.printBar1.Text = "";
            // 
            // navigatorBar1
            // 
            this.navigatorBar1.BarName = "";
            this.navigatorBar1.Control = this.schedulerControl1;
            this.navigatorBar1.DockCol = 3;
            this.navigatorBar1.DockRow = 0;
            this.navigatorBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.navigatorBar1.FloatLocation = new System.Drawing.Point(1102, 289);
            this.navigatorBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.navigateViewBackwardItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.navigateViewForwardItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.gotoTodayItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewZoomInItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewZoomOutItem1)});
            this.navigatorBar1.OptionsBar.DisableClose = true;
            this.navigatorBar1.OptionsBar.DrawDragBorder = false;
            this.navigatorBar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.navigatorBar1.Text = "";
            // 
            // arrangeBar1
            // 
            this.arrangeBar1.BarName = "";
            this.arrangeBar1.Control = this.schedulerControl1;
            this.arrangeBar1.DockCol = 2;
            this.arrangeBar1.DockRow = 0;
            this.arrangeBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.arrangeBar1.FloatLocation = new System.Drawing.Point(1212, 219);
            this.arrangeBar1.FloatSize = new System.Drawing.Size(192, 26);
            this.arrangeBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToDayViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToWorkWeekViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToWeekViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToMonthViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToTimelineViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToGanttViewItem1)});
            this.arrangeBar1.OptionsBar.DisableClose = true;
            this.arrangeBar1.OptionsBar.DrawDragBorder = false;
            this.arrangeBar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.arrangeBar1.Text = "";
            // 
            // groupByBar1
            // 
            this.groupByBar1.BarName = "";
            this.groupByBar1.Control = this.schedulerControl1;
            this.groupByBar1.DockCol = 1;
            this.groupByBar1.DockRow = 0;
            this.groupByBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.groupByBar1.FloatLocation = new System.Drawing.Point(1192, 296);
            this.groupByBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.groupByNoneItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.groupByDateItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.groupByResourceItem1)});
            this.groupByBar1.OptionsBar.DisableClose = true;
            this.groupByBar1.OptionsBar.DrawDragBorder = false;
            this.groupByBar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.groupByBar1.Text = "";
            // 
            // timeScaleBar1
            // 
            this.timeScaleBar1.BarName = "";
            this.timeScaleBar1.Control = this.schedulerControl1;
            this.timeScaleBar1.DockCol = 5;
            this.timeScaleBar1.DockRow = 0;
            this.timeScaleBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.timeScaleBar1.FloatLocation = new System.Drawing.Point(1224, 292);
            this.timeScaleBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.switchTimeScalesItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.changeScaleWidthItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchTimeScalesCaptionItem1)});
            this.timeScaleBar1.OptionsBar.DisableClose = true;
            this.timeScaleBar1.OptionsBar.DrawDragBorder = false;
            this.timeScaleBar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.timeScaleBar1.Text = "";
            // 
            // layoutBar1
            // 
            this.layoutBar1.BarName = "";
            this.layoutBar1.Control = this.schedulerControl1;
            this.layoutBar1.DockCol = 4;
            this.layoutBar1.DockRow = 0;
            this.layoutBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.layoutBar1.FloatLocation = new System.Drawing.Point(1279, 273);
            this.layoutBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.switchCompressWeekendItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchShowWorkTimeOnlyItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchCellsAutoHeightItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.changeSnapToCellsUIItem1)});
            this.layoutBar1.OptionsBar.DisableClose = true;
            this.layoutBar1.OptionsBar.DrawDragBorder = false;
            this.layoutBar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.layoutBar1.Text = "";
            // 
            // repositoryItemDuration1
            // 
            this.repositoryItemDuration1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDuration1.AutoHeight = false;
            this.repositoryItemDuration1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDuration1.DisabledStateText = null;
            this.repositoryItemDuration1.Name = "repositoryItemDuration1";
            this.repositoryItemDuration1.NullValuePromptShowForEmptyValue = true;
            this.repositoryItemDuration1.ShowEmptyItem = true;
            this.repositoryItemDuration1.ValidateOnEnterKey = true;
            // 
            // sp06250_OM_Visit_Categories_With_BlankTableAdapter
            // 
            this.sp06250_OM_Visit_Categories_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // bsiRecordTicking
            // 
            this.bsiRecordTicking.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiRecordTicking.Caption = "Record <b>Ticking</b>";
            this.bsiRecordTicking.Id = 91;
            this.bsiRecordTicking.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiRecordTicking.ImageOptions.Image")));
            this.bsiRecordTicking.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTick),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUntick)});
            this.bsiRecordTicking.Name = "bsiRecordTicking";
            // 
            // bbiTick
            // 
            this.bbiTick.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiTick.Caption = "<b>Tick</b> Selected Records";
            this.bbiTick.Id = 92;
            this.bbiTick.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiTick.ImageOptions.Image")));
            this.bbiTick.Name = "bbiTick";
            this.bbiTick.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTick_ItemClick);
            // 
            // bbiUntick
            // 
            this.bbiUntick.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiUntick.Caption = "<b>Untick</b> Selected Records";
            this.bbiUntick.Id = 93;
            this.bbiUntick.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUntick.ImageOptions.Image")));
            this.bbiUntick.Name = "bbiUntick";
            this.bbiUntick.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUntick_ItemClick);
            // 
            // sp06144_OM_Visit_Wizard_Site_Contract_Start_DatesTableAdapter
            // 
            this.sp06144_OM_Visit_Wizard_Site_Contract_Start_DatesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06308_OM_Visit_TypesTableAdapter
            // 
            this.sp06308_OM_Visit_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // transitionManager1
            // 
            this.transitionManager1.FrameInterval = 5000;
            transition2.Control = this.xtraTabControl1;
            transition2.EasingMode = DevExpress.Data.Utils.EasingMode.EaseInOut;
            transition2.WaitingIndicatorProperties.Caption = "Loading...";
            this.transitionManager1.Transitions.Add(transition2);
            // 
            // sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter
            // 
            this.sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // timerWelcomePage
            // 
            this.timerWelcomePage.Interval = 2000;
            this.timerWelcomePage.Tick += new System.EventHandler(this.timerWelcomePage_Tick);
            // 
            // frm_OM_Visit_Wizard
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1234, 537);
            this.Controls.Add(this.xtraTabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Visit_Wizard";
            this.Text = "Visit Wizard - Operations";
            this.Activated += new System.EventHandler(this.frm_OM_Visit_Wizard_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Visit_Wizard_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Visit_Wizard_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageWelcome.ResumeLayout(false);
            this.xtraTabPageWelcome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.xtraTabPageStep1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06131OMVisitWizardClientContractsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).EndInit();
            this.xtraTabPageStep2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06132OMVisitWizardSiteContractsForClientContractBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditDefaultScheduleTemplateID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06141OMVisitTemplateHeadersWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            this.xtraTabPageStep3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl7)).EndInit();
            this.splitContainerControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06142OMVisitTemplateHeadersSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06143OMVisitTemplateItemsSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditOccurrenceCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupEndType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditRecurrenceEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditRecurrenceEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupRecurrence.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSellCalculationLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06134OMJobCalculationLevelDescriptorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditCostCalculationLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupNumberOfVisitsMethod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditGapBetweenVisitsDescriptor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06133OMVisitGapUnitDescriptorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditGapBetweenVisitsUnits.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditNumberOfVisits.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.panelControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedSiteContractCountTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit10.Properties)).EndInit();
            this.xtraTabPageStep4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            this.panelControl12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditApplyDate1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditApplyDate1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06144OMVisitWizardSiteContractStartDatesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).EndInit();
            this.xtraTabPageStep5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            this.panelControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.panelControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VisitTimeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitDescriptorComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitDurationSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.panelControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VisitDaysSeparationSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPage5GridView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06145OMVisitEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditVisitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditCostCalculationLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditFloat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditVisitCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06250OMVisitCategoriesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditVisitTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06308OMVisitTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditClientPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditDaysSeparation)).EndInit();
            this.xtraTabPage5Calendar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resourcesCheckedListBoxControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).EndInit();
            this.xtraTabPageFinish.ResumeLayout(false);
            this.xtraTabPageFinish.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseStaff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerBarController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageWelcome;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep1;
        private DevExpress.XtraEditors.SimpleButton btnWelcomeNext;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageFinish;
        private DevExpress.XtraEditors.SimpleButton btnStep1Previous;
        private DevExpress.XtraEditors.SimpleButton btnStep1Next;
        private DevExpress.XtraEditors.SimpleButton btnStep2Next;
        private DevExpress.XtraEditors.SimpleButton btnStep2Previous;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.SimpleButton btnFinish;
        private DevExpress.XtraEditors.SimpleButton btnFinishPrevious;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep4;
        private DevExpress.XtraEditors.SimpleButton btnStep4Next;
        private DevExpress.XtraEditors.SimpleButton btnStep4Previous;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit7;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep5;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.PictureEdit pictureEdit8;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.SimpleButton btnStep5Next;
        private DevExpress.XtraEditors.SimpleButton btnStep5Previous;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraBars.Bar barStep1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl3;
        private DevExpress.XtraBars.BarEditItem barEditItemDateRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateRange;
        private DevExpress.XtraBars.BarButtonItem bbiRefreshClientContracts;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeOK;
        private DevExpress.XtraBars.BarEditItem barEditItemActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colGCCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDirectorID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime3;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colActive1;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency3;
        private DevExpress.XtraGrid.Columns.GridColumn colYearlyPercentageIncrease;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage3;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckRPIDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLastClientPaymentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPreviousContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colGCCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colContractType;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDirector;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorType;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraEditors.LabelControl labelControlSelectedSiteCount;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep3;
        private DevExpress.XtraEditors.SimpleButton btnStep3Next;
        private DevExpress.XtraEditors.SimpleButton btnStep3Previous;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.PictureEdit pictureEdit10;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage4;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditChooseStaff;
        private DevExpress.XtraGrid.Columns.GridColumn colClientYearCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger;
        private DevExpress.XtraEditors.TextEdit SelectedSiteContractCountTextEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private System.Windows.Forms.BindingSource sp06131OMVisitWizardClientContractsBindingSource;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private DataSet_OM_VisitTableAdapters.sp06131_OM_Visit_Wizard_Client_ContractsTableAdapter sp06131_OM_Visit_Wizard_Client_ContractsTableAdapter;
        private System.Windows.Forms.BindingSource sp06132OMVisitWizardSiteContractsForClientContractBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate3;
        private DevExpress.XtraGrid.Columns.GridColumn colActive3;
        private DevExpress.XtraGrid.Columns.GridColumn colContractValue2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraGrid.Columns.GridColumn colYearlyPercentageIncrease3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage2;
        private DevExpress.XtraGrid.Columns.GridColumn colLastClientPaymentDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks4;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent3;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger2;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumberMax;
        private DataSet_OM_VisitTableAdapters.sp06132_OM_Visit_Wizard_Site_Contracts_For_Client_ContractTableAdapter sp06132_OM_Visit_Wizard_Site_Contracts_For_Client_ContractTableAdapter;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControlNumberOfVisits;
        private DevExpress.XtraEditors.SpinEdit spinEditNumberOfVisits;
        private DevExpress.XtraEditors.LabelControl labelControlGapBetweenVisits;
        private DevExpress.XtraEditors.SpinEdit spinEditGapBetweenVisitsUnits;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditGapBetweenVisitsDescriptor;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditCostCalculationLevel;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView17;
        private DevExpress.XtraEditors.LabelControl labelControlCostCalculationLevel;
        private System.Windows.Forms.BindingSource sp06134OMJobCalculationLevelDescriptorsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn129;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn130;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn131;
        private System.Windows.Forms.BindingSource sp06133OMVisitGapUnitDescriptorsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn126;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn127;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn128;
        private DataSet_OM_VisitTableAdapters.sp06133_OM_Visit_Gap_Unit_DescriptorsTableAdapter sp06133_OM_Visit_Gap_Unit_DescriptorsTableAdapter;
        private DataSet_OM_VisitTableAdapters.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter;
        private DevExpress.XtraEditors.RadioGroup radioGroupNumberOfVisitsMethod;
        private System.Windows.Forms.BindingSource sp06142OMVisitTemplateHeadersSelectBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06142_OM_Visit_Template_Headers_SelectTableAdapter sp06142_OM_Visit_Template_Headers_SelectTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colTemplateHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks5;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedItemCount;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl7;
        private DevExpress.XtraGrid.GridControl gridControl18;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView18;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit16;
        private System.Windows.Forms.BindingSource sp06143OMVisitTemplateItemsSelectBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTemplateItemID;
        private DevExpress.XtraGrid.Columns.GridColumn colTemplateHeaderID1;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsFromStart;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsFromStartDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCalculationLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderID1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks7;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsFromStartDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCalculationLevelDescriptor;
        private DataSet_OM_VisitTableAdapters.sp06143_OM_Visit_Template_Items_SelectTableAdapter sp06143_OM_Visit_Template_Items_SelectTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn132;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitStartDate;
        private DevExpress.XtraEditors.LabelControl labelControlSelectedClientCount;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.SimpleButton simpleButtonApplyDate1;
        private DevExpress.XtraEditors.DateEdit dateEditApplyDate1;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private System.Windows.Forms.BindingSource sp06145OMVisitEditBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colstrMode;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRecordIDs;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCalculationLevel1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditCostCalculationLevel;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit2View;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCost;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitSell;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLatitude;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditFloat;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName;
        private DataSet_OM_VisitTableAdapters.sp06145_OM_Visit_EditTableAdapter sp06145_OM_Visit_EditTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription2;
        private DevExpress.XtraScheduler.SchedulerControl schedulerControl1;
        private DevExpress.XtraScheduler.SchedulerStorage schedulerStorage1;
        private DevExpress.XtraScheduler.UI.PrintBar printBar1;
        private DevExpress.XtraScheduler.UI.PrintPreviewItem printPreviewItem1;
        private DevExpress.XtraScheduler.UI.PrintItem printItem1;
        private DevExpress.XtraScheduler.UI.PrintPageSetupItem printPageSetupItem1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraScheduler.UI.NavigatorBar navigatorBar1;
        private DevExpress.XtraScheduler.UI.NavigateViewBackwardItem navigateViewBackwardItem1;
        private DevExpress.XtraScheduler.UI.NavigateViewForwardItem navigateViewForwardItem1;
        private DevExpress.XtraScheduler.UI.GotoTodayItem gotoTodayItem1;
        private DevExpress.XtraScheduler.UI.ViewZoomInItem viewZoomInItem1;
        private DevExpress.XtraScheduler.UI.ViewZoomOutItem viewZoomOutItem1;
        private DevExpress.XtraScheduler.UI.ArrangeBar arrangeBar1;
        private DevExpress.XtraScheduler.UI.SwitchToDayViewItem switchToDayViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToWorkWeekViewItem switchToWorkWeekViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToWeekViewItem switchToWeekViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToMonthViewItem switchToMonthViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToTimelineViewItem switchToTimelineViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToGanttViewItem switchToGanttViewItem1;
        private DevExpress.XtraScheduler.UI.GroupByBar groupByBar1;
        private DevExpress.XtraScheduler.UI.GroupByNoneItem groupByNoneItem1;
        private DevExpress.XtraScheduler.UI.GroupByDateItem groupByDateItem1;
        private DevExpress.XtraScheduler.UI.GroupByResourceItem groupByResourceItem1;
        private DevExpress.XtraScheduler.UI.TimeScaleBar timeScaleBar1;
        private DevExpress.XtraScheduler.UI.SwitchTimeScalesItem switchTimeScalesItem1;
        private DevExpress.XtraScheduler.UI.ChangeScaleWidthItem changeScaleWidthItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraScheduler.UI.SwitchTimeScalesCaptionItem switchTimeScalesCaptionItem1;
        private DevExpress.XtraScheduler.UI.LayoutBar layoutBar1;
        private DevExpress.XtraScheduler.UI.SwitchCompressWeekendItem switchCompressWeekendItem1;
        private DevExpress.XtraScheduler.UI.SwitchShowWorkTimeOnlyItem switchShowWorkTimeOnlyItem1;
        private DevExpress.XtraScheduler.UI.SwitchCellsAutoHeightItem switchCellsAutoHeightItem1;
        private DevExpress.XtraScheduler.UI.ChangeSnapToCellsUIItem changeSnapToCellsUIItem1;
        private DevExpress.XtraScheduler.UI.EditOccurrenceUICommandItem editOccurrenceUICommandItem1;
        private DevExpress.XtraScheduler.UI.EditSeriesUICommandItem editSeriesUICommandItem1;
        private DevExpress.XtraScheduler.UI.DeleteOccurrenceItem deleteOccurrenceItem1;
        private DevExpress.XtraScheduler.UI.DeleteSeriesItem deleteSeriesItem1;
        private DevExpress.XtraScheduler.UI.RepositoryItemDuration repositoryItemDuration1;
        private DevExpress.XtraScheduler.UI.SchedulerBarController schedulerBarController1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5GridView;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5Calendar;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraScheduler.DateNavigator dateNavigator1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraScheduler.UI.ResourcesCheckedListBoxControl resourcesCheckedListBoxControl1;
        private DevExpress.XtraGrid.Columns.GridColumn colCalendarLabel;
        private DevExpress.XtraGrid.Columns.GridColumn colLabelColourID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkNumber;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCategoryID;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditVisitCategory;
        private System.Windows.Forms.BindingSource sp06250OMVisitCategoriesWithBlankBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitStatusID;
        private DataSet_OM_VisitTableAdapters.sp06250_OM_Visit_Categories_With_BlankTableAdapter sp06250_OM_Visit_Categories_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCategoryID1;
        private DevExpress.XtraBars.BarSubItem bsiRecordTicking;
        private DevExpress.XtraBars.BarButtonItem bbiTick;
        private DevExpress.XtraBars.BarButtonItem bbiUntick;
        private DevExpress.XtraGrid.Columns.GridColumn colDurationUnits;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraGrid.Columns.GridColumn colDurationUnitsDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colDurationUnitsDescriptorID;
        private System.Windows.Forms.BindingSource sp06144OMVisitWizardSiteContractStartDatesBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06144_OM_Visit_Wizard_Site_Contract_Start_DatesTableAdapter sp06144_OM_Visit_Wizard_Site_Contract_Start_DatesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitTypeID;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditVisitTypes;
        private System.Windows.Forms.BindingSource sp06308OMVisitTypesBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder1;
        private DataSet_OM_VisitTableAdapters.sp06308_OM_Visit_TypesTableAdapter sp06308_OM_Visit_TypesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditClientPONumber;
        private DevExpress.Utils.Animation.TransitionManager transitionManager1;
        private DevExpress.XtraGrid.Columns.GridColumn colSellCalculationLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colSellCalculationLevelDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colSellCalculationLevel1;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditSellCalculationLevel;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.LabelControl labelControlSellCalculationLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultScheduleTemplateID;
        private DevExpress.XtraEditors.LabelControl labelControlDefaultScheduleTemplateUnavailable;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditDefaultScheduleTemplateID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private System.Windows.Forms.BindingSource sp06141OMVisitTemplateHeadersWithBlankBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromVisitTemplateID;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHold;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldReasonID;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldReason;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHold1;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldReasonID1;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldReason1;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCategory;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraScheduler.UI.YearlyRecurrenceControl yearlyRecurrenceControl1;
        private DevExpress.XtraScheduler.UI.WeeklyRecurrenceControl weeklyRecurrenceControl1;
        private DevExpress.XtraScheduler.UI.MonthlyRecurrenceControl monthlyRecurrenceControl1;
        private DevExpress.XtraScheduler.UI.DailyRecurrenceControl dailyRecurrenceControl1;
        private DevExpress.XtraEditors.RadioGroup radioGroupRecurrence;
        private DevExpress.XtraEditors.DateEdit dateEditRecurrenceEnd;
        private DevExpress.XtraEditors.LabelControl labelControlOccurrences;
        private DevExpress.XtraEditors.SpinEdit spinEditOccurrenceCount;
        private DevExpress.XtraEditors.RadioGroup radioGroupEndType;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysSeparation;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditDaysSeparation;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractDaysSeparationPercent;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractDaysSeparationPercent1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractDaysSeparationPercent2;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.SpinEdit VisitDaysSeparationSpinEdit;
        private DevExpress.XtraEditors.SimpleButton btnApplyVisitDaysSeparation;
        private DevExpress.XtraEditors.SimpleButton btnCalculateVisitDaysSeparation;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.SpinEdit VisitDurationSpinEdit;
        private DevExpress.XtraEditors.SimpleButton btnApplyVisitDuration;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.ComboBoxEdit VisitDescriptorComboBoxEdit;
        private DevExpress.XtraEditors.TimeSpanEdit VisitTimeEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceLevel;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.SimpleButton btnApplyVisitNumber;
        private DevExpress.XtraEditors.SpinEdit VisitNumberSpinEdit;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private System.Windows.Forms.Timer timerWelcomePage;
    }
}

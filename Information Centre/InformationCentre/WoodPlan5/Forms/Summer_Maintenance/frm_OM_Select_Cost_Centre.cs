using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Select_Cost_Centre : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        public string _Mode = "single";  // single or multiple //
        public int intOriginalBillingCodeID = 0;
        public string strOriginalBillingCodeIDs = "";
        public string strOriginalDepartmentIDs = "";

        public int intSelectedBillingCentreCodeID = 0;
        public string strSelectedCostCentreIDs = "";
        public int intSelectedCostCentreID = 0;
        public string strSelectedCostCentreCodes = "";
        public string strSelectedCostCentres = "";
        public string strSelectedBillingCentreCode = "";

        BaseObjects.GridCheckMarksSelection selection1;

        #endregion

        public frm_OM_Select_Cost_Centre()
        {
            InitializeComponent();
        }

        private void frm_OM_Select_Cost_Centre_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500128;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp06065_OM_Select_Cost_CentreTableAdapter.Connection.ConnectionString = strConnectionString;
             GridView view = (GridView)gridControl1.MainView;
            //view.Appearance.FocusedRow.BackColor = Color.FromArgb(60, 0, 0, 240);

            LoadData();
            gridControl1.ForceInitialize();

            if (_Mode != "single")
            {
                // Add record selection checkboxes to popup grid control //
                selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
                selection1.CheckMarkColumn.VisibleIndex = 0;
                selection1.CheckMarkColumn.Width = 30;

                Array arrayRecords = strOriginalBillingCodeIDs.Split(',');  // Single quotes because char expected for delimeter //
                view.BeginUpdate();
                int intFoundRow = 0;
                foreach (string strElement in arrayRecords)
                {
                    if (strElement == "") break;
                    intFoundRow = view.LocateByValue(0, view.Columns["BillingCentreCodeID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
                view.EndUpdate();
            }
            else  // Single selection mode //
            {
                if (intOriginalBillingCodeID != 0)  // Record selected so try to find and highlight //
                {
                    int intFoundRow = view.LocateByValue(0, view.Columns["BillingCentreCodeID"], intOriginalBillingCodeID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intFoundRow;
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
            }
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp06065_OM_Select_Cost_CentreTableAdapter.Fill(dataSet_OM_Contract.sp06065_OM_Select_Cost_Centre, strOriginalDepartmentIDs);
            view.EndUpdate();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Billing Centre Codes Available");
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (_Mode != "single")
            {
                if (string.IsNullOrEmpty(strSelectedCostCentreCodes))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records by ticking them before proceeding.", "Select Billing Centre Code", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else  // Single selection mode //
            {
                if (intSelectedBillingCentreCodeID == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Billing Centre Code", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            if (_Mode != "single")
            {
                strSelectedCostCentreCodes = "";    // Reset any prior values first //
                strSelectedBillingCentreCode = "";  // Reset any prior values first //
                if (selection1.SelectedCount <= 0) return;

                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSelectedCostCentreCodes += Convert.ToString(view.GetRowCellValue(i, "BillingCentreCodeID")) + ",";
                        if (intCount == 0)
                        {
                            strSelectedBillingCentreCode = Convert.ToString(view.GetRowCellValue(i, "CostCentreCode")) + " - " + Convert.ToString(view.GetRowCellValue(i, "CostCentre"));
                        }
                        else if (intCount >= 1)
                        {
                            strSelectedBillingCentreCode += ", " + Convert.ToString(view.GetRowCellValue(i, "CostCentreCode")) + " - " + Convert.ToString(view.GetRowCellValue(i, "CostCentre"));
                        }
                        intCount++;
                    }
                }
            }
            else  // Single record selection //
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                {
                    intSelectedBillingCentreCodeID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "BillingCentreCodeID"));
                    strSelectedBillingCentreCode = Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "CostCentreCode")) + " - " + Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "CostCentre"));
                    strSelectedCostCentreCodes = Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "CostCentreCode"));
                    strSelectedCostCentres = Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "CostCentre"));
                    intSelectedCostCentreID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "CostCentreID"));        
                }
            }
        }



    }
}


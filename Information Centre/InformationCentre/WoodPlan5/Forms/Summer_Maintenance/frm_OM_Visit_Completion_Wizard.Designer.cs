﻿namespace WoodPlan5
{
    partial class frm_OM_Visit_Completion_Wizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Visit_Completion_Wizard));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule4 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue4 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule5 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue5 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule6 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue6 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.Animation.Transition transition1 = new DevExpress.Utils.Animation.Transition();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageWelcome = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.btnWelcomeNext = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.xtraTabPageStep1 = new DevExpress.XtraTab.XtraTabPage();
            this.labelControlSelectedVisitCount = new DevExpress.XtraEditors.LabelControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp06413OMVisitCompletionWizardGetVisitsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVisitID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colImagesFolderOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumericLatLong = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSiteLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSiteContractEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colLinkedToParent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFriendlyVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedOutstandingJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeEditInteger = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.colExpectedStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysAllowed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditVisitStatusID = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06415OMVisitCompletionWizardVisitStatusesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colStartLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientSignaturePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditChoosePath = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colIssueTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditIssueTypeID = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06251OMIssueTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRework = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExtraWorkRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExtraWorkTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditExtraWorkTypeID = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06267OMExtraWorkTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExtraWorkComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManagerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCharacter50 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colManagerNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoOnetoSign = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletionSheetNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueFound = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.btnStep1Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep1Previous = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPageStep2 = new DevExpress.XtraTab.XtraTabPage();
            this.labelControlSelectedJobCount = new DevExpress.XtraEditors.LabelControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp06416OMVisitCompletionWizardGetJobsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colFriendlyVisitNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colExpectedStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpectedEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysLeeway = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEditStartDate = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colActualDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colActualDurationUnitsDescriptionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditUnitDescriptor = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06177OMJobDurationDescriptorsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEditEndDate = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colJobStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditJobStatusID = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06418OMVisitCompletionWizardJobStatusesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCancelledReasonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditCancelledReasonID = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06268OMCancelledReasonsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartLatitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumericLatLong2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colStartLongitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLatitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLongitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colJobNoLongerRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoWorkRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManuallyCompleted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPayContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRework1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnStep2Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep2Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit7 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageStep3 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.sp06419OMVisitCompletionWizardLabourBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLabourUsedID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colFriendlyVisitNumber2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobExpectedStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colFullDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalExternal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDateTime1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDateTime1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colJobTypeJobSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedTimingCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl9 = new DevExpress.XtraGrid.GridControl();
            this.sp06420OMVisitCompletionWizardLabourTimeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colstrMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRecordIDs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourUsedTimingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourUsedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamMemberID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEditStartTime = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colEndDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEditEndTime = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colDuration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHours = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colPdaCreatedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamMemberName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditTeamMemberName = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDatetime3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVisitNumber2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFriendlyVisitNumber3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeJobSubType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControlSelectedLabourCount = new DevExpress.XtraEditors.LabelControl();
            this.btnStep3Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep3Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageFinish = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.btnFinish = new DevExpress.XtraEditors.SimpleButton();
            this.btnFinishPrevious = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.repositoryItemSpinEdit2DP8 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemGridLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemSpinEditCurrency8 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemSpinEditPercentage8 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemMemoExEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemButtonEditContractorName = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemTextEditDateTime8 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemButtonEditLinkedToPersonType = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemButtonEditChooseStaff = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemDuration1 = new DevExpress.XtraScheduler.UI.RepositoryItemDuration();
            this.bsiRecordTicking = new DevExpress.XtraBars.BarSubItem();
            this.bbiTick = new DevExpress.XtraBars.BarButtonItem();
            this.bbiUntick = new DevExpress.XtraBars.BarButtonItem();
            this.transitionManager1 = new DevExpress.Utils.Animation.TransitionManager(this.components);
            this.sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            this.sp06413_OM_Visit_Completion_Wizard_Get_VisitsTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06413_OM_Visit_Completion_Wizard_Get_VisitsTableAdapter();
            this.sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_BlankTableAdapter();
            this.sp06251_OM_Issue_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06251_OM_Issue_Types_With_BlankTableAdapter();
            this.sp06267_OM_Extra_Work_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06267_OM_Extra_Work_Types_With_BlankTableAdapter();
            this.sp06416_OM_Visit_Completion_Wizard_Get_JobsTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06416_OM_Visit_Completion_Wizard_Get_JobsTableAdapter();
            this.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter();
            this.sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter();
            this.sp06418_OM_Visit_Completion_Wizard_Job_StatusesTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06418_OM_Visit_Completion_Wizard_Job_StatusesTableAdapter();
            this.sp06419_OM_Visit_Completion_Wizard_LabourTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06419_OM_Visit_Completion_Wizard_LabourTableAdapter();
            this.sp06420_OM_Visit_Completion_Wizard_Labour_TimeTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06420_OM_Visit_Completion_Wizard_Labour_TimeTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageWelcome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.xtraTabPageStep1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06413OMVisitCompletionWizardGetVisitsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericLatLong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEditInteger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditVisitStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06415OMVisitCompletionWizardVisitStatusesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChoosePath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditIssueTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06251OMIssueTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditExtraWorkTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06267OMExtraWorkTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCharacter50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            this.xtraTabPageStep2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06416OMVisitCompletionWizardGetJobsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditStartDate.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditUnitDescriptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06177OMJobDurationDescriptorsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditEndDate.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditJobStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06418OMVisitCompletionWizardJobStatusesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditCancelledReasonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06268OMCancelledReasonsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericLatLong2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).BeginInit();
            this.xtraTabPageStep3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06419OMVisitCompletionWizardLabourBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06420OMVisitCompletionWizardLabourTimeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditStartTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditStartTime.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditEndTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditEndTime.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditTeamMemberName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDatetime3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            this.xtraTabPageFinish.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2DP8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCurrency8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercentage8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditContractorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditLinkedToPersonType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseStaff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiRecordTicking, true)});
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1178, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Size = new System.Drawing.Size(1178, 0);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1178, 0);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Images = this.imageCollection2;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bsiRecordTicking,
            this.bbiTick,
            this.bbiUntick});
            this.barManager1.MaxItemId = 96;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit4,
            this.repositoryItemSpinEdit1,
            this.repositoryItemDuration1});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "ID";
            this.gridColumn10.FieldName = "ID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 53;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 53;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 53;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "ID";
            this.gridColumn7.FieldName = "ID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 53;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "ID";
            this.gridColumn16.FieldName = "ID";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Width = 53;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "ID";
            this.gridColumn13.FieldName = "ID";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 53;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, -1);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
            this.xtraTabControl1.Size = new System.Drawing.Size(1182, 542);
            this.xtraTabControl1.TabIndex = 6;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageWelcome,
            this.xtraTabPageStep1,
            this.xtraTabPageStep2,
            this.xtraTabPageStep3,
            this.xtraTabPageFinish});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            this.xtraTabControl1.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.xtraTabControl1_SelectedPageChanging);
            // 
            // xtraTabPageWelcome
            // 
            this.xtraTabPageWelcome.Controls.Add(this.panelControl1);
            this.xtraTabPageWelcome.Controls.Add(this.btnWelcomeNext);
            this.xtraTabPageWelcome.Controls.Add(this.labelControl2);
            this.xtraTabPageWelcome.Controls.Add(this.pictureEdit1);
            this.xtraTabPageWelcome.Name = "xtraTabPageWelcome";
            this.xtraTabPageWelcome.Size = new System.Drawing.Size(1155, 537);
            this.xtraTabPageWelcome.Tag = "0";
            this.xtraTabPageWelcome.Text = "Welcome";
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.pictureEdit4);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Location = new System.Drawing.Point(211, 6);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(938, 48);
            this.panelControl1.TabIndex = 17;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit4.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit4.Location = new System.Drawing.Point(894, 4);
            this.pictureEdit4.MenuManager = this.barManager1;
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Properties.ReadOnly = true;
            this.pictureEdit4.Properties.ShowMenu = false;
            this.pictureEdit4.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit4.TabIndex = 16;
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(5, 1);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(463, 29);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "Welcome to the <b>Visit Completion Wizard</b>";
            // 
            // labelControl9
            // 
            this.labelControl9.AllowHtmlString = true;
            this.labelControl9.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl9.Appearance.Options.UseBackColor = true;
            this.labelControl9.Location = new System.Drawing.Point(9, 31);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(572, 13);
            this.labelControl9.TabIndex = 15;
            this.labelControl9.Text = "This wizard will allow you to set one or more Visits as Completed as well as reco" +
    "rd Start and End times and Labour used.";
            // 
            // btnWelcomeNext
            // 
            this.btnWelcomeNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWelcomeNext.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnWelcomeNext.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnWelcomeNext.Location = new System.Drawing.Point(1061, 501);
            this.btnWelcomeNext.Name = "btnWelcomeNext";
            this.btnWelcomeNext.Size = new System.Drawing.Size(88, 30);
            this.btnWelcomeNext.TabIndex = 7;
            this.btnWelcomeNext.Text = "Next";
            this.btnWelcomeNext.Click += new System.EventHandler(this.btnWelcomeNext_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.AllowHtmlString = true;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(220, 104);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(220, 25);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Click <b>Next</b> To Continue";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = global::WoodPlan5.Properties.Resources.wizard_image;
            this.pictureEdit1.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(205, 537);
            this.pictureEdit1.TabIndex = 4;
            // 
            // xtraTabPageStep1
            // 
            this.xtraTabPageStep1.Controls.Add(this.labelControlSelectedVisitCount);
            this.xtraTabPageStep1.Controls.Add(this.gridControl2);
            this.xtraTabPageStep1.Controls.Add(this.panelControl4);
            this.xtraTabPageStep1.Controls.Add(this.btnStep1Next);
            this.xtraTabPageStep1.Controls.Add(this.btnStep1Previous);
            this.xtraTabPageStep1.Name = "xtraTabPageStep1";
            this.xtraTabPageStep1.Size = new System.Drawing.Size(1155, 537);
            this.xtraTabPageStep1.Tag = "1";
            this.xtraTabPageStep1.Text = "Step 1";
            // 
            // labelControlSelectedVisitCount
            // 
            this.labelControlSelectedVisitCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControlSelectedVisitCount.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlSelectedVisitCount.Appearance.ImageIndex = 10;
            this.labelControlSelectedVisitCount.Appearance.ImageList = this.imageCollection1;
            this.labelControlSelectedVisitCount.Appearance.Options.UseImageAlign = true;
            this.labelControlSelectedVisitCount.Appearance.Options.UseImageIndex = true;
            this.labelControlSelectedVisitCount.Appearance.Options.UseImageList = true;
            this.labelControlSelectedVisitCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlSelectedVisitCount.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControlSelectedVisitCount.Location = new System.Drawing.Point(6, 497);
            this.labelControlSelectedVisitCount.Name = "labelControlSelectedVisitCount";
            this.labelControlSelectedVisitCount.Size = new System.Drawing.Size(159, 17);
            this.labelControlSelectedVisitCount.TabIndex = 27;
            this.labelControlSelectedVisitCount.Text = "0 Selected Visits";
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 6);
            this.imageCollection1.Images.SetKeyName(6, "refresh_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.BlockEdit_16x16, "BlockEdit_16x16", typeof(global::WoodPlan5.Properties.Resources), 7);
            this.imageCollection1.Images.SetKeyName(7, "BlockEdit_16x16");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 8);
            this.imageCollection1.Images.SetKeyName(8, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("groupheader_16x16.png", "images/reports/groupheader_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/reports/groupheader_16x16.png"), 9);
            this.imageCollection1.Images.SetKeyName(9, "groupheader_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 10);
            this.imageCollection1.Images.SetKeyName(10, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 11);
            this.imageCollection1.Images.SetKeyName(11, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.BlockAdd_16x16, "BlockAdd_16x16", typeof(global::WoodPlan5.Properties.Resources), 12);
            this.imageCollection1.Images.SetKeyName(12, "BlockAdd_16x16");
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp06413OMVisitCompletionWizardGetVisitsBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Block Edit selected Record(s)", "block edit")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(7, 62);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemDateEdit1,
            this.repositoryItemGridLookUpEditVisitStatusID,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTimeEditInteger,
            this.repositoryItemTextEditCharacter50,
            this.repositoryItemTextEditNumericLatLong,
            this.repositoryItemButtonEditChoosePath,
            this.repositoryItemGridLookUpEditIssueTypeID,
            this.repositoryItemGridLookUpEditExtraWorkTypeID});
            this.gridControl2.Size = new System.Drawing.Size(1142, 433);
            this.gridControl2.TabIndex = 16;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp06413OMVisitCompletionWizardGetVisitsBindingSource
            // 
            this.sp06413OMVisitCompletionWizardGetVisitsBindingSource.DataMember = "sp06413_OM_Visit_Completion_Wizard_Get_Visits";
            this.sp06413OMVisitCompletionWizardGetVisitsBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVisitID1,
            this.colSiteContractID,
            this.colClientContractID,
            this.colClientID,
            this.colSiteID,
            this.colVisitNumber1,
            this.colClientName1,
            this.colImagesFolderOM,
            this.colSiteName,
            this.colSiteCode,
            this.colSitePostcode,
            this.colSiteLocationX,
            this.colSiteLocationY,
            this.colSiteAddress,
            this.colSiteContractStartDate,
            this.colSiteContractEndDate,
            this.colSiteContractActive,
            this.colLinkedToParent,
            this.colVisitCategory,
            this.colFriendlyVisitNumber,
            this.colVisitType,
            this.colLinkedOutstandingJobCount,
            this.colExpectedStartDate1,
            this.colExpectedEndDate1,
            this.colDaysAllowed,
            this.colStartDate,
            this.colEndDate,
            this.colVisitStatusID,
            this.colRemarks,
            this.colStartLatitude,
            this.colStartLongitude,
            this.colFinishLatitude,
            this.colFinishLongitude,
            this.colClientSignaturePath,
            this.colIssueTypeID,
            this.colRework,
            this.colExtraWorkRequired,
            this.colExtraWorkTypeID,
            this.colExtraWorkComments,
            this.colManagerName,
            this.colManagerNotes,
            this.colNoOnetoSign,
            this.colCompletionSheetNumber,
            this.colContractDescription,
            this.colIssueFound});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 3;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colExpectedStartDate1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            this.gridView2.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView2_ValidatingEditor);
            // 
            // colVisitID1
            // 
            this.colVisitID1.Caption = "Visit ID";
            this.colVisitID1.FieldName = "VisitID";
            this.colVisitID1.Name = "colVisitID1";
            this.colVisitID1.OptionsColumn.AllowEdit = false;
            this.colVisitID1.OptionsColumn.AllowFocus = false;
            this.colVisitID1.OptionsColumn.ReadOnly = true;
            // 
            // colSiteContractID
            // 
            this.colSiteContractID.Caption = "Site Contract ID";
            this.colSiteContractID.FieldName = "SiteContractID";
            this.colSiteContractID.Name = "colSiteContractID";
            this.colSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSiteContractID.Width = 96;
            // 
            // colClientContractID
            // 
            this.colClientContractID.Caption = "Client Contract ID";
            this.colClientContractID.FieldName = "ClientContractID";
            this.colClientContractID.Name = "colClientContractID";
            this.colClientContractID.OptionsColumn.AllowEdit = false;
            this.colClientContractID.OptionsColumn.AllowFocus = false;
            this.colClientContractID.OptionsColumn.ReadOnly = true;
            this.colClientContractID.Width = 105;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colVisitNumber1
            // 
            this.colVisitNumber1.Caption = "Visit Number";
            this.colVisitNumber1.FieldName = "VisitNumber";
            this.colVisitNumber1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colVisitNumber1.Name = "colVisitNumber1";
            this.colVisitNumber1.OptionsColumn.AllowEdit = false;
            this.colVisitNumber1.OptionsColumn.AllowFocus = false;
            this.colVisitNumber1.OptionsColumn.ReadOnly = true;
            this.colVisitNumber1.Visible = true;
            this.colVisitNumber1.VisibleIndex = 0;
            this.colVisitNumber1.Width = 89;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 4;
            this.colClientName1.Width = 325;
            // 
            // colImagesFolderOM
            // 
            this.colImagesFolderOM.Caption = "Images Folder";
            this.colImagesFolderOM.FieldName = "ImagesFolderOM";
            this.colImagesFolderOM.Name = "colImagesFolderOM";
            this.colImagesFolderOM.OptionsColumn.AllowEdit = false;
            this.colImagesFolderOM.OptionsColumn.AllowFocus = false;
            this.colImagesFolderOM.OptionsColumn.ReadOnly = true;
            this.colImagesFolderOM.Width = 183;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 18;
            this.colSiteName.Width = 352;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            // 
            // colSitePostcode
            // 
            this.colSitePostcode.Caption = "Site Postcode";
            this.colSitePostcode.FieldName = "SitePostcode";
            this.colSitePostcode.Name = "colSitePostcode";
            this.colSitePostcode.OptionsColumn.AllowEdit = false;
            this.colSitePostcode.OptionsColumn.AllowFocus = false;
            this.colSitePostcode.OptionsColumn.ReadOnly = true;
            this.colSitePostcode.Width = 84;
            // 
            // colSiteLocationX
            // 
            this.colSiteLocationX.Caption = "Site Latitude";
            this.colSiteLocationX.ColumnEdit = this.repositoryItemTextEditNumericLatLong;
            this.colSiteLocationX.FieldName = "SiteLocationX";
            this.colSiteLocationX.Name = "colSiteLocationX";
            this.colSiteLocationX.OptionsColumn.AllowEdit = false;
            this.colSiteLocationX.OptionsColumn.AllowFocus = false;
            this.colSiteLocationX.OptionsColumn.ReadOnly = true;
            this.colSiteLocationX.Width = 79;
            // 
            // repositoryItemTextEditNumericLatLong
            // 
            this.repositoryItemTextEditNumericLatLong.AutoHeight = false;
            this.repositoryItemTextEditNumericLatLong.Mask.EditMask = "n8";
            this.repositoryItemTextEditNumericLatLong.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumericLatLong.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumericLatLong.Name = "repositoryItemTextEditNumericLatLong";
            // 
            // colSiteLocationY
            // 
            this.colSiteLocationY.Caption = "Site Longitude";
            this.colSiteLocationY.ColumnEdit = this.repositoryItemTextEditNumericLatLong;
            this.colSiteLocationY.FieldName = "SiteLocationY";
            this.colSiteLocationY.Name = "colSiteLocationY";
            this.colSiteLocationY.OptionsColumn.AllowEdit = false;
            this.colSiteLocationY.OptionsColumn.AllowFocus = false;
            this.colSiteLocationY.OptionsColumn.ReadOnly = true;
            this.colSiteLocationY.Width = 87;
            // 
            // colSiteAddress
            // 
            this.colSiteAddress.Caption = "Site Address";
            this.colSiteAddress.FieldName = "SiteAddress";
            this.colSiteAddress.Name = "colSiteAddress";
            this.colSiteAddress.OptionsColumn.AllowEdit = false;
            this.colSiteAddress.OptionsColumn.AllowFocus = false;
            this.colSiteAddress.OptionsColumn.ReadOnly = true;
            this.colSiteAddress.Width = 232;
            // 
            // colSiteContractStartDate
            // 
            this.colSiteContractStartDate.Caption = "Site Contract Start";
            this.colSiteContractStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSiteContractStartDate.FieldName = "SiteContractStartDate";
            this.colSiteContractStartDate.Name = "colSiteContractStartDate";
            this.colSiteContractStartDate.OptionsColumn.AllowEdit = false;
            this.colSiteContractStartDate.OptionsColumn.AllowFocus = false;
            this.colSiteContractStartDate.OptionsColumn.ReadOnly = true;
            this.colSiteContractStartDate.Width = 109;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colSiteContractEndDate
            // 
            this.colSiteContractEndDate.Caption = "Site Contract End";
            this.colSiteContractEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSiteContractEndDate.FieldName = "SiteContractEndDate";
            this.colSiteContractEndDate.Name = "colSiteContractEndDate";
            this.colSiteContractEndDate.OptionsColumn.AllowEdit = false;
            this.colSiteContractEndDate.OptionsColumn.AllowFocus = false;
            this.colSiteContractEndDate.OptionsColumn.ReadOnly = true;
            this.colSiteContractEndDate.Width = 103;
            // 
            // colSiteContractActive
            // 
            this.colSiteContractActive.Caption = "Site Contract Active";
            this.colSiteContractActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSiteContractActive.FieldName = "SiteContractActive";
            this.colSiteContractActive.Name = "colSiteContractActive";
            this.colSiteContractActive.OptionsColumn.AllowEdit = false;
            this.colSiteContractActive.OptionsColumn.AllowFocus = false;
            this.colSiteContractActive.OptionsColumn.ReadOnly = true;
            this.colSiteContractActive.Width = 115;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            this.repositoryItemCheckEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemCheckEdit1_EditValueChanged);
            // 
            // colLinkedToParent
            // 
            this.colLinkedToParent.Caption = "Linked To";
            this.colLinkedToParent.FieldName = "LinkedToParent";
            this.colLinkedToParent.Name = "colLinkedToParent";
            this.colLinkedToParent.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent.Width = 444;
            // 
            // colVisitCategory
            // 
            this.colVisitCategory.Caption = "Visit Category";
            this.colVisitCategory.FieldName = "VisitCategory";
            this.colVisitCategory.Name = "colVisitCategory";
            this.colVisitCategory.OptionsColumn.AllowEdit = false;
            this.colVisitCategory.OptionsColumn.AllowFocus = false;
            this.colVisitCategory.OptionsColumn.ReadOnly = true;
            this.colVisitCategory.Visible = true;
            this.colVisitCategory.VisibleIndex = 5;
            this.colVisitCategory.Width = 131;
            // 
            // colFriendlyVisitNumber
            // 
            this.colFriendlyVisitNumber.Caption = "Firendly Visit #";
            this.colFriendlyVisitNumber.FieldName = "FriendlyVisitNumber";
            this.colFriendlyVisitNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colFriendlyVisitNumber.Name = "colFriendlyVisitNumber";
            this.colFriendlyVisitNumber.OptionsColumn.AllowEdit = false;
            this.colFriendlyVisitNumber.OptionsColumn.AllowFocus = false;
            this.colFriendlyVisitNumber.OptionsColumn.ReadOnly = true;
            this.colFriendlyVisitNumber.Visible = true;
            this.colFriendlyVisitNumber.VisibleIndex = 1;
            this.colFriendlyVisitNumber.Width = 90;
            // 
            // colVisitType
            // 
            this.colVisitType.Caption = "Visit Type";
            this.colVisitType.FieldName = "VisitType";
            this.colVisitType.Name = "colVisitType";
            this.colVisitType.OptionsColumn.AllowEdit = false;
            this.colVisitType.OptionsColumn.AllowFocus = false;
            this.colVisitType.OptionsColumn.ReadOnly = true;
            this.colVisitType.Visible = true;
            this.colVisitType.VisibleIndex = 6;
            this.colVisitType.Width = 133;
            // 
            // colLinkedOutstandingJobCount
            // 
            this.colLinkedOutstandingJobCount.Caption = "Outstanding Job Count";
            this.colLinkedOutstandingJobCount.ColumnEdit = this.repositoryItemTimeEditInteger;
            this.colLinkedOutstandingJobCount.FieldName = "LinkedOutstandingJobCount";
            this.colLinkedOutstandingJobCount.Name = "colLinkedOutstandingJobCount";
            this.colLinkedOutstandingJobCount.OptionsColumn.AllowEdit = false;
            this.colLinkedOutstandingJobCount.OptionsColumn.AllowFocus = false;
            this.colLinkedOutstandingJobCount.OptionsColumn.ReadOnly = true;
            this.colLinkedOutstandingJobCount.Visible = true;
            this.colLinkedOutstandingJobCount.VisibleIndex = 26;
            this.colLinkedOutstandingJobCount.Width = 130;
            // 
            // repositoryItemTimeEditInteger
            // 
            this.repositoryItemTimeEditInteger.AutoHeight = false;
            this.repositoryItemTimeEditInteger.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTimeEditInteger.Mask.EditMask = "n0";
            this.repositoryItemTimeEditInteger.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTimeEditInteger.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTimeEditInteger.Name = "repositoryItemTimeEditInteger";
            // 
            // colExpectedStartDate1
            // 
            this.colExpectedStartDate1.Caption = "Expected Start";
            this.colExpectedStartDate1.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colExpectedStartDate1.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colExpectedStartDate1.Name = "colExpectedStartDate1";
            this.colExpectedStartDate1.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate1.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate1.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate1.Visible = true;
            this.colExpectedStartDate1.VisibleIndex = 2;
            this.colExpectedStartDate1.Width = 104;
            // 
            // colExpectedEndDate1
            // 
            this.colExpectedEndDate1.Caption = "Expected End";
            this.colExpectedEndDate1.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colExpectedEndDate1.FieldName = "ExpectedEndDate";
            this.colExpectedEndDate1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colExpectedEndDate1.Name = "colExpectedEndDate1";
            this.colExpectedEndDate1.OptionsColumn.AllowEdit = false;
            this.colExpectedEndDate1.OptionsColumn.AllowFocus = false;
            this.colExpectedEndDate1.OptionsColumn.ReadOnly = true;
            this.colExpectedEndDate1.Visible = true;
            this.colExpectedEndDate1.VisibleIndex = 3;
            this.colExpectedEndDate1.Width = 96;
            // 
            // colDaysAllowed
            // 
            this.colDaysAllowed.Caption = "Days Allowed";
            this.colDaysAllowed.ColumnEdit = this.repositoryItemTimeEditInteger;
            this.colDaysAllowed.FieldName = "DaysAllowed";
            this.colDaysAllowed.Name = "colDaysAllowed";
            this.colDaysAllowed.OptionsColumn.AllowEdit = false;
            this.colDaysAllowed.OptionsColumn.AllowFocus = false;
            this.colDaysAllowed.OptionsColumn.ReadOnly = true;
            this.colDaysAllowed.Visible = true;
            this.colDaysAllowed.VisibleIndex = 4;
            this.colDaysAllowed.Width = 83;
            // 
            // colStartDate
            // 
            this.colStartDate.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colStartDate.AppearanceCell.Options.UseBackColor = true;
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 7;
            this.colStartDate.Width = 120;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Mask.EditMask = "g";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // colEndDate
            // 
            this.colEndDate.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colEndDate.AppearanceCell.Options.UseBackColor = true;
            this.colEndDate.Caption = "End Date";
            this.colEndDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 8;
            this.colEndDate.Width = 120;
            // 
            // colVisitStatusID
            // 
            this.colVisitStatusID.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colVisitStatusID.AppearanceCell.Options.UseBackColor = true;
            this.colVisitStatusID.Caption = "Visit Status";
            this.colVisitStatusID.ColumnEdit = this.repositoryItemGridLookUpEditVisitStatusID;
            this.colVisitStatusID.FieldName = "VisitStatusID";
            this.colVisitStatusID.Name = "colVisitStatusID";
            this.colVisitStatusID.Visible = true;
            this.colVisitStatusID.VisibleIndex = 9;
            this.colVisitStatusID.Width = 149;
            // 
            // repositoryItemGridLookUpEditVisitStatusID
            // 
            this.repositoryItemGridLookUpEditVisitStatusID.AutoHeight = false;
            this.repositoryItemGridLookUpEditVisitStatusID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditVisitStatusID.DataSource = this.sp06415OMVisitCompletionWizardVisitStatusesWithBlankBindingSource;
            this.repositoryItemGridLookUpEditVisitStatusID.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditVisitStatusID.Name = "repositoryItemGridLookUpEditVisitStatusID";
            this.repositoryItemGridLookUpEditVisitStatusID.NullText = "";
            this.repositoryItemGridLookUpEditVisitStatusID.PopupView = this.repositoryItemGridLookUpEdit2View;
            this.repositoryItemGridLookUpEditVisitStatusID.ValueMember = "ID";
            // 
            // sp06415OMVisitCompletionWizardVisitStatusesWithBlankBindingSource
            // 
            this.sp06415OMVisitCompletionWizardVisitStatusesWithBlankBindingSource.DataMember = "sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_Blank";
            this.sp06415OMVisitCompletionWizardVisitStatusesWithBlankBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // repositoryItemGridLookUpEdit2View
            // 
            this.repositoryItemGridLookUpEdit2View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.repositoryItemGridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.gridColumn10;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.repositoryItemGridLookUpEdit2View.FormatRules.Add(gridFormatRule1);
            this.repositoryItemGridLookUpEdit2View.Name = "repositoryItemGridLookUpEdit2View";
            this.repositoryItemGridLookUpEdit2View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit2View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit2View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit2View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Visit Status";
            this.gridColumn11.FieldName = "Description";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            this.gridColumn11.Width = 220;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Order";
            this.gridColumn12.FieldName = "RecordOrder";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks
            // 
            this.colRemarks.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colRemarks.AppearanceCell.Options.UseBackColor = true;
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 10;
            this.colRemarks.Width = 129;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colStartLatitude
            // 
            this.colStartLatitude.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colStartLatitude.AppearanceCell.Options.UseBackColor = true;
            this.colStartLatitude.Caption = "Start Latitude";
            this.colStartLatitude.ColumnEdit = this.repositoryItemTextEditNumericLatLong;
            this.colStartLatitude.FieldName = "StartLatitude";
            this.colStartLatitude.Name = "colStartLatitude";
            this.colStartLatitude.Visible = true;
            this.colStartLatitude.VisibleIndex = 22;
            this.colStartLatitude.Width = 104;
            // 
            // colStartLongitude
            // 
            this.colStartLongitude.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colStartLongitude.AppearanceCell.Options.UseBackColor = true;
            this.colStartLongitude.Caption = "Start Longitude";
            this.colStartLongitude.ColumnEdit = this.repositoryItemTextEditNumericLatLong;
            this.colStartLongitude.FieldName = "StartLongitude";
            this.colStartLongitude.Name = "colStartLongitude";
            this.colStartLongitude.Visible = true;
            this.colStartLongitude.VisibleIndex = 23;
            this.colStartLongitude.Width = 101;
            // 
            // colFinishLatitude
            // 
            this.colFinishLatitude.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colFinishLatitude.AppearanceCell.Options.UseBackColor = true;
            this.colFinishLatitude.Caption = "Finish Latitude";
            this.colFinishLatitude.ColumnEdit = this.repositoryItemTextEditNumericLatLong;
            this.colFinishLatitude.FieldName = "FinishLatitude";
            this.colFinishLatitude.Name = "colFinishLatitude";
            this.colFinishLatitude.Visible = true;
            this.colFinishLatitude.VisibleIndex = 24;
            this.colFinishLatitude.Width = 88;
            // 
            // colFinishLongitude
            // 
            this.colFinishLongitude.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colFinishLongitude.AppearanceCell.Options.UseBackColor = true;
            this.colFinishLongitude.Caption = "Finish Longitude";
            this.colFinishLongitude.ColumnEdit = this.repositoryItemTextEditNumericLatLong;
            this.colFinishLongitude.FieldName = "FinishLongitude";
            this.colFinishLongitude.Name = "colFinishLongitude";
            this.colFinishLongitude.Visible = true;
            this.colFinishLongitude.VisibleIndex = 25;
            this.colFinishLongitude.Width = 96;
            // 
            // colClientSignaturePath
            // 
            this.colClientSignaturePath.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colClientSignaturePath.AppearanceCell.Options.UseBackColor = true;
            this.colClientSignaturePath.Caption = "Client Signature Path";
            this.colClientSignaturePath.ColumnEdit = this.repositoryItemButtonEditChoosePath;
            this.colClientSignaturePath.FieldName = "ClientSignaturePath";
            this.colClientSignaturePath.Name = "colClientSignaturePath";
            this.colClientSignaturePath.Visible = true;
            this.colClientSignaturePath.VisibleIndex = 12;
            this.colClientSignaturePath.Width = 188;
            // 
            // repositoryItemButtonEditChoosePath
            // 
            this.repositoryItemButtonEditChoosePath.AutoHeight = false;
            this.repositoryItemButtonEditChoosePath.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to Select Client Signature file.", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to View the linked Client Signature file.", "view", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditChoosePath.Name = "repositoryItemButtonEditChoosePath";
            this.repositoryItemButtonEditChoosePath.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditChoosePath.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditChoosePath_ButtonClick);
            // 
            // colIssueTypeID
            // 
            this.colIssueTypeID.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colIssueTypeID.AppearanceCell.Options.UseBackColor = true;
            this.colIssueTypeID.Caption = "Issue Type";
            this.colIssueTypeID.ColumnEdit = this.repositoryItemGridLookUpEditIssueTypeID;
            this.colIssueTypeID.FieldName = "IssueTypeID";
            this.colIssueTypeID.Name = "colIssueTypeID";
            this.colIssueTypeID.Visible = true;
            this.colIssueTypeID.VisibleIndex = 17;
            this.colIssueTypeID.Width = 121;
            // 
            // repositoryItemGridLookUpEditIssueTypeID
            // 
            this.repositoryItemGridLookUpEditIssueTypeID.AutoHeight = false;
            this.repositoryItemGridLookUpEditIssueTypeID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditIssueTypeID.DataSource = this.sp06251OMIssueTypesWithBlankBindingSource;
            this.repositoryItemGridLookUpEditIssueTypeID.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditIssueTypeID.Name = "repositoryItemGridLookUpEditIssueTypeID";
            this.repositoryItemGridLookUpEditIssueTypeID.PopupView = this.gridView1;
            this.repositoryItemGridLookUpEditIssueTypeID.ValueMember = "ID";
            // 
            // sp06251OMIssueTypesWithBlankBindingSource
            // 
            this.sp06251OMIssueTypesWithBlankBindingSource.DataMember = "sp06251_OM_Issue_Types_With_Blank";
            this.sp06251OMIssueTypesWithBlankBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.gridColumn1;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView1.FormatRules.Add(gridFormatRule2);
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Issue Type";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // colRework
            // 
            this.colRework.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colRework.AppearanceCell.Options.UseBackColor = true;
            this.colRework.Caption = "Rework";
            this.colRework.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colRework.FieldName = "Rework";
            this.colRework.Name = "colRework";
            this.colRework.Visible = true;
            this.colRework.VisibleIndex = 18;
            this.colRework.Width = 55;
            // 
            // colExtraWorkRequired
            // 
            this.colExtraWorkRequired.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colExtraWorkRequired.AppearanceCell.Options.UseBackColor = true;
            this.colExtraWorkRequired.Caption = "Extra Work Required";
            this.colExtraWorkRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colExtraWorkRequired.FieldName = "ExtraWorkRequired";
            this.colExtraWorkRequired.Name = "colExtraWorkRequired";
            this.colExtraWorkRequired.Visible = true;
            this.colExtraWorkRequired.VisibleIndex = 19;
            this.colExtraWorkRequired.Width = 119;
            // 
            // colExtraWorkTypeID
            // 
            this.colExtraWorkTypeID.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colExtraWorkTypeID.AppearanceCell.Options.UseBackColor = true;
            this.colExtraWorkTypeID.Caption = "Extra Work Type";
            this.colExtraWorkTypeID.ColumnEdit = this.repositoryItemGridLookUpEditExtraWorkTypeID;
            this.colExtraWorkTypeID.FieldName = "ExtraWorkTypeID";
            this.colExtraWorkTypeID.Name = "colExtraWorkTypeID";
            this.colExtraWorkTypeID.Visible = true;
            this.colExtraWorkTypeID.VisibleIndex = 20;
            this.colExtraWorkTypeID.Width = 146;
            // 
            // repositoryItemGridLookUpEditExtraWorkTypeID
            // 
            this.repositoryItemGridLookUpEditExtraWorkTypeID.AutoHeight = false;
            this.repositoryItemGridLookUpEditExtraWorkTypeID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditExtraWorkTypeID.DataSource = this.sp06267OMExtraWorkTypesWithBlankBindingSource;
            this.repositoryItemGridLookUpEditExtraWorkTypeID.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditExtraWorkTypeID.Name = "repositoryItemGridLookUpEditExtraWorkTypeID";
            this.repositoryItemGridLookUpEditExtraWorkTypeID.PopupView = this.gridView3;
            this.repositoryItemGridLookUpEditExtraWorkTypeID.ValueMember = "ID";
            // 
            // sp06267OMExtraWorkTypesWithBlankBindingSource
            // 
            this.sp06267OMExtraWorkTypesWithBlankBindingSource.DataMember = "sp06267_OM_Extra_Work_Types_With_Blank";
            this.sp06267OMExtraWorkTypesWithBlankBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Column = this.gridColumn4;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue3.Value1 = 0;
            gridFormatRule3.Rule = formatConditionRuleValue3;
            this.gridView3.FormatRules.Add(gridFormatRule3);
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Extra Work type";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "RecordOrder";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            // 
            // colExtraWorkComments
            // 
            this.colExtraWorkComments.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colExtraWorkComments.AppearanceCell.Options.UseBackColor = true;
            this.colExtraWorkComments.Caption = "Extra Work Comments";
            this.colExtraWorkComments.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colExtraWorkComments.FieldName = "ExtraWorkComments";
            this.colExtraWorkComments.Name = "colExtraWorkComments";
            this.colExtraWorkComments.Visible = true;
            this.colExtraWorkComments.VisibleIndex = 21;
            this.colExtraWorkComments.Width = 126;
            // 
            // colManagerName
            // 
            this.colManagerName.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colManagerName.AppearanceCell.Options.UseBackColor = true;
            this.colManagerName.Caption = "Manager Name";
            this.colManagerName.ColumnEdit = this.repositoryItemTextEditCharacter50;
            this.colManagerName.FieldName = "ManagerName";
            this.colManagerName.Name = "colManagerName";
            this.colManagerName.Visible = true;
            this.colManagerName.VisibleIndex = 13;
            this.colManagerName.Width = 126;
            // 
            // repositoryItemTextEditCharacter50
            // 
            this.repositoryItemTextEditCharacter50.AutoHeight = false;
            this.repositoryItemTextEditCharacter50.MaxLength = 50;
            this.repositoryItemTextEditCharacter50.Name = "repositoryItemTextEditCharacter50";
            // 
            // colManagerNotes
            // 
            this.colManagerNotes.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colManagerNotes.AppearanceCell.Options.UseBackColor = true;
            this.colManagerNotes.Caption = "Manager Notes";
            this.colManagerNotes.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colManagerNotes.FieldName = "ManagerNotes";
            this.colManagerNotes.Name = "colManagerNotes";
            this.colManagerNotes.Visible = true;
            this.colManagerNotes.VisibleIndex = 14;
            this.colManagerNotes.Width = 92;
            // 
            // colNoOnetoSign
            // 
            this.colNoOnetoSign.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colNoOnetoSign.AppearanceCell.Options.UseBackColor = true;
            this.colNoOnetoSign.Caption = "No One To Sign";
            this.colNoOnetoSign.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNoOnetoSign.FieldName = "NoOnetoSign";
            this.colNoOnetoSign.Name = "colNoOnetoSign";
            this.colNoOnetoSign.Visible = true;
            this.colNoOnetoSign.VisibleIndex = 11;
            this.colNoOnetoSign.Width = 93;
            // 
            // colCompletionSheetNumber
            // 
            this.colCompletionSheetNumber.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colCompletionSheetNumber.AppearanceCell.Options.UseBackColor = true;
            this.colCompletionSheetNumber.Caption = "Completion Sheet Number";
            this.colCompletionSheetNumber.ColumnEdit = this.repositoryItemTextEditCharacter50;
            this.colCompletionSheetNumber.FieldName = "CompletionSheetNumber";
            this.colCompletionSheetNumber.Name = "colCompletionSheetNumber";
            this.colCompletionSheetNumber.Visible = true;
            this.colCompletionSheetNumber.VisibleIndex = 15;
            this.colCompletionSheetNumber.Width = 143;
            // 
            // colContractDescription
            // 
            this.colContractDescription.Caption = "Contract Description";
            this.colContractDescription.FieldName = "ContractDescription";
            this.colContractDescription.Name = "colContractDescription";
            this.colContractDescription.OptionsColumn.AllowEdit = false;
            this.colContractDescription.OptionsColumn.AllowFocus = false;
            this.colContractDescription.OptionsColumn.ReadOnly = true;
            this.colContractDescription.Visible = true;
            this.colContractDescription.VisibleIndex = 29;
            this.colContractDescription.Width = 289;
            // 
            // colIssueFound
            // 
            this.colIssueFound.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colIssueFound.AppearanceCell.Options.UseBackColor = true;
            this.colIssueFound.Caption = "Issue Found";
            this.colIssueFound.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIssueFound.FieldName = "IssueFound";
            this.colIssueFound.Name = "colIssueFound";
            this.colIssueFound.Visible = true;
            this.colIssueFound.VisibleIndex = 16;
            this.colIssueFound.Width = 78;
            // 
            // panelControl4
            // 
            this.panelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl4.Controls.Add(this.pictureEdit5);
            this.panelControl4.Controls.Add(this.labelControl7);
            this.panelControl4.Controls.Add(this.labelControl8);
            this.panelControl4.Location = new System.Drawing.Point(7, 6);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1142, 48);
            this.panelControl4.TabIndex = 15;
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit5.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit5.Location = new System.Drawing.Point(1098, 4);
            this.pictureEdit5.MenuManager = this.barManager1;
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Properties.ReadOnly = true;
            this.pictureEdit5.Properties.ShowMenu = false;
            this.pictureEdit5.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit5.TabIndex = 9;
            // 
            // labelControl7
            // 
            this.labelControl7.AllowHtmlString = true;
            this.labelControl7.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseBackColor = true;
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(5, 5);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(269, 16);
            this.labelControl7.TabIndex = 6;
            this.labelControl7.Text = "<b>Step 1:</b> Set the Status for the selected Visit(s)";
            // 
            // labelControl8
            // 
            this.labelControl8.AllowHtmlString = true;
            this.labelControl8.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl8.Appearance.Options.UseBackColor = true;
            this.labelControl8.Location = new System.Drawing.Point(57, 29);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(409, 13);
            this.labelControl8.TabIndex = 7;
            this.labelControl8.Text = "Select the Visit Status then enter the Start and End date\\time. Click Next when d" +
    "one.";
            // 
            // btnStep1Next
            // 
            this.btnStep1Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep1Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep1Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep1Next.Location = new System.Drawing.Point(1061, 501);
            this.btnStep1Next.Name = "btnStep1Next";
            this.btnStep1Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep1Next.TabIndex = 0;
            this.btnStep1Next.Text = "Next";
            this.btnStep1Next.Click += new System.EventHandler(this.btnStep1Next_Click);
            // 
            // btnStep1Previous
            // 
            this.btnStep1Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep1Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep1Previous.Location = new System.Drawing.Point(967, 501);
            this.btnStep1Previous.Name = "btnStep1Previous";
            this.btnStep1Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep1Previous.TabIndex = 10;
            this.btnStep1Previous.Text = "Previous";
            this.btnStep1Previous.Click += new System.EventHandler(this.btnStep1Previous_Click);
            // 
            // xtraTabPageStep2
            // 
            this.xtraTabPageStep2.Controls.Add(this.labelControlSelectedJobCount);
            this.xtraTabPageStep2.Controls.Add(this.gridControl4);
            this.xtraTabPageStep2.Controls.Add(this.btnStep2Next);
            this.xtraTabPageStep2.Controls.Add(this.btnStep2Previous);
            this.xtraTabPageStep2.Controls.Add(this.panelControl5);
            this.xtraTabPageStep2.Name = "xtraTabPageStep2";
            this.xtraTabPageStep2.Size = new System.Drawing.Size(1155, 537);
            this.xtraTabPageStep2.Tag = "2";
            this.xtraTabPageStep2.Text = "Step 2";
            // 
            // labelControlSelectedJobCount
            // 
            this.labelControlSelectedJobCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControlSelectedJobCount.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlSelectedJobCount.Appearance.ImageIndex = 10;
            this.labelControlSelectedJobCount.Appearance.ImageList = this.imageCollection1;
            this.labelControlSelectedJobCount.Appearance.Options.UseImageAlign = true;
            this.labelControlSelectedJobCount.Appearance.Options.UseImageIndex = true;
            this.labelControlSelectedJobCount.Appearance.Options.UseImageList = true;
            this.labelControlSelectedJobCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlSelectedJobCount.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControlSelectedJobCount.Location = new System.Drawing.Point(6, 497);
            this.labelControlSelectedJobCount.Name = "labelControlSelectedJobCount";
            this.labelControlSelectedJobCount.Size = new System.Drawing.Size(159, 17);
            this.labelControlSelectedJobCount.TabIndex = 26;
            this.labelControlSelectedJobCount.Text = "0 Selected Jobs";
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.sp06416OMVisitCompletionWizardGetJobsBindingSource;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Block Edit Selected Records", "block edit")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(7, 62);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit8,
            this.repositoryItemTextEditDateTime4,
            this.repositoryItemTextEditInteger4,
            this.repositoryItemDateEditStartDate,
            this.repositoryItemSpinEdit2,
            this.repositoryItemGridLookUpEditUnitDescriptor,
            this.repositoryItemGridLookUpEditJobStatusID,
            this.repositoryItemGridLookUpEditCancelledReasonID,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditNumericLatLong2,
            this.repositoryItemDateEditEndDate});
            this.gridControl4.Size = new System.Drawing.Size(1142, 433);
            this.gridControl4.TabIndex = 25;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp06416OMVisitCompletionWizardGetJobsBindingSource
            // 
            this.sp06416OMVisitCompletionWizardGetJobsBindingSource.DataMember = "sp06416_OM_Visit_Completion_Wizard_Get_Jobs";
            this.sp06416OMVisitCompletionWizardGetJobsBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobID,
            this.colVisitID,
            this.colSiteContractID1,
            this.colVisitNumber,
            this.colFriendlyVisitNumber1,
            this.colClientContractID1,
            this.colSiteID1,
            this.colClientID1,
            this.colSiteName1,
            this.colClientName,
            this.colContractDescription1,
            this.colLinkedToParent1,
            this.colJobSubTypeID,
            this.colJobSubType,
            this.colJobTypeID,
            this.colJobType,
            this.colReactive,
            this.colExpectedStartDate,
            this.colExpectedEndDate,
            this.colDaysLeeway,
            this.colLabourCount,
            this.colActualStartDate,
            this.colActualDurationUnits,
            this.colActualDurationUnitsDescriptionID,
            this.colActualEndDate,
            this.colJobStatusID,
            this.colCancelledReasonID,
            this.colStartLatitude1,
            this.colStartLongitude1,
            this.colFinishLatitude1,
            this.colFinishLongitude1,
            this.colRemarks1,
            this.colJobNoLongerRequired,
            this.colNoWorkRequired,
            this.colManuallyCompleted,
            this.colDoNotPayContractor,
            this.colDoNotInvoiceClient,
            this.colRework1});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsFind.AlwaysVisible = true;
            this.gridView4.OptionsFind.FindDelay = 2000;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsLayout.StoreFormatRules = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractDescription1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFriendlyVisitNumber1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colExpectedStartDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            this.gridView4.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView4_ValidatingEditor);
            // 
            // colJobID
            // 
            this.colJobID.Caption = "Job ID";
            this.colJobID.FieldName = "JobID";
            this.colJobID.Name = "colJobID";
            this.colJobID.OptionsColumn.AllowEdit = false;
            this.colJobID.OptionsColumn.AllowFocus = false;
            this.colJobID.OptionsColumn.ReadOnly = true;
            // 
            // colVisitID
            // 
            this.colVisitID.Caption = "Visit ID";
            this.colVisitID.FieldName = "VisitID";
            this.colVisitID.Name = "colVisitID";
            this.colVisitID.OptionsColumn.AllowEdit = false;
            this.colVisitID.OptionsColumn.AllowFocus = false;
            this.colVisitID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteContractID1
            // 
            this.colSiteContractID1.Caption = "Site Contract ID";
            this.colSiteContractID1.FieldName = "SiteContractID";
            this.colSiteContractID1.Name = "colSiteContractID1";
            this.colSiteContractID1.OptionsColumn.AllowEdit = false;
            this.colSiteContractID1.OptionsColumn.AllowFocus = false;
            this.colSiteContractID1.OptionsColumn.ReadOnly = true;
            this.colSiteContractID1.Width = 96;
            // 
            // colVisitNumber
            // 
            this.colVisitNumber.Caption = "Visit #";
            this.colVisitNumber.ColumnEdit = this.repositoryItemTextEditInteger4;
            this.colVisitNumber.FieldName = "VisitNumber";
            this.colVisitNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colVisitNumber.Name = "colVisitNumber";
            this.colVisitNumber.OptionsColumn.AllowEdit = false;
            this.colVisitNumber.OptionsColumn.AllowFocus = false;
            this.colVisitNumber.OptionsColumn.ReadOnly = true;
            this.colVisitNumber.Visible = true;
            this.colVisitNumber.VisibleIndex = 2;
            this.colVisitNumber.Width = 49;
            // 
            // repositoryItemTextEditInteger4
            // 
            this.repositoryItemTextEditInteger4.AutoHeight = false;
            this.repositoryItemTextEditInteger4.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger4.Name = "repositoryItemTextEditInteger4";
            // 
            // colFriendlyVisitNumber1
            // 
            this.colFriendlyVisitNumber1.Caption = "Friendly Visit #";
            this.colFriendlyVisitNumber1.FieldName = "FriendlyVisitNumber";
            this.colFriendlyVisitNumber1.Name = "colFriendlyVisitNumber1";
            this.colFriendlyVisitNumber1.OptionsColumn.AllowEdit = false;
            this.colFriendlyVisitNumber1.OptionsColumn.AllowFocus = false;
            this.colFriendlyVisitNumber1.OptionsColumn.ReadOnly = true;
            this.colFriendlyVisitNumber1.Visible = true;
            this.colFriendlyVisitNumber1.VisibleIndex = 3;
            this.colFriendlyVisitNumber1.Width = 90;
            // 
            // colClientContractID1
            // 
            this.colClientContractID1.Caption = "Client Contract ID";
            this.colClientContractID1.FieldName = "ClientContractID";
            this.colClientContractID1.Name = "colClientContractID1";
            this.colClientContractID1.OptionsColumn.AllowEdit = false;
            this.colClientContractID1.OptionsColumn.AllowFocus = false;
            this.colClientContractID1.OptionsColumn.ReadOnly = true;
            this.colClientContractID1.Width = 105;
            // 
            // colSiteID1
            // 
            this.colSiteID1.Caption = "Site ID";
            this.colSiteID1.FieldName = "SiteID";
            this.colSiteID1.Name = "colSiteID1";
            this.colSiteID1.OptionsColumn.AllowEdit = false;
            this.colSiteID1.OptionsColumn.AllowFocus = false;
            this.colSiteID1.OptionsColumn.ReadOnly = true;
            this.colSiteID1.Width = 51;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            this.colClientID1.Width = 60;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Visible = true;
            this.colSiteName1.VisibleIndex = 14;
            this.colSiteName1.Width = 180;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 25;
            this.colClientName.Width = 184;
            // 
            // colContractDescription1
            // 
            this.colContractDescription1.Caption = "Contract Description";
            this.colContractDescription1.FieldName = "ContractDescription";
            this.colContractDescription1.Name = "colContractDescription1";
            this.colContractDescription1.OptionsColumn.AllowEdit = false;
            this.colContractDescription1.OptionsColumn.AllowFocus = false;
            this.colContractDescription1.OptionsColumn.ReadOnly = true;
            this.colContractDescription1.Visible = true;
            this.colContractDescription1.VisibleIndex = 28;
            this.colContractDescription1.Width = 284;
            // 
            // colLinkedToParent1
            // 
            this.colLinkedToParent1.Caption = "Linked To";
            this.colLinkedToParent1.FieldName = "LinkedToParent";
            this.colLinkedToParent1.Name = "colLinkedToParent1";
            this.colLinkedToParent1.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent1.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent1.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent1.Width = 370;
            // 
            // colJobSubTypeID
            // 
            this.colJobSubTypeID.Caption = "Job Sub-Type ID";
            this.colJobSubTypeID.FieldName = "JobSubTypeID";
            this.colJobSubTypeID.Name = "colJobSubTypeID";
            this.colJobSubTypeID.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeID.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeID.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeID.Width = 99;
            // 
            // colJobSubType
            // 
            this.colJobSubType.Caption = "Job Sub-Type";
            this.colJobSubType.FieldName = "JobSubType";
            this.colJobSubType.Name = "colJobSubType";
            this.colJobSubType.OptionsColumn.AllowEdit = false;
            this.colJobSubType.OptionsColumn.AllowFocus = false;
            this.colJobSubType.OptionsColumn.ReadOnly = true;
            this.colJobSubType.Visible = true;
            this.colJobSubType.VisibleIndex = 5;
            this.colJobSubType.Width = 140;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            this.colJobTypeID.Width = 77;
            // 
            // colJobType
            // 
            this.colJobType.Caption = "Job Type";
            this.colJobType.FieldName = "JobType";
            this.colJobType.Name = "colJobType";
            this.colJobType.OptionsColumn.AllowEdit = false;
            this.colJobType.OptionsColumn.AllowFocus = false;
            this.colJobType.OptionsColumn.ReadOnly = true;
            this.colJobType.Visible = true;
            this.colJobType.VisibleIndex = 4;
            this.colJobType.Width = 140;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 6;
            this.colReactive.Width = 61;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colExpectedStartDate
            // 
            this.colExpectedStartDate.Caption = "Expected Start";
            this.colExpectedStartDate.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colExpectedStartDate.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colExpectedStartDate.Name = "colExpectedStartDate";
            this.colExpectedStartDate.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate.Visible = true;
            this.colExpectedStartDate.VisibleIndex = 0;
            this.colExpectedStartDate.Width = 148;
            // 
            // repositoryItemTextEditDateTime4
            // 
            this.repositoryItemTextEditDateTime4.AutoHeight = false;
            this.repositoryItemTextEditDateTime4.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime4.Name = "repositoryItemTextEditDateTime4";
            // 
            // colExpectedEndDate
            // 
            this.colExpectedEndDate.Caption = "Expected End";
            this.colExpectedEndDate.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colExpectedEndDate.FieldName = "ExpectedEndDate";
            this.colExpectedEndDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colExpectedEndDate.Name = "colExpectedEndDate";
            this.colExpectedEndDate.OptionsColumn.AllowEdit = false;
            this.colExpectedEndDate.OptionsColumn.AllowFocus = false;
            this.colExpectedEndDate.OptionsColumn.ReadOnly = true;
            this.colExpectedEndDate.Visible = true;
            this.colExpectedEndDate.VisibleIndex = 1;
            this.colExpectedEndDate.Width = 100;
            // 
            // colDaysLeeway
            // 
            this.colDaysLeeway.Caption = "Days Leeway";
            this.colDaysLeeway.ColumnEdit = this.repositoryItemTextEditInteger4;
            this.colDaysLeeway.FieldName = "DaysLeeway";
            this.colDaysLeeway.Name = "colDaysLeeway";
            this.colDaysLeeway.OptionsColumn.AllowEdit = false;
            this.colDaysLeeway.OptionsColumn.AllowFocus = false;
            this.colDaysLeeway.OptionsColumn.ReadOnly = true;
            this.colDaysLeeway.Visible = true;
            this.colDaysLeeway.VisibleIndex = 3;
            this.colDaysLeeway.Width = 83;
            // 
            // colLabourCount
            // 
            this.colLabourCount.Caption = "Labour Count";
            this.colLabourCount.ColumnEdit = this.repositoryItemTextEditInteger4;
            this.colLabourCount.FieldName = "LabourCount";
            this.colLabourCount.Name = "colLabourCount";
            this.colLabourCount.OptionsColumn.AllowEdit = false;
            this.colLabourCount.OptionsColumn.AllowFocus = false;
            this.colLabourCount.OptionsColumn.ReadOnly = true;
            this.colLabourCount.Visible = true;
            this.colLabourCount.VisibleIndex = 24;
            this.colLabourCount.Width = 84;
            // 
            // colActualStartDate
            // 
            this.colActualStartDate.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colActualStartDate.AppearanceCell.Options.UseBackColor = true;
            this.colActualStartDate.Caption = "Start";
            this.colActualStartDate.ColumnEdit = this.repositoryItemDateEditStartDate;
            this.colActualStartDate.FieldName = "ActualStartDate";
            this.colActualStartDate.Name = "colActualStartDate";
            this.colActualStartDate.Visible = true;
            this.colActualStartDate.VisibleIndex = 7;
            this.colActualStartDate.Width = 120;
            // 
            // repositoryItemDateEditStartDate
            // 
            this.repositoryItemDateEditStartDate.AutoHeight = false;
            this.repositoryItemDateEditStartDate.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditStartDate.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditStartDate.Mask.EditMask = "g";
            this.repositoryItemDateEditStartDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEditStartDate.Name = "repositoryItemDateEditStartDate";
            this.repositoryItemDateEditStartDate.EditValueChanged += new System.EventHandler(this.repositoryItemDateEditStartDate_EditValueChanged);
            // 
            // colActualDurationUnits
            // 
            this.colActualDurationUnits.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colActualDurationUnits.AppearanceCell.Options.UseBackColor = true;
            this.colActualDurationUnits.Caption = "Duration Units";
            this.colActualDurationUnits.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colActualDurationUnits.FieldName = "ActualDurationUnits";
            this.colActualDurationUnits.Name = "colActualDurationUnits";
            this.colActualDurationUnits.Visible = true;
            this.colActualDurationUnits.VisibleIndex = 8;
            this.colActualDurationUnits.Width = 87;
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit2.Mask.EditMask = "f2";
            this.repositoryItemSpinEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            131072});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            this.repositoryItemSpinEdit2.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEdit2_EditValueChanged);
            // 
            // colActualDurationUnitsDescriptionID
            // 
            this.colActualDurationUnitsDescriptionID.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colActualDurationUnitsDescriptionID.AppearanceCell.Options.UseBackColor = true;
            this.colActualDurationUnitsDescriptionID.Caption = "Duration Descriptor";
            this.colActualDurationUnitsDescriptionID.ColumnEdit = this.repositoryItemGridLookUpEditUnitDescriptor;
            this.colActualDurationUnitsDescriptionID.FieldName = "ActualDurationUnitsDescriptionID";
            this.colActualDurationUnitsDescriptionID.Name = "colActualDurationUnitsDescriptionID";
            this.colActualDurationUnitsDescriptionID.Visible = true;
            this.colActualDurationUnitsDescriptionID.VisibleIndex = 9;
            this.colActualDurationUnitsDescriptionID.Width = 112;
            // 
            // repositoryItemGridLookUpEditUnitDescriptor
            // 
            this.repositoryItemGridLookUpEditUnitDescriptor.AutoHeight = false;
            this.repositoryItemGridLookUpEditUnitDescriptor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditUnitDescriptor.DataSource = this.sp06177OMJobDurationDescriptorsWithBlankBindingSource;
            this.repositoryItemGridLookUpEditUnitDescriptor.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditUnitDescriptor.Name = "repositoryItemGridLookUpEditUnitDescriptor";
            this.repositoryItemGridLookUpEditUnitDescriptor.NullText = "";
            this.repositoryItemGridLookUpEditUnitDescriptor.PopupView = this.gridView5;
            this.repositoryItemGridLookUpEditUnitDescriptor.ValueMember = "ID";
            this.repositoryItemGridLookUpEditUnitDescriptor.EditValueChanged += new System.EventHandler(this.repositoryItemGridLookUpEditUnitDescriptor_EditValueChanged);
            // 
            // sp06177OMJobDurationDescriptorsWithBlankBindingSource
            // 
            this.sp06177OMJobDurationDescriptorsWithBlankBindingSource.DataMember = "sp06177_OM_Job_Duration_Descriptors_With_Blank";
            this.sp06177OMJobDurationDescriptorsWithBlankBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule4.ApplyToRow = true;
            gridFormatRule4.Column = this.gridColumn7;
            gridFormatRule4.Name = "Format0";
            formatConditionRuleValue4.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue4.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue4.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue4.Value1 = 0;
            gridFormatRule4.Rule = formatConditionRuleValue4;
            this.gridView5.FormatRules.Add(gridFormatRule4);
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn9, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Unit Descriptor";
            this.gridColumn8.FieldName = "Description";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 220;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Order";
            this.gridColumn9.FieldName = "RecordOrder";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            // 
            // colActualEndDate
            // 
            this.colActualEndDate.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colActualEndDate.AppearanceCell.Options.UseBackColor = true;
            this.colActualEndDate.Caption = "End";
            this.colActualEndDate.ColumnEdit = this.repositoryItemDateEditEndDate;
            this.colActualEndDate.FieldName = "ActualEndDate";
            this.colActualEndDate.Name = "colActualEndDate";
            this.colActualEndDate.Visible = true;
            this.colActualEndDate.VisibleIndex = 10;
            this.colActualEndDate.Width = 120;
            // 
            // repositoryItemDateEditEndDate
            // 
            this.repositoryItemDateEditEndDate.AutoHeight = false;
            this.repositoryItemDateEditEndDate.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditEndDate.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditEndDate.Mask.EditMask = "g";
            this.repositoryItemDateEditEndDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEditEndDate.Name = "repositoryItemDateEditEndDate";
            this.repositoryItemDateEditEndDate.EditValueChanged += new System.EventHandler(this.repositoryItemDateEditEndDate_EditValueChanged);
            // 
            // colJobStatusID
            // 
            this.colJobStatusID.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colJobStatusID.AppearanceCell.Options.UseBackColor = true;
            this.colJobStatusID.Caption = "Job Status";
            this.colJobStatusID.ColumnEdit = this.repositoryItemGridLookUpEditJobStatusID;
            this.colJobStatusID.FieldName = "JobStatusID";
            this.colJobStatusID.Name = "colJobStatusID";
            this.colJobStatusID.Visible = true;
            this.colJobStatusID.VisibleIndex = 11;
            this.colJobStatusID.Width = 128;
            // 
            // repositoryItemGridLookUpEditJobStatusID
            // 
            this.repositoryItemGridLookUpEditJobStatusID.AutoHeight = false;
            this.repositoryItemGridLookUpEditJobStatusID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditJobStatusID.DataSource = this.sp06418OMVisitCompletionWizardJobStatusesBindingSource;
            this.repositoryItemGridLookUpEditJobStatusID.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditJobStatusID.Name = "repositoryItemGridLookUpEditJobStatusID";
            this.repositoryItemGridLookUpEditJobStatusID.NullText = "";
            this.repositoryItemGridLookUpEditJobStatusID.PopupView = this.gridView6;
            this.repositoryItemGridLookUpEditJobStatusID.ValueMember = "ID";
            this.repositoryItemGridLookUpEditJobStatusID.EditValueChanged += new System.EventHandler(this.repositoryItemGridLookUpEditJobStatusID_EditValueChanged);
            // 
            // sp06418OMVisitCompletionWizardJobStatusesBindingSource
            // 
            this.sp06418OMVisitCompletionWizardJobStatusesBindingSource.DataMember = "sp06418_OM_Visit_Completion_Wizard_Job_Statuses";
            this.sp06418OMVisitCompletionWizardJobStatusesBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18});
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule5.ApplyToRow = true;
            gridFormatRule5.Column = this.gridColumn16;
            gridFormatRule5.Name = "Format0";
            formatConditionRuleValue5.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue5.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue5.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue5.Value1 = 0;
            gridFormatRule5.Rule = formatConditionRuleValue5;
            this.gridView6.FormatRules.Add(gridFormatRule5);
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView6.OptionsView.ShowIndicator = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn18, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Job Status";
            this.gridColumn17.FieldName = "Description";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 0;
            this.gridColumn17.Width = 220;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Order";
            this.gridColumn18.FieldName = "RecordOrder";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            // 
            // colCancelledReasonID
            // 
            this.colCancelledReasonID.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colCancelledReasonID.AppearanceCell.Options.UseBackColor = true;
            this.colCancelledReasonID.Caption = "Cancelled Reason";
            this.colCancelledReasonID.ColumnEdit = this.repositoryItemGridLookUpEditCancelledReasonID;
            this.colCancelledReasonID.FieldName = "CancelledReasonID";
            this.colCancelledReasonID.Name = "colCancelledReasonID";
            this.colCancelledReasonID.Visible = true;
            this.colCancelledReasonID.VisibleIndex = 13;
            this.colCancelledReasonID.Width = 144;
            // 
            // repositoryItemGridLookUpEditCancelledReasonID
            // 
            this.repositoryItemGridLookUpEditCancelledReasonID.AutoHeight = false;
            this.repositoryItemGridLookUpEditCancelledReasonID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditCancelledReasonID.DataSource = this.sp06268OMCancelledReasonsWithBlankBindingSource;
            this.repositoryItemGridLookUpEditCancelledReasonID.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditCancelledReasonID.Name = "repositoryItemGridLookUpEditCancelledReasonID";
            this.repositoryItemGridLookUpEditCancelledReasonID.NullText = "";
            this.repositoryItemGridLookUpEditCancelledReasonID.PopupView = this.gridView7;
            this.repositoryItemGridLookUpEditCancelledReasonID.ValueMember = "ID";
            // 
            // sp06268OMCancelledReasonsWithBlankBindingSource
            // 
            this.sp06268OMCancelledReasonsWithBlankBindingSource.DataMember = "sp06268_OM_Cancelled_Reasons_With_Blank";
            this.sp06268OMCancelledReasonsWithBlankBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15});
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule6.ApplyToRow = true;
            gridFormatRule6.Column = this.gridColumn13;
            gridFormatRule6.Name = "Format0";
            formatConditionRuleValue6.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue6.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue6.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue6.Value1 = 0;
            gridFormatRule6.Rule = formatConditionRuleValue6;
            this.gridView7.FormatRules.Add(gridFormatRule6);
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn15, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Cancelled Reason";
            this.gridColumn14.FieldName = "Description";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 0;
            this.gridColumn14.Width = 220;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Order";
            this.gridColumn15.FieldName = "RecordOrder";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            // 
            // colStartLatitude1
            // 
            this.colStartLatitude1.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colStartLatitude1.AppearanceCell.Options.UseBackColor = true;
            this.colStartLatitude1.Caption = "Start Latitude";
            this.colStartLatitude1.ColumnEdit = this.repositoryItemTextEditNumericLatLong2;
            this.colStartLatitude1.FieldName = "StartLatitude";
            this.colStartLatitude1.Name = "colStartLatitude1";
            this.colStartLatitude1.Visible = true;
            this.colStartLatitude1.VisibleIndex = 20;
            this.colStartLatitude1.Width = 85;
            // 
            // repositoryItemTextEditNumericLatLong2
            // 
            this.repositoryItemTextEditNumericLatLong2.AutoHeight = false;
            this.repositoryItemTextEditNumericLatLong2.Mask.EditMask = "n8";
            this.repositoryItemTextEditNumericLatLong2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumericLatLong2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumericLatLong2.Name = "repositoryItemTextEditNumericLatLong2";
            // 
            // colStartLongitude1
            // 
            this.colStartLongitude1.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colStartLongitude1.AppearanceCell.Options.UseBackColor = true;
            this.colStartLongitude1.Caption = "Start Longitude";
            this.colStartLongitude1.ColumnEdit = this.repositoryItemTextEditNumericLatLong2;
            this.colStartLongitude1.FieldName = "StartLongitude";
            this.colStartLongitude1.Name = "colStartLongitude1";
            this.colStartLongitude1.Visible = true;
            this.colStartLongitude1.VisibleIndex = 21;
            this.colStartLongitude1.Width = 93;
            // 
            // colFinishLatitude1
            // 
            this.colFinishLatitude1.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colFinishLatitude1.AppearanceCell.Options.UseBackColor = true;
            this.colFinishLatitude1.Caption = "Finish Latitude";
            this.colFinishLatitude1.ColumnEdit = this.repositoryItemTextEditNumericLatLong2;
            this.colFinishLatitude1.FieldName = "FinishLatitude";
            this.colFinishLatitude1.Name = "colFinishLatitude1";
            this.colFinishLatitude1.Visible = true;
            this.colFinishLatitude1.VisibleIndex = 22;
            this.colFinishLatitude1.Width = 88;
            // 
            // colFinishLongitude1
            // 
            this.colFinishLongitude1.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colFinishLongitude1.AppearanceCell.Options.UseBackColor = true;
            this.colFinishLongitude1.Caption = "Finish Longitude";
            this.colFinishLongitude1.ColumnEdit = this.repositoryItemTextEditNumericLatLong2;
            this.colFinishLongitude1.FieldName = "FinishLongitude";
            this.colFinishLongitude1.Name = "colFinishLongitude1";
            this.colFinishLongitude1.Visible = true;
            this.colFinishLongitude1.VisibleIndex = 23;
            this.colFinishLongitude1.Width = 96;
            // 
            // colRemarks1
            // 
            this.colRemarks1.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colRemarks1.AppearanceCell.Options.UseBackColor = true;
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit8;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 17;
            this.colRemarks1.Width = 86;
            // 
            // repositoryItemMemoExEdit8
            // 
            this.repositoryItemMemoExEdit8.AutoHeight = false;
            this.repositoryItemMemoExEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit8.Name = "repositoryItemMemoExEdit8";
            this.repositoryItemMemoExEdit8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit8.ShowIcon = false;
            // 
            // colJobNoLongerRequired
            // 
            this.colJobNoLongerRequired.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colJobNoLongerRequired.AppearanceCell.Options.UseBackColor = true;
            this.colJobNoLongerRequired.Caption = "Job Not Required";
            this.colJobNoLongerRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colJobNoLongerRequired.FieldName = "JobNoLongerRequired";
            this.colJobNoLongerRequired.Name = "colJobNoLongerRequired";
            this.colJobNoLongerRequired.Visible = true;
            this.colJobNoLongerRequired.VisibleIndex = 14;
            this.colJobNoLongerRequired.Width = 102;
            // 
            // colNoWorkRequired
            // 
            this.colNoWorkRequired.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colNoWorkRequired.AppearanceCell.Options.UseBackColor = true;
            this.colNoWorkRequired.Caption = "No Work Required";
            this.colNoWorkRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colNoWorkRequired.FieldName = "NoWorkRequired";
            this.colNoWorkRequired.Name = "colNoWorkRequired";
            this.colNoWorkRequired.Visible = true;
            this.colNoWorkRequired.VisibleIndex = 15;
            this.colNoWorkRequired.Width = 106;
            // 
            // colManuallyCompleted
            // 
            this.colManuallyCompleted.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colManuallyCompleted.AppearanceCell.Options.UseBackColor = true;
            this.colManuallyCompleted.Caption = "Manually Completed";
            this.colManuallyCompleted.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colManuallyCompleted.FieldName = "ManuallyCompleted";
            this.colManuallyCompleted.Name = "colManuallyCompleted";
            this.colManuallyCompleted.Visible = true;
            this.colManuallyCompleted.VisibleIndex = 12;
            this.colManuallyCompleted.Width = 115;
            // 
            // colDoNotPayContractor
            // 
            this.colDoNotPayContractor.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colDoNotPayContractor.AppearanceCell.Options.UseBackColor = true;
            this.colDoNotPayContractor.Caption = "Don\'t Pay Team";
            this.colDoNotPayContractor.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDoNotPayContractor.FieldName = "DoNotPayContractor";
            this.colDoNotPayContractor.Name = "colDoNotPayContractor";
            this.colDoNotPayContractor.Visible = true;
            this.colDoNotPayContractor.VisibleIndex = 18;
            this.colDoNotPayContractor.Width = 94;
            // 
            // colDoNotInvoiceClient
            // 
            this.colDoNotInvoiceClient.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colDoNotInvoiceClient.AppearanceCell.Options.UseBackColor = true;
            this.colDoNotInvoiceClient.Caption = "Don\'t Invoice Client";
            this.colDoNotInvoiceClient.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDoNotInvoiceClient.FieldName = "DoNotInvoiceClient";
            this.colDoNotInvoiceClient.Name = "colDoNotInvoiceClient";
            this.colDoNotInvoiceClient.Visible = true;
            this.colDoNotInvoiceClient.VisibleIndex = 19;
            this.colDoNotInvoiceClient.Width = 112;
            // 
            // colRework1
            // 
            this.colRework1.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colRework1.AppearanceCell.Options.UseBackColor = true;
            this.colRework1.Caption = "Rework";
            this.colRework1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colRework1.FieldName = "Rework";
            this.colRework1.Name = "colRework1";
            this.colRework1.Visible = true;
            this.colRework1.VisibleIndex = 16;
            this.colRework1.Width = 55;
            // 
            // btnStep2Next
            // 
            this.btnStep2Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep2Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep2Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep2Next.Location = new System.Drawing.Point(1061, 501);
            this.btnStep2Next.Name = "btnStep2Next";
            this.btnStep2Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep2Next.TabIndex = 17;
            this.btnStep2Next.Text = "Next";
            this.btnStep2Next.Click += new System.EventHandler(this.btnStep2Next_Click);
            // 
            // btnStep2Previous
            // 
            this.btnStep2Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep2Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep2Previous.Location = new System.Drawing.Point(967, 501);
            this.btnStep2Previous.Name = "btnStep2Previous";
            this.btnStep2Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep2Previous.TabIndex = 18;
            this.btnStep2Previous.Text = "Previous";
            this.btnStep2Previous.Click += new System.EventHandler(this.btnStep2Previous_Click);
            // 
            // panelControl5
            // 
            this.panelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl5.Controls.Add(this.pictureEdit7);
            this.panelControl5.Controls.Add(this.labelControl4);
            this.panelControl5.Controls.Add(this.labelControl6);
            this.panelControl5.Location = new System.Drawing.Point(7, 6);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1142, 48);
            this.panelControl5.TabIndex = 16;
            // 
            // pictureEdit7
            // 
            this.pictureEdit7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit7.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit7.Location = new System.Drawing.Point(1098, 4);
            this.pictureEdit7.MenuManager = this.barManager1;
            this.pictureEdit7.Name = "pictureEdit7";
            this.pictureEdit7.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit7.Properties.ReadOnly = true;
            this.pictureEdit7.Properties.ShowMenu = false;
            this.pictureEdit7.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit7.TabIndex = 9;
            // 
            // labelControl4
            // 
            this.labelControl4.AllowHtmlString = true;
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseBackColor = true;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(5, 5);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(133, 16);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "<b>Step 2:</b> Set Job Status";
            // 
            // labelControl6
            // 
            this.labelControl6.AllowHtmlString = true;
            this.labelControl6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl6.Appearance.Options.UseBackColor = true;
            this.labelControl6.Location = new System.Drawing.Point(57, 29);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(407, 13);
            this.labelControl6.TabIndex = 7;
            this.labelControl6.Text = "Select the Job Status then enter the Start and End date\\time. Click Next when don" +
    "e.";
            // 
            // xtraTabPageStep3
            // 
            this.xtraTabPageStep3.Controls.Add(this.splitContainerControl1);
            this.xtraTabPageStep3.Controls.Add(this.labelControlSelectedLabourCount);
            this.xtraTabPageStep3.Controls.Add(this.btnStep3Next);
            this.xtraTabPageStep3.Controls.Add(this.btnStep3Previous);
            this.xtraTabPageStep3.Controls.Add(this.panelControl3);
            this.xtraTabPageStep3.Name = "xtraTabPageStep3";
            this.xtraTabPageStep3.Size = new System.Drawing.Size(1155, 537);
            this.xtraTabPageStep3.Tag = "3";
            this.xtraTabPageStep3.Text = "Step 3";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Location = new System.Drawing.Point(7, 62);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl8);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl9);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1142, 433);
            this.splitContainerControl1.SplitterPosition = 700;
            this.splitContainerControl1.TabIndex = 28;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridControl8
            // 
            this.gridControl8.DataSource = this.sp06419OMVisitCompletionWizardLabourBindingSource;
            this.gridControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl8.Location = new System.Drawing.Point(0, 0);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.MenuManager = this.barManager1;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEditDateTime5,
            this.repositoryItemTextEditInteger6});
            this.gridControl8.Size = new System.Drawing.Size(436, 433);
            this.gridControl8.TabIndex = 25;
            this.gridControl8.UseEmbeddedNavigator = true;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // sp06419OMVisitCompletionWizardLabourBindingSource
            // 
            this.sp06419OMVisitCompletionWizardLabourBindingSource.DataMember = "sp06419_OM_Visit_Completion_Wizard_Labour";
            this.sp06419OMVisitCompletionWizardLabourBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLabourUsedID1,
            this.colContractorID1,
            this.colJobID2,
            this.colVisitID3,
            this.colClientID2,
            this.colSiteID2,
            this.colSiteName2,
            this.colClientName2,
            this.colContractDescription2,
            this.colVisitNumber3,
            this.colFriendlyVisitNumber2,
            this.colJobSubTypeDescription,
            this.colJobTypeDescription,
            this.colJobExpectedStartDate1,
            this.colFullDescription,
            this.colContractorName1,
            this.colInternalExternal,
            this.colStartDateTime1,
            this.colEndDateTime1,
            this.colLinkedToPersonTypeID1,
            this.colLinkedToPersonType1,
            this.colRemarks2,
            this.colJobTypeJobSubType,
            this.colLinkedTimingCount,
            this.colClientContractID3,
            this.colSiteContractID3});
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.GroupCount = 5;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView8.OptionsFilter.AllowFilterEditor = false;
            this.gridView8.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView8.OptionsFilter.AllowMRUFilterList = false;
            this.gridView8.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView8.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView8.OptionsFind.FindDelay = 2000;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsLayout.StoreFormatRules = true;
            this.gridView8.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.MultiSelect = true;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractDescription2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFriendlyVisitNumber2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobTypeJobSubType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView8.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView8_CustomDrawCell);
            this.gridView8.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView8.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView8.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView8.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView8.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView8_MouseUp);
            this.gridView8.GotFocus += new System.EventHandler(this.gridView8_GotFocus);
            // 
            // colLabourUsedID1
            // 
            this.colLabourUsedID1.Caption = "Labour Used ID";
            this.colLabourUsedID1.FieldName = "LabourUsedID";
            this.colLabourUsedID1.Name = "colLabourUsedID1";
            this.colLabourUsedID1.OptionsColumn.AllowEdit = false;
            this.colLabourUsedID1.OptionsColumn.AllowFocus = false;
            this.colLabourUsedID1.OptionsColumn.ReadOnly = true;
            this.colLabourUsedID1.Width = 93;
            // 
            // colContractorID1
            // 
            this.colContractorID1.Caption = "Contractor ID";
            this.colContractorID1.FieldName = "ContractorID";
            this.colContractorID1.Name = "colContractorID1";
            this.colContractorID1.OptionsColumn.AllowEdit = false;
            this.colContractorID1.OptionsColumn.AllowFocus = false;
            this.colContractorID1.OptionsColumn.ReadOnly = true;
            this.colContractorID1.Width = 85;
            // 
            // colJobID2
            // 
            this.colJobID2.Caption = "Job ID";
            this.colJobID2.FieldName = "JobID";
            this.colJobID2.Name = "colJobID2";
            this.colJobID2.OptionsColumn.AllowEdit = false;
            this.colJobID2.OptionsColumn.AllowFocus = false;
            this.colJobID2.OptionsColumn.ReadOnly = true;
            // 
            // colVisitID3
            // 
            this.colVisitID3.Caption = "Visit ID";
            this.colVisitID3.FieldName = "VisitID";
            this.colVisitID3.Name = "colVisitID3";
            this.colVisitID3.OptionsColumn.AllowEdit = false;
            this.colVisitID3.OptionsColumn.AllowFocus = false;
            this.colVisitID3.OptionsColumn.ReadOnly = true;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            // 
            // colSiteID2
            // 
            this.colSiteID2.Caption = "Site ID";
            this.colSiteID2.FieldName = "SiteID";
            this.colSiteID2.Name = "colSiteID2";
            this.colSiteID2.OptionsColumn.AllowEdit = false;
            this.colSiteID2.OptionsColumn.AllowFocus = false;
            this.colSiteID2.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName2
            // 
            this.colSiteName2.Caption = "Site Name";
            this.colSiteName2.FieldName = "SiteName";
            this.colSiteName2.Name = "colSiteName2";
            this.colSiteName2.OptionsColumn.AllowEdit = false;
            this.colSiteName2.OptionsColumn.AllowFocus = false;
            this.colSiteName2.OptionsColumn.ReadOnly = true;
            this.colSiteName2.Visible = true;
            this.colSiteName2.VisibleIndex = 0;
            this.colSiteName2.Width = 145;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Visible = true;
            this.colClientName2.VisibleIndex = 10;
            this.colClientName2.Width = 156;
            // 
            // colContractDescription2
            // 
            this.colContractDescription2.Caption = "Contract Description";
            this.colContractDescription2.FieldName = "ContractDescription";
            this.colContractDescription2.Name = "colContractDescription2";
            this.colContractDescription2.OptionsColumn.AllowEdit = false;
            this.colContractDescription2.OptionsColumn.AllowFocus = false;
            this.colContractDescription2.OptionsColumn.ReadOnly = true;
            this.colContractDescription2.Visible = true;
            this.colContractDescription2.VisibleIndex = 11;
            this.colContractDescription2.Width = 272;
            // 
            // colVisitNumber3
            // 
            this.colVisitNumber3.Caption = "Visit #";
            this.colVisitNumber3.ColumnEdit = this.repositoryItemTextEditInteger6;
            this.colVisitNumber3.FieldName = "VisitNumber";
            this.colVisitNumber3.Name = "colVisitNumber3";
            this.colVisitNumber3.OptionsColumn.AllowEdit = false;
            this.colVisitNumber3.OptionsColumn.AllowFocus = false;
            this.colVisitNumber3.OptionsColumn.ReadOnly = true;
            this.colVisitNumber3.Visible = true;
            this.colVisitNumber3.VisibleIndex = 7;
            this.colVisitNumber3.Width = 51;
            // 
            // repositoryItemTextEditInteger6
            // 
            this.repositoryItemTextEditInteger6.AutoHeight = false;
            this.repositoryItemTextEditInteger6.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger6.Name = "repositoryItemTextEditInteger6";
            // 
            // colFriendlyVisitNumber2
            // 
            this.colFriendlyVisitNumber2.Caption = "Friendly Visit #";
            this.colFriendlyVisitNumber2.FieldName = "FriendlyVisitNumber";
            this.colFriendlyVisitNumber2.Name = "colFriendlyVisitNumber2";
            this.colFriendlyVisitNumber2.OptionsColumn.AllowEdit = false;
            this.colFriendlyVisitNumber2.OptionsColumn.AllowFocus = false;
            this.colFriendlyVisitNumber2.OptionsColumn.ReadOnly = true;
            this.colFriendlyVisitNumber2.Visible = true;
            this.colFriendlyVisitNumber2.VisibleIndex = 12;
            this.colFriendlyVisitNumber2.Width = 90;
            // 
            // colJobSubTypeDescription
            // 
            this.colJobSubTypeDescription.Caption = "Job Sub-Type";
            this.colJobSubTypeDescription.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription.Name = "colJobSubTypeDescription";
            this.colJobSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription.Width = 195;
            // 
            // colJobTypeDescription
            // 
            this.colJobTypeDescription.Caption = "Job Type";
            this.colJobTypeDescription.FieldName = "JobTypeDescription";
            this.colJobTypeDescription.Name = "colJobTypeDescription";
            this.colJobTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription.Width = 214;
            // 
            // colJobExpectedStartDate1
            // 
            this.colJobExpectedStartDate1.Caption = "Job Expected Start";
            this.colJobExpectedStartDate1.ColumnEdit = this.repositoryItemTextEditDateTime5;
            this.colJobExpectedStartDate1.FieldName = "JobExpectedStartDate";
            this.colJobExpectedStartDate1.Name = "colJobExpectedStartDate1";
            this.colJobExpectedStartDate1.OptionsColumn.AllowEdit = false;
            this.colJobExpectedStartDate1.OptionsColumn.AllowFocus = false;
            this.colJobExpectedStartDate1.OptionsColumn.ReadOnly = true;
            this.colJobExpectedStartDate1.Visible = true;
            this.colJobExpectedStartDate1.VisibleIndex = 3;
            this.colJobExpectedStartDate1.Width = 111;
            // 
            // repositoryItemTextEditDateTime5
            // 
            this.repositoryItemTextEditDateTime5.AutoHeight = false;
            this.repositoryItemTextEditDateTime5.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime5.Name = "repositoryItemTextEditDateTime5";
            // 
            // colFullDescription
            // 
            this.colFullDescription.Caption = "Linked To";
            this.colFullDescription.FieldName = "FullDescription";
            this.colFullDescription.Name = "colFullDescription";
            this.colFullDescription.OptionsColumn.AllowEdit = false;
            this.colFullDescription.OptionsColumn.AllowFocus = false;
            this.colFullDescription.OptionsColumn.ReadOnly = true;
            this.colFullDescription.Width = 501;
            // 
            // colContractorName1
            // 
            this.colContractorName1.Caption = "Team Name";
            this.colContractorName1.FieldName = "ContractorName";
            this.colContractorName1.Name = "colContractorName1";
            this.colContractorName1.OptionsColumn.AllowEdit = false;
            this.colContractorName1.OptionsColumn.AllowFocus = false;
            this.colContractorName1.OptionsColumn.ReadOnly = true;
            this.colContractorName1.Visible = true;
            this.colContractorName1.VisibleIndex = 0;
            this.colContractorName1.Width = 199;
            // 
            // colInternalExternal
            // 
            this.colInternalExternal.Caption = "Internal \\ External";
            this.colInternalExternal.FieldName = "InternalExternal";
            this.colInternalExternal.Name = "colInternalExternal";
            this.colInternalExternal.OptionsColumn.AllowEdit = false;
            this.colInternalExternal.OptionsColumn.AllowFocus = false;
            this.colInternalExternal.OptionsColumn.ReadOnly = true;
            this.colInternalExternal.Visible = true;
            this.colInternalExternal.VisibleIndex = 2;
            this.colInternalExternal.Width = 107;
            // 
            // colStartDateTime1
            // 
            this.colStartDateTime1.Caption = "Start Time";
            this.colStartDateTime1.ColumnEdit = this.repositoryItemTextEditDateTime5;
            this.colStartDateTime1.FieldName = "StartDateTime";
            this.colStartDateTime1.Name = "colStartDateTime1";
            this.colStartDateTime1.OptionsColumn.AllowEdit = false;
            this.colStartDateTime1.OptionsColumn.AllowFocus = false;
            this.colStartDateTime1.OptionsColumn.ReadOnly = true;
            this.colStartDateTime1.Visible = true;
            this.colStartDateTime1.VisibleIndex = 4;
            this.colStartDateTime1.Width = 100;
            // 
            // colEndDateTime1
            // 
            this.colEndDateTime1.Caption = "End Time";
            this.colEndDateTime1.ColumnEdit = this.repositoryItemTextEditDateTime5;
            this.colEndDateTime1.FieldName = "EndDateTime";
            this.colEndDateTime1.Name = "colEndDateTime1";
            this.colEndDateTime1.OptionsColumn.AllowEdit = false;
            this.colEndDateTime1.OptionsColumn.AllowFocus = false;
            this.colEndDateTime1.OptionsColumn.ReadOnly = true;
            this.colEndDateTime1.Visible = true;
            this.colEndDateTime1.VisibleIndex = 5;
            this.colEndDateTime1.Width = 100;
            // 
            // colLinkedToPersonTypeID1
            // 
            this.colLinkedToPersonTypeID1.Caption = "Labour Type ID";
            this.colLinkedToPersonTypeID1.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID1.Name = "colLinkedToPersonTypeID1";
            this.colLinkedToPersonTypeID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID1.Width = 93;
            // 
            // colLinkedToPersonType1
            // 
            this.colLinkedToPersonType1.Caption = "Labour Type";
            this.colLinkedToPersonType1.FieldName = "LinkedToPersonType";
            this.colLinkedToPersonType1.Name = "colLinkedToPersonType1";
            this.colLinkedToPersonType1.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonType1.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonType1.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonType1.Visible = true;
            this.colLinkedToPersonType1.VisibleIndex = 1;
            this.colLinkedToPersonType1.Width = 100;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 6;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colJobTypeJobSubType
            // 
            this.colJobTypeJobSubType.Caption = "Job Type \\ Job Sub-Type";
            this.colJobTypeJobSubType.FieldName = "JobTypeJobSubType";
            this.colJobTypeJobSubType.Name = "colJobTypeJobSubType";
            this.colJobTypeJobSubType.OptionsColumn.AllowEdit = false;
            this.colJobTypeJobSubType.OptionsColumn.AllowFocus = false;
            this.colJobTypeJobSubType.OptionsColumn.ReadOnly = true;
            this.colJobTypeJobSubType.Visible = true;
            this.colJobTypeJobSubType.VisibleIndex = 13;
            this.colJobTypeJobSubType.Width = 340;
            // 
            // colLinkedTimingCount
            // 
            this.colLinkedTimingCount.Caption = "Linked Timing Records";
            this.colLinkedTimingCount.ColumnEdit = this.repositoryItemTextEditInteger6;
            this.colLinkedTimingCount.FieldName = "LinkedTimingCount";
            this.colLinkedTimingCount.Name = "colLinkedTimingCount";
            this.colLinkedTimingCount.OptionsColumn.AllowEdit = false;
            this.colLinkedTimingCount.OptionsColumn.AllowFocus = false;
            this.colLinkedTimingCount.OptionsColumn.ReadOnly = true;
            this.colLinkedTimingCount.Visible = true;
            this.colLinkedTimingCount.VisibleIndex = 8;
            this.colLinkedTimingCount.Width = 124;
            // 
            // colClientContractID3
            // 
            this.colClientContractID3.Caption = "Client Contract ID";
            this.colClientContractID3.FieldName = "ClientContractID";
            this.colClientContractID3.Name = "colClientContractID3";
            this.colClientContractID3.OptionsColumn.AllowEdit = false;
            this.colClientContractID3.OptionsColumn.AllowFocus = false;
            this.colClientContractID3.OptionsColumn.ReadOnly = true;
            this.colClientContractID3.Width = 105;
            // 
            // colSiteContractID3
            // 
            this.colSiteContractID3.Caption = "Site Contract ID";
            this.colSiteContractID3.FieldName = "SiteContractID";
            this.colSiteContractID3.Name = "colSiteContractID3";
            this.colSiteContractID3.OptionsColumn.AllowEdit = false;
            this.colSiteContractID3.OptionsColumn.AllowFocus = false;
            this.colSiteContractID3.OptionsColumn.ReadOnly = true;
            this.colSiteContractID3.Width = 96;
            // 
            // gridControl9
            // 
            this.gridControl9.DataSource = this.sp06420OMVisitCompletionWizardLabourTimeBindingSource;
            this.gridControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl9.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl9.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl9.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 12, true, true, "Block Add New Records", "block add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Block Edit Selected Records", "block edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl9.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl9_EmbeddedNavigator_ButtonClick);
            this.gridControl9.Location = new System.Drawing.Point(0, 0);
            this.gridControl9.MainView = this.gridView9;
            this.gridControl9.MenuManager = this.barManager1;
            this.gridControl9.Name = "gridControl9";
            this.gridControl9.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEditStartTime,
            this.repositoryItemDateEditEndTime,
            this.repositoryItemTextEditHours,
            this.repositoryItemMemoExEdit3,
            this.repositoryItemButtonEditTeamMemberName,
            this.repositoryItemTextEditDatetime3});
            this.gridControl9.Size = new System.Drawing.Size(700, 433);
            this.gridControl9.TabIndex = 22;
            this.gridControl9.UseEmbeddedNavigator = true;
            this.gridControl9.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView9});
            // 
            // sp06420OMVisitCompletionWizardLabourTimeBindingSource
            // 
            this.sp06420OMVisitCompletionWizardLabourTimeBindingSource.DataMember = "sp06420_OM_Visit_Completion_Wizard_Labour_Time";
            this.sp06420OMVisitCompletionWizardLabourTimeBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colstrMode,
            this.colstrRecordIDs,
            this.colLabourUsedTimingID,
            this.colLabourUsedID,
            this.colTeamMemberID,
            this.colStartDateTime,
            this.colEndDateTime,
            this.colDuration,
            this.colRemarks3,
            this.colPdaCreatedID,
            this.colTeamMemberName,
            this.colContractorID,
            this.colJobID1,
            this.colClientID3,
            this.colSiteID3,
            this.colSiteName3,
            this.colClientName3,
            this.colJobSubTypeDescription1,
            this.colJobTypeDescription1,
            this.colExpectedStartDate2,
            this.colVisitNumber2,
            this.colFriendlyVisitNumber3,
            this.colFullDescription1,
            this.colContractorName,
            this.colVisitID2,
            this.colClientContractID2,
            this.colSiteContractID2,
            this.colJobTypeID1,
            this.colJobSubTypeID1,
            this.colContractDescription3,
            this.colLinkedToPersonTypeID,
            this.colLinkedToPersonType,
            this.colJobTypeJobSubType1});
            this.gridView9.GridControl = this.gridControl9;
            this.gridView9.GroupCount = 6;
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView9.OptionsFilter.AllowFilterEditor = false;
            this.gridView9.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView9.OptionsFilter.AllowMRUFilterList = false;
            this.gridView9.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView9.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView9.OptionsFind.FindDelay = 2000;
            this.gridView9.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView9.OptionsLayout.StoreAppearance = true;
            this.gridView9.OptionsLayout.StoreFormatRules = true;
            this.gridView9.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView9.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView9.OptionsNavigation.AutoFocusNewRow = true;
            this.gridView9.OptionsSelection.MultiSelect = true;
            this.gridView9.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView9.OptionsView.ColumnAutoWidth = false;
            this.gridView9.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView9.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView9.OptionsView.ShowFooter = true;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            this.gridView9.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractDescription3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFriendlyVisitNumber3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobTypeJobSubType1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTeamMemberName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView9.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView9.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView9.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView9.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView9_MouseUp);
            this.gridView9.GotFocus += new System.EventHandler(this.gridView9_GotFocus);
            this.gridView9.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView9_ValidatingEditor);
            // 
            // colstrMode
            // 
            this.colstrMode.Caption = "Mode";
            this.colstrMode.FieldName = "strMode";
            this.colstrMode.Name = "colstrMode";
            this.colstrMode.OptionsColumn.AllowEdit = false;
            this.colstrMode.OptionsColumn.AllowFocus = false;
            this.colstrMode.OptionsColumn.ReadOnly = true;
            this.colstrMode.Width = 50;
            // 
            // colstrRecordIDs
            // 
            this.colstrRecordIDs.Caption = "Record IDs";
            this.colstrRecordIDs.FieldName = "strRecordIDs";
            this.colstrRecordIDs.Name = "colstrRecordIDs";
            this.colstrRecordIDs.OptionsColumn.AllowEdit = false;
            this.colstrRecordIDs.OptionsColumn.AllowFocus = false;
            this.colstrRecordIDs.OptionsColumn.ReadOnly = true;
            // 
            // colLabourUsedTimingID
            // 
            this.colLabourUsedTimingID.Caption = "Labour Used Timing ID";
            this.colLabourUsedTimingID.FieldName = "LabourUsedTimingID";
            this.colLabourUsedTimingID.Name = "colLabourUsedTimingID";
            this.colLabourUsedTimingID.OptionsColumn.AllowEdit = false;
            this.colLabourUsedTimingID.OptionsColumn.AllowFocus = false;
            this.colLabourUsedTimingID.OptionsColumn.ReadOnly = true;
            this.colLabourUsedTimingID.Width = 126;
            // 
            // colLabourUsedID
            // 
            this.colLabourUsedID.Caption = "Labour Used ID";
            this.colLabourUsedID.FieldName = "LabourUsedID";
            this.colLabourUsedID.Name = "colLabourUsedID";
            this.colLabourUsedID.OptionsColumn.AllowEdit = false;
            this.colLabourUsedID.OptionsColumn.AllowFocus = false;
            this.colLabourUsedID.OptionsColumn.ReadOnly = true;
            this.colLabourUsedID.Width = 93;
            // 
            // colTeamMemberID
            // 
            this.colTeamMemberID.Caption = "Team Member ID";
            this.colTeamMemberID.FieldName = "TeamMemberID";
            this.colTeamMemberID.Name = "colTeamMemberID";
            this.colTeamMemberID.OptionsColumn.AllowEdit = false;
            this.colTeamMemberID.OptionsColumn.AllowFocus = false;
            this.colTeamMemberID.OptionsColumn.ReadOnly = true;
            this.colTeamMemberID.Width = 100;
            // 
            // colStartDateTime
            // 
            this.colStartDateTime.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colStartDateTime.AppearanceCell.Options.UseBackColor = true;
            this.colStartDateTime.Caption = "Start Time";
            this.colStartDateTime.ColumnEdit = this.repositoryItemDateEditStartTime;
            this.colStartDateTime.FieldName = "StartDateTime";
            this.colStartDateTime.Name = "colStartDateTime";
            this.colStartDateTime.Visible = true;
            this.colStartDateTime.VisibleIndex = 1;
            this.colStartDateTime.Width = 120;
            // 
            // repositoryItemDateEditStartTime
            // 
            this.repositoryItemDateEditStartTime.AutoHeight = false;
            this.repositoryItemDateEditStartTime.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditStartTime.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditStartTime.CalendarTimeProperties.LookAndFeel.SkinName = "Blue";
            this.repositoryItemDateEditStartTime.Mask.EditMask = "g";
            this.repositoryItemDateEditStartTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEditStartTime.Name = "repositoryItemDateEditStartTime";
            this.repositoryItemDateEditStartTime.EditValueChanged += new System.EventHandler(this.repositoryItemDateEditStartTime_EditValueChanged);
            // 
            // colEndDateTime
            // 
            this.colEndDateTime.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colEndDateTime.AppearanceCell.Options.UseBackColor = true;
            this.colEndDateTime.Caption = "End Time";
            this.colEndDateTime.ColumnEdit = this.repositoryItemDateEditEndTime;
            this.colEndDateTime.FieldName = "EndDateTime";
            this.colEndDateTime.Name = "colEndDateTime";
            this.colEndDateTime.Visible = true;
            this.colEndDateTime.VisibleIndex = 2;
            this.colEndDateTime.Width = 120;
            // 
            // repositoryItemDateEditEndTime
            // 
            this.repositoryItemDateEditEndTime.AutoHeight = false;
            this.repositoryItemDateEditEndTime.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditEndTime.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditEndTime.Mask.EditMask = "g";
            this.repositoryItemDateEditEndTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEditEndTime.Name = "repositoryItemDateEditEndTime";
            this.repositoryItemDateEditEndTime.EditValueChanged += new System.EventHandler(this.repositoryItemDateEditEndTime_EditValueChanged);
            // 
            // colDuration
            // 
            this.colDuration.Caption = "Duration";
            this.colDuration.ColumnEdit = this.repositoryItemTextEditHours;
            this.colDuration.FieldName = "Duration";
            this.colDuration.Name = "colDuration";
            this.colDuration.OptionsColumn.AllowEdit = false;
            this.colDuration.OptionsColumn.AllowFocus = false;
            this.colDuration.OptionsColumn.ReadOnly = true;
            this.colDuration.Visible = true;
            this.colDuration.VisibleIndex = 3;
            // 
            // repositoryItemTextEditHours
            // 
            this.repositoryItemTextEditHours.AutoHeight = false;
            this.repositoryItemTextEditHours.Mask.EditMask = "######0.00 Hours";
            this.repositoryItemTextEditHours.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditHours.Name = "repositoryItemTextEditHours";
            // 
            // colRemarks3
            // 
            this.colRemarks3.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colRemarks3.AppearanceCell.Options.UseBackColor = true;
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 4;
            this.colRemarks3.Width = 97;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colPdaCreatedID
            // 
            this.colPdaCreatedID.Caption = "Device Created ID";
            this.colPdaCreatedID.FieldName = "PdaCreatedID";
            this.colPdaCreatedID.Name = "colPdaCreatedID";
            this.colPdaCreatedID.OptionsColumn.AllowEdit = false;
            this.colPdaCreatedID.OptionsColumn.AllowFocus = false;
            this.colPdaCreatedID.OptionsColumn.ReadOnly = true;
            this.colPdaCreatedID.Width = 95;
            // 
            // colTeamMemberName
            // 
            this.colTeamMemberName.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colTeamMemberName.AppearanceCell.Options.UseBackColor = true;
            this.colTeamMemberName.Caption = "Team Member Name";
            this.colTeamMemberName.ColumnEdit = this.repositoryItemButtonEditTeamMemberName;
            this.colTeamMemberName.FieldName = "TeamMemberName";
            this.colTeamMemberName.Name = "colTeamMemberName";
            this.colTeamMemberName.Visible = true;
            this.colTeamMemberName.VisibleIndex = 0;
            this.colTeamMemberName.Width = 240;
            // 
            // repositoryItemButtonEditTeamMemberName
            // 
            this.repositoryItemButtonEditTeamMemberName.AutoHeight = false;
            this.repositoryItemButtonEditTeamMemberName.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click me to open the Select Team Member screen.", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditTeamMemberName.Name = "repositoryItemButtonEditTeamMemberName";
            this.repositoryItemButtonEditTeamMemberName.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditTeamMemberName.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditChooseTeamMember_ButtonClick);
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Team ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            // 
            // colJobID1
            // 
            this.colJobID1.Caption = "Job ID";
            this.colJobID1.FieldName = "JobID";
            this.colJobID1.Name = "colJobID1";
            this.colJobID1.OptionsColumn.AllowEdit = false;
            this.colJobID1.OptionsColumn.AllowFocus = false;
            this.colJobID1.OptionsColumn.ReadOnly = true;
            this.colJobID1.Width = 64;
            // 
            // colClientID3
            // 
            this.colClientID3.Caption = "Client ID";
            this.colClientID3.FieldName = "ClientID";
            this.colClientID3.Name = "colClientID3";
            this.colClientID3.OptionsColumn.AllowEdit = false;
            this.colClientID3.OptionsColumn.AllowFocus = false;
            this.colClientID3.OptionsColumn.ReadOnly = true;
            this.colClientID3.Width = 64;
            // 
            // colSiteID3
            // 
            this.colSiteID3.Caption = "Site ID";
            this.colSiteID3.FieldName = "SiteID";
            this.colSiteID3.Name = "colSiteID3";
            this.colSiteID3.OptionsColumn.AllowEdit = false;
            this.colSiteID3.OptionsColumn.AllowFocus = false;
            this.colSiteID3.OptionsColumn.ReadOnly = true;
            this.colSiteID3.Width = 59;
            // 
            // colSiteName3
            // 
            this.colSiteName3.Caption = "Site Name";
            this.colSiteName3.FieldName = "SiteName";
            this.colSiteName3.Name = "colSiteName3";
            this.colSiteName3.OptionsColumn.AllowEdit = false;
            this.colSiteName3.OptionsColumn.AllowFocus = false;
            this.colSiteName3.OptionsColumn.ReadOnly = true;
            this.colSiteName3.Visible = true;
            this.colSiteName3.VisibleIndex = 6;
            this.colSiteName3.Width = 169;
            // 
            // colClientName3
            // 
            this.colClientName3.Caption = "Client Name";
            this.colClientName3.FieldName = "ClientName";
            this.colClientName3.Name = "colClientName3";
            this.colClientName3.OptionsColumn.AllowEdit = false;
            this.colClientName3.OptionsColumn.AllowFocus = false;
            this.colClientName3.OptionsColumn.ReadOnly = true;
            this.colClientName3.Visible = true;
            this.colClientName3.VisibleIndex = 8;
            this.colClientName3.Width = 169;
            // 
            // colJobSubTypeDescription1
            // 
            this.colJobSubTypeDescription1.Caption = "Job Sub-Type";
            this.colJobSubTypeDescription1.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription1.Name = "colJobSubTypeDescription1";
            this.colJobSubTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription1.Width = 193;
            // 
            // colJobTypeDescription1
            // 
            this.colJobTypeDescription1.Caption = "Job Type";
            this.colJobTypeDescription1.FieldName = "JobTypeDescription";
            this.colJobTypeDescription1.Name = "colJobTypeDescription1";
            this.colJobTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription1.Width = 197;
            // 
            // colExpectedStartDate2
            // 
            this.colExpectedStartDate2.Caption = "Expected Start";
            this.colExpectedStartDate2.ColumnEdit = this.repositoryItemTextEditDatetime3;
            this.colExpectedStartDate2.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate2.Name = "colExpectedStartDate2";
            this.colExpectedStartDate2.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate2.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate2.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate2.Width = 100;
            // 
            // repositoryItemTextEditDatetime3
            // 
            this.repositoryItemTextEditDatetime3.AutoHeight = false;
            this.repositoryItemTextEditDatetime3.Mask.EditMask = "g";
            this.repositoryItemTextEditDatetime3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDatetime3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDatetime3.Name = "repositoryItemTextEditDatetime3";
            // 
            // colVisitNumber2
            // 
            this.colVisitNumber2.Caption = "Visit #";
            this.colVisitNumber2.FieldName = "VisitNumber";
            this.colVisitNumber2.Name = "colVisitNumber2";
            this.colVisitNumber2.OptionsColumn.AllowEdit = false;
            this.colVisitNumber2.OptionsColumn.AllowFocus = false;
            this.colVisitNumber2.OptionsColumn.ReadOnly = true;
            this.colVisitNumber2.Width = 49;
            // 
            // colFriendlyVisitNumber3
            // 
            this.colFriendlyVisitNumber3.Caption = "Friendly Visit #";
            this.colFriendlyVisitNumber3.FieldName = "FriendlyVisitNumber";
            this.colFriendlyVisitNumber3.Name = "colFriendlyVisitNumber3";
            this.colFriendlyVisitNumber3.OptionsColumn.AllowEdit = false;
            this.colFriendlyVisitNumber3.OptionsColumn.AllowFocus = false;
            this.colFriendlyVisitNumber3.OptionsColumn.ReadOnly = true;
            this.colFriendlyVisitNumber3.Visible = true;
            this.colFriendlyVisitNumber3.VisibleIndex = 9;
            this.colFriendlyVisitNumber3.Width = 119;
            // 
            // colFullDescription1
            // 
            this.colFullDescription1.Caption = "Linked To";
            this.colFullDescription1.FieldName = "FullDescription";
            this.colFullDescription1.Name = "colFullDescription1";
            this.colFullDescription1.OptionsColumn.AllowEdit = false;
            this.colFullDescription1.OptionsColumn.AllowFocus = false;
            this.colFullDescription1.OptionsColumn.ReadOnly = true;
            this.colFullDescription1.Width = 474;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Team Name";
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.OptionsColumn.AllowEdit = false;
            this.colContractorName.OptionsColumn.AllowFocus = false;
            this.colContractorName.OptionsColumn.ReadOnly = true;
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 5;
            this.colContractorName.Width = 186;
            // 
            // colVisitID2
            // 
            this.colVisitID2.Caption = "Visit ID";
            this.colVisitID2.FieldName = "VisitID";
            this.colVisitID2.Name = "colVisitID2";
            this.colVisitID2.OptionsColumn.AllowEdit = false;
            this.colVisitID2.OptionsColumn.AllowFocus = false;
            this.colVisitID2.OptionsColumn.ReadOnly = true;
            // 
            // colClientContractID2
            // 
            this.colClientContractID2.Caption = "Client Contract ID";
            this.colClientContractID2.FieldName = "ClientContractID";
            this.colClientContractID2.Name = "colClientContractID2";
            this.colClientContractID2.OptionsColumn.AllowEdit = false;
            this.colClientContractID2.OptionsColumn.AllowFocus = false;
            this.colClientContractID2.OptionsColumn.ReadOnly = true;
            this.colClientContractID2.Width = 105;
            // 
            // colSiteContractID2
            // 
            this.colSiteContractID2.Caption = "Site Contract ID";
            this.colSiteContractID2.FieldName = "SiteContractID";
            this.colSiteContractID2.Name = "colSiteContractID2";
            this.colSiteContractID2.OptionsColumn.AllowEdit = false;
            this.colSiteContractID2.OptionsColumn.AllowFocus = false;
            this.colSiteContractID2.OptionsColumn.ReadOnly = true;
            this.colSiteContractID2.Width = 96;
            // 
            // colJobTypeID1
            // 
            this.colJobTypeID1.Caption = "Job Type ID";
            this.colJobTypeID1.FieldName = "JobTypeID";
            this.colJobTypeID1.Name = "colJobTypeID1";
            this.colJobTypeID1.OptionsColumn.AllowEdit = false;
            this.colJobTypeID1.OptionsColumn.AllowFocus = false;
            this.colJobTypeID1.OptionsColumn.ReadOnly = true;
            this.colJobTypeID1.Width = 77;
            // 
            // colJobSubTypeID1
            // 
            this.colJobSubTypeID1.Caption = "Job Sub-Type ID";
            this.colJobSubTypeID1.FieldName = "JobSubTypeID";
            this.colJobSubTypeID1.Name = "colJobSubTypeID1";
            this.colJobSubTypeID1.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeID1.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeID1.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeID1.Width = 99;
            // 
            // colContractDescription3
            // 
            this.colContractDescription3.Caption = "Contract";
            this.colContractDescription3.FieldName = "ContractDescription";
            this.colContractDescription3.Name = "colContractDescription3";
            this.colContractDescription3.OptionsColumn.AllowEdit = false;
            this.colContractDescription3.OptionsColumn.AllowFocus = false;
            this.colContractDescription3.OptionsColumn.ReadOnly = true;
            this.colContractDescription3.Visible = true;
            this.colContractDescription3.VisibleIndex = 10;
            this.colContractDescription3.Width = 236;
            // 
            // colLinkedToPersonTypeID
            // 
            this.colLinkedToPersonTypeID.Caption = "Linked To Type ID";
            this.colLinkedToPersonTypeID.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.Name = "colLinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID.Width = 105;
            // 
            // colLinkedToPersonType
            // 
            this.colLinkedToPersonType.Caption = "Linked To Type";
            this.colLinkedToPersonType.FieldName = "LinkedToPersonType";
            this.colLinkedToPersonType.Name = "colLinkedToPersonType";
            this.colLinkedToPersonType.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonType.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonType.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonType.Width = 119;
            // 
            // colJobTypeJobSubType1
            // 
            this.colJobTypeJobSubType1.Caption = "Job Type \\ Sub-Type";
            this.colJobTypeJobSubType1.FieldName = "JobTypeJobSubType";
            this.colJobTypeJobSubType1.Name = "colJobTypeJobSubType1";
            this.colJobTypeJobSubType1.OptionsColumn.AllowEdit = false;
            this.colJobTypeJobSubType1.OptionsColumn.AllowFocus = false;
            this.colJobTypeJobSubType1.OptionsColumn.ReadOnly = true;
            this.colJobTypeJobSubType1.Visible = true;
            this.colJobTypeJobSubType1.VisibleIndex = 7;
            this.colJobTypeJobSubType1.Width = 306;
            // 
            // labelControlSelectedLabourCount
            // 
            this.labelControlSelectedLabourCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControlSelectedLabourCount.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlSelectedLabourCount.Appearance.ImageIndex = 10;
            this.labelControlSelectedLabourCount.Appearance.ImageList = this.imageCollection1;
            this.labelControlSelectedLabourCount.Appearance.Options.UseImageAlign = true;
            this.labelControlSelectedLabourCount.Appearance.Options.UseImageIndex = true;
            this.labelControlSelectedLabourCount.Appearance.Options.UseImageList = true;
            this.labelControlSelectedLabourCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlSelectedLabourCount.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControlSelectedLabourCount.Location = new System.Drawing.Point(6, 497);
            this.labelControlSelectedLabourCount.Name = "labelControlSelectedLabourCount";
            this.labelControlSelectedLabourCount.Size = new System.Drawing.Size(159, 17);
            this.labelControlSelectedLabourCount.TabIndex = 27;
            this.labelControlSelectedLabourCount.Text = "0 Selected Labour";
            // 
            // btnStep3Next
            // 
            this.btnStep3Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep3Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep3Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep3Next.Location = new System.Drawing.Point(1061, 501);
            this.btnStep3Next.Name = "btnStep3Next";
            this.btnStep3Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep3Next.TabIndex = 19;
            this.btnStep3Next.Text = "Next";
            this.btnStep3Next.Click += new System.EventHandler(this.btnStep3Next_Click);
            // 
            // btnStep3Previous
            // 
            this.btnStep3Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep3Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep3Previous.Location = new System.Drawing.Point(967, 501);
            this.btnStep3Previous.Name = "btnStep3Previous";
            this.btnStep3Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep3Previous.TabIndex = 20;
            this.btnStep3Previous.Text = "Previous";
            this.btnStep3Previous.Click += new System.EventHandler(this.btnStep3Previous_Click);
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.Controls.Add(this.pictureEdit3);
            this.panelControl3.Controls.Add(this.labelControl13);
            this.panelControl3.Controls.Add(this.labelControl14);
            this.panelControl3.Location = new System.Drawing.Point(7, 6);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1142, 48);
            this.panelControl3.TabIndex = 17;
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit3.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit3.Location = new System.Drawing.Point(1098, 4);
            this.pictureEdit3.MenuManager = this.barManager1;
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.ReadOnly = true;
            this.pictureEdit3.Properties.ShowMenu = false;
            this.pictureEdit3.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit3.TabIndex = 9;
            // 
            // labelControl13
            // 
            this.labelControl13.AllowHtmlString = true;
            this.labelControl13.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.Options.UseBackColor = true;
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Location = new System.Drawing.Point(5, 5);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(291, 16);
            this.labelControl13.TabIndex = 6;
            this.labelControl13.Text = "<b>Step 3:</b> Set Labour Time On \\ Off Site <b>[Optional]</b>";
            // 
            // labelControl14
            // 
            this.labelControl14.AllowHtmlString = true;
            this.labelControl14.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl14.Appearance.Options.UseBackColor = true;
            this.labelControl14.Location = new System.Drawing.Point(57, 29);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(709, 13);
            this.labelControl14.TabIndex = 7;
            this.labelControl14.Text = "Select one or more Job Labour records from the left list then click add to add ti" +
    "me on \\ off site. Repeat until all job labour records have time records.";
            // 
            // xtraTabPageFinish
            // 
            this.xtraTabPageFinish.Controls.Add(this.panelControl2);
            this.xtraTabPageFinish.Controls.Add(this.groupControl1);
            this.xtraTabPageFinish.Controls.Add(this.labelControl10);
            this.xtraTabPageFinish.Controls.Add(this.btnFinish);
            this.xtraTabPageFinish.Controls.Add(this.btnFinishPrevious);
            this.xtraTabPageFinish.Controls.Add(this.pictureEdit2);
            this.xtraTabPageFinish.Name = "xtraTabPageFinish";
            this.xtraTabPageFinish.Size = new System.Drawing.Size(1155, 537);
            this.xtraTabPageFinish.Tag = "99";
            this.xtraTabPageFinish.Text = "Finished";
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.pictureEdit6);
            this.panelControl2.Controls.Add(this.labelControl11);
            this.panelControl2.Controls.Add(this.labelControl12);
            this.panelControl2.Location = new System.Drawing.Point(211, 6);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(938, 48);
            this.panelControl2.TabIndex = 18;
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit6.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit6.Location = new System.Drawing.Point(894, 4);
            this.pictureEdit6.MenuManager = this.barManager1;
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit6.Properties.ReadOnly = true;
            this.pictureEdit6.Properties.ShowMenu = false;
            this.pictureEdit6.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit6.TabIndex = 19;
            // 
            // labelControl11
            // 
            this.labelControl11.AllowHtmlString = true;
            this.labelControl11.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl11.Appearance.Options.UseBackColor = true;
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(5, 1);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(240, 29);
            this.labelControl11.TabIndex = 5;
            this.labelControl11.Text = "Completing the Wizard";
            // 
            // labelControl12
            // 
            this.labelControl12.AllowHtmlString = true;
            this.labelControl12.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl12.Appearance.Options.UseBackColor = true;
            this.labelControl12.Location = new System.Drawing.Point(9, 31);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(533, 13);
            this.labelControl12.TabIndex = 15;
            this.labelControl12.Text = "You have successfully completed the Wizard. On clicking <b>Finish</b>, your selec" +
    "ted Visit(s) will be set as Completed.";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.checkEdit1);
            this.groupControl1.Location = new System.Drawing.Point(211, 155);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(938, 55);
            this.groupControl1.TabIndex = 16;
            this.groupControl1.Text = "Available Choices:";
            // 
            // checkEdit1
            // 
            this.checkEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit1.EditValue = true;
            this.checkEdit1.Location = new System.Drawing.Point(6, 26);
            this.checkEdit1.MenuManager = this.barManager1;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit1.Properties.Caption = "Set the Visit(s) as Completed then <b>return</b> to the parent<b> Manager</b>.";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit1.Properties.RadioGroupIndex = 1;
            this.checkEdit1.Size = new System.Drawing.Size(926, 19);
            this.checkEdit1.TabIndex = 0;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(213, 132);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(154, 13);
            this.labelControl10.TabIndex = 15;
            this.labelControl10.Text = "What would you like to do next?";
            // 
            // btnFinish
            // 
            this.btnFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinish.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnFinish.Appearance.Options.UseFont = true;
            this.btnFinish.ImageOptions.Image = global::WoodPlan5.Properties.Resources.apply_32x32;
            this.btnFinish.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnFinish.Location = new System.Drawing.Point(1061, 501);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(88, 30);
            this.btnFinish.TabIndex = 12;
            this.btnFinish.Text = "Finish";
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnFinishPrevious
            // 
            this.btnFinishPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinishPrevious.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnFinishPrevious.Location = new System.Drawing.Point(967, 501);
            this.btnFinishPrevious.Name = "btnFinishPrevious";
            this.btnFinishPrevious.Size = new System.Drawing.Size(88, 30);
            this.btnFinishPrevious.TabIndex = 11;
            this.btnFinishPrevious.Text = "Previous";
            this.btnFinishPrevious.Click += new System.EventHandler(this.btnFinishPrevious_Click);
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureEdit2.EditValue = global::WoodPlan5.Properties.Resources.wizard_finish1;
            this.pictureEdit2.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit2.MenuManager = this.barManager1;
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.ShowMenu = false;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit2.Size = new System.Drawing.Size(205, 537);
            this.pictureEdit2.TabIndex = 5;
            // 
            // repositoryItemSpinEdit2DP8
            // 
            this.repositoryItemSpinEdit2DP8.AutoHeight = false;
            this.repositoryItemSpinEdit2DP8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit2DP8.LookAndFeel.SkinName = "Blue";
            this.repositoryItemSpinEdit2DP8.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit2DP8.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit2DP8.Name = "repositoryItemSpinEdit2DP8";
            // 
            // repositoryItemGridLookUpEdit2
            // 
            this.repositoryItemGridLookUpEdit2.AutoHeight = false;
            this.repositoryItemGridLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit2.DisplayMember = "Description";
            this.repositoryItemGridLookUpEdit2.Name = "repositoryItemGridLookUpEdit2";
            this.repositoryItemGridLookUpEdit2.NullText = "";
            this.repositoryItemGridLookUpEdit2.ValueMember = "ID";
            // 
            // repositoryItemSpinEditCurrency8
            // 
            this.repositoryItemSpinEditCurrency8.AutoHeight = false;
            this.repositoryItemSpinEditCurrency8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditCurrency8.Mask.EditMask = "c";
            this.repositoryItemSpinEditCurrency8.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditCurrency8.Name = "repositoryItemSpinEditCurrency8";
            // 
            // repositoryItemSpinEditPercentage8
            // 
            this.repositoryItemSpinEditPercentage8.AutoHeight = false;
            this.repositoryItemSpinEditPercentage8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditPercentage8.Mask.EditMask = "P";
            this.repositoryItemSpinEditPercentage8.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditPercentage8.Name = "repositoryItemSpinEditPercentage8";
            // 
            // repositoryItemMemoExEdit9
            // 
            this.repositoryItemMemoExEdit9.AutoHeight = false;
            this.repositoryItemMemoExEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit9.Name = "repositoryItemMemoExEdit9";
            this.repositoryItemMemoExEdit9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit9.ShowIcon = false;
            // 
            // repositoryItemButtonEditContractorName
            // 
            this.repositoryItemButtonEditContractorName.AutoHeight = false;
            this.repositoryItemButtonEditContractorName.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click me to open the Select Contractor screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditContractorName.Name = "repositoryItemButtonEditContractorName";
            this.repositoryItemButtonEditContractorName.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // repositoryItemTextEditDateTime8
            // 
            this.repositoryItemTextEditDateTime8.AutoHeight = false;
            this.repositoryItemTextEditDateTime8.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime8.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime8.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime8.Name = "repositoryItemTextEditDateTime8";
            // 
            // repositoryItemTextEdit6
            // 
            this.repositoryItemTextEdit6.AutoHeight = false;
            this.repositoryItemTextEdit6.Mask.EditMask = "########0.00 Miles";
            this.repositoryItemTextEdit6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit6.Name = "repositoryItemTextEdit6";
            // 
            // repositoryItemButtonEditLinkedToPersonType
            // 
            this.repositoryItemButtonEditLinkedToPersonType.AutoHeight = false;
            this.repositoryItemButtonEditLinkedToPersonType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Click me to Open the Select Labour Type screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditLinkedToPersonType.Name = "repositoryItemButtonEditLinkedToPersonType";
            this.repositoryItemButtonEditLinkedToPersonType.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource
            // 
            this.sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource.DataMember = "sp06406_OM_Site_Contract_On_Hold_Reasons_With_Blank";
            this.sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.LookAndFeel.SkinName = "Blue";
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.DisplayMember = "Description";
            this.repositoryItemGridLookUpEdit1.LookAndFeel.SkinName = "Blue";
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.NullText = "";
            this.repositoryItemGridLookUpEdit1.ValueMember = "ID";
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // repositoryItemButtonEditChooseStaff
            // 
            this.repositoryItemButtonEditChooseStaff.AutoHeight = false;
            this.repositoryItemButtonEditChooseStaff.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Click me to open the Select Staff screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditChooseStaff.Name = "repositoryItemButtonEditChooseStaff";
            this.repositoryItemButtonEditChooseStaff.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "";
            this.repositoryItemCheckEdit4.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemDuration1
            // 
            this.repositoryItemDuration1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDuration1.AutoHeight = false;
            this.repositoryItemDuration1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDuration1.DisabledStateText = null;
            this.repositoryItemDuration1.Name = "repositoryItemDuration1";
            this.repositoryItemDuration1.NullValuePromptShowForEmptyValue = true;
            this.repositoryItemDuration1.ShowEmptyItem = true;
            this.repositoryItemDuration1.ValidateOnEnterKey = true;
            // 
            // bsiRecordTicking
            // 
            this.bsiRecordTicking.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiRecordTicking.Caption = "Record <b>Ticking</b>";
            this.bsiRecordTicking.Id = 91;
            this.bsiRecordTicking.ImageOptions.ImageIndex = 0;
            this.bsiRecordTicking.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTick),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUntick)});
            this.bsiRecordTicking.Name = "bsiRecordTicking";
            // 
            // bbiTick
            // 
            this.bbiTick.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiTick.Caption = "<b>Tick</b> Selected Records";
            this.bbiTick.Id = 94;
            this.bbiTick.ImageOptions.ImageIndex = 0;
            this.bbiTick.Name = "bbiTick";
            this.bbiTick.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTick_ItemClick);
            // 
            // bbiUntick
            // 
            this.bbiUntick.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiUntick.Caption = "<b>Untick</b> Selected Records";
            this.bbiUntick.Id = 95;
            this.bbiUntick.ImageOptions.ImageIndex = 1;
            this.bbiUntick.Name = "bbiUntick";
            this.bbiUntick.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUntick_ItemClick);
            // 
            // transitionManager1
            // 
            this.transitionManager1.FrameInterval = 5000;
            transition1.Control = this.xtraTabControl1;
            transition1.EasingMode = DevExpress.Data.Utils.EasingMode.EaseInOut;
            transition1.WaitingIndicatorProperties.Caption = "Loading...";
            this.transitionManager1.Transitions.Add(transition1);
            // 
            // sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter
            // 
            this.sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.checked_32, "checked_32", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection2.Images.SetKeyName(0, "checked_32");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.unchecked_32, "unchecked_32", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection2.Images.SetKeyName(1, "unchecked_32");
            // 
            // sp06413_OM_Visit_Completion_Wizard_Get_VisitsTableAdapter
            // 
            this.sp06413_OM_Visit_Completion_Wizard_Get_VisitsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_BlankTableAdapter
            // 
            this.sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06251_OM_Issue_Types_With_BlankTableAdapter
            // 
            this.sp06251_OM_Issue_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06267_OM_Extra_Work_Types_With_BlankTableAdapter
            // 
            this.sp06267_OM_Extra_Work_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06416_OM_Visit_Completion_Wizard_Get_JobsTableAdapter
            // 
            this.sp06416_OM_Visit_Completion_Wizard_Get_JobsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter
            // 
            this.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter
            // 
            this.sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06418_OM_Visit_Completion_Wizard_Job_StatusesTableAdapter
            // 
            this.sp06418_OM_Visit_Completion_Wizard_Job_StatusesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06419_OM_Visit_Completion_Wizard_LabourTableAdapter
            // 
            this.sp06419_OM_Visit_Completion_Wizard_LabourTableAdapter.ClearBeforeFill = true;
            // 
            // sp06420_OM_Visit_Completion_Wizard_Labour_TimeTableAdapter
            // 
            this.sp06420_OM_Visit_Completion_Wizard_Labour_TimeTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Visit_Completion_Wizard
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1178, 537);
            this.Controls.Add(this.xtraTabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Visit_Completion_Wizard";
            this.Text = "Visit Completion Wizard";
            this.Activated += new System.EventHandler(this.frm_OM_Visit_Completion_Wizard_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Visit_Completion_Wizard_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Visit_Completion_Wizard_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageWelcome.ResumeLayout(false);
            this.xtraTabPageWelcome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.xtraTabPageStep1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06413OMVisitCompletionWizardGetVisitsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericLatLong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEditInteger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditVisitStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06415OMVisitCompletionWizardVisitStatusesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChoosePath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditIssueTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06251OMIssueTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditExtraWorkTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06267OMExtraWorkTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCharacter50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            this.xtraTabPageStep2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06416OMVisitCompletionWizardGetJobsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditStartDate.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditUnitDescriptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06177OMJobDurationDescriptorsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditEndDate.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditJobStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06418OMVisitCompletionWizardJobStatusesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditCancelledReasonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06268OMCancelledReasonsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericLatLong2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).EndInit();
            this.xtraTabPageStep3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06419OMVisitCompletionWizardLabourBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06420OMVisitCompletionWizardLabourTimeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditStartTime.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditStartTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditEndTime.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditEndTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditTeamMemberName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDatetime3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            this.xtraTabPageFinish.ResumeLayout(false);
            this.xtraTabPageFinish.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2DP8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCurrency8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercentage8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditContractorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditLinkedToPersonType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseStaff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageWelcome;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btnWelcomeNext;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageFinish;
        private DevExpress.XtraEditors.SimpleButton btnStep1Next;
        private DevExpress.XtraEditors.SimpleButton btnStep1Previous;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.SimpleButton btnFinish;
        private DevExpress.XtraEditors.SimpleButton btnFinishPrevious;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep2;
        private DevExpress.XtraEditors.SimpleButton btnStep2Next;
        private DevExpress.XtraEditors.SimpleButton btnStep2Previous;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit7;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditChooseStaff;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraScheduler.UI.RepositoryItemDuration repositoryItemDuration1;
        private DevExpress.XtraBars.BarSubItem bsiRecordTicking;
        private DevExpress.Utils.Animation.TransitionManager transitionManager1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private System.Windows.Forms.BindingSource sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditVisitStatusID;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit2View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger4;
        private DevExpress.XtraEditors.LabelControl labelControlSelectedJobCount;
        private DevExpress.XtraBars.BarButtonItem bbiTick;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.XtraBars.BarButtonItem bbiUntick;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep3;
        private DevExpress.XtraEditors.SimpleButton btnStep3Next;
        private DevExpress.XtraEditors.SimpleButton btnStep3Previous;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControlSelectedLabourCount;
        private System.Windows.Forms.BindingSource sp06413OMVisitCompletionWizardGetVisitsBindingSource;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colImagesFolderOM;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractActive;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colFriendlyVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedOutstandingJobCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEditInteger;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysAllowed;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colClientSignaturePath;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRework;
        private DevExpress.XtraGrid.Columns.GridColumn colExtraWorkRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colExtraWorkTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colExtraWorkComments;
        private DevExpress.XtraGrid.Columns.GridColumn colManagerName;
        private DevExpress.XtraGrid.Columns.GridColumn colManagerNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colNoOnetoSign;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletionSheetNumber;
        private DataSet_OM_VisitTableAdapters.sp06413_OM_Visit_Completion_Wizard_Get_VisitsTableAdapter sp06413_OM_Visit_Completion_Wizard_Get_VisitsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCharacter50;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumericLatLong;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditChoosePath;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription;
        private System.Windows.Forms.BindingSource sp06415OMVisitCompletionWizardVisitStatusesWithBlankBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_BlankTableAdapter sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditIssueTypeID;
        private System.Windows.Forms.BindingSource sp06251OMIssueTypesWithBlankBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DataSet_OM_VisitTableAdapters.sp06251_OM_Issue_Types_With_BlankTableAdapter sp06251_OM_Issue_Types_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditExtraWorkTypeID;
        private System.Windows.Forms.BindingSource sp06267OMExtraWorkTypesWithBlankBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DataSet_OM_VisitTableAdapters.sp06267_OM_Extra_Work_Types_With_BlankTableAdapter sp06267_OM_Extra_Work_Types_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueFound;
        private DevExpress.XtraEditors.LabelControl labelControlSelectedVisitCount;
        private System.Windows.Forms.BindingSource sp06416OMVisitCompletionWizardGetJobsBindingSource;
        private DataSet_OM_Job dataSet_OM_Job;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colFriendlyVisitNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubType;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobType;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysLeeway;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourCount;
        private DevExpress.XtraGrid.Columns.GridColumn colActualStartDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEditStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnits;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnitsDescriptionID;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditUnitDescriptor;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusID;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditJobStatusID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn colCancelledReasonID;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditCancelledReasonID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLatitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLongitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLatitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLongitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobNoLongerRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colNoWorkRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colManuallyCompleted;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPayContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClient;
        private DevExpress.XtraGrid.Columns.GridColumn colRework1;
        private DataSet_OM_JobTableAdapters.sp06416_OM_Visit_Completion_Wizard_Get_JobsTableAdapter sp06416_OM_Visit_Completion_Wizard_Get_JobsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumericLatLong2;
        private System.Windows.Forms.BindingSource sp06177OMJobDurationDescriptorsWithBlankBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DataSet_OM_JobTableAdapters.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter;
        private System.Windows.Forms.BindingSource sp06268OMCancelledReasonsWithBlankBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DataSet_OM_JobTableAdapters.sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter;
        private System.Windows.Forms.BindingSource sp06418OMVisitCompletionWizardJobStatusesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DataSet_OM_JobTableAdapters.sp06418_OM_Visit_Completion_Wizard_Job_StatusesTableAdapter sp06418_OM_Visit_Completion_Wizard_Job_StatusesTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEditEndDate;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime5;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger6;
        private DevExpress.XtraGrid.GridControl gridControl9;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2DP8;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditCurrency8;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditPercentage8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditContractorName;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime8;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditLinkedToPersonType;
        private System.Windows.Forms.BindingSource sp06419OMVisitCompletionWizardLabourBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourUsedID1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID2;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber3;
        private DevExpress.XtraGrid.Columns.GridColumn colFriendlyVisitNumber2;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobExpectedStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colFullDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName1;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalExternal;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDateTime1;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDateTime1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonType1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DataSet_OM_JobTableAdapters.sp06419_OM_Visit_Completion_Wizard_LabourTableAdapter sp06419_OM_Visit_Completion_Wizard_LabourTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeJobSubType;
        private System.Windows.Forms.BindingSource sp06420OMVisitCompletionWizardLabourTimeBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colstrMode;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRecordIDs;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourUsedTimingID;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourUsedID;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamMemberID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colDuration;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaCreatedID;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamMemberName;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName3;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber2;
        private DevExpress.XtraGrid.Columns.GridColumn colFriendlyVisitNumber3;
        private DevExpress.XtraGrid.Columns.GridColumn colFullDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID2;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription3;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonType;
        private DataSet_OM_JobTableAdapters.sp06420_OM_Visit_Completion_Wizard_Labour_TimeTableAdapter sp06420_OM_Visit_Completion_Wizard_Labour_TimeTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeJobSubType1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedTimingCount;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID3;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEditStartTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEditEndTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHours;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditTeamMemberName;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDatetime3;
    }
}

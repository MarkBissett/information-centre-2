namespace WoodPlan5
{
    partial class frm_OM_Tender_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Tender_Manager));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions customHeaderButtonImageOptions1 = new DevExpress.XtraBars.Docking.CustomHeaderButtonImageOptions();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling22 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling23 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling24 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling25 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling26 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling27 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling28 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling29 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling30 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling31 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem16 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            this.colDisabled2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit16 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit17 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDaysQuoteOverdue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumericDays = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colStatusIssueID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusIsssue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemTextEditLatLong = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemTextEditHTML0 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemHyperLinkEditLinkedDocumentsVisit = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemHyperLinkEditGrid1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemHyperLinkEditLinkedVisits = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemHyperLinkEditLabour = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemHyperLinkEditClientSignature = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemPictureEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemHyperLinkEditSelfBillingInvoice = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemHyperLinkEditClientInvoiceID = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemHyperLinkEditPictureLink = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemTextEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemMemoExEdit20 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemTextEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVisitStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPopupContainerEditDateRange = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridControlVisit = new DevExpress.XtraGrid.GridControl();
            this.sp06495OMVisitsLinkedtoTenderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Tender = new WoodPlan5.DataSet_OM_Tender();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridViewVisit = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVisitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCalculationLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCalculationLevelDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDirectorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDirector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedDocumentCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedOutstandingJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedCompletedJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeliness = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedVisitCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueFound = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientSignaturePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colImagesFolderOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRework = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExtraWorkComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExtraWorkRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExtraWorkType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentOnSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorNames = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Alert2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSuspendedJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedPictureCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFriendlyVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDAVersion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellCalculationLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellCalculationLevelDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCMName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysAllowed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManagerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManagerNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoOnetoSign = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedFromVisitTemplate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedFromVisitTemplateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExternalCustomerStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletionSheetNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusIssueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusIssue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysSeparation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractDaysSeparationPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPayContractor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClient1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKAMName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteInstructions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExtraCostsFinalised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlJobSubTypeFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnJobSubTypeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl11 = new DevExpress.XtraGrid.GridControl();
            this.sp06494OMJobSubTypeFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlJobTypeFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnJobTypeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl10 = new DevExpress.XtraGrid.GridControl();
            this.sp06493OMJobTypeFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlCMFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnCMFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl9 = new DevExpress.XtraGrid.GridControl();
            this.sp06020OMJobManagerCMsFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Summer_Core = new WoodPlan5.DataSet_GC_Summer_Core();
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn218 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn219 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn220 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlDateRangeFromToday = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.spinEditDaysAfter = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditDaysPrior = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.btnDateRangeFromTodayOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlKAMFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnKAMFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp06019OMJobManagerKAMsFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlLabourFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnLabourFilter = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl12 = new DevExpress.XtraGrid.GridControl();
            this.sp06490OMTenderStaffFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView12 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlShowTabPages = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp04022CoreDummyTabPageListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTabPageName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colParentLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit18 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.btnShowTabPagesOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlVisitSystemStatusFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnVisitSystemStatusFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp06489OMTenderStatusFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn244 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn245 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn246 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.btnDateRangeOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlTender = new DevExpress.XtraGrid.GridControl();
            this.sp06491OMTenderManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewTender = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCreatedByStaffID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContactID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKAM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderGroupID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colQuoteNotRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequestReceivedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colReturnToClientByDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCMInitialAttendanceDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuoteSubmittedToClientDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuoteAcceptedByClientDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequiredWorkCompletedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKAMQuoteRejectionReasonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOurInternalComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCMComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTPORequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthorityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTPOCheckedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeProtected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthorityNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthoritySubmittedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthorityOutcomeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthorityOkToProceed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCCompanyName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddress1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContractDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationX1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditLatLong2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSiteLocationY1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedVisitCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedVisits2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colLinkedDocumentCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocumentsJobs = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colTenderGroupDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditTenderGroupDescription = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colCreatedByStaffName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKAMName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCMName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPersonPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPersonTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthorityOutcome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningAuthority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKAMQuoteRejectionReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRevisionNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoClientPO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoClientPOAuthorisedByDirector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoClientPOAuthorisedByDirectorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoClientPOEmailedToDirector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubmittedToCMDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProposedLabour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProposedLabourID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProposedLabourTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProposedLabourType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuotedLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colQuotedMaterialSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuotedEquipmentSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuotedTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAcceptedLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAcceptedMaterialSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAcceptedEquipmentSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAcceptedTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualMaterialCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderProactiveDaysToReturnQuote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderReactiveDaysToReturnQuote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProgressBarValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemProgressBarGreen = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.colClientQuoteSpreadsheetExtractFile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditClientQuoteExtractFilt = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colLinkedJobCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningProceedDateSet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningProceedStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConservationStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colListedBuildingStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRAMS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProposedLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProposedMaterialCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProposedEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProposedTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemProgressBarRed = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageVisits = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPageHistory = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlHistory = new DevExpress.XtraGrid.GridControl();
            this.sp06515OMTenderHistoryForTenderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewHistory = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTenderHistoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOldStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNewStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOldStatusIssueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNewStatusIssueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateChanged = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedToRecord = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOldStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNewStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOldStatusIssue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNewStatusIssue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageCRM = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlCRM = new DevExpress.XtraGrid.GridControl();
            this.sp05089CRMContactsLinkedToRecordBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.gridViewCRM = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCRMID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn208 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContactID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDueDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContactMadeDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactMethodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn209 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn210 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn211 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDirectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML12 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn212 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDirectionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn213 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreated1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.xtraTabPageLinkedDocuments = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp06514OMLinkedDocumentsLinkedToRecordBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Core = new WoodPlan5.DataSet_OM_Core();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedRecord = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedToRecordParent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.dataSet_OM_Client_PO = new WoodPlan5.DataSet_OM_Client_PO();
            this.dataSet_Common_Functionality = new WoodPlan5.DataSet_Common_Functionality();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bsiToolbarAdd = new DevExpress.XtraBars.BarSubItem();
            this.bbiToolbarAdd = new DevExpress.XtraBars.BarButtonItem();
            this.bbiToolbarAddFromTemplate = new DevExpress.XtraBars.BarButtonItem();
            this.bsiStatus = new DevExpress.XtraBars.BarSubItem();
            this.bsiSendQuoteToCM = new DevExpress.XtraBars.BarSubItem();
            this.bbiSendQuoteToCM = new DevExpress.XtraBars.BarButtonItem();
            this.bbiQuoteNotRequired = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCMReturnedQuote = new DevExpress.XtraBars.BarButtonItem();
            this.bsiKAMAuthorised = new DevExpress.XtraBars.BarSubItem();
            this.bbiKAMAccepted = new DevExpress.XtraBars.BarButtonItem();
            this.bbiKAMRejected = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSentToClient = new DevExpress.XtraBars.BarButtonItem();
            this.bsiClientResponse = new DevExpress.XtraBars.BarSubItem();
            this.bbiClientAccepted = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClientRejected = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClientOnHold = new DevExpress.XtraBars.BarButtonItem();
            this.bsiVisitJobWizard = new DevExpress.XtraBars.BarSubItem();
            this.bbiAddVisitWizard = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddJobWizard = new DevExpress.XtraBars.BarButtonItem();
            this.bbiTenderClosed = new DevExpress.XtraBars.BarButtonItem();
            this.bbiTenderCancelled = new DevExpress.XtraBars.BarButtonItem();
            this.bsiSendVisits = new DevExpress.XtraBars.BarSubItem();
            this.bbiSelectJobsReadyToSend = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSendJobs = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.beiLoadingProgress = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemMarqueeProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar();
            this.bbiLoadingCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiVisitDrillDown = new DevExpress.XtraBars.BarButtonItem();
            this.bbiViewSiteOnMap = new DevExpress.XtraBars.BarButtonItem();
            this.bsiFilterSelected = new DevExpress.XtraBars.BarSubItem();
            this.bciFilterTenderSelected = new DevExpress.XtraBars.BarCheckItem();
            this.bciFilterVisitsSelected = new DevExpress.XtraBars.BarCheckItem();
            this.bsiSelectedCount = new DevExpress.XtraBars.BarStaticItem();
            this.repositoryItemCheckEditIgnoreHistoric = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.bbiImport = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.sp06145OMVisitEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemDuration1 = new DevExpress.XtraScheduler.UI.RepositoryItemDuration();
            this.toggleRecurrenceItem1 = new DevExpress.XtraScheduler.UI.ToggleRecurrenceItem();
            this.sp06145_OM_Visit_EditTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06145_OM_Visit_EditTableAdapter();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            this.sp05089_CRM_Contacts_Linked_To_RecordTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp05089_CRM_Contacts_Linked_To_RecordTableAdapter();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.hideContainerRight = new DevExpress.XtraBars.Docking.AutoHideContainer();
            this.dockPanelVisitDetails = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer1 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.LinkedJobCountTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LinkedVisitCountTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientCommentsMemoExEdit = new DevExpress.XtraEditors.MemoExEdit();
            this.CMCommentsMemoExEdit = new DevExpress.XtraEditors.MemoExEdit();
            this.ProposedLabourTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ProposedLabourTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StatusIsssueTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RevisionNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TenderIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TenderDescriptionMemoExEdit = new DevExpress.XtraEditors.MemoExEdit();
            this.JobSubTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ContactPersonPositionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ContactPersonNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientPONumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RequiredWorkCompletedDateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.QuoteAcceptedByClientDateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ContractDescriptionMemoExEdit = new DevExpress.XtraEditors.MemoExEdit();
            this.OurInternalCommentsMemoExEdit = new DevExpress.XtraEditors.MemoExEdit();
            this.CreatedByStaffNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TenderStatusTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FriendlyVisitNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.QuoteSubmittedToClientDateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CMInitialAttendanceDateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ReturnToClientByDateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RequestReceivedDateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CMTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.KAMTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SitePostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteAddressMemoExEdit = new DevExpress.XtraEditors.MemoExEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.ItemForVisitID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFriendlyVisitNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem20 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForSiteCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem23 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem24 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleSeparator3 = new DevExpress.XtraLayout.SimpleSeparator();
            this.ItemForSiteAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSitePostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTenderDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobSubType = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForContactPersonName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContactPersonPosition = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOurInternalComments = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTenderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTenderStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRevisionNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatusIsssue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForProposedLabourType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForProposedLabour = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCMComments = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRequestReceivedDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientComments = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCMInitialAttendanceDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReturnToClientByDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRequiredWorkCompletedDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForQuoteAcceptedByClientDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForQuoteSubmittedToClientDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForKAM = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedJobCount = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCM = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientPONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedVisitCount = new DevExpress.XtraLayout.LayoutControlItem();
            this.hideContainerLeft = new DevExpress.XtraBars.Docking.AutoHideContainer();
            this.dockPanelFilters = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.btnFormatData = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEditJobSubTypeFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditJobTypeFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.TenderIDsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.checkEditDatesFromToday = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditDateRange = new DevExpress.XtraEditors.CheckEdit();
            this.popupContainerEditVisitSystemStatusFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.btnClearAllFilters = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEditCMFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditShowTabPages = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditLabourFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.buttonEditSiteFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.buttonEditClientFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.popupContainerEditDateRange = new DevExpress.XtraEditors.PopupContainerEdit();
            this.checkEditIgnoreHistoric = new DevExpress.XtraEditors.CheckEdit();
            this.btnLoad = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEditKAMFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tabbedControlGroupFilterType = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroupFilterCriteria = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDateRange = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLabourFilter = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForClearAllFilters = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupSpecificVisitIDs = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFormatDataBtn = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp06019_OM_Job_Manager_KAMs_FilterTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06019_OM_Job_Manager_KAMs_FilterTableAdapter();
            this.sp06020_OM_Job_Manager_CMs_FilterTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06020_OM_Job_Manager_CMs_FilterTableAdapter();
            this.sp04022_Core_Dummy_TabPageListTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04022_Core_Dummy_TabPageListTableAdapter();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.sp06489_OM_Tender_Status_FilterTableAdapter = new WoodPlan5.DataSet_OM_TenderTableAdapters.sp06489_OM_Tender_Status_FilterTableAdapter();
            this.sp06490_OM_Tender_Staff_FilterTableAdapter = new WoodPlan5.DataSet_OM_TenderTableAdapters.sp06490_OM_Tender_Staff_FilterTableAdapter();
            this.sp06491_OM_Tender_ManagerTableAdapter = new WoodPlan5.DataSet_OM_TenderTableAdapters.sp06491_OM_Tender_ManagerTableAdapter();
            this.sp06493_OM_Job_Type_FilterTableAdapter = new WoodPlan5.DataSet_OM_TenderTableAdapters.sp06493_OM_Job_Type_FilterTableAdapter();
            this.sp06494_OM_Job_Sub_Type_FilterTableAdapter = new WoodPlan5.DataSet_OM_TenderTableAdapters.sp06494_OM_Job_Sub_Type_FilterTableAdapter();
            this.sp06495_OM_Visits_Linked_to_TenderTableAdapter = new WoodPlan5.DataSet_OM_TenderTableAdapters.sp06495_OM_Visits_Linked_to_TenderTableAdapter();
            this.sp06514_OM_Linked_Documents_Linked_To_RecordTableAdapter = new WoodPlan5.DataSet_OM_CoreTableAdapters.sp06514_OM_Linked_Documents_Linked_To_RecordTableAdapter();
            this.sp06515_OM_Tender_History_For_TenderTableAdapter = new WoodPlan5.DataSet_OM_TenderTableAdapters.sp06515_OM_Tender_History_For_TenderTableAdapter();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsVisit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedVisits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLabour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditClientSignature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditSelfBillingInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditClientInvoiceID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditPictureLink)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVisit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06495OMVisitsLinkedtoTenderBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Tender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewVisit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlJobSubTypeFilter)).BeginInit();
            this.popupContainerControlJobSubTypeFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06494OMJobSubTypeFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlJobTypeFilter)).BeginInit();
            this.popupContainerControlJobTypeFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06493OMJobTypeFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCMFilter)).BeginInit();
            this.popupContainerControlCMFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06020OMJobManagerCMsFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Summer_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRangeFromToday)).BeginInit();
            this.popupContainerControlDateRangeFromToday.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDaysAfter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDaysPrior.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlKAMFilter)).BeginInit();
            this.popupContainerControlKAMFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06019OMJobManagerKAMsFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLabourFilter)).BeginInit();
            this.popupContainerControlLabourFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06490OMTenderStaffFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlShowTabPages)).BeginInit();
            this.popupContainerControlShowTabPages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04022CoreDummyTabPageListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlVisitSystemStatusFilter)).BeginInit();
            this.popupContainerControlVisitSystemStatusFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06489OMTenderStatusFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06491OMTenderManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedVisits2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsJobs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditTenderGroupDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBarGreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditClientQuoteExtractFilt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBarRed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPageVisits.SuspendLayout();
            this.xtraTabPageHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06515OMTenderHistoryForTenderBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).BeginInit();
            this.xtraTabPageCRM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCRM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05089CRMContactsLinkedToRecordBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCRM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            this.xtraTabPageLinkedDocuments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06514OMLinkedDocumentsLinkedToRecordBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Client_PO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMarqueeProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditIgnoreHistoric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06145OMVisitEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.hideContainerRight.SuspendLayout();
            this.dockPanelVisitDetails.SuspendLayout();
            this.controlContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedJobCountTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedVisitCountTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientCommentsMemoExEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CMCommentsMemoExEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProposedLabourTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProposedLabourTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIsssueTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RevisionNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderDescriptionMemoExEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactPersonPositionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactPersonNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequiredWorkCompletedDateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteAcceptedByClientDateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractDescriptionMemoExEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OurInternalCommentsMemoExEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderStatusTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FriendlyVisitNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteSubmittedToClientDateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CMInitialAttendanceDateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReturnToClientByDateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequestReceivedDateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CMTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KAMTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SitePostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressMemoExEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFriendlyVisitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSitePostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactPersonName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactPersonPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOurInternalComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRevisionNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusIsssue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProposedLabourType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProposedLabour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCMComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRequestReceivedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCMInitialAttendanceDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReturnToClientByDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRequiredWorkCompletedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuoteAcceptedByClientDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuoteSubmittedToClientDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKAM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedJobCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedVisitCount)).BeginInit();
            this.hideContainerLeft.SuspendLayout();
            this.dockPanelFilters.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditJobSubTypeFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditJobTypeFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderIDsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDatesFromToday.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDateRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditVisitSystemStatusFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditCMFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditShowTabPages.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditLabourFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSiteFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditDateRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIgnoreHistoric.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditKAMFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupFilterType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupFilterCriteria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClearAllFilters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSpecificVisitIDs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFormatDataBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1382, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 654);
            this.barDockControlBottom.Size = new System.Drawing.Size(1382, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 654);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1382, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 654);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Images = this.imageCollection2;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiAddVisitWizard,
            this.bbiRefresh,
            this.bbiImport,
            this.bbiViewSiteOnMap,
            this.bciFilterTenderSelected,
            this.bsiSelectedCount,
            this.bsiSendVisits,
            this.bbiSelectJobsReadyToSend,
            this.bbiSendJobs,
            this.bbiAddJobWizard,
            this.bsiFilterSelected,
            this.bciFilterVisitsSelected,
            this.bbiSendQuoteToCM,
            this.bsiStatus,
            this.bbiCMReturnedQuote,
            this.beiLoadingProgress,
            this.bbiLoadingCancel,
            this.bsiToolbarAdd,
            this.bbiToolbarAdd,
            this.bbiToolbarAddFromTemplate,
            this.bbiTenderClosed,
            this.bsiKAMAuthorised,
            this.bbiKAMAccepted,
            this.bbiKAMRejected,
            this.bbiSentToClient,
            this.bsiClientResponse,
            this.bbiClientAccepted,
            this.bbiClientRejected,
            this.bbiClientOnHold,
            this.bbiTenderCancelled,
            this.bsiSendQuoteToCM,
            this.bbiQuoteNotRequired,
            this.bsiVisitJobWizard,
            this.bbiVisitDrillDown});
            this.barManager1.MaxItemId = 159;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1,
            this.repositoryItemCheckEditIgnoreHistoric,
            this.repositoryItemDuration1,
            this.repositoryItemSpinEdit1,
            this.repositoryItemMarqueeProgressBar1});
            this.barManager1.HighlightedLinkChanged += new DevExpress.XtraBars.HighlightedLinkChangedEventHandler(this.barManager1_HighlightedLinkChanged);
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colDisabled2
            // 
            this.colDisabled2.Caption = "Disabled";
            this.colDisabled2.ColumnEdit = this.repositoryItemCheckEdit16;
            this.colDisabled2.FieldName = "Disabled";
            this.colDisabled2.Name = "colDisabled2";
            this.colDisabled2.OptionsColumn.AllowEdit = false;
            this.colDisabled2.OptionsColumn.AllowFocus = false;
            this.colDisabled2.OptionsColumn.ReadOnly = true;
            this.colDisabled2.Width = 61;
            // 
            // repositoryItemCheckEdit16
            // 
            this.repositoryItemCheckEdit16.AutoHeight = false;
            this.repositoryItemCheckEdit16.Caption = "Check";
            this.repositoryItemCheckEdit16.Name = "repositoryItemCheckEdit16";
            this.repositoryItemCheckEdit16.ValueChecked = 1;
            this.repositoryItemCheckEdit16.ValueUnchecked = 0;
            // 
            // colDisabled
            // 
            this.colDisabled.Caption = "Disabled";
            this.colDisabled.ColumnEdit = this.repositoryItemCheckEdit8;
            this.colDisabled.FieldName = "Disabled";
            this.colDisabled.Name = "colDisabled";
            this.colDisabled.OptionsColumn.AllowEdit = false;
            this.colDisabled.OptionsColumn.AllowFocus = false;
            this.colDisabled.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemCheckEdit8
            // 
            this.repositoryItemCheckEdit8.AutoHeight = false;
            this.repositoryItemCheckEdit8.Caption = "Check";
            this.repositoryItemCheckEdit8.Name = "repositoryItemCheckEdit8";
            this.repositoryItemCheckEdit8.ValueChecked = 1;
            this.repositoryItemCheckEdit8.ValueUnchecked = 0;
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit17;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 51;
            // 
            // repositoryItemCheckEdit17
            // 
            this.repositoryItemCheckEdit17.AutoHeight = false;
            this.repositoryItemCheckEdit17.Caption = "Check";
            this.repositoryItemCheckEdit17.Name = "repositoryItemCheckEdit17";
            this.repositoryItemCheckEdit17.ValueChecked = 1;
            this.repositoryItemCheckEdit17.ValueUnchecked = 0;
            // 
            // colDaysQuoteOverdue
            // 
            this.colDaysQuoteOverdue.Caption = "Quote Overdue";
            this.colDaysQuoteOverdue.ColumnEdit = this.repositoryItemTextEditNumericDays;
            this.colDaysQuoteOverdue.FieldName = "DaysQuoteOverdue";
            this.colDaysQuoteOverdue.Name = "colDaysQuoteOverdue";
            this.colDaysQuoteOverdue.OptionsColumn.AllowEdit = false;
            this.colDaysQuoteOverdue.OptionsColumn.AllowFocus = false;
            this.colDaysQuoteOverdue.OptionsColumn.ReadOnly = true;
            this.colDaysQuoteOverdue.Visible = true;
            this.colDaysQuoteOverdue.VisibleIndex = 13;
            this.colDaysQuoteOverdue.Width = 94;
            // 
            // repositoryItemTextEditNumericDays
            // 
            this.repositoryItemTextEditNumericDays.AutoHeight = false;
            this.repositoryItemTextEditNumericDays.Mask.EditMask = "#####0 Days";
            this.repositoryItemTextEditNumericDays.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumericDays.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumericDays.Name = "repositoryItemTextEditNumericDays";
            // 
            // colStatusIssueID1
            // 
            this.colStatusIssueID1.Caption = "Status Issue ID";
            this.colStatusIssueID1.FieldName = "StatusIssueID";
            this.colStatusIssueID1.Name = "colStatusIssueID1";
            this.colStatusIssueID1.OptionsColumn.AllowEdit = false;
            this.colStatusIssueID1.OptionsColumn.AllowFocus = false;
            this.colStatusIssueID1.OptionsColumn.ReadOnly = true;
            this.colStatusIssueID1.Width = 93;
            // 
            // colStatusIsssue
            // 
            this.colStatusIsssue.Caption = "Status Issue";
            this.colStatusIsssue.FieldName = "StatusIsssue";
            this.colStatusIsssue.Name = "colStatusIsssue";
            this.colStatusIsssue.OptionsColumn.AllowEdit = false;
            this.colStatusIsssue.OptionsColumn.AllowFocus = false;
            this.colStatusIsssue.OptionsColumn.ReadOnly = true;
            this.colStatusIsssue.Width = 193;
            // 
            // colStatusID1
            // 
            this.colStatusID1.Caption = "Status ID";
            this.colStatusID1.FieldName = "StatusID";
            this.colStatusID1.Name = "colStatusID1";
            this.colStatusID1.OptionsColumn.AllowEdit = false;
            this.colStatusID1.OptionsColumn.AllowFocus = false;
            this.colStatusID1.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemTextEditInteger2
            // 
            this.repositoryItemTextEditInteger2.AutoHeight = false;
            this.repositoryItemTextEditInteger2.LookAndFeel.SkinName = "Blue";
            this.repositoryItemTextEditInteger2.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger2.Name = "repositoryItemTextEditInteger2";
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // repositoryItemTextEditLatLong
            // 
            this.repositoryItemTextEditLatLong.AutoHeight = false;
            this.repositoryItemTextEditLatLong.Mask.EditMask = "f8";
            this.repositoryItemTextEditLatLong.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditLatLong.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditLatLong.Name = "repositoryItemTextEditLatLong";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // repositoryItemTextEditHTML0
            // 
            this.repositoryItemTextEditHTML0.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML0.AutoHeight = false;
            this.repositoryItemTextEditHTML0.Name = "repositoryItemTextEditHTML0";
            // 
            // repositoryItemHyperLinkEditLinkedDocumentsVisit
            // 
            this.repositoryItemHyperLinkEditLinkedDocumentsVisit.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocumentsVisit.Name = "repositoryItemHyperLinkEditLinkedDocumentsVisit";
            this.repositoryItemHyperLinkEditLinkedDocumentsVisit.SingleClick = true;
            // 
            // repositoryItemHyperLinkEditGrid1
            // 
            this.repositoryItemHyperLinkEditGrid1.AutoHeight = false;
            this.repositoryItemHyperLinkEditGrid1.Name = "repositoryItemHyperLinkEditGrid1";
            this.repositoryItemHyperLinkEditGrid1.SingleClick = true;
            // 
            // repositoryItemHyperLinkEditLinkedVisits
            // 
            this.repositoryItemHyperLinkEditLinkedVisits.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedVisits.Name = "repositoryItemHyperLinkEditLinkedVisits";
            this.repositoryItemHyperLinkEditLinkedVisits.SingleClick = true;
            // 
            // repositoryItemHyperLinkEditLabour
            // 
            this.repositoryItemHyperLinkEditLabour.AutoHeight = false;
            this.repositoryItemHyperLinkEditLabour.Name = "repositoryItemHyperLinkEditLabour";
            this.repositoryItemHyperLinkEditLabour.SingleClick = true;
            // 
            // repositoryItemHyperLinkEditClientSignature
            // 
            this.repositoryItemHyperLinkEditClientSignature.AutoHeight = false;
            this.repositoryItemHyperLinkEditClientSignature.Name = "repositoryItemHyperLinkEditClientSignature";
            this.repositoryItemHyperLinkEditClientSignature.SingleClick = true;
            // 
            // repositoryItemPictureEdit2
            // 
            this.repositoryItemPictureEdit2.Name = "repositoryItemPictureEdit2";
            this.repositoryItemPictureEdit2.ReadOnly = true;
            this.repositoryItemPictureEdit2.ShowMenu = false;
            // 
            // repositoryItemHyperLinkEditSelfBillingInvoice
            // 
            this.repositoryItemHyperLinkEditSelfBillingInvoice.AutoHeight = false;
            this.repositoryItemHyperLinkEditSelfBillingInvoice.Name = "repositoryItemHyperLinkEditSelfBillingInvoice";
            this.repositoryItemHyperLinkEditSelfBillingInvoice.SingleClick = true;
            // 
            // repositoryItemHyperLinkEditClientInvoiceID
            // 
            this.repositoryItemHyperLinkEditClientInvoiceID.AutoHeight = false;
            this.repositoryItemHyperLinkEditClientInvoiceID.Name = "repositoryItemHyperLinkEditClientInvoiceID";
            this.repositoryItemHyperLinkEditClientInvoiceID.SingleClick = true;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // repositoryItemHyperLinkEditPictureLink
            // 
            this.repositoryItemHyperLinkEditPictureLink.AutoHeight = false;
            this.repositoryItemHyperLinkEditPictureLink.LookAndFeel.SkinName = "Blue";
            this.repositoryItemHyperLinkEditPictureLink.Name = "repositoryItemHyperLinkEditPictureLink";
            this.repositoryItemHyperLinkEditPictureLink.SingleClick = true;
            // 
            // repositoryItemTextEdit14
            // 
            this.repositoryItemTextEdit14.AutoHeight = false;
            this.repositoryItemTextEdit14.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEdit14.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit14.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit14.Name = "repositoryItemTextEdit14";
            // 
            // repositoryItemMemoExEdit20
            // 
            this.repositoryItemMemoExEdit20.AutoHeight = false;
            this.repositoryItemMemoExEdit20.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit20.Name = "repositoryItemMemoExEdit20";
            this.repositoryItemMemoExEdit20.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit20.ShowIcon = false;
            // 
            // repositoryItemTextEdit15
            // 
            this.repositoryItemTextEdit15.AllowFocused = false;
            this.repositoryItemTextEdit15.AutoHeight = false;
            this.repositoryItemTextEdit15.Name = "repositoryItemTextEdit15";
            // 
            // colVisitStatusID
            // 
            this.colVisitStatusID.Caption = "System Status ID";
            this.colVisitStatusID.FieldName = "VisitStatusID";
            this.colVisitStatusID.Name = "colVisitStatusID";
            this.colVisitStatusID.OptionsColumn.AllowEdit = false;
            this.colVisitStatusID.OptionsColumn.AllowFocus = false;
            this.colVisitStatusID.OptionsColumn.ReadOnly = true;
            this.colVisitStatusID.Width = 88;
            // 
            // repositoryItemPopupContainerEditDateRange
            // 
            this.repositoryItemPopupContainerEditDateRange.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateRange.LookAndFeel.SkinName = "Blue";
            this.repositoryItemPopupContainerEditDateRange.Name = "repositoryItemPopupContainerEditDateRange";
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControlVisit
            // 
            this.gridControlVisit.DataSource = this.sp06495OMVisitsLinkedtoTenderBindingSource;
            this.gridControlVisit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlVisit.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlVisit.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlVisit.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 13, true, true, "Reload Data", "reload"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 24, true, true, "Set Selected Visit(s) As Ready To Send", "ready_to_send"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 23, true, true, "Manually Complete", "manual_complete")});
            this.gridControlVisit.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlVisit_EmbeddedNavigator_ButtonClick);
            this.gridControlVisit.Location = new System.Drawing.Point(0, 0);
            this.gridControlVisit.MainView = this.gridViewVisit;
            this.gridControlVisit.MenuManager = this.barManager1;
            this.gridControlVisit.Name = "gridControlVisit";
            this.gridControlVisit.Size = new System.Drawing.Size(1331, 180);
            this.gridControlVisit.TabIndex = 4;
            this.gridControlVisit.ToolTipController = this.toolTipController1;
            this.gridControlVisit.UseEmbeddedNavigator = true;
            this.gridControlVisit.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewVisit});
            // 
            // sp06495OMVisitsLinkedtoTenderBindingSource
            // 
            this.sp06495OMVisitsLinkedtoTenderBindingSource.DataMember = "sp06495_OM_Visits_Linked_to_Tender";
            this.sp06495OMVisitsLinkedtoTenderBindingSource.DataSource = this.dataSet_OM_Tender;
            // 
            // dataSet_OM_Tender
            // 
            this.dataSet_OM_Tender.DataSetName = "DataSet_OM_Tender";
            this.dataSet_OM_Tender.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "Scheduler_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 7);
            this.imageCollection1.Images.SetKeyName(7, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.linked_documents_16_16, "linked_documents_16_16", typeof(global::WoodPlan5.Properties.Resources), 8);
            this.imageCollection1.Images.SetKeyName(8, "linked_documents_16_16");
            this.imageCollection1.InsertGalleryImage("addgroupheader_16x16.png", "images/reports/addgroupheader_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/reports/addgroupheader_16x16.png"), 9);
            this.imageCollection1.Images.SetKeyName(9, "addgroupheader_16x16.png");
            this.imageCollection1.InsertGalleryImage("groupheader_16x16.png", "images/reports/groupheader_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/reports/groupheader_16x16.png"), 10);
            this.imageCollection1.Images.SetKeyName(10, "groupheader_16x16.png");
            this.imageCollection1.InsertGalleryImage("copy_16x16.png", "images/edit/copy_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/copy_16x16.png"), 11);
            this.imageCollection1.Images.SetKeyName(11, "copy_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.BlockAdd_16x16, "BlockAdd_16x16", typeof(global::WoodPlan5.Properties.Resources), 12);
            this.imageCollection1.Images.SetKeyName(12, "BlockAdd_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 13);
            this.imageCollection1.Images.SetKeyName(13, "refresh_16x16");
            this.imageCollection1.InsertGalleryImage("switchtimescalesto_16x16.png", "images/scheduling/switchtimescalesto_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/scheduling/switchtimescalesto_16x16.png"), 14);
            this.imageCollection1.Images.SetKeyName(14, "switchtimescalesto_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention2_16, "attention2_16", typeof(global::WoodPlan5.Properties.Resources), 15);
            this.imageCollection1.Images.SetKeyName(15, "attention2_16");
            this.imageCollection1.InsertGalleryImage("topbottomrules_16x16.png", "images/conditional%20formatting/topbottomrules_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/conditional%20formatting/topbottomrules_16x16.png"), 16);
            this.imageCollection1.Images.SetKeyName(16, "topbottomrules_16x16.png");
            this.imageCollection1.InsertGalleryImage("dayview_16x16.png", "images/scheduling/dayview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/scheduling/dayview_16x16.png"), 17);
            this.imageCollection1.Images.SetKeyName(17, "dayview_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh2_16x16.png", "images/actions/refresh2_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh2_16x16.png"), 18);
            this.imageCollection1.Images.SetKeyName(18, "refresh2_16x16.png");
            this.imageCollection1.InsertGalleryImage("clearfilter_16x16.png", "images/filter/clearfilter_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/filter/clearfilter_16x16.png"), 19);
            this.imageCollection1.Images.SetKeyName(19, "clearfilter_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.BlockEdit_16x16, "BlockEdit_16x16", typeof(global::WoodPlan5.Properties.Resources), 20);
            this.imageCollection1.Images.SetKeyName(20, "BlockEdit_16x16");
            this.imageCollection1.InsertGalleryImage("attach_16x16.png", "images/mail/attach_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/mail/attach_16x16.png"), 21);
            this.imageCollection1.Images.SetKeyName(21, "attach_16x16.png");
            this.imageCollection1.InsertGalleryImage("convert_16x16.png", "images/actions/convert_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/convert_16x16.png"), 22);
            this.imageCollection1.Images.SetKeyName(22, "convert_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Apply_16x16, "Apply_16x16", typeof(global::WoodPlan5.Properties.Resources), 23);
            this.imageCollection1.Images.SetKeyName(23, "Apply_16x16");
            this.imageCollection1.InsertGalleryImage("iconsetsymbols3_16x16.png", "images/conditional%20formatting/iconsetsymbols3_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/conditional%20formatting/iconsetsymbols3_16x16.png"), 24);
            this.imageCollection1.Images.SetKeyName(24, "iconsetsymbols3_16x16.png");
            // 
            // gridViewVisit
            // 
            this.gridViewVisit.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVisitID,
            this.colSiteContractID,
            this.colVisitNumber,
            this.colCreatedByStaffID,
            this.colExpectedStartDate,
            this.colExpectedEndDate,
            this.colStartDate,
            this.colEndDate,
            this.colCostCalculationLevel,
            this.colVisitCost,
            this.colVisitSell,
            this.colRemarks,
            this.colStartLatitude,
            this.colStartLongitude,
            this.colFinishLatitude,
            this.colFinishLongitude,
            this.colCostCalculationLevelDescription,
            this.colClientContractID,
            this.colSiteID,
            this.colSiteContractStartDate,
            this.colSiteContractEndDate,
            this.colSiteContractActive,
            this.colSiteContractValue,
            this.colClientID,
            this.colGCCompanyID,
            this.colContractTypeID,
            this.colContractStatusID,
            this.colContractDirectorID,
            this.colClientContractStartDate,
            this.colClientContractEndDate,
            this.colClientContractActive,
            this.colSectorTypeID,
            this.colClientContractValue,
            this.colFinanceClientCode,
            this.colClientName,
            this.colGCCompanyName,
            this.colContractType,
            this.colContractStatus,
            this.colContractDirector,
            this.colSectorType,
            this.colSiteName,
            this.colLinkedToParent1,
            this.colContractDescription,
            this.colSitePostcode,
            this.colSiteLocationX,
            this.colSiteLocationY,
            this.colLinkedDocumentCount,
            this.colLinkedJobCount,
            this.colLinkedOutstandingJobCount,
            this.colLinkedCompletedJobCount,
            this.colCreatedByStaffName,
            this.colSiteAddress,
            this.colVisitStatus,
            this.colVisitStatusID,
            this.colTimeliness,
            this.colWorkNumber,
            this.colVisitCategory,
            this.colVisitCategoryID,
            this.colLinkedVisitCount,
            this.colLabourCount,
            this.colIssueFound,
            this.colIssueType,
            this.colIssueTypeID,
            this.colIssueRemarks,
            this.colClientSignaturePath,
            this.colImagesFolderOM,
            this.colRework,
            this.colExtraWorkComments,
            this.colExtraWorkRequired,
            this.colExtraWorkType,
            this.colAccidentOnSite,
            this.colAccidentComment,
            this.colContractorNames,
            this.Alert2,
            this.colSuspendedJobCount,
            this.colLinkedPictureCount,
            this.colFriendlyVisitNumber,
            this.colVisitType,
            this.colVisitTypeID,
            this.colClientPO,
            this.colClientPOID1,
            this.colPDAVersion,
            this.colSiteCode,
            this.colSellCalculationLevel,
            this.colSellCalculationLevelDescription,
            this.colSelected4,
            this.colSelfBillingInvoiceID1,
            this.colClientInvoiceID1,
            this.colCMName,
            this.colDaysAllowed,
            this.colManagerName,
            this.colManagerNotes,
            this.colNoOnetoSign,
            this.colSiteCategory,
            this.colCreatedFromVisitTemplate,
            this.colCreatedFromVisitTemplateID,
            this.colExternalCustomerStatus,
            this.colCompletionSheetNumber,
            this.colStatusIssueID,
            this.colStatusIssue,
            this.colDaysSeparation,
            this.colSiteContractDaysSeparationPercent,
            this.colSiteAddressLine1,
            this.colSiteAddressLine2,
            this.colSiteAddressLine3,
            this.colSiteAddressLine4,
            this.colSiteAddressLine5,
            this.colDoNotPayContractor1,
            this.colDoNotInvoiceClient1,
            this.colKAMName,
            this.colSiteInstructions,
            this.colTenderID1,
            this.colExtraCostsFinalised});
            this.gridViewVisit.GridControl = this.gridControlVisit;
            this.gridViewVisit.GroupCount = 1;
            this.gridViewVisit.Name = "gridViewVisit";
            this.gridViewVisit.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewVisit.OptionsFind.FindDelay = 2000;
            this.gridViewVisit.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewVisit.OptionsLayout.StoreAppearance = true;
            this.gridViewVisit.OptionsLayout.StoreFormatRules = true;
            this.gridViewVisit.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewVisit.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewVisit.OptionsSelection.MultiSelect = true;
            this.gridViewVisit.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewVisit.OptionsView.ColumnAutoWidth = false;
            this.gridViewVisit.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewVisit.OptionsView.ShowGroupPanel = false;
            this.gridViewVisit.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colVisitNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewVisit.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewVisit_CustomDrawCell);
            this.gridViewVisit.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewVisit_CustomRowCellEdit);
            this.gridViewVisit.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewVisit.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewVisit.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewVisit.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewVisit_ShowingEditor);
            this.gridViewVisit.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewVisit.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewVisit.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridViewVisit_CustomUnboundColumnData);
            this.gridViewVisit.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewVisit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewVisit_MouseUp);
            this.gridViewVisit.DoubleClick += new System.EventHandler(this.gridViewVisit_DoubleClick);
            this.gridViewVisit.GotFocus += new System.EventHandler(this.gridViewVisit_GotFocus);
            // 
            // colVisitID
            // 
            this.colVisitID.Caption = "Visit ID";
            this.colVisitID.FieldName = "VisitID";
            this.colVisitID.Name = "colVisitID";
            this.colVisitID.OptionsColumn.AllowEdit = false;
            this.colVisitID.OptionsColumn.AllowFocus = false;
            this.colVisitID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteContractID
            // 
            this.colSiteContractID.Caption = "Site Contract ID";
            this.colSiteContractID.FieldName = "SiteContractID";
            this.colSiteContractID.Name = "colSiteContractID";
            this.colSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSiteContractID.Width = 98;
            // 
            // colVisitNumber
            // 
            this.colVisitNumber.Caption = "Visit #";
            this.colVisitNumber.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colVisitNumber.FieldName = "VisitNumber";
            this.colVisitNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colVisitNumber.Name = "colVisitNumber";
            this.colVisitNumber.OptionsColumn.AllowEdit = false;
            this.colVisitNumber.OptionsColumn.AllowFocus = false;
            this.colVisitNumber.OptionsColumn.ReadOnly = true;
            this.colVisitNumber.Visible = true;
            this.colVisitNumber.VisibleIndex = 2;
            this.colVisitNumber.Width = 82;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 116;
            // 
            // colExpectedStartDate
            // 
            this.colExpectedStartDate.Caption = "Expected Start";
            this.colExpectedStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colExpectedStartDate.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpectedStartDate.Name = "colExpectedStartDate";
            this.colExpectedStartDate.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpectedStartDate.Visible = true;
            this.colExpectedStartDate.VisibleIndex = 9;
            this.colExpectedStartDate.Width = 104;
            // 
            // colExpectedEndDate
            // 
            this.colExpectedEndDate.Caption = "Expected End";
            this.colExpectedEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colExpectedEndDate.FieldName = "ExpectedEndDate";
            this.colExpectedEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpectedEndDate.Name = "colExpectedEndDate";
            this.colExpectedEndDate.OptionsColumn.AllowEdit = false;
            this.colExpectedEndDate.OptionsColumn.AllowFocus = false;
            this.colExpectedEndDate.OptionsColumn.ReadOnly = true;
            this.colExpectedEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpectedEndDate.Visible = true;
            this.colExpectedEndDate.VisibleIndex = 10;
            this.colExpectedEndDate.Width = 100;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start";
            this.colStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 11;
            this.colStartDate.Width = 100;
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "End";
            this.colEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 12;
            this.colEndDate.Width = 100;
            // 
            // colCostCalculationLevel
            // 
            this.colCostCalculationLevel.Caption = "Cost Calculation ID";
            this.colCostCalculationLevel.FieldName = "CostCalculationLevel";
            this.colCostCalculationLevel.Name = "colCostCalculationLevel";
            this.colCostCalculationLevel.OptionsColumn.AllowEdit = false;
            this.colCostCalculationLevel.OptionsColumn.AllowFocus = false;
            this.colCostCalculationLevel.OptionsColumn.ReadOnly = true;
            this.colCostCalculationLevel.Width = 140;
            // 
            // colVisitCost
            // 
            this.colVisitCost.Caption = "Visit Cost";
            this.colVisitCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colVisitCost.FieldName = "VisitCost";
            this.colVisitCost.Name = "colVisitCost";
            this.colVisitCost.OptionsColumn.AllowEdit = false;
            this.colVisitCost.OptionsColumn.AllowFocus = false;
            this.colVisitCost.OptionsColumn.ReadOnly = true;
            this.colVisitCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitCost", "{0:c}")});
            // 
            // colVisitSell
            // 
            this.colVisitSell.Caption = "Visit Sell";
            this.colVisitSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colVisitSell.FieldName = "VisitSell";
            this.colVisitSell.Name = "colVisitSell";
            this.colVisitSell.OptionsColumn.AllowEdit = false;
            this.colVisitSell.OptionsColumn.AllowFocus = false;
            this.colVisitSell.OptionsColumn.ReadOnly = true;
            this.colVisitSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitSell", "{0:c}")});
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            // 
            // colStartLatitude
            // 
            this.colStartLatitude.Caption = "Start Latitude";
            this.colStartLatitude.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colStartLatitude.FieldName = "StartLatitude";
            this.colStartLatitude.Name = "colStartLatitude";
            this.colStartLatitude.OptionsColumn.AllowEdit = false;
            this.colStartLatitude.OptionsColumn.AllowFocus = false;
            this.colStartLatitude.OptionsColumn.ReadOnly = true;
            this.colStartLatitude.Width = 87;
            // 
            // colStartLongitude
            // 
            this.colStartLongitude.Caption = "Start Longitude";
            this.colStartLongitude.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colStartLongitude.FieldName = "StartLongitude";
            this.colStartLongitude.Name = "colStartLongitude";
            this.colStartLongitude.OptionsColumn.AllowEdit = false;
            this.colStartLongitude.OptionsColumn.AllowFocus = false;
            this.colStartLongitude.OptionsColumn.ReadOnly = true;
            this.colStartLongitude.Width = 95;
            // 
            // colFinishLatitude
            // 
            this.colFinishLatitude.Caption = "Finish Latitude";
            this.colFinishLatitude.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colFinishLatitude.FieldName = "FinishLatitude";
            this.colFinishLatitude.Name = "colFinishLatitude";
            this.colFinishLatitude.OptionsColumn.AllowEdit = false;
            this.colFinishLatitude.OptionsColumn.AllowFocus = false;
            this.colFinishLatitude.OptionsColumn.ReadOnly = true;
            this.colFinishLatitude.Width = 90;
            // 
            // colFinishLongitude
            // 
            this.colFinishLongitude.Caption = "Finish Longitude";
            this.colFinishLongitude.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colFinishLongitude.FieldName = "FinishLongitude";
            this.colFinishLongitude.Name = "colFinishLongitude";
            this.colFinishLongitude.OptionsColumn.AllowEdit = false;
            this.colFinishLongitude.OptionsColumn.AllowFocus = false;
            this.colFinishLongitude.OptionsColumn.ReadOnly = true;
            this.colFinishLongitude.Width = 98;
            // 
            // colCostCalculationLevelDescription
            // 
            this.colCostCalculationLevelDescription.Caption = "Cost Calculation";
            this.colCostCalculationLevelDescription.FieldName = "CostCalculationLevelDescription";
            this.colCostCalculationLevelDescription.Name = "colCostCalculationLevelDescription";
            this.colCostCalculationLevelDescription.OptionsColumn.AllowEdit = false;
            this.colCostCalculationLevelDescription.OptionsColumn.AllowFocus = false;
            this.colCostCalculationLevelDescription.OptionsColumn.ReadOnly = true;
            this.colCostCalculationLevelDescription.Width = 100;
            // 
            // colClientContractID
            // 
            this.colClientContractID.Caption = "Client Contract ID";
            this.colClientContractID.FieldName = "ClientContractID";
            this.colClientContractID.Name = "colClientContractID";
            this.colClientContractID.OptionsColumn.AllowEdit = false;
            this.colClientContractID.OptionsColumn.AllowFocus = false;
            this.colClientContractID.OptionsColumn.ReadOnly = true;
            this.colClientContractID.Width = 107;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.Visible = true;
            this.colSiteID.VisibleIndex = 1;
            this.colSiteID.Width = 70;
            // 
            // colSiteContractStartDate
            // 
            this.colSiteContractStartDate.Caption = "Site Contract Start";
            this.colSiteContractStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSiteContractStartDate.FieldName = "SiteContractStartDate";
            this.colSiteContractStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSiteContractStartDate.Name = "colSiteContractStartDate";
            this.colSiteContractStartDate.OptionsColumn.AllowEdit = false;
            this.colSiteContractStartDate.OptionsColumn.AllowFocus = false;
            this.colSiteContractStartDate.OptionsColumn.ReadOnly = true;
            this.colSiteContractStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSiteContractStartDate.Width = 111;
            // 
            // colSiteContractEndDate
            // 
            this.colSiteContractEndDate.Caption = "Site Contract End";
            this.colSiteContractEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSiteContractEndDate.FieldName = "SiteContractEndDate";
            this.colSiteContractEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSiteContractEndDate.Name = "colSiteContractEndDate";
            this.colSiteContractEndDate.OptionsColumn.AllowEdit = false;
            this.colSiteContractEndDate.OptionsColumn.AllowFocus = false;
            this.colSiteContractEndDate.OptionsColumn.ReadOnly = true;
            this.colSiteContractEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSiteContractEndDate.Width = 105;
            // 
            // colSiteContractActive
            // 
            this.colSiteContractActive.Caption = "Site Contract Active";
            this.colSiteContractActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSiteContractActive.FieldName = "SiteContractActive";
            this.colSiteContractActive.Name = "colSiteContractActive";
            this.colSiteContractActive.OptionsColumn.AllowEdit = false;
            this.colSiteContractActive.OptionsColumn.AllowFocus = false;
            this.colSiteContractActive.OptionsColumn.ReadOnly = true;
            this.colSiteContractActive.Width = 117;
            // 
            // colSiteContractValue
            // 
            this.colSiteContractValue.Caption = "Site Contract Value";
            this.colSiteContractValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSiteContractValue.FieldName = "SiteContractValue";
            this.colSiteContractValue.Name = "colSiteContractValue";
            this.colSiteContractValue.OptionsColumn.AllowEdit = false;
            this.colSiteContractValue.OptionsColumn.AllowFocus = false;
            this.colSiteContractValue.OptionsColumn.ReadOnly = true;
            this.colSiteContractValue.Width = 113;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colGCCompanyID
            // 
            this.colGCCompanyID.Caption = "GC Company ID";
            this.colGCCompanyID.FieldName = "GCCompanyID";
            this.colGCCompanyID.Name = "colGCCompanyID";
            this.colGCCompanyID.OptionsColumn.AllowEdit = false;
            this.colGCCompanyID.OptionsColumn.AllowFocus = false;
            this.colGCCompanyID.OptionsColumn.ReadOnly = true;
            this.colGCCompanyID.Width = 97;
            // 
            // colContractTypeID
            // 
            this.colContractTypeID.Caption = "Contract Type ID";
            this.colContractTypeID.FieldName = "ContractTypeID";
            this.colContractTypeID.Name = "colContractTypeID";
            this.colContractTypeID.OptionsColumn.AllowEdit = false;
            this.colContractTypeID.OptionsColumn.AllowFocus = false;
            this.colContractTypeID.OptionsColumn.ReadOnly = true;
            this.colContractTypeID.Width = 104;
            // 
            // colContractStatusID
            // 
            this.colContractStatusID.Caption = "Contract Status ID";
            this.colContractStatusID.FieldName = "ContractStatusID";
            this.colContractStatusID.Name = "colContractStatusID";
            this.colContractStatusID.OptionsColumn.AllowEdit = false;
            this.colContractStatusID.OptionsColumn.AllowFocus = false;
            this.colContractStatusID.OptionsColumn.ReadOnly = true;
            this.colContractStatusID.Width = 111;
            // 
            // colContractDirectorID
            // 
            this.colContractDirectorID.Caption = "Contract Director ID";
            this.colContractDirectorID.FieldName = "ContractDirectorID";
            this.colContractDirectorID.Name = "colContractDirectorID";
            this.colContractDirectorID.OptionsColumn.AllowEdit = false;
            this.colContractDirectorID.OptionsColumn.AllowFocus = false;
            this.colContractDirectorID.OptionsColumn.ReadOnly = true;
            this.colContractDirectorID.Width = 118;
            // 
            // colClientContractStartDate
            // 
            this.colClientContractStartDate.Caption = "Client Contract Start";
            this.colClientContractStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colClientContractStartDate.FieldName = "ClientContractStartDate";
            this.colClientContractStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colClientContractStartDate.Name = "colClientContractStartDate";
            this.colClientContractStartDate.OptionsColumn.AllowEdit = false;
            this.colClientContractStartDate.OptionsColumn.AllowFocus = false;
            this.colClientContractStartDate.OptionsColumn.ReadOnly = true;
            this.colClientContractStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colClientContractStartDate.Width = 120;
            // 
            // colClientContractEndDate
            // 
            this.colClientContractEndDate.Caption = "Client Contract End";
            this.colClientContractEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colClientContractEndDate.FieldName = "ClientContractEndDate";
            this.colClientContractEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colClientContractEndDate.Name = "colClientContractEndDate";
            this.colClientContractEndDate.OptionsColumn.AllowEdit = false;
            this.colClientContractEndDate.OptionsColumn.AllowFocus = false;
            this.colClientContractEndDate.OptionsColumn.ReadOnly = true;
            this.colClientContractEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colClientContractEndDate.Width = 114;
            // 
            // colClientContractActive
            // 
            this.colClientContractActive.Caption = "Client Contract Active";
            this.colClientContractActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colClientContractActive.FieldName = "ClientContractActive";
            this.colClientContractActive.Name = "colClientContractActive";
            this.colClientContractActive.OptionsColumn.AllowEdit = false;
            this.colClientContractActive.OptionsColumn.AllowFocus = false;
            this.colClientContractActive.OptionsColumn.ReadOnly = true;
            this.colClientContractActive.Width = 126;
            // 
            // colSectorTypeID
            // 
            this.colSectorTypeID.Caption = "Sector Type ID";
            this.colSectorTypeID.FieldName = "SectorTypeID";
            this.colSectorTypeID.Name = "colSectorTypeID";
            this.colSectorTypeID.OptionsColumn.AllowEdit = false;
            this.colSectorTypeID.OptionsColumn.AllowFocus = false;
            this.colSectorTypeID.OptionsColumn.ReadOnly = true;
            this.colSectorTypeID.Width = 93;
            // 
            // colClientContractValue
            // 
            this.colClientContractValue.Caption = "Client Contract Value";
            this.colClientContractValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colClientContractValue.FieldName = "ClientContractValue";
            this.colClientContractValue.Name = "colClientContractValue";
            this.colClientContractValue.OptionsColumn.AllowEdit = false;
            this.colClientContractValue.OptionsColumn.AllowFocus = false;
            this.colClientContractValue.OptionsColumn.ReadOnly = true;
            this.colClientContractValue.Width = 122;
            // 
            // colFinanceClientCode
            // 
            this.colFinanceClientCode.Caption = "Finance Client Code";
            this.colFinanceClientCode.FieldName = "FinanceClientCode";
            this.colFinanceClientCode.Name = "colFinanceClientCode";
            this.colFinanceClientCode.OptionsColumn.AllowEdit = false;
            this.colFinanceClientCode.OptionsColumn.AllowFocus = false;
            this.colFinanceClientCode.OptionsColumn.ReadOnly = true;
            this.colFinanceClientCode.Width = 116;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Width = 193;
            // 
            // colGCCompanyName
            // 
            this.colGCCompanyName.Caption = "GC Company Name";
            this.colGCCompanyName.FieldName = "GCCompanyName";
            this.colGCCompanyName.Name = "colGCCompanyName";
            this.colGCCompanyName.OptionsColumn.AllowEdit = false;
            this.colGCCompanyName.OptionsColumn.AllowFocus = false;
            this.colGCCompanyName.OptionsColumn.ReadOnly = true;
            this.colGCCompanyName.Width = 113;
            // 
            // colContractType
            // 
            this.colContractType.Caption = "Contract Type";
            this.colContractType.FieldName = "ContractType";
            this.colContractType.Name = "colContractType";
            this.colContractType.OptionsColumn.AllowEdit = false;
            this.colContractType.OptionsColumn.AllowFocus = false;
            this.colContractType.OptionsColumn.ReadOnly = true;
            this.colContractType.Width = 117;
            // 
            // colContractStatus
            // 
            this.colContractStatus.Caption = "Contract Status";
            this.colContractStatus.FieldName = "ContractStatus";
            this.colContractStatus.Name = "colContractStatus";
            this.colContractStatus.OptionsColumn.AllowEdit = false;
            this.colContractStatus.OptionsColumn.AllowFocus = false;
            this.colContractStatus.OptionsColumn.ReadOnly = true;
            this.colContractStatus.Width = 117;
            // 
            // colContractDirector
            // 
            this.colContractDirector.Caption = "Contract Director";
            this.colContractDirector.FieldName = "ContractDirector";
            this.colContractDirector.Name = "colContractDirector";
            this.colContractDirector.OptionsColumn.AllowEdit = false;
            this.colContractDirector.OptionsColumn.AllowFocus = false;
            this.colContractDirector.OptionsColumn.ReadOnly = true;
            this.colContractDirector.Width = 122;
            // 
            // colSectorType
            // 
            this.colSectorType.Caption = "Sector Type";
            this.colSectorType.FieldName = "SectorType";
            this.colSectorType.Name = "colSectorType";
            this.colSectorType.OptionsColumn.AllowEdit = false;
            this.colSectorType.OptionsColumn.AllowFocus = false;
            this.colSectorType.OptionsColumn.ReadOnly = true;
            this.colSectorType.Width = 107;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Width = 227;
            // 
            // colLinkedToParent1
            // 
            this.colLinkedToParent1.Caption = "Linked To Tender";
            this.colLinkedToParent1.ColumnEdit = this.repositoryItemTextEditHTML0;
            this.colLinkedToParent1.FieldName = "LinkedToParent";
            this.colLinkedToParent1.Name = "colLinkedToParent1";
            this.colLinkedToParent1.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent1.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent1.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent1.Width = 333;
            // 
            // colContractDescription
            // 
            this.colContractDescription.Caption = "Contract Description";
            this.colContractDescription.FieldName = "ContractDescription";
            this.colContractDescription.Name = "colContractDescription";
            this.colContractDescription.OptionsColumn.AllowEdit = false;
            this.colContractDescription.OptionsColumn.AllowFocus = false;
            this.colContractDescription.OptionsColumn.ReadOnly = true;
            this.colContractDescription.Width = 225;
            // 
            // colSitePostcode
            // 
            this.colSitePostcode.Caption = "Site Postcode";
            this.colSitePostcode.FieldName = "SitePostcode";
            this.colSitePostcode.Name = "colSitePostcode";
            this.colSitePostcode.OptionsColumn.AllowEdit = false;
            this.colSitePostcode.OptionsColumn.AllowFocus = false;
            this.colSitePostcode.OptionsColumn.ReadOnly = true;
            this.colSitePostcode.Visible = true;
            this.colSitePostcode.VisibleIndex = 4;
            this.colSitePostcode.Width = 86;
            // 
            // colSiteLocationX
            // 
            this.colSiteLocationX.Caption = "Site Latitude";
            this.colSiteLocationX.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colSiteLocationX.FieldName = "SiteLocationX";
            this.colSiteLocationX.Name = "colSiteLocationX";
            this.colSiteLocationX.OptionsColumn.AllowEdit = false;
            this.colSiteLocationX.OptionsColumn.AllowFocus = false;
            this.colSiteLocationX.OptionsColumn.ReadOnly = true;
            this.colSiteLocationX.Width = 81;
            // 
            // colSiteLocationY
            // 
            this.colSiteLocationY.Caption = "Site Longitude";
            this.colSiteLocationY.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colSiteLocationY.FieldName = "SiteLocationY";
            this.colSiteLocationY.Name = "colSiteLocationY";
            this.colSiteLocationY.OptionsColumn.AllowEdit = false;
            this.colSiteLocationY.OptionsColumn.AllowFocus = false;
            this.colSiteLocationY.OptionsColumn.ReadOnly = true;
            this.colSiteLocationY.Width = 89;
            // 
            // colLinkedDocumentCount
            // 
            this.colLinkedDocumentCount.Caption = "Linked Documents";
            this.colLinkedDocumentCount.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocumentsVisit;
            this.colLinkedDocumentCount.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount.Name = "colLinkedDocumentCount";
            this.colLinkedDocumentCount.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinkedDocumentCount", "{0:n}")});
            this.colLinkedDocumentCount.Width = 107;
            // 
            // colLinkedJobCount
            // 
            this.colLinkedJobCount.Caption = "Linked Jobs Total";
            this.colLinkedJobCount.ColumnEdit = this.repositoryItemHyperLinkEditGrid1;
            this.colLinkedJobCount.FieldName = "LinkedJobCount";
            this.colLinkedJobCount.Name = "colLinkedJobCount";
            this.colLinkedJobCount.OptionsColumn.ReadOnly = true;
            this.colLinkedJobCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinkedJobCount", "{0:n}")});
            this.colLinkedJobCount.Width = 103;
            // 
            // colLinkedOutstandingJobCount
            // 
            this.colLinkedOutstandingJobCount.Caption = "Linked Jobs Outstanding";
            this.colLinkedOutstandingJobCount.ColumnEdit = this.repositoryItemHyperLinkEditGrid1;
            this.colLinkedOutstandingJobCount.FieldName = "LinkedOutstandingJobCount";
            this.colLinkedOutstandingJobCount.Name = "colLinkedOutstandingJobCount";
            this.colLinkedOutstandingJobCount.OptionsColumn.ReadOnly = true;
            this.colLinkedOutstandingJobCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinkedOutstandingJobCount", "{0:n}")});
            this.colLinkedOutstandingJobCount.Width = 138;
            // 
            // colLinkedCompletedJobCount
            // 
            this.colLinkedCompletedJobCount.Caption = "Linked Jobs Completed";
            this.colLinkedCompletedJobCount.ColumnEdit = this.repositoryItemHyperLinkEditGrid1;
            this.colLinkedCompletedJobCount.FieldName = "LinkedCompletedJobCount";
            this.colLinkedCompletedJobCount.Name = "colLinkedCompletedJobCount";
            this.colLinkedCompletedJobCount.OptionsColumn.ReadOnly = true;
            this.colLinkedCompletedJobCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinkedCompletedJobCount", "{0:n}")});
            this.colLinkedCompletedJobCount.Width = 130;
            // 
            // colCreatedByStaffName
            // 
            this.colCreatedByStaffName.Caption = "Created By";
            this.colCreatedByStaffName.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName.Name = "colCreatedByStaffName";
            this.colCreatedByStaffName.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName.Width = 122;
            // 
            // colSiteAddress
            // 
            this.colSiteAddress.Caption = "Site Address";
            this.colSiteAddress.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colSiteAddress.FieldName = "SiteAddress";
            this.colSiteAddress.Name = "colSiteAddress";
            this.colSiteAddress.OptionsColumn.ReadOnly = true;
            this.colSiteAddress.Width = 125;
            // 
            // colVisitStatus
            // 
            this.colVisitStatus.Caption = "System Status";
            this.colVisitStatus.FieldName = "VisitStatus";
            this.colVisitStatus.Name = "colVisitStatus";
            this.colVisitStatus.OptionsColumn.AllowEdit = false;
            this.colVisitStatus.OptionsColumn.AllowFocus = false;
            this.colVisitStatus.OptionsColumn.ReadOnly = true;
            this.colVisitStatus.Visible = true;
            this.colVisitStatus.VisibleIndex = 7;
            this.colVisitStatus.Width = 130;
            // 
            // colTimeliness
            // 
            this.colTimeliness.Caption = "Visit Status";
            this.colTimeliness.FieldName = "Timeliness";
            this.colTimeliness.Name = "colTimeliness";
            this.colTimeliness.OptionsColumn.AllowEdit = false;
            this.colTimeliness.OptionsColumn.AllowFocus = false;
            this.colTimeliness.OptionsColumn.ReadOnly = true;
            this.colTimeliness.Visible = true;
            this.colTimeliness.VisibleIndex = 8;
            this.colTimeliness.Width = 89;
            // 
            // colWorkNumber
            // 
            this.colWorkNumber.Caption = "Work Number";
            this.colWorkNumber.FieldName = "WorkNumber";
            this.colWorkNumber.Name = "colWorkNumber";
            this.colWorkNumber.OptionsColumn.AllowEdit = false;
            this.colWorkNumber.OptionsColumn.AllowFocus = false;
            this.colWorkNumber.OptionsColumn.ReadOnly = true;
            this.colWorkNumber.Width = 86;
            // 
            // colVisitCategory
            // 
            this.colVisitCategory.Caption = "Visit Category";
            this.colVisitCategory.FieldName = "VisitCategory";
            this.colVisitCategory.Name = "colVisitCategory";
            this.colVisitCategory.OptionsColumn.AllowEdit = false;
            this.colVisitCategory.OptionsColumn.AllowFocus = false;
            this.colVisitCategory.OptionsColumn.ReadOnly = true;
            this.colVisitCategory.Width = 130;
            // 
            // colVisitCategoryID
            // 
            this.colVisitCategoryID.Caption = "Visit Category ID";
            this.colVisitCategoryID.FieldName = "VisitCategoryID";
            this.colVisitCategoryID.Name = "colVisitCategoryID";
            this.colVisitCategoryID.OptionsColumn.AllowEdit = false;
            this.colVisitCategoryID.OptionsColumn.AllowFocus = false;
            this.colVisitCategoryID.OptionsColumn.ReadOnly = true;
            this.colVisitCategoryID.Width = 102;
            // 
            // colLinkedVisitCount
            // 
            this.colLinkedVisitCount.Caption = "Linked Visit Count";
            this.colLinkedVisitCount.ColumnEdit = this.repositoryItemHyperLinkEditLinkedVisits;
            this.colLinkedVisitCount.FieldName = "LinkedVisitCount";
            this.colLinkedVisitCount.Name = "colLinkedVisitCount";
            this.colLinkedVisitCount.OptionsColumn.ReadOnly = true;
            this.colLinkedVisitCount.Width = 105;
            // 
            // colLabourCount
            // 
            this.colLabourCount.Caption = "Labour Count";
            this.colLabourCount.ColumnEdit = this.repositoryItemHyperLinkEditLabour;
            this.colLabourCount.FieldName = "LabourCount";
            this.colLabourCount.Name = "colLabourCount";
            this.colLabourCount.OptionsColumn.ReadOnly = true;
            this.colLabourCount.Width = 86;
            // 
            // colIssueFound
            // 
            this.colIssueFound.Caption = "Issue Found";
            this.colIssueFound.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIssueFound.FieldName = "IssueFound";
            this.colIssueFound.Name = "colIssueFound";
            this.colIssueFound.OptionsColumn.AllowEdit = false;
            this.colIssueFound.OptionsColumn.AllowFocus = false;
            this.colIssueFound.OptionsColumn.ReadOnly = true;
            this.colIssueFound.Visible = true;
            this.colIssueFound.VisibleIndex = 17;
            this.colIssueFound.Width = 80;
            // 
            // colIssueType
            // 
            this.colIssueType.Caption = "Issue Type";
            this.colIssueType.FieldName = "IssueType";
            this.colIssueType.Name = "colIssueType";
            this.colIssueType.OptionsColumn.AllowEdit = false;
            this.colIssueType.OptionsColumn.AllowFocus = false;
            this.colIssueType.OptionsColumn.ReadOnly = true;
            // 
            // colIssueTypeID
            // 
            this.colIssueTypeID.Caption = "Issue Type ID";
            this.colIssueTypeID.FieldName = "IssueTypeID";
            this.colIssueTypeID.Name = "colIssueTypeID";
            this.colIssueTypeID.OptionsColumn.AllowEdit = false;
            this.colIssueTypeID.OptionsColumn.AllowFocus = false;
            this.colIssueTypeID.OptionsColumn.ReadOnly = true;
            this.colIssueTypeID.Width = 88;
            // 
            // colIssueRemarks
            // 
            this.colIssueRemarks.Caption = "Issue Remarks";
            this.colIssueRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colIssueRemarks.FieldName = "IssueRemarks";
            this.colIssueRemarks.Name = "colIssueRemarks";
            this.colIssueRemarks.OptionsColumn.ReadOnly = true;
            this.colIssueRemarks.Width = 91;
            // 
            // colClientSignaturePath
            // 
            this.colClientSignaturePath.Caption = "Client Signature";
            this.colClientSignaturePath.ColumnEdit = this.repositoryItemHyperLinkEditClientSignature;
            this.colClientSignaturePath.FieldName = "ClientSignaturePath";
            this.colClientSignaturePath.Name = "colClientSignaturePath";
            this.colClientSignaturePath.OptionsColumn.ReadOnly = true;
            this.colClientSignaturePath.Width = 97;
            // 
            // colImagesFolderOM
            // 
            this.colImagesFolderOM.Caption = "Client Images Folder";
            this.colImagesFolderOM.FieldName = "ImagesFolderOM";
            this.colImagesFolderOM.Name = "colImagesFolderOM";
            this.colImagesFolderOM.OptionsColumn.AllowEdit = false;
            this.colImagesFolderOM.OptionsColumn.AllowFocus = false;
            this.colImagesFolderOM.OptionsColumn.ReadOnly = true;
            this.colImagesFolderOM.Width = 119;
            // 
            // colRework
            // 
            this.colRework.Caption = "Rework";
            this.colRework.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colRework.FieldName = "Rework";
            this.colRework.Name = "colRework";
            this.colRework.OptionsColumn.AllowEdit = false;
            this.colRework.OptionsColumn.AllowFocus = false;
            this.colRework.OptionsColumn.ReadOnly = true;
            this.colRework.Visible = true;
            this.colRework.VisibleIndex = 16;
            this.colRework.Width = 57;
            // 
            // colExtraWorkComments
            // 
            this.colExtraWorkComments.Caption = "Extra Work Remarks";
            this.colExtraWorkComments.FieldName = "ExtraWorkComments";
            this.colExtraWorkComments.Name = "colExtraWorkComments";
            this.colExtraWorkComments.OptionsColumn.AllowEdit = false;
            this.colExtraWorkComments.OptionsColumn.AllowFocus = false;
            this.colExtraWorkComments.OptionsColumn.ReadOnly = true;
            this.colExtraWorkComments.Width = 119;
            // 
            // colExtraWorkRequired
            // 
            this.colExtraWorkRequired.Caption = "Extra Work Required";
            this.colExtraWorkRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colExtraWorkRequired.FieldName = "ExtraWorkRequired";
            this.colExtraWorkRequired.Name = "colExtraWorkRequired";
            this.colExtraWorkRequired.OptionsColumn.AllowEdit = false;
            this.colExtraWorkRequired.OptionsColumn.AllowFocus = false;
            this.colExtraWorkRequired.OptionsColumn.ReadOnly = true;
            this.colExtraWorkRequired.Width = 121;
            // 
            // colExtraWorkType
            // 
            this.colExtraWorkType.Caption = "Extra Work Type";
            this.colExtraWorkType.FieldName = "ExtraWorkType";
            this.colExtraWorkType.Name = "colExtraWorkType";
            this.colExtraWorkType.OptionsColumn.AllowEdit = false;
            this.colExtraWorkType.OptionsColumn.AllowFocus = false;
            this.colExtraWorkType.OptionsColumn.ReadOnly = true;
            this.colExtraWorkType.Width = 102;
            // 
            // colAccidentOnSite
            // 
            this.colAccidentOnSite.Caption = "Accident on Site";
            this.colAccidentOnSite.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colAccidentOnSite.FieldName = "AccidentOnSite";
            this.colAccidentOnSite.Name = "colAccidentOnSite";
            this.colAccidentOnSite.OptionsColumn.AllowEdit = false;
            this.colAccidentOnSite.OptionsColumn.AllowFocus = false;
            this.colAccidentOnSite.OptionsColumn.ReadOnly = true;
            this.colAccidentOnSite.Visible = true;
            this.colAccidentOnSite.VisibleIndex = 15;
            this.colAccidentOnSite.Width = 98;
            // 
            // colAccidentComment
            // 
            this.colAccidentComment.Caption = "Accident Comments";
            this.colAccidentComment.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colAccidentComment.FieldName = "AccidentComment";
            this.colAccidentComment.Name = "colAccidentComment";
            this.colAccidentComment.OptionsColumn.ReadOnly = true;
            this.colAccidentComment.Width = 115;
            // 
            // colContractorNames
            // 
            this.colContractorNames.Caption = "Team";
            this.colContractorNames.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colContractorNames.FieldName = "ContractorNames";
            this.colContractorNames.Name = "colContractorNames";
            this.colContractorNames.OptionsColumn.ReadOnly = true;
            this.colContractorNames.Visible = true;
            this.colContractorNames.VisibleIndex = 5;
            this.colContractorNames.Width = 160;
            // 
            // Alert2
            // 
            this.Alert2.Caption = "gridColumn16";
            this.Alert2.ColumnEdit = this.repositoryItemPictureEdit2;
            this.Alert2.CustomizationCaption = "Alert";
            this.Alert2.FieldName = "Alert2";
            this.Alert2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.Alert2.ImageOptions.Alignment = System.Drawing.StringAlignment.Center;
            this.Alert2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("Alert2.ImageOptions.Image")));
            this.Alert2.Name = "Alert2";
            this.Alert2.OptionsColumn.AllowEdit = false;
            this.Alert2.OptionsColumn.AllowFocus = false;
            this.Alert2.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.Alert2.OptionsColumn.AllowSize = false;
            this.Alert2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.Alert2.OptionsColumn.FixedWidth = true;
            this.Alert2.OptionsColumn.ReadOnly = true;
            this.Alert2.OptionsColumn.ShowCaption = false;
            this.Alert2.OptionsColumn.ShowInCustomizationForm = false;
            this.Alert2.OptionsColumn.ShowInExpressionEditor = false;
            this.Alert2.OptionsFilter.AllowAutoFilter = false;
            this.Alert2.OptionsFilter.AllowFilter = false;
            this.Alert2.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Alert2.Visible = true;
            this.Alert2.VisibleIndex = 0;
            this.Alert2.Width = 24;
            // 
            // colSuspendedJobCount
            // 
            this.colSuspendedJobCount.Caption = "Suspended Jobs";
            this.colSuspendedJobCount.ColumnEdit = this.repositoryItemHyperLinkEditGrid1;
            this.colSuspendedJobCount.FieldName = "SuspendedJobCount";
            this.colSuspendedJobCount.Name = "colSuspendedJobCount";
            this.colSuspendedJobCount.OptionsColumn.ReadOnly = true;
            this.colSuspendedJobCount.Width = 97;
            // 
            // colLinkedPictureCount
            // 
            this.colLinkedPictureCount.Caption = "Linked Pictures";
            this.colLinkedPictureCount.ColumnEdit = this.repositoryItemHyperLinkEditGrid1;
            this.colLinkedPictureCount.FieldName = "LinkedPictureCount";
            this.colLinkedPictureCount.Name = "colLinkedPictureCount";
            this.colLinkedPictureCount.OptionsColumn.ReadOnly = true;
            this.colLinkedPictureCount.Visible = true;
            this.colLinkedPictureCount.VisibleIndex = 13;
            this.colLinkedPictureCount.Width = 90;
            // 
            // colFriendlyVisitNumber
            // 
            this.colFriendlyVisitNumber.Caption = "Friendly Visit #";
            this.colFriendlyVisitNumber.FieldName = "FriendlyVisitNumber";
            this.colFriendlyVisitNumber.Name = "colFriendlyVisitNumber";
            this.colFriendlyVisitNumber.OptionsColumn.AllowEdit = false;
            this.colFriendlyVisitNumber.OptionsColumn.AllowFocus = false;
            this.colFriendlyVisitNumber.OptionsColumn.ReadOnly = true;
            this.colFriendlyVisitNumber.Width = 90;
            // 
            // colVisitType
            // 
            this.colVisitType.Caption = "Visit Type";
            this.colVisitType.FieldName = "VisitType";
            this.colVisitType.Name = "colVisitType";
            this.colVisitType.OptionsColumn.AllowEdit = false;
            this.colVisitType.OptionsColumn.AllowFocus = false;
            this.colVisitType.OptionsColumn.ReadOnly = true;
            this.colVisitType.Width = 99;
            // 
            // colVisitTypeID
            // 
            this.colVisitTypeID.Caption = "Visit Type ID";
            this.colVisitTypeID.FieldName = "VisitTypeID";
            this.colVisitTypeID.Name = "colVisitTypeID";
            this.colVisitTypeID.OptionsColumn.AllowEdit = false;
            this.colVisitTypeID.OptionsColumn.AllowFocus = false;
            this.colVisitTypeID.OptionsColumn.ReadOnly = true;
            this.colVisitTypeID.Width = 79;
            // 
            // colClientPO
            // 
            this.colClientPO.Caption = "Client PO #";
            this.colClientPO.FieldName = "ClientPO";
            this.colClientPO.Name = "colClientPO";
            this.colClientPO.OptionsColumn.AllowEdit = false;
            this.colClientPO.OptionsColumn.AllowFocus = false;
            this.colClientPO.OptionsColumn.ReadOnly = true;
            this.colClientPO.Width = 112;
            // 
            // colClientPOID1
            // 
            this.colClientPOID1.Caption = "Client PO ID";
            this.colClientPOID1.FieldName = "ClientPOID";
            this.colClientPOID1.Name = "colClientPOID1";
            this.colClientPOID1.OptionsColumn.AllowEdit = false;
            this.colClientPOID1.OptionsColumn.AllowFocus = false;
            this.colClientPOID1.OptionsColumn.ReadOnly = true;
            this.colClientPOID1.Width = 77;
            // 
            // colPDAVersion
            // 
            this.colPDAVersion.Caption = "App Version";
            this.colPDAVersion.FieldName = "PDAVersion";
            this.colPDAVersion.Name = "colPDAVersion";
            this.colPDAVersion.OptionsColumn.AllowEdit = false;
            this.colPDAVersion.OptionsColumn.AllowFocus = false;
            this.colPDAVersion.OptionsColumn.ReadOnly = true;
            this.colPDAVersion.Width = 76;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Visible = true;
            this.colSiteCode.VisibleIndex = 3;
            // 
            // colSellCalculationLevel
            // 
            this.colSellCalculationLevel.Caption = "Sell Calculation ID";
            this.colSellCalculationLevel.FieldName = "SellCalculationLevel";
            this.colSellCalculationLevel.Name = "colSellCalculationLevel";
            this.colSellCalculationLevel.OptionsColumn.AllowEdit = false;
            this.colSellCalculationLevel.OptionsColumn.AllowFocus = false;
            this.colSellCalculationLevel.OptionsColumn.ReadOnly = true;
            this.colSellCalculationLevel.Width = 104;
            // 
            // colSellCalculationLevelDescription
            // 
            this.colSellCalculationLevelDescription.Caption = "Sell Calculation";
            this.colSellCalculationLevelDescription.FieldName = "SellCalculationLevelDescription";
            this.colSellCalculationLevelDescription.Name = "colSellCalculationLevelDescription";
            this.colSellCalculationLevelDescription.OptionsColumn.AllowEdit = false;
            this.colSellCalculationLevelDescription.OptionsColumn.AllowFocus = false;
            this.colSellCalculationLevelDescription.OptionsColumn.ReadOnly = true;
            this.colSellCalculationLevelDescription.Width = 100;
            // 
            // colSelected4
            // 
            this.colSelected4.Caption = "Selected";
            this.colSelected4.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSelected4.FieldName = "Selected";
            this.colSelected4.Name = "colSelected4";
            this.colSelected4.Width = 60;
            // 
            // colSelfBillingInvoiceID1
            // 
            this.colSelfBillingInvoiceID1.Caption = "Self Billing Invoice";
            this.colSelfBillingInvoiceID1.ColumnEdit = this.repositoryItemHyperLinkEditSelfBillingInvoice;
            this.colSelfBillingInvoiceID1.FieldName = "SelfBillingInvoice";
            this.colSelfBillingInvoiceID1.Name = "colSelfBillingInvoiceID1";
            this.colSelfBillingInvoiceID1.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceID1.Width = 118;
            // 
            // colClientInvoiceID1
            // 
            this.colClientInvoiceID1.Caption = "Client Invoice ID";
            this.colClientInvoiceID1.ColumnEdit = this.repositoryItemHyperLinkEditClientInvoiceID;
            this.colClientInvoiceID1.FieldName = "ClientInvoiceID";
            this.colClientInvoiceID1.Name = "colClientInvoiceID1";
            this.colClientInvoiceID1.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceID1.Width = 98;
            // 
            // colCMName
            // 
            this.colCMName.Caption = "Contract Manager";
            this.colCMName.FieldName = "CMName";
            this.colCMName.Name = "colCMName";
            this.colCMName.OptionsColumn.AllowEdit = false;
            this.colCMName.OptionsColumn.AllowFocus = false;
            this.colCMName.OptionsColumn.ReadOnly = true;
            this.colCMName.Visible = true;
            this.colCMName.VisibleIndex = 6;
            this.colCMName.Width = 125;
            // 
            // colDaysAllowed
            // 
            this.colDaysAllowed.Caption = "Days Allowed";
            this.colDaysAllowed.FieldName = "DaysAllowed";
            this.colDaysAllowed.Name = "colDaysAllowed";
            this.colDaysAllowed.OptionsColumn.AllowEdit = false;
            this.colDaysAllowed.OptionsColumn.AllowFocus = false;
            this.colDaysAllowed.OptionsColumn.ReadOnly = true;
            this.colDaysAllowed.Visible = true;
            this.colDaysAllowed.VisibleIndex = 14;
            this.colDaysAllowed.Width = 83;
            // 
            // colManagerName
            // 
            this.colManagerName.Caption = "Signed By Manager";
            this.colManagerName.FieldName = "ManagerName";
            this.colManagerName.Name = "colManagerName";
            this.colManagerName.OptionsColumn.AllowEdit = false;
            this.colManagerName.OptionsColumn.AllowFocus = false;
            this.colManagerName.OptionsColumn.ReadOnly = true;
            this.colManagerName.Width = 111;
            // 
            // colManagerNotes
            // 
            this.colManagerNotes.Caption = "Signed By Notes";
            this.colManagerNotes.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colManagerNotes.FieldName = "ManagerNotes";
            this.colManagerNotes.Name = "colManagerNotes";
            this.colManagerNotes.OptionsColumn.ReadOnly = true;
            this.colManagerNotes.Width = 97;
            // 
            // colNoOnetoSign
            // 
            this.colNoOnetoSign.Caption = "No One To Sign ";
            this.colNoOnetoSign.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNoOnetoSign.FieldName = "NoOnetoSign";
            this.colNoOnetoSign.Name = "colNoOnetoSign";
            this.colNoOnetoSign.OptionsColumn.AllowEdit = false;
            this.colNoOnetoSign.OptionsColumn.AllowFocus = false;
            this.colNoOnetoSign.OptionsColumn.ReadOnly = true;
            this.colNoOnetoSign.Width = 96;
            // 
            // colSiteCategory
            // 
            this.colSiteCategory.Caption = "Site Category";
            this.colSiteCategory.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colSiteCategory.FieldName = "SiteCategory";
            this.colSiteCategory.Name = "colSiteCategory";
            this.colSiteCategory.OptionsColumn.ReadOnly = true;
            this.colSiteCategory.Width = 124;
            // 
            // colCreatedFromVisitTemplate
            // 
            this.colCreatedFromVisitTemplate.Caption = "Created From Visit Template";
            this.colCreatedFromVisitTemplate.FieldName = "CreatedFromVisitTemplate";
            this.colCreatedFromVisitTemplate.Name = "colCreatedFromVisitTemplate";
            this.colCreatedFromVisitTemplate.OptionsColumn.AllowEdit = false;
            this.colCreatedFromVisitTemplate.OptionsColumn.AllowFocus = false;
            this.colCreatedFromVisitTemplate.OptionsColumn.ReadOnly = true;
            this.colCreatedFromVisitTemplate.Width = 197;
            // 
            // colCreatedFromVisitTemplateID
            // 
            this.colCreatedFromVisitTemplateID.Caption = "Created From Visit Template ID";
            this.colCreatedFromVisitTemplateID.FieldName = "CreatedFromVisitTemplateID";
            this.colCreatedFromVisitTemplateID.Name = "colCreatedFromVisitTemplateID";
            this.colCreatedFromVisitTemplateID.OptionsColumn.AllowEdit = false;
            this.colCreatedFromVisitTemplateID.OptionsColumn.AllowFocus = false;
            this.colCreatedFromVisitTemplateID.OptionsColumn.ReadOnly = true;
            this.colCreatedFromVisitTemplateID.Width = 168;
            // 
            // colExternalCustomerStatus
            // 
            this.colExternalCustomerStatus.Caption = "External Customer Status";
            this.colExternalCustomerStatus.FieldName = "ExternalCustomerStatus";
            this.colExternalCustomerStatus.Name = "colExternalCustomerStatus";
            this.colExternalCustomerStatus.OptionsColumn.AllowEdit = false;
            this.colExternalCustomerStatus.OptionsColumn.AllowFocus = false;
            this.colExternalCustomerStatus.OptionsColumn.ReadOnly = true;
            this.colExternalCustomerStatus.Width = 142;
            // 
            // colCompletionSheetNumber
            // 
            this.colCompletionSheetNumber.Caption = "Completion Sheet #";
            this.colCompletionSheetNumber.FieldName = "CompletionSheetNumber";
            this.colCompletionSheetNumber.Name = "colCompletionSheetNumber";
            this.colCompletionSheetNumber.OptionsColumn.AllowEdit = false;
            this.colCompletionSheetNumber.OptionsColumn.AllowFocus = false;
            this.colCompletionSheetNumber.OptionsColumn.ReadOnly = true;
            this.colCompletionSheetNumber.Width = 114;
            // 
            // colStatusIssueID
            // 
            this.colStatusIssueID.Caption = "Status Issue ID";
            this.colStatusIssueID.FieldName = "StatusIssueID";
            this.colStatusIssueID.Name = "colStatusIssueID";
            this.colStatusIssueID.OptionsColumn.AllowEdit = false;
            this.colStatusIssueID.OptionsColumn.AllowFocus = false;
            this.colStatusIssueID.OptionsColumn.ReadOnly = true;
            this.colStatusIssueID.Width = 93;
            // 
            // colStatusIssue
            // 
            this.colStatusIssue.Caption = "Status Issue";
            this.colStatusIssue.FieldName = "StatusIssue";
            this.colStatusIssue.Name = "colStatusIssue";
            this.colStatusIssue.OptionsColumn.AllowEdit = false;
            this.colStatusIssue.OptionsColumn.AllowFocus = false;
            this.colStatusIssue.OptionsColumn.ReadOnly = true;
            this.colStatusIssue.Width = 110;
            // 
            // colDaysSeparation
            // 
            this.colDaysSeparation.Caption = "Days Separation";
            this.colDaysSeparation.FieldName = "DaysSeparation";
            this.colDaysSeparation.Name = "colDaysSeparation";
            this.colDaysSeparation.OptionsColumn.AllowEdit = false;
            this.colDaysSeparation.OptionsColumn.AllowFocus = false;
            this.colDaysSeparation.OptionsColumn.ReadOnly = true;
            this.colDaysSeparation.Width = 98;
            // 
            // colSiteContractDaysSeparationPercent
            // 
            this.colSiteContractDaysSeparationPercent.Caption = "Site Contract Days Separation %";
            this.colSiteContractDaysSeparationPercent.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colSiteContractDaysSeparationPercent.FieldName = "SiteContractDaysSeparationPercent";
            this.colSiteContractDaysSeparationPercent.Name = "colSiteContractDaysSeparationPercent";
            this.colSiteContractDaysSeparationPercent.OptionsColumn.AllowEdit = false;
            this.colSiteContractDaysSeparationPercent.OptionsColumn.AllowFocus = false;
            this.colSiteContractDaysSeparationPercent.OptionsColumn.ReadOnly = true;
            this.colSiteContractDaysSeparationPercent.Width = 178;
            // 
            // colSiteAddressLine1
            // 
            this.colSiteAddressLine1.Caption = "Address Line 1";
            this.colSiteAddressLine1.FieldName = "SiteAddressLine1";
            this.colSiteAddressLine1.Name = "colSiteAddressLine1";
            this.colSiteAddressLine1.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine1.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine1.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine1.Width = 89;
            // 
            // colSiteAddressLine2
            // 
            this.colSiteAddressLine2.Caption = "Address Line 2";
            this.colSiteAddressLine2.FieldName = "SiteAddressLine2";
            this.colSiteAddressLine2.Name = "colSiteAddressLine2";
            this.colSiteAddressLine2.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine2.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine2.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine2.Width = 89;
            // 
            // colSiteAddressLine3
            // 
            this.colSiteAddressLine3.Caption = "Address Line 3";
            this.colSiteAddressLine3.FieldName = "SiteAddressLine3";
            this.colSiteAddressLine3.Name = "colSiteAddressLine3";
            this.colSiteAddressLine3.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine3.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine3.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine3.Width = 89;
            // 
            // colSiteAddressLine4
            // 
            this.colSiteAddressLine4.Caption = "Address Line 4";
            this.colSiteAddressLine4.FieldName = "SiteAddressLine4";
            this.colSiteAddressLine4.Name = "colSiteAddressLine4";
            this.colSiteAddressLine4.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine4.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine4.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine4.Width = 89;
            // 
            // colSiteAddressLine5
            // 
            this.colSiteAddressLine5.Caption = "Address Line 5";
            this.colSiteAddressLine5.FieldName = "SiteAddressLine5";
            this.colSiteAddressLine5.Name = "colSiteAddressLine5";
            this.colSiteAddressLine5.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine5.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine5.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine5.Width = 89;
            // 
            // colDoNotPayContractor1
            // 
            this.colDoNotPayContractor1.Caption = "Do Not Pay Team";
            this.colDoNotPayContractor1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotPayContractor1.FieldName = "DoNotPayContractor";
            this.colDoNotPayContractor1.Name = "colDoNotPayContractor1";
            this.colDoNotPayContractor1.OptionsColumn.AllowEdit = false;
            this.colDoNotPayContractor1.OptionsColumn.AllowFocus = false;
            this.colDoNotPayContractor1.OptionsColumn.ReadOnly = true;
            this.colDoNotPayContractor1.Width = 102;
            // 
            // colDoNotInvoiceClient1
            // 
            this.colDoNotInvoiceClient1.Caption = "Do Not Invoice Client";
            this.colDoNotInvoiceClient1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotInvoiceClient1.FieldName = "DoNotInvoiceClient";
            this.colDoNotInvoiceClient1.Name = "colDoNotInvoiceClient1";
            this.colDoNotInvoiceClient1.OptionsColumn.AllowEdit = false;
            this.colDoNotInvoiceClient1.OptionsColumn.AllowFocus = false;
            this.colDoNotInvoiceClient1.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClient1.Width = 120;
            // 
            // colKAMName
            // 
            this.colKAMName.Caption = "KAM";
            this.colKAMName.FieldName = "KAMName";
            this.colKAMName.Name = "colKAMName";
            this.colKAMName.OptionsColumn.AllowEdit = false;
            this.colKAMName.OptionsColumn.AllowFocus = false;
            this.colKAMName.OptionsColumn.ReadOnly = true;
            this.colKAMName.Width = 122;
            // 
            // colSiteInstructions
            // 
            this.colSiteInstructions.Caption = "Site Instructions";
            this.colSiteInstructions.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colSiteInstructions.FieldName = "SiteInstructions";
            this.colSiteInstructions.Name = "colSiteInstructions";
            this.colSiteInstructions.OptionsColumn.ReadOnly = true;
            this.colSiteInstructions.Width = 97;
            // 
            // colTenderID1
            // 
            this.colTenderID1.FieldName = "TenderID";
            this.colTenderID1.Name = "colTenderID1";
            this.colTenderID1.OptionsColumn.AllowEdit = false;
            this.colTenderID1.OptionsColumn.AllowFocus = false;
            this.colTenderID1.OptionsColumn.ReadOnly = true;
            // 
            // colExtraCostsFinalised
            // 
            this.colExtraCostsFinalised.Caption = "Extra Costs Finalised";
            this.colExtraCostsFinalised.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colExtraCostsFinalised.FieldName = "ExtraCostsFinalised";
            this.colExtraCostsFinalised.Name = "colExtraCostsFinalised";
            this.colExtraCostsFinalised.OptionsColumn.AllowEdit = false;
            this.colExtraCostsFinalised.OptionsColumn.AllowFocus = false;
            this.colExtraCostsFinalised.OptionsColumn.ReadOnly = true;
            this.colExtraCostsFinalised.Visible = true;
            this.colExtraCostsFinalised.VisibleIndex = 18;
            this.colExtraCostsFinalised.Width = 119;
            // 
            // toolTipController1
            // 
            this.toolTipController1.CloseOnClick = DevExpress.Utils.DefaultBoolean.True;
            this.toolTipController1.ToolTipType = DevExpress.Utils.ToolTipType.SuperTip;
            this.toolTipController1.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.toolTipController1_GetActiveObjectInfo);
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(23, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlJobSubTypeFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlJobTypeFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlCMFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlDateRangeFromToday);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlKAMFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlLabourFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlShowTabPages);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlVisitSystemStatusFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlDateRange);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControlTender);
            this.splitContainerControl1.Panel1.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl1.Panel1.Text = "Jobs";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1336, 654);
            this.splitContainerControl1.SplitterPosition = 209;
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.SplitGroupPanelCollapsed += new DevExpress.XtraEditors.SplitGroupPanelCollapsedEventHandler(this.splitContainerControl1_SplitGroupPanelCollapsed);
            // 
            // popupContainerControlJobSubTypeFilter
            // 
            this.popupContainerControlJobSubTypeFilter.Controls.Add(this.btnJobSubTypeFilterOK);
            this.popupContainerControlJobSubTypeFilter.Controls.Add(this.gridControl11);
            this.popupContainerControlJobSubTypeFilter.Location = new System.Drawing.Point(1106, 155);
            this.popupContainerControlJobSubTypeFilter.Name = "popupContainerControlJobSubTypeFilter";
            this.popupContainerControlJobSubTypeFilter.Size = new System.Drawing.Size(140, 126);
            this.popupContainerControlJobSubTypeFilter.TabIndex = 25;
            // 
            // btnJobSubTypeFilterOK
            // 
            this.btnJobSubTypeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnJobSubTypeFilterOK.Location = new System.Drawing.Point(3, 103);
            this.btnJobSubTypeFilterOK.Name = "btnJobSubTypeFilterOK";
            this.btnJobSubTypeFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnJobSubTypeFilterOK.TabIndex = 12;
            this.btnJobSubTypeFilterOK.Text = "OK";
            this.btnJobSubTypeFilterOK.Click += new System.EventHandler(this.btnJobSubTypeFilterOK_Click);
            // 
            // gridControl11
            // 
            this.gridControl11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl11.DataSource = this.sp06494OMJobSubTypeFilterBindingSource;
            this.gridControl11.Location = new System.Drawing.Point(3, 3);
            this.gridControl11.MainView = this.gridView11;
            this.gridControl11.MenuManager = this.barManager1;
            this.gridControl11.Name = "gridControl11";
            this.gridControl11.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit16});
            this.gridControl11.Size = new System.Drawing.Size(134, 98);
            this.gridControl11.TabIndex = 4;
            this.gridControl11.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView11});
            // 
            // sp06494OMJobSubTypeFilterBindingSource
            // 
            this.sp06494OMJobSubTypeFilterBindingSource.DataMember = "sp06494_OM_Job_Sub_Type_Filter";
            this.sp06494OMJobSubTypeFilterBindingSource.DataSource = this.dataSet_OM_Tender;
            // 
            // gridView11
            // 
            this.gridView11.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID3,
            this.colDescription5,
            this.colRecordOrder1,
            this.colJobTypeID,
            this.colJobTypeDescription,
            this.colDisabled2});
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colDisabled2;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 1;
            this.gridView11.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView11.GridControl = this.gridControl11;
            this.gridView11.GroupCount = 1;
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView11.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView11.OptionsFind.AlwaysVisible = true;
            this.gridView11.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView11.OptionsLayout.StoreAppearance = true;
            this.gridView11.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView11.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView11.OptionsView.ColumnAutoWidth = false;
            this.gridView11.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            this.gridView11.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView11.OptionsView.ShowIndicator = false;
            this.gridView11.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID3
            // 
            this.colID3.Caption = "Job Sub-Type ID";
            this.colID3.FieldName = "ID";
            this.colID3.Name = "colID3";
            this.colID3.OptionsColumn.AllowEdit = false;
            this.colID3.OptionsColumn.AllowFocus = false;
            this.colID3.OptionsColumn.ReadOnly = true;
            this.colID3.Width = 101;
            // 
            // colDescription5
            // 
            this.colDescription5.Caption = "Job Sub-Type";
            this.colDescription5.FieldName = "Description";
            this.colDescription5.Name = "colDescription5";
            this.colDescription5.OptionsColumn.AllowEdit = false;
            this.colDescription5.OptionsColumn.AllowFocus = false;
            this.colDescription5.OptionsColumn.ReadOnly = true;
            this.colDescription5.Visible = true;
            this.colDescription5.VisibleIndex = 0;
            this.colDescription5.Width = 200;
            // 
            // colRecordOrder1
            // 
            this.colRecordOrder1.Caption = "Job Sub-Type Order";
            this.colRecordOrder1.FieldName = "RecordOrder";
            this.colRecordOrder1.Name = "colRecordOrder1";
            this.colRecordOrder1.OptionsColumn.AllowEdit = false;
            this.colRecordOrder1.OptionsColumn.AllowFocus = false;
            this.colRecordOrder1.OptionsColumn.ReadOnly = true;
            this.colRecordOrder1.Width = 131;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            this.colJobTypeID.Width = 79;
            // 
            // colJobTypeDescription
            // 
            this.colJobTypeDescription.Caption = "Job Type";
            this.colJobTypeDescription.FieldName = "JobTypeDescription";
            this.colJobTypeDescription.Name = "colJobTypeDescription";
            this.colJobTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription.Visible = true;
            this.colJobTypeDescription.VisibleIndex = 1;
            this.colJobTypeDescription.Width = 200;
            // 
            // popupContainerControlJobTypeFilter
            // 
            this.popupContainerControlJobTypeFilter.Controls.Add(this.btnJobTypeFilterOK);
            this.popupContainerControlJobTypeFilter.Controls.Add(this.gridControl10);
            this.popupContainerControlJobTypeFilter.Location = new System.Drawing.Point(961, 155);
            this.popupContainerControlJobTypeFilter.Name = "popupContainerControlJobTypeFilter";
            this.popupContainerControlJobTypeFilter.Size = new System.Drawing.Size(139, 126);
            this.popupContainerControlJobTypeFilter.TabIndex = 24;
            // 
            // btnJobTypeFilterOK
            // 
            this.btnJobTypeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnJobTypeFilterOK.Location = new System.Drawing.Point(3, 103);
            this.btnJobTypeFilterOK.Name = "btnJobTypeFilterOK";
            this.btnJobTypeFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnJobTypeFilterOK.TabIndex = 12;
            this.btnJobTypeFilterOK.Text = "OK";
            this.btnJobTypeFilterOK.Click += new System.EventHandler(this.btnJobTypeFilterOK_Click);
            // 
            // gridControl10
            // 
            this.gridControl10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl10.DataSource = this.sp06493OMJobTypeFilterBindingSource;
            this.gridControl10.Location = new System.Drawing.Point(3, 3);
            this.gridControl10.MainView = this.gridView10;
            this.gridControl10.MenuManager = this.barManager1;
            this.gridControl10.Name = "gridControl10";
            this.gridControl10.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit8});
            this.gridControl10.Size = new System.Drawing.Size(133, 98);
            this.gridControl10.TabIndex = 4;
            this.gridControl10.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView10});
            // 
            // sp06493OMJobTypeFilterBindingSource
            // 
            this.sp06493OMJobTypeFilterBindingSource.DataMember = "sp06493_OM_Job_Type_Filter";
            this.sp06493OMJobTypeFilterBindingSource.DataSource = this.dataSet_OM_Tender;
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID2,
            this.colDescription4,
            this.colDisabled});
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colDisabled;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = "1";
            this.gridView10.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView10.GridControl = this.gridControl10;
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView10.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView10.OptionsFind.AlwaysVisible = true;
            this.gridView10.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView10.OptionsLayout.StoreAppearance = true;
            this.gridView10.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView10.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView10.OptionsView.ColumnAutoWidth = false;
            this.gridView10.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            this.gridView10.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView10.OptionsView.ShowIndicator = false;
            this.gridView10.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription4, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID2
            // 
            this.colID2.Caption = "ID";
            this.colID2.FieldName = "ID";
            this.colID2.Name = "colID2";
            this.colID2.OptionsColumn.AllowEdit = false;
            this.colID2.OptionsColumn.AllowFocus = false;
            this.colID2.OptionsColumn.ReadOnly = true;
            this.colID2.Width = 57;
            // 
            // colDescription4
            // 
            this.colDescription4.Caption = "Job Type";
            this.colDescription4.FieldName = "Description";
            this.colDescription4.Name = "colDescription4";
            this.colDescription4.OptionsColumn.AllowEdit = false;
            this.colDescription4.OptionsColumn.AllowFocus = false;
            this.colDescription4.OptionsColumn.ReadOnly = true;
            this.colDescription4.Visible = true;
            this.colDescription4.VisibleIndex = 0;
            this.colDescription4.Width = 200;
            // 
            // popupContainerControlCMFilter
            // 
            this.popupContainerControlCMFilter.Controls.Add(this.btnCMFilterOK);
            this.popupContainerControlCMFilter.Controls.Add(this.gridControl9);
            this.popupContainerControlCMFilter.Location = new System.Drawing.Point(823, 155);
            this.popupContainerControlCMFilter.Name = "popupContainerControlCMFilter";
            this.popupContainerControlCMFilter.Size = new System.Drawing.Size(132, 135);
            this.popupContainerControlCMFilter.TabIndex = 20;
            // 
            // btnCMFilterOK
            // 
            this.btnCMFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCMFilterOK.Location = new System.Drawing.Point(3, 112);
            this.btnCMFilterOK.Name = "btnCMFilterOK";
            this.btnCMFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnCMFilterOK.TabIndex = 18;
            this.btnCMFilterOK.Text = "OK";
            this.btnCMFilterOK.Click += new System.EventHandler(this.btnCMFilterOK_Click);
            // 
            // gridControl9
            // 
            this.gridControl9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl9.DataSource = this.sp06020OMJobManagerCMsFilterBindingSource;
            this.gridControl9.Location = new System.Drawing.Point(3, 3);
            this.gridControl9.MainView = this.gridView9;
            this.gridControl9.MenuManager = this.barManager1;
            this.gridControl9.Name = "gridControl9";
            this.gridControl9.Size = new System.Drawing.Size(126, 107);
            this.gridControl9.TabIndex = 1;
            this.gridControl9.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView9});
            // 
            // sp06020OMJobManagerCMsFilterBindingSource
            // 
            this.sp06020OMJobManagerCMsFilterBindingSource.DataMember = "sp06020_OM_Job_Manager_CMs_Filter";
            this.sp06020OMJobManagerCMsFilterBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // dataSet_GC_Summer_Core
            // 
            this.dataSet_GC_Summer_Core.DataSetName = "DataSet_GC_Summer_Core";
            this.dataSet_GC_Summer_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn218,
            this.gridColumn219,
            this.gridColumn220});
            this.gridView9.GridControl = this.gridControl9;
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView9.OptionsFind.AlwaysVisible = true;
            this.gridView9.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView9.OptionsLayout.StoreAppearance = true;
            this.gridView9.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView9.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView9.OptionsView.ColumnAutoWidth = false;
            this.gridView9.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            this.gridView9.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView9.OptionsView.ShowIndicator = false;
            this.gridView9.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn218, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn218
            // 
            this.gridColumn218.Caption = "Surname";
            this.gridColumn218.FieldName = "Surname";
            this.gridColumn218.Name = "gridColumn218";
            this.gridColumn218.OptionsColumn.AllowEdit = false;
            this.gridColumn218.OptionsColumn.AllowFocus = false;
            this.gridColumn218.OptionsColumn.ReadOnly = true;
            this.gridColumn218.Visible = true;
            this.gridColumn218.VisibleIndex = 0;
            this.gridColumn218.Width = 122;
            // 
            // gridColumn219
            // 
            this.gridColumn219.Caption = "Forename";
            this.gridColumn219.FieldName = "Forename";
            this.gridColumn219.Name = "gridColumn219";
            this.gridColumn219.OptionsColumn.AllowEdit = false;
            this.gridColumn219.OptionsColumn.AllowFocus = false;
            this.gridColumn219.OptionsColumn.ReadOnly = true;
            this.gridColumn219.Visible = true;
            this.gridColumn219.VisibleIndex = 1;
            this.gridColumn219.Width = 82;
            // 
            // gridColumn220
            // 
            this.gridColumn220.Caption = "ID";
            this.gridColumn220.FieldName = "ID";
            this.gridColumn220.Name = "gridColumn220";
            this.gridColumn220.OptionsColumn.AllowEdit = false;
            this.gridColumn220.OptionsColumn.AllowFocus = false;
            this.gridColumn220.OptionsColumn.ReadOnly = true;
            this.gridColumn220.Width = 53;
            // 
            // popupContainerControlDateRangeFromToday
            // 
            this.popupContainerControlDateRangeFromToday.Controls.Add(this.groupControl2);
            this.popupContainerControlDateRangeFromToday.Controls.Add(this.btnDateRangeFromTodayOK);
            this.popupContainerControlDateRangeFromToday.Location = new System.Drawing.Point(467, 274);
            this.popupContainerControlDateRangeFromToday.Name = "popupContainerControlDateRangeFromToday";
            this.popupContainerControlDateRangeFromToday.Size = new System.Drawing.Size(219, 107);
            this.popupContainerControlDateRangeFromToday.TabIndex = 18;
            // 
            // groupControl2
            // 
            this.groupControl2.AllowHtmlText = true;
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.spinEditDaysAfter);
            this.groupControl2.Controls.Add(this.spinEditDaysPrior);
            this.groupControl2.Controls.Add(this.labelControl4);
            this.groupControl2.Controls.Add(this.labelControl5);
            this.groupControl2.Location = new System.Drawing.Point(3, 5);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(213, 76);
            this.groupControl2.TabIndex = 20;
            this.groupControl2.Text = "From <b>Today</b>  [Request Received Date]";
            // 
            // spinEditDaysAfter
            // 
            this.spinEditDaysAfter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.spinEditDaysAfter.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditDaysAfter.Location = new System.Drawing.Point(116, 51);
            this.spinEditDaysAfter.MenuManager = this.barManager1;
            this.spinEditDaysAfter.Name = "spinEditDaysAfter";
            this.spinEditDaysAfter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditDaysAfter.Properties.IsFloatValue = false;
            this.spinEditDaysAfter.Properties.Mask.EditMask = "n0";
            this.spinEditDaysAfter.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditDaysAfter.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.spinEditDaysAfter.Size = new System.Drawing.Size(92, 20);
            this.spinEditDaysAfter.TabIndex = 22;
            // 
            // spinEditDaysPrior
            // 
            this.spinEditDaysPrior.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.spinEditDaysPrior.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditDaysPrior.Location = new System.Drawing.Point(116, 25);
            this.spinEditDaysPrior.MenuManager = this.barManager1;
            this.spinEditDaysPrior.Name = "spinEditDaysPrior";
            this.spinEditDaysPrior.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditDaysPrior.Properties.IsFloatValue = false;
            this.spinEditDaysPrior.Properties.Mask.EditMask = "n0";
            this.spinEditDaysPrior.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditDaysPrior.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.spinEditDaysPrior.Size = new System.Drawing.Size(92, 20);
            this.spinEditDaysPrior.TabIndex = 21;
            // 
            // labelControl4
            // 
            this.labelControl4.AllowHtmlString = true;
            this.labelControl4.Location = new System.Drawing.Point(6, 53);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(93, 13);
            this.labelControl4.TabIndex = 13;
            this.labelControl4.Text = "Days <b>After</b> Today:";
            // 
            // labelControl5
            // 
            this.labelControl5.AllowHtmlString = true;
            this.labelControl5.Location = new System.Drawing.Point(6, 27);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(104, 13);
            this.labelControl5.TabIndex = 12;
            this.labelControl5.Text = "Days <b>Prior</b> to Today:";
            // 
            // btnDateRangeFromTodayOK
            // 
            this.btnDateRangeFromTodayOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeFromTodayOK.Location = new System.Drawing.Point(3, 84);
            this.btnDateRangeFromTodayOK.Name = "btnDateRangeFromTodayOK";
            this.btnDateRangeFromTodayOK.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDateRangeFromTodayOK.Size = new System.Drawing.Size(50, 20);
            this.btnDateRangeFromTodayOK.TabIndex = 19;
            this.btnDateRangeFromTodayOK.Text = "OK";
            this.btnDateRangeFromTodayOK.Click += new System.EventHandler(this.btnDateRangeFromTodayOK_Click);
            // 
            // popupContainerControlKAMFilter
            // 
            this.popupContainerControlKAMFilter.Controls.Add(this.btnKAMFilterOK);
            this.popupContainerControlKAMFilter.Controls.Add(this.gridControl5);
            this.popupContainerControlKAMFilter.Location = new System.Drawing.Point(686, 158);
            this.popupContainerControlKAMFilter.Name = "popupContainerControlKAMFilter";
            this.popupContainerControlKAMFilter.Size = new System.Drawing.Size(131, 135);
            this.popupContainerControlKAMFilter.TabIndex = 19;
            // 
            // btnKAMFilterOK
            // 
            this.btnKAMFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnKAMFilterOK.Location = new System.Drawing.Point(3, 112);
            this.btnKAMFilterOK.Name = "btnKAMFilterOK";
            this.btnKAMFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnKAMFilterOK.TabIndex = 18;
            this.btnKAMFilterOK.Text = "OK";
            this.btnKAMFilterOK.Click += new System.EventHandler(this.btnKAMFilterOK_Click);
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.sp06019OMJobManagerKAMsFilterBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(3, 3);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(125, 107);
            this.gridControl5.TabIndex = 1;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp06019OMJobManagerKAMsFilterBindingSource
            // 
            this.sp06019OMJobManagerKAMsFilterBindingSource.DataMember = "sp06019_OM_Job_Manager_KAMs_Filter";
            this.sp06019OMJobManagerKAMsFilterBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription3,
            this.colForename,
            this.colID1});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsFind.AlwaysVisible = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription3
            // 
            this.colDescription3.Caption = "Surname";
            this.colDescription3.FieldName = "Surname";
            this.colDescription3.Name = "colDescription3";
            this.colDescription3.OptionsColumn.AllowEdit = false;
            this.colDescription3.OptionsColumn.AllowFocus = false;
            this.colDescription3.OptionsColumn.ReadOnly = true;
            this.colDescription3.Visible = true;
            this.colDescription3.VisibleIndex = 0;
            this.colDescription3.Width = 122;
            // 
            // colForename
            // 
            this.colForename.Caption = "Forename";
            this.colForename.FieldName = "Forename";
            this.colForename.Name = "colForename";
            this.colForename.OptionsColumn.AllowEdit = false;
            this.colForename.OptionsColumn.AllowFocus = false;
            this.colForename.OptionsColumn.ReadOnly = true;
            this.colForename.Visible = true;
            this.colForename.VisibleIndex = 1;
            this.colForename.Width = 82;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.Width = 53;
            // 
            // popupContainerControlLabourFilter
            // 
            this.popupContainerControlLabourFilter.Controls.Add(this.btnLabourFilter);
            this.popupContainerControlLabourFilter.Controls.Add(this.gridControl12);
            this.popupContainerControlLabourFilter.Location = new System.Drawing.Point(149, 158);
            this.popupContainerControlLabourFilter.Name = "popupContainerControlLabourFilter";
            this.popupContainerControlLabourFilter.Size = new System.Drawing.Size(139, 126);
            this.popupContainerControlLabourFilter.TabIndex = 20;
            // 
            // btnLabourFilter
            // 
            this.btnLabourFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLabourFilter.Location = new System.Drawing.Point(3, 103);
            this.btnLabourFilter.Name = "btnLabourFilter";
            this.btnLabourFilter.Size = new System.Drawing.Size(50, 20);
            this.btnLabourFilter.TabIndex = 12;
            this.btnLabourFilter.Text = "OK";
            this.btnLabourFilter.Click += new System.EventHandler(this.btnLabourFilterOK_Click);
            // 
            // gridControl12
            // 
            this.gridControl12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl12.DataSource = this.sp06490OMTenderStaffFilterBindingSource;
            this.gridControl12.Location = new System.Drawing.Point(3, 3);
            this.gridControl12.MainView = this.gridView12;
            this.gridControl12.MenuManager = this.barManager1;
            this.gridControl12.Name = "gridControl12";
            this.gridControl12.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit17});
            this.gridControl12.Size = new System.Drawing.Size(133, 98);
            this.gridControl12.TabIndex = 4;
            this.gridControl12.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView12});
            // 
            // sp06490OMTenderStaffFilterBindingSource
            // 
            this.sp06490OMTenderStaffFilterBindingSource.DataMember = "sp06490_OM_Tender_Staff_Filter";
            this.sp06490OMTenderStaffFilterBindingSource.DataSource = this.dataSet_OM_Tender;
            // 
            // gridView12
            // 
            this.gridView12.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID4,
            this.colName,
            this.colActive});
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.colActive;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView12.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView12.GridControl = this.gridControl12;
            this.gridView12.Name = "gridView12";
            this.gridView12.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView12.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView12.OptionsFind.AlwaysVisible = true;
            this.gridView12.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView12.OptionsLayout.StoreAppearance = true;
            this.gridView12.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView12.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView12.OptionsView.ColumnAutoWidth = false;
            this.gridView12.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView12.OptionsView.ShowGroupPanel = false;
            this.gridView12.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView12.OptionsView.ShowIndicator = false;
            this.gridView12.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID4
            // 
            this.colID4.Caption = "ID";
            this.colID4.FieldName = "ID";
            this.colID4.Name = "colID4";
            this.colID4.OptionsColumn.AllowEdit = false;
            this.colID4.OptionsColumn.AllowFocus = false;
            this.colID4.OptionsColumn.ReadOnly = true;
            this.colID4.Visible = true;
            this.colID4.VisibleIndex = 2;
            this.colID4.Width = 57;
            // 
            // colName
            // 
            this.colName.Caption = "Staff Name";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.AllowFocus = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 214;
            // 
            // popupContainerControlShowTabPages
            // 
            this.popupContainerControlShowTabPages.Controls.Add(this.gridControl7);
            this.popupContainerControlShowTabPages.Controls.Add(this.btnShowTabPagesOK);
            this.popupContainerControlShowTabPages.Location = new System.Drawing.Point(294, 161);
            this.popupContainerControlShowTabPages.Name = "popupContainerControlShowTabPages";
            this.popupContainerControlShowTabPages.Size = new System.Drawing.Size(167, 125);
            this.popupContainerControlShowTabPages.TabIndex = 25;
            // 
            // gridControl7
            // 
            this.gridControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl7.DataSource = this.sp04022CoreDummyTabPageListBindingSource;
            this.gridControl7.Location = new System.Drawing.Point(3, 3);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit18});
            this.gridControl7.Size = new System.Drawing.Size(161, 98);
            this.gridControl7.TabIndex = 1;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp04022CoreDummyTabPageListBindingSource
            // 
            this.sp04022CoreDummyTabPageListBindingSource.DataMember = "sp04022_Core_Dummy_TabPageList";
            this.sp04022CoreDummyTabPageListBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTabPageName,
            this.colParentLevel});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.GroupCount = 1;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsView.AutoCalcPreviewLineCount = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colParentLevel, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // colTabPageName
            // 
            this.colTabPageName.Caption = "Linked Records";
            this.colTabPageName.FieldName = "TabPageName";
            this.colTabPageName.Name = "colTabPageName";
            this.colTabPageName.OptionsColumn.AllowEdit = false;
            this.colTabPageName.OptionsColumn.AllowFocus = false;
            this.colTabPageName.OptionsColumn.ReadOnly = true;
            this.colTabPageName.Visible = true;
            this.colTabPageName.VisibleIndex = 0;
            this.colTabPageName.Width = 156;
            // 
            // colParentLevel
            // 
            this.colParentLevel.Caption = "Level";
            this.colParentLevel.FieldName = "ParentLevel";
            this.colParentLevel.Name = "colParentLevel";
            this.colParentLevel.OptionsColumn.AllowEdit = false;
            this.colParentLevel.OptionsColumn.AllowFocus = false;
            this.colParentLevel.OptionsColumn.ReadOnly = true;
            this.colParentLevel.Visible = true;
            this.colParentLevel.VisibleIndex = 1;
            this.colParentLevel.Width = 113;
            // 
            // repositoryItemCheckEdit18
            // 
            this.repositoryItemCheckEdit18.AutoHeight = false;
            this.repositoryItemCheckEdit18.Caption = "Check";
            this.repositoryItemCheckEdit18.Name = "repositoryItemCheckEdit18";
            // 
            // btnShowTabPagesOK
            // 
            this.btnShowTabPagesOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnShowTabPagesOK.Location = new System.Drawing.Point(3, 103);
            this.btnShowTabPagesOK.Name = "btnShowTabPagesOK";
            this.btnShowTabPagesOK.Size = new System.Drawing.Size(50, 20);
            this.btnShowTabPagesOK.TabIndex = 0;
            this.btnShowTabPagesOK.Text = "OK";
            this.btnShowTabPagesOK.Click += new System.EventHandler(this.btnShowTabPagesOK_Click);
            // 
            // popupContainerControlVisitSystemStatusFilter
            // 
            this.popupContainerControlVisitSystemStatusFilter.Controls.Add(this.btnVisitSystemStatusFilterOK);
            this.popupContainerControlVisitSystemStatusFilter.Controls.Add(this.gridControl4);
            this.popupContainerControlVisitSystemStatusFilter.Location = new System.Drawing.Point(4, 158);
            this.popupContainerControlVisitSystemStatusFilter.Name = "popupContainerControlVisitSystemStatusFilter";
            this.popupContainerControlVisitSystemStatusFilter.Size = new System.Drawing.Size(139, 126);
            this.popupContainerControlVisitSystemStatusFilter.TabIndex = 9;
            // 
            // btnVisitSystemStatusFilterOK
            // 
            this.btnVisitSystemStatusFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnVisitSystemStatusFilterOK.Location = new System.Drawing.Point(3, 103);
            this.btnVisitSystemStatusFilterOK.Name = "btnVisitSystemStatusFilterOK";
            this.btnVisitSystemStatusFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnVisitSystemStatusFilterOK.TabIndex = 12;
            this.btnVisitSystemStatusFilterOK.Text = "OK";
            this.btnVisitSystemStatusFilterOK.Click += new System.EventHandler(this.btnVisitSystemStatusFilterOK_Click);
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.sp06489OMTenderStatusFilterBindingSource;
            this.gridControl4.Location = new System.Drawing.Point(3, 3);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.Size = new System.Drawing.Size(133, 98);
            this.gridControl4.TabIndex = 4;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp06489OMTenderStatusFilterBindingSource
            // 
            this.sp06489OMTenderStatusFilterBindingSource.DataMember = "sp06489_OM_Tender_Status_Filter";
            this.sp06489OMTenderStatusFilterBindingSource.DataSource = this.dataSet_OM_Tender;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn244,
            this.gridColumn245,
            this.gridColumn246});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn246, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn244
            // 
            this.gridColumn244.Caption = "Tender Status";
            this.gridColumn244.FieldName = "Description";
            this.gridColumn244.Name = "gridColumn244";
            this.gridColumn244.OptionsColumn.AllowEdit = false;
            this.gridColumn244.OptionsColumn.AllowFocus = false;
            this.gridColumn244.OptionsColumn.ReadOnly = true;
            this.gridColumn244.Visible = true;
            this.gridColumn244.VisibleIndex = 0;
            this.gridColumn244.Width = 215;
            // 
            // gridColumn245
            // 
            this.gridColumn245.Caption = "ID";
            this.gridColumn245.FieldName = "ID";
            this.gridColumn245.Name = "gridColumn245";
            this.gridColumn245.OptionsColumn.AllowEdit = false;
            this.gridColumn245.OptionsColumn.AllowFocus = false;
            this.gridColumn245.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn246
            // 
            this.gridColumn246.Caption = "Order";
            this.gridColumn246.FieldName = "RecordOrder";
            this.gridColumn246.Name = "gridColumn246";
            this.gridColumn246.OptionsColumn.AllowEdit = false;
            this.gridColumn246.OptionsColumn.AllowFocus = false;
            this.gridColumn246.OptionsColumn.ReadOnly = true;
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeOK);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(467, 161);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(216, 107);
            this.popupContainerControlDateRange.TabIndex = 17;
            // 
            // groupControl1
            // 
            this.groupControl1.AllowHtmlText = true;
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFromDate);
            this.groupControl1.Controls.Add(this.dateEditToDate);
            this.groupControl1.Location = new System.Drawing.Point(3, 5);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(210, 76);
            this.groupControl1.TabIndex = 20;
            this.groupControl1.Text = "Date <b>Range</b>  [Request Received Date]";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(165, 20);
            this.dateEditFromDate.TabIndex = 10;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(165, 20);
            this.dateEditToDate.TabIndex = 11;
            // 
            // btnDateRangeOK
            // 
            this.btnDateRangeOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeOK.Location = new System.Drawing.Point(3, 84);
            this.btnDateRangeOK.Name = "btnDateRangeOK";
            this.btnDateRangeOK.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDateRangeOK.Size = new System.Drawing.Size(50, 20);
            this.btnDateRangeOK.TabIndex = 19;
            this.btnDateRangeOK.Text = "OK";
            this.btnDateRangeOK.Click += new System.EventHandler(this.btnDateRangeOK_Click);
            // 
            // gridControlTender
            // 
            this.gridControlTender.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlTender.DataSource = this.sp06491OMTenderManagerBindingSource;
            this.gridControlTender.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlTender.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlTender.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlTender.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlTender.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlTender.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlTender.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlTender.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlTender.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlTender.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlTender.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlTender.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 22, true, true, "Reload Selected Records", "reload_selected"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 9, true, true, "Add from Template Tender Record", "template_add")});
            this.gridControlTender.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlTender_EmbeddedNavigator_ButtonClick);
            this.gridControlTender.Location = new System.Drawing.Point(0, 43);
            this.gridControlTender.MainView = this.gridViewTender;
            this.gridControlTender.MenuManager = this.barManager1;
            this.gridControlTender.Name = "gridControlTender";
            this.gridControlTender.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemTextEditMoney2,
            this.repositoryItemHyperLinkEditLinkedDocumentsJobs,
            this.repositoryItemTextEditLatLong2,
            this.repositoryItemTextEditHTML1,
            this.repositoryItemHyperLinkEditLinkedVisits2,
            this.repositoryItemHyperLinkEditTenderGroupDescription,
            this.repositoryItemTextEditNumericDays,
            this.repositoryItemProgressBarRed,
            this.repositoryItemProgressBarGreen,
            this.repositoryItemHyperLinkEditClientQuoteExtractFilt});
            this.gridControlTender.Size = new System.Drawing.Size(1331, 393);
            this.gridControlTender.TabIndex = 5;
            this.gridControlTender.ToolTipController = this.toolTipController1;
            this.gridControlTender.UseEmbeddedNavigator = true;
            this.gridControlTender.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTender});
            // 
            // sp06491OMTenderManagerBindingSource
            // 
            this.sp06491OMTenderManagerBindingSource.DataMember = "sp06491_OM_Tender_Manager";
            this.sp06491OMTenderManagerBindingSource.DataSource = this.dataSet_OM_Tender;
            // 
            // gridViewTender
            // 
            this.gridViewTender.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientContractID1,
            this.colSiteContractID1,
            this.colClientReferenceNumber,
            this.colStatusID1,
            this.colTenderDescription,
            this.colCreatedByStaffID1,
            this.colClientContactID1,
            this.colKAM,
            this.colCM,
            this.colWorkTypeID,
            this.colTenderGroupID,
            this.colReactive,
            this.colQuoteNotRequired,
            this.colRequestReceivedDate,
            this.colReturnToClientByDate,
            this.colCMInitialAttendanceDate,
            this.colQuoteSubmittedToClientDate,
            this.colQuoteAcceptedByClientDate,
            this.colRequiredWorkCompletedDate,
            this.colClientPONumber,
            this.colKAMQuoteRejectionReasonID,
            this.colOurInternalComments,
            this.colCMComments,
            this.colClientComments,
            this.colTPORequired,
            this.colPlanningAuthorityID,
            this.colTPOCheckedDate,
            this.colTreeProtected,
            this.colPlanningAuthorityNumber,
            this.colPlanningAuthoritySubmittedDate,
            this.colPlanningAuthorityOutcomeID,
            this.colPlanningAuthorityOkToProceed,
            this.colGCCompanyName1,
            this.colSectorType1,
            this.colSiteAddress1,
            this.colLinkedToParent,
            this.colContractDescription1,
            this.colSitePostcode1,
            this.colSiteLocationX1,
            this.colSiteLocationY1,
            this.colLinkedVisitCount1,
            this.colLinkedDocumentCount1,
            this.colTenderGroupDescription,
            this.colCreatedByStaffName1,
            this.colKAMName1,
            this.colCMName1,
            this.colTenderStatus,
            this.colContactPersonName,
            this.colContactPersonPosition,
            this.colContactPersonTitle,
            this.colPlanningAuthorityOutcome,
            this.colPlanningAuthority,
            this.colJobSubType,
            this.colJobType,
            this.colKAMQuoteRejectionReason,
            this.colWorkType,
            this.colSelected,
            this.colSiteCode1,
            this.colSiteID1,
            this.colClientID1,
            this.colRevisionNumber,
            this.colDaysQuoteOverdue,
            this.colNoClientPO,
            this.colNoClientPOAuthorisedByDirector,
            this.colNoClientPOAuthorisedByDirectorID,
            this.colNoClientPOEmailedToDirector,
            this.colJobTypeID1,
            this.colStatusIsssue,
            this.colStatusIssueID1,
            this.colSubmittedToCMDate,
            this.colProposedLabour,
            this.colProposedLabourID,
            this.colProposedLabourTypeID,
            this.colProposedLabourType,
            this.colQuotedLabourSell,
            this.colQuotedMaterialSell,
            this.colQuotedEquipmentSell,
            this.colQuotedTotalSell,
            this.colAcceptedLabourSell,
            this.colAcceptedMaterialSell,
            this.colAcceptedEquipmentSell,
            this.colAcceptedTotalSell,
            this.colActualLabourCost,
            this.colActualMaterialCost,
            this.colActualEquipmentCost,
            this.colActualTotalCost,
            this.colTenderProactiveDaysToReturnQuote,
            this.colTenderReactiveDaysToReturnQuote,
            this.colProgressBarValue,
            this.colClientQuoteSpreadsheetExtractFile,
            this.colLinkedJobCount1,
            this.colPlanningProceedDateSet,
            this.colPlanningProceedStaffName,
            this.colTenderType,
            this.colTenderTypeID,
            this.colTPONumber,
            this.colConservationStatus,
            this.colListedBuildingStatus,
            this.colRAMS,
            this.colProposedLabourCost,
            this.colProposedMaterialCost,
            this.colProposedEquipmentCost,
            this.colProposedTotalCost,
            this.colSiteName1,
            this.colTenderID,
            this.colClientName1});
            gridFormatRule1.Column = this.colDaysQuoteOverdue;
            gridFormatRule1.ColumnApplyTo = this.colTenderStatus;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue1.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue1.Value1 = 0;
            formatConditionRuleValue1.Value2 = "";
            gridFormatRule1.Rule = formatConditionRuleValue1;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.colStatusIssueID1;
            gridFormatRule2.ColumnApplyTo = this.colStatusIsssue;
            gridFormatRule2.Enabled = false;
            gridFormatRule2.Name = "Format1";
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue2.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue2.Value1 = 0;
            formatConditionRuleValue2.Value2 = "";
            gridFormatRule2.Rule = formatConditionRuleValue2;
            gridFormatRule3.Column = this.colStatusID1;
            gridFormatRule3.ColumnApplyTo = this.colTenderStatus;
            gridFormatRule3.Name = "Format2";
            formatConditionRuleValue3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(214)))), ((int)(((byte)(155)))));
            formatConditionRuleValue3.Appearance.Options.UseBackColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue3.PredefinedName = "Green Fill, Green Text";
            formatConditionRuleValue3.Value1 = 70;
            formatConditionRuleValue3.Value2 = "";
            gridFormatRule3.Rule = formatConditionRuleValue3;
            this.gridViewTender.FormatRules.Add(gridFormatRule1);
            this.gridViewTender.FormatRules.Add(gridFormatRule2);
            this.gridViewTender.FormatRules.Add(gridFormatRule3);
            this.gridViewTender.GridControl = this.gridControlTender;
            this.gridViewTender.Name = "gridViewTender";
            this.gridViewTender.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewTender.OptionsFind.AlwaysVisible = true;
            this.gridViewTender.OptionsFind.FindDelay = 2000;
            this.gridViewTender.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewTender.OptionsLayout.StoreAppearance = true;
            this.gridViewTender.OptionsLayout.StoreFormatRules = true;
            this.gridViewTender.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewTender.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewTender.OptionsSelection.MultiSelect = true;
            this.gridViewTender.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewTender.OptionsView.ColumnAutoWidth = false;
            this.gridViewTender.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewTender.OptionsView.ShowGroupPanel = false;
            this.gridViewTender.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTenderID, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewTender.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewTender_CustomDrawCell);
            this.gridViewTender.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewTender_CustomRowCellEdit);
            this.gridViewTender.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewTender.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewTender.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewTender.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewTender_ShowingEditor);
            this.gridViewTender.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewTender.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewTender.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridViewTender_CustomUnboundColumnData);
            this.gridViewTender.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewTender.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewTender_MouseUp);
            this.gridViewTender.DoubleClick += new System.EventHandler(this.gridViewTender_DoubleClick);
            this.gridViewTender.GotFocus += new System.EventHandler(this.gridViewTender_GotFocus);
            // 
            // colClientContractID1
            // 
            this.colClientContractID1.Caption = "Client Contract ID";
            this.colClientContractID1.FieldName = "ClientContractID";
            this.colClientContractID1.Name = "colClientContractID1";
            this.colClientContractID1.OptionsColumn.AllowEdit = false;
            this.colClientContractID1.OptionsColumn.AllowFocus = false;
            this.colClientContractID1.OptionsColumn.ReadOnly = true;
            this.colClientContractID1.Width = 105;
            // 
            // colSiteContractID1
            // 
            this.colSiteContractID1.Caption = "Site Contract ID";
            this.colSiteContractID1.FieldName = "SiteContractID";
            this.colSiteContractID1.Name = "colSiteContractID1";
            this.colSiteContractID1.OptionsColumn.AllowEdit = false;
            this.colSiteContractID1.OptionsColumn.AllowFocus = false;
            this.colSiteContractID1.OptionsColumn.ReadOnly = true;
            this.colSiteContractID1.Width = 96;
            // 
            // colClientReferenceNumber
            // 
            this.colClientReferenceNumber.Caption = "Client Reference #";
            this.colClientReferenceNumber.FieldName = "ClientReferenceNumber";
            this.colClientReferenceNumber.Name = "colClientReferenceNumber";
            this.colClientReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colClientReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colClientReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colClientReferenceNumber.Visible = true;
            this.colClientReferenceNumber.VisibleIndex = 17;
            this.colClientReferenceNumber.Width = 110;
            // 
            // colTenderDescription
            // 
            this.colTenderDescription.Caption = "Tender Description";
            this.colTenderDescription.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colTenderDescription.FieldName = "TenderDescription";
            this.colTenderDescription.Name = "colTenderDescription";
            this.colTenderDescription.OptionsColumn.ReadOnly = true;
            this.colTenderDescription.Visible = true;
            this.colTenderDescription.VisibleIndex = 7;
            this.colTenderDescription.Width = 224;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colCreatedByStaffID1
            // 
            this.colCreatedByStaffID1.Caption = "Created By Staff ID";
            this.colCreatedByStaffID1.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID1.Name = "colCreatedByStaffID1";
            this.colCreatedByStaffID1.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID1.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID1.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID1.Width = 127;
            // 
            // colClientContactID1
            // 
            this.colClientContactID1.Caption = "Client Contact ID";
            this.colClientContactID1.FieldName = "ClientContactID";
            this.colClientContactID1.Name = "colClientContactID1";
            this.colClientContactID1.OptionsColumn.AllowEdit = false;
            this.colClientContactID1.OptionsColumn.AllowFocus = false;
            this.colClientContactID1.OptionsColumn.ReadOnly = true;
            this.colClientContactID1.Width = 101;
            // 
            // colKAM
            // 
            this.colKAM.Caption = "KAM";
            this.colKAM.FieldName = "KAM";
            this.colKAM.Name = "colKAM";
            this.colKAM.OptionsColumn.AllowEdit = false;
            this.colKAM.OptionsColumn.AllowFocus = false;
            this.colKAM.OptionsColumn.ReadOnly = true;
            this.colKAM.Width = 40;
            // 
            // colCM
            // 
            this.colCM.Caption = "CM";
            this.colCM.FieldName = "CM";
            this.colCM.Name = "colCM";
            this.colCM.OptionsColumn.AllowEdit = false;
            this.colCM.OptionsColumn.AllowFocus = false;
            this.colCM.OptionsColumn.ReadOnly = true;
            this.colCM.Width = 34;
            // 
            // colWorkTypeID
            // 
            this.colWorkTypeID.Caption = "Work Sub-Type ID";
            this.colWorkTypeID.FieldName = "WorkSubTypeID";
            this.colWorkTypeID.Name = "colWorkTypeID";
            this.colWorkTypeID.OptionsColumn.AllowEdit = false;
            this.colWorkTypeID.OptionsColumn.AllowFocus = false;
            this.colWorkTypeID.OptionsColumn.ReadOnly = true;
            this.colWorkTypeID.Width = 85;
            // 
            // colTenderGroupID
            // 
            this.colTenderGroupID.Caption = "Tender Group ID";
            this.colTenderGroupID.FieldName = "TenderGroupID";
            this.colTenderGroupID.Name = "colTenderGroupID";
            this.colTenderGroupID.OptionsColumn.AllowEdit = false;
            this.colTenderGroupID.OptionsColumn.AllowFocus = false;
            this.colTenderGroupID.OptionsColumn.ReadOnly = true;
            this.colTenderGroupID.Width = 99;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colQuoteNotRequired
            // 
            this.colQuoteNotRequired.Caption = "Quote Not Required";
            this.colQuoteNotRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colQuoteNotRequired.FieldName = "QuoteNotRequired";
            this.colQuoteNotRequired.Name = "colQuoteNotRequired";
            this.colQuoteNotRequired.OptionsColumn.AllowEdit = false;
            this.colQuoteNotRequired.OptionsColumn.AllowFocus = false;
            this.colQuoteNotRequired.OptionsColumn.ReadOnly = true;
            this.colQuoteNotRequired.Width = 115;
            // 
            // colRequestReceivedDate
            // 
            this.colRequestReceivedDate.Caption = "Request Received";
            this.colRequestReceivedDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colRequestReceivedDate.CustomizationCaption = "Request Received From Client";
            this.colRequestReceivedDate.FieldName = "RequestReceivedDate";
            this.colRequestReceivedDate.Name = "colRequestReceivedDate";
            this.colRequestReceivedDate.OptionsColumn.AllowEdit = false;
            this.colRequestReceivedDate.OptionsColumn.AllowFocus = false;
            this.colRequestReceivedDate.OptionsColumn.ReadOnly = true;
            this.colRequestReceivedDate.Visible = true;
            this.colRequestReceivedDate.VisibleIndex = 10;
            this.colRequestReceivedDate.Width = 106;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colReturnToClientByDate
            // 
            this.colReturnToClientByDate.Caption = "Return To Client By";
            this.colReturnToClientByDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colReturnToClientByDate.FieldName = "ReturnToClientByDate";
            this.colReturnToClientByDate.Name = "colReturnToClientByDate";
            this.colReturnToClientByDate.OptionsColumn.AllowEdit = false;
            this.colReturnToClientByDate.OptionsColumn.AllowFocus = false;
            this.colReturnToClientByDate.OptionsColumn.ReadOnly = true;
            this.colReturnToClientByDate.Visible = true;
            this.colReturnToClientByDate.VisibleIndex = 11;
            this.colReturnToClientByDate.Width = 112;
            // 
            // colCMInitialAttendanceDate
            // 
            this.colCMInitialAttendanceDate.Caption = "CM Initial Attendance";
            this.colCMInitialAttendanceDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colCMInitialAttendanceDate.FieldName = "CMInitialAttendanceDate";
            this.colCMInitialAttendanceDate.Name = "colCMInitialAttendanceDate";
            this.colCMInitialAttendanceDate.OptionsColumn.AllowEdit = false;
            this.colCMInitialAttendanceDate.OptionsColumn.AllowFocus = false;
            this.colCMInitialAttendanceDate.OptionsColumn.ReadOnly = true;
            this.colCMInitialAttendanceDate.Width = 122;
            // 
            // colQuoteSubmittedToClientDate
            // 
            this.colQuoteSubmittedToClientDate.Caption = "Quote Submitted To Client";
            this.colQuoteSubmittedToClientDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colQuoteSubmittedToClientDate.FieldName = "QuoteSubmittedToClientDate";
            this.colQuoteSubmittedToClientDate.Name = "colQuoteSubmittedToClientDate";
            this.colQuoteSubmittedToClientDate.OptionsColumn.AllowEdit = false;
            this.colQuoteSubmittedToClientDate.OptionsColumn.AllowFocus = false;
            this.colQuoteSubmittedToClientDate.OptionsColumn.ReadOnly = true;
            this.colQuoteSubmittedToClientDate.Width = 145;
            // 
            // colQuoteAcceptedByClientDate
            // 
            this.colQuoteAcceptedByClientDate.Caption = "Quote Accepted By Client";
            this.colQuoteAcceptedByClientDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colQuoteAcceptedByClientDate.FieldName = "QuoteAcceptedByClientDate";
            this.colQuoteAcceptedByClientDate.Name = "colQuoteAcceptedByClientDate";
            this.colQuoteAcceptedByClientDate.OptionsColumn.AllowEdit = false;
            this.colQuoteAcceptedByClientDate.OptionsColumn.AllowFocus = false;
            this.colQuoteAcceptedByClientDate.OptionsColumn.ReadOnly = true;
            this.colQuoteAcceptedByClientDate.Width = 142;
            // 
            // colRequiredWorkCompletedDate
            // 
            this.colRequiredWorkCompletedDate.Caption = "Required Completion";
            this.colRequiredWorkCompletedDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colRequiredWorkCompletedDate.CustomizationCaption = "Required Completion By";
            this.colRequiredWorkCompletedDate.FieldName = "RequiredWorkCompletedDate";
            this.colRequiredWorkCompletedDate.Name = "colRequiredWorkCompletedDate";
            this.colRequiredWorkCompletedDate.OptionsColumn.AllowEdit = false;
            this.colRequiredWorkCompletedDate.OptionsColumn.AllowFocus = false;
            this.colRequiredWorkCompletedDate.OptionsColumn.ReadOnly = true;
            this.colRequiredWorkCompletedDate.Visible = true;
            this.colRequiredWorkCompletedDate.VisibleIndex = 12;
            this.colRequiredWorkCompletedDate.Width = 118;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "Client PO #";
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.OptionsColumn.AllowEdit = false;
            this.colClientPONumber.OptionsColumn.AllowFocus = false;
            this.colClientPONumber.OptionsColumn.ReadOnly = true;
            this.colClientPONumber.Visible = true;
            this.colClientPONumber.VisibleIndex = 18;
            this.colClientPONumber.Width = 91;
            // 
            // colKAMQuoteRejectionReasonID
            // 
            this.colKAMQuoteRejectionReasonID.Caption = "KAM Quote Rejection Reason ID";
            this.colKAMQuoteRejectionReasonID.FieldName = "KAMQuoteRejectionReasonID";
            this.colKAMQuoteRejectionReasonID.Name = "colKAMQuoteRejectionReasonID";
            this.colKAMQuoteRejectionReasonID.OptionsColumn.AllowEdit = false;
            this.colKAMQuoteRejectionReasonID.OptionsColumn.AllowFocus = false;
            this.colKAMQuoteRejectionReasonID.OptionsColumn.ReadOnly = true;
            this.colKAMQuoteRejectionReasonID.Width = 174;
            // 
            // colOurInternalComments
            // 
            this.colOurInternalComments.Caption = "Our Internal Comments";
            this.colOurInternalComments.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colOurInternalComments.FieldName = "OurInternalComments";
            this.colOurInternalComments.Name = "colOurInternalComments";
            this.colOurInternalComments.OptionsColumn.ReadOnly = true;
            this.colOurInternalComments.Width = 131;
            // 
            // colCMComments
            // 
            this.colCMComments.Caption = "CM Comments";
            this.colCMComments.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colCMComments.FieldName = "CMComments";
            this.colCMComments.Name = "colCMComments";
            this.colCMComments.OptionsColumn.ReadOnly = true;
            this.colCMComments.Width = 120;
            // 
            // colClientComments
            // 
            this.colClientComments.Caption = "Client Comments";
            this.colClientComments.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colClientComments.FieldName = "ClientComments";
            this.colClientComments.Name = "colClientComments";
            this.colClientComments.OptionsColumn.ReadOnly = true;
            this.colClientComments.Width = 122;
            // 
            // colTPORequired
            // 
            this.colTPORequired.Caption = "TPO Check Required";
            this.colTPORequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTPORequired.FieldName = "TPORequired";
            this.colTPORequired.Name = "colTPORequired";
            this.colTPORequired.OptionsColumn.AllowEdit = false;
            this.colTPORequired.OptionsColumn.AllowFocus = false;
            this.colTPORequired.OptionsColumn.ReadOnly = true;
            this.colTPORequired.Width = 117;
            // 
            // colPlanningAuthorityID
            // 
            this.colPlanningAuthorityID.Caption = "Planning Authority ID";
            this.colPlanningAuthorityID.FieldName = "PlanningAuthorityID";
            this.colPlanningAuthorityID.Name = "colPlanningAuthorityID";
            this.colPlanningAuthorityID.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthorityID.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthorityID.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthorityID.Width = 121;
            // 
            // colTPOCheckedDate
            // 
            this.colTPOCheckedDate.Caption = "TPO Checked Date";
            this.colTPOCheckedDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colTPOCheckedDate.FieldName = "TPOCheckedDate";
            this.colTPOCheckedDate.Name = "colTPOCheckedDate";
            this.colTPOCheckedDate.OptionsColumn.AllowEdit = false;
            this.colTPOCheckedDate.OptionsColumn.AllowFocus = false;
            this.colTPOCheckedDate.OptionsColumn.ReadOnly = true;
            this.colTPOCheckedDate.Width = 109;
            // 
            // colTreeProtected
            // 
            this.colTreeProtected.Caption = "Tree Protected";
            this.colTreeProtected.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTreeProtected.FieldName = "TreeProtected";
            this.colTreeProtected.Name = "colTreeProtected";
            this.colTreeProtected.OptionsColumn.AllowEdit = false;
            this.colTreeProtected.OptionsColumn.AllowFocus = false;
            this.colTreeProtected.OptionsColumn.ReadOnly = true;
            this.colTreeProtected.Width = 91;
            // 
            // colPlanningAuthorityNumber
            // 
            this.colPlanningAuthorityNumber.Caption = "Planning Authority #";
            this.colPlanningAuthorityNumber.FieldName = "PlanningAuthorityNumber";
            this.colPlanningAuthorityNumber.Name = "colPlanningAuthorityNumber";
            this.colPlanningAuthorityNumber.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthorityNumber.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthorityNumber.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthorityNumber.Width = 118;
            // 
            // colPlanningAuthoritySubmittedDate
            // 
            this.colPlanningAuthoritySubmittedDate.Caption = "Planning Submitted Date";
            this.colPlanningAuthoritySubmittedDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colPlanningAuthoritySubmittedDate.FieldName = "PlanningAuthoritySubmittedDate";
            this.colPlanningAuthoritySubmittedDate.Name = "colPlanningAuthoritySubmittedDate";
            this.colPlanningAuthoritySubmittedDate.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthoritySubmittedDate.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthoritySubmittedDate.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthoritySubmittedDate.Width = 136;
            // 
            // colPlanningAuthorityOutcomeID
            // 
            this.colPlanningAuthorityOutcomeID.Caption = "Planning Outcome ID";
            this.colPlanningAuthorityOutcomeID.FieldName = "PlanningAuthorityOutcomeID";
            this.colPlanningAuthorityOutcomeID.Name = "colPlanningAuthorityOutcomeID";
            this.colPlanningAuthorityOutcomeID.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthorityOutcomeID.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthorityOutcomeID.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthorityOutcomeID.Width = 119;
            // 
            // colPlanningAuthorityOkToProceed
            // 
            this.colPlanningAuthorityOkToProceed.Caption = "Planning OK To Proceed";
            this.colPlanningAuthorityOkToProceed.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colPlanningAuthorityOkToProceed.FieldName = "PlanningAuthorityOkToProceed";
            this.colPlanningAuthorityOkToProceed.Name = "colPlanningAuthorityOkToProceed";
            this.colPlanningAuthorityOkToProceed.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthorityOkToProceed.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthorityOkToProceed.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthorityOkToProceed.Width = 133;
            // 
            // colGCCompanyName1
            // 
            this.colGCCompanyName1.Caption = "GC Company Name";
            this.colGCCompanyName1.FieldName = "GCCompanyName";
            this.colGCCompanyName1.Name = "colGCCompanyName1";
            this.colGCCompanyName1.OptionsColumn.AllowEdit = false;
            this.colGCCompanyName1.OptionsColumn.AllowFocus = false;
            this.colGCCompanyName1.OptionsColumn.ReadOnly = true;
            this.colGCCompanyName1.Width = 111;
            // 
            // colSectorType1
            // 
            this.colSectorType1.Caption = "Sector Type";
            this.colSectorType1.FieldName = "SectorType";
            this.colSectorType1.Name = "colSectorType1";
            this.colSectorType1.OptionsColumn.AllowEdit = false;
            this.colSectorType1.OptionsColumn.AllowFocus = false;
            this.colSectorType1.OptionsColumn.ReadOnly = true;
            this.colSectorType1.Width = 77;
            // 
            // colSiteAddress1
            // 
            this.colSiteAddress1.Caption = "Site Address";
            this.colSiteAddress1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colSiteAddress1.FieldName = "SiteAddress";
            this.colSiteAddress1.Name = "colSiteAddress1";
            this.colSiteAddress1.OptionsColumn.ReadOnly = true;
            this.colSiteAddress1.Width = 208;
            // 
            // colLinkedToParent
            // 
            this.colLinkedToParent.Caption = "Linked To Parent";
            this.colLinkedToParent.ColumnEdit = this.repositoryItemTextEditHTML1;
            this.colLinkedToParent.FieldName = "LinkedToParent";
            this.colLinkedToParent.Name = "colLinkedToParent";
            this.colLinkedToParent.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent.Width = 276;
            // 
            // repositoryItemTextEditHTML1
            // 
            this.repositoryItemTextEditHTML1.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML1.AutoHeight = false;
            this.repositoryItemTextEditHTML1.Name = "repositoryItemTextEditHTML1";
            // 
            // colContractDescription1
            // 
            this.colContractDescription1.Caption = "Contract Description";
            this.colContractDescription1.FieldName = "ContractDescription";
            this.colContractDescription1.Name = "colContractDescription1";
            this.colContractDescription1.OptionsColumn.AllowEdit = false;
            this.colContractDescription1.OptionsColumn.AllowFocus = false;
            this.colContractDescription1.OptionsColumn.ReadOnly = true;
            this.colContractDescription1.Width = 155;
            // 
            // colSitePostcode1
            // 
            this.colSitePostcode1.Caption = "Postcode";
            this.colSitePostcode1.CustomizationCaption = "Site Postcode";
            this.colSitePostcode1.FieldName = "SitePostcode";
            this.colSitePostcode1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSitePostcode1.Name = "colSitePostcode1";
            this.colSitePostcode1.OptionsColumn.AllowEdit = false;
            this.colSitePostcode1.OptionsColumn.AllowFocus = false;
            this.colSitePostcode1.OptionsColumn.ReadOnly = true;
            this.colSitePostcode1.Visible = true;
            this.colSitePostcode1.VisibleIndex = 4;
            this.colSitePostcode1.Width = 63;
            // 
            // colSiteLocationX1
            // 
            this.colSiteLocationX1.Caption = "Site Location X";
            this.colSiteLocationX1.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.colSiteLocationX1.FieldName = "SiteLocationX";
            this.colSiteLocationX1.Name = "colSiteLocationX1";
            this.colSiteLocationX1.OptionsColumn.AllowEdit = false;
            this.colSiteLocationX1.OptionsColumn.AllowFocus = false;
            this.colSiteLocationX1.OptionsColumn.ReadOnly = true;
            this.colSiteLocationX1.Width = 89;
            // 
            // repositoryItemTextEditLatLong2
            // 
            this.repositoryItemTextEditLatLong2.AutoHeight = false;
            this.repositoryItemTextEditLatLong2.Mask.EditMask = "n8";
            this.repositoryItemTextEditLatLong2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditLatLong2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditLatLong2.Name = "repositoryItemTextEditLatLong2";
            // 
            // colSiteLocationY1
            // 
            this.colSiteLocationY1.Caption = "Site Location Y";
            this.colSiteLocationY1.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.colSiteLocationY1.FieldName = "SiteLocationY";
            this.colSiteLocationY1.Name = "colSiteLocationY1";
            this.colSiteLocationY1.OptionsColumn.AllowEdit = false;
            this.colSiteLocationY1.OptionsColumn.AllowFocus = false;
            this.colSiteLocationY1.OptionsColumn.ReadOnly = true;
            this.colSiteLocationY1.Width = 89;
            // 
            // colLinkedVisitCount1
            // 
            this.colLinkedVisitCount1.Caption = "Visit Count";
            this.colLinkedVisitCount1.ColumnEdit = this.repositoryItemHyperLinkEditLinkedVisits2;
            this.colLinkedVisitCount1.CustomizationCaption = "Linked Visit Count";
            this.colLinkedVisitCount1.FieldName = "LinkedVisitCount";
            this.colLinkedVisitCount1.Name = "colLinkedVisitCount1";
            this.colLinkedVisitCount1.OptionsColumn.ReadOnly = true;
            this.colLinkedVisitCount1.Visible = true;
            this.colLinkedVisitCount1.VisibleIndex = 19;
            this.colLinkedVisitCount1.Width = 70;
            // 
            // repositoryItemHyperLinkEditLinkedVisits2
            // 
            this.repositoryItemHyperLinkEditLinkedVisits2.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedVisits2.Name = "repositoryItemHyperLinkEditLinkedVisits2";
            this.repositoryItemHyperLinkEditLinkedVisits2.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedVisits2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedVisits2_OpenLink);
            // 
            // colLinkedDocumentCount1
            // 
            this.colLinkedDocumentCount1.Caption = "Linked Documents";
            this.colLinkedDocumentCount1.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocumentsJobs;
            this.colLinkedDocumentCount1.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount1.Name = "colLinkedDocumentCount1";
            this.colLinkedDocumentCount1.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount1.Width = 105;
            // 
            // repositoryItemHyperLinkEditLinkedDocumentsJobs
            // 
            this.repositoryItemHyperLinkEditLinkedDocumentsJobs.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocumentsJobs.Name = "repositoryItemHyperLinkEditLinkedDocumentsJobs";
            this.repositoryItemHyperLinkEditLinkedDocumentsJobs.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocumentsJobs.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink);
            // 
            // colTenderGroupDescription
            // 
            this.colTenderGroupDescription.Caption = "Tender Group";
            this.colTenderGroupDescription.ColumnEdit = this.repositoryItemHyperLinkEditTenderGroupDescription;
            this.colTenderGroupDescription.FieldName = "TenderGroupDescription";
            this.colTenderGroupDescription.Name = "colTenderGroupDescription";
            this.colTenderGroupDescription.OptionsColumn.ReadOnly = true;
            this.colTenderGroupDescription.Width = 222;
            // 
            // repositoryItemHyperLinkEditTenderGroupDescription
            // 
            this.repositoryItemHyperLinkEditTenderGroupDescription.AutoHeight = false;
            this.repositoryItemHyperLinkEditTenderGroupDescription.Name = "repositoryItemHyperLinkEditTenderGroupDescription";
            this.repositoryItemHyperLinkEditTenderGroupDescription.SingleClick = true;
            this.repositoryItemHyperLinkEditTenderGroupDescription.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditTenderGroupDescription_OpenLink);
            // 
            // colCreatedByStaffName1
            // 
            this.colCreatedByStaffName1.Caption = "Created By Name";
            this.colCreatedByStaffName1.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName1.Name = "colCreatedByStaffName1";
            this.colCreatedByStaffName1.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName1.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName1.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName1.Width = 112;
            // 
            // colKAMName1
            // 
            this.colKAMName1.Caption = "KAM Name";
            this.colKAMName1.FieldName = "KAMName";
            this.colKAMName1.Name = "colKAMName1";
            this.colKAMName1.OptionsColumn.AllowEdit = false;
            this.colKAMName1.OptionsColumn.AllowFocus = false;
            this.colKAMName1.OptionsColumn.ReadOnly = true;
            this.colKAMName1.Visible = true;
            this.colKAMName1.VisibleIndex = 8;
            this.colKAMName1.Width = 116;
            // 
            // colCMName1
            // 
            this.colCMName1.Caption = "CM Name";
            this.colCMName1.FieldName = "CMName";
            this.colCMName1.Name = "colCMName1";
            this.colCMName1.OptionsColumn.AllowEdit = false;
            this.colCMName1.OptionsColumn.AllowFocus = false;
            this.colCMName1.OptionsColumn.ReadOnly = true;
            this.colCMName1.Visible = true;
            this.colCMName1.VisibleIndex = 9;
            this.colCMName1.Width = 113;
            // 
            // colTenderStatus
            // 
            this.colTenderStatus.Caption = "Tender Status";
            this.colTenderStatus.FieldName = "TenderStatus";
            this.colTenderStatus.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colTenderStatus.Name = "colTenderStatus";
            this.colTenderStatus.OptionsColumn.AllowEdit = false;
            this.colTenderStatus.OptionsColumn.AllowFocus = false;
            this.colTenderStatus.OptionsColumn.ReadOnly = true;
            this.colTenderStatus.Visible = true;
            this.colTenderStatus.VisibleIndex = 5;
            this.colTenderStatus.Width = 156;
            // 
            // colContactPersonName
            // 
            this.colContactPersonName.Caption = "Client Contact Person";
            this.colContactPersonName.FieldName = "ContactPersonName";
            this.colContactPersonName.Name = "colContactPersonName";
            this.colContactPersonName.OptionsColumn.AllowEdit = false;
            this.colContactPersonName.OptionsColumn.AllowFocus = false;
            this.colContactPersonName.OptionsColumn.ReadOnly = true;
            this.colContactPersonName.Width = 123;
            // 
            // colContactPersonPosition
            // 
            this.colContactPersonPosition.Caption = "Client Contact Position";
            this.colContactPersonPosition.FieldName = "ContactPersonPosition";
            this.colContactPersonPosition.Name = "colContactPersonPosition";
            this.colContactPersonPosition.OptionsColumn.AllowEdit = false;
            this.colContactPersonPosition.OptionsColumn.AllowFocus = false;
            this.colContactPersonPosition.OptionsColumn.ReadOnly = true;
            this.colContactPersonPosition.Width = 127;
            // 
            // colContactPersonTitle
            // 
            this.colContactPersonTitle.Caption = "Client Contact Title";
            this.colContactPersonTitle.FieldName = "ContactPersonTitle";
            this.colContactPersonTitle.Name = "colContactPersonTitle";
            this.colContactPersonTitle.OptionsColumn.AllowEdit = false;
            this.colContactPersonTitle.OptionsColumn.AllowFocus = false;
            this.colContactPersonTitle.OptionsColumn.ReadOnly = true;
            this.colContactPersonTitle.Width = 110;
            // 
            // colPlanningAuthorityOutcome
            // 
            this.colPlanningAuthorityOutcome.Caption = "Planning Outcome";
            this.colPlanningAuthorityOutcome.FieldName = "PlanningAuthorityOutcome";
            this.colPlanningAuthorityOutcome.Name = "colPlanningAuthorityOutcome";
            this.colPlanningAuthorityOutcome.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthorityOutcome.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthorityOutcome.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthorityOutcome.Width = 105;
            // 
            // colPlanningAuthority
            // 
            this.colPlanningAuthority.Caption = "Planning Authority";
            this.colPlanningAuthority.FieldName = "PlanningAuthority";
            this.colPlanningAuthority.Name = "colPlanningAuthority";
            this.colPlanningAuthority.OptionsColumn.AllowEdit = false;
            this.colPlanningAuthority.OptionsColumn.AllowFocus = false;
            this.colPlanningAuthority.OptionsColumn.ReadOnly = true;
            this.colPlanningAuthority.Width = 162;
            // 
            // colJobSubType
            // 
            this.colJobSubType.Caption = "Work Sub-Type";
            this.colJobSubType.FieldName = "JobSubType";
            this.colJobSubType.Name = "colJobSubType";
            this.colJobSubType.OptionsColumn.AllowEdit = false;
            this.colJobSubType.OptionsColumn.AllowFocus = false;
            this.colJobSubType.OptionsColumn.ReadOnly = true;
            this.colJobSubType.Visible = true;
            this.colJobSubType.VisibleIndex = 6;
            this.colJobSubType.Width = 158;
            // 
            // colJobType
            // 
            this.colJobType.Caption = "Work Type";
            this.colJobType.FieldName = "JobType";
            this.colJobType.Name = "colJobType";
            this.colJobType.OptionsColumn.AllowEdit = false;
            this.colJobType.OptionsColumn.AllowFocus = false;
            this.colJobType.OptionsColumn.ReadOnly = true;
            this.colJobType.Width = 166;
            // 
            // colKAMQuoteRejectionReason
            // 
            this.colKAMQuoteRejectionReason.FieldName = "KAMQuoteRejectionReason";
            this.colKAMQuoteRejectionReason.Name = "colKAMQuoteRejectionReason";
            this.colKAMQuoteRejectionReason.OptionsColumn.AllowEdit = false;
            this.colKAMQuoteRejectionReason.OptionsColumn.AllowFocus = false;
            this.colKAMQuoteRejectionReason.OptionsColumn.ReadOnly = true;
            this.colKAMQuoteRejectionReason.Width = 160;
            // 
            // colWorkType
            // 
            this.colWorkType.Caption = "Work Type : Sub-Type";
            this.colWorkType.FieldName = "WorkType";
            this.colWorkType.Name = "colWorkType";
            this.colWorkType.OptionsColumn.AllowEdit = false;
            this.colWorkType.OptionsColumn.AllowFocus = false;
            this.colWorkType.OptionsColumn.ReadOnly = true;
            // 
            // colSelected
            // 
            this.colSelected.Caption = "Selected";
            this.colSelected.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            this.colSelected.Width = 60;
            // 
            // colSiteCode1
            // 
            this.colSiteCode1.Caption = "Site Code";
            this.colSiteCode1.FieldName = "SiteCode";
            this.colSiteCode1.Name = "colSiteCode1";
            this.colSiteCode1.OptionsColumn.AllowEdit = false;
            this.colSiteCode1.OptionsColumn.AllowFocus = false;
            this.colSiteCode1.OptionsColumn.ReadOnly = true;
            // 
            // colSiteID1
            // 
            this.colSiteID1.Caption = "Site ID";
            this.colSiteID1.FieldName = "SiteID";
            this.colSiteID1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteID1.Name = "colSiteID1";
            this.colSiteID1.OptionsColumn.AllowEdit = false;
            this.colSiteID1.OptionsColumn.AllowFocus = false;
            this.colSiteID1.OptionsColumn.ReadOnly = true;
            this.colSiteID1.Visible = true;
            this.colSiteID1.VisibleIndex = 1;
            this.colSiteID1.Width = 51;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colRevisionNumber
            // 
            this.colRevisionNumber.Caption = "Revision #";
            this.colRevisionNumber.FieldName = "RevisionNumber";
            this.colRevisionNumber.Name = "colRevisionNumber";
            this.colRevisionNumber.OptionsColumn.AllowEdit = false;
            this.colRevisionNumber.OptionsColumn.AllowFocus = false;
            this.colRevisionNumber.OptionsColumn.ReadOnly = true;
            this.colRevisionNumber.Width = 70;
            // 
            // colNoClientPO
            // 
            this.colNoClientPO.Caption = "No Client PO";
            this.colNoClientPO.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colNoClientPO.FieldName = "NoClientPO";
            this.colNoClientPO.Name = "colNoClientPO";
            this.colNoClientPO.OptionsColumn.AllowEdit = false;
            this.colNoClientPO.OptionsColumn.AllowFocus = false;
            this.colNoClientPO.OptionsColumn.ReadOnly = true;
            this.colNoClientPO.Width = 79;
            // 
            // colNoClientPOAuthorisedByDirector
            // 
            this.colNoClientPOAuthorisedByDirector.Caption = "No Client PO Authorised By";
            this.colNoClientPOAuthorisedByDirector.FieldName = "NoClientPOAuthorisedByDirector";
            this.colNoClientPOAuthorisedByDirector.Name = "colNoClientPOAuthorisedByDirector";
            this.colNoClientPOAuthorisedByDirector.OptionsColumn.AllowEdit = false;
            this.colNoClientPOAuthorisedByDirector.OptionsColumn.AllowFocus = false;
            this.colNoClientPOAuthorisedByDirector.OptionsColumn.ReadOnly = true;
            this.colNoClientPOAuthorisedByDirector.Width = 149;
            // 
            // colNoClientPOAuthorisedByDirectorID
            // 
            this.colNoClientPOAuthorisedByDirectorID.Caption = "No Client PO Authorised By ID";
            this.colNoClientPOAuthorisedByDirectorID.FieldName = "NoClientPOAuthorisedByDirectorID";
            this.colNoClientPOAuthorisedByDirectorID.Name = "colNoClientPOAuthorisedByDirectorID";
            this.colNoClientPOAuthorisedByDirectorID.OptionsColumn.AllowEdit = false;
            this.colNoClientPOAuthorisedByDirectorID.OptionsColumn.AllowFocus = false;
            this.colNoClientPOAuthorisedByDirectorID.OptionsColumn.ReadOnly = true;
            this.colNoClientPOAuthorisedByDirectorID.Width = 163;
            // 
            // colNoClientPOEmailedToDirector
            // 
            this.colNoClientPOEmailedToDirector.Caption = "No Client PO Emailed To Director";
            this.colNoClientPOEmailedToDirector.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colNoClientPOEmailedToDirector.FieldName = "NoClientPOEmailedToDirector";
            this.colNoClientPOEmailedToDirector.Name = "colNoClientPOEmailedToDirector";
            this.colNoClientPOEmailedToDirector.OptionsColumn.AllowEdit = false;
            this.colNoClientPOEmailedToDirector.OptionsColumn.AllowFocus = false;
            this.colNoClientPOEmailedToDirector.OptionsColumn.ReadOnly = true;
            this.colNoClientPOEmailedToDirector.Width = 174;
            // 
            // colJobTypeID1
            // 
            this.colJobTypeID1.Caption = "Job Type ID";
            this.colJobTypeID1.FieldName = "JobTypeID";
            this.colJobTypeID1.Name = "colJobTypeID1";
            this.colJobTypeID1.OptionsColumn.AllowEdit = false;
            this.colJobTypeID1.OptionsColumn.AllowFocus = false;
            this.colJobTypeID1.OptionsColumn.ReadOnly = true;
            this.colJobTypeID1.Width = 77;
            // 
            // colSubmittedToCMDate
            // 
            this.colSubmittedToCMDate.Caption = "Submitted To CM";
            this.colSubmittedToCMDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colSubmittedToCMDate.FieldName = "SubmittedToCMDate";
            this.colSubmittedToCMDate.Name = "colSubmittedToCMDate";
            this.colSubmittedToCMDate.OptionsColumn.AllowEdit = false;
            this.colSubmittedToCMDate.OptionsColumn.AllowFocus = false;
            this.colSubmittedToCMDate.OptionsColumn.ReadOnly = true;
            this.colSubmittedToCMDate.Width = 100;
            // 
            // colProposedLabour
            // 
            this.colProposedLabour.Caption = "Proposed Labour";
            this.colProposedLabour.FieldName = "ProposedLabour";
            this.colProposedLabour.Name = "colProposedLabour";
            this.colProposedLabour.OptionsColumn.AllowEdit = false;
            this.colProposedLabour.OptionsColumn.AllowFocus = false;
            this.colProposedLabour.OptionsColumn.ReadOnly = true;
            this.colProposedLabour.Visible = true;
            this.colProposedLabour.VisibleIndex = 14;
            this.colProposedLabour.Width = 162;
            // 
            // colProposedLabourID
            // 
            this.colProposedLabourID.Caption = "Proposed Labour ID";
            this.colProposedLabourID.FieldName = "ProposedLabourID";
            this.colProposedLabourID.Name = "colProposedLabourID";
            this.colProposedLabourID.OptionsColumn.AllowEdit = false;
            this.colProposedLabourID.OptionsColumn.AllowFocus = false;
            this.colProposedLabourID.OptionsColumn.ReadOnly = true;
            this.colProposedLabourID.Width = 114;
            // 
            // colProposedLabourTypeID
            // 
            this.colProposedLabourTypeID.Caption = "Proposed Labour Type ID";
            this.colProposedLabourTypeID.FieldName = "ProposedLabourTypeID";
            this.colProposedLabourTypeID.Name = "colProposedLabourTypeID";
            this.colProposedLabourTypeID.OptionsColumn.AllowEdit = false;
            this.colProposedLabourTypeID.OptionsColumn.AllowFocus = false;
            this.colProposedLabourTypeID.OptionsColumn.ReadOnly = true;
            this.colProposedLabourTypeID.Width = 141;
            // 
            // colProposedLabourType
            // 
            this.colProposedLabourType.Caption = "Proposed Labour Type";
            this.colProposedLabourType.FieldName = "ProposedLabourType";
            this.colProposedLabourType.Name = "colProposedLabourType";
            this.colProposedLabourType.OptionsColumn.AllowEdit = false;
            this.colProposedLabourType.OptionsColumn.AllowFocus = false;
            this.colProposedLabourType.OptionsColumn.ReadOnly = true;
            this.colProposedLabourType.Width = 127;
            // 
            // colQuotedLabourSell
            // 
            this.colQuotedLabourSell.Caption = "Quoted Labour Sell";
            this.colQuotedLabourSell.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colQuotedLabourSell.FieldName = "QuotedLabourSell";
            this.colQuotedLabourSell.Name = "colQuotedLabourSell";
            this.colQuotedLabourSell.OptionsColumn.AllowEdit = false;
            this.colQuotedLabourSell.OptionsColumn.AllowFocus = false;
            this.colQuotedLabourSell.OptionsColumn.ReadOnly = true;
            this.colQuotedLabourSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuotedLabourSell", "{0:c}")});
            this.colQuotedLabourSell.Width = 110;
            // 
            // repositoryItemTextEditMoney2
            // 
            this.repositoryItemTextEditMoney2.AutoHeight = false;
            this.repositoryItemTextEditMoney2.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney2.Name = "repositoryItemTextEditMoney2";
            // 
            // colQuotedMaterialSell
            // 
            this.colQuotedMaterialSell.Caption = "Quoted Material Sell";
            this.colQuotedMaterialSell.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colQuotedMaterialSell.FieldName = "QuotedMaterialSell";
            this.colQuotedMaterialSell.Name = "colQuotedMaterialSell";
            this.colQuotedMaterialSell.OptionsColumn.AllowEdit = false;
            this.colQuotedMaterialSell.OptionsColumn.AllowFocus = false;
            this.colQuotedMaterialSell.OptionsColumn.ReadOnly = true;
            this.colQuotedMaterialSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuotedMaterialSell", "{0:c}")});
            this.colQuotedMaterialSell.Width = 115;
            // 
            // colQuotedEquipmentSell
            // 
            this.colQuotedEquipmentSell.Caption = "Quoted Equipment Sell";
            this.colQuotedEquipmentSell.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colQuotedEquipmentSell.FieldName = "QuotedEquipmentSell";
            this.colQuotedEquipmentSell.Name = "colQuotedEquipmentSell";
            this.colQuotedEquipmentSell.OptionsColumn.AllowEdit = false;
            this.colQuotedEquipmentSell.OptionsColumn.AllowFocus = false;
            this.colQuotedEquipmentSell.OptionsColumn.ReadOnly = true;
            this.colQuotedEquipmentSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuotedMaterialSell", "{0:c}")});
            this.colQuotedEquipmentSell.Width = 127;
            // 
            // colQuotedTotalSell
            // 
            this.colQuotedTotalSell.Caption = "Quoted Total Sell";
            this.colQuotedTotalSell.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colQuotedTotalSell.FieldName = "QuotedTotalSell";
            this.colQuotedTotalSell.Name = "colQuotedTotalSell";
            this.colQuotedTotalSell.OptionsColumn.AllowEdit = false;
            this.colQuotedTotalSell.OptionsColumn.AllowFocus = false;
            this.colQuotedTotalSell.OptionsColumn.ReadOnly = true;
            this.colQuotedTotalSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "QuotedTotalSell", "{0:c}")});
            this.colQuotedTotalSell.Width = 101;
            // 
            // colAcceptedLabourSell
            // 
            this.colAcceptedLabourSell.Caption = "Accepted Labour Sell";
            this.colAcceptedLabourSell.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colAcceptedLabourSell.FieldName = "AcceptedLabourSell";
            this.colAcceptedLabourSell.Name = "colAcceptedLabourSell";
            this.colAcceptedLabourSell.OptionsColumn.AllowEdit = false;
            this.colAcceptedLabourSell.OptionsColumn.AllowFocus = false;
            this.colAcceptedLabourSell.OptionsColumn.ReadOnly = true;
            this.colAcceptedLabourSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AcceptedLabourSell", "{0:c}")});
            this.colAcceptedLabourSell.Width = 119;
            // 
            // colAcceptedMaterialSell
            // 
            this.colAcceptedMaterialSell.Caption = "Accepted Material Sell";
            this.colAcceptedMaterialSell.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colAcceptedMaterialSell.FieldName = "AcceptedMaterialSell";
            this.colAcceptedMaterialSell.Name = "colAcceptedMaterialSell";
            this.colAcceptedMaterialSell.OptionsColumn.AllowEdit = false;
            this.colAcceptedMaterialSell.OptionsColumn.AllowFocus = false;
            this.colAcceptedMaterialSell.OptionsColumn.ReadOnly = true;
            this.colAcceptedMaterialSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AcceptedMaterialSell", "{0:c}")});
            this.colAcceptedMaterialSell.Width = 124;
            // 
            // colAcceptedEquipmentSell
            // 
            this.colAcceptedEquipmentSell.Caption = "Accepted Equipment Sell";
            this.colAcceptedEquipmentSell.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colAcceptedEquipmentSell.FieldName = "AcceptedEquipmentSell";
            this.colAcceptedEquipmentSell.Name = "colAcceptedEquipmentSell";
            this.colAcceptedEquipmentSell.OptionsColumn.AllowEdit = false;
            this.colAcceptedEquipmentSell.OptionsColumn.AllowFocus = false;
            this.colAcceptedEquipmentSell.OptionsColumn.ReadOnly = true;
            this.colAcceptedEquipmentSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AcceptedEquipmentSell", "{0:c}")});
            this.colAcceptedEquipmentSell.Width = 136;
            // 
            // colAcceptedTotalSell
            // 
            this.colAcceptedTotalSell.Caption = "Accepted Total Sell";
            this.colAcceptedTotalSell.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colAcceptedTotalSell.FieldName = "AcceptedTotalSell";
            this.colAcceptedTotalSell.Name = "colAcceptedTotalSell";
            this.colAcceptedTotalSell.OptionsColumn.AllowEdit = false;
            this.colAcceptedTotalSell.OptionsColumn.AllowFocus = false;
            this.colAcceptedTotalSell.OptionsColumn.ReadOnly = true;
            this.colAcceptedTotalSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AcceptedTotalSell", "{0:c}")});
            this.colAcceptedTotalSell.Visible = true;
            this.colAcceptedTotalSell.VisibleIndex = 16;
            this.colAcceptedTotalSell.Width = 110;
            // 
            // colActualLabourCost
            // 
            this.colActualLabourCost.Caption = "Actual Labour Cost";
            this.colActualLabourCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colActualLabourCost.FieldName = "ActualLabourCost";
            this.colActualLabourCost.Name = "colActualLabourCost";
            this.colActualLabourCost.OptionsColumn.AllowEdit = false;
            this.colActualLabourCost.OptionsColumn.AllowFocus = false;
            this.colActualLabourCost.OptionsColumn.ReadOnly = true;
            this.colActualLabourCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ActualLabourCost", "{0:c}")});
            this.colActualLabourCost.Width = 110;
            // 
            // colActualMaterialCost
            // 
            this.colActualMaterialCost.Caption = "Actual Material Cost";
            this.colActualMaterialCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colActualMaterialCost.FieldName = "ActualMaterialCost";
            this.colActualMaterialCost.Name = "colActualMaterialCost";
            this.colActualMaterialCost.OptionsColumn.AllowEdit = false;
            this.colActualMaterialCost.OptionsColumn.AllowFocus = false;
            this.colActualMaterialCost.OptionsColumn.ReadOnly = true;
            this.colActualMaterialCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ActualMaterialCost", "{0:c}")});
            this.colActualMaterialCost.Width = 115;
            // 
            // colActualEquipmentCost
            // 
            this.colActualEquipmentCost.Caption = "Actual Equipment Cost";
            this.colActualEquipmentCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colActualEquipmentCost.FieldName = "ActualEquipmentCost";
            this.colActualEquipmentCost.Name = "colActualEquipmentCost";
            this.colActualEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colActualEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colActualEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colActualEquipmentCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ActualEquipmentCost", "{0:c}")});
            this.colActualEquipmentCost.Width = 127;
            // 
            // colActualTotalCost
            // 
            this.colActualTotalCost.Caption = "Actual Total Cost";
            this.colActualTotalCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colActualTotalCost.FieldName = "ActualTotalCost";
            this.colActualTotalCost.Name = "colActualTotalCost";
            this.colActualTotalCost.OptionsColumn.AllowEdit = false;
            this.colActualTotalCost.OptionsColumn.AllowFocus = false;
            this.colActualTotalCost.OptionsColumn.ReadOnly = true;
            this.colActualTotalCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ActualTotalCost", "{0:c}")});
            this.colActualTotalCost.Visible = true;
            this.colActualTotalCost.VisibleIndex = 15;
            this.colActualTotalCost.Width = 101;
            // 
            // colTenderProactiveDaysToReturnQuote
            // 
            this.colTenderProactiveDaysToReturnQuote.Caption = "Proactive Return Quote";
            this.colTenderProactiveDaysToReturnQuote.ColumnEdit = this.repositoryItemTextEditNumericDays;
            this.colTenderProactiveDaysToReturnQuote.FieldName = "TenderProactiveDaysToReturnQuote";
            this.colTenderProactiveDaysToReturnQuote.Name = "colTenderProactiveDaysToReturnQuote";
            this.colTenderProactiveDaysToReturnQuote.OptionsColumn.AllowEdit = false;
            this.colTenderProactiveDaysToReturnQuote.OptionsColumn.AllowFocus = false;
            this.colTenderProactiveDaysToReturnQuote.OptionsColumn.ReadOnly = true;
            this.colTenderProactiveDaysToReturnQuote.Width = 133;
            // 
            // colTenderReactiveDaysToReturnQuote
            // 
            this.colTenderReactiveDaysToReturnQuote.Caption = "Reactive Return Quote";
            this.colTenderReactiveDaysToReturnQuote.ColumnEdit = this.repositoryItemTextEditNumericDays;
            this.colTenderReactiveDaysToReturnQuote.FieldName = "TenderReactiveDaysToReturnQuote";
            this.colTenderReactiveDaysToReturnQuote.Name = "colTenderReactiveDaysToReturnQuote";
            this.colTenderReactiveDaysToReturnQuote.OptionsColumn.AllowEdit = false;
            this.colTenderReactiveDaysToReturnQuote.OptionsColumn.AllowFocus = false;
            this.colTenderReactiveDaysToReturnQuote.OptionsColumn.ReadOnly = true;
            this.colTenderReactiveDaysToReturnQuote.Width = 130;
            // 
            // colProgressBarValue
            // 
            this.colProgressBarValue.Caption = "Progress";
            this.colProgressBarValue.ColumnEdit = this.repositoryItemProgressBarGreen;
            this.colProgressBarValue.FieldName = "ProgressBarValue";
            this.colProgressBarValue.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colProgressBarValue.Name = "colProgressBarValue";
            this.colProgressBarValue.OptionsColumn.AllowEdit = false;
            this.colProgressBarValue.OptionsColumn.AllowFocus = false;
            this.colProgressBarValue.OptionsColumn.ReadOnly = true;
            this.colProgressBarValue.Visible = true;
            this.colProgressBarValue.VisibleIndex = 20;
            // 
            // repositoryItemProgressBarGreen
            // 
            this.repositoryItemProgressBarGreen.EndColor = System.Drawing.Color.Green;
            this.repositoryItemProgressBarGreen.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.repositoryItemProgressBarGreen.LookAndFeel.UseDefaultLookAndFeel = false;
            this.repositoryItemProgressBarGreen.Maximum = 70;
            this.repositoryItemProgressBarGreen.Name = "repositoryItemProgressBarGreen";
            this.repositoryItemProgressBarGreen.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.repositoryItemProgressBarGreen.ShowTitle = true;
            this.repositoryItemProgressBarGreen.StartColor = System.Drawing.Color.PaleGreen;
            // 
            // colClientQuoteSpreadsheetExtractFile
            // 
            this.colClientQuoteSpreadsheetExtractFile.Caption = "Client Quote SS Extract File";
            this.colClientQuoteSpreadsheetExtractFile.ColumnEdit = this.repositoryItemHyperLinkEditClientQuoteExtractFilt;
            this.colClientQuoteSpreadsheetExtractFile.FieldName = "ClientQuoteSpreadsheetExtractFile";
            this.colClientQuoteSpreadsheetExtractFile.Name = "colClientQuoteSpreadsheetExtractFile";
            this.colClientQuoteSpreadsheetExtractFile.OptionsColumn.ReadOnly = true;
            this.colClientQuoteSpreadsheetExtractFile.Width = 300;
            // 
            // repositoryItemHyperLinkEditClientQuoteExtractFilt
            // 
            this.repositoryItemHyperLinkEditClientQuoteExtractFilt.AutoHeight = false;
            this.repositoryItemHyperLinkEditClientQuoteExtractFilt.Name = "repositoryItemHyperLinkEditClientQuoteExtractFilt";
            this.repositoryItemHyperLinkEditClientQuoteExtractFilt.SingleClick = true;
            this.repositoryItemHyperLinkEditClientQuoteExtractFilt.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditClientQuoteExtractFilt_OpenLink);
            // 
            // colLinkedJobCount1
            // 
            this.colLinkedJobCount1.Caption = "Linked Jobs";
            this.colLinkedJobCount1.ColumnEdit = this.repositoryItemHyperLinkEditLinkedVisits2;
            this.colLinkedJobCount1.FieldName = "LinkedJobCount";
            this.colLinkedJobCount1.Name = "colLinkedJobCount1";
            this.colLinkedJobCount1.OptionsColumn.ReadOnly = true;
            this.colLinkedJobCount1.Width = 74;
            // 
            // colPlanningProceedDateSet
            // 
            this.colPlanningProceedDateSet.Caption = "Planning Ok Date";
            this.colPlanningProceedDateSet.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colPlanningProceedDateSet.FieldName = "PlanningProceedDateSet";
            this.colPlanningProceedDateSet.Name = "colPlanningProceedDateSet";
            this.colPlanningProceedDateSet.OptionsColumn.AllowEdit = false;
            this.colPlanningProceedDateSet.OptionsColumn.AllowFocus = false;
            this.colPlanningProceedDateSet.OptionsColumn.ReadOnly = true;
            this.colPlanningProceedDateSet.Width = 101;
            // 
            // colPlanningProceedStaffName
            // 
            this.colPlanningProceedStaffName.Caption = "Planning OK Name";
            this.colPlanningProceedStaffName.FieldName = "PlanningProceedStaffName";
            this.colPlanningProceedStaffName.Name = "colPlanningProceedStaffName";
            this.colPlanningProceedStaffName.OptionsColumn.AllowEdit = false;
            this.colPlanningProceedStaffName.OptionsColumn.AllowFocus = false;
            this.colPlanningProceedStaffName.OptionsColumn.ReadOnly = true;
            this.colPlanningProceedStaffName.Width = 106;
            // 
            // colTenderType
            // 
            this.colTenderType.Caption = "Tender Type";
            this.colTenderType.FieldName = "TenderType";
            this.colTenderType.Name = "colTenderType";
            this.colTenderType.OptionsColumn.AllowEdit = false;
            this.colTenderType.OptionsColumn.AllowFocus = false;
            this.colTenderType.OptionsColumn.ReadOnly = true;
            this.colTenderType.Width = 136;
            // 
            // colTenderTypeID
            // 
            this.colTenderTypeID.Caption = "Tender Type ID";
            this.colTenderTypeID.FieldName = "TenderTypeID";
            this.colTenderTypeID.Name = "colTenderTypeID";
            this.colTenderTypeID.OptionsColumn.AllowEdit = false;
            this.colTenderTypeID.OptionsColumn.AllowFocus = false;
            this.colTenderTypeID.OptionsColumn.ReadOnly = true;
            this.colTenderTypeID.Width = 94;
            // 
            // colTPONumber
            // 
            this.colTPONumber.Caption = "TPO Number";
            this.colTPONumber.FieldName = "TPONumber";
            this.colTPONumber.Name = "colTPONumber";
            this.colTPONumber.OptionsColumn.AllowEdit = false;
            this.colTPONumber.OptionsColumn.AllowFocus = false;
            this.colTPONumber.OptionsColumn.ReadOnly = true;
            this.colTPONumber.Width = 113;
            // 
            // colConservationStatus
            // 
            this.colConservationStatus.Caption = "Conservation Status";
            this.colConservationStatus.FieldName = "ConservationStatus";
            this.colConservationStatus.Name = "colConservationStatus";
            this.colConservationStatus.OptionsColumn.AllowEdit = false;
            this.colConservationStatus.OptionsColumn.AllowFocus = false;
            this.colConservationStatus.OptionsColumn.ReadOnly = true;
            this.colConservationStatus.Width = 155;
            // 
            // colListedBuildingStatus
            // 
            this.colListedBuildingStatus.Caption = "Listed Building Status";
            this.colListedBuildingStatus.FieldName = "ListedBuildingStatus";
            this.colListedBuildingStatus.Name = "colListedBuildingStatus";
            this.colListedBuildingStatus.OptionsColumn.AllowEdit = false;
            this.colListedBuildingStatus.OptionsColumn.AllowFocus = false;
            this.colListedBuildingStatus.OptionsColumn.ReadOnly = true;
            this.colListedBuildingStatus.Width = 146;
            // 
            // colRAMS
            // 
            this.colRAMS.Caption = "RAMS";
            this.colRAMS.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colRAMS.FieldName = "RAMS";
            this.colRAMS.Name = "colRAMS";
            this.colRAMS.OptionsColumn.AllowEdit = false;
            this.colRAMS.OptionsColumn.AllowFocus = false;
            this.colRAMS.OptionsColumn.ReadOnly = true;
            this.colRAMS.Width = 47;
            // 
            // colProposedLabourCost
            // 
            this.colProposedLabourCost.Caption = "Proposed Labour Cost";
            this.colProposedLabourCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colProposedLabourCost.FieldName = "ProposedLabourCost";
            this.colProposedLabourCost.Name = "colProposedLabourCost";
            this.colProposedLabourCost.OptionsColumn.AllowEdit = false;
            this.colProposedLabourCost.OptionsColumn.AllowFocus = false;
            this.colProposedLabourCost.OptionsColumn.ReadOnly = true;
            this.colProposedLabourCost.Width = 125;
            // 
            // colProposedMaterialCost
            // 
            this.colProposedMaterialCost.Caption = "Proposed Material Cost";
            this.colProposedMaterialCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colProposedMaterialCost.FieldName = "ProposedMaterialCost";
            this.colProposedMaterialCost.Name = "colProposedMaterialCost";
            this.colProposedMaterialCost.OptionsColumn.AllowEdit = false;
            this.colProposedMaterialCost.OptionsColumn.AllowFocus = false;
            this.colProposedMaterialCost.OptionsColumn.ReadOnly = true;
            this.colProposedMaterialCost.Width = 130;
            // 
            // colProposedEquipmentCost
            // 
            this.colProposedEquipmentCost.Caption = "Proposed Equipment Cost";
            this.colProposedEquipmentCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colProposedEquipmentCost.FieldName = "ProposedEquipmentCost";
            this.colProposedEquipmentCost.Name = "colProposedEquipmentCost";
            this.colProposedEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colProposedEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colProposedEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colProposedEquipmentCost.Width = 142;
            // 
            // colProposedTotalCost
            // 
            this.colProposedTotalCost.Caption = "Proposed Total Cost";
            this.colProposedTotalCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colProposedTotalCost.FieldName = "ProposedTotalCost";
            this.colProposedTotalCost.Name = "colProposedTotalCost";
            this.colProposedTotalCost.OptionsColumn.AllowEdit = false;
            this.colProposedTotalCost.OptionsColumn.AllowFocus = false;
            this.colProposedTotalCost.OptionsColumn.ReadOnly = true;
            this.colProposedTotalCost.Width = 116;
            // 
            // repositoryItemProgressBarRed
            // 
            this.repositoryItemProgressBarRed.EndColor = System.Drawing.Color.Red;
            this.repositoryItemProgressBarRed.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.repositoryItemProgressBarRed.LookAndFeel.UseDefaultLookAndFeel = false;
            this.repositoryItemProgressBarRed.Maximum = 70;
            this.repositoryItemProgressBarRed.Name = "repositoryItemProgressBarRed";
            this.repositoryItemProgressBarRed.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.repositoryItemProgressBarRed.ShowTitle = true;
            this.repositoryItemProgressBarRed.StartColor = System.Drawing.Color.Pink;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 1);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(1334, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl2.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPageVisits;
            this.xtraTabControl2.Size = new System.Drawing.Size(1336, 209);
            this.xtraTabControl2.TabIndex = 6;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageVisits,
            this.xtraTabPageHistory,
            this.xtraTabPageCRM,
            this.xtraTabPageLinkedDocuments});
            this.xtraTabControl2.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl2_SelectedPageChanged);
            // 
            // xtraTabPageVisits
            // 
            this.xtraTabPageVisits.Controls.Add(this.gridControlVisit);
            this.xtraTabPageVisits.Name = "xtraTabPageVisits";
            this.xtraTabPageVisits.Size = new System.Drawing.Size(1331, 180);
            this.xtraTabPageVisits.Tag = "1";
            this.xtraTabPageVisits.Text = "Linked Visits";
            // 
            // xtraTabPageHistory
            // 
            this.xtraTabPageHistory.Controls.Add(this.gridControlHistory);
            this.xtraTabPageHistory.Name = "xtraTabPageHistory";
            this.xtraTabPageHistory.Size = new System.Drawing.Size(1331, 180);
            this.xtraTabPageHistory.Tag = "1";
            this.xtraTabPageHistory.Text = "Tender History";
            // 
            // gridControlHistory
            // 
            this.gridControlHistory.DataSource = this.sp06515OMTenderHistoryForTenderBindingSource;
            this.gridControlHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlHistory.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlHistory.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlHistory.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlHistory.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlHistory.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlHistory.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlHistory.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlHistory.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlHistory.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlHistory.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlHistory.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlHistory.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 13, true, true, "Reload Data", "reload")});
            this.gridControlHistory.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlHistory_EmbeddedNavigator_ButtonClick);
            this.gridControlHistory.Location = new System.Drawing.Point(0, 0);
            this.gridControlHistory.MainView = this.gridViewHistory;
            this.gridControlHistory.MenuManager = this.barManager1;
            this.gridControlHistory.Name = "gridControlHistory";
            this.gridControlHistory.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit8,
            this.repositoryItemTextEditDateTime7});
            this.gridControlHistory.Size = new System.Drawing.Size(1331, 180);
            this.gridControlHistory.TabIndex = 2;
            this.gridControlHistory.UseEmbeddedNavigator = true;
            this.gridControlHistory.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewHistory});
            // 
            // sp06515OMTenderHistoryForTenderBindingSource
            // 
            this.sp06515OMTenderHistoryForTenderBindingSource.DataMember = "sp06515_OM_Tender_History_For_Tender";
            this.sp06515OMTenderHistoryForTenderBindingSource.DataSource = this.dataSet_OM_Tender;
            // 
            // gridViewHistory
            // 
            this.gridViewHistory.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTenderHistoryID,
            this.colTenderID2,
            this.colOldStatusID,
            this.colNewStatusID,
            this.colOldStatusIssueID,
            this.colNewStatusIssueID,
            this.colUserName,
            this.colDateChanged,
            this.colRemarks1,
            this.colLinkedToRecord,
            this.colClientName2,
            this.colSiteName2,
            this.colContractDescription2,
            this.colOldStatus,
            this.colNewStatus,
            this.colOldStatusIssue,
            this.colNewStatusIssue});
            this.gridViewHistory.GridControl = this.gridControlHistory;
            this.gridViewHistory.GroupCount = 1;
            this.gridViewHistory.Name = "gridViewHistory";
            this.gridViewHistory.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewHistory.OptionsFind.FindDelay = 2000;
            this.gridViewHistory.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewHistory.OptionsLayout.StoreAppearance = true;
            this.gridViewHistory.OptionsLayout.StoreFormatRules = true;
            this.gridViewHistory.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewHistory.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewHistory.OptionsSelection.MultiSelect = true;
            this.gridViewHistory.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewHistory.OptionsView.ColumnAutoWidth = false;
            this.gridViewHistory.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewHistory.OptionsView.ShowGroupPanel = false;
            this.gridViewHistory.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToRecord, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractDescription2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTenderID2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateChanged, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewHistory.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewHistory.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewHistory.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewHistory.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewHistory.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewHistory.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridViewHistory_CustomColumnDisplayText);
            this.gridViewHistory.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewHistory.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewHistory_MouseUp);
            this.gridViewHistory.DoubleClick += new System.EventHandler(this.gridViewHistory_DoubleClick);
            this.gridViewHistory.GotFocus += new System.EventHandler(this.gridViewHistory_GotFocus);
            // 
            // colTenderHistoryID
            // 
            this.colTenderHistoryID.Caption = "Tender History ID";
            this.colTenderHistoryID.FieldName = "TenderHistoryID";
            this.colTenderHistoryID.Name = "colTenderHistoryID";
            this.colTenderHistoryID.OptionsColumn.AllowEdit = false;
            this.colTenderHistoryID.OptionsColumn.AllowFocus = false;
            this.colTenderHistoryID.OptionsColumn.ReadOnly = true;
            this.colTenderHistoryID.Width = 104;
            // 
            // colTenderID2
            // 
            this.colTenderID2.Caption = "Tender ID";
            this.colTenderID2.FieldName = "TenderID";
            this.colTenderID2.Name = "colTenderID2";
            this.colTenderID2.OptionsColumn.AllowEdit = false;
            this.colTenderID2.OptionsColumn.AllowFocus = false;
            this.colTenderID2.OptionsColumn.ReadOnly = true;
            this.colTenderID2.Width = 93;
            // 
            // colOldStatusID
            // 
            this.colOldStatusID.Caption = "Old Status ID";
            this.colOldStatusID.FieldName = "OldStatusID";
            this.colOldStatusID.Name = "colOldStatusID";
            this.colOldStatusID.OptionsColumn.AllowEdit = false;
            this.colOldStatusID.OptionsColumn.AllowFocus = false;
            this.colOldStatusID.OptionsColumn.ReadOnly = true;
            this.colOldStatusID.Width = 83;
            // 
            // colNewStatusID
            // 
            this.colNewStatusID.Caption = "New Status ID";
            this.colNewStatusID.FieldName = "NewStatusID";
            this.colNewStatusID.Name = "colNewStatusID";
            this.colNewStatusID.OptionsColumn.AllowEdit = false;
            this.colNewStatusID.OptionsColumn.AllowFocus = false;
            this.colNewStatusID.OptionsColumn.ReadOnly = true;
            this.colNewStatusID.Width = 88;
            // 
            // colOldStatusIssueID
            // 
            this.colOldStatusIssueID.Caption = "Old Status Issue ID";
            this.colOldStatusIssueID.FieldName = "OldStatusIssueID";
            this.colOldStatusIssueID.Name = "colOldStatusIssueID";
            this.colOldStatusIssueID.OptionsColumn.AllowEdit = false;
            this.colOldStatusIssueID.OptionsColumn.AllowFocus = false;
            this.colOldStatusIssueID.OptionsColumn.ReadOnly = true;
            this.colOldStatusIssueID.Width = 112;
            // 
            // colNewStatusIssueID
            // 
            this.colNewStatusIssueID.Caption = "New Status Issue ID";
            this.colNewStatusIssueID.FieldName = "NewStatusIssueID";
            this.colNewStatusIssueID.Name = "colNewStatusIssueID";
            this.colNewStatusIssueID.OptionsColumn.AllowEdit = false;
            this.colNewStatusIssueID.OptionsColumn.AllowFocus = false;
            this.colNewStatusIssueID.OptionsColumn.ReadOnly = true;
            this.colNewStatusIssueID.Width = 117;
            // 
            // colUserName
            // 
            this.colUserName.Caption = "User Name";
            this.colUserName.FieldName = "UserName";
            this.colUserName.Name = "colUserName";
            this.colUserName.OptionsColumn.AllowEdit = false;
            this.colUserName.OptionsColumn.AllowFocus = false;
            this.colUserName.OptionsColumn.ReadOnly = true;
            this.colUserName.Visible = true;
            this.colUserName.VisibleIndex = 1;
            this.colUserName.Width = 199;
            // 
            // colDateChanged
            // 
            this.colDateChanged.Caption = "Date Changed";
            this.colDateChanged.ColumnEdit = this.repositoryItemTextEditDateTime7;
            this.colDateChanged.FieldName = "DateChanged";
            this.colDateChanged.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateChanged.Name = "colDateChanged";
            this.colDateChanged.OptionsColumn.AllowEdit = false;
            this.colDateChanged.OptionsColumn.AllowFocus = false;
            this.colDateChanged.OptionsColumn.ReadOnly = true;
            this.colDateChanged.Visible = true;
            this.colDateChanged.VisibleIndex = 0;
            this.colDateChanged.Width = 142;
            // 
            // repositoryItemTextEditDateTime7
            // 
            this.repositoryItemTextEditDateTime7.AutoHeight = false;
            this.repositoryItemTextEditDateTime7.Mask.EditMask = "G";
            this.repositoryItemTextEditDateTime7.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime7.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime7.Name = "repositoryItemTextEditDateTime7";
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit8;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 6;
            // 
            // repositoryItemMemoExEdit8
            // 
            this.repositoryItemMemoExEdit8.AutoHeight = false;
            this.repositoryItemMemoExEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit8.Name = "repositoryItemMemoExEdit8";
            this.repositoryItemMemoExEdit8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit8.ShowIcon = false;
            // 
            // colLinkedToRecord
            // 
            this.colLinkedToRecord.Caption = "Linked To Tender";
            this.colLinkedToRecord.FieldName = "LinkedToRecord";
            this.colLinkedToRecord.Name = "colLinkedToRecord";
            this.colLinkedToRecord.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecord.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecord.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecord.Visible = true;
            this.colLinkedToRecord.VisibleIndex = 3;
            this.colLinkedToRecord.Width = 475;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Cient Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 218;
            // 
            // colSiteName2
            // 
            this.colSiteName2.Caption = "Site Name";
            this.colSiteName2.FieldName = "SiteName";
            this.colSiteName2.Name = "colSiteName2";
            this.colSiteName2.OptionsColumn.AllowEdit = false;
            this.colSiteName2.OptionsColumn.AllowFocus = false;
            this.colSiteName2.OptionsColumn.ReadOnly = true;
            this.colSiteName2.Width = 232;
            // 
            // colContractDescription2
            // 
            this.colContractDescription2.Caption = "Contract Description";
            this.colContractDescription2.FieldName = "ContractDescription";
            this.colContractDescription2.Name = "colContractDescription2";
            this.colContractDescription2.OptionsColumn.AllowEdit = false;
            this.colContractDescription2.OptionsColumn.AllowFocus = false;
            this.colContractDescription2.OptionsColumn.ReadOnly = true;
            this.colContractDescription2.Width = 257;
            // 
            // colOldStatus
            // 
            this.colOldStatus.Caption = "Old Status";
            this.colOldStatus.FieldName = "OldStatus";
            this.colOldStatus.Name = "colOldStatus";
            this.colOldStatus.OptionsColumn.AllowEdit = false;
            this.colOldStatus.OptionsColumn.AllowFocus = false;
            this.colOldStatus.OptionsColumn.ReadOnly = true;
            this.colOldStatus.Visible = true;
            this.colOldStatus.VisibleIndex = 2;
            this.colOldStatus.Width = 200;
            // 
            // colNewStatus
            // 
            this.colNewStatus.Caption = "New Status";
            this.colNewStatus.FieldName = "NewStatus";
            this.colNewStatus.Name = "colNewStatus";
            this.colNewStatus.OptionsColumn.AllowEdit = false;
            this.colNewStatus.OptionsColumn.AllowFocus = false;
            this.colNewStatus.OptionsColumn.ReadOnly = true;
            this.colNewStatus.Visible = true;
            this.colNewStatus.VisibleIndex = 3;
            this.colNewStatus.Width = 200;
            // 
            // colOldStatusIssue
            // 
            this.colOldStatusIssue.Caption = "Old Status Issue";
            this.colOldStatusIssue.FieldName = "OldStatusIssue";
            this.colOldStatusIssue.Name = "colOldStatusIssue";
            this.colOldStatusIssue.OptionsColumn.AllowEdit = false;
            this.colOldStatusIssue.OptionsColumn.AllowFocus = false;
            this.colOldStatusIssue.OptionsColumn.ReadOnly = true;
            this.colOldStatusIssue.Visible = true;
            this.colOldStatusIssue.VisibleIndex = 4;
            this.colOldStatusIssue.Width = 200;
            // 
            // colNewStatusIssue
            // 
            this.colNewStatusIssue.Caption = "New Status Issue";
            this.colNewStatusIssue.FieldName = "NewStatusIssue";
            this.colNewStatusIssue.Name = "colNewStatusIssue";
            this.colNewStatusIssue.OptionsColumn.AllowEdit = false;
            this.colNewStatusIssue.OptionsColumn.AllowFocus = false;
            this.colNewStatusIssue.OptionsColumn.ReadOnly = true;
            this.colNewStatusIssue.Visible = true;
            this.colNewStatusIssue.VisibleIndex = 5;
            this.colNewStatusIssue.Width = 200;
            // 
            // xtraTabPageCRM
            // 
            this.xtraTabPageCRM.Controls.Add(this.gridSplitContainer1);
            this.xtraTabPageCRM.Name = "xtraTabPageCRM";
            this.xtraTabPageCRM.Size = new System.Drawing.Size(1331, 180);
            this.xtraTabPageCRM.Tag = "1";
            this.xtraTabPageCRM.Text = "Tender CRM";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControlCRM;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControlCRM);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1331, 180);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // gridControlCRM
            // 
            this.gridControlCRM.DataSource = this.sp05089CRMContactsLinkedToRecordBindingSource;
            this.gridControlCRM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlCRM.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlCRM.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlCRM.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document")});
            this.gridControlCRM.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlCRM_EmbeddedNavigator_ButtonClick);
            this.gridControlCRM.Location = new System.Drawing.Point(0, 0);
            this.gridControlCRM.MainView = this.gridViewCRM;
            this.gridControlCRM.Name = "gridControlCRM";
            this.gridControlCRM.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit15,
            this.repositoryItemHyperLinkEdit3,
            this.repositoryItemTextDateTime,
            this.repositoryItemTextEditHTML12});
            this.gridControlCRM.Size = new System.Drawing.Size(1331, 180);
            this.gridControlCRM.TabIndex = 6;
            this.gridControlCRM.UseEmbeddedNavigator = true;
            this.gridControlCRM.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCRM});
            // 
            // sp05089CRMContactsLinkedToRecordBindingSource
            // 
            this.sp05089CRMContactsLinkedToRecordBindingSource.DataMember = "sp05089_CRM_Contacts_Linked_To_Record";
            this.sp05089CRMContactsLinkedToRecordBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewCRM
            // 
            this.gridViewCRM.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCRMID,
            this.gridColumn208,
            this.colClientContactID,
            this.colLinkedToRecordTypeID1,
            this.colLinkedToRecordID1,
            this.colContactDueDateTime,
            this.colContactMadeDateTime,
            this.colContactMethodID,
            this.colContactTypeID,
            this.gridColumn209,
            this.gridColumn210,
            this.colStatusID,
            this.gridColumn211,
            this.colContactedByStaffID,
            this.colContactDirectionID,
            this.colLinkedRecordDescription1,
            this.gridColumn212,
            this.colContactedByStaffName,
            this.colContactMethod,
            this.colContactType,
            this.colStatusDescription,
            this.colContactDirectionDescription,
            this.gridColumn213,
            this.colLinkedRecordTypeDescription,
            this.colDateCreated1,
            this.colClientContact});
            this.gridViewCRM.GridControl = this.gridControlCRM;
            this.gridViewCRM.GroupCount = 1;
            this.gridViewCRM.Name = "gridViewCRM";
            this.gridViewCRM.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewCRM.OptionsFind.FindDelay = 2000;
            this.gridViewCRM.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewCRM.OptionsLayout.StoreAppearance = true;
            this.gridViewCRM.OptionsLayout.StoreFormatRules = true;
            this.gridViewCRM.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewCRM.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewCRM.OptionsSelection.MultiSelect = true;
            this.gridViewCRM.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewCRM.OptionsView.ColumnAutoWidth = false;
            this.gridViewCRM.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewCRM.OptionsView.ShowGroupPanel = false;
            this.gridViewCRM.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateCreated1, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewCRM.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewCRM.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewCRM.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewCRM.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewCRM.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewCRM.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewCRM.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewCRM_MouseUp);
            this.gridViewCRM.DoubleClick += new System.EventHandler(this.gridViewCRM_DoubleClick);
            this.gridViewCRM.GotFocus += new System.EventHandler(this.gridViewCRM_GotFocus);
            // 
            // colCRMID
            // 
            this.colCRMID.Caption = "CRM ID";
            this.colCRMID.FieldName = "CRMID";
            this.colCRMID.Name = "colCRMID";
            this.colCRMID.OptionsColumn.AllowEdit = false;
            this.colCRMID.OptionsColumn.AllowFocus = false;
            this.colCRMID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn208
            // 
            this.gridColumn208.Caption = "Client ID";
            this.gridColumn208.FieldName = "ClientID";
            this.gridColumn208.Name = "gridColumn208";
            this.gridColumn208.OptionsColumn.AllowEdit = false;
            this.gridColumn208.OptionsColumn.AllowFocus = false;
            this.gridColumn208.OptionsColumn.ReadOnly = true;
            // 
            // colClientContactID
            // 
            this.colClientContactID.Caption = "Client Contact ID";
            this.colClientContactID.FieldName = "ClientContactID";
            this.colClientContactID.Name = "colClientContactID";
            this.colClientContactID.OptionsColumn.AllowEdit = false;
            this.colClientContactID.OptionsColumn.AllowFocus = false;
            this.colClientContactID.OptionsColumn.ReadOnly = true;
            this.colClientContactID.Width = 100;
            // 
            // colLinkedToRecordTypeID1
            // 
            this.colLinkedToRecordTypeID1.Caption = "Linked To Record Type ID";
            this.colLinkedToRecordTypeID1.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID1.Name = "colLinkedToRecordTypeID1";
            this.colLinkedToRecordTypeID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID1.Width = 141;
            // 
            // colLinkedToRecordID1
            // 
            this.colLinkedToRecordID1.Caption = "Linked To Record ID";
            this.colLinkedToRecordID1.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID1.Name = "colLinkedToRecordID1";
            this.colLinkedToRecordID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID1.Width = 114;
            // 
            // colContactDueDateTime
            // 
            this.colContactDueDateTime.Caption = "Contact Due";
            this.colContactDueDateTime.ColumnEdit = this.repositoryItemTextDateTime;
            this.colContactDueDateTime.FieldName = "ContactDueDateTime";
            this.colContactDueDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContactDueDateTime.Name = "colContactDueDateTime";
            this.colContactDueDateTime.OptionsColumn.AllowEdit = false;
            this.colContactDueDateTime.OptionsColumn.AllowFocus = false;
            this.colContactDueDateTime.OptionsColumn.ReadOnly = true;
            this.colContactDueDateTime.Visible = true;
            this.colContactDueDateTime.VisibleIndex = 3;
            this.colContactDueDateTime.Width = 108;
            // 
            // repositoryItemTextDateTime
            // 
            this.repositoryItemTextDateTime.AutoHeight = false;
            this.repositoryItemTextDateTime.Mask.EditMask = "g";
            this.repositoryItemTextDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextDateTime.Name = "repositoryItemTextDateTime";
            // 
            // colContactMadeDateTime
            // 
            this.colContactMadeDateTime.Caption = "Contact Made";
            this.colContactMadeDateTime.ColumnEdit = this.repositoryItemTextDateTime;
            this.colContactMadeDateTime.FieldName = "ContactMadeDateTime";
            this.colContactMadeDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContactMadeDateTime.Name = "colContactMadeDateTime";
            this.colContactMadeDateTime.OptionsColumn.AllowEdit = false;
            this.colContactMadeDateTime.OptionsColumn.AllowFocus = false;
            this.colContactMadeDateTime.OptionsColumn.ReadOnly = true;
            this.colContactMadeDateTime.Visible = true;
            this.colContactMadeDateTime.VisibleIndex = 4;
            this.colContactMadeDateTime.Width = 102;
            // 
            // colContactMethodID
            // 
            this.colContactMethodID.Caption = "Contact Method ID";
            this.colContactMethodID.FieldName = "ContactMethodID";
            this.colContactMethodID.Name = "colContactMethodID";
            this.colContactMethodID.OptionsColumn.AllowEdit = false;
            this.colContactMethodID.OptionsColumn.AllowFocus = false;
            this.colContactMethodID.OptionsColumn.ReadOnly = true;
            this.colContactMethodID.Width = 109;
            // 
            // colContactTypeID
            // 
            this.colContactTypeID.Caption = "Contact Type ID";
            this.colContactTypeID.FieldName = "ContactTypeID";
            this.colContactTypeID.Name = "colContactTypeID";
            this.colContactTypeID.OptionsColumn.AllowEdit = false;
            this.colContactTypeID.OptionsColumn.AllowFocus = false;
            this.colContactTypeID.OptionsColumn.ReadOnly = true;
            this.colContactTypeID.Width = 97;
            // 
            // gridColumn209
            // 
            this.gridColumn209.Caption = "Description";
            this.gridColumn209.FieldName = "Description";
            this.gridColumn209.Name = "gridColumn209";
            this.gridColumn209.OptionsColumn.AllowEdit = false;
            this.gridColumn209.OptionsColumn.AllowFocus = false;
            this.gridColumn209.OptionsColumn.ReadOnly = true;
            this.gridColumn209.Visible = true;
            this.gridColumn209.VisibleIndex = 7;
            this.gridColumn209.Width = 260;
            // 
            // gridColumn210
            // 
            this.gridColumn210.Caption = "Remarks";
            this.gridColumn210.ColumnEdit = this.repositoryItemMemoExEdit15;
            this.gridColumn210.FieldName = "Remarks";
            this.gridColumn210.Name = "gridColumn210";
            this.gridColumn210.OptionsColumn.ReadOnly = true;
            this.gridColumn210.Visible = true;
            this.gridColumn210.VisibleIndex = 12;
            // 
            // repositoryItemMemoExEdit15
            // 
            this.repositoryItemMemoExEdit15.AutoHeight = false;
            this.repositoryItemMemoExEdit15.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit15.Name = "repositoryItemMemoExEdit15";
            this.repositoryItemMemoExEdit15.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit15.ShowIcon = false;
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn211
            // 
            this.gridColumn211.Caption = "Created By Staff ID";
            this.gridColumn211.FieldName = "CreatedByStaffID";
            this.gridColumn211.Name = "gridColumn211";
            this.gridColumn211.OptionsColumn.AllowEdit = false;
            this.gridColumn211.OptionsColumn.AllowFocus = false;
            this.gridColumn211.OptionsColumn.ReadOnly = true;
            this.gridColumn211.Width = 113;
            // 
            // colContactedByStaffID
            // 
            this.colContactedByStaffID.Caption = "Contacted By";
            this.colContactedByStaffID.FieldName = "ContactedByStaffID";
            this.colContactedByStaffID.Name = "colContactedByStaffID";
            this.colContactedByStaffID.OptionsColumn.AllowEdit = false;
            this.colContactedByStaffID.OptionsColumn.AllowFocus = false;
            this.colContactedByStaffID.OptionsColumn.ReadOnly = true;
            this.colContactedByStaffID.Width = 83;
            // 
            // colContactDirectionID
            // 
            this.colContactDirectionID.Caption = "Contact Direction ID";
            this.colContactDirectionID.FieldName = "ContactDirectionID";
            this.colContactDirectionID.Name = "colContactDirectionID";
            this.colContactDirectionID.OptionsColumn.AllowEdit = false;
            this.colContactDirectionID.OptionsColumn.AllowFocus = false;
            this.colContactDirectionID.OptionsColumn.ReadOnly = true;
            this.colContactDirectionID.Width = 101;
            // 
            // colLinkedRecordDescription1
            // 
            this.colLinkedRecordDescription1.Caption = "Linked To Tender";
            this.colLinkedRecordDescription1.ColumnEdit = this.repositoryItemTextEditHTML12;
            this.colLinkedRecordDescription1.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription1.Name = "colLinkedRecordDescription1";
            this.colLinkedRecordDescription1.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription1.Width = 191;
            // 
            // repositoryItemTextEditHTML12
            // 
            this.repositoryItemTextEditHTML12.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML12.AutoHeight = false;
            this.repositoryItemTextEditHTML12.Name = "repositoryItemTextEditHTML12";
            // 
            // gridColumn212
            // 
            this.gridColumn212.Caption = "Created By";
            this.gridColumn212.FieldName = "CreatedByStaffName";
            this.gridColumn212.Name = "gridColumn212";
            this.gridColumn212.OptionsColumn.AllowEdit = false;
            this.gridColumn212.OptionsColumn.AllowFocus = false;
            this.gridColumn212.OptionsColumn.ReadOnly = true;
            this.gridColumn212.Visible = true;
            this.gridColumn212.VisibleIndex = 11;
            this.gridColumn212.Width = 121;
            // 
            // colContactedByStaffName
            // 
            this.colContactedByStaffName.Caption = "GC Contact Person";
            this.colContactedByStaffName.FieldName = "ContactedByStaffName";
            this.colContactedByStaffName.Name = "colContactedByStaffName";
            this.colContactedByStaffName.OptionsColumn.AllowEdit = false;
            this.colContactedByStaffName.OptionsColumn.AllowFocus = false;
            this.colContactedByStaffName.OptionsColumn.ReadOnly = true;
            this.colContactedByStaffName.Visible = true;
            this.colContactedByStaffName.VisibleIndex = 5;
            this.colContactedByStaffName.Width = 133;
            // 
            // colContactMethod
            // 
            this.colContactMethod.Caption = "Contact Method";
            this.colContactMethod.FieldName = "ContactMethod";
            this.colContactMethod.Name = "colContactMethod";
            this.colContactMethod.OptionsColumn.AllowEdit = false;
            this.colContactMethod.OptionsColumn.AllowFocus = false;
            this.colContactMethod.OptionsColumn.ReadOnly = true;
            this.colContactMethod.Visible = true;
            this.colContactMethod.VisibleIndex = 8;
            this.colContactMethod.Width = 104;
            // 
            // colContactType
            // 
            this.colContactType.Caption = "Contact Type";
            this.colContactType.FieldName = "ContactType";
            this.colContactType.Name = "colContactType";
            this.colContactType.OptionsColumn.AllowEdit = false;
            this.colContactType.OptionsColumn.AllowFocus = false;
            this.colContactType.OptionsColumn.ReadOnly = true;
            this.colContactType.Visible = true;
            this.colContactType.VisibleIndex = 9;
            this.colContactType.Width = 102;
            // 
            // colStatusDescription
            // 
            this.colStatusDescription.Caption = "Status";
            this.colStatusDescription.FieldName = "StatusDescription";
            this.colStatusDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStatusDescription.Name = "colStatusDescription";
            this.colStatusDescription.OptionsColumn.AllowEdit = false;
            this.colStatusDescription.OptionsColumn.AllowFocus = false;
            this.colStatusDescription.OptionsColumn.ReadOnly = true;
            this.colStatusDescription.Visible = true;
            this.colStatusDescription.VisibleIndex = 1;
            // 
            // colContactDirectionDescription
            // 
            this.colContactDirectionDescription.Caption = "Contact Direction";
            this.colContactDirectionDescription.FieldName = "ContactDirectionDescription";
            this.colContactDirectionDescription.Name = "colContactDirectionDescription";
            this.colContactDirectionDescription.OptionsColumn.AllowEdit = false;
            this.colContactDirectionDescription.OptionsColumn.AllowFocus = false;
            this.colContactDirectionDescription.OptionsColumn.ReadOnly = true;
            this.colContactDirectionDescription.Visible = true;
            this.colContactDirectionDescription.VisibleIndex = 10;
            this.colContactDirectionDescription.Width = 101;
            // 
            // gridColumn213
            // 
            this.gridColumn213.Caption = "Client";
            this.gridColumn213.FieldName = "ClientName";
            this.gridColumn213.Name = "gridColumn213";
            this.gridColumn213.OptionsColumn.AllowEdit = false;
            this.gridColumn213.OptionsColumn.AllowFocus = false;
            this.gridColumn213.OptionsColumn.ReadOnly = true;
            this.gridColumn213.Visible = true;
            this.gridColumn213.VisibleIndex = 2;
            this.gridColumn213.Width = 204;
            // 
            // colLinkedRecordTypeDescription
            // 
            this.colLinkedRecordTypeDescription.Caption = "Linked Record Type";
            this.colLinkedRecordTypeDescription.FieldName = "LinkedRecordTypeDescription";
            this.colLinkedRecordTypeDescription.Name = "colLinkedRecordTypeDescription";
            this.colLinkedRecordTypeDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordTypeDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordTypeDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordTypeDescription.Width = 112;
            // 
            // colDateCreated1
            // 
            this.colDateCreated1.Caption = "Date Created";
            this.colDateCreated1.ColumnEdit = this.repositoryItemTextDateTime;
            this.colDateCreated1.FieldName = "DateCreated";
            this.colDateCreated1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateCreated1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateCreated1.Name = "colDateCreated1";
            this.colDateCreated1.OptionsColumn.AllowEdit = false;
            this.colDateCreated1.OptionsColumn.AllowFocus = false;
            this.colDateCreated1.OptionsColumn.ReadOnly = true;
            this.colDateCreated1.Visible = true;
            this.colDateCreated1.VisibleIndex = 0;
            this.colDateCreated1.Width = 120;
            // 
            // colClientContact
            // 
            this.colClientContact.Caption = "Client Contact Person";
            this.colClientContact.FieldName = "ClientContact";
            this.colClientContact.Name = "colClientContact";
            this.colClientContact.OptionsColumn.AllowEdit = false;
            this.colClientContact.OptionsColumn.AllowFocus = false;
            this.colClientContact.OptionsColumn.ReadOnly = true;
            this.colClientContact.Visible = true;
            this.colClientContact.VisibleIndex = 6;
            this.colClientContact.Width = 122;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.SingleClick = true;
            // 
            // xtraTabPageLinkedDocuments
            // 
            this.xtraTabPageLinkedDocuments.Controls.Add(this.gridControl3);
            this.xtraTabPageLinkedDocuments.ImageOptions.Image = global::WoodPlan5.Properties.Resources.linked_documents_16_16;
            this.xtraTabPageLinkedDocuments.Name = "xtraTabPageLinkedDocuments";
            this.xtraTabPageLinkedDocuments.Size = new System.Drawing.Size(1331, 180);
            this.xtraTabPageLinkedDocuments.Tag = "1";
            this.xtraTabPageLinkedDocuments.Text = "Linked Documents";
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp06514OMLinkedDocumentsLinkedToRecordBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 13, true, true, "Reload Data", "reload"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 21, true, true, "Email Selected Linked Document(s)", "email")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemHyperLinkEdit2,
            this.repositoryItemTextEditDateTime4});
            this.gridControl3.Size = new System.Drawing.Size(1331, 180);
            this.gridControl3.TabIndex = 2;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp06514OMLinkedDocumentsLinkedToRecordBindingSource
            // 
            this.sp06514OMLinkedDocumentsLinkedToRecordBindingSource.DataMember = "sp06514_OM_Linked_Documents_Linked_To_Record";
            this.sp06514OMLinkedDocumentsLinkedToRecordBindingSource.DataSource = this.dataSet_OM_Core;
            // 
            // dataSet_OM_Core
            // 
            this.dataSet_OM_Core.DataSetName = "DataSet_OM_Core";
            this.dataSet_OM_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.colDescription,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecord,
            this.colAddedByStaff,
            this.colDocumentRemarks,
            this.colLinkedToRecordParent});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToRecordParent, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedDocumentTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 130;
            // 
            // repositoryItemTextEditDateTime4
            // 
            this.repositoryItemTextEditDateTime4.AutoHeight = false;
            this.repositoryItemTextEditDateTime4.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime4.Name = "repositoryItemTextEditDateTime4";
            // 
            // colLinkedRecord
            // 
            this.colLinkedRecord.Caption = "Linked To";
            this.colLinkedRecord.FieldName = "LinkedToRecord";
            this.colLinkedRecord.Name = "colLinkedRecord";
            this.colLinkedRecord.OptionsColumn.AllowEdit = false;
            this.colLinkedRecord.OptionsColumn.AllowFocus = false;
            this.colLinkedRecord.OptionsColumn.ReadOnly = true;
            this.colLinkedRecord.Width = 131;
            // 
            // colAddedByStaff
            // 
            this.colAddedByStaff.Caption = "Added By";
            this.colAddedByStaff.FieldName = "AddedByStaff";
            this.colAddedByStaff.Name = "colAddedByStaff";
            this.colAddedByStaff.OptionsColumn.AllowEdit = false;
            this.colAddedByStaff.OptionsColumn.AllowFocus = false;
            this.colAddedByStaff.OptionsColumn.ReadOnly = true;
            this.colAddedByStaff.Visible = true;
            this.colAddedByStaff.VisibleIndex = 4;
            this.colAddedByStaff.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colDocumentRemarks.FieldName = "Remarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 5;
            this.colDocumentRemarks.Width = 134;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colLinkedToRecordParent
            // 
            this.colLinkedToRecordParent.Caption = "Linked To Tender";
            this.colLinkedToRecordParent.FieldName = "LinkedToRecordParent";
            this.colLinkedToRecordParent.Name = "colLinkedToRecordParent";
            this.colLinkedToRecordParent.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordParent.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordParent.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordParent.Visible = true;
            this.colLinkedToRecordParent.VisibleIndex = 6;
            this.colLinkedToRecordParent.Width = 375;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_OM_Client_PO
            // 
            this.dataSet_OM_Client_PO.DataSetName = "DataSet_OM_Client_PO";
            this.dataSet_OM_Client_PO.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_Common_Functionality
            // 
            this.dataSet_Common_Functionality.DataSetName = "DataSet_Common_Functionality";
            this.dataSet_Common_Functionality.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bar1
            // 
            this.bar1.BarName = "GrittingCalloutToolbar";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(519, 261);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiToolbarAdd, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiStatus, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSendVisits, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.beiLoadingProgress, "", true, true, true, 83),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLoadingCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiVisitDrillDown, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewSiteOnMap, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiFilterSelected, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSelectedCount, true)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Gritting Callout Toolbar";
            // 
            // bsiToolbarAdd
            // 
            this.bsiToolbarAdd.Caption = "Add Tender";
            this.bsiToolbarAdd.Id = 141;
            this.bsiToolbarAdd.ImageOptions.ImageIndex = 2;
            this.bsiToolbarAdd.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiToolbarAdd),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiToolbarAddFromTemplate)});
            this.bsiToolbarAdd.Name = "bsiToolbarAdd";
            this.bsiToolbarAdd.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiToolbarAdd
            // 
            this.bbiToolbarAdd.Caption = "Add New Tender";
            this.bbiToolbarAdd.Id = 142;
            this.bbiToolbarAdd.ImageOptions.ImageIndex = 2;
            this.bbiToolbarAdd.Name = "bbiToolbarAdd";
            this.bbiToolbarAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiToolbarAdd_ItemClick);
            // 
            // bbiToolbarAddFromTemplate
            // 
            this.bbiToolbarAddFromTemplate.Caption = "Add New Tender Using Template";
            this.bbiToolbarAddFromTemplate.Id = 143;
            this.bbiToolbarAddFromTemplate.ImageOptions.ImageIndex = 6;
            this.bbiToolbarAddFromTemplate.Name = "bbiToolbarAddFromTemplate";
            this.bbiToolbarAddFromTemplate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiToolbarAddFromTemplate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiToolbarAddFromTemplate_ItemClick);
            // 
            // bsiStatus
            // 
            this.bsiStatus.Caption = "Status";
            this.bsiStatus.Id = 128;
            this.bsiStatus.ImageOptions.ImageIndex = 5;
            this.bsiStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSendQuoteToCM),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCMReturnedQuote),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiKAMAuthorised),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSentToClient),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiClientResponse),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiVisitJobWizard),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTenderClosed, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTenderCancelled, true)});
            this.bsiStatus.Name = "bsiStatus";
            this.bsiStatus.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bsiSendQuoteToCM
            // 
            this.bsiSendQuoteToCM.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSendQuoteToCM.Caption = "<b>Send</b> Quote to <b>CM</b>";
            this.bsiSendQuoteToCM.Id = 155;
            this.bsiSendQuoteToCM.ImageOptions.ImageIndex = 7;
            this.bsiSendQuoteToCM.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSendQuoteToCM),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiQuoteNotRequired)});
            this.bsiSendQuoteToCM.Name = "bsiSendQuoteToCM";
            // 
            // bbiSendQuoteToCM
            // 
            this.bbiSendQuoteToCM.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSendQuoteToCM.Caption = "<b>Send</b> Quote to <b>CM</b>";
            this.bbiSendQuoteToCM.Enabled = false;
            this.bbiSendQuoteToCM.Id = 126;
            this.bbiSendQuoteToCM.ImageOptions.ImageIndex = 7;
            this.bbiSendQuoteToCM.Name = "bbiSendQuoteToCM";
            this.bbiSendQuoteToCM.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiSendQuoteToCM.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSendQuoteToCM_ItemClick);
            // 
            // bbiQuoteNotRequired
            // 
            this.bbiQuoteNotRequired.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiQuoteNotRequired.Caption = "<b>No</b> Quote Required";
            this.bbiQuoteNotRequired.Id = 156;
            this.bbiQuoteNotRequired.ImageOptions.ImageIndex = 22;
            this.bbiQuoteNotRequired.Name = "bbiQuoteNotRequired";
            this.bbiQuoteNotRequired.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiQuoteNotRequired_ItemClick);
            // 
            // bbiCMReturnedQuote
            // 
            this.bbiCMReturnedQuote.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiCMReturnedQuote.Caption = "<b>CM Returned</b> Quote ";
            this.bbiCMReturnedQuote.Id = 129;
            this.bbiCMReturnedQuote.ImageOptions.ImageIndex = 8;
            this.bbiCMReturnedQuote.Name = "bbiCMReturnedQuote";
            this.bbiCMReturnedQuote.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCMReturnedQuote_ItemClick);
            // 
            // bsiKAMAuthorised
            // 
            this.bsiKAMAuthorised.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiKAMAuthorised.Caption = "<b>KAM</b> Authorised";
            this.bsiKAMAuthorised.Id = 146;
            this.bsiKAMAuthorised.ImageOptions.ImageIndex = 12;
            this.bsiKAMAuthorised.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiKAMAccepted),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiKAMRejected)});
            this.bsiKAMAuthorised.Name = "bsiKAMAuthorised";
            // 
            // bbiKAMAccepted
            // 
            this.bbiKAMAccepted.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiKAMAccepted.Caption = "KAM <b>Accepted</b>";
            this.bbiKAMAccepted.Id = 147;
            this.bbiKAMAccepted.ImageOptions.ImageIndex = 13;
            this.bbiKAMAccepted.Name = "bbiKAMAccepted";
            this.bbiKAMAccepted.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiKAMAccepted_ItemClick);
            // 
            // bbiKAMRejected
            // 
            this.bbiKAMRejected.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiKAMRejected.Caption = "KAM <b>Rejected</b>...";
            this.bbiKAMRejected.Id = 148;
            this.bbiKAMRejected.ImageOptions.ImageIndex = 14;
            this.bbiKAMRejected.Name = "bbiKAMRejected";
            this.bbiKAMRejected.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiKAMRejected_ItemClick);
            // 
            // bbiSentToClient
            // 
            this.bbiSentToClient.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSentToClient.Caption = "<b>Sent</b> To <b>Client</b>";
            this.bbiSentToClient.Id = 149;
            this.bbiSentToClient.ImageOptions.ImageIndex = 15;
            this.bbiSentToClient.Name = "bbiSentToClient";
            this.bbiSentToClient.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSentToClient_ItemClick);
            // 
            // bsiClientResponse
            // 
            this.bsiClientResponse.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiClientResponse.Caption = "Client <b>Response</b>";
            this.bsiClientResponse.Id = 150;
            this.bsiClientResponse.ImageOptions.ImageIndex = 20;
            this.bsiClientResponse.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClientAccepted),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClientRejected),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClientOnHold)});
            this.bsiClientResponse.Name = "bsiClientResponse";
            // 
            // bbiClientAccepted
            // 
            this.bbiClientAccepted.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiClientAccepted.Caption = "Client <b>Accepted</b>";
            this.bbiClientAccepted.Id = 151;
            this.bbiClientAccepted.ImageOptions.ImageIndex = 16;
            this.bbiClientAccepted.Name = "bbiClientAccepted";
            this.bbiClientAccepted.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClientAccepted_ItemClick);
            // 
            // bbiClientRejected
            // 
            this.bbiClientRejected.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiClientRejected.Caption = "Client <b>Rejected</b>...";
            this.bbiClientRejected.Id = 152;
            this.bbiClientRejected.ImageOptions.ImageIndex = 18;
            this.bbiClientRejected.Name = "bbiClientRejected";
            this.bbiClientRejected.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClientRejected_ItemClick);
            // 
            // bbiClientOnHold
            // 
            this.bbiClientOnHold.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiClientOnHold.Caption = "Client <b>On-Hold</b>";
            this.bbiClientOnHold.Id = 153;
            this.bbiClientOnHold.ImageOptions.ImageIndex = 17;
            this.bbiClientOnHold.Name = "bbiClientOnHold";
            this.bbiClientOnHold.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClientOnHold_ItemClick);
            // 
            // bsiVisitJobWizard
            // 
            this.bsiVisitJobWizard.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiVisitJobWizard.Caption = "Create <b>Visits</b> \\ <b>Jobs</b>";
            this.bsiVisitJobWizard.Id = 157;
            this.bsiVisitJobWizard.ImageOptions.ImageIndex = 4;
            this.bsiVisitJobWizard.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddVisitWizard),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddJobWizard)});
            this.bsiVisitJobWizard.Name = "bsiVisitJobWizard";
            // 
            // bbiAddVisitWizard
            // 
            this.bbiAddVisitWizard.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiAddVisitWizard.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiAddVisitWizard.Caption = "<b>Visit</b> Wizard";
            this.bbiAddVisitWizard.Id = 42;
            this.bbiAddVisitWizard.ImageOptions.ImageIndex = 4;
            this.bbiAddVisitWizard.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiAddVisitWizard.ImageOptions.LargeImage")));
            this.bbiAddVisitWizard.Name = "bbiAddVisitWizard";
            this.bbiAddVisitWizard.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Visit Wizard - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to open the <b>Visit Wizard</b> screen.\r\n\r\nThe Wizard will guide you ste" +
    "p-by-step through the visit creation process.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiAddVisitWizard.SuperTip = superToolTip1;
            this.bbiAddVisitWizard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddVisitWizard_ItemClick);
            // 
            // bbiAddJobWizard
            // 
            this.bbiAddJobWizard.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiAddJobWizard.Caption = "<b>Job</b> Wizard";
            this.bbiAddJobWizard.Id = 121;
            this.bbiAddJobWizard.ImageOptions.ImageIndex = 4;
            this.bbiAddJobWizard.Name = "bbiAddJobWizard";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Job Wizard - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to open the <b>Job Wizard</b> screen.\r\n\r\nThe Wizard will guide you step-" +
    "by-step through the job creation process.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiAddJobWizard.SuperTip = superToolTip2;
            this.bbiAddJobWizard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddJobWizard_ItemClick);
            // 
            // bbiTenderClosed
            // 
            this.bbiTenderClosed.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiTenderClosed.Caption = "Tender <b> Closed</b>";
            this.bbiTenderClosed.Id = 144;
            this.bbiTenderClosed.ImageOptions.ImageIndex = 11;
            this.bbiTenderClosed.Name = "bbiTenderClosed";
            this.bbiTenderClosed.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTenderClosed_ItemClick);
            // 
            // bbiTenderCancelled
            // 
            this.bbiTenderCancelled.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiTenderCancelled.Caption = "Tender <b>Cancelled</b>...";
            this.bbiTenderCancelled.Id = 154;
            this.bbiTenderCancelled.ImageOptions.ImageIndex = 21;
            this.bbiTenderCancelled.Name = "bbiTenderCancelled";
            this.bbiTenderCancelled.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTenderCancelled_ItemClick);
            // 
            // bsiSendVisits
            // 
            this.bsiSendVisits.Caption = "Send Visits";
            this.bsiSendVisits.Id = 117;
            this.bsiSendVisits.ImageOptions.ImageIndex = 19;
            this.bsiSendVisits.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectJobsReadyToSend),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSendJobs, true)});
            this.bsiSendVisits.Name = "bsiSendVisits";
            this.bsiSendVisits.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiSelectJobsReadyToSend
            // 
            this.bbiSelectJobsReadyToSend.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSelectJobsReadyToSend.Caption = "<b>Select</b> \'Ready to Send\' Visits";
            this.bbiSelectJobsReadyToSend.Id = 118;
            this.bbiSelectJobsReadyToSend.ImageOptions.ImageIndex = 10;
            this.bbiSelectJobsReadyToSend.Name = "bbiSelectJobsReadyToSend";
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Select \'Ready To Send\' Visits - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to select all visits which are ready to send. Visits selected must have " +
    "at least one job with one or more labour records attached.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiSelectJobsReadyToSend.SuperTip = superToolTip3;
            this.bbiSelectJobsReadyToSend.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectJobsReadyToSend_ItemClick);
            // 
            // bbiSendJobs
            // 
            this.bbiSendJobs.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSendJobs.Caption = "<b>Send</b> Visits";
            this.bbiSendJobs.Id = 119;
            this.bbiSendJobs.ImageOptions.ImageIndex = 19;
            this.bbiSendJobs.Name = "bbiSendJobs";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Send Visits - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = resources.GetString("toolTipItem4.Text");
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiSendJobs.SuperTip = superToolTip4;
            this.bbiSendJobs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSendJobs_ItemClick);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 58;
            this.bbiRefresh.ImageOptions.ImageIndex = 3;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // beiLoadingProgress
            // 
            this.beiLoadingProgress.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.beiLoadingProgress.Caption = "Loading Data Progress";
            this.beiLoadingProgress.Edit = this.repositoryItemMarqueeProgressBar1;
            this.beiLoadingProgress.EditValue = "Loading Data...";
            this.beiLoadingProgress.Id = 136;
            this.beiLoadingProgress.Name = "beiLoadingProgress";
            this.beiLoadingProgress.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // repositoryItemMarqueeProgressBar1
            // 
            this.repositoryItemMarqueeProgressBar1.MarqueeWidth = 20;
            this.repositoryItemMarqueeProgressBar1.Name = "repositoryItemMarqueeProgressBar1";
            this.repositoryItemMarqueeProgressBar1.ShowTitle = true;
            // 
            // bbiLoadingCancel
            // 
            this.bbiLoadingCancel.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiLoadingCancel.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiLoadingCancel.Caption = "Cancel Load";
            this.bbiLoadingCancel.Id = 138;
            this.bbiLoadingCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiLoadingCancel.ImageOptions.Image")));
            this.bbiLoadingCancel.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiLoadingCancel.ImageOptions.LargeImage")));
            this.bbiLoadingCancel.Name = "bbiLoadingCancel";
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Cancel Load - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to cancel the current data loading operation.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bbiLoadingCancel.SuperTip = superToolTip5;
            this.bbiLoadingCancel.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiLoadingCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLoadingCancel_ItemClick);
            // 
            // bbiVisitDrillDown
            // 
            this.bbiVisitDrillDown.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiVisitDrillDown.Caption = "Visits";
            this.bbiVisitDrillDown.Id = 158;
            this.bbiVisitDrillDown.ImageOptions.ImageIndex = 23;
            this.bbiVisitDrillDown.Name = "bbiVisitDrillDown";
            this.bbiVisitDrillDown.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Visits Drill Down - information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to drill down to the Visit Manager.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.bbiVisitDrillDown.SuperTip = superToolTip6;
            this.bbiVisitDrillDown.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiVisitDrillDown_ItemClick);
            // 
            // bbiViewSiteOnMap
            // 
            this.bbiViewSiteOnMap.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiViewSiteOnMap.AllowHtmlText = DevExpress.Utils.DefaultBoolean.False;
            this.bbiViewSiteOnMap.Caption = "Mapping";
            this.bbiViewSiteOnMap.Id = 67;
            this.bbiViewSiteOnMap.ImageOptions.ImageIndex = 1;
            this.bbiViewSiteOnMap.Name = "bbiViewSiteOnMap";
            this.bbiViewSiteOnMap.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "View Tender Site on Map - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to View the Site Location on the Map.\r\n\r\n<b>Note:</b> An internet connec" +
    "tion is required.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.bbiViewSiteOnMap.SuperTip = superToolTip7;
            this.bbiViewSiteOnMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewSiteOnMap_ItemClick);
            // 
            // bsiFilterSelected
            // 
            this.bsiFilterSelected.Caption = "Filter Selected";
            this.bsiFilterSelected.Id = 122;
            this.bsiFilterSelected.ImageOptions.ImageIndex = 0;
            this.bsiFilterSelected.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterTenderSelected, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterVisitsSelected)});
            this.bsiFilterSelected.Name = "bsiFilterSelected";
            this.bsiFilterSelected.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            // 
            // bciFilterTenderSelected
            // 
            this.bciFilterTenderSelected.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bciFilterTenderSelected.Caption = "Filter Selected <b>Tenders</b>";
            this.bciFilterTenderSelected.Id = 113;
            this.bciFilterTenderSelected.ImageOptions.ImageIndex = 0;
            this.bciFilterTenderSelected.Name = "bciFilterTenderSelected";
            toolTipTitleItem8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Text = "Filter Selected Tenders - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = resources.GetString("toolTipItem8.Text");
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bciFilterTenderSelected.SuperTip = superToolTip8;
            this.bciFilterTenderSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterTenderSelected_CheckedChanged);
            // 
            // bciFilterVisitsSelected
            // 
            this.bciFilterVisitsSelected.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bciFilterVisitsSelected.Caption = "Filter Selected <b>Visits</b>";
            this.bciFilterVisitsSelected.Id = 124;
            this.bciFilterVisitsSelected.ImageOptions.ImageIndex = 0;
            this.bciFilterVisitsSelected.Name = "bciFilterVisitsSelected";
            toolTipTitleItem9.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Text = "Filter Selected Visits - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = resources.GetString("toolTipItem9.Text");
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.bciFilterVisitsSelected.SuperTip = superToolTip9;
            this.bciFilterVisitsSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterVisitsSelected_CheckedChanged);
            // 
            // bsiSelectedCount
            // 
            this.bsiSelectedCount.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSelectedCount.Caption = "0 Visits Selected";
            this.bsiSelectedCount.Id = 116;
            this.bsiSelectedCount.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_16x16;
            this.bsiSelectedCount.ItemAppearance.Disabled.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Disabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.Name = "bsiSelectedCount";
            this.bsiSelectedCount.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiSelectedCount.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // repositoryItemCheckEditIgnoreHistoric
            // 
            this.repositoryItemCheckEditIgnoreHistoric.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditIgnoreHistoric.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditIgnoreHistoric.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditIgnoreHistoric.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditIgnoreHistoric.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditIgnoreHistoric.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditIgnoreHistoric.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditIgnoreHistoric.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditIgnoreHistoric.AutoHeight = false;
            this.repositoryItemCheckEditIgnoreHistoric.Caption = "Check";
            this.repositoryItemCheckEditIgnoreHistoric.Name = "repositoryItemCheckEditIgnoreHistoric";
            this.repositoryItemCheckEditIgnoreHistoric.ValueChecked = 1;
            this.repositoryItemCheckEditIgnoreHistoric.ValueUnchecked = 0;
            // 
            // bbiImport
            // 
            this.bbiImport.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiImport.Caption = "Import";
            this.bbiImport.Id = 59;
            this.bbiImport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiImport.ImageOptions.Image")));
            this.bbiImport.Name = "bbiImport";
            this.bbiImport.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            // 
            // sp06145OMVisitEditBindingSource
            // 
            this.sp06145OMVisitEditBindingSource.DataMember = "sp06145_OM_Visit_Edit";
            this.sp06145OMVisitEditBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemDuration1
            // 
            this.repositoryItemDuration1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDuration1.AutoHeight = false;
            this.repositoryItemDuration1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDuration1.DisabledStateText = null;
            this.repositoryItemDuration1.Name = "repositoryItemDuration1";
            this.repositoryItemDuration1.NullValuePromptShowForEmptyValue = true;
            this.repositoryItemDuration1.ShowEmptyItem = true;
            this.repositoryItemDuration1.ValidateOnEnterKey = true;
            // 
            // toggleRecurrenceItem1
            // 
            this.toggleRecurrenceItem1.Id = 105;
            this.toggleRecurrenceItem1.Name = "toggleRecurrenceItem1";
            // 
            // sp06145_OM_Visit_EditTableAdapter
            // 
            this.sp06145_OM_Visit_EditTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            this.imageCollection2.InsertGalleryImage("masterfilter_32x32.png", "images/filter/masterfilter_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/filter/masterfilter_32x32.png"), 0);
            this.imageCollection2.Images.SetKeyName(0, "masterfilter_32x32.png");
            this.imageCollection2.InsertGalleryImage("geopointmap_32x32.png", "images/maps/geopointmap_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/maps/geopointmap_32x32.png"), 1);
            this.imageCollection2.Images.SetKeyName(1, "geopointmap_32x32.png");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.Add_32x32, "Add_32x32", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection2.Images.SetKeyName(2, "Add_32x32");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.refresh_32x32, "refresh_32x32", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection2.Images.SetKeyName(3, "refresh_32x32");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.wizard_32_32, "wizard_32_32", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection2.Images.SetKeyName(4, "wizard_32_32");
            this.imageCollection2.Images.SetKeyName(5, "progress_32.png");
            this.imageCollection2.InsertGalleryImage("addgroupheader_32x32.png", "images/reports/addgroupheader_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/reports/addgroupheader_32x32.png"), 6);
            this.imageCollection2.Images.SetKeyName(6, "addgroupheader_32x32.png");
            this.imageCollection2.Images.SetKeyName(7, "Employee_send_quote_32_.png");
            this.imageCollection2.Images.SetKeyName(8, "Employee_receive_quote_32_.png");
            this.imageCollection2.InsertGalleryImage("issue_32x32.png", "images/support/issue_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/support/issue_32x32.png"), 9);
            this.imageCollection2.Images.SetKeyName(9, "issue_32x32.png");
            this.imageCollection2.InsertGalleryImage("contentarrangeinrows_32x32.png", "images/alignment/contentarrangeinrows_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/alignment/contentarrangeinrows_32x32.png"), 10);
            this.imageCollection2.Images.SetKeyName(10, "contentarrangeinrows_32x32.png");
            this.imageCollection2.Images.SetKeyName(11, "Finish_32.png");
            this.imageCollection2.Images.SetKeyName(12, "employee2_question_32.png");
            this.imageCollection2.Images.SetKeyName(13, "employee2_accept_32.png");
            this.imageCollection2.Images.SetKeyName(14, "employee2_reject_32.png");
            this.imageCollection2.Images.SetKeyName(15, "customer_send_quote_32.png");
            this.imageCollection2.Images.SetKeyName(16, "customer_accept_quote_32.png");
            this.imageCollection2.Images.SetKeyName(17, "customer_on_hold_quote_32.png");
            this.imageCollection2.Images.SetKeyName(18, "customer_reject_quote_32.png");
            this.imageCollection2.Images.SetKeyName(19, "team_send_32x32.png");
            this.imageCollection2.Images.SetKeyName(20, "customer_question_32.png");
            this.imageCollection2.InsertGalleryImage("deletegroupheader_32x32.png", "images/reports/deletegroupheader_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/reports/deletegroupheader_32x32.png"), 21);
            this.imageCollection2.Images.SetKeyName(21, "deletegroupheader_32x32.png");
            this.imageCollection2.Images.SetKeyName(22, "quote_not_applicable_32.png");
            this.imageCollection2.Images.SetKeyName(23, "drill_down_32.png");
            // 
            // sp05089_CRM_Contacts_Linked_To_RecordTableAdapter
            // 
            this.sp05089_CRM_Contacts_Linked_To_RecordTableAdapter.ClearBeforeFill = true;
            // 
            // dockManager1
            // 
            this.dockManager1.AutoHideContainers.AddRange(new DevExpress.XtraBars.Docking.AutoHideContainer[] {
            this.hideContainerRight,
            this.hideContainerLeft});
            this.dockManager1.Form = this;
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // hideContainerRight
            // 
            this.hideContainerRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.hideContainerRight.Controls.Add(this.dockPanelVisitDetails);
            this.hideContainerRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.hideContainerRight.Location = new System.Drawing.Point(1359, 0);
            this.hideContainerRight.Name = "hideContainerRight";
            this.hideContainerRight.Size = new System.Drawing.Size(23, 654);
            // 
            // dockPanelVisitDetails
            // 
            this.dockPanelVisitDetails.Controls.Add(this.controlContainer1);
            customHeaderButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("customHeaderButtonImageOptions1.Image")));
            toolTipTitleItem10.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Text = "Sync Grid - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = resources.GetString("toolTipItem10.Text");
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.dockPanelVisitDetails.CustomHeaderButtons.AddRange(new DevExpress.XtraBars.Docking2010.IButton[] {
            new DevExpress.XtraBars.Docking.CustomHeaderButton("Sync Grid", false, customHeaderButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.CheckButton, "", -1, true, superToolTip10, true, false, true, serializableAppearanceObject9, "sync", -1)});
            this.dockPanelVisitDetails.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanelVisitDetails.ID = new System.Guid("915e9f1c-8413-4b34-8074-cde937e8c0a3");
            this.dockPanelVisitDetails.Location = new System.Drawing.Point(0, 0);
            this.dockPanelVisitDetails.Name = "dockPanelVisitDetails";
            this.dockPanelVisitDetails.Options.ShowCloseButton = false;
            this.dockPanelVisitDetails.Options.ShowMaximizeButton = false;
            this.dockPanelVisitDetails.OriginalSize = new System.Drawing.Size(550, 500);
            this.dockPanelVisitDetails.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanelVisitDetails.SavedIndex = 0;
            this.dockPanelVisitDetails.Size = new System.Drawing.Size(550, 654);
            this.dockPanelVisitDetails.Text = "Selected Tender Details";
            this.dockPanelVisitDetails.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            this.dockPanelVisitDetails.CustomButtonUnchecked += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.dockPanelVisitDetails_CustomButtonUnchecked);
            this.dockPanelVisitDetails.CustomButtonChecked += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.dockPanelVisitDetails_CustomButtonChecked);
            this.dockPanelVisitDetails.Expanded += new DevExpress.XtraBars.Docking.DockPanelEventHandler(this.dockPanelVisitDetails_Expanded);
            // 
            // controlContainer1
            // 
            this.controlContainer1.Controls.Add(this.dataLayoutControl1);
            this.controlContainer1.Location = new System.Drawing.Point(4, 34);
            this.controlContainer1.Name = "controlContainer1";
            this.controlContainer1.Size = new System.Drawing.Size(543, 617);
            this.controlContainer1.TabIndex = 0;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.LinkedJobCountTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedVisitCountTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientCommentsMemoExEdit);
            this.dataLayoutControl1.Controls.Add(this.CMCommentsMemoExEdit);
            this.dataLayoutControl1.Controls.Add(this.ProposedLabourTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ProposedLabourTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StatusIsssueTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RevisionNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TenderIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TenderDescriptionMemoExEdit);
            this.dataLayoutControl1.Controls.Add(this.JobSubTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactPersonPositionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactPersonNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientPONumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RequiredWorkCompletedDateTextEdit);
            this.dataLayoutControl1.Controls.Add(this.QuoteAcceptedByClientDateTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractDescriptionMemoExEdit);
            this.dataLayoutControl1.Controls.Add(this.OurInternalCommentsMemoExEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TenderStatusTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FriendlyVisitNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.QuoteSubmittedToClientDateTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CMInitialAttendanceDateTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ReturnToClientByDateTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RequestReceivedDateTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CMTextEdit);
            this.dataLayoutControl1.Controls.Add(this.KAMTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SitePostcodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddressMemoExEdit);
            this.dataLayoutControl1.Controls.Add(this.textEdit1);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForVisitID,
            this.ItemForFriendlyVisitNumber});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(157, 204, 523, 511);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup6;
            this.dataLayoutControl1.Size = new System.Drawing.Size(543, 617);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // LinkedJobCountTextEdit
            // 
            this.LinkedJobCountTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "LinkedJobCount", true));
            this.LinkedJobCountTextEdit.Location = new System.Drawing.Point(112, 415);
            this.LinkedJobCountTextEdit.MenuManager = this.barManager1;
            this.LinkedJobCountTextEdit.Name = "LinkedJobCountTextEdit";
            this.LinkedJobCountTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedJobCountTextEdit, true);
            this.LinkedJobCountTextEdit.Size = new System.Drawing.Size(157, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedJobCountTextEdit, optionsSpelling1);
            this.LinkedJobCountTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedJobCountTextEdit.TabIndex = 16;
            // 
            // LinkedVisitCountTextEdit
            // 
            this.LinkedVisitCountTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "LinkedVisitCount", true));
            this.LinkedVisitCountTextEdit.Location = new System.Drawing.Point(112, 391);
            this.LinkedVisitCountTextEdit.MenuManager = this.barManager1;
            this.LinkedVisitCountTextEdit.Name = "LinkedVisitCountTextEdit";
            this.LinkedVisitCountTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedVisitCountTextEdit, true);
            this.LinkedVisitCountTextEdit.Size = new System.Drawing.Size(157, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedVisitCountTextEdit, optionsSpelling2);
            this.LinkedVisitCountTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedVisitCountTextEdit.TabIndex = 15;
            // 
            // ClientCommentsMemoExEdit
            // 
            this.ClientCommentsMemoExEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "ClientComments", true));
            this.ClientCommentsMemoExEdit.Location = new System.Drawing.Point(112, 497);
            this.ClientCommentsMemoExEdit.MenuManager = this.barManager1;
            this.ClientCommentsMemoExEdit.Name = "ClientCommentsMemoExEdit";
            this.ClientCommentsMemoExEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ClientCommentsMemoExEdit.Properties.ReadOnly = true;
            this.ClientCommentsMemoExEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ClientCommentsMemoExEdit.Properties.ShowIcon = false;
            this.ClientCommentsMemoExEdit.Size = new System.Drawing.Size(424, 20);
            this.ClientCommentsMemoExEdit.StyleController = this.dataLayoutControl1;
            this.ClientCommentsMemoExEdit.TabIndex = 22;
            // 
            // CMCommentsMemoExEdit
            // 
            this.CMCommentsMemoExEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "CMComments", true));
            this.CMCommentsMemoExEdit.Location = new System.Drawing.Point(112, 473);
            this.CMCommentsMemoExEdit.MenuManager = this.barManager1;
            this.CMCommentsMemoExEdit.Name = "CMCommentsMemoExEdit";
            this.CMCommentsMemoExEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CMCommentsMemoExEdit.Properties.ReadOnly = true;
            this.CMCommentsMemoExEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.CMCommentsMemoExEdit.Properties.ShowIcon = false;
            this.CMCommentsMemoExEdit.Size = new System.Drawing.Size(424, 20);
            this.CMCommentsMemoExEdit.StyleController = this.dataLayoutControl1;
            this.CMCommentsMemoExEdit.TabIndex = 21;
            // 
            // ProposedLabourTextEdit
            // 
            this.ProposedLabourTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "ProposedLabour", true));
            this.ProposedLabourTextEdit.Location = new System.Drawing.Point(378, 247);
            this.ProposedLabourTextEdit.MenuManager = this.barManager1;
            this.ProposedLabourTextEdit.Name = "ProposedLabourTextEdit";
            this.ProposedLabourTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ProposedLabourTextEdit, true);
            this.ProposedLabourTextEdit.Size = new System.Drawing.Size(158, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ProposedLabourTextEdit, optionsSpelling3);
            this.ProposedLabourTextEdit.StyleController = this.dataLayoutControl1;
            this.ProposedLabourTextEdit.TabIndex = 15;
            // 
            // ProposedLabourTypeTextEdit
            // 
            this.ProposedLabourTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "ProposedLabourType", true));
            this.ProposedLabourTypeTextEdit.Location = new System.Drawing.Point(112, 247);
            this.ProposedLabourTypeTextEdit.MenuManager = this.barManager1;
            this.ProposedLabourTypeTextEdit.Name = "ProposedLabourTypeTextEdit";
            this.ProposedLabourTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ProposedLabourTypeTextEdit, true);
            this.ProposedLabourTypeTextEdit.Size = new System.Drawing.Size(157, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ProposedLabourTypeTextEdit, optionsSpelling4);
            this.ProposedLabourTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.ProposedLabourTypeTextEdit.TabIndex = 15;
            // 
            // StatusIsssueTextEdit
            // 
            this.StatusIsssueTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "StatusIsssue", true));
            this.StatusIsssueTextEdit.Location = new System.Drawing.Point(378, 175);
            this.StatusIsssueTextEdit.MenuManager = this.barManager1;
            this.StatusIsssueTextEdit.Name = "StatusIsssueTextEdit";
            this.StatusIsssueTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StatusIsssueTextEdit, true);
            this.StatusIsssueTextEdit.Size = new System.Drawing.Size(158, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StatusIsssueTextEdit, optionsSpelling5);
            this.StatusIsssueTextEdit.StyleController = this.dataLayoutControl1;
            this.StatusIsssueTextEdit.TabIndex = 14;
            // 
            // RevisionNumberTextEdit
            // 
            this.RevisionNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "RevisionNumber", true));
            this.RevisionNumberTextEdit.Location = new System.Drawing.Point(378, 151);
            this.RevisionNumberTextEdit.MenuManager = this.barManager1;
            this.RevisionNumberTextEdit.Name = "RevisionNumberTextEdit";
            this.RevisionNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RevisionNumberTextEdit, true);
            this.RevisionNumberTextEdit.Size = new System.Drawing.Size(158, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RevisionNumberTextEdit, optionsSpelling6);
            this.RevisionNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.RevisionNumberTextEdit.TabIndex = 16;
            // 
            // TenderIDTextEdit
            // 
            this.TenderIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "TenderID", true));
            this.TenderIDTextEdit.Location = new System.Drawing.Point(112, 151);
            this.TenderIDTextEdit.MenuManager = this.barManager1;
            this.TenderIDTextEdit.Name = "TenderIDTextEdit";
            this.TenderIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TenderIDTextEdit, true);
            this.TenderIDTextEdit.Size = new System.Drawing.Size(157, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TenderIDTextEdit, optionsSpelling7);
            this.TenderIDTextEdit.StyleController = this.dataLayoutControl1;
            this.TenderIDTextEdit.TabIndex = 15;
            // 
            // TenderDescriptionMemoExEdit
            // 
            this.TenderDescriptionMemoExEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "TenderDescription", true));
            this.TenderDescriptionMemoExEdit.Location = new System.Drawing.Point(112, 199);
            this.TenderDescriptionMemoExEdit.MenuManager = this.barManager1;
            this.TenderDescriptionMemoExEdit.Name = "TenderDescriptionMemoExEdit";
            this.TenderDescriptionMemoExEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TenderDescriptionMemoExEdit.Properties.ReadOnly = true;
            this.TenderDescriptionMemoExEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TenderDescriptionMemoExEdit.Properties.ShowIcon = false;
            this.TenderDescriptionMemoExEdit.Size = new System.Drawing.Size(424, 20);
            this.TenderDescriptionMemoExEdit.StyleController = this.dataLayoutControl1;
            this.TenderDescriptionMemoExEdit.TabIndex = 11;
            // 
            // JobSubTypeTextEdit
            // 
            this.JobSubTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "JobSubType", true));
            this.JobSubTypeTextEdit.Location = new System.Drawing.Point(378, 223);
            this.JobSubTypeTextEdit.MenuManager = this.barManager1;
            this.JobSubTypeTextEdit.Name = "JobSubTypeTextEdit";
            this.JobSubTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobSubTypeTextEdit, true);
            this.JobSubTypeTextEdit.Size = new System.Drawing.Size(158, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobSubTypeTextEdit, optionsSpelling8);
            this.JobSubTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.JobSubTypeTextEdit.TabIndex = 15;
            // 
            // JobTypeTextEdit
            // 
            this.JobTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "JobType", true));
            this.JobTypeTextEdit.Location = new System.Drawing.Point(112, 223);
            this.JobTypeTextEdit.MenuManager = this.barManager1;
            this.JobTypeTextEdit.Name = "JobTypeTextEdit";
            this.JobTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobTypeTextEdit, true);
            this.JobTypeTextEdit.Size = new System.Drawing.Size(157, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobTypeTextEdit, optionsSpelling9);
            this.JobTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.JobTypeTextEdit.TabIndex = 14;
            // 
            // ContactPersonPositionTextEdit
            // 
            this.ContactPersonPositionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "ContactPersonPosition", true));
            this.ContactPersonPositionTextEdit.Location = new System.Drawing.Point(378, 31);
            this.ContactPersonPositionTextEdit.MenuManager = this.barManager1;
            this.ContactPersonPositionTextEdit.Name = "ContactPersonPositionTextEdit";
            this.ContactPersonPositionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContactPersonPositionTextEdit, true);
            this.ContactPersonPositionTextEdit.Size = new System.Drawing.Size(158, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContactPersonPositionTextEdit, optionsSpelling10);
            this.ContactPersonPositionTextEdit.StyleController = this.dataLayoutControl1;
            this.ContactPersonPositionTextEdit.TabIndex = 15;
            // 
            // ContactPersonNameTextEdit
            // 
            this.ContactPersonNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "ContactPersonName", true));
            this.ContactPersonNameTextEdit.Location = new System.Drawing.Point(112, 31);
            this.ContactPersonNameTextEdit.MenuManager = this.barManager1;
            this.ContactPersonNameTextEdit.Name = "ContactPersonNameTextEdit";
            this.ContactPersonNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContactPersonNameTextEdit, true);
            this.ContactPersonNameTextEdit.Size = new System.Drawing.Size(157, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContactPersonNameTextEdit, optionsSpelling11);
            this.ContactPersonNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ContactPersonNameTextEdit.TabIndex = 14;
            // 
            // ClientPONumberTextEdit
            // 
            this.ClientPONumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "ClientPONumber", true));
            this.ClientPONumberTextEdit.Location = new System.Drawing.Point(112, 367);
            this.ClientPONumberTextEdit.MenuManager = this.barManager1;
            this.ClientPONumberTextEdit.Name = "ClientPONumberTextEdit";
            this.ClientPONumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientPONumberTextEdit, true);
            this.ClientPONumberTextEdit.Size = new System.Drawing.Size(157, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientPONumberTextEdit, optionsSpelling12);
            this.ClientPONumberTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientPONumberTextEdit.TabIndex = 14;
            // 
            // RequiredWorkCompletedDateTextEdit
            // 
            this.RequiredWorkCompletedDateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "CMInitialAttendanceDate", true));
            this.RequiredWorkCompletedDateTextEdit.Location = new System.Drawing.Point(378, 343);
            this.RequiredWorkCompletedDateTextEdit.MenuManager = this.barManager1;
            this.RequiredWorkCompletedDateTextEdit.Name = "RequiredWorkCompletedDateTextEdit";
            this.RequiredWorkCompletedDateTextEdit.Properties.Mask.EditMask = "g";
            this.RequiredWorkCompletedDateTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.RequiredWorkCompletedDateTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.RequiredWorkCompletedDateTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RequiredWorkCompletedDateTextEdit, true);
            this.RequiredWorkCompletedDateTextEdit.Size = new System.Drawing.Size(158, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RequiredWorkCompletedDateTextEdit, optionsSpelling13);
            this.RequiredWorkCompletedDateTextEdit.StyleController = this.dataLayoutControl1;
            this.RequiredWorkCompletedDateTextEdit.TabIndex = 17;
            // 
            // QuoteAcceptedByClientDateTextEdit
            // 
            this.QuoteAcceptedByClientDateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "QuoteAcceptedByClientDate", true));
            this.QuoteAcceptedByClientDateTextEdit.Location = new System.Drawing.Point(378, 415);
            this.QuoteAcceptedByClientDateTextEdit.MenuManager = this.barManager1;
            this.QuoteAcceptedByClientDateTextEdit.Name = "QuoteAcceptedByClientDateTextEdit";
            this.QuoteAcceptedByClientDateTextEdit.Properties.Mask.EditMask = "g";
            this.QuoteAcceptedByClientDateTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.QuoteAcceptedByClientDateTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.QuoteAcceptedByClientDateTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.QuoteAcceptedByClientDateTextEdit, true);
            this.QuoteAcceptedByClientDateTextEdit.Size = new System.Drawing.Size(158, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.QuoteAcceptedByClientDateTextEdit, optionsSpelling14);
            this.QuoteAcceptedByClientDateTextEdit.StyleController = this.dataLayoutControl1;
            this.QuoteAcceptedByClientDateTextEdit.TabIndex = 17;
            // 
            // ContractDescriptionMemoExEdit
            // 
            this.ContractDescriptionMemoExEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "ContractDescription", true));
            this.ContractDescriptionMemoExEdit.Location = new System.Drawing.Point(378, 7);
            this.ContractDescriptionMemoExEdit.MenuManager = this.barManager1;
            this.ContractDescriptionMemoExEdit.Name = "ContractDescriptionMemoExEdit";
            this.ContractDescriptionMemoExEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "View Client Contract", "view", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ContractDescriptionMemoExEdit.Properties.ReadOnly = true;
            this.ContractDescriptionMemoExEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ContractDescriptionMemoExEdit.Properties.ShowIcon = false;
            this.ContractDescriptionMemoExEdit.Size = new System.Drawing.Size(158, 20);
            this.ContractDescriptionMemoExEdit.StyleController = this.dataLayoutControl1;
            this.ContractDescriptionMemoExEdit.TabIndex = 22;
            this.ContractDescriptionMemoExEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ContractDescriptionMemoExEdit_ButtonClick);
            // 
            // OurInternalCommentsMemoExEdit
            // 
            this.OurInternalCommentsMemoExEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "OurInternalComments", true));
            this.OurInternalCommentsMemoExEdit.Location = new System.Drawing.Point(112, 449);
            this.OurInternalCommentsMemoExEdit.MenuManager = this.barManager1;
            this.OurInternalCommentsMemoExEdit.Name = "OurInternalCommentsMemoExEdit";
            this.OurInternalCommentsMemoExEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.OurInternalCommentsMemoExEdit.Properties.ReadOnly = true;
            this.OurInternalCommentsMemoExEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.OurInternalCommentsMemoExEdit.Properties.ShowIcon = false;
            this.OurInternalCommentsMemoExEdit.Size = new System.Drawing.Size(424, 20);
            this.OurInternalCommentsMemoExEdit.StyleController = this.dataLayoutControl1;
            this.OurInternalCommentsMemoExEdit.TabIndex = 20;
            // 
            // CreatedByStaffNameTextEdit
            // 
            this.CreatedByStaffNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "CreatedByStaffName", true));
            this.CreatedByStaffNameTextEdit.Location = new System.Drawing.Point(112, 343);
            this.CreatedByStaffNameTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffNameTextEdit.Name = "CreatedByStaffNameTextEdit";
            this.CreatedByStaffNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffNameTextEdit, true);
            this.CreatedByStaffNameTextEdit.Size = new System.Drawing.Size(157, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffNameTextEdit, optionsSpelling15);
            this.CreatedByStaffNameTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffNameTextEdit.TabIndex = 14;
            // 
            // TenderStatusTextEdit
            // 
            this.TenderStatusTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "TenderStatus", true));
            this.TenderStatusTextEdit.Location = new System.Drawing.Point(112, 175);
            this.TenderStatusTextEdit.MenuManager = this.barManager1;
            this.TenderStatusTextEdit.Name = "TenderStatusTextEdit";
            this.TenderStatusTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TenderStatusTextEdit, true);
            this.TenderStatusTextEdit.Size = new System.Drawing.Size(157, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TenderStatusTextEdit, optionsSpelling16);
            this.TenderStatusTextEdit.StyleController = this.dataLayoutControl1;
            this.TenderStatusTextEdit.TabIndex = 13;
            // 
            // FriendlyVisitNumberTextEdit
            // 
            this.FriendlyVisitNumberTextEdit.Location = new System.Drawing.Point(361, 149);
            this.FriendlyVisitNumberTextEdit.MenuManager = this.barManager1;
            this.FriendlyVisitNumberTextEdit.Name = "FriendlyVisitNumberTextEdit";
            this.FriendlyVisitNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FriendlyVisitNumberTextEdit, true);
            this.FriendlyVisitNumberTextEdit.Size = new System.Drawing.Size(159, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FriendlyVisitNumberTextEdit, optionsSpelling17);
            this.FriendlyVisitNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.FriendlyVisitNumberTextEdit.TabIndex = 10;
            // 
            // VisitIDTextEdit
            // 
            this.VisitIDTextEdit.Location = new System.Drawing.Point(103, 125);
            this.VisitIDTextEdit.MenuManager = this.barManager1;
            this.VisitIDTextEdit.Name = "VisitIDTextEdit";
            this.VisitIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitIDTextEdit, true);
            this.VisitIDTextEdit.Size = new System.Drawing.Size(158, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitIDTextEdit, optionsSpelling18);
            this.VisitIDTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitIDTextEdit.TabIndex = 10;
            // 
            // SiteIDTextEdit
            // 
            this.SiteIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "SiteID", true));
            this.SiteIDTextEdit.Location = new System.Drawing.Point(112, 55);
            this.SiteIDTextEdit.MenuManager = this.barManager1;
            this.SiteIDTextEdit.Name = "SiteIDTextEdit";
            this.SiteIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteIDTextEdit, true);
            this.SiteIDTextEdit.Size = new System.Drawing.Size(157, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteIDTextEdit, optionsSpelling19);
            this.SiteIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteIDTextEdit.TabIndex = 9;
            this.SiteIDTextEdit.Tag = "SiteID";
            // 
            // QuoteSubmittedToClientDateTextEdit
            // 
            this.QuoteSubmittedToClientDateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "QuoteSubmittedToClientDate", true));
            this.QuoteSubmittedToClientDateTextEdit.Location = new System.Drawing.Point(378, 391);
            this.QuoteSubmittedToClientDateTextEdit.MenuManager = this.barManager1;
            this.QuoteSubmittedToClientDateTextEdit.Name = "QuoteSubmittedToClientDateTextEdit";
            this.QuoteSubmittedToClientDateTextEdit.Properties.Mask.EditMask = "g";
            this.QuoteSubmittedToClientDateTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.QuoteSubmittedToClientDateTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.QuoteSubmittedToClientDateTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.QuoteSubmittedToClientDateTextEdit, true);
            this.QuoteSubmittedToClientDateTextEdit.Size = new System.Drawing.Size(158, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.QuoteSubmittedToClientDateTextEdit, optionsSpelling20);
            this.QuoteSubmittedToClientDateTextEdit.StyleController = this.dataLayoutControl1;
            this.QuoteSubmittedToClientDateTextEdit.TabIndex = 17;
            // 
            // CMInitialAttendanceDateTextEdit
            // 
            this.CMInitialAttendanceDateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "CMInitialAttendanceDate", true));
            this.CMInitialAttendanceDateTextEdit.Location = new System.Drawing.Point(378, 367);
            this.CMInitialAttendanceDateTextEdit.MenuManager = this.barManager1;
            this.CMInitialAttendanceDateTextEdit.Name = "CMInitialAttendanceDateTextEdit";
            this.CMInitialAttendanceDateTextEdit.Properties.Mask.EditMask = "g";
            this.CMInitialAttendanceDateTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.CMInitialAttendanceDateTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CMInitialAttendanceDateTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CMInitialAttendanceDateTextEdit, true);
            this.CMInitialAttendanceDateTextEdit.Size = new System.Drawing.Size(158, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CMInitialAttendanceDateTextEdit, optionsSpelling21);
            this.CMInitialAttendanceDateTextEdit.StyleController = this.dataLayoutControl1;
            this.CMInitialAttendanceDateTextEdit.TabIndex = 16;
            // 
            // ReturnToClientByDateTextEdit
            // 
            this.ReturnToClientByDateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "ReturnToClientByDate", true));
            this.ReturnToClientByDateTextEdit.Location = new System.Drawing.Point(378, 319);
            this.ReturnToClientByDateTextEdit.MenuManager = this.barManager1;
            this.ReturnToClientByDateTextEdit.Name = "ReturnToClientByDateTextEdit";
            this.ReturnToClientByDateTextEdit.Properties.Mask.EditMask = "g";
            this.ReturnToClientByDateTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.ReturnToClientByDateTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ReturnToClientByDateTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ReturnToClientByDateTextEdit, true);
            this.ReturnToClientByDateTextEdit.Size = new System.Drawing.Size(158, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ReturnToClientByDateTextEdit, optionsSpelling22);
            this.ReturnToClientByDateTextEdit.StyleController = this.dataLayoutControl1;
            this.ReturnToClientByDateTextEdit.TabIndex = 16;
            // 
            // RequestReceivedDateTextEdit
            // 
            this.RequestReceivedDateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "RequestReceivedDate", true));
            this.RequestReceivedDateTextEdit.Location = new System.Drawing.Point(378, 295);
            this.RequestReceivedDateTextEdit.MenuManager = this.barManager1;
            this.RequestReceivedDateTextEdit.Name = "RequestReceivedDateTextEdit";
            this.RequestReceivedDateTextEdit.Properties.Mask.EditMask = "g";
            this.RequestReceivedDateTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.RequestReceivedDateTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.RequestReceivedDateTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RequestReceivedDateTextEdit, true);
            this.RequestReceivedDateTextEdit.Size = new System.Drawing.Size(158, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RequestReceivedDateTextEdit, optionsSpelling23);
            this.RequestReceivedDateTextEdit.StyleController = this.dataLayoutControl1;
            this.RequestReceivedDateTextEdit.TabIndex = 15;
            // 
            // CMTextEdit
            // 
            this.CMTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "CMName", true));
            this.CMTextEdit.Location = new System.Drawing.Point(112, 319);
            this.CMTextEdit.MenuManager = this.barManager1;
            this.CMTextEdit.Name = "CMTextEdit";
            this.CMTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CMTextEdit, true);
            this.CMTextEdit.Size = new System.Drawing.Size(157, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CMTextEdit, optionsSpelling24);
            this.CMTextEdit.StyleController = this.dataLayoutControl1;
            this.CMTextEdit.TabIndex = 14;
            // 
            // KAMTextEdit
            // 
            this.KAMTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "KAMName", true));
            this.KAMTextEdit.Location = new System.Drawing.Point(112, 295);
            this.KAMTextEdit.MenuManager = this.barManager1;
            this.KAMTextEdit.Name = "KAMTextEdit";
            this.KAMTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.KAMTextEdit, true);
            this.KAMTextEdit.Size = new System.Drawing.Size(157, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.KAMTextEdit, optionsSpelling25);
            this.KAMTextEdit.StyleController = this.dataLayoutControl1;
            this.KAMTextEdit.TabIndex = 13;
            // 
            // SiteCodeTextEdit
            // 
            this.SiteCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "SiteCode", true));
            this.SiteCodeTextEdit.Location = new System.Drawing.Point(378, 55);
            this.SiteCodeTextEdit.MenuManager = this.barManager1;
            this.SiteCodeTextEdit.Name = "SiteCodeTextEdit";
            this.SiteCodeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteCodeTextEdit, true);
            this.SiteCodeTextEdit.Size = new System.Drawing.Size(158, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteCodeTextEdit, optionsSpelling26);
            this.SiteCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteCodeTextEdit.TabIndex = 11;
            // 
            // SitePostcodeTextEdit
            // 
            this.SitePostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "SitePostcode", true));
            this.SitePostcodeTextEdit.Location = new System.Drawing.Point(378, 79);
            this.SitePostcodeTextEdit.MenuManager = this.barManager1;
            this.SitePostcodeTextEdit.Name = "SitePostcodeTextEdit";
            this.SitePostcodeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SitePostcodeTextEdit, true);
            this.SitePostcodeTextEdit.Size = new System.Drawing.Size(158, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SitePostcodeTextEdit, optionsSpelling27);
            this.SitePostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.SitePostcodeTextEdit.TabIndex = 10;
            // 
            // SiteNameTextEdit
            // 
            this.SiteNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "SiteName", true));
            this.SiteNameTextEdit.Location = new System.Drawing.Point(112, 79);
            this.SiteNameTextEdit.MenuManager = this.barManager1;
            this.SiteNameTextEdit.Name = "SiteNameTextEdit";
            this.SiteNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteNameTextEdit, true);
            this.SiteNameTextEdit.Size = new System.Drawing.Size(157, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteNameTextEdit, optionsSpelling28);
            this.SiteNameTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteNameTextEdit.TabIndex = 5;
            this.SiteNameTextEdit.Tag = "SiteName";
            // 
            // ClientNameTextEdit
            // 
            this.ClientNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "ClientName", true));
            this.ClientNameTextEdit.Location = new System.Drawing.Point(112, 7);
            this.ClientNameTextEdit.MenuManager = this.barManager1;
            this.ClientNameTextEdit.Name = "ClientNameTextEdit";
            this.ClientNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameTextEdit, true);
            this.ClientNameTextEdit.Size = new System.Drawing.Size(157, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameTextEdit, optionsSpelling29);
            this.ClientNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameTextEdit.TabIndex = 4;
            this.ClientNameTextEdit.Tag = "ClientName";
            // 
            // SiteAddressMemoExEdit
            // 
            this.SiteAddressMemoExEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "SiteAddress", true));
            this.SiteAddressMemoExEdit.Location = new System.Drawing.Point(112, 103);
            this.SiteAddressMemoExEdit.MenuManager = this.barManager1;
            this.SiteAddressMemoExEdit.Name = "SiteAddressMemoExEdit";
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            this.SiteAddressMemoExEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View On Map", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "View Site On Map", "view_on_map", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SiteAddressMemoExEdit.Properties.ReadOnly = true;
            this.SiteAddressMemoExEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.SiteAddressMemoExEdit.Properties.ShowIcon = false;
            this.SiteAddressMemoExEdit.Size = new System.Drawing.Size(424, 22);
            this.SiteAddressMemoExEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddressMemoExEdit.TabIndex = 10;
            this.SiteAddressMemoExEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SiteAddressMemoExEdit_ButtonClick);
            // 
            // textEdit1
            // 
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06491OMTenderManagerBindingSource, "ClientPONumber", true));
            this.textEdit1.Location = new System.Drawing.Point(7, 590);
            this.textEdit1.MenuManager = this.barManager1;
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEdit1, true);
            this.textEdit1.Size = new System.Drawing.Size(529, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEdit1, optionsSpelling30);
            this.textEdit1.StyleController = this.dataLayoutControl1;
            this.textEdit1.TabIndex = 23;
            // 
            // ItemForVisitID
            // 
            this.ItemForVisitID.Control = this.VisitIDTextEdit;
            this.ItemForVisitID.Location = new System.Drawing.Point(0, 118);
            this.ItemForVisitID.Name = "ItemForVisitID";
            this.ItemForVisitID.Size = new System.Drawing.Size(258, 24);
            this.ItemForVisitID.Text = "Visit ID:";
            this.ItemForVisitID.TextSize = new System.Drawing.Size(93, 13);
            // 
            // ItemForFriendlyVisitNumber
            // 
            this.ItemForFriendlyVisitNumber.Control = this.FriendlyVisitNumberTextEdit;
            this.ItemForFriendlyVisitNumber.Location = new System.Drawing.Point(258, 142);
            this.ItemForFriendlyVisitNumber.Name = "ItemForFriendlyVisitNumber";
            this.ItemForFriendlyVisitNumber.Size = new System.Drawing.Size(259, 24);
            this.ItemForFriendlyVisitNumber.Text = "Friendly Visit #:";
            this.ItemForFriendlyVisitNumber.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup6.GroupBordersVisible = false;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.ItemForClientName,
            this.emptySpaceItem17,
            this.emptySpaceItem20,
            this.ItemForSiteCode,
            this.ItemForContractDescription,
            this.ItemForSiteID,
            this.ItemForSiteName,
            this.emptySpaceItem23,
            this.emptySpaceItem24,
            this.simpleSeparator3,
            this.ItemForSiteAddress,
            this.ItemForSitePostcode,
            this.ItemForTenderDescription,
            this.ItemForJobType,
            this.ItemForJobSubType,
            this.emptySpaceItem15,
            this.simpleSeparator1,
            this.emptySpaceItem19,
            this.ItemForContactPersonName,
            this.ItemForContactPersonPosition,
            this.ItemForOurInternalComments,
            this.ItemForTenderID,
            this.ItemForTenderStatus,
            this.ItemForRevisionNumber,
            this.ItemForStatusIsssue,
            this.ItemForProposedLabourType,
            this.ItemForProposedLabour,
            this.ItemForCMComments,
            this.ItemForRequestReceivedDate,
            this.ItemForClientComments,
            this.layoutControlItem1,
            this.ItemForCMInitialAttendanceDate,
            this.ItemForReturnToClientByDate,
            this.ItemForRequiredWorkCompletedDate,
            this.ItemForQuoteAcceptedByClientDate,
            this.ItemForQuoteSubmittedToClientDate,
            this.ItemForKAM,
            this.ItemForLinkedJobCount,
            this.ItemForCM,
            this.ItemForCreatedByName,
            this.ItemForClientPONumber,
            this.ItemForLinkedVisitCount});
            this.layoutControlGroup6.Name = "Root";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup6.Size = new System.Drawing.Size(543, 617);
            this.layoutControlGroup6.TextVisible = false;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.AllowDrawBackground = false;
            this.layoutControlGroup7.GroupBordersVisible = false;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleSeparator2,
            this.emptySpaceItem14});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 524);
            this.layoutControlGroup7.Name = "autoGeneratedGroup0";
            this.layoutControlGroup7.Size = new System.Drawing.Size(533, 59);
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.Location = new System.Drawing.Point(0, 0);
            this.simpleSeparator2.Name = "simpleSeparator2";
            this.simpleSeparator2.Size = new System.Drawing.Size(533, 2);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(0, 2);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(533, 57);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.Control = this.ClientNameTextEdit;
            this.ItemForClientName.Location = new System.Drawing.Point(0, 0);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(266, 24);
            this.ItemForClientName.Text = "Client Name:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.Location = new System.Drawing.Point(0, 432);
            this.emptySpaceItem17.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem17.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(533, 10);
            this.emptySpaceItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem20
            // 
            this.emptySpaceItem20.AllowHotTrack = false;
            this.emptySpaceItem20.Location = new System.Drawing.Point(0, 514);
            this.emptySpaceItem20.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem20.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem20.Name = "emptySpaceItem20";
            this.emptySpaceItem20.Size = new System.Drawing.Size(533, 10);
            this.emptySpaceItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem20.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForSiteCode
            // 
            this.ItemForSiteCode.Control = this.SiteCodeTextEdit;
            this.ItemForSiteCode.Location = new System.Drawing.Point(266, 48);
            this.ItemForSiteCode.Name = "ItemForSiteCode";
            this.ItemForSiteCode.Size = new System.Drawing.Size(267, 24);
            this.ItemForSiteCode.Text = "Site Code:";
            this.ItemForSiteCode.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForContractDescription
            // 
            this.ItemForContractDescription.Control = this.ContractDescriptionMemoExEdit;
            this.ItemForContractDescription.CustomizationFormText = "Contract Description:";
            this.ItemForContractDescription.Location = new System.Drawing.Point(266, 0);
            this.ItemForContractDescription.Name = "ItemForContractDescription";
            this.ItemForContractDescription.Size = new System.Drawing.Size(267, 24);
            this.ItemForContractDescription.Text = "Contract:";
            this.ItemForContractDescription.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSiteID
            // 
            this.ItemForSiteID.Control = this.SiteIDTextEdit;
            this.ItemForSiteID.Location = new System.Drawing.Point(0, 48);
            this.ItemForSiteID.Name = "ItemForSiteID";
            this.ItemForSiteID.Size = new System.Drawing.Size(266, 24);
            this.ItemForSiteID.Text = "Site ID:";
            this.ItemForSiteID.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSiteName
            // 
            this.ItemForSiteName.Control = this.SiteNameTextEdit;
            this.ItemForSiteName.Location = new System.Drawing.Point(0, 72);
            this.ItemForSiteName.Name = "ItemForSiteName";
            this.ItemForSiteName.Size = new System.Drawing.Size(266, 24);
            this.ItemForSiteName.Text = "Site Name:";
            this.ItemForSiteName.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem23
            // 
            this.emptySpaceItem23.AllowHotTrack = false;
            this.emptySpaceItem23.Location = new System.Drawing.Point(0, 122);
            this.emptySpaceItem23.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem23.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem23.Name = "emptySpaceItem23";
            this.emptySpaceItem23.Size = new System.Drawing.Size(533, 10);
            this.emptySpaceItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem23.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem24
            // 
            this.emptySpaceItem24.AllowHotTrack = false;
            this.emptySpaceItem24.Location = new System.Drawing.Point(0, 134);
            this.emptySpaceItem24.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem24.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem24.Name = "emptySpaceItem24";
            this.emptySpaceItem24.Size = new System.Drawing.Size(533, 10);
            this.emptySpaceItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem24.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleSeparator3
            // 
            this.simpleSeparator3.AllowHotTrack = false;
            this.simpleSeparator3.Location = new System.Drawing.Point(0, 132);
            this.simpleSeparator3.Name = "simpleSeparator3";
            this.simpleSeparator3.Size = new System.Drawing.Size(533, 2);
            // 
            // ItemForSiteAddress
            // 
            this.ItemForSiteAddress.Control = this.SiteAddressMemoExEdit;
            this.ItemForSiteAddress.Location = new System.Drawing.Point(0, 96);
            this.ItemForSiteAddress.Name = "ItemForSiteAddress";
            this.ItemForSiteAddress.Size = new System.Drawing.Size(533, 26);
            this.ItemForSiteAddress.Text = "Site Address:";
            this.ItemForSiteAddress.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSitePostcode
            // 
            this.ItemForSitePostcode.Control = this.SitePostcodeTextEdit;
            this.ItemForSitePostcode.Location = new System.Drawing.Point(266, 72);
            this.ItemForSitePostcode.Name = "ItemForSitePostcode";
            this.ItemForSitePostcode.Size = new System.Drawing.Size(267, 24);
            this.ItemForSitePostcode.Text = "Site Postcode:";
            this.ItemForSitePostcode.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForTenderDescription
            // 
            this.ItemForTenderDescription.Control = this.TenderDescriptionMemoExEdit;
            this.ItemForTenderDescription.Location = new System.Drawing.Point(0, 192);
            this.ItemForTenderDescription.Name = "ItemForTenderDescription";
            this.ItemForTenderDescription.Size = new System.Drawing.Size(533, 24);
            this.ItemForTenderDescription.Text = "Tender Description:";
            this.ItemForTenderDescription.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForJobType
            // 
            this.ItemForJobType.Control = this.JobTypeTextEdit;
            this.ItemForJobType.Location = new System.Drawing.Point(0, 216);
            this.ItemForJobType.Name = "ItemForJobType";
            this.ItemForJobType.Size = new System.Drawing.Size(266, 24);
            this.ItemForJobType.Text = "Work Type:";
            this.ItemForJobType.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForJobSubType
            // 
            this.ItemForJobSubType.Control = this.JobSubTypeTextEdit;
            this.ItemForJobSubType.Location = new System.Drawing.Point(266, 216);
            this.ItemForJobSubType.Name = "ItemForJobSubType";
            this.ItemForJobSubType.Size = new System.Drawing.Size(267, 24);
            this.ItemForJobSubType.Text = "Work Sub-Type:";
            this.ItemForJobSubType.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(0, 264);
            this.emptySpaceItem15.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem15.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(533, 12);
            this.emptySpaceItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 276);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(533, 2);
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.Location = new System.Drawing.Point(0, 278);
            this.emptySpaceItem19.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem19.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(533, 10);
            this.emptySpaceItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForContactPersonName
            // 
            this.ItemForContactPersonName.Control = this.ContactPersonNameTextEdit;
            this.ItemForContactPersonName.Location = new System.Drawing.Point(0, 24);
            this.ItemForContactPersonName.Name = "ItemForContactPersonName";
            this.ItemForContactPersonName.Size = new System.Drawing.Size(266, 24);
            this.ItemForContactPersonName.Text = "Client Contact Name:";
            this.ItemForContactPersonName.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForContactPersonPosition
            // 
            this.ItemForContactPersonPosition.Control = this.ContactPersonPositionTextEdit;
            this.ItemForContactPersonPosition.Location = new System.Drawing.Point(266, 24);
            this.ItemForContactPersonPosition.Name = "ItemForContactPersonPosition";
            this.ItemForContactPersonPosition.Size = new System.Drawing.Size(267, 24);
            this.ItemForContactPersonPosition.Text = "Contact Position:";
            this.ItemForContactPersonPosition.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForOurInternalComments
            // 
            this.ItemForOurInternalComments.Control = this.OurInternalCommentsMemoExEdit;
            this.ItemForOurInternalComments.Location = new System.Drawing.Point(0, 442);
            this.ItemForOurInternalComments.Name = "ItemForOurInternalComments";
            this.ItemForOurInternalComments.Size = new System.Drawing.Size(533, 24);
            this.ItemForOurInternalComments.Text = "Our Remarks:";
            this.ItemForOurInternalComments.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForTenderID
            // 
            this.ItemForTenderID.Control = this.TenderIDTextEdit;
            this.ItemForTenderID.Location = new System.Drawing.Point(0, 144);
            this.ItemForTenderID.Name = "ItemForTenderID";
            this.ItemForTenderID.Size = new System.Drawing.Size(266, 24);
            this.ItemForTenderID.Text = "Tender ID:";
            this.ItemForTenderID.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForTenderStatus
            // 
            this.ItemForTenderStatus.Control = this.TenderStatusTextEdit;
            this.ItemForTenderStatus.Location = new System.Drawing.Point(0, 168);
            this.ItemForTenderStatus.Name = "ItemForTenderStatus";
            this.ItemForTenderStatus.Size = new System.Drawing.Size(266, 24);
            this.ItemForTenderStatus.Text = "Tender Status:";
            this.ItemForTenderStatus.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForRevisionNumber
            // 
            this.ItemForRevisionNumber.Control = this.RevisionNumberTextEdit;
            this.ItemForRevisionNumber.Location = new System.Drawing.Point(266, 144);
            this.ItemForRevisionNumber.Name = "ItemForRevisionNumber";
            this.ItemForRevisionNumber.Size = new System.Drawing.Size(267, 24);
            this.ItemForRevisionNumber.Text = "Revision:";
            this.ItemForRevisionNumber.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForStatusIsssue
            // 
            this.ItemForStatusIsssue.Control = this.StatusIsssueTextEdit;
            this.ItemForStatusIsssue.Location = new System.Drawing.Point(266, 168);
            this.ItemForStatusIsssue.Name = "ItemForStatusIsssue";
            this.ItemForStatusIsssue.Size = new System.Drawing.Size(267, 24);
            this.ItemForStatusIsssue.Text = "Status Isssue:";
            this.ItemForStatusIsssue.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForProposedLabourType
            // 
            this.ItemForProposedLabourType.Control = this.ProposedLabourTypeTextEdit;
            this.ItemForProposedLabourType.Location = new System.Drawing.Point(0, 240);
            this.ItemForProposedLabourType.Name = "ItemForProposedLabourType";
            this.ItemForProposedLabourType.Size = new System.Drawing.Size(266, 24);
            this.ItemForProposedLabourType.Text = "Labour Type:";
            this.ItemForProposedLabourType.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForProposedLabour
            // 
            this.ItemForProposedLabour.Control = this.ProposedLabourTextEdit;
            this.ItemForProposedLabour.Location = new System.Drawing.Point(266, 240);
            this.ItemForProposedLabour.Name = "ItemForProposedLabour";
            this.ItemForProposedLabour.Size = new System.Drawing.Size(267, 24);
            this.ItemForProposedLabour.Text = "Labour Name:";
            this.ItemForProposedLabour.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForCMComments
            // 
            this.ItemForCMComments.Control = this.CMCommentsMemoExEdit;
            this.ItemForCMComments.Location = new System.Drawing.Point(0, 466);
            this.ItemForCMComments.Name = "ItemForCMComments";
            this.ItemForCMComments.Size = new System.Drawing.Size(533, 24);
            this.ItemForCMComments.Text = "CM Remarks:";
            this.ItemForCMComments.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForRequestReceivedDate
            // 
            this.ItemForRequestReceivedDate.Control = this.RequestReceivedDateTextEdit;
            this.ItemForRequestReceivedDate.Location = new System.Drawing.Point(266, 288);
            this.ItemForRequestReceivedDate.Name = "ItemForRequestReceivedDate";
            this.ItemForRequestReceivedDate.Size = new System.Drawing.Size(267, 24);
            this.ItemForRequestReceivedDate.Text = "Request Received:";
            this.ItemForRequestReceivedDate.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForClientComments
            // 
            this.ItemForClientComments.Control = this.ClientCommentsMemoExEdit;
            this.ItemForClientComments.Location = new System.Drawing.Point(0, 490);
            this.ItemForClientComments.Name = "ItemForClientComments";
            this.ItemForClientComments.Size = new System.Drawing.Size(533, 24);
            this.ItemForClientComments.Text = "Client Remarks:";
            this.ItemForClientComments.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textEdit1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 583);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(533, 24);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // ItemForCMInitialAttendanceDate
            // 
            this.ItemForCMInitialAttendanceDate.Control = this.CMInitialAttendanceDateTextEdit;
            this.ItemForCMInitialAttendanceDate.Location = new System.Drawing.Point(266, 360);
            this.ItemForCMInitialAttendanceDate.Name = "ItemForCMInitialAttendanceDate";
            this.ItemForCMInitialAttendanceDate.Size = new System.Drawing.Size(267, 24);
            this.ItemForCMInitialAttendanceDate.Text = "CM Attendance:";
            this.ItemForCMInitialAttendanceDate.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForReturnToClientByDate
            // 
            this.ItemForReturnToClientByDate.Control = this.ReturnToClientByDateTextEdit;
            this.ItemForReturnToClientByDate.Location = new System.Drawing.Point(266, 312);
            this.ItemForReturnToClientByDate.Name = "ItemForReturnToClientByDate";
            this.ItemForReturnToClientByDate.Size = new System.Drawing.Size(267, 24);
            this.ItemForReturnToClientByDate.Text = "Return To Client By:";
            this.ItemForReturnToClientByDate.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForRequiredWorkCompletedDate
            // 
            this.ItemForRequiredWorkCompletedDate.Control = this.RequiredWorkCompletedDateTextEdit;
            this.ItemForRequiredWorkCompletedDate.Location = new System.Drawing.Point(266, 336);
            this.ItemForRequiredWorkCompletedDate.Name = "ItemForRequiredWorkCompletedDate";
            this.ItemForRequiredWorkCompletedDate.Size = new System.Drawing.Size(267, 24);
            this.ItemForRequiredWorkCompletedDate.Text = "Complete Work By:";
            this.ItemForRequiredWorkCompletedDate.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForQuoteAcceptedByClientDate
            // 
            this.ItemForQuoteAcceptedByClientDate.Control = this.QuoteAcceptedByClientDateTextEdit;
            this.ItemForQuoteAcceptedByClientDate.Location = new System.Drawing.Point(266, 408);
            this.ItemForQuoteAcceptedByClientDate.Name = "ItemForQuoteAcceptedByClientDate";
            this.ItemForQuoteAcceptedByClientDate.Size = new System.Drawing.Size(267, 24);
            this.ItemForQuoteAcceptedByClientDate.Text = "Quote Accepted:";
            this.ItemForQuoteAcceptedByClientDate.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForQuoteSubmittedToClientDate
            // 
            this.ItemForQuoteSubmittedToClientDate.Control = this.QuoteSubmittedToClientDateTextEdit;
            this.ItemForQuoteSubmittedToClientDate.Location = new System.Drawing.Point(266, 384);
            this.ItemForQuoteSubmittedToClientDate.Name = "ItemForQuoteSubmittedToClientDate";
            this.ItemForQuoteSubmittedToClientDate.Size = new System.Drawing.Size(267, 24);
            this.ItemForQuoteSubmittedToClientDate.Text = "Quote Submitted:";
            this.ItemForQuoteSubmittedToClientDate.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForKAM
            // 
            this.ItemForKAM.Control = this.KAMTextEdit;
            this.ItemForKAM.Location = new System.Drawing.Point(0, 288);
            this.ItemForKAM.Name = "ItemForKAM";
            this.ItemForKAM.Size = new System.Drawing.Size(266, 24);
            this.ItemForKAM.Text = "KAM:";
            this.ItemForKAM.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForLinkedJobCount
            // 
            this.ItemForLinkedJobCount.Control = this.LinkedJobCountTextEdit;
            this.ItemForLinkedJobCount.Location = new System.Drawing.Point(0, 408);
            this.ItemForLinkedJobCount.Name = "ItemForLinkedJobCount";
            this.ItemForLinkedJobCount.Size = new System.Drawing.Size(266, 24);
            this.ItemForLinkedJobCount.Text = "Linked Job Count:";
            this.ItemForLinkedJobCount.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForCM
            // 
            this.ItemForCM.Control = this.CMTextEdit;
            this.ItemForCM.Location = new System.Drawing.Point(0, 312);
            this.ItemForCM.Name = "ItemForCM";
            this.ItemForCM.Size = new System.Drawing.Size(266, 24);
            this.ItemForCM.Text = "CM:";
            this.ItemForCM.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForCreatedByName
            // 
            this.ItemForCreatedByName.Control = this.CreatedByStaffNameTextEdit;
            this.ItemForCreatedByName.Location = new System.Drawing.Point(0, 336);
            this.ItemForCreatedByName.Name = "ItemForCreatedByName";
            this.ItemForCreatedByName.Size = new System.Drawing.Size(266, 24);
            this.ItemForCreatedByName.Text = "Created By:";
            this.ItemForCreatedByName.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForClientPONumber
            // 
            this.ItemForClientPONumber.Control = this.ClientPONumberTextEdit;
            this.ItemForClientPONumber.Location = new System.Drawing.Point(0, 360);
            this.ItemForClientPONumber.Name = "ItemForClientPONumber";
            this.ItemForClientPONumber.Size = new System.Drawing.Size(266, 24);
            this.ItemForClientPONumber.Text = "Client PO Number:";
            this.ItemForClientPONumber.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForLinkedVisitCount
            // 
            this.ItemForLinkedVisitCount.Control = this.LinkedVisitCountTextEdit;
            this.ItemForLinkedVisitCount.Location = new System.Drawing.Point(0, 384);
            this.ItemForLinkedVisitCount.Name = "ItemForLinkedVisitCount";
            this.ItemForLinkedVisitCount.Size = new System.Drawing.Size(266, 24);
            this.ItemForLinkedVisitCount.Text = "Linked Visit Count:";
            this.ItemForLinkedVisitCount.TextSize = new System.Drawing.Size(102, 13);
            // 
            // hideContainerLeft
            // 
            this.hideContainerLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.hideContainerLeft.Controls.Add(this.dockPanelFilters);
            this.hideContainerLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.hideContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.hideContainerLeft.Name = "hideContainerLeft";
            this.hideContainerLeft.Size = new System.Drawing.Size(23, 654);
            // 
            // dockPanelFilters
            // 
            this.dockPanelFilters.Controls.Add(this.dockPanel1_Container);
            this.dockPanelFilters.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.FloatVertical = true;
            this.dockPanelFilters.ID = new System.Guid("88d829bd-f6cb-4abc-9203-5d9c628ceebe");
            this.dockPanelFilters.Location = new System.Drawing.Point(0, 0);
            this.dockPanelFilters.Name = "dockPanelFilters";
            this.dockPanelFilters.Options.ShowCloseButton = false;
            this.dockPanelFilters.OriginalSize = new System.Drawing.Size(344, 200);
            this.dockPanelFilters.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.SavedIndex = 0;
            this.dockPanelFilters.Size = new System.Drawing.Size(344, 654);
            this.dockPanelFilters.Text = "Data Filter";
            this.dockPanelFilters.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControl2);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(337, 622);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // layoutControl2
            // 
            this.layoutControl2.AllowCustomization = false;
            this.layoutControl2.Controls.Add(this.btnFormatData);
            this.layoutControl2.Controls.Add(this.popupContainerEditJobSubTypeFilter);
            this.layoutControl2.Controls.Add(this.popupContainerEditJobTypeFilter);
            this.layoutControl2.Controls.Add(this.TenderIDsMemoEdit);
            this.layoutControl2.Controls.Add(this.checkEditDatesFromToday);
            this.layoutControl2.Controls.Add(this.checkEditDateRange);
            this.layoutControl2.Controls.Add(this.popupContainerEditVisitSystemStatusFilter);
            this.layoutControl2.Controls.Add(this.btnClearAllFilters);
            this.layoutControl2.Controls.Add(this.popupContainerEditCMFilter);
            this.layoutControl2.Controls.Add(this.popupContainerEditShowTabPages);
            this.layoutControl2.Controls.Add(this.popupContainerEditLabourFilter);
            this.layoutControl2.Controls.Add(this.buttonEditSiteFilter);
            this.layoutControl2.Controls.Add(this.buttonEditClientFilter);
            this.layoutControl2.Controls.Add(this.popupContainerEditDateRange);
            this.layoutControl2.Controls.Add(this.checkEditIgnoreHistoric);
            this.layoutControl2.Controls.Add(this.btnLoad);
            this.layoutControl2.Controls.Add(this.popupContainerEditKAMFilter);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1281, 412, 460, 350);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(337, 622);
            this.layoutControl2.TabIndex = 8;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // btnFormatData
            // 
            this.btnFormatData.Location = new System.Drawing.Point(253, 331);
            this.btnFormatData.Name = "btnFormatData";
            this.btnFormatData.Size = new System.Drawing.Size(69, 22);
            this.btnFormatData.StyleController = this.layoutControl2;
            toolTipTitleItem11.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Text = "Format data - Information";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Click me to convert carriage returns into commas and clean up any spaces within t" +
    "he Specific Tender IDs box.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.btnFormatData.SuperTip = superToolTip11;
            this.btnFormatData.TabIndex = 23;
            this.btnFormatData.Text = "Format Data";
            this.btnFormatData.Click += new System.EventHandler(this.btnFormatData_Click);
            // 
            // popupContainerEditJobSubTypeFilter
            // 
            this.popupContainerEditJobSubTypeFilter.EditValue = "No Work Sub-Type Filter";
            this.popupContainerEditJobSubTypeFilter.Location = new System.Drawing.Point(97, 297);
            this.popupContainerEditJobSubTypeFilter.MenuManager = this.barManager1;
            this.popupContainerEditJobSubTypeFilter.Name = "popupContainerEditJobSubTypeFilter";
            this.popupContainerEditJobSubTypeFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditJobSubTypeFilter.Properties.PopupControl = this.popupContainerControlJobSubTypeFilter;
            this.popupContainerEditJobSubTypeFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditJobSubTypeFilter.Properties.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditJobSubTypeFilter_QueryResultValue);
            this.popupContainerEditJobSubTypeFilter.Size = new System.Drawing.Size(225, 20);
            this.popupContainerEditJobSubTypeFilter.StyleController = this.layoutControl2;
            this.popupContainerEditJobSubTypeFilter.TabIndex = 11;
            // 
            // popupContainerEditJobTypeFilter
            // 
            this.popupContainerEditJobTypeFilter.EditValue = "No work Type Filter";
            this.popupContainerEditJobTypeFilter.Location = new System.Drawing.Point(97, 273);
            this.popupContainerEditJobTypeFilter.MenuManager = this.barManager1;
            this.popupContainerEditJobTypeFilter.Name = "popupContainerEditJobTypeFilter";
            this.popupContainerEditJobTypeFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditJobTypeFilter.Properties.PopupControl = this.popupContainerControlJobTypeFilter;
            this.popupContainerEditJobTypeFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditJobTypeFilter.Properties.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditJobTypeFilter_QueryResultValue);
            this.popupContainerEditJobTypeFilter.Size = new System.Drawing.Size(225, 20);
            this.popupContainerEditJobTypeFilter.StyleController = this.layoutControl2;
            this.popupContainerEditJobTypeFilter.TabIndex = 10;
            // 
            // TenderIDsMemoEdit
            // 
            this.TenderIDsMemoEdit.Location = new System.Drawing.Point(15, 36);
            this.TenderIDsMemoEdit.MenuManager = this.barManager1;
            this.TenderIDsMemoEdit.Name = "TenderIDsMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.TenderIDsMemoEdit, true);
            this.TenderIDsMemoEdit.Size = new System.Drawing.Size(307, 291);
            this.scSpellChecker.SetSpellCheckerOptions(this.TenderIDsMemoEdit, optionsSpelling31);
            this.TenderIDsMemoEdit.StyleController = this.layoutControl2;
            this.TenderIDsMemoEdit.TabIndex = 22;
            // 
            // checkEditDatesFromToday
            // 
            this.checkEditDatesFromToday.Location = new System.Drawing.Point(161, 64);
            this.checkEditDatesFromToday.MenuManager = this.barManager1;
            this.checkEditDatesFromToday.Name = "checkEditDatesFromToday";
            this.checkEditDatesFromToday.Properties.Caption = "From Today:";
            this.checkEditDatesFromToday.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditDatesFromToday.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditDatesFromToday.Properties.RadioGroupIndex = 2;
            this.checkEditDatesFromToday.Size = new System.Drawing.Size(82, 19);
            this.checkEditDatesFromToday.StyleController = this.layoutControl2;
            superToolTip12.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem12.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem12.Appearance.Options.UseImage = true;
            toolTipTitleItem12.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem12.Text = "Date Range From Today - Information";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Click me to filter by a number of days before and after today based on Request Re" +
    "ceived date.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            this.checkEditDatesFromToday.SuperTip = superToolTip12;
            this.checkEditDatesFromToday.TabIndex = 20;
            this.checkEditDatesFromToday.TabStop = false;
            this.checkEditDatesFromToday.CheckedChanged += new System.EventHandler(this.checkEditDatesFromToday_CheckedChanged);
            // 
            // checkEditDateRange
            // 
            this.checkEditDateRange.EditValue = true;
            this.checkEditDateRange.Location = new System.Drawing.Point(87, 64);
            this.checkEditDateRange.MenuManager = this.barManager1;
            this.checkEditDateRange.Name = "checkEditDateRange";
            this.checkEditDateRange.Properties.Caption = "Range:";
            this.checkEditDateRange.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditDateRange.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditDateRange.Properties.RadioGroupIndex = 2;
            this.checkEditDateRange.Size = new System.Drawing.Size(56, 19);
            this.checkEditDateRange.StyleController = this.layoutControl2;
            superToolTip13.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem13.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem13.Appearance.Options.UseImage = true;
            toolTipTitleItem13.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem13.Text = "Date Range - Information";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "Click me to filter by Date Range based on Request Received date.";
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            this.checkEditDateRange.SuperTip = superToolTip13;
            this.checkEditDateRange.TabIndex = 19;
            this.checkEditDateRange.CheckedChanged += new System.EventHandler(this.checkEditDateRange_CheckedChanged);
            // 
            // popupContainerEditVisitSystemStatusFilter
            // 
            this.popupContainerEditVisitSystemStatusFilter.EditValue = "No Tender Status Filter";
            this.popupContainerEditVisitSystemStatusFilter.Location = new System.Drawing.Point(97, 177);
            this.popupContainerEditVisitSystemStatusFilter.MenuManager = this.barManager1;
            this.popupContainerEditVisitSystemStatusFilter.Name = "popupContainerEditVisitSystemStatusFilter";
            this.popupContainerEditVisitSystemStatusFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditVisitSystemStatusFilter.Properties.PopupControl = this.popupContainerControlVisitSystemStatusFilter;
            this.popupContainerEditVisitSystemStatusFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditVisitSystemStatusFilter.Size = new System.Drawing.Size(225, 20);
            this.popupContainerEditVisitSystemStatusFilter.StyleController = this.layoutControl2;
            this.popupContainerEditVisitSystemStatusFilter.TabIndex = 12;
            this.popupContainerEditVisitSystemStatusFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditVisitSystemStatusFilter_QueryResultValue);
            // 
            // btnClearAllFilters
            // 
            this.btnClearAllFilters.ImageOptions.ImageIndex = 19;
            this.btnClearAllFilters.ImageOptions.ImageList = this.imageCollection1;
            this.btnClearAllFilters.Location = new System.Drawing.Point(234, 331);
            this.btnClearAllFilters.Name = "btnClearAllFilters";
            this.btnClearAllFilters.Size = new System.Drawing.Size(88, 22);
            this.btnClearAllFilters.StyleController = this.layoutControl2;
            toolTipTitleItem14.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem14.Appearance.Options.UseImage = true;
            toolTipTitleItem14.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem14.Text = "Clear Filters - Information";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Click me to clear all filters (except Date Filter).";
            superToolTip14.Items.Add(toolTipTitleItem14);
            superToolTip14.Items.Add(toolTipItem14);
            this.btnClearAllFilters.SuperTip = superToolTip14;
            this.btnClearAllFilters.TabIndex = 20;
            this.btnClearAllFilters.Text = "Clear Filters";
            this.btnClearAllFilters.Click += new System.EventHandler(this.btnClearAllFilters_Click);
            // 
            // popupContainerEditCMFilter
            // 
            this.popupContainerEditCMFilter.EditValue = "No CM Filter";
            this.popupContainerEditCMFilter.Location = new System.Drawing.Point(97, 225);
            this.popupContainerEditCMFilter.MenuManager = this.barManager1;
            this.popupContainerEditCMFilter.Name = "popupContainerEditCMFilter";
            this.popupContainerEditCMFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditCMFilter.Properties.PopupControl = this.popupContainerControlCMFilter;
            this.popupContainerEditCMFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditCMFilter.Properties.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditCMFilter_QueryResultValue);
            this.popupContainerEditCMFilter.Size = new System.Drawing.Size(225, 20);
            this.popupContainerEditCMFilter.StyleController = this.layoutControl2;
            this.popupContainerEditCMFilter.TabIndex = 5;
            // 
            // popupContainerEditShowTabPages
            // 
            this.popupContainerEditShowTabPages.EditValue = "All Linked Data";
            this.popupContainerEditShowTabPages.Location = new System.Drawing.Point(89, 375);
            this.popupContainerEditShowTabPages.MenuManager = this.barManager1;
            this.popupContainerEditShowTabPages.Name = "popupContainerEditShowTabPages";
            this.popupContainerEditShowTabPages.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditShowTabPages.Properties.PopupControl = this.popupContainerControlShowTabPages;
            this.popupContainerEditShowTabPages.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditShowTabPages.Properties.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditShowTabPages_QueryResultValue);
            this.popupContainerEditShowTabPages.Size = new System.Drawing.Size(241, 20);
            this.popupContainerEditShowTabPages.StyleController = this.layoutControl2;
            superToolTip15.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem15.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem15.Appearance.Options.UseImage = true;
            toolTipTitleItem15.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem15.Text = "Linked Data - Information";
            toolTipItem15.LeftIndent = 6;
            toolTipItem15.Text = resources.GetString("toolTipItem15.Text");
            superToolTip15.Items.Add(toolTipTitleItem15);
            superToolTip15.Items.Add(toolTipItem15);
            this.popupContainerEditShowTabPages.SuperTip = superToolTip15;
            this.popupContainerEditShowTabPages.TabIndex = 15;
            // 
            // popupContainerEditLabourFilter
            // 
            this.popupContainerEditLabourFilter.EditValue = "No Created By Filter";
            this.popupContainerEditLabourFilter.Location = new System.Drawing.Point(97, 249);
            this.popupContainerEditLabourFilter.MenuManager = this.barManager1;
            this.popupContainerEditLabourFilter.Name = "popupContainerEditLabourFilter";
            this.popupContainerEditLabourFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditLabourFilter.Properties.PopupControl = this.popupContainerControlLabourFilter;
            this.popupContainerEditLabourFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditLabourFilter.Properties.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditLabourFilter_QueryResultValue);
            this.popupContainerEditLabourFilter.Size = new System.Drawing.Size(225, 20);
            this.popupContainerEditLabourFilter.StyleController = this.layoutControl2;
            this.popupContainerEditLabourFilter.TabIndex = 12;
            // 
            // buttonEditSiteFilter
            // 
            this.buttonEditSiteFilter.Location = new System.Drawing.Point(97, 153);
            this.buttonEditSiteFilter.MenuManager = this.barManager1;
            this.buttonEditSiteFilter.Name = "buttonEditSiteFilter";
            this.buttonEditSiteFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, serializableAppearanceObject13, "Click to open Choose Site Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, serializableAppearanceObject17, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditSiteFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditSiteFilter.Size = new System.Drawing.Size(225, 20);
            this.buttonEditSiteFilter.StyleController = this.layoutControl2;
            this.buttonEditSiteFilter.TabIndex = 3;
            this.buttonEditSiteFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditSiteFilter_ButtonClick);
            // 
            // buttonEditClientFilter
            // 
            this.buttonEditClientFilter.Location = new System.Drawing.Point(97, 129);
            this.buttonEditClientFilter.MenuManager = this.barManager1;
            this.buttonEditClientFilter.Name = "buttonEditClientFilter";
            this.buttonEditClientFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, serializableAppearanceObject21, "Click to open Choose Client Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, serializableAppearanceObject25, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditClientFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditClientFilter.Size = new System.Drawing.Size(225, 20);
            this.buttonEditClientFilter.StyleController = this.layoutControl2;
            this.buttonEditClientFilter.TabIndex = 2;
            this.buttonEditClientFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditClientFilter_ButtonClick);
            // 
            // popupContainerEditDateRange
            // 
            this.popupContainerEditDateRange.EditValue = "No Date Range Filter";
            this.popupContainerEditDateRange.Location = new System.Drawing.Point(84, 87);
            this.popupContainerEditDateRange.MenuManager = this.barManager1;
            this.popupContainerEditDateRange.Name = "popupContainerEditDateRange";
            this.popupContainerEditDateRange.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditDateRange.Properties.PopupControl = this.popupContainerControlDateRange;
            this.popupContainerEditDateRange.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditDateRange.Size = new System.Drawing.Size(230, 20);
            this.popupContainerEditDateRange.StyleController = this.layoutControl2;
            this.popupContainerEditDateRange.TabIndex = 1;
            this.popupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditDateRange_QueryResultValue);
            // 
            // checkEditIgnoreHistoric
            // 
            this.checkEditIgnoreHistoric.Location = new System.Drawing.Point(97, 331);
            this.checkEditIgnoreHistoric.MenuManager = this.barManager1;
            this.checkEditIgnoreHistoric.Name = "checkEditIgnoreHistoric";
            this.checkEditIgnoreHistoric.Properties.Caption = "[Tick if Yes]";
            this.checkEditIgnoreHistoric.Size = new System.Drawing.Size(133, 19);
            this.checkEditIgnoreHistoric.StyleController = this.layoutControl2;
            this.checkEditIgnoreHistoric.TabIndex = 7;
            // 
            // btnLoad
            // 
            this.btnLoad.ImageOptions.ImageIndex = 13;
            this.btnLoad.ImageOptions.ImageList = this.imageCollection1;
            this.btnLoad.Location = new System.Drawing.Point(251, 399);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(79, 22);
            this.btnLoad.StyleController = this.layoutControl2;
            toolTipTitleItem16.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem16.Appearance.Options.UseImage = true;
            toolTipTitleItem16.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem16.Text = "Load Data - Information";
            toolTipItem16.LeftIndent = 6;
            toolTipItem16.Text = "Click me to Load Tender Data.\r\n\r\nOnly data matching the specified Filter Criteria" +
    " (From and To Dates and Tender Status etc) will be loaded.";
            superToolTip16.Items.Add(toolTipTitleItem16);
            superToolTip16.Items.Add(toolTipItem16);
            this.btnLoad.SuperTip = superToolTip16;
            this.btnLoad.TabIndex = 16;
            this.btnLoad.Text = "Load Data";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // popupContainerEditKAMFilter
            // 
            this.popupContainerEditKAMFilter.EditValue = "No KAM Filter";
            this.popupContainerEditKAMFilter.Location = new System.Drawing.Point(97, 201);
            this.popupContainerEditKAMFilter.MenuManager = this.barManager1;
            this.popupContainerEditKAMFilter.Name = "popupContainerEditKAMFilter";
            this.popupContainerEditKAMFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditKAMFilter.Properties.PopupControl = this.popupContainerControlKAMFilter;
            this.popupContainerEditKAMFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditKAMFilter.Properties.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditKAMFilter_QueryResultValue);
            this.popupContainerEditKAMFilter.Size = new System.Drawing.Size(225, 20);
            this.popupContainerEditKAMFilter.StyleController = this.layoutControl2;
            this.popupContainerEditKAMFilter.TabIndex = 4;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Root";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.emptySpaceItem3,
            this.layoutControlItem15,
            this.emptySpaceItem4,
            this.emptySpaceItem1,
            this.tabbedControlGroupFilterType});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup2.Size = new System.Drawing.Size(337, 622);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnLoad;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(244, 392);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(83, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(83, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(83, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 418);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(327, 194);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.popupContainerEditShowTabPages;
            this.layoutControlItem15.CustomizationFormText = "Linked Data:";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 368);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(327, 24);
            this.layoutControlItem15.Text = "Linked Data:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(79, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 358);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(327, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 392);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(244, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // tabbedControlGroupFilterType
            // 
            this.tabbedControlGroupFilterType.AllowHide = false;
            this.tabbedControlGroupFilterType.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroupFilterType.Name = "tabbedControlGroupFilterType";
            this.tabbedControlGroupFilterType.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.tabbedControlGroupFilterType.SelectedTabPage = this.layoutControlGroupFilterCriteria;
            this.tabbedControlGroupFilterType.SelectedTabPageIndex = 0;
            this.tabbedControlGroupFilterType.Size = new System.Drawing.Size(327, 358);
            this.tabbedControlGroupFilterType.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupFilterCriteria,
            this.layoutControlGroupSpecificVisitIDs});
            // 
            // layoutControlGroupFilterCriteria
            // 
            this.layoutControlGroupFilterCriteria.AllowHide = false;
            this.layoutControlGroupFilterCriteria.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.emptySpaceItem10,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem12,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem5,
            this.ItemForLabourFilter,
            this.emptySpaceItem2,
            this.ItemForClearAllFilters,
            this.layoutControlItem2,
            this.layoutControlItem9});
            this.layoutControlGroupFilterCriteria.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupFilterCriteria.Name = "layoutControlGroupFilterCriteria";
            this.layoutControlGroupFilterCriteria.Size = new System.Drawing.Size(311, 321);
            this.layoutControlGroupFilterCriteria.Text = "Filter Criteria";
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDateRange,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.emptySpaceItem9,
            this.emptySpaceItem11,
            this.emptySpaceItem12});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup5.Size = new System.Drawing.Size(311, 83);
            this.layoutControlGroup5.Text = "Date Filter:";
            // 
            // layoutControlItemDateRange
            // 
            this.layoutControlItemDateRange.Control = this.popupContainerEditDateRange;
            this.layoutControlItemDateRange.CustomizationFormText = "Date Range:";
            this.layoutControlItemDateRange.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItemDateRange.Name = "layoutControlItemDateRange";
            this.layoutControlItemDateRange.Size = new System.Drawing.Size(295, 24);
            this.layoutControlItemDateRange.Text = "Requested:";
            this.layoutControlItemDateRange.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemDateRange.TextSize = new System.Drawing.Size(56, 13);
            this.layoutControlItemDateRange.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.checkEditDateRange;
            this.layoutControlItem20.Location = new System.Drawing.Point(64, 0);
            this.layoutControlItem20.MaxSize = new System.Drawing.Size(60, 23);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(60, 23);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(60, 23);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "Date Range Active:";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.checkEditDatesFromToday;
            this.layoutControlItem21.Location = new System.Drawing.Point(138, 0);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(86, 23);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(86, 23);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(86, 23);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "Dates From Today:";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextVisible = false;
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(224, 0);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(71, 23);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem11.MaxSize = new System.Drawing.Size(64, 0);
            this.emptySpaceItem11.MinSize = new System.Drawing.Size(64, 10);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(64, 23);
            this.emptySpaceItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(124, 0);
            this.emptySpaceItem12.MaxSize = new System.Drawing.Size(14, 0);
            this.emptySpaceItem12.MinSize = new System.Drawing.Size(14, 10);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(14, 23);
            this.emptySpaceItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 83);
            this.emptySpaceItem10.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem10.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(311, 10);
            this.emptySpaceItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.buttonEditClientFilter;
            this.layoutControlItem7.CustomizationFormText = "Client:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 93);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(311, 24);
            this.layoutControlItem7.Text = "Client:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.buttonEditSiteFilter;
            this.layoutControlItem8.CustomizationFormText = "Site:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 117);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(311, 24);
            this.layoutControlItem8.Text = "Site:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.popupContainerEditVisitSystemStatusFilter;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 141);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(311, 24);
            this.layoutControlItem12.Text = "Tender Status:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.popupContainerEditKAMFilter;
            this.layoutControlItem4.CustomizationFormText = "KAM:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 165);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(311, 24);
            this.layoutControlItem4.Text = "KAM:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.popupContainerEditCMFilter;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 189);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(311, 24);
            this.layoutControlItem6.Text = "CM:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.checkEditIgnoreHistoric;
            this.layoutControlItem5.CustomizationFormText = "Ignore Completed:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 295);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(219, 26);
            this.layoutControlItem5.Text = "Hide Completed:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForLabourFilter
            // 
            this.ItemForLabourFilter.Control = this.popupContainerEditLabourFilter;
            this.ItemForLabourFilter.CustomizationFormText = "Team:";
            this.ItemForLabourFilter.Location = new System.Drawing.Point(0, 213);
            this.ItemForLabourFilter.Name = "ItemForLabourFilter";
            this.ItemForLabourFilter.Size = new System.Drawing.Size(311, 24);
            this.ItemForLabourFilter.Text = "Created By:";
            this.ItemForLabourFilter.TextSize = new System.Drawing.Size(79, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 285);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(311, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForClearAllFilters
            // 
            this.ItemForClearAllFilters.AllowHide = false;
            this.ItemForClearAllFilters.Control = this.btnClearAllFilters;
            this.ItemForClearAllFilters.Location = new System.Drawing.Point(219, 295);
            this.ItemForClearAllFilters.MaxSize = new System.Drawing.Size(92, 26);
            this.ItemForClearAllFilters.MinSize = new System.Drawing.Size(92, 26);
            this.ItemForClearAllFilters.Name = "ItemForClearAllFilters";
            this.ItemForClearAllFilters.Size = new System.Drawing.Size(92, 26);
            this.ItemForClearAllFilters.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClearAllFilters.Text = "Clear All Filters";
            this.ItemForClearAllFilters.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForClearAllFilters.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.popupContainerEditJobTypeFilter;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 237);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(311, 24);
            this.layoutControlItem2.Text = "Work Type:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.popupContainerEditJobSubTypeFilter;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 261);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(311, 24);
            this.layoutControlItem9.Text = "Work Sub-Type:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlGroupSpecificVisitIDs
            // 
            this.layoutControlGroupSpecificVisitIDs.AllowHide = false;
            this.layoutControlGroupSpecificVisitIDs.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem22,
            this.ItemForFormatDataBtn,
            this.emptySpaceItem5});
            this.layoutControlGroupSpecificVisitIDs.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupSpecificVisitIDs.Name = "layoutControlGroupSpecificVisitIDs";
            this.layoutControlGroupSpecificVisitIDs.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroupSpecificVisitIDs.Size = new System.Drawing.Size(311, 321);
            this.layoutControlGroupSpecificVisitIDs.Text = "Specific Tender IDs";
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.TenderIDsMemoEdit;
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(311, 295);
            this.layoutControlItem22.Text = "Visit IDs:";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextVisible = false;
            // 
            // ItemForFormatDataBtn
            // 
            this.ItemForFormatDataBtn.Control = this.btnFormatData;
            this.ItemForFormatDataBtn.Location = new System.Drawing.Point(238, 295);
            this.ItemForFormatDataBtn.MaxSize = new System.Drawing.Size(73, 26);
            this.ItemForFormatDataBtn.MinSize = new System.Drawing.Size(73, 26);
            this.ItemForFormatDataBtn.Name = "ItemForFormatDataBtn";
            this.ItemForFormatDataBtn.Size = new System.Drawing.Size(73, 26);
            this.ItemForFormatDataBtn.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForFormatDataBtn.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForFormatDataBtn.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 295);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(238, 26);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp06019_OM_Job_Manager_KAMs_FilterTableAdapter
            // 
            this.sp06019_OM_Job_Manager_KAMs_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06020_OM_Job_Manager_CMs_FilterTableAdapter
            // 
            this.sp06020_OM_Job_Manager_CMs_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp04022_Core_Dummy_TabPageListTableAdapter
            // 
            this.sp04022_Core_Dummy_TabPageListTableAdapter.ClearBeforeFill = true;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // sp06489_OM_Tender_Status_FilterTableAdapter
            // 
            this.sp06489_OM_Tender_Status_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06490_OM_Tender_Staff_FilterTableAdapter
            // 
            this.sp06490_OM_Tender_Staff_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06491_OM_Tender_ManagerTableAdapter
            // 
            this.sp06491_OM_Tender_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp06493_OM_Job_Type_FilterTableAdapter
            // 
            this.sp06493_OM_Job_Type_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06494_OM_Job_Sub_Type_FilterTableAdapter
            // 
            this.sp06494_OM_Job_Sub_Type_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06495_OM_Visits_Linked_to_TenderTableAdapter
            // 
            this.sp06495_OM_Visits_Linked_to_TenderTableAdapter.ClearBeforeFill = true;
            // 
            // sp06514_OM_Linked_Documents_Linked_To_RecordTableAdapter
            // 
            this.sp06514_OM_Linked_Documents_Linked_To_RecordTableAdapter.ClearBeforeFill = true;
            // 
            // sp06515_OM_Tender_History_For_TenderTableAdapter
            // 
            this.sp06515_OM_Tender_History_For_TenderTableAdapter.ClearBeforeFill = true;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Visible = true;
            this.colSiteName1.VisibleIndex = 3;
            this.colSiteName1.Width = 140;
            // 
            // colTenderID
            // 
            this.colTenderID.Caption = "Tender ID";
            this.colTenderID.FieldName = "TenderID";
            this.colTenderID.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colTenderID.Name = "colTenderID";
            this.colTenderID.OptionsColumn.AllowEdit = false;
            this.colTenderID.OptionsColumn.AllowFocus = false;
            this.colTenderID.OptionsColumn.ReadOnly = true;
            this.colTenderID.Visible = true;
            this.colTenderID.VisibleIndex = 0;
            this.colTenderID.Width = 80;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 2;
            this.colClientName1.Width = 130;
            // 
            // frm_OM_Tender_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1382, 654);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.hideContainerLeft);
            this.Controls.Add(this.hideContainerRight);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Tender_Manager";
            this.Text = "Tender Manager";
            this.Activated += new System.EventHandler(this.frm_OM_Tender_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Tender_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Tender_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.hideContainerRight, 0);
            this.Controls.SetChildIndex(this.hideContainerLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsVisit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedVisits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLabour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditClientSignature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditSelfBillingInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditClientInvoiceID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditPictureLink)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVisit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06495OMVisitsLinkedtoTenderBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Tender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewVisit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlJobSubTypeFilter)).EndInit();
            this.popupContainerControlJobSubTypeFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06494OMJobSubTypeFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlJobTypeFilter)).EndInit();
            this.popupContainerControlJobTypeFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06493OMJobTypeFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCMFilter)).EndInit();
            this.popupContainerControlCMFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06020OMJobManagerCMsFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Summer_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRangeFromToday)).EndInit();
            this.popupContainerControlDateRangeFromToday.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDaysAfter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDaysPrior.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlKAMFilter)).EndInit();
            this.popupContainerControlKAMFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06019OMJobManagerKAMsFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLabourFilter)).EndInit();
            this.popupContainerControlLabourFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06490OMTenderStaffFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlShowTabPages)).EndInit();
            this.popupContainerControlShowTabPages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04022CoreDummyTabPageListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlVisitSystemStatusFilter)).EndInit();
            this.popupContainerControlVisitSystemStatusFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06489OMTenderStatusFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06491OMTenderManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedVisits2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsJobs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditTenderGroupDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBarGreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditClientQuoteExtractFilt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBarRed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPageVisits.ResumeLayout(false);
            this.xtraTabPageHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06515OMTenderHistoryForTenderBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).EndInit();
            this.xtraTabPageCRM.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCRM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05089CRMContactsLinkedToRecordBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCRM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            this.xtraTabPageLinkedDocuments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06514OMLinkedDocumentsLinkedToRecordBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Client_PO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMarqueeProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditIgnoreHistoric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06145OMVisitEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.hideContainerRight.ResumeLayout(false);
            this.dockPanelVisitDetails.ResumeLayout(false);
            this.controlContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LinkedJobCountTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedVisitCountTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientCommentsMemoExEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CMCommentsMemoExEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProposedLabourTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProposedLabourTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIsssueTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RevisionNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderDescriptionMemoExEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactPersonPositionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactPersonNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequiredWorkCompletedDateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteAcceptedByClientDateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractDescriptionMemoExEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OurInternalCommentsMemoExEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderStatusTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FriendlyVisitNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteSubmittedToClientDateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CMInitialAttendanceDateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReturnToClientByDateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequestReceivedDateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CMTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KAMTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SitePostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressMemoExEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFriendlyVisitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSitePostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactPersonName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactPersonPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOurInternalComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRevisionNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusIsssue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProposedLabourType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProposedLabour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCMComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRequestReceivedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCMInitialAttendanceDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReturnToClientByDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRequiredWorkCompletedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuoteAcceptedByClientDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuoteSubmittedToClientDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKAM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedJobCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedVisitCount)).EndInit();
            this.hideContainerLeft.ResumeLayout(false);
            this.dockPanelFilters.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditJobSubTypeFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditJobTypeFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderIDsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDatesFromToday.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDateRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditVisitSystemStatusFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditCMFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditShowTabPages.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditLabourFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSiteFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditDateRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIgnoreHistoric.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditKAMFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupFilterType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupFilterCriteria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClearAllFilters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSpecificVisitIDs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFormatDataBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControlVisit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewVisit;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        public DevExpress.XtraBars.BarButtonItem bbiAddVisitWizard;
        private WoodPlan5.DataSet_AT dataSet_AT;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarButtonItem bbiImport;
        private DevExpress.XtraGrid.GridControl gridControlTender;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewTender;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditIgnoreHistoric;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocumentsJobs;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCalculationLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCost;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitSell;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCalculationLevelDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractActive;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractValue;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colGCCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDirectorID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractActive;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractValue;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colGCCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colContractType;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDirector;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorType;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedJobCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedOutstandingJobCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedCompletedJobCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditLatLong2;
        private DevExpress.XtraBars.BarButtonItem bbiViewSiteOnMap;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraScheduler.UI.RepositoryItemDuration repositoryItemDuration1;
        private DevExpress.XtraScheduler.UI.ToggleRecurrenceItem toggleRecurrenceItem1;
        private System.Windows.Forms.BindingSource sp06145OMVisitEditBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06145_OM_Visit_EditTableAdapter sp06145_OM_Visit_EditTableAdapter;
        private DevExpress.XtraBars.BarCheckItem bciFilterTenderSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeliness;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedVisitCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourCount;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueFound;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueType;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colClientSignaturePath;
        private DevExpress.XtraGrid.Columns.GridColumn colImagesFolderOM;
        private DevExpress.XtraGrid.Columns.GridColumn colRework;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.XtraBars.BarStaticItem bsiSelectedCount;
        private DevExpress.XtraGrid.Columns.GridColumn colExtraWorkComments;
        private DevExpress.XtraGrid.Columns.GridColumn colExtraWorkRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colExtraWorkType;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageVisits;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageHistory;
        private DevExpress.XtraGrid.GridControl gridControlHistory;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewHistory;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime7;
        private DataSet_OM_Job dataSet_OM_Job;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentOnSite;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentComment;
        private DevExpress.XtraBars.BarSubItem bsiSendVisits;
        private DevExpress.XtraBars.BarButtonItem bbiSelectJobsReadyToSend;
        private DevExpress.XtraBars.BarButtonItem bbiSendJobs;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorNames;
        private DevExpress.XtraGrid.Columns.GridColumn Alert2;
        private DevExpress.XtraGrid.Columns.GridColumn colSuspendedJobCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedPictureCount;
        private DevExpress.XtraGrid.GridControl gridControlCRM;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCRM;
        private DevExpress.XtraGrid.Columns.GridColumn colCRMID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn208;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContactID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID1;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDueDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMadeDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMethodID;
        private DevExpress.XtraGrid.Columns.GridColumn colContactTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn209;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn210;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit15;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn211;
        private DevExpress.XtraGrid.Columns.GridColumn colContactedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDirectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn212;
        private DevExpress.XtraGrid.Columns.GridColumn colContactedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colContactType;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDirectionDescription;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn213;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreated1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContact;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private System.Windows.Forms.BindingSource sp05089CRMContactsLinkedToRecordBindingSource;
        private WoodPlanDataSetTableAdapters.sp05089_CRM_Contacts_Linked_To_RecordTableAdapter sp05089_CRM_Contacts_Linked_To_RecordTableAdapter;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelFilters;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditShowTabPages;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditLabourFilter;
        private DevExpress.XtraEditors.ButtonEdit buttonEditSiteFilter;
        private DevExpress.XtraEditors.ButtonEdit buttonEditClientFilter;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditDateRange;
        private DevExpress.XtraEditors.CheckEdit checkEditIgnoreHistoric;
        private DevExpress.XtraEditors.SimpleButton btnLoad;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditKAMFilter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDateRange;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLabourFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditCMFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeOK;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateRange;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlKAMFilter;
        private DevExpress.XtraEditors.SimpleButton btnKAMFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription3;
        private DevExpress.XtraGrid.Columns.GridColumn colForename;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DataSet_GC_Summer_Core dataSet_GC_Summer_Core;
        private System.Windows.Forms.BindingSource sp06019OMJobManagerKAMsFilterBindingSource;
        private DataSet_GC_Summer_CoreTableAdapters.sp06019_OM_Job_Manager_KAMs_FilterTableAdapter sp06019_OM_Job_Manager_KAMs_FilterTableAdapter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlCMFilter;
        private DevExpress.XtraEditors.SimpleButton btnCMFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl9;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn218;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn219;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn220;
        private System.Windows.Forms.BindingSource sp06020OMJobManagerCMsFilterBindingSource;
        private DataSet_GC_Summer_CoreTableAdapters.sp06020_OM_Job_Manager_CMs_FilterTableAdapter sp06020_OM_Job_Manager_CMs_FilterTableAdapter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlLabourFilter;
        private DevExpress.XtraEditors.SimpleButton btnLabourFilter;
        private DevExpress.XtraGrid.GridControl gridControl12;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView12;
        private DevExpress.XtraGrid.Columns.GridColumn colID4;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit17;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlShowTabPages;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn colTabPageName;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit18;
        private DevExpress.XtraEditors.SimpleButton btnShowTabPagesOK;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private System.Windows.Forms.BindingSource sp04022CoreDummyTabPageListBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04022_Core_Dummy_TabPageListTableAdapter sp04022_Core_Dummy_TabPageListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colFriendlyVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitType;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitTypeID;
        private DataSet_OM_Client_PO dataSet_OM_Client_PO;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPO;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID1;
        private DevExpress.XtraBars.BarButtonItem bbiAddJobWizard;
        private DevExpress.XtraGrid.Columns.GridColumn colPDAVersion;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSellCalculationLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colSellCalculationLevelDescription;
        private DevExpress.XtraEditors.SimpleButton btnClearAllFilters;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClearAllFilters;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected4;
        private DevExpress.XtraBars.BarSubItem bsiFilterSelected;
        private DevExpress.XtraBars.BarCheckItem bciFilterVisitsSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceID1;
        private DevExpress.XtraGrid.Columns.GridColumn colParentLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colCMName;
        private DevExpress.XtraBars.BarButtonItem bbiSendQuoteToCM;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysAllowed;
        private DevExpress.XtraGrid.Columns.GridColumn colManagerName;
        private DevExpress.XtraGrid.Columns.GridColumn colManagerNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colNoOnetoSign;
        private DevExpress.XtraBars.BarSubItem bsiStatus;
        private DevExpress.XtraBars.BarButtonItem bbiCMReturnedQuote;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCategory;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML12;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromVisitTemplate;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromVisitTemplateID;
        private DevExpress.XtraGrid.Columns.GridColumn colExternalCustomerStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletionSheetNumber;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlVisitSystemStatusFilter;
        private DevExpress.XtraEditors.SimpleButton btnVisitSystemStatusFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn244;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn245;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn246;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditVisitSystemStatusFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.CheckEdit checkEditDatesFromToday;
        private DevExpress.XtraEditors.CheckEdit checkEditDateRange;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRangeFromToday;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeFromTodayOK;
        private DevExpress.XtraEditors.SpinEdit spinEditDaysAfter;
        private DevExpress.XtraEditors.SpinEdit spinEditDaysPrior;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusIssueID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusIssue;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditPictureLink;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit14;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit20;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit15;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysSeparation;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractDaysSeparationPercent;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine5;
        private DataSet_Common_Functionality dataSet_Common_Functionality;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPayContractor1;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClient1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelVisitDetails;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer1;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit SitePostcodeTextEdit;
        private DevExpress.XtraEditors.TextEdit SiteNameTextEdit;
        private DevExpress.XtraEditors.TextEdit ClientNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSitePostcode;
        private DevExpress.XtraEditors.TextEdit SiteCodeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteCode;
        private DevExpress.XtraEditors.TextEdit ReturnToClientByDateTextEdit;
        private DevExpress.XtraEditors.TextEdit RequestReceivedDateTextEdit;
        private DevExpress.XtraEditors.TextEdit CMTextEdit;
        private DevExpress.XtraEditors.TextEdit KAMTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForKAM;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCM;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRequestReceivedDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReturnToClientByDate;
        private DevExpress.XtraEditors.TextEdit VisitIDTextEdit;
        private DevExpress.XtraEditors.TextEdit SiteIDTextEdit;
        private DevExpress.XtraEditors.TextEdit QuoteSubmittedToClientDateTextEdit;
        private DevExpress.XtraEditors.TextEdit CMInitialAttendanceDateTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCMInitialAttendanceDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQuoteSubmittedToClientDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitID;
        private DevExpress.XtraEditors.TextEdit FriendlyVisitNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFriendlyVisitNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddress;
        private DevExpress.XtraEditors.TextEdit TenderStatusTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTenderStatus;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colKAMName;
        private DevExpress.XtraEditors.MemoExEdit OurInternalCommentsMemoExEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOurInternalComments;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem20;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private DevExpress.XtraBars.BarEditItem beiLoadingProgress;
        private DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar repositoryItemMarqueeProgressBar1;
        private DevExpress.XtraBars.BarButtonItem bbiLoadingCancel;
        private DevExpress.XtraEditors.MemoExEdit ContractDescriptionMemoExEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractDescription;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem23;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem24;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteInstructions;
        private DevExpress.XtraEditors.MemoEdit TenderIDsMemoEdit;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroupFilterType;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupFilterCriteria;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupSpecificVisitIDs;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DataSet_OM_Tender dataSet_OM_Tender;
        private System.Windows.Forms.BindingSource sp06489OMTenderStatusFilterBindingSource;
        private DataSet_OM_TenderTableAdapters.sp06489_OM_Tender_Status_FilterTableAdapter sp06489_OM_Tender_Status_FilterTableAdapter;
        private System.Windows.Forms.BindingSource sp06490OMTenderStaffFilterBindingSource;
        private DataSet_OM_TenderTableAdapters.sp06490_OM_Tender_Staff_FilterTableAdapter sp06490_OM_Tender_Staff_FilterTableAdapter;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageCRM;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private System.Windows.Forms.BindingSource sp06491OMTenderManagerBindingSource;
        private DataSet_OM_TenderTableAdapters.sp06491_OM_Tender_ManagerTableAdapter sp06491_OM_Tender_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContactID1;
        private DevExpress.XtraGrid.Columns.GridColumn colKAM;
        private DevExpress.XtraGrid.Columns.GridColumn colCM;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderGroupID;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteNotRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colRequestReceivedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colReturnToClientByDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCMInitialAttendanceDate;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteSubmittedToClientDate;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteAcceptedByClientDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRequiredWorkCompletedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colKAMQuoteRejectionReasonID;
        private DevExpress.XtraGrid.Columns.GridColumn colOurInternalComments;
        private DevExpress.XtraGrid.Columns.GridColumn colCMComments;
        private DevExpress.XtraGrid.Columns.GridColumn colClientComments;
        private DevExpress.XtraGrid.Columns.GridColumn colTPORequired;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthorityID;
        private DevExpress.XtraGrid.Columns.GridColumn colTPOCheckedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeProtected;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthorityNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthoritySubmittedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthorityOutcomeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthorityOkToProceed;
        private DevExpress.XtraGrid.Columns.GridColumn colGCCompanyName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorType1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddress1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationX1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationY1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedVisitCount1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedVisits2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount1;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderGroupDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName1;
        private DevExpress.XtraGrid.Columns.GridColumn colKAMName1;
        private DevExpress.XtraGrid.Columns.GridColumn colCMName1;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPersonPosition;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPersonTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthorityOutcome;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningAuthority;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubType;
        private DevExpress.XtraGrid.Columns.GridColumn colJobType;
        private DevExpress.XtraGrid.Columns.GridColumn colKAMQuoteRejectionReason;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkType;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlJobTypeFilter;
        private DevExpress.XtraEditors.SimpleButton btnJobTypeFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl10;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraGrid.Columns.GridColumn colID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription4;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit8;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlJobSubTypeFilter;
        private DevExpress.XtraEditors.SimpleButton btnJobSubTypeFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl11;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private DevExpress.XtraGrid.Columns.GridColumn colID3;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription5;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit16;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditJobTypeFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditJobSubTypeFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private System.Windows.Forms.BindingSource sp06493OMJobTypeFilterBindingSource;
        private DataSet_OM_TenderTableAdapters.sp06493_OM_Job_Type_FilterTableAdapter sp06493_OM_Job_Type_FilterTableAdapter;
        private System.Windows.Forms.BindingSource sp06494OMJobSubTypeFilterBindingSource;
        private DataSet_OM_TenderTableAdapters.sp06494_OM_Job_Sub_Type_FilterTableAdapter sp06494_OM_Job_Sub_Type_FilterTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DevExpress.XtraEditors.MemoExEdit SiteAddressMemoExEdit;
        private DevExpress.XtraEditors.TextEdit ContactPersonNameTextEdit;
        private DevExpress.XtraEditors.TextEdit ClientPONumberTextEdit;
        private DevExpress.XtraEditors.TextEdit RequiredWorkCompletedDateTextEdit;
        private DevExpress.XtraEditors.TextEdit QuoteAcceptedByClientDateTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQuoteAcceptedByClientDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRequiredWorkCompletedDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPONumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactPersonName;
        private DevExpress.XtraEditors.MemoExEdit TenderDescriptionMemoExEdit;
        private DevExpress.XtraEditors.TextEdit JobSubTypeTextEdit;
        private DevExpress.XtraEditors.TextEdit JobTypeTextEdit;
        private DevExpress.XtraEditors.TextEdit ContactPersonPositionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTenderDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobSubType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactPersonPosition;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraEditors.TextEdit TenderIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTenderID;
        private System.Windows.Forms.BindingSource sp06495OMVisitsLinkedtoTenderBindingSource;
        private DataSet_OM_TenderTableAdapters.sp06495_OM_Visits_Linked_to_TenderTableAdapter sp06495_OM_Visits_Linked_to_TenderTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderID1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditTenderGroupDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRevisionNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysQuoteOverdue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumericDays;
        private DevExpress.XtraGrid.Columns.GridColumn colNoClientPO;
        private DevExpress.XtraGrid.Columns.GridColumn colNoClientPOAuthorisedByDirector;
        private DevExpress.XtraGrid.Columns.GridColumn colNoClientPOAuthorisedByDirectorID;
        private DevExpress.XtraGrid.Columns.GridColumn colNoClientPOEmailedToDirector;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusIsssue;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusIssueID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubmittedToCMDate;
        private DevExpress.XtraGrid.Columns.GridColumn colProposedLabour;
        private DevExpress.XtraGrid.Columns.GridColumn colProposedLabourID;
        private DevExpress.XtraGrid.Columns.GridColumn colProposedLabourType;
        private DevExpress.XtraGrid.Columns.GridColumn colProposedLabourTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuotedLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colQuotedMaterialSell;
        private DevExpress.XtraGrid.Columns.GridColumn colQuotedEquipmentSell;
        private DevExpress.XtraGrid.Columns.GridColumn colQuotedTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colAcceptedLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colAcceptedMaterialSell;
        private DevExpress.XtraGrid.Columns.GridColumn colAcceptedEquipmentSell;
        private DevExpress.XtraGrid.Columns.GridColumn colAcceptedTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualMaterialCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderProactiveDaysToReturnQuote;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderReactiveDaysToReturnQuote;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLinkedDocuments;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecord;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaff;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private System.Windows.Forms.BindingSource sp06514OMLinkedDocumentsLinkedToRecordBindingSource;
        private DataSet_OM_Core dataSet_OM_Core;
        private DataSet_OM_CoreTableAdapters.sp06514_OM_Linked_Documents_Linked_To_RecordTableAdapter sp06514_OM_Linked_Documents_Linked_To_RecordTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime4;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordParent;
        private DevExpress.XtraBars.BarSubItem bsiToolbarAdd;
        private DevExpress.XtraBars.BarButtonItem bbiToolbarAdd;
        private DevExpress.XtraBars.BarButtonItem bbiToolbarAddFromTemplate;
        private DevExpress.XtraGrid.Columns.GridColumn colProgressBarValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBarRed;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBarGreen;
        private System.Windows.Forms.BindingSource sp06515OMTenderHistoryForTenderBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderHistoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderID2;
        private DevExpress.XtraGrid.Columns.GridColumn colOldStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colNewStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colOldStatusIssueID;
        private DevExpress.XtraGrid.Columns.GridColumn colNewStatusIssueID;
        private DevExpress.XtraGrid.Columns.GridColumn colUserName;
        private DevExpress.XtraGrid.Columns.GridColumn colDateChanged;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecord;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName2;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colOldStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colNewStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colOldStatusIssue;
        private DevExpress.XtraGrid.Columns.GridColumn colNewStatusIssue;
        private DataSet_OM_TenderTableAdapters.sp06515_OM_Tender_History_For_TenderTableAdapter sp06515_OM_Tender_History_For_TenderTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientQuoteSpreadsheetExtractFile;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditClientQuoteExtractFilt;
        private DevExpress.XtraBars.BarButtonItem bbiTenderClosed;
        private DevExpress.XtraBars.BarSubItem bsiKAMAuthorised;
        private DevExpress.XtraBars.BarButtonItem bbiKAMAccepted;
        private DevExpress.XtraBars.BarButtonItem bbiKAMRejected;
        private DevExpress.XtraBars.BarButtonItem bbiSentToClient;
        private DevExpress.XtraBars.BarSubItem bsiClientResponse;
        private DevExpress.XtraBars.BarButtonItem bbiClientAccepted;
        private DevExpress.XtraBars.BarButtonItem bbiClientRejected;
        private DevExpress.XtraBars.BarButtonItem bbiClientOnHold;
        private DevExpress.XtraBars.BarButtonItem bbiTenderCancelled;
        private DevExpress.XtraBars.BarSubItem bsiSendQuoteToCM;
        private DevExpress.XtraBars.BarButtonItem bbiQuoteNotRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedJobCount1;
        private DevExpress.XtraBars.BarSubItem bsiVisitJobWizard;
        private DevExpress.XtraEditors.MemoExEdit CMCommentsMemoExEdit;
        private DevExpress.XtraEditors.TextEdit ProposedLabourTextEdit;
        private DevExpress.XtraEditors.TextEdit ProposedLabourTypeTextEdit;
        private DevExpress.XtraEditors.TextEdit StatusIsssueTextEdit;
        private DevExpress.XtraEditors.TextEdit RevisionNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRevisionNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusIsssue;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProposedLabourType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProposedLabour;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCMComments;
        private DevExpress.XtraEditors.MemoExEdit ClientCommentsMemoExEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientComments;
        private DevExpress.XtraBars.Docking.AutoHideContainer hideContainerRight;
        private DevExpress.XtraEditors.TextEdit LinkedJobCountTextEdit;
        private DevExpress.XtraEditors.TextEdit LinkedVisitCountTextEdit;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedJobCount;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedVisitCount;
        private DevExpress.XtraBars.BarButtonItem bbiVisitDrillDown;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningProceedDateSet;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningProceedStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderType;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colExtraCostsFinalised;
        private DevExpress.XtraGrid.Columns.GridColumn colTPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colConservationStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colListedBuildingStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colRAMS;
        private DevExpress.XtraEditors.SimpleButton btnFormatData;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFormatDataBtn;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraBars.Docking.AutoHideContainer hideContainerLeft;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditLatLong;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML0;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocumentsVisit;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditGrid1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedVisits;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLabour;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditClientSignature;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditSelfBillingInvoice;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditClientInvoiceID;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colProposedLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colProposedMaterialCost;
        private DevExpress.XtraGrid.Columns.GridColumn colProposedEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colProposedTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
    }
}

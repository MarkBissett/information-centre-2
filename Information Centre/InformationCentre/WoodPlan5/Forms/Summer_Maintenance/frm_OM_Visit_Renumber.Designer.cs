﻿namespace WoodPlan5
{
    partial class frm_OM_Visit_Renumber
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Visit_Renumber));
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.sharedImageCollection1 = new DevExpress.Utils.SharedImageCollection(this.components);
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.spinEditVisitNumber = new DevExpress.XtraEditors.SpinEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp06390OMVisitReNumberingSiteContractsSelectedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colClientContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientNameContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelectedVisitCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTotalVisitCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFirstSelectedVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubsequentVisitCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNewFirstVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.checkEditRenumberSubsequentVisits = new DevExpress.XtraEditors.CheckEdit();
            this.sp06390_OM_Visit_ReNumbering_Site_Contracts_SelectedTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06390_OM_Visit_ReNumbering_Site_Contracts_SelectedTableAdapter();
            this.btnApplyBlockEdit = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharedImageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharedImageCollection1.ImageSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditVisitNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06390OMVisitReNumberingSiteContractsSelectedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditRenumberSubsequentVisits.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(823, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 442);
            this.barDockControlBottom.Size = new System.Drawing.Size(823, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 416);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(823, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 416);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.ImageOptions.ImageIndex = 9;
            this.btnOK.ImageOptions.ImageList = this.sharedImageCollection1;
            this.btnOK.Location = new System.Drawing.Point(675, 412);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(67, 24);
            this.btnOK.TabIndex = 13;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // sharedImageCollection1
            // 
            // 
            // 
            // 
            this.sharedImageCollection1.ImageSource.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("sharedImageCollection1.ImageSource.ImageStream")));
            this.sharedImageCollection1.ImageSource.Images.SetKeyName(0, "Add_16x16.png");
            this.sharedImageCollection1.ImageSource.Images.SetKeyName(1, "Edit_16x16.png");
            this.sharedImageCollection1.ImageSource.Images.SetKeyName(2, "Delete_16x16.png");
            this.sharedImageCollection1.ImageSource.Images.SetKeyName(3, "Preview_16x16.png");
            this.sharedImageCollection1.ImageSource.Images.SetKeyName(4, "Refresh_16x16.png");
            this.sharedImageCollection1.ImageSource.Images.SetKeyName(5, "BlockAdd_16x16.png");
            this.sharedImageCollection1.ImageSource.Images.SetKeyName(6, "BlockEdit_16x16.png");
            this.sharedImageCollection1.ImageSource.Images.SetKeyName(7, "Info_16x16.png");
            this.sharedImageCollection1.ImageSource.Images.SetKeyName(8, "attention_16.png");
            this.sharedImageCollection1.ImageSource.Images.SetKeyName(9, "apply_16x16.png");
            this.sharedImageCollection1.ImageSource.Images.SetKeyName(10, "cancel_16x16.png");
            this.sharedImageCollection1.ParentControl = this;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.ImageOptions.ImageIndex = 10;
            this.btnCancel.ImageOptions.ImageList = this.sharedImageCollection1;
            this.btnCancel.Location = new System.Drawing.Point(749, 412);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(67, 24);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Location = new System.Drawing.Point(457, 91);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(245, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "<b>Block Edit</b> selected records - New Starting Visit #:";
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.pictureEdit1);
            this.panelControl1.Location = new System.Drawing.Point(7, 33);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(809, 49);
            this.panelControl1.TabIndex = 29;
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl3.Location = new System.Drawing.Point(41, 3);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(764, 39);
            this.labelControl3.TabIndex = 31;
            this.labelControl3.Text = resources.GetString("labelControl3.Text");
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = global::WoodPlan5.Properties.Resources.Info_32x32;
            this.pictureEdit1.Location = new System.Drawing.Point(-2, 2);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Size = new System.Drawing.Size(32, 32);
            this.pictureEdit1.TabIndex = 30;
            // 
            // spinEditVisitNumber
            // 
            this.spinEditVisitNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.spinEditVisitNumber.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditVisitNumber.Location = new System.Drawing.Point(706, 88);
            this.spinEditVisitNumber.MenuManager = this.barManager1;
            this.spinEditVisitNumber.Name = "spinEditVisitNumber";
            this.spinEditVisitNumber.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditVisitNumber.Properties.IsFloatValue = false;
            this.spinEditVisitNumber.Properties.Mask.EditMask = "N00";
            this.spinEditVisitNumber.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditVisitNumber.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.spinEditVisitNumber.Properties.MinValue = new decimal(new int[] {
            999999,
            0,
            0,
            -2147483648});
            this.spinEditVisitNumber.Size = new System.Drawing.Size(67, 20);
            this.spinEditVisitNumber.TabIndex = 32;
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp06390OMVisitReNumberingSiteContractsSelectedBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(7, 109);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditNumeric,
            this.repositoryItemSpinEdit1});
            this.gridControl1.Size = new System.Drawing.Size(809, 296);
            this.gridControl1.TabIndex = 33;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp06390OMVisitReNumberingSiteContractsSelectedBindingSource
            // 
            this.sp06390OMVisitReNumberingSiteContractsSelectedBindingSource.DataMember = "sp06390_OM_Visit_ReNumbering_Site_Contracts_Selected";
            this.sp06390OMVisitReNumberingSiteContractsSelectedBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteContractID,
            this.colSiteID,
            this.colStartDate,
            this.colEndDate,
            this.colActive,
            this.colClientContractID,
            this.colSiteName,
            this.colClientID,
            this.colClientName,
            this.colContractDescription,
            this.colClientNameContractDescription,
            this.colSelectedVisitCount,
            this.colTotalVisitCount,
            this.colFirstSelectedVisitNumber,
            this.colSubsequentVisitCount,
            this.colNewFirstVisitNumber});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.FindDelay = 2000;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientNameContractDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridViewVisit_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            // 
            // colSiteContractID
            // 
            this.colSiteContractID.Caption = "Site Contract ID";
            this.colSiteContractID.FieldName = "SiteContractID";
            this.colSiteContractID.Name = "colSiteContractID";
            this.colSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSiteContractID.Width = 97;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.Visible = true;
            this.colSiteID.VisibleIndex = 0;
            this.colSiteID.Width = 62;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.Width = 80;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Mask.EditMask = "g";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "End Date";
            this.colEndDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.Width = 80;
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Width = 50;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colClientContractID
            // 
            this.colClientContractID.Caption = "Client Contract ID";
            this.colClientContractID.FieldName = "ClientContractID";
            this.colClientContractID.Name = "colClientContractID";
            this.colClientContractID.OptionsColumn.AllowEdit = false;
            this.colClientContractID.OptionsColumn.AllowFocus = false;
            this.colClientContractID.OptionsColumn.ReadOnly = true;
            this.colClientContractID.Width = 106;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 1;
            this.colSiteName.Width = 191;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Width = 164;
            // 
            // colContractDescription
            // 
            this.colContractDescription.Caption = "Contract Description";
            this.colContractDescription.FieldName = "ContractDescription";
            this.colContractDescription.Name = "colContractDescription";
            this.colContractDescription.OptionsColumn.AllowEdit = false;
            this.colContractDescription.OptionsColumn.AllowFocus = false;
            this.colContractDescription.OptionsColumn.ReadOnly = true;
            this.colContractDescription.Width = 190;
            // 
            // colClientNameContractDescription
            // 
            this.colClientNameContractDescription.Caption = "Client Name \\ Contract Description";
            this.colClientNameContractDescription.FieldName = "ClientNameContractDescription";
            this.colClientNameContractDescription.Name = "colClientNameContractDescription";
            this.colClientNameContractDescription.OptionsColumn.AllowEdit = false;
            this.colClientNameContractDescription.OptionsColumn.AllowFocus = false;
            this.colClientNameContractDescription.Visible = true;
            this.colClientNameContractDescription.VisibleIndex = 7;
            this.colClientNameContractDescription.Width = 274;
            // 
            // colSelectedVisitCount
            // 
            this.colSelectedVisitCount.Caption = "<b>Selected</b> Visits";
            this.colSelectedVisitCount.ColumnEdit = this.repositoryItemTextEditNumeric;
            this.colSelectedVisitCount.FieldName = "SelectedVisitCount";
            this.colSelectedVisitCount.Name = "colSelectedVisitCount";
            this.colSelectedVisitCount.OptionsColumn.AllowEdit = false;
            this.colSelectedVisitCount.OptionsColumn.AllowFocus = false;
            this.colSelectedVisitCount.OptionsColumn.ReadOnly = true;
            this.colSelectedVisitCount.Visible = true;
            this.colSelectedVisitCount.VisibleIndex = 2;
            this.colSelectedVisitCount.Width = 95;
            // 
            // repositoryItemTextEditNumeric
            // 
            this.repositoryItemTextEditNumeric.AutoHeight = false;
            this.repositoryItemTextEditNumeric.Mask.EditMask = "n0";
            this.repositoryItemTextEditNumeric.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric.Name = "repositoryItemTextEditNumeric";
            // 
            // colTotalVisitCount
            // 
            this.colTotalVisitCount.Caption = "<b>Total</b> Visit Count";
            this.colTotalVisitCount.ColumnEdit = this.repositoryItemTextEditNumeric;
            this.colTotalVisitCount.FieldName = "TotalVisitCount";
            this.colTotalVisitCount.Name = "colTotalVisitCount";
            this.colTotalVisitCount.OptionsColumn.AllowEdit = false;
            this.colTotalVisitCount.OptionsColumn.AllowFocus = false;
            this.colTotalVisitCount.OptionsColumn.ReadOnly = true;
            this.colTotalVisitCount.Visible = true;
            this.colTotalVisitCount.VisibleIndex = 4;
            this.colTotalVisitCount.Width = 102;
            // 
            // colFirstSelectedVisitNumber
            // 
            this.colFirstSelectedVisitNumber.Caption = "<b>First</b> Selected <b>#</b>";
            this.colFirstSelectedVisitNumber.ColumnEdit = this.repositoryItemTextEditNumeric;
            this.colFirstSelectedVisitNumber.FieldName = "FirstSelectedVisitNumber";
            this.colFirstSelectedVisitNumber.Name = "colFirstSelectedVisitNumber";
            this.colFirstSelectedVisitNumber.OptionsColumn.AllowEdit = false;
            this.colFirstSelectedVisitNumber.OptionsColumn.AllowFocus = false;
            this.colFirstSelectedVisitNumber.OptionsColumn.ReadOnly = true;
            this.colFirstSelectedVisitNumber.Visible = true;
            this.colFirstSelectedVisitNumber.VisibleIndex = 5;
            this.colFirstSelectedVisitNumber.Width = 100;
            // 
            // colSubsequentVisitCount
            // 
            this.colSubsequentVisitCount.Caption = "<b>Subsequent</b> Visits";
            this.colSubsequentVisitCount.ColumnEdit = this.repositoryItemTextEditNumeric;
            this.colSubsequentVisitCount.FieldName = "SubsequentVisitCount";
            this.colSubsequentVisitCount.Name = "colSubsequentVisitCount";
            this.colSubsequentVisitCount.OptionsColumn.AllowEdit = false;
            this.colSubsequentVisitCount.OptionsColumn.AllowFocus = false;
            this.colSubsequentVisitCount.OptionsColumn.ReadOnly = true;
            this.colSubsequentVisitCount.Visible = true;
            this.colSubsequentVisitCount.VisibleIndex = 3;
            this.colSubsequentVisitCount.Width = 113;
            // 
            // colNewFirstVisitNumber
            // 
            this.colNewFirstVisitNumber.Caption = "<b>New</b> First Visit <b>#</b>";
            this.colNewFirstVisitNumber.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colNewFirstVisitNumber.FieldName = "NewFirstVisitNumber";
            this.colNewFirstVisitNumber.Name = "colNewFirstVisitNumber";
            this.colNewFirstVisitNumber.Visible = true;
            this.colNewFirstVisitNumber.VisibleIndex = 6;
            this.colNewFirstVisitNumber.Width = 100;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.Mask.EditMask = "n0";
            this.repositoryItemSpinEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            99999,
            0,
            0,
            -2147483648});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // labelControl2
            // 
            this.labelControl2.AllowHtmlString = true;
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl2.Location = new System.Drawing.Point(9, 419);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(167, 13);
            this.labelControl2.TabIndex = 34;
            this.labelControl2.Text = "<b>Re-number</b> all subsequent visits:";
            // 
            // checkEditRenumberSubsequentVisits
            // 
            this.checkEditRenumberSubsequentVisits.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEditRenumberSubsequentVisits.EditValue = 1;
            this.checkEditRenumberSubsequentVisits.Location = new System.Drawing.Point(181, 416);
            this.checkEditRenumberSubsequentVisits.MenuManager = this.barManager1;
            this.checkEditRenumberSubsequentVisits.Name = "checkEditRenumberSubsequentVisits";
            this.checkEditRenumberSubsequentVisits.Properties.Caption = "[Tick if Yes]";
            this.checkEditRenumberSubsequentVisits.Properties.ValueChecked = 1;
            this.checkEditRenumberSubsequentVisits.Properties.ValueGrayed = 0;
            this.checkEditRenumberSubsequentVisits.Size = new System.Drawing.Size(75, 19);
            this.checkEditRenumberSubsequentVisits.TabIndex = 35;
            // 
            // sp06390_OM_Visit_ReNumbering_Site_Contracts_SelectedTableAdapter
            // 
            this.sp06390_OM_Visit_ReNumbering_Site_Contracts_SelectedTableAdapter.ClearBeforeFill = true;
            // 
            // btnApplyBlockEdit
            // 
            this.btnApplyBlockEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApplyBlockEdit.Location = new System.Drawing.Point(775, 88);
            this.btnApplyBlockEdit.Name = "btnApplyBlockEdit";
            this.btnApplyBlockEdit.Size = new System.Drawing.Size(40, 20);
            this.btnApplyBlockEdit.TabIndex = 36;
            this.btnApplyBlockEdit.Text = "Apply";
            this.btnApplyBlockEdit.ToolTip = "Click me to apply the block edit of Starting Visit Number against selected Site C" +
    "ontracts.";
            this.btnApplyBlockEdit.Click += new System.EventHandler(this.btnApplyBlockEdit_Click);
            // 
            // frm_OM_Visit_Renumber
            // 
            this.ClientSize = new System.Drawing.Size(823, 442);
            this.ControlBox = false;
            this.Controls.Add(this.btnApplyBlockEdit);
            this.Controls.Add(this.checkEditRenumberSubsequentVisits);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.spinEditVisitNumber);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OM_Visit_Renumber";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Re-number Visits";
            this.Load += new System.EventHandler(this.frm_OM_Visit_Renumber_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.spinEditVisitNumber, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.checkEditRenumberSubsequentVisits, 0);
            this.Controls.SetChildIndex(this.btnApplyBlockEdit, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharedImageCollection1.ImageSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharedImageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditVisitNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06390OMVisitReNumberingSiteContractsSelectedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditRenumberSubsequentVisits.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.SpinEdit spinEditVisitNumber;
        private DevExpress.Utils.SharedImageCollection sharedImageCollection1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CheckEdit checkEditRenumberSubsequentVisits;
        private System.Windows.Forms.BindingSource sp06390OMVisitReNumberingSiteContractsSelectedBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSelectedVisitCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalVisitCount;
        private DevExpress.XtraGrid.Columns.GridColumn colFirstSelectedVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSubsequentVisitCount;
        private DevExpress.XtraGrid.Columns.GridColumn colNewFirstVisitNumber;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DataSet_OM_VisitTableAdapters.sp06390_OM_Visit_ReNumbering_Site_Contracts_SelectedTableAdapter sp06390_OM_Visit_ReNumbering_Site_Contracts_SelectedTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientNameContractDescription;
        private DevExpress.XtraEditors.SimpleButton btnApplyBlockEdit;
        public DevExpress.XtraGrid.GridControl gridControl1;
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_OM_Job_Attribute_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        public int _ClientID = 0;
        public int _ClientContractID = 0;
        public int _SiteID = 0;
        public int _SiteContractID = 0;
        public int _VisitID = 0;
        public int _VisitNumber = 0;
        public int _JobID = 0;
        public int _JobTypeID = 0;
        public int _JobSubTypeID = 0;
        public string _ClientName = "";
        public string _ContractDescription = "";
        public string _SiteName = "";
        public string _JobTypeDescription = "";
        public string _JobSubTypeDescription = "";
        public int _MaxOrder;
        #endregion

        public frm_OM_Job_Attribute_Edit()
        {
            InitializeComponent();
        }
        
        private void frm_OM_Job_Attribute_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 500194;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            Binding binding = this.ValueRecordedDateEdit.DataBindings["EditValue"];
            binding.Format += new ConvertEventHandler(StringToDateTime);
            binding.Parse += new ConvertEventHandler(DateTimeToString);

            sp06202_OM_Attribute_Picklist_Headers_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06206_OM_Attribute_DataTypes_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06207_OM_Attribute_EditorTypes_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06481_OM_Attribute_Formula_Evaluate_Location_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06236_OM_Attribute_Picklist_Items_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            try
            {
                sp06202_OM_Attribute_Picklist_Headers_With_BlankTableAdapter.Fill(dataSet_OM_Job.sp06202_OM_Attribute_Picklist_Headers_With_Blank, 1);
                sp06206_OM_Attribute_DataTypes_With_BlankTableAdapter.Fill(dataSet_OM_Job.sp06206_OM_Attribute_DataTypes_With_Blank, 1);
                sp06207_OM_Attribute_EditorTypes_With_BlankTableAdapter.Fill(dataSet_OM_Job.sp06207_OM_Attribute_EditorTypes_With_Blank, 1);
                sp06481_OM_Attribute_Formula_Evaluate_Location_ListTableAdapter.Fill(dataSet_OM_Job.sp06481_OM_Attribute_Formula_Evaluate_Location_List);
                sp06236_OM_Attribute_Picklist_Items_With_BlankTableAdapter.Fill(dataSet_OM_Job.sp06236_OM_Attribute_Picklist_Items_With_Blank, 1);
            }
            catch (Exception) { }
            
            // Populate Main Dataset //    
            sp06234_OM_Job_Attribure_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();

            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow = dataSet_OM_Job.sp06234_OM_Job_Attribure_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["JobAttributeID"] = 0;
                        drNewRow["MasterJobAttributeID"] = 0;
                        drNewRow["JobID"] = _JobID;
                        drNewRow["DataTypeID"] = 0;
                        drNewRow["EditorTypeID"] = 0;
                        drNewRow["EditorMask"] = "";
                        drNewRow["DataEntryMinValue"] = (decimal)0.00;
                        drNewRow["DataEntryMaxValue"] = (decimal)0.00;
                        drNewRow["DataEntryRequired"] = 0;
                        drNewRow["PicklistHeaderID"] = 0;
                        drNewRow["OnScreenLabel"] = "";
                        drNewRow["AttributeOrder"] = _MaxOrder + 1;
                        drNewRow["DefaultValue"] = "";
                        drNewRow["ValueRecorded"] = "";
                        drNewRow["ValueRecordedText"] = "";
                        drNewRow["JobTypeJobSubTypeDescription"] = (strFormMode == "blockadd" ? "N\\A - Block Adding" : _JobTypeDescription + " - " + _JobSubTypeDescription);
                        drNewRow["ClientNameContractDescription"] =  (strFormMode == "blockadd" ? "N\\A - Block Adding" : (string.IsNullOrWhiteSpace(_ClientName) ? "" : _ClientName + ", Contract: " ) + (string.IsNullOrEmpty(_ContractDescription) ? "" : _ContractDescription));
                        drNewRow["ClientID"] = _ClientID;
                        drNewRow["ClientName"] = (strFormMode == "blockadd" ? "N\\A - Block Adding" : _ClientName);
                        drNewRow["SiteID"] = _SiteID;
                        drNewRow["SiteName"] = (strFormMode == "blockadd" ? "N\\A - Block Adding" : _SiteName);
                        drNewRow["VisitID"] = _VisitID;
                        drNewRow["VisitNumber"] = (strFormMode == "blockadd" ? 0 : _VisitNumber);
                        drNewRow["JobTypeID"] = _JobTypeID;
                        drNewRow["JobSubTypeID"] = _JobSubTypeID;
                        drNewRow["SiteContractID"] = _SiteContractID;
                        drNewRow["ClientContractID"] = _ClientContractID;
                        drNewRow["ContractDescription"] = (strFormMode == "blockadd" ? "N\\A - Block Adding" : _ContractDescription);
                        drNewRow["JobTypeDescription"] = (strFormMode == "blockadd" ? "N\\A - Block Adding" : _JobTypeDescription);
                        drNewRow["JobSubTypeDescription"] = (strFormMode == "blockadd" ? "N\\A - Block Adding" : _JobSubTypeDescription);
                        drNewRow["AttributeName"] = "";
                        drNewRow["ValueFormula"] = "";
                        drNewRow["ValueFormulaEvaluateLocation"] = 0;  // Server //
                        drNewRow["ValueFormulaEvaluateOrder"] = 0;
                        drNewRow["HideFromApp"] = 0;  // No //
                        dataSet_OM_Job.sp06234_OM_Job_Attribure_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        //DataRow drNewRow;
                        DataRow drNewRow = dataSet_OM_Job.sp06234_OM_Job_Attribure_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        dataSet_OM_Job.sp06234_OM_Job_Attribure_Edit.Rows.Add(drNewRow);
                        drNewRow.AcceptChanges();
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp06234_OM_Job_Attribure_EditTableAdapter.Fill(this.dataSet_OM_Job.sp06234_OM_Job_Attribure_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);

                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //

        }
        
        private void StringToDateTime(object sender, ConvertEventArgs cevent)
        {
            try
            {
                cevent.Value = Convert.ToDateTime(cevent.Value.ToString());
            }
            catch (Exception) { }

        }
        private void DateTimeToString(object sender, ConvertEventArgs cevent)
        {
            // The method converts only to string type. Test this using the DesiredType.
            if (cevent.DesiredType != typeof(string)) return;
            try
            {
                cevent.Value = Convert.ToDateTime(cevent.Value.ToString()).ToString("yyyy-MM-dd HH:mm:ss.fff");
            }
            catch (Exception) { }
        }

        private void frm_OM_Job_Attribute_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_OM_Job_Attribute_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                       
                        Binding binding = this.ValueRecordedDateEdit.DataBindings["EditValue"];
                        binding.Format -= new ConvertEventHandler(StringToDateTime);
                        binding.Parse -= new ConvertEventHandler(DateTimeToString);

                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

 
        private void Attach_EditValueChanged_To_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_OM_Job.sp06234_OM_Job_Attribure_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Job Attribute", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        JobTypeJobSubTypeDescriptionButtonEdit.Focus();

                        JobTypeJobSubTypeDescriptionButtonEdit.Properties.ReadOnly = false;
                        JobTypeJobSubTypeDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        AttributeOrderSpinEdit.Focus();

                        JobTypeJobSubTypeDescriptionButtonEdit.Properties.ReadOnly = true;
                        JobTypeJobSubTypeDescriptionButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ValueRecordedMemoEdit.Focus();

                        JobTypeJobSubTypeDescriptionButtonEdit.Properties.ReadOnly = false;
                        JobTypeJobSubTypeDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ValueRecordedMemoEdit.Focus();

                        JobTypeJobSubTypeDescriptionButtonEdit.Properties.ReadOnly = false;
                        JobTypeJobSubTypeDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
            Set_Control_Readonly_Status("");
            ibool_ignoreValidation = false;
            ibool_FormStillLoading = false;
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_OM_Job.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            int intID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp06234OMJobAttribureEditBindingSource.Current;
            if (currentRow != null)
            {
                intID = (currentRow["JobAttributeID"] == null ? 0 : Convert.ToInt32(currentRow["JobAttributeID"]));
            }
            bbiLinkedDocuments.Enabled = intID > 0;  // Set status of button //
            
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Saving Changes...");

            this.sp06234OMJobAttribureEditBindingSource.EndEdit();
            try
            {
                sp06234_OM_Job_Attribure_EditTableAdapter.Update(dataSet_OM_Job);  // Insert and Update queries defined in Table Adapter //
            }
            catch (Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                var currentRowView = (DataRowView)sp06234OMJobAttribureEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06234_OM_Job_Attribure_EditRow)currentRowView.Row;
                this.strFormMode = "edit";  // Switch mode to Edit so than any subsequent changes update this record //
                if (currentRow != null)
                {
                    currentRow.strMode = "edit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    strNewIDs = currentRow.JobAttributeID + ";";
                }
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                var currentRowView = (DataRowView)sp06234OMJobAttribureEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06234_OM_Job_Attribure_EditRow)currentRowView.Row;
                this.strFormMode = "blockedit";  // Switch mode to BlockEdit so than any subsequent changes update these record //
                if (currentRow != null)
                {
                    currentRow.strMode = "blockedit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Values returned from Update SP //
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_OM_Job_Manager")
                    {
                        var fParentForm = (frm_OM_Job_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(2, Utils.enmFocusedGrid.ExtraInfo, strNewIDs);
                    }
                    /*else if (frmChild.Name == "frm_OM_Job_Edit")
                    {
                        var fParentForm = (frm_OM_Job_Edit)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Materials, strNewIDs);
                    }*/
                    else if (frmChild.Name == "frm_OM_Visit_Manager")
                    {
                        var fParentForm = (frm_OM_Visit_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(59, Utils.enmFocusedGrid.ExtraInfo, strNewIDs);
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_OM_Job.sp06234_OM_Job_Attribure_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_OM_Job.sp06234_OM_Job_Attribure_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    Set_Control_Readonly_Status("");
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void JobTypeJobSubTypeDescriptionButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockadd") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06234OMJobAttribureEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06234_OM_Job_Attribure_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                int intClientContractID = (string.IsNullOrEmpty(currentRow.ClientContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientContractID));
                int intSiteContractID = (string.IsNullOrEmpty(currentRow.SiteContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteContractID));
                int intVisitID = (string.IsNullOrEmpty(currentRow.VisitID.ToString()) ? 0 : Convert.ToInt32(currentRow.VisitID));
                int intJobID = (string.IsNullOrEmpty(currentRow.JobID.ToString()) ? 0 : Convert.ToInt32(currentRow.JobID));
                var fChildForm = new frm_OM_Select_Job();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalClientContractID = intClientContractID;
                fChildForm.intOriginalSiteContractID = intSiteContractID;
                fChildForm.intOriginalVisitID = intVisitID;
                fChildForm.intOriginalJobID = intJobID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.ClientID = fChildForm.intSelectedClientID;
                    currentRow.ClientContractID = fChildForm.intSelectedClientContractID;
                    currentRow.SiteID = fChildForm.intSelectedSiteID;
                    currentRow.SiteContractID = fChildForm.intSelectedSiteContractID;
                    currentRow.VisitID = fChildForm.intSelectedVisitID;
                    currentRow.VisitNumber = fChildForm.intSelectedVisitNumber;
                    currentRow.JobID = fChildForm.intSelectedJobID;
                    currentRow.JobTypeID = fChildForm.intSelectedJobTypeID;
                    currentRow.JobSubTypeID = fChildForm.intSelectedJobSubTypeID;
                    currentRow.ClientName = fChildForm.strSelectedClientName;
                    currentRow.ContractDescription = fChildForm.strSelectedContractDescription;
                    currentRow.SiteName = fChildForm.strSelectedSiteName;
                    currentRow.JobTypeDescription = fChildForm.strSelectedJobTypeDescription;
                    currentRow.JobSubTypeDescription = fChildForm.strSelectedJobSubTypeDescription;
                    currentRow.JobTypeJobSubTypeDescription = fChildForm.strSelectedJobTypeDescription + " - " + fChildForm.strSelectedJobSubTypeDescription;
                    currentRow.JobExpectedStartDate = fChildForm.dtSelectedJobExpectedStartDate;
                    sp06234OMJobAttribureEditBindingSource.EndEdit();
                }
            }
        }
        private void JobTypeJobSubTypeDescriptionButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if ((this.strFormMode == "add" || this.strFormMode == "edit") && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(JobTypeJobSubTypeDescriptionButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(JobTypeJobSubTypeDescriptionButtonEdit, "");
            }
        }

        private void AttributeNameTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if ((this.strFormMode == "add" || this.strFormMode == "blockadd" || this.strFormMode == "edit") && string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(AttributeNameTextEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex("^[a-zA-Z0-9_-]+$"); 
            if (!r.IsMatch(te.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(AttributeNameTextEdit, "Invalid Name entered - Only Letters, Numbers, Hyphens and Underscores can be entered.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(AttributeNameTextEdit, "");
            }
        }

        private void OnScreenLabelTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if ((this.strFormMode == "add" || this.strFormMode == "blockadd" || this.strFormMode == "edit") && string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(OnScreenLabelTextEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(OnScreenLabelTextEdit, "");
            }
        }

        private void DataTypeIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("DataTypeID");
        }
        private void DataTypeIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if ((this.strFormMode == "add" || this.strFormMode == "edit") && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(DataTypeIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(DataTypeIDGridLookUpEdit, "");
            }
        }

        DataTable data;  // Used for holding filtered dataset for picklist //
        private void EditorTypeIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            Set_Control_Readonly_Status("EditorTypeID");
        }
        private void EditorTypeIDGridLookUpEdit_Enter(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            int intHeaderID = Convert.ToInt32(glue.Tag);
            data = this.dataSet_OM_Job.sp06207_OM_Attribute_EditorTypes_With_Blank;
            DataView newView = new DataView(data);

            string strFilter = "";
            int intDataTypeID = (DataTypeIDGridLookUpEdit.EditValue == DBNull.Value ? 0 : Convert.ToInt32(DataTypeIDGridLookUpEdit.EditValue));
            switch (intDataTypeID)
            {
                case 1:  // Text Short //
                    {
                        strFilter = "[ID] = 0 or [ID] = 1";
                    }
                    break;
                case 2:  // Text Long //
                    {
                        strFilter = "[ID] = 0 or [ID] = 2 or [ID] = 3";
                    }
                    break;
                case 3:  // Integer //
                    {
                        strFilter = "[ID] = 0 or [ID] = 4 or [ID] = 5 or [ID] = 6";
                    }
                    break;
                case 4:  // Decimal //
                case 5:  // Money //
                    {
                        strFilter = "[ID] = 0 or [ID] = 4";
                    }
                    break;
                case 6:  // DateTime //
                    {
                        strFilter = "[ID] = 0 or [ID] = 7";
                    }
                    break;
                case 7:  // Image //
                    {
                        strFilter = "[ID] = 0 or [ID] = 8";
                    }
                    break;
                case 8:  // Calculated //
                    {
                        strFilter = "[ID] = 0 or [ID] = 9";
                    }
                    break;
                default:
                    {
                        strFilter = "[ID] = 0";
                    }
                    break;
            }
            newView.RowFilter = strFilter;
            glue.Properties.DataSource = newView;
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }
        private void EditorTypeIDGridLookUpEdit_Leave(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            glue.Properties.DataSource = this.dataSet_OM_Job.sp06207_OM_Attribute_EditorTypes_With_Blank;
            view.EndUpdate();
        }
        private void EditorTypeIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if ((this.strFormMode == "add" || this.strFormMode == "edit") && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(EditorTypeIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(EditorTypeIDGridLookUpEdit, "");
            }
        }

        private void EditorMaskTextEdit_EditValueChanged(object sender, EventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            int intEditorTypeID = (EditorTypeIDGridLookUpEdit.EditValue == DBNull.Value ? 0 : Convert.ToInt32(EditorTypeIDGridLookUpEdit.EditValue));
            if (intEditorTypeID == 4)
            {
                if (!string.IsNullOrWhiteSpace(te.EditValue.ToString()))
                {
                    //ValueRecordedSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
                    //ValueRecordedSpinEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
                    //ValueRecordedSpinEdit.Properties.Mask.EditMask = te.EditValue.ToString();
                    ValueRecordedSpinEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                    ValueRecordedSpinEdit.Properties.DisplayFormat.FormatString = te.EditValue.ToString();
                    ValueRecordedSpinEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                    ValueRecordedSpinEdit.Properties.EditFormat.FormatString = te.EditValue.ToString();
                }
                else
                {
                    //ValueRecordedSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = false;
                    //ValueRecordedSpinEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
                    //ValueRecordedSpinEdit.Properties.Mask.EditMask = null;
                    ValueRecordedSpinEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.None;
                    ValueRecordedSpinEdit.Properties.DisplayFormat.FormatString = null;
                    ValueRecordedSpinEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.None;
                    ValueRecordedSpinEdit.Properties.EditFormat.FormatString = null;
                }
            }
            else if (intEditorTypeID == 7)
            {
                if (!string.IsNullOrWhiteSpace(te.EditValue.ToString()))
                {
                    //ValueRecordedDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
                    //ValueRecordedDateEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                    //ValueRecordedDateEdit.Properties.Mask.EditMask = te.EditValue.ToString(); ;
                    ValueRecordedDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                    ValueRecordedDateEdit.Properties.DisplayFormat.FormatString = te.EditValue.ToString();
                    ValueRecordedDateEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                    ValueRecordedDateEdit.Properties.EditFormat.FormatString = te.EditValue.ToString();
                }
                else
                {
                    //ValueRecordedDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
                    //ValueRecordedDateEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                    //ValueRecordedDateEdit.Properties.Mask.EditMask = "g";
                    ValueRecordedDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                    ValueRecordedDateEdit.Properties.DisplayFormat.FormatString = "{0:g}";
                    ValueRecordedDateEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                    ValueRecordedDateEdit.Properties.EditFormat.FormatString = "{0:g}";
                }
            }
        }

        private void PicklistHeaderIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if ((this.strFormMode == "add" || this.strFormMode == "edit") && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                int intEditorTypeID = (EditorTypeIDGridLookUpEdit.EditValue == DBNull.Value ? 0 : Convert.ToInt32(EditorTypeIDGridLookUpEdit.EditValue));
                if (intEditorTypeID == 6)  // Picklist //
                {
                    dxErrorProvider1.SetError(PicklistHeaderIDGridLookUpEdit, "Select a value.");
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
                else
                {
                    dxErrorProvider1.SetError(PicklistHeaderIDGridLookUpEdit, "");
                }
            }
            else
            {
                dxErrorProvider1.SetError(PicklistHeaderIDGridLookUpEdit, "");
            }
        }
        
        private void ValueFormulaMemoExEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if (e.Button.Tag.ToString() == "formula_builder")  // Choose Button //
                {
                    if (strFormMode == "view") return;
                    int intDataTypeID = (DataTypeIDGridLookUpEdit.EditValue == DBNull.Value ? 0 : Convert.ToInt32(DataTypeIDGridLookUpEdit.EditValue));
                    if (intDataTypeID != 8) return;

                    var currentRowView = (DataRowView)sp06234OMJobAttribureEditBindingSource.Current;
                    var currentRow = (DataSet_OM_Job.sp06234_OM_Job_Attribure_EditRow)currentRowView.Row;
                    if (currentRow == null) return;
                    string strFormula = currentRow.ValueFormula;

                    int intParentID = 0;
                    try { intParentID = currentRow.JobID; }
                    catch (Exception) { }

                    if (intParentID <= 0)
                    {
                        XtraMessageBox.Show("Select the Job Description first then try again.\n\n", "Set Forumula", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    string strAttributeName = currentRow.AttributeName;
                    if (string.IsNullOrWhiteSpace(strAttributeName))
                    {
                        XtraMessageBox.Show("Enter the Attribute Name first then try again.\n\n", "Set Forumula", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    int intRecordID = 0;
                    try { intRecordID = currentRow.JobAttributeID; }
                    catch (Exception) { }

                    int intFormulaEvaluateOrder = 0;
                    try { intFormulaEvaluateOrder = currentRow.ValueFormulaEvaluateOrder; }
                    catch (Exception) { }

                    frm_OM_Formula_Builder fChildForm = new frm_OM_Formula_Builder();
                    this.ParentForm.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strFormula = strFormula;
                    fChildForm.intCallerID = 2;
                    fChildForm.strAttributeName = strAttributeName;
                    fChildForm.intParentRecordID = intParentID;
                    fChildForm.intRecordID = intRecordID;
                    fChildForm.intFormulaEvaluateOrder = intFormulaEvaluateOrder;
                    if (fChildForm.ShowDialog() == DialogResult.OK)
                    {
                        if (currentRow.ValueFormula != fChildForm.strFormula)
                        {
                            currentRow.ValueFormula = fChildForm.strFormula;
                        }
                    }
                }
            }
        }

        DataTable data2;  // Used for holding filtered dataset for picklist //
        private void ValueRecordedTextGridLookUpEdit_Enter(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            int intHeaderID = (PicklistHeaderIDGridLookUpEdit.EditValue == DBNull.Value ? 0 : Convert.ToInt32(PicklistHeaderIDGridLookUpEdit.EditValue));
            data2 = this.dataSet_OM_Job.sp06236_OM_Attribute_Picklist_Items_With_Blank;
            DataView newView = new DataView(data2);

            newView.RowFilter = "[HeaderID] = 0 or [HeaderID] = " + intHeaderID.ToString();
            glue.Properties.DataSource = newView;
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }
        private void ValueRecordedTextGridLookUpEdit_Leave(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            glue.Properties.DataSource = this.dataSet_OM_Job.sp06236_OM_Attribute_Picklist_Items_With_Blank;
            view.EndUpdate();
        }
        private void ValueRecordedTextGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
        }

        private void Set_Control_Readonly_Status(string strCheckWhat)
        {
            if (strFormMode == "view") return;

            decimal decDataEntryMinValue = (DataEntryMinValueSpinEdit.EditValue == DBNull.Value ? (decimal)0.00 : Convert.ToDecimal(DataEntryMinValueSpinEdit.EditValue));
            decimal decDataEntryMaxValue = (DataEntryMaxValueSpinEdit.EditValue == DBNull.Value ? (decimal)0.00 : Convert.ToDecimal(DataEntryMaxValueSpinEdit.EditValue));
            string strDefaultValue = (DefaultValueMemoEdit.EditValue == DBNull.Value ? "" : DefaultValueMemoEdit.EditValue.ToString());
            int intPicklistHeaderID = (PicklistHeaderIDGridLookUpEdit.EditValue == DBNull.Value ? 0 : Convert.ToInt32(PicklistHeaderIDGridLookUpEdit.EditValue));
            string strEditorMask = (EditorMaskTextEdit.EditValue == DBNull.Value ? "" : EditorMaskTextEdit.EditValue.ToString());

            int intDataTypeID = (DataTypeIDGridLookUpEdit.EditValue == DBNull.Value ? 0 : Convert.ToInt32(DataTypeIDGridLookUpEdit.EditValue));
            int intEditorTypeID = (EditorTypeIDGridLookUpEdit.EditValue == DBNull.Value ? 0 : Convert.ToInt32(EditorTypeIDGridLookUpEdit.EditValue));

            // Check Data Type //
            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "DataTypeID")
            {
                switch (intDataTypeID)
                {
                    case 1:  // Text Short //
                        {
                            if (!(intEditorTypeID == 1))  // Text Edit //
                            {
                                EditorTypeIDGridLookUpEdit.EditValue = 1;  // Switch To Text Edit //
                                strCheckWhat = "EditorTypeID";  // Toggle this value so we force a check on this field later in this process //
                            }
                        }
                        break;
                    case 2:  // Text Long //
                        {
                            if (!(intEditorTypeID == 2 || intEditorTypeID == 3))  // Memo Edit, Memo Ex Edit //
                            {
                                EditorTypeIDGridLookUpEdit.EditValue = 0;  // Clear EditorTypeID //
                                strCheckWhat = "EditorTypeID";  // Toggle this value so we force a check on this field later in this process //
                            }
                        }
                        break;
                    case 3:  // Integer //
                        {
                            if (!(intEditorTypeID == 4 || intEditorTypeID == 5 || intEditorTypeID == 6))  // Spinner, CheckBox, Picklist //
                            {
                                EditorTypeIDGridLookUpEdit.EditValue = 0;  // Clear EditorTypeID //
                                strCheckWhat = "EditorTypeID";  // Toggle this value so we force a check on this field later in this process //
                            }
                        }
                        break;
                    case 4:  // Decimal //
                    case 5:  // Money //
                        {
                            if (!(intEditorTypeID == 4))  // Spinner //
                            {
                                EditorTypeIDGridLookUpEdit.EditValue = 4;  // Switch To Spinner //
                                strCheckWhat = "EditorTypeID";  // Toggle this value so we force a check on this field later in this process //
                            }
                        }
                        break;
                    case 6:  // DateTime //
                        {
                            if (!(intEditorTypeID == 7))  // DateTime Edit //
                            {
                                EditorTypeIDGridLookUpEdit.EditValue = 7;  // Switch to DateTime Edit //
                                strCheckWhat = "EditorTypeID";  // Toggle this value so we force a check on this field later in this process //
                            }
                        }
                        break;
                    case 7:  // Image //
                        {
                            if (!(intEditorTypeID == 8))  // Image Edit //
                            {
                                EditorTypeIDGridLookUpEdit.EditValue = 8;  // Switch to Image Edit //
                                strCheckWhat = "EditorTypeID";  // Toggle this value so we force a check on this field later in this process //
                            }
                        }
                        break;
                    case 8:  // Calculated //
                        {
                            if (!(intEditorTypeID == 9))  // Formula Builder Edit //
                            {
                                EditorTypeIDGridLookUpEdit.EditValue = 9;  // Switch to Formula Builder Edit //
                                ValueFormulaEvaluateLocationGridLookUpEdit.EditValue = 0;  // Run at Server //
                                ValueFormulaEvaluateOrderSpinEdit.EditValue = 0;  // Evaluate Order //
                                strCheckWhat = "EditorTypeID";  // Toggle this value so we force a check on this field later in this process //
                            }
                        }
                        break;
                    default:
                        {
                            EditorTypeIDGridLookUpEdit.EditValue = 0;  // Unknown so clear value //
                            strCheckWhat = "EditorTypeID";  // Toggle this value so we force a check on this field later in this process //
                        }
                        break;
                }
                if (!(intDataTypeID == 3 || intDataTypeID == 4 || intDataTypeID == 5 || intDataTypeID == 6) && !string.IsNullOrWhiteSpace(strEditorMask))
                {
                    EditorMaskTextEdit.EditValue = "";
                }
            }

            // Check Editor Type //
            intEditorTypeID = (EditorTypeIDGridLookUpEdit.EditValue == DBNull.Value ? 0 : Convert.ToInt32(EditorTypeIDGridLookUpEdit.EditValue));
            if (string.IsNullOrEmpty(strCheckWhat) || strCheckWhat == "EditorTypeID")
            {
                if (intEditorTypeID != 4)  // Spinner //
                {
                    if (decDataEntryMinValue != (decimal)0.00) DataEntryMinValueSpinEdit.EditValue = (decimal)0.00;
                    if (decDataEntryMaxValue != (decimal)0.00) DataEntryMaxValueSpinEdit.EditValue = (decimal)0.00;
                }
                if (intEditorTypeID != 6)  // Picklist //
                {
                    if (intPicklistHeaderID != 0) PicklistHeaderIDGridLookUpEdit.EditValue = 0;
                    if (intPicklistHeaderID != 0) ValueRecordedGridLookUpEdit.EditValue = 0;
                }
                if (intEditorTypeID == 8)  // Image Edit //
                {
                    if (!string.IsNullOrWhiteSpace(strDefaultValue)) DefaultValueMemoEdit.EditValue = "";
                }

                string strMask = (EditorMaskTextEdit.EditValue == DBNull.Value ? "" : EditorMaskTextEdit.EditValue.ToString());
                switch (intEditorTypeID)
                {
                    case 1:  // Text Edit //
                        {
                            ItemForValueRecordedTextEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                            ValueRecordedTextEdit.Properties.ReadOnly = false;

                            ItemForValueRecordedMemoEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedMemoEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedMemoExEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedMemoExEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedSpinEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedSpinEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedCheckEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedCheckEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedPicklist.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedGridLookUpEdit.Properties.ReadOnly = true;
                            
                            ItemForValueRecordedDateEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedDateEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedPictureEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedPictureEdit.Properties.ReadOnly = true;
                        }
                        break;
                    case 2:  // Memo Edit //
                        {
                            ItemForValueRecordedTextEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedTextEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedMemoEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                            ValueRecordedMemoEdit.Properties.ReadOnly = false;

                            ItemForValueRecordedMemoExEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedMemoExEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedSpinEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedSpinEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedCheckEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedCheckEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedPicklist.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedGridLookUpEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedDateEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedDateEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedPictureEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedPictureEdit.Properties.ReadOnly = true;
                        }
                        break;
                    case 3:  // Memo Ex Edit //
                        {
                            ItemForValueRecordedTextEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedTextEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedMemoEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedMemoEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedMemoExEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                            ValueRecordedMemoExEdit.Properties.ReadOnly = false;

                            ItemForValueRecordedSpinEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedSpinEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedCheckEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedCheckEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedPicklist.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedGridLookUpEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedDateEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedDateEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedPictureEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedPictureEdit.Properties.ReadOnly = true;
                        }
                        break;
                    case 4:  // Spinner Edit //
                        {
                            ItemForValueRecordedTextEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedTextEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedMemoEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedMemoEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedMemoExEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedMemoExEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedSpinEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                            ValueRecordedSpinEdit.Properties.ReadOnly = false;

                            ItemForValueRecordedCheckEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedCheckEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedPicklist.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedGridLookUpEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedDateEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedDateEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedPictureEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedPictureEdit.Properties.ReadOnly = true;

                            if (!string.IsNullOrWhiteSpace(strMask))
                            {
                                //ValueRecordedSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
                                //ValueRecordedSpinEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
                                //ValueRecordedSpinEdit.Properties.Mask.EditMask = strMask;
                                ValueRecordedSpinEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                                ValueRecordedSpinEdit.Properties.DisplayFormat.FormatString = strMask;
                                ValueRecordedSpinEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                                ValueRecordedSpinEdit.Properties.EditFormat.FormatString = strMask;
                            }
                            else
                            {
                                //ValueRecordedSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = false;
                                //ValueRecordedSpinEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
                                //ValueRecordedSpinEdit.Properties.Mask.EditMask = null;
                                ValueRecordedSpinEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.None;
                                ValueRecordedSpinEdit.Properties.DisplayFormat.FormatString = null;
                                ValueRecordedSpinEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.None;
                                ValueRecordedSpinEdit.Properties.EditFormat.FormatString = null;
                            }
                        }
                        break;
                    case 5:  // Checkbox Edit //
                        {
                            ItemForValueRecordedTextEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedTextEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedMemoEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedMemoEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedMemoExEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedMemoExEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedSpinEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedSpinEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedCheckEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                            ValueRecordedCheckEdit.Properties.ReadOnly = false;

                            ItemForValueRecordedPicklist.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedGridLookUpEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedDateEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedDateEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedPictureEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedPictureEdit.Properties.ReadOnly = true;
                        }
                        break;
                    case 6:  // Picklist Edit //
                        {
                            ItemForValueRecordedTextEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedTextEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedMemoEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedMemoEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedMemoExEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedMemoExEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedSpinEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedSpinEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedCheckEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedCheckEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedPicklist.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                            ValueRecordedGridLookUpEdit.Properties.ReadOnly = false;

                            ItemForValueRecordedDateEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedDateEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedPictureEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedPictureEdit.Properties.ReadOnly = true;
                        }
                        break;
                    case 7:  // DateTime Edit //
                        {
                            ItemForValueRecordedTextEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedTextEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedMemoEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedMemoEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedMemoExEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedMemoExEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedSpinEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedSpinEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedCheckEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedCheckEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedPicklist.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedGridLookUpEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedDateEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                            ValueRecordedDateEdit.Properties.ReadOnly = false;

                            ItemForValueRecordedPictureEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedPictureEdit.Properties.ReadOnly = true;

                            if (!string.IsNullOrWhiteSpace(strMask))
                            {
                                //ValueRecordedDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
                                //ValueRecordedDateEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                                //ValueRecordedDateEdit.Properties.Mask.EditMask = strMask;
                                ValueRecordedDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                                ValueRecordedDateEdit.Properties.DisplayFormat.FormatString = strMask;
                                ValueRecordedDateEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                                ValueRecordedDateEdit.Properties.EditFormat.FormatString = strMask;
                            }
                            else
                            {
                                //ValueRecordedDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
                                //ValueRecordedDateEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                                //ValueRecordedDateEdit.Properties.Mask.EditMask = "g";
                                ValueRecordedDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                                ValueRecordedDateEdit.Properties.DisplayFormat.FormatString = "{0:g}";
                                ValueRecordedDateEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                                ValueRecordedDateEdit.Properties.EditFormat.FormatString = "{0:g}";
                            }
                        }
                        break;
                    case 8:  // Image Edit //
                        {
                            ItemForValueRecordedTextEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedTextEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedMemoEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedMemoEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedMemoExEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedMemoExEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedSpinEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedSpinEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedCheckEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedCheckEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedPicklist.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedGridLookUpEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedDateEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedDateEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedPictureEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                            ValueRecordedPictureEdit.Properties.ReadOnly = false;
                        }
                        break;
                    case 9:  // Calculated //
                        {
                            ItemForValueRecordedTextEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedTextEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedMemoEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                            ValueRecordedMemoEdit.Properties.ReadOnly = false;

                            ItemForValueRecordedMemoExEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedMemoExEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedSpinEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedSpinEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedCheckEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedCheckEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedPicklist.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedGridLookUpEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedDateEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedDateEdit.Properties.ReadOnly = true;

                            ItemForValueRecordedPictureEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                            ValueRecordedPictureEdit.Properties.ReadOnly = true;
                        }
                        break;
                }
            }

            EditorTypeIDGridLookUpEdit.Properties.ReadOnly = !(intDataTypeID > 0);
            DataEntryMinValueSpinEdit.Properties.ReadOnly = !(intEditorTypeID == 4);
            DataEntryMaxValueSpinEdit.Properties.ReadOnly = !(intEditorTypeID == 4);
            DefaultValueMemoEdit.Properties.ReadOnly = (intEditorTypeID == 8);
            PicklistHeaderIDGridLookUpEdit.Properties.ReadOnly = (intEditorTypeID != 6);
            ValueRecordedGridLookUpEdit.Properties.ReadOnly = (intEditorTypeID != 6);
            EditorMaskTextEdit.Properties.ReadOnly = !(intDataTypeID == 3 || intDataTypeID == 4 || intDataTypeID == 5 || intDataTypeID == 6);
            ValueFormulaMemoExEdit.Properties.ReadOnly = !(intDataTypeID == 8);
            ValueFormulaEvaluateLocationGridLookUpEdit.Properties.ReadOnly = !(intDataTypeID == 8);
            ValueFormulaEvaluateOrderSpinEdit.Properties.ReadOnly = !(intDataTypeID == 8);
            if (!ibool_FormStillLoading) this.ValidateChildren();
        }

        #endregion


        private void bbiLinkedDocuments_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "blockedit" || strFormMode == "blockadd") return;
            DataRowView currentRow = (DataRowView)sp06234OMJobAttribureEditBindingSource.Current;
            if (currentRow == null) return;
            int intRecordID = (string.IsNullOrEmpty(currentRow["JobAttributeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["JobAttributeID"]));
            if (intRecordID <= 0) return;

            // Drilldown to Linked Document Manager //
            int intRecordType = 31;  // Attribute //
            int intRecordSubType = 0;  // Not Used //
            string strRecordDescription = currentRow["ClientName"].ToString() + ", Contract: " + currentRow["ContractDescription"].ToString() + ", Site: " + currentRow["SiteName"].ToString() + ", Visit #: " + currentRow["VisitNumber"].ToString() + ", Job: " + currentRow["JobTypeDescription"].ToString() + " - " + currentRow["JobSubTypeDescription"].ToString() + ", Attribute: " + currentRow["OnScreenLabel"].ToString();
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp06049_OM_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "om_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);

        }










    }
}


namespace WoodPlan5
{
    partial class frm_OM_Client_Contract_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Client_Contract_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.DefaultCompletionSheetTemplateIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06059OMClientContractEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.sp01055CoreGetReportLayoutsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Common_Functionality = new WoodPlan5.DataSet_Common_Functionality();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colReportLayoutID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportLayoutName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModuleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPublishedToWeb = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.CMTenderRequestNotesFileButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.TenderProactiveDaysToReturnQuoteSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TenderReactiveDaysToReturnQuoteSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SignatureForClientBillingCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SignatureForTeamSelfBillingCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ClientInstructionsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ReactiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ContractDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CostCentreIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CostCentreCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CostCentreTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LinkedToPreviousContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LinkedToPreviousContractButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.LastClientPaymentDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.BillingCentreCodeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.BillingCentreCodeButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.FinanceClientCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CheckRPIDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.YearlyPercentageIncreaseSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ContractValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ActiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.EndDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.StartDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ContractTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06063OMContractTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ContractDirectorIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ContractDirectorButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ContractStatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06062OMContractStatusesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SectorTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06061OMSectorsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GCCompanyIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06060OMGCCompaniesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ClientIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.ClientNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ClientContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForClientContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractDirectorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBillingCentreCodeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToPreviousContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostCentreCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostCentre = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostCentreID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layGrpAddress = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForGCCompanyID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSectorTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractDirector = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForActive = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForContractValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForYearlyPercentageIncrease = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCheckRPIDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForFinanceClientCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForBillingCentreCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLastClientPaymentDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForLinkedToPreviousContract = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForReactive = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientInstructions = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForSignatureForTeamSelfBilling = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemforSignatureForClientBilling = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTenderReactiveDaysToReturnQuote = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCMTenderRequestNotesFile = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForTenderProactiveDaysToReturnQuote = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem20 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDefaultCompletionSheetTemplateID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp06059_OM_Client_Contract_EditTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06059_OM_Client_Contract_EditTableAdapter();
            this.sp06060_OM_GC_Companies_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06060_OM_GC_Companies_With_BlankTableAdapter();
            this.sp06061_OM_Sectors_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06061_OM_Sectors_With_BlankTableAdapter();
            this.sp06062_OM_Contract_Statuses_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06062_OM_Contract_Statuses_With_BlankTableAdapter();
            this.sp06063_OM_Contract_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06063_OM_Contract_Types_With_BlankTableAdapter();
            this.sp01055_Core_Get_Report_LayoutsTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01055_Core_Get_Report_LayoutsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultCompletionSheetTemplateIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06059OMClientContractEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01055CoreGetReportLayoutsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CMTenderRequestNotesFileButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderProactiveDaysToReturnQuoteSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderReactiveDaysToReturnQuoteSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureForClientBillingCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureForTeamSelfBillingCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientInstructionsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPreviousContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPreviousContractButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastClientPaymentDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastClientPaymentDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BillingCentreCodeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BillingCentreCodeButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinanceClientCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckRPIDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckRPIDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YearlyPercentageIncreaseSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06063OMContractTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractDirectorIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractDirectorButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractStatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06062OMContractStatusesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SectorTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06061OMSectorsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GCCompanyIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06060OMGCCompaniesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractDirectorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBillingCentreCodeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPreviousContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCentreCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCentre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCentreID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGCCompanyID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSectorTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractDirector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForYearlyPercentageIncrease)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCheckRPIDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinanceClientCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBillingCentreCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastClientPaymentDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPreviousContract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientInstructions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSignatureForTeamSelfBilling)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforSignatureForClientBilling)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderReactiveDaysToReturnQuote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCMTenderRequestNotesFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderProactiveDaysToReturnQuote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultCompletionSheetTemplateID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 631);
            this.barDockControlBottom.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 605);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(686, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 605);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "ID";
            this.gridColumn7.FieldName = "ID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 53;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 53;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 53;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.Width = 53;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(686, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 631);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(686, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 605);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(686, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 605);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.DefaultCompletionSheetTemplateIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.CMTenderRequestNotesFileButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.TenderProactiveDaysToReturnQuoteSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TenderReactiveDaysToReturnQuoteSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SignatureForClientBillingCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SignatureForTeamSelfBillingCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientInstructionsMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ReactiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CostCentreIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CostCentreCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CostCentreTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToPreviousContractIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToPreviousContractButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.LastClientPaymentDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.BillingCentreCodeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.BillingCentreCodeButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.FinanceClientCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CheckRPIDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.YearlyPercentageIncreaseSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ActiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.EndDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.StartDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractDirectorIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractDirectorButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractStatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.SectorTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.GCCompanyIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.ClientNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientContractIDTextEdit);
            this.dataLayoutControl1.DataSource = this.sp06059OMClientContractEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientContractID,
            this.ItemForClientID,
            this.ItemForContractDirectorID,
            this.ItemForBillingCentreCodeID,
            this.ItemForLinkedToPreviousContractID,
            this.ItemForCostCentreCode,
            this.ItemForCostCentre,
            this.ItemForCostCentreID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1234, 162, 573, 422);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(686, 605);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // DefaultCompletionSheetTemplateIDGridLookUpEdit
            // 
            this.DefaultCompletionSheetTemplateIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "DefaultCompletionSheetTemplateID", true));
            this.DefaultCompletionSheetTemplateIDGridLookUpEdit.Location = new System.Drawing.Point(201, 242);
            this.DefaultCompletionSheetTemplateIDGridLookUpEdit.MenuManager = this.barManager1;
            this.DefaultCompletionSheetTemplateIDGridLookUpEdit.Name = "DefaultCompletionSheetTemplateIDGridLookUpEdit";
            this.DefaultCompletionSheetTemplateIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DefaultCompletionSheetTemplateIDGridLookUpEdit.Properties.DataSource = this.sp01055CoreGetReportLayoutsBindingSource;
            this.DefaultCompletionSheetTemplateIDGridLookUpEdit.Properties.DisplayMember = "ReportLayoutName";
            this.DefaultCompletionSheetTemplateIDGridLookUpEdit.Properties.NullText = "";
            this.DefaultCompletionSheetTemplateIDGridLookUpEdit.Properties.PopupView = this.gridView1;
            this.DefaultCompletionSheetTemplateIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.DefaultCompletionSheetTemplateIDGridLookUpEdit.Properties.ValueMember = "ReportLayoutID";
            this.DefaultCompletionSheetTemplateIDGridLookUpEdit.Size = new System.Drawing.Size(432, 20);
            this.DefaultCompletionSheetTemplateIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.DefaultCompletionSheetTemplateIDGridLookUpEdit.TabIndex = 13;
            // 
            // sp06059OMClientContractEditBindingSource
            // 
            this.sp06059OMClientContractEditBindingSource.DataMember = "sp06059_OM_Client_Contract_Edit";
            this.sp06059OMClientContractEditBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp01055CoreGetReportLayoutsBindingSource
            // 
            this.sp01055CoreGetReportLayoutsBindingSource.DataMember = "sp01055_Core_Get_Report_Layouts";
            this.sp01055CoreGetReportLayoutsBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // dataSet_Common_Functionality
            // 
            this.dataSet_Common_Functionality.DataSetName = "DataSet_Common_Functionality";
            this.dataSet_Common_Functionality.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colReportLayoutID,
            this.colReportLayoutName,
            this.colReportTypeName,
            this.colModuleID,
            this.colCreatedBy,
            this.colPublishedToWeb});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colReportLayoutID;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            // 
            // colReportLayoutID
            // 
            this.colReportLayoutID.Caption = "Report Layout ID";
            this.colReportLayoutID.FieldName = "ReportLayoutID";
            this.colReportLayoutID.Name = "colReportLayoutID";
            this.colReportLayoutID.OptionsColumn.AllowEdit = false;
            this.colReportLayoutID.OptionsColumn.AllowFocus = false;
            this.colReportLayoutID.OptionsColumn.ReadOnly = true;
            this.colReportLayoutID.Width = 102;
            // 
            // colReportLayoutName
            // 
            this.colReportLayoutName.Caption = "Layout Name";
            this.colReportLayoutName.FieldName = "ReportLayoutName";
            this.colReportLayoutName.Name = "colReportLayoutName";
            this.colReportLayoutName.OptionsColumn.AllowEdit = false;
            this.colReportLayoutName.OptionsColumn.AllowFocus = false;
            this.colReportLayoutName.OptionsColumn.ReadOnly = true;
            this.colReportLayoutName.Visible = true;
            this.colReportLayoutName.VisibleIndex = 0;
            this.colReportLayoutName.Width = 264;
            // 
            // colReportTypeName
            // 
            this.colReportTypeName.Caption = "Layout Type";
            this.colReportTypeName.FieldName = "ReportTypeName";
            this.colReportTypeName.Name = "colReportTypeName";
            this.colReportTypeName.OptionsColumn.AllowEdit = false;
            this.colReportTypeName.OptionsColumn.AllowFocus = false;
            this.colReportTypeName.OptionsColumn.ReadOnly = true;
            this.colReportTypeName.Width = 119;
            // 
            // colModuleID
            // 
            this.colModuleID.Caption = "Module ID";
            this.colModuleID.FieldName = "ModuleID";
            this.colModuleID.Name = "colModuleID";
            this.colModuleID.OptionsColumn.AllowEdit = false;
            this.colModuleID.OptionsColumn.AllowFocus = false;
            this.colModuleID.OptionsColumn.ReadOnly = true;
            this.colModuleID.Width = 67;
            // 
            // colCreatedBy
            // 
            this.colCreatedBy.Caption = "Created By";
            this.colCreatedBy.FieldName = "CreatedBy";
            this.colCreatedBy.Name = "colCreatedBy";
            this.colCreatedBy.OptionsColumn.AllowEdit = false;
            this.colCreatedBy.OptionsColumn.AllowFocus = false;
            this.colCreatedBy.OptionsColumn.ReadOnly = true;
            this.colCreatedBy.Visible = true;
            this.colCreatedBy.VisibleIndex = 1;
            this.colCreatedBy.Width = 125;
            // 
            // colPublishedToWeb
            // 
            this.colPublishedToWeb.Caption = "Publish To Web";
            this.colPublishedToWeb.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPublishedToWeb.FieldName = "PublishedToWeb";
            this.colPublishedToWeb.Name = "colPublishedToWeb";
            this.colPublishedToWeb.OptionsColumn.AllowEdit = false;
            this.colPublishedToWeb.OptionsColumn.AllowFocus = false;
            this.colPublishedToWeb.OptionsColumn.ReadOnly = true;
            this.colPublishedToWeb.Width = 92;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // CMTenderRequestNotesFileButtonEdit
            // 
            this.CMTenderRequestNotesFileButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "CMTenderRequestNotesFile", true));
            this.CMTenderRequestNotesFileButtonEdit.Location = new System.Drawing.Point(248, 358);
            this.CMTenderRequestNotesFileButtonEdit.MenuManager = this.barManager1;
            this.CMTenderRequestNotesFileButtonEdit.Name = "CMTenderRequestNotesFileButtonEdit";
            editorButtonImageOptions1.Location = DevExpress.XtraEditors.ImageLocation.Default;
            this.CMTenderRequestNotesFileButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "click me to Choose file", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View File", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to open the selected file", "view", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click me to Clear value", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.CMTenderRequestNotesFileButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.CMTenderRequestNotesFileButtonEdit.Size = new System.Drawing.Size(373, 20);
            this.CMTenderRequestNotesFileButtonEdit.StyleController = this.dataLayoutControl1;
            this.CMTenderRequestNotesFileButtonEdit.TabIndex = 14;
            this.CMTenderRequestNotesFileButtonEdit.TabStop = false;
            this.CMTenderRequestNotesFileButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.CMTenderRequestNotesFileButtonEdit_ButtonClick);
            // 
            // TenderProactiveDaysToReturnQuoteSpinEdit
            // 
            this.TenderProactiveDaysToReturnQuoteSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "TenderProactiveDaysToReturnQuote", true));
            this.TenderProactiveDaysToReturnQuoteSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TenderProactiveDaysToReturnQuoteSpinEdit.Location = new System.Drawing.Point(248, 334);
            this.TenderProactiveDaysToReturnQuoteSpinEdit.MenuManager = this.barManager1;
            this.TenderProactiveDaysToReturnQuoteSpinEdit.Name = "TenderProactiveDaysToReturnQuoteSpinEdit";
            this.TenderProactiveDaysToReturnQuoteSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TenderProactiveDaysToReturnQuoteSpinEdit.Properties.IsFloatValue = false;
            this.TenderProactiveDaysToReturnQuoteSpinEdit.Properties.Mask.EditMask = "N00";
            this.TenderProactiveDaysToReturnQuoteSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TenderProactiveDaysToReturnQuoteSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.TenderProactiveDaysToReturnQuoteSpinEdit.Size = new System.Drawing.Size(84, 20);
            this.TenderProactiveDaysToReturnQuoteSpinEdit.StyleController = this.dataLayoutControl1;
            this.TenderProactiveDaysToReturnQuoteSpinEdit.TabIndex = 51;
            // 
            // TenderReactiveDaysToReturnQuoteSpinEdit
            // 
            this.TenderReactiveDaysToReturnQuoteSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "TenderReactiveDaysToReturnQuote", true));
            this.TenderReactiveDaysToReturnQuoteSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TenderReactiveDaysToReturnQuoteSpinEdit.Location = new System.Drawing.Point(248, 310);
            this.TenderReactiveDaysToReturnQuoteSpinEdit.MenuManager = this.barManager1;
            this.TenderReactiveDaysToReturnQuoteSpinEdit.Name = "TenderReactiveDaysToReturnQuoteSpinEdit";
            this.TenderReactiveDaysToReturnQuoteSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TenderReactiveDaysToReturnQuoteSpinEdit.Properties.IsFloatValue = false;
            this.TenderReactiveDaysToReturnQuoteSpinEdit.Properties.Mask.EditMask = "N00";
            this.TenderReactiveDaysToReturnQuoteSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TenderReactiveDaysToReturnQuoteSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.TenderReactiveDaysToReturnQuoteSpinEdit.Size = new System.Drawing.Size(84, 20);
            this.TenderReactiveDaysToReturnQuoteSpinEdit.StyleController = this.dataLayoutControl1;
            this.TenderReactiveDaysToReturnQuoteSpinEdit.TabIndex = 50;
            // 
            // SignatureForClientBillingCheckEdit
            // 
            this.SignatureForClientBillingCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "SignatureForClientBilling", true));
            this.SignatureForClientBillingCheckEdit.Location = new System.Drawing.Point(201, 41);
            this.SignatureForClientBillingCheckEdit.MenuManager = this.barManager1;
            this.SignatureForClientBillingCheckEdit.Name = "SignatureForClientBillingCheckEdit";
            this.SignatureForClientBillingCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.SignatureForClientBillingCheckEdit.Properties.ValueChecked = 1;
            this.SignatureForClientBillingCheckEdit.Properties.ValueUnchecked = 0;
            this.SignatureForClientBillingCheckEdit.Size = new System.Drawing.Size(131, 19);
            this.SignatureForClientBillingCheckEdit.StyleController = this.dataLayoutControl1;
            this.SignatureForClientBillingCheckEdit.TabIndex = 49;
            // 
            // SignatureForTeamSelfBillingCheckEdit
            // 
            this.SignatureForTeamSelfBillingCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "SignatureForTeamSelfBilling", true));
            this.SignatureForTeamSelfBillingCheckEdit.Location = new System.Drawing.Point(201, 18);
            this.SignatureForTeamSelfBillingCheckEdit.MenuManager = this.barManager1;
            this.SignatureForTeamSelfBillingCheckEdit.Name = "SignatureForTeamSelfBillingCheckEdit";
            this.SignatureForTeamSelfBillingCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.SignatureForTeamSelfBillingCheckEdit.Properties.ValueChecked = 1;
            this.SignatureForTeamSelfBillingCheckEdit.Properties.ValueUnchecked = 0;
            this.SignatureForTeamSelfBillingCheckEdit.Size = new System.Drawing.Size(131, 19);
            this.SignatureForTeamSelfBillingCheckEdit.StyleController = this.dataLayoutControl1;
            this.SignatureForTeamSelfBillingCheckEdit.TabIndex = 13;
            // 
            // ClientInstructionsMemoEdit
            // 
            this.ClientInstructionsMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ClientInstructions", true));
            this.ClientInstructionsMemoEdit.Location = new System.Drawing.Point(201, 404);
            this.ClientInstructionsMemoEdit.MenuManager = this.barManager1;
            this.ClientInstructionsMemoEdit.Name = "ClientInstructionsMemoEdit";
            this.ClientInstructionsMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientInstructionsMemoEdit, true);
            this.ClientInstructionsMemoEdit.Size = new System.Drawing.Size(432, 145);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientInstructionsMemoEdit, optionsSpelling1);
            this.ClientInstructionsMemoEdit.StyleController = this.dataLayoutControl1;
            this.ClientInstructionsMemoEdit.TabIndex = 48;
            // 
            // ReactiveCheckEdit
            // 
            this.ReactiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "Reactive", true));
            this.ReactiveCheckEdit.Location = new System.Drawing.Point(201, -96);
            this.ReactiveCheckEdit.MenuManager = this.barManager1;
            this.ReactiveCheckEdit.Name = "ReactiveCheckEdit";
            this.ReactiveCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.ReactiveCheckEdit.Properties.ValueChecked = 1;
            this.ReactiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ReactiveCheckEdit.Size = new System.Drawing.Size(432, 19);
            this.ReactiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ReactiveCheckEdit.TabIndex = 7;
            // 
            // ContractDescriptionTextEdit
            // 
            this.ContractDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ContractDescription", true));
            this.ContractDescriptionTextEdit.Location = new System.Drawing.Point(177, -320);
            this.ContractDescriptionTextEdit.MenuManager = this.barManager1;
            this.ContractDescriptionTextEdit.Name = "ContractDescriptionTextEdit";
            this.ContractDescriptionTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContractDescriptionTextEdit, true);
            this.ContractDescriptionTextEdit.Size = new System.Drawing.Size(480, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContractDescriptionTextEdit, optionsSpelling2);
            this.ContractDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.ContractDescriptionTextEdit.TabIndex = 1;
            this.ContractDescriptionTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ContractDescriptionTextEdit_Validating);
            // 
            // CostCentreIDTextEdit
            // 
            this.CostCentreIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "CostCentreID", true));
            this.CostCentreIDTextEdit.Location = new System.Drawing.Point(153, 59);
            this.CostCentreIDTextEdit.MenuManager = this.barManager1;
            this.CostCentreIDTextEdit.Name = "CostCentreIDTextEdit";
            this.CostCentreIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CostCentreIDTextEdit, true);
            this.CostCentreIDTextEdit.Size = new System.Drawing.Size(504, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CostCentreIDTextEdit, optionsSpelling3);
            this.CostCentreIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CostCentreIDTextEdit.TabIndex = 42;
            // 
            // CostCentreCodeTextEdit
            // 
            this.CostCentreCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "CostCentreCode", true));
            this.CostCentreCodeTextEdit.Location = new System.Drawing.Point(153, 59);
            this.CostCentreCodeTextEdit.MenuManager = this.barManager1;
            this.CostCentreCodeTextEdit.Name = "CostCentreCodeTextEdit";
            this.CostCentreCodeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CostCentreCodeTextEdit, true);
            this.CostCentreCodeTextEdit.Size = new System.Drawing.Size(504, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CostCentreCodeTextEdit, optionsSpelling4);
            this.CostCentreCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.CostCentreCodeTextEdit.TabIndex = 43;
            // 
            // CostCentreTextEdit
            // 
            this.CostCentreTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "CostCentre", true));
            this.CostCentreTextEdit.Location = new System.Drawing.Point(153, 59);
            this.CostCentreTextEdit.MenuManager = this.barManager1;
            this.CostCentreTextEdit.Name = "CostCentreTextEdit";
            this.CostCentreTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CostCentreTextEdit, true);
            this.CostCentreTextEdit.Size = new System.Drawing.Size(504, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CostCentreTextEdit, optionsSpelling5);
            this.CostCentreTextEdit.StyleController = this.dataLayoutControl1;
            this.CostCentreTextEdit.TabIndex = 44;
            // 
            // LinkedToPreviousContractIDTextEdit
            // 
            this.LinkedToPreviousContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "LinkedToPreviousContractID", true));
            this.LinkedToPreviousContractIDTextEdit.Location = new System.Drawing.Point(191, 305);
            this.LinkedToPreviousContractIDTextEdit.MenuManager = this.barManager1;
            this.LinkedToPreviousContractIDTextEdit.Name = "LinkedToPreviousContractIDTextEdit";
            this.LinkedToPreviousContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToPreviousContractIDTextEdit, true);
            this.LinkedToPreviousContractIDTextEdit.Size = new System.Drawing.Size(442, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToPreviousContractIDTextEdit, optionsSpelling6);
            this.LinkedToPreviousContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToPreviousContractIDTextEdit.TabIndex = 47;
            // 
            // LinkedToPreviousContractButtonEdit
            // 
            this.LinkedToPreviousContractButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "LinkedToPreviousContract", true));
            this.LinkedToPreviousContractButtonEdit.Location = new System.Drawing.Point(201, 218);
            this.LinkedToPreviousContractButtonEdit.MenuManager = this.barManager1;
            this.LinkedToPreviousContractButtonEdit.Name = "LinkedToPreviousContractButtonEdit";
            this.LinkedToPreviousContractButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click to Open Select Contract screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LinkedToPreviousContractButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.LinkedToPreviousContractButtonEdit.Size = new System.Drawing.Size(432, 20);
            this.LinkedToPreviousContractButtonEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToPreviousContractButtonEdit.TabIndex = 17;
            this.LinkedToPreviousContractButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LinkedToPreviousContractButtonEdit_ButtonClick);
            // 
            // LastClientPaymentDateDateEdit
            // 
            this.LastClientPaymentDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "LastClientPaymentDate", true));
            this.LastClientPaymentDateDateEdit.EditValue = null;
            this.LastClientPaymentDateDateEdit.Location = new System.Drawing.Point(201, 194);
            this.LastClientPaymentDateDateEdit.MenuManager = this.barManager1;
            this.LastClientPaymentDateDateEdit.Name = "LastClientPaymentDateDateEdit";
            this.LastClientPaymentDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LastClientPaymentDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LastClientPaymentDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LastClientPaymentDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.LastClientPaymentDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.LastClientPaymentDateDateEdit.Size = new System.Drawing.Size(131, 20);
            this.LastClientPaymentDateDateEdit.StyleController = this.dataLayoutControl1;
            this.LastClientPaymentDateDateEdit.TabIndex = 16;
            // 
            // BillingCentreCodeIDTextEdit
            // 
            this.BillingCentreCodeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "BillingCentreCodeID", true));
            this.BillingCentreCodeIDTextEdit.Location = new System.Drawing.Point(147, 172);
            this.BillingCentreCodeIDTextEdit.MenuManager = this.barManager1;
            this.BillingCentreCodeIDTextEdit.Name = "BillingCentreCodeIDTextEdit";
            this.BillingCentreCodeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.BillingCentreCodeIDTextEdit, true);
            this.BillingCentreCodeIDTextEdit.Size = new System.Drawing.Size(486, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.BillingCentreCodeIDTextEdit, optionsSpelling7);
            this.BillingCentreCodeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.BillingCentreCodeIDTextEdit.TabIndex = 46;
            // 
            // BillingCentreCodeButtonEdit
            // 
            this.BillingCentreCodeButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "BillingCentreCode", true));
            this.BillingCentreCodeButtonEdit.Location = new System.Drawing.Point(201, 170);
            this.BillingCentreCodeButtonEdit.MenuManager = this.barManager1;
            this.BillingCentreCodeButtonEdit.Name = "BillingCentreCodeButtonEdit";
            this.BillingCentreCodeButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Click me to open Select Billing Code screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.BillingCentreCodeButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.BillingCentreCodeButtonEdit.Size = new System.Drawing.Size(432, 20);
            this.BillingCentreCodeButtonEdit.StyleController = this.dataLayoutControl1;
            this.BillingCentreCodeButtonEdit.TabIndex = 15;
            this.BillingCentreCodeButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BillingCentreCodeButtonEdit_ButtonClick);
            // 
            // FinanceClientCodeTextEdit
            // 
            this.FinanceClientCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "FinanceClientCode", true));
            this.FinanceClientCodeTextEdit.Location = new System.Drawing.Point(201, 146);
            this.FinanceClientCodeTextEdit.MenuManager = this.barManager1;
            this.FinanceClientCodeTextEdit.Name = "FinanceClientCodeTextEdit";
            this.FinanceClientCodeTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FinanceClientCodeTextEdit, true);
            this.FinanceClientCodeTextEdit.Size = new System.Drawing.Size(432, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FinanceClientCodeTextEdit, optionsSpelling8);
            this.FinanceClientCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.FinanceClientCodeTextEdit.TabIndex = 14;
            // 
            // CheckRPIDateDateEdit
            // 
            this.CheckRPIDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "CheckRPIDate", true));
            this.CheckRPIDateDateEdit.EditValue = null;
            this.CheckRPIDateDateEdit.Location = new System.Drawing.Point(201, 112);
            this.CheckRPIDateDateEdit.MenuManager = this.barManager1;
            this.CheckRPIDateDateEdit.Name = "CheckRPIDateDateEdit";
            this.CheckRPIDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.CheckRPIDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CheckRPIDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CheckRPIDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.CheckRPIDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.CheckRPIDateDateEdit.Size = new System.Drawing.Size(131, 20);
            this.CheckRPIDateDateEdit.StyleController = this.dataLayoutControl1;
            this.CheckRPIDateDateEdit.TabIndex = 13;
            // 
            // YearlyPercentageIncreaseSpinEdit
            // 
            this.YearlyPercentageIncreaseSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "YearlyPercentageIncrease", true));
            this.YearlyPercentageIncreaseSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.YearlyPercentageIncreaseSpinEdit.Location = new System.Drawing.Point(201, 88);
            this.YearlyPercentageIncreaseSpinEdit.MenuManager = this.barManager1;
            this.YearlyPercentageIncreaseSpinEdit.Name = "YearlyPercentageIncreaseSpinEdit";
            this.YearlyPercentageIncreaseSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.YearlyPercentageIncreaseSpinEdit.Properties.Mask.EditMask = "f2";
            this.YearlyPercentageIncreaseSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.YearlyPercentageIncreaseSpinEdit.Size = new System.Drawing.Size(131, 20);
            this.YearlyPercentageIncreaseSpinEdit.StyleController = this.dataLayoutControl1;
            this.YearlyPercentageIncreaseSpinEdit.TabIndex = 12;
            // 
            // ContractValueSpinEdit
            // 
            this.ContractValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ContractValue", true));
            this.ContractValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ContractValueSpinEdit.Location = new System.Drawing.Point(201, 64);
            this.ContractValueSpinEdit.MenuManager = this.barManager1;
            this.ContractValueSpinEdit.Name = "ContractValueSpinEdit";
            this.ContractValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ContractValueSpinEdit.Properties.Mask.EditMask = "c";
            this.ContractValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ContractValueSpinEdit.Size = new System.Drawing.Size(131, 20);
            this.ContractValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.ContractValueSpinEdit.TabIndex = 11;
            // 
            // ActiveCheckEdit
            // 
            this.ActiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "Active", true));
            this.ActiveCheckEdit.Location = new System.Drawing.Point(201, -15);
            this.ActiveCheckEdit.MenuManager = this.barManager1;
            this.ActiveCheckEdit.Name = "ActiveCheckEdit";
            this.ActiveCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.ActiveCheckEdit.Properties.ValueChecked = 1;
            this.ActiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ActiveCheckEdit.Size = new System.Drawing.Size(131, 19);
            this.ActiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ActiveCheckEdit.TabIndex = 10;
            // 
            // EndDateDateEdit
            // 
            this.EndDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "EndDate", true));
            this.EndDateDateEdit.EditValue = null;
            this.EndDateDateEdit.Location = new System.Drawing.Point(201, -39);
            this.EndDateDateEdit.MenuManager = this.barManager1;
            this.EndDateDateEdit.Name = "EndDateDateEdit";
            this.EndDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.EndDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.EndDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.EndDateDateEdit.Size = new System.Drawing.Size(131, 20);
            this.EndDateDateEdit.StyleController = this.dataLayoutControl1;
            this.EndDateDateEdit.TabIndex = 9;
            // 
            // StartDateDateEdit
            // 
            this.StartDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "StartDate", true));
            this.StartDateDateEdit.EditValue = null;
            this.StartDateDateEdit.Location = new System.Drawing.Point(201, -63);
            this.StartDateDateEdit.MenuManager = this.barManager1;
            this.StartDateDateEdit.Name = "StartDateDateEdit";
            this.StartDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.StartDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.StartDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.StartDateDateEdit.Size = new System.Drawing.Size(131, 20);
            this.StartDateDateEdit.StyleController = this.dataLayoutControl1;
            this.StartDateDateEdit.TabIndex = 8;
            this.StartDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.StartDateDateEdit_Validating);
            // 
            // ContractTypeIDGridLookUpEdit
            // 
            this.ContractTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ContractTypeID", true));
            this.ContractTypeIDGridLookUpEdit.Location = new System.Drawing.Point(201, -120);
            this.ContractTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ContractTypeIDGridLookUpEdit.Name = "ContractTypeIDGridLookUpEdit";
            this.ContractTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ContractTypeIDGridLookUpEdit.Properties.DataSource = this.sp06063OMContractTypesWithBlankBindingSource;
            this.ContractTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ContractTypeIDGridLookUpEdit.Properties.NullText = "";
            this.ContractTypeIDGridLookUpEdit.Properties.PopupView = this.gridView2;
            this.ContractTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ContractTypeIDGridLookUpEdit.Size = new System.Drawing.Size(432, 20);
            this.ContractTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ContractTypeIDGridLookUpEdit.TabIndex = 6;
            this.ContractTypeIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ContractTypeIDGridLookUpEdit_Validating);
            // 
            // sp06063OMContractTypesWithBlankBindingSource
            // 
            this.sp06063OMContractTypesWithBlankBindingSource.DataMember = "sp06063_OM_Contract_Types_With_Blank";
            this.sp06063OMContractTypesWithBlankBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn7;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn9, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Contract Type";
            this.gridColumn8.FieldName = "Description";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 220;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Order";
            this.gridColumn9.FieldName = "RecordOrder";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            // 
            // ContractDirectorIDTextEdit
            // 
            this.ContractDirectorIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ContractDirectorID", true));
            this.ContractDirectorIDTextEdit.Location = new System.Drawing.Point(140, 234);
            this.ContractDirectorIDTextEdit.MenuManager = this.barManager1;
            this.ContractDirectorIDTextEdit.Name = "ContractDirectorIDTextEdit";
            this.ContractDirectorIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContractDirectorIDTextEdit, true);
            this.ContractDirectorIDTextEdit.Size = new System.Drawing.Size(510, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContractDirectorIDTextEdit, optionsSpelling9);
            this.ContractDirectorIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ContractDirectorIDTextEdit.TabIndex = 13;
            // 
            // ContractDirectorButtonEdit
            // 
            this.ContractDirectorButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ContractDirector", true));
            this.ContractDirectorButtonEdit.Location = new System.Drawing.Point(201, -144);
            this.ContractDirectorButtonEdit.MenuManager = this.barManager1;
            this.ContractDirectorButtonEdit.Name = "ContractDirectorButtonEdit";
            this.ContractDirectorButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Click to open Select Staff screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ContractDirectorButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ContractDirectorButtonEdit.Size = new System.Drawing.Size(432, 20);
            this.ContractDirectorButtonEdit.StyleController = this.dataLayoutControl1;
            this.ContractDirectorButtonEdit.TabIndex = 5;
            this.ContractDirectorButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ContractDirectorButtonEdit_ButtonClick);
            this.ContractDirectorButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ContractDirectorButtonEdit_Validating);
            // 
            // ContractStatusIDGridLookUpEdit
            // 
            this.ContractStatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ContractStatusID", true));
            this.ContractStatusIDGridLookUpEdit.Location = new System.Drawing.Point(201, -168);
            this.ContractStatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ContractStatusIDGridLookUpEdit.Name = "ContractStatusIDGridLookUpEdit";
            this.ContractStatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ContractStatusIDGridLookUpEdit.Properties.DataSource = this.sp06062OMContractStatusesWithBlankBindingSource;
            this.ContractStatusIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ContractStatusIDGridLookUpEdit.Properties.NullText = "";
            this.ContractStatusIDGridLookUpEdit.Properties.PopupView = this.gridView3;
            this.ContractStatusIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ContractStatusIDGridLookUpEdit.Size = new System.Drawing.Size(432, 20);
            this.ContractStatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ContractStatusIDGridLookUpEdit.TabIndex = 4;
            this.ContractStatusIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ContractStatusIDGridLookUpEdit_Validating);
            // 
            // sp06062OMContractStatusesWithBlankBindingSource
            // 
            this.sp06062OMContractStatusesWithBlankBindingSource.DataMember = "sp06062_OM_Contract_Statuses_With_Blank";
            this.sp06062OMContractStatusesWithBlankBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn4;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Contract Status";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "RecordOrder";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            // 
            // SectorTypeIDGridLookUpEdit
            // 
            this.SectorTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "SectorTypeID", true));
            this.SectorTypeIDGridLookUpEdit.Location = new System.Drawing.Point(201, -192);
            this.SectorTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SectorTypeIDGridLookUpEdit.Name = "SectorTypeIDGridLookUpEdit";
            this.SectorTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SectorTypeIDGridLookUpEdit.Properties.DataSource = this.sp06061OMSectorsWithBlankBindingSource;
            this.SectorTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.SectorTypeIDGridLookUpEdit.Properties.NullText = "";
            this.SectorTypeIDGridLookUpEdit.Properties.PopupView = this.gridView4;
            this.SectorTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.SectorTypeIDGridLookUpEdit.Size = new System.Drawing.Size(432, 20);
            this.SectorTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SectorTypeIDGridLookUpEdit.TabIndex = 3;
            this.SectorTypeIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SectorTypeIDGridLookUpEdit_Validating);
            // 
            // sp06061OMSectorsWithBlankBindingSource
            // 
            this.sp06061OMSectorsWithBlankBindingSource.DataMember = "sp06061_OM_Sectors_With_Blank";
            this.sp06061OMSectorsWithBlankBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.gridColumn1;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Sector";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // GCCompanyIDGridLookUpEdit
            // 
            this.GCCompanyIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "GCCompanyID", true));
            this.GCCompanyIDGridLookUpEdit.Location = new System.Drawing.Point(201, -216);
            this.GCCompanyIDGridLookUpEdit.MenuManager = this.barManager1;
            this.GCCompanyIDGridLookUpEdit.Name = "GCCompanyIDGridLookUpEdit";
            this.GCCompanyIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GCCompanyIDGridLookUpEdit.Properties.DataSource = this.sp06060OMGCCompaniesWithBlankBindingSource;
            this.GCCompanyIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.GCCompanyIDGridLookUpEdit.Properties.NullText = "";
            this.GCCompanyIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.GCCompanyIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.GCCompanyIDGridLookUpEdit.Size = new System.Drawing.Size(432, 20);
            this.GCCompanyIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.GCCompanyIDGridLookUpEdit.TabIndex = 2;
            this.GCCompanyIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.GCCompanyIDGridLookUpEdit_Validating);
            // 
            // sp06060OMGCCompaniesWithBlankBindingSource
            // 
            this.sp06060OMGCCompaniesWithBlankBindingSource.DataMember = "sp06060_OM_GC_Companies_With_Blank";
            this.sp06060OMGCCompaniesWithBlankBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.colID1;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "GC Company";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "RecordOrder";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // ClientIDTextEdit
            // 
            this.ClientIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ClientID", true));
            this.ClientIDTextEdit.Location = new System.Drawing.Point(114, 59);
            this.ClientIDTextEdit.MenuManager = this.barManager1;
            this.ClientIDTextEdit.Name = "ClientIDTextEdit";
            this.ClientIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientIDTextEdit, true);
            this.ClientIDTextEdit.Size = new System.Drawing.Size(543, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientIDTextEdit, optionsSpelling10);
            this.ClientIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDTextEdit.TabIndex = 44;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, -216);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(597, 775);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling11);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 35;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp06059OMClientContractEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(177, -367);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(149, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // ClientNameButtonEdit
            // 
            this.ClientNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ClientName", true));
            this.ClientNameButtonEdit.EditValue = "";
            this.ClientNameButtonEdit.Location = new System.Drawing.Point(177, -344);
            this.ClientNameButtonEdit.MenuManager = this.barManager1;
            this.ClientNameButtonEdit.Name = "ClientNameButtonEdit";
            this.ClientNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Click me to open the Select Client screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ClientNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientNameButtonEdit.Size = new System.Drawing.Size(480, 20);
            this.ClientNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameButtonEdit.TabIndex = 0;
            this.ClientNameButtonEdit.TabStop = false;
            this.ClientNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientNameButtonEdit_ButtonClick);
            this.ClientNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ClientNameButtonEdit_Validating);
            // 
            // ClientContractIDTextEdit
            // 
            this.ClientContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ClientContractID", true));
            this.ClientContractIDTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ClientContractIDTextEdit.Location = new System.Drawing.Point(116, 59);
            this.ClientContractIDTextEdit.MenuManager = this.barManager1;
            this.ClientContractIDTextEdit.Name = "ClientContractIDTextEdit";
            this.ClientContractIDTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.ClientContractIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.ClientContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientContractIDTextEdit, true);
            this.ClientContractIDTextEdit.Size = new System.Drawing.Size(541, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientContractIDTextEdit, optionsSpelling12);
            this.ClientContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientContractIDTextEdit.TabIndex = 27;
            // 
            // ItemForClientContractID
            // 
            this.ItemForClientContractID.Control = this.ClientContractIDTextEdit;
            this.ItemForClientContractID.CustomizationFormText = "Client Contract ID:";
            this.ItemForClientContractID.Location = new System.Drawing.Point(0, 47);
            this.ItemForClientContractID.Name = "ItemForClientContractID";
            this.ItemForClientContractID.Size = new System.Drawing.Size(649, 24);
            this.ItemForClientContractID.Text = "Client Contract ID:";
            this.ItemForClientContractID.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientIDTextEdit;
            this.ItemForClientID.CustomizationFormText = "Client ID:";
            this.ItemForClientID.Location = new System.Drawing.Point(0, 47);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(649, 24);
            this.ItemForClientID.Text = "Client ID:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForContractDirectorID
            // 
            this.ItemForContractDirectorID.Control = this.ContractDirectorIDTextEdit;
            this.ItemForContractDirectorID.CustomizationFormText = "Contract Director ID:";
            this.ItemForContractDirectorID.Location = new System.Drawing.Point(0, 96);
            this.ItemForContractDirectorID.Name = "ItemForContractDirectorID";
            this.ItemForContractDirectorID.Size = new System.Drawing.Size(618, 24);
            this.ItemForContractDirectorID.Text = "Contract Director ID:";
            this.ItemForContractDirectorID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForBillingCentreCodeID
            // 
            this.ItemForBillingCentreCodeID.Control = this.BillingCentreCodeIDTextEdit;
            this.ItemForBillingCentreCodeID.CustomizationFormText = "Billing Centre Code ID:";
            this.ItemForBillingCentreCodeID.Location = new System.Drawing.Point(0, 120);
            this.ItemForBillingCentreCodeID.Name = "ItemForBillingCentreCodeID";
            this.ItemForBillingCentreCodeID.Size = new System.Drawing.Size(601, 24);
            this.ItemForBillingCentreCodeID.Text = "Billing Centre Code ID:";
            this.ItemForBillingCentreCodeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLinkedToPreviousContractID
            // 
            this.ItemForLinkedToPreviousContractID.Control = this.LinkedToPreviousContractIDTextEdit;
            this.ItemForLinkedToPreviousContractID.CustomizationFormText = "Linked To Previous Contract ID:";
            this.ItemForLinkedToPreviousContractID.Location = new System.Drawing.Point(0, 355);
            this.ItemForLinkedToPreviousContractID.Name = "ItemForLinkedToPreviousContractID";
            this.ItemForLinkedToPreviousContractID.Size = new System.Drawing.Size(601, 24);
            this.ItemForLinkedToPreviousContractID.Text = "Linked To Previous Contract ID:";
            this.ItemForLinkedToPreviousContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCostCentreCode
            // 
            this.ItemForCostCentreCode.Control = this.CostCentreCodeTextEdit;
            this.ItemForCostCentreCode.CustomizationFormText = "Cost Centre Code:";
            this.ItemForCostCentreCode.Location = new System.Drawing.Point(0, 47);
            this.ItemForCostCentreCode.Name = "ItemForCostCentreCode";
            this.ItemForCostCentreCode.Size = new System.Drawing.Size(649, 24);
            this.ItemForCostCentreCode.Text = "Cost Centre Code:";
            this.ItemForCostCentreCode.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCostCentre
            // 
            this.ItemForCostCentre.Control = this.CostCentreTextEdit;
            this.ItemForCostCentre.CustomizationFormText = "Cost Centre:";
            this.ItemForCostCentre.Location = new System.Drawing.Point(0, 47);
            this.ItemForCostCentre.Name = "ItemForCostCentre";
            this.ItemForCostCentre.Size = new System.Drawing.Size(649, 24);
            this.ItemForCostCentre.Text = "Cost Centre:";
            this.ItemForCostCentre.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCostCentreID
            // 
            this.ItemForCostCentreID.Control = this.CostCentreIDTextEdit;
            this.ItemForCostCentreID.CustomizationFormText = "Cost Centre ID:";
            this.ItemForCostCentreID.Location = new System.Drawing.Point(0, 47);
            this.ItemForCostCentreID.Name = "ItemForCostCentreID";
            this.ItemForCostCentreID.Size = new System.Drawing.Size(649, 24);
            this.ItemForCostCentreID.Text = "Cost Centre ID:";
            this.ItemForCostCentreID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(669, 984);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientName,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem4,
            this.layoutControlGroup6,
            this.ItemForContractDescription});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(649, 954);
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.AllowHide = false;
            this.ItemForClientName.Control = this.ClientNameButtonEdit;
            this.ItemForClientName.CustomizationFormText = "Client Name:";
            this.ItemForClientName.Location = new System.Drawing.Point(0, 23);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(649, 24);
            this.ItemForClientName.Text = "Client Name:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(162, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(165, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(165, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(165, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(318, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(331, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(165, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(153, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 71);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(649, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 81);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(649, 873);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layGrpAddress;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(625, 827);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layGrpAddress,
            this.layoutControlGroup4});
            // 
            // layGrpAddress
            // 
            this.layGrpAddress.CustomizationFormText = "Address";
            this.layGrpAddress.ExpandButtonVisible = true;
            this.layGrpAddress.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layGrpAddress.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForGCCompanyID,
            this.ItemForSectorTypeID,
            this.ItemForContractStatusID,
            this.ItemForContractDirector,
            this.ItemForContractTypeID,
            this.emptySpaceItem6,
            this.ItemForStartDate,
            this.emptySpaceItem7,
            this.ItemForEndDate,
            this.emptySpaceItem8,
            this.ItemForActive,
            this.emptySpaceItem9,
            this.ItemForContractValue,
            this.ItemForYearlyPercentageIncrease,
            this.ItemForCheckRPIDate,
            this.emptySpaceItem10,
            this.ItemForFinanceClientCode,
            this.emptySpaceItem5,
            this.ItemForBillingCentreCode,
            this.ItemForLastClientPaymentDate,
            this.emptySpaceItem11,
            this.ItemForLinkedToPreviousContract,
            this.emptySpaceItem12,
            this.ItemForReactive,
            this.ItemForClientInstructions,
            this.emptySpaceItem13,
            this.ItemForSignatureForTeamSelfBilling,
            this.ItemforSignatureForClientBilling,
            this.emptySpaceItem14,
            this.emptySpaceItem15,
            this.emptySpaceItem16,
            this.layoutControlGroup5,
            this.emptySpaceItem17,
            this.emptySpaceItem19,
            this.emptySpaceItem20,
            this.ItemForDefaultCompletionSheetTemplateID});
            this.layGrpAddress.Location = new System.Drawing.Point(0, 0);
            this.layGrpAddress.Name = "layGrpAddress";
            this.layGrpAddress.Size = new System.Drawing.Size(601, 779);
            this.layGrpAddress.Text = "Details";
            // 
            // ItemForGCCompanyID
            // 
            this.ItemForGCCompanyID.Control = this.GCCompanyIDGridLookUpEdit;
            this.ItemForGCCompanyID.CustomizationFormText = "GC Company:";
            this.ItemForGCCompanyID.Location = new System.Drawing.Point(0, 0);
            this.ItemForGCCompanyID.Name = "ItemForGCCompanyID";
            this.ItemForGCCompanyID.Size = new System.Drawing.Size(601, 24);
            this.ItemForGCCompanyID.Text = "GC Company:";
            this.ItemForGCCompanyID.TextSize = new System.Drawing.Size(162, 13);
            // 
            // ItemForSectorTypeID
            // 
            this.ItemForSectorTypeID.Control = this.SectorTypeIDGridLookUpEdit;
            this.ItemForSectorTypeID.CustomizationFormText = "Sector Type:";
            this.ItemForSectorTypeID.Location = new System.Drawing.Point(0, 24);
            this.ItemForSectorTypeID.Name = "ItemForSectorTypeID";
            this.ItemForSectorTypeID.Size = new System.Drawing.Size(601, 24);
            this.ItemForSectorTypeID.Text = "Sector Type:";
            this.ItemForSectorTypeID.TextSize = new System.Drawing.Size(162, 13);
            // 
            // ItemForContractStatusID
            // 
            this.ItemForContractStatusID.Control = this.ContractStatusIDGridLookUpEdit;
            this.ItemForContractStatusID.CustomizationFormText = "Contract Status:";
            this.ItemForContractStatusID.Location = new System.Drawing.Point(0, 48);
            this.ItemForContractStatusID.Name = "ItemForContractStatusID";
            this.ItemForContractStatusID.Size = new System.Drawing.Size(601, 24);
            this.ItemForContractStatusID.Text = "Contract Status:";
            this.ItemForContractStatusID.TextSize = new System.Drawing.Size(162, 13);
            // 
            // ItemForContractDirector
            // 
            this.ItemForContractDirector.Control = this.ContractDirectorButtonEdit;
            this.ItemForContractDirector.CustomizationFormText = "Contract Director:";
            this.ItemForContractDirector.Location = new System.Drawing.Point(0, 72);
            this.ItemForContractDirector.Name = "ItemForContractDirector";
            this.ItemForContractDirector.Size = new System.Drawing.Size(601, 24);
            this.ItemForContractDirector.Text = "Contract Director:";
            this.ItemForContractDirector.TextSize = new System.Drawing.Size(162, 13);
            // 
            // ItemForContractTypeID
            // 
            this.ItemForContractTypeID.Control = this.ContractTypeIDGridLookUpEdit;
            this.ItemForContractTypeID.CustomizationFormText = "Contract Type:";
            this.ItemForContractTypeID.Location = new System.Drawing.Point(0, 96);
            this.ItemForContractTypeID.Name = "ItemForContractTypeID";
            this.ItemForContractTypeID.Size = new System.Drawing.Size(601, 24);
            this.ItemForContractTypeID.Text = "Contract Type:";
            this.ItemForContractTypeID.TextSize = new System.Drawing.Size(162, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 143);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(601, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForStartDate
            // 
            this.ItemForStartDate.Control = this.StartDateDateEdit;
            this.ItemForStartDate.CustomizationFormText = "Start Date:";
            this.ItemForStartDate.Location = new System.Drawing.Point(0, 153);
            this.ItemForStartDate.MaxSize = new System.Drawing.Size(300, 24);
            this.ItemForStartDate.MinSize = new System.Drawing.Size(300, 24);
            this.ItemForStartDate.Name = "ItemForStartDate";
            this.ItemForStartDate.Size = new System.Drawing.Size(300, 24);
            this.ItemForStartDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForStartDate.Text = "Start Date:";
            this.ItemForStartDate.TextSize = new System.Drawing.Size(162, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(300, 153);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(301, 24);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForEndDate
            // 
            this.ItemForEndDate.Control = this.EndDateDateEdit;
            this.ItemForEndDate.CustomizationFormText = "End Date:";
            this.ItemForEndDate.Location = new System.Drawing.Point(0, 177);
            this.ItemForEndDate.MaxSize = new System.Drawing.Size(300, 24);
            this.ItemForEndDate.MinSize = new System.Drawing.Size(300, 24);
            this.ItemForEndDate.Name = "ItemForEndDate";
            this.ItemForEndDate.Size = new System.Drawing.Size(300, 24);
            this.ItemForEndDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForEndDate.Text = "End Date:";
            this.ItemForEndDate.TextSize = new System.Drawing.Size(162, 13);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(300, 177);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(301, 24);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForActive
            // 
            this.ItemForActive.Control = this.ActiveCheckEdit;
            this.ItemForActive.CustomizationFormText = "Active:";
            this.ItemForActive.Location = new System.Drawing.Point(0, 201);
            this.ItemForActive.Name = "ItemForActive";
            this.ItemForActive.Size = new System.Drawing.Size(300, 23);
            this.ItemForActive.Text = "Active:";
            this.ItemForActive.TextSize = new System.Drawing.Size(162, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 224);
            this.emptySpaceItem9.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem9.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(601, 10);
            this.emptySpaceItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForContractValue
            // 
            this.ItemForContractValue.Control = this.ContractValueSpinEdit;
            this.ItemForContractValue.CustomizationFormText = "Contract Value:";
            this.ItemForContractValue.Location = new System.Drawing.Point(0, 280);
            this.ItemForContractValue.MaxSize = new System.Drawing.Size(300, 24);
            this.ItemForContractValue.MinSize = new System.Drawing.Size(300, 24);
            this.ItemForContractValue.Name = "ItemForContractValue";
            this.ItemForContractValue.Size = new System.Drawing.Size(300, 24);
            this.ItemForContractValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForContractValue.Text = "Contract Value:";
            this.ItemForContractValue.TextSize = new System.Drawing.Size(162, 13);
            // 
            // ItemForYearlyPercentageIncrease
            // 
            this.ItemForYearlyPercentageIncrease.Control = this.YearlyPercentageIncreaseSpinEdit;
            this.ItemForYearlyPercentageIncrease.CustomizationFormText = "Yearly % Increase:";
            this.ItemForYearlyPercentageIncrease.Location = new System.Drawing.Point(0, 304);
            this.ItemForYearlyPercentageIncrease.MaxSize = new System.Drawing.Size(300, 24);
            this.ItemForYearlyPercentageIncrease.MinSize = new System.Drawing.Size(300, 24);
            this.ItemForYearlyPercentageIncrease.Name = "ItemForYearlyPercentageIncrease";
            this.ItemForYearlyPercentageIncrease.Size = new System.Drawing.Size(300, 24);
            this.ItemForYearlyPercentageIncrease.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForYearlyPercentageIncrease.Text = "Yearly % Increase:";
            this.ItemForYearlyPercentageIncrease.TextSize = new System.Drawing.Size(162, 13);
            // 
            // ItemForCheckRPIDate
            // 
            this.ItemForCheckRPIDate.Control = this.CheckRPIDateDateEdit;
            this.ItemForCheckRPIDate.CustomizationFormText = "Check RPI Date:";
            this.ItemForCheckRPIDate.Location = new System.Drawing.Point(0, 328);
            this.ItemForCheckRPIDate.MaxSize = new System.Drawing.Size(300, 24);
            this.ItemForCheckRPIDate.MinSize = new System.Drawing.Size(300, 24);
            this.ItemForCheckRPIDate.Name = "ItemForCheckRPIDate";
            this.ItemForCheckRPIDate.Size = new System.Drawing.Size(300, 24);
            this.ItemForCheckRPIDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCheckRPIDate.Text = "Check RPI Date:";
            this.ItemForCheckRPIDate.TextSize = new System.Drawing.Size(162, 13);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(300, 328);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(301, 24);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForFinanceClientCode
            // 
            this.ItemForFinanceClientCode.Control = this.FinanceClientCodeTextEdit;
            this.ItemForFinanceClientCode.CustomizationFormText = "Finance Client Code:";
            this.ItemForFinanceClientCode.Location = new System.Drawing.Point(0, 362);
            this.ItemForFinanceClientCode.Name = "ItemForFinanceClientCode";
            this.ItemForFinanceClientCode.Size = new System.Drawing.Size(601, 24);
            this.ItemForFinanceClientCode.Text = "Finance Client Code:";
            this.ItemForFinanceClientCode.TextSize = new System.Drawing.Size(162, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 769);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(601, 10);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForBillingCentreCode
            // 
            this.ItemForBillingCentreCode.Control = this.BillingCentreCodeButtonEdit;
            this.ItemForBillingCentreCode.CustomizationFormText = "Billing Centre Code:";
            this.ItemForBillingCentreCode.Location = new System.Drawing.Point(0, 386);
            this.ItemForBillingCentreCode.Name = "ItemForBillingCentreCode";
            this.ItemForBillingCentreCode.Size = new System.Drawing.Size(601, 24);
            this.ItemForBillingCentreCode.Text = "Billing Centre Code:";
            this.ItemForBillingCentreCode.TextSize = new System.Drawing.Size(162, 13);
            // 
            // ItemForLastClientPaymentDate
            // 
            this.ItemForLastClientPaymentDate.Control = this.LastClientPaymentDateDateEdit;
            this.ItemForLastClientPaymentDate.CustomizationFormText = "Last Client Payment Date:";
            this.ItemForLastClientPaymentDate.Location = new System.Drawing.Point(0, 410);
            this.ItemForLastClientPaymentDate.MaxSize = new System.Drawing.Size(300, 24);
            this.ItemForLastClientPaymentDate.MinSize = new System.Drawing.Size(300, 24);
            this.ItemForLastClientPaymentDate.Name = "ItemForLastClientPaymentDate";
            this.ItemForLastClientPaymentDate.Size = new System.Drawing.Size(300, 24);
            this.ItemForLastClientPaymentDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLastClientPaymentDate.Text = "Last Client Payment Date:";
            this.ItemForLastClientPaymentDate.TextSize = new System.Drawing.Size(162, 13);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(300, 410);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(301, 24);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForLinkedToPreviousContract
            // 
            this.ItemForLinkedToPreviousContract.Control = this.LinkedToPreviousContractButtonEdit;
            this.ItemForLinkedToPreviousContract.CustomizationFormText = "Linked To Previous Contract:";
            this.ItemForLinkedToPreviousContract.Location = new System.Drawing.Point(0, 434);
            this.ItemForLinkedToPreviousContract.Name = "ItemForLinkedToPreviousContract";
            this.ItemForLinkedToPreviousContract.Size = new System.Drawing.Size(601, 24);
            this.ItemForLinkedToPreviousContract.Text = "Linked To Previous Contract:";
            this.ItemForLinkedToPreviousContract.TextSize = new System.Drawing.Size(162, 13);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 352);
            this.emptySpaceItem12.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem12.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(601, 10);
            this.emptySpaceItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForReactive
            // 
            this.ItemForReactive.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.ItemForReactive.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForReactive.Control = this.ReactiveCheckEdit;
            this.ItemForReactive.CustomizationFormText = "Reactive:";
            this.ItemForReactive.Location = new System.Drawing.Point(0, 120);
            this.ItemForReactive.MaxSize = new System.Drawing.Size(601, 23);
            this.ItemForReactive.MinSize = new System.Drawing.Size(601, 23);
            this.ItemForReactive.Name = "ItemForReactive";
            this.ItemForReactive.Size = new System.Drawing.Size(601, 23);
            this.ItemForReactive.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForReactive.Text = "Reactive:";
            this.ItemForReactive.TextSize = new System.Drawing.Size(162, 13);
            // 
            // ItemForClientInstructions
            // 
            this.ItemForClientInstructions.Control = this.ClientInstructionsMemoEdit;
            this.ItemForClientInstructions.CustomizationFormText = "Client Instructions:";
            this.ItemForClientInstructions.Location = new System.Drawing.Point(0, 620);
            this.ItemForClientInstructions.MaxSize = new System.Drawing.Size(0, 149);
            this.ItemForClientInstructions.MinSize = new System.Drawing.Size(155, 149);
            this.ItemForClientInstructions.Name = "ItemForClientInstructions";
            this.ItemForClientInstructions.Size = new System.Drawing.Size(601, 149);
            this.ItemForClientInstructions.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClientInstructions.Text = "Client Instructions:";
            this.ItemForClientInstructions.TextSize = new System.Drawing.Size(162, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(0, 482);
            this.emptySpaceItem13.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem13.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(601, 10);
            this.emptySpaceItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForSignatureForTeamSelfBilling
            // 
            this.ItemForSignatureForTeamSelfBilling.Control = this.SignatureForTeamSelfBillingCheckEdit;
            this.ItemForSignatureForTeamSelfBilling.Location = new System.Drawing.Point(0, 234);
            this.ItemForSignatureForTeamSelfBilling.MaxSize = new System.Drawing.Size(300, 23);
            this.ItemForSignatureForTeamSelfBilling.MinSize = new System.Drawing.Size(300, 23);
            this.ItemForSignatureForTeamSelfBilling.Name = "ItemForSignatureForTeamSelfBilling";
            this.ItemForSignatureForTeamSelfBilling.Size = new System.Drawing.Size(300, 23);
            this.ItemForSignatureForTeamSelfBilling.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSignatureForTeamSelfBilling.Text = "Signature For Team Self-Billing:";
            this.ItemForSignatureForTeamSelfBilling.TextSize = new System.Drawing.Size(162, 13);
            // 
            // ItemforSignatureForClientBilling
            // 
            this.ItemforSignatureForClientBilling.Control = this.SignatureForClientBillingCheckEdit;
            this.ItemforSignatureForClientBilling.Location = new System.Drawing.Point(0, 257);
            this.ItemforSignatureForClientBilling.MaxSize = new System.Drawing.Size(300, 23);
            this.ItemforSignatureForClientBilling.MinSize = new System.Drawing.Size(300, 23);
            this.ItemforSignatureForClientBilling.Name = "ItemforSignatureForClientBilling";
            this.ItemforSignatureForClientBilling.Size = new System.Drawing.Size(300, 23);
            this.ItemforSignatureForClientBilling.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemforSignatureForClientBilling.Text = "Signature For Client Billing:";
            this.ItemforSignatureForClientBilling.TextSize = new System.Drawing.Size(162, 13);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(300, 201);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(301, 23);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(300, 234);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(301, 23);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(300, 257);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(301, 23);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTenderReactiveDaysToReturnQuote,
            this.ItemForCMTenderRequestNotesFile,
            this.emptySpaceItem18,
            this.ItemForTenderProactiveDaysToReturnQuote});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 492);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutControlGroup5.Size = new System.Drawing.Size(601, 118);
            this.layoutControlGroup5.Text = "Extra Works Contracts";
            // 
            // ItemForTenderReactiveDaysToReturnQuote
            // 
            this.ItemForTenderReactiveDaysToReturnQuote.Control = this.TenderReactiveDaysToReturnQuoteSpinEdit;
            this.ItemForTenderReactiveDaysToReturnQuote.Location = new System.Drawing.Point(0, 0);
            this.ItemForTenderReactiveDaysToReturnQuote.MaxSize = new System.Drawing.Size(288, 24);
            this.ItemForTenderReactiveDaysToReturnQuote.MinSize = new System.Drawing.Size(288, 24);
            this.ItemForTenderReactiveDaysToReturnQuote.Name = "ItemForTenderReactiveDaysToReturnQuote";
            this.ItemForTenderReactiveDaysToReturnQuote.Size = new System.Drawing.Size(288, 24);
            this.ItemForTenderReactiveDaysToReturnQuote.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTenderReactiveDaysToReturnQuote.Text = "Tender Reactive Days To Return Quote:";
            this.ItemForTenderReactiveDaysToReturnQuote.TextSize = new System.Drawing.Size(197, 13);
            // 
            // ItemForCMTenderRequestNotesFile
            // 
            this.ItemForCMTenderRequestNotesFile.Control = this.CMTenderRequestNotesFileButtonEdit;
            this.ItemForCMTenderRequestNotesFile.Location = new System.Drawing.Point(0, 48);
            this.ItemForCMTenderRequestNotesFile.Name = "ItemForCMTenderRequestNotesFile";
            this.ItemForCMTenderRequestNotesFile.Size = new System.Drawing.Size(577, 24);
            this.ItemForCMTenderRequestNotesFile.Text = "CM Tender Request Notes File:";
            this.ItemForCMTenderRequestNotesFile.TextSize = new System.Drawing.Size(197, 13);
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.Location = new System.Drawing.Point(288, 0);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(289, 48);
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForTenderProactiveDaysToReturnQuote
            // 
            this.ItemForTenderProactiveDaysToReturnQuote.Control = this.TenderProactiveDaysToReturnQuoteSpinEdit;
            this.ItemForTenderProactiveDaysToReturnQuote.Location = new System.Drawing.Point(0, 24);
            this.ItemForTenderProactiveDaysToReturnQuote.MaxSize = new System.Drawing.Size(288, 24);
            this.ItemForTenderProactiveDaysToReturnQuote.MinSize = new System.Drawing.Size(288, 24);
            this.ItemForTenderProactiveDaysToReturnQuote.Name = "ItemForTenderProactiveDaysToReturnQuote";
            this.ItemForTenderProactiveDaysToReturnQuote.Size = new System.Drawing.Size(288, 24);
            this.ItemForTenderProactiveDaysToReturnQuote.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTenderProactiveDaysToReturnQuote.Text = "Tender Proactive Days To Return Quote:";
            this.ItemForTenderProactiveDaysToReturnQuote.TextSize = new System.Drawing.Size(197, 13);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.Location = new System.Drawing.Point(0, 610);
            this.emptySpaceItem17.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem17.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(601, 10);
            this.emptySpaceItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.Location = new System.Drawing.Point(300, 280);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(301, 24);
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem20
            // 
            this.emptySpaceItem20.AllowHotTrack = false;
            this.emptySpaceItem20.Location = new System.Drawing.Point(300, 304);
            this.emptySpaceItem20.Name = "emptySpaceItem20";
            this.emptySpaceItem20.Size = new System.Drawing.Size(301, 24);
            this.emptySpaceItem20.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDefaultCompletionSheetTemplateID
            // 
            this.ItemForDefaultCompletionSheetTemplateID.Control = this.DefaultCompletionSheetTemplateIDGridLookUpEdit;
            this.ItemForDefaultCompletionSheetTemplateID.Location = new System.Drawing.Point(0, 458);
            this.ItemForDefaultCompletionSheetTemplateID.Name = "ItemForDefaultCompletionSheetTemplateID";
            this.ItemForDefaultCompletionSheetTemplateID.Size = new System.Drawing.Size(601, 24);
            this.ItemForDefaultCompletionSheetTemplateID.Text = "Default Completion Sheet Layout:";
            this.ItemForDefaultCompletionSheetTemplateID.TextSize = new System.Drawing.Size(162, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImageOptions.Image")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(601, 779);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Comments:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(601, 779);
            this.ItemForRemarks.Text = "Comments:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // ItemForContractDescription
            // 
            this.ItemForContractDescription.Control = this.ContractDescriptionTextEdit;
            this.ItemForContractDescription.CustomizationFormText = "Contract Description:";
            this.ItemForContractDescription.Location = new System.Drawing.Point(0, 47);
            this.ItemForContractDescription.Name = "ItemForContractDescription";
            this.ItemForContractDescription.Size = new System.Drawing.Size(649, 24);
            this.ItemForContractDescription.Text = "Contract Description:";
            this.ItemForContractDescription.TextSize = new System.Drawing.Size(162, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 954);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(649, 10);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(649, 10);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06059_OM_Client_Contract_EditTableAdapter
            // 
            this.sp06059_OM_Client_Contract_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp06060_OM_GC_Companies_With_BlankTableAdapter
            // 
            this.sp06060_OM_GC_Companies_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06061_OM_Sectors_With_BlankTableAdapter
            // 
            this.sp06061_OM_Sectors_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06062_OM_Contract_Statuses_With_BlankTableAdapter
            // 
            this.sp06062_OM_Contract_Statuses_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06063_OM_Contract_Types_With_BlankTableAdapter
            // 
            this.sp06063_OM_Contract_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp01055_Core_Get_Report_LayoutsTableAdapter
            // 
            this.sp01055_Core_Get_Report_LayoutsTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Client_Contract_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(686, 661);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Client_Contract_Edit";
            this.Text = "Edit Client Contract";
            this.Activated += new System.EventHandler(this.frm_OM_Client_Contract_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Client_Contract_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Client_Contract_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DefaultCompletionSheetTemplateIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06059OMClientContractEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01055CoreGetReportLayoutsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CMTenderRequestNotesFileButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderProactiveDaysToReturnQuoteSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderReactiveDaysToReturnQuoteSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureForClientBillingCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureForTeamSelfBillingCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientInstructionsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPreviousContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPreviousContractButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastClientPaymentDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastClientPaymentDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BillingCentreCodeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BillingCentreCodeButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinanceClientCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckRPIDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckRPIDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YearlyPercentageIncreaseSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06063OMContractTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractDirectorIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractDirectorButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractStatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06062OMContractStatusesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SectorTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06061OMSectorsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GCCompanyIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06060OMGCCompaniesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractDirectorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBillingCentreCodeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPreviousContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCentreCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCentre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCentreID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGCCompanyID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSectorTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractDirector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForYearlyPercentageIncrease)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCheckRPIDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinanceClientCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBillingCentreCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastClientPaymentDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPreviousContract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientInstructions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSignatureForTeamSelfBilling)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforSignatureForClientBilling)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderReactiveDaysToReturnQuote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCMTenderRequestNotesFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderProactiveDaysToReturnQuote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultCompletionSheetTemplateID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.ButtonEdit ClientNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientContractID;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit ClientContractIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpAddress;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private System.Windows.Forms.BindingSource sp06059OMClientContractEditBindingSource;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DataSet_OM_ContractTableAdapters.sp06059_OM_Client_Contract_EditTableAdapter sp06059_OM_Client_Contract_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit ClientIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DevExpress.XtraEditors.GridLookUpEdit GCCompanyIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGCCompanyID;
        private System.Windows.Forms.BindingSource sp06060OMGCCompaniesWithBlankBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06060_OM_GC_Companies_With_BlankTableAdapter sp06060_OM_GC_Companies_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.GridLookUpEdit SectorTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSectorTypeID;
        private System.Windows.Forms.BindingSource sp06061OMSectorsWithBlankBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06061_OM_Sectors_With_BlankTableAdapter sp06061_OM_Sectors_With_BlankTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit ContractStatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractStatusID;
        private System.Windows.Forms.BindingSource sp06062OMContractStatusesWithBlankBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06062_OM_Contract_Statuses_With_BlankTableAdapter sp06062_OM_Contract_Statuses_With_BlankTableAdapter;
        private DevExpress.XtraEditors.ButtonEdit ContractDirectorButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractDirector;
        private DevExpress.XtraEditors.TextEdit ContractDirectorIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractDirectorID;
        private DevExpress.XtraEditors.GridLookUpEdit ContractTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractTypeID;
        private System.Windows.Forms.BindingSource sp06063OMContractTypesWithBlankBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06063_OM_Contract_Types_With_BlankTableAdapter sp06063_OM_Contract_Types_With_BlankTableAdapter;
        private DevExpress.XtraEditors.DateEdit StartDateDateEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.DateEdit EndDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraEditors.CheckEdit ActiveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActive;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraEditors.SpinEdit ContractValueSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractValue;
        private DevExpress.XtraEditors.SpinEdit YearlyPercentageIncreaseSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForYearlyPercentageIncrease;
        private DevExpress.XtraEditors.DateEdit CheckRPIDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCheckRPIDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraEditors.TextEdit FinanceClientCodeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinanceClientCode;
        private DevExpress.XtraEditors.TextEdit BillingCentreCodeIDTextEdit;
        private DevExpress.XtraEditors.ButtonEdit BillingCentreCodeButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBillingCentreCodeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBillingCentreCode;
        private DevExpress.XtraEditors.DateEdit LastClientPaymentDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLastClientPaymentDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraEditors.ButtonEdit LinkedToPreviousContractButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToPreviousContract;
        private DevExpress.XtraEditors.TextEdit LinkedToPreviousContractIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToPreviousContractID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraEditors.TextEdit CostCentreTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostCentre;
        private DevExpress.XtraEditors.TextEdit CostCentreCodeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostCentreCode;
        private DevExpress.XtraEditors.TextEdit CostCentreIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostCentreID;
        private DevExpress.XtraEditors.CheckEdit ReactiveCheckEdit;
        private DevExpress.XtraEditors.TextEdit ContractDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReactive;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractDescription;
        private DevExpress.XtraEditors.MemoEdit ClientInstructionsMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientInstructions;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraEditors.CheckEdit SignatureForClientBillingCheckEdit;
        private DevExpress.XtraEditors.CheckEdit SignatureForTeamSelfBillingCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSignatureForTeamSelfBilling;
        private DevExpress.XtraLayout.LayoutControlItem ItemforSignatureForClientBilling;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraEditors.SpinEdit TenderProactiveDaysToReturnQuoteSpinEdit;
        private DevExpress.XtraEditors.SpinEdit TenderReactiveDaysToReturnQuoteSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTenderReactiveDaysToReturnQuote;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTenderProactiveDaysToReturnQuote;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem20;
        private DevExpress.XtraEditors.ButtonEdit CMTenderRequestNotesFileButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCMTenderRequestNotesFile;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraEditors.GridLookUpEdit DefaultCompletionSheetTemplateIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDefaultCompletionSheetTemplateID;
        private System.Windows.Forms.BindingSource sp01055CoreGetReportLayoutsBindingSource;
        private DataSet_Common_Functionality dataSet_Common_Functionality;
        private DataSet_Common_FunctionalityTableAdapters.sp01055_Core_Get_Report_LayoutsTableAdapter sp01055_Core_Get_Report_LayoutsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLayoutID;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLayoutName;
        private DevExpress.XtraGrid.Columns.GridColumn colReportTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colPublishedToWeb;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
    }
}

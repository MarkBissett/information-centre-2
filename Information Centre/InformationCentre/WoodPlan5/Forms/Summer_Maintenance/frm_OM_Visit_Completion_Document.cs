using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;  // Used by File.Delete and FileSystemWatcher //
using System.Data.SqlClient;  // Used by Generate Map process //
using System.Reflection;

using DevExpress.LookAndFeel;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UserDesigner;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;
using System.ComponentModel.Design;  // Used by process to auto link event for custom sorting when a object is dropped onto the design panel of the report //
using DevExpress.XtraReports.Design;
using DevExpress.XtraReports.UI;
using DevExpress.XtraEditors.Repository;  // Required by Hyperlink Repository Items //
using DevExpress.XtraPrinting;

using WoodPlan5.Properties;
using WoodPlan5.Reports;
using BaseObjects;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace WoodPlan5
{
    public partial class frm_OM_Visit_Completion_Document : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        private string strConnectionStringREADONLY = "";
        Settings set = Settings.Default;
        public string strSignaturePath = "";
        public string strPicturePath = "";
        public string _ReportLayoutFolder = "";
        public string _strExistingPDFFileName = "";
        public string _strPDFFolder = "";
 
        public string _strVisitIDs = "";
        public int _intClientID = 0;
        public int _intClientCount = 0;

        rpt_OM_Visit_Completion_Sheet rptReport;
        WaitDialogForm loadingForm = null;

        public int i_int_PassedInTemplateID = 0;
 
        private int i_int_SelectedLayoutID = 0;
        private string i_str_SelectedFilename = "";
        public string i_str_SavedDirectoryName = "";

        private bool iBool_AllowAdd = false;
        private bool iBool_AllowEdit = false;
        private bool iBool_AllowDelete = false;
        private bool _iBool_AllowDefaultLayoutEdit = false;
        private string _DefaultLayoutReportName = "VisitCompletionSheet.repx";

        GridHitInfo downHitInfo = null;

        #endregion
       
        public frm_OM_Visit_Completion_Document()
        {
            InitializeComponent();
        }

        private void frm_OM_Visit_Completion_Document_Load(object sender, EventArgs e)
        {
            this.FormID = 7025;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            strConnectionStringREADONLY = GlobalSettings.ConnectionStringREADONLY;

            Set_Grid_Highlighter_Transparent(this.Controls);

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();

            ribbonControl1.Minimized = false;
            bbiEmailToClient.Enabled = (_intClientID > 0 && _intClientCount == 1);
            bsiInformation.Visibility = (bbiEmailToClient.Enabled ? DevExpress.XtraBars.BarItemVisibility.Never : DevExpress.XtraBars.BarItemVisibility.Always);

            try
            {
                sp01055_Core_Get_Report_LayoutsTableAdapter.Connection.ConnectionString = strConnectionString;
                sp01055_Core_Get_Report_LayoutsTableAdapter.Fill(dataSet_Common_Functionality.sp01055_Core_Get_Report_Layouts, 7, 101, 1);
                gridControl1.ForceInitialize();
            }
            catch (Exception) { }
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                _ReportLayoutFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_ReportLayouts").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Report Layouts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Report Layouts Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!_ReportLayoutFolder.EndsWith("\\")) _ReportLayoutFolder += "\\";  // Add Backslash to end //
            i_str_SavedDirectoryName = _ReportLayoutFolder;

            // Get Save Folder for PDFs //
            try
            {
                var GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                _strPDFFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_VisitCompletionSheetSaveFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Saved Completion Sheet Folder (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Folder", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!_strPDFFolder.EndsWith("\\")) _strPDFFolder += "\\";

            SetMenuStatus();
        }

        public override void PostOpen(object objParameter)
        {           
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();

            // Default layout passed in so find it and select it //
            if (i_int_PassedInTemplateID > 0)
            {
                GridView view = (GridView)gridControl1.MainView;
                int intFoundRow = view.LocateByValue(0, view.Columns["ReportLayoutID"], i_int_PassedInTemplateID);
                if (intFoundRow > -1)
                {
                    view.FocusedRowHandle = intFoundRow;
                }
                barEditItem1.EditValue = PopupContainerEdit1_Get_Layout_Selected();  // Set Selected Layout Text //
            }

            bbiView.PerformClick();  // Load Document //
        }

        private void frm_OM_Visit_Completion_Document_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_OM_Visit_Completion_Document_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (string.IsNullOrEmpty(printControl1.PrintingSystem.Document.Name)) return;
            /*if (printControl1.PrintingSystem.Document.Name != "Document")
            {
                printControl1.PrintingSystem.ClearContent(); // This should free up any map jpg if open //
                rptReport.Dispose();
                rptReport = null;
                GC.GetTotalMemory(true);
            }*/
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        iBool_AllowAdd = sfpPermissions.blCreate;
                        iBool_AllowEdit = sfpPermissions.blUpdate;
                        iBool_AllowDelete = sfpPermissions.blDelete;
                        break;
                    case 1:  // Edit default layout Button //    
                        _iBool_AllowDefaultLayoutEdit = sfpPermissions.blUpdate;
                        break;
                }
            }
        }
        
        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                bsiAdd.Enabled = true;
                bbiSingleAdd.Enabled = true;
                btnLayoutAddNew.Enabled = true;
            }
            else
            {
                bsiAdd.Enabled = false;
                bbiSingleAdd.Enabled = false;
                btnLayoutAddNew.Enabled = false;
            }
            if ((iBool_AllowEdit || _iBool_AllowDefaultLayoutEdit) && intRowHandles.Length == 1)
            {
                alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                bsiEdit.Enabled = true;
                bbiSingleEdit.Enabled = true;
                bbiEditLayout.Enabled = true;
            }
            else
            {
                bsiEdit.Enabled = false;
                bbiSingleEdit.Enabled = false;
                bbiEditLayout.Enabled = false;
            }
            if (iBool_AllowDelete && intRowHandles.Length >= 1)
            {
                alItems.Add("iDelete");
                bbiDelete.Enabled = true;
            }
            else
            {
                bbiDelete.Enabled = false;
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

        }

        private void bbiView_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {           
            if (i_int_SelectedLayoutID <= 0)
            {
                Load_Layout(_ReportLayoutFolder + _DefaultLayoutReportName);  // Standard default layout - not a customised one //
            }
            else
            {
                Load_Layout(_ReportLayoutFolder + i_int_SelectedLayoutID.ToString() + ".repx");  // Customised layout //
            }
        }

        private void Load_Layout(string strLayoutPath)
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                this.splashScreenManager = splashScreenManager1;
                //this.splashScreenManager.ShowWaitForm();
            }

            rpt_OM_Visit_Completion_Sheet rptReport = new rpt_OM_Visit_Completion_Sheet(this.GlobalSettings, _strVisitIDs, strSignaturePath, strPicturePath, 0, 0);
            try
            {
                rptReport.LoadLayout(strLayoutPath);

                AdjustEventHandlers(rptReport);  // Tie in custom Sorting //

                printControl1.PrintingSystem = rptReport.PrintingSystem;
                printingSystem1.Begin();
                rptReport.CreateDocument();
                //printControl1.Zoom = 0.90f;  // 90% //

                //printControl1.ExecCommand(PrintingSystemCommand.DocumentMap, new object[] { false });  // Hide the Document Map //
                printingSystem1.End();
            }
            catch (Exception Ex)
            {
                //if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                Console.WriteLine(Ex.Message);
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            //if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void bbiSaveDocument_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string strDateTime = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");

            // Check how many Visits we have loaded //
            DialogResult dr = DialogResult.Yes;
            char[] delimiters = new char[] { ',' };
            string[] strArray = _strVisitIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length > 1)
            {
                dr = XtraMessageBox.Show("You have " + strArray.Length.ToString() + " Visits Loaded.\n\nDo you wish to save each visit as a separate PDF file or save all visits in one PDF file?\n\nClick <b>Yes</b> to save each visit to <b>individual files.</b>.\n\nClick <b>No</b> to save all visits as <b>one file</b>.", "Save Visit Completion Sheets", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, DefaultBoolean.True);
            }
            switch (dr)
            {
                case DialogResult.Cancel:
                    return;
                case DialogResult.No:
                    {
                        var fProgress = new frmProgress(0);
                        fProgress.UpdateCaption("Preparing Email...");
                        fProgress.Show();
                        System.Windows.Forms.Application.DoEvents();

                        string strPDFName = SaveReport(fProgress, _strVisitIDs, Convert.ToInt32(strArray[0]), 1, 0, strDateTime, 0, 0);
                        if (fProgress != null)
                        {
                            fProgress.SetProgressValue(100);
                            fProgress.Close();
                            fProgress = null;
                        }
                        if (!string.IsNullOrWhiteSpace(strPDFName)) DevExpress.XtraEditors.XtraMessageBox.Show("PDF File Saved Successfully.\n\nSaved File Location: " + strPDFName, "Save Visit Completion Sheet", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case DialogResult.Yes:
                    {
                        var fProgress = new frmProgress(0);
                        fProgress.UpdateCaption("Preparing Email...");
                        fProgress.Show();
                        System.Windows.Forms.Application.DoEvents();
                        int intUpdateProgressThreshhold = strArray.Length / 10;
                        int intUpdateProgressTempCount = 0;

                        foreach (string strElement in strArray)
                        {
                            string strPDFName = SaveReport(fProgress, strElement + ",", Convert.ToInt32(strElement), 0, 0, strDateTime, 0, 0);
                            if (string.IsNullOrWhiteSpace(strPDFName))
                            {
                                if (fProgress != null)
                                {
                                    fProgress.Close();
                                    fProgress = null;
                                }
                                return;
                            }
                            intUpdateProgressTempCount++;  // Update Progress Bar //
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                if (fProgress != null) fProgress.UpdateProgress(10);
                            }
                        }
                        if (fProgress != null)
                        {
                            fProgress.SetProgressValue(100);
                            fProgress.Close();
                            fProgress = null;
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("PDF Files Saved Successfully.\n\nSaved File Folder Location: " + _strPDFFolder, "Save Visit Completion Sheet", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                default:
                    return;
            }
        }
        private string SaveReport(frmProgress fProgress, string strVisitIDs, int intVisitID, int intNameType, int intRecordID, string strDateTime, int intTeamID, int intLabourTypeID)
        {
            // Load Report into memory //
            rpt_OM_Visit_Completion_Sheet rptReport = new rpt_OM_Visit_Completion_Sheet(this.GlobalSettings, strVisitIDs, strSignaturePath, strPicturePath, intTeamID, intLabourTypeID);
            try
            {
                if (i_int_SelectedLayoutID <= 0)
                {
                    rptReport.LoadLayout(_ReportLayoutFolder + _DefaultLayoutReportName);  // Standard default layout - not a customised one //
                }
                else
                {
                    rptReport.LoadLayout(_ReportLayoutFolder + i_int_SelectedLayoutID.ToString() + ".repx");  // Customised layout //
                }
            }
            catch (Exception Ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Save Visit Completion Sheet", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "";
            }
            // Set security options of report so when it is exported, it can't be edited and is password protected //
            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.EnableCopying = false;
            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.ChangingPermissions = DevExpress.XtraPrinting.ChangingPermissions.None;
            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.PrintingPermissions = DevExpress.XtraPrinting.PrintingPermissions.HighResolution;
            rptReport.ExportOptions.Pdf.Compressed = true;
            rptReport.ExportOptions.Pdf.ImageQuality = PdfJpegImageQuality.Low;
            //rptReport.ExportOptions.Pdf.NeverEmbeddedFonts = "";

            // Get Filename //
            string strSavedFileName = "";
            try
            {
                BaseObjects.ExtensionFunctions extFunctions = new BaseObjects.ExtensionFunctions();
                var GetFilename = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                GetFilename.ChangeConnectionString(strConnectionString);
                strSavedFileName = extFunctions.RemoveSpecialCharacters(GetFilename.sp06384_OM_Completion_Sheet_Report_Get_Saved_File_Name(intVisitID, intNameType, intRecordID).ToString());

                fProgress.UpdateCaption("Saving - " + strSavedFileName + "...");
                System.Windows.Forms.Application.DoEvents();
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Filename for the Saved PDF File - Save process aborted.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Save Visit Completion Sheet - Get Filename", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "";
            }

            // Save physical PDF file //
            string strPDFName = _strPDFFolder + strSavedFileName + "_" + strDateTime + ".pdf"; ;
            try
            {
                rptReport.ExportToPdf(strPDFName);
            }
            catch (Exception Ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save PDF File - an error occurred while generating the PDF File [" + Ex.Message + "].\n\nTry again. If problems persists - contact Technical Support.", "Save Visit Completion Sheet", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "";
            }
            return strPDFName;
        }

        private void bbiEmailToClient_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Get Save Folder //
            string strSubject = "";
            string strCCToEmailAddress = "";
            string strEmailAddress = "";
            try
            {
                var GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strSubject = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_VisitCompletionSheetSubjectLine").ToString();
                strCCToEmailAddress = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_VisitCompletionSheetCCToEmailAddress").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Email Subject Line and\\or Email CC To Address (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Open Select Receipient List to find who to send the email to //
            var fChildForm = new frm_Core_Select_Client_Contact();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "multiple";
            fChildForm.strPassedInClientIDsFilter = _intClientID.ToString() + ",";
            fChildForm.strPassedInContactTypeIDsFilter = "4,";  // 4: Email //

            if (fChildForm.ShowDialog() != DialogResult.OK) return;
            strEmailAddress = fChildForm.strSelectedDescription1;

            string strDateTime = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");

            // Check how many Visits we have loaded //
            DialogResult dr = DialogResult.Yes;
            char[] delimiters = new char[] { ',' };
            string[] strArray = _strVisitIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            Array.Sort(strArray, StringComparer.InvariantCulture); // Sort by VisitID //

            if (strArray.Length > 1)
            {
                dr = XtraMessageBox.Show("You have " + strArray.Length.ToString() + " Visits Loaded.\n\nDo you wish to save each visit as a separate PDF file or save all visits in one PDF file?\n\nClick <b>Yes</b> to save each visit to <b>individual files.</b>.\n\nClick <b>No</b> to save all visits as <b>one file</b>.", "Save Visit Completion Sheets", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, DefaultBoolean.True);
            }
            switch (dr)
            {
                case DialogResult.Cancel:
                    return;
                case DialogResult.No:
                    {
                        var fProgress = new frmProgress(0);
                        fProgress.UpdateCaption("Preparing Email...");
                        fProgress.Show();
                        System.Windows.Forms.Application.DoEvents();

                        string strPDFName = SaveReport(fProgress, _strVisitIDs, Convert.ToInt32(strArray[0]), 1, 0, strDateTime, 0, 0);
                        int intCounter = 0;  // Check if PDF file has completed writting //
                        do
                        {
                            System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds to give time for writing of PDF to complete //
                            intCounter++;
                            if (intCounter >= 60) break;
                        } while (!File.Exists(@strPDFName));
                        if (intCounter >= 60)
                        {
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the PDF File - The system Timed Out.\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        // Note can't use a simple mailto clause and a System.Diagnostics.Process.Start since it won't allow attachments so use custom MapiMailMessage class //
                        try
                        {
                            string strBody = "";
                            //MapiMailMessage message = new MapiMailMessage(strSubject, strBody);
                            Outlook.Application outlookApp = new Outlook.Application();
                            Outlook._MailItem oMailItem = (Outlook._MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                            Outlook.Inspector oInspector = oMailItem.GetInspector;
                            Outlook.Recipients oRecips = (Outlook.Recipients)oMailItem.Recipients;

                            Array arrayItems = strEmailAddress.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string strElement in arrayItems)
                            {
                                //message.Recipients.Add(strElement);
                                Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(strElement);
                                oRecip.Resolve();
                            }
                            //if (!string.IsNullOrWhiteSpace(strCCToEmailAddress)) message.Recipients.Add(strCCToEmailAddress, MapiMailMessage.RecipientType.CC);
                            if (!string.IsNullOrWhiteSpace(strCCToEmailAddress))
                            {
                                //Add CC
                                Outlook.Recipient oCCRecip = oRecips.Add(strCCToEmailAddress);
                                oCCRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                                oCCRecip.Resolve();
                            }
                            if (fProgress != null)
                            {
                                fProgress.SetProgressValue(100);
                                fProgress.Close();
                                fProgress = null;
                            }
                            //message.Files.Add(@strPDFName);
                            oMailItem.Attachments.Add(@strPDFName);
                            oMailItem.Subject = strSubject;
                            oMailItem.Body = strBody;
                            oMailItem.Display();  // Display the mailbox... oMailItem.Display(true) will open it modally //

                            //message.ShowDialog();  // Display Email Window to User //
                        }
                        catch (Exception ex) 
                        {
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the email.\n\nError: " + ex.Message + "\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
                case DialogResult.Yes:
                    {
                        var fProgress = new frmProgress(0);
                        fProgress.UpdateCaption("Preparing Email...");
                        fProgress.Show();
                        System.Windows.Forms.Application.DoEvents();

                        int intUpdateProgressThreshhold = strArray.Length / 10;
                        int intUpdateProgressTempCount = 0;

                        // Note can't use a simple mailto clause and a System.Diagnostics.Process.Start since it won't allow attachments so use custom MapiMailMessage class //
                        try
                        {
                            string strBody = "";
                            //MapiMailMessage message = new MapiMailMessage(strSubject, strBody);
                            Outlook.Application outlookApp = new Outlook.Application();
                            Outlook._MailItem oMailItem = (Outlook._MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                            Outlook.Inspector oInspector = oMailItem.GetInspector;
                            Outlook.Recipients oRecips = (Outlook.Recipients)oMailItem.Recipients;

                            Array arrayItems = strEmailAddress.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string strElement in arrayItems)
                            {
                                //message.Recipients.Add(strElement);
                                Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(strElement);
                                oRecip.Resolve();
                            }
                            //if (!string.IsNullOrWhiteSpace(strCCToEmailAddress)) message.Recipients.Add(strCCToEmailAddress, MapiMailMessage.RecipientType.CC);
                            if (!string.IsNullOrWhiteSpace(strCCToEmailAddress))
                            {
                                //Add CC
                                Outlook.Recipient oCCRecip = oRecips.Add(strCCToEmailAddress);
                                oCCRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                                oCCRecip.Resolve();
                            }

                            foreach (string strElement in strArray)
                            {
                                string strPDFName = SaveReport(fProgress, strElement + ",", Convert.ToInt32(strElement), 0, 0, strDateTime, 0, 0);
                                int intCounter = 0;  // Check if PDF file has completed writting //
                                do
                                {
                                    System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds to give time for writing of PDF to complete //
                                    intCounter++;
                                    if (intCounter >= 60) break;
                                } while (!File.Exists(@strPDFName));
                                if (intCounter >= 60)
                                {
                                    if (fProgress != null)
                                    {
                                        fProgress.Close();
                                        fProgress = null;
                                    }
                                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the PDF File - The system Timed Out.\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    return;
                                }
                                
                                if (string.IsNullOrWhiteSpace(strPDFName))
                                {
                                    if (fProgress != null)
                                    {
                                        fProgress.Close();
                                        fProgress = null;
                                    }
                                    return;
                                }
                                //message.Files.Add(@strPDFName);
                                oMailItem.Attachments.Add(@strPDFName);
                               
                                intUpdateProgressTempCount++;  // Update Progress Bar //
                                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                                {
                                    intUpdateProgressTempCount = 0;
                                    if (fProgress != null) fProgress.UpdateProgress(10);
                                }
                            }
                            if (fProgress != null)
                            {
                                fProgress.SetProgressValue(100);
                                fProgress.Close();
                                fProgress = null;
                            }
                            //message.ShowDialog();  // Display Email Window to User //
                            oMailItem.Subject = strSubject;
                            oMailItem.Body = strBody;
                            oMailItem.Display();  // Display the mailbox... oMailItem.Display(true) will open it modally //
                        }
                        catch (Exception ex)
                        {
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the email.\n\nError: " + ex.Message + "\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
                default:
                    return;
            }
        }

        private void bbiEmailToTeam_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Get Save Folder //
            string strSubject = "";
            string strCCToEmailAddress = "";
            string strPersonIDs = "";
            string strPersonTypeIDs = "";
            try
            {
                var GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strSubject = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_VisitCompletionSheetSubjectLine").ToString();
                strCCToEmailAddress = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_VisitCompletionSheetCCToEmailAddress").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Email Subject Line and\\or Email CC To Address (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            
            // Get unique list of TeamIDs from VisitIDs //
            SqlDataAdapter sdaData_Recipients = new SqlDataAdapter();
            DataSet dsData_Recipients = new DataSet("NewDataSet");
            try
            {
                using (var conn = new SqlConnection(strConnectionString))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand())
                    {
                        cmd.CommandText = "sp06426_OM_Get_Unique_Teams_From_VisitIDs";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@VisitIDs", _strVisitIDs));
                        cmd.Connection = conn;
                        sdaData_Recipients = new SqlDataAdapter(cmd);
                        sdaData_Recipients.Fill(dsData_Recipients, "Table");
                    }
                    conn.Close();
                }
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the TeamIDs from the loaded Visits.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Team Email Addresses", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            switch (dsData_Recipients.Tables[0].Rows.Count)
            {
                case 0:  // No Teams loaded //
                    {
                        XtraMessageBox.Show("The Selected Visits have no Teams working on them. Unable to generate Completion Sheets", "Save Visit Completion Sheets", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                        return;
                    }
                case 1:  // 1 Team - No action required //
                    break;
                default:  // Greater than one team loaded //
                    {
                        if (XtraMessageBox.Show("You have Visits for multiple teams loaded. If you proceed, separate emails will be generated for each team showing only thoses visits specific to that team. Are you sure you wish to proceed?", "Save Visit Completion Sheets", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == System.Windows.Forms.DialogResult.No)
                        {
                            return;
                        }
                    }
                    break;
            }

            // Open Select Recipient List to find who to send the email to //
            var fChildForm = new frm_OM_Visit_Completion_Document_Select_Recipient();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "multiple";
            fChildForm.dsData_Recipients = dsData_Recipients;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;
            if (fChildForm._SelectedCount <= 0) return;
            strPersonIDs = fChildForm._strParentIDs;
            strPersonTypeIDs = fChildForm._strParentTypeIDs;
            
            int intOwnersWithMultipleVisits = 0;
            int intLabourID = 0;
            int intLabourTypeID = 0;
            int intUniqueLabourCount = 0;
            GridView RecipientView = (GridView)fChildForm.gridControl1.MainView;

            char[] delimiters = new char[] { ',' };
            string[] strArrayPersons = strPersonIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            string[] strArrayPersonTypes = strPersonTypeIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            int intPosition = 0;
            foreach (string strLabourID in strArrayPersons)
            {
                intUniqueLabourCount++;
                intLabourID = Convert.ToInt32(strLabourID);
                intLabourTypeID = Convert.ToInt32(strArrayPersonTypes[intPosition]);
                try
                {
                    var GetCount = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                    GetCount.ChangeConnectionString(strConnectionString);
                    int intCount = Convert.ToInt32(GetCount.sp06424_OM_Get_Visit_Count_From_Team_IDs_And_VisitIDs(_strVisitIDs, intLabourID, intLabourTypeID));
                    if (intCount > 1) intOwnersWithMultipleVisits++;
                }
                catch (Exception) { }
                intPosition++;
            }

            DialogResult dialogResult = DialogResult.No;
            if (intOwnersWithMultipleVisits > 0)
            {
                dialogResult = XtraMessageBox.Show("You have " + intOwnersWithMultipleVisits.ToString() + " Teams with multiple Visits Loaded.\n\nDo you wish to save each visit as a separate PDF file or save all visits for each Team in one PDF file?\n\nClick <b>Yes</b> to save each visit to <b>individual files.</b>.\n\nClick <b>No</b> to save all visits as <b>one file</b>.", "Save Visit Completion Sheets", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, DefaultBoolean.True);
            }
            
            string strDateTime = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
            
            string strVisitIDs = "";
            switch (dialogResult)
            {
                case DialogResult.Cancel:
                    return;
                case DialogResult.No: // One PDF file for all visits per team //
                    {
                        var fProgress = new frmProgress(0);
                        fProgress.UpdateCaption("Preparing Email...");
                        fProgress.Show();
                        System.Windows.Forms.Application.DoEvents();

                        int intUpdateProgressThreshhold = intUniqueLabourCount / 10;
                        int intUpdateProgressTempCount = 0;

                        intPosition = 0;
                        foreach (string strLabourID in strArrayPersons)
                        {
                            intLabourID = Convert.ToInt32(strLabourID);
                            intLabourTypeID = Convert.ToInt32(strArrayPersonTypes[intPosition]);

                            intUpdateProgressTempCount++;  // Update Progress Bar //
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                if (fProgress != null) fProgress.UpdateProgress(10);
                            }
                            
                            try
                            {
                                var GetVisits = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                                GetVisits.ChangeConnectionString(strConnectionString);
                                strVisitIDs = GetVisits.sp06425_OM_Get_VisitIDs_From_Team_IDs_And_VisitIDs(_strVisitIDs, intLabourID, intLabourTypeID).ToString();
                            }
                            catch (Exception) 
                            {
                                continue;
                            }

                            // Note can't use a simple mailto clause and a System.Diagnostics.Process.Start since it won't allow attachments. Can't use custom MapiMailMessage class as it will only allow one email to be opened so use Outlook interop instead //
                            try
                            {
                                string strBody = "";
                                Outlook.Application outlookApp = new Outlook.Application();
                                Outlook._MailItem oMailItem = (Outlook._MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                                Outlook.Inspector oInspector = oMailItem.GetInspector;
                                Outlook.Recipients oRecips = (Outlook.Recipients)oMailItem.Recipients;

                                // Get email addresses from original select screen //
                                GridView view = (GridView)fChildForm.gridControl1.MainView;
                                for (int i = 0; i < view.DataRowCount; i++)
                                {
                                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                                    {
                                        if (Convert.ToInt32(view.GetRowCellValue(i, "OwnerID")) == intLabourID && Convert.ToInt32(view.GetRowCellValue(i, "OwnerTypeID")) == intLabourTypeID)
                                        {
                                            Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(view.GetRowCellValue(i, "Email").ToString());
                                            oRecip.Resolve();
                                        }
                                    }
                                }
                                if (!string.IsNullOrWhiteSpace(strCCToEmailAddress))
                                {
                                    //Add CC
                                    Outlook.Recipient oCCRecip = oRecips.Add(strCCToEmailAddress);
                                    oCCRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                                    oCCRecip.Resolve();
                                }

                                // Create PDF File //
                                string strPDFName = SaveReport(fProgress, strVisitIDs, 0, 3, intLabourID, strDateTime, intLabourID, intLabourTypeID);
                                int intCounter = 0;  // Check if PDF file has completed writting //
                                do
                                {
                                    System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds to give time for writing of PDF to complete //
                                    intCounter++;
                                    if (intCounter >= 60) break;
                                } while (!File.Exists(@strPDFName));
                                if (intCounter >= 60)
                                {
                                    if (fProgress != null)
                                    {
                                        fProgress.Close();
                                        fProgress = null;
                                    }
                                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the PDF File - The system Timed Out.\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    return;
                                }
                                oMailItem.Attachments.Add(@strPDFName);
                                oMailItem.Subject = strSubject;
                                oMailItem.Body = strBody;
                                oMailItem.Display();  // Display the mailbox... oMailItem.Display(true) will open it modally //

                                outlookApp = null;
                                oMailItem = null;
                                oInspector = null;
                                oRecips = null;
                            }
                            catch (Exception ex)
                            {
                                if (fProgress != null)
                                {
                                    fProgress.Close();
                                    fProgress = null;
                                }
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the email.\n\nError: " + ex.Message + "\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                            intPosition++;
                        }
                        if (fProgress != null)
                        {
                            fProgress.SetProgressValue(100);
                            fProgress.Close();
                            fProgress = null;
                        }
                    }
                    break;
                case DialogResult.Yes: // Separate PDF files per visit per team //
                    {
                        var fProgress = new frmProgress(0);
                        fProgress.UpdateCaption("Preparing Email...");
                        fProgress.Show();
                        System.Windows.Forms.Application.DoEvents();

                        int intUpdateProgressThreshhold = intUniqueLabourCount / 10;
                        int intUpdateProgressTempCount = 0;

                        intPosition = 0;
                        foreach (string strLabourID in strArrayPersons)
                        {
                            intLabourID = Convert.ToInt32(strLabourID);
                            intLabourTypeID = Convert.ToInt32(strArrayPersonTypes[intPosition]);

                            intUpdateProgressTempCount++;  // Update Progress Bar //
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                if (fProgress != null) fProgress.UpdateProgress(10);
                            }

                            try
                            {
                                var GetVisits = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                                GetVisits.ChangeConnectionString(strConnectionString);
                                strVisitIDs = GetVisits.sp06425_OM_Get_VisitIDs_From_Team_IDs_And_VisitIDs(_strVisitIDs, intLabourID, intLabourTypeID).ToString();
                            }
                            catch (Exception)
                            {
                                continue;
                            }

                            // Note can't use a simple mailto clause and a System.Diagnostics.Process.Start since it won't allow attachments. Can't use custom MapiMailMessage class as it will only allow one email to be opened so use Outlook interop instead //
                            try
                            {
                                string strBody = "";
                                Outlook.Application outlookApp = new Outlook.Application();
                                Outlook._MailItem oMailItem = (Outlook._MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                                Outlook.Inspector oInspector = oMailItem.GetInspector;
                                Outlook.Recipients oRecips = (Outlook.Recipients)oMailItem.Recipients;

                                // Get email addresses from original select screen //
                                GridView view = (GridView)fChildForm.gridControl1.MainView;
                                for (int i = 0; i < view.DataRowCount; i++)
                                {
                                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                                    {
                                        if (Convert.ToInt32(view.GetRowCellValue(i, "OwnerID")) == intLabourID && Convert.ToInt32(view.GetRowCellValue(i, "OwnerTypeID")) == intLabourTypeID)
                                        {
                                            Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(view.GetRowCellValue(i, "Email").ToString());
                                            oRecip.Resolve();
                                        }
                                    }
                                }
                                if (!string.IsNullOrWhiteSpace(strCCToEmailAddress))
                                {
                                    //Add CC
                                    Outlook.Recipient oCCRecip = oRecips.Add(strCCToEmailAddress);
                                    oCCRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                                    oCCRecip.Resolve();
                                }

                                // Create PDF Files //
                                string[] strArray = strVisitIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                                Array.Sort(strArray, StringComparer.InvariantCulture);  // Sort by VisitID //
                                foreach (string strElement in strArray)
                                {
                                    string strPDFName = SaveReport(fProgress, strElement + ",", Convert.ToInt32(strElement), 2, intLabourID, strDateTime, intLabourID, intLabourTypeID);
                                    int intCounter = 0;  // Check if PDF file has completed writting //
                                    do
                                    {
                                        System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds to give time for writing of PDF to complete //
                                        intCounter++;
                                        if (intCounter >= 60) break;
                                    } while (!File.Exists(@strPDFName));
                                    if (intCounter >= 60)
                                    {
                                        if (fProgress != null)
                                        {
                                            fProgress.Close();
                                            fProgress = null;
                                        }
                                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the PDF File - The system Timed Out.\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        return;
                                    }

                                    if (string.IsNullOrWhiteSpace(strPDFName))
                                    {
                                        if (fProgress != null)
                                        {
                                            fProgress.Close();
                                            fProgress = null;
                                        }
                                        return;
                                    }
                                    oMailItem.Attachments.Add(@strPDFName);

                                    intUpdateProgressTempCount++;  // Update Progress Bar //
                                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                                    {
                                        intUpdateProgressTempCount = 0;
                                        if (fProgress != null) fProgress.UpdateProgress(10);
                                    }
                                }

                                oMailItem.Subject = strSubject;
                                oMailItem.Body = strBody;
                                oMailItem.Display();  // Display the mailbox... oMailItem.Display(true) will open it modally //

                                outlookApp = null;
                                oMailItem = null;
                                oInspector = null;
                                oRecips = null;
                            }
                            catch (Exception ex)
                            {
                                if (fProgress != null)
                                {
                                    fProgress.Close();
                                    fProgress = null;
                                }
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the email.\n\nError: " + ex.Message + "\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                            intPosition++;
                        }
                        if (fProgress != null)
                        {
                            fProgress.SetProgressValue(100);
                            fProgress.Close();
                            fProgress = null;
                        }
                    }
                    break;
                default:
                    return;
            }
        }

        private void bbiEmailToCM_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Get Save Folder //
            string strSubject = "";
            string strCCToEmailAddress = "";
            string strPersonIDs = "";
            string strPersonTypeIDs = "";
            try
            {
                var GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strSubject = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_VisitCompletionSheetSubjectLine").ToString();
                strCCToEmailAddress = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_VisitCompletionSheetCCToEmailAddress").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Email Subject Line and\\or Email CC To Address (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get unique list of Contract ManagerIDs from VisitIDs //
            SqlDataAdapter sdaData_Recipients = new SqlDataAdapter();
            DataSet dsData_Recipients = new DataSet("NewDataSet");
            try
            {
                using (var conn = new SqlConnection(strConnectionString))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand())
                    {
                        cmd.CommandText = "sp06428_OM_Get_CMs_From_VisitIDs";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@VisitIDs", _strVisitIDs));
                        cmd.Connection = conn;
                        sdaData_Recipients = new SqlDataAdapter(cmd);
                        sdaData_Recipients.Fill(dsData_Recipients, "Table");
                    }
                    conn.Close();
                }
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Contract Manager IDs from the loaded Visits.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Contract Manager Email Addresses", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            switch (dsData_Recipients.Tables[0].Rows.Count)
            {
                case 0:  // No Teams loaded //
                    {
                        XtraMessageBox.Show("The Selected Visits have no Contract Managers linked to them. Unable to generate Completion Sheets", "Save Visit Completion Sheets", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                        return;
                    }
                case 1:  // 1 Team - No action required //
                    break;
                default:  // Greater than one team loaded //
                    {
                        if (XtraMessageBox.Show("You have Visits for multiple Contract Managers loaded. If you proceed, separate emails will be generated for each Contract Manager showing only thoses visits specific to that Contract Manager. Are you sure you wish to proceed?", "Save Visit Completion Sheets", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == System.Windows.Forms.DialogResult.No)
                        {
                            return;
                        }
                    }
                    break;
            }

            // Open Select Recipient List to find who to send the email to //
            var fChildForm = new frm_OM_Visit_Completion_Document_Select_Recipient();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "multiple";
            fChildForm.dsData_Recipients = dsData_Recipients;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;
            if (fChildForm._SelectedCount <= 0) return;
            strPersonIDs = fChildForm._strParentIDs;
            strPersonTypeIDs = fChildForm._strParentTypeIDs;

            int intOwnersWithMultipleVisits = 0;
            int intLabourID = 0;
            int intUniqueLabourCount = 0;
            GridView RecipientView = (GridView)fChildForm.gridControl1.MainView;

            char[] delimiters = new char[] { ',' };
            string[] strArrayPersons = strPersonIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            int intPosition = 0;
            foreach (string strLabourID in strArrayPersons)
            {
                intUniqueLabourCount++;
                intLabourID = Convert.ToInt32(strLabourID);
                try
                {
                    var GetCount = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                    GetCount.ChangeConnectionString(strConnectionString);
                    int intCount = Convert.ToInt32(GetCount.sp06423_OM_Get_Visit_Count_From_CM_IDs_And_VisitIDs(_strVisitIDs, intLabourID));
                    if (intCount > 1) intOwnersWithMultipleVisits++;
                }
                catch (Exception) { }
                intPosition++;
            }

            DialogResult dialogResult = DialogResult.No;
            if (intOwnersWithMultipleVisits > 0)
            {
                dialogResult = XtraMessageBox.Show("You have " + intOwnersWithMultipleVisits.ToString() + " Teams with multiple Visits Loaded.\n\nDo you wish to save each visit as a separate PDF file or save all visits for each Team in one PDF file?\n\nClick <b>Yes</b> to save each visit to <b>individual files.</b>.\n\nClick <b>No</b> to save all visits as <b>one file</b>.", "Save Visit Completion Sheets", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, DefaultBoolean.True);
            }

            string strDateTime = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");

            string strVisitIDs = "";
            switch (dialogResult)
            {
                case DialogResult.Cancel:
                    return;
                case DialogResult.No: // One PDF file for all visits per team //
                    {
                        var fProgress = new frmProgress(0);
                        fProgress.UpdateCaption("Preparing Email...");
                        fProgress.Show();
                        System.Windows.Forms.Application.DoEvents();

                        int intUpdateProgressThreshhold = intUniqueLabourCount / 10;
                        int intUpdateProgressTempCount = 0;

                        intPosition = 0;
                        foreach (string strLabourID in strArrayPersons)
                        {
                            intLabourID = Convert.ToInt32(strLabourID);

                            intUpdateProgressTempCount++;  // Update Progress Bar //
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                if (fProgress != null) fProgress.UpdateProgress(10);
                            }

                            try
                            {
                                var GetVisits = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                                GetVisits.ChangeConnectionString(strConnectionString);
                                strVisitIDs = GetVisits.sp06429_OM_Get_VisitIDs_From_CM_IDs_And_VisitIDs(_strVisitIDs, intLabourID).ToString();
                            }
                            catch (Exception)
                            {
                                continue;
                            }

                            // Note can't use a simple mailto clause and a System.Diagnostics.Process.Start since it won't allow attachments. Can't use custom MapiMailMessage class as it will only allow one email to be opened so use Outlook interop instead //
                            try
                            {
                                string strBody = "";
                                Outlook.Application outlookApp = new Outlook.Application();
                                Outlook._MailItem oMailItem = (Outlook._MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                                Outlook.Inspector oInspector = oMailItem.GetInspector;
                                Outlook.Recipients oRecips = (Outlook.Recipients)oMailItem.Recipients;

                                // Get email addresses from original select screen //
                                GridView view = (GridView)fChildForm.gridControl1.MainView;
                                for (int i = 0; i < view.DataRowCount; i++)
                                {
                                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                                    {
                                        if (Convert.ToInt32(view.GetRowCellValue(i, "OwnerID")) == intLabourID)
                                        {
                                            Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(view.GetRowCellValue(i, "Email").ToString());
                                            oRecip.Resolve();
                                        }
                                    }
                                }
                                if (!string.IsNullOrWhiteSpace(strCCToEmailAddress))
                                {
                                    //Add CC
                                    Outlook.Recipient oCCRecip = oRecips.Add(strCCToEmailAddress);
                                    oCCRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                                    oCCRecip.Resolve();
                                }

                                // Create PDF File //
                                string strPDFName = SaveReport(fProgress, strVisitIDs, 0, 5, intLabourID, strDateTime, 0, 0);
                                int intCounter = 0;  // Check if PDF file has completed writting //
                                do
                                {
                                    System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds to give time for writing of PDF to complete //
                                    intCounter++;
                                    if (intCounter >= 60) break;
                                } while (!File.Exists(@strPDFName));
                                if (intCounter >= 60)
                                {
                                    if (fProgress != null)
                                    {
                                        fProgress.Close();
                                        fProgress = null;
                                    }
                                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the PDF File - The system Timed Out.\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    return;
                                }
                                oMailItem.Attachments.Add(@strPDFName);
                                oMailItem.Subject = strSubject;
                                oMailItem.Body = strBody;
                                oMailItem.Display();  // Display the mailbox... oMailItem.Display(true) will open it modally //

                                outlookApp = null;
                                oMailItem = null;
                                oInspector = null;
                                oRecips = null;
                            }
                            catch (Exception ex)
                            {
                                if (fProgress != null)
                                {
                                    fProgress.Close();
                                    fProgress = null;
                                }
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the email.\n\nError: " + ex.Message + "\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                            intPosition++;
                        }
                        if (fProgress != null)
                        {
                            fProgress.SetProgressValue(100);
                            fProgress.Close();
                            fProgress = null;
                        }
                    }
                    break;
                case DialogResult.Yes: // Separate PDF files per visit per team //
                    {
                        var fProgress = new frmProgress(0);
                        fProgress.UpdateCaption("Preparing Email...");
                        fProgress.Show();
                        System.Windows.Forms.Application.DoEvents();

                        int intUpdateProgressThreshhold = intUniqueLabourCount / 10;
                        int intUpdateProgressTempCount = 0;

                        intPosition = 0;
                        foreach (string strLabourID in strArrayPersons)
                        {
                            intLabourID = Convert.ToInt32(strLabourID);

                            intUpdateProgressTempCount++;  // Update Progress Bar //
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                if (fProgress != null) fProgress.UpdateProgress(10);
                            }

                            try
                            {
                                var GetVisits = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                                GetVisits.ChangeConnectionString(strConnectionString);
                                strVisitIDs = GetVisits.sp06429_OM_Get_VisitIDs_From_CM_IDs_And_VisitIDs(_strVisitIDs, intLabourID).ToString();
                            }
                            catch (Exception)
                            {
                                continue;
                            }

                            // Note can't use a simple mailto clause and a System.Diagnostics.Process.Start since it won't allow attachments. Can't use custom MapiMailMessage class as it will only allow one email to be opened so use Outlook interop instead //
                            try
                            {
                                string strBody = "";
                                Outlook.Application outlookApp = new Outlook.Application();
                                Outlook._MailItem oMailItem = (Outlook._MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                                Outlook.Inspector oInspector = oMailItem.GetInspector;
                                Outlook.Recipients oRecips = (Outlook.Recipients)oMailItem.Recipients;

                                // Get email addresses from original select screen //
                                GridView view = (GridView)fChildForm.gridControl1.MainView;
                                for (int i = 0; i < view.DataRowCount; i++)
                                {
                                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                                    {
                                        if (Convert.ToInt32(view.GetRowCellValue(i, "OwnerID")) == intLabourID)
                                        {
                                            Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(view.GetRowCellValue(i, "Email").ToString());
                                            oRecip.Resolve();
                                        }
                                    }
                                }
                                if (!string.IsNullOrWhiteSpace(strCCToEmailAddress))
                                {
                                    //Add CC
                                    Outlook.Recipient oCCRecip = oRecips.Add(strCCToEmailAddress);
                                    oCCRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                                    oCCRecip.Resolve();
                                }

                                // Create PDF Files //
                                string[] strArray = strVisitIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                                Array.Sort(strArray, StringComparer.InvariantCulture);  // Sort by VisitID //
                                foreach (string strElement in strArray)
                                {
                                    string strPDFName = SaveReport(fProgress, strElement + ",", Convert.ToInt32(strElement), 4, intLabourID, strDateTime, 0, 0);
                                    int intCounter = 0;  // Check if PDF file has completed writting //
                                    do
                                    {
                                        System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds to give time for writing of PDF to complete //
                                        intCounter++;
                                        if (intCounter >= 60) break;
                                    } while (!File.Exists(@strPDFName));
                                    if (intCounter >= 60)
                                    {
                                        if (fProgress != null)
                                        {
                                            fProgress.Close();
                                            fProgress = null;
                                        }
                                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the PDF File - The system Timed Out.\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        return;
                                    }

                                    if (string.IsNullOrWhiteSpace(strPDFName))
                                    {
                                        if (fProgress != null)
                                        {
                                            fProgress.Close();
                                            fProgress = null;
                                        }
                                        return;
                                    }
                                    oMailItem.Attachments.Add(@strPDFName);

                                    intUpdateProgressTempCount++;  // Update Progress Bar //
                                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                                    {
                                        intUpdateProgressTempCount = 0;
                                        if (fProgress != null) fProgress.UpdateProgress(10);
                                    }
                                }

                                oMailItem.Subject = strSubject;
                                oMailItem.Body = strBody;
                                oMailItem.Display();  // Display the mailbox... oMailItem.Display(true) will open it modally //

                                outlookApp = null;
                                oMailItem = null;
                                oInspector = null;
                                oRecips = null;
                            }
                            catch (Exception ex)
                            {
                                if (fProgress != null)
                                {
                                    fProgress.Close();
                                    fProgress = null;
                                }
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the email.\n\nError: " + ex.Message + "\n\nPlease try again. If the problem persists, contact Technical Support.", "Email Completion Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                            intPosition++;
                        }
                        if (fProgress != null)
                        {
                            fProgress.SetProgressValue(100);
                            fProgress.Close();
                            fProgress = null;
                        }
                    }
                    break;
                default:
                    return;
            }
        }

        private void bbiEditLayout_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditRecord();
        }


        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            DeleteRecord();
        }
        private void DeleteRecord()
        {
            if (!iBool_AllowDelete) return;
            GridView view = (GridView)gridControl1.FocusedView;
            int? intReportLayoutID = Convert.ToInt32(view.GetFocusedRowCellValue("ReportLayoutID"));
            int intCreatedByID = Convert.ToInt32(view.GetFocusedRowCellValue("CreatedByID"));
            string strReportLayoutName = Convert.ToString(view.GetFocusedRowCellValue("ReportLayoutName"));
            string strReportFileName = Convert.ToString(intReportLayoutID) + ".repx";
            if (intReportLayoutID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to delete layout - no layout selected for deletion!\n\nSelect a layout before proceeding.\n\nNote: You cannot delete the Default Layout.", "Delete Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (intCreatedByID != GlobalSettings.UserID && GlobalSettings.UserType1.ToLower() != "super")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to delete layout. Only the creator of a layout can delete it.", "Delete Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to delete the selected layout: " + strReportLayoutName + "?\n\nIf you proceed, the layout will no longer be available for use!", "Delete Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Deleting...");

                // Delete the record then the physical file layout //
                using (var DeleteLayout = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
                {
                    DeleteLayout.ChangeConnectionString(strConnectionString);
                    try
                    {
                        DeleteLayout.sp01058_Core_Report_Layouts_Delete_Layout_Record(intReportLayoutID);
                    }
                    catch (Exception) 
                    {
                        if (splashScreenManager1.IsSplashFormVisible) splashScreenManager1.CloseWaitForm();
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to delete layout - it may no longer exist or you may not have the necessary permissions to delete it - contact Technical Support.", "Delete Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                }

                try
                {
                    File.Delete(i_str_SavedDirectoryName + strReportFileName);
                }
                catch (Exception) { }

                // Reload list of available layout and pre-select new one //
                sp01055_Core_Get_Report_LayoutsTableAdapter.Fill(dataSet_Common_Functionality.sp01055_Core_Get_Report_Layouts, 7, 101, 1);
                barEditItem1.EditValue = "-- No Custom Layout --";
                i_int_SelectedLayoutID = 0;
                if (splashScreenManager1.IsSplashFormVisible) splashScreenManager1.CloseWaitForm();
                bbiView.PerformClick();
            }
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            EditRecord();
        }
        private void EditRecord()
        {
            if (!iBool_AllowEdit) return;
            GridView view = (GridView)gridControl1.FocusedView;
            int? intReportLayoutID = Convert.ToInt32(view.GetFocusedRowCellValue("ReportLayoutID"));
            int intCreatedByID = Convert.ToInt32(view.GetFocusedRowCellValue("CreatedByID"));
            string strReportLayoutName = Convert.ToString(view.GetFocusedRowCellValue("ReportLayoutName"));
            string strReportFileName = (intReportLayoutID == 0 ? _DefaultLayoutReportName : intReportLayoutID.ToString() + ".repx");
            if (intReportLayoutID <= 0 && !_iBool_AllowDefaultLayoutEdit)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to edit default layout - you do not have the required permissions to do this.", "Edit Default Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (intReportLayoutID > 0 && intCreatedByID != GlobalSettings.UserID && intCreatedByID > 0 && GlobalSettings.UserType1.ToLower() != "super")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to edit layout. Only the creator of a layout can edit it.", "Edit Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to edit the selected layout?", "Edit Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                rptReport = new rpt_OM_Visit_Completion_Sheet(this.GlobalSettings, _strVisitIDs, strSignaturePath, strPicturePath, 0, 0);

                try
                {
                    rptReport.LoadLayout(i_str_SavedDirectoryName + strReportFileName);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                loadingForm = new WaitDialogForm("Loading Report Builder...", "Custom Completion Sheet");
                loadingForm.Show();

                // Open the report in the Report Builder //
                // Create a design form and get its panel.
                XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
                //XRDesignFormEx form = new XRDesignFormEx();
                XRDesignPanel panel = form.DesignPanel;
                panel.SetCommandVisibility(ReportCommand.NewReport, DevExpress.XtraReports.UserDesigner.CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.OpenFile, DevExpress.XtraReports.UserDesigner.CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.SaveFileAs, DevExpress.XtraReports.UserDesigner.CommandVisibility.None);

                // Add a new command handler to the Report Designer which saves the report in a custom way.
                panel.AddCommandHandler(new SaveCommandHandler(panel, i_str_SavedDirectoryName, strReportFileName));

                // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
                panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

                panel.OpenReport(rptReport);
                form.Shown += new EventHandler(form_Shown);  // Fires event after report builder is shown //
                form.WindowState = FormWindowState.Maximized;
                loadingForm.Close();
                form.ShowDialog();
                panel.CloseReport();

                // check if the loaded report is the edited one - if yes then refresh the currently viewed report //
                if (Convert.ToInt32(intReportLayoutID) == i_int_SelectedLayoutID)
                {
                    bbiView.PerformClick();
                }
            }
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            if (!iBool_AllowAdd) return;
            AddLayout();
        }
        private void AddLayout()
        {
            Form frmMain = this.MdiParent;
            var fChildForm = new frm_Core_Report_Layout_Create();
            frmMain.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            int intNewLayoutID = 0;
            string strNewLayoutName = "";
            if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            {
                return;
            }
            intNewLayoutID = fChildForm.intReturnedValue;
            strNewLayoutName = fChildForm.strReturnedLayoutName;

            // Save the new Layout and get back it's ID for the physical filename //
            int intNewID = 0;
            using (var AddLayout = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
            {
                AddLayout.ChangeConnectionString(strConnectionString);
                try
                {
                    intNewID = Convert.ToInt32(AddLayout.sp01057_Core_Report_Add_Layouts_Create_Layout_Record(101, strNewLayoutName, this.GlobalSettings.UserID, 0));
                    if (intNewID <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to create new report layout, a problem occurred - contact Technical Support.", "Create Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                }
                catch (Exception) { }
            } 

            // Load the report - Create a design form and get its panel.
            XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
            //XRDesignFormEx form = new XRDesignFormEx();

            XRDesignPanel panel = form.DesignPanel;
            panel.SetCommandVisibility(ReportCommand.NewReport, DevExpress.XtraReports.UserDesigner.CommandVisibility.None);
            panel.SetCommandVisibility(ReportCommand.OpenFile, DevExpress.XtraReports.UserDesigner.CommandVisibility.None);
            panel.SetCommandVisibility(ReportCommand.SaveFileAs, DevExpress.XtraReports.UserDesigner.CommandVisibility.None);

            rptReport = new rpt_OM_Visit_Completion_Sheet(this.GlobalSettings, _strVisitIDs, strSignaturePath, strPicturePath, 0, 0);
            rptReport.Name = "Completion Sheet";  // Set Document Root Node to meaningful text //
            if (intNewLayoutID > 0)  // Load existing custom layout as a starting point //
            {
                i_str_SelectedFilename = Convert.ToInt32(intNewLayoutID) + ".repx";
                try
                {
                    rptReport.LoadLayout(i_str_SavedDirectoryName + i_str_SelectedFilename);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to view report, the layout may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Create Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else if (intNewLayoutID == -1)  // Load default layout as a starting point //
            {
                try
                {
                    rptReport.LoadLayout(i_str_SavedDirectoryName + _DefaultLayoutReportName);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load report, the default layout may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Create Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            // if neither above, a blank will be loaded //

            // Set filename to correct one so that when save fires from the report designer, the correct file is created/updated //
            i_str_SelectedFilename = Convert.ToInt32(intNewID) + ".repx";

            // Add a new command handler to the Report Designer which saves the report in a custom way.
            panel.AddCommandHandler(new SaveCommandHandler(panel, i_str_SavedDirectoryName, i_str_SelectedFilename));

            // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
            panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

            loadingForm = new WaitDialogForm("Loading Report Builder...", "Create Report Layout");
            loadingForm.Show();

            // Load the report into the design form and show the form.
            panel.OpenReport(rptReport);
            form.Shown += new EventHandler(form_Shown);  // Fires event after report builder is shown //
            form.WindowState = FormWindowState.Maximized;
            loadingForm.Close();
            form.ShowDialog();
            panel.CloseReport();

            // Reload list of available layout and pre-select new one //
            sp01055_Core_Get_Report_LayoutsTableAdapter.Fill(dataSet_Common_Functionality.sp01055_Core_Get_Report_Layouts, 7, 101, 1);
            GridView view = (GridView)gridControl1.MainView;
            int intFoundRow = view.LocateByValue(0, view.Columns["ReportLayoutID"], intNewID);
            if (intFoundRow > -1)
            {
                view.FocusedRowHandle = intFoundRow;
            }
            barEditItem1.EditValue = PopupContainerEdit1_Get_Layout_Selected();  // Set Selected Layout Text //
            bbiView.PerformClick();  // Load the new layout //
        }


        #region PopupContainerLayout

        private void btnLayoutOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void btnLayoutAddNew_Click(object sender, EventArgs e)
        {
            if (!iBool_AllowAdd) return;
            AddLayout();
        }

        private void repositoryItemPopupContainerEdit1_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Layout_Selected();
            bbiView.PerformClick();
        }
        private string PopupContainerEdit1_Get_Layout_Selected()
        {
            int[] intRowHandles;
            int intCount = 0;

            GridView view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                i_int_SelectedLayoutID = 0;
                i_str_SelectedFilename = "";
                return "-- No Custom Layout --";
            }
            else
            {
                i_int_SelectedLayoutID = Convert.ToInt32(view.GetFocusedRowCellValue("ReportLayoutID"));
                i_str_SelectedFilename = Convert.ToString(i_int_SelectedLayoutID) + ".repx";
                return Convert.ToString(view.GetFocusedRowCellValue("ReportLayoutName"));
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            if (!iBool_AllowEdit) return;
            EditRecord();
        }
        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }
        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion
     
   

        void form_Shown(object sender, EventArgs e)
        {
            if (loadingForm != null) loadingForm.Close();
        }

        void ReportBuilder_Shown(object sender, EventArgs e)
        {
            //if (loadingForm != null) loadingForm.Close();
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        void panel_ComponentAdded(object sender, ComponentEventArgs e)
        {
            if (e.Component is XRTableCell)// && FormLoaded)
            {
                //((XRTableCell)e.Component).Font = new Font("Arial", 12, FontStyle.Bold);
            }
            if (e.Component is XRLabel)
            {
                //((XRLabel)e.Component).PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);

            }
        }
       
        private void AdjustEventHandlers(XtraReport ActiveReport)
        {
            foreach (Band b in ActiveReport.Bands)
            {
                foreach (XRControl c in b.Controls)
                {
                    if (c is XRTable)
                    {
                        XRTable t = (XRTable)c;
                        foreach (XRControl row in t.Controls)
                        {
                            foreach (XRControl cell in row.Controls)
                            {
                                if (cell.Tag.ToString() != "")
                                {
                                    cell.PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
                                }
                            }
                        }
                    }
                }
            }            
         }

        private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private Boolean SortAscending = false;
        private void My_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting //
            DetailBand db = (DetailBand)rptReport.Bands.GetBandByType(typeof(DetailBand));
            if (db != null)
            {
                if (((XRControl)sender).Tag.ToString() == null) return;

                printingSystem1.Begin();  // Switch redraw off //
                loadingForm = new WaitDialogForm("Sorting Data...", "Report Printing");
                loadingForm.Show();

                db.SortFields.Clear();
                if (currentSortCell != null)
                {
                    currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
                }
                // Create a new field to sort //
                GroupField grField = new GroupField();
                grField.FieldName = ((XRControl)sender).Tag.ToString();
                if (currentSortCell != null)
                {
                    if (currentSortCell.Text == ((XRLabel)sender).Text)
                    {
                        if (SortAscending)
                        {
                            grField.SortOrder = XRColumnSortOrder.Descending;
                            SortAscending = !SortAscending;
                        }
                        else
                        {
                            grField.SortOrder = XRColumnSortOrder.Ascending;
                            SortAscending = !SortAscending;
                        }
                    }
                    else
                    {
                        grField.SortOrder = XRColumnSortOrder.Ascending;
                        SortAscending = true;
                    }
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                    SortAscending = true;
                }
                // Add sorting //
                db.SortFields.Add(grField);
                ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
                currentSortCell = (XRTableCell)sender;
                // Recreate the report document.
                rptReport.CreateDocument();
                loadingForm.Close();
                printingSystem1.End();  // Switch redraw back on //

            }
        }

        /*
                public class SaveCommandHandler : ICommandHandler
                {
                    XRDesignPanel panel;

                    public string strFullPath = "";

                    public string strFileName = "";

                    public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
                    {
                        this.panel = panel;
                        this.strFullPath = strFullPath;
                        this.strFileName = strFileName;
                    }

                    public virtual void HandleCommand(ReportCommand command, object[] args, ref bool handled)
                    {
                        if (!CanHandleCommand(command)) return;
                        Save();  // Save report //
                        handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
                    }

                    public virtual bool CanHandleCommand(ReportCommand command)
                    {
                        // This handler is used for SaveFile, SaveFileAs and Closing commands.
                        return command == ReportCommand.SaveFile ||
                            command == ReportCommand.SaveFileAs ||
                            command == ReportCommand.Closing;
                    }

                    void Save()
                    {
                        // Custom Saving Logic //
                        Boolean blSaved = false;
                        panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                        // Update existing file layout //
                        panel.Report.DataSource = null;
                        panel.Report.DataMember = null;
                        panel.Report.DataAdapter = null;
                        try
                        {
                            panel.Report.SaveLayout(strFullPath + strFileName);
                            blSaved = true;
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            Console.WriteLine(ex.Message);
                            blSaved = false;
                        }
                        if (blSaved)
                        {
                            panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                        }
                    }
                }
        */
        public class SaveCommandHandler : DevExpress.XtraReports.UserDesigner.ICommandHandler
        {
            XRDesignPanel panel;
            public string strFullPath = "";
            public string strFileName = "";

            public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
            {
                this.panel = panel;
                this.strFullPath = strFullPath;
                this.strFileName = strFileName;
            }

            public void HandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, object[] args)
            {
                //if (!CanHandleCommand(command)) return;
                Save();  // Save report //
                //handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
            }

            public bool CanHandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, ref bool useNextHandler)
            {
                useNextHandler = !(command == ReportCommand.SaveFile ||
                    command == ReportCommand.SaveFileAs ||
                    command == ReportCommand.Closing);
                return !useNextHandler;
            }

            void Save()
            {
                Boolean blSaved = false;
                panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                // Update existing file layout //
                panel.Report.DataSource = null;
                panel.Report.DataMember = null;
                panel.Report.DataAdapter = null;
                try
                {
                    panel.Report.SaveLayout(strFullPath + strFileName);
                    blSaved = true;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Console.WriteLine(ex.Message);
                    blSaved = false;
                }
                if (blSaved)
                {
                    panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                }
            }
        }



 


    }
}


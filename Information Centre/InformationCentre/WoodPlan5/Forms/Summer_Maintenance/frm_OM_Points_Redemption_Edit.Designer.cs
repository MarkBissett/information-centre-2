namespace WoodPlan5
{
    partial class frm_OM_Points_Redemption_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Points_Redemption_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataSet_Teams = new WoodPlan5.DataSet_Teams();
            this.sp06808OMTeamRedemptionItemTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp06800OMTeamRedemptionListItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp06800_OM_Team_RedemptionListItemTableAdapter = new WoodPlan5.DataSet_TeamsTableAdapters.sp06800_OM_Team_RedemptionListItemTableAdapter();
            this.sp06808_OM_Team_RedemptionItemTypeTableAdapter = new WoodPlan5.DataSet_TeamsTableAdapters.sp06808_OM_Team_RedemptionItemTypeTableAdapter();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.ItemTypeIDLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.ReferenceCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NumberOfPointsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.PointsRatioSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForItemTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNumberOfPoints = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPointsRatio = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReferenceCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForItemDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Teams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06808OMTeamRedemptionItemTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06800OMTeamRedemptionListItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ItemTypeIDLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReferenceCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumberOfPointsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PointsRatioSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForItemTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNumberOfPoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPointsRatio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReferenceCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForItemDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 31);
            this.barDockControlTop.Size = new System.Drawing.Size(808, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 528);
            this.barDockControlBottom.Size = new System.Drawing.Size(808, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 31);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 497);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(808, 31);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 497);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(808, 31);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 528);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(808, 27);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 31);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 497);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(808, 31);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 497);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // dataSet_Teams
            // 
            this.dataSet_Teams.DataSetName = "DataSet_Teams";
            this.dataSet_Teams.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp06808OMTeamRedemptionItemTypeBindingSource
            // 
            this.sp06808OMTeamRedemptionItemTypeBindingSource.DataMember = "sp06808_OM_Team_RedemptionItemType";
            this.sp06808OMTeamRedemptionItemTypeBindingSource.DataSource = this.dataSet_Teams;
            // 
            // sp06800OMTeamRedemptionListItemBindingSource
            // 
            this.sp06800OMTeamRedemptionListItemBindingSource.DataMember = "sp06800_OM_Team_RedemptionListItem";
            this.sp06800OMTeamRedemptionListItemBindingSource.DataSource = this.dataSet_Teams;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp06800_OM_Team_RedemptionListItemTableAdapter
            // 
            this.sp06800_OM_Team_RedemptionListItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp06808_OM_Team_RedemptionItemTypeTableAdapter
            // 
            this.sp06808_OM_Team_RedemptionItemTypeTableAdapter.ClearBeforeFill = true;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.ItemTypeIDLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ReferenceCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NumberOfPointsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.PointsRatioSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ItemDescriptionTextEdit);
            this.dataLayoutControl1.DataSource = this.sp06800OMTeamRedemptionListItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 31);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(700, 468, 650, 400);
            this.dataLayoutControl1.OptionsView.UseDefaultDragAndDropRendering = false;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(808, 497);
            this.dataLayoutControl1.TabIndex = 13;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ItemTypeIDLookUpEdit
            // 
            this.ItemTypeIDLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06800OMTeamRedemptionListItemBindingSource, "ItemTypeID", true));
            this.ItemTypeIDLookUpEdit.Location = new System.Drawing.Point(99, 12);
            this.ItemTypeIDLookUpEdit.MenuManager = this.barManager1;
            this.ItemTypeIDLookUpEdit.Name = "ItemTypeIDLookUpEdit";
            this.ItemTypeIDLookUpEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ItemTypeIDLookUpEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemTypeIDLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ItemTypeIDLookUpEdit.Properties.DataSource = this.sp06808OMTeamRedemptionItemTypeBindingSource;
            this.ItemTypeIDLookUpEdit.Properties.DisplayMember = "ItemType";
            this.ItemTypeIDLookUpEdit.Properties.NullText = "";
            this.ItemTypeIDLookUpEdit.Properties.ValueMember = "ItemTypeID";
            this.ItemTypeIDLookUpEdit.Size = new System.Drawing.Size(302, 20);
            this.ItemTypeIDLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ItemTypeIDLookUpEdit.TabIndex = 4;
            this.ItemTypeIDLookUpEdit.EditValueChanged += new System.EventHandler(this.ItemTypeIDLookUpEdit_EditValueChanged);
            this.ItemTypeIDLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ItemTypeIDLookUpEdit_Validating);
            // 
            // ReferenceCodeTextEdit
            // 
            this.ReferenceCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06800OMTeamRedemptionListItemBindingSource, "ReferenceCode", true));
            this.ReferenceCodeTextEdit.Location = new System.Drawing.Point(492, 12);
            this.ReferenceCodeTextEdit.MenuManager = this.barManager1;
            this.ReferenceCodeTextEdit.Name = "ReferenceCodeTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.ReferenceCodeTextEdit, true);
            this.ReferenceCodeTextEdit.Size = new System.Drawing.Size(304, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ReferenceCodeTextEdit, optionsSpelling1);
            this.ReferenceCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.ReferenceCodeTextEdit.TabIndex = 6;
            this.ReferenceCodeTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ReferenceCodeTextEdit_Validating);
            // 
            // NumberOfPointsSpinEdit
            // 
            this.NumberOfPointsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06800OMTeamRedemptionListItemBindingSource, "NumberOfPoints", true));
            this.NumberOfPointsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.NumberOfPointsSpinEdit.Location = new System.Drawing.Point(99, 36);
            this.NumberOfPointsSpinEdit.MenuManager = this.barManager1;
            this.NumberOfPointsSpinEdit.Name = "NumberOfPointsSpinEdit";
            this.NumberOfPointsSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.NumberOfPointsSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.NumberOfPointsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NumberOfPointsSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.NumberOfPointsSpinEdit.Properties.Mask.EditMask = "N0";
            this.NumberOfPointsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.NumberOfPointsSpinEdit.Size = new System.Drawing.Size(302, 20);
            this.NumberOfPointsSpinEdit.StyleController = this.dataLayoutControl1;
            this.NumberOfPointsSpinEdit.TabIndex = 8;
            this.NumberOfPointsSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.NumberOfPointsSpinEdit_Validating);
            // 
            // PointsRatioSpinEdit
            // 
            this.PointsRatioSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06800OMTeamRedemptionListItemBindingSource, "PointsRatio", true));
            this.PointsRatioSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.PointsRatioSpinEdit.Location = new System.Drawing.Point(492, 36);
            this.PointsRatioSpinEdit.MenuManager = this.barManager1;
            this.PointsRatioSpinEdit.Name = "PointsRatioSpinEdit";
            this.PointsRatioSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.PointsRatioSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.PointsRatioSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PointsRatioSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.PointsRatioSpinEdit.Properties.Mask.EditMask = "N0";
            this.PointsRatioSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.PointsRatioSpinEdit.Size = new System.Drawing.Size(304, 20);
            this.PointsRatioSpinEdit.StyleController = this.dataLayoutControl1;
            this.PointsRatioSpinEdit.TabIndex = 9;
            this.PointsRatioSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.PointsRatioSpinEdit_Validating);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(808, 497);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForItemTypeID,
            this.ItemForNumberOfPoints,
            this.ItemForPointsRatio,
            this.ItemForReferenceCode,
            this.ItemForItemDescription});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(788, 72);
            // 
            // ItemForItemTypeID
            // 
            this.ItemForItemTypeID.Control = this.ItemTypeIDLookUpEdit;
            this.ItemForItemTypeID.Location = new System.Drawing.Point(0, 0);
            this.ItemForItemTypeID.Name = "ItemForItemTypeID";
            this.ItemForItemTypeID.Size = new System.Drawing.Size(393, 24);
            this.ItemForItemTypeID.Text = "Item Type";
            this.ItemForItemTypeID.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForNumberOfPoints
            // 
            this.ItemForNumberOfPoints.Control = this.NumberOfPointsSpinEdit;
            this.ItemForNumberOfPoints.Location = new System.Drawing.Point(0, 24);
            this.ItemForNumberOfPoints.Name = "ItemForNumberOfPoints";
            this.ItemForNumberOfPoints.Size = new System.Drawing.Size(393, 24);
            this.ItemForNumberOfPoints.Text = "Number Of Points";
            this.ItemForNumberOfPoints.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForPointsRatio
            // 
            this.ItemForPointsRatio.Control = this.PointsRatioSpinEdit;
            this.ItemForPointsRatio.Location = new System.Drawing.Point(393, 24);
            this.ItemForPointsRatio.Name = "ItemForPointsRatio";
            this.ItemForPointsRatio.Size = new System.Drawing.Size(395, 24);
            this.ItemForPointsRatio.Text = "Points Ratio";
            this.ItemForPointsRatio.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForReferenceCode
            // 
            this.ItemForReferenceCode.Control = this.ReferenceCodeTextEdit;
            this.ItemForReferenceCode.Location = new System.Drawing.Point(393, 0);
            this.ItemForReferenceCode.Name = "ItemForReferenceCode";
            this.ItemForReferenceCode.Size = new System.Drawing.Size(395, 24);
            this.ItemForReferenceCode.Text = "Reference Code";
            this.ItemForReferenceCode.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemDescriptionTextEdit
            // 
            this.ItemDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06800OMTeamRedemptionListItemBindingSource, "ItemDescription", true));
            this.ItemDescriptionTextEdit.Location = new System.Drawing.Point(99, 60);
            this.ItemDescriptionTextEdit.MenuManager = this.barManager1;
            this.ItemDescriptionTextEdit.Name = "ItemDescriptionTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.ItemDescriptionTextEdit, true);
            this.ItemDescriptionTextEdit.Size = new System.Drawing.Size(697, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ItemDescriptionTextEdit, optionsSpelling2);
            this.ItemDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.ItemDescriptionTextEdit.TabIndex = 10;
            this.ItemDescriptionTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ItemDescriptionTextEdit_Validating);
            // 
            // ItemForItemDescription
            // 
            this.ItemForItemDescription.Control = this.ItemDescriptionTextEdit;
            this.ItemForItemDescription.Location = new System.Drawing.Point(0, 48);
            this.ItemForItemDescription.Name = "ItemForItemDescription";
            this.ItemForItemDescription.Size = new System.Drawing.Size(788, 24);
            this.ItemForItemDescription.StartNewLine = true;
            this.ItemForItemDescription.Text = "Item Description";
            this.ItemForItemDescription.TextSize = new System.Drawing.Size(84, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(788, 405);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // frm_OM_Points_Redemption_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(808, 555);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Points_Redemption_Edit";
            this.Text = "Edit Point Redemption Manager";
            this.Activated += new System.EventHandler(this.frm_OM_Points_Redemption_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Points_Redemption_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Points_Redemption_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Teams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06808OMTeamRedemptionItemTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06800OMTeamRedemptionListItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ItemTypeIDLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReferenceCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumberOfPointsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PointsRatioSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForItemTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNumberOfPoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPointsRatio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReferenceCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForItemDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DataSet_AT dataSet_AT;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_Teams dataSet_Teams;
        private System.Windows.Forms.BindingSource sp06800OMTeamRedemptionListItemBindingSource;
        private DataSet_TeamsTableAdapters.sp06800_OM_Team_RedemptionListItemTableAdapter sp06800_OM_Team_RedemptionListItemTableAdapter;
        private System.Windows.Forms.BindingSource sp06808OMTeamRedemptionItemTypeBindingSource;
        private DataSet_TeamsTableAdapters.sp06808_OM_Team_RedemptionItemTypeTableAdapter sp06808_OM_Team_RedemptionItemTypeTableAdapter;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.LookUpEdit ItemTypeIDLookUpEdit;
        private DevExpress.XtraEditors.TextEdit ReferenceCodeTextEdit;
        private DevExpress.XtraEditors.SpinEdit NumberOfPointsSpinEdit;
        private DevExpress.XtraEditors.SpinEdit PointsRatioSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForItemTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReferenceCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNumberOfPoints;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPointsRatio;
        private DevExpress.XtraEditors.TextEdit ItemDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForItemDescription;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraLayout;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Site_Contract_Utilities_InActive : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";

        public int intCancelVisits = 1;
        public string strRemarks = "";
        public DateTime dtEndDate = DateTime.Now;

        #endregion

        public frm_OM_Site_Contract_Utilities_InActive()
        {
            InitializeComponent();
        }

        private void frm_OM_Site_Contract_Utilities_InActive_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500264;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            checkEdit1.EditValue = 1;
            RemarksMemoEdit.EditValue = "";
            dateEdit1.DateTime = DateTime.Now;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            dxErrorProvider1.ClearErrors();
            this.ValidateChildren();
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void dateEdit1_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (de.EditValue == null || string.IsNullOrEmpty(de.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(dateEdit1, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(dateEdit1, "");
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, LayoutControl Layoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                //layoutControl1.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = Layoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        Layoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        Layoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, layoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the screen!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Mark Callout as Don't Pay", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            intCancelVisits = Convert.ToInt32(checkEdit1.EditValue);
            strRemarks = RemarksMemoEdit.EditValue.ToString();
            dtEndDate = dateEdit1.DateTime;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }






    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;  // Required for Path Statement //
using System.Xml;
using System.Linq;
using System.Xml.Linq;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;
using DevExpress.XtraTab;
using DevExpress.XtraBars.Docking;

using DevExpress.Utils.Menu;  // Required to disable Column Chooser on Grids //
using DevExpress.XtraGrid.Localization;  // Required to disable Column Chooser on Grids //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using LocusEffects;
using WoodPlan5.Classes.Operations;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_OM_Client_Contract_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_TemplateManager = false;
        bool iBool_BillingRequirementTemplateManager = false;

        private Utils.enmFocusedGrid _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;

        bool iBool_AddWizardButtonEnabled = false;
        bool iBool_ImportButtonEnabled = false;
        bool iBool_EnableGridColumnChooser = true;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewStateContract;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateYear;
        public RefreshGridState RefreshGridViewStateProfile;
        public RefreshGridState RefreshGridViewStateResponsibility;
        public RefreshGridState RefreshGridViewStateClientPO;
        public RefreshGridState RefreshGridViewStateBillingRequirement;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDsContract = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDsYear = "";
        string i_str_AddedRecordIDsProfile = "";
        string i_str_AddedRecordIDsResponsibility = "";
        string i_str_AddedRecordIDsClientPO = "";
        string i_str_AddedRecordIDsBillingRequirement = "";

        string i_str_selected_ContractType_ids = "";
        string i_str_selected_ContractType_names = "";
        BaseObjects.GridCheckMarksSelection selection1;
        
        string i_str_selected_ContractStatus_ids = "";
        string i_str_selected_ContractStatus_names = "";
        BaseObjects.GridCheckMarksSelection selection2;

        string i_str_selected_KAM_ids = "";
        string i_str_selected_KAM_names = "";
        BaseObjects.GridCheckMarksSelection selection5;

        string i_str_selected_Construction_Manager_ids = "";
        string i_str_selected_Construction_Manager_names = "";
        BaseObjects.GridCheckMarksSelection selection3;

        string i_str_selected_Contract_Director_ids = "";
        string i_str_selected_Contract_Director_names = "";
        BaseObjects.GridCheckMarksSelection selection4;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string i_str_TenderRequestNotesFileFolder = "";

        public string strPassedInDrillDownIDs = "";

        Bitmap bmpBlank = null;
        Bitmap bmpAlert = null;
        
        private DateTime i_dtStart = DateTime.MinValue;
        private DateTime i_dtEnd = DateTime.MaxValue;

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        private int intBillingRequirementLinkedRecordTypeID = 1;  // 1 = Client Contracts //

        #endregion

        public frm_OM_Client_Contract_Manager()
        {
            InitializeComponent();
        }

        private void frm_OM_Client_Contract_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 7006;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();
            
            Set_Grid_Highlighter_Transparent(this.Controls);

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            bmpBlank = new Bitmap(16, 16);
            bmpAlert = new Bitmap(imageCollection1.Images[6], 16, 16);

            #region Get Default Paths
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_PictureFilesFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Job Pictures (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Linked Job Pictures Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            try
            {
                i_str_TenderRequestNotesFileFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_CMTenderRequestNotesFileFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Saved CM Tender Request Note Files (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Saved CM Tender Request Note Files Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!i_str_TenderRequestNotesFileFolder.EndsWith("\\")) i_str_TenderRequestNotesFileFolder += "\\";  // Add Backslash to end //
            #endregion

            sp06052_OM_Client_Contract_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateContract = new RefreshGridState(gridViewContract, "ClientContractID");

            sp06050_OM_Contract_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06050_OM_Contract_TypesTableAdapter.Fill(dataSet_OM_Contract.sp06050_OM_Contract_Types);
            gridControl1.ForceInitialize();
            // Add record selection checkboxes to popup Callout Type grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            sp06051_OM_Contract_StatusesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06051_OM_Contract_StatusesTableAdapter.Fill(dataSet_OM_Contract.sp06051_OM_Contract_Statuses);
            gridControl2.ForceInitialize();
            // Add record selection checkboxes to popup Callout Type grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;


            // Add record selection checkboxes to popup KAM Filter grid control //
            selection5 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection5.CheckMarkColumn.VisibleIndex = 0;
            selection5.CheckMarkColumn.Width = 30;
            sp06019_OM_Job_Manager_KAMs_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06019_OM_Job_Manager_KAMs_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06019_OM_Job_Manager_KAMs_Filter);
            gridControl5.ForceInitialize();

            // Add record selection checkboxes to popup Construction Manager Filter grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;
            sp06388_OM_Client_Contract_Manager_Construction_Manager_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06388_OM_Client_Contract_Manager_Construction_Manager_FilterTableAdapter.Fill(dataSet_OM_Contract.sp06388_OM_Client_Contract_Manager_Construction_Manager_Filter);
            gridControl3.ForceInitialize();

            // Add record selection checkboxes to popup Contract Director Filter grid control //
            selection4 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl4.MainView);
            selection4.CheckMarkColumn.VisibleIndex = 0;
            selection4.CheckMarkColumn.Width = 30;
            sp06389_OM_Client_Contract_Manager_Contract_Director_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06389_OM_Client_Contract_Manager_Contract_Director_FilterTableAdapter.Fill(dataSet_OM_Contract.sp06389_OM_Client_Contract_Manager_Contract_Director_Filter);
            gridControl4.ForceInitialize();

            i_dtStart = DateTime.Today.AddMonths(-6);
            i_dtEnd = DateTime.Today.AddMonths(6);
            dateEditFromDate.DateTime = i_dtStart;
            dateEditToDate.DateTime = i_dtEnd;
            popupContainerEditDateRange.EditValue = PopupContainerEditDateRange_Get_Selected();

            sp06055_OM_Client_Contract_Manager_Linked_YearsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateYear = new RefreshGridState(gridViewYear, "ClientContractYearID");

            sp06056_OM_Client_Contract_Manager_Linked_ProfilesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateProfile = new RefreshGridState(gridViewProfile, "ClientContractProfileID");

            sp06057_OM_Client_Contract_Manager_Linked_ResponsibilitiesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateResponsibility = new RefreshGridState(gridViewResponsibility, "PersonResponsibilityID");

            sp06326_OM_Client_POs_Linked_To_ParentTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateClientPO = new RefreshGridState(gridViewClientPO, "ClientPurchaseOrderLinkID");

            sp01037_Core_Billing_Requirement_ItemsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateBillingRequirement = new RefreshGridState(gridViewBillingRequirement, "BillingRequirementID");

            if (strPassedInDrillDownIDs != "")  // Opened in drill-down mode //
            {
                popupContainerEditDateRange.EditValue = "Custom Filter";
                popupContainerEditContractType.EditValue = "Custom Filter";
                popupContainerEditContractStatus.EditValue = "Custom Filter";
                popupContainerEditKAMFilter.Text = "Custom Filter";
                popupContainerEditConstructionManager.Text = "Custom Filter";
                popupContainerEditContractDirector.Text = "Custom Filter";
                Load_Data();  // Load records //
            }
            popupContainerControlContractTypeFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlContractStatusFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlDateRange.Size = new System.Drawing.Size(200, 107);
            popupContainerControlKAMFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlConstructionManagerFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlContractDirectorFilter.Size = new System.Drawing.Size(270, 400);

            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
        }

        private void frm_OM_Client_Contract_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDsContract))
                {
                    Load_Data();
                }
                if (UpdateRefreshStatus == 2 || !string.IsNullOrEmpty(i_str_AddedRecordIDsYear) || !string.IsNullOrEmpty(i_str_AddedRecordIDsResponsibility) || !string.IsNullOrEmpty(i_str_AddedRecordIDsClientPO) || !string.IsNullOrEmpty(i_str_AddedRecordIDsBillingRequirement))
                {
                    LoadLinkedRecords();
                }
                if (UpdateRefreshStatus == 3 || !string.IsNullOrEmpty(i_str_AddedRecordIDsProfile))
                {
                    LoadLinkedRecords2();
                }
            }
            SetMenuStatus();
        }

        private void frm_OM_Client_Contract_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ContractTypeFilter", i_str_selected_ContractType_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ContractStatusFilter", i_str_selected_ContractStatus_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ActiveOnly", checkEditActiveOnly.EditValue.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "KAMFilter", i_str_selected_KAM_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ConstructionManagerFilter", i_str_selected_Construction_Manager_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ContractDirectorFilter", i_str_selected_Contract_Director_ids);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }


        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                int intFoundRow = 0;
                string strItemFilter = "";

                // Contract Type Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("ContractTypeFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl1.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditContractType.EditValue = PopupContainerEditContractTypeFilter_Get_Selected();
                }

                // Contract Status Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("ContractStatusFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl2.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditContractStatus.EditValue = PopupContainerEditContractStatusFilter_Get_Selected();
                }

                // Active Only //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("ActiveOnly");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    checkEditActiveOnly.EditValue = (strItemFilter == "1" ? 1 : 0);
                }

                // KAM Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("KAMFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl5.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditKAMFilter.Text = PopupContainerEditKAMFilter_Get_Selected();
                }

                // Construction Manager Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("ConstructionManagerFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl3.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditConstructionManager.Text = PopupContainerEditConstructionManagerFilter_Get_Selected();
                }

                // Contract Director Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("ContractDirectorFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl4.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditContractDirector.Text = PopupContainerEditContractDirector_Get_Selected();
                }

                Load_Data();

                // Prepare LocusEffects and add custom effect //
                locusEffectsProvider1 = new LocusEffectsProvider();
                locusEffectsProvider1.Initialize();
                locusEffectsProvider1.FramesPerSecond = 30;
                m_customArrowLocusEffect1 = new ArrowLocusEffect();
                m_customArrowLocusEffect1.Name = "CustomeArrow1";
                m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                m_customArrowLocusEffect1.MovementCycles = 20;
                m_customArrowLocusEffect1.MovementAmplitude = 200;
                m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                m_customArrowLocusEffect1.LeadInTime = 0; //msec
                m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                Point location = dockPanelFilters.PointToScreen(Point.Empty);

                System.Drawing.Point screenPoint = new System.Drawing.Point(location.X + 10, location.Y + 5);
                locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
            }
        }
        private System.Drawing.Rectangle GetLinksScreenRect(BarItemLink link)
        {
            System.Reflection.PropertyInfo info = typeof(BarItemLink).GetProperty("BarControl", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            Control c = (Control)info.GetValue(link, null);
            return c.RectangleToScreen(link.Bounds);
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            Load_Data();
        }

        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Client Contracts...");
            }

            if (i_str_selected_ContractStatus_ids == null) i_str_selected_ContractStatus_ids = "";
            int intActive = Convert.ToInt32(checkEditActiveOnly.EditValue);
            RefreshGridViewStateContract.SaveViewInfo();
            GridView view = (GridView)gridControlContract.MainView;
            view.BeginUpdate();
            try
            {
                if (popupContainerEditContractType.EditValue.ToString() == "Custom Filter" && strPassedInDrillDownIDs != "")  // Load passed in Callouts //
                {
                    sp06052_OM_Client_Contract_ManagerTableAdapter.Fill(this.dataSet_OM_Contract.sp06052_OM_Client_Contract_Manager, strPassedInDrillDownIDs, i_dtStart, i_dtEnd, "", "", 0, "", "", "");
                    this.RefreshGridViewStateContract.LoadViewInfo();  // Reload any expanded groups and selected rows //
                    view.ExpandAllGroups();
                }
                else // Load users selection //
                {
                    sp06052_OM_Client_Contract_ManagerTableAdapter.Fill(this.dataSet_OM_Contract.sp06052_OM_Client_Contract_Manager, "", i_dtStart, i_dtEnd, i_str_selected_ContractType_ids, i_str_selected_ContractStatus_ids, intActive, i_str_selected_KAM_ids, i_str_selected_Construction_Manager_ids, i_str_selected_Contract_Director_ids);
                    this.RefreshGridViewStateContract.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Client Contract Data.\n\nMessage = [" + ex.Message + "].\n\nIf the error is a result of a query timeout, try adjusting the parameters at the top of the screen to reduce the amount of data returned then try again. If the problem persists, contact Technical Support.", "Load Client Contract", MessageBoxButtons.OK, MessageBoxIcon.Stop);

            }
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsContract != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDsContract.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ClientContractID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsContract = "";
                view.EndSelection();
            }

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void LoadLinkedRecords()
        {
            if (splitContainerControl1.Collapsed) return;  // Don't bother loading related data as the parent panel is collapsed so the grids are invisible //

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControlContract.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ClientContractID"])) + ',';
            }
            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            gridControlYear.MainView.BeginUpdate();
            RefreshGridViewStateYear.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Contract.sp06055_OM_Client_Contract_Manager_Linked_Years.Clear();
            }
            else
            {
                sp06055_OM_Client_Contract_Manager_Linked_YearsTableAdapter.Fill(dataSet_OM_Contract.sp06055_OM_Client_Contract_Manager_Linked_Years, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                RefreshGridViewStateYear.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlYear.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsYear != "")
            {
                strArray = i_str_AddedRecordIDsYear.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlYear.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ClientContractYearID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsYear = "";
                view.EndSelection();
            }


            gridControlResponsibility.MainView.BeginUpdate();
            RefreshGridViewStateResponsibility.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Contract.sp06057_OM_Client_Contract_Manager_Linked_Responsibilities.Clear();
            }
            else
            {
                sp06057_OM_Client_Contract_Manager_Linked_ResponsibilitiesTableAdapter.Fill(dataSet_OM_Contract.sp06057_OM_Client_Contract_Manager_Linked_Responsibilities, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                RefreshGridViewStateResponsibility.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlResponsibility.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsResponsibility != "")
            {
                strArray = i_str_AddedRecordIDsResponsibility.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlResponsibility.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["PersonResponsibilityID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsResponsibility = "";
                view.EndSelection();
            }


            gridControlClientPO.MainView.BeginUpdate();
            RefreshGridViewStateClientPO.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Client_PO.sp06326_OM_Client_POs_Linked_To_Parent.Clear();
            }
            else
            {
                sp06326_OM_Client_POs_Linked_To_ParentTableAdapter.Fill(dataSet_OM_Client_PO.sp06326_OM_Client_POs_Linked_To_Parent, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 0);
                RefreshGridViewStateClientPO.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlClientPO.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsClientPO != "")
            {
                strArray = i_str_AddedRecordIDsClientPO.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlClientPO.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ClientPurchaseOrderLinkID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsClientPO = "";
                view.EndSelection();
            }

            gridControlBillingRequirement.MainView.BeginUpdate();
            RefreshGridViewStateBillingRequirement.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_Common_Functionality.sp01037_Core_Billing_Requirement_Items.Clear();
            }
            else
            {
                sp01037_Core_Billing_Requirement_ItemsTableAdapter.Fill(dataSet_Common_Functionality.sp01037_Core_Billing_Requirement_Items, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), intBillingRequirementLinkedRecordTypeID);
                RefreshGridViewStateBillingRequirement.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlBillingRequirement.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsBillingRequirement != "")
            {
                strArray = i_str_AddedRecordIDsBillingRequirement.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlBillingRequirement.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["BillingRequirementID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsBillingRequirement = "";
                view.EndSelection();
            }
        }

        private void LoadLinkedRecords2()
        {
            if (splitContainerControl1.Collapsed) return;  // Don't bother loading related data as the parent panel is collapsed so the grids are invisible //

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControlYear.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ClientContractYearID"])) + ',';
            }
            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            gridControlProfile.MainView.BeginUpdate();
            RefreshGridViewStateProfile.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Contract.sp06056_OM_Client_Contract_Manager_Linked_Profiles.Clear();
            }
            else
            {
                sp06056_OM_Client_Contract_Manager_Linked_ProfilesTableAdapter.Fill(dataSet_OM_Contract.sp06056_OM_Client_Contract_Manager_Linked_Profiles, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                RefreshGridViewStateProfile.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlProfile.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsProfile != "")
            {
                strArray = i_str_AddedRecordIDsProfile.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlProfile.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ClientContractProfileID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsProfile = "";
                view.EndSelection();
            }

        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            Load_Data();
        }

        public void UpdateFormRefreshStatus(int status, Utils.enmFocusedGrid grid, string newIds)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            switch (grid)
            {
                case Utils.enmFocusedGrid.Contract:
                    i_str_AddedRecordIDsContract = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsContract : newIds);
                    break;
                case Utils.enmFocusedGrid.ContractYear:
                    i_str_AddedRecordIDsYear = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsYear : newIds);
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    i_str_AddedRecordIDsProfile = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsProfile : newIds);
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    i_str_AddedRecordIDsResponsibility = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsResponsibility : newIds);
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    i_str_AddedRecordIDsClientPO = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsClientPO : newIds);
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    i_str_AddedRecordIDsBillingRequirement = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsBillingRequirement : newIds);
                    break;
                default:
                    break;
            }
        }


        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                    case 1:  // Add Wizard Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_AddWizardButtonEnabled = true;
                        }
                        break;
                    case 2:  // Import Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_ImportButtonEnabled = true;
                        }
                        break;
                }
            }
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, 7008, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);  // Billing Templates //
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        iBool_TemplateManager = sfpPermissions.blRead;
                        break;
                }
            }
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, 7018, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);  // 7018 //
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        iBool_BillingRequirementTemplateManager = sfpPermissions.blRead;
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = (GridView)gridControlContract.MainView;
            GridView ParentView = (GridView)gridControlContract.MainView;
            GridView ChildParentView = (GridView)gridControlYear.MainView;

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    view = (GridView)gridControlContract.MainView;
                    break;
                case Utils.enmFocusedGrid.ContractYear:
                    view = (GridView)gridControlYear.MainView;
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    view = (GridView)gridControlProfile.MainView;
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    view = (GridView)gridControlResponsibility.MainView;
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    view = (GridView)gridControlClientPO.MainView;
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    view = (GridView)gridControlBillingRequirement.MainView;
                    break;
                default: 
                    break;
            }
            int[] intRowHandles = view.GetSelectedRows();
            int[] intParentRowHandles = ParentView.GetSelectedRows();
            int[] intChildParentRowHandles = ChildParentView.GetSelectedRows();

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                case Utils.enmFocusedGrid.ContractYear:
                case Utils.enmFocusedGrid.ContractYearProfile:
                case Utils.enmFocusedGrid.PersonResponsibility:
                case Utils.enmFocusedGrid.ClientPOLink:
                case Utils.enmFocusedGrid.BillingRequirement:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                default:
                    break;
            }
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        bbiBlockAdd.Enabled = false;
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYear:
                case Utils.enmFocusedGrid.PersonResponsibility:
                case Utils.enmFocusedGrid.ClientPOLink:
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        bbiBlockAdd.Enabled = (iBool_AllowAdd && intParentRowHandles.Length > 1);
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    {
                        bbiBlockAdd.Enabled = (iBool_AllowAdd && intChildParentRowHandles.Length > 1);
                    }
                    break;
            }
            bsiAuditTrail.Enabled = (intRowHandles.Length > 0);
            bbiViewAuditTrail.Enabled = (intRowHandles.Length > 0);


            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of navigator custom buttons //
            gridControlContract.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlContract.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intParentRowHandles.Length > 0);
            gridControlContract.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intParentRowHandles.Length > 0);
            gridControlContract.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intParentRowHandles.Length > 0);
            gridControlContract.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intParentRowHandles.Length == 1);

            view = (GridView)gridControlYear.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlYear.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlYear.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlYear.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlYear.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlYear.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1);

            view = (GridView)gridControlProfile.MainView;
            intRowHandles = view.GetSelectedRows();
            ChildParentView = (GridView)gridControlYear.MainView;
            intChildParentRowHandles = ChildParentView.GetSelectedRows();
            gridControlProfile.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlProfile.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlProfile.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlProfile.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlProfile.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (iBool_AllowAdd && intChildParentRowHandles.Length > 0);
            gridControlProfile.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = (iBool_TemplateManager);

            view = (GridView)gridControlResponsibility.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlResponsibility.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlResponsibility.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlResponsibility.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlResponsibility.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            
            view = (GridView)gridControlClientPO.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlClientPO.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlClientPO.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlClientPO.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlClientPO.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);

            view = (GridView)gridControlBillingRequirement.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (iBool_AllowAdd);
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = (iBool_BillingRequirementTemplateManager);

            // Set Enabled Status of Main Toolbar Buttons //
            bbiAddWizard.Enabled = iBool_AddWizardButtonEnabled;
            bbiImport.Enabled = iBool_ImportButtonEnabled;
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            System.Reflection.MethodInfo method = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Client_Contract_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYear:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlYear.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Client_Contract_Year_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlContract.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Contract.sp06052_OM_Client_Contract_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.ClientContractID;
                            fChildForm.strLinkedToRecordDesc = row.ClientName;
                            fChildForm.strLinkedToRecordDesc = "Client: " + ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString()
                                                                + " - " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "ContractDescription"))) ? "Unknown Description" : ParentView.GetRowCellValue(intRowHandles[0], "ContractDescription").ToString());
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlProfile.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Client_Contract_Year_Billing_Profile_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlYear.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Contract.sp06055_OM_Client_Contract_Manager_Linked_YearsRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.ClientContractYearID;
                            fChildForm.strLinkedToRecordDesc = HtmlRemoval.StripTagsCharArray(row.LinkedToParent) + ", Year: "
                                                                + (string.IsNullOrWhiteSpace(ParentView.GetRowCellValue(intRowHandles[0], "StartDate").ToString()) ? "Unknown Start" : Convert.ToDateTime(ParentView.GetRowCellValue(intRowHandles[0], "StartDate")).ToString("dd/MM/yyyy")) + " - "
                                                                + (string.IsNullOrWhiteSpace(ParentView.GetRowCellValue(intRowHandles[0], "EndDate").ToString()) ? "Unknown End" : Convert.ToDateTime(ParentView.GetRowCellValue(intRowHandles[0], "EndDate")).ToString("dd/MM/yyyy"));

                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlResponsibility.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Client_Contract_Person_Responsibility_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.i_strPassedInRepsonsibilityTypes = "11,12,31,32,41,42,901";
                        fChildForm.i_intPassedInRecordTypeID = 2;  // Client //
                        fChildForm.i_intPassedInPersonTypeID = 0;
                        fChildForm.i_strPassedInPersonTypeDescription = "Staff";

                        ParentView = (GridView)gridControlContract.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Contract.sp06052_OM_Client_Contract_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.ClientContractID;
                            fChildForm.strLinkedToRecordDesc = row.ClientName;
                            fChildForm.strLinkedToRecordDesc = "Client: " + ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString() + ", Duration: "
                                                                 + " - " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "ContractDescription"))) ? "Unknown Description" : ParentView.GetRowCellValue(intRowHandles[0], "ContractDescription").ToString());
                                //+(string.IsNullOrWhiteSpace(ParentView.GetRowCellValue(intRowHandles[0], "StartDate").ToString()) ? "Unknown Start" : Convert.ToDateTime(ParentView.GetRowCellValue(intRowHandles[0], "StartDate")).ToString("dd/MM/yyyy")) + " - "
                                //+ (string.IsNullOrWhiteSpace(ParentView.GetRowCellValue(intRowHandles[0], "EndDate").ToString()) ? "Unknown End" : Convert.ToDateTime(ParentView.GetRowCellValue(intRowHandles[0], "EndDate")).ToString("dd/MM/yyyy"));
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlClientPO.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Client_PO_Link_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm._LinkedToRecordTypeID = 0;
                        fChildForm._LinkedToRecordType = "Client Contract";

                        ParentView = (GridView)gridControlContract.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Contract.sp06052_OM_Client_Contract_ManagerRow)rowView.Row;
                            fChildForm._ClientID = row.ClientID;
                            fChildForm._LinkedToRecordID = row.ClientContractID;
                            fChildForm._LinkedToRecord = "Client: " + row.ClientName + ", Contract: " + row.ContractDescription;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlBillingRequirement.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_Core_Billing_Requirement_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.LinkedToRecordTypeID = 1;
                        fChildForm.LinkedToRecordType = "Client Contract";
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlContract.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Contract.sp06052_OM_Client_Contract_ManagerRow)rowView.Row;
                            fChildForm.ParentRecordId = row.ClientContractID;
                            fChildForm.ParentRecordDescription = "Client: " + ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString()
                                                                + ", Contract: " + (String.IsNullOrEmpty(Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "ContractDescription"))) ? "Unknown Description" : ParentView.GetRowCellValue(intRowHandles[0], "ContractDescription").ToString());
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Add()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.ContractYear:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractID")) + ',';
                        }

                        var fChildForm = new frm_OM_Client_Contract_Year_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlYear.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractYearID")) + ',';
                        }

                        var fChildForm = new frm_OM_Client_Contract_Year_Billing_Profile_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractID")) + ',';
                        }

                        var fChildForm = new frm_OM_Client_Contract_Person_Responsibility_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.i_strPassedInRepsonsibilityTypes = "11,12,31,32,41,42,901";
                        fChildForm.i_intPassedInRecordTypeID = 2;  // Client //
                        fChildForm.i_intPassedInPersonTypeID = 0;
                        fChildForm.i_strPassedInPersonTypeDescription = "Staff";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one parent Contract record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        int intClientID = 0;
                        string strClientName = "";
                        int intLastClientID = 0;
                        bool boolMultipleClients = false;
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractID")) + ',';
                            intClientID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ClientID"));
                            strClientName = Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientName"));
                            if (intLastClientID == 0) intLastClientID = intClientID;
                            if (intLastClientID != intClientID) boolMultipleClients = true;
                        }

                        var fChildForm = new frm_OM_Client_PO_Link_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm._LinkedToRecordTypeID = 0;
                        fChildForm._LinkedToRecordType = "Client Contract";
                        fChildForm._ClientID = (!boolMultipleClients ? intClientID : 0);
                        fChildForm._ClientName = (!boolMultipleClients ? strClientName : "");
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one parent Contract record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                         foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractID")) + ',';
                        }

                         var fChildForm = new frm_Core_Billing_Requirement_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.LinkedToRecordTypeID = 1;
                        fChildForm.LinkedToRecordType = "Client Contract";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractID")) + ',';
                        }

                        var fChildForm = new frm_OM_Client_Contract_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYear:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlYear.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractYearID")) + ',';
                        }

                        var fChildForm = new frm_OM_Client_Contract_Year_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlProfile.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractProfileID")) + ',';
                        }

                        var fChildForm = new frm_OM_Client_Contract_Year_Billing_Profile_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlResponsibility.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PersonResponsibilityID")) + ',';
                        }

                        var fChildForm = new frm_OM_Client_Contract_Person_Responsibility_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.i_strPassedInRepsonsibilityTypes = "11,12,31,32,41,42,901";
                        fChildForm.i_intPassedInRecordTypeID = 2;  // Client //
                        fChildForm.i_intPassedInPersonTypeID = 0;  // Staff //
                        fChildForm.i_strPassedInPersonTypeDescription = "Staff";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlClientPO.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientPurchaseOrderLinkID")) + ',';
                        }

                        var fChildForm = new frm_OM_Client_PO_Link_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm._LinkedToRecordTypeID = 0;
                        fChildForm._LinkedToRecordType = "Client Contract";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlBillingRequirement.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "BillingRequirementID")) + ',';
                        }

                        var fChildForm = new frm_Core_Billing_Requirement_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.LinkedToRecordTypeID = 1;
                        fChildForm.LinkedToRecordType = "Client Contract";
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractID")) + ',';
                        }

                        var fChildForm = new frm_OM_Client_Contract_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYear:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlYear.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractYearID")) + ',';
                        }

                        var fChildForm = new frm_OM_Client_Contract_Year_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlProfile.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractProfileID")) + ',';
                        }

                        var fChildForm = new frm_OM_Client_Contract_Year_Billing_Profile_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlResponsibility.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PersonResponsibilityID")) + ',';
                        }

                        var fChildForm = new frm_OM_Client_Contract_Person_Responsibility_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.i_strPassedInRepsonsibilityTypes = "11,12,31,32,41,42,901";
                        fChildForm.i_intPassedInRecordTypeID = 2;  // Client //
                        fChildForm.i_intPassedInPersonTypeID = 0;  // Staff //
                        fChildForm.i_strPassedInPersonTypeDescription = "Staff";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlClientPO.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientPurchaseOrderLinkID")) + ',';
                        }

                        var fChildForm = new frm_OM_Client_PO_Link_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm._LinkedToRecordTypeID = 0;
                        fChildForm._LinkedToRecordType = "Client Contract";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlBillingRequirement.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "BillingRequirementID")) + ',';
                        }

                        var fChildForm = new frm_Core_Billing_Requirement_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.LinkedToRecordTypeID = 1;
                        fChildForm.LinkedToRecordType = "Client Contract";
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            string strRecordIDs = "";
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlContract.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Contracts to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Contract" : Convert.ToString(intRowHandles.Length) + " Contracts") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Contract" : "these Contracts") + " will no longer be available for selection and any related records will also be deleted!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "ClientContractID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("client_contract", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Data();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYear:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlYear.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Contract Years to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Contract Year" : Convert.ToString(intRowHandles.Length) + " Linked Contract Years") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Contract Year" : "these Linked Contract Years") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "ClientContractYearID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("client_contract_year", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlProfile.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Billing Profiles to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Billing Profile" : Convert.ToString(intRowHandles.Length) + " Linked Billing Profiles") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Billing Profile" : "these Linked Billing Profiles") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "ClientContractProfileID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("client_contract_year_billing_profile", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords2();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlResponsibility.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Person Responsibilities to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Person Responsibility" : Convert.ToString(intRowHandles.Length) + " Linked Person Responsibilities") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Person Responsibility" : "these Linked Person Responsibilities") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "PersonResponsibilityID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("person_responsibility", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlClientPO.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Client Purchase Orders to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Client Purchase Order" : Convert.ToString(intRowHandles.Length) + " Linked Client Purchase Orders") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Client Purchase Order" : "these Linked Client Purchase Orders") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "ClientPurchaseOrderLinkID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("client_po_link", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlBillingRequirement.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Billing Requirements to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Billing Requirement" : Convert.ToString(intRowHandles.Length) + " Linked Billing Requirements") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Billing Requirement" : "these Linked Billing Requirements") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "BillingRequirementID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp01000_Core_Delete("billing_requirement", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            //switch (i_int_FocusedGrid)
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractID")) + ',';
                        }
                        var fChildForm = new frm_OM_Client_Contract_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYear:
                    {
                        view = (GridView)gridControlYear.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractYearID")) + ',';
                        }
                        var fChildForm = new frm_OM_Client_Contract_Year_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    {
                        view = (GridView)gridControlProfile.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractProfileID")) + ',';
                        }
                        var fChildForm = new frm_OM_Client_Contract_Year_Billing_Profile_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    {
                        view = (GridView)gridControlResponsibility.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PersonResponsibilityID")) + ',';
                        }
                        var fChildForm = new frm_OM_Client_Contract_Person_Responsibility_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.i_strPassedInRepsonsibilityTypes = "11,12,31,32,41,42,901";
                        fChildForm.i_intPassedInRecordTypeID = 2;  // Client //
                        fChildForm.i_intPassedInPersonTypeID = 0;  // Staff //
                        fChildForm.i_strPassedInPersonTypeDescription = "Staff";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    {
                        view = (GridView)gridControlClientPO.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientPurchaseOrderLinkID")) + ',';
                        }
                        var fChildForm = new frm_OM_Client_PO_Link_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm._LinkedToRecordTypeID = 0;
                        fChildForm._LinkedToRecordType = "Client Contract";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        view = (GridView)gridControlBillingRequirement.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "BillingRequirementID")) + ',';
                        }
                        var fChildForm = new frm_Core_Billing_Requirement_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.LinkedToRecordTypeID = 1;
                        fChildForm.LinkedToRecordType = "Client Contract";
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }


        public override void OnViewAuditTrail(object sender, EventArgs e)
        {
            View_Audit_Trail();
        }
        private void View_Audit_Trail()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Client_Contract";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYear:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlYear.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractYearID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Client_Contract_Year";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlProfile.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractProfileID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Client_Contract_Year_Billing_Profile";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlResponsibility.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PersonResponsibilityID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Person_Responsibility";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlClientPO.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientPurchaseOrderLinkID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Client_Purchase_Order_Link";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridViewContract":
                    message = "No Client Contracts Available - Adjust any filters and click Refresh button";
                    break;
                case "gridViewYear":
                    message = "No Linked Years Available - Select one or more Client Contracts to view Linked Years";
                    break;
                case "gridViewProfile":
                    message = "No Linked Billing Profiles Available - Select one or more Years to view Linked Billing Profiles";
                    break;
                case "gridViewResponsibility":
                    message = "No Linked Person Responsibilities Available - Select one or more Client Contracts to view Linked Person Responsibilities";
                    break;
                case "gridViewClientPO":
                    message = "No Linked Client POs Available - Select one or more Client Contracts to view Linked Client POs";
                    break;
                case "gridViewBillingRequirement":
                    message = "No Linked Billing Requirements Available - Select one or more Client Contracts to view Linked Billing Requirements";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridViewContract":
                    LoadLinkedRecords();

                    view = (GridView)gridControlYear.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlResponsibility.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlClientPO.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlBillingRequirement.MainView;
                    view.ExpandAllGroups();

                    SetMenuStatus();

                    int intSelectedCount = view.GetSelectedRows().Length;
                    if (intSelectedCount == 0)
                    {
                        bsiSelectedCount.Visibility = BarItemVisibility.Never;
                    }
                    else
                    {
                        bsiSelectedCount.Visibility = BarItemVisibility.Always;
                        bsiSelectedCount.Caption = (intSelectedCount == 1 ? "1 Client Contract Selected" : "<color=red>" + intSelectedCount.ToString() + " Client Contracts</Color> Selected");
                    }
                    break;
                case "gridViewYear":
                    LoadLinkedRecords2();
                    view = (GridView)gridControlProfile.MainView;
                    view.ExpandAllGroups();
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView - Contract

        private void gridControlContract_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = gridViewContract;
                        int intRecordType = 41;  // Client Contract //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientContractID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "ClientName").ToString() + ", Duration: " + (Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "StartDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date") + " - " + (Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "EndDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date");
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewContract_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            /*GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "ClientName")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DoNotInvoiceClient")) == 1)
                {
                    //e.Appearance.BackColor = Color.LightCoral;
                    //e.Appearance.BackColor2 = Color.Red;
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }*/
        }

        private void gridViewContract_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "LinkedContractCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedContractCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedParentContractCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedParentContractCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedSiteContractCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedSiteContractCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedDocumentCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "CMTenderRequestNotesFile":
                        if (string.IsNullOrWhiteSpace(view.GetRowCellValue(e.RowHandle, "CMTenderRequestNotesFile").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                       
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridViewContract_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewContract_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Contract;
            SetMenuStatus();
        }

        private void gridViewContract_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Contract;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewContract_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedContractCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedContractCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedParentContractCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedParentContractCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedSiteContractCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedSiteContractCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) <= 0) e.Cancel = true;
                    break;
                case "CMTenderRequestNotesFile":
                    if (string.IsNullOrWhiteSpace(view.GetFocusedRowCellValue("CMTenderRequestNotesFile").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intClientContractID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientContractID"));
            if (intClientContractID <= 0) return;
            string strRecordIDs = "";

            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedContractCount":
                    {
                        try
                        {
                            string strParameter = intClientContractID.ToString() + ",";
                            var GetValue = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter();
                            GetValue.ChangeConnectionString(strConnectionString);
                            strRecordIDs = GetValue.sp06053_OM_Get_Linked_Client_Contract_IDs(intClientContractID.ToString() + ",").ToString();
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view client contracts linked to this contract [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Client Contracts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_client_contracts");
                    }
                    break;
                case "LinkedParentContractCount":
                    {
                        int intLinkedToPreviousContractID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "LinkedToPreviousContractID"));
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, intLinkedToPreviousContractID.ToString() + ",", "operations_client_contracts");
                    }
                    break;
                case "LinkedSiteContractCount":
                    {
                        try
                        {
                            string strParameter = intClientContractID.ToString() + ",";
                            var GetValue = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter();
                            GetValue.ChangeConnectionString(strConnectionString);
                            strRecordIDs = GetValue.sp06054_OM_Get_Linked_Site_Contract_IDs(intClientContractID.ToString() + ",").ToString();
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view site contracts linked to this contract [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Site Contracts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_site_contracts");
                    }
                    break;
                default:
                    break;
            }
            
        }

        private void repositoryItemHyperLinkEditLinkedDocumentsClientContract_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 41;  // Client Contract //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientContractID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "ClientName").ToString() + ", Duration: " + (Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "StartDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date") + " - " + (Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "EndDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date");
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void repositoryItemHyperLinkEditTenderRequestNotesFile_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "CMTenderRequestNotesFile").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No CM Tender Quote Notes File Linked - unable to proceed.", "View CM Tender Quote Notes File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                if (Path.GetExtension(Path.Combine(i_str_TenderRequestNotesFileFolder + strFile)).ToLower() != ".pdf")
                {
                    System.Diagnostics.Process.Start(Path.Combine(i_str_TenderRequestNotesFileFolder + strFile));
                }
                else
                {
                    if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                    {
                        frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                        fChildForm.strPDFFile = Path.Combine(i_str_TenderRequestNotesFileFolder + strFile);
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.Show();
                    }
                    else
                    {
                        System.Diagnostics.Process.Start(Path.Combine(i_str_TenderRequestNotesFileFolder + strFile));
                    }
                }
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the CM Tender Quote Notes: " + strFile + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View CM Tender Quote Notes File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #region GridView - Year

        private void gridControlYear_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlYear.MainView;
                        int intRecordType = 42;  // Client Contract Years //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientContractYearID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "LinkedToParent").ToString() + ", Contract Year: " + view.GetRowCellValue(view.FocusedRowHandle, "YearDescription").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription); 
                    }
                    break;
                default:
                    break;
            }
        }
       
        private void gridViewYear_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "LinkedDocumentCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridViewYear_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewYear_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.ContractYear;
            SetMenuStatus();
        }

        private void gridViewYear_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.ContractYear;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewYear_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) <= 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }
        
        private void repositoryItemHyperLinkEditLinkedDocumentsClientContractYear_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 42;  // Client Contract Year //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientContractYearID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "LinkedToParent").ToString() + ", Contract Year: " + view.GetRowCellValue(view.FocusedRowHandle, "YearDescription").ToString();
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region GridView - Billing Profile

        private void gridControlProfile_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        /*// Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlBonuses.MainView;
                        int intRecordType = 11;  // Bonuses //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "BonusID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "BonusDueDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription); */
                    }
                    else if ("add_from_template".Equals(e.Button.Tag))
                    {
                        Add_From_Billing_Profile();
                    }
                    else if ("template_manager".Equals(e.Button.Tag))
                    {
                        var fChildForm = new frm_OM_Template_Billing_Profile_Manager();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();
                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                   break;
                default:
                    break;
            }
        }

        private void gridViewProfile_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewProfile_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.ContractYearProfile;
            SetMenuStatus();
        }

        private void gridViewProfile_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.ContractYearProfile;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void Add_From_Billing_Profile()
        {
            GridView parentView = (GridView)gridControlYear.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Contract Years to add the Billing Profile Template to by clicking on them then try again.", "Add Billing Profile From Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intCount > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intCount.ToString() + " Contract Years selected. If you proceed a new Billing Profile will be created for each of these records from the chosen Template.\n\nProceed?", "Add Billing Profile From Template", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }
            GridView view = (GridView)gridControlProfile.MainView;
            view.PostEditor();

            var fChildForm = new frm_OM_Select_Template_Billing_Profile_Header();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                int intTemplateID = fChildForm.intSelectedTemplateHeaderID;
                if (intTemplateID <= 0) return;

                GridControl gridControlTemp = fChildForm.gridControl2;
                GridView viewTemp = (GridView)gridControlTemp.MainView;
                int intRowCount = viewTemp.DataRowCount;
                if (intCount <= 0) return;

                i_str_AddedRecordIDsProfile = "";
                using (var AddRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                {
                    AddRecords.ChangeConnectionString(strConnectionString);
                    try
                    {
                        decimal decValuePercentage = (decimal)0.00;
                        decimal decEstimatedBillAmount = (decimal)0.00;
                        DateTime dtInvoiceDateDue = DateTime.Now; ;
                        int intUnits = 0;
                        int intUnitDescriptor = 0;
                        int intClientContractYearID = 0;

                        foreach (int i in intRowHandles)  // Selected Years //
                        {
                            for (int j = 0; j < intRowCount; j++)  // Template Items //
                            {
                                intClientContractYearID = Convert.ToInt32(parentView.GetRowCellValue(i, "ClientContractYearID"));

                                if (!string.IsNullOrWhiteSpace(parentView.GetRowCellValue(i, "StartDate").ToString()))
                                {
                                    intUnits = Convert.ToInt32(viewTemp.GetRowCellValue(j, "UnitsFromStart"));
                                    intUnitDescriptor = Convert.ToInt32(viewTemp.GetRowCellValue(j, "UnitsFromStartDescriptorID"));
                                    if (intUnits > 0 && intUnitDescriptor > 0)
                                    {
                                        dtInvoiceDateDue = Convert.ToDateTime(parentView.GetRowCellValue(i, "StartDate"));
                                        switch (intUnitDescriptor)
                                        {
                                            case 1:  // Days //
                                                {
                                                    dtInvoiceDateDue = dtInvoiceDateDue.AddDays(intUnits);
                                                }
                                                break;
                                            case 2:  // Weeks //
                                                {
                                                    dtInvoiceDateDue = dtInvoiceDateDue.AddDays(intUnits * 7);
                                                }
                                                break;
                                            case 3:  // Month //
                                                {
                                                    dtInvoiceDateDue = dtInvoiceDateDue.AddMonths(intUnits);
                                                }
                                                break;
                                            case 4:  // Years //
                                                {
                                                    dtInvoiceDateDue = dtInvoiceDateDue.AddYears(intUnits);
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                                decValuePercentage = Convert.ToDecimal(viewTemp.GetRowCellValue(j, "ValuePercentage"));
                                decEstimatedBillAmount = (decValuePercentage > (decimal)0.00 ? Convert.ToDecimal(parentView.GetRowCellValue(i, "ClientValue")) * (decValuePercentage / 100) : (decimal)0.00);

                                i_str_AddedRecordIDsProfile += AddRecords.sp06085_OM_Client_Contract_Year_Billing_Profile_Update(intClientContractYearID, dtInvoiceDateDue, null, decEstimatedBillAmount, (decimal)0.00, 0, null).ToString() + ";";
                            }
                        }
                    }
                    catch (Exception) { }
                }
                LoadLinkedRecords2();
            }
        }

        #endregion


        #region GridView - Person Responsibility

        private void gridControlResponsibility_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewResponsibility_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewResponsibility_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.PersonResponsibility;
            SetMenuStatus();
        }

        private void gridViewResponsibility_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.PersonResponsibility;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView - Client POs

        private void gridControlClientPO_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewClientPO_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "StartingValue")
            {
                decimal decWarningValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "StartingValue"));
                if (decWarningValue <= (decimal)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "WarningValue")
            {
                decimal decWarningValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "WarningValue"));
                decimal decRemainingValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "RemainingValue"));
                if (decWarningValue > decRemainingValue && decWarningValue > (decimal)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "RemainingValue")
            {
                decimal decRemainingValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "RemainingValue"));
                if (decRemainingValue <= (decimal)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridViewClientPO_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewClientPO_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.ClientPOLink;
            SetMenuStatus();
        }

        private void gridViewClientPO_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.ClientPOLink;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView - Billing Requirement

        private void gridControlBillingRequirement_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("add_from_template".Equals(e.Button.Tag))
                    {
                        Add_From_Billing_Requirement_Template();
                    }
                    else if ("template_manager".Equals(e.Button.Tag))
                    {
                        if (!iBool_BillingRequirementTemplateManager) return;
                        var fChildForm = new frm_Core_Billing_Requirement_Template_Manager();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();
                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridControlBillingRequirement_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridControlBillingRequirement_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.BillingRequirement;
            SetMenuStatus();
        }

        private void gridControlBillingRequirement_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.BillingRequirement;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void Add_From_Billing_Requirement_Template()
        {
            GridView parentView = (GridView)gridControlContract.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Client Contracts to add the Billing Requirement Template to by clicking on them then try again.", "Add Billing Requirements From Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intCount > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intCount.ToString() + " Contracts selected. If you proceed new Billing Requirements will be created for each of these records from the chosen Template.\n\nProceed?", "Add Billing Requirements From Template", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }
            GridView view = (GridView)gridControlBillingRequirement.MainView;
            view.PostEditor();

            var fChildForm = new frm_Core_Select_Billing_Requirement_Template();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                int intTemplateID = fChildForm.intSelectedTemplateHeaderID;
                if (intTemplateID <= 0) return;

                StringBuilder sb = new StringBuilder();
                foreach (int i in intRowHandles)  // Selected Contracts //
                {
                    sb.Append(parentView.GetRowCellValue(i, "ClientContractID").ToString() + ",");
                }
                if (string.IsNullOrWhiteSpace(sb.ToString())) return;

                i_str_AddedRecordIDsBillingRequirement = "";
                using (var AddRecords = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
                {
                    AddRecords.ChangeConnectionString(strConnectionString);
                    try
                    {
                        i_str_AddedRecordIDsBillingRequirement += AddRecords.sp01042_Core_Billing_Requirement_Item_Add_From_Template(sb.ToString(), intBillingRequirementLinkedRecordTypeID, intTemplateID).ToString();
                    }
                    catch (Exception) { }
                }
                LoadLinkedRecords();
            }
        }

        #endregion


        #region Contract Type Filter Panel

        private void btnContractTypeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditContractType_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditContractTypeFilter_Get_Selected();
        }

        private string PopupContainerEditContractTypeFilter_Get_Selected()
        {
            i_str_selected_ContractType_ids = "";    // Reset any prior values first //
            i_str_selected_ContractType_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl1.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_ContractType_ids = "";
                return "No Contract Type Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_ContractType_ids = "";
                return "No Contract Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_ContractType_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_ContractType_names = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_ContractType_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_ContractType_names;
        }

        #endregion


        #region Contract Status Filter Panel

        private void btnContractStatusFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditContractStatus_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditContractStatusFilter_Get_Selected();
        }

        private string PopupContainerEditContractStatusFilter_Get_Selected()
        {
            i_str_selected_ContractStatus_ids = "";    // Reset any prior values first //
            i_str_selected_ContractStatus_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_ContractStatus_ids = "";
                return "No Contract Status Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_ContractStatus_ids = "";
                return "No Contract Status Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_ContractStatus_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_ContractStatus_names = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_ContractStatus_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_ContractStatus_names;
        }

        #endregion


        #region Callout Date Range Filter Panel

        private void btnDateRangeOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            string strValue = "";
            if (dateEditFromDate.DateTime == DateTime.MinValue)
            {
                i_dtStart = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtStart = dateEditFromDate.DateTime;
            }
            if (dateEditToDate.DateTime == DateTime.MinValue)
            {
                i_dtEnd = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtEnd = dateEditToDate.DateTime;
            }
            if (dateEditFromDate.DateTime == DateTime.MinValue && dateEditToDate.DateTime == DateTime.MinValue)
            {
                strValue = "No Date Range Filter";
            }
            else
            {
                strValue = (i_dtStart == new DateTime(1900, 1, 1) ? "No Start" : i_dtStart.ToString("dd/MM/yyyy HH:mm")) + " - " + (i_dtEnd == new DateTime(1900, 1, 1) ? "No End" : i_dtEnd.ToString("dd/MM/yyyy HH:mm"));
            }
            return strValue;
        }

        #endregion


        #region KAM Filter Panel

        private void popupContainerEditKAMFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditKAMFilter_Get_Selected();
        }

        private void btnKAMFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditKAMFilter_Get_Selected()
        {
            i_str_selected_KAM_ids = "";    // Reset any prior values first //
            i_str_selected_KAM_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_KAM_ids = "";
                return "All KAMs";
            }
            else if (selection5.SelectedCount <= 0)
            {
                i_str_selected_KAM_ids = "";
                return "All KAMs";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_KAM_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_KAM_names = Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_KAM_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_KAM_names;
        }

        #endregion


        #region Construction Manager Filter Panel

        private void popupContainerEditConstructionManager_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditConstructionManagerFilter_Get_Selected();
        }

        private void btnConstructionManagerFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditConstructionManagerFilter_Get_Selected()
        {
            i_str_selected_Construction_Manager_ids = "";    // Reset any prior values first //
            i_str_selected_Construction_Manager_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl3.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_Construction_Manager_ids = "";
                return "All Construction Managers";
            }
            else if (selection3.SelectedCount <= 0)
            {
                i_str_selected_Construction_Manager_ids = "";
                return "All Construction Managers";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_Construction_Manager_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_Construction_Manager_names = Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_Construction_Manager_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_Construction_Manager_names;
        }

        #endregion


        #region Contract Director Filter Panel

        private void popupContainerEditContractDirector_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditContractDirector_Get_Selected();
        }
        
        private void btnContractDirectorFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditContractDirector_Get_Selected()
        {
            i_str_selected_Contract_Director_ids = "";    // Reset any prior values first //
            i_str_selected_KAM_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl4.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_Contract_Director_ids = "";
                return "All Contract Directors";

            }
            else if (selection4.SelectedCount <= 0)
            {
                i_str_selected_Contract_Director_ids = "";
                return "All Contract Directors";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_Contract_Director_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_Contract_Director_names = Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_Contract_Director_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_Contract_Director_names;
        }

        #endregion



        private void btnClearAllFilters_Click(object sender, EventArgs e)
        {
            /*i_str_selected_client_ids = "";
            i_str_selected_client_names = "";
            buttonEditClientFilter.Text = "";
            superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\nNo Filter Set.";
            
            i_str_selected_site_ids = "";
            i_str_selected_site_names = "";
            buttonEditSiteFilter.Text = "";
            superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
            */
            selection1.ClearSelection();
            popupContainerEditContractType.Text = PopupContainerEditContractTypeFilter_Get_Selected();

            selection2.ClearSelection();
            popupContainerControlContractStatusFilter.Text = PopupContainerEditContractStatusFilter_Get_Selected();

            selection5.ClearSelection();
            popupContainerEditKAMFilter.Text = PopupContainerEditKAMFilter_Get_Selected();

            selection3.ClearSelection();
            popupContainerEditConstructionManager.Text = PopupContainerEditConstructionManagerFilter_Get_Selected();

            selection4.ClearSelection();
            popupContainerEditContractDirector.Text = PopupContainerEditContractDirector_Get_Selected();
        }

        private void btnLoad_Click_1(object sender, EventArgs e)
        {
            Load_Data();
        }


        #region Buttons

        private void bbiAddWizard_ItemClick(object sender, ItemClickEventArgs e)
        {
            var fChildForm = new frm_OM_Client_Contract_Wizard();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = "";
            fChildForm.strFormMode = "add";
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = 0;
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void bbiViewSchedule_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void bbiSendSchedule_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        #endregion


        private void splitContainerControl1_SplitGroupPanelCollapsed(object sender, SplitGroupPanelCollapsedEventArgs e)
        {
            if (!e.Collapsed) LoadLinkedRecords();
        }

        private void barManager1_HighlightedLinkChanged(object sender, DevExpress.XtraBars.HighlightedLinkChangedEventArgs e)
        {
            // This event is required by the ToolTipControl object [it handles firing the Tooltips on BarSubItem objects on the toolbar because they don't show Tooltips] //
            toolTipController1.HideHint();
            if (e.Link == null) return;

            BarSubItemLink link = e.PrevLink as BarSubItemLink;
            if (link != null) link.CloseMenu();

            if (e.Link.Item is BarLargeButtonItem) return;

            var Info = new ToolTipControlInfo {Object = e.Link.Item, SuperTip = e.Link.Item.SuperTip};

            toolTipController1.ShowHint(Info);
        }

        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp06049_OM_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "om_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }












    }
}



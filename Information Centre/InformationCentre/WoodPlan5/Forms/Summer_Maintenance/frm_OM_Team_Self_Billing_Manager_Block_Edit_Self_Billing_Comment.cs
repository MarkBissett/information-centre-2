﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Team_Self_Billing_Manager_Block_Edit_Self_Billing_Comment : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string SelfBillingComment = null;

        #endregion

        public frm_OM_Team_Self_Billing_Manager_Block_Edit_Self_Billing_Comment()
        {
            InitializeComponent();
        }

        private void frm_OM_Team_Self_Billing_Manager_Block_Edit_Self_Billing_Comment_Load(object sender, EventArgs e)
        {
            this.FormID = 500275;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //
            
            strConnectionString = this.GlobalSettings.ConnectionString;

            memoEditSelfBillingComment.EditValue = null;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }
       
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (memoEditSelfBillingComment.EditValue != null) SelfBillingComment = memoEditSelfBillingComment.EditValue.ToString();
            
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }






    }
}

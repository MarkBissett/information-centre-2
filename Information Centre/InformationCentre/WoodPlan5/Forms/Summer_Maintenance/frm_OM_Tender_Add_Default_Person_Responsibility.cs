﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils;

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Tender_Add_Default_Person_Responsibility : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        private string strConnectionStringREADONLY = "";
        Settings set = Settings.Default;

        public int _PassedInIncludeBlank = 1;
        public int _PassedInStaffID = 0;
        public string _PassedInResponsibilityDescription = "";
        public string _PassedInAddToDescription = "";

        public int _SelectedStaffID = 0;
        public string _SelectedSurname = "";
        public string _SelectedForename = "";
        public string _SelectedSurnameForename = "";
 
        #endregion

        public frm_OM_Tender_Add_Default_Person_Responsibility()
        {
            InitializeComponent();
        }

        private void frm_OM_Tender_Add_Default_Person_Responsibility_Load(object sender, EventArgs e)
        {
            strConnectionString = this.GlobalSettings.ConnectionString;
            strConnectionStringREADONLY = GlobalSettings.ConnectionStringREADONLY;
            Application.DoEvents();  // Allow Form time to repaint itself //

            sp06511_OM_Tender_Add_Default_Person_Responsibility_Select_PersonTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp06511_OM_Tender_Add_Default_Person_Responsibility_Select_PersonTableAdapter.Fill(dataSet_OM_Tender.sp06511_OM_Tender_Add_Default_Person_Responsibility_Select_Person, _PassedInIncludeBlank);

            AddToTextEdit.EditValue = _PassedInAddToDescription;
            ResponsibilityTypeTextEdit.EditValue = _PassedInResponsibilityDescription;
            PersonIDGridLookUpEdit.EditValue = _PassedInStaffID;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

      
        private void btnOK_Click(object sender, EventArgs e)
        {
            _SelectedStaffID = Convert.ToInt32(PersonIDGridLookUpEdit.EditValue);

            GridLookUpEdit glue = (GridLookUpEdit)PersonIDGridLookUpEdit;
            if (glue.EditValue == DBNull.Value || glue.EditValue.ToString() == "0")
            {
                _SelectedStaffID = 0; 
                _SelectedSurname = "";
                _SelectedForename = "";
                _SelectedSurnameForename = "";
            }
            else
            {
                glue = (GridLookUpEdit)PersonIDGridLookUpEdit;
                _SelectedStaffID = Convert.ToInt32(glue.EditValue);
                GridView view = glue.Properties.View;
                int intFoundRow = 0;
                intFoundRow = (glue.EditValue == DBNull.Value ? GridControl.InvalidRowHandle : view.LocateByValue(0, view.Columns["StaffID"], Convert.ToInt32(glue.EditValue)));
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    _SelectedSurname = view.GetRowCellValue(intFoundRow, "Surname").ToString();
                    _SelectedForename = view.GetRowCellValue(intFoundRow, "Forename").ToString();
                    _SelectedSurnameForename = view.GetRowCellValue(intFoundRow, "SurnameForename").ToString();
                }
            }

            if (_SelectedStaffID == 0)
            {
                XtraMessageBox.Show("Select the Person before proceeding.", "Add Default Person Responsibility", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }


        #region Editors
 
        #endregion








    }
}

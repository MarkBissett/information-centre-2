namespace WoodPlan5
{
    partial class frm_OM_Job_Attribute_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>ValueRecordedDateEdit.DataBindings
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Job_Attribute_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule4 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue4 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule5 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue5 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling22 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling23 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling24 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLinkedDocuments = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.ValueFormulaEvaluateOrderSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.sp06234OMJobAttribureEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.ValueFormulaEvaluateLocationGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06481OMAttributeFormulaEvaluateLocationListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ValueFormulaMemoExEdit = new DevExpress.XtraEditors.MemoExEdit();
            this.AttributeNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.HideFromAppCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ValueRecordedMemoExEdit = new DevExpress.XtraEditors.MemoExEdit();
            this.ValueRecordedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ValueRecordedPictureEdit = new DevExpress.XtraEditors.PictureEdit();
            this.ValueRecordedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ValueRecordedTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ValueRecordedSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.PicklistHeaderIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06202OMAttributePicklistHeadersWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ValueRecordedGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06236OMAttributePicklistItemsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ValueRecordedMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.DefaultValueMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.DataEntryMaxValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.DataEntryMinValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EditorMaskTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EditorTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06207OMAttributeEditorTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewEditorType = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DataTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06206OMAttributeDataTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.OnScreenLabelTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DataEntryRequiredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.AttributeOrderSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.JobExpectedStartDateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobSubTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobSubTypeDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobTypeDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameContractDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobAttributeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.JobTypeJobSubTypeDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.MasterJobAttributeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForClientContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobSubTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobSubTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobAttributeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMasterJobAttributeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDefaultValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForJobTypeJobSubTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layGrpAddress = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForAttributeOrder = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDataEntryRequired = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOnScreenLabel = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDataTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForEditorTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForEditorMask = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.DataEntryMinValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDataEntryMaxValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForValueRecordedMemoEdit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForValueRecordedPicklist = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPicklistHeaderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForValueRecordedSpinEdit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForValueRecordedTextEdit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForValueRecordedDateEdit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForValueRecordedPictureEdit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForValueRecordedCheckEdit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForValueRecordedMemoExEdit = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForHideFromApp = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAttributeName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForValueFormulaEvaluateLocation = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForValueFormula = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForValueFormulaEvaluateOrder = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientNameContractDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForJobExpectedStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp06234_OM_Job_Attribure_EditTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06234_OM_Job_Attribure_EditTableAdapter();
            this.sp06206_OM_Attribute_DataTypes_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06206_OM_Attribute_DataTypes_With_BlankTableAdapter();
            this.sp06207_OM_Attribute_EditorTypes_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06207_OM_Attribute_EditorTypes_With_BlankTableAdapter();
            this.sp06202_OM_Attribute_Picklist_Headers_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06202_OM_Attribute_Picklist_Headers_With_BlankTableAdapter();
            this.sp06236_OM_Attribute_Picklist_Items_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06236_OM_Attribute_Picklist_Items_With_BlankTableAdapter();
            this.sp06481_OM_Attribute_Formula_Evaluate_Location_ListTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06481_OM_Attribute_Formula_Evaluate_Location_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ValueFormulaEvaluateOrderSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06234OMJobAttribureEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueFormulaEvaluateLocationGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06481OMAttributeFormulaEvaluateLocationListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueFormulaMemoExEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AttributeNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HideFromAppCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueRecordedMemoExEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueRecordedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueRecordedPictureEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueRecordedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueRecordedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueRecordedTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueRecordedSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicklistHeaderIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06202OMAttributePicklistHeadersWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueRecordedGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06236OMAttributePicklistItemsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueRecordedMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultValueMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataEntryMaxValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataEntryMinValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditorMaskTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditorTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06207OMAttributeEditorTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEditorType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06206OMAttributeDataTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnScreenLabelTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataEntryRequiredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AttributeOrderSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobExpectedStartDateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameContractDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobAttributeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeJobSubTypeDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MasterJobAttributeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobAttributeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMasterJobAttributeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeJobSubTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAttributeOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDataEntryRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOnScreenLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDataTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEditorTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEditorMask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataEntryMinValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDataEntryMaxValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueRecordedMemoEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueRecordedPicklist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPicklistHeaderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueRecordedSpinEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueRecordedTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueRecordedDateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueRecordedPictureEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueRecordedCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueRecordedMemoExEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHideFromApp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAttributeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueFormulaEvaluateLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueFormula)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueFormulaEvaluateOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientNameContractDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobExpectedStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(715, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 614);
            this.barDockControlBottom.Size = new System.Drawing.Size(715, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 588);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(715, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 588);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Linked Items";
            this.gridColumn10.FieldName = "PicklistItemCount";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 1;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "ID";
            this.gridColumn7.FieldName = "ID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn7.Width = 53;
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 2;
            this.colActive.Width = 49;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colID1.Width = 53;
            // 
            // colActive1
            // 
            this.colActive1.Caption = "Active";
            this.colActive1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colActive1.FieldName = "Active";
            this.colActive1.Name = "colActive1";
            this.colActive1.OptionsColumn.AllowEdit = false;
            this.colActive1.OptionsColumn.AllowFocus = false;
            this.colActive1.OptionsColumn.ReadOnly = true;
            this.colActive1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActive1.Visible = true;
            this.colActive1.VisibleIndex = 1;
            this.colActive1.Width = 49;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn4.Width = 53;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn1.Width = 53;
            // 
            // colHeaderID
            // 
            this.colHeaderID.Caption = "Header ID";
            this.colHeaderID.FieldName = "HeaderID";
            this.colHeaderID.Name = "colHeaderID";
            this.colHeaderID.OptionsColumn.AllowEdit = false;
            this.colHeaderID.OptionsColumn.AllowFocus = false;
            this.colHeaderID.OptionsColumn.ReadOnly = true;
            this.colHeaderID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiLinkedDocuments});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLinkedDocuments, true)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiLinkedDocuments
            // 
            this.bbiLinkedDocuments.Caption = "Linked Documents";
            this.bbiLinkedDocuments.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLinkedDocuments.Glyph")));
            this.bbiLinkedDocuments.Id = 15;
            this.bbiLinkedDocuments.Name = "bbiLinkedDocuments";
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Linked Documents - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to open the Linked Documents Manager, filtered on the current data entry" +
    " record.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiLinkedDocuments.SuperTip = superToolTip3;
            this.bbiLinkedDocuments.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLinkedDocuments_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Form Mode - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.barStaticItemFormMode.SuperTip = superToolTip4;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(715, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 614);
            this.barDockControl2.Size = new System.Drawing.Size(715, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 588);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(715, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 588);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.ValueFormulaEvaluateOrderSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ValueFormulaEvaluateLocationGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ValueFormulaMemoExEdit);
            this.dataLayoutControl1.Controls.Add(this.AttributeNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.HideFromAppCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ValueRecordedMemoExEdit);
            this.dataLayoutControl1.Controls.Add(this.ValueRecordedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ValueRecordedPictureEdit);
            this.dataLayoutControl1.Controls.Add(this.ValueRecordedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ValueRecordedTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ValueRecordedSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.PicklistHeaderIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ValueRecordedGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ValueRecordedMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.DefaultValueMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.DataEntryMaxValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.DataEntryMinValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EditorMaskTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EditorTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.DataTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.OnScreenLabelTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DataEntryRequiredCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.AttributeOrderSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.JobExpectedStartDateTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientContractIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteContractIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobSubTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobSubTypeDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobTypeDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameContractDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobAttributeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.JobTypeJobSubTypeDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.MasterJobAttributeIDTextEdit);
            this.dataLayoutControl1.DataSource = this.sp06234OMJobAttribureEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientContractID,
            this.ItemForClientID,
            this.ItemForClientName,
            this.ItemForJobID,
            this.ItemForVisitID,
            this.ItemForSiteID,
            this.ItemForSiteContractID,
            this.ItemForJobTypeID,
            this.ItemForJobTypeDescription,
            this.ItemForJobSubTypeID,
            this.ItemForJobSubTypeDescription,
            this.ItemForJobAttributeID,
            this.ItemForMasterJobAttributeID,
            this.ItemForDefaultValue});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1234, 162, 523, 422);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(715, 588);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ValueFormulaEvaluateOrderSpinEdit
            // 
            this.ValueFormulaEvaluateOrderSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "ValueFormulaEvaluateOrder", true));
            this.ValueFormulaEvaluateOrderSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ValueFormulaEvaluateOrderSpinEdit.Location = new System.Drawing.Point(147, 235);
            this.ValueFormulaEvaluateOrderSpinEdit.MenuManager = this.barManager1;
            this.ValueFormulaEvaluateOrderSpinEdit.Name = "ValueFormulaEvaluateOrderSpinEdit";
            this.ValueFormulaEvaluateOrderSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ValueFormulaEvaluateOrderSpinEdit.Properties.IsFloatValue = false;
            this.ValueFormulaEvaluateOrderSpinEdit.Properties.Mask.EditMask = "N0";
            this.ValueFormulaEvaluateOrderSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ValueFormulaEvaluateOrderSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.ValueFormulaEvaluateOrderSpinEdit.Size = new System.Drawing.Size(195, 20);
            this.ValueFormulaEvaluateOrderSpinEdit.StyleController = this.dataLayoutControl1;
            this.ValueFormulaEvaluateOrderSpinEdit.TabIndex = 58;
            // 
            // sp06234OMJobAttribureEditBindingSource
            // 
            this.sp06234OMJobAttribureEditBindingSource.DataMember = "sp06234_OM_Job_Attribure_Edit";
            this.sp06234OMJobAttribureEditBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ValueFormulaEvaluateLocationGridLookUpEdit
            // 
            this.ValueFormulaEvaluateLocationGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "ValueFormulaEvaluateLocation", true));
            this.ValueFormulaEvaluateLocationGridLookUpEdit.Location = new System.Drawing.Point(147, 211);
            this.ValueFormulaEvaluateLocationGridLookUpEdit.MenuManager = this.barManager1;
            this.ValueFormulaEvaluateLocationGridLookUpEdit.Name = "ValueFormulaEvaluateLocationGridLookUpEdit";
            this.ValueFormulaEvaluateLocationGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ValueFormulaEvaluateLocationGridLookUpEdit.Properties.DataSource = this.sp06481OMAttributeFormulaEvaluateLocationListBindingSource;
            this.ValueFormulaEvaluateLocationGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ValueFormulaEvaluateLocationGridLookUpEdit.Properties.NullText = "";
            this.ValueFormulaEvaluateLocationGridLookUpEdit.Properties.ValueMember = "ID";
            this.ValueFormulaEvaluateLocationGridLookUpEdit.Properties.View = this.gridView4;
            this.ValueFormulaEvaluateLocationGridLookUpEdit.Size = new System.Drawing.Size(515, 20);
            this.ValueFormulaEvaluateLocationGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ValueFormulaEvaluateLocationGridLookUpEdit.TabIndex = 52;
            // 
            // sp06481OMAttributeFormulaEvaluateLocationListBindingSource
            // 
            this.sp06481OMAttributeFormulaEvaluateLocationListBindingSource.DataMember = "sp06481_OM_Attribute_Formula_Evaluate_Location_List";
            this.sp06481OMAttributeFormulaEvaluateLocationListBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn13, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "ID";
            this.gridColumn11.FieldName = "ID";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn11.Width = 53;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Editor Type";
            this.gridColumn12.FieldName = "Description";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 0;
            this.gridColumn12.Width = 220;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Order";
            this.gridColumn13.FieldName = "RecordOrder";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // ValueFormulaMemoExEdit
            // 
            this.ValueFormulaMemoExEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "ValueFormula", true));
            this.ValueFormulaMemoExEdit.Location = new System.Drawing.Point(147, 187);
            this.ValueFormulaMemoExEdit.MenuManager = this.barManager1;
            this.ValueFormulaMemoExEdit.Name = "ValueFormulaMemoExEdit";
            this.ValueFormulaMemoExEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Formula Builder", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to open the Formula Builder screen.", "formula_builder", null, true)});
            this.ValueFormulaMemoExEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ValueFormulaMemoExEdit.Properties.ShowIcon = false;
            this.ValueFormulaMemoExEdit.Size = new System.Drawing.Size(515, 20);
            this.ValueFormulaMemoExEdit.StyleController = this.dataLayoutControl1;
            this.ValueFormulaMemoExEdit.TabIndex = 62;
            this.ValueFormulaMemoExEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ValueFormulaMemoExEdit_ButtonClick);
            // 
            // AttributeNameTextEdit
            // 
            this.AttributeNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "AttributeName", true));
            this.AttributeNameTextEdit.Location = new System.Drawing.Point(147, -99);
            this.AttributeNameTextEdit.MenuManager = this.barManager1;
            this.AttributeNameTextEdit.Name = "AttributeNameTextEdit";
            this.AttributeNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AttributeNameTextEdit, true);
            this.AttributeNameTextEdit.Size = new System.Drawing.Size(515, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AttributeNameTextEdit, optionsSpelling1);
            this.AttributeNameTextEdit.StyleController = this.dataLayoutControl1;
            this.AttributeNameTextEdit.TabIndex = 61;
            this.AttributeNameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.AttributeNameTextEdit_Validating);
            // 
            // HideFromAppCheckEdit
            // 
            this.HideFromAppCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "HideFromApp", true));
            this.HideFromAppCheckEdit.Location = new System.Drawing.Point(147, -28);
            this.HideFromAppCheckEdit.MenuManager = this.barManager1;
            this.HideFromAppCheckEdit.Name = "HideFromAppCheckEdit";
            this.HideFromAppCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.HideFromAppCheckEdit.Properties.ValueChecked = 1;
            this.HideFromAppCheckEdit.Properties.ValueUnchecked = 0;
            this.HideFromAppCheckEdit.Size = new System.Drawing.Size(94, 19);
            this.HideFromAppCheckEdit.StyleController = this.dataLayoutControl1;
            this.HideFromAppCheckEdit.TabIndex = 63;
            // 
            // ValueRecordedMemoExEdit
            // 
            this.ValueRecordedMemoExEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "ValueRecorded", true));
            this.ValueRecordedMemoExEdit.Location = new System.Drawing.Point(147, 307);
            this.ValueRecordedMemoExEdit.MenuManager = this.barManager1;
            this.ValueRecordedMemoExEdit.Name = "ValueRecordedMemoExEdit";
            this.ValueRecordedMemoExEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ValueRecordedMemoExEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ValueRecordedMemoExEdit.Properties.ShowIcon = false;
            this.ValueRecordedMemoExEdit.Size = new System.Drawing.Size(515, 20);
            this.ValueRecordedMemoExEdit.StyleController = this.dataLayoutControl1;
            this.ValueRecordedMemoExEdit.TabIndex = 74;
            // 
            // ValueRecordedCheckEdit
            // 
            this.ValueRecordedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "ValueRecorded", true));
            this.ValueRecordedCheckEdit.Location = new System.Drawing.Point(147, 523);
            this.ValueRecordedCheckEdit.MenuManager = this.barManager1;
            this.ValueRecordedCheckEdit.Name = "ValueRecordedCheckEdit";
            this.ValueRecordedCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.ValueRecordedCheckEdit.Properties.ValueChecked = "1";
            this.ValueRecordedCheckEdit.Properties.ValueUnchecked = "0";
            this.ValueRecordedCheckEdit.Size = new System.Drawing.Size(515, 19);
            this.ValueRecordedCheckEdit.StyleController = this.dataLayoutControl1;
            this.ValueRecordedCheckEdit.TabIndex = 73;
            this.ValueRecordedCheckEdit.Tag = "";
            // 
            // ValueRecordedPictureEdit
            // 
            this.ValueRecordedPictureEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "ValueRecorded", true));
            this.ValueRecordedPictureEdit.Location = new System.Drawing.Point(147, 403);
            this.ValueRecordedPictureEdit.MenuManager = this.barManager1;
            this.ValueRecordedPictureEdit.Name = "ValueRecordedPictureEdit";
            this.ValueRecordedPictureEdit.Size = new System.Drawing.Size(515, 116);
            this.ValueRecordedPictureEdit.StyleController = this.dataLayoutControl1;
            this.ValueRecordedPictureEdit.TabIndex = 72;
            // 
            // ValueRecordedDateEdit
            // 
            this.ValueRecordedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "ValueRecorded", true));
            this.ValueRecordedDateEdit.EditValue = null;
            this.ValueRecordedDateEdit.Location = new System.Drawing.Point(147, 379);
            this.ValueRecordedDateEdit.MenuManager = this.barManager1;
            this.ValueRecordedDateEdit.Name = "ValueRecordedDateEdit";
            this.ValueRecordedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ValueRecordedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ValueRecordedDateEdit.Properties.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
            this.ValueRecordedDateEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.ValueRecordedDateEdit.Properties.EditFormat.FormatString = "dd/MM/yyyy HH:mm:ss";
            this.ValueRecordedDateEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.ValueRecordedDateEdit.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.ValueRecordedDateEdit.Size = new System.Drawing.Size(515, 20);
            this.ValueRecordedDateEdit.StyleController = this.dataLayoutControl1;
            this.ValueRecordedDateEdit.TabIndex = 71;
            // 
            // ValueRecordedTextEdit
            // 
            this.ValueRecordedTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "ValueRecorded", true));
            this.ValueRecordedTextEdit.Location = new System.Drawing.Point(147, 355);
            this.ValueRecordedTextEdit.MenuManager = this.barManager1;
            this.ValueRecordedTextEdit.Name = "ValueRecordedTextEdit";
            this.ValueRecordedTextEdit.Properties.MaxLength = 250;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ValueRecordedTextEdit, true);
            this.ValueRecordedTextEdit.Size = new System.Drawing.Size(515, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ValueRecordedTextEdit, optionsSpelling2);
            this.ValueRecordedTextEdit.StyleController = this.dataLayoutControl1;
            this.ValueRecordedTextEdit.TabIndex = 70;
            // 
            // ValueRecordedSpinEdit
            // 
            this.ValueRecordedSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "ValueRecorded", true));
            this.ValueRecordedSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ValueRecordedSpinEdit.Location = new System.Drawing.Point(147, 331);
            this.ValueRecordedSpinEdit.MenuManager = this.barManager1;
            this.ValueRecordedSpinEdit.Name = "ValueRecordedSpinEdit";
            this.ValueRecordedSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ValueRecordedSpinEdit.Size = new System.Drawing.Size(515, 20);
            this.ValueRecordedSpinEdit.StyleController = this.dataLayoutControl1;
            this.ValueRecordedSpinEdit.TabIndex = 69;
            // 
            // PicklistHeaderIDGridLookUpEdit
            // 
            this.PicklistHeaderIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "PicklistHeaderID", true));
            this.PicklistHeaderIDGridLookUpEdit.Location = new System.Drawing.Point(147, 139);
            this.PicklistHeaderIDGridLookUpEdit.MenuManager = this.barManager1;
            this.PicklistHeaderIDGridLookUpEdit.Name = "PicklistHeaderIDGridLookUpEdit";
            this.PicklistHeaderIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PicklistHeaderIDGridLookUpEdit.Properties.DataSource = this.sp06202OMAttributePicklistHeadersWithBlankBindingSource;
            this.PicklistHeaderIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.PicklistHeaderIDGridLookUpEdit.Properties.NullText = "";
            this.PicklistHeaderIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.PicklistHeaderIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.PicklistHeaderIDGridLookUpEdit.Properties.View = this.gridView2;
            this.PicklistHeaderIDGridLookUpEdit.Size = new System.Drawing.Size(515, 20);
            this.PicklistHeaderIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.PicklistHeaderIDGridLookUpEdit.TabIndex = 52;
            this.PicklistHeaderIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.PicklistHeaderIDGridLookUpEdit_Validating);
            // 
            // sp06202OMAttributePicklistHeadersWithBlankBindingSource
            // 
            this.sp06202OMAttributePicklistHeadersWithBlankBindingSource.DataMember = "sp06202_OM_Attribute_Picklist_Headers_With_Blank";
            this.sp06202OMAttributePicklistHeadersWithBlankBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.colActive});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.Column = this.gridColumn10;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.gridColumn7;
            gridFormatRule2.Name = "Format1";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Column = this.colActive;
            gridFormatRule3.Name = "Format2";
            formatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue3.Value1 = 0;
            gridFormatRule3.Rule = formatConditionRuleValue3;
            this.gridView2.FormatRules.Add(gridFormatRule1);
            this.gridView2.FormatRules.Add(gridFormatRule2);
            this.gridView2.FormatRules.Add(gridFormatRule3);
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn9, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Picklist Header";
            this.gridColumn8.FieldName = "Description";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 319;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Order";
            this.gridColumn9.FieldName = "RecordOrder";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // ValueRecordedGridLookUpEdit
            // 
            this.ValueRecordedGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "ValueRecorded", true));
            this.ValueRecordedGridLookUpEdit.Location = new System.Drawing.Point(147, 163);
            this.ValueRecordedGridLookUpEdit.MenuManager = this.barManager1;
            this.ValueRecordedGridLookUpEdit.Name = "ValueRecordedGridLookUpEdit";
            this.ValueRecordedGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ValueRecordedGridLookUpEdit.Properties.DataSource = this.sp06236OMAttributePicklistItemsWithBlankBindingSource;
            this.ValueRecordedGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ValueRecordedGridLookUpEdit.Properties.NullText = "";
            this.ValueRecordedGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2});
            this.ValueRecordedGridLookUpEdit.Properties.ValueMember = "ID";
            this.ValueRecordedGridLookUpEdit.Properties.View = this.gridView3;
            this.ValueRecordedGridLookUpEdit.Size = new System.Drawing.Size(515, 20);
            this.ValueRecordedGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ValueRecordedGridLookUpEdit.TabIndex = 51;
            this.ValueRecordedGridLookUpEdit.Enter += new System.EventHandler(this.ValueRecordedTextGridLookUpEdit_Enter);
            this.ValueRecordedGridLookUpEdit.Leave += new System.EventHandler(this.ValueRecordedTextGridLookUpEdit_Leave);
            this.ValueRecordedGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ValueRecordedTextGridLookUpEdit_Validating);
            // 
            // sp06236OMAttributePicklistItemsWithBlankBindingSource
            // 
            this.sp06236OMAttributePicklistItemsWithBlankBindingSource.DataMember = "sp06236_OM_Attribute_Picklist_Items_With_Blank";
            this.sp06236OMAttributePicklistItemsWithBlankBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder,
            this.colHeaderID,
            this.colActive1});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule4.ApplyToRow = true;
            gridFormatRule4.Column = this.colID1;
            gridFormatRule4.Name = "Format0";
            formatConditionRuleValue4.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue4.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue4.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue4.Value1 = 0;
            gridFormatRule4.Rule = formatConditionRuleValue4;
            gridFormatRule5.ApplyToRow = true;
            gridFormatRule5.Column = this.colActive1;
            gridFormatRule5.Name = "Format1";
            formatConditionRuleValue5.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue5.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue5.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue5.Value1 = 0;
            gridFormatRule5.Rule = formatConditionRuleValue5;
            this.gridView3.FormatRules.Add(gridFormatRule4);
            this.gridView3.FormatRules.Add(gridFormatRule5);
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Picklist Item";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 319;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "RecordOrder";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // ValueRecordedMemoEdit
            // 
            this.ValueRecordedMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "ValueRecorded", true));
            this.ValueRecordedMemoEdit.Location = new System.Drawing.Point(147, 259);
            this.ValueRecordedMemoEdit.MenuManager = this.barManager1;
            this.ValueRecordedMemoEdit.Name = "ValueRecordedMemoEdit";
            this.ValueRecordedMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ValueRecordedMemoEdit, true);
            this.ValueRecordedMemoEdit.Size = new System.Drawing.Size(515, 44);
            this.scSpellChecker.SetSpellCheckerOptions(this.ValueRecordedMemoEdit, optionsSpelling3);
            this.ValueRecordedMemoEdit.StyleController = this.dataLayoutControl1;
            this.ValueRecordedMemoEdit.TabIndex = 59;
            // 
            // DefaultValueMemoEdit
            // 
            this.DefaultValueMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "DefaultValue", true));
            this.DefaultValueMemoEdit.Location = new System.Drawing.Point(147, 474);
            this.DefaultValueMemoEdit.MenuManager = this.barManager1;
            this.DefaultValueMemoEdit.Name = "DefaultValueMemoEdit";
            this.DefaultValueMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DefaultValueMemoEdit, true);
            this.DefaultValueMemoEdit.Size = new System.Drawing.Size(503, 44);
            this.scSpellChecker.SetSpellCheckerOptions(this.DefaultValueMemoEdit, optionsSpelling4);
            this.DefaultValueMemoEdit.StyleController = this.dataLayoutControl1;
            this.DefaultValueMemoEdit.TabIndex = 58;
            // 
            // DataEntryMaxValueSpinEdit
            // 
            this.DataEntryMaxValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "DataEntryMaxValue", true));
            this.DataEntryMaxValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DataEntryMaxValueSpinEdit.Location = new System.Drawing.Point(147, 115);
            this.DataEntryMaxValueSpinEdit.MenuManager = this.barManager1;
            this.DataEntryMaxValueSpinEdit.Name = "DataEntryMaxValueSpinEdit";
            this.DataEntryMaxValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DataEntryMaxValueSpinEdit.Properties.Mask.EditMask = "n2";
            this.DataEntryMaxValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DataEntryMaxValueSpinEdit.Size = new System.Drawing.Size(194, 20);
            this.DataEntryMaxValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.DataEntryMaxValueSpinEdit.TabIndex = 54;
            // 
            // DataEntryMinValueSpinEdit
            // 
            this.DataEntryMinValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "DataEntryMinValue", true));
            this.DataEntryMinValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DataEntryMinValueSpinEdit.Location = new System.Drawing.Point(147, 91);
            this.DataEntryMinValueSpinEdit.MenuManager = this.barManager1;
            this.DataEntryMinValueSpinEdit.Name = "DataEntryMinValueSpinEdit";
            this.DataEntryMinValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DataEntryMinValueSpinEdit.Properties.Mask.EditMask = "n2";
            this.DataEntryMinValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DataEntryMinValueSpinEdit.Size = new System.Drawing.Size(194, 20);
            this.DataEntryMinValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.DataEntryMinValueSpinEdit.TabIndex = 53;
            // 
            // EditorMaskTextEdit
            // 
            this.EditorMaskTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "EditorMask", true));
            this.EditorMaskTextEdit.Location = new System.Drawing.Point(147, 67);
            this.EditorMaskTextEdit.MenuManager = this.barManager1;
            this.EditorMaskTextEdit.Name = "EditorMaskTextEdit";
            this.EditorMaskTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EditorMaskTextEdit, true);
            this.EditorMaskTextEdit.Size = new System.Drawing.Size(194, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EditorMaskTextEdit, optionsSpelling5);
            this.EditorMaskTextEdit.StyleController = this.dataLayoutControl1;
            this.EditorMaskTextEdit.TabIndex = 52;
            this.EditorMaskTextEdit.EditValueChanged += new System.EventHandler(this.EditorMaskTextEdit_EditValueChanged);
            // 
            // EditorTypeIDGridLookUpEdit
            // 
            this.EditorTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "EditorTypeID", true));
            this.EditorTypeIDGridLookUpEdit.Location = new System.Drawing.Point(147, 43);
            this.EditorTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.EditorTypeIDGridLookUpEdit.Name = "EditorTypeIDGridLookUpEdit";
            this.EditorTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EditorTypeIDGridLookUpEdit.Properties.DataSource = this.sp06207OMAttributeEditorTypesWithBlankBindingSource;
            this.EditorTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.EditorTypeIDGridLookUpEdit.Properties.NullText = "";
            this.EditorTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.EditorTypeIDGridLookUpEdit.Properties.View = this.gridViewEditorType;
            this.EditorTypeIDGridLookUpEdit.Size = new System.Drawing.Size(194, 20);
            this.EditorTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.EditorTypeIDGridLookUpEdit.TabIndex = 51;
            this.EditorTypeIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.EditorTypeIDGridLookUpEdit_EditValueChanged);
            this.EditorTypeIDGridLookUpEdit.Enter += new System.EventHandler(this.EditorTypeIDGridLookUpEdit_Enter);
            this.EditorTypeIDGridLookUpEdit.Leave += new System.EventHandler(this.EditorTypeIDGridLookUpEdit_Leave);
            this.EditorTypeIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.EditorTypeIDGridLookUpEdit_Validating);
            // 
            // sp06207OMAttributeEditorTypesWithBlankBindingSource
            // 
            this.sp06207OMAttributeEditorTypesWithBlankBindingSource.DataMember = "sp06207_OM_Attribute_EditorTypes_With_Blank";
            this.sp06207OMAttributeEditorTypesWithBlankBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridViewEditorType
            // 
            this.gridViewEditorType.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridViewEditorType.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn4;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridViewEditorType.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridViewEditorType.Name = "gridViewEditorType";
            this.gridViewEditorType.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewEditorType.OptionsLayout.StoreAppearance = true;
            this.gridViewEditorType.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewEditorType.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewEditorType.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewEditorType.OptionsView.ColumnAutoWidth = false;
            this.gridViewEditorType.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewEditorType.OptionsView.ShowGroupPanel = false;
            this.gridViewEditorType.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewEditorType.OptionsView.ShowIndicator = false;
            this.gridViewEditorType.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Editor Type";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "RecordOrder";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // DataTypeIDGridLookUpEdit
            // 
            this.DataTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "DataTypeID", true));
            this.DataTypeIDGridLookUpEdit.Location = new System.Drawing.Point(147, 19);
            this.DataTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.DataTypeIDGridLookUpEdit.Name = "DataTypeIDGridLookUpEdit";
            this.DataTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DataTypeIDGridLookUpEdit.Properties.DataSource = this.sp06206OMAttributeDataTypesWithBlankBindingSource;
            this.DataTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.DataTypeIDGridLookUpEdit.Properties.NullText = "";
            this.DataTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.DataTypeIDGridLookUpEdit.Properties.View = this.gridView1;
            this.DataTypeIDGridLookUpEdit.Size = new System.Drawing.Size(194, 20);
            this.DataTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.DataTypeIDGridLookUpEdit.TabIndex = 50;
            this.DataTypeIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.DataTypeIDGridLookUpEdit_EditValueChanged);
            this.DataTypeIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DataTypeIDGridLookUpEdit_Validating);
            // 
            // sp06206OMAttributeDataTypesWithBlankBindingSource
            // 
            this.sp06206OMAttributeDataTypesWithBlankBindingSource.DataMember = "sp06206_OM_Attribute_DataTypes_With_Blank";
            this.sp06206OMAttributeDataTypesWithBlankBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn1;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Data Types";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // OnScreenLabelTextEdit
            // 
            this.OnScreenLabelTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "OnScreenLabel", true));
            this.OnScreenLabelTextEdit.Location = new System.Drawing.Point(147, -5);
            this.OnScreenLabelTextEdit.MenuManager = this.barManager1;
            this.OnScreenLabelTextEdit.Name = "OnScreenLabelTextEdit";
            this.OnScreenLabelTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.OnScreenLabelTextEdit, true);
            this.OnScreenLabelTextEdit.Size = new System.Drawing.Size(515, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.OnScreenLabelTextEdit, optionsSpelling6);
            this.OnScreenLabelTextEdit.StyleController = this.dataLayoutControl1;
            this.OnScreenLabelTextEdit.TabIndex = 56;
            this.OnScreenLabelTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.OnScreenLabelTextEdit_Validating);
            // 
            // DataEntryRequiredCheckEdit
            // 
            this.DataEntryRequiredCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "DataEntryRequired", true));
            this.DataEntryRequiredCheckEdit.Location = new System.Drawing.Point(147, -51);
            this.DataEntryRequiredCheckEdit.MenuManager = this.barManager1;
            this.DataEntryRequiredCheckEdit.Name = "DataEntryRequiredCheckEdit";
            this.DataEntryRequiredCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.DataEntryRequiredCheckEdit.Properties.ValueChecked = 1;
            this.DataEntryRequiredCheckEdit.Properties.ValueUnchecked = 0;
            this.DataEntryRequiredCheckEdit.Size = new System.Drawing.Size(94, 19);
            this.DataEntryRequiredCheckEdit.StyleController = this.dataLayoutControl1;
            this.DataEntryRequiredCheckEdit.TabIndex = 55;
            // 
            // AttributeOrderSpinEdit
            // 
            this.AttributeOrderSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "AttributeOrder", true));
            this.AttributeOrderSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AttributeOrderSpinEdit.Location = new System.Drawing.Point(147, -75);
            this.AttributeOrderSpinEdit.MenuManager = this.barManager1;
            this.AttributeOrderSpinEdit.Name = "AttributeOrderSpinEdit";
            this.AttributeOrderSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AttributeOrderSpinEdit.Properties.IsFloatValue = false;
            this.AttributeOrderSpinEdit.Properties.Mask.EditMask = "N0";
            this.AttributeOrderSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AttributeOrderSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.AttributeOrderSpinEdit.Size = new System.Drawing.Size(194, 20);
            this.AttributeOrderSpinEdit.StyleController = this.dataLayoutControl1;
            this.AttributeOrderSpinEdit.TabIndex = 57;
            // 
            // JobExpectedStartDateTextEdit
            // 
            this.JobExpectedStartDateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "JobExpectedStartDate", true));
            this.JobExpectedStartDateTextEdit.Location = new System.Drawing.Point(123, -204);
            this.JobExpectedStartDateTextEdit.MenuManager = this.barManager1;
            this.JobExpectedStartDateTextEdit.Name = "JobExpectedStartDateTextEdit";
            this.JobExpectedStartDateTextEdit.Properties.Mask.EditMask = "g";
            this.JobExpectedStartDateTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.JobExpectedStartDateTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.JobExpectedStartDateTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobExpectedStartDateTextEdit, true);
            this.JobExpectedStartDateTextEdit.Size = new System.Drawing.Size(185, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobExpectedStartDateTextEdit, optionsSpelling7);
            this.JobExpectedStartDateTextEdit.StyleController = this.dataLayoutControl1;
            this.JobExpectedStartDateTextEdit.TabIndex = 68;
            // 
            // ClientContractIDTextEdit
            // 
            this.ClientContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "ClientContractID", true));
            this.ClientContractIDTextEdit.Location = new System.Drawing.Point(117, 157);
            this.ClientContractIDTextEdit.MenuManager = this.barManager1;
            this.ClientContractIDTextEdit.Name = "ClientContractIDTextEdit";
            this.ClientContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientContractIDTextEdit, true);
            this.ClientContractIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientContractIDTextEdit, optionsSpelling8);
            this.ClientContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientContractIDTextEdit.TabIndex = 67;
            this.ClientContractIDTextEdit.TabStop = false;
            // 
            // SiteContractIDTextEdit
            // 
            this.SiteContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "SiteContractID", true));
            this.SiteContractIDTextEdit.Location = new System.Drawing.Point(117, 301);
            this.SiteContractIDTextEdit.MenuManager = this.barManager1;
            this.SiteContractIDTextEdit.Name = "SiteContractIDTextEdit";
            this.SiteContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteContractIDTextEdit, true);
            this.SiteContractIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteContractIDTextEdit, optionsSpelling9);
            this.SiteContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteContractIDTextEdit.TabIndex = 66;
            this.SiteContractIDTextEdit.TabStop = false;
            // 
            // JobTypeIDTextEdit
            // 
            this.JobTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "JobTypeID", true));
            this.JobTypeIDTextEdit.Location = new System.Drawing.Point(117, 229);
            this.JobTypeIDTextEdit.MenuManager = this.barManager1;
            this.JobTypeIDTextEdit.Name = "JobTypeIDTextEdit";
            this.JobTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobTypeIDTextEdit, true);
            this.JobTypeIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobTypeIDTextEdit, optionsSpelling10);
            this.JobTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.JobTypeIDTextEdit.TabIndex = 65;
            this.JobTypeIDTextEdit.TabStop = false;
            // 
            // JobSubTypeIDTextEdit
            // 
            this.JobSubTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "JobSubTypeID", true));
            this.JobSubTypeIDTextEdit.Location = new System.Drawing.Point(117, 214);
            this.JobSubTypeIDTextEdit.MenuManager = this.barManager1;
            this.JobSubTypeIDTextEdit.Name = "JobSubTypeIDTextEdit";
            this.JobSubTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobSubTypeIDTextEdit, true);
            this.JobSubTypeIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobSubTypeIDTextEdit, optionsSpelling11);
            this.JobSubTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.JobSubTypeIDTextEdit.TabIndex = 64;
            this.JobSubTypeIDTextEdit.TabStop = false;
            // 
            // JobSubTypeDescriptionTextEdit
            // 
            this.JobSubTypeDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "JobSubTypeDescription", true));
            this.JobSubTypeDescriptionTextEdit.Location = new System.Drawing.Point(117, 214);
            this.JobSubTypeDescriptionTextEdit.MenuManager = this.barManager1;
            this.JobSubTypeDescriptionTextEdit.Name = "JobSubTypeDescriptionTextEdit";
            this.JobSubTypeDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobSubTypeDescriptionTextEdit, true);
            this.JobSubTypeDescriptionTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobSubTypeDescriptionTextEdit, optionsSpelling12);
            this.JobSubTypeDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.JobSubTypeDescriptionTextEdit.TabIndex = 59;
            this.JobSubTypeDescriptionTextEdit.TabStop = false;
            // 
            // JobTypeDescriptionTextEdit
            // 
            this.JobTypeDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "JobTypeDescription", true));
            this.JobTypeDescriptionTextEdit.Location = new System.Drawing.Point(117, 214);
            this.JobTypeDescriptionTextEdit.MenuManager = this.barManager1;
            this.JobTypeDescriptionTextEdit.Name = "JobTypeDescriptionTextEdit";
            this.JobTypeDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobTypeDescriptionTextEdit, true);
            this.JobTypeDescriptionTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobTypeDescriptionTextEdit, optionsSpelling13);
            this.JobTypeDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.JobTypeDescriptionTextEdit.TabIndex = 58;
            this.JobTypeDescriptionTextEdit.TabStop = false;
            // 
            // ClientNameTextEdit
            // 
            this.ClientNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "ClientName", true));
            this.ClientNameTextEdit.Location = new System.Drawing.Point(117, 157);
            this.ClientNameTextEdit.MenuManager = this.barManager1;
            this.ClientNameTextEdit.Name = "ClientNameTextEdit";
            this.ClientNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameTextEdit, true);
            this.ClientNameTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameTextEdit, optionsSpelling14);
            this.ClientNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameTextEdit.TabIndex = 57;
            this.ClientNameTextEdit.TabStop = false;
            // 
            // VisitNumberTextEdit
            // 
            this.VisitNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "VisitNumber", true));
            this.VisitNumberTextEdit.Location = new System.Drawing.Point(123, -252);
            this.VisitNumberTextEdit.MenuManager = this.barManager1;
            this.VisitNumberTextEdit.Name = "VisitNumberTextEdit";
            this.VisitNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitNumberTextEdit, true);
            this.VisitNumberTextEdit.Size = new System.Drawing.Size(563, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitNumberTextEdit, optionsSpelling15);
            this.VisitNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitNumberTextEdit.TabIndex = 56;
            this.VisitNumberTextEdit.TabStop = false;
            // 
            // SiteNameTextEdit
            // 
            this.SiteNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "SiteName", true));
            this.SiteNameTextEdit.Location = new System.Drawing.Point(123, -276);
            this.SiteNameTextEdit.MenuManager = this.barManager1;
            this.SiteNameTextEdit.Name = "SiteNameTextEdit";
            this.SiteNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteNameTextEdit, true);
            this.SiteNameTextEdit.Size = new System.Drawing.Size(563, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteNameTextEdit, optionsSpelling16);
            this.SiteNameTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteNameTextEdit.TabIndex = 55;
            this.SiteNameTextEdit.TabStop = false;
            // 
            // ClientNameContractDescriptionTextEdit
            // 
            this.ClientNameContractDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "ClientNameContractDescription", true));
            this.ClientNameContractDescriptionTextEdit.Location = new System.Drawing.Point(123, -300);
            this.ClientNameContractDescriptionTextEdit.MenuManager = this.barManager1;
            this.ClientNameContractDescriptionTextEdit.Name = "ClientNameContractDescriptionTextEdit";
            this.ClientNameContractDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameContractDescriptionTextEdit, true);
            this.ClientNameContractDescriptionTextEdit.Size = new System.Drawing.Size(563, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameContractDescriptionTextEdit, optionsSpelling17);
            this.ClientNameContractDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameContractDescriptionTextEdit.TabIndex = 54;
            this.ClientNameContractDescriptionTextEdit.TabStop = false;
            // 
            // VisitIDTextEdit
            // 
            this.VisitIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "VisitID", true));
            this.VisitIDTextEdit.Location = new System.Drawing.Point(441, 253);
            this.VisitIDTextEdit.MenuManager = this.barManager1;
            this.VisitIDTextEdit.Name = "VisitIDTextEdit";
            this.VisitIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitIDTextEdit, true);
            this.VisitIDTextEdit.Size = new System.Drawing.Size(216, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitIDTextEdit, optionsSpelling18);
            this.VisitIDTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitIDTextEdit.TabIndex = 53;
            // 
            // SiteIDTextEdit
            // 
            this.SiteIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "SiteID", true));
            this.SiteIDTextEdit.Location = new System.Drawing.Point(117, 325);
            this.SiteIDTextEdit.MenuManager = this.barManager1;
            this.SiteIDTextEdit.Name = "SiteIDTextEdit";
            this.SiteIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteIDTextEdit, true);
            this.SiteIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteIDTextEdit, optionsSpelling19);
            this.SiteIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteIDTextEdit.TabIndex = 52;
            // 
            // ClientIDTextEdit
            // 
            this.ClientIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "ClientID", true));
            this.ClientIDTextEdit.Location = new System.Drawing.Point(117, 157);
            this.ClientIDTextEdit.MenuManager = this.barManager1;
            this.ClientIDTextEdit.Name = "ClientIDTextEdit";
            this.ClientIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientIDTextEdit, true);
            this.ClientIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientIDTextEdit, optionsSpelling20);
            this.ClientIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDTextEdit.TabIndex = 51;
            // 
            // JobAttributeIDTextEdit
            // 
            this.JobAttributeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "JobAttributeID", true));
            this.JobAttributeIDTextEdit.Location = new System.Drawing.Point(142, 389);
            this.JobAttributeIDTextEdit.MenuManager = this.barManager1;
            this.JobAttributeIDTextEdit.Name = "JobAttributeIDTextEdit";
            this.JobAttributeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobAttributeIDTextEdit, true);
            this.JobAttributeIDTextEdit.Size = new System.Drawing.Size(532, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobAttributeIDTextEdit, optionsSpelling21);
            this.JobAttributeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.JobAttributeIDTextEdit.TabIndex = 42;
            // 
            // JobIDTextEdit
            // 
            this.JobIDTextEdit.Location = new System.Drawing.Point(117, 157);
            this.JobIDTextEdit.MenuManager = this.barManager1;
            this.JobIDTextEdit.Name = "JobIDTextEdit";
            this.JobIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobIDTextEdit, true);
            this.JobIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobIDTextEdit, optionsSpelling22);
            this.JobIDTextEdit.StyleController = this.dataLayoutControl1;
            this.JobIDTextEdit.TabIndex = 44;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, -99);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(626, 641);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling23);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 35;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp06234OMJobAttribureEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(123, -323);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(148, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // JobTypeJobSubTypeDescriptionButtonEdit
            // 
            this.JobTypeJobSubTypeDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "JobTypeJobSubTypeDescription", true));
            this.JobTypeJobSubTypeDescriptionButtonEdit.EditValue = "";
            this.JobTypeJobSubTypeDescriptionButtonEdit.Location = new System.Drawing.Point(123, -228);
            this.JobTypeJobSubTypeDescriptionButtonEdit.MenuManager = this.barManager1;
            this.JobTypeJobSubTypeDescriptionButtonEdit.Name = "JobTypeJobSubTypeDescriptionButtonEdit";
            this.JobTypeJobSubTypeDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to open the Select Job screen", "choose", null, true)});
            this.JobTypeJobSubTypeDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.JobTypeJobSubTypeDescriptionButtonEdit.Size = new System.Drawing.Size(563, 20);
            this.JobTypeJobSubTypeDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.JobTypeJobSubTypeDescriptionButtonEdit.TabIndex = 6;
            this.JobTypeJobSubTypeDescriptionButtonEdit.TabStop = false;
            this.JobTypeJobSubTypeDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.JobTypeJobSubTypeDescriptionButtonEdit_ButtonClick);
            this.JobTypeJobSubTypeDescriptionButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.JobTypeJobSubTypeDescriptionButtonEdit_Validating);
            // 
            // MasterJobAttributeIDTextEdit
            // 
            this.MasterJobAttributeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06234OMJobAttribureEditBindingSource, "MasterJobAttributeID", true));
            this.MasterJobAttributeIDTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MasterJobAttributeIDTextEdit.Location = new System.Drawing.Point(142, 365);
            this.MasterJobAttributeIDTextEdit.MenuManager = this.barManager1;
            this.MasterJobAttributeIDTextEdit.Name = "MasterJobAttributeIDTextEdit";
            this.MasterJobAttributeIDTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.MasterJobAttributeIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.MasterJobAttributeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.MasterJobAttributeIDTextEdit, true);
            this.MasterJobAttributeIDTextEdit.Size = new System.Drawing.Size(532, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.MasterJobAttributeIDTextEdit, optionsSpelling24);
            this.MasterJobAttributeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.MasterJobAttributeIDTextEdit.TabIndex = 27;
            // 
            // ItemForClientContractID
            // 
            this.ItemForClientContractID.Control = this.ClientContractIDTextEdit;
            this.ItemForClientContractID.CustomizationFormText = "Client Contract ID:";
            this.ItemForClientContractID.Location = new System.Drawing.Point(0, 225);
            this.ItemForClientContractID.Name = "ItemForClientContractID";
            this.ItemForClientContractID.Size = new System.Drawing.Size(649, 24);
            this.ItemForClientContractID.Text = "Client Contract ID:";
            this.ItemForClientContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientIDTextEdit;
            this.ItemForClientID.CustomizationFormText = "Client ID:";
            this.ItemForClientID.Location = new System.Drawing.Point(0, 225);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(649, 24);
            this.ItemForClientID.Text = "Client ID:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.Control = this.ClientNameTextEdit;
            this.ItemForClientName.CustomizationFormText = "Client Name:";
            this.ItemForClientName.Location = new System.Drawing.Point(0, 225);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(649, 24);
            this.ItemForClientName.Text = "Client Name:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobID
            // 
            this.ItemForJobID.Control = this.JobIDTextEdit;
            this.ItemForJobID.CustomizationFormText = "Job ID:";
            this.ItemForJobID.Location = new System.Drawing.Point(0, 225);
            this.ItemForJobID.Name = "ItemForJobID";
            this.ItemForJobID.Size = new System.Drawing.Size(649, 24);
            this.ItemForJobID.Text = "Job ID:";
            this.ItemForJobID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForVisitID
            // 
            this.ItemForVisitID.Control = this.VisitIDTextEdit;
            this.ItemForVisitID.CustomizationFormText = "Visit ID:";
            this.ItemForVisitID.Location = new System.Drawing.Point(324, 321);
            this.ItemForVisitID.Name = "ItemForVisitID";
            this.ItemForVisitID.Size = new System.Drawing.Size(325, 24);
            this.ItemForVisitID.Text = "Visit ID:";
            this.ItemForVisitID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteID
            // 
            this.ItemForSiteID.Control = this.SiteIDTextEdit;
            this.ItemForSiteID.CustomizationFormText = "Site ID:";
            this.ItemForSiteID.Location = new System.Drawing.Point(0, 393);
            this.ItemForSiteID.Name = "ItemForSiteID";
            this.ItemForSiteID.Size = new System.Drawing.Size(649, 24);
            this.ItemForSiteID.Text = "Site ID:";
            this.ItemForSiteID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteContractID
            // 
            this.ItemForSiteContractID.Control = this.SiteContractIDTextEdit;
            this.ItemForSiteContractID.CustomizationFormText = "Site Contract ID:";
            this.ItemForSiteContractID.Location = new System.Drawing.Point(0, 369);
            this.ItemForSiteContractID.Name = "ItemForSiteContractID";
            this.ItemForSiteContractID.Size = new System.Drawing.Size(649, 24);
            this.ItemForSiteContractID.Text = "Site Contract ID:";
            this.ItemForSiteContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobTypeID
            // 
            this.ItemForJobTypeID.Control = this.JobTypeIDTextEdit;
            this.ItemForJobTypeID.CustomizationFormText = "Job Type ID:";
            this.ItemForJobTypeID.Location = new System.Drawing.Point(0, 297);
            this.ItemForJobTypeID.Name = "ItemForJobTypeID";
            this.ItemForJobTypeID.Size = new System.Drawing.Size(649, 24);
            this.ItemForJobTypeID.Text = "Job Type ID:";
            this.ItemForJobTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobTypeDescription
            // 
            this.ItemForJobTypeDescription.Control = this.JobTypeDescriptionTextEdit;
            this.ItemForJobTypeDescription.CustomizationFormText = "Job Type:";
            this.ItemForJobTypeDescription.Location = new System.Drawing.Point(0, 273);
            this.ItemForJobTypeDescription.Name = "ItemForJobTypeDescription";
            this.ItemForJobTypeDescription.Size = new System.Drawing.Size(649, 24);
            this.ItemForJobTypeDescription.Text = "Job Type:";
            this.ItemForJobTypeDescription.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobSubTypeID
            // 
            this.ItemForJobSubTypeID.Control = this.JobSubTypeIDTextEdit;
            this.ItemForJobSubTypeID.CustomizationFormText = "Job Sub-Type ID:";
            this.ItemForJobSubTypeID.Location = new System.Drawing.Point(0, 249);
            this.ItemForJobSubTypeID.Name = "ItemForJobSubTypeID";
            this.ItemForJobSubTypeID.Size = new System.Drawing.Size(649, 24);
            this.ItemForJobSubTypeID.Text = "Job Sub-Type ID:";
            this.ItemForJobSubTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobSubTypeDescription
            // 
            this.ItemForJobSubTypeDescription.Control = this.JobSubTypeDescriptionTextEdit;
            this.ItemForJobSubTypeDescription.CustomizationFormText = "Job Sub-Type:";
            this.ItemForJobSubTypeDescription.Location = new System.Drawing.Point(0, 225);
            this.ItemForJobSubTypeDescription.Name = "ItemForJobSubTypeDescription";
            this.ItemForJobSubTypeDescription.Size = new System.Drawing.Size(649, 24);
            this.ItemForJobSubTypeDescription.Text = "Job Sub-Type:";
            this.ItemForJobSubTypeDescription.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobAttributeID
            // 
            this.ItemForJobAttributeID.Control = this.JobAttributeIDTextEdit;
            this.ItemForJobAttributeID.CustomizationFormText = "Job Attribute ID:";
            this.ItemForJobAttributeID.Location = new System.Drawing.Point(0, 24);
            this.ItemForJobAttributeID.Name = "ItemForJobAttributeID";
            this.ItemForJobAttributeID.Size = new System.Drawing.Size(666, 24);
            this.ItemForJobAttributeID.Text = "Job Attribute ID:";
            this.ItemForJobAttributeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForMasterJobAttributeID
            // 
            this.ItemForMasterJobAttributeID.Control = this.MasterJobAttributeIDTextEdit;
            this.ItemForMasterJobAttributeID.CustomizationFormText = "Master Job Attribute ID:";
            this.ItemForMasterJobAttributeID.Location = new System.Drawing.Point(0, 0);
            this.ItemForMasterJobAttributeID.Name = "ItemForMasterJobAttributeID";
            this.ItemForMasterJobAttributeID.Size = new System.Drawing.Size(666, 24);
            this.ItemForMasterJobAttributeID.Text = "Master Job Attribute ID:";
            this.ItemForMasterJobAttributeID.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForDefaultValue
            // 
            this.ItemForDefaultValue.Control = this.DefaultValueMemoEdit;
            this.ItemForDefaultValue.CustomizationFormText = "Default Value:";
            this.ItemForDefaultValue.Location = new System.Drawing.Point(0, 239);
            this.ItemForDefaultValue.MaxSize = new System.Drawing.Size(0, 48);
            this.ItemForDefaultValue.MinSize = new System.Drawing.Size(125, 48);
            this.ItemForDefaultValue.Name = "ItemForDefaultValue";
            this.ItemForDefaultValue.Size = new System.Drawing.Size(618, 48);
            this.ItemForDefaultValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDefaultValue.Text = "Default Value:";
            this.ItemForDefaultValue.TextSize = new System.Drawing.Size(108, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, -335);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(698, 923);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForJobTypeJobSubTypeDescription,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.layoutControlGroup6,
            this.ItemForClientNameContractDescription,
            this.ItemForSiteName,
            this.ItemForVisitNumber,
            this.emptySpaceItem6,
            this.emptySpaceItem7,
            this.ItemForJobExpectedStartDate});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(678, 893);
            // 
            // ItemForJobTypeJobSubTypeDescription
            // 
            this.ItemForJobTypeJobSubTypeDescription.AllowHide = false;
            this.ItemForJobTypeJobSubTypeDescription.Control = this.JobTypeJobSubTypeDescriptionButtonEdit;
            this.ItemForJobTypeJobSubTypeDescription.CustomizationFormText = "Job Description:";
            this.ItemForJobTypeJobSubTypeDescription.Location = new System.Drawing.Point(0, 95);
            this.ItemForJobTypeJobSubTypeDescription.Name = "ItemForJobTypeJobSubTypeDescription";
            this.ItemForJobTypeJobSubTypeDescription.Size = new System.Drawing.Size(678, 24);
            this.ItemForJobTypeJobSubTypeDescription.Text = "Job Description:";
            this.ItemForJobTypeJobSubTypeDescription.TextSize = new System.Drawing.Size(108, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(111, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(111, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(111, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(263, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(415, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(111, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(152, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 154);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(678, 739);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layGrpAddress;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(654, 693);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layGrpAddress,
            this.layoutControlGroup4});
            // 
            // layGrpAddress
            // 
            this.layGrpAddress.CustomizationFormText = "Address";
            this.layGrpAddress.ExpandButtonVisible = true;
            this.layGrpAddress.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layGrpAddress.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAttributeOrder,
            this.emptySpaceItem4,
            this.ItemForDataEntryRequired,
            this.ItemForOnScreenLabel,
            this.ItemForDataTypeID,
            this.emptySpaceItem8,
            this.ItemForEditorTypeID,
            this.emptySpaceItem9,
            this.ItemForEditorMask,
            this.emptySpaceItem10,
            this.DataEntryMinValue,
            this.emptySpaceItem11,
            this.ItemForDataEntryMaxValue,
            this.emptySpaceItem12,
            this.ItemForValueRecordedMemoEdit,
            this.ItemForValueRecordedPicklist,
            this.ItemForPicklistHeaderID,
            this.ItemForValueRecordedSpinEdit,
            this.ItemForValueRecordedTextEdit,
            this.ItemForValueRecordedDateEdit,
            this.ItemForValueRecordedPictureEdit,
            this.ItemForValueRecordedCheckEdit,
            this.ItemForValueRecordedMemoExEdit,
            this.emptySpaceItem5,
            this.ItemForHideFromApp,
            this.ItemForAttributeName,
            this.ItemForValueFormulaEvaluateLocation,
            this.ItemForValueFormula,
            this.ItemForValueFormulaEvaluateOrder,
            this.emptySpaceItem13});
            this.layGrpAddress.Location = new System.Drawing.Point(0, 0);
            this.layGrpAddress.Name = "layGrpAddress";
            this.layGrpAddress.Size = new System.Drawing.Size(630, 645);
            this.layGrpAddress.Text = "Details";
            // 
            // ItemForAttributeOrder
            // 
            this.ItemForAttributeOrder.Control = this.AttributeOrderSpinEdit;
            this.ItemForAttributeOrder.CustomizationFormText = "Attribute Order:";
            this.ItemForAttributeOrder.Location = new System.Drawing.Point(0, 24);
            this.ItemForAttributeOrder.MaxSize = new System.Drawing.Size(309, 24);
            this.ItemForAttributeOrder.MinSize = new System.Drawing.Size(309, 24);
            this.ItemForAttributeOrder.Name = "ItemForAttributeOrder";
            this.ItemForAttributeOrder.Size = new System.Drawing.Size(309, 24);
            this.ItemForAttributeOrder.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForAttributeOrder.Text = "Attribute Order:";
            this.ItemForAttributeOrder.TextSize = new System.Drawing.Size(108, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(309, 24);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(321, 24);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDataEntryRequired
            // 
            this.ItemForDataEntryRequired.Control = this.DataEntryRequiredCheckEdit;
            this.ItemForDataEntryRequired.CustomizationFormText = "Required:";
            this.ItemForDataEntryRequired.Location = new System.Drawing.Point(0, 48);
            this.ItemForDataEntryRequired.MaxSize = new System.Drawing.Size(209, 23);
            this.ItemForDataEntryRequired.MinSize = new System.Drawing.Size(209, 23);
            this.ItemForDataEntryRequired.Name = "ItemForDataEntryRequired";
            this.ItemForDataEntryRequired.Size = new System.Drawing.Size(209, 23);
            this.ItemForDataEntryRequired.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDataEntryRequired.Text = "Required:";
            this.ItemForDataEntryRequired.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForOnScreenLabel
            // 
            this.ItemForOnScreenLabel.Control = this.OnScreenLabelTextEdit;
            this.ItemForOnScreenLabel.CustomizationFormText = "On-Screen Label:";
            this.ItemForOnScreenLabel.Location = new System.Drawing.Point(0, 94);
            this.ItemForOnScreenLabel.Name = "ItemForOnScreenLabel";
            this.ItemForOnScreenLabel.Size = new System.Drawing.Size(630, 24);
            this.ItemForOnScreenLabel.Text = "On-Screen Label:";
            this.ItemForOnScreenLabel.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForDataTypeID
            // 
            this.ItemForDataTypeID.Control = this.DataTypeIDGridLookUpEdit;
            this.ItemForDataTypeID.CustomizationFormText = "Data Type:";
            this.ItemForDataTypeID.Location = new System.Drawing.Point(0, 118);
            this.ItemForDataTypeID.MaxSize = new System.Drawing.Size(309, 24);
            this.ItemForDataTypeID.MinSize = new System.Drawing.Size(309, 24);
            this.ItemForDataTypeID.Name = "ItemForDataTypeID";
            this.ItemForDataTypeID.Size = new System.Drawing.Size(309, 24);
            this.ItemForDataTypeID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDataTypeID.Text = "Data Type:";
            this.ItemForDataTypeID.TextSize = new System.Drawing.Size(108, 13);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(309, 118);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(321, 24);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForEditorTypeID
            // 
            this.ItemForEditorTypeID.Control = this.EditorTypeIDGridLookUpEdit;
            this.ItemForEditorTypeID.CustomizationFormText = "Editor Type:";
            this.ItemForEditorTypeID.Location = new System.Drawing.Point(0, 142);
            this.ItemForEditorTypeID.MaxSize = new System.Drawing.Size(309, 24);
            this.ItemForEditorTypeID.MinSize = new System.Drawing.Size(309, 24);
            this.ItemForEditorTypeID.Name = "ItemForEditorTypeID";
            this.ItemForEditorTypeID.Size = new System.Drawing.Size(309, 24);
            this.ItemForEditorTypeID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForEditorTypeID.Text = "Editor Type:";
            this.ItemForEditorTypeID.TextSize = new System.Drawing.Size(108, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(309, 142);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(321, 24);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForEditorMask
            // 
            this.ItemForEditorMask.Control = this.EditorMaskTextEdit;
            this.ItemForEditorMask.CustomizationFormText = "Editor Mask:";
            this.ItemForEditorMask.Location = new System.Drawing.Point(0, 166);
            this.ItemForEditorMask.MaxSize = new System.Drawing.Size(309, 24);
            this.ItemForEditorMask.MinSize = new System.Drawing.Size(309, 24);
            this.ItemForEditorMask.Name = "ItemForEditorMask";
            this.ItemForEditorMask.Size = new System.Drawing.Size(309, 24);
            this.ItemForEditorMask.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForEditorMask.Text = "Editor Mask:";
            this.ItemForEditorMask.TextSize = new System.Drawing.Size(108, 13);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(309, 166);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(321, 24);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // DataEntryMinValue
            // 
            this.DataEntryMinValue.Control = this.DataEntryMinValueSpinEdit;
            this.DataEntryMinValue.CustomizationFormText = "Data Entry Min Value:";
            this.DataEntryMinValue.Location = new System.Drawing.Point(0, 190);
            this.DataEntryMinValue.MaxSize = new System.Drawing.Size(309, 24);
            this.DataEntryMinValue.MinSize = new System.Drawing.Size(309, 24);
            this.DataEntryMinValue.Name = "DataEntryMinValue";
            this.DataEntryMinValue.Size = new System.Drawing.Size(309, 24);
            this.DataEntryMinValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.DataEntryMinValue.Text = "Data Entry Min Value:";
            this.DataEntryMinValue.TextSize = new System.Drawing.Size(108, 13);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(309, 190);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(321, 24);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDataEntryMaxValue
            // 
            this.ItemForDataEntryMaxValue.Control = this.DataEntryMaxValueSpinEdit;
            this.ItemForDataEntryMaxValue.CustomizationFormText = "Data Entry Max Value:";
            this.ItemForDataEntryMaxValue.Location = new System.Drawing.Point(0, 214);
            this.ItemForDataEntryMaxValue.MaxSize = new System.Drawing.Size(309, 24);
            this.ItemForDataEntryMaxValue.MinSize = new System.Drawing.Size(309, 24);
            this.ItemForDataEntryMaxValue.Name = "ItemForDataEntryMaxValue";
            this.ItemForDataEntryMaxValue.Size = new System.Drawing.Size(309, 24);
            this.ItemForDataEntryMaxValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDataEntryMaxValue.Text = "Data Entry Max Value:";
            this.ItemForDataEntryMaxValue.TextSize = new System.Drawing.Size(108, 13);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(309, 214);
            this.emptySpaceItem12.MaxSize = new System.Drawing.Size(309, 0);
            this.emptySpaceItem12.MinSize = new System.Drawing.Size(309, 10);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(321, 24);
            this.emptySpaceItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForValueRecordedMemoEdit
            // 
            this.ItemForValueRecordedMemoEdit.Control = this.ValueRecordedMemoEdit;
            this.ItemForValueRecordedMemoEdit.CustomizationFormText = "Value Recorded:";
            this.ItemForValueRecordedMemoEdit.Location = new System.Drawing.Point(0, 358);
            this.ItemForValueRecordedMemoEdit.MaxSize = new System.Drawing.Size(0, 48);
            this.ItemForValueRecordedMemoEdit.MinSize = new System.Drawing.Size(125, 48);
            this.ItemForValueRecordedMemoEdit.Name = "ItemForValueRecordedMemoEdit";
            this.ItemForValueRecordedMemoEdit.Size = new System.Drawing.Size(630, 48);
            this.ItemForValueRecordedMemoEdit.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForValueRecordedMemoEdit.Text = "Value Recorded:";
            this.ItemForValueRecordedMemoEdit.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForValueRecordedPicklist
            // 
            this.ItemForValueRecordedPicklist.Control = this.ValueRecordedGridLookUpEdit;
            this.ItemForValueRecordedPicklist.CustomizationFormText = "Picklist Value:";
            this.ItemForValueRecordedPicklist.Location = new System.Drawing.Point(0, 262);
            this.ItemForValueRecordedPicklist.Name = "ItemForValueRecordedPicklist";
            this.ItemForValueRecordedPicklist.Size = new System.Drawing.Size(630, 24);
            this.ItemForValueRecordedPicklist.Text = "Picklist Value:";
            this.ItemForValueRecordedPicklist.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForPicklistHeaderID
            // 
            this.ItemForPicklistHeaderID.Control = this.PicklistHeaderIDGridLookUpEdit;
            this.ItemForPicklistHeaderID.CustomizationFormText = "Picklist Header:";
            this.ItemForPicklistHeaderID.Location = new System.Drawing.Point(0, 238);
            this.ItemForPicklistHeaderID.Name = "ItemForPicklistHeaderID";
            this.ItemForPicklistHeaderID.Size = new System.Drawing.Size(630, 24);
            this.ItemForPicklistHeaderID.Text = "Picklist Header:";
            this.ItemForPicklistHeaderID.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForValueRecordedSpinEdit
            // 
            this.ItemForValueRecordedSpinEdit.Control = this.ValueRecordedSpinEdit;
            this.ItemForValueRecordedSpinEdit.CustomizationFormText = "Value Recorded:";
            this.ItemForValueRecordedSpinEdit.Location = new System.Drawing.Point(0, 430);
            this.ItemForValueRecordedSpinEdit.Name = "ItemForValueRecordedSpinEdit";
            this.ItemForValueRecordedSpinEdit.Size = new System.Drawing.Size(630, 24);
            this.ItemForValueRecordedSpinEdit.Text = "Value Recorded:";
            this.ItemForValueRecordedSpinEdit.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForValueRecordedTextEdit
            // 
            this.ItemForValueRecordedTextEdit.Control = this.ValueRecordedTextEdit;
            this.ItemForValueRecordedTextEdit.CustomizationFormText = "Value Recorded:";
            this.ItemForValueRecordedTextEdit.Location = new System.Drawing.Point(0, 454);
            this.ItemForValueRecordedTextEdit.Name = "ItemForValueRecordedTextEdit";
            this.ItemForValueRecordedTextEdit.Size = new System.Drawing.Size(630, 24);
            this.ItemForValueRecordedTextEdit.Text = "Value Recorded:";
            this.ItemForValueRecordedTextEdit.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForValueRecordedDateEdit
            // 
            this.ItemForValueRecordedDateEdit.Control = this.ValueRecordedDateEdit;
            this.ItemForValueRecordedDateEdit.CustomizationFormText = "Value Recorded:";
            this.ItemForValueRecordedDateEdit.Location = new System.Drawing.Point(0, 478);
            this.ItemForValueRecordedDateEdit.Name = "ItemForValueRecordedDateEdit";
            this.ItemForValueRecordedDateEdit.Size = new System.Drawing.Size(630, 24);
            this.ItemForValueRecordedDateEdit.Text = "Value Recorded:";
            this.ItemForValueRecordedDateEdit.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForValueRecordedPictureEdit
            // 
            this.ItemForValueRecordedPictureEdit.Control = this.ValueRecordedPictureEdit;
            this.ItemForValueRecordedPictureEdit.CustomizationFormText = "Value Recorded:";
            this.ItemForValueRecordedPictureEdit.Location = new System.Drawing.Point(0, 502);
            this.ItemForValueRecordedPictureEdit.MaxSize = new System.Drawing.Size(0, 120);
            this.ItemForValueRecordedPictureEdit.MinSize = new System.Drawing.Size(135, 120);
            this.ItemForValueRecordedPictureEdit.Name = "ItemForValueRecordedPictureEdit";
            this.ItemForValueRecordedPictureEdit.Size = new System.Drawing.Size(630, 120);
            this.ItemForValueRecordedPictureEdit.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForValueRecordedPictureEdit.Text = "Value Recorded:";
            this.ItemForValueRecordedPictureEdit.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForValueRecordedCheckEdit
            // 
            this.ItemForValueRecordedCheckEdit.Control = this.ValueRecordedCheckEdit;
            this.ItemForValueRecordedCheckEdit.CustomizationFormText = "Value Recorded:";
            this.ItemForValueRecordedCheckEdit.Location = new System.Drawing.Point(0, 622);
            this.ItemForValueRecordedCheckEdit.Name = "ItemForValueRecordedCheckEdit";
            this.ItemForValueRecordedCheckEdit.Size = new System.Drawing.Size(630, 23);
            this.ItemForValueRecordedCheckEdit.Text = "Value Recorded:";
            this.ItemForValueRecordedCheckEdit.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForValueRecordedMemoExEdit
            // 
            this.ItemForValueRecordedMemoExEdit.Control = this.ValueRecordedMemoExEdit;
            this.ItemForValueRecordedMemoExEdit.CustomizationFormText = "Value Recorded:";
            this.ItemForValueRecordedMemoExEdit.Location = new System.Drawing.Point(0, 406);
            this.ItemForValueRecordedMemoExEdit.Name = "ItemForValueRecordedMemoExEdit";
            this.ItemForValueRecordedMemoExEdit.Size = new System.Drawing.Size(630, 24);
            this.ItemForValueRecordedMemoExEdit.Text = "Value Recorded:";
            this.ItemForValueRecordedMemoExEdit.TextSize = new System.Drawing.Size(108, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(209, 48);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(421, 46);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForHideFromApp
            // 
            this.ItemForHideFromApp.Control = this.HideFromAppCheckEdit;
            this.ItemForHideFromApp.Location = new System.Drawing.Point(0, 71);
            this.ItemForHideFromApp.MaxSize = new System.Drawing.Size(209, 23);
            this.ItemForHideFromApp.MinSize = new System.Drawing.Size(209, 23);
            this.ItemForHideFromApp.Name = "ItemForHideFromApp";
            this.ItemForHideFromApp.Size = new System.Drawing.Size(209, 23);
            this.ItemForHideFromApp.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForHideFromApp.Text = "Hide From App:";
            this.ItemForHideFromApp.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForAttributeName
            // 
            this.ItemForAttributeName.Control = this.AttributeNameTextEdit;
            this.ItemForAttributeName.Location = new System.Drawing.Point(0, 0);
            this.ItemForAttributeName.Name = "ItemForAttributeName";
            this.ItemForAttributeName.Size = new System.Drawing.Size(630, 24);
            this.ItemForAttributeName.Text = "Attribute Name:";
            this.ItemForAttributeName.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForValueFormulaEvaluateLocation
            // 
            this.ItemForValueFormulaEvaluateLocation.Control = this.ValueFormulaEvaluateLocationGridLookUpEdit;
            this.ItemForValueFormulaEvaluateLocation.Location = new System.Drawing.Point(0, 310);
            this.ItemForValueFormulaEvaluateLocation.Name = "ItemForValueFormulaEvaluateLocation";
            this.ItemForValueFormulaEvaluateLocation.Size = new System.Drawing.Size(630, 24);
            this.ItemForValueFormulaEvaluateLocation.Text = "Evaluate Location:";
            this.ItemForValueFormulaEvaluateLocation.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForValueFormula
            // 
            this.ItemForValueFormula.Control = this.ValueFormulaMemoExEdit;
            this.ItemForValueFormula.Location = new System.Drawing.Point(0, 286);
            this.ItemForValueFormula.Name = "ItemForValueFormula";
            this.ItemForValueFormula.Size = new System.Drawing.Size(630, 24);
            this.ItemForValueFormula.Text = "Calculated Formula:";
            this.ItemForValueFormula.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForValueFormulaEvaluateOrder
            // 
            this.ItemForValueFormulaEvaluateOrder.Control = this.ValueFormulaEvaluateOrderSpinEdit;
            this.ItemForValueFormulaEvaluateOrder.Location = new System.Drawing.Point(0, 334);
            this.ItemForValueFormulaEvaluateOrder.MaxSize = new System.Drawing.Size(310, 24);
            this.ItemForValueFormulaEvaluateOrder.MinSize = new System.Drawing.Size(310, 24);
            this.ItemForValueFormulaEvaluateOrder.Name = "ItemForValueFormulaEvaluateOrder";
            this.ItemForValueFormulaEvaluateOrder.Size = new System.Drawing.Size(310, 24);
            this.ItemForValueFormulaEvaluateOrder.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForValueFormulaEvaluateOrder.Text = "Evaluate Order:";
            this.ItemForValueFormulaEvaluateOrder.TextSize = new System.Drawing.Size(108, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(310, 334);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(320, 24);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImage")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(630, 645);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Comments:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(630, 645);
            this.ItemForRemarks.Text = "Comments:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // ItemForClientNameContractDescription
            // 
            this.ItemForClientNameContractDescription.Control = this.ClientNameContractDescriptionTextEdit;
            this.ItemForClientNameContractDescription.CustomizationFormText = "Client \\ Contract:";
            this.ItemForClientNameContractDescription.Location = new System.Drawing.Point(0, 23);
            this.ItemForClientNameContractDescription.Name = "ItemForClientNameContractDescription";
            this.ItemForClientNameContractDescription.Size = new System.Drawing.Size(678, 24);
            this.ItemForClientNameContractDescription.Text = "Client \\ Contract:";
            this.ItemForClientNameContractDescription.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForSiteName
            // 
            this.ItemForSiteName.Control = this.SiteNameTextEdit;
            this.ItemForSiteName.CustomizationFormText = "Site Name:";
            this.ItemForSiteName.Location = new System.Drawing.Point(0, 47);
            this.ItemForSiteName.Name = "ItemForSiteName";
            this.ItemForSiteName.Size = new System.Drawing.Size(678, 24);
            this.ItemForSiteName.Text = "Site Name:";
            this.ItemForSiteName.TextSize = new System.Drawing.Size(108, 13);
            // 
            // ItemForVisitNumber
            // 
            this.ItemForVisitNumber.Control = this.VisitNumberTextEdit;
            this.ItemForVisitNumber.CustomizationFormText = "Visit Number:";
            this.ItemForVisitNumber.Location = new System.Drawing.Point(0, 71);
            this.ItemForVisitNumber.Name = "ItemForVisitNumber";
            this.ItemForVisitNumber.Size = new System.Drawing.Size(678, 24);
            this.ItemForVisitNumber.Text = "Visit Number:";
            this.ItemForVisitNumber.TextSize = new System.Drawing.Size(108, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 143);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 11);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 11);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(678, 11);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(300, 119);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(378, 24);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForJobExpectedStartDate
            // 
            this.ItemForJobExpectedStartDate.Control = this.JobExpectedStartDateTextEdit;
            this.ItemForJobExpectedStartDate.CustomizationFormText = "Job Expected Start Date:";
            this.ItemForJobExpectedStartDate.Location = new System.Drawing.Point(0, 119);
            this.ItemForJobExpectedStartDate.MaxSize = new System.Drawing.Size(300, 24);
            this.ItemForJobExpectedStartDate.MinSize = new System.Drawing.Size(300, 24);
            this.ItemForJobExpectedStartDate.Name = "ItemForJobExpectedStartDate";
            this.ItemForJobExpectedStartDate.Size = new System.Drawing.Size(300, 24);
            this.ItemForJobExpectedStartDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForJobExpectedStartDate.Text = "Expected Start Date:";
            this.ItemForJobExpectedStartDate.TextSize = new System.Drawing.Size(108, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 893);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(678, 10);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(678, 10);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06234_OM_Job_Attribure_EditTableAdapter
            // 
            this.sp06234_OM_Job_Attribure_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp06206_OM_Attribute_DataTypes_With_BlankTableAdapter
            // 
            this.sp06206_OM_Attribute_DataTypes_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06207_OM_Attribute_EditorTypes_With_BlankTableAdapter
            // 
            this.sp06207_OM_Attribute_EditorTypes_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06202_OM_Attribute_Picklist_Headers_With_BlankTableAdapter
            // 
            this.sp06202_OM_Attribute_Picklist_Headers_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06236_OM_Attribute_Picklist_Items_With_BlankTableAdapter
            // 
            this.sp06236_OM_Attribute_Picklist_Items_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06481_OM_Attribute_Formula_Evaluate_Location_ListTableAdapter
            // 
            this.sp06481_OM_Attribute_Formula_Evaluate_Location_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Job_Attribute_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(715, 644);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Job_Attribute_Edit";
            this.Text = "Edit Job Attribute";
            this.Activated += new System.EventHandler(this.frm_OM_Job_Attribute_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Job_Attribute_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Job_Attribute_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ValueFormulaEvaluateOrderSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06234OMJobAttribureEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueFormulaEvaluateLocationGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06481OMAttributeFormulaEvaluateLocationListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueFormulaMemoExEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AttributeNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HideFromAppCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueRecordedMemoExEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueRecordedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueRecordedPictureEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueRecordedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueRecordedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueRecordedTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueRecordedSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicklistHeaderIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06202OMAttributePicklistHeadersWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueRecordedGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06236OMAttributePicklistItemsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueRecordedMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultValueMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataEntryMaxValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataEntryMinValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditorMaskTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditorTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06207OMAttributeEditorTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEditorType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06206OMAttributeDataTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OnScreenLabelTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataEntryRequiredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AttributeOrderSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobExpectedStartDateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameContractDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobAttributeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeJobSubTypeDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MasterJobAttributeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobAttributeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMasterJobAttributeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeJobSubTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAttributeOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDataEntryRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOnScreenLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDataTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEditorTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEditorMask)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataEntryMinValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDataEntryMaxValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueRecordedMemoEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueRecordedPicklist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPicklistHeaderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueRecordedSpinEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueRecordedTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueRecordedDateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueRecordedPictureEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueRecordedCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueRecordedMemoExEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHideFromApp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAttributeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueFormulaEvaluateLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueFormula)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValueFormulaEvaluateOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientNameContractDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobExpectedStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobTypeJobSubTypeDescription;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.ButtonEdit JobTypeJobSubTypeDescriptionButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMasterJobAttributeID;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit MasterJobAttributeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpAddress;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DevExpress.XtraEditors.TextEdit JobIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobID;
        private DevExpress.XtraEditors.TextEdit JobAttributeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobAttributeID;
        private DataSet_OM_Job dataSet_OM_Job;
        private DevExpress.XtraEditors.TextEdit SiteIDTextEdit;
        private DevExpress.XtraEditors.TextEdit ClientIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteID;
        private DevExpress.XtraEditors.TextEdit VisitIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitID;
        private DevExpress.XtraEditors.TextEdit ClientNameContractDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientNameContractDescription;
        private DevExpress.XtraEditors.TextEdit SiteNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteName;
        private DevExpress.XtraEditors.TextEdit VisitNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitNumber;
        private DevExpress.XtraEditors.TextEdit ClientNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraEditors.TextEdit JobTypeDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobTypeDescription;
        private DevExpress.XtraEditors.TextEdit JobSubTypeDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobSubTypeDescription;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.TextEdit JobTypeIDTextEdit;
        private DevExpress.XtraEditors.TextEdit JobSubTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobSubTypeID;
        private DevExpress.XtraEditors.TextEdit ClientContractIDTextEdit;
        private DevExpress.XtraEditors.TextEdit SiteContractIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientContractID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteContractID;
        private DevExpress.XtraBars.BarButtonItem bbiLinkedDocuments;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.TextEdit JobExpectedStartDateTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobExpectedStartDate;
        private System.Windows.Forms.BindingSource sp06234OMJobAttribureEditBindingSource;
        private DataSet_OM_JobTableAdapters.sp06234_OM_Job_Attribure_EditTableAdapter sp06234_OM_Job_Attribure_EditTableAdapter;
        private DevExpress.XtraEditors.SpinEdit AttributeOrderSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAttributeOrder;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.CheckEdit DataEntryRequiredCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDataEntryRequired;
        private DevExpress.XtraEditors.TextEdit OnScreenLabelTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOnScreenLabel;
        private DevExpress.XtraEditors.GridLookUpEdit DataTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDataTypeID;
        private System.Windows.Forms.BindingSource sp06206OMAttributeDataTypesWithBlankBindingSource;
        private DataSet_OM_JobTableAdapters.sp06206_OM_Attribute_DataTypes_With_BlankTableAdapter sp06206_OM_Attribute_DataTypes_With_BlankTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraEditors.GridLookUpEdit EditorTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewEditorType;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEditorTypeID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private System.Windows.Forms.BindingSource sp06207OMAttributeEditorTypesWithBlankBindingSource;
        private DataSet_OM_JobTableAdapters.sp06207_OM_Attribute_EditorTypes_With_BlankTableAdapter sp06207_OM_Attribute_EditorTypes_With_BlankTableAdapter;
        private DevExpress.XtraEditors.TextEdit EditorMaskTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEditorMask;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraEditors.SpinEdit DataEntryMinValueSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem DataEntryMinValue;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraEditors.SpinEdit DataEntryMaxValueSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDataEntryMaxValue;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraEditors.MemoEdit DefaultValueMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDefaultValue;
        private DevExpress.XtraEditors.MemoEdit ValueRecordedMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForValueRecordedMemoEdit;
        private DevExpress.XtraEditors.GridLookUpEdit ValueRecordedGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForValueRecordedPicklist;
        private System.Windows.Forms.BindingSource sp06202OMAttributePicklistHeadersWithBlankBindingSource;
        private DataSet_OM_JobTableAdapters.sp06202_OM_Attribute_Picklist_Headers_With_BlankTableAdapter sp06202_OM_Attribute_Picklist_Headers_With_BlankTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit PicklistHeaderIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPicklistHeaderID;
        private System.Windows.Forms.BindingSource sp06236OMAttributePicklistItemsWithBlankBindingSource;
        private DataSet_OM_JobTableAdapters.sp06236_OM_Attribute_Picklist_Items_With_BlankTableAdapter sp06236_OM_Attribute_Picklist_Items_With_BlankTableAdapter;
        private DevExpress.XtraEditors.SpinEdit ValueRecordedSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForValueRecordedSpinEdit;
        private DevExpress.XtraEditors.TextEdit ValueRecordedTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForValueRecordedTextEdit;
        private DevExpress.XtraEditors.DateEdit ValueRecordedDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForValueRecordedDateEdit;
        private DevExpress.XtraEditors.PictureEdit ValueRecordedPictureEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForValueRecordedPictureEdit;
        private DevExpress.XtraEditors.CheckEdit ValueRecordedCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForValueRecordedCheckEdit;
        private DevExpress.XtraEditors.MemoExEdit ValueRecordedMemoExEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForValueRecordedMemoExEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colActive1;
        private DevExpress.XtraEditors.CheckEdit HideFromAppCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHideFromApp;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.TextEdit AttributeNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAttributeName;
        private DevExpress.XtraEditors.MemoExEdit ValueFormulaMemoExEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForValueFormula;
        private DevExpress.XtraEditors.GridLookUpEdit ValueFormulaEvaluateLocationGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraLayout.LayoutControlItem ItemForValueFormulaEvaluateLocation;
        private System.Windows.Forms.BindingSource sp06481OMAttributeFormulaEvaluateLocationListBindingSource;
        private DataSet_OM_JobTableAdapters.sp06481_OM_Attribute_Formula_Evaluate_Location_ListTableAdapter sp06481_OM_Attribute_Formula_Evaluate_Location_ListTableAdapter;
        private DevExpress.XtraEditors.SpinEdit ValueFormulaEvaluateOrderSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForValueFormulaEvaluateOrder;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
    }
}

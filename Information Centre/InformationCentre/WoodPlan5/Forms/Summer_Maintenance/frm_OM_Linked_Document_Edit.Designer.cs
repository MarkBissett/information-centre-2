namespace WoodPlan5
{
    partial class frm_OM_Linked_Document_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Linked_Document_Edit));
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.LinkedDocumentSubTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp06045OMLinkedDocumentEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Summer_DataEntry = new WoodPlan5.DataSet_GC_Summer_DataEntry();
            this.LinkedDocumentSubTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LinkedDocumentTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LinkedDocumentTypeButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.LinkedToRecordIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LinkedDocumentIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddedByStaffNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LinkedRecordDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.DocumentExtensionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DateAddedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.DocumentPathButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.AddedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForLinkedDocumentID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedDocumentTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedDocumentSubTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToRecordID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDocumentPath = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDateAdded = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDocumentExtension = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddedByStaffName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedRecordDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedDocumentType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedDocumentSubType = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.sp06045_OM_Linked_Document_EditTableAdapter = new WoodPlan5.DataSet_GC_Summer_DataEntryTableAdapters.sp06045_OM_Linked_Document_EditTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedDocumentSubTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06045OMLinkedDocumentEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Summer_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedDocumentSubTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedDocumentTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedDocumentTypeButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToRecordIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedDocumentIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddedByStaffNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedRecordDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentExtensionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentPathButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedDocumentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedDocumentTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedDocumentSubTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToRecordID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDocumentPath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateAdded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDocumentExtension)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddedByStaffName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedRecordDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedDocumentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedDocumentSubType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 502);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 476);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem4.Text = "Save Button - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiFormSave.SuperTip = superToolTip4;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem5.Text = "Cancel Button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bbiFormCancel.SuperTip = superToolTip5;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Form Mode - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.barStaticItemFormMode.SuperTip = superToolTip6;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 502);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 476);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "info_16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.LinkedDocumentSubTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedDocumentSubTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedDocumentTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedDocumentTypeButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToRecordIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedDocumentIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddedByStaffNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedRecordDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.DocumentExtensionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DateAddedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.DocumentPathButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.AddedByStaffIDTextEdit);
            this.dataLayoutControl1.DataSource = this.sp06045OMLinkedDocumentEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLinkedDocumentID,
            this.ItemForAddedByStaffID,
            this.ItemForLinkedDocumentTypeID,
            this.ItemForLinkedDocumentSubTypeID,
            this.ItemForLinkedToRecordID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(738, 185, 740, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 476);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // LinkedDocumentSubTypeTextEdit
            // 
            this.LinkedDocumentSubTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06045OMLinkedDocumentEditBindingSource, "LinkedDocumentSubType", true));
            this.LinkedDocumentSubTypeTextEdit.Location = new System.Drawing.Point(122, 60);
            this.LinkedDocumentSubTypeTextEdit.MenuManager = this.barManager1;
            this.LinkedDocumentSubTypeTextEdit.Name = "LinkedDocumentSubTypeTextEdit";
            this.LinkedDocumentSubTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedDocumentSubTypeTextEdit, true);
            this.LinkedDocumentSubTypeTextEdit.Size = new System.Drawing.Size(494, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedDocumentSubTypeTextEdit, optionsSpelling11);
            this.LinkedDocumentSubTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedDocumentSubTypeTextEdit.TabIndex = 23;
            // 
            // sp06045OMLinkedDocumentEditBindingSource
            // 
            this.sp06045OMLinkedDocumentEditBindingSource.DataMember = "sp06045_OM_Linked_Document_Edit";
            this.sp06045OMLinkedDocumentEditBindingSource.DataSource = this.dataSet_GC_Summer_DataEntry;
            // 
            // dataSet_GC_Summer_DataEntry
            // 
            this.dataSet_GC_Summer_DataEntry.DataSetName = "DataSet_GC_Summer_DataEntry";
            this.dataSet_GC_Summer_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // LinkedDocumentSubTypeIDTextEdit
            // 
            this.LinkedDocumentSubTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06045OMLinkedDocumentEditBindingSource, "LinkedDocumentSubTypeID", true));
            this.LinkedDocumentSubTypeIDTextEdit.Location = new System.Drawing.Point(129, 60);
            this.LinkedDocumentSubTypeIDTextEdit.MenuManager = this.barManager1;
            this.LinkedDocumentSubTypeIDTextEdit.Name = "LinkedDocumentSubTypeIDTextEdit";
            this.LinkedDocumentSubTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedDocumentSubTypeIDTextEdit, true);
            this.LinkedDocumentSubTypeIDTextEdit.Size = new System.Drawing.Size(487, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedDocumentSubTypeIDTextEdit, optionsSpelling1);
            this.LinkedDocumentSubTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedDocumentSubTypeIDTextEdit.TabIndex = 22;
            // 
            // LinkedDocumentTypeIDTextEdit
            // 
            this.LinkedDocumentTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06045OMLinkedDocumentEditBindingSource, "LinkedDocumentTypeID", true));
            this.LinkedDocumentTypeIDTextEdit.Location = new System.Drawing.Point(122, 60);
            this.LinkedDocumentTypeIDTextEdit.MenuManager = this.barManager1;
            this.LinkedDocumentTypeIDTextEdit.Name = "LinkedDocumentTypeIDTextEdit";
            this.LinkedDocumentTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedDocumentTypeIDTextEdit, true);
            this.LinkedDocumentTypeIDTextEdit.Size = new System.Drawing.Size(494, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedDocumentTypeIDTextEdit, optionsSpelling2);
            this.LinkedDocumentTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedDocumentTypeIDTextEdit.TabIndex = 21;
            // 
            // LinkedDocumentTypeButtonEdit
            // 
            this.LinkedDocumentTypeButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06045OMLinkedDocumentEditBindingSource, "LinkedDocumentType", true));
            this.LinkedDocumentTypeButtonEdit.Location = new System.Drawing.Point(122, 36);
            this.LinkedDocumentTypeButtonEdit.MenuManager = this.barManager1;
            this.LinkedDocumentTypeButtonEdit.Name = "LinkedDocumentTypeButtonEdit";
            this.LinkedDocumentTypeButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Click to Open Select Linked Document Type \\ Sub-Type screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LinkedDocumentTypeButtonEdit.Size = new System.Drawing.Size(494, 20);
            this.LinkedDocumentTypeButtonEdit.StyleController = this.dataLayoutControl1;
            this.LinkedDocumentTypeButtonEdit.TabIndex = 20;
            this.LinkedDocumentTypeButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LinkedDocumentTypeButtonEdit_ButtonClick);
            this.LinkedDocumentTypeButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.LinkedDocumentTypeButtonEdit_Validating);
            // 
            // LinkedToRecordIDTextEdit
            // 
            this.LinkedToRecordIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06045OMLinkedDocumentEditBindingSource, "LinkedToRecordID", true));
            this.LinkedToRecordIDTextEdit.Location = new System.Drawing.Point(122, 132);
            this.LinkedToRecordIDTextEdit.MenuManager = this.barManager1;
            this.LinkedToRecordIDTextEdit.Name = "LinkedToRecordIDTextEdit";
            this.LinkedToRecordIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToRecordIDTextEdit, true);
            this.LinkedToRecordIDTextEdit.Size = new System.Drawing.Size(494, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToRecordIDTextEdit, optionsSpelling3);
            this.LinkedToRecordIDTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToRecordIDTextEdit.TabIndex = 19;
            // 
            // LinkedDocumentIDTextEdit
            // 
            this.LinkedDocumentIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06045OMLinkedDocumentEditBindingSource, "LinkedDocumentID", true));
            this.LinkedDocumentIDTextEdit.Location = new System.Drawing.Point(122, 182);
            this.LinkedDocumentIDTextEdit.MenuManager = this.barManager1;
            this.LinkedDocumentIDTextEdit.Name = "LinkedDocumentIDTextEdit";
            this.LinkedDocumentIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedDocumentIDTextEdit, true);
            this.LinkedDocumentIDTextEdit.Size = new System.Drawing.Size(494, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedDocumentIDTextEdit, optionsSpelling4);
            this.LinkedDocumentIDTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedDocumentIDTextEdit.TabIndex = 18;
            // 
            // AddedByStaffNameTextEdit
            // 
            this.AddedByStaffNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06045OMLinkedDocumentEditBindingSource, "AddedByStaffName", true));
            this.AddedByStaffNameTextEdit.Location = new System.Drawing.Point(146, 288);
            this.AddedByStaffNameTextEdit.MenuManager = this.barManager1;
            this.AddedByStaffNameTextEdit.Name = "AddedByStaffNameTextEdit";
            this.AddedByStaffNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddedByStaffNameTextEdit, true);
            this.AddedByStaffNameTextEdit.Size = new System.Drawing.Size(446, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddedByStaffNameTextEdit, optionsSpelling5);
            this.AddedByStaffNameTextEdit.StyleController = this.dataLayoutControl1;
            this.AddedByStaffNameTextEdit.TabIndex = 15;
            // 
            // LinkedRecordDescriptionButtonEdit
            // 
            this.LinkedRecordDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06045OMLinkedDocumentEditBindingSource, "LinkedRecordDescription", true));
            this.LinkedRecordDescriptionButtonEdit.Location = new System.Drawing.Point(122, 84);
            this.LinkedRecordDescriptionButtonEdit.MenuManager = this.barManager1;
            this.LinkedRecordDescriptionButtonEdit.Name = "LinkedRecordDescriptionButtonEdit";
            this.LinkedRecordDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Select", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Select From List", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LinkedRecordDescriptionButtonEdit.Properties.MaxLength = 100;
            this.LinkedRecordDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.LinkedRecordDescriptionButtonEdit.Size = new System.Drawing.Size(494, 20);
            this.LinkedRecordDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.LinkedRecordDescriptionButtonEdit.TabIndex = 14;
            this.LinkedRecordDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LinkedRecordDescriptionButtonEdit_ButtonClick);
            this.LinkedRecordDescriptionButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.LinkedRecordDescriptionButtonEdit_Validating);
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp06045OMLinkedDocumentEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(124, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 13;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // DocumentExtensionTextEdit
            // 
            this.DocumentExtensionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06045OMLinkedDocumentEditBindingSource, "DocumentExtension", true));
            this.DocumentExtensionTextEdit.Location = new System.Drawing.Point(146, 240);
            this.DocumentExtensionTextEdit.MenuManager = this.barManager1;
            this.DocumentExtensionTextEdit.Name = "DocumentExtensionTextEdit";
            this.DocumentExtensionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DocumentExtensionTextEdit, true);
            this.DocumentExtensionTextEdit.Size = new System.Drawing.Size(446, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.DocumentExtensionTextEdit, optionsSpelling6);
            this.DocumentExtensionTextEdit.StyleController = this.dataLayoutControl1;
            this.DocumentExtensionTextEdit.TabIndex = 8;
            // 
            // DescriptionTextEdit
            // 
            this.DescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06045OMLinkedDocumentEditBindingSource, "Description", true));
            this.DescriptionTextEdit.Location = new System.Drawing.Point(122, 108);
            this.DescriptionTextEdit.MenuManager = this.barManager1;
            this.DescriptionTextEdit.Name = "DescriptionTextEdit";
            this.DescriptionTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DescriptionTextEdit, true);
            this.DescriptionTextEdit.Size = new System.Drawing.Size(494, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.DescriptionTextEdit, optionsSpelling7);
            this.DescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.DescriptionTextEdit.TabIndex = 9;
            this.DescriptionTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DescriptionTextEdit_Validating);
            // 
            // DateAddedDateEdit
            // 
            this.DateAddedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06045OMLinkedDocumentEditBindingSource, "DateAdded", true));
            this.DateAddedDateEdit.EditValue = null;
            this.DateAddedDateEdit.Location = new System.Drawing.Point(146, 264);
            this.DateAddedDateEdit.MenuManager = this.barManager1;
            this.DateAddedDateEdit.Name = "DateAddedDateEdit";
            this.DateAddedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateAddedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateAddedDateEdit.Properties.Mask.EditMask = "g";
            this.DateAddedDateEdit.Properties.ReadOnly = true;
            this.DateAddedDateEdit.Size = new System.Drawing.Size(446, 20);
            this.DateAddedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateAddedDateEdit.TabIndex = 11;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06045OMLinkedDocumentEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 240);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(556, 190);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling8);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 12;
            // 
            // DocumentPathButtonEdit
            // 
            this.DocumentPathButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06045OMLinkedDocumentEditBindingSource, "DocumentPath", true));
            this.DocumentPathButtonEdit.Location = new System.Drawing.Point(122, 132);
            this.DocumentPathButtonEdit.MenuManager = this.barManager1;
            this.DocumentPathButtonEdit.Name = "DocumentPathButtonEdit";
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            editorButtonImageOptions2.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.DocumentPathButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose File", -1, true, true, true, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Select File", "select file", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View File", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", "view file", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.DocumentPathButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.DocumentPathButtonEdit.Size = new System.Drawing.Size(494, 22);
            this.DocumentPathButtonEdit.StyleController = this.dataLayoutControl1;
            this.DocumentPathButtonEdit.TabIndex = 7;
            this.DocumentPathButtonEdit.TabStop = false;
            this.DocumentPathButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.DocumentPathButtonEdit_ButtonClick);
            this.DocumentPathButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DocumentPathButtonEdit_Validating);
            // 
            // AddedByStaffIDTextEdit
            // 
            this.AddedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06045OMLinkedDocumentEditBindingSource, "AddedByStaffID", true));
            this.AddedByStaffIDTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AddedByStaffIDTextEdit.Location = new System.Drawing.Point(122, 182);
            this.AddedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.AddedByStaffIDTextEdit.Name = "AddedByStaffIDTextEdit";
            this.AddedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddedByStaffIDTextEdit, true);
            this.AddedByStaffIDTextEdit.Size = new System.Drawing.Size(494, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddedByStaffIDTextEdit, optionsSpelling9);
            this.AddedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.AddedByStaffIDTextEdit.TabIndex = 10;
            this.AddedByStaffIDTextEdit.TabStop = false;
            // 
            // ItemForLinkedDocumentID
            // 
            this.ItemForLinkedDocumentID.Control = this.LinkedDocumentIDTextEdit;
            this.ItemForLinkedDocumentID.CustomizationFormText = "Linked Document ID:";
            this.ItemForLinkedDocumentID.Location = new System.Drawing.Point(0, 170);
            this.ItemForLinkedDocumentID.Name = "ItemForLinkedDocumentID";
            this.ItemForLinkedDocumentID.Size = new System.Drawing.Size(608, 24);
            this.ItemForLinkedDocumentID.Text = "Linked Document ID:";
            this.ItemForLinkedDocumentID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAddedByStaffID
            // 
            this.ItemForAddedByStaffID.Control = this.AddedByStaffIDTextEdit;
            this.ItemForAddedByStaffID.CustomizationFormText = "Added By Staff ID:";
            this.ItemForAddedByStaffID.Location = new System.Drawing.Point(0, 170);
            this.ItemForAddedByStaffID.Name = "ItemForAddedByStaffID";
            this.ItemForAddedByStaffID.Size = new System.Drawing.Size(608, 24);
            this.ItemForAddedByStaffID.Text = "Added By Staff ID:";
            this.ItemForAddedByStaffID.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForLinkedDocumentTypeID
            // 
            this.ItemForLinkedDocumentTypeID.Control = this.LinkedDocumentTypeIDTextEdit;
            this.ItemForLinkedDocumentTypeID.CustomizationFormText = "Document Type ID:";
            this.ItemForLinkedDocumentTypeID.Location = new System.Drawing.Point(0, 48);
            this.ItemForLinkedDocumentTypeID.Name = "ItemForLinkedDocumentTypeID";
            this.ItemForLinkedDocumentTypeID.Size = new System.Drawing.Size(608, 24);
            this.ItemForLinkedDocumentTypeID.Text = "Document Type ID:";
            this.ItemForLinkedDocumentTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLinkedDocumentSubTypeID
            // 
            this.ItemForLinkedDocumentSubTypeID.Control = this.LinkedDocumentSubTypeIDTextEdit;
            this.ItemForLinkedDocumentSubTypeID.CustomizationFormText = "Document Sub Type ID:";
            this.ItemForLinkedDocumentSubTypeID.Location = new System.Drawing.Point(0, 48);
            this.ItemForLinkedDocumentSubTypeID.Name = "ItemForLinkedDocumentSubTypeID";
            this.ItemForLinkedDocumentSubTypeID.Size = new System.Drawing.Size(608, 24);
            this.ItemForLinkedDocumentSubTypeID.Text = "Document Sub Type ID:";
            this.ItemForLinkedDocumentSubTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLinkedToRecordID
            // 
            this.ItemForLinkedToRecordID.Control = this.LinkedToRecordIDTextEdit;
            this.ItemForLinkedToRecordID.CustomizationFormText = "Linked To Record ID:";
            this.ItemForLinkedToRecordID.Location = new System.Drawing.Point(0, 120);
            this.ItemForLinkedToRecordID.Name = "ItemForLinkedToRecordID";
            this.ItemForLinkedToRecordID.Size = new System.Drawing.Size(608, 24);
            this.ItemForLinkedToRecordID.Text = "Linked To Record ID:";
            this.ItemForLinkedToRecordID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 476);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDocumentPath,
            this.emptySpaceItem1,
            this.layoutControlGroup4,
            this.emptySpaceItem2,
            this.ItemForDescription,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.layoutControlItem1,
            this.ItemForLinkedRecordDescription,
            this.ItemForLinkedDocumentType,
            this.ItemForLinkedDocumentSubType});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 456);
            // 
            // ItemForDocumentPath
            // 
            this.ItemForDocumentPath.AllowHide = false;
            this.ItemForDocumentPath.Control = this.DocumentPathButtonEdit;
            this.ItemForDocumentPath.CustomizationFormText = "Linked Document:";
            this.ItemForDocumentPath.Location = new System.Drawing.Point(0, 120);
            this.ItemForDocumentPath.Name = "ItemForDocumentPath";
            this.ItemForDocumentPath.Size = new System.Drawing.Size(608, 26);
            this.ItemForDocumentPath.Text = "Linked Document:";
            this.ItemForDocumentPath.TextSize = new System.Drawing.Size(107, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 446);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 158);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(608, 288);
            this.layoutControlGroup4.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(584, 242);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup3});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Details";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDateAdded,
            this.emptySpaceItem3,
            this.ItemForDocumentExtension,
            this.ItemForAddedByStaffName});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(560, 194);
            this.layoutControlGroup5.Text = "Details";
            // 
            // ItemForDateAdded
            // 
            this.ItemForDateAdded.Control = this.DateAddedDateEdit;
            this.ItemForDateAdded.CustomizationFormText = "Date Added:";
            this.ItemForDateAdded.Location = new System.Drawing.Point(0, 24);
            this.ItemForDateAdded.Name = "ItemForDateAdded";
            this.ItemForDateAdded.Size = new System.Drawing.Size(560, 24);
            this.ItemForDateAdded.Text = "Date Added:";
            this.ItemForDateAdded.TextSize = new System.Drawing.Size(107, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(560, 122);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDocumentExtension
            // 
            this.ItemForDocumentExtension.Control = this.DocumentExtensionTextEdit;
            this.ItemForDocumentExtension.CustomizationFormText = "Extension Type:";
            this.ItemForDocumentExtension.Location = new System.Drawing.Point(0, 0);
            this.ItemForDocumentExtension.Name = "ItemForDocumentExtension";
            this.ItemForDocumentExtension.Size = new System.Drawing.Size(560, 24);
            this.ItemForDocumentExtension.Text = "Extension Type:";
            this.ItemForDocumentExtension.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForAddedByStaffName
            // 
            this.ItemForAddedByStaffName.Control = this.AddedByStaffNameTextEdit;
            this.ItemForAddedByStaffName.CustomizationFormText = "Added By Staff Name:";
            this.ItemForAddedByStaffName.Location = new System.Drawing.Point(0, 48);
            this.ItemForAddedByStaffName.Name = "ItemForAddedByStaffName";
            this.ItemForAddedByStaffName.Size = new System.Drawing.Size(560, 24);
            this.ItemForAddedByStaffName.Text = "Added By Staff Name:";
            this.ItemForAddedByStaffName.TextSize = new System.Drawing.Size(107, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup3.CaptionImageOptions.Image")));
            this.layoutControlGroup3.CustomizationFormText = "Remarks";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(560, 194);
            this.layoutControlGroup3.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(560, 194);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 146);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(608, 12);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDescription
            // 
            this.ItemForDescription.AllowHide = false;
            this.ItemForDescription.Control = this.DescriptionTextEdit;
            this.ItemForDescription.CustomizationFormText = "Link Description:";
            this.ItemForDescription.Location = new System.Drawing.Point(0, 96);
            this.ItemForDescription.Name = "ItemForDescription";
            this.ItemForDescription.Size = new System.Drawing.Size(608, 24);
            this.ItemForDescription.Text = "Link Description:";
            this.ItemForDescription.TextSize = new System.Drawing.Size(107, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(112, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(112, 24);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(112, 24);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(289, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(319, 24);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(112, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 24);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // ItemForLinkedRecordDescription
            // 
            this.ItemForLinkedRecordDescription.AllowHide = false;
            this.ItemForLinkedRecordDescription.Control = this.LinkedRecordDescriptionButtonEdit;
            this.ItemForLinkedRecordDescription.CustomizationFormText = "Linked To Record:";
            this.ItemForLinkedRecordDescription.Location = new System.Drawing.Point(0, 72);
            this.ItemForLinkedRecordDescription.Name = "ItemForLinkedRecordDescription";
            this.ItemForLinkedRecordDescription.Size = new System.Drawing.Size(608, 24);
            this.ItemForLinkedRecordDescription.Text = "Linked To Record:";
            this.ItemForLinkedRecordDescription.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForLinkedDocumentType
            // 
            this.ItemForLinkedDocumentType.Control = this.LinkedDocumentTypeButtonEdit;
            this.ItemForLinkedDocumentType.CustomizationFormText = "Document Type:";
            this.ItemForLinkedDocumentType.Location = new System.Drawing.Point(0, 24);
            this.ItemForLinkedDocumentType.Name = "ItemForLinkedDocumentType";
            this.ItemForLinkedDocumentType.Size = new System.Drawing.Size(608, 24);
            this.ItemForLinkedDocumentType.Text = "Document Type:";
            this.ItemForLinkedDocumentType.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForLinkedDocumentSubType
            // 
            this.ItemForLinkedDocumentSubType.Control = this.LinkedDocumentSubTypeTextEdit;
            this.ItemForLinkedDocumentSubType.CustomizationFormText = "Document Sub-Type:";
            this.ItemForLinkedDocumentSubType.Location = new System.Drawing.Point(0, 48);
            this.ItemForLinkedDocumentSubType.Name = "ItemForLinkedDocumentSubType";
            this.ItemForLinkedDocumentSubType.Size = new System.Drawing.Size(608, 24);
            this.ItemForLinkedDocumentSubType.Text = "Document Sub-Type:";
            this.ItemForLinkedDocumentSubType.TextSize = new System.Drawing.Size(107, 13);
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp06045_OM_Linked_Document_EditTableAdapter
            // 
            this.sp06045_OM_Linked_Document_EditTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Linked_Document_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 532);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Linked_Document_Edit";
            this.Text = "Edit Linked Document - Operations";
            this.Activated += new System.EventHandler(this.frm_OM_Linked_Document_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Linked_Document_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Linked_Document_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LinkedDocumentSubTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06045OMLinkedDocumentEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Summer_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedDocumentSubTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedDocumentTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedDocumentTypeButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToRecordIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedDocumentIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddedByStaffNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedRecordDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentExtensionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentPathButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedDocumentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedDocumentTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedDocumentSubTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToRecordID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDocumentPath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateAdded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDocumentExtension)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddedByStaffName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedRecordDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedDocumentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedDocumentSubType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraEditors.TextEdit DocumentExtensionTextEdit;
        private DevExpress.XtraEditors.TextEdit DescriptionTextEdit;
        private DevExpress.XtraEditors.DateEdit DateAddedDateEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDocumentPath;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDocumentExtension;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddedByStaffID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateAdded;
        private DevExpress.XtraEditors.ButtonEdit DocumentPathButtonEdit;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraEditors.ButtonEdit LinkedRecordDescriptionButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedRecordDescription;
        private DevExpress.XtraEditors.TextEdit AddedByStaffNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddedByStaffName;
        private DevExpress.XtraEditors.TextEdit AddedByStaffIDTextEdit;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_HR_Core dataSet_HR_Core;
        private DevExpress.XtraEditors.TextEdit LinkedDocumentIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedDocumentID;
        private DevExpress.XtraEditors.TextEdit LinkedToRecordIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToRecordID;
        private DevExpress.XtraEditors.ButtonEdit LinkedDocumentTypeButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedDocumentType;
        private DevExpress.XtraEditors.TextEdit LinkedDocumentTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedDocumentTypeID;
        private DevExpress.XtraEditors.TextEdit LinkedDocumentSubTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedDocumentSubTypeID;
        private DevExpress.XtraEditors.TextEdit LinkedDocumentSubTypeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedDocumentSubType;
        private System.Windows.Forms.BindingSource sp06045OMLinkedDocumentEditBindingSource;
        private DataSet_GC_Summer_DataEntry dataSet_GC_Summer_DataEntry;
        private DataSet_GC_Summer_DataEntryTableAdapters.sp06045_OM_Linked_Document_EditTableAdapter sp06045_OM_Linked_Document_EditTableAdapter;
    }
}

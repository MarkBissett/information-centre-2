namespace WoodPlan5
{
    partial class frm_OM_Team_Self_Billing_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Team_Self_Billing_Manager));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule4 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue4 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule5 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue5 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule6 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue6 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule7 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue7 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule8 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue8 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule9 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue9 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule10 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue10 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule11 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue11 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule12 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue12 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule13 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue13 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule14 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue14 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule15 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue15 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions10 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject37 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject38 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject39 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject40 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions11 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject41 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject42 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject43 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject44 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions12 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject45 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject46 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject47 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject48 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions13 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject49 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject50 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject51 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject52 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule16 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue16 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule17 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue17 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule18 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue18 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule19 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue19 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule20 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue20 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule21 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue21 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule22 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue22 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule23 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue23 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule24 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue24 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule25 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue25 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule26 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue26 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule27 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue27 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem16 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip17 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem17 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem17 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip18 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem18 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem18 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip19 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem19 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem19 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip20 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem20 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem20 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip21 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem21 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem21 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip22 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem22 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem22 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip23 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem23 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem23 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip24 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem24 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem24 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip25 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem25 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem25 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip26 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem26 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem26 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip27 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem27 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem27 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip28 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem28 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem28 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip29 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem29 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem29 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip30 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem30 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem30 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip31 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem31 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem31 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip32 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem32 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem32 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip33 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem33 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem33 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip34 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem34 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem34 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip35 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem35 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem35 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip36 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem36 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem36 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip37 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem37 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem37 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip38 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem38 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem38 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip39 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem39 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem39 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip40 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem40 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem40 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip41 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem41 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem41 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip42 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem42 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem42 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip43 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem43 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem43 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip44 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem44 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem44 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip45 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem45 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem45 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip46 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem46 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem46 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip47 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem47 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem47 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip48 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem48 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem48 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip49 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem49 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem49 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip50 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem50 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem50 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip51 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem51 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem51 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip52 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem52 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem52 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip53 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem53 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem53 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip54 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem54 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem54 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip55 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem55 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem55 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip56 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem56 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem56 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip57 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem57 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem57 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip58 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem58 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem58 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip59 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem59 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem59 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip60 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem60 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem60 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip61 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem61 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem61 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip62 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem62 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem62 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip63 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem63 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem63 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip64 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem64 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem64 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip65 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem65 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem65 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            this.colSelfBillingInvoiceHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingRequirmentsUnfulfilled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colBillingRequirmentsFulfilled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingWarningUnauthorised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingWarningAuthorised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOverdueDays = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEarlyDays = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequirementUnfulfilledCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarningUnauthorisedCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequirementFulfilled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit24 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colAuthorised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEditAuthorised = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit17 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colNoPDFCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnsentCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoCMReportCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnsentCMReportCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastSentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colInvoicePDF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditInvoicePDF = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colLastSentCMReportDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCMReportPDF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditCMReportPDF = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemMemoExEdit21 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemTextEditHTML2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colInvoiceCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemTextEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemTextEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemHyperLinkEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemTextEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemCheckEdit21 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemTextEditLatLong = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemTextEditHTML0 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemHyperLinkEditGrid1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemHyperLinkEditClientSignature = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemPictureEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemHyperLinkEditSelfBillingInvoice = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemHyperLinkEditClientInvoiceID = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemHyperLinkEditPictureLink = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemTextEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemMemoExEdit20 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemTextEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVisitStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPopupContainerEditDateRange = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridControlVisit = new DevExpress.XtraGrid.GridControl();
            this.sp06445OMTeamSelfBillingManagerVisitsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Billing = new WoodPlan5.DataSet_OM_Billing();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridViewVisit = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVisitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCalculationLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCalculationLevelDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDirectorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDirector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedOutstandingJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedCompletedJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeliness = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueFound = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssueRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientSignaturePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colImagesFolderOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRework = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExtraWorkComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExtraWorkRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExtraWorkType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentOnSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSuspendedJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedPictureCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFriendlyVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellCalculationLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellCalculationLevelDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCMName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysAllowed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManagerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManagerNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoOnetoSign = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExternalCustomerStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletionSheetNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusIssueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusIssue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysSeparation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractDaysSeparationPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClient1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPayContractor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitLabourCalculatedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDuration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.btnDateRangeOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlInvoicingParameters = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditAutoSendCreatedInvoices = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditExcludeVisitWithRequirements = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditExcludeVisitsWithWarnings = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditExcludeTeamsWithOutstandingRequirements = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditExcludeTeamsWithOutstandingWarnings = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButtonInvoicingParameters = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlLabour = new DevExpress.XtraGrid.GridControl();
            this.sp06452OMBillingManagerLabourBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewLabour = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLabourTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRequirementCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequirementFulfilledCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarningCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarningAuthorisedCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControlBillingRequirement = new DevExpress.XtraGrid.GridControl();
            this.sp06446OMTeamSelfBillingBillingRequirementsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewBillingRequirement = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBillingRequirementID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequirementID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn263 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn264 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientBillRequirement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamSelfBillRequirement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingRequirement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecord1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckedByStaff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControlBillingException = new DevExpress.XtraGrid.GridControl();
            this.sp06448OMTeamSelfBillingBillingExceptionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewBillingException = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBillingWarningID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarningTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colAuthorisedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarningType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisit2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedToRecordType2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAuthorisedByStaff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridControlJob = new DevExpress.XtraGrid.GridControl();
            this.sp06454OMSelfBillingJobsLinkedtoVisitsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewJob = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colJobSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colJobNoLongerRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequiresAccessPermit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessPermitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceSystemPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedDurationUnitsDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScheduleSentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnitsDescriptionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalMaterialExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCostTotalMaterialVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalMaterialCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalMaterialExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalMaterialVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalMaterialCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalEquipmentExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalEquipmentVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalEquipmentExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalEquipmentVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalLabourExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalLabourVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalLabourExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalLabourVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalCostExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalCostVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalCostExVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalCostVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateClientInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceReceivedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoicePaidDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceAmountPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotPayContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoNotInvoiceClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRouteOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditLatLong2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedDurationUnitsDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualDurationUnitsDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTenderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTenderJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumDaysFromLastVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaximumDaysFromLastVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysLeeway = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocumentsJobs = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalEquipmentVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCostTotalLabourVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostTotalMaterialVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalMaterialVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalEquipmentVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellTotalLabourVATRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToVisit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientAndContract = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReworkOriginalJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRework1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCancelledReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCancelledReasonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysUntilDue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditIntegerDays = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colManuallyCompleted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedPictureCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditPictures = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colImagesFolderOM1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFriendlyVisitNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPO1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCostCalculationLevelID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitSellCalculationLevelID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCostCalculationLevelDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitSellCalculationLevelDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisplayOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMandatory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSuspendedReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSuspendedRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourUsedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditViewJob = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlLabourFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnLabourFilter = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl12 = new DevExpress.XtraGrid.GridControl();
            this.sp06028OMLabourFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Summer_Core = new WoodPlan5.DataSet_GC_Summer_Core();
            this.gridView12 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlWarningFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnWarningFilter = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp06451OMSelfBillingBillingWarningsFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.popupContainerControlVisitTypeFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp06308OMVisitTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn224 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn226 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn227 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnVisitTypeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlVisitCategoryFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp06528OMVisitCategoriesFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Core = new WoodPlan5.DataSet_OM_Core();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.btnVisitCategoryFilter = new DevExpress.XtraEditors.SimpleButton();
            this.dataSet_Common_Functionality = new WoodPlan5.DataSet_Common_Functionality();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.sp06404OMVisitSystemStatusFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp06357OMVisitManagerLinkedVisitLabourBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bsiFilterSelected = new DevExpress.XtraBars.BarSubItem();
            this.bciFilterTeamsSelected = new DevExpress.XtraBars.BarCheckItem();
            this.bciFilterVisitsSelected = new DevExpress.XtraBars.BarCheckItem();
            this.bciFilterJobsSelected = new DevExpress.XtraBars.BarCheckItem();
            this.bsiSelectedCount0 = new DevExpress.XtraBars.BarStaticItem();
            this.bsiSelectedCount1 = new DevExpress.XtraBars.BarStaticItem();
            this.bsiSelectedCount2 = new DevExpress.XtraBars.BarStaticItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRecalculateTotal = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemInvoicingParameters = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditInvoicingParameters = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiInvoice = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemCheckEditIgnoreHistoric = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.sp06145OMVisitEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemDuration1 = new DevExpress.XtraScheduler.UI.RepositoryItemDuration();
            this.toggleRecurrenceItem1 = new DevExpress.XtraScheduler.UI.ToggleRecurrenceItem();
            this.sp06145_OM_Visit_EditTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06145_OM_Visit_EditTableAdapter();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.hideContainerLeft = new DevExpress.XtraBars.Docking.AutoHideContainer();
            this.dockPanelFilters = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.textEditSBNumber = new DevExpress.XtraEditors.TextEdit();
            this.popupContainerEditVisitCategoryFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.checkEditDoNotPay = new DevExpress.XtraEditors.CheckEdit();
            this.popupContainerEditInvoiceNumber = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControlInvoiceNumber = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnInvoiceNumber = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp06477OMGetUniqueSelfBillingInvoiceListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSelfBillingInvoiceNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.buttonEditExcludeClientFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.btnLoadHistorical = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEditHistoricalDateRange = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControlHistoricalDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditHistoricalFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditHistoricalToDate = new DevExpress.XtraEditors.DateEdit();
            this.btnHistoricalDateRange = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEditWarningsFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.checkEditIncludeUnpaidHistorical = new DevExpress.XtraEditors.CheckEdit();
            this.spinEditYear = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditWeekNumber = new DevExpress.XtraEditors.SpinEdit();
            this.checkEditDateRange = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditWeekNumber = new DevExpress.XtraEditors.CheckEdit();
            this.spinEditSiteID = new DevExpress.XtraEditors.SpinEdit();
            this.checkEditStaff = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditTeam = new DevExpress.XtraEditors.CheckEdit();
            this.popupContainerEditVisitTypeFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditLabourFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.buttonEditSiteFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.buttonEditClientFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.popupContainerEditDateRange = new DevExpress.XtraEditors.PopupContainerEdit();
            this.checkEditShowInvoiced = new DevExpress.XtraEditors.CheckEdit();
            this.btnLoad = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tabbedControlGroupFilters = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroupChecking = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDateRange = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWeekNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForYear = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemforSBNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForShowInvoiced = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLabourFilter = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupHistorical = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForHistoricalDateRange = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForLoadHistorical = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInvoiceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp06028_OM_Labour_FilterTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06028_OM_Labour_FilterTableAdapter();
            this.sp06308_OM_Visit_TypesTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06308_OM_Visit_TypesTableAdapter();
            this.sp06357_OM_Visit_Manager_Linked_Visit_LabourTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06357_OM_Visit_Manager_Linked_Visit_LabourTableAdapter();
            this.sp06445_OM_Team_Self_Billing_Manager_VisitsTableAdapter = new WoodPlan5.DataSet_OM_BillingTableAdapters.sp06445_OM_Team_Self_Billing_Manager_VisitsTableAdapter();
            this.sp06446_OM_Team_Self_Billing_Billing_RequirementsTableAdapter = new WoodPlan5.DataSet_OM_BillingTableAdapters.sp06446_OM_Team_Self_Billing_Billing_RequirementsTableAdapter();
            this.sp06448_OM_Team_Self_Billing_Billing_ExceptionsTableAdapter = new WoodPlan5.DataSet_OM_BillingTableAdapters.sp06448_OM_Team_Self_Billing_Billing_ExceptionsTableAdapter();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageChecking = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPageHistoricalInvoices = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl5 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControlLabour2 = new DevExpress.XtraGrid.GridControl();
            this.sp06463OMBillingManagerInvoicedLabourBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewLabour2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnLabourTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLabourID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControlInvoice = new DevExpress.XtraGrid.GridControl();
            this.sp06464OMBillingManagerInvoicesForLabourBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewInvoice = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSelfBillingInvoiceHeaderID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToLabourID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToLabourTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalBillAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabourName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSelected2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedVisitCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditVisitCount = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colEmailPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractManagerEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedFinanceExportedCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.standaloneBarDockControl2 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.xtraTabPageInvoiceDesigner = new DevExpress.XtraTab.XtraTabPage();
            this.documentViewer1 = new DevExpress.XtraPrinting.Preview.DocumentViewer();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.printPreviewStaticItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.progressBarEditItem1 = new DevExpress.XtraPrinting.Preview.ProgressBarEditItem();
            this.documentViewerRibbonController1 = new DevExpress.XtraPrinting.Preview.DocumentViewerRibbonController(this.components);
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.printPreviewBarItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem3 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem4 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem5 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem6 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem7 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem8 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem9 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem10 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem11 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem12 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem13 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem14 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem15 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem16 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem17 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem18 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem19 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem20 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem21 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem22 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem23 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem24 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem25 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem26 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem27 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem28 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem29 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem30 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem31 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem32 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem33 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem34 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem35 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem36 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem37 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem38 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem39 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem40 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem41 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem42 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem43 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem44 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem45 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem46 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem49 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewStaticItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.zoomTrackBarEditItem1 = new DevExpress.XtraPrinting.Preview.ZoomTrackBarEditItem();
            this.repositoryItemZoomTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.bbiBack = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEditReportLayout = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEditCMReportLayout = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage();
            this.printPreviewRibbonPageGroup1 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup2 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup3 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup4 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup5 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup6 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup7 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.printPreviewRibbonPageGroup8 = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.sp06451_OM_Self_Billing_Billing_Warnings_FilterTableAdapter = new WoodPlan5.DataSet_OM_BillingTableAdapters.sp06451_OM_Self_Billing_Billing_Warnings_FilterTableAdapter();
            this.sp06452_OM_Billing_Manager_LabourTableAdapter = new WoodPlan5.DataSet_OM_BillingTableAdapters.sp06452_OM_Billing_Manager_LabourTableAdapter();
            this.sp06454_OM_Self_Billing_Jobs_Linked_to_VisitsTableAdapter = new WoodPlan5.DataSet_OM_BillingTableAdapters.sp06454_OM_Self_Billing_Jobs_Linked_to_VisitsTableAdapter();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bsiFilterSelected2 = new DevExpress.XtraBars.BarSubItem();
            this.bciFilterTeamsSelected2 = new DevExpress.XtraBars.BarCheckItem();
            this.bciFilterInvoicesSelected = new DevExpress.XtraBars.BarCheckItem();
            this.bsiSelectedCount3 = new DevExpress.XtraBars.BarStaticItem();
            this.bbiRefresh2 = new DevExpress.XtraBars.BarButtonItem();
            this.bsiRecreatePDF = new DevExpress.XtraBars.BarSubItem();
            this.bbiSelectInvoicesMissingPDFs = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRecreatePDF = new DevExpress.XtraBars.BarButtonItem();
            this.bsiSendToTeam = new DevExpress.XtraBars.BarSubItem();
            this.bsiUnsentInvoices = new DevExpress.XtraBars.BarSubItem();
            this.bbiSelectUnsentInvoices = new DevExpress.XtraBars.BarButtonItem();
            this.bbiResend = new DevExpress.XtraBars.BarButtonItem();
            this.bsiUnsentReports = new DevExpress.XtraBars.BarSubItem();
            this.bbiSelectUnsentInvoiceReports = new DevExpress.XtraBars.BarButtonItem();
            this.bbiResendCM = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDeleteInvoice = new DevExpress.XtraBars.BarButtonItem();
            this.sp06463_OM_Billing_Manager_Invoiced_LabourTableAdapter = new WoodPlan5.DataSet_OM_BillingTableAdapters.sp06463_OM_Billing_Manager_Invoiced_LabourTableAdapter();
            this.sp06464_OM_Billing_Manager_Invoices_For_LabourTableAdapter = new WoodPlan5.DataSet_OM_BillingTableAdapters.sp06464_OM_Billing_Manager_Invoices_For_LabourTableAdapter();
            this.sp06477_OM_Get_Unique_Self_Billing_Invoice_ListTableAdapter = new WoodPlan5.DataSet_OM_BillingTableAdapters.sp06477_OM_Get_Unique_Self_Billing_Invoice_ListTableAdapter();
            this.sp06528_OM_Visit_Categories_FilterTableAdapter = new WoodPlan5.DataSet_OM_CoreTableAdapters.sp06528_OM_Visit_Categories_FilterTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditAuthorised)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditInvoicePDF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditCMReportPDF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditClientSignature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditSelfBillingInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditClientInvoiceID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditPictureLink)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVisit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06445OMTeamSelfBillingManagerVisitsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Billing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewVisit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlInvoicingParameters)).BeginInit();
            this.popupContainerControlInvoicingParameters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAutoSendCreatedInvoices.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditExcludeVisitWithRequirements.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditExcludeVisitsWithWarnings.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditExcludeTeamsWithOutstandingRequirements.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditExcludeTeamsWithOutstandingWarnings.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLabour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06452OMBillingManagerLabourBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLabour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBillingRequirement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06446OMTeamSelfBillingBillingRequirementsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBillingRequirement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBillingException)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06448OMTeamSelfBillingBillingExceptionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBillingException)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06454OMSelfBillingJobsLinkedtoVisitsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsJobs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditIntegerDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditPictures)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditViewJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLabourFilter)).BeginInit();
            this.popupContainerControlLabourFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06028OMLabourFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Summer_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlWarningFilter)).BeginInit();
            this.popupContainerControlWarningFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06451OMSelfBillingBillingWarningsFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlVisitTypeFilter)).BeginInit();
            this.popupContainerControlVisitTypeFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06308OMVisitTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlVisitCategoryFilter)).BeginInit();
            this.popupContainerControlVisitCategoryFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06528OMVisitCategoriesFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06404OMVisitSystemStatusFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06357OMVisitManagerLinkedVisitLabourBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditInvoicingParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditIgnoreHistoric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06145OMVisitEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.hideContainerLeft.SuspendLayout();
            this.dockPanelFilters.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSBNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditVisitCategoryFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDoNotPay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditInvoiceNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlInvoiceNumber)).BeginInit();
            this.popupContainerControlInvoiceNumber.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06477OMGetUniqueSelfBillingInvoiceListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditExcludeClientFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditHistoricalDateRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlHistoricalDateRange)).BeginInit();
            this.popupContainerControlHistoricalDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditHistoricalFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditHistoricalFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditHistoricalToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditHistoricalToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditWarningsFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIncludeUnpaidHistorical.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditYear.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditWeekNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDateRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditWeekNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditSiteID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditStaff.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTeam.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditVisitTypeFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditLabourFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSiteFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditDateRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShowInvoiced.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupFilters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupChecking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWeekNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforSBNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShowInvoiced)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupHistorical)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHistoricalDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLoadHistorical)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvoiceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageChecking.SuspendLayout();
            this.xtraTabPageHistoricalInvoices.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).BeginInit();
            this.splitContainerControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLabour2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06463OMBillingManagerInvoicedLabourBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLabour2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06464OMBillingManagerInvoicesForLabourBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditVisitCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            this.xtraTabPageInvoiceDesigner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentViewerRibbonController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1384, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 728);
            this.barDockControlBottom.Size = new System.Drawing.Size(1384, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 728);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1384, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 728);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // pmEditContextMenu
            // 
            this.pmEditContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUndo),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRedo),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCut, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCopy),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPaste),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectAll, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClear),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSpellChecker, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGrammarCheck)});
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl2);
            this.barManager1.Images = this.imageCollection2;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.bciFilterJobsSelected,
            this.bsiSelectedCount1,
            this.bsiFilterSelected,
            this.bciFilterVisitsSelected,
            this.bsiSelectedCount2,
            this.bsiSelectedCount0,
            this.bciFilterTeamsSelected,
            this.bbiRecalculateTotal,
            this.barEditItemInvoicingParameters,
            this.bbiInvoice,
            this.bsiSelectedCount3,
            this.bsiFilterSelected2,
            this.bciFilterTeamsSelected2,
            this.bciFilterInvoicesSelected,
            this.bbiRefresh2,
            this.bbiRecreatePDF,
            this.bbiResend,
            this.bbiDeleteInvoice,
            this.bsiRecreatePDF,
            this.bbiSelectInvoicesMissingPDFs,
            this.bsiSendToTeam,
            this.bbiSelectUnsentInvoices,
            this.bsiUnsentInvoices,
            this.bsiUnsentReports,
            this.bbiSelectUnsentInvoiceReports,
            this.bbiResendCM});
            this.barManager1.MaxItemId = 158;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1,
            this.repositoryItemCheckEditIgnoreHistoric,
            this.repositoryItemDuration1,
            this.repositoryItemSpinEdit1,
            this.repositoryItemPopupContainerEditInvoicingParameters});
            this.barManager1.HighlightedLinkChanged += new DevExpress.XtraBars.HighlightedLinkChangedEventHandler(this.barManager1_HighlightedLinkChanged);
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colSelfBillingInvoiceHeaderID
            // 
            this.colSelfBillingInvoiceHeaderID.Caption = "Self-Billing Invoice ID";
            this.colSelfBillingInvoiceHeaderID.FieldName = "SelfBillingInvoiceHeaderID";
            this.colSelfBillingInvoiceHeaderID.Name = "colSelfBillingInvoiceHeaderID";
            this.colSelfBillingInvoiceHeaderID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceHeaderID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceHeaderID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceHeaderID.Visible = true;
            this.colSelfBillingInvoiceHeaderID.VisibleIndex = 31;
            this.colSelfBillingInvoiceHeaderID.Width = 119;
            // 
            // colBillingRequirmentsUnfulfilled
            // 
            this.colBillingRequirmentsUnfulfilled.Caption = "Billing Requirements Unfulfilled";
            this.colBillingRequirmentsUnfulfilled.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colBillingRequirmentsUnfulfilled.FieldName = "BillingRequirmentsUnfulfilled";
            this.colBillingRequirmentsUnfulfilled.Name = "colBillingRequirmentsUnfulfilled";
            this.colBillingRequirmentsUnfulfilled.OptionsColumn.AllowEdit = false;
            this.colBillingRequirmentsUnfulfilled.OptionsColumn.AllowFocus = false;
            this.colBillingRequirmentsUnfulfilled.OptionsColumn.ReadOnly = true;
            this.colBillingRequirmentsUnfulfilled.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BillingRequirmentsUnfulfilled", "{0:0}")});
            this.colBillingRequirmentsUnfulfilled.Visible = true;
            this.colBillingRequirmentsUnfulfilled.VisibleIndex = 14;
            this.colBillingRequirmentsUnfulfilled.Width = 164;
            // 
            // repositoryItemTextEditInteger2
            // 
            this.repositoryItemTextEditInteger2.AutoHeight = false;
            this.repositoryItemTextEditInteger2.LookAndFeel.SkinName = "Blue";
            this.repositoryItemTextEditInteger2.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger2.Name = "repositoryItemTextEditInteger2";
            // 
            // colBillingRequirmentsFulfilled
            // 
            this.colBillingRequirmentsFulfilled.Caption = "Billing Requirements Fullfilled";
            this.colBillingRequirmentsFulfilled.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colBillingRequirmentsFulfilled.FieldName = "BillingRequirmentsFulfilled";
            this.colBillingRequirmentsFulfilled.Name = "colBillingRequirmentsFulfilled";
            this.colBillingRequirmentsFulfilled.OptionsColumn.AllowEdit = false;
            this.colBillingRequirmentsFulfilled.OptionsColumn.AllowFocus = false;
            this.colBillingRequirmentsFulfilled.OptionsColumn.ReadOnly = true;
            this.colBillingRequirmentsFulfilled.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BillingRequirmentsFulfilled", "{0:0}")});
            this.colBillingRequirmentsFulfilled.Visible = true;
            this.colBillingRequirmentsFulfilled.VisibleIndex = 13;
            this.colBillingRequirmentsFulfilled.Width = 155;
            // 
            // colBillingWarningUnauthorised
            // 
            this.colBillingWarningUnauthorised.Caption = "Billing Warnings Unauthorised";
            this.colBillingWarningUnauthorised.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colBillingWarningUnauthorised.FieldName = "BillingWarningUnauthorised";
            this.colBillingWarningUnauthorised.Name = "colBillingWarningUnauthorised";
            this.colBillingWarningUnauthorised.OptionsColumn.AllowEdit = false;
            this.colBillingWarningUnauthorised.OptionsColumn.AllowFocus = false;
            this.colBillingWarningUnauthorised.OptionsColumn.ReadOnly = true;
            this.colBillingWarningUnauthorised.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BillingWarningUnauthorised", "{0:0}")});
            this.colBillingWarningUnauthorised.Visible = true;
            this.colBillingWarningUnauthorised.VisibleIndex = 16;
            this.colBillingWarningUnauthorised.Width = 160;
            // 
            // colBillingWarningAuthorised
            // 
            this.colBillingWarningAuthorised.Caption = "Billing Warnings Authorised";
            this.colBillingWarningAuthorised.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colBillingWarningAuthorised.FieldName = "BillingWarningAuthorised";
            this.colBillingWarningAuthorised.Name = "colBillingWarningAuthorised";
            this.colBillingWarningAuthorised.OptionsColumn.AllowEdit = false;
            this.colBillingWarningAuthorised.OptionsColumn.AllowFocus = false;
            this.colBillingWarningAuthorised.OptionsColumn.ReadOnly = true;
            this.colBillingWarningAuthorised.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BillingWarningAuthorised", "{0:0}")});
            this.colBillingWarningAuthorised.Visible = true;
            this.colBillingWarningAuthorised.VisibleIndex = 15;
            this.colBillingWarningAuthorised.Width = 148;
            // 
            // colOverdueDays
            // 
            this.colOverdueDays.Caption = "Days Overdue";
            this.colOverdueDays.FieldName = "OverdueDays";
            this.colOverdueDays.Name = "colOverdueDays";
            this.colOverdueDays.OptionsColumn.AllowEdit = false;
            this.colOverdueDays.OptionsColumn.AllowFocus = false;
            this.colOverdueDays.OptionsColumn.ReadOnly = true;
            this.colOverdueDays.Visible = true;
            this.colOverdueDays.VisibleIndex = 9;
            this.colOverdueDays.Width = 88;
            // 
            // colEarlyDays
            // 
            this.colEarlyDays.Caption = "Days Early";
            this.colEarlyDays.FieldName = "EarlyDays";
            this.colEarlyDays.Name = "colEarlyDays";
            this.colEarlyDays.OptionsColumn.AllowEdit = false;
            this.colEarlyDays.OptionsColumn.AllowFocus = false;
            this.colEarlyDays.OptionsColumn.ReadOnly = true;
            this.colEarlyDays.Visible = true;
            this.colEarlyDays.VisibleIndex = 8;
            this.colEarlyDays.Width = 70;
            // 
            // colRequirementUnfulfilledCount
            // 
            this.colRequirementUnfulfilledCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colRequirementUnfulfilledCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colRequirementUnfulfilledCount.Caption = "Requirements Unfulfilled";
            this.colRequirementUnfulfilledCount.FieldName = "RequirementUnfulfilledCount";
            this.colRequirementUnfulfilledCount.Name = "colRequirementUnfulfilledCount";
            this.colRequirementUnfulfilledCount.OptionsColumn.AllowEdit = false;
            this.colRequirementUnfulfilledCount.OptionsColumn.AllowFocus = false;
            this.colRequirementUnfulfilledCount.OptionsColumn.ReadOnly = true;
            this.colRequirementUnfulfilledCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "RequirementUnfulfilledCount", "{0:0}")});
            this.colRequirementUnfulfilledCount.Visible = true;
            this.colRequirementUnfulfilledCount.VisibleIndex = 1;
            this.colRequirementUnfulfilledCount.Width = 86;
            // 
            // colWarningUnauthorisedCount
            // 
            this.colWarningUnauthorisedCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colWarningUnauthorisedCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWarningUnauthorisedCount.Caption = "Warnings Unauthorised";
            this.colWarningUnauthorisedCount.FieldName = "WarningUnauthorisedCount";
            this.colWarningUnauthorisedCount.Name = "colWarningUnauthorisedCount";
            this.colWarningUnauthorisedCount.OptionsColumn.AllowEdit = false;
            this.colWarningUnauthorisedCount.OptionsColumn.AllowFocus = false;
            this.colWarningUnauthorisedCount.OptionsColumn.ReadOnly = true;
            this.colWarningUnauthorisedCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "WarningUnauthorisedCount", "{0:0}")});
            this.colWarningUnauthorisedCount.Visible = true;
            this.colWarningUnauthorisedCount.VisibleIndex = 4;
            this.colWarningUnauthorisedCount.Width = 83;
            // 
            // colRequirementFulfilled
            // 
            this.colRequirementFulfilled.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colRequirementFulfilled.AppearanceHeader.Options.UseFont = true;
            this.colRequirementFulfilled.Caption = "Fullfilled";
            this.colRequirementFulfilled.ColumnEdit = this.repositoryItemCheckEdit24;
            this.colRequirementFulfilled.CustomizationCaption = "Requirement Fullfilled";
            this.colRequirementFulfilled.FieldName = "RequirementFulfilled";
            this.colRequirementFulfilled.Name = "colRequirementFulfilled";
            this.colRequirementFulfilled.Visible = true;
            this.colRequirementFulfilled.VisibleIndex = 1;
            this.colRequirementFulfilled.Width = 72;
            // 
            // repositoryItemCheckEdit24
            // 
            this.repositoryItemCheckEdit24.AutoHeight = false;
            this.repositoryItemCheckEdit24.LookAndFeel.SkinName = "Blue";
            this.repositoryItemCheckEdit24.Name = "repositoryItemCheckEdit24";
            this.repositoryItemCheckEdit24.ValueChecked = 1;
            this.repositoryItemCheckEdit24.ValueUnchecked = 0;
            // 
            // colAuthorised
            // 
            this.colAuthorised.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colAuthorised.AppearanceHeader.Options.UseFont = true;
            this.colAuthorised.Caption = "Authorised";
            this.colAuthorised.ColumnEdit = this.repositoryItemCheckEditAuthorised;
            this.colAuthorised.FieldName = "Authorised";
            this.colAuthorised.Name = "colAuthorised";
            this.colAuthorised.Visible = true;
            this.colAuthorised.VisibleIndex = 1;
            this.colAuthorised.Width = 81;
            // 
            // repositoryItemCheckEditAuthorised
            // 
            this.repositoryItemCheckEditAuthorised.AutoHeight = false;
            this.repositoryItemCheckEditAuthorised.Name = "repositoryItemCheckEditAuthorised";
            this.repositoryItemCheckEditAuthorised.ValueChecked = 1;
            this.repositoryItemCheckEditAuthorised.ValueUnchecked = 0;
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit17;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 51;
            // 
            // repositoryItemCheckEdit17
            // 
            this.repositoryItemCheckEdit17.AutoHeight = false;
            this.repositoryItemCheckEdit17.Caption = "Check";
            this.repositoryItemCheckEdit17.Name = "repositoryItemCheckEdit17";
            this.repositoryItemCheckEdit17.ValueChecked = 1;
            this.repositoryItemCheckEdit17.ValueUnchecked = 0;
            // 
            // colNoPDFCount
            // 
            this.colNoPDFCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colNoPDFCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNoPDFCount.Caption = "No Invoice Created Count";
            this.colNoPDFCount.FieldName = "NoPDFCount";
            this.colNoPDFCount.Name = "colNoPDFCount";
            this.colNoPDFCount.OptionsColumn.AllowEdit = false;
            this.colNoPDFCount.OptionsColumn.AllowFocus = false;
            this.colNoPDFCount.OptionsColumn.ReadOnly = true;
            this.colNoPDFCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NoPDFCount", "{0:0}")});
            this.colNoPDFCount.Visible = true;
            this.colNoPDFCount.VisibleIndex = 2;
            this.colNoPDFCount.Width = 144;
            // 
            // colUnsentCount
            // 
            this.colUnsentCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colUnsentCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colUnsentCount.Caption = "Un-Sent Invoice Count";
            this.colUnsentCount.FieldName = "UnsentCount";
            this.colUnsentCount.Name = "colUnsentCount";
            this.colUnsentCount.OptionsColumn.AllowEdit = false;
            this.colUnsentCount.OptionsColumn.AllowFocus = false;
            this.colUnsentCount.OptionsColumn.ReadOnly = true;
            this.colUnsentCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "UnsentCount", "{0:0}")});
            this.colUnsentCount.Visible = true;
            this.colUnsentCount.VisibleIndex = 3;
            this.colUnsentCount.Width = 128;
            // 
            // colNoCMReportCount
            // 
            this.colNoCMReportCount.Caption = "No CM Report Created Count";
            this.colNoCMReportCount.FieldName = "NoCMReportCount";
            this.colNoCMReportCount.Name = "colNoCMReportCount";
            this.colNoCMReportCount.Visible = true;
            this.colNoCMReportCount.VisibleIndex = 4;
            this.colNoCMReportCount.Width = 160;
            // 
            // colUnsentCMReportCount
            // 
            this.colUnsentCMReportCount.Caption = "Un-sent CM Report Count";
            this.colUnsentCMReportCount.FieldName = "UnsentCMReportCount";
            this.colUnsentCMReportCount.Name = "colUnsentCMReportCount";
            this.colUnsentCMReportCount.Visible = true;
            this.colUnsentCMReportCount.VisibleIndex = 5;
            this.colUnsentCMReportCount.Width = 143;
            // 
            // colLastSentDate
            // 
            this.colLastSentDate.Caption = "Invoice Last Sent Date";
            this.colLastSentDate.ColumnEdit = this.repositoryItemTextEditDateTime5;
            this.colLastSentDate.FieldName = "LastSentDate";
            this.colLastSentDate.Name = "colLastSentDate";
            this.colLastSentDate.OptionsColumn.AllowEdit = false;
            this.colLastSentDate.OptionsColumn.AllowFocus = false;
            this.colLastSentDate.OptionsColumn.ReadOnly = true;
            this.colLastSentDate.Visible = true;
            this.colLastSentDate.VisibleIndex = 3;
            this.colLastSentDate.Width = 128;
            // 
            // repositoryItemTextEditDateTime5
            // 
            this.repositoryItemTextEditDateTime5.AutoHeight = false;
            this.repositoryItemTextEditDateTime5.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime5.Name = "repositoryItemTextEditDateTime5";
            // 
            // colInvoicePDF
            // 
            this.colInvoicePDF.Caption = "Invoice PDF File";
            this.colInvoicePDF.ColumnEdit = this.repositoryItemHyperLinkEditInvoicePDF;
            this.colInvoicePDF.FieldName = "InvoicePDF";
            this.colInvoicePDF.Name = "colInvoicePDF";
            this.colInvoicePDF.OptionsColumn.ReadOnly = true;
            this.colInvoicePDF.Visible = true;
            this.colInvoicePDF.VisibleIndex = 4;
            this.colInvoicePDF.Width = 261;
            // 
            // repositoryItemHyperLinkEditInvoicePDF
            // 
            this.repositoryItemHyperLinkEditInvoicePDF.AutoHeight = false;
            this.repositoryItemHyperLinkEditInvoicePDF.Name = "repositoryItemHyperLinkEditInvoicePDF";
            this.repositoryItemHyperLinkEditInvoicePDF.SingleClick = true;
            this.repositoryItemHyperLinkEditInvoicePDF.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditInvoicePDF_OpenLink);
            // 
            // colLastSentCMReportDate
            // 
            this.colLastSentCMReportDate.Caption = "CM Report Last Sent Date";
            this.colLastSentCMReportDate.ColumnEdit = this.repositoryItemTextEditDateTime5;
            this.colLastSentCMReportDate.FieldName = "LastSentCMReportDate";
            this.colLastSentCMReportDate.Name = "colLastSentCMReportDate";
            this.colLastSentCMReportDate.OptionsColumn.AllowEdit = false;
            this.colLastSentCMReportDate.OptionsColumn.AllowFocus = false;
            this.colLastSentCMReportDate.OptionsColumn.ReadOnly = true;
            this.colLastSentCMReportDate.Visible = true;
            this.colLastSentCMReportDate.VisibleIndex = 5;
            this.colLastSentCMReportDate.Width = 144;
            // 
            // colCMReportPDF
            // 
            this.colCMReportPDF.Caption = "CM Report PDF";
            this.colCMReportPDF.ColumnEdit = this.repositoryItemHyperLinkEditCMReportPDF;
            this.colCMReportPDF.FieldName = "CMReportPDF";
            this.colCMReportPDF.Name = "colCMReportPDF";
            this.colCMReportPDF.OptionsColumn.ReadOnly = true;
            this.colCMReportPDF.Visible = true;
            this.colCMReportPDF.VisibleIndex = 6;
            this.colCMReportPDF.Width = 284;
            // 
            // repositoryItemHyperLinkEditCMReportPDF
            // 
            this.repositoryItemHyperLinkEditCMReportPDF.AutoHeight = false;
            this.repositoryItemHyperLinkEditCMReportPDF.Name = "repositoryItemHyperLinkEditCMReportPDF";
            this.repositoryItemHyperLinkEditCMReportPDF.SingleClick = true;
            this.repositoryItemHyperLinkEditCMReportPDF.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditCMReportPDF_OpenLink);
            // 
            // repositoryItemMemoExEdit21
            // 
            this.repositoryItemMemoExEdit21.AutoHeight = false;
            this.repositoryItemMemoExEdit21.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit21.Name = "repositoryItemMemoExEdit21";
            this.repositoryItemMemoExEdit21.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit21.ShowIcon = false;
            // 
            // repositoryItemTextEditHTML2
            // 
            this.repositoryItemTextEditHTML2.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML2.AutoHeight = false;
            this.repositoryItemTextEditHTML2.Name = "repositoryItemTextEditHTML2";
            // 
            // colInvoiceCount
            // 
            this.colInvoiceCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colInvoiceCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colInvoiceCount.Caption = "Invoice Count";
            this.colInvoiceCount.FieldName = "InvoiceCount";
            this.colInvoiceCount.Name = "colInvoiceCount";
            this.colInvoiceCount.OptionsColumn.AllowEdit = false;
            this.colInvoiceCount.OptionsColumn.AllowFocus = false;
            this.colInvoiceCount.OptionsColumn.ReadOnly = true;
            this.colInvoiceCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "InvoiceCount", "{0:0}")});
            this.colInvoiceCount.Visible = true;
            this.colInvoiceCount.VisibleIndex = 1;
            this.colInvoiceCount.Width = 86;
            // 
            // colSelfBillingStatus
            // 
            this.colSelfBillingStatus.Caption = "Self-Billing Status";
            this.colSelfBillingStatus.FieldName = "SelfBillingStatus";
            this.colSelfBillingStatus.Name = "colSelfBillingStatus";
            this.colSelfBillingStatus.OptionsColumn.AllowEdit = false;
            this.colSelfBillingStatus.OptionsColumn.AllowFocus = false;
            this.colSelfBillingStatus.OptionsColumn.ReadOnly = true;
            this.colSelfBillingStatus.Visible = true;
            this.colSelfBillingStatus.VisibleIndex = 17;
            this.colSelfBillingStatus.Width = 126;
            // 
            // colSelfBillingComment
            // 
            this.colSelfBillingComment.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colSelfBillingComment.AppearanceHeader.Options.UseFont = true;
            this.colSelfBillingComment.Caption = "Self-Billing Comment";
            this.colSelfBillingComment.FieldName = "SelfBillingComment";
            this.colSelfBillingComment.Name = "colSelfBillingComment";
            this.colSelfBillingComment.Visible = true;
            this.colSelfBillingComment.VisibleIndex = 18;
            this.colSelfBillingComment.Width = 136;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.LookAndFeel.SkinName = "Blue";
            this.repositoryItemTextEdit3.Mask.EditMask = "n0";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.Mask.EditMask = "c";
            this.repositoryItemTextEdit5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // repositoryItemTextEdit6
            // 
            this.repositoryItemTextEdit6.AutoHeight = false;
            this.repositoryItemTextEdit6.Mask.EditMask = "f8";
            this.repositoryItemTextEdit6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit6.Name = "repositoryItemTextEdit6";
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // repositoryItemTextEdit7
            // 
            this.repositoryItemTextEdit7.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEdit7.AutoHeight = false;
            this.repositoryItemTextEdit7.Name = "repositoryItemTextEdit7";
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.ReadOnly = true;
            this.repositoryItemPictureEdit1.ShowMenu = false;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.SingleClick = true;
            // 
            // repositoryItemHyperLinkEdit4
            // 
            this.repositoryItemHyperLinkEdit4.AutoHeight = false;
            this.repositoryItemHyperLinkEdit4.Name = "repositoryItemHyperLinkEdit4";
            this.repositoryItemHyperLinkEdit4.SingleClick = true;
            // 
            // repositoryItemTextEdit8
            // 
            this.repositoryItemTextEdit8.AutoHeight = false;
            this.repositoryItemTextEdit8.Mask.EditMask = "P";
            this.repositoryItemTextEdit8.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit8.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit8.Name = "repositoryItemTextEdit8";
            // 
            // repositoryItemCheckEdit21
            // 
            this.repositoryItemCheckEdit21.AutoHeight = false;
            this.repositoryItemCheckEdit21.Caption = "Check";
            this.repositoryItemCheckEdit21.LookAndFeel.SkinName = "Blue";
            this.repositoryItemCheckEdit21.Name = "repositoryItemCheckEdit21";
            this.repositoryItemCheckEdit21.ValueChecked = 1;
            this.repositoryItemCheckEdit21.ValueUnchecked = 0;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // repositoryItemTextEditLatLong
            // 
            this.repositoryItemTextEditLatLong.AutoHeight = false;
            this.repositoryItemTextEditLatLong.Mask.EditMask = "f8";
            this.repositoryItemTextEditLatLong.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditLatLong.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditLatLong.Name = "repositoryItemTextEditLatLong";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // repositoryItemTextEditHTML0
            // 
            this.repositoryItemTextEditHTML0.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML0.AutoHeight = false;
            this.repositoryItemTextEditHTML0.Name = "repositoryItemTextEditHTML0";
            // 
            // repositoryItemHyperLinkEditGrid1
            // 
            this.repositoryItemHyperLinkEditGrid1.AutoHeight = false;
            this.repositoryItemHyperLinkEditGrid1.Name = "repositoryItemHyperLinkEditGrid1";
            this.repositoryItemHyperLinkEditGrid1.SingleClick = true;
            this.repositoryItemHyperLinkEditGrid1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditGrid1_OpenLink);
            // 
            // repositoryItemHyperLinkEditClientSignature
            // 
            this.repositoryItemHyperLinkEditClientSignature.AutoHeight = false;
            this.repositoryItemHyperLinkEditClientSignature.Name = "repositoryItemHyperLinkEditClientSignature";
            this.repositoryItemHyperLinkEditClientSignature.SingleClick = true;
            this.repositoryItemHyperLinkEditClientSignature.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditClientSignature_OpenLink);
            // 
            // repositoryItemPictureEdit2
            // 
            this.repositoryItemPictureEdit2.Name = "repositoryItemPictureEdit2";
            this.repositoryItemPictureEdit2.ReadOnly = true;
            this.repositoryItemPictureEdit2.ShowMenu = false;
            // 
            // repositoryItemHyperLinkEditSelfBillingInvoice
            // 
            this.repositoryItemHyperLinkEditSelfBillingInvoice.AutoHeight = false;
            this.repositoryItemHyperLinkEditSelfBillingInvoice.Name = "repositoryItemHyperLinkEditSelfBillingInvoice";
            this.repositoryItemHyperLinkEditSelfBillingInvoice.SingleClick = true;
            this.repositoryItemHyperLinkEditSelfBillingInvoice.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditSelfBillingInvoice_OpenLink);
            // 
            // repositoryItemHyperLinkEditClientInvoiceID
            // 
            this.repositoryItemHyperLinkEditClientInvoiceID.AutoHeight = false;
            this.repositoryItemHyperLinkEditClientInvoiceID.Name = "repositoryItemHyperLinkEditClientInvoiceID";
            this.repositoryItemHyperLinkEditClientInvoiceID.SingleClick = true;
            this.repositoryItemHyperLinkEditClientInvoiceID.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditClientInvoiceID_OpenLink);
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // repositoryItemHyperLinkEditPictureLink
            // 
            this.repositoryItemHyperLinkEditPictureLink.AutoHeight = false;
            this.repositoryItemHyperLinkEditPictureLink.LookAndFeel.SkinName = "Blue";
            this.repositoryItemHyperLinkEditPictureLink.Name = "repositoryItemHyperLinkEditPictureLink";
            this.repositoryItemHyperLinkEditPictureLink.SingleClick = true;
            // 
            // repositoryItemTextEdit14
            // 
            this.repositoryItemTextEdit14.AutoHeight = false;
            this.repositoryItemTextEdit14.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEdit14.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit14.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit14.Name = "repositoryItemTextEdit14";
            // 
            // repositoryItemMemoExEdit20
            // 
            this.repositoryItemMemoExEdit20.AutoHeight = false;
            this.repositoryItemMemoExEdit20.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit20.Name = "repositoryItemMemoExEdit20";
            this.repositoryItemMemoExEdit20.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit20.ShowIcon = false;
            // 
            // repositoryItemTextEdit15
            // 
            this.repositoryItemTextEdit15.AllowFocused = false;
            this.repositoryItemTextEdit15.AutoHeight = false;
            this.repositoryItemTextEdit15.Name = "repositoryItemTextEdit15";
            // 
            // colVisitStatusID
            // 
            this.colVisitStatusID.Caption = "System Status ID";
            this.colVisitStatusID.FieldName = "VisitStatusID";
            this.colVisitStatusID.Name = "colVisitStatusID";
            this.colVisitStatusID.OptionsColumn.AllowEdit = false;
            this.colVisitStatusID.OptionsColumn.AllowFocus = false;
            this.colVisitStatusID.OptionsColumn.ReadOnly = true;
            this.colVisitStatusID.Width = 88;
            // 
            // colJobStatusID
            // 
            this.colJobStatusID.Caption = "Job Status ID";
            this.colJobStatusID.FieldName = "JobStatusID";
            this.colJobStatusID.Name = "colJobStatusID";
            this.colJobStatusID.OptionsColumn.AllowEdit = false;
            this.colJobStatusID.OptionsColumn.AllowFocus = false;
            this.colJobStatusID.OptionsColumn.ReadOnly = true;
            this.colJobStatusID.Width = 86;
            // 
            // repositoryItemPopupContainerEditDateRange
            // 
            this.repositoryItemPopupContainerEditDateRange.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateRange.LookAndFeel.SkinName = "Blue";
            this.repositoryItemPopupContainerEditDateRange.Name = "repositoryItemPopupContainerEditDateRange";
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControlVisit
            // 
            this.gridControlVisit.DataSource = this.sp06445OMTeamSelfBillingManagerVisitsBindingSource;
            this.gridControlVisit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlVisit.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlVisit.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlVisit.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlVisit.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 20, true, true, "Block Edit Select Records Self-Billing Comments", "blockedit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 25, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 13, true, true, "Reload Data", "reload")});
            this.gridControlVisit.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlVisit_EmbeddedNavigator_ButtonClick);
            this.gridControlVisit.Location = new System.Drawing.Point(0, 0);
            this.gridControlVisit.MainView = this.gridViewVisit;
            this.gridControlVisit.MenuManager = this.barManager1;
            this.gridControlVisit.Name = "gridControlVisit";
            this.gridControlVisit.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditInteger2,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditLatLong,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditHTML0,
            this.repositoryItemHyperLinkEditGrid1,
            this.repositoryItemHyperLinkEditClientSignature,
            this.repositoryItemPictureEdit2,
            this.repositoryItemHyperLinkEditSelfBillingInvoice,
            this.repositoryItemHyperLinkEditClientInvoiceID,
            this.repositoryItemTextEditPercentage});
            this.gridControlVisit.Size = new System.Drawing.Size(1329, 263);
            this.gridControlVisit.TabIndex = 4;
            this.gridControlVisit.ToolTipController = this.toolTipController1;
            this.gridControlVisit.UseEmbeddedNavigator = true;
            this.gridControlVisit.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewVisit});
            // 
            // sp06445OMTeamSelfBillingManagerVisitsBindingSource
            // 
            this.sp06445OMTeamSelfBillingManagerVisitsBindingSource.DataMember = "sp06445_OM_Team_Self_Billing_Manager_Visits";
            this.sp06445OMTeamSelfBillingManagerVisitsBindingSource.DataSource = this.dataSet_OM_Billing;
            // 
            // dataSet_OM_Billing
            // 
            this.dataSet_OM_Billing.DataSetName = "DataSet_OM_Billing";
            this.dataSet_OM_Billing.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "Scheduler_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 7);
            this.imageCollection1.Images.SetKeyName(7, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.linked_documents_16_16, "linked_documents_16_16", typeof(global::WoodPlan5.Properties.Resources), 8);
            this.imageCollection1.Images.SetKeyName(8, "linked_documents_16_16");
            this.imageCollection1.InsertGalleryImage("addgroupheader_16x16.png", "images/reports/addgroupheader_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/reports/addgroupheader_16x16.png"), 9);
            this.imageCollection1.Images.SetKeyName(9, "addgroupheader_16x16.png");
            this.imageCollection1.InsertGalleryImage("groupheader_16x16.png", "images/reports/groupheader_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/reports/groupheader_16x16.png"), 10);
            this.imageCollection1.Images.SetKeyName(10, "groupheader_16x16.png");
            this.imageCollection1.InsertGalleryImage("copy_16x16.png", "images/edit/copy_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/copy_16x16.png"), 11);
            this.imageCollection1.Images.SetKeyName(11, "copy_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.BlockAdd_16x16, "BlockAdd_16x16", typeof(global::WoodPlan5.Properties.Resources), 12);
            this.imageCollection1.Images.SetKeyName(12, "BlockAdd_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 13);
            this.imageCollection1.Images.SetKeyName(13, "refresh_16x16");
            this.imageCollection1.InsertGalleryImage("switchtimescalesto_16x16.png", "images/scheduling/switchtimescalesto_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/scheduling/switchtimescalesto_16x16.png"), 14);
            this.imageCollection1.Images.SetKeyName(14, "switchtimescalesto_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention2_16, "attention2_16", typeof(global::WoodPlan5.Properties.Resources), 15);
            this.imageCollection1.Images.SetKeyName(15, "attention2_16");
            this.imageCollection1.InsertGalleryImage("topbottomrules_16x16.png", "images/conditional%20formatting/topbottomrules_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/conditional%20formatting/topbottomrules_16x16.png"), 16);
            this.imageCollection1.Images.SetKeyName(16, "topbottomrules_16x16.png");
            this.imageCollection1.InsertGalleryImage("dayview_16x16.png", "images/scheduling/dayview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/scheduling/dayview_16x16.png"), 17);
            this.imageCollection1.Images.SetKeyName(17, "dayview_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh2_16x16.png", "images/actions/refresh2_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh2_16x16.png"), 18);
            this.imageCollection1.Images.SetKeyName(18, "refresh2_16x16.png");
            this.imageCollection1.InsertGalleryImage("clearfilter_16x16.png", "images/filter/clearfilter_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/filter/clearfilter_16x16.png"), 19);
            this.imageCollection1.Images.SetKeyName(19, "clearfilter_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.BlockEdit_16x16, "BlockEdit_16x16", typeof(global::WoodPlan5.Properties.Resources), 20);
            this.imageCollection1.Images.SetKeyName(20, "BlockEdit_16x16");
            this.imageCollection1.InsertGalleryImage("today_16x16.png", "images/scheduling/today_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/scheduling/today_16x16.png"), 21);
            this.imageCollection1.Images.SetKeyName(21, "today_16x16.png");
            this.imageCollection1.InsertGalleryImage("assigntome_16x16.png", "images/people/assigntome_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/people/assigntome_16x16.png"), 22);
            this.imageCollection1.Images.SetKeyName(22, "assigntome_16x16.png");
            this.imageCollection1.InsertGalleryImage("showworktimeonly_16x16.png", "images/scheduling/showworktimeonly_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/scheduling/showworktimeonly_16x16.png"), 23);
            this.imageCollection1.Images.SetKeyName(23, "showworktimeonly_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Refresh2_16x16, "Refresh2_16x16", typeof(global::WoodPlan5.Properties.Resources), 24);
            this.imageCollection1.Images.SetKeyName(24, "Refresh2_16x16");
            this.imageCollection1.Images.SetKeyName(25, "drill_up_16.png");
            this.imageCollection1.Images.SetKeyName(26, "drill_up_2_levels_16.png");
            // 
            // gridViewVisit
            // 
            this.gridViewVisit.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVisitID,
            this.colSiteContractID,
            this.colVisitNumber,
            this.colCreatedByStaffID,
            this.colExpectedStartDate,
            this.colExpectedEndDate,
            this.colStartDate,
            this.colEndDate,
            this.colCostCalculationLevel,
            this.colVisitCost,
            this.colVisitSell,
            this.colRemarks,
            this.colStartLatitude,
            this.colStartLongitude,
            this.colFinishLatitude,
            this.colFinishLongitude,
            this.colCostCalculationLevelDescription,
            this.colClientContractID,
            this.colSiteID,
            this.colSiteContractValue,
            this.colClientID,
            this.colContractDirectorID,
            this.colClientContractStartDate,
            this.colClientContractEndDate,
            this.colClientContractActive,
            this.colClientContractValue,
            this.colFinanceClientCode,
            this.colClientName,
            this.colContractType,
            this.colContractStatus,
            this.colContractDirector,
            this.colSiteName,
            this.colLinkedToParent1,
            this.colContractDescription,
            this.colSitePostcode,
            this.colSiteLocationX,
            this.colSiteLocationY,
            this.colLinkedJobCount,
            this.colLinkedOutstandingJobCount,
            this.colLinkedCompletedJobCount,
            this.colCreatedByStaffName,
            this.colSiteAddress,
            this.colVisitStatus,
            this.colVisitStatusID,
            this.colTimeliness,
            this.colWorkNumber,
            this.colVisitCategory,
            this.colVisitCategoryID,
            this.colIssueFound,
            this.colIssueType,
            this.colIssueTypeID,
            this.colIssueRemarks,
            this.colClientSignaturePath,
            this.colImagesFolderOM,
            this.colRework,
            this.colExtraWorkComments,
            this.colExtraWorkRequired,
            this.colExtraWorkType,
            this.colAccidentOnSite,
            this.colAccidentComment,
            this.colSuspendedJobCount,
            this.colLinkedPictureCount,
            this.colFriendlyVisitNumber,
            this.colVisitType,
            this.colVisitTypeID,
            this.colClientPO,
            this.colClientPOID1,
            this.colSiteCode,
            this.colSellCalculationLevel,
            this.colSellCalculationLevelDescription,
            this.colSelected4,
            this.colSelfBillingInvoiceID1,
            this.colClientInvoiceID1,
            this.colCMName,
            this.colDaysAllowed,
            this.colManagerName,
            this.colManagerNotes,
            this.colNoOnetoSign,
            this.colSiteCategory,
            this.colExternalCustomerStatus,
            this.colCompletionSheetNumber,
            this.colStatusIssueID,
            this.colStatusIssue,
            this.colDaysSeparation,
            this.colSiteContractDaysSeparationPercent,
            this.colBillingRequirmentsFulfilled,
            this.colBillingRequirmentsUnfulfilled,
            this.colBillingWarningAuthorised,
            this.colBillingWarningUnauthorised,
            this.colLabourName1,
            this.colLinkedToPersonType,
            this.colLinkedToPersonTypeID,
            this.colDoNotInvoiceClient1,
            this.colDoNotPayContractor1,
            this.colSelfBillingAmount,
            this.colSelfBillingComment,
            this.colSelfBillingStatus,
            this.colSelfBillingInvoiceHeaderID,
            this.colVisitLabourCalculatedID,
            this.colOverdueDays,
            this.colEarlyDays,
            this.colDuration});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colSelfBillingInvoiceHeaderID;
            gridFormatRule1.Name = "FormatSelfBillingStatusBilllingDone";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            gridFormatRule2.Column = this.colBillingRequirmentsUnfulfilled;
            gridFormatRule2.ColumnApplyTo = this.colBillingRequirmentsFulfilled;
            gridFormatRule2.Name = "FormatBillingRequirementFulfilled";
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.PredefinedName = "Green Fill, Green Text";
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            gridFormatRule3.Column = this.colBillingRequirmentsUnfulfilled;
            gridFormatRule3.ColumnApplyTo = this.colBillingRequirmentsUnfulfilled;
            gridFormatRule3.Name = "FormatBillingRequirementUnfulfilled";
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue3.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue3.Value1 = 0;
            gridFormatRule3.Rule = formatConditionRuleValue3;
            gridFormatRule4.Column = this.colBillingWarningUnauthorised;
            gridFormatRule4.ColumnApplyTo = this.colBillingWarningAuthorised;
            gridFormatRule4.Name = "FormatBillingWarningAuthorised";
            formatConditionRuleValue4.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue4.PredefinedName = "Green Fill, Green Text";
            formatConditionRuleValue4.Value1 = 0;
            gridFormatRule4.Rule = formatConditionRuleValue4;
            gridFormatRule5.Column = this.colBillingWarningUnauthorised;
            gridFormatRule5.ColumnApplyTo = this.colBillingWarningUnauthorised;
            gridFormatRule5.Name = "FormatBillingWarningUnauthorised";
            formatConditionRuleValue5.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue5.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue5.Value1 = 0;
            gridFormatRule5.Rule = formatConditionRuleValue5;
            gridFormatRule6.Column = this.colOverdueDays;
            gridFormatRule6.Name = "FormatConditionRuleOverdueDays";
            formatConditionRuleValue6.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue6.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue6.Value1 = 0;
            gridFormatRule6.Rule = formatConditionRuleValue6;
            gridFormatRule7.Column = this.colEarlyDays;
            gridFormatRule7.Name = "FormatConditionRuleEarlyDays";
            formatConditionRuleValue7.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue7.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue7.Value1 = 0;
            gridFormatRule7.Rule = formatConditionRuleValue7;
            this.gridViewVisit.FormatRules.Add(gridFormatRule1);
            this.gridViewVisit.FormatRules.Add(gridFormatRule2);
            this.gridViewVisit.FormatRules.Add(gridFormatRule3);
            this.gridViewVisit.FormatRules.Add(gridFormatRule4);
            this.gridViewVisit.FormatRules.Add(gridFormatRule5);
            this.gridViewVisit.FormatRules.Add(gridFormatRule6);
            this.gridViewVisit.FormatRules.Add(gridFormatRule7);
            this.gridViewVisit.GridControl = this.gridControlVisit;
            this.gridViewVisit.GroupCount = 3;
            this.gridViewVisit.Name = "gridViewVisit";
            this.gridViewVisit.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewVisit.OptionsFind.AlwaysVisible = true;
            this.gridViewVisit.OptionsFind.FindDelay = 2000;
            this.gridViewVisit.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewVisit.OptionsLayout.StoreAppearance = true;
            this.gridViewVisit.OptionsLayout.StoreFormatRules = true;
            this.gridViewVisit.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewVisit.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewVisit.OptionsSelection.MultiSelect = true;
            this.gridViewVisit.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewVisit.OptionsView.ColumnAutoWidth = false;
            this.gridViewVisit.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewVisit.OptionsView.ShowGroupPanel = false;
            this.gridViewVisit.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colVisitNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEndDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewVisit.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewVisit_CustomDrawCell);
            this.gridViewVisit.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewVisit_CustomRowCellEdit);
            this.gridViewVisit.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewVisit.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewVisit.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewVisit.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewVisit_ShowingEditor);
            this.gridViewVisit.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewVisit.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewVisit.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridViewVisit_CustomUnboundColumnData);
            this.gridViewVisit.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewVisit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewVisit_MouseUp);
            this.gridViewVisit.DoubleClick += new System.EventHandler(this.gridViewVisit_DoubleClick);
            this.gridViewVisit.GotFocus += new System.EventHandler(this.gridViewVisit_GotFocus);
            this.gridViewVisit.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridViewVisit_ValidatingEditor);
            // 
            // colVisitID
            // 
            this.colVisitID.Caption = "Visit ID";
            this.colVisitID.FieldName = "VisitID";
            this.colVisitID.Name = "colVisitID";
            this.colVisitID.OptionsColumn.AllowEdit = false;
            this.colVisitID.OptionsColumn.AllowFocus = false;
            this.colVisitID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteContractID
            // 
            this.colSiteContractID.Caption = "Site Contract ID";
            this.colSiteContractID.FieldName = "SiteContractID";
            this.colSiteContractID.Name = "colSiteContractID";
            this.colSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSiteContractID.Width = 98;
            // 
            // colVisitNumber
            // 
            this.colVisitNumber.Caption = "Visit #";
            this.colVisitNumber.ColumnEdit = this.repositoryItemTextEditInteger2;
            this.colVisitNumber.FieldName = "VisitNumber";
            this.colVisitNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colVisitNumber.Name = "colVisitNumber";
            this.colVisitNumber.OptionsColumn.AllowEdit = false;
            this.colVisitNumber.OptionsColumn.AllowFocus = false;
            this.colVisitNumber.OptionsColumn.ReadOnly = true;
            this.colVisitNumber.Visible = true;
            this.colVisitNumber.VisibleIndex = 0;
            this.colVisitNumber.Width = 124;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 116;
            // 
            // colExpectedStartDate
            // 
            this.colExpectedStartDate.Caption = "Expected Start";
            this.colExpectedStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colExpectedStartDate.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpectedStartDate.Name = "colExpectedStartDate";
            this.colExpectedStartDate.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpectedStartDate.Visible = true;
            this.colExpectedStartDate.VisibleIndex = 12;
            this.colExpectedStartDate.Width = 104;
            // 
            // colExpectedEndDate
            // 
            this.colExpectedEndDate.Caption = "Expected End";
            this.colExpectedEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colExpectedEndDate.FieldName = "ExpectedEndDate";
            this.colExpectedEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpectedEndDate.Name = "colExpectedEndDate";
            this.colExpectedEndDate.OptionsColumn.AllowEdit = false;
            this.colExpectedEndDate.OptionsColumn.AllowFocus = false;
            this.colExpectedEndDate.OptionsColumn.ReadOnly = true;
            this.colExpectedEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpectedEndDate.Visible = true;
            this.colExpectedEndDate.VisibleIndex = 11;
            this.colExpectedEndDate.Width = 100;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start";
            this.colStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 1;
            this.colStartDate.Width = 100;
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "End";
            this.colEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 2;
            this.colEndDate.Width = 100;
            // 
            // colCostCalculationLevel
            // 
            this.colCostCalculationLevel.Caption = "Cost Calculation ID";
            this.colCostCalculationLevel.FieldName = "CostCalculationLevel";
            this.colCostCalculationLevel.Name = "colCostCalculationLevel";
            this.colCostCalculationLevel.OptionsColumn.AllowEdit = false;
            this.colCostCalculationLevel.OptionsColumn.AllowFocus = false;
            this.colCostCalculationLevel.OptionsColumn.ReadOnly = true;
            this.colCostCalculationLevel.Width = 140;
            // 
            // colVisitCost
            // 
            this.colVisitCost.Caption = "Visit Cost";
            this.colVisitCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colVisitCost.FieldName = "VisitCost";
            this.colVisitCost.Name = "colVisitCost";
            this.colVisitCost.OptionsColumn.AllowEdit = false;
            this.colVisitCost.OptionsColumn.AllowFocus = false;
            this.colVisitCost.OptionsColumn.ReadOnly = true;
            this.colVisitCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitCost", "{0:c}")});
            this.colVisitCost.Visible = true;
            this.colVisitCost.VisibleIndex = 28;
            // 
            // colVisitSell
            // 
            this.colVisitSell.Caption = "Visit Sell";
            this.colVisitSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colVisitSell.FieldName = "VisitSell";
            this.colVisitSell.Name = "colVisitSell";
            this.colVisitSell.OptionsColumn.AllowEdit = false;
            this.colVisitSell.OptionsColumn.AllowFocus = false;
            this.colVisitSell.OptionsColumn.ReadOnly = true;
            this.colVisitSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitSell", "{0:c}")});
            this.colVisitSell.Visible = true;
            this.colVisitSell.VisibleIndex = 29;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 68;
            // 
            // colStartLatitude
            // 
            this.colStartLatitude.Caption = "Start Latitude";
            this.colStartLatitude.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colStartLatitude.FieldName = "StartLatitude";
            this.colStartLatitude.Name = "colStartLatitude";
            this.colStartLatitude.OptionsColumn.AllowEdit = false;
            this.colStartLatitude.OptionsColumn.AllowFocus = false;
            this.colStartLatitude.OptionsColumn.ReadOnly = true;
            this.colStartLatitude.Visible = true;
            this.colStartLatitude.VisibleIndex = 38;
            this.colStartLatitude.Width = 87;
            // 
            // colStartLongitude
            // 
            this.colStartLongitude.Caption = "Start Longitude";
            this.colStartLongitude.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colStartLongitude.FieldName = "StartLongitude";
            this.colStartLongitude.Name = "colStartLongitude";
            this.colStartLongitude.OptionsColumn.AllowEdit = false;
            this.colStartLongitude.OptionsColumn.AllowFocus = false;
            this.colStartLongitude.OptionsColumn.ReadOnly = true;
            this.colStartLongitude.Visible = true;
            this.colStartLongitude.VisibleIndex = 39;
            this.colStartLongitude.Width = 95;
            // 
            // colFinishLatitude
            // 
            this.colFinishLatitude.Caption = "Finish Latitude";
            this.colFinishLatitude.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colFinishLatitude.FieldName = "FinishLatitude";
            this.colFinishLatitude.Name = "colFinishLatitude";
            this.colFinishLatitude.OptionsColumn.AllowEdit = false;
            this.colFinishLatitude.OptionsColumn.AllowFocus = false;
            this.colFinishLatitude.OptionsColumn.ReadOnly = true;
            this.colFinishLatitude.Visible = true;
            this.colFinishLatitude.VisibleIndex = 40;
            this.colFinishLatitude.Width = 90;
            // 
            // colFinishLongitude
            // 
            this.colFinishLongitude.Caption = "Finish Longitude";
            this.colFinishLongitude.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colFinishLongitude.FieldName = "FinishLongitude";
            this.colFinishLongitude.Name = "colFinishLongitude";
            this.colFinishLongitude.OptionsColumn.AllowEdit = false;
            this.colFinishLongitude.OptionsColumn.AllowFocus = false;
            this.colFinishLongitude.OptionsColumn.ReadOnly = true;
            this.colFinishLongitude.Visible = true;
            this.colFinishLongitude.VisibleIndex = 41;
            this.colFinishLongitude.Width = 98;
            // 
            // colCostCalculationLevelDescription
            // 
            this.colCostCalculationLevelDescription.Caption = "Cost Calculation";
            this.colCostCalculationLevelDescription.FieldName = "CostCalculationLevelDescription";
            this.colCostCalculationLevelDescription.Name = "colCostCalculationLevelDescription";
            this.colCostCalculationLevelDescription.OptionsColumn.AllowEdit = false;
            this.colCostCalculationLevelDescription.OptionsColumn.AllowFocus = false;
            this.colCostCalculationLevelDescription.OptionsColumn.ReadOnly = true;
            this.colCostCalculationLevelDescription.Visible = true;
            this.colCostCalculationLevelDescription.VisibleIndex = 26;
            this.colCostCalculationLevelDescription.Width = 100;
            // 
            // colClientContractID
            // 
            this.colClientContractID.Caption = "Client Contract ID";
            this.colClientContractID.FieldName = "ClientContractID";
            this.colClientContractID.Name = "colClientContractID";
            this.colClientContractID.OptionsColumn.AllowEdit = false;
            this.colClientContractID.OptionsColumn.AllowFocus = false;
            this.colClientContractID.OptionsColumn.ReadOnly = true;
            this.colClientContractID.Width = 107;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.Visible = true;
            this.colSiteID.VisibleIndex = 34;
            this.colSiteID.Width = 51;
            // 
            // colSiteContractValue
            // 
            this.colSiteContractValue.Caption = "Site Contract Value";
            this.colSiteContractValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSiteContractValue.FieldName = "SiteContractValue";
            this.colSiteContractValue.Name = "colSiteContractValue";
            this.colSiteContractValue.OptionsColumn.AllowEdit = false;
            this.colSiteContractValue.OptionsColumn.AllowFocus = false;
            this.colSiteContractValue.OptionsColumn.ReadOnly = true;
            this.colSiteContractValue.Width = 113;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colContractDirectorID
            // 
            this.colContractDirectorID.Caption = "Contract Director ID";
            this.colContractDirectorID.FieldName = "ContractDirectorID";
            this.colContractDirectorID.Name = "colContractDirectorID";
            this.colContractDirectorID.OptionsColumn.AllowEdit = false;
            this.colContractDirectorID.OptionsColumn.AllowFocus = false;
            this.colContractDirectorID.OptionsColumn.ReadOnly = true;
            this.colContractDirectorID.Width = 118;
            // 
            // colClientContractStartDate
            // 
            this.colClientContractStartDate.Caption = "Client Contract Start";
            this.colClientContractStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colClientContractStartDate.FieldName = "ClientContractStartDate";
            this.colClientContractStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colClientContractStartDate.Name = "colClientContractStartDate";
            this.colClientContractStartDate.OptionsColumn.AllowEdit = false;
            this.colClientContractStartDate.OptionsColumn.AllowFocus = false;
            this.colClientContractStartDate.OptionsColumn.ReadOnly = true;
            this.colClientContractStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colClientContractStartDate.Width = 120;
            // 
            // colClientContractEndDate
            // 
            this.colClientContractEndDate.Caption = "Client Contract End";
            this.colClientContractEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colClientContractEndDate.FieldName = "ClientContractEndDate";
            this.colClientContractEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colClientContractEndDate.Name = "colClientContractEndDate";
            this.colClientContractEndDate.OptionsColumn.AllowEdit = false;
            this.colClientContractEndDate.OptionsColumn.AllowFocus = false;
            this.colClientContractEndDate.OptionsColumn.ReadOnly = true;
            this.colClientContractEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colClientContractEndDate.Width = 114;
            // 
            // colClientContractActive
            // 
            this.colClientContractActive.Caption = "Client Contract Active";
            this.colClientContractActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colClientContractActive.FieldName = "ClientContractActive";
            this.colClientContractActive.Name = "colClientContractActive";
            this.colClientContractActive.OptionsColumn.AllowEdit = false;
            this.colClientContractActive.OptionsColumn.AllowFocus = false;
            this.colClientContractActive.OptionsColumn.ReadOnly = true;
            this.colClientContractActive.Width = 126;
            // 
            // colClientContractValue
            // 
            this.colClientContractValue.Caption = "Client Contract Value";
            this.colClientContractValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colClientContractValue.FieldName = "ClientContractValue";
            this.colClientContractValue.Name = "colClientContractValue";
            this.colClientContractValue.OptionsColumn.AllowEdit = false;
            this.colClientContractValue.OptionsColumn.AllowFocus = false;
            this.colClientContractValue.OptionsColumn.ReadOnly = true;
            this.colClientContractValue.Width = 122;
            // 
            // colFinanceClientCode
            // 
            this.colFinanceClientCode.Caption = "Finance Client Code";
            this.colFinanceClientCode.FieldName = "FinanceClientCode";
            this.colFinanceClientCode.Name = "colFinanceClientCode";
            this.colFinanceClientCode.OptionsColumn.AllowEdit = false;
            this.colFinanceClientCode.OptionsColumn.AllowFocus = false;
            this.colFinanceClientCode.OptionsColumn.ReadOnly = true;
            this.colFinanceClientCode.Width = 116;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 60;
            this.colClientName.Width = 174;
            // 
            // colContractType
            // 
            this.colContractType.Caption = "Contract Type";
            this.colContractType.FieldName = "ContractType";
            this.colContractType.Name = "colContractType";
            this.colContractType.OptionsColumn.AllowEdit = false;
            this.colContractType.OptionsColumn.AllowFocus = false;
            this.colContractType.OptionsColumn.ReadOnly = true;
            this.colContractType.Visible = true;
            this.colContractType.VisibleIndex = 43;
            this.colContractType.Width = 117;
            // 
            // colContractStatus
            // 
            this.colContractStatus.Caption = "Contract Status";
            this.colContractStatus.FieldName = "ContractStatus";
            this.colContractStatus.Name = "colContractStatus";
            this.colContractStatus.OptionsColumn.AllowEdit = false;
            this.colContractStatus.OptionsColumn.AllowFocus = false;
            this.colContractStatus.OptionsColumn.ReadOnly = true;
            this.colContractStatus.Visible = true;
            this.colContractStatus.VisibleIndex = 44;
            this.colContractStatus.Width = 117;
            // 
            // colContractDirector
            // 
            this.colContractDirector.Caption = "Contract Director";
            this.colContractDirector.FieldName = "ContractDirector";
            this.colContractDirector.Name = "colContractDirector";
            this.colContractDirector.OptionsColumn.AllowEdit = false;
            this.colContractDirector.OptionsColumn.AllowFocus = false;
            this.colContractDirector.OptionsColumn.ReadOnly = true;
            this.colContractDirector.Visible = true;
            this.colContractDirector.VisibleIndex = 45;
            this.colContractDirector.Width = 122;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 21;
            this.colSiteName.Width = 227;
            // 
            // colLinkedToParent1
            // 
            this.colLinkedToParent1.Caption = "Linked To Site Contract";
            this.colLinkedToParent1.ColumnEdit = this.repositoryItemTextEditHTML0;
            this.colLinkedToParent1.FieldName = "LinkedToParent";
            this.colLinkedToParent1.Name = "colLinkedToParent1";
            this.colLinkedToParent1.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent1.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent1.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent1.Width = 333;
            // 
            // colContractDescription
            // 
            this.colContractDescription.Caption = "Contract Description";
            this.colContractDescription.FieldName = "ContractDescription";
            this.colContractDescription.Name = "colContractDescription";
            this.colContractDescription.OptionsColumn.AllowEdit = false;
            this.colContractDescription.OptionsColumn.AllowFocus = false;
            this.colContractDescription.OptionsColumn.ReadOnly = true;
            this.colContractDescription.Visible = true;
            this.colContractDescription.VisibleIndex = 61;
            this.colContractDescription.Width = 225;
            // 
            // colSitePostcode
            // 
            this.colSitePostcode.Caption = "Site Postcode";
            this.colSitePostcode.FieldName = "SitePostcode";
            this.colSitePostcode.Name = "colSitePostcode";
            this.colSitePostcode.OptionsColumn.AllowEdit = false;
            this.colSitePostcode.OptionsColumn.AllowFocus = false;
            this.colSitePostcode.OptionsColumn.ReadOnly = true;
            this.colSitePostcode.Width = 86;
            // 
            // colSiteLocationX
            // 
            this.colSiteLocationX.Caption = "Site Latitude";
            this.colSiteLocationX.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colSiteLocationX.FieldName = "SiteLocationX";
            this.colSiteLocationX.Name = "colSiteLocationX";
            this.colSiteLocationX.OptionsColumn.AllowEdit = false;
            this.colSiteLocationX.OptionsColumn.AllowFocus = false;
            this.colSiteLocationX.OptionsColumn.ReadOnly = true;
            this.colSiteLocationX.Width = 81;
            // 
            // colSiteLocationY
            // 
            this.colSiteLocationY.Caption = "Site Longitude";
            this.colSiteLocationY.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colSiteLocationY.FieldName = "SiteLocationY";
            this.colSiteLocationY.Name = "colSiteLocationY";
            this.colSiteLocationY.OptionsColumn.AllowEdit = false;
            this.colSiteLocationY.OptionsColumn.AllowFocus = false;
            this.colSiteLocationY.OptionsColumn.ReadOnly = true;
            this.colSiteLocationY.Width = 89;
            // 
            // colLinkedJobCount
            // 
            this.colLinkedJobCount.Caption = "Linked Jobs Total";
            this.colLinkedJobCount.ColumnEdit = this.repositoryItemHyperLinkEditGrid1;
            this.colLinkedJobCount.FieldName = "LinkedJobCount";
            this.colLinkedJobCount.Name = "colLinkedJobCount";
            this.colLinkedJobCount.OptionsColumn.ReadOnly = true;
            this.colLinkedJobCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinkedJobCount", "{0:n}")});
            this.colLinkedJobCount.Width = 103;
            // 
            // colLinkedOutstandingJobCount
            // 
            this.colLinkedOutstandingJobCount.Caption = "Linked Jobs Outstanding";
            this.colLinkedOutstandingJobCount.ColumnEdit = this.repositoryItemHyperLinkEditGrid1;
            this.colLinkedOutstandingJobCount.FieldName = "LinkedOutstandingJobCount";
            this.colLinkedOutstandingJobCount.Name = "colLinkedOutstandingJobCount";
            this.colLinkedOutstandingJobCount.OptionsColumn.ReadOnly = true;
            this.colLinkedOutstandingJobCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinkedOutstandingJobCount", "{0:n}")});
            this.colLinkedOutstandingJobCount.Visible = true;
            this.colLinkedOutstandingJobCount.VisibleIndex = 46;
            this.colLinkedOutstandingJobCount.Width = 138;
            // 
            // colLinkedCompletedJobCount
            // 
            this.colLinkedCompletedJobCount.Caption = "Linked Jobs Completed";
            this.colLinkedCompletedJobCount.ColumnEdit = this.repositoryItemHyperLinkEditGrid1;
            this.colLinkedCompletedJobCount.FieldName = "LinkedCompletedJobCount";
            this.colLinkedCompletedJobCount.Name = "colLinkedCompletedJobCount";
            this.colLinkedCompletedJobCount.OptionsColumn.ReadOnly = true;
            this.colLinkedCompletedJobCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinkedCompletedJobCount", "{0:n}")});
            this.colLinkedCompletedJobCount.Visible = true;
            this.colLinkedCompletedJobCount.VisibleIndex = 47;
            this.colLinkedCompletedJobCount.Width = 130;
            // 
            // colCreatedByStaffName
            // 
            this.colCreatedByStaffName.Caption = "Created By";
            this.colCreatedByStaffName.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName.Name = "colCreatedByStaffName";
            this.colCreatedByStaffName.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName.Visible = true;
            this.colCreatedByStaffName.VisibleIndex = 32;
            this.colCreatedByStaffName.Width = 122;
            // 
            // colSiteAddress
            // 
            this.colSiteAddress.Caption = "Site Address";
            this.colSiteAddress.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colSiteAddress.FieldName = "SiteAddress";
            this.colSiteAddress.Name = "colSiteAddress";
            this.colSiteAddress.OptionsColumn.ReadOnly = true;
            this.colSiteAddress.Visible = true;
            this.colSiteAddress.VisibleIndex = 37;
            this.colSiteAddress.Width = 125;
            // 
            // colVisitStatus
            // 
            this.colVisitStatus.Caption = "System Status";
            this.colVisitStatus.FieldName = "VisitStatus";
            this.colVisitStatus.Name = "colVisitStatus";
            this.colVisitStatus.OptionsColumn.AllowEdit = false;
            this.colVisitStatus.OptionsColumn.AllowFocus = false;
            this.colVisitStatus.OptionsColumn.ReadOnly = true;
            this.colVisitStatus.Visible = true;
            this.colVisitStatus.VisibleIndex = 21;
            this.colVisitStatus.Width = 130;
            // 
            // colTimeliness
            // 
            this.colTimeliness.Caption = "Visit Status";
            this.colTimeliness.FieldName = "Timeliness";
            this.colTimeliness.Name = "colTimeliness";
            this.colTimeliness.OptionsColumn.AllowEdit = false;
            this.colTimeliness.OptionsColumn.AllowFocus = false;
            this.colTimeliness.OptionsColumn.ReadOnly = true;
            this.colTimeliness.Visible = true;
            this.colTimeliness.VisibleIndex = 22;
            this.colTimeliness.Width = 89;
            // 
            // colWorkNumber
            // 
            this.colWorkNumber.Caption = "Work Number";
            this.colWorkNumber.FieldName = "WorkNumber";
            this.colWorkNumber.Name = "colWorkNumber";
            this.colWorkNumber.OptionsColumn.AllowEdit = false;
            this.colWorkNumber.OptionsColumn.AllowFocus = false;
            this.colWorkNumber.OptionsColumn.ReadOnly = true;
            this.colWorkNumber.Visible = true;
            this.colWorkNumber.VisibleIndex = 25;
            this.colWorkNumber.Width = 86;
            // 
            // colVisitCategory
            // 
            this.colVisitCategory.Caption = "Visit Category";
            this.colVisitCategory.FieldName = "VisitCategory";
            this.colVisitCategory.Name = "colVisitCategory";
            this.colVisitCategory.OptionsColumn.AllowEdit = false;
            this.colVisitCategory.OptionsColumn.AllowFocus = false;
            this.colVisitCategory.OptionsColumn.ReadOnly = true;
            this.colVisitCategory.Visible = true;
            this.colVisitCategory.VisibleIndex = 19;
            this.colVisitCategory.Width = 130;
            // 
            // colVisitCategoryID
            // 
            this.colVisitCategoryID.Caption = "Visit Category ID";
            this.colVisitCategoryID.FieldName = "VisitCategoryID";
            this.colVisitCategoryID.Name = "colVisitCategoryID";
            this.colVisitCategoryID.OptionsColumn.AllowEdit = false;
            this.colVisitCategoryID.OptionsColumn.AllowFocus = false;
            this.colVisitCategoryID.OptionsColumn.ReadOnly = true;
            this.colVisitCategoryID.Width = 102;
            // 
            // colIssueFound
            // 
            this.colIssueFound.Caption = "Issue Found";
            this.colIssueFound.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIssueFound.FieldName = "IssueFound";
            this.colIssueFound.Name = "colIssueFound";
            this.colIssueFound.OptionsColumn.AllowEdit = false;
            this.colIssueFound.OptionsColumn.AllowFocus = false;
            this.colIssueFound.OptionsColumn.ReadOnly = true;
            this.colIssueFound.Visible = true;
            this.colIssueFound.VisibleIndex = 54;
            this.colIssueFound.Width = 80;
            // 
            // colIssueType
            // 
            this.colIssueType.Caption = "Issue Type";
            this.colIssueType.FieldName = "IssueType";
            this.colIssueType.Name = "colIssueType";
            this.colIssueType.OptionsColumn.AllowEdit = false;
            this.colIssueType.OptionsColumn.AllowFocus = false;
            this.colIssueType.OptionsColumn.ReadOnly = true;
            this.colIssueType.Visible = true;
            this.colIssueType.VisibleIndex = 55;
            // 
            // colIssueTypeID
            // 
            this.colIssueTypeID.Caption = "Issue Type ID";
            this.colIssueTypeID.FieldName = "IssueTypeID";
            this.colIssueTypeID.Name = "colIssueTypeID";
            this.colIssueTypeID.OptionsColumn.AllowEdit = false;
            this.colIssueTypeID.OptionsColumn.AllowFocus = false;
            this.colIssueTypeID.OptionsColumn.ReadOnly = true;
            this.colIssueTypeID.Width = 88;
            // 
            // colIssueRemarks
            // 
            this.colIssueRemarks.Caption = "Issue Remarks";
            this.colIssueRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colIssueRemarks.FieldName = "IssueRemarks";
            this.colIssueRemarks.Name = "colIssueRemarks";
            this.colIssueRemarks.OptionsColumn.ReadOnly = true;
            this.colIssueRemarks.Visible = true;
            this.colIssueRemarks.VisibleIndex = 56;
            this.colIssueRemarks.Width = 91;
            // 
            // colClientSignaturePath
            // 
            this.colClientSignaturePath.Caption = "Client Signature";
            this.colClientSignaturePath.ColumnEdit = this.repositoryItemHyperLinkEditClientSignature;
            this.colClientSignaturePath.FieldName = "ClientSignaturePath";
            this.colClientSignaturePath.Name = "colClientSignaturePath";
            this.colClientSignaturePath.OptionsColumn.ReadOnly = true;
            this.colClientSignaturePath.Visible = true;
            this.colClientSignaturePath.VisibleIndex = 50;
            this.colClientSignaturePath.Width = 97;
            // 
            // colImagesFolderOM
            // 
            this.colImagesFolderOM.Caption = "Client Images Folder";
            this.colImagesFolderOM.FieldName = "ImagesFolderOM";
            this.colImagesFolderOM.Name = "colImagesFolderOM";
            this.colImagesFolderOM.OptionsColumn.AllowEdit = false;
            this.colImagesFolderOM.OptionsColumn.AllowFocus = false;
            this.colImagesFolderOM.OptionsColumn.ReadOnly = true;
            this.colImagesFolderOM.Width = 119;
            // 
            // colRework
            // 
            this.colRework.Caption = "Rework";
            this.colRework.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colRework.FieldName = "Rework";
            this.colRework.Name = "colRework";
            this.colRework.OptionsColumn.AllowEdit = false;
            this.colRework.OptionsColumn.AllowFocus = false;
            this.colRework.OptionsColumn.ReadOnly = true;
            this.colRework.Visible = true;
            this.colRework.VisibleIndex = 57;
            this.colRework.Width = 57;
            // 
            // colExtraWorkComments
            // 
            this.colExtraWorkComments.Caption = "Extra Work Remarks";
            this.colExtraWorkComments.FieldName = "ExtraWorkComments";
            this.colExtraWorkComments.Name = "colExtraWorkComments";
            this.colExtraWorkComments.OptionsColumn.AllowEdit = false;
            this.colExtraWorkComments.OptionsColumn.AllowFocus = false;
            this.colExtraWorkComments.OptionsColumn.ReadOnly = true;
            this.colExtraWorkComments.Visible = true;
            this.colExtraWorkComments.VisibleIndex = 60;
            this.colExtraWorkComments.Width = 119;
            // 
            // colExtraWorkRequired
            // 
            this.colExtraWorkRequired.Caption = "Extra Work Required";
            this.colExtraWorkRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colExtraWorkRequired.FieldName = "ExtraWorkRequired";
            this.colExtraWorkRequired.Name = "colExtraWorkRequired";
            this.colExtraWorkRequired.OptionsColumn.AllowEdit = false;
            this.colExtraWorkRequired.OptionsColumn.AllowFocus = false;
            this.colExtraWorkRequired.OptionsColumn.ReadOnly = true;
            this.colExtraWorkRequired.Visible = true;
            this.colExtraWorkRequired.VisibleIndex = 58;
            this.colExtraWorkRequired.Width = 121;
            // 
            // colExtraWorkType
            // 
            this.colExtraWorkType.Caption = "Extra Work Type";
            this.colExtraWorkType.FieldName = "ExtraWorkType";
            this.colExtraWorkType.Name = "colExtraWorkType";
            this.colExtraWorkType.OptionsColumn.AllowEdit = false;
            this.colExtraWorkType.OptionsColumn.AllowFocus = false;
            this.colExtraWorkType.OptionsColumn.ReadOnly = true;
            this.colExtraWorkType.Visible = true;
            this.colExtraWorkType.VisibleIndex = 59;
            this.colExtraWorkType.Width = 102;
            // 
            // colAccidentOnSite
            // 
            this.colAccidentOnSite.Caption = "Accident on Site";
            this.colAccidentOnSite.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colAccidentOnSite.FieldName = "AccidentOnSite";
            this.colAccidentOnSite.Name = "colAccidentOnSite";
            this.colAccidentOnSite.OptionsColumn.AllowEdit = false;
            this.colAccidentOnSite.OptionsColumn.AllowFocus = false;
            this.colAccidentOnSite.OptionsColumn.ReadOnly = true;
            this.colAccidentOnSite.Visible = true;
            this.colAccidentOnSite.VisibleIndex = 61;
            this.colAccidentOnSite.Width = 98;
            // 
            // colAccidentComment
            // 
            this.colAccidentComment.Caption = "Accident Comments";
            this.colAccidentComment.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colAccidentComment.FieldName = "AccidentComment";
            this.colAccidentComment.Name = "colAccidentComment";
            this.colAccidentComment.OptionsColumn.ReadOnly = true;
            this.colAccidentComment.Visible = true;
            this.colAccidentComment.VisibleIndex = 62;
            this.colAccidentComment.Width = 115;
            // 
            // colSuspendedJobCount
            // 
            this.colSuspendedJobCount.Caption = "Suspended Jobs";
            this.colSuspendedJobCount.ColumnEdit = this.repositoryItemHyperLinkEditGrid1;
            this.colSuspendedJobCount.FieldName = "SuspendedJobCount";
            this.colSuspendedJobCount.Name = "colSuspendedJobCount";
            this.colSuspendedJobCount.OptionsColumn.ReadOnly = true;
            this.colSuspendedJobCount.Visible = true;
            this.colSuspendedJobCount.VisibleIndex = 49;
            this.colSuspendedJobCount.Width = 97;
            // 
            // colLinkedPictureCount
            // 
            this.colLinkedPictureCount.Caption = "Linked Pictures";
            this.colLinkedPictureCount.ColumnEdit = this.repositoryItemHyperLinkEditGrid1;
            this.colLinkedPictureCount.FieldName = "LinkedPictureCount";
            this.colLinkedPictureCount.Name = "colLinkedPictureCount";
            this.colLinkedPictureCount.OptionsColumn.ReadOnly = true;
            this.colLinkedPictureCount.Visible = true;
            this.colLinkedPictureCount.VisibleIndex = 48;
            this.colLinkedPictureCount.Width = 90;
            // 
            // colFriendlyVisitNumber
            // 
            this.colFriendlyVisitNumber.Caption = "Friendly Visit #";
            this.colFriendlyVisitNumber.FieldName = "FriendlyVisitNumber";
            this.colFriendlyVisitNumber.Name = "colFriendlyVisitNumber";
            this.colFriendlyVisitNumber.OptionsColumn.AllowEdit = false;
            this.colFriendlyVisitNumber.OptionsColumn.AllowFocus = false;
            this.colFriendlyVisitNumber.OptionsColumn.ReadOnly = true;
            this.colFriendlyVisitNumber.Visible = true;
            this.colFriendlyVisitNumber.VisibleIndex = 3;
            this.colFriendlyVisitNumber.Width = 90;
            // 
            // colVisitType
            // 
            this.colVisitType.Caption = "Visit Type";
            this.colVisitType.FieldName = "VisitType";
            this.colVisitType.Name = "colVisitType";
            this.colVisitType.OptionsColumn.AllowEdit = false;
            this.colVisitType.OptionsColumn.AllowFocus = false;
            this.colVisitType.OptionsColumn.ReadOnly = true;
            this.colVisitType.Visible = true;
            this.colVisitType.VisibleIndex = 20;
            this.colVisitType.Width = 99;
            // 
            // colVisitTypeID
            // 
            this.colVisitTypeID.Caption = "Visit Type ID";
            this.colVisitTypeID.FieldName = "VisitTypeID";
            this.colVisitTypeID.Name = "colVisitTypeID";
            this.colVisitTypeID.OptionsColumn.AllowEdit = false;
            this.colVisitTypeID.OptionsColumn.AllowFocus = false;
            this.colVisitTypeID.OptionsColumn.ReadOnly = true;
            this.colVisitTypeID.Width = 79;
            // 
            // colClientPO
            // 
            this.colClientPO.Caption = "Client PO #";
            this.colClientPO.FieldName = "ClientPO";
            this.colClientPO.Name = "colClientPO";
            this.colClientPO.OptionsColumn.AllowEdit = false;
            this.colClientPO.OptionsColumn.AllowFocus = false;
            this.colClientPO.OptionsColumn.ReadOnly = true;
            this.colClientPO.Visible = true;
            this.colClientPO.VisibleIndex = 33;
            this.colClientPO.Width = 112;
            // 
            // colClientPOID1
            // 
            this.colClientPOID1.Caption = "Client PO ID";
            this.colClientPOID1.FieldName = "ClientPOID";
            this.colClientPOID1.Name = "colClientPOID1";
            this.colClientPOID1.OptionsColumn.AllowEdit = false;
            this.colClientPOID1.OptionsColumn.AllowFocus = false;
            this.colClientPOID1.OptionsColumn.ReadOnly = true;
            this.colClientPOID1.Width = 77;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Visible = true;
            this.colSiteCode.VisibleIndex = 35;
            // 
            // colSellCalculationLevel
            // 
            this.colSellCalculationLevel.Caption = "Sell Calculation ID";
            this.colSellCalculationLevel.FieldName = "SellCalculationLevel";
            this.colSellCalculationLevel.Name = "colSellCalculationLevel";
            this.colSellCalculationLevel.OptionsColumn.AllowEdit = false;
            this.colSellCalculationLevel.OptionsColumn.AllowFocus = false;
            this.colSellCalculationLevel.OptionsColumn.ReadOnly = true;
            this.colSellCalculationLevel.Width = 104;
            // 
            // colSellCalculationLevelDescription
            // 
            this.colSellCalculationLevelDescription.Caption = "Sell Calculation";
            this.colSellCalculationLevelDescription.FieldName = "SellCalculationLevelDescription";
            this.colSellCalculationLevelDescription.Name = "colSellCalculationLevelDescription";
            this.colSellCalculationLevelDescription.OptionsColumn.AllowEdit = false;
            this.colSellCalculationLevelDescription.OptionsColumn.AllowFocus = false;
            this.colSellCalculationLevelDescription.OptionsColumn.ReadOnly = true;
            this.colSellCalculationLevelDescription.Visible = true;
            this.colSellCalculationLevelDescription.VisibleIndex = 27;
            this.colSellCalculationLevelDescription.Width = 100;
            // 
            // colSelected4
            // 
            this.colSelected4.Caption = "Selected";
            this.colSelected4.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSelected4.FieldName = "Selected";
            this.colSelected4.Name = "colSelected4";
            this.colSelected4.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Selected", "{0:0}")});
            this.colSelected4.Width = 60;
            // 
            // colSelfBillingInvoiceID1
            // 
            this.colSelfBillingInvoiceID1.Caption = "Self Billing Invoice";
            this.colSelfBillingInvoiceID1.ColumnEdit = this.repositoryItemHyperLinkEditSelfBillingInvoice;
            this.colSelfBillingInvoiceID1.FieldName = "SelfBillingInvoice";
            this.colSelfBillingInvoiceID1.Name = "colSelfBillingInvoiceID1";
            this.colSelfBillingInvoiceID1.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceID1.Visible = true;
            this.colSelfBillingInvoiceID1.VisibleIndex = 64;
            this.colSelfBillingInvoiceID1.Width = 118;
            // 
            // colClientInvoiceID1
            // 
            this.colClientInvoiceID1.Caption = "Client Invoice ID";
            this.colClientInvoiceID1.ColumnEdit = this.repositoryItemHyperLinkEditClientInvoiceID;
            this.colClientInvoiceID1.FieldName = "ClientInvoiceID";
            this.colClientInvoiceID1.Name = "colClientInvoiceID1";
            this.colClientInvoiceID1.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceID1.Visible = true;
            this.colClientInvoiceID1.VisibleIndex = 66;
            this.colClientInvoiceID1.Width = 98;
            // 
            // colCMName
            // 
            this.colCMName.Caption = "Contract Manager";
            this.colCMName.FieldName = "CMName";
            this.colCMName.Name = "colCMName";
            this.colCMName.OptionsColumn.AllowEdit = false;
            this.colCMName.OptionsColumn.AllowFocus = false;
            this.colCMName.OptionsColumn.ReadOnly = true;
            this.colCMName.Visible = true;
            this.colCMName.VisibleIndex = 24;
            this.colCMName.Width = 106;
            // 
            // colDaysAllowed
            // 
            this.colDaysAllowed.Caption = "Days Allowed";
            this.colDaysAllowed.FieldName = "DaysAllowed";
            this.colDaysAllowed.Name = "colDaysAllowed";
            this.colDaysAllowed.OptionsColumn.AllowEdit = false;
            this.colDaysAllowed.OptionsColumn.AllowFocus = false;
            this.colDaysAllowed.OptionsColumn.ReadOnly = true;
            this.colDaysAllowed.Visible = true;
            this.colDaysAllowed.VisibleIndex = 6;
            this.colDaysAllowed.Width = 83;
            // 
            // colManagerName
            // 
            this.colManagerName.Caption = "Signed By Manager";
            this.colManagerName.FieldName = "ManagerName";
            this.colManagerName.Name = "colManagerName";
            this.colManagerName.OptionsColumn.AllowEdit = false;
            this.colManagerName.OptionsColumn.AllowFocus = false;
            this.colManagerName.OptionsColumn.ReadOnly = true;
            this.colManagerName.Visible = true;
            this.colManagerName.VisibleIndex = 51;
            this.colManagerName.Width = 111;
            // 
            // colManagerNotes
            // 
            this.colManagerNotes.Caption = "Signed By Notes";
            this.colManagerNotes.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colManagerNotes.FieldName = "ManagerNotes";
            this.colManagerNotes.Name = "colManagerNotes";
            this.colManagerNotes.OptionsColumn.ReadOnly = true;
            this.colManagerNotes.Visible = true;
            this.colManagerNotes.VisibleIndex = 52;
            this.colManagerNotes.Width = 97;
            // 
            // colNoOnetoSign
            // 
            this.colNoOnetoSign.Caption = "No One To Sign ";
            this.colNoOnetoSign.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNoOnetoSign.FieldName = "NoOnetoSign";
            this.colNoOnetoSign.Name = "colNoOnetoSign";
            this.colNoOnetoSign.OptionsColumn.AllowEdit = false;
            this.colNoOnetoSign.OptionsColumn.AllowFocus = false;
            this.colNoOnetoSign.OptionsColumn.ReadOnly = true;
            this.colNoOnetoSign.Visible = true;
            this.colNoOnetoSign.VisibleIndex = 53;
            this.colNoOnetoSign.Width = 96;
            // 
            // colSiteCategory
            // 
            this.colSiteCategory.Caption = "Site Category";
            this.colSiteCategory.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colSiteCategory.FieldName = "SiteCategory";
            this.colSiteCategory.Name = "colSiteCategory";
            this.colSiteCategory.OptionsColumn.ReadOnly = true;
            this.colSiteCategory.Visible = true;
            this.colSiteCategory.VisibleIndex = 36;
            this.colSiteCategory.Width = 124;
            // 
            // colExternalCustomerStatus
            // 
            this.colExternalCustomerStatus.Caption = "External Customer Status";
            this.colExternalCustomerStatus.FieldName = "ExternalCustomerStatus";
            this.colExternalCustomerStatus.Name = "colExternalCustomerStatus";
            this.colExternalCustomerStatus.OptionsColumn.AllowEdit = false;
            this.colExternalCustomerStatus.OptionsColumn.AllowFocus = false;
            this.colExternalCustomerStatus.OptionsColumn.ReadOnly = true;
            this.colExternalCustomerStatus.Visible = true;
            this.colExternalCustomerStatus.VisibleIndex = 67;
            this.colExternalCustomerStatus.Width = 142;
            // 
            // colCompletionSheetNumber
            // 
            this.colCompletionSheetNumber.Caption = "Completion Sheet #";
            this.colCompletionSheetNumber.FieldName = "CompletionSheetNumber";
            this.colCompletionSheetNumber.Name = "colCompletionSheetNumber";
            this.colCompletionSheetNumber.OptionsColumn.AllowEdit = false;
            this.colCompletionSheetNumber.OptionsColumn.AllowFocus = false;
            this.colCompletionSheetNumber.OptionsColumn.ReadOnly = true;
            this.colCompletionSheetNumber.Visible = true;
            this.colCompletionSheetNumber.VisibleIndex = 42;
            this.colCompletionSheetNumber.Width = 114;
            // 
            // colStatusIssueID
            // 
            this.colStatusIssueID.Caption = "Status Issue ID";
            this.colStatusIssueID.FieldName = "StatusIssueID";
            this.colStatusIssueID.Name = "colStatusIssueID";
            this.colStatusIssueID.OptionsColumn.AllowEdit = false;
            this.colStatusIssueID.OptionsColumn.AllowFocus = false;
            this.colStatusIssueID.OptionsColumn.ReadOnly = true;
            this.colStatusIssueID.Width = 93;
            // 
            // colStatusIssue
            // 
            this.colStatusIssue.Caption = "Status Issue";
            this.colStatusIssue.FieldName = "StatusIssue";
            this.colStatusIssue.Name = "colStatusIssue";
            this.colStatusIssue.OptionsColumn.AllowEdit = false;
            this.colStatusIssue.OptionsColumn.AllowFocus = false;
            this.colStatusIssue.OptionsColumn.ReadOnly = true;
            this.colStatusIssue.Visible = true;
            this.colStatusIssue.VisibleIndex = 23;
            this.colStatusIssue.Width = 110;
            // 
            // colDaysSeparation
            // 
            this.colDaysSeparation.Caption = "Days Separation";
            this.colDaysSeparation.FieldName = "DaysSeparation";
            this.colDaysSeparation.Name = "colDaysSeparation";
            this.colDaysSeparation.OptionsColumn.AllowEdit = false;
            this.colDaysSeparation.OptionsColumn.AllowFocus = false;
            this.colDaysSeparation.OptionsColumn.ReadOnly = true;
            this.colDaysSeparation.Visible = true;
            this.colDaysSeparation.VisibleIndex = 7;
            this.colDaysSeparation.Width = 98;
            // 
            // colSiteContractDaysSeparationPercent
            // 
            this.colSiteContractDaysSeparationPercent.Caption = "Site Contract Days Separation %";
            this.colSiteContractDaysSeparationPercent.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colSiteContractDaysSeparationPercent.FieldName = "SiteContractDaysSeparationPercent";
            this.colSiteContractDaysSeparationPercent.Name = "colSiteContractDaysSeparationPercent";
            this.colSiteContractDaysSeparationPercent.OptionsColumn.AllowEdit = false;
            this.colSiteContractDaysSeparationPercent.OptionsColumn.AllowFocus = false;
            this.colSiteContractDaysSeparationPercent.OptionsColumn.ReadOnly = true;
            this.colSiteContractDaysSeparationPercent.Width = 178;
            // 
            // colLabourName1
            // 
            this.colLabourName1.Caption = "Labour Name";
            this.colLabourName1.FieldName = "LabourName";
            this.colLabourName1.Name = "colLabourName1";
            this.colLabourName1.OptionsColumn.AllowEdit = false;
            this.colLabourName1.OptionsColumn.AllowFocus = false;
            this.colLabourName1.OptionsColumn.ReadOnly = true;
            this.colLabourName1.Visible = true;
            this.colLabourName1.VisibleIndex = 4;
            this.colLabourName1.Width = 203;
            // 
            // colLinkedToPersonType
            // 
            this.colLinkedToPersonType.Caption = "Labour Type";
            this.colLinkedToPersonType.FieldName = "LinkedToPersonType";
            this.colLinkedToPersonType.Name = "colLinkedToPersonType";
            this.colLinkedToPersonType.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonType.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonType.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonType.Visible = true;
            this.colLinkedToPersonType.VisibleIndex = 5;
            this.colLinkedToPersonType.Width = 79;
            // 
            // colLinkedToPersonTypeID
            // 
            this.colLinkedToPersonTypeID.Caption = "Labour Type ID";
            this.colLinkedToPersonTypeID.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.Name = "colLinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID.Width = 93;
            // 
            // colDoNotInvoiceClient1
            // 
            this.colDoNotInvoiceClient1.Caption = "Do Not Invoice Client";
            this.colDoNotInvoiceClient1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotInvoiceClient1.FieldName = "DoNotInvoiceClient";
            this.colDoNotInvoiceClient1.Name = "colDoNotInvoiceClient1";
            this.colDoNotInvoiceClient1.OptionsColumn.AllowEdit = false;
            this.colDoNotInvoiceClient1.OptionsColumn.AllowFocus = false;
            this.colDoNotInvoiceClient1.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClient1.Visible = true;
            this.colDoNotInvoiceClient1.VisibleIndex = 65;
            this.colDoNotInvoiceClient1.Width = 120;
            // 
            // colDoNotPayContractor1
            // 
            this.colDoNotPayContractor1.Caption = "Do Not Pay Team";
            this.colDoNotPayContractor1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDoNotPayContractor1.FieldName = "DoNotPayContractor";
            this.colDoNotPayContractor1.Name = "colDoNotPayContractor1";
            this.colDoNotPayContractor1.OptionsColumn.AllowEdit = false;
            this.colDoNotPayContractor1.OptionsColumn.AllowFocus = false;
            this.colDoNotPayContractor1.OptionsColumn.ReadOnly = true;
            this.colDoNotPayContractor1.Visible = true;
            this.colDoNotPayContractor1.VisibleIndex = 63;
            this.colDoNotPayContractor1.Width = 102;
            // 
            // colSelfBillingAmount
            // 
            this.colSelfBillingAmount.Caption = "Self-Billing Amount";
            this.colSelfBillingAmount.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSelfBillingAmount.FieldName = "SelfBillingAmount";
            this.colSelfBillingAmount.Name = "colSelfBillingAmount";
            this.colSelfBillingAmount.OptionsColumn.AllowEdit = false;
            this.colSelfBillingAmount.OptionsColumn.AllowFocus = false;
            this.colSelfBillingAmount.OptionsColumn.ReadOnly = true;
            this.colSelfBillingAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SelfBillingAmount", "{0:c}")});
            this.colSelfBillingAmount.Visible = true;
            this.colSelfBillingAmount.VisibleIndex = 30;
            this.colSelfBillingAmount.Width = 107;
            // 
            // colVisitLabourCalculatedID
            // 
            this.colVisitLabourCalculatedID.Caption = "Visit Labour Calculated ID";
            this.colVisitLabourCalculatedID.FieldName = "VisitLabourCalculatedID";
            this.colVisitLabourCalculatedID.Name = "colVisitLabourCalculatedID";
            this.colVisitLabourCalculatedID.OptionsColumn.AllowEdit = false;
            this.colVisitLabourCalculatedID.OptionsColumn.AllowFocus = false;
            this.colVisitLabourCalculatedID.OptionsColumn.ReadOnly = true;
            this.colVisitLabourCalculatedID.Width = 141;
            // 
            // colDuration
            // 
            this.colDuration.Caption = "Duration (Mins)";
            this.colDuration.FieldName = "Duration";
            this.colDuration.Name = "colDuration";
            this.colDuration.OptionsColumn.AllowEdit = false;
            this.colDuration.OptionsColumn.AllowFocus = false;
            this.colDuration.OptionsColumn.ReadOnly = true;
            this.colDuration.Visible = true;
            this.colDuration.VisibleIndex = 10;
            this.colDuration.Width = 95;
            // 
            // toolTipController1
            // 
            this.toolTipController1.CloseOnClick = DevExpress.Utils.DefaultBoolean.True;
            this.toolTipController1.InitialDelay = 2000;
            this.toolTipController1.ToolTipType = DevExpress.Utils.ToolTipType.SuperTip;
            this.toolTipController1.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.toolTipController1_GetActiveObjectInfo);
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlDateRange);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlInvoicingParameters);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControlLabour);
            this.splitContainerControl1.Panel1.Text = "Jobs";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl4);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1329, 384);
            this.splitContainerControl1.SplitterPosition = 483;
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.SplitGroupPanelCollapsed += new DevExpress.XtraEditors.SplitGroupPanelCollapsedEventHandler(this.splitContainerControl1_SplitGroupPanelCollapsed);
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeOK);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(74, 60);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(205, 107);
            this.popupContainerControlDateRange.TabIndex = 17;
            // 
            // groupControl1
            // 
            this.groupControl1.AllowHtmlText = true;
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFromDate);
            this.groupControl1.Controls.Add(this.dateEditToDate);
            this.groupControl1.Location = new System.Drawing.Point(3, 5);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(199, 76);
            this.groupControl1.TabIndex = 20;
            this.groupControl1.Text = "Date <b>Range</b>  [Actual End Date]";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(154, 20);
            this.dateEditFromDate.TabIndex = 10;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(154, 20);
            this.dateEditToDate.TabIndex = 11;
            // 
            // btnDateRangeOK
            // 
            this.btnDateRangeOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeOK.Location = new System.Drawing.Point(3, 84);
            this.btnDateRangeOK.Name = "btnDateRangeOK";
            this.btnDateRangeOK.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDateRangeOK.Size = new System.Drawing.Size(50, 20);
            this.btnDateRangeOK.TabIndex = 19;
            this.btnDateRangeOK.Text = "OK";
            this.btnDateRangeOK.Click += new System.EventHandler(this.btnDateRangeOK_Click);
            // 
            // popupContainerControlInvoicingParameters
            // 
            this.popupContainerControlInvoicingParameters.Controls.Add(this.groupControl2);
            this.popupContainerControlInvoicingParameters.Controls.Add(this.simpleButtonInvoicingParameters);
            this.popupContainerControlInvoicingParameters.Location = new System.Drawing.Point(1, 173);
            this.popupContainerControlInvoicingParameters.Name = "popupContainerControlInvoicingParameters";
            this.popupContainerControlInvoicingParameters.Size = new System.Drawing.Size(278, 181);
            this.popupContainerControlInvoicingParameters.TabIndex = 18;
            // 
            // groupControl2
            // 
            this.groupControl2.AllowHtmlText = true;
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.checkEditAutoSendCreatedInvoices);
            this.groupControl2.Controls.Add(this.checkEditExcludeVisitWithRequirements);
            this.groupControl2.Controls.Add(this.checkEditExcludeVisitsWithWarnings);
            this.groupControl2.Controls.Add(this.checkEditExcludeTeamsWithOutstandingRequirements);
            this.groupControl2.Controls.Add(this.checkEditExcludeTeamsWithOutstandingWarnings);
            this.groupControl2.Location = new System.Drawing.Point(3, 5);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(272, 150);
            this.groupControl2.TabIndex = 20;
            this.groupControl2.Text = "Invoicing Parameters";
            // 
            // checkEditAutoSendCreatedInvoices
            // 
            this.checkEditAutoSendCreatedInvoices.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditAutoSendCreatedInvoices.EditValue = 1;
            this.checkEditAutoSendCreatedInvoices.Location = new System.Drawing.Point(5, 26);
            this.checkEditAutoSendCreatedInvoices.MenuManager = this.barManager1;
            this.checkEditAutoSendCreatedInvoices.Name = "checkEditAutoSendCreatedInvoices";
            this.checkEditAutoSendCreatedInvoices.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditAutoSendCreatedInvoices.Properties.Caption = "<b>Auto-Send</b> Created Invoices";
            this.checkEditAutoSendCreatedInvoices.Properties.ValueChecked = 0;
            this.checkEditAutoSendCreatedInvoices.Properties.ValueUnchecked = 1;
            this.checkEditAutoSendCreatedInvoices.Size = new System.Drawing.Size(262, 19);
            this.checkEditAutoSendCreatedInvoices.TabIndex = 4;
            // 
            // checkEditExcludeVisitWithRequirements
            // 
            this.checkEditExcludeVisitWithRequirements.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditExcludeVisitWithRequirements.EditValue = 0;
            this.checkEditExcludeVisitWithRequirements.Location = new System.Drawing.Point(5, 101);
            this.checkEditExcludeVisitWithRequirements.MenuManager = this.barManager1;
            this.checkEditExcludeVisitWithRequirements.Name = "checkEditExcludeVisitWithRequirements";
            this.checkEditExcludeVisitWithRequirements.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditExcludeVisitWithRequirements.Properties.Caption = "Exclude Visits With Outstanding <b>Requirements</b>";
            this.checkEditExcludeVisitWithRequirements.Properties.ValueChecked = 0;
            this.checkEditExcludeVisitWithRequirements.Properties.ValueUnchecked = 1;
            this.checkEditExcludeVisitWithRequirements.Size = new System.Drawing.Size(262, 19);
            this.checkEditExcludeVisitWithRequirements.TabIndex = 3;
            // 
            // checkEditExcludeVisitsWithWarnings
            // 
            this.checkEditExcludeVisitsWithWarnings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditExcludeVisitsWithWarnings.EditValue = 0;
            this.checkEditExcludeVisitsWithWarnings.Location = new System.Drawing.Point(5, 126);
            this.checkEditExcludeVisitsWithWarnings.MenuManager = this.barManager1;
            this.checkEditExcludeVisitsWithWarnings.Name = "checkEditExcludeVisitsWithWarnings";
            this.checkEditExcludeVisitsWithWarnings.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditExcludeVisitsWithWarnings.Properties.Caption = "Exclude Visits with Outstanding <b>Warnings</b>";
            this.checkEditExcludeVisitsWithWarnings.Properties.ValueChecked = 0;
            this.checkEditExcludeVisitsWithWarnings.Properties.ValueUnchecked = 1;
            this.checkEditExcludeVisitsWithWarnings.Size = new System.Drawing.Size(262, 19);
            this.checkEditExcludeVisitsWithWarnings.TabIndex = 2;
            // 
            // checkEditExcludeTeamsWithOutstandingRequirements
            // 
            this.checkEditExcludeTeamsWithOutstandingRequirements.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditExcludeTeamsWithOutstandingRequirements.EditValue = 1;
            this.checkEditExcludeTeamsWithOutstandingRequirements.Location = new System.Drawing.Point(5, 51);
            this.checkEditExcludeTeamsWithOutstandingRequirements.MenuManager = this.barManager1;
            this.checkEditExcludeTeamsWithOutstandingRequirements.Name = "checkEditExcludeTeamsWithOutstandingRequirements";
            this.checkEditExcludeTeamsWithOutstandingRequirements.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditExcludeTeamsWithOutstandingRequirements.Properties.Caption = "Exclude Teams with Outstanding <b>Requirements</b>";
            this.checkEditExcludeTeamsWithOutstandingRequirements.Properties.ValueChecked = 1;
            this.checkEditExcludeTeamsWithOutstandingRequirements.Properties.ValueUnchecked = 0;
            this.checkEditExcludeTeamsWithOutstandingRequirements.Size = new System.Drawing.Size(262, 19);
            this.checkEditExcludeTeamsWithOutstandingRequirements.TabIndex = 1;
            // 
            // checkEditExcludeTeamsWithOutstandingWarnings
            // 
            this.checkEditExcludeTeamsWithOutstandingWarnings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditExcludeTeamsWithOutstandingWarnings.EditValue = 0;
            this.checkEditExcludeTeamsWithOutstandingWarnings.Location = new System.Drawing.Point(5, 76);
            this.checkEditExcludeTeamsWithOutstandingWarnings.MenuManager = this.barManager1;
            this.checkEditExcludeTeamsWithOutstandingWarnings.Name = "checkEditExcludeTeamsWithOutstandingWarnings";
            this.checkEditExcludeTeamsWithOutstandingWarnings.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditExcludeTeamsWithOutstandingWarnings.Properties.Caption = "Exclude Teams with Outstanding <b>Warnings</b>";
            this.checkEditExcludeTeamsWithOutstandingWarnings.Properties.ValueChecked = 0;
            this.checkEditExcludeTeamsWithOutstandingWarnings.Properties.ValueUnchecked = 1;
            this.checkEditExcludeTeamsWithOutstandingWarnings.Size = new System.Drawing.Size(262, 19);
            this.checkEditExcludeTeamsWithOutstandingWarnings.TabIndex = 0;
            // 
            // simpleButtonInvoicingParameters
            // 
            this.simpleButtonInvoicingParameters.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonInvoicingParameters.Location = new System.Drawing.Point(3, 158);
            this.simpleButtonInvoicingParameters.Name = "simpleButtonInvoicingParameters";
            this.simpleButtonInvoicingParameters.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.simpleButtonInvoicingParameters.Size = new System.Drawing.Size(50, 20);
            this.simpleButtonInvoicingParameters.TabIndex = 19;
            this.simpleButtonInvoicingParameters.Text = "OK";
            this.simpleButtonInvoicingParameters.Click += new System.EventHandler(this.simpleButtonInvoicingParameters_Click);
            // 
            // gridControlLabour
            // 
            this.gridControlLabour.DataSource = this.sp06452OMBillingManagerLabourBindingSource;
            this.gridControlLabour.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 13, true, true, "Reload Data", "reload")});
            this.gridControlLabour.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlLabour_EmbeddedNavigator_ButtonClick);
            this.gridControlLabour.Location = new System.Drawing.Point(0, 0);
            this.gridControlLabour.MainView = this.gridViewLabour;
            this.gridControlLabour.MenuManager = this.barManager1;
            this.gridControlLabour.Name = "gridControlLabour";
            this.gridControlLabour.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit5,
            this.repositoryItemMemoExEdit5});
            this.gridControlLabour.Size = new System.Drawing.Size(479, 380);
            this.gridControlLabour.TabIndex = 5;
            this.gridControlLabour.ToolTipController = this.toolTipController1;
            this.gridControlLabour.UseEmbeddedNavigator = true;
            this.gridControlLabour.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLabour});
            // 
            // sp06452OMBillingManagerLabourBindingSource
            // 
            this.sp06452OMBillingManagerLabourBindingSource.DataMember = "sp06452_OM_Billing_Manager_Labour";
            this.sp06452OMBillingManagerLabourBindingSource.DataSource = this.dataSet_OM_Billing;
            // 
            // gridViewLabour
            // 
            this.gridViewLabour.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewLabour.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewLabour.ColumnPanelRowHeight = 34;
            this.gridViewLabour.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLabourTypeID,
            this.colLabourType,
            this.colLabourID,
            this.colLabourName,
            this.colTelephone,
            this.colEmail,
            this.colRequirementCount,
            this.colRequirementFulfilledCount,
            this.colRequirementUnfulfilledCount,
            this.colWarningCount,
            this.colWarningAuthorisedCount,
            this.colWarningUnauthorisedCount,
            this.colSelected1});
            gridFormatRule8.Column = this.colRequirementUnfulfilledCount;
            gridFormatRule8.Name = "FormatBillingRequirementUnfulfilledCount1";
            formatConditionRuleValue8.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue8.PredefinedName = "Green Fill, Green Text";
            formatConditionRuleValue8.Value1 = 0;
            gridFormatRule8.Rule = formatConditionRuleValue8;
            gridFormatRule9.Column = this.colRequirementUnfulfilledCount;
            gridFormatRule9.Name = "FormatBillingRequirementUnfulfilledCount2";
            formatConditionRuleValue9.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue9.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue9.Value1 = 0;
            gridFormatRule9.Rule = formatConditionRuleValue9;
            gridFormatRule10.Column = this.colWarningUnauthorisedCount;
            gridFormatRule10.Name = "FormatBillingWarningUnauthorisedCount1";
            formatConditionRuleValue10.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue10.PredefinedName = "Green Fill, Green Text";
            formatConditionRuleValue10.Value1 = 0;
            gridFormatRule10.Rule = formatConditionRuleValue10;
            gridFormatRule11.Column = this.colWarningUnauthorisedCount;
            gridFormatRule11.Name = "FormatBillingWarningUnauthorisedCount2";
            formatConditionRuleValue11.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue11.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue11.Value1 = 0;
            gridFormatRule11.Rule = formatConditionRuleValue11;
            this.gridViewLabour.FormatRules.Add(gridFormatRule8);
            this.gridViewLabour.FormatRules.Add(gridFormatRule9);
            this.gridViewLabour.FormatRules.Add(gridFormatRule10);
            this.gridViewLabour.FormatRules.Add(gridFormatRule11);
            this.gridViewLabour.GridControl = this.gridControlLabour;
            this.gridViewLabour.GroupCount = 1;
            this.gridViewLabour.Name = "gridViewLabour";
            this.gridViewLabour.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewLabour.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewLabour.OptionsFind.AlwaysVisible = true;
            this.gridViewLabour.OptionsFind.FindDelay = 2000;
            this.gridViewLabour.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewLabour.OptionsLayout.StoreAppearance = true;
            this.gridViewLabour.OptionsLayout.StoreFormatRules = true;
            this.gridViewLabour.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewLabour.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewLabour.OptionsSelection.MultiSelect = true;
            this.gridViewLabour.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewLabour.OptionsView.ColumnAutoWidth = false;
            this.gridViewLabour.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewLabour.OptionsView.ShowGroupPanel = false;
            this.gridViewLabour.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLabourType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLabourName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewLabour.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewLabour_CustomDrawCell);
            this.gridViewLabour.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewLabour.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewLabour.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewLabour.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewLabour_ShowingEditor);
            this.gridViewLabour.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewLabour.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewLabour.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewLabour.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewLabour_MouseUp);
            this.gridViewLabour.DoubleClick += new System.EventHandler(this.gridViewLabour_DoubleClick);
            this.gridViewLabour.GotFocus += new System.EventHandler(this.gridViewLabour_GotFocus);
            // 
            // colLabourTypeID
            // 
            this.colLabourTypeID.Caption = "Labour Type ID";
            this.colLabourTypeID.FieldName = "LabourTypeID";
            this.colLabourTypeID.Name = "colLabourTypeID";
            this.colLabourTypeID.OptionsColumn.AllowEdit = false;
            this.colLabourTypeID.OptionsColumn.AllowFocus = false;
            this.colLabourTypeID.OptionsColumn.ReadOnly = true;
            this.colLabourTypeID.Width = 93;
            // 
            // colLabourType
            // 
            this.colLabourType.Caption = "Labour Type";
            this.colLabourType.FieldName = "LabourType";
            this.colLabourType.Name = "colLabourType";
            this.colLabourType.OptionsColumn.AllowEdit = false;
            this.colLabourType.OptionsColumn.AllowFocus = false;
            this.colLabourType.OptionsColumn.ReadOnly = true;
            this.colLabourType.Visible = true;
            this.colLabourType.VisibleIndex = 0;
            this.colLabourType.Width = 204;
            // 
            // colLabourID
            // 
            this.colLabourID.Caption = "Labour ID";
            this.colLabourID.FieldName = "LabourID";
            this.colLabourID.Name = "colLabourID";
            this.colLabourID.OptionsColumn.AllowEdit = false;
            this.colLabourID.OptionsColumn.AllowFocus = false;
            this.colLabourID.OptionsColumn.ReadOnly = true;
            // 
            // colLabourName
            // 
            this.colLabourName.Caption = "Labour Name";
            this.colLabourName.FieldName = "LabourName";
            this.colLabourName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colLabourName.Name = "colLabourName";
            this.colLabourName.OptionsColumn.AllowEdit = false;
            this.colLabourName.OptionsColumn.AllowFocus = false;
            this.colLabourName.OptionsColumn.ReadOnly = true;
            this.colLabourName.Visible = true;
            this.colLabourName.VisibleIndex = 0;
            this.colLabourName.Width = 263;
            // 
            // colTelephone
            // 
            this.colTelephone.Caption = "Telephone";
            this.colTelephone.FieldName = "Telephone";
            this.colTelephone.Name = "colTelephone";
            this.colTelephone.OptionsColumn.AllowEdit = false;
            this.colTelephone.OptionsColumn.AllowFocus = false;
            this.colTelephone.OptionsColumn.ReadOnly = true;
            this.colTelephone.Visible = true;
            this.colTelephone.VisibleIndex = 7;
            this.colTelephone.Width = 90;
            // 
            // colEmail
            // 
            this.colEmail.Caption = "Email";
            this.colEmail.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colEmail.FieldName = "Email";
            this.colEmail.Name = "colEmail";
            this.colEmail.OptionsColumn.ReadOnly = true;
            this.colEmail.Visible = true;
            this.colEmail.VisibleIndex = 8;
            this.colEmail.Width = 173;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colRequirementCount
            // 
            this.colRequirementCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colRequirementCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colRequirementCount.Caption = "Requirements Count";
            this.colRequirementCount.FieldName = "RequirementCount";
            this.colRequirementCount.Name = "colRequirementCount";
            this.colRequirementCount.OptionsColumn.AllowEdit = false;
            this.colRequirementCount.OptionsColumn.AllowFocus = false;
            this.colRequirementCount.OptionsColumn.ReadOnly = true;
            this.colRequirementCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "RequirementCount", "{0:0}")});
            this.colRequirementCount.Visible = true;
            this.colRequirementCount.VisibleIndex = 3;
            this.colRequirementCount.Width = 84;
            // 
            // colRequirementFulfilledCount
            // 
            this.colRequirementFulfilledCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colRequirementFulfilledCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colRequirementFulfilledCount.Caption = "Requirements Fulfilled";
            this.colRequirementFulfilledCount.FieldName = "RequirementFulfilledCount";
            this.colRequirementFulfilledCount.Name = "colRequirementFulfilledCount";
            this.colRequirementFulfilledCount.OptionsColumn.AllowEdit = false;
            this.colRequirementFulfilledCount.OptionsColumn.AllowFocus = false;
            this.colRequirementFulfilledCount.OptionsColumn.ReadOnly = true;
            this.colRequirementFulfilledCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "RequirementFulfilledCount", "{0:0}")});
            this.colRequirementFulfilledCount.Visible = true;
            this.colRequirementFulfilledCount.VisibleIndex = 2;
            this.colRequirementFulfilledCount.Width = 84;
            // 
            // colWarningCount
            // 
            this.colWarningCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colWarningCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWarningCount.Caption = "Warnings Count";
            this.colWarningCount.FieldName = "WarningCount";
            this.colWarningCount.Name = "colWarningCount";
            this.colWarningCount.OptionsColumn.AllowEdit = false;
            this.colWarningCount.OptionsColumn.AllowFocus = false;
            this.colWarningCount.OptionsColumn.ReadOnly = true;
            this.colWarningCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "WarningCount", "{0:0}")});
            this.colWarningCount.Visible = true;
            this.colWarningCount.VisibleIndex = 6;
            this.colWarningCount.Width = 67;
            // 
            // colWarningAuthorisedCount
            // 
            this.colWarningAuthorisedCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colWarningAuthorisedCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWarningAuthorisedCount.Caption = "Warnings Authorised";
            this.colWarningAuthorisedCount.FieldName = "WarningAuthorisedCount";
            this.colWarningAuthorisedCount.Name = "colWarningAuthorisedCount";
            this.colWarningAuthorisedCount.OptionsColumn.AllowEdit = false;
            this.colWarningAuthorisedCount.OptionsColumn.AllowFocus = false;
            this.colWarningAuthorisedCount.OptionsColumn.ReadOnly = true;
            this.colWarningAuthorisedCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "WarningAuthorisedCount", "{0:0}")});
            this.colWarningAuthorisedCount.Visible = true;
            this.colWarningAuthorisedCount.VisibleIndex = 5;
            this.colWarningAuthorisedCount.Width = 72;
            // 
            // colSelected1
            // 
            this.colSelected1.Caption = "Selected";
            this.colSelected1.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colSelected1.FieldName = "Selected";
            this.colSelected1.Name = "colSelected1";
            this.colSelected1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Selected", "{0:0}")});
            this.colSelected1.Width = 60;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.ValueChecked = 1;
            this.repositoryItemCheckEdit5.ValueUnchecked = 0;
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl4.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl4.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl4.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl4.Panel1.Controls.Add(this.gridControlBillingRequirement);
            this.splitContainerControl4.Panel1.ShowCaption = true;
            this.splitContainerControl4.Panel1.Text = "Visit Billing Requirements";
            this.splitContainerControl4.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl4.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl4.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl4.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl4.Panel2.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl4.Panel2.ShowCaption = true;
            this.splitContainerControl4.Panel2.Text = "Visit Billing Warnings";
            this.splitContainerControl4.Size = new System.Drawing.Size(840, 384);
            this.splitContainerControl4.SplitterPosition = 280;
            this.splitContainerControl4.TabIndex = 6;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // gridControlBillingRequirement
            // 
            this.gridControlBillingRequirement.DataSource = this.sp06446OMTeamSelfBillingBillingRequirementsBindingSource;
            this.gridControlBillingRequirement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 20, true, true, "Block Edit Records", "blockedit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 13, true, true, "Reload Data", "reload"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 9, true, true, "Add from Template", "add_from_template"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Template Manager", "template_manager"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 18, true, true, "Add from parent Site Contract", "add_from_parent"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 25, true, true, "View Parent Visit", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 26, true, true, "Drill Up to parent Site Contract", "drill_site_contract")});
            this.gridControlBillingRequirement.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlBillingRequirement_EmbeddedNavigator_ButtonClick);
            this.gridControlBillingRequirement.Location = new System.Drawing.Point(0, 0);
            this.gridControlBillingRequirement.MainView = this.gridViewBillingRequirement;
            this.gridControlBillingRequirement.MenuManager = this.barManager1;
            this.gridControlBillingRequirement.Name = "gridControlBillingRequirement";
            this.gridControlBillingRequirement.Size = new System.Drawing.Size(255, 381);
            this.gridControlBillingRequirement.TabIndex = 8;
            this.gridControlBillingRequirement.UseEmbeddedNavigator = true;
            this.gridControlBillingRequirement.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBillingRequirement});
            // 
            // sp06446OMTeamSelfBillingBillingRequirementsBindingSource
            // 
            this.sp06446OMTeamSelfBillingBillingRequirementsBindingSource.DataMember = "sp06446_OM_Team_Self_Billing_Billing_Requirements";
            this.sp06446OMTeamSelfBillingBillingRequirementsBindingSource.DataSource = this.dataSet_OM_Billing;
            // 
            // gridViewBillingRequirement
            // 
            this.gridViewBillingRequirement.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBillingRequirementID,
            this.colRequirementID,
            this.gridColumn263,
            this.gridColumn264,
            this.colClientBillRequirement,
            this.colTeamSelfBillRequirement,
            this.colRemarks3,
            this.colRequirementFulfilled,
            this.colCheckedByStaffID,
            this.colBillingRequirement,
            this.colLinkedToRecord1,
            this.colLinkedToRecordType1,
            this.colCheckedByStaff});
            gridFormatRule12.Column = this.colRequirementFulfilled;
            gridFormatRule12.ColumnApplyTo = this.colRequirementFulfilled;
            gridFormatRule12.Name = "FormatRequirementFulfilled";
            formatConditionRuleValue12.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue12.PredefinedName = "Green Fill, Green Text";
            formatConditionRuleValue12.Value1 = 1;
            gridFormatRule12.Rule = formatConditionRuleValue12;
            gridFormatRule13.Column = this.colRequirementFulfilled;
            gridFormatRule13.ColumnApplyTo = this.colRequirementFulfilled;
            gridFormatRule13.Name = "FormatRequirementUnfulfilled";
            formatConditionRuleValue13.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue13.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue13.Value1 = 0;
            gridFormatRule13.Rule = formatConditionRuleValue13;
            this.gridViewBillingRequirement.FormatRules.Add(gridFormatRule12);
            this.gridViewBillingRequirement.FormatRules.Add(gridFormatRule13);
            this.gridViewBillingRequirement.GridControl = this.gridControlBillingRequirement;
            this.gridViewBillingRequirement.GroupCount = 1;
            this.gridViewBillingRequirement.Name = "gridViewBillingRequirement";
            this.gridViewBillingRequirement.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDownFocused;
            this.gridViewBillingRequirement.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewBillingRequirement.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewBillingRequirement.OptionsLayout.StoreAppearance = true;
            this.gridViewBillingRequirement.OptionsLayout.StoreFormatRules = true;
            this.gridViewBillingRequirement.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewBillingRequirement.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewBillingRequirement.OptionsSelection.MultiSelect = true;
            this.gridViewBillingRequirement.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewBillingRequirement.OptionsView.ColumnAutoWidth = false;
            this.gridViewBillingRequirement.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewBillingRequirement.OptionsView.ShowGroupPanel = false;
            this.gridViewBillingRequirement.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToRecord1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBillingRequirement, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewBillingRequirement.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewBillingRequirement.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewBillingRequirement.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewBillingRequirement.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewBillingRequirement.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewBillingRequirement.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewBillingRequirement.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridControlBillingRequirement_MouseUp);
            this.gridViewBillingRequirement.DoubleClick += new System.EventHandler(this.gridControlBillingRequirement_DoubleClick);
            this.gridViewBillingRequirement.GotFocus += new System.EventHandler(this.gridControlBillingRequirement_GotFocus);
            this.gridViewBillingRequirement.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridViewBillingRequirement_ValidatingEditor);
            // 
            // colBillingRequirementID
            // 
            this.colBillingRequirementID.Caption = "Billing Requirement ID";
            this.colBillingRequirementID.FieldName = "BillingRequirementID";
            this.colBillingRequirementID.Name = "colBillingRequirementID";
            this.colBillingRequirementID.OptionsColumn.AllowEdit = false;
            this.colBillingRequirementID.OptionsColumn.AllowFocus = false;
            this.colBillingRequirementID.OptionsColumn.ReadOnly = true;
            this.colBillingRequirementID.Width = 123;
            // 
            // colRequirementID
            // 
            this.colRequirementID.Caption = "Requirement ID";
            this.colRequirementID.FieldName = "RequirementID";
            this.colRequirementID.Name = "colRequirementID";
            this.colRequirementID.OptionsColumn.AllowEdit = false;
            this.colRequirementID.OptionsColumn.AllowFocus = false;
            this.colRequirementID.OptionsColumn.ReadOnly = true;
            this.colRequirementID.Width = 94;
            // 
            // gridColumn263
            // 
            this.gridColumn263.Caption = "Linked To Record ID";
            this.gridColumn263.FieldName = "LinkedToRecordID";
            this.gridColumn263.Name = "gridColumn263";
            this.gridColumn263.OptionsColumn.AllowEdit = false;
            this.gridColumn263.OptionsColumn.AllowFocus = false;
            this.gridColumn263.OptionsColumn.ReadOnly = true;
            this.gridColumn263.Width = 115;
            // 
            // gridColumn264
            // 
            this.gridColumn264.Caption = "Linked to Record Type ID";
            this.gridColumn264.FieldName = "LinkedToRecordTypeID";
            this.gridColumn264.Name = "gridColumn264";
            this.gridColumn264.OptionsColumn.AllowEdit = false;
            this.gridColumn264.OptionsColumn.AllowFocus = false;
            this.gridColumn264.OptionsColumn.ReadOnly = true;
            this.gridColumn264.Width = 140;
            // 
            // colClientBillRequirement
            // 
            this.colClientBillRequirement.Caption = "Client Bill Requirement";
            this.colClientBillRequirement.ColumnEdit = this.repositoryItemCheckEdit24;
            this.colClientBillRequirement.FieldName = "ClientBillRequirement";
            this.colClientBillRequirement.Name = "colClientBillRequirement";
            this.colClientBillRequirement.OptionsColumn.AllowEdit = false;
            this.colClientBillRequirement.OptionsColumn.AllowFocus = false;
            this.colClientBillRequirement.OptionsColumn.ReadOnly = true;
            this.colClientBillRequirement.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ClientBillRequirement", "{0:0}")});
            this.colClientBillRequirement.Visible = true;
            this.colClientBillRequirement.VisibleIndex = 5;
            this.colClientBillRequirement.Width = 125;
            // 
            // colTeamSelfBillRequirement
            // 
            this.colTeamSelfBillRequirement.Caption = "Team Self-Bill Requirement";
            this.colTeamSelfBillRequirement.ColumnEdit = this.repositoryItemCheckEdit24;
            this.colTeamSelfBillRequirement.FieldName = "TeamSelfBillRequirement";
            this.colTeamSelfBillRequirement.Name = "colTeamSelfBillRequirement";
            this.colTeamSelfBillRequirement.OptionsColumn.AllowEdit = false;
            this.colTeamSelfBillRequirement.OptionsColumn.AllowFocus = false;
            this.colTeamSelfBillRequirement.OptionsColumn.ReadOnly = true;
            this.colTeamSelfBillRequirement.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TeamSelfBillRequirement", "{0:0}")});
            this.colTeamSelfBillRequirement.Visible = true;
            this.colTeamSelfBillRequirement.VisibleIndex = 4;
            this.colTeamSelfBillRequirement.Width = 146;
            // 
            // colRemarks3
            // 
            this.colRemarks3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colRemarks3.AppearanceHeader.Options.UseFont = true;
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 2;
            this.colRemarks3.Width = 159;
            // 
            // colCheckedByStaffID
            // 
            this.colCheckedByStaffID.Caption = "Checked By Staff ID";
            this.colCheckedByStaffID.FieldName = "CheckedByStaffID";
            this.colCheckedByStaffID.Name = "colCheckedByStaffID";
            this.colCheckedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCheckedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCheckedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCheckedByStaffID.Width = 116;
            // 
            // colBillingRequirement
            // 
            this.colBillingRequirement.Caption = "Billing Requirement";
            this.colBillingRequirement.FieldName = "BillingRequirement";
            this.colBillingRequirement.Name = "colBillingRequirement";
            this.colBillingRequirement.OptionsColumn.AllowEdit = false;
            this.colBillingRequirement.OptionsColumn.AllowFocus = false;
            this.colBillingRequirement.OptionsColumn.ReadOnly = true;
            this.colBillingRequirement.Visible = true;
            this.colBillingRequirement.VisibleIndex = 0;
            this.colBillingRequirement.Width = 268;
            // 
            // colLinkedToRecord1
            // 
            this.colLinkedToRecord1.Caption = "Visit";
            this.colLinkedToRecord1.ColumnEdit = this.repositoryItemTextEditHTML2;
            this.colLinkedToRecord1.FieldName = "LinkedToRecord";
            this.colLinkedToRecord1.Name = "colLinkedToRecord1";
            this.colLinkedToRecord1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecord1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecord1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecord1.Visible = true;
            this.colLinkedToRecord1.VisibleIndex = 4;
            this.colLinkedToRecord1.Width = 469;
            // 
            // colLinkedToRecordType1
            // 
            this.colLinkedToRecordType1.Caption = "Linked To Record Type";
            this.colLinkedToRecordType1.FieldName = "LinkedToRecordType";
            this.colLinkedToRecordType1.Name = "colLinkedToRecordType1";
            this.colLinkedToRecordType1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordType1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordType1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordType1.Width = 142;
            // 
            // colCheckedByStaff
            // 
            this.colCheckedByStaff.Caption = "Checked By Staff";
            this.colCheckedByStaff.FieldName = "CheckedByStaff";
            this.colCheckedByStaff.Name = "colCheckedByStaff";
            this.colCheckedByStaff.OptionsColumn.AllowEdit = false;
            this.colCheckedByStaff.OptionsColumn.AllowFocus = false;
            this.colCheckedByStaff.OptionsColumn.ReadOnly = true;
            this.colCheckedByStaff.Visible = true;
            this.colCheckedByStaff.VisibleIndex = 3;
            this.colCheckedByStaff.Width = 140;
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.gridControlBillingException);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl3.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl3.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl3.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl3.Panel2.Controls.Add(this.gridControlJob);
            this.splitContainerControl3.Panel2.ShowCaption = true;
            this.splitContainerControl3.Panel2.Text = "Visit Jobs";
            this.splitContainerControl3.Size = new System.Drawing.Size(529, 381);
            this.splitContainerControl3.SplitterPosition = 250;
            this.splitContainerControl3.TabIndex = 10;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // gridControlBillingException
            // 
            this.gridControlBillingException.DataSource = this.sp06448OMTeamSelfBillingBillingExceptionsBindingSource;
            this.gridControlBillingException.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlBillingException.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlBillingException.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlBillingException.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlBillingException.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlBillingException.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlBillingException.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlBillingException.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlBillingException.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlBillingException.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlBillingException.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlBillingException.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlBillingException.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 20, true, true, "Block Edit Records", "blockedit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 13, true, true, "Reload Data", "reload"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 18, true, true, "Check and Re-create Warnings for Selected Visits", "recreate"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 25, true, true, "View Parent Visit", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 26, true, true, "Drill Up to parent Site Contract", "drill_site_contract")});
            this.gridControlBillingException.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlBillingException_EmbeddedNavigator_ButtonClick);
            this.gridControlBillingException.Location = new System.Drawing.Point(0, 0);
            this.gridControlBillingException.MainView = this.gridViewBillingException;
            this.gridControlBillingException.MenuManager = this.barManager1;
            this.gridControlBillingException.Name = "gridControlBillingException";
            this.gridControlBillingException.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemCheckEditAuthorised,
            this.repositoryItemTextEditHTML3});
            this.gridControlBillingException.Size = new System.Drawing.Size(250, 381);
            this.gridControlBillingException.TabIndex = 9;
            this.gridControlBillingException.UseEmbeddedNavigator = true;
            this.gridControlBillingException.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBillingException});
            // 
            // sp06448OMTeamSelfBillingBillingExceptionsBindingSource
            // 
            this.sp06448OMTeamSelfBillingBillingExceptionsBindingSource.DataMember = "sp06448_OM_Team_Self_Billing_Billing_Exceptions";
            this.sp06448OMTeamSelfBillingBillingExceptionsBindingSource.DataSource = this.dataSet_OM_Billing;
            // 
            // gridViewBillingException
            // 
            this.gridViewBillingException.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBillingWarningID,
            this.colWarningTypeID,
            this.colLinkedToRecordTypeID,
            this.colLinkedToRecordTypeID2,
            this.colRemarks4,
            this.colAuthorised,
            this.colAuthorisedByStaffID,
            this.colWarningType,
            this.colVisit2,
            this.colLinkedToRecordType2,
            this.colAuthorisedByStaff,
            this.colBillingTypeID});
            gridFormatRule14.Column = this.colAuthorised;
            gridFormatRule14.ColumnApplyTo = this.colAuthorised;
            gridFormatRule14.Name = "FormatWarningAuthorised";
            formatConditionRuleValue14.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue14.PredefinedName = "Green Fill, Green Text";
            formatConditionRuleValue14.Value1 = 1;
            gridFormatRule14.Rule = formatConditionRuleValue14;
            gridFormatRule15.Column = this.colAuthorised;
            gridFormatRule15.ColumnApplyTo = this.colAuthorised;
            gridFormatRule15.Name = "FormatWarningUnauthorised";
            formatConditionRuleValue15.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue15.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue15.Value1 = 0;
            gridFormatRule15.Rule = formatConditionRuleValue15;
            this.gridViewBillingException.FormatRules.Add(gridFormatRule14);
            this.gridViewBillingException.FormatRules.Add(gridFormatRule15);
            this.gridViewBillingException.GridControl = this.gridControlBillingException;
            this.gridViewBillingException.GroupCount = 1;
            this.gridViewBillingException.Name = "gridViewBillingException";
            this.gridViewBillingException.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDownFocused;
            this.gridViewBillingException.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewBillingException.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewBillingException.OptionsLayout.StoreAppearance = true;
            this.gridViewBillingException.OptionsLayout.StoreFormatRules = true;
            this.gridViewBillingException.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewBillingException.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewBillingException.OptionsSelection.MultiSelect = true;
            this.gridViewBillingException.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewBillingException.OptionsView.ColumnAutoWidth = false;
            this.gridViewBillingException.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewBillingException.OptionsView.ShowGroupPanel = false;
            this.gridViewBillingException.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colVisit2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colWarningType, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewBillingException.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewBillingException.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewBillingException.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewBillingException.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewBillingException.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewBillingException.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewBillingException.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridControlBillingException_MouseUp);
            this.gridViewBillingException.DoubleClick += new System.EventHandler(this.gridControlBillingException_DoubleClick);
            this.gridViewBillingException.GotFocus += new System.EventHandler(this.gridControlBillingException_GotFocus);
            this.gridViewBillingException.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridViewBillingException_ValidatingEditor);
            // 
            // colBillingWarningID
            // 
            this.colBillingWarningID.Caption = "Billing Warning ID";
            this.colBillingWarningID.FieldName = "BillingWarningID";
            this.colBillingWarningID.Name = "colBillingWarningID";
            this.colBillingWarningID.OptionsColumn.AllowEdit = false;
            this.colBillingWarningID.OptionsColumn.AllowFocus = false;
            this.colBillingWarningID.OptionsColumn.ReadOnly = true;
            this.colBillingWarningID.Width = 123;
            // 
            // colWarningTypeID
            // 
            this.colWarningTypeID.Caption = "Warning Type ID";
            this.colWarningTypeID.FieldName = "WarningTypeID";
            this.colWarningTypeID.Name = "colWarningTypeID";
            this.colWarningTypeID.OptionsColumn.AllowEdit = false;
            this.colWarningTypeID.OptionsColumn.AllowFocus = false;
            this.colWarningTypeID.OptionsColumn.ReadOnly = true;
            this.colWarningTypeID.Width = 94;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked To Record ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 115;
            // 
            // colLinkedToRecordTypeID2
            // 
            this.colLinkedToRecordTypeID2.Caption = "Linked to Record Type ID";
            this.colLinkedToRecordTypeID2.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID2.Name = "colLinkedToRecordTypeID2";
            this.colLinkedToRecordTypeID2.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID2.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID2.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID2.Width = 140;
            // 
            // colRemarks4
            // 
            this.colRemarks4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colRemarks4.AppearanceHeader.Options.UseFont = true;
            this.colRemarks4.Caption = "Remarks";
            this.colRemarks4.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks4.FieldName = "Remarks";
            this.colRemarks4.Name = "colRemarks4";
            this.colRemarks4.Visible = true;
            this.colRemarks4.VisibleIndex = 2;
            this.colRemarks4.Width = 159;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colAuthorisedByStaffID
            // 
            this.colAuthorisedByStaffID.Caption = "Authorised By Staff ID";
            this.colAuthorisedByStaffID.FieldName = "AuthorisedByStaffID";
            this.colAuthorisedByStaffID.Name = "colAuthorisedByStaffID";
            this.colAuthorisedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAuthorisedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAuthorisedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAuthorisedByStaffID.Width = 116;
            // 
            // colWarningType
            // 
            this.colWarningType.Caption = "Warning Type";
            this.colWarningType.FieldName = "WarningType";
            this.colWarningType.Name = "colWarningType";
            this.colWarningType.OptionsColumn.AllowEdit = false;
            this.colWarningType.OptionsColumn.AllowFocus = false;
            this.colWarningType.OptionsColumn.ReadOnly = true;
            this.colWarningType.Visible = true;
            this.colWarningType.VisibleIndex = 0;
            this.colWarningType.Width = 268;
            // 
            // colVisit2
            // 
            this.colVisit2.Caption = "Visit";
            this.colVisit2.ColumnEdit = this.repositoryItemTextEditHTML3;
            this.colVisit2.FieldName = "LinkedToRecord";
            this.colVisit2.Name = "colVisit2";
            this.colVisit2.OptionsColumn.AllowEdit = false;
            this.colVisit2.OptionsColumn.AllowFocus = false;
            this.colVisit2.OptionsColumn.ReadOnly = true;
            this.colVisit2.Visible = true;
            this.colVisit2.VisibleIndex = 4;
            this.colVisit2.Width = 469;
            // 
            // repositoryItemTextEditHTML3
            // 
            this.repositoryItemTextEditHTML3.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML3.AutoHeight = false;
            this.repositoryItemTextEditHTML3.Name = "repositoryItemTextEditHTML3";
            // 
            // colLinkedToRecordType2
            // 
            this.colLinkedToRecordType2.Caption = "Linked To Record Type";
            this.colLinkedToRecordType2.FieldName = "LinkedToRecordType";
            this.colLinkedToRecordType2.Name = "colLinkedToRecordType2";
            this.colLinkedToRecordType2.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordType2.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordType2.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordType2.Width = 142;
            // 
            // colAuthorisedByStaff
            // 
            this.colAuthorisedByStaff.Caption = "Authorised By Staff";
            this.colAuthorisedByStaff.FieldName = "AuthorisedByStaff";
            this.colAuthorisedByStaff.Name = "colAuthorisedByStaff";
            this.colAuthorisedByStaff.OptionsColumn.AllowEdit = false;
            this.colAuthorisedByStaff.OptionsColumn.AllowFocus = false;
            this.colAuthorisedByStaff.OptionsColumn.ReadOnly = true;
            this.colAuthorisedByStaff.Visible = true;
            this.colAuthorisedByStaff.VisibleIndex = 3;
            this.colAuthorisedByStaff.Width = 140;
            // 
            // colBillingTypeID
            // 
            this.colBillingTypeID.Caption = "Billing Type ID";
            this.colBillingTypeID.FieldName = "BillingTypeID";
            this.colBillingTypeID.Name = "colBillingTypeID";
            this.colBillingTypeID.OptionsColumn.AllowEdit = false;
            this.colBillingTypeID.OptionsColumn.AllowFocus = false;
            this.colBillingTypeID.OptionsColumn.ReadOnly = true;
            this.colBillingTypeID.Width = 86;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // gridControlJob
            // 
            this.gridControlJob.DataSource = this.sp06454OMSelfBillingJobsLinkedtoVisitsBindingSource;
            this.gridControlJob.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlJob.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlJob.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlJob.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlJob.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 13, true, true, "Reload Data", "reload")});
            this.gridControlJob.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlJob_EmbeddedNavigator_ButtonClick);
            this.gridControlJob.Location = new System.Drawing.Point(0, 0);
            this.gridControlJob.MainView = this.gridViewJob;
            this.gridControlJob.MenuManager = this.barManager1;
            this.gridControlJob.Name = "gridControlJob";
            this.gridControlJob.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemTextEditMoney2,
            this.repositoryItemHyperLinkEditLinkedDocumentsJobs,
            this.repositoryItemTextEditPercentage2,
            this.repositoryItemTextEditLatLong2,
            this.repositoryItemHyperLinkEditViewJob,
            this.repositoryItemTextEditInteger,
            this.repositoryItemTextEditIntegerDays,
            this.repositoryItemHyperLinkEditPictures,
            this.repositoryItemTextEditHTML1});
            this.gridControlJob.Size = new System.Drawing.Size(248, 378);
            this.gridControlJob.TabIndex = 5;
            this.gridControlJob.ToolTipController = this.toolTipController1;
            this.gridControlJob.UseEmbeddedNavigator = true;
            this.gridControlJob.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewJob});
            // 
            // sp06454OMSelfBillingJobsLinkedtoVisitsBindingSource
            // 
            this.sp06454OMSelfBillingJobsLinkedtoVisitsBindingSource.DataMember = "sp06454_OM_Self_Billing_Jobs_Linked_to_Visits";
            this.sp06454OMSelfBillingJobsLinkedtoVisitsBindingSource.DataSource = this.dataSet_OM_Billing;
            // 
            // gridViewJob
            // 
            this.gridViewJob.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobID,
            this.gridColumn1,
            this.gridColumn2,
            this.colJobStatusID,
            this.colJobSubTypeID,
            this.colCreatedByStaffID1,
            this.colReactive,
            this.colJobNoLongerRequired,
            this.colRequiresAccessPermit,
            this.colAccessPermitID,
            this.colClientPONumber,
            this.colClientPOID,
            this.colFinanceSystemPONumber,
            this.gridColumn3,
            this.gridColumn4,
            this.colExpectedDurationUnits,
            this.colExpectedDurationUnitsDescriptorID,
            this.colScheduleSentDate,
            this.colActualStartDate,
            this.colActualEndDate,
            this.colActualDurationUnits,
            this.colActualDurationUnitsDescriptionID,
            this.colCostTotalMaterialExVAT,
            this.colCostTotalMaterialVAT,
            this.colCostTotalMaterialCost,
            this.colSellTotalMaterialExVAT,
            this.colSellTotalMaterialVAT,
            this.colSellTotalMaterialCost,
            this.colCostTotalEquipmentExVAT,
            this.colCostTotalEquipmentVAT,
            this.colCostTotalEquipmentCost,
            this.colSellTotalEquipmentExVAT,
            this.colSellTotalEquipmentVAT,
            this.colSellTotalEquipmentCost,
            this.colCostTotalLabourExVAT,
            this.colCostTotalLabourVAT,
            this.colCostTotalLabourCost,
            this.colSellTotalLabourExVAT,
            this.colSellTotalLabourVAT,
            this.colSellTotalLabourCost,
            this.colCostTotalCostExVAT,
            this.colCostTotalCostVAT,
            this.colCostTotalCost,
            this.colSellTotalCostExVAT,
            this.colSellTotalCostVAT,
            this.colSellTotalCost,
            this.colClientInvoiceID,
            this.colDateClientInvoiced,
            this.colSelfBillingInvoiceID,
            this.colSelfBillingInvoiceReceivedDate,
            this.colSelfBillingInvoicePaidDate,
            this.colSelfBillingInvoiceAmountPaid,
            this.colDoNotPayContractor,
            this.colDoNotInvoiceClient,
            this.colRemarks2,
            this.colRouteOrder,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.colJobStatusDescription,
            this.gridColumn9,
            this.gridColumn10,
            this.colCreatedByStaffName1,
            this.colExpectedDurationUnitsDescriptor,
            this.colActualDurationUnitsDescription,
            this.colJobTypeID1,
            this.colJobSubTypeDescription,
            this.colJobTypeDescription1,
            this.colRecordType,
            this.colSelected,
            this.colFullDescription1,
            this.colTenderID,
            this.colTenderJobID,
            this.colMinimumDaysFromLastVisit,
            this.colMaximumDaysFromLastVisit,
            this.colDaysLeeway,
            this.colBillingCentreCodeID,
            this.gridColumn12,
            this.gridColumn13,
            this.colCostTotalEquipmentVATRate,
            this.colCostTotalLabourVATRate,
            this.colCostTotalMaterialVATRate,
            this.colSellTotalMaterialVATRate,
            this.colSellTotalEquipmentVATRate,
            this.colSellTotalLabourVATRate,
            this.gridColumn14,
            this.colClientID2,
            this.gridColumn15,
            this.colSiteID2,
            this.colLocationX,
            this.colLocationY,
            this.colSitePostcode1,
            this.colLinkedToVisit,
            this.colClientAndContract,
            this.colReworkOriginalJobID,
            this.colRework1,
            this.colCancelledReason,
            this.colCancelledReasonID,
            this.colDaysUntilDue,
            this.colManuallyCompleted,
            this.colLinkedPictureCount1,
            this.colImagesFolderOM1,
            this.colFriendlyVisitNumber1,
            this.colClientPO1,
            this.colVisitType1,
            this.colVisitTypeID1,
            this.colSiteCode1,
            this.colVisitCostCalculationLevelID,
            this.colVisitSellCalculationLevelID,
            this.colVisitCostCalculationLevelDescription,
            this.colVisitSellCalculationLevelDescription,
            this.colDisplayOrder,
            this.colMandatory,
            this.colSuspendedReason,
            this.colSuspendedRemarks,
            this.colLabourName2,
            this.colLinkedToPersonType1,
            this.colLinkedToPersonTypeID1,
            this.colLabourUsedID});
            this.gridViewJob.GridControl = this.gridControlJob;
            this.gridViewJob.GroupCount = 3;
            this.gridViewJob.Name = "gridViewJob";
            this.gridViewJob.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewJob.OptionsFind.FindDelay = 2000;
            this.gridViewJob.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewJob.OptionsLayout.StoreAppearance = true;
            this.gridViewJob.OptionsLayout.StoreFormatRules = true;
            this.gridViewJob.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewJob.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewJob.OptionsSelection.MultiSelect = true;
            this.gridViewJob.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewJob.OptionsView.ColumnAutoWidth = false;
            this.gridViewJob.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewJob.OptionsView.ShowGroupPanel = false;
            this.gridViewJob.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientAndContract, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn9, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDisplayOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobTypeDescription1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobSubTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn4, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewJob.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewJob_CustomDrawCell);
            this.gridViewJob.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewJob_CustomRowCellEdit);
            this.gridViewJob.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewJob.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewJob.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewJob.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewJob_ShowingEditor);
            this.gridViewJob.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewJob.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewJob.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridViewJob_CustomUnboundColumnData);
            this.gridViewJob.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewJob.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewJob_MouseUp);
            this.gridViewJob.DoubleClick += new System.EventHandler(this.gridViewJob_DoubleClick);
            this.gridViewJob.GotFocus += new System.EventHandler(this.gridViewJob_GotFocus);
            // 
            // colJobID
            // 
            this.colJobID.Caption = "Job ID";
            this.colJobID.FieldName = "JobID";
            this.colJobID.Name = "colJobID";
            this.colJobID.OptionsColumn.AllowEdit = false;
            this.colJobID.OptionsColumn.AllowFocus = false;
            this.colJobID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Visit ID";
            this.gridColumn1.FieldName = "VisitID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 54;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Visit #";
            this.gridColumn2.ColumnEdit = this.repositoryItemTextEditInteger;
            this.gridColumn2.FieldName = "VisitNumber";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 52;
            this.gridColumn2.Width = 51;
            // 
            // repositoryItemTextEditInteger
            // 
            this.repositoryItemTextEditInteger.AutoHeight = false;
            this.repositoryItemTextEditInteger.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger.Name = "repositoryItemTextEditInteger";
            // 
            // colJobSubTypeID
            // 
            this.colJobSubTypeID.Caption = "Job Sub-Type ID";
            this.colJobSubTypeID.FieldName = "JobSubTypeID";
            this.colJobSubTypeID.Name = "colJobSubTypeID";
            this.colJobSubTypeID.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeID.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeID.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeID.Width = 102;
            // 
            // colCreatedByStaffID1
            // 
            this.colCreatedByStaffID1.Caption = "Created By Staff ID";
            this.colCreatedByStaffID1.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID1.Name = "colCreatedByStaffID1";
            this.colCreatedByStaffID1.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID1.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID1.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID1.Width = 116;
            // 
            // colReactive
            // 
            this.colReactive.Caption = "Reactive";
            this.colReactive.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colReactive.FieldName = "Reactive";
            this.colReactive.Name = "colReactive";
            this.colReactive.OptionsColumn.AllowEdit = false;
            this.colReactive.OptionsColumn.AllowFocus = false;
            this.colReactive.OptionsColumn.ReadOnly = true;
            this.colReactive.Visible = true;
            this.colReactive.VisibleIndex = 12;
            this.colReactive.Width = 63;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colJobNoLongerRequired
            // 
            this.colJobNoLongerRequired.Caption = "No Longer Required";
            this.colJobNoLongerRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colJobNoLongerRequired.FieldName = "JobNoLongerRequired";
            this.colJobNoLongerRequired.Name = "colJobNoLongerRequired";
            this.colJobNoLongerRequired.OptionsColumn.AllowEdit = false;
            this.colJobNoLongerRequired.OptionsColumn.AllowFocus = false;
            this.colJobNoLongerRequired.OptionsColumn.ReadOnly = true;
            this.colJobNoLongerRequired.Visible = true;
            this.colJobNoLongerRequired.VisibleIndex = 25;
            this.colJobNoLongerRequired.Width = 116;
            // 
            // colRequiresAccessPermit
            // 
            this.colRequiresAccessPermit.Caption = "Access Permit Required";
            this.colRequiresAccessPermit.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colRequiresAccessPermit.FieldName = "RequiresAccessPermit";
            this.colRequiresAccessPermit.Name = "colRequiresAccessPermit";
            this.colRequiresAccessPermit.OptionsColumn.AllowEdit = false;
            this.colRequiresAccessPermit.OptionsColumn.AllowFocus = false;
            this.colRequiresAccessPermit.OptionsColumn.ReadOnly = true;
            this.colRequiresAccessPermit.Visible = true;
            this.colRequiresAccessPermit.VisibleIndex = 13;
            this.colRequiresAccessPermit.Width = 133;
            // 
            // colAccessPermitID
            // 
            this.colAccessPermitID.Caption = "Access Permit ID";
            this.colAccessPermitID.FieldName = "AccessPermitID";
            this.colAccessPermitID.Name = "colAccessPermitID";
            this.colAccessPermitID.OptionsColumn.AllowEdit = false;
            this.colAccessPermitID.OptionsColumn.AllowFocus = false;
            this.colAccessPermitID.OptionsColumn.ReadOnly = true;
            this.colAccessPermitID.Width = 101;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "Client PO # [Manual]";
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.OptionsColumn.AllowEdit = false;
            this.colClientPONumber.OptionsColumn.AllowFocus = false;
            this.colClientPONumber.OptionsColumn.ReadOnly = true;
            this.colClientPONumber.Visible = true;
            this.colClientPONumber.VisibleIndex = 57;
            this.colClientPONumber.Width = 119;
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "Client PO ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            this.colClientPOID.Width = 79;
            // 
            // colFinanceSystemPONumber
            // 
            this.colFinanceSystemPONumber.Caption = "Finance System PO #";
            this.colFinanceSystemPONumber.FieldName = "FinanceSystemPONumber";
            this.colFinanceSystemPONumber.Name = "colFinanceSystemPONumber";
            this.colFinanceSystemPONumber.OptionsColumn.AllowEdit = false;
            this.colFinanceSystemPONumber.OptionsColumn.AllowFocus = false;
            this.colFinanceSystemPONumber.OptionsColumn.ReadOnly = true;
            this.colFinanceSystemPONumber.Visible = true;
            this.colFinanceSystemPONumber.VisibleIndex = 59;
            this.colFinanceSystemPONumber.Width = 124;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Expected Start";
            this.gridColumn3.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn3.FieldName = "ExpectedStartDate";
            this.gridColumn3.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 8;
            this.gridColumn3.Width = 106;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Expected End";
            this.gridColumn4.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn4.FieldName = "ExpectedEndDate";
            this.gridColumn4.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 9;
            this.gridColumn4.Width = 100;
            // 
            // colExpectedDurationUnits
            // 
            this.colExpectedDurationUnits.Caption = "Expected Duration";
            this.colExpectedDurationUnits.FieldName = "ExpectedDurationUnits";
            this.colExpectedDurationUnits.Name = "colExpectedDurationUnits";
            this.colExpectedDurationUnits.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnits.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnits.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnits.Visible = true;
            this.colExpectedDurationUnits.VisibleIndex = 10;
            this.colExpectedDurationUnits.Width = 110;
            // 
            // colExpectedDurationUnitsDescriptorID
            // 
            this.colExpectedDurationUnitsDescriptorID.Caption = "Expected Duration Unit Descriptor ID";
            this.colExpectedDurationUnitsDescriptorID.FieldName = "ExpectedDurationUnitsDescriptorID";
            this.colExpectedDurationUnitsDescriptorID.Name = "colExpectedDurationUnitsDescriptorID";
            this.colExpectedDurationUnitsDescriptorID.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnitsDescriptorID.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnitsDescriptorID.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnitsDescriptorID.Width = 198;
            // 
            // colScheduleSentDate
            // 
            this.colScheduleSentDate.Caption = "Schedule Sent";
            this.colScheduleSentDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colScheduleSentDate.FieldName = "ScheduleSentDate";
            this.colScheduleSentDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colScheduleSentDate.Name = "colScheduleSentDate";
            this.colScheduleSentDate.OptionsColumn.AllowEdit = false;
            this.colScheduleSentDate.OptionsColumn.AllowFocus = false;
            this.colScheduleSentDate.OptionsColumn.ReadOnly = true;
            this.colScheduleSentDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colScheduleSentDate.Visible = true;
            this.colScheduleSentDate.VisibleIndex = 20;
            this.colScheduleSentDate.Width = 106;
            // 
            // colActualStartDate
            // 
            this.colActualStartDate.Caption = "Actual Start";
            this.colActualStartDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colActualStartDate.FieldName = "ActualStartDate";
            this.colActualStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colActualStartDate.Name = "colActualStartDate";
            this.colActualStartDate.OptionsColumn.AllowEdit = false;
            this.colActualStartDate.OptionsColumn.AllowFocus = false;
            this.colActualStartDate.OptionsColumn.ReadOnly = true;
            this.colActualStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colActualStartDate.Visible = true;
            this.colActualStartDate.VisibleIndex = 21;
            this.colActualStartDate.Width = 100;
            // 
            // colActualEndDate
            // 
            this.colActualEndDate.Caption = "Actual End";
            this.colActualEndDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colActualEndDate.FieldName = "ActualEndDate";
            this.colActualEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colActualEndDate.Name = "colActualEndDate";
            this.colActualEndDate.OptionsColumn.AllowEdit = false;
            this.colActualEndDate.OptionsColumn.AllowFocus = false;
            this.colActualEndDate.OptionsColumn.ReadOnly = true;
            this.colActualEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colActualEndDate.Visible = true;
            this.colActualEndDate.VisibleIndex = 22;
            this.colActualEndDate.Width = 100;
            // 
            // colActualDurationUnits
            // 
            this.colActualDurationUnits.Caption = "Actual Duration";
            this.colActualDurationUnits.FieldName = "ActualDurationUnits";
            this.colActualDurationUnits.Name = "colActualDurationUnits";
            this.colActualDurationUnits.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnits.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnits.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnits.Visible = true;
            this.colActualDurationUnits.VisibleIndex = 23;
            this.colActualDurationUnits.Width = 95;
            // 
            // colActualDurationUnitsDescriptionID
            // 
            this.colActualDurationUnitsDescriptionID.Caption = "Actual Duration Unit Descriptor ID";
            this.colActualDurationUnitsDescriptionID.FieldName = "ActualDurationUnitsDescriptionID";
            this.colActualDurationUnitsDescriptionID.Name = "colActualDurationUnitsDescriptionID";
            this.colActualDurationUnitsDescriptionID.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnitsDescriptionID.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnitsDescriptionID.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnitsDescriptionID.Width = 183;
            // 
            // colCostTotalMaterialExVAT
            // 
            this.colCostTotalMaterialExVAT.Caption = "Material Cost Ex VAT";
            this.colCostTotalMaterialExVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalMaterialExVAT.FieldName = "CostTotalMaterialExVAT";
            this.colCostTotalMaterialExVAT.Name = "colCostTotalMaterialExVAT";
            this.colCostTotalMaterialExVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalMaterialExVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalMaterialExVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalMaterialExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalMaterialExVAT", "{0:c}")});
            this.colCostTotalMaterialExVAT.Visible = true;
            this.colCostTotalMaterialExVAT.VisibleIndex = 26;
            this.colCostTotalMaterialExVAT.Width = 121;
            // 
            // repositoryItemTextEditMoney2
            // 
            this.repositoryItemTextEditMoney2.AutoHeight = false;
            this.repositoryItemTextEditMoney2.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney2.Name = "repositoryItemTextEditMoney2";
            // 
            // colCostTotalMaterialVAT
            // 
            this.colCostTotalMaterialVAT.Caption = "Material Cost VAT";
            this.colCostTotalMaterialVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalMaterialVAT.FieldName = "CostTotalMaterialVAT";
            this.colCostTotalMaterialVAT.Name = "colCostTotalMaterialVAT";
            this.colCostTotalMaterialVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalMaterialVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalMaterialVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalMaterialVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalMaterialVAT", "{0:c}")});
            this.colCostTotalMaterialVAT.Visible = true;
            this.colCostTotalMaterialVAT.VisibleIndex = 28;
            this.colCostTotalMaterialVAT.Width = 106;
            // 
            // colCostTotalMaterialCost
            // 
            this.colCostTotalMaterialCost.Caption = "Material Cost Total";
            this.colCostTotalMaterialCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalMaterialCost.FieldName = "CostTotalMaterialCost";
            this.colCostTotalMaterialCost.Name = "colCostTotalMaterialCost";
            this.colCostTotalMaterialCost.OptionsColumn.AllowEdit = false;
            this.colCostTotalMaterialCost.OptionsColumn.AllowFocus = false;
            this.colCostTotalMaterialCost.OptionsColumn.ReadOnly = true;
            this.colCostTotalMaterialCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalMaterialCost", "{0:c}")});
            this.colCostTotalMaterialCost.Visible = true;
            this.colCostTotalMaterialCost.VisibleIndex = 29;
            this.colCostTotalMaterialCost.Width = 111;
            // 
            // colSellTotalMaterialExVAT
            // 
            this.colSellTotalMaterialExVAT.Caption = "Material Sell Ex VAT";
            this.colSellTotalMaterialExVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalMaterialExVAT.FieldName = "SellTotalMaterialExVAT";
            this.colSellTotalMaterialExVAT.Name = "colSellTotalMaterialExVAT";
            this.colSellTotalMaterialExVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalMaterialExVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalMaterialExVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalMaterialExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalMaterialExVAT", "{0:c}")});
            this.colSellTotalMaterialExVAT.Visible = true;
            this.colSellTotalMaterialExVAT.VisibleIndex = 30;
            this.colSellTotalMaterialExVAT.Width = 115;
            // 
            // colSellTotalMaterialVAT
            // 
            this.colSellTotalMaterialVAT.Caption = "Material Sell VAT";
            this.colSellTotalMaterialVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalMaterialVAT.FieldName = "SellTotalMaterialVAT";
            this.colSellTotalMaterialVAT.Name = "colSellTotalMaterialVAT";
            this.colSellTotalMaterialVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalMaterialVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalMaterialVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalMaterialVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalMaterialVAT", "{0:c}")});
            this.colSellTotalMaterialVAT.Visible = true;
            this.colSellTotalMaterialVAT.VisibleIndex = 32;
            this.colSellTotalMaterialVAT.Width = 100;
            // 
            // colSellTotalMaterialCost
            // 
            this.colSellTotalMaterialCost.Caption = "Material Sell Total";
            this.colSellTotalMaterialCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalMaterialCost.FieldName = "SellTotalMaterialCost";
            this.colSellTotalMaterialCost.Name = "colSellTotalMaterialCost";
            this.colSellTotalMaterialCost.OptionsColumn.AllowEdit = false;
            this.colSellTotalMaterialCost.OptionsColumn.AllowFocus = false;
            this.colSellTotalMaterialCost.OptionsColumn.ReadOnly = true;
            this.colSellTotalMaterialCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalMaterialCost", "{0:c}")});
            this.colSellTotalMaterialCost.Visible = true;
            this.colSellTotalMaterialCost.VisibleIndex = 33;
            this.colSellTotalMaterialCost.Width = 105;
            // 
            // colCostTotalEquipmentExVAT
            // 
            this.colCostTotalEquipmentExVAT.Caption = "Equipment Cost Ex VAT";
            this.colCostTotalEquipmentExVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalEquipmentExVAT.FieldName = "CostTotalEquipmentExVAT";
            this.colCostTotalEquipmentExVAT.Name = "colCostTotalEquipmentExVAT";
            this.colCostTotalEquipmentExVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalEquipmentExVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalEquipmentExVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalEquipmentExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalEquipmentExVAT", "{0:c}")});
            this.colCostTotalEquipmentExVAT.Visible = true;
            this.colCostTotalEquipmentExVAT.VisibleIndex = 34;
            this.colCostTotalEquipmentExVAT.Width = 133;
            // 
            // colCostTotalEquipmentVAT
            // 
            this.colCostTotalEquipmentVAT.Caption = "Equipment Cost VAT";
            this.colCostTotalEquipmentVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalEquipmentVAT.FieldName = "CostTotalEquipmentVAT";
            this.colCostTotalEquipmentVAT.Name = "colCostTotalEquipmentVAT";
            this.colCostTotalEquipmentVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalEquipmentVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalEquipmentVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalEquipmentVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalEquipmentVAT", "{0:c}")});
            this.colCostTotalEquipmentVAT.Visible = true;
            this.colCostTotalEquipmentVAT.VisibleIndex = 36;
            this.colCostTotalEquipmentVAT.Width = 118;
            // 
            // colCostTotalEquipmentCost
            // 
            this.colCostTotalEquipmentCost.Caption = "Equipment Cost Total";
            this.colCostTotalEquipmentCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalEquipmentCost.FieldName = "CostTotalEquipmentCost";
            this.colCostTotalEquipmentCost.Name = "colCostTotalEquipmentCost";
            this.colCostTotalEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colCostTotalEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colCostTotalEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colCostTotalEquipmentCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalEquipmentCost", "{0:c}")});
            this.colCostTotalEquipmentCost.Visible = true;
            this.colCostTotalEquipmentCost.VisibleIndex = 37;
            this.colCostTotalEquipmentCost.Width = 123;
            // 
            // colSellTotalEquipmentExVAT
            // 
            this.colSellTotalEquipmentExVAT.Caption = "Equipment Sell Ex VAT";
            this.colSellTotalEquipmentExVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalEquipmentExVAT.FieldName = "SellTotalEquipmentExVAT";
            this.colSellTotalEquipmentExVAT.Name = "colSellTotalEquipmentExVAT";
            this.colSellTotalEquipmentExVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalEquipmentExVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalEquipmentExVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalEquipmentExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalEquipmentExVAT", "{0:c}")});
            this.colSellTotalEquipmentExVAT.Visible = true;
            this.colSellTotalEquipmentExVAT.VisibleIndex = 38;
            this.colSellTotalEquipmentExVAT.Width = 127;
            // 
            // colSellTotalEquipmentVAT
            // 
            this.colSellTotalEquipmentVAT.Caption = "Equipment Sell VAT";
            this.colSellTotalEquipmentVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalEquipmentVAT.FieldName = "SellTotalEquipmentVAT";
            this.colSellTotalEquipmentVAT.Name = "colSellTotalEquipmentVAT";
            this.colSellTotalEquipmentVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalEquipmentVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalEquipmentVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalEquipmentVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalEquipmentVAT", "{0:c}")});
            this.colSellTotalEquipmentVAT.Visible = true;
            this.colSellTotalEquipmentVAT.VisibleIndex = 40;
            this.colSellTotalEquipmentVAT.Width = 112;
            // 
            // colSellTotalEquipmentCost
            // 
            this.colSellTotalEquipmentCost.Caption = "Equipment Sell Total";
            this.colSellTotalEquipmentCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalEquipmentCost.FieldName = "SellTotalEquipmentCost";
            this.colSellTotalEquipmentCost.Name = "colSellTotalEquipmentCost";
            this.colSellTotalEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colSellTotalEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colSellTotalEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colSellTotalEquipmentCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalEquipmentCost", "{0:c}")});
            this.colSellTotalEquipmentCost.Visible = true;
            this.colSellTotalEquipmentCost.VisibleIndex = 41;
            this.colSellTotalEquipmentCost.Width = 117;
            // 
            // colCostTotalLabourExVAT
            // 
            this.colCostTotalLabourExVAT.Caption = "Labour Cost Ex VAT";
            this.colCostTotalLabourExVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalLabourExVAT.FieldName = "CostTotalLabourExVAT";
            this.colCostTotalLabourExVAT.Name = "colCostTotalLabourExVAT";
            this.colCostTotalLabourExVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalLabourExVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalLabourExVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalLabourExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalLabourExVAT", "{0:c}")});
            this.colCostTotalLabourExVAT.Visible = true;
            this.colCostTotalLabourExVAT.VisibleIndex = 42;
            this.colCostTotalLabourExVAT.Width = 116;
            // 
            // colCostTotalLabourVAT
            // 
            this.colCostTotalLabourVAT.Caption = "Labour Cost VAT";
            this.colCostTotalLabourVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalLabourVAT.FieldName = "CostTotalLabourVAT";
            this.colCostTotalLabourVAT.Name = "colCostTotalLabourVAT";
            this.colCostTotalLabourVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalLabourVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalLabourVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalLabourVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalLabourVAT", "{0:c}")});
            this.colCostTotalLabourVAT.Visible = true;
            this.colCostTotalLabourVAT.VisibleIndex = 44;
            this.colCostTotalLabourVAT.Width = 101;
            // 
            // colCostTotalLabourCost
            // 
            this.colCostTotalLabourCost.Caption = "Labour Cost Total";
            this.colCostTotalLabourCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalLabourCost.FieldName = "CostTotalLabourCost";
            this.colCostTotalLabourCost.Name = "colCostTotalLabourCost";
            this.colCostTotalLabourCost.OptionsColumn.AllowEdit = false;
            this.colCostTotalLabourCost.OptionsColumn.AllowFocus = false;
            this.colCostTotalLabourCost.OptionsColumn.ReadOnly = true;
            this.colCostTotalLabourCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalLabourCost", "{0:c}")});
            this.colCostTotalLabourCost.Visible = true;
            this.colCostTotalLabourCost.VisibleIndex = 45;
            this.colCostTotalLabourCost.Width = 106;
            // 
            // colSellTotalLabourExVAT
            // 
            this.colSellTotalLabourExVAT.Caption = "Labour Sell Ex VAT";
            this.colSellTotalLabourExVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalLabourExVAT.FieldName = "SellTotalLabourExVAT";
            this.colSellTotalLabourExVAT.Name = "colSellTotalLabourExVAT";
            this.colSellTotalLabourExVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalLabourExVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalLabourExVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalLabourExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalLabourExVAT", "{0:c}")});
            this.colSellTotalLabourExVAT.Visible = true;
            this.colSellTotalLabourExVAT.VisibleIndex = 46;
            this.colSellTotalLabourExVAT.Width = 110;
            // 
            // colSellTotalLabourVAT
            // 
            this.colSellTotalLabourVAT.Caption = "Labour Sell VAT";
            this.colSellTotalLabourVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalLabourVAT.FieldName = "SellTotalLabourVAT";
            this.colSellTotalLabourVAT.Name = "colSellTotalLabourVAT";
            this.colSellTotalLabourVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalLabourVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalLabourVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalLabourVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalLabourVAT", "{0:c}")});
            this.colSellTotalLabourVAT.Visible = true;
            this.colSellTotalLabourVAT.VisibleIndex = 48;
            this.colSellTotalLabourVAT.Width = 95;
            // 
            // colSellTotalLabourCost
            // 
            this.colSellTotalLabourCost.Caption = "Labour Sell Total";
            this.colSellTotalLabourCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalLabourCost.FieldName = "SellTotalLabourCost";
            this.colSellTotalLabourCost.Name = "colSellTotalLabourCost";
            this.colSellTotalLabourCost.OptionsColumn.AllowEdit = false;
            this.colSellTotalLabourCost.OptionsColumn.AllowFocus = false;
            this.colSellTotalLabourCost.OptionsColumn.ReadOnly = true;
            this.colSellTotalLabourCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalLabourCost", "{0:c}")});
            this.colSellTotalLabourCost.Visible = true;
            this.colSellTotalLabourCost.VisibleIndex = 49;
            this.colSellTotalLabourCost.Width = 100;
            // 
            // colCostTotalCostExVAT
            // 
            this.colCostTotalCostExVAT.Caption = "Total Cost Ex VAT";
            this.colCostTotalCostExVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalCostExVAT.FieldName = "CostTotalCostExVAT";
            this.colCostTotalCostExVAT.Name = "colCostTotalCostExVAT";
            this.colCostTotalCostExVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalCostExVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalCostExVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalCostExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalCostExVAT", "{0:c}")});
            this.colCostTotalCostExVAT.Visible = true;
            this.colCostTotalCostExVAT.VisibleIndex = 50;
            this.colCostTotalCostExVAT.Width = 107;
            // 
            // colCostTotalCostVAT
            // 
            this.colCostTotalCostVAT.Caption = "Total Cost VAT";
            this.colCostTotalCostVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalCostVAT.FieldName = "CostTotalCostVAT";
            this.colCostTotalCostVAT.Name = "colCostTotalCostVAT";
            this.colCostTotalCostVAT.OptionsColumn.AllowEdit = false;
            this.colCostTotalCostVAT.OptionsColumn.AllowFocus = false;
            this.colCostTotalCostVAT.OptionsColumn.ReadOnly = true;
            this.colCostTotalCostVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalCostVAT", "{0:c}")});
            this.colCostTotalCostVAT.Visible = true;
            this.colCostTotalCostVAT.VisibleIndex = 51;
            this.colCostTotalCostVAT.Width = 92;
            // 
            // colCostTotalCost
            // 
            this.colCostTotalCost.Caption = "Total Cost";
            this.colCostTotalCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colCostTotalCost.FieldName = "CostTotalCost";
            this.colCostTotalCost.Name = "colCostTotalCost";
            this.colCostTotalCost.OptionsColumn.AllowEdit = false;
            this.colCostTotalCost.OptionsColumn.AllowFocus = false;
            this.colCostTotalCost.OptionsColumn.ReadOnly = true;
            this.colCostTotalCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "CostTotalCost", "{0:c}")});
            this.colCostTotalCost.Visible = true;
            this.colCostTotalCost.VisibleIndex = 52;
            // 
            // colSellTotalCostExVAT
            // 
            this.colSellTotalCostExVAT.Caption = "Total Sell Ex VAT";
            this.colSellTotalCostExVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalCostExVAT.FieldName = "SellTotalCostExVAT";
            this.colSellTotalCostExVAT.Name = "colSellTotalCostExVAT";
            this.colSellTotalCostExVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalCostExVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalCostExVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalCostExVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalCostExVAT", "{0:c}")});
            this.colSellTotalCostExVAT.Visible = true;
            this.colSellTotalCostExVAT.VisibleIndex = 53;
            this.colSellTotalCostExVAT.Width = 101;
            // 
            // colSellTotalCostVAT
            // 
            this.colSellTotalCostVAT.Caption = "Total Sell VAT";
            this.colSellTotalCostVAT.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalCostVAT.FieldName = "SellTotalCostVAT";
            this.colSellTotalCostVAT.Name = "colSellTotalCostVAT";
            this.colSellTotalCostVAT.OptionsColumn.AllowEdit = false;
            this.colSellTotalCostVAT.OptionsColumn.AllowFocus = false;
            this.colSellTotalCostVAT.OptionsColumn.ReadOnly = true;
            this.colSellTotalCostVAT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalCostVAT", "{0:c}")});
            this.colSellTotalCostVAT.Visible = true;
            this.colSellTotalCostVAT.VisibleIndex = 54;
            this.colSellTotalCostVAT.Width = 86;
            // 
            // colSellTotalCost
            // 
            this.colSellTotalCost.Caption = "Total Sell";
            this.colSellTotalCost.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSellTotalCost.FieldName = "SellTotalCost";
            this.colSellTotalCost.Name = "colSellTotalCost";
            this.colSellTotalCost.OptionsColumn.AllowEdit = false;
            this.colSellTotalCost.OptionsColumn.AllowFocus = false;
            this.colSellTotalCost.OptionsColumn.ReadOnly = true;
            this.colSellTotalCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SellTotalCost", "{0:c}")});
            this.colSellTotalCost.Visible = true;
            this.colSellTotalCost.VisibleIndex = 55;
            // 
            // colClientInvoiceID
            // 
            this.colClientInvoiceID.Caption = "Client Invoice ID";
            this.colClientInvoiceID.FieldName = "ClientInvoiceID";
            this.colClientInvoiceID.Name = "colClientInvoiceID";
            this.colClientInvoiceID.OptionsColumn.AllowEdit = false;
            this.colClientInvoiceID.OptionsColumn.AllowFocus = false;
            this.colClientInvoiceID.OptionsColumn.ReadOnly = true;
            this.colClientInvoiceID.Width = 100;
            // 
            // colDateClientInvoiced
            // 
            this.colDateClientInvoiced.Caption = "Client Invoice Date";
            this.colDateClientInvoiced.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colDateClientInvoiced.FieldName = "DateClientInvoiced";
            this.colDateClientInvoiced.Name = "colDateClientInvoiced";
            this.colDateClientInvoiced.OptionsColumn.AllowEdit = false;
            this.colDateClientInvoiced.OptionsColumn.AllowFocus = false;
            this.colDateClientInvoiced.OptionsColumn.ReadOnly = true;
            this.colDateClientInvoiced.Visible = true;
            this.colDateClientInvoiced.VisibleIndex = 58;
            this.colDateClientInvoiced.Width = 112;
            // 
            // colSelfBillingInvoiceID
            // 
            this.colSelfBillingInvoiceID.Caption = "Self Billing Invoice ID";
            this.colSelfBillingInvoiceID.FieldName = "SelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.Name = "colSelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceID.Width = 120;
            // 
            // colSelfBillingInvoiceReceivedDate
            // 
            this.colSelfBillingInvoiceReceivedDate.Caption = "Self Billing Invoice Received Date";
            this.colSelfBillingInvoiceReceivedDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colSelfBillingInvoiceReceivedDate.FieldName = "SelfBillingInvoiceReceivedDate";
            this.colSelfBillingInvoiceReceivedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSelfBillingInvoiceReceivedDate.Name = "colSelfBillingInvoiceReceivedDate";
            this.colSelfBillingInvoiceReceivedDate.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceReceivedDate.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceReceivedDate.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceReceivedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSelfBillingInvoiceReceivedDate.Visible = true;
            this.colSelfBillingInvoiceReceivedDate.VisibleIndex = 60;
            this.colSelfBillingInvoiceReceivedDate.Width = 179;
            // 
            // colSelfBillingInvoicePaidDate
            // 
            this.colSelfBillingInvoicePaidDate.Caption = "Self Billing Invoice Paid Date";
            this.colSelfBillingInvoicePaidDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colSelfBillingInvoicePaidDate.FieldName = "SelfBillingInvoicePaidDate";
            this.colSelfBillingInvoicePaidDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSelfBillingInvoicePaidDate.Name = "colSelfBillingInvoicePaidDate";
            this.colSelfBillingInvoicePaidDate.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoicePaidDate.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoicePaidDate.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoicePaidDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSelfBillingInvoicePaidDate.Visible = true;
            this.colSelfBillingInvoicePaidDate.VisibleIndex = 61;
            this.colSelfBillingInvoicePaidDate.Width = 155;
            // 
            // colSelfBillingInvoiceAmountPaid
            // 
            this.colSelfBillingInvoiceAmountPaid.Caption = "Self Billing Invoice Amunt Paid";
            this.colSelfBillingInvoiceAmountPaid.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colSelfBillingInvoiceAmountPaid.FieldName = "SelfBillingInvoiceAmountPaid";
            this.colSelfBillingInvoiceAmountPaid.Name = "colSelfBillingInvoiceAmountPaid";
            this.colSelfBillingInvoiceAmountPaid.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceAmountPaid.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceAmountPaid.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceAmountPaid.Visible = true;
            this.colSelfBillingInvoiceAmountPaid.VisibleIndex = 62;
            this.colSelfBillingInvoiceAmountPaid.Width = 163;
            // 
            // colDoNotPayContractor
            // 
            this.colDoNotPayContractor.Caption = "Do Not Pay Contractor";
            this.colDoNotPayContractor.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDoNotPayContractor.FieldName = "DoNotPayContractor";
            this.colDoNotPayContractor.Name = "colDoNotPayContractor";
            this.colDoNotPayContractor.OptionsColumn.AllowEdit = false;
            this.colDoNotPayContractor.OptionsColumn.AllowFocus = false;
            this.colDoNotPayContractor.OptionsColumn.ReadOnly = true;
            this.colDoNotPayContractor.Visible = true;
            this.colDoNotPayContractor.VisibleIndex = 63;
            this.colDoNotPayContractor.Width = 130;
            // 
            // colDoNotInvoiceClient
            // 
            this.colDoNotInvoiceClient.Caption = "Do Not Invoice Client";
            this.colDoNotInvoiceClient.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDoNotInvoiceClient.FieldName = "DoNotInvoiceClient";
            this.colDoNotInvoiceClient.Name = "colDoNotInvoiceClient";
            this.colDoNotInvoiceClient.OptionsColumn.AllowEdit = false;
            this.colDoNotInvoiceClient.OptionsColumn.AllowFocus = false;
            this.colDoNotInvoiceClient.OptionsColumn.ReadOnly = true;
            this.colDoNotInvoiceClient.Visible = true;
            this.colDoNotInvoiceClient.VisibleIndex = 64;
            this.colDoNotInvoiceClient.Width = 122;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 80;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colRouteOrder
            // 
            this.colRouteOrder.Caption = "Route Order";
            this.colRouteOrder.FieldName = "RouteOrder";
            this.colRouteOrder.Name = "colRouteOrder";
            this.colRouteOrder.OptionsColumn.AllowEdit = false;
            this.colRouteOrder.OptionsColumn.AllowFocus = false;
            this.colRouteOrder.OptionsColumn.ReadOnly = true;
            this.colRouteOrder.Visible = true;
            this.colRouteOrder.VisibleIndex = 65;
            this.colRouteOrder.Width = 81;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Start Latitude";
            this.gridColumn5.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.gridColumn5.FieldName = "StartLatitude";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 66;
            this.gridColumn5.Width = 87;
            // 
            // repositoryItemTextEditLatLong2
            // 
            this.repositoryItemTextEditLatLong2.AutoHeight = false;
            this.repositoryItemTextEditLatLong2.Mask.EditMask = "n8";
            this.repositoryItemTextEditLatLong2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditLatLong2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditLatLong2.Name = "repositoryItemTextEditLatLong2";
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Start Longitude";
            this.gridColumn6.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.gridColumn6.FieldName = "StartLongitude";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 67;
            this.gridColumn6.Width = 95;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Finish Latitude";
            this.gridColumn7.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.gridColumn7.FieldName = "FinishLatitude";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 68;
            this.gridColumn7.Width = 90;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Finish Longitude";
            this.gridColumn8.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.gridColumn8.FieldName = "FinishLongitude";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 69;
            this.gridColumn8.Width = 98;
            // 
            // colJobStatusDescription
            // 
            this.colJobStatusDescription.Caption = "Job Status";
            this.colJobStatusDescription.FieldName = "JobStatusDescription";
            this.colJobStatusDescription.Name = "colJobStatusDescription";
            this.colJobStatusDescription.OptionsColumn.AllowEdit = false;
            this.colJobStatusDescription.OptionsColumn.AllowFocus = false;
            this.colJobStatusDescription.OptionsColumn.ReadOnly = true;
            this.colJobStatusDescription.Visible = true;
            this.colJobStatusDescription.VisibleIndex = 4;
            this.colJobStatusDescription.Width = 129;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Site Name";
            this.gridColumn9.FieldName = "SiteName";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 13;
            this.gridColumn9.Width = 122;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Client Name";
            this.gridColumn10.FieldName = "ClientName";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 125;
            // 
            // colCreatedByStaffName1
            // 
            this.colCreatedByStaffName1.Caption = "Created By";
            this.colCreatedByStaffName1.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName1.Name = "colCreatedByStaffName1";
            this.colCreatedByStaffName1.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName1.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName1.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName1.Visible = true;
            this.colCreatedByStaffName1.VisibleIndex = 70;
            this.colCreatedByStaffName1.Width = 89;
            // 
            // colExpectedDurationUnitsDescriptor
            // 
            this.colExpectedDurationUnitsDescriptor.Caption = "Expected Duration Units";
            this.colExpectedDurationUnitsDescriptor.FieldName = "ExpectedDurationUnitsDescriptor";
            this.colExpectedDurationUnitsDescriptor.Name = "colExpectedDurationUnitsDescriptor";
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.AllowEdit = false;
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.AllowFocus = false;
            this.colExpectedDurationUnitsDescriptor.OptionsColumn.ReadOnly = true;
            this.colExpectedDurationUnitsDescriptor.Visible = true;
            this.colExpectedDurationUnitsDescriptor.VisibleIndex = 11;
            this.colExpectedDurationUnitsDescriptor.Width = 137;
            // 
            // colActualDurationUnitsDescription
            // 
            this.colActualDurationUnitsDescription.Caption = "Actual Duration Units";
            this.colActualDurationUnitsDescription.FieldName = "ActualDurationUnitsDescriptor";
            this.colActualDurationUnitsDescription.Name = "colActualDurationUnitsDescription";
            this.colActualDurationUnitsDescription.OptionsColumn.AllowEdit = false;
            this.colActualDurationUnitsDescription.OptionsColumn.AllowFocus = false;
            this.colActualDurationUnitsDescription.OptionsColumn.ReadOnly = true;
            this.colActualDurationUnitsDescription.Visible = true;
            this.colActualDurationUnitsDescription.VisibleIndex = 24;
            this.colActualDurationUnitsDescription.Width = 122;
            // 
            // colJobTypeID1
            // 
            this.colJobTypeID1.Caption = "Job Type ID";
            this.colJobTypeID1.FieldName = "JobTypeID";
            this.colJobTypeID1.Name = "colJobTypeID1";
            this.colJobTypeID1.OptionsColumn.AllowEdit = false;
            this.colJobTypeID1.OptionsColumn.AllowFocus = false;
            this.colJobTypeID1.OptionsColumn.ReadOnly = true;
            this.colJobTypeID1.Width = 79;
            // 
            // colJobSubTypeDescription
            // 
            this.colJobSubTypeDescription.Caption = "Job Sub-Type";
            this.colJobSubTypeDescription.FieldName = "JobSubTypeDescription";
            this.colJobSubTypeDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colJobSubTypeDescription.Name = "colJobSubTypeDescription";
            this.colJobSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeDescription.Visible = true;
            this.colJobSubTypeDescription.VisibleIndex = 1;
            this.colJobSubTypeDescription.Width = 128;
            // 
            // colJobTypeDescription1
            // 
            this.colJobTypeDescription1.Caption = "Job Type";
            this.colJobTypeDescription1.FieldName = "JobTypeDescription";
            this.colJobTypeDescription1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colJobTypeDescription1.Name = "colJobTypeDescription1";
            this.colJobTypeDescription1.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription1.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription1.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription1.Visible = true;
            this.colJobTypeDescription1.VisibleIndex = 0;
            this.colJobTypeDescription1.Width = 132;
            // 
            // colRecordType
            // 
            this.colRecordType.Caption = "Record Type";
            this.colRecordType.FieldName = "RecordType";
            this.colRecordType.Name = "colRecordType";
            this.colRecordType.OptionsColumn.AllowEdit = false;
            this.colRecordType.OptionsColumn.AllowFocus = false;
            this.colRecordType.OptionsColumn.ReadOnly = true;
            this.colRecordType.Visible = true;
            this.colRecordType.VisibleIndex = 14;
            this.colRecordType.Width = 120;
            // 
            // colSelected
            // 
            this.colSelected.Caption = "Selected";
            this.colSelected.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            this.colSelected.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Selected", "{0:0}")});
            this.colSelected.Width = 62;
            // 
            // colFullDescription1
            // 
            this.colFullDescription1.Caption = "Full Description";
            this.colFullDescription1.ColumnEdit = this.repositoryItemTextEditHTML1;
            this.colFullDescription1.FieldName = "FullDescription";
            this.colFullDescription1.Name = "colFullDescription1";
            this.colFullDescription1.OptionsColumn.AllowEdit = false;
            this.colFullDescription1.OptionsColumn.AllowFocus = false;
            this.colFullDescription1.OptionsColumn.ReadOnly = true;
            this.colFullDescription1.Width = 420;
            // 
            // repositoryItemTextEditHTML1
            // 
            this.repositoryItemTextEditHTML1.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML1.AutoHeight = false;
            this.repositoryItemTextEditHTML1.Name = "repositoryItemTextEditHTML1";
            // 
            // colTenderID
            // 
            this.colTenderID.Caption = "Tender ID";
            this.colTenderID.FieldName = "TenderID";
            this.colTenderID.Name = "colTenderID";
            this.colTenderID.OptionsColumn.AllowEdit = false;
            this.colTenderID.OptionsColumn.AllowFocus = false;
            this.colTenderID.OptionsColumn.ReadOnly = true;
            this.colTenderID.Width = 69;
            // 
            // colTenderJobID
            // 
            this.colTenderJobID.Caption = "Tender Job ID";
            this.colTenderJobID.FieldName = "TenderJobID";
            this.colTenderJobID.Name = "colTenderJobID";
            this.colTenderJobID.OptionsColumn.AllowEdit = false;
            this.colTenderJobID.OptionsColumn.AllowFocus = false;
            this.colTenderJobID.OptionsColumn.ReadOnly = true;
            this.colTenderJobID.Width = 89;
            // 
            // colMinimumDaysFromLastVisit
            // 
            this.colMinimumDaysFromLastVisit.Caption = "Min Days From Last Visit";
            this.colMinimumDaysFromLastVisit.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colMinimumDaysFromLastVisit.FieldName = "MinimumDaysFromLastVisit";
            this.colMinimumDaysFromLastVisit.Name = "colMinimumDaysFromLastVisit";
            this.colMinimumDaysFromLastVisit.OptionsColumn.AllowEdit = false;
            this.colMinimumDaysFromLastVisit.OptionsColumn.AllowFocus = false;
            this.colMinimumDaysFromLastVisit.OptionsColumn.ReadOnly = true;
            this.colMinimumDaysFromLastVisit.Visible = true;
            this.colMinimumDaysFromLastVisit.VisibleIndex = 72;
            this.colMinimumDaysFromLastVisit.Width = 136;
            // 
            // colMaximumDaysFromLastVisit
            // 
            this.colMaximumDaysFromLastVisit.Caption = "Max Days From Last Visit";
            this.colMaximumDaysFromLastVisit.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colMaximumDaysFromLastVisit.FieldName = "MaximumDaysFromLastVisit";
            this.colMaximumDaysFromLastVisit.Name = "colMaximumDaysFromLastVisit";
            this.colMaximumDaysFromLastVisit.OptionsColumn.AllowEdit = false;
            this.colMaximumDaysFromLastVisit.OptionsColumn.AllowFocus = false;
            this.colMaximumDaysFromLastVisit.OptionsColumn.ReadOnly = true;
            this.colMaximumDaysFromLastVisit.Visible = true;
            this.colMaximumDaysFromLastVisit.VisibleIndex = 73;
            this.colMaximumDaysFromLastVisit.Width = 140;
            // 
            // colDaysLeeway
            // 
            this.colDaysLeeway.Caption = "Days Leeway";
            this.colDaysLeeway.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colDaysLeeway.FieldName = "DaysLeeway";
            this.colDaysLeeway.Name = "colDaysLeeway";
            this.colDaysLeeway.OptionsColumn.AllowEdit = false;
            this.colDaysLeeway.OptionsColumn.AllowFocus = false;
            this.colDaysLeeway.OptionsColumn.ReadOnly = true;
            this.colDaysLeeway.Visible = true;
            this.colDaysLeeway.VisibleIndex = 74;
            this.colDaysLeeway.Width = 85;
            // 
            // colBillingCentreCodeID
            // 
            this.colBillingCentreCodeID.Caption = "Billing Centre Code ID";
            this.colBillingCentreCodeID.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID.Name = "colBillingCentreCodeID";
            this.colBillingCentreCodeID.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID.Width = 125;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Linked Documents";
            this.gridColumn12.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocumentsJobs;
            this.gridColumn12.FieldName = "LinkedDocumentCount";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 71;
            this.gridColumn12.Width = 107;
            // 
            // repositoryItemHyperLinkEditLinkedDocumentsJobs
            // 
            this.repositoryItemHyperLinkEditLinkedDocumentsJobs.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocumentsJobs.Name = "repositoryItemHyperLinkEditLinkedDocumentsJobs";
            this.repositoryItemHyperLinkEditLinkedDocumentsJobs.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocumentsJobs.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink);
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Contract Description";
            this.gridColumn13.FieldName = "ContractDescription";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 15;
            this.gridColumn13.Width = 150;
            // 
            // colCostTotalEquipmentVATRate
            // 
            this.colCostTotalEquipmentVATRate.Caption = "Equipment Cost VAT Rate";
            this.colCostTotalEquipmentVATRate.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colCostTotalEquipmentVATRate.FieldName = "CostTotalEquipmentVATRate";
            this.colCostTotalEquipmentVATRate.Name = "colCostTotalEquipmentVATRate";
            this.colCostTotalEquipmentVATRate.OptionsColumn.AllowEdit = false;
            this.colCostTotalEquipmentVATRate.OptionsColumn.AllowFocus = false;
            this.colCostTotalEquipmentVATRate.OptionsColumn.ReadOnly = true;
            this.colCostTotalEquipmentVATRate.Visible = true;
            this.colCostTotalEquipmentVATRate.VisibleIndex = 35;
            this.colCostTotalEquipmentVATRate.Width = 144;
            // 
            // repositoryItemTextEditPercentage2
            // 
            this.repositoryItemTextEditPercentage2.AutoHeight = false;
            this.repositoryItemTextEditPercentage2.Name = "repositoryItemTextEditPercentage2";
            // 
            // colCostTotalLabourVATRate
            // 
            this.colCostTotalLabourVATRate.Caption = "Labour Cost VAT Rate";
            this.colCostTotalLabourVATRate.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colCostTotalLabourVATRate.FieldName = "CostTotalLabourVATRate";
            this.colCostTotalLabourVATRate.Name = "colCostTotalLabourVATRate";
            this.colCostTotalLabourVATRate.OptionsColumn.AllowEdit = false;
            this.colCostTotalLabourVATRate.OptionsColumn.AllowFocus = false;
            this.colCostTotalLabourVATRate.OptionsColumn.ReadOnly = true;
            this.colCostTotalLabourVATRate.Visible = true;
            this.colCostTotalLabourVATRate.VisibleIndex = 43;
            this.colCostTotalLabourVATRate.Width = 127;
            // 
            // colCostTotalMaterialVATRate
            // 
            this.colCostTotalMaterialVATRate.Caption = "Material Cost VAT Rate";
            this.colCostTotalMaterialVATRate.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colCostTotalMaterialVATRate.FieldName = "CostTotalMaterialVATRate";
            this.colCostTotalMaterialVATRate.Name = "colCostTotalMaterialVATRate";
            this.colCostTotalMaterialVATRate.OptionsColumn.AllowEdit = false;
            this.colCostTotalMaterialVATRate.OptionsColumn.AllowFocus = false;
            this.colCostTotalMaterialVATRate.OptionsColumn.ReadOnly = true;
            this.colCostTotalMaterialVATRate.Visible = true;
            this.colCostTotalMaterialVATRate.VisibleIndex = 27;
            this.colCostTotalMaterialVATRate.Width = 132;
            // 
            // colSellTotalMaterialVATRate
            // 
            this.colSellTotalMaterialVATRate.Caption = "Material Sell VAT Rate";
            this.colSellTotalMaterialVATRate.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colSellTotalMaterialVATRate.FieldName = "SellTotalMaterialVATRate";
            this.colSellTotalMaterialVATRate.Name = "colSellTotalMaterialVATRate";
            this.colSellTotalMaterialVATRate.OptionsColumn.AllowEdit = false;
            this.colSellTotalMaterialVATRate.OptionsColumn.AllowFocus = false;
            this.colSellTotalMaterialVATRate.OptionsColumn.ReadOnly = true;
            this.colSellTotalMaterialVATRate.Visible = true;
            this.colSellTotalMaterialVATRate.VisibleIndex = 31;
            this.colSellTotalMaterialVATRate.Width = 126;
            // 
            // colSellTotalEquipmentVATRate
            // 
            this.colSellTotalEquipmentVATRate.Caption = "Equipment Sell VAT Rate";
            this.colSellTotalEquipmentVATRate.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colSellTotalEquipmentVATRate.FieldName = "SellTotalEquipmentVATRate";
            this.colSellTotalEquipmentVATRate.Name = "colSellTotalEquipmentVATRate";
            this.colSellTotalEquipmentVATRate.OptionsColumn.AllowEdit = false;
            this.colSellTotalEquipmentVATRate.OptionsColumn.AllowFocus = false;
            this.colSellTotalEquipmentVATRate.OptionsColumn.ReadOnly = true;
            this.colSellTotalEquipmentVATRate.Visible = true;
            this.colSellTotalEquipmentVATRate.VisibleIndex = 39;
            this.colSellTotalEquipmentVATRate.Width = 138;
            // 
            // colSellTotalLabourVATRate
            // 
            this.colSellTotalLabourVATRate.Caption = "Labour Sell VAT Rate";
            this.colSellTotalLabourVATRate.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colSellTotalLabourVATRate.FieldName = "SellTotalLabourVATRate";
            this.colSellTotalLabourVATRate.Name = "colSellTotalLabourVATRate";
            this.colSellTotalLabourVATRate.OptionsColumn.AllowEdit = false;
            this.colSellTotalLabourVATRate.OptionsColumn.AllowFocus = false;
            this.colSellTotalLabourVATRate.OptionsColumn.ReadOnly = true;
            this.colSellTotalLabourVATRate.Visible = true;
            this.colSellTotalLabourVATRate.VisibleIndex = 47;
            this.colSellTotalLabourVATRate.Width = 121;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Client Contract ID";
            this.gridColumn14.FieldName = "ClientContractID";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Width = 107;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            this.colClientID2.Width = 62;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Site Contract ID";
            this.gridColumn15.FieldName = "SiteContractID";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Width = 98;
            // 
            // colSiteID2
            // 
            this.colSiteID2.Caption = "Site ID";
            this.colSiteID2.FieldName = "SiteID";
            this.colSiteID2.Name = "colSiteID2";
            this.colSiteID2.OptionsColumn.AllowEdit = false;
            this.colSiteID2.OptionsColumn.AllowFocus = false;
            this.colSiteID2.OptionsColumn.ReadOnly = true;
            this.colSiteID2.Visible = true;
            this.colSiteID2.VisibleIndex = 18;
            this.colSiteID2.Width = 53;
            // 
            // colLocationX
            // 
            this.colLocationX.Caption = "Site Latitude";
            this.colLocationX.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.colLocationX.FieldName = "LocationX";
            this.colLocationX.Name = "colLocationX";
            this.colLocationX.OptionsColumn.AllowEdit = false;
            this.colLocationX.OptionsColumn.AllowFocus = false;
            this.colLocationX.OptionsColumn.ReadOnly = true;
            this.colLocationX.Width = 81;
            // 
            // colLocationY
            // 
            this.colLocationY.Caption = "Site Longitude";
            this.colLocationY.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.colLocationY.FieldName = "LocationY";
            this.colLocationY.Name = "colLocationY";
            this.colLocationY.OptionsColumn.AllowEdit = false;
            this.colLocationY.OptionsColumn.AllowFocus = false;
            this.colLocationY.OptionsColumn.ReadOnly = true;
            this.colLocationY.Width = 89;
            // 
            // colSitePostcode1
            // 
            this.colSitePostcode1.Caption = "Site Postcode";
            this.colSitePostcode1.FieldName = "SitePostcode";
            this.colSitePostcode1.Name = "colSitePostcode1";
            this.colSitePostcode1.OptionsColumn.AllowEdit = false;
            this.colSitePostcode1.OptionsColumn.AllowFocus = false;
            this.colSitePostcode1.OptionsColumn.ReadOnly = true;
            this.colSitePostcode1.Width = 86;
            // 
            // colLinkedToVisit
            // 
            this.colLinkedToVisit.Caption = "Linked To Visit";
            this.colLinkedToVisit.ColumnEdit = this.repositoryItemTextEditHTML1;
            this.colLinkedToVisit.FieldName = "LinkedToVisit";
            this.colLinkedToVisit.Name = "colLinkedToVisit";
            this.colLinkedToVisit.OptionsColumn.AllowEdit = false;
            this.colLinkedToVisit.OptionsColumn.AllowFocus = false;
            this.colLinkedToVisit.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.List;
            this.colLinkedToVisit.Width = 336;
            // 
            // colClientAndContract
            // 
            this.colClientAndContract.Caption = "Client \\ Contract";
            this.colClientAndContract.ColumnEdit = this.repositoryItemTextEditHTML1;
            this.colClientAndContract.FieldName = "ClientAndContract";
            this.colClientAndContract.Name = "colClientAndContract";
            this.colClientAndContract.OptionsColumn.AllowEdit = false;
            this.colClientAndContract.OptionsColumn.AllowFocus = false;
            this.colClientAndContract.OptionsColumn.ReadOnly = true;
            this.colClientAndContract.Visible = true;
            this.colClientAndContract.VisibleIndex = 79;
            this.colClientAndContract.Width = 299;
            // 
            // colReworkOriginalJobID
            // 
            this.colReworkOriginalJobID.Caption = "Reworked Original Job ID";
            this.colReworkOriginalJobID.FieldName = "ReworkOriginalJobID";
            this.colReworkOriginalJobID.Name = "colReworkOriginalJobID";
            this.colReworkOriginalJobID.OptionsColumn.AllowEdit = false;
            this.colReworkOriginalJobID.OptionsColumn.AllowFocus = false;
            this.colReworkOriginalJobID.OptionsColumn.ReadOnly = true;
            this.colReworkOriginalJobID.Width = 142;
            // 
            // colRework1
            // 
            this.colRework1.Caption = "Rework";
            this.colRework1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colRework1.FieldName = "Rework";
            this.colRework1.Name = "colRework1";
            this.colRework1.OptionsColumn.AllowEdit = false;
            this.colRework1.OptionsColumn.AllowFocus = false;
            this.colRework1.OptionsColumn.ReadOnly = true;
            this.colRework1.Visible = true;
            this.colRework1.VisibleIndex = 75;
            this.colRework1.Width = 57;
            // 
            // colCancelledReason
            // 
            this.colCancelledReason.Caption = "Cancelled Reason";
            this.colCancelledReason.FieldName = "CancelledReason";
            this.colCancelledReason.Name = "colCancelledReason";
            this.colCancelledReason.OptionsColumn.AllowEdit = false;
            this.colCancelledReason.OptionsColumn.AllowFocus = false;
            this.colCancelledReason.OptionsColumn.ReadOnly = true;
            this.colCancelledReason.Visible = true;
            this.colCancelledReason.VisibleIndex = 76;
            this.colCancelledReason.Width = 106;
            // 
            // colCancelledReasonID
            // 
            this.colCancelledReasonID.Caption = "Cancelled Reason ID";
            this.colCancelledReasonID.FieldName = "CancelledReasonID";
            this.colCancelledReasonID.Name = "colCancelledReasonID";
            this.colCancelledReasonID.OptionsColumn.AllowEdit = false;
            this.colCancelledReasonID.OptionsColumn.AllowFocus = false;
            this.colCancelledReasonID.OptionsColumn.ReadOnly = true;
            this.colCancelledReasonID.Width = 120;
            // 
            // colDaysUntilDue
            // 
            this.colDaysUntilDue.Caption = "Due In";
            this.colDaysUntilDue.ColumnEdit = this.repositoryItemTextEditIntegerDays;
            this.colDaysUntilDue.FieldName = "DaysUntilDue";
            this.colDaysUntilDue.Name = "colDaysUntilDue";
            this.colDaysUntilDue.OptionsColumn.AllowEdit = false;
            this.colDaysUntilDue.OptionsColumn.AllowFocus = false;
            this.colDaysUntilDue.OptionsColumn.ReadOnly = true;
            this.colDaysUntilDue.Visible = true;
            this.colDaysUntilDue.VisibleIndex = 7;
            this.colDaysUntilDue.Width = 60;
            // 
            // repositoryItemTextEditIntegerDays
            // 
            this.repositoryItemTextEditIntegerDays.AutoHeight = false;
            this.repositoryItemTextEditIntegerDays.Mask.EditMask = "#####0 Days";
            this.repositoryItemTextEditIntegerDays.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditIntegerDays.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditIntegerDays.Name = "repositoryItemTextEditIntegerDays";
            // 
            // colManuallyCompleted
            // 
            this.colManuallyCompleted.Caption = "Manually Completed";
            this.colManuallyCompleted.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colManuallyCompleted.FieldName = "ManuallyCompleted";
            this.colManuallyCompleted.Name = "colManuallyCompleted";
            this.colManuallyCompleted.OptionsColumn.AllowEdit = false;
            this.colManuallyCompleted.OptionsColumn.AllowFocus = false;
            this.colManuallyCompleted.OptionsColumn.ReadOnly = true;
            this.colManuallyCompleted.Visible = true;
            this.colManuallyCompleted.VisibleIndex = 79;
            this.colManuallyCompleted.Width = 117;
            // 
            // colLinkedPictureCount1
            // 
            this.colLinkedPictureCount1.Caption = "Linked Pictures";
            this.colLinkedPictureCount1.ColumnEdit = this.repositoryItemHyperLinkEditPictures;
            this.colLinkedPictureCount1.FieldName = "LinkedPictureCount";
            this.colLinkedPictureCount1.Name = "colLinkedPictureCount1";
            this.colLinkedPictureCount1.OptionsColumn.ReadOnly = true;
            this.colLinkedPictureCount1.Width = 90;
            // 
            // repositoryItemHyperLinkEditPictures
            // 
            this.repositoryItemHyperLinkEditPictures.AutoHeight = false;
            this.repositoryItemHyperLinkEditPictures.Name = "repositoryItemHyperLinkEditPictures";
            this.repositoryItemHyperLinkEditPictures.SingleClick = true;
            this.repositoryItemHyperLinkEditPictures.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditPictures_OpenLink);
            // 
            // colImagesFolderOM1
            // 
            this.colImagesFolderOM1.Caption = "Images Folder";
            this.colImagesFolderOM1.FieldName = "ImagesFolderOM";
            this.colImagesFolderOM1.Name = "colImagesFolderOM1";
            this.colImagesFolderOM1.OptionsColumn.AllowEdit = false;
            this.colImagesFolderOM1.OptionsColumn.AllowFocus = false;
            this.colImagesFolderOM1.OptionsColumn.ReadOnly = true;
            this.colImagesFolderOM1.Width = 87;
            // 
            // colFriendlyVisitNumber1
            // 
            this.colFriendlyVisitNumber1.Caption = "Friendly Visit #";
            this.colFriendlyVisitNumber1.FieldName = "FriendlyVisitNumber";
            this.colFriendlyVisitNumber1.Name = "colFriendlyVisitNumber1";
            this.colFriendlyVisitNumber1.OptionsColumn.AllowEdit = false;
            this.colFriendlyVisitNumber1.OptionsColumn.AllowFocus = false;
            this.colFriendlyVisitNumber1.OptionsColumn.ReadOnly = true;
            this.colFriendlyVisitNumber1.Visible = true;
            this.colFriendlyVisitNumber1.VisibleIndex = 2;
            this.colFriendlyVisitNumber1.Width = 90;
            // 
            // colClientPO1
            // 
            this.colClientPO1.Caption = "Client PO #";
            this.colClientPO1.FieldName = "ClientPO";
            this.colClientPO1.Name = "colClientPO1";
            this.colClientPO1.OptionsColumn.AllowEdit = false;
            this.colClientPO1.OptionsColumn.AllowFocus = false;
            this.colClientPO1.OptionsColumn.ReadOnly = true;
            this.colClientPO1.Visible = true;
            this.colClientPO1.VisibleIndex = 56;
            this.colClientPO1.Width = 94;
            // 
            // colVisitType1
            // 
            this.colVisitType1.Caption = "Visit Type";
            this.colVisitType1.FieldName = "VisitType";
            this.colVisitType1.Name = "colVisitType1";
            this.colVisitType1.OptionsColumn.AllowEdit = false;
            this.colVisitType1.OptionsColumn.AllowFocus = false;
            this.colVisitType1.OptionsColumn.ReadOnly = true;
            this.colVisitType1.Visible = true;
            this.colVisitType1.VisibleIndex = 3;
            this.colVisitType1.Width = 106;
            // 
            // colVisitTypeID1
            // 
            this.colVisitTypeID1.Caption = "Visit Type ID";
            this.colVisitTypeID1.FieldName = "VisitTypeID";
            this.colVisitTypeID1.Name = "colVisitTypeID1";
            this.colVisitTypeID1.OptionsColumn.AllowEdit = false;
            this.colVisitTypeID1.OptionsColumn.AllowFocus = false;
            this.colVisitTypeID1.OptionsColumn.ReadOnly = true;
            this.colVisitTypeID1.Width = 79;
            // 
            // colSiteCode1
            // 
            this.colSiteCode1.Caption = "Site Code";
            this.colSiteCode1.FieldName = "SiteCode";
            this.colSiteCode1.Name = "colSiteCode1";
            this.colSiteCode1.OptionsColumn.AllowEdit = false;
            this.colSiteCode1.OptionsColumn.AllowFocus = false;
            this.colSiteCode1.OptionsColumn.ReadOnly = true;
            this.colSiteCode1.Visible = true;
            this.colSiteCode1.VisibleIndex = 19;
            // 
            // colVisitCostCalculationLevelID
            // 
            this.colVisitCostCalculationLevelID.Caption = "Visit Cost Calculation Method ID";
            this.colVisitCostCalculationLevelID.FieldName = "VisitCostCalculationLevelID";
            this.colVisitCostCalculationLevelID.Name = "colVisitCostCalculationLevelID";
            this.colVisitCostCalculationLevelID.OptionsColumn.AllowEdit = false;
            this.colVisitCostCalculationLevelID.OptionsColumn.AllowFocus = false;
            this.colVisitCostCalculationLevelID.OptionsColumn.ReadOnly = true;
            this.colVisitCostCalculationLevelID.Width = 171;
            // 
            // colVisitSellCalculationLevelID
            // 
            this.colVisitSellCalculationLevelID.Caption = "Visit Sell Calculation Method ID";
            this.colVisitSellCalculationLevelID.FieldName = "VisitSellCalculationLevelID";
            this.colVisitSellCalculationLevelID.Name = "colVisitSellCalculationLevelID";
            this.colVisitSellCalculationLevelID.OptionsColumn.AllowEdit = false;
            this.colVisitSellCalculationLevelID.OptionsColumn.AllowFocus = false;
            this.colVisitSellCalculationLevelID.OptionsColumn.ReadOnly = true;
            this.colVisitSellCalculationLevelID.Width = 165;
            // 
            // colVisitCostCalculationLevelDescription
            // 
            this.colVisitCostCalculationLevelDescription.Caption = "Visit Cost Calculation Method";
            this.colVisitCostCalculationLevelDescription.FieldName = "VisitCostCalculationLevelDescription";
            this.colVisitCostCalculationLevelDescription.Name = "colVisitCostCalculationLevelDescription";
            this.colVisitCostCalculationLevelDescription.OptionsColumn.AllowEdit = false;
            this.colVisitCostCalculationLevelDescription.OptionsColumn.AllowFocus = false;
            this.colVisitCostCalculationLevelDescription.OptionsColumn.ReadOnly = true;
            this.colVisitCostCalculationLevelDescription.Width = 157;
            // 
            // colVisitSellCalculationLevelDescription
            // 
            this.colVisitSellCalculationLevelDescription.Caption = "Visit Sell Calculation Method";
            this.colVisitSellCalculationLevelDescription.FieldName = "VisitSellCalculationLevelDescription";
            this.colVisitSellCalculationLevelDescription.Name = "colVisitSellCalculationLevelDescription";
            this.colVisitSellCalculationLevelDescription.OptionsColumn.AllowEdit = false;
            this.colVisitSellCalculationLevelDescription.OptionsColumn.AllowFocus = false;
            this.colVisitSellCalculationLevelDescription.OptionsColumn.ReadOnly = true;
            this.colVisitSellCalculationLevelDescription.Width = 151;
            // 
            // colDisplayOrder
            // 
            this.colDisplayOrder.Caption = "App Display Order";
            this.colDisplayOrder.FieldName = "DisplayOrder";
            this.colDisplayOrder.Name = "colDisplayOrder";
            this.colDisplayOrder.OptionsColumn.AllowEdit = false;
            this.colDisplayOrder.OptionsColumn.AllowFocus = false;
            this.colDisplayOrder.OptionsColumn.ReadOnly = true;
            this.colDisplayOrder.Visible = true;
            this.colDisplayOrder.VisibleIndex = 17;
            this.colDisplayOrder.Width = 119;
            // 
            // colMandatory
            // 
            this.colMandatory.Caption = "Mandatory";
            this.colMandatory.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colMandatory.FieldName = "Mandatory";
            this.colMandatory.Name = "colMandatory";
            this.colMandatory.OptionsColumn.AllowEdit = false;
            this.colMandatory.OptionsColumn.AllowFocus = false;
            this.colMandatory.OptionsColumn.ReadOnly = true;
            this.colMandatory.Visible = true;
            this.colMandatory.VisibleIndex = 16;
            // 
            // colSuspendedReason
            // 
            this.colSuspendedReason.Caption = "Suspended Reason";
            this.colSuspendedReason.FieldName = "SuspendedReason";
            this.colSuspendedReason.Name = "colSuspendedReason";
            this.colSuspendedReason.OptionsColumn.AllowEdit = false;
            this.colSuspendedReason.OptionsColumn.AllowFocus = false;
            this.colSuspendedReason.OptionsColumn.ReadOnly = true;
            this.colSuspendedReason.Visible = true;
            this.colSuspendedReason.VisibleIndex = 77;
            this.colSuspendedReason.Width = 111;
            // 
            // colSuspendedRemarks
            // 
            this.colSuspendedRemarks.Caption = "Suspended Remarks";
            this.colSuspendedRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colSuspendedRemarks.FieldName = "SuspendedRemarks";
            this.colSuspendedRemarks.Name = "colSuspendedRemarks";
            this.colSuspendedRemarks.OptionsColumn.ReadOnly = true;
            this.colSuspendedRemarks.Visible = true;
            this.colSuspendedRemarks.VisibleIndex = 78;
            this.colSuspendedRemarks.Width = 116;
            // 
            // colLabourName2
            // 
            this.colLabourName2.Caption = "Labour Name";
            this.colLabourName2.FieldName = "LabourName";
            this.colLabourName2.Name = "colLabourName2";
            this.colLabourName2.OptionsColumn.AllowEdit = false;
            this.colLabourName2.OptionsColumn.AllowFocus = false;
            this.colLabourName2.OptionsColumn.ReadOnly = true;
            this.colLabourName2.Visible = true;
            this.colLabourName2.VisibleIndex = 5;
            this.colLabourName2.Width = 146;
            // 
            // colLinkedToPersonType1
            // 
            this.colLinkedToPersonType1.Caption = "Labour Type";
            this.colLinkedToPersonType1.FieldName = "LinkedToPersonType";
            this.colLinkedToPersonType1.Name = "colLinkedToPersonType1";
            this.colLinkedToPersonType1.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonType1.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonType1.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonType1.Visible = true;
            this.colLinkedToPersonType1.VisibleIndex = 6;
            this.colLinkedToPersonType1.Width = 79;
            // 
            // colLinkedToPersonTypeID1
            // 
            this.colLinkedToPersonTypeID1.Caption = "Labour Type ID";
            this.colLinkedToPersonTypeID1.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID1.Name = "colLinkedToPersonTypeID1";
            this.colLinkedToPersonTypeID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID1.Width = 93;
            // 
            // colLabourUsedID
            // 
            this.colLabourUsedID.Caption = "Labour Used ID";
            this.colLabourUsedID.FieldName = "LabourUsedID";
            this.colLabourUsedID.Name = "colLabourUsedID";
            this.colLabourUsedID.OptionsColumn.AllowEdit = false;
            this.colLabourUsedID.OptionsColumn.AllowFocus = false;
            this.colLabourUsedID.OptionsColumn.ReadOnly = true;
            this.colLabourUsedID.Width = 93;
            // 
            // repositoryItemHyperLinkEditViewJob
            // 
            this.repositoryItemHyperLinkEditViewJob.AutoHeight = false;
            this.repositoryItemHyperLinkEditViewJob.Name = "repositoryItemHyperLinkEditViewJob";
            this.repositoryItemHyperLinkEditViewJob.SingleClick = true;
            this.repositoryItemHyperLinkEditViewJob.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditViewJob_OpenLink);
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(2, 43);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel1.Controls.Add(this.splitContainerControl1);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Teams For Self-Billing";
            this.splitContainerControl2.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel2.Controls.Add(this.popupContainerControlLabourFilter);
            this.splitContainerControl2.Panel2.Controls.Add(this.popupContainerControlWarningFilter);
            this.splitContainerControl2.Panel2.Controls.Add(this.popupContainerControlVisitTypeFilter);
            this.splitContainerControl2.Panel2.Controls.Add(this.popupContainerControlVisitCategoryFilter);
            this.splitContainerControl2.Panel2.Controls.Add(this.gridControlVisit);
            this.splitContainerControl2.Panel2.ShowCaption = true;
            this.splitContainerControl2.Panel2.Text = "Visits For Self-Billing";
            this.splitContainerControl2.Size = new System.Drawing.Size(1354, 659);
            this.splitContainerControl2.SplitterPosition = 266;
            this.splitContainerControl2.TabIndex = 5;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // popupContainerControlLabourFilter
            // 
            this.popupContainerControlLabourFilter.Controls.Add(this.btnLabourFilter);
            this.popupContainerControlLabourFilter.Controls.Add(this.gridControl12);
            this.popupContainerControlLabourFilter.Location = new System.Drawing.Point(798, 82);
            this.popupContainerControlLabourFilter.Name = "popupContainerControlLabourFilter";
            this.popupContainerControlLabourFilter.Size = new System.Drawing.Size(171, 126);
            this.popupContainerControlLabourFilter.TabIndex = 20;
            // 
            // btnLabourFilter
            // 
            this.btnLabourFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLabourFilter.Location = new System.Drawing.Point(3, 103);
            this.btnLabourFilter.Name = "btnLabourFilter";
            this.btnLabourFilter.Size = new System.Drawing.Size(50, 20);
            this.btnLabourFilter.TabIndex = 12;
            this.btnLabourFilter.Text = "OK";
            this.btnLabourFilter.Click += new System.EventHandler(this.btnLabourFilterOK_Click);
            // 
            // gridControl12
            // 
            this.gridControl12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl12.DataSource = this.sp06028OMLabourFilterBindingSource;
            this.gridControl12.Location = new System.Drawing.Point(3, 3);
            this.gridControl12.MainView = this.gridView12;
            this.gridControl12.MenuManager = this.barManager1;
            this.gridControl12.Name = "gridControl12";
            this.gridControl12.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit17});
            this.gridControl12.Size = new System.Drawing.Size(165, 98);
            this.gridControl12.TabIndex = 4;
            this.gridControl12.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView12});
            // 
            // sp06028OMLabourFilterBindingSource
            // 
            this.sp06028OMLabourFilterBindingSource.DataMember = "sp06028_OM_Labour_Filter";
            this.sp06028OMLabourFilterBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // dataSet_GC_Summer_Core
            // 
            this.dataSet_GC_Summer_Core.DataSetName = "DataSet_GC_Summer_Core";
            this.dataSet_GC_Summer_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView12
            // 
            this.gridView12.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID4,
            this.colName,
            this.colActive,
            this.colTypeID,
            this.colTypeDescription});
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colActive;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView12.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView12.GridControl = this.gridControl12;
            this.gridView12.GroupCount = 1;
            this.gridView12.Name = "gridView12";
            this.gridView12.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView12.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView12.OptionsFind.AlwaysVisible = true;
            this.gridView12.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView12.OptionsLayout.StoreAppearance = true;
            this.gridView12.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView12.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView12.OptionsView.ColumnAutoWidth = false;
            this.gridView12.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView12.OptionsView.ShowGroupPanel = false;
            this.gridView12.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView12.OptionsView.ShowIndicator = false;
            this.gridView12.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID4
            // 
            this.colID4.Caption = "ID";
            this.colID4.FieldName = "ID";
            this.colID4.Name = "colID4";
            this.colID4.OptionsColumn.AllowEdit = false;
            this.colID4.OptionsColumn.AllowFocus = false;
            this.colID4.OptionsColumn.ReadOnly = true;
            this.colID4.Visible = true;
            this.colID4.VisibleIndex = 2;
            this.colID4.Width = 57;
            // 
            // colName
            // 
            this.colName.Caption = "Name";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.AllowFocus = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 214;
            // 
            // colTypeID
            // 
            this.colTypeID.Caption = "Type ID";
            this.colTypeID.FieldName = "TypeID";
            this.colTypeID.Name = "colTypeID";
            this.colTypeID.OptionsColumn.AllowEdit = false;
            this.colTypeID.OptionsColumn.AllowFocus = false;
            this.colTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colTypeDescription
            // 
            this.colTypeDescription.Caption = "Labour Type";
            this.colTypeDescription.FieldName = "TypeDescription";
            this.colTypeDescription.Name = "colTypeDescription";
            this.colTypeDescription.OptionsColumn.AllowEdit = false;
            this.colTypeDescription.OptionsColumn.AllowFocus = false;
            this.colTypeDescription.OptionsColumn.ReadOnly = true;
            this.colTypeDescription.Visible = true;
            this.colTypeDescription.VisibleIndex = 2;
            this.colTypeDescription.Width = 153;
            // 
            // popupContainerControlWarningFilter
            // 
            this.popupContainerControlWarningFilter.Controls.Add(this.btnWarningFilter);
            this.popupContainerControlWarningFilter.Controls.Add(this.gridControl1);
            this.popupContainerControlWarningFilter.Location = new System.Drawing.Point(539, 85);
            this.popupContainerControlWarningFilter.Name = "popupContainerControlWarningFilter";
            this.popupContainerControlWarningFilter.Size = new System.Drawing.Size(248, 126);
            this.popupContainerControlWarningFilter.TabIndex = 21;
            // 
            // btnWarningFilter
            // 
            this.btnWarningFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnWarningFilter.Location = new System.Drawing.Point(3, 103);
            this.btnWarningFilter.Name = "btnWarningFilter";
            this.btnWarningFilter.Size = new System.Drawing.Size(50, 20);
            this.btnWarningFilter.TabIndex = 12;
            this.btnWarningFilter.Text = "OK";
            this.btnWarningFilter.Click += new System.EventHandler(this.btnWarningFilterOK_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp06451OMSelfBillingBillingWarningsFilterBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(3, 3);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit4});
            this.gridControl1.Size = new System.Drawing.Size(242, 98);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp06451OMSelfBillingBillingWarningsFilterBindingSource
            // 
            this.sp06451OMSelfBillingBillingWarningsFilterBindingSource.DataMember = "sp06451_OM_Self_Billing_Billing_Warnings_Filter";
            this.sp06451OMSelfBillingBillingWarningsFilterBindingSource.DataSource = this.dataSet_OM_Billing;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colID,
            this.colOrder});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Warning Type";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 201;
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // popupContainerControlVisitTypeFilter
            // 
            this.popupContainerControlVisitTypeFilter.Controls.Add(this.gridControl2);
            this.popupContainerControlVisitTypeFilter.Controls.Add(this.btnVisitTypeFilterOK);
            this.popupContainerControlVisitTypeFilter.Location = new System.Drawing.Point(284, 87);
            this.popupContainerControlVisitTypeFilter.Name = "popupContainerControlVisitTypeFilter";
            this.popupContainerControlVisitTypeFilter.Size = new System.Drawing.Size(239, 126);
            this.popupContainerControlVisitTypeFilter.TabIndex = 23;
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp06308OMVisitTypesBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(3, 3);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(233, 99);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp06308OMVisitTypesBindingSource
            // 
            this.sp06308OMVisitTypesBindingSource.DataMember = "sp06308_OM_Visit_Types";
            this.sp06308OMVisitTypesBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn224,
            this.gridColumn226,
            this.gridColumn227,
            this.colActive1});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsView.AutoCalcPreviewLineCount = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn227, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn224
            // 
            this.gridColumn224.Caption = "ID";
            this.gridColumn224.FieldName = "ID";
            this.gridColumn224.Name = "gridColumn224";
            this.gridColumn224.OptionsColumn.AllowEdit = false;
            this.gridColumn224.OptionsColumn.AllowFocus = false;
            this.gridColumn224.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn226
            // 
            this.gridColumn226.Caption = "Visit Type";
            this.gridColumn226.FieldName = "Description";
            this.gridColumn226.Name = "gridColumn226";
            this.gridColumn226.OptionsColumn.AllowEdit = false;
            this.gridColumn226.OptionsColumn.AllowFocus = false;
            this.gridColumn226.OptionsColumn.ReadOnly = true;
            this.gridColumn226.Visible = true;
            this.gridColumn226.VisibleIndex = 0;
            this.gridColumn226.Width = 143;
            // 
            // gridColumn227
            // 
            this.gridColumn227.Caption = "Order";
            this.gridColumn227.FieldName = "Order";
            this.gridColumn227.Name = "gridColumn227";
            this.gridColumn227.OptionsColumn.AllowEdit = false;
            this.gridColumn227.OptionsColumn.AllowFocus = false;
            this.gridColumn227.OptionsColumn.ReadOnly = true;
            this.gridColumn227.Width = 63;
            // 
            // colActive1
            // 
            this.colActive1.Caption = "Active";
            this.colActive1.ColumnEdit = this.repositoryItemCheckEdit21;
            this.colActive1.FieldName = "Active";
            this.colActive1.Name = "colActive1";
            this.colActive1.OptionsColumn.AllowEdit = false;
            this.colActive1.OptionsColumn.AllowFocus = false;
            this.colActive1.OptionsColumn.ReadOnly = true;
            this.colActive1.Visible = true;
            this.colActive1.VisibleIndex = 1;
            this.colActive1.Width = 48;
            // 
            // btnVisitTypeFilterOK
            // 
            this.btnVisitTypeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnVisitTypeFilterOK.Location = new System.Drawing.Point(3, 104);
            this.btnVisitTypeFilterOK.Name = "btnVisitTypeFilterOK";
            this.btnVisitTypeFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnVisitTypeFilterOK.TabIndex = 0;
            this.btnVisitTypeFilterOK.Text = "OK";
            this.btnVisitTypeFilterOK.Click += new System.EventHandler(this.btnVisitTypeFilterOK_Click);
            // 
            // popupContainerControlVisitCategoryFilter
            // 
            this.popupContainerControlVisitCategoryFilter.Controls.Add(this.gridControl4);
            this.popupContainerControlVisitCategoryFilter.Controls.Add(this.btnVisitCategoryFilter);
            this.popupContainerControlVisitCategoryFilter.Location = new System.Drawing.Point(34, 85);
            this.popupContainerControlVisitCategoryFilter.Name = "popupContainerControlVisitCategoryFilter";
            this.popupContainerControlVisitCategoryFilter.Size = new System.Drawing.Size(239, 126);
            this.popupContainerControlVisitCategoryFilter.TabIndex = 24;
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.sp06528OMVisitCategoriesFilterBindingSource;
            this.gridControl4.Location = new System.Drawing.Point(3, 3);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit9});
            this.gridControl4.Size = new System.Drawing.Size(233, 99);
            this.gridControl4.TabIndex = 1;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp06528OMVisitCategoriesFilterBindingSource
            // 
            this.sp06528OMVisitCategoriesFilterBindingSource.DataMember = "sp06528_OM_Visit_Categories_Filter";
            this.sp06528OMVisitCategoriesFilterBindingSource.DataSource = this.dataSet_OM_Core;
            // 
            // dataSet_OM_Core
            // 
            this.dataSet_OM_Core.DataSetName = "DataSet_OM_Core";
            this.dataSet_OM_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn11,
            this.gridColumn17,
            this.gridColumn21,
            this.gridColumn22});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsView.AutoCalcPreviewLineCount = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn21, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "ID";
            this.gridColumn11.FieldName = "ID";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Visit Category";
            this.gridColumn17.FieldName = "Description";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 0;
            this.gridColumn17.Width = 143;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Order";
            this.gridColumn21.FieldName = "Order";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Width = 63;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Active";
            this.gridColumn22.ColumnEdit = this.repositoryItemCheckEdit9;
            this.gridColumn22.FieldName = "Active";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 1;
            this.gridColumn22.Width = 48;
            // 
            // repositoryItemCheckEdit9
            // 
            this.repositoryItemCheckEdit9.AutoHeight = false;
            this.repositoryItemCheckEdit9.Caption = "Check";
            this.repositoryItemCheckEdit9.Name = "repositoryItemCheckEdit9";
            this.repositoryItemCheckEdit9.ValueChecked = 1;
            this.repositoryItemCheckEdit9.ValueUnchecked = 0;
            // 
            // btnVisitCategoryFilter
            // 
            this.btnVisitCategoryFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnVisitCategoryFilter.Location = new System.Drawing.Point(3, 104);
            this.btnVisitCategoryFilter.Name = "btnVisitCategoryFilter";
            this.btnVisitCategoryFilter.Size = new System.Drawing.Size(50, 20);
            this.btnVisitCategoryFilter.TabIndex = 0;
            this.btnVisitCategoryFilter.Text = "OK";
            this.btnVisitCategoryFilter.Click += new System.EventHandler(this.btnVisitCategoryFilterOK_Click);
            // 
            // dataSet_Common_Functionality
            // 
            this.dataSet_Common_Functionality.DataSetName = "DataSet_Common_Functionality";
            this.dataSet_Common_Functionality.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(1356, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp06404OMVisitSystemStatusFilterBindingSource
            // 
            this.sp06404OMVisitSystemStatusFilterBindingSource.DataMember = "sp06404_OM_Visit_System_Status_Filter";
            this.sp06404OMVisitSystemStatusFilterBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // sp06357OMVisitManagerLinkedVisitLabourBindingSource
            // 
            this.sp06357OMVisitManagerLinkedVisitLabourBindingSource.DataMember = "sp06357_OM_Visit_Manager_Linked_Visit_Labour";
            this.sp06357OMVisitManagerLinkedVisitLabourBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // bar1
            // 
            this.bar1.BarName = "GrittingCalloutToolbar";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(519, 261);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiFilterSelected, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSelectedCount0, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSelectedCount1),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSelectedCount2),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRecalculateTotal, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.barEditItemInvoicingParameters, "", true, true, true, 304),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiInvoice)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Self Billing Toolbar 1";
            // 
            // bsiFilterSelected
            // 
            this.bsiFilterSelected.Caption = "Filter Selected";
            this.bsiFilterSelected.Id = 122;
            this.bsiFilterSelected.ImageOptions.ImageIndex = 0;
            this.bsiFilterSelected.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterTeamsSelected),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterVisitsSelected),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterJobsSelected)});
            this.bsiFilterSelected.Name = "bsiFilterSelected";
            this.bsiFilterSelected.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            // 
            // bciFilterTeamsSelected
            // 
            this.bciFilterTeamsSelected.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bciFilterTeamsSelected.Caption = "Filter Selected <b>Teams</b>";
            this.bciFilterTeamsSelected.Id = 138;
            this.bciFilterTeamsSelected.ImageOptions.ImageIndex = 0;
            this.bciFilterTeamsSelected.Name = "bciFilterTeamsSelected";
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Filter Selected Teams - Information";
            toolTipItem1.LeftIndent = 6;
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bciFilterTeamsSelected.SuperTip = superToolTip1;
            this.bciFilterTeamsSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterTeamsSelected_CheckedChanged);
            // 
            // bciFilterVisitsSelected
            // 
            this.bciFilterVisitsSelected.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bciFilterVisitsSelected.Caption = "Filter Selected <b>Visits</b>";
            this.bciFilterVisitsSelected.Id = 124;
            this.bciFilterVisitsSelected.ImageOptions.ImageIndex = 0;
            this.bciFilterVisitsSelected.Name = "bciFilterVisitsSelected";
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Filter Selected Visits - Information";
            toolTipItem2.LeftIndent = 6;
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bciFilterVisitsSelected.SuperTip = superToolTip2;
            this.bciFilterVisitsSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterVisitsSelected_CheckedChanged);
            // 
            // bciFilterJobsSelected
            // 
            this.bciFilterJobsSelected.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bciFilterJobsSelected.Caption = "Filter Selected <b>Jobs</b>";
            this.bciFilterJobsSelected.Id = 113;
            this.bciFilterJobsSelected.ImageOptions.ImageIndex = 0;
            this.bciFilterJobsSelected.Name = "bciFilterJobsSelected";
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Filter Selected Jobs - Information";
            toolTipItem3.LeftIndent = 6;
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bciFilterJobsSelected.SuperTip = superToolTip3;
            this.bciFilterJobsSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterJobsSelected_CheckedChanged);
            // 
            // bsiSelectedCount0
            // 
            this.bsiSelectedCount0.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSelectedCount0.Caption = "0 Teams Selected";
            this.bsiSelectedCount0.Id = 137;
            this.bsiSelectedCount0.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_16x16;
            this.bsiSelectedCount0.Name = "bsiSelectedCount0";
            this.bsiSelectedCount0.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiSelectedCount0.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bsiSelectedCount1
            // 
            this.bsiSelectedCount1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSelectedCount1.Caption = "0 Visits Selected";
            this.bsiSelectedCount1.Id = 116;
            this.bsiSelectedCount1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiSelectedCount1.ImageOptions.Image")));
            this.bsiSelectedCount1.ItemAppearance.Disabled.Options.UseTextOptions = true;
            this.bsiSelectedCount1.ItemAppearance.Disabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount1.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.bsiSelectedCount1.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount1.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.bsiSelectedCount1.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount1.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.bsiSelectedCount1.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount1.Name = "bsiSelectedCount1";
            this.bsiSelectedCount1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiSelectedCount1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bsiSelectedCount2
            // 
            this.bsiSelectedCount2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSelectedCount2.Caption = "0 Requirements Selected";
            this.bsiSelectedCount2.Id = 136;
            this.bsiSelectedCount2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiSelectedCount2.ImageOptions.Image")));
            this.bsiSelectedCount2.ItemAppearance.Disabled.Options.UseTextOptions = true;
            this.bsiSelectedCount2.ItemAppearance.Disabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount2.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.bsiSelectedCount2.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount2.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.bsiSelectedCount2.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount2.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.bsiSelectedCount2.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount2.Name = "bsiSelectedCount2";
            this.bsiSelectedCount2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiSelectedCount2.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 58;
            this.bbiRefresh.ImageOptions.ImageIndex = 1;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bbiRecalculateTotal
            // 
            this.bbiRecalculateTotal.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRecalculateTotal.Caption = "Re-Calculate Costs";
            this.bbiRecalculateTotal.Enabled = false;
            this.bbiRecalculateTotal.Id = 139;
            this.bbiRecalculateTotal.ImageOptions.ImageIndex = 2;
            this.bbiRecalculateTotal.Name = "bbiRecalculateTotal";
            this.bbiRecalculateTotal.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Re-Calculate Costs- Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to re-calculate the costs of the visits and jobs.\r\nNote this process ret" +
    "urns each time the Refresh button on this page is clicked.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiRecalculateTotal.SuperTip = superToolTip4;
            this.bbiRecalculateTotal.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiRecalculateTotal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRecalculateTotal_ItemClick);
            // 
            // barEditItemInvoicingParameters
            // 
            this.barEditItemInvoicingParameters.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barEditItemInvoicingParameters.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.barEditItemInvoicingParameters.Caption = "Invoicing Parameters:";
            this.barEditItemInvoicingParameters.Edit = this.repositoryItemPopupContainerEditInvoicingParameters;
            this.barEditItemInvoicingParameters.EditValue = resources.GetString("barEditItemInvoicingParameters.EditValue");
            this.barEditItemInvoicingParameters.Id = 140;
            this.barEditItemInvoicingParameters.Name = "barEditItemInvoicingParameters";
            this.barEditItemInvoicingParameters.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Invoicing Parameters - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "I control if Teams can be invoiced for Visits with outstanding <b>Requirements</b" +
    "> and <b>Warnings</b>. \r\n\r\nTick to allow or un-tick to disallow.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.barEditItemInvoicingParameters.SuperTip = superToolTip5;
            // 
            // repositoryItemPopupContainerEditInvoicingParameters
            // 
            this.repositoryItemPopupContainerEditInvoicingParameters.AutoHeight = false;
            this.repositoryItemPopupContainerEditInvoicingParameters.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditInvoicingParameters.Name = "repositoryItemPopupContainerEditInvoicingParameters";
            this.repositoryItemPopupContainerEditInvoicingParameters.PopupControl = this.popupContainerControlInvoicingParameters;
            this.repositoryItemPopupContainerEditInvoicingParameters.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditInvoicingParameters.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditInvoicingParameters_QueryResultValue);
            // 
            // bbiInvoice
            // 
            this.bbiInvoice.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiInvoice.Caption = "Invoice";
            this.bbiInvoice.Enabled = false;
            this.bbiInvoice.Id = 141;
            this.bbiInvoice.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiInvoice.ImageOptions.Image")));
            this.bbiInvoice.Name = "bbiInvoice";
            this.bbiInvoice.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Invoice - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to create and send Team Self-Billing Invoices for the selected Teams.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.bbiInvoice.SuperTip = superToolTip6;
            this.bbiInvoice.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiInvoice_ItemClick);
            // 
            // repositoryItemCheckEditIgnoreHistoric
            // 
            this.repositoryItemCheckEditIgnoreHistoric.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditIgnoreHistoric.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditIgnoreHistoric.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditIgnoreHistoric.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditIgnoreHistoric.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditIgnoreHistoric.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditIgnoreHistoric.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditIgnoreHistoric.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditIgnoreHistoric.AutoHeight = false;
            this.repositoryItemCheckEditIgnoreHistoric.Caption = "Check";
            this.repositoryItemCheckEditIgnoreHistoric.Name = "repositoryItemCheckEditIgnoreHistoric";
            this.repositoryItemCheckEditIgnoreHistoric.ValueChecked = 1;
            this.repositoryItemCheckEditIgnoreHistoric.ValueUnchecked = 0;
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            // 
            // sp06145OMVisitEditBindingSource
            // 
            this.sp06145OMVisitEditBindingSource.DataMember = "sp06145_OM_Visit_Edit";
            this.sp06145OMVisitEditBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemDuration1
            // 
            this.repositoryItemDuration1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDuration1.AutoHeight = false;
            this.repositoryItemDuration1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDuration1.DisabledStateText = null;
            this.repositoryItemDuration1.Name = "repositoryItemDuration1";
            this.repositoryItemDuration1.NullValuePromptShowForEmptyValue = true;
            this.repositoryItemDuration1.ShowEmptyItem = true;
            this.repositoryItemDuration1.ValidateOnEnterKey = true;
            // 
            // toggleRecurrenceItem1
            // 
            this.toggleRecurrenceItem1.Id = 105;
            this.toggleRecurrenceItem1.Name = "toggleRecurrenceItem1";
            // 
            // sp06145_OM_Visit_EditTableAdapter
            // 
            this.sp06145_OM_Visit_EditTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.Filter_32x32, "Filter_32x32", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection2.Images.SetKeyName(0, "Filter_32x32");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.refresh_32x32, "refresh_32x32", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection2.Images.SetKeyName(1, "refresh_32x32");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.Cost_Calculate_32x32, "Cost_Calculate_32x32", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection2.Images.SetKeyName(2, "Cost_Calculate_32x32");
            this.imageCollection2.Images.SetKeyName(3, "team_send_32x32.png");
            this.imageCollection2.Images.SetKeyName(4, "employee_send_2_32_.png");
            // 
            // dockManager1
            // 
            this.dockManager1.AutoHideContainers.AddRange(new DevExpress.XtraBars.Docking.AutoHideContainer[] {
            this.hideContainerLeft});
            this.dockManager1.Form = this;
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // hideContainerLeft
            // 
            this.hideContainerLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.hideContainerLeft.Controls.Add(this.dockPanelFilters);
            this.hideContainerLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.hideContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.hideContainerLeft.Name = "hideContainerLeft";
            this.hideContainerLeft.Size = new System.Drawing.Size(23, 728);
            // 
            // dockPanelFilters
            // 
            this.dockPanelFilters.Controls.Add(this.dockPanel1_Container);
            this.dockPanelFilters.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.FloatVertical = true;
            this.dockPanelFilters.ID = new System.Guid("88d829bd-f6cb-4abc-9203-5d9c628ceebe");
            this.dockPanelFilters.Location = new System.Drawing.Point(0, 0);
            this.dockPanelFilters.Name = "dockPanelFilters";
            this.dockPanelFilters.Options.ShowCloseButton = false;
            this.dockPanelFilters.OriginalSize = new System.Drawing.Size(360, 200);
            this.dockPanelFilters.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.SavedIndex = 0;
            this.dockPanelFilters.Size = new System.Drawing.Size(360, 728);
            this.dockPanelFilters.Text = "Data Filter";
            this.dockPanelFilters.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControl2);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(353, 696);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.textEditSBNumber);
            this.layoutControl2.Controls.Add(this.popupContainerEditVisitCategoryFilter);
            this.layoutControl2.Controls.Add(this.checkEditDoNotPay);
            this.layoutControl2.Controls.Add(this.popupContainerEditInvoiceNumber);
            this.layoutControl2.Controls.Add(this.buttonEditExcludeClientFilter);
            this.layoutControl2.Controls.Add(this.btnLoadHistorical);
            this.layoutControl2.Controls.Add(this.popupContainerEditHistoricalDateRange);
            this.layoutControl2.Controls.Add(this.popupContainerEditWarningsFilter);
            this.layoutControl2.Controls.Add(this.checkEditIncludeUnpaidHistorical);
            this.layoutControl2.Controls.Add(this.spinEditYear);
            this.layoutControl2.Controls.Add(this.spinEditWeekNumber);
            this.layoutControl2.Controls.Add(this.checkEditDateRange);
            this.layoutControl2.Controls.Add(this.checkEditWeekNumber);
            this.layoutControl2.Controls.Add(this.spinEditSiteID);
            this.layoutControl2.Controls.Add(this.checkEditStaff);
            this.layoutControl2.Controls.Add(this.checkEditTeam);
            this.layoutControl2.Controls.Add(this.popupContainerEditVisitTypeFilter);
            this.layoutControl2.Controls.Add(this.popupContainerEditLabourFilter);
            this.layoutControl2.Controls.Add(this.buttonEditSiteFilter);
            this.layoutControl2.Controls.Add(this.buttonEditClientFilter);
            this.layoutControl2.Controls.Add(this.popupContainerEditDateRange);
            this.layoutControl2.Controls.Add(this.checkEditShowInvoiced);
            this.layoutControl2.Controls.Add(this.btnLoad);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1281, 412, 460, 350);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(353, 696);
            this.layoutControl2.TabIndex = 8;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // textEditSBNumber
            // 
            this.textEditSBNumber.Location = new System.Drawing.Point(113, 191);
            this.textEditSBNumber.MenuManager = this.barManager1;
            this.textEditSBNumber.Name = "textEditSBNumber";
            this.textEditSBNumber.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditSBNumber, true);
            this.textEditSBNumber.Size = new System.Drawing.Size(209, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditSBNumber, optionsSpelling1);
            this.textEditSBNumber.StyleController = this.layoutControl2;
            this.textEditSBNumber.TabIndex = 26;
            // 
            // popupContainerEditVisitCategoryFilter
            // 
            this.popupContainerEditVisitCategoryFilter.EditValue = "No Visit Category Filter";
            this.popupContainerEditVisitCategoryFilter.Location = new System.Drawing.Point(101, 261);
            this.popupContainerEditVisitCategoryFilter.MenuManager = this.barManager1;
            this.popupContainerEditVisitCategoryFilter.Name = "popupContainerEditVisitCategoryFilter";
            this.popupContainerEditVisitCategoryFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.popupContainerEditVisitCategoryFilter.Properties.PopupControl = this.popupContainerControlVisitCategoryFilter;
            this.popupContainerEditVisitCategoryFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditVisitCategoryFilter.Size = new System.Drawing.Size(233, 20);
            this.popupContainerEditVisitCategoryFilter.StyleController = this.layoutControl2;
            this.popupContainerEditVisitCategoryFilter.TabIndex = 9;
            this.popupContainerEditVisitCategoryFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditVisitCategoryFilter_QueryResultValue);
            this.popupContainerEditVisitCategoryFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.popupContainerEditVisitCategoryFilter_ButtonClick);
            // 
            // checkEditDoNotPay
            // 
            this.checkEditDoNotPay.Location = new System.Drawing.Point(31, 166);
            this.checkEditDoNotPay.MenuManager = this.barManager1;
            this.checkEditDoNotPay.Name = "checkEditDoNotPay";
            this.checkEditDoNotPay.Properties.Caption = "Include Do Not Pay";
            this.checkEditDoNotPay.Properties.ValueChecked = 1;
            this.checkEditDoNotPay.Properties.ValueUnchecked = 0;
            this.checkEditDoNotPay.Size = new System.Drawing.Size(291, 19);
            this.checkEditDoNotPay.StyleController = this.layoutControl2;
            this.checkEditDoNotPay.TabIndex = 25;
            // 
            // popupContainerEditInvoiceNumber
            // 
            this.popupContainerEditInvoiceNumber.EditValue = "No Invoice Number Filter";
            this.popupContainerEditInvoiceNumber.Location = new System.Drawing.Point(113, 96);
            this.popupContainerEditInvoiceNumber.MenuManager = this.barManager1;
            this.popupContainerEditInvoiceNumber.Name = "popupContainerEditInvoiceNumber";
            this.popupContainerEditInvoiceNumber.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.popupContainerEditInvoiceNumber.Properties.PopupControl = this.popupContainerControlInvoiceNumber;
            this.popupContainerEditInvoiceNumber.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditInvoiceNumber.Size = new System.Drawing.Size(209, 20);
            this.popupContainerEditInvoiceNumber.StyleController = this.layoutControl2;
            this.popupContainerEditInvoiceNumber.TabIndex = 11;
            this.popupContainerEditInvoiceNumber.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditInvoiceNumber_QueryResultValue);
            this.popupContainerEditInvoiceNumber.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.popupContainerEditInvoiceNumber_ButtonClick);
            // 
            // popupContainerControlInvoiceNumber
            // 
            this.popupContainerControlInvoiceNumber.Controls.Add(this.btnInvoiceNumber);
            this.popupContainerControlInvoiceNumber.Controls.Add(this.gridControl3);
            this.popupContainerControlInvoiceNumber.Location = new System.Drawing.Point(237, 126);
            this.popupContainerControlInvoiceNumber.Name = "popupContainerControlInvoiceNumber";
            this.popupContainerControlInvoiceNumber.Size = new System.Drawing.Size(208, 267);
            this.popupContainerControlInvoiceNumber.TabIndex = 22;
            // 
            // btnInvoiceNumber
            // 
            this.btnInvoiceNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnInvoiceNumber.Location = new System.Drawing.Point(3, 244);
            this.btnInvoiceNumber.Name = "btnInvoiceNumber";
            this.btnInvoiceNumber.Size = new System.Drawing.Size(50, 20);
            this.btnInvoiceNumber.TabIndex = 12;
            this.btnInvoiceNumber.Text = "OK";
            this.btnInvoiceNumber.Click += new System.EventHandler(this.btnInvoiceNumber_Click);
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp06477OMGetUniqueSelfBillingInvoiceListBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(3, 3);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit8});
            this.gridControl3.Size = new System.Drawing.Size(202, 239);
            this.gridControl3.TabIndex = 4;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp06477OMGetUniqueSelfBillingInvoiceListBindingSource
            // 
            this.sp06477OMGetUniqueSelfBillingInvoiceListBindingSource.DataMember = "sp06477_OM_Get_Unique_Self_Billing_Invoice_List";
            this.sp06477OMGetUniqueSelfBillingInvoiceListBindingSource.DataSource = this.dataSet_OM_Billing;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSelfBillingInvoiceNumber1});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSelfBillingInvoiceNumber1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colSelfBillingInvoiceNumber1
            // 
            this.colSelfBillingInvoiceNumber1.Caption = "Invoice Number";
            this.colSelfBillingInvoiceNumber1.FieldName = "SelfBillingInvoiceNumber";
            this.colSelfBillingInvoiceNumber1.Name = "colSelfBillingInvoiceNumber1";
            this.colSelfBillingInvoiceNumber1.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceNumber1.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceNumber1.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceNumber1.Visible = true;
            this.colSelfBillingInvoiceNumber1.VisibleIndex = 0;
            this.colSelfBillingInvoiceNumber1.Width = 124;
            // 
            // repositoryItemCheckEdit8
            // 
            this.repositoryItemCheckEdit8.AutoHeight = false;
            this.repositoryItemCheckEdit8.Caption = "Check";
            this.repositoryItemCheckEdit8.Name = "repositoryItemCheckEdit8";
            this.repositoryItemCheckEdit8.ValueChecked = 1;
            this.repositoryItemCheckEdit8.ValueUnchecked = 0;
            // 
            // buttonEditExcludeClientFilter
            // 
            this.buttonEditExcludeClientFilter.Location = new System.Drawing.Point(101, 426);
            this.buttonEditExcludeClientFilter.MenuManager = this.barManager1;
            this.buttonEditExcludeClientFilter.Name = "buttonEditExcludeClientFilter";
            this.buttonEditExcludeClientFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to open Choose Client Filter Screen to select Clients to Exclude", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditExcludeClientFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditExcludeClientFilter.Size = new System.Drawing.Size(233, 20);
            this.buttonEditExcludeClientFilter.StyleController = this.layoutControl2;
            this.buttonEditExcludeClientFilter.TabIndex = 9;
            this.buttonEditExcludeClientFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditExcludeClientFilter_ButtonClick);
            // 
            // btnLoadHistorical
            // 
            this.btnLoadHistorical.ImageOptions.ImageIndex = 13;
            this.btnLoadHistorical.ImageOptions.ImageList = this.imageCollection1;
            this.btnLoadHistorical.Location = new System.Drawing.Point(243, 130);
            this.btnLoadHistorical.Name = "btnLoadHistorical";
            this.btnLoadHistorical.Size = new System.Drawing.Size(79, 22);
            this.btnLoadHistorical.StyleController = this.layoutControl2;
            toolTipTitleItem10.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Text = "Load Data - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Click me to load Team Historical Data.\r\n\r\nOnly data matching the specified Filter" +
    " Criteria (From and To Dates) will be loaded.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.btnLoadHistorical.SuperTip = superToolTip10;
            this.btnLoadHistorical.TabIndex = 17;
            this.btnLoadHistorical.Text = "Load Data";
            this.btnLoadHistorical.Click += new System.EventHandler(this.btnLoadHistorical_Click);
            // 
            // popupContainerEditHistoricalDateRange
            // 
            this.popupContainerEditHistoricalDateRange.EditValue = "No Date Range Filter";
            this.popupContainerEditHistoricalDateRange.Location = new System.Drawing.Point(113, 72);
            this.popupContainerEditHistoricalDateRange.MenuManager = this.barManager1;
            this.popupContainerEditHistoricalDateRange.Name = "popupContainerEditHistoricalDateRange";
            this.popupContainerEditHistoricalDateRange.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditHistoricalDateRange.Properties.PopupControl = this.popupContainerControlHistoricalDateRange;
            this.popupContainerEditHistoricalDateRange.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditHistoricalDateRange.Size = new System.Drawing.Size(209, 20);
            this.popupContainerEditHistoricalDateRange.StyleController = this.layoutControl2;
            this.popupContainerEditHistoricalDateRange.TabIndex = 9;
            this.popupContainerEditHistoricalDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditHistoricalDateRange_QueryResultValue);
            // 
            // popupContainerControlHistoricalDateRange
            // 
            this.popupContainerControlHistoricalDateRange.Controls.Add(this.groupControl3);
            this.popupContainerControlHistoricalDateRange.Controls.Add(this.btnHistoricalDateRange);
            this.popupContainerControlHistoricalDateRange.Location = new System.Drawing.Point(26, 126);
            this.popupContainerControlHistoricalDateRange.Name = "popupContainerControlHistoricalDateRange";
            this.popupContainerControlHistoricalDateRange.Size = new System.Drawing.Size(196, 107);
            this.popupContainerControlHistoricalDateRange.TabIndex = 18;
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.labelControl3);
            this.groupControl3.Controls.Add(this.labelControl4);
            this.groupControl3.Controls.Add(this.dateEditHistoricalFromDate);
            this.groupControl3.Controls.Add(this.dateEditHistoricalToDate);
            this.groupControl3.Location = new System.Drawing.Point(3, 5);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(190, 76);
            this.groupControl3.TabIndex = 20;
            this.groupControl3.Text = "Date Range  [Invoice Raised Date]";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(6, 53);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(16, 13);
            this.labelControl3.TabIndex = 13;
            this.labelControl3.Text = "To:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(6, 27);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(28, 13);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "From:";
            // 
            // dateEditHistoricalFromDate
            // 
            this.dateEditHistoricalFromDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditHistoricalFromDate.EditValue = null;
            this.dateEditHistoricalFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditHistoricalFromDate.MenuManager = this.barManager1;
            this.dateEditHistoricalFromDate.Name = "dateEditHistoricalFromDate";
            this.dateEditHistoricalFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditHistoricalFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditHistoricalFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditHistoricalFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditHistoricalFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditHistoricalFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditHistoricalFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditHistoricalFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditHistoricalFromDate.Size = new System.Drawing.Size(145, 20);
            this.dateEditHistoricalFromDate.TabIndex = 10;
            // 
            // dateEditHistoricalToDate
            // 
            this.dateEditHistoricalToDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditHistoricalToDate.EditValue = null;
            this.dateEditHistoricalToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditHistoricalToDate.MenuManager = this.barManager1;
            this.dateEditHistoricalToDate.Name = "dateEditHistoricalToDate";
            this.dateEditHistoricalToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditHistoricalToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditHistoricalToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditHistoricalToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditHistoricalToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditHistoricalToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditHistoricalToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditHistoricalToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditHistoricalToDate.Size = new System.Drawing.Size(145, 20);
            this.dateEditHistoricalToDate.TabIndex = 11;
            // 
            // btnHistoricalDateRange
            // 
            this.btnHistoricalDateRange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnHistoricalDateRange.Location = new System.Drawing.Point(3, 84);
            this.btnHistoricalDateRange.Name = "btnHistoricalDateRange";
            this.btnHistoricalDateRange.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnHistoricalDateRange.Size = new System.Drawing.Size(50, 20);
            this.btnHistoricalDateRange.TabIndex = 19;
            this.btnHistoricalDateRange.Text = "OK";
            this.btnHistoricalDateRange.Click += new System.EventHandler(this.btnHistoricalDateRange_Click);
            // 
            // popupContainerEditWarningsFilter
            // 
            this.popupContainerEditWarningsFilter.EditValue = "No Warnings Filter";
            this.popupContainerEditWarningsFilter.Location = new System.Drawing.Point(101, 357);
            this.popupContainerEditWarningsFilter.MenuManager = this.barManager1;
            this.popupContainerEditWarningsFilter.Name = "popupContainerEditWarningsFilter";
            this.popupContainerEditWarningsFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.popupContainerEditWarningsFilter.Properties.PopupControl = this.popupContainerControlWarningFilter;
            this.popupContainerEditWarningsFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditWarningsFilter.Size = new System.Drawing.Size(233, 20);
            this.popupContainerEditWarningsFilter.StyleController = this.layoutControl2;
            this.popupContainerEditWarningsFilter.TabIndex = 10;
            this.popupContainerEditWarningsFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditWarnings_QueryResultValue);
            this.popupContainerEditWarningsFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.popupContainerEditWarningsFilter_ButtonClick);
            // 
            // checkEditIncludeUnpaidHistorical
            // 
            this.checkEditIncludeUnpaidHistorical.Location = new System.Drawing.Point(31, 143);
            this.checkEditIncludeUnpaidHistorical.MenuManager = this.barManager1;
            this.checkEditIncludeUnpaidHistorical.Name = "checkEditIncludeUnpaidHistorical";
            this.checkEditIncludeUnpaidHistorical.Properties.Caption = "Include Unpaid Historical Visits";
            this.checkEditIncludeUnpaidHistorical.Properties.ValueChecked = 1;
            this.checkEditIncludeUnpaidHistorical.Properties.ValueUnchecked = 0;
            this.checkEditIncludeUnpaidHistorical.Size = new System.Drawing.Size(291, 19);
            this.checkEditIncludeUnpaidHistorical.StyleController = this.layoutControl2;
            this.checkEditIncludeUnpaidHistorical.TabIndex = 24;
            // 
            // spinEditYear
            // 
            this.spinEditYear.EditValue = new decimal(new int[] {
            2015,
            0,
            0,
            0});
            this.spinEditYear.Location = new System.Drawing.Point(259, 95);
            this.spinEditYear.MenuManager = this.barManager1;
            this.spinEditYear.Name = "spinEditYear";
            this.spinEditYear.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditYear.Properties.IsFloatValue = false;
            this.spinEditYear.Properties.Mask.EditMask = "f0";
            this.spinEditYear.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditYear.Properties.MaxValue = new decimal(new int[] {
            2500,
            0,
            0,
            0});
            this.spinEditYear.Properties.MinValue = new decimal(new int[] {
            2015,
            0,
            0,
            0});
            this.spinEditYear.Size = new System.Drawing.Size(63, 20);
            this.spinEditYear.StyleController = this.layoutControl2;
            this.spinEditYear.TabIndex = 23;
            this.spinEditYear.EditValueChanged += new System.EventHandler(this.spinEditYear_EditValueChanged);
            // 
            // spinEditWeekNumber
            // 
            this.spinEditWeekNumber.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditWeekNumber.Location = new System.Drawing.Point(113, 95);
            this.spinEditWeekNumber.MenuManager = this.barManager1;
            this.spinEditWeekNumber.Name = "spinEditWeekNumber";
            this.spinEditWeekNumber.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditWeekNumber.Properties.IsFloatValue = false;
            this.spinEditWeekNumber.Properties.Mask.EditMask = "f0";
            this.spinEditWeekNumber.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditWeekNumber.Properties.MaxValue = new decimal(new int[] {
            53,
            0,
            0,
            0});
            this.spinEditWeekNumber.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditWeekNumber.Size = new System.Drawing.Size(60, 20);
            this.spinEditWeekNumber.StyleController = this.layoutControl2;
            this.spinEditWeekNumber.TabIndex = 22;
            this.spinEditWeekNumber.EditValueChanged += new System.EventHandler(this.spinEditWeekNumber_EditValueChanged);
            // 
            // checkEditDateRange
            // 
            this.checkEditDateRange.Location = new System.Drawing.Point(228, 72);
            this.checkEditDateRange.MenuManager = this.barManager1;
            this.checkEditDateRange.Name = "checkEditDateRange";
            this.checkEditDateRange.Properties.Caption = "Date Range:";
            this.checkEditDateRange.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditDateRange.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditDateRange.Properties.RadioGroupIndex = 2;
            this.checkEditDateRange.Size = new System.Drawing.Size(82, 19);
            this.checkEditDateRange.StyleController = this.layoutControl2;
            superToolTip11.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem11.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Appearance.Options.UseImage = true;
            toolTipTitleItem11.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Text = "Date Range - Information";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Click me to activate the Date Range drop down so you can enter a custom date dura" +
    "tion for the loaded data.\r\n\r\nNote: If I am selected, the Week Number box is disa" +
    "bled.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.checkEditDateRange.SuperTip = superToolTip11;
            this.checkEditDateRange.TabIndex = 21;
            this.checkEditDateRange.TabStop = false;
            this.checkEditDateRange.CheckedChanged += new System.EventHandler(this.checkEditDateRange_CheckedChanged);
            // 
            // checkEditWeekNumber
            // 
            this.checkEditWeekNumber.EditValue = true;
            this.checkEditWeekNumber.Location = new System.Drawing.Point(111, 72);
            this.checkEditWeekNumber.MenuManager = this.barManager1;
            this.checkEditWeekNumber.Name = "checkEditWeekNumber";
            this.checkEditWeekNumber.Properties.Caption = "Week Number:";
            this.checkEditWeekNumber.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditWeekNumber.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditWeekNumber.Properties.RadioGroupIndex = 2;
            this.checkEditWeekNumber.Size = new System.Drawing.Size(113, 19);
            this.checkEditWeekNumber.StyleController = this.layoutControl2;
            superToolTip12.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem12.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem12.Appearance.Options.UseImage = true;
            toolTipTitleItem12.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem12.Text = "Date Range - Information";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Click me to use week numbering for the loaded data - the date range will then be " +
    "set to a single week window.\r\n\r\nNote: If I am selected, the Date Range dropdown " +
    "is disabled.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            this.checkEditWeekNumber.SuperTip = superToolTip12;
            this.checkEditWeekNumber.TabIndex = 20;
            this.checkEditWeekNumber.CheckedChanged += new System.EventHandler(this.checkEditWeekNumber_CheckedChanged);
            // 
            // spinEditSiteID
            // 
            this.spinEditSiteID.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditSiteID.Location = new System.Drawing.Point(101, 333);
            this.spinEditSiteID.MenuManager = this.barManager1;
            this.spinEditSiteID.Name = "spinEditSiteID";
            this.spinEditSiteID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.spinEditSiteID.Properties.IsFloatValue = false;
            this.spinEditSiteID.Properties.Mask.EditMask = "n0";
            this.spinEditSiteID.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditSiteID.Size = new System.Drawing.Size(233, 20);
            this.spinEditSiteID.StyleController = this.layoutControl2;
            superToolTip13.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem13.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem13.Appearance.Options.UseImage = true;
            toolTipTitleItem13.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem13.Text = "Site ID - Information";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "Set my value to <b>0</b> for <b>no</b> filtering by Site ID.";
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            this.spinEditSiteID.SuperTip = superToolTip13;
            this.spinEditSiteID.TabIndex = 20;
            this.spinEditSiteID.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.spinEditSiteID_ButtonClick);
            // 
            // checkEditStaff
            // 
            this.checkEditStaff.Location = new System.Drawing.Point(132, 492);
            this.checkEditStaff.MenuManager = this.barManager1;
            this.checkEditStaff.Name = "checkEditStaff";
            this.checkEditStaff.Properties.Caption = "Staff:";
            this.checkEditStaff.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditStaff.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditStaff.Properties.RadioGroupIndex = 1;
            this.checkEditStaff.Size = new System.Drawing.Size(49, 19);
            this.checkEditStaff.StyleController = this.layoutControl2;
            this.checkEditStaff.TabIndex = 19;
            this.checkEditStaff.TabStop = false;
            this.checkEditStaff.CheckedChanged += new System.EventHandler(this.checkEditStaff_CheckedChanged);
            // 
            // checkEditTeam
            // 
            this.checkEditTeam.EditValue = true;
            this.checkEditTeam.Location = new System.Drawing.Point(67, 492);
            this.checkEditTeam.MenuManager = this.barManager1;
            this.checkEditTeam.Name = "checkEditTeam";
            this.checkEditTeam.Properties.Caption = "Team:";
            this.checkEditTeam.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditTeam.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditTeam.Properties.RadioGroupIndex = 1;
            this.checkEditTeam.Size = new System.Drawing.Size(51, 19);
            this.checkEditTeam.StyleController = this.layoutControl2;
            this.checkEditTeam.TabIndex = 18;
            this.checkEditTeam.CheckedChanged += new System.EventHandler(this.checkEditTeam_CheckedChanged);
            // 
            // popupContainerEditVisitTypeFilter
            // 
            this.popupContainerEditVisitTypeFilter.EditValue = "No Visit Type Filter";
            this.popupContainerEditVisitTypeFilter.Location = new System.Drawing.Point(101, 237);
            this.popupContainerEditVisitTypeFilter.MenuManager = this.barManager1;
            this.popupContainerEditVisitTypeFilter.Name = "popupContainerEditVisitTypeFilter";
            this.popupContainerEditVisitTypeFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.popupContainerEditVisitTypeFilter.Properties.PopupControl = this.popupContainerControlVisitTypeFilter;
            this.popupContainerEditVisitTypeFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditVisitTypeFilter.Size = new System.Drawing.Size(233, 20);
            this.popupContainerEditVisitTypeFilter.StyleController = this.layoutControl2;
            this.popupContainerEditVisitTypeFilter.TabIndex = 0;
            this.popupContainerEditVisitTypeFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditVisitTypeFilter_QueryResultValue);
            this.popupContainerEditVisitTypeFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.popupContainerEditVisitTypeFilter_ButtonClick);
            // 
            // popupContainerEditLabourFilter
            // 
            this.popupContainerEditLabourFilter.EditValue = "No Labour Filter";
            this.popupContainerEditLabourFilter.Location = new System.Drawing.Point(66, 515);
            this.popupContainerEditLabourFilter.MenuManager = this.barManager1;
            this.popupContainerEditLabourFilter.Name = "popupContainerEditLabourFilter";
            this.popupContainerEditLabourFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Reload Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.popupContainerEditLabourFilter.Properties.PopupControl = this.popupContainerControlLabourFilter;
            this.popupContainerEditLabourFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditLabourFilter.Properties.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditLabourFilter_QueryResultValue);
            this.popupContainerEditLabourFilter.Size = new System.Drawing.Size(256, 20);
            this.popupContainerEditLabourFilter.StyleController = this.layoutControl2;
            this.popupContainerEditLabourFilter.TabIndex = 12;
            this.popupContainerEditLabourFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.popupContainerEditLabourFilter_ButtonClick);
            // 
            // buttonEditSiteFilter
            // 
            this.buttonEditSiteFilter.Location = new System.Drawing.Point(101, 309);
            this.buttonEditSiteFilter.MenuManager = this.barManager1;
            this.buttonEditSiteFilter.Name = "buttonEditSiteFilter";
            this.buttonEditSiteFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions10, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject37, serializableAppearanceObject38, serializableAppearanceObject39, serializableAppearanceObject40, "Click to open Choose Site Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions11, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject41, serializableAppearanceObject42, serializableAppearanceObject43, serializableAppearanceObject44, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditSiteFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditSiteFilter.Size = new System.Drawing.Size(233, 20);
            this.buttonEditSiteFilter.StyleController = this.layoutControl2;
            this.buttonEditSiteFilter.TabIndex = 3;
            this.buttonEditSiteFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditSiteFilter_ButtonClick);
            // 
            // buttonEditClientFilter
            // 
            this.buttonEditClientFilter.Location = new System.Drawing.Point(101, 285);
            this.buttonEditClientFilter.MenuManager = this.barManager1;
            this.buttonEditClientFilter.Name = "buttonEditClientFilter";
            this.buttonEditClientFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions12, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject45, serializableAppearanceObject46, serializableAppearanceObject47, serializableAppearanceObject48, "Click to open Choose Client Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions13, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject49, serializableAppearanceObject50, serializableAppearanceObject51, serializableAppearanceObject52, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditClientFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditClientFilter.Size = new System.Drawing.Size(233, 20);
            this.buttonEditClientFilter.StyleController = this.layoutControl2;
            this.buttonEditClientFilter.TabIndex = 2;
            this.buttonEditClientFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditClientFilter_ButtonClick);
            // 
            // popupContainerEditDateRange
            // 
            this.popupContainerEditDateRange.EditValue = "No Date Range Filter";
            this.popupContainerEditDateRange.Location = new System.Drawing.Point(113, 119);
            this.popupContainerEditDateRange.MenuManager = this.barManager1;
            this.popupContainerEditDateRange.Name = "popupContainerEditDateRange";
            this.popupContainerEditDateRange.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditDateRange.Properties.PopupControl = this.popupContainerControlDateRange;
            this.popupContainerEditDateRange.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditDateRange.Size = new System.Drawing.Size(209, 20);
            this.popupContainerEditDateRange.StyleController = this.layoutControl2;
            this.popupContainerEditDateRange.TabIndex = 1;
            this.popupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditDateRange_QueryResultValue);
            // 
            // checkEditShowInvoiced
            // 
            this.checkEditShowInvoiced.Location = new System.Drawing.Point(101, 381);
            this.checkEditShowInvoiced.MenuManager = this.barManager1;
            this.checkEditShowInvoiced.Name = "checkEditShowInvoiced";
            this.checkEditShowInvoiced.Properties.Caption = "[Tick if Yes]";
            this.checkEditShowInvoiced.Size = new System.Drawing.Size(233, 19);
            this.checkEditShowInvoiced.StyleController = this.layoutControl2;
            superToolTip14.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem14.Text = "Show Invoiced - Information";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Tick me to show visits already Invoiced.\r\n\r\n<b><color=red>Important Note:</color>" +
    "</b>  If ticked, only Invoiced Visits are shown - these are read only and can\'t " +
    "be changed.";
            superToolTip14.Items.Add(toolTipTitleItem14);
            superToolTip14.Items.Add(toolTipItem14);
            this.checkEditShowInvoiced.SuperTip = superToolTip14;
            this.checkEditShowInvoiced.TabIndex = 7;
            // 
            // btnLoad
            // 
            this.btnLoad.ImageOptions.ImageIndex = 13;
            this.btnLoad.ImageOptions.ImageList = this.imageCollection1;
            this.btnLoad.Location = new System.Drawing.Point(255, 561);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(79, 22);
            this.btnLoad.StyleController = this.layoutControl2;
            toolTipTitleItem15.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem15.Appearance.Options.UseImage = true;
            toolTipTitleItem15.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem15.Text = "Load Data - Information";
            toolTipItem15.LeftIndent = 6;
            toolTipItem15.Text = "Click me to Load Team and Visit Data.\r\n\r\nOnly data matching the specified Filter " +
    "Criteria (Date Range \\ Week Number, Clients, Sites etc) will be loaded.";
            superToolTip15.Items.Add(toolTipTitleItem15);
            superToolTip15.Items.Add(toolTipItem15);
            this.btnLoad.SuperTip = superToolTip15;
            this.btnLoad.TabIndex = 16;
            this.btnLoad.Text = "Load Data";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Root";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3,
            this.tabbedControlGroupFilters});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup2.Size = new System.Drawing.Size(353, 696);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 592);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(343, 94);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // tabbedControlGroupFilters
            // 
            this.tabbedControlGroupFilters.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroupFilters.Name = "tabbedControlGroupFilters";
            this.tabbedControlGroupFilters.SelectedTabPage = this.layoutControlGroupChecking;
            this.tabbedControlGroupFilters.SelectedTabPageIndex = 0;
            this.tabbedControlGroupFilters.Size = new System.Drawing.Size(343, 592);
            this.tabbedControlGroupFilters.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupChecking,
            this.layoutControlGroupHistorical});
            // 
            // layoutControlGroupChecking
            // 
            this.layoutControlGroupChecking.AllowHide = false;
            this.layoutControlGroupChecking.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem13,
            this.layoutControlGroup5,
            this.layoutControlItem17,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem1,
            this.layoutControlItem5,
            this.ItemForShowInvoiced,
            this.emptySpaceItem10,
            this.layoutControlGroup4,
            this.emptySpaceItem2,
            this.emptySpaceItem1,
            this.layoutControlItem3,
            this.emptySpaceItem15,
            this.emptySpaceItem16,
            this.simpleSeparator1,
            this.layoutControlItem6,
            this.layoutControlItem11});
            this.layoutControlGroupChecking.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupChecking.Name = "layoutControlGroupChecking";
            this.layoutControlGroupChecking.Size = new System.Drawing.Size(319, 547);
            this.layoutControlGroupChecking.Text = "Checking \\ Invoicing";
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(0, 187);
            this.emptySpaceItem13.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem13.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(319, 10);
            this.emptySpaceItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.emptySpaceItem5,
            this.layoutControlItem4,
            this.emptySpaceItem9,
            this.ItemForDateRange,
            this.ItemForWeekNumber,
            this.emptySpaceItem12,
            this.ItemForYear,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.ItemforSBNumber,
            this.simpleSeparator2});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(319, 187);
            this.layoutControlGroup5.Text = "Date Filter:";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.checkEditWeekNumber;
            this.layoutControlItem2.Location = new System.Drawing.Point(80, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(117, 23);
            this.layoutControlItem2.Text = "Week Nunmber:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(283, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(12, 23);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.checkEditDateRange;
            this.layoutControlItem4.Location = new System.Drawing.Point(197, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(86, 23);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(80, 23);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDateRange
            // 
            this.ItemForDateRange.Control = this.popupContainerEditDateRange;
            this.ItemForDateRange.CustomizationFormText = "Date Range:";
            this.ItemForDateRange.Location = new System.Drawing.Point(0, 47);
            this.ItemForDateRange.Name = "ItemForDateRange";
            this.ItemForDateRange.Size = new System.Drawing.Size(295, 24);
            this.ItemForDateRange.Text = "Date Range:";
            this.ItemForDateRange.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForWeekNumber
            // 
            this.ItemForWeekNumber.Control = this.spinEditWeekNumber;
            this.ItemForWeekNumber.Location = new System.Drawing.Point(0, 23);
            this.ItemForWeekNumber.Name = "ItemForWeekNumber";
            this.ItemForWeekNumber.Size = new System.Drawing.Size(146, 24);
            this.ItemForWeekNumber.Text = "Week Number:";
            this.ItemForWeekNumber.TextSize = new System.Drawing.Size(79, 13);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(146, 23);
            this.emptySpaceItem12.MaxSize = new System.Drawing.Size(51, 0);
            this.emptySpaceItem12.MinSize = new System.Drawing.Size(51, 10);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(51, 24);
            this.emptySpaceItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForYear
            // 
            this.ItemForYear.Control = this.spinEditYear;
            this.ItemForYear.Location = new System.Drawing.Point(197, 23);
            this.ItemForYear.MaxSize = new System.Drawing.Size(98, 24);
            this.ItemForYear.MinSize = new System.Drawing.Size(98, 24);
            this.ItemForYear.Name = "ItemForYear";
            this.ItemForYear.Size = new System.Drawing.Size(98, 24);
            this.ItemForYear.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForYear.Text = "Year:";
            this.ItemForYear.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForYear.TextSize = new System.Drawing.Size(26, 13);
            this.ItemForYear.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.checkEditIncludeUnpaidHistorical;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 71);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(295, 23);
            this.layoutControlItem9.Text = "Include Unpaid Historic:al";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.checkEditDoNotPay;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 94);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(295, 23);
            this.layoutControlItem10.Text = "Include Do Not Pay:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // ItemforSBNumber
            // 
            this.ItemforSBNumber.Control = this.textEditSBNumber;
            this.ItemforSBNumber.Location = new System.Drawing.Point(0, 119);
            this.ItemforSBNumber.Name = "ItemforSBNumber";
            this.ItemforSBNumber.Size = new System.Drawing.Size(295, 24);
            this.ItemforSBNumber.Text = "SB Number:";
            this.ItemforSBNumber.TextSize = new System.Drawing.Size(79, 13);
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.Location = new System.Drawing.Point(0, 117);
            this.simpleSeparator2.Name = "simpleSeparator2";
            this.simpleSeparator2.Size = new System.Drawing.Size(295, 2);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.popupContainerEditVisitTypeFilter;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 197);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(319, 24);
            this.layoutControlItem17.Text = "Visit Type:";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.buttonEditClientFilter;
            this.layoutControlItem7.CustomizationFormText = "Client:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 245);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(319, 24);
            this.layoutControlItem7.Text = "Client:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.buttonEditSiteFilter;
            this.layoutControlItem8.CustomizationFormText = "Site:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 269);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(319, 24);
            this.layoutControlItem8.Text = "Site:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.spinEditSiteID;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 293);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(319, 24);
            this.layoutControlItem1.Text = "Site ID:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.popupContainerEditWarningsFilter;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 317);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(319, 24);
            this.layoutControlItem5.Text = "Warnings:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(79, 13);
            // 
            // ItemForShowInvoiced
            // 
            this.ItemForShowInvoiced.Control = this.checkEditShowInvoiced;
            this.ItemForShowInvoiced.CustomizationFormText = "Show Invoiced:";
            this.ItemForShowInvoiced.Location = new System.Drawing.Point(0, 341);
            this.ItemForShowInvoiced.Name = "ItemForShowInvoiced";
            this.ItemForShowInvoiced.Size = new System.Drawing.Size(319, 23);
            this.ItemForShowInvoiced.Text = "Show Invoiced:";
            this.ItemForShowInvoiced.TextSize = new System.Drawing.Size(79, 13);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 364);
            this.emptySpaceItem10.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem10.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(319, 10);
            this.emptySpaceItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLabourFilter,
            this.layoutControlItem18,
            this.emptySpaceItem6,
            this.emptySpaceItem7,
            this.layoutControlItem19,
            this.emptySpaceItem8});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 420);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(319, 91);
            this.layoutControlGroup4.Text = "Labour";
            // 
            // ItemForLabourFilter
            // 
            this.ItemForLabourFilter.Control = this.popupContainerEditLabourFilter;
            this.ItemForLabourFilter.CustomizationFormText = "Team:";
            this.ItemForLabourFilter.Location = new System.Drawing.Point(0, 23);
            this.ItemForLabourFilter.Name = "ItemForLabourFilter";
            this.ItemForLabourFilter.Size = new System.Drawing.Size(295, 24);
            this.ItemForLabourFilter.Text = "Team:";
            this.ItemForLabourFilter.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForLabourFilter.TextSize = new System.Drawing.Size(30, 13);
            this.ItemForLabourFilter.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.checkEditTeam;
            this.layoutControlItem18.Location = new System.Drawing.Point(36, 0);
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(55, 23);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(55, 23);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(55, 23);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "Team Filter:";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(36, 0);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(36, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(36, 23);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(154, 0);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(141, 23);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.checkEditStaff;
            this.layoutControlItem19.Location = new System.Drawing.Point(101, 0);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(53, 23);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(53, 23);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(53, 23);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "Staff Filter:";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(91, 0);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(10, 0);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(10, 23);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 511);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(319, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 521);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(236, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnLoad;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(236, 521);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(83, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(83, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(83, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(0, 410);
            this.emptySpaceItem15.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem15.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(319, 10);
            this.emptySpaceItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(0, 376);
            this.emptySpaceItem16.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem16.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(319, 10);
            this.emptySpaceItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 374);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(319, 2);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.buttonEditExcludeClientFilter;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 386);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(319, 24);
            this.layoutControlItem6.Text = "Exclude Client:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.popupContainerEditVisitCategoryFilter;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 221);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(319, 24);
            this.layoutControlItem11.Text = "Visit Category:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(79, 13);
            // 
            // layoutControlGroupHistorical
            // 
            this.layoutControlGroupHistorical.AllowHide = false;
            this.layoutControlGroupHistorical.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6});
            this.layoutControlGroupHistorical.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupHistorical.Name = "layoutControlGroupHistorical";
            this.layoutControlGroupHistorical.Size = new System.Drawing.Size(319, 547);
            this.layoutControlGroupHistorical.Text = "Historical Invoices";
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForHistoricalDateRange,
            this.emptySpaceItem11,
            this.emptySpaceItem4,
            this.emptySpaceItem14,
            this.ItemForLoadHistorical,
            this.ItemForInvoiceNumber});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(319, 547);
            this.layoutControlGroup6.Text = "Historical Date Filter:";
            // 
            // ItemForHistoricalDateRange
            // 
            this.ItemForHistoricalDateRange.Control = this.popupContainerEditHistoricalDateRange;
            this.ItemForHistoricalDateRange.Location = new System.Drawing.Point(0, 0);
            this.ItemForHistoricalDateRange.Name = "ItemForHistoricalDateRange";
            this.ItemForHistoricalDateRange.Size = new System.Drawing.Size(295, 24);
            this.ItemForHistoricalDateRange.Text = "Date Range:";
            this.ItemForHistoricalDateRange.TextSize = new System.Drawing.Size(79, 13);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 84);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(295, 419);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(295, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(0, 58);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(212, 26);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForLoadHistorical
            // 
            this.ItemForLoadHistorical.Control = this.btnLoadHistorical;
            this.ItemForLoadHistorical.Location = new System.Drawing.Point(212, 58);
            this.ItemForLoadHistorical.MaxSize = new System.Drawing.Size(83, 26);
            this.ItemForLoadHistorical.MinSize = new System.Drawing.Size(83, 26);
            this.ItemForLoadHistorical.Name = "ItemForLoadHistorical";
            this.ItemForLoadHistorical.Size = new System.Drawing.Size(83, 26);
            this.ItemForLoadHistorical.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLoadHistorical.Text = "Load Historical Button:";
            this.ItemForLoadHistorical.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForLoadHistorical.TextVisible = false;
            // 
            // ItemForInvoiceNumber
            // 
            this.ItemForInvoiceNumber.Control = this.popupContainerEditInvoiceNumber;
            this.ItemForInvoiceNumber.Location = new System.Drawing.Point(0, 24);
            this.ItemForInvoiceNumber.Name = "ItemForInvoiceNumber";
            this.ItemForInvoiceNumber.Size = new System.Drawing.Size(295, 24);
            this.ItemForInvoiceNumber.Text = "Invoice Number:";
            this.ItemForInvoiceNumber.TextSize = new System.Drawing.Size(79, 13);
            // 
            // sp06028_OM_Labour_FilterTableAdapter
            // 
            this.sp06028_OM_Labour_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06308_OM_Visit_TypesTableAdapter
            // 
            this.sp06308_OM_Visit_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06357_OM_Visit_Manager_Linked_Visit_LabourTableAdapter
            // 
            this.sp06357_OM_Visit_Manager_Linked_Visit_LabourTableAdapter.ClearBeforeFill = true;
            // 
            // sp06445_OM_Team_Self_Billing_Manager_VisitsTableAdapter
            // 
            this.sp06445_OM_Team_Self_Billing_Manager_VisitsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06446_OM_Team_Self_Billing_Billing_RequirementsTableAdapter
            // 
            this.sp06446_OM_Team_Self_Billing_Billing_RequirementsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06448_OM_Team_Self_Billing_Billing_ExceptionsTableAdapter
            // 
            this.sp06448_OM_Team_Self_Billing_Billing_ExceptionsTableAdapter.ClearBeforeFill = true;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(23, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageChecking;
            this.xtraTabControl1.Size = new System.Drawing.Size(1361, 728);
            this.xtraTabControl1.TabIndex = 8;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageChecking,
            this.xtraTabPageHistoricalInvoices,
            this.xtraTabPageInvoiceDesigner});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            // 
            // xtraTabPageChecking
            // 
            this.xtraTabPageChecking.Controls.Add(this.splitContainerControl2);
            this.xtraTabPageChecking.Controls.Add(this.standaloneBarDockControl1);
            this.xtraTabPageChecking.Name = "xtraTabPageChecking";
            this.xtraTabPageChecking.Size = new System.Drawing.Size(1356, 702);
            this.xtraTabPageChecking.Text = "Checking \\ Invoice Teams";
            // 
            // xtraTabPageHistoricalInvoices
            // 
            this.xtraTabPageHistoricalInvoices.Controls.Add(this.splitContainerControl5);
            this.xtraTabPageHistoricalInvoices.Controls.Add(this.standaloneBarDockControl2);
            this.xtraTabPageHistoricalInvoices.Name = "xtraTabPageHistoricalInvoices";
            this.xtraTabPageHistoricalInvoices.Size = new System.Drawing.Size(1356, 702);
            this.xtraTabPageHistoricalInvoices.Text = "Historical Team Invoices";
            // 
            // splitContainerControl5
            // 
            this.splitContainerControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl5.Location = new System.Drawing.Point(0, 42);
            this.splitContainerControl5.Name = "splitContainerControl5";
            this.splitContainerControl5.Panel1.Controls.Add(this.popupContainerControlInvoiceNumber);
            this.splitContainerControl5.Panel1.Controls.Add(this.popupContainerControlHistoricalDateRange);
            this.splitContainerControl5.Panel1.Controls.Add(this.gridControlLabour2);
            this.splitContainerControl5.Panel1.Text = "Panel1";
            this.splitContainerControl5.Panel2.Controls.Add(this.gridControlInvoice);
            this.splitContainerControl5.Panel2.Text = "Panel2";
            this.splitContainerControl5.Size = new System.Drawing.Size(1356, 660);
            this.splitContainerControl5.SplitterPosition = 504;
            this.splitContainerControl5.TabIndex = 1;
            this.splitContainerControl5.Text = "splitContainerControl5";
            // 
            // gridControlLabour2
            // 
            this.gridControlLabour2.DataSource = this.sp06463OMBillingManagerInvoicedLabourBindingSource;
            this.gridControlLabour2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlLabour2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlLabour2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlLabour2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlLabour2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlLabour2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlLabour2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlLabour2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlLabour2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlLabour2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlLabour2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlLabour2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlLabour2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 13, true, true, "Reload Data", "reload")});
            this.gridControlLabour2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlLabour2_EmbeddedNavigator_ButtonClick);
            this.gridControlLabour2.Location = new System.Drawing.Point(0, 0);
            this.gridControlLabour2.MainView = this.gridViewLabour2;
            this.gridControlLabour2.MenuManager = this.barManager1;
            this.gridControlLabour2.Name = "gridControlLabour2";
            this.gridControlLabour2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit6,
            this.repositoryItemMemoExEdit6});
            this.gridControlLabour2.Size = new System.Drawing.Size(504, 660);
            this.gridControlLabour2.TabIndex = 6;
            this.gridControlLabour2.ToolTipController = this.toolTipController1;
            this.gridControlLabour2.UseEmbeddedNavigator = true;
            this.gridControlLabour2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLabour2});
            // 
            // sp06463OMBillingManagerInvoicedLabourBindingSource
            // 
            this.sp06463OMBillingManagerInvoicedLabourBindingSource.DataMember = "sp06463_OM_Billing_Manager_Invoiced_Labour";
            this.sp06463OMBillingManagerInvoicedLabourBindingSource.DataSource = this.dataSet_OM_Billing;
            // 
            // gridViewLabour2
            // 
            this.gridViewLabour2.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewLabour2.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewLabour2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnLabourTypeID,
            this.gridColumn16,
            this.gridColumnLabourID,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.colUnsentCount,
            this.colNoPDFCount,
            this.colInvoiceCount,
            this.gridColumn27,
            this.colNoCMReportCount,
            this.colUnsentCMReportCount});
            gridFormatRule16.Column = this.colNoPDFCount;
            gridFormatRule16.Name = "FormatNoPDFCount>0";
            formatConditionRuleValue16.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue16.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue16.Value1 = 0;
            gridFormatRule16.Rule = formatConditionRuleValue16;
            gridFormatRule17.Column = this.colNoPDFCount;
            gridFormatRule17.Name = "FormatNoPDFCount=0";
            formatConditionRuleValue17.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue17.PredefinedName = "Green Fill, Green Text";
            formatConditionRuleValue17.Value1 = 0;
            gridFormatRule17.Rule = formatConditionRuleValue17;
            gridFormatRule18.Column = this.colUnsentCount;
            gridFormatRule18.Name = "FormatUnsentCount>0";
            formatConditionRuleValue18.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue18.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue18.Value1 = 0;
            gridFormatRule18.Rule = formatConditionRuleValue18;
            gridFormatRule19.Column = this.colUnsentCount;
            gridFormatRule19.Name = "FormatUnsentCount=0";
            formatConditionRuleValue19.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue19.PredefinedName = "Green Fill, Green Text";
            formatConditionRuleValue19.Value1 = 0;
            gridFormatRule19.Rule = formatConditionRuleValue19;
            gridFormatRule20.Column = this.colNoCMReportCount;
            gridFormatRule20.Name = "FormatNoCMReportCount";
            formatConditionRuleValue20.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue20.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue20.Value1 = 0;
            gridFormatRule20.Rule = formatConditionRuleValue20;
            gridFormatRule21.Column = this.colNoCMReportCount;
            gridFormatRule21.Name = "FormatNoCMReportCount=0";
            formatConditionRuleValue21.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue21.PredefinedName = "Green Fill, Green Text";
            formatConditionRuleValue21.Value1 = 0;
            gridFormatRule21.Rule = formatConditionRuleValue21;
            gridFormatRule22.Column = this.colUnsentCMReportCount;
            gridFormatRule22.Name = "FormatUnsentCMReportCount>0";
            formatConditionRuleValue22.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue22.PredefinedName = "Red Fill, Red Text";
            formatConditionRuleValue22.Value1 = 0;
            gridFormatRule22.Rule = formatConditionRuleValue22;
            gridFormatRule23.Column = this.colUnsentCMReportCount;
            gridFormatRule23.Name = "FormatUnsentCMReportCount=0";
            formatConditionRuleValue23.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue23.PredefinedName = "Green Fill, Green Text";
            formatConditionRuleValue23.Value1 = 0;
            gridFormatRule23.Rule = formatConditionRuleValue23;
            this.gridViewLabour2.FormatRules.Add(gridFormatRule16);
            this.gridViewLabour2.FormatRules.Add(gridFormatRule17);
            this.gridViewLabour2.FormatRules.Add(gridFormatRule18);
            this.gridViewLabour2.FormatRules.Add(gridFormatRule19);
            this.gridViewLabour2.FormatRules.Add(gridFormatRule20);
            this.gridViewLabour2.FormatRules.Add(gridFormatRule21);
            this.gridViewLabour2.FormatRules.Add(gridFormatRule22);
            this.gridViewLabour2.FormatRules.Add(gridFormatRule23);
            this.gridViewLabour2.GridControl = this.gridControlLabour2;
            this.gridViewLabour2.GroupCount = 1;
            this.gridViewLabour2.Name = "gridViewLabour2";
            this.gridViewLabour2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewLabour2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewLabour2.OptionsFind.AlwaysVisible = true;
            this.gridViewLabour2.OptionsFind.FindDelay = 2000;
            this.gridViewLabour2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewLabour2.OptionsLayout.StoreAppearance = true;
            this.gridViewLabour2.OptionsLayout.StoreFormatRules = true;
            this.gridViewLabour2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewLabour2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewLabour2.OptionsSelection.MultiSelect = true;
            this.gridViewLabour2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewLabour2.OptionsView.ColumnAutoWidth = false;
            this.gridViewLabour2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewLabour2.OptionsView.ShowGroupPanel = false;
            this.gridViewLabour2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn16, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn18, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewLabour2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewLabour2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewLabour2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewLabour2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewLabour2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewLabour2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewLabour2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewLabour2_MouseUp);
            this.gridViewLabour2.GotFocus += new System.EventHandler(this.gridViewLabour2_GotFocus);
            // 
            // gridColumnLabourTypeID
            // 
            this.gridColumnLabourTypeID.Caption = "Labour Type ID";
            this.gridColumnLabourTypeID.FieldName = "LabourTypeID";
            this.gridColumnLabourTypeID.Name = "gridColumnLabourTypeID";
            this.gridColumnLabourTypeID.OptionsColumn.AllowEdit = false;
            this.gridColumnLabourTypeID.OptionsColumn.AllowFocus = false;
            this.gridColumnLabourTypeID.OptionsColumn.ReadOnly = true;
            this.gridColumnLabourTypeID.Width = 93;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Labour Type";
            this.gridColumn16.FieldName = "LabourType";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 0;
            this.gridColumn16.Width = 204;
            // 
            // gridColumnLabourID
            // 
            this.gridColumnLabourID.Caption = "Labour ID";
            this.gridColumnLabourID.FieldName = "LabourID";
            this.gridColumnLabourID.Name = "gridColumnLabourID";
            this.gridColumnLabourID.OptionsColumn.AllowEdit = false;
            this.gridColumnLabourID.OptionsColumn.AllowFocus = false;
            this.gridColumnLabourID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Labour Name";
            this.gridColumn18.FieldName = "LabourName";
            this.gridColumn18.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 0;
            this.gridColumn18.Width = 227;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Telephone";
            this.gridColumn19.FieldName = "Telephone";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 6;
            this.gridColumn19.Width = 90;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Email";
            this.gridColumn20.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.gridColumn20.FieldName = "Email";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 7;
            this.gridColumn20.Width = 226;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Selected";
            this.gridColumn27.ColumnEdit = this.repositoryItemCheckEdit6;
            this.gridColumn27.FieldName = "Selected";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Width = 60;
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            this.repositoryItemCheckEdit6.ValueChecked = 1;
            this.repositoryItemCheckEdit6.ValueUnchecked = 0;
            // 
            // gridControlInvoice
            // 
            this.gridControlInvoice.DataSource = this.sp06464OMBillingManagerInvoicesForLabourBindingSource;
            this.gridControlInvoice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlInvoice.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlInvoice.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlInvoice.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlInvoice.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlInvoice.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlInvoice.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlInvoice.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlInvoice.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlInvoice.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlInvoice.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlInvoice.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlInvoice.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 13, true, true, "Reload Data", "reload")});
            this.gridControlInvoice.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlInvoice_EmbeddedNavigator_ButtonClick);
            this.gridControlInvoice.Location = new System.Drawing.Point(0, 0);
            this.gridControlInvoice.MainView = this.gridViewInvoice;
            this.gridControlInvoice.MenuManager = this.barManager1;
            this.gridControlInvoice.Name = "gridControlInvoice";
            this.gridControlInvoice.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit7,
            this.repositoryItemTextEditDateTime5,
            this.repositoryItemTextEditCurrency5,
            this.repositoryItemHyperLinkEditInvoicePDF,
            this.repositoryItemHyperLinkEditVisitCount,
            this.repositoryItemMemoExEdit7,
            this.repositoryItemHyperLinkEditCMReportPDF});
            this.gridControlInvoice.Size = new System.Drawing.Size(846, 660);
            this.gridControlInvoice.TabIndex = 7;
            this.gridControlInvoice.ToolTipController = this.toolTipController1;
            this.gridControlInvoice.UseEmbeddedNavigator = true;
            this.gridControlInvoice.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewInvoice});
            // 
            // sp06464OMBillingManagerInvoicesForLabourBindingSource
            // 
            this.sp06464OMBillingManagerInvoicesForLabourBindingSource.DataMember = "sp06464_OM_Billing_Manager_Invoices_For_Labour";
            this.sp06464OMBillingManagerInvoicesForLabourBindingSource.DataSource = this.dataSet_OM_Billing;
            // 
            // gridViewInvoice
            // 
            this.gridViewInvoice.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewInvoice.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewInvoice.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSelfBillingInvoiceHeaderID1,
            this.colSelfBillingInvoiceNumber,
            this.colLinkedToLabourID,
            this.colLinkedToLabourTypeID,
            this.colDateRaised,
            this.colTotalBillAmount,
            this.colRemarks1,
            this.colLastSentDate,
            this.colInvoicePDF,
            this.colLabourType1,
            this.colLabourName3,
            this.colTelephone1,
            this.colEmail1,
            this.colSelected2,
            this.colLinkedVisitCount,
            this.colEmailPassword,
            this.colLastSentCMReportDate,
            this.colCMReportPDF,
            this.colContractManagerEmail,
            this.colLinkedFinanceExportedCount});
            gridFormatRule24.Column = this.colLastSentDate;
            gridFormatRule24.Name = "FormatLastDateSent";
            formatConditionRuleValue24.Condition = DevExpress.XtraEditors.FormatCondition.Expression;
            formatConditionRuleValue24.Expression = "IsNullOrEmpty([LastSentDate])";
            formatConditionRuleValue24.PredefinedName = "Red Fill";
            gridFormatRule24.Rule = formatConditionRuleValue24;
            gridFormatRule25.Column = this.colInvoicePDF;
            gridFormatRule25.Name = "FormatInvoicePDF";
            formatConditionRuleValue25.Condition = DevExpress.XtraEditors.FormatCondition.Expression;
            formatConditionRuleValue25.Expression = "IsNullOrEmpty([InvoicePDF])";
            formatConditionRuleValue25.PredefinedName = "Red Fill";
            gridFormatRule25.Rule = formatConditionRuleValue25;
            gridFormatRule26.Column = this.colLastSentCMReportDate;
            gridFormatRule26.Name = "FormatLastDateSent2";
            formatConditionRuleValue26.Condition = DevExpress.XtraEditors.FormatCondition.Expression;
            formatConditionRuleValue26.Expression = "IsNullOrEmpty([LastSentCMReportDate])";
            formatConditionRuleValue26.PredefinedName = "Red Fill";
            gridFormatRule26.Rule = formatConditionRuleValue26;
            gridFormatRule27.Column = this.colCMReportPDF;
            gridFormatRule27.Name = "FormatCMReportPDF";
            formatConditionRuleValue27.Condition = DevExpress.XtraEditors.FormatCondition.Expression;
            formatConditionRuleValue27.Expression = "IsNullOrEmpty([CMReportPDF])";
            formatConditionRuleValue27.PredefinedName = "Red Fill";
            gridFormatRule27.Rule = formatConditionRuleValue27;
            this.gridViewInvoice.FormatRules.Add(gridFormatRule24);
            this.gridViewInvoice.FormatRules.Add(gridFormatRule25);
            this.gridViewInvoice.FormatRules.Add(gridFormatRule26);
            this.gridViewInvoice.FormatRules.Add(gridFormatRule27);
            this.gridViewInvoice.GridControl = this.gridControlInvoice;
            this.gridViewInvoice.GroupCount = 2;
            this.gridViewInvoice.Name = "gridViewInvoice";
            this.gridViewInvoice.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewInvoice.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewInvoice.OptionsFind.AlwaysVisible = true;
            this.gridViewInvoice.OptionsFind.FindDelay = 2000;
            this.gridViewInvoice.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewInvoice.OptionsLayout.StoreAppearance = true;
            this.gridViewInvoice.OptionsLayout.StoreFormatRules = true;
            this.gridViewInvoice.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewInvoice.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewInvoice.OptionsSelection.MultiSelect = true;
            this.gridViewInvoice.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewInvoice.OptionsView.ColumnAutoWidth = false;
            this.gridViewInvoice.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewInvoice.OptionsView.ShowGroupPanel = false;
            this.gridViewInvoice.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLabourType1, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLabourName3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateRaised, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewInvoice.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewInvoice_CustomRowCellEdit);
            this.gridViewInvoice.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewInvoice.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewInvoice.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewInvoice.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewInvoice_ShowingEditor);
            this.gridViewInvoice.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewInvoice.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewInvoice.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewInvoice.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewInvoice_MouseUp);
            this.gridViewInvoice.GotFocus += new System.EventHandler(this.gridViewInvoice_GotFocus);
            // 
            // colSelfBillingInvoiceHeaderID1
            // 
            this.colSelfBillingInvoiceHeaderID1.Caption = "Self Billing Invoice Header ID";
            this.colSelfBillingInvoiceHeaderID1.FieldName = "SelfBillingInvoiceHeaderID";
            this.colSelfBillingInvoiceHeaderID1.Name = "colSelfBillingInvoiceHeaderID1";
            this.colSelfBillingInvoiceHeaderID1.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceHeaderID1.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceHeaderID1.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceHeaderID1.Width = 156;
            // 
            // colSelfBillingInvoiceNumber
            // 
            this.colSelfBillingInvoiceNumber.Caption = "Self Billing Invoice Number";
            this.colSelfBillingInvoiceNumber.FieldName = "SelfBillingInvoiceNumber";
            this.colSelfBillingInvoiceNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSelfBillingInvoiceNumber.Name = "colSelfBillingInvoiceNumber";
            this.colSelfBillingInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceNumber.Visible = true;
            this.colSelfBillingInvoiceNumber.VisibleIndex = 1;
            this.colSelfBillingInvoiceNumber.Width = 153;
            // 
            // colLinkedToLabourID
            // 
            this.colLinkedToLabourID.Caption = "Labour ID";
            this.colLinkedToLabourID.FieldName = "LinkedToLabourID";
            this.colLinkedToLabourID.Name = "colLinkedToLabourID";
            this.colLinkedToLabourID.OptionsColumn.AllowEdit = false;
            this.colLinkedToLabourID.OptionsColumn.AllowFocus = false;
            this.colLinkedToLabourID.OptionsColumn.ReadOnly = true;
            // 
            // colLinkedToLabourTypeID
            // 
            this.colLinkedToLabourTypeID.Caption = "Labour Type ID";
            this.colLinkedToLabourTypeID.FieldName = "LinkedToLabourTypeID";
            this.colLinkedToLabourTypeID.Name = "colLinkedToLabourTypeID";
            this.colLinkedToLabourTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToLabourTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToLabourTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToLabourTypeID.Width = 93;
            // 
            // colDateRaised
            // 
            this.colDateRaised.Caption = "Date Raised";
            this.colDateRaised.ColumnEdit = this.repositoryItemTextEditDateTime5;
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 0;
            this.colDateRaised.Width = 150;
            // 
            // colTotalBillAmount
            // 
            this.colTotalBillAmount.Caption = "Total Bill Amount";
            this.colTotalBillAmount.ColumnEdit = this.repositoryItemTextEditCurrency5;
            this.colTotalBillAmount.FieldName = "TotalBillAmount";
            this.colTotalBillAmount.Name = "colTotalBillAmount";
            this.colTotalBillAmount.OptionsColumn.AllowEdit = false;
            this.colTotalBillAmount.OptionsColumn.AllowFocus = false;
            this.colTotalBillAmount.OptionsColumn.ReadOnly = true;
            this.colTotalBillAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalBillAmount", "{0:c}")});
            this.colTotalBillAmount.Visible = true;
            this.colTotalBillAmount.VisibleIndex = 2;
            this.colTotalBillAmount.Width = 98;
            // 
            // repositoryItemTextEditCurrency5
            // 
            this.repositoryItemTextEditCurrency5.AutoHeight = false;
            this.repositoryItemTextEditCurrency5.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency5.Name = "repositoryItemTextEditCurrency5";
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 8;
            // 
            // colLabourType1
            // 
            this.colLabourType1.Caption = "Labour Type";
            this.colLabourType1.FieldName = "LabourType";
            this.colLabourType1.Name = "colLabourType1";
            this.colLabourType1.OptionsColumn.AllowEdit = false;
            this.colLabourType1.OptionsColumn.AllowFocus = false;
            this.colLabourType1.OptionsColumn.ReadOnly = true;
            this.colLabourType1.Visible = true;
            this.colLabourType1.VisibleIndex = 6;
            this.colLabourType1.Width = 146;
            // 
            // colLabourName3
            // 
            this.colLabourName3.Caption = "Labour Name";
            this.colLabourName3.FieldName = "LabourName";
            this.colLabourName3.Name = "colLabourName3";
            this.colLabourName3.OptionsColumn.AllowEdit = false;
            this.colLabourName3.OptionsColumn.AllowFocus = false;
            this.colLabourName3.OptionsColumn.ReadOnly = true;
            this.colLabourName3.Visible = true;
            this.colLabourName3.VisibleIndex = 6;
            this.colLabourName3.Width = 313;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone";
            this.colTelephone1.FieldName = "Telephone";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Visible = true;
            this.colTelephone1.VisibleIndex = 9;
            // 
            // colEmail1
            // 
            this.colEmail1.Caption = "Team Email";
            this.colEmail1.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.colEmail1.FieldName = "Email";
            this.colEmail1.Name = "colEmail1";
            this.colEmail1.OptionsColumn.ReadOnly = true;
            this.colEmail1.Visible = true;
            this.colEmail1.VisibleIndex = 10;
            this.colEmail1.Width = 128;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // colSelected2
            // 
            this.colSelected2.Caption = "Selected";
            this.colSelected2.FieldName = "Selected";
            this.colSelected2.Name = "colSelected2";
            this.colSelected2.OptionsColumn.AllowEdit = false;
            this.colSelected2.OptionsColumn.AllowFocus = false;
            this.colSelected2.OptionsColumn.ReadOnly = true;
            this.colSelected2.Width = 60;
            // 
            // colLinkedVisitCount
            // 
            this.colLinkedVisitCount.Caption = "Linked Visits";
            this.colLinkedVisitCount.ColumnEdit = this.repositoryItemHyperLinkEditVisitCount;
            this.colLinkedVisitCount.FieldName = "LinkedVisitCount";
            this.colLinkedVisitCount.Name = "colLinkedVisitCount";
            this.colLinkedVisitCount.OptionsColumn.ReadOnly = true;
            this.colLinkedVisitCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinkedVisitCount", "{0:0}")});
            this.colLinkedVisitCount.Visible = true;
            this.colLinkedVisitCount.VisibleIndex = 7;
            // 
            // repositoryItemHyperLinkEditVisitCount
            // 
            this.repositoryItemHyperLinkEditVisitCount.AutoHeight = false;
            this.repositoryItemHyperLinkEditVisitCount.Name = "repositoryItemHyperLinkEditVisitCount";
            this.repositoryItemHyperLinkEditVisitCount.SingleClick = true;
            this.repositoryItemHyperLinkEditVisitCount.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditVisitCount_OpenLink);
            // 
            // colEmailPassword
            // 
            this.colEmailPassword.Caption = "Email Password";
            this.colEmailPassword.FieldName = "EmailPassword";
            this.colEmailPassword.Name = "colEmailPassword";
            this.colEmailPassword.OptionsColumn.AllowEdit = false;
            this.colEmailPassword.OptionsColumn.AllowFocus = false;
            this.colEmailPassword.OptionsColumn.ReadOnly = true;
            this.colEmailPassword.Visible = true;
            this.colEmailPassword.VisibleIndex = 11;
            this.colEmailPassword.Width = 92;
            // 
            // colContractManagerEmail
            // 
            this.colContractManagerEmail.Caption = "CM Email";
            this.colContractManagerEmail.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.colContractManagerEmail.FieldName = "ContractManagerEmail";
            this.colContractManagerEmail.Name = "colContractManagerEmail";
            this.colContractManagerEmail.OptionsColumn.ReadOnly = true;
            this.colContractManagerEmail.Visible = true;
            this.colContractManagerEmail.VisibleIndex = 12;
            this.colContractManagerEmail.Width = 121;
            // 
            // colLinkedFinanceExportedCount
            // 
            this.colLinkedFinanceExportedCount.Caption = "Finance Visits Exported";
            this.colLinkedFinanceExportedCount.FieldName = "LinkedFinanceExportedCount";
            this.colLinkedFinanceExportedCount.Name = "colLinkedFinanceExportedCount";
            this.colLinkedFinanceExportedCount.OptionsColumn.AllowEdit = false;
            this.colLinkedFinanceExportedCount.OptionsColumn.AllowFocus = false;
            this.colLinkedFinanceExportedCount.OptionsColumn.ReadOnly = true;
            this.colLinkedFinanceExportedCount.Width = 130;
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            this.repositoryItemCheckEdit7.ValueChecked = 1;
            this.repositoryItemCheckEdit7.ValueUnchecked = 0;
            // 
            // standaloneBarDockControl2
            // 
            this.standaloneBarDockControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl2.CausesValidation = false;
            this.standaloneBarDockControl2.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl2.Manager = this.barManager1;
            this.standaloneBarDockControl2.Name = "standaloneBarDockControl2";
            this.standaloneBarDockControl2.Size = new System.Drawing.Size(1356, 42);
            this.standaloneBarDockControl2.Text = "standaloneBarDockControl2";
            // 
            // xtraTabPageInvoiceDesigner
            // 
            this.xtraTabPageInvoiceDesigner.Controls.Add(this.documentViewer1);
            this.xtraTabPageInvoiceDesigner.Controls.Add(this.ribbonStatusBar1);
            this.xtraTabPageInvoiceDesigner.Controls.Add(this.ribbonControl1);
            this.xtraTabPageInvoiceDesigner.Name = "xtraTabPageInvoiceDesigner";
            this.xtraTabPageInvoiceDesigner.Size = new System.Drawing.Size(1356, 702);
            this.xtraTabPageInvoiceDesigner.Text = "Invoice Designer";
            // 
            // documentViewer1
            // 
            this.documentViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.documentViewer1.IsMetric = true;
            this.documentViewer1.Location = new System.Drawing.Point(0, 142);
            this.documentViewer1.Name = "documentViewer1";
            this.documentViewer1.Size = new System.Drawing.Size(1356, 537);
            this.documentViewer1.TabIndex = 0;
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.ItemLinks.Add(this.printPreviewStaticItem1);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItem1, true);
            this.ribbonStatusBar1.ItemLinks.Add(this.progressBarEditItem1);
            this.ribbonStatusBar1.ItemLinks.Add(this.printPreviewBarItem49);
            this.ribbonStatusBar1.ItemLinks.Add(this.printPreviewStaticItem2);
            this.ribbonStatusBar1.ItemLinks.Add(this.zoomTrackBarEditItem1);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 679);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbonControl1;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(1356, 23);
            // 
            // printPreviewStaticItem1
            // 
            this.printPreviewStaticItem1.Caption = "Nothing";
            this.printPreviewStaticItem1.Id = 49;
            this.printPreviewStaticItem1.LeftIndent = 1;
            this.printPreviewStaticItem1.Name = "printPreviewStaticItem1";
            this.printPreviewStaticItem1.RightIndent = 1;
            this.printPreviewStaticItem1.Type = "PageOfPages";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Id = 50;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInRuntime;
            // 
            // progressBarEditItem1
            // 
            this.progressBarEditItem1.ContextSpecifier = this.documentViewerRibbonController1;
            this.progressBarEditItem1.Edit = this.repositoryItemProgressBar1;
            this.progressBarEditItem1.EditHeight = 12;
            this.progressBarEditItem1.EditWidth = 150;
            this.progressBarEditItem1.Id = 51;
            this.progressBarEditItem1.Name = "progressBarEditItem1";
            this.progressBarEditItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // documentViewerRibbonController1
            // 
            this.documentViewerRibbonController1.DocumentViewer = this.documentViewer1;
            this.documentViewerRibbonController1.RibbonControl = this.ribbonControl1;
            this.documentViewerRibbonController1.RibbonStatusBar = this.ribbonStatusBar1;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.AutoHideEmptyItems = true;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.printPreviewBarItem1,
            this.printPreviewBarItem2,
            this.printPreviewBarItem3,
            this.printPreviewBarItem4,
            this.printPreviewBarItem5,
            this.printPreviewBarItem6,
            this.printPreviewBarItem7,
            this.printPreviewBarItem8,
            this.printPreviewBarItem9,
            this.printPreviewBarItem10,
            this.printPreviewBarItem11,
            this.printPreviewBarItem12,
            this.printPreviewBarItem13,
            this.printPreviewBarItem14,
            this.printPreviewBarItem15,
            this.printPreviewBarItem16,
            this.printPreviewBarItem17,
            this.printPreviewBarItem18,
            this.printPreviewBarItem19,
            this.printPreviewBarItem20,
            this.printPreviewBarItem21,
            this.printPreviewBarItem22,
            this.printPreviewBarItem23,
            this.printPreviewBarItem24,
            this.printPreviewBarItem25,
            this.printPreviewBarItem26,
            this.printPreviewBarItem27,
            this.printPreviewBarItem28,
            this.printPreviewBarItem29,
            this.printPreviewBarItem30,
            this.printPreviewBarItem31,
            this.printPreviewBarItem32,
            this.printPreviewBarItem33,
            this.printPreviewBarItem34,
            this.printPreviewBarItem35,
            this.printPreviewBarItem36,
            this.printPreviewBarItem37,
            this.printPreviewBarItem38,
            this.printPreviewBarItem39,
            this.printPreviewBarItem40,
            this.printPreviewBarItem41,
            this.printPreviewBarItem42,
            this.printPreviewBarItem43,
            this.printPreviewBarItem44,
            this.printPreviewBarItem45,
            this.printPreviewBarItem46,
            this.printPreviewStaticItem1,
            this.barStaticItem1,
            this.progressBarEditItem1,
            this.printPreviewBarItem49,
            this.printPreviewStaticItem2,
            this.zoomTrackBarEditItem1,
            this.bbiBack,
            this.bbiEditReportLayout,
            this.bbiEditCMReportLayout});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 195;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemProgressBar1,
            this.repositoryItemZoomTrackBar1});
            this.ribbonControl1.Size = new System.Drawing.Size(1356, 142);
            this.ribbonControl1.StatusBar = this.ribbonStatusBar1;
            this.ribbonControl1.TransparentEditorsMode = DevExpress.Utils.DefaultBoolean.True;
            // 
            // printPreviewBarItem1
            // 
            this.printPreviewBarItem1.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem1.Caption = "Bookmarks";
            this.printPreviewBarItem1.Command = DevExpress.XtraPrinting.PrintingSystemCommand.DocumentMap;
            this.printPreviewBarItem1.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem1.Enabled = false;
            this.printPreviewBarItem1.Id = 1;
            this.printPreviewBarItem1.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_DocumentMap;
            this.printPreviewBarItem1.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_DocumentMapLarge;
            this.printPreviewBarItem1.Name = "printPreviewBarItem1";
            superToolTip16.FixedTooltipWidth = true;
            toolTipTitleItem16.Text = "Document Map";
            toolTipItem16.LeftIndent = 6;
            toolTipItem16.Text = "Open the Document Map, which allows you to navigate through a structural view of " +
    "the document.";
            superToolTip16.Items.Add(toolTipTitleItem16);
            superToolTip16.Items.Add(toolTipItem16);
            superToolTip16.MaxWidth = 210;
            this.printPreviewBarItem1.SuperTip = superToolTip16;
            // 
            // printPreviewBarItem2
            // 
            this.printPreviewBarItem2.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem2.Caption = "Parameters";
            this.printPreviewBarItem2.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Parameters;
            this.printPreviewBarItem2.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem2.Enabled = false;
            this.printPreviewBarItem2.Id = 2;
            this.printPreviewBarItem2.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Parameters;
            this.printPreviewBarItem2.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ParametersLarge;
            this.printPreviewBarItem2.Name = "printPreviewBarItem2";
            superToolTip17.FixedTooltipWidth = true;
            toolTipTitleItem17.Text = "Parameters";
            toolTipItem17.LeftIndent = 6;
            toolTipItem17.Text = "Open the Parameters pane, which allows you to enter values for report parameters." +
    "";
            superToolTip17.Items.Add(toolTipTitleItem17);
            superToolTip17.Items.Add(toolTipItem17);
            superToolTip17.MaxWidth = 210;
            this.printPreviewBarItem2.SuperTip = superToolTip17;
            // 
            // printPreviewBarItem3
            // 
            this.printPreviewBarItem3.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem3.Caption = "Find";
            this.printPreviewBarItem3.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Find;
            this.printPreviewBarItem3.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem3.Enabled = false;
            this.printPreviewBarItem3.Id = 3;
            this.printPreviewBarItem3.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Find;
            this.printPreviewBarItem3.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_FindLarge;
            this.printPreviewBarItem3.Name = "printPreviewBarItem3";
            superToolTip18.FixedTooltipWidth = true;
            toolTipTitleItem18.Text = "Find";
            toolTipItem18.LeftIndent = 6;
            toolTipItem18.Text = "Show the Find dialog to find text in the document.";
            superToolTip18.Items.Add(toolTipTitleItem18);
            superToolTip18.Items.Add(toolTipItem18);
            superToolTip18.MaxWidth = 210;
            this.printPreviewBarItem3.SuperTip = superToolTip18;
            // 
            // printPreviewBarItem4
            // 
            this.printPreviewBarItem4.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem4.Caption = "Thumbnails";
            this.printPreviewBarItem4.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Thumbnails;
            this.printPreviewBarItem4.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem4.Enabled = false;
            this.printPreviewBarItem4.Id = 4;
            this.printPreviewBarItem4.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Thumbnails;
            this.printPreviewBarItem4.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ThumbnailsLarge;
            this.printPreviewBarItem4.Name = "printPreviewBarItem4";
            superToolTip19.FixedTooltipWidth = true;
            toolTipTitleItem19.Text = "Thumbnails";
            toolTipItem19.LeftIndent = 6;
            toolTipItem19.Text = "Open the Thumbnails, which allows you to navigate through the document.";
            superToolTip19.Items.Add(toolTipTitleItem19);
            superToolTip19.Items.Add(toolTipItem19);
            superToolTip19.MaxWidth = 210;
            this.printPreviewBarItem4.SuperTip = superToolTip19;
            // 
            // printPreviewBarItem5
            // 
            this.printPreviewBarItem5.Caption = "Options";
            this.printPreviewBarItem5.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Customize;
            this.printPreviewBarItem5.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem5.Enabled = false;
            this.printPreviewBarItem5.Id = 5;
            this.printPreviewBarItem5.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Customize;
            this.printPreviewBarItem5.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_CustomizeLarge;
            this.printPreviewBarItem5.Name = "printPreviewBarItem5";
            superToolTip20.FixedTooltipWidth = true;
            toolTipTitleItem20.Text = "Options";
            toolTipItem20.LeftIndent = 6;
            toolTipItem20.Text = "Open the Print Options dialog, in which you can change printing options.";
            superToolTip20.Items.Add(toolTipTitleItem20);
            superToolTip20.Items.Add(toolTipItem20);
            superToolTip20.MaxWidth = 210;
            this.printPreviewBarItem5.SuperTip = superToolTip20;
            // 
            // printPreviewBarItem6
            // 
            this.printPreviewBarItem6.Caption = "Print";
            this.printPreviewBarItem6.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Print;
            this.printPreviewBarItem6.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem6.Enabled = false;
            this.printPreviewBarItem6.Id = 6;
            this.printPreviewBarItem6.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Print;
            this.printPreviewBarItem6.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PrintLarge;
            this.printPreviewBarItem6.Name = "printPreviewBarItem6";
            superToolTip21.FixedTooltipWidth = true;
            toolTipTitleItem21.Text = "Print (Ctrl+P)";
            toolTipItem21.LeftIndent = 6;
            toolTipItem21.Text = "Select a printer, number of copies and other printing options before printing.";
            superToolTip21.Items.Add(toolTipTitleItem21);
            superToolTip21.Items.Add(toolTipItem21);
            superToolTip21.MaxWidth = 210;
            this.printPreviewBarItem6.SuperTip = superToolTip21;
            // 
            // printPreviewBarItem7
            // 
            this.printPreviewBarItem7.Caption = "Quick Print";
            this.printPreviewBarItem7.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PrintDirect;
            this.printPreviewBarItem7.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem7.Enabled = false;
            this.printPreviewBarItem7.Id = 7;
            this.printPreviewBarItem7.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PrintDirect;
            this.printPreviewBarItem7.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PrintDirectLarge;
            this.printPreviewBarItem7.Name = "printPreviewBarItem7";
            superToolTip22.FixedTooltipWidth = true;
            toolTipTitleItem22.Text = "Quick Print";
            toolTipItem22.LeftIndent = 6;
            toolTipItem22.Text = "Send the document directly to the default printer without making changes.";
            superToolTip22.Items.Add(toolTipTitleItem22);
            superToolTip22.Items.Add(toolTipItem22);
            superToolTip22.MaxWidth = 210;
            this.printPreviewBarItem7.SuperTip = superToolTip22;
            // 
            // printPreviewBarItem8
            // 
            this.printPreviewBarItem8.Caption = "Custom Margins...";
            this.printPreviewBarItem8.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageSetup;
            this.printPreviewBarItem8.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem8.Enabled = false;
            this.printPreviewBarItem8.Id = 8;
            this.printPreviewBarItem8.Name = "printPreviewBarItem8";
            superToolTip23.FixedTooltipWidth = true;
            toolTipTitleItem23.Text = "Page Setup";
            toolTipItem23.LeftIndent = 6;
            toolTipItem23.Text = "Show the Page Setup dialog.";
            superToolTip23.Items.Add(toolTipTitleItem23);
            superToolTip23.Items.Add(toolTipItem23);
            superToolTip23.MaxWidth = 210;
            this.printPreviewBarItem8.SuperTip = superToolTip23;
            // 
            // printPreviewBarItem9
            // 
            this.printPreviewBarItem9.Caption = "Header/Footer";
            this.printPreviewBarItem9.Command = DevExpress.XtraPrinting.PrintingSystemCommand.EditPageHF;
            this.printPreviewBarItem9.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem9.Enabled = false;
            this.printPreviewBarItem9.Id = 9;
            this.printPreviewBarItem9.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_EditPageHF;
            this.printPreviewBarItem9.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_EditPageHFLarge;
            this.printPreviewBarItem9.Name = "printPreviewBarItem9";
            superToolTip24.FixedTooltipWidth = true;
            toolTipTitleItem24.Text = "Header and Footer";
            toolTipItem24.LeftIndent = 6;
            toolTipItem24.Text = "Edit the header and footer of the document.";
            superToolTip24.Items.Add(toolTipTitleItem24);
            superToolTip24.Items.Add(toolTipItem24);
            superToolTip24.MaxWidth = 210;
            this.printPreviewBarItem9.SuperTip = superToolTip24;
            // 
            // printPreviewBarItem10
            // 
            this.printPreviewBarItem10.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem10.Caption = "Scale";
            this.printPreviewBarItem10.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Scale;
            this.printPreviewBarItem10.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem10.Enabled = false;
            this.printPreviewBarItem10.Id = 10;
            this.printPreviewBarItem10.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Scale;
            this.printPreviewBarItem10.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ScaleLarge;
            this.printPreviewBarItem10.Name = "printPreviewBarItem10";
            superToolTip25.FixedTooltipWidth = true;
            toolTipTitleItem25.Text = "Scale";
            toolTipItem25.LeftIndent = 6;
            toolTipItem25.Text = "Stretch or shrink the printed output to a percentage of its actual size.";
            superToolTip25.Items.Add(toolTipTitleItem25);
            superToolTip25.Items.Add(toolTipItem25);
            superToolTip25.MaxWidth = 210;
            this.printPreviewBarItem10.SuperTip = superToolTip25;
            // 
            // printPreviewBarItem11
            // 
            this.printPreviewBarItem11.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem11.Caption = "Pointer";
            this.printPreviewBarItem11.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Pointer;
            this.printPreviewBarItem11.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem11.Down = true;
            this.printPreviewBarItem11.Enabled = false;
            this.printPreviewBarItem11.GroupIndex = 1;
            this.printPreviewBarItem11.Id = 11;
            this.printPreviewBarItem11.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Pointer;
            this.printPreviewBarItem11.Name = "printPreviewBarItem11";
            this.printPreviewBarItem11.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            superToolTip26.FixedTooltipWidth = true;
            toolTipTitleItem26.Text = "Mouse Pointer";
            toolTipItem26.LeftIndent = 6;
            toolTipItem26.Text = "Show the mouse pointer.";
            superToolTip26.Items.Add(toolTipTitleItem26);
            superToolTip26.Items.Add(toolTipItem26);
            superToolTip26.MaxWidth = 210;
            this.printPreviewBarItem11.SuperTip = superToolTip26;
            // 
            // printPreviewBarItem12
            // 
            this.printPreviewBarItem12.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem12.Caption = "Hand Tool";
            this.printPreviewBarItem12.Command = DevExpress.XtraPrinting.PrintingSystemCommand.HandTool;
            this.printPreviewBarItem12.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem12.Enabled = false;
            this.printPreviewBarItem12.GroupIndex = 1;
            this.printPreviewBarItem12.Id = 12;
            this.printPreviewBarItem12.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_HandTool;
            this.printPreviewBarItem12.Name = "printPreviewBarItem12";
            this.printPreviewBarItem12.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            superToolTip27.FixedTooltipWidth = true;
            toolTipTitleItem27.Text = "Hand Tool";
            toolTipItem27.LeftIndent = 6;
            toolTipItem27.Text = "Invoke the Hand tool to manually scroll through pages.";
            superToolTip27.Items.Add(toolTipTitleItem27);
            superToolTip27.Items.Add(toolTipItem27);
            superToolTip27.MaxWidth = 210;
            this.printPreviewBarItem12.SuperTip = superToolTip27;
            // 
            // printPreviewBarItem13
            // 
            this.printPreviewBarItem13.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem13.Caption = "Magnifier";
            this.printPreviewBarItem13.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Magnifier;
            this.printPreviewBarItem13.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem13.Enabled = false;
            this.printPreviewBarItem13.GroupIndex = 1;
            this.printPreviewBarItem13.Id = 13;
            this.printPreviewBarItem13.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Magnifier;
            this.printPreviewBarItem13.Name = "printPreviewBarItem13";
            this.printPreviewBarItem13.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            superToolTip28.FixedTooltipWidth = true;
            toolTipTitleItem28.Text = "Magnifier";
            toolTipItem28.LeftIndent = 6;
            toolTipItem28.Text = "Invoke the Magnifier tool.\r\n\r\nClicking once on a document zooms it so that a sing" +
    "le page becomes entirely visible, while clicking another time zooms it to 100% o" +
    "f the normal size.";
            superToolTip28.Items.Add(toolTipTitleItem28);
            superToolTip28.Items.Add(toolTipItem28);
            superToolTip28.MaxWidth = 210;
            this.printPreviewBarItem13.SuperTip = superToolTip28;
            // 
            // printPreviewBarItem14
            // 
            this.printPreviewBarItem14.Caption = "Zoom Out";
            this.printPreviewBarItem14.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ZoomOut;
            this.printPreviewBarItem14.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem14.Enabled = false;
            this.printPreviewBarItem14.Id = 14;
            this.printPreviewBarItem14.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ZoomOut;
            this.printPreviewBarItem14.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ZoomOutLarge;
            this.printPreviewBarItem14.Name = "printPreviewBarItem14";
            superToolTip29.FixedTooltipWidth = true;
            toolTipTitleItem29.Text = "Zoom Out";
            toolTipItem29.LeftIndent = 6;
            toolTipItem29.Text = "Zoom out to see more of the page at a reduced size.";
            superToolTip29.Items.Add(toolTipTitleItem29);
            superToolTip29.Items.Add(toolTipItem29);
            superToolTip29.MaxWidth = 210;
            this.printPreviewBarItem14.SuperTip = superToolTip29;
            // 
            // printPreviewBarItem15
            // 
            this.printPreviewBarItem15.Caption = "Zoom In";
            this.printPreviewBarItem15.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ZoomIn;
            this.printPreviewBarItem15.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem15.Enabled = false;
            this.printPreviewBarItem15.Id = 15;
            this.printPreviewBarItem15.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ZoomIn;
            this.printPreviewBarItem15.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ZoomInLarge;
            this.printPreviewBarItem15.Name = "printPreviewBarItem15";
            superToolTip30.FixedTooltipWidth = true;
            toolTipTitleItem30.Text = "Zoom In";
            toolTipItem30.LeftIndent = 6;
            toolTipItem30.Text = "Zoom in to get a close-up view of the document.";
            superToolTip30.Items.Add(toolTipTitleItem30);
            superToolTip30.Items.Add(toolTipItem30);
            superToolTip30.MaxWidth = 210;
            this.printPreviewBarItem15.SuperTip = superToolTip30;
            // 
            // printPreviewBarItem16
            // 
            this.printPreviewBarItem16.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem16.Caption = "Zoom";
            this.printPreviewBarItem16.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Zoom;
            this.printPreviewBarItem16.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem16.Enabled = false;
            this.printPreviewBarItem16.Id = 16;
            this.printPreviewBarItem16.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Zoom;
            this.printPreviewBarItem16.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ZoomLarge;
            this.printPreviewBarItem16.Name = "printPreviewBarItem16";
            superToolTip31.FixedTooltipWidth = true;
            toolTipTitleItem31.Text = "Zoom";
            toolTipItem31.LeftIndent = 6;
            toolTipItem31.Text = "Change the zoom level of the document preview.";
            superToolTip31.Items.Add(toolTipTitleItem31);
            superToolTip31.Items.Add(toolTipItem31);
            superToolTip31.MaxWidth = 210;
            this.printPreviewBarItem16.SuperTip = superToolTip31;
            // 
            // printPreviewBarItem17
            // 
            this.printPreviewBarItem17.Caption = "First Page";
            this.printPreviewBarItem17.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowFirstPage;
            this.printPreviewBarItem17.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem17.Enabled = false;
            this.printPreviewBarItem17.Id = 17;
            this.printPreviewBarItem17.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowFirstPage;
            this.printPreviewBarItem17.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowFirstPageLarge;
            this.printPreviewBarItem17.Name = "printPreviewBarItem17";
            superToolTip32.FixedTooltipWidth = true;
            toolTipTitleItem32.Text = "First Page (Ctrl+Home)";
            toolTipItem32.LeftIndent = 6;
            toolTipItem32.Text = "Navigate to the first page of the document.";
            superToolTip32.Items.Add(toolTipTitleItem32);
            superToolTip32.Items.Add(toolTipItem32);
            superToolTip32.MaxWidth = 210;
            this.printPreviewBarItem17.SuperTip = superToolTip32;
            // 
            // printPreviewBarItem18
            // 
            this.printPreviewBarItem18.Caption = "Previous Page";
            this.printPreviewBarItem18.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowPrevPage;
            this.printPreviewBarItem18.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem18.Enabled = false;
            this.printPreviewBarItem18.Id = 18;
            this.printPreviewBarItem18.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowPrevPage;
            this.printPreviewBarItem18.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowPrevPageLarge;
            this.printPreviewBarItem18.Name = "printPreviewBarItem18";
            superToolTip33.FixedTooltipWidth = true;
            toolTipTitleItem33.Text = "Previous Page (PageUp)";
            toolTipItem33.LeftIndent = 6;
            toolTipItem33.Text = "Navigate to the previous page of the document.";
            superToolTip33.Items.Add(toolTipTitleItem33);
            superToolTip33.Items.Add(toolTipItem33);
            superToolTip33.MaxWidth = 210;
            this.printPreviewBarItem18.SuperTip = superToolTip33;
            // 
            // printPreviewBarItem19
            // 
            this.printPreviewBarItem19.Caption = "Next  Page ";
            this.printPreviewBarItem19.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowNextPage;
            this.printPreviewBarItem19.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem19.Enabled = false;
            this.printPreviewBarItem19.Id = 19;
            this.printPreviewBarItem19.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowNextPage;
            this.printPreviewBarItem19.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowNextPageLarge;
            this.printPreviewBarItem19.Name = "printPreviewBarItem19";
            superToolTip34.FixedTooltipWidth = true;
            toolTipTitleItem34.Text = "Next Page (PageDown)";
            toolTipItem34.LeftIndent = 6;
            toolTipItem34.Text = "Navigate to the next page of the document.";
            superToolTip34.Items.Add(toolTipTitleItem34);
            superToolTip34.Items.Add(toolTipItem34);
            superToolTip34.MaxWidth = 210;
            this.printPreviewBarItem19.SuperTip = superToolTip34;
            // 
            // printPreviewBarItem20
            // 
            this.printPreviewBarItem20.Caption = "Last  Page ";
            this.printPreviewBarItem20.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowLastPage;
            this.printPreviewBarItem20.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem20.Enabled = false;
            this.printPreviewBarItem20.Id = 20;
            this.printPreviewBarItem20.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowLastPage;
            this.printPreviewBarItem20.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ShowLastPageLarge;
            this.printPreviewBarItem20.Name = "printPreviewBarItem20";
            superToolTip35.FixedTooltipWidth = true;
            toolTipTitleItem35.Text = "Last Page (Ctrl+End)";
            toolTipItem35.LeftIndent = 6;
            toolTipItem35.Text = "Navigate to the last page of the document.";
            superToolTip35.Items.Add(toolTipTitleItem35);
            superToolTip35.Items.Add(toolTipItem35);
            superToolTip35.MaxWidth = 210;
            this.printPreviewBarItem20.SuperTip = superToolTip35;
            // 
            // printPreviewBarItem21
            // 
            this.printPreviewBarItem21.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem21.Caption = "Many Pages";
            this.printPreviewBarItem21.Command = DevExpress.XtraPrinting.PrintingSystemCommand.MultiplePages;
            this.printPreviewBarItem21.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem21.Enabled = false;
            this.printPreviewBarItem21.Id = 21;
            this.printPreviewBarItem21.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_MultiplePages;
            this.printPreviewBarItem21.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_MultiplePagesLarge;
            this.printPreviewBarItem21.Name = "printPreviewBarItem21";
            superToolTip36.FixedTooltipWidth = true;
            toolTipTitleItem36.Text = "View Many Pages";
            toolTipItem36.LeftIndent = 6;
            toolTipItem36.Text = "Choose the page layout to arrange the document pages in preview.";
            superToolTip36.Items.Add(toolTipTitleItem36);
            superToolTip36.Items.Add(toolTipItem36);
            superToolTip36.MaxWidth = 210;
            this.printPreviewBarItem21.SuperTip = superToolTip36;
            // 
            // printPreviewBarItem22
            // 
            this.printPreviewBarItem22.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem22.Caption = "Page Color";
            this.printPreviewBarItem22.Command = DevExpress.XtraPrinting.PrintingSystemCommand.FillBackground;
            this.printPreviewBarItem22.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem22.Enabled = false;
            this.printPreviewBarItem22.Id = 22;
            this.printPreviewBarItem22.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_FillBackground;
            this.printPreviewBarItem22.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_FillBackgroundLarge;
            this.printPreviewBarItem22.Name = "printPreviewBarItem22";
            superToolTip37.FixedTooltipWidth = true;
            toolTipTitleItem37.Text = "Background Color";
            toolTipItem37.LeftIndent = 6;
            toolTipItem37.Text = "Choose a color for the background of the document pages.";
            superToolTip37.Items.Add(toolTipTitleItem37);
            superToolTip37.Items.Add(toolTipItem37);
            superToolTip37.MaxWidth = 210;
            this.printPreviewBarItem22.SuperTip = superToolTip37;
            // 
            // printPreviewBarItem23
            // 
            this.printPreviewBarItem23.Caption = "Watermark";
            this.printPreviewBarItem23.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Watermark;
            this.printPreviewBarItem23.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem23.Enabled = false;
            this.printPreviewBarItem23.Id = 23;
            this.printPreviewBarItem23.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Watermark;
            this.printPreviewBarItem23.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_WatermarkLarge;
            this.printPreviewBarItem23.Name = "printPreviewBarItem23";
            superToolTip38.FixedTooltipWidth = true;
            toolTipTitleItem38.Text = "Watermark";
            toolTipItem38.LeftIndent = 6;
            toolTipItem38.Text = "Insert ghosted text or image behind the content of a page.\r\n\r\nThis is often used " +
    "to indicate that a document is to be treated specially.";
            superToolTip38.Items.Add(toolTipTitleItem38);
            superToolTip38.Items.Add(toolTipItem38);
            superToolTip38.MaxWidth = 210;
            this.printPreviewBarItem23.SuperTip = superToolTip38;
            // 
            // printPreviewBarItem24
            // 
            this.printPreviewBarItem24.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem24.Caption = "Export To";
            this.printPreviewBarItem24.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportFile;
            this.printPreviewBarItem24.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem24.Enabled = false;
            this.printPreviewBarItem24.Id = 24;
            this.printPreviewBarItem24.Name = "printPreviewBarItem24";
            superToolTip39.FixedTooltipWidth = true;
            toolTipTitleItem39.Text = "Export To...";
            toolTipItem39.LeftIndent = 6;
            toolTipItem39.Text = "Export the current document in one of the available formats, and save it to the f" +
    "ile on a disk.";
            superToolTip39.Items.Add(toolTipTitleItem39);
            superToolTip39.Items.Add(toolTipItem39);
            superToolTip39.MaxWidth = 210;
            this.printPreviewBarItem24.SuperTip = superToolTip39;
            // 
            // printPreviewBarItem25
            // 
            this.printPreviewBarItem25.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem25.Caption = "E-Mail As";
            this.printPreviewBarItem25.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendFile;
            this.printPreviewBarItem25.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem25.Enabled = false;
            this.printPreviewBarItem25.Id = 25;
            this.printPreviewBarItem25.Name = "printPreviewBarItem25";
            superToolTip40.FixedTooltipWidth = true;
            toolTipTitleItem40.Text = "E-Mail As...";
            toolTipItem40.LeftIndent = 6;
            toolTipItem40.Text = "Export the current document in one of the available formats, and attach it to the" +
    " e-mail.";
            superToolTip40.Items.Add(toolTipTitleItem40);
            superToolTip40.Items.Add(toolTipItem40);
            superToolTip40.MaxWidth = 210;
            this.printPreviewBarItem25.SuperTip = superToolTip40;
            // 
            // printPreviewBarItem26
            // 
            this.printPreviewBarItem26.Caption = "Close";
            this.printPreviewBarItem26.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ClosePreview;
            this.printPreviewBarItem26.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem26.Enabled = false;
            this.printPreviewBarItem26.Id = 26;
            this.printPreviewBarItem26.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ClosePreview;
            this.printPreviewBarItem26.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ClosePreviewLarge;
            this.printPreviewBarItem26.Name = "printPreviewBarItem26";
            superToolTip41.FixedTooltipWidth = true;
            toolTipTitleItem41.Text = "Close Print Preview";
            toolTipItem41.LeftIndent = 6;
            toolTipItem41.Text = "Close Print Preview of the document.";
            superToolTip41.Items.Add(toolTipTitleItem41);
            superToolTip41.Items.Add(toolTipItem41);
            superToolTip41.MaxWidth = 210;
            this.printPreviewBarItem26.SuperTip = superToolTip41;
            // 
            // printPreviewBarItem27
            // 
            this.printPreviewBarItem27.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem27.Caption = "Orientation";
            this.printPreviewBarItem27.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageOrientation;
            this.printPreviewBarItem27.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem27.Enabled = false;
            this.printPreviewBarItem27.Id = 27;
            this.printPreviewBarItem27.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageOrientation;
            this.printPreviewBarItem27.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageOrientationLarge;
            this.printPreviewBarItem27.Name = "printPreviewBarItem27";
            superToolTip42.FixedTooltipWidth = true;
            toolTipTitleItem42.Text = "Page Orientation";
            toolTipItem42.LeftIndent = 6;
            toolTipItem42.Text = "Switch the pages between portrait and landscape layouts.";
            superToolTip42.Items.Add(toolTipTitleItem42);
            superToolTip42.Items.Add(toolTipItem42);
            superToolTip42.MaxWidth = 210;
            this.printPreviewBarItem27.SuperTip = superToolTip42;
            // 
            // printPreviewBarItem28
            // 
            this.printPreviewBarItem28.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem28.Caption = "Size";
            this.printPreviewBarItem28.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PaperSize;
            this.printPreviewBarItem28.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem28.Enabled = false;
            this.printPreviewBarItem28.Id = 28;
            this.printPreviewBarItem28.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PaperSize;
            this.printPreviewBarItem28.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PaperSizeLarge;
            this.printPreviewBarItem28.Name = "printPreviewBarItem28";
            superToolTip43.FixedTooltipWidth = true;
            toolTipTitleItem43.Text = "Page Size";
            toolTipItem43.LeftIndent = 6;
            toolTipItem43.Text = "Choose the paper size of the document.";
            superToolTip43.Items.Add(toolTipTitleItem43);
            superToolTip43.Items.Add(toolTipItem43);
            superToolTip43.MaxWidth = 210;
            this.printPreviewBarItem28.SuperTip = superToolTip43;
            // 
            // printPreviewBarItem29
            // 
            this.printPreviewBarItem29.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem29.Caption = "Margins";
            this.printPreviewBarItem29.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageMargins;
            this.printPreviewBarItem29.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem29.Enabled = false;
            this.printPreviewBarItem29.Id = 29;
            this.printPreviewBarItem29.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageMargins;
            this.printPreviewBarItem29.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageMarginsLarge;
            this.printPreviewBarItem29.Name = "printPreviewBarItem29";
            superToolTip44.FixedTooltipWidth = true;
            toolTipTitleItem44.Text = "Page Margins";
            toolTipItem44.LeftIndent = 6;
            toolTipItem44.Text = "Select the margin sizes for the entire document.\r\n\r\nTo apply specific margin size" +
    "s to the document, click Custom Margins.";
            superToolTip44.Items.Add(toolTipTitleItem44);
            superToolTip44.Items.Add(toolTipItem44);
            superToolTip44.MaxWidth = 210;
            this.printPreviewBarItem29.SuperTip = superToolTip44;
            // 
            // printPreviewBarItem30
            // 
            this.printPreviewBarItem30.Caption = "PDF File";
            this.printPreviewBarItem30.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendPdf;
            this.printPreviewBarItem30.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem30.Description = "Adobe Portable Document Format";
            this.printPreviewBarItem30.Enabled = false;
            this.printPreviewBarItem30.Id = 30;
            this.printPreviewBarItem30.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendPdf;
            this.printPreviewBarItem30.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendPdfLarge;
            this.printPreviewBarItem30.Name = "printPreviewBarItem30";
            superToolTip45.FixedTooltipWidth = true;
            toolTipTitleItem45.Text = "E-Mail As PDF";
            toolTipItem45.LeftIndent = 6;
            toolTipItem45.Text = "Export the document to PDF and attach it to the e-mail.";
            superToolTip45.Items.Add(toolTipTitleItem45);
            superToolTip45.Items.Add(toolTipItem45);
            superToolTip45.MaxWidth = 210;
            this.printPreviewBarItem30.SuperTip = superToolTip45;
            // 
            // printPreviewBarItem31
            // 
            this.printPreviewBarItem31.Caption = "Text File";
            this.printPreviewBarItem31.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendTxt;
            this.printPreviewBarItem31.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem31.Description = "Plain Text";
            this.printPreviewBarItem31.Enabled = false;
            this.printPreviewBarItem31.Id = 31;
            this.printPreviewBarItem31.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendTxt;
            this.printPreviewBarItem31.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendTxtLarge;
            this.printPreviewBarItem31.Name = "printPreviewBarItem31";
            superToolTip46.FixedTooltipWidth = true;
            toolTipTitleItem46.Text = "E-Mail As Text";
            toolTipItem46.LeftIndent = 6;
            toolTipItem46.Text = "Export the document to Text and attach it to the e-mail.";
            superToolTip46.Items.Add(toolTipTitleItem46);
            superToolTip46.Items.Add(toolTipItem46);
            superToolTip46.MaxWidth = 210;
            this.printPreviewBarItem31.SuperTip = superToolTip46;
            // 
            // printPreviewBarItem32
            // 
            this.printPreviewBarItem32.Caption = "CSV File";
            this.printPreviewBarItem32.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendCsv;
            this.printPreviewBarItem32.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem32.Description = "Comma-Separated Values Text";
            this.printPreviewBarItem32.Enabled = false;
            this.printPreviewBarItem32.Id = 32;
            this.printPreviewBarItem32.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendCsv;
            this.printPreviewBarItem32.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendCsvLarge;
            this.printPreviewBarItem32.Name = "printPreviewBarItem32";
            superToolTip47.FixedTooltipWidth = true;
            toolTipTitleItem47.Text = "E-Mail As CSV";
            toolTipItem47.LeftIndent = 6;
            toolTipItem47.Text = "Export the document to CSV and attach it to the e-mail.";
            superToolTip47.Items.Add(toolTipTitleItem47);
            superToolTip47.Items.Add(toolTipItem47);
            superToolTip47.MaxWidth = 210;
            this.printPreviewBarItem32.SuperTip = superToolTip47;
            // 
            // printPreviewBarItem33
            // 
            this.printPreviewBarItem33.Caption = "MHT File";
            this.printPreviewBarItem33.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendMht;
            this.printPreviewBarItem33.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem33.Description = "Single File Web Page";
            this.printPreviewBarItem33.Enabled = false;
            this.printPreviewBarItem33.Id = 33;
            this.printPreviewBarItem33.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendMht;
            this.printPreviewBarItem33.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendMhtLarge;
            this.printPreviewBarItem33.Name = "printPreviewBarItem33";
            superToolTip48.FixedTooltipWidth = true;
            toolTipTitleItem48.Text = "E-Mail As MHT";
            toolTipItem48.LeftIndent = 6;
            toolTipItem48.Text = "Export the document to MHT and attach it to the e-mail.";
            superToolTip48.Items.Add(toolTipTitleItem48);
            superToolTip48.Items.Add(toolTipItem48);
            superToolTip48.MaxWidth = 210;
            this.printPreviewBarItem33.SuperTip = superToolTip48;
            // 
            // printPreviewBarItem34
            // 
            this.printPreviewBarItem34.Caption = "XLS File";
            this.printPreviewBarItem34.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendXls;
            this.printPreviewBarItem34.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem34.Description = "Microsoft Excel 2000-2003 Workbook";
            this.printPreviewBarItem34.Enabled = false;
            this.printPreviewBarItem34.Id = 34;
            this.printPreviewBarItem34.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendXls;
            this.printPreviewBarItem34.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendXlsLarge;
            this.printPreviewBarItem34.Name = "printPreviewBarItem34";
            superToolTip49.FixedTooltipWidth = true;
            toolTipTitleItem49.Text = "E-Mail As XLS";
            toolTipItem49.LeftIndent = 6;
            toolTipItem49.Text = "Export the document to XLS and attach it to the e-mail.";
            superToolTip49.Items.Add(toolTipTitleItem49);
            superToolTip49.Items.Add(toolTipItem49);
            superToolTip49.MaxWidth = 210;
            this.printPreviewBarItem34.SuperTip = superToolTip49;
            // 
            // printPreviewBarItem35
            // 
            this.printPreviewBarItem35.Caption = "XLSX File";
            this.printPreviewBarItem35.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendXlsx;
            this.printPreviewBarItem35.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem35.Description = "Microsoft Excel 2007 Workbook";
            this.printPreviewBarItem35.Enabled = false;
            this.printPreviewBarItem35.Id = 35;
            this.printPreviewBarItem35.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendXlsx;
            this.printPreviewBarItem35.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendXlsxLarge;
            this.printPreviewBarItem35.Name = "printPreviewBarItem35";
            superToolTip50.FixedTooltipWidth = true;
            toolTipTitleItem50.Text = "E-Mail As XLSX";
            toolTipItem50.LeftIndent = 6;
            toolTipItem50.Text = "Export the document to XLSX and attach it to the e-mail.";
            superToolTip50.Items.Add(toolTipTitleItem50);
            superToolTip50.Items.Add(toolTipItem50);
            superToolTip50.MaxWidth = 210;
            this.printPreviewBarItem35.SuperTip = superToolTip50;
            // 
            // printPreviewBarItem36
            // 
            this.printPreviewBarItem36.Caption = "RTF File";
            this.printPreviewBarItem36.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendRtf;
            this.printPreviewBarItem36.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem36.Description = "Rich Text Format";
            this.printPreviewBarItem36.Enabled = false;
            this.printPreviewBarItem36.Id = 36;
            this.printPreviewBarItem36.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendRtf;
            this.printPreviewBarItem36.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendRtfLarge;
            this.printPreviewBarItem36.Name = "printPreviewBarItem36";
            superToolTip51.FixedTooltipWidth = true;
            toolTipTitleItem51.Text = "E-Mail As RTF";
            toolTipItem51.LeftIndent = 6;
            toolTipItem51.Text = "Export the document to RTF and attach it to the e-mail.";
            superToolTip51.Items.Add(toolTipTitleItem51);
            superToolTip51.Items.Add(toolTipItem51);
            superToolTip51.MaxWidth = 210;
            this.printPreviewBarItem36.SuperTip = superToolTip51;
            // 
            // printPreviewBarItem37
            // 
            this.printPreviewBarItem37.Caption = "Image File";
            this.printPreviewBarItem37.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendGraphic;
            this.printPreviewBarItem37.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem37.Description = "BMP, GIF, JPEG, PNG, TIFF, EMF, WMF";
            this.printPreviewBarItem37.Enabled = false;
            this.printPreviewBarItem37.Id = 37;
            this.printPreviewBarItem37.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendGraphic;
            this.printPreviewBarItem37.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_SendGraphicLarge;
            this.printPreviewBarItem37.Name = "printPreviewBarItem37";
            superToolTip52.FixedTooltipWidth = true;
            toolTipTitleItem52.Text = "E-Mail As Image";
            toolTipItem52.LeftIndent = 6;
            toolTipItem52.Text = "Export the document to Image and attach it to the e-mail.";
            superToolTip52.Items.Add(toolTipTitleItem52);
            superToolTip52.Items.Add(toolTipItem52);
            superToolTip52.MaxWidth = 210;
            this.printPreviewBarItem37.SuperTip = superToolTip52;
            // 
            // printPreviewBarItem38
            // 
            this.printPreviewBarItem38.Caption = "PDF File";
            this.printPreviewBarItem38.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportPdf;
            this.printPreviewBarItem38.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem38.Description = "Adobe Portable Document Format";
            this.printPreviewBarItem38.Enabled = false;
            this.printPreviewBarItem38.Id = 38;
            this.printPreviewBarItem38.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportPdf;
            this.printPreviewBarItem38.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportPdfLarge;
            this.printPreviewBarItem38.Name = "printPreviewBarItem38";
            superToolTip53.FixedTooltipWidth = true;
            toolTipTitleItem53.Text = "Export to PDF";
            toolTipItem53.LeftIndent = 6;
            toolTipItem53.Text = "Export the document to PDF and save it to the file on a disk.";
            superToolTip53.Items.Add(toolTipTitleItem53);
            superToolTip53.Items.Add(toolTipItem53);
            superToolTip53.MaxWidth = 210;
            this.printPreviewBarItem38.SuperTip = superToolTip53;
            // 
            // printPreviewBarItem39
            // 
            this.printPreviewBarItem39.Caption = "HTML File";
            this.printPreviewBarItem39.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportHtm;
            this.printPreviewBarItem39.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem39.Description = "Web Page";
            this.printPreviewBarItem39.Enabled = false;
            this.printPreviewBarItem39.Id = 39;
            this.printPreviewBarItem39.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportHtm;
            this.printPreviewBarItem39.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportHtmLarge;
            this.printPreviewBarItem39.Name = "printPreviewBarItem39";
            superToolTip54.FixedTooltipWidth = true;
            toolTipTitleItem54.Text = "Export to HTML";
            toolTipItem54.LeftIndent = 6;
            toolTipItem54.Text = "Export the document to HTML and save it to the file on a disk.";
            superToolTip54.Items.Add(toolTipTitleItem54);
            superToolTip54.Items.Add(toolTipItem54);
            superToolTip54.MaxWidth = 210;
            this.printPreviewBarItem39.SuperTip = superToolTip54;
            // 
            // printPreviewBarItem40
            // 
            this.printPreviewBarItem40.Caption = "Text File";
            this.printPreviewBarItem40.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportTxt;
            this.printPreviewBarItem40.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem40.Description = "Plain Text";
            this.printPreviewBarItem40.Enabled = false;
            this.printPreviewBarItem40.Id = 40;
            this.printPreviewBarItem40.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportTxt;
            this.printPreviewBarItem40.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportTxtLarge;
            this.printPreviewBarItem40.Name = "printPreviewBarItem40";
            superToolTip55.FixedTooltipWidth = true;
            toolTipTitleItem55.Text = "Export to Text";
            toolTipItem55.LeftIndent = 6;
            toolTipItem55.Text = "Export the document to Text and save it to the file on a disk.";
            superToolTip55.Items.Add(toolTipTitleItem55);
            superToolTip55.Items.Add(toolTipItem55);
            superToolTip55.MaxWidth = 210;
            this.printPreviewBarItem40.SuperTip = superToolTip55;
            // 
            // printPreviewBarItem41
            // 
            this.printPreviewBarItem41.Caption = "CSV File";
            this.printPreviewBarItem41.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportCsv;
            this.printPreviewBarItem41.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem41.Description = "Comma-Separated Values Text";
            this.printPreviewBarItem41.Enabled = false;
            this.printPreviewBarItem41.Id = 41;
            this.printPreviewBarItem41.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportCsv;
            this.printPreviewBarItem41.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportCsvLarge;
            this.printPreviewBarItem41.Name = "printPreviewBarItem41";
            superToolTip56.FixedTooltipWidth = true;
            toolTipTitleItem56.Text = "Export to CSV";
            toolTipItem56.LeftIndent = 6;
            toolTipItem56.Text = "Export the document to CSV and save it to the file on a disk.";
            superToolTip56.Items.Add(toolTipTitleItem56);
            superToolTip56.Items.Add(toolTipItem56);
            superToolTip56.MaxWidth = 210;
            this.printPreviewBarItem41.SuperTip = superToolTip56;
            // 
            // printPreviewBarItem42
            // 
            this.printPreviewBarItem42.Caption = "MHT File";
            this.printPreviewBarItem42.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportMht;
            this.printPreviewBarItem42.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem42.Description = "Single File Web Page";
            this.printPreviewBarItem42.Enabled = false;
            this.printPreviewBarItem42.Id = 42;
            this.printPreviewBarItem42.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportMht;
            this.printPreviewBarItem42.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportMhtLarge;
            this.printPreviewBarItem42.Name = "printPreviewBarItem42";
            superToolTip57.FixedTooltipWidth = true;
            toolTipTitleItem57.Text = "Export to MHT";
            toolTipItem57.LeftIndent = 6;
            toolTipItem57.Text = "Export the document to MHT and save it to the file on a disk.";
            superToolTip57.Items.Add(toolTipTitleItem57);
            superToolTip57.Items.Add(toolTipItem57);
            superToolTip57.MaxWidth = 210;
            this.printPreviewBarItem42.SuperTip = superToolTip57;
            // 
            // printPreviewBarItem43
            // 
            this.printPreviewBarItem43.Caption = "XLS File";
            this.printPreviewBarItem43.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportXls;
            this.printPreviewBarItem43.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem43.Description = "Microsoft Excel 2000-2003 Workbook";
            this.printPreviewBarItem43.Enabled = false;
            this.printPreviewBarItem43.Id = 43;
            this.printPreviewBarItem43.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportXls;
            this.printPreviewBarItem43.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportXlsLarge;
            this.printPreviewBarItem43.Name = "printPreviewBarItem43";
            superToolTip58.FixedTooltipWidth = true;
            toolTipTitleItem58.Text = "Export to XLS";
            toolTipItem58.LeftIndent = 6;
            toolTipItem58.Text = "Export the document to XLS and save it to the file on a disk.";
            superToolTip58.Items.Add(toolTipTitleItem58);
            superToolTip58.Items.Add(toolTipItem58);
            superToolTip58.MaxWidth = 210;
            this.printPreviewBarItem43.SuperTip = superToolTip58;
            // 
            // printPreviewBarItem44
            // 
            this.printPreviewBarItem44.Caption = "XLSX File";
            this.printPreviewBarItem44.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportXlsx;
            this.printPreviewBarItem44.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem44.Description = "Microsoft Excel 2007 Workbook";
            this.printPreviewBarItem44.Enabled = false;
            this.printPreviewBarItem44.Id = 44;
            this.printPreviewBarItem44.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportXlsx;
            this.printPreviewBarItem44.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportXlsxLarge;
            this.printPreviewBarItem44.Name = "printPreviewBarItem44";
            superToolTip59.FixedTooltipWidth = true;
            toolTipTitleItem59.Text = "Export to XLSX";
            toolTipItem59.LeftIndent = 6;
            toolTipItem59.Text = "Export the document to XLSX and save it to the file on a disk.";
            superToolTip59.Items.Add(toolTipTitleItem59);
            superToolTip59.Items.Add(toolTipItem59);
            superToolTip59.MaxWidth = 210;
            this.printPreviewBarItem44.SuperTip = superToolTip59;
            // 
            // printPreviewBarItem45
            // 
            this.printPreviewBarItem45.Caption = "RTF File";
            this.printPreviewBarItem45.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportRtf;
            this.printPreviewBarItem45.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem45.Description = "Rich Text Format";
            this.printPreviewBarItem45.Enabled = false;
            this.printPreviewBarItem45.Id = 45;
            this.printPreviewBarItem45.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportRtf;
            this.printPreviewBarItem45.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportRtfLarge;
            this.printPreviewBarItem45.Name = "printPreviewBarItem45";
            superToolTip60.FixedTooltipWidth = true;
            toolTipTitleItem60.Text = "Export to RTF";
            toolTipItem60.LeftIndent = 6;
            toolTipItem60.Text = "Export the document to RTF and save it to the file on a disk.";
            superToolTip60.Items.Add(toolTipTitleItem60);
            superToolTip60.Items.Add(toolTipItem60);
            superToolTip60.MaxWidth = 210;
            this.printPreviewBarItem45.SuperTip = superToolTip60;
            // 
            // printPreviewBarItem46
            // 
            this.printPreviewBarItem46.Caption = "Image File";
            this.printPreviewBarItem46.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportGraphic;
            this.printPreviewBarItem46.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem46.Description = "BMP, GIF, JPEG, PNG, TIFF, EMF, WMF";
            this.printPreviewBarItem46.Enabled = false;
            this.printPreviewBarItem46.Id = 46;
            this.printPreviewBarItem46.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportGraphic;
            this.printPreviewBarItem46.ImageOptions.LargeImage = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportGraphicLarge;
            this.printPreviewBarItem46.Name = "printPreviewBarItem46";
            superToolTip61.FixedTooltipWidth = true;
            toolTipTitleItem61.Text = "Export to Image";
            toolTipItem61.LeftIndent = 6;
            toolTipItem61.Text = "Export the document to Image and save it to the file on a disk.";
            superToolTip61.Items.Add(toolTipTitleItem61);
            superToolTip61.Items.Add(toolTipItem61);
            superToolTip61.MaxWidth = 210;
            this.printPreviewBarItem46.SuperTip = superToolTip61;
            // 
            // printPreviewBarItem49
            // 
            this.printPreviewBarItem49.Caption = "Stop";
            this.printPreviewBarItem49.Command = DevExpress.XtraPrinting.PrintingSystemCommand.StopPageBuilding;
            this.printPreviewBarItem49.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewBarItem49.Enabled = false;
            this.printPreviewBarItem49.Hint = "Stop";
            this.printPreviewBarItem49.Id = 52;
            this.printPreviewBarItem49.Name = "printPreviewBarItem49";
            this.printPreviewBarItem49.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // printPreviewStaticItem2
            // 
            this.printPreviewStaticItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.printPreviewStaticItem2.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.printPreviewStaticItem2.Caption = "100%";
            this.printPreviewStaticItem2.Id = 54;
            this.printPreviewStaticItem2.Name = "printPreviewStaticItem2";
            this.printPreviewStaticItem2.Type = "ZoomFactorText";
            // 
            // zoomTrackBarEditItem1
            // 
            this.zoomTrackBarEditItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.zoomTrackBarEditItem1.ContextSpecifier = this.documentViewerRibbonController1;
            this.zoomTrackBarEditItem1.Edit = this.repositoryItemZoomTrackBar1;
            this.zoomTrackBarEditItem1.EditValue = 90;
            this.zoomTrackBarEditItem1.EditWidth = 140;
            this.zoomTrackBarEditItem1.Enabled = false;
            this.zoomTrackBarEditItem1.Id = 55;
            this.zoomTrackBarEditItem1.Name = "zoomTrackBarEditItem1";
            this.zoomTrackBarEditItem1.Range = new int[] {
        10,
        500};
            // 
            // repositoryItemZoomTrackBar1
            // 
            this.repositoryItemZoomTrackBar1.Alignment = DevExpress.Utils.VertAlignment.Center;
            this.repositoryItemZoomTrackBar1.AllowFocused = false;
            this.repositoryItemZoomTrackBar1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemZoomTrackBar1.Maximum = 180;
            this.repositoryItemZoomTrackBar1.Name = "repositoryItemZoomTrackBar1";
            // 
            // bbiBack
            // 
            this.bbiBack.Caption = "Back";
            this.bbiBack.Id = 57;
            this.bbiBack.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBack.ImageOptions.Image")));
            this.bbiBack.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiBack.ImageOptions.LargeImage")));
            this.bbiBack.Name = "bbiBack";
            toolTipTitleItem62.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem62.Appearance.Options.UseImage = true;
            toolTipTitleItem62.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem62.Text = "Back - Information";
            toolTipItem62.LeftIndent = 6;
            toolTipItem62.Text = "Click me to return back to the Checking Page.";
            superToolTip62.Items.Add(toolTipTitleItem62);
            superToolTip62.Items.Add(toolTipItem62);
            this.bbiBack.SuperTip = superToolTip62;
            this.bbiBack.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBack_ItemClick);
            // 
            // bbiEditReportLayout
            // 
            this.bbiEditReportLayout.Caption = "Edit Invoice Layout";
            this.bbiEditReportLayout.Id = 59;
            this.bbiEditReportLayout.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiEditReportLayout.ImageOptions.Image")));
            this.bbiEditReportLayout.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiEditReportLayout.ImageOptions.LargeImage")));
            this.bbiEditReportLayout.Name = "bbiEditReportLayout";
            toolTipTitleItem63.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem63.Appearance.Options.UseImage = true;
            toolTipTitleItem63.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem63.Text = "Edit Layout - Information";
            toolTipItem63.LeftIndent = 6;
            toolTipItem63.Text = "Click me to edit the Self-Billing Invoice layout in the Report Designer.";
            superToolTip63.Items.Add(toolTipTitleItem63);
            superToolTip63.Items.Add(toolTipItem63);
            this.bbiEditReportLayout.SuperTip = superToolTip63;
            this.bbiEditReportLayout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEditReportLayout_ItemClick);
            // 
            // bbiEditCMReportLayout
            // 
            this.bbiEditCMReportLayout.Caption = "Edit CM Report Layout";
            this.bbiEditCMReportLayout.Id = 158;
            this.bbiEditCMReportLayout.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiEditCMReportLayout.ImageOptions.Image")));
            this.bbiEditCMReportLayout.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiEditCMReportLayout.ImageOptions.LargeImage")));
            this.bbiEditCMReportLayout.Name = "bbiEditCMReportLayout";
            toolTipTitleItem64.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem64.Appearance.Options.UseImage = true;
            toolTipTitleItem64.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem64.Text = "Edit Layout - Information";
            toolTipItem64.LeftIndent = 6;
            toolTipItem64.Text = "Click me to edit the CM Invoice report layout in the Report Designer.";
            superToolTip64.Items.Add(toolTipTitleItem64);
            superToolTip64.Items.Add(toolTipItem64);
            this.bbiEditCMReportLayout.SuperTip = superToolTip64;
            this.bbiEditCMReportLayout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEditCMReportLayout_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.ContextSpecifier = this.documentViewerRibbonController1;
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.printPreviewRibbonPageGroup1,
            this.printPreviewRibbonPageGroup2,
            this.printPreviewRibbonPageGroup3,
            this.printPreviewRibbonPageGroup4,
            this.printPreviewRibbonPageGroup5,
            this.printPreviewRibbonPageGroup6,
            this.printPreviewRibbonPageGroup7,
            this.printPreviewRibbonPageGroup8});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Print Preview";
            // 
            // printPreviewRibbonPageGroup1
            // 
            this.printPreviewRibbonPageGroup1.AllowTextClipping = false;
            this.printPreviewRibbonPageGroup1.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewRibbonPageGroup1.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Document;
            this.printPreviewRibbonPageGroup1.ItemLinks.Add(this.bbiBack);
            this.printPreviewRibbonPageGroup1.ItemLinks.Add(this.bbiEditReportLayout);
            this.printPreviewRibbonPageGroup1.ItemLinks.Add(this.bbiEditCMReportLayout);
            this.printPreviewRibbonPageGroup1.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Document;
            this.printPreviewRibbonPageGroup1.Name = "printPreviewRibbonPageGroup1";
            this.printPreviewRibbonPageGroup1.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup1.Text = "Document";
            // 
            // printPreviewRibbonPageGroup2
            // 
            this.printPreviewRibbonPageGroup2.AllowTextClipping = false;
            this.printPreviewRibbonPageGroup2.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewRibbonPageGroup2.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PrintDirect;
            this.printPreviewRibbonPageGroup2.ItemLinks.Add(this.printPreviewBarItem6);
            this.printPreviewRibbonPageGroup2.ItemLinks.Add(this.printPreviewBarItem7);
            this.printPreviewRibbonPageGroup2.ItemLinks.Add(this.printPreviewBarItem5);
            this.printPreviewRibbonPageGroup2.ItemLinks.Add(this.printPreviewBarItem2);
            this.printPreviewRibbonPageGroup2.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Print;
            this.printPreviewRibbonPageGroup2.Name = "printPreviewRibbonPageGroup2";
            this.printPreviewRibbonPageGroup2.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup2.Text = "Print";
            // 
            // printPreviewRibbonPageGroup3
            // 
            this.printPreviewRibbonPageGroup3.AllowTextClipping = false;
            this.printPreviewRibbonPageGroup3.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewRibbonPageGroup3.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PageMargins;
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem9);
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem10);
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem29);
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem27);
            this.printPreviewRibbonPageGroup3.ItemLinks.Add(this.printPreviewBarItem28);
            this.printPreviewRibbonPageGroup3.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.PageSetup;
            this.printPreviewRibbonPageGroup3.Name = "printPreviewRibbonPageGroup3";
            superToolTip65.FixedTooltipWidth = true;
            toolTipTitleItem65.Text = "Page Setup";
            toolTipItem65.LeftIndent = 6;
            toolTipItem65.Text = "Show the Page Setup dialog.";
            superToolTip65.Items.Add(toolTipTitleItem65);
            superToolTip65.Items.Add(toolTipItem65);
            superToolTip65.MaxWidth = 210;
            this.printPreviewRibbonPageGroup3.SuperTip = superToolTip65;
            this.printPreviewRibbonPageGroup3.Text = "Page Setup";
            // 
            // printPreviewRibbonPageGroup4
            // 
            this.printPreviewRibbonPageGroup4.AllowTextClipping = false;
            this.printPreviewRibbonPageGroup4.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewRibbonPageGroup4.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Find;
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem3);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem4);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem1);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem17, true);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem18);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem19);
            this.printPreviewRibbonPageGroup4.ItemLinks.Add(this.printPreviewBarItem20);
            this.printPreviewRibbonPageGroup4.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Navigation;
            this.printPreviewRibbonPageGroup4.Name = "printPreviewRibbonPageGroup4";
            this.printPreviewRibbonPageGroup4.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup4.Text = "Navigation";
            // 
            // printPreviewRibbonPageGroup5
            // 
            this.printPreviewRibbonPageGroup5.AllowTextClipping = false;
            this.printPreviewRibbonPageGroup5.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewRibbonPageGroup5.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Zoom;
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem11);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem12);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem13);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem21);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem14);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem16);
            this.printPreviewRibbonPageGroup5.ItemLinks.Add(this.printPreviewBarItem15);
            this.printPreviewRibbonPageGroup5.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Zoom;
            this.printPreviewRibbonPageGroup5.Name = "printPreviewRibbonPageGroup5";
            this.printPreviewRibbonPageGroup5.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup5.Text = "Zoom";
            // 
            // printPreviewRibbonPageGroup6
            // 
            this.printPreviewRibbonPageGroup6.AllowTextClipping = false;
            this.printPreviewRibbonPageGroup6.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewRibbonPageGroup6.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Watermark;
            this.printPreviewRibbonPageGroup6.ItemLinks.Add(this.printPreviewBarItem22);
            this.printPreviewRibbonPageGroup6.ItemLinks.Add(this.printPreviewBarItem23);
            this.printPreviewRibbonPageGroup6.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Background;
            this.printPreviewRibbonPageGroup6.Name = "printPreviewRibbonPageGroup6";
            this.printPreviewRibbonPageGroup6.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup6.Text = "Page Background";
            // 
            // printPreviewRibbonPageGroup7
            // 
            this.printPreviewRibbonPageGroup7.AllowTextClipping = false;
            this.printPreviewRibbonPageGroup7.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewRibbonPageGroup7.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportFile;
            this.printPreviewRibbonPageGroup7.ItemLinks.Add(this.printPreviewBarItem24);
            this.printPreviewRibbonPageGroup7.ItemLinks.Add(this.printPreviewBarItem25);
            this.printPreviewRibbonPageGroup7.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Export;
            this.printPreviewRibbonPageGroup7.Name = "printPreviewRibbonPageGroup7";
            this.printPreviewRibbonPageGroup7.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup7.Text = "Export";
            // 
            // printPreviewRibbonPageGroup8
            // 
            this.printPreviewRibbonPageGroup8.AllowTextClipping = false;
            this.printPreviewRibbonPageGroup8.ContextSpecifier = this.documentViewerRibbonController1;
            this.printPreviewRibbonPageGroup8.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ClosePreview;
            this.printPreviewRibbonPageGroup8.ItemLinks.Add(this.printPreviewBarItem26);
            this.printPreviewRibbonPageGroup8.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Close;
            this.printPreviewRibbonPageGroup8.Name = "printPreviewRibbonPageGroup8";
            this.printPreviewRibbonPageGroup8.ShowCaptionButton = false;
            this.printPreviewRibbonPageGroup8.Text = "Close";
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            // 
            // sp06451_OM_Self_Billing_Billing_Warnings_FilterTableAdapter
            // 
            this.sp06451_OM_Self_Billing_Billing_Warnings_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06452_OM_Billing_Manager_LabourTableAdapter
            // 
            this.sp06452_OM_Billing_Manager_LabourTableAdapter.ClearBeforeFill = true;
            // 
            // sp06454_OM_Self_Billing_Jobs_Linked_to_VisitsTableAdapter
            // 
            this.sp06454_OM_Self_Billing_Jobs_Linked_to_VisitsTableAdapter.ClearBeforeFill = true;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 3";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar2.FloatLocation = new System.Drawing.Point(593, 203);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiFilterSelected2),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSelectedCount3, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh2),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiRecreatePDF, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSendToTeam, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDeleteInvoice)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DisableClose = true;
            this.bar2.OptionsBar.DisableCustomization = true;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.StandaloneBarDockControl = this.standaloneBarDockControl2;
            this.bar2.Text = "Custom 3";
            // 
            // bsiFilterSelected2
            // 
            this.bsiFilterSelected2.Caption = "Filter Selected";
            this.bsiFilterSelected2.Id = 143;
            this.bsiFilterSelected2.ImageOptions.ImageIndex = 0;
            this.bsiFilterSelected2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterTeamsSelected2),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterInvoicesSelected)});
            this.bsiFilterSelected2.Name = "bsiFilterSelected2";
            this.bsiFilterSelected2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            // 
            // bciFilterTeamsSelected2
            // 
            this.bciFilterTeamsSelected2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bciFilterTeamsSelected2.Caption = "Filter Selected <b>Teams</b>";
            this.bciFilterTeamsSelected2.Id = 144;
            this.bciFilterTeamsSelected2.ImageOptions.ImageIndex = 0;
            this.bciFilterTeamsSelected2.Name = "bciFilterTeamsSelected2";
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Filter Selected Teams - Information";
            toolTipItem7.LeftIndent = 6;
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.bciFilterTeamsSelected2.SuperTip = superToolTip7;
            this.bciFilterTeamsSelected2.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterTeamsSelected2_CheckedChanged);
            // 
            // bciFilterInvoicesSelected
            // 
            this.bciFilterInvoicesSelected.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bciFilterInvoicesSelected.Caption = "Filter  Selected <b>Invoices</b>";
            this.bciFilterInvoicesSelected.Id = 145;
            this.bciFilterInvoicesSelected.ImageOptions.ImageIndex = 0;
            this.bciFilterInvoicesSelected.Name = "bciFilterInvoicesSelected";
            this.bciFilterInvoicesSelected.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Text = "Filter Selected Invoices - Information";
            toolTipItem8.LeftIndent = 6;
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bciFilterInvoicesSelected.SuperTip = superToolTip8;
            this.bciFilterInvoicesSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterInvoicesSelected_CheckedChanged);
            // 
            // bsiSelectedCount3
            // 
            this.bsiSelectedCount3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSelectedCount3.Caption = "0 Teams Selected";
            this.bsiSelectedCount3.Id = 142;
            this.bsiSelectedCount3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_16x16;
            this.bsiSelectedCount3.ItemAppearance.Disabled.Options.UseTextOptions = true;
            this.bsiSelectedCount3.ItemAppearance.Disabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount3.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.bsiSelectedCount3.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount3.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.bsiSelectedCount3.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount3.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.bsiSelectedCount3.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount3.Name = "bsiSelectedCount3";
            this.bsiSelectedCount3.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiSelectedCount3.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bbiRefresh2
            // 
            this.bbiRefresh2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh2.Caption = "Refresh";
            this.bbiRefresh2.Id = 146;
            this.bbiRefresh2.ImageOptions.ImageIndex = 1;
            this.bbiRefresh2.Name = "bbiRefresh2";
            this.bbiRefresh2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh2_ItemClick);
            // 
            // bsiRecreatePDF
            // 
            this.bsiRecreatePDF.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bsiRecreatePDF.Caption = "Re-Create PDF";
            this.bsiRecreatePDF.Id = 150;
            this.bsiRecreatePDF.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiRecreatePDF.ImageOptions.Image")));
            this.bsiRecreatePDF.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectInvoicesMissingPDFs),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRecreatePDF, true)});
            this.bsiRecreatePDF.Name = "bsiRecreatePDF";
            this.bsiRecreatePDF.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiSelectInvoicesMissingPDFs
            // 
            this.bbiSelectInvoicesMissingPDFs.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSelectInvoicesMissingPDFs.Caption = "<b>Select</b> Invoices Without PDF Files";
            this.bbiSelectInvoicesMissingPDFs.Id = 151;
            this.bbiSelectInvoicesMissingPDFs.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectInvoicesMissingPDFs.ImageOptions.Image")));
            this.bbiSelectInvoicesMissingPDFs.Name = "bbiSelectInvoicesMissingPDFs";
            this.bbiSelectInvoicesMissingPDFs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectInvoicesMissingPDFs_ItemClick);
            // 
            // bbiRecreatePDF
            // 
            this.bbiRecreatePDF.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRecreatePDF.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiRecreatePDF.Caption = "<b>Re-Create</b> PDFs";
            this.bbiRecreatePDF.Enabled = false;
            this.bbiRecreatePDF.Id = 147;
            this.bbiRecreatePDF.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRecreatePDF.ImageOptions.Image")));
            this.bbiRecreatePDF.Name = "bbiRecreatePDF";
            this.bbiRecreatePDF.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRecreatePDF.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRecreatePDF_ItemClick);
            // 
            // bsiSendToTeam
            // 
            this.bsiSendToTeam.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bsiSendToTeam.Caption = "Send";
            this.bsiSendToTeam.Id = 152;
            this.bsiSendToTeam.ImageOptions.ImageIndex = 3;
            this.bsiSendToTeam.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiUnsentInvoices),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiUnsentReports, true)});
            this.bsiSendToTeam.Name = "bsiSendToTeam";
            this.bsiSendToTeam.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bsiUnsentInvoices
            // 
            this.bsiUnsentInvoices.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiUnsentInvoices.Caption = "<b>Team</b> Invoices";
            this.bsiUnsentInvoices.Id = 154;
            this.bsiUnsentInvoices.ImageOptions.ImageIndex = 3;
            this.bsiUnsentInvoices.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectUnsentInvoices),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiResend)});
            this.bsiUnsentInvoices.Name = "bsiUnsentInvoices";
            // 
            // bbiSelectUnsentInvoices
            // 
            this.bbiSelectUnsentInvoices.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSelectUnsentInvoices.Caption = "<b>Select</b> Un-sent Invoices";
            this.bbiSelectUnsentInvoices.Id = 153;
            this.bbiSelectUnsentInvoices.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectUnsentInvoices.ImageOptions.Image")));
            this.bbiSelectUnsentInvoices.Name = "bbiSelectUnsentInvoices";
            this.bbiSelectUnsentInvoices.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectUnsentInvoices_ItemClick);
            // 
            // bbiResend
            // 
            this.bbiResend.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiResend.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiResend.Caption = "<b>Send</b> Selected Invoices To Team \\ Contract Manager";
            this.bbiResend.Enabled = false;
            this.bbiResend.Id = 148;
            this.bbiResend.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiResend.ImageOptions.Image")));
            this.bbiResend.Name = "bbiResend";
            this.bbiResend.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiResend.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiResend_ItemClick);
            // 
            // bsiUnsentReports
            // 
            this.bsiUnsentReports.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiUnsentReports.Caption = "<b>Contract Manager</b> Reports";
            this.bsiUnsentReports.Id = 155;
            this.bsiUnsentReports.ImageOptions.ImageIndex = 4;
            this.bsiUnsentReports.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectUnsentInvoiceReports),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiResendCM)});
            this.bsiUnsentReports.Name = "bsiUnsentReports";
            // 
            // bbiSelectUnsentInvoiceReports
            // 
            this.bbiSelectUnsentInvoiceReports.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSelectUnsentInvoiceReports.Caption = "<b>Select</b> Un-sent CM Reports";
            this.bbiSelectUnsentInvoiceReports.Id = 156;
            this.bbiSelectUnsentInvoiceReports.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectUnsentInvoiceReports.ImageOptions.Image")));
            this.bbiSelectUnsentInvoiceReports.Name = "bbiSelectUnsentInvoiceReports";
            this.bbiSelectUnsentInvoiceReports.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectUnsentInvoiceReports_ItemClick);
            // 
            // bbiResendCM
            // 
            this.bbiResendCM.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiResendCM.Caption = "<b>Send</b> Selected CM Reports To Contract Manager";
            this.bbiResendCM.Enabled = false;
            this.bbiResendCM.Id = 157;
            this.bbiResendCM.ImageOptions.ImageIndex = 4;
            this.bbiResendCM.Name = "bbiResendCM";
            this.bbiResendCM.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiResendCM_ItemClick);
            // 
            // bbiDeleteInvoice
            // 
            this.bbiDeleteInvoice.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiDeleteInvoice.Caption = "Delete Invoice";
            this.bbiDeleteInvoice.Enabled = false;
            this.bbiDeleteInvoice.Id = 149;
            this.bbiDeleteInvoice.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDeleteInvoice.ImageOptions.Image")));
            this.bbiDeleteInvoice.Name = "bbiDeleteInvoice";
            this.bbiDeleteInvoice.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip9.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem9.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Text = "Delete Invoice - Information";
            toolTipItem9.LeftIndent = 6;
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.bbiDeleteInvoice.SuperTip = superToolTip9;
            this.bbiDeleteInvoice.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDeleteInvoice_ItemClick);
            // 
            // sp06463_OM_Billing_Manager_Invoiced_LabourTableAdapter
            // 
            this.sp06463_OM_Billing_Manager_Invoiced_LabourTableAdapter.ClearBeforeFill = true;
            // 
            // sp06464_OM_Billing_Manager_Invoices_For_LabourTableAdapter
            // 
            this.sp06464_OM_Billing_Manager_Invoices_For_LabourTableAdapter.ClearBeforeFill = true;
            // 
            // sp06477_OM_Get_Unique_Self_Billing_Invoice_ListTableAdapter
            // 
            this.sp06477_OM_Get_Unique_Self_Billing_Invoice_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp06528_OM_Visit_Categories_FilterTableAdapter
            // 
            this.sp06528_OM_Visit_Categories_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Team_Self_Billing_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1384, 728);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.hideContainerLeft);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Team_Self_Billing_Manager";
            this.Text = "Team Self-Billing Manager - Operations";
            this.Activated += new System.EventHandler(this.frm_OM_Team_Self_Billing_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Team_Self_Billing_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Team_Self_Billing_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.hideContainerLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditAuthorised)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditInvoicePDF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditCMReportPDF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditClientSignature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditSelfBillingInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditClientInvoiceID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditPictureLink)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVisit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06445OMTeamSelfBillingManagerVisitsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Billing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewVisit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlInvoicingParameters)).EndInit();
            this.popupContainerControlInvoicingParameters.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAutoSendCreatedInvoices.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditExcludeVisitWithRequirements.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditExcludeVisitsWithWarnings.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditExcludeTeamsWithOutstandingRequirements.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditExcludeTeamsWithOutstandingWarnings.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLabour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06452OMBillingManagerLabourBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLabour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBillingRequirement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06446OMTeamSelfBillingBillingRequirementsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBillingRequirement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBillingException)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06448OMTeamSelfBillingBillingExceptionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBillingException)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06454OMSelfBillingJobsLinkedtoVisitsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsJobs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditIntegerDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditPictures)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditViewJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLabourFilter)).EndInit();
            this.popupContainerControlLabourFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06028OMLabourFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Summer_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlWarningFilter)).EndInit();
            this.popupContainerControlWarningFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06451OMSelfBillingBillingWarningsFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlVisitTypeFilter)).EndInit();
            this.popupContainerControlVisitTypeFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06308OMVisitTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlVisitCategoryFilter)).EndInit();
            this.popupContainerControlVisitCategoryFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06528OMVisitCategoriesFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06404OMVisitSystemStatusFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06357OMVisitManagerLinkedVisitLabourBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditInvoicingParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditIgnoreHistoric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06145OMVisitEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.hideContainerLeft.ResumeLayout(false);
            this.dockPanelFilters.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditSBNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditVisitCategoryFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDoNotPay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditInvoiceNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlInvoiceNumber)).EndInit();
            this.popupContainerControlInvoiceNumber.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06477OMGetUniqueSelfBillingInvoiceListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditExcludeClientFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditHistoricalDateRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlHistoricalDateRange)).EndInit();
            this.popupContainerControlHistoricalDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditHistoricalFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditHistoricalFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditHistoricalToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditHistoricalToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditWarningsFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIncludeUnpaidHistorical.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditYear.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditWeekNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDateRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditWeekNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditSiteID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditStaff.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTeam.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditVisitTypeFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditLabourFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSiteFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditDateRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShowInvoiced.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupFilters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupChecking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWeekNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforSBNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShowInvoiced)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLabourFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupHistorical)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHistoricalDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLoadHistorical)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvoiceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageChecking.ResumeLayout(false);
            this.xtraTabPageHistoricalInvoices.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).EndInit();
            this.splitContainerControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLabour2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06463OMBillingManagerInvoicedLabourBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLabour2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06464OMBillingManagerInvoicesForLabourBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditVisitCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            this.xtraTabPageInvoiceDesigner.ResumeLayout(false);
            this.xtraTabPageInvoiceDesigner.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentViewerRibbonController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControlVisit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewVisit;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private WoodPlan5.DataSet_AT dataSet_AT;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraGrid.GridControl gridControlJob;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewJob;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditIgnoreHistoric;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocumentsJobs;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage2;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCalculationLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCost;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitSell;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCalculationLevelDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractValue;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDirectorID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractActive;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractValue;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colContractType;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDirector;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedJobCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedOutstandingJobCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedCompletedJobCount;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID1;
        private DevExpress.XtraGrid.Columns.GridColumn colReactive;
        private DevExpress.XtraGrid.Columns.GridColumn colJobNoLongerRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colRequiresAccessPermit;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessPermitID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceSystemPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnitsDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colScheduleSentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnitsDescriptionID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalMaterialExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalMaterialVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalMaterialCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalMaterialExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalMaterialVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalMaterialCost;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalEquipmentExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalEquipmentVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalEquipmentExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalEquipmentVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalLabourExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalLabourVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalLabourExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalLabourVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalCostExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalCostVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalCostExVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalCostVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateClientInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceReceivedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoicePaidDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceAmountPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPayContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClient;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colRouteOrder;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditLatLong2;
        private DevExpress.XtraGrid.Columns.GridColumn colJobStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName1;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedDurationUnitsDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colActualDurationUnitsDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordType;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colFullDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderID;
        private DevExpress.XtraGrid.Columns.GridColumn colTenderJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumDaysFromLastVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colMaximumDaysFromLastVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysLeeway;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditViewJob;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraScheduler.UI.RepositoryItemDuration repositoryItemDuration1;
        private DevExpress.XtraScheduler.UI.ToggleRecurrenceItem toggleRecurrenceItem1;
        private System.Windows.Forms.BindingSource sp06145OMVisitEditBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06145_OM_Visit_EditTableAdapter sp06145_OM_Visit_EditTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalEquipmentVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalLabourVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostTotalMaterialVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalMaterialVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalEquipmentVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellTotalLabourVATRate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID2;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToVisit;
        private DevExpress.XtraGrid.Columns.GridColumn colClientAndContract;
        private DevExpress.XtraBars.BarCheckItem bciFilterJobsSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeliness;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueFound;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueType;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colIssueRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colClientSignaturePath;
        private DevExpress.XtraGrid.Columns.GridColumn colImagesFolderOM;
        private DevExpress.XtraGrid.Columns.GridColumn colRework;
        private DevExpress.XtraGrid.Columns.GridColumn colReworkOriginalJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colRework1;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.XtraBars.BarStaticItem bsiSelectedCount1;
        private DevExpress.XtraGrid.Columns.GridColumn colExtraWorkComments;
        private DevExpress.XtraGrid.Columns.GridColumn colExtraWorkRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colExtraWorkType;
        private DataSet_OM_Job dataSet_OM_Job;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentOnSite;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentComment;
        private DevExpress.XtraGrid.Columns.GridColumn colCancelledReason;
        private DevExpress.XtraGrid.Columns.GridColumn colCancelledReasonID;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysUntilDue;
        private DevExpress.XtraGrid.Columns.GridColumn colManuallyCompleted;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditIntegerDays;
        private DevExpress.XtraGrid.Columns.GridColumn colSuspendedJobCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedPictureCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedPictureCount1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditPictures;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraGrid.Columns.GridColumn colImagesFolderOM1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelFilters;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditLabourFilter;
        private DevExpress.XtraEditors.ButtonEdit buttonEditSiteFilter;
        private DevExpress.XtraEditors.ButtonEdit buttonEditClientFilter;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditDateRange;
        private DevExpress.XtraEditors.CheckEdit checkEditShowInvoiced;
        private DevExpress.XtraEditors.SimpleButton btnLoad;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateRange;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForShowInvoiced;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLabourFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeOK;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateRange;
        private DataSet_GC_Summer_Core dataSet_GC_Summer_Core;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlLabourFilter;
        private DevExpress.XtraEditors.SimpleButton btnLabourFilter;
        private DevExpress.XtraGrid.GridControl gridControl12;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView12;
        private DevExpress.XtraGrid.Columns.GridColumn colID4;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit17;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeDescription;
        private System.Windows.Forms.BindingSource sp06028OMLabourFilterBindingSource;
        private DataSet_GC_Summer_CoreTableAdapters.sp06028_OM_Labour_FilterTableAdapter sp06028_OM_Labour_FilterTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colFriendlyVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colFriendlyVisitNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitType;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitTypeID;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlVisitTypeFilter;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn224;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn226;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn227;
        private DevExpress.XtraEditors.SimpleButton btnVisitTypeFilterOK;
        private System.Windows.Forms.BindingSource sp06308OMVisitTypesBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06308_OM_Visit_TypesTableAdapter sp06308_OM_Visit_TypesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colActive1;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditVisitTypeFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.CheckEdit checkEditTeam;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.CheckEdit checkEditStaff;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPO;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPO1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitType1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colSellCalculationLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colSellCalculationLevelDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected4;
        private DevExpress.XtraBars.BarSubItem bsiFilterSelected;
        private DevExpress.XtraBars.BarCheckItem bciFilterVisitsSelected;
        private System.Windows.Forms.BindingSource sp06357OMVisitManagerLinkedVisitLabourBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06357_OM_Visit_Manager_Linked_Visit_LabourTableAdapter sp06357_OM_Visit_Manager_Linked_Visit_LabourTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientInvoiceID1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCostCalculationLevelID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitSellCalculationLevelID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCostCalculationLevelDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitSellCalculationLevelDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCMName;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysAllowed;
        private DevExpress.XtraGrid.Columns.GridColumn colManagerName;
        private DevExpress.XtraGrid.Columns.GridColumn colManagerNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colNoOnetoSign;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCategory;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML1;
        private DevExpress.XtraGrid.Columns.GridColumn colExternalCustomerStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletionSheetNumber;
        private System.Windows.Forms.BindingSource sp06404OMVisitSystemStatusFilterBindingSource;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusIssueID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusIssue;
        private DevExpress.XtraGrid.Columns.GridColumn colDisplayOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colMandatory;
        private DevExpress.XtraGrid.Columns.GridColumn colSuspendedReason;
        private DevExpress.XtraGrid.Columns.GridColumn colSuspendedRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditPictureLink;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit14;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit20;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit15;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysSeparation;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractDaysSeparationPercent;
        private DevExpress.XtraGrid.GridControl gridControlBillingRequirement;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewBillingRequirement;
        private DataSet_Common_Functionality dataSet_Common_Functionality;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingRequirementID;
        private DevExpress.XtraGrid.Columns.GridColumn colRequirementID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn263;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn264;
        private DevExpress.XtraGrid.Columns.GridColumn colClientBillRequirement;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamSelfBillRequirement;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colRequirementFulfilled;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingRequirement;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecord1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordType1;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckedByStaff;
        private System.Windows.Forms.BindingSource sp06445OMTeamSelfBillingManagerVisitsBindingSource;
        private DataSet_OM_Billing dataSet_OM_Billing;
        private DataSet_OM_BillingTableAdapters.sp06445_OM_Team_Self_Billing_Manager_VisitsTableAdapter sp06445_OM_Team_Self_Billing_Manager_VisitsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingRequirmentsFulfilled;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingRequirmentsUnfulfilled;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingWarningAuthorised;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingWarningUnauthorised;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private System.Windows.Forms.BindingSource sp06446OMTeamSelfBillingBillingRequirementsBindingSource;
        private DataSet_OM_BillingTableAdapters.sp06446_OM_Team_Self_Billing_Billing_RequirementsTableAdapter sp06446_OM_Team_Self_Billing_Billing_RequirementsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditLatLong;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML0;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditGrid1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditClientSignature;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditSelfBillingInvoice;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditClientInvoiceID;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraBars.BarStaticItem bsiSelectedCount2;
        private DevExpress.XtraGrid.GridControl gridControlBillingException;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewBillingException;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingWarningID;
        private DevExpress.XtraGrid.Columns.GridColumn colWarningTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditAuthorised;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colAuthorised;
        private DevExpress.XtraGrid.Columns.GridColumn colAuthorisedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colWarningType;
        private DevExpress.XtraGrid.Columns.GridColumn colVisit2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordType2;
        private DevExpress.XtraGrid.Columns.GridColumn colAuthorisedByStaff;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private System.Windows.Forms.BindingSource sp06448OMTeamSelfBillingBillingExceptionsBindingSource;
        private DataSet_OM_BillingTableAdapters.sp06448_OM_Team_Self_Billing_Billing_ExceptionsTableAdapter sp06448_OM_Team_Self_Billing_Billing_ExceptionsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingTypeID;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageChecking;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageHistoricalInvoices;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML3;
        private DevExpress.XtraEditors.SpinEdit spinEditSiteID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SpinEdit spinEditYear;
        private DevExpress.XtraEditors.SpinEdit spinEditWeekNumber;
        private DevExpress.XtraEditors.CheckEdit checkEditDateRange;
        private DevExpress.XtraEditors.CheckEdit checkEditWeekNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWeekNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForYear;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit21;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraEditors.CheckEdit checkEditIncludeUnpaidHistorical;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditWarningsFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraGrid.GridControl gridControlLabour;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLabour;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit8;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlWarningFilter;
        private DevExpress.XtraEditors.SimpleButton btnWarningFilter;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private System.Windows.Forms.BindingSource sp06451OMSelfBillingBillingWarningsFilterBindingSource;
        private DataSet_OM_BillingTableAdapters.sp06451_OM_Self_Billing_Billing_Warnings_FilterTableAdapter sp06451_OM_Self_Billing_Billing_Warnings_FilterTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private System.Windows.Forms.BindingSource sp06452OMBillingManagerLabourBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourType;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourID;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourName;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DataSet_OM_BillingTableAdapters.sp06452_OM_Billing_Manager_LabourTableAdapter sp06452_OM_Billing_Manager_LabourTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourName1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRequirementCount;
        private DevExpress.XtraGrid.Columns.GridColumn colRequirementFulfilledCount;
        private DevExpress.XtraGrid.Columns.GridColumn colRequirementUnfulfilledCount;
        private DevExpress.XtraGrid.Columns.GridColumn colWarningCount;
        private DevExpress.XtraGrid.Columns.GridColumn colWarningAuthorisedCount;
        private DevExpress.XtraGrid.Columns.GridColumn colWarningUnauthorisedCount;
        private System.Windows.Forms.BindingSource sp06454OMSelfBillingJobsLinkedtoVisitsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourName2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonType1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID1;
        private DataSet_OM_BillingTableAdapters.sp06454_OM_Self_Billing_Jobs_Linked_to_VisitsTableAdapter sp06454_OM_Self_Billing_Jobs_Linked_to_VisitsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourUsedID;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotInvoiceClient1;
        private DevExpress.XtraGrid.Columns.GridColumn colDoNotPayContractor1;
        private DevExpress.XtraBars.BarStaticItem bsiSelectedCount0;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingComment;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingStatus;
        private DevExpress.XtraBars.BarCheckItem bciFilterTeamsSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraBars.BarButtonItem bbiRecalculateTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitLabourCalculatedID;
        private DevExpress.XtraBars.BarEditItem barEditItemInvoicingParameters;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditInvoicingParameters;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlInvoicingParameters;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.CheckEdit checkEditExcludeTeamsWithOutstandingRequirements;
        private DevExpress.XtraEditors.CheckEdit checkEditExcludeTeamsWithOutstandingWarnings;
        private DevExpress.XtraEditors.SimpleButton simpleButtonInvoicingParameters;
        private DevExpress.XtraBars.BarButtonItem bbiInvoice;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageInvoiceDesigner;
        private DevExpress.XtraPrinting.Preview.DocumentViewer documentViewer1;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem1;
        private DevExpress.XtraPrinting.Preview.DocumentViewerRibbonController documentViewerRibbonController1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem printPreviewStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraPrinting.Preview.ProgressBarEditItem progressBarEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem49;
        private DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem printPreviewStaticItem2;
        private DevExpress.XtraPrinting.Preview.ZoomTrackBarEditItem zoomTrackBarEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar repositoryItemZoomTrackBar1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem3;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem4;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem5;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem6;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem7;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem8;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem9;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem10;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem11;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem12;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem13;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem14;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem15;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem16;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem17;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem18;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem19;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem20;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem21;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem22;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem23;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem24;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem25;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem26;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem27;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem28;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem29;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem30;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem31;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem32;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem33;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem34;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem35;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem36;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem37;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem38;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem39;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem40;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem41;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem42;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem43;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem44;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem45;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem46;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage ribbonPage1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup3;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup4;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup5;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup6;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup7;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup printPreviewRibbonPageGroup8;
        private DevExpress.XtraBars.BarButtonItem bbiBack;
        private DevExpress.XtraBars.BarButtonItem bbiEditReportLayout;
        private DevExpress.XtraEditors.CheckEdit checkEditExcludeVisitWithRequirements;
        private DevExpress.XtraEditors.CheckEdit checkEditExcludeVisitsWithWarnings;
        private DevExpress.XtraEditors.CheckEdit checkEditAutoSendCreatedInvoices;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl2;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarSubItem bsiFilterSelected2;
        private DevExpress.XtraBars.BarCheckItem bciFilterTeamsSelected2;
        private DevExpress.XtraBars.BarStaticItem bsiSelectedCount3;
        private DevExpress.XtraBars.BarCheckItem bciFilterInvoicesSelected;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl5;
        private DevExpress.XtraGrid.GridControl gridControlLabour2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLabour2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLabourTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLabourID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn colUnsentCount;
        private DevExpress.XtraGrid.Columns.GridColumn colNoPDFCount;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceCount;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraGrid.GridControl gridControlInvoice;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewInvoice;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private System.Windows.Forms.BindingSource sp06463OMBillingManagerInvoicedLabourBindingSource;
        private DataSet_OM_BillingTableAdapters.sp06463_OM_Billing_Manager_Invoiced_LabourTableAdapter sp06463_OM_Billing_Manager_Invoiced_LabourTableAdapter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlHistoricalDateRange;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.DateEdit dateEditHistoricalFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditHistoricalToDate;
        private DevExpress.XtraEditors.SimpleButton btnHistoricalDateRange;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditHistoricalDateRange;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHistoricalDateRange;
        private DevExpress.XtraEditors.SimpleButton btnLoadHistorical;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroupFilters;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupChecking;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupHistorical;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLoadHistorical;
        private System.Windows.Forms.BindingSource sp06464OMBillingManagerInvoicesForLabourBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceHeaderID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToLabourID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToLabourTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime5;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalBillAmount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency5;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colLastSentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoicePDF;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditInvoicePDF;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourType1;
        private DevExpress.XtraGrid.Columns.GridColumn colLabourName3;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail1;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected2;
        private DataSet_OM_BillingTableAdapters.sp06464_OM_Billing_Manager_Invoices_For_LabourTableAdapter sp06464_OM_Billing_Manager_Invoices_For_LabourTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedVisitCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditVisitCount;
        private DevExpress.XtraBars.BarButtonItem bbiRecreatePDF;
        private DevExpress.XtraBars.BarButtonItem bbiResend;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailPassword;
        private DevExpress.XtraBars.BarButtonItem bbiDeleteInvoice;
        private DevExpress.XtraBars.BarSubItem bsiRecreatePDF;
        private DevExpress.XtraBars.BarButtonItem bbiSelectInvoicesMissingPDFs;
        private DevExpress.XtraBars.BarSubItem bsiSendToTeam;
        private DevExpress.XtraBars.BarButtonItem bbiSelectUnsentInvoices;
        private DevExpress.XtraGrid.Columns.GridColumn colOverdueDays;
        private DevExpress.XtraEditors.ButtonEdit buttonEditExcludeClientFilter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraGrid.Columns.GridColumn colEarlyDays;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlInvoiceNumber;
        private DevExpress.XtraEditors.SimpleButton btnInvoiceNumber;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private System.Windows.Forms.BindingSource sp06477OMGetUniqueSelfBillingInvoiceListBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceNumber1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit8;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditInvoiceNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInvoiceNumber;
        private DataSet_OM_BillingTableAdapters.sp06477_OM_Get_Unique_Self_Billing_Invoice_ListTableAdapter sp06477_OM_Get_Unique_Self_Billing_Invoice_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn colLastSentCMReportDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCMReportPDF;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditCMReportPDF;
        private DevExpress.XtraBars.BarButtonItem bbiEditCMReportLayout;
        private DevExpress.XtraGrid.Columns.GridColumn colContractManagerEmail;
        private DevExpress.XtraBars.BarSubItem bsiUnsentInvoices;
        private DevExpress.XtraBars.BarSubItem bsiUnsentReports;
        private DevExpress.XtraBars.BarButtonItem bbiSelectUnsentInvoiceReports;
        private DevExpress.XtraBars.BarButtonItem bbiResendCM;
        private DevExpress.XtraGrid.Columns.GridColumn colNoCMReportCount;
        private DevExpress.XtraGrid.Columns.GridColumn colUnsentCMReportCount;
        private DevExpress.XtraEditors.CheckEdit checkEditDoNotPay;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit24;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit21;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML2;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlVisitCategoryFilter;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit9;
        private DevExpress.XtraEditors.SimpleButton btnVisitCategoryFilter;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditVisitCategoryFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private System.Windows.Forms.BindingSource sp06528OMVisitCategoriesFilterBindingSource;
        private DataSet_OM_Core dataSet_OM_Core;
        private DataSet_OM_CoreTableAdapters.sp06528_OM_Visit_Categories_FilterTableAdapter sp06528_OM_Visit_Categories_FilterTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDuration;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedFinanceExportedCount;
        private DevExpress.XtraEditors.TextEdit textEditSBNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemforSBNumber;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraBars.Docking.AutoHideContainer hideContainerLeft;
    }
}

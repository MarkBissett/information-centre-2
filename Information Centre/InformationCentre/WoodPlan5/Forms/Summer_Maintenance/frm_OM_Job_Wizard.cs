﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;
using DevExpress.XtraScheduler;
using DevExpress.Utils.Animation;  // Required by Transition Effects //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;

namespace WoodPlan5
{
    public partial class frm_OM_Job_Wizard : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        bool internalRowFocusing;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_TemplateManager = false;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        public int i_intClientID = 0;

        public DateTime i_dtStart;
        public DateTime i_dtEnd;

        public string i_str_PassedInClientContractIDs = "";
        public string i_str_PassedInSiteContractIDs = "";
        public string i_str_PassedInVisitIDs = "";

        private string i_str_SelectedClientContractID = "";
        private string i_str_SelectedSiteContractID = "";
        private string i_str_SelectedVisitID = "";
        private string i_str_SelectedJobSubTypeIDs = "";

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection3;
        BaseObjects.GridCheckMarksSelection selection4;

        private decimal _decDefaultVatRate = (decimal)0.00;

        private Utils.enmFocusedGrid _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
        
        private int intSelectedJobTemplateCollectionID = 0;

        // Following only populated when called by the Tender Manager \ Tender Visit Wizard //
        public int i_intPassedInTenderID = 0;
        public string i_strPassedInTenderDescription = "";
        public int i_intPassedInJobSubTypeID = 0;
        public int i_intPassedInLabourTypeID = 0;
        public int i_intPassedInLabourID = 0;
        public string i_strPassedInLabourName = "";
        public double i_dbPassedInLabourLatitude = (double)0.00;
        public double i_dbPassedInLabourLongitude = (double)0.00;
        public string i_strPassedInLabourPostcode = "";
        public DateTime? i_dtPassedInWorkCompletedBy = null;

        public frm_OM_Tender_Edit frmCaller = null;

        private string i_str_SelectedVisitTypeIDs = "";

        #endregion

        public frm_OM_Job_Wizard()
        {
            InitializeComponent();
        }
        
        private void frm_OM_Job_Wizard_Load(object sender, EventArgs e)
        {
            this.FormID = 500159;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            // Get Form Permissions (For Edit Templates Button) //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            try
            {
                sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, 7008, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);  // 7008 //
            }
            catch (Exception) { }
            ProcessPermissionsForForm();

            Set_Grid_Highlighter_Transparent(this.Controls);

            xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;  // Hide Table Page headers so it doesn't look like a pageframe //
            xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;

            if (i_intPassedInTenderID > 0)
            {
                TenderDetailsLabelControl.Text = i_strPassedInTenderDescription;
                TenderDetailsLabelControl.Visible = true;
            }
            else
            {
                TenderDetailsLabelControl.Visible = false;
            }

            i_dtStart = DateTime.Today.AddMonths(-6);
            i_dtEnd = DateTime.Today.AddMonths(6);
            dateEditFromDate.DateTime = i_dtStart;
            dateEditToDate.DateTime = i_dtEnd;

            try
            {
                var GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                _decDefaultVatRate = Convert.ToDecimal(GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingDefaultVatRate"));
            }
            catch (Exception)
            {
                _decDefaultVatRate = (decimal)0.00;
            }

            sp06182_OM_Job_Labour_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06187_OM_Job_Equipment_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06191_OM_Job_Material_EditTableAdapter.Connection.ConnectionString = strConnectionString;

            sp06156_OM_Job_Wizard_Client_ContractsTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl1.ForceInitialize();
            Load_Client_Contracts();
            // Add record selection checkboxes to grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.Width = 30;
            selection1.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            selection1.CheckMarkColumn.VisibleIndex = 0;

            sp06157_OM_Job_Wizard_Site_Contracts_For_Client_ContractTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl2.ForceInitialize();
            // Add record selection checkboxes to grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.Width = 30;
            selection2.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            selection2.CheckMarkColumn.VisibleIndex = 0;

            sp06158_OM_Job_Wizard_Visits_For_Site_ContractTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl3.ForceInitialize();
            // Add record selection checkboxes to grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection3.CheckMarkColumn.Width = 30;
            selection3.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            selection3.CheckMarkColumn.VisibleIndex = 0;

            sp06162_OM_Job_Wizard_Master_Job_Sub_Types_AvailableTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl4.ForceInitialize();
            // Add record selection checkboxes to grid control //
            selection4 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl4.MainView);
            selection4.CheckMarkColumn.Width = 30;
            selection4.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            selection4.CheckMarkColumn.VisibleIndex = 0;
            //Load_Master_Jobs();

            sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            try
            {
                sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter.Fill(dataSet_OM_Job.sp06177_OM_Job_Duration_Descriptors_With_Blank, 1);
            }
            catch (Exception) { }

            sp06101_OM_Work_Unit_Types_PicklistTableAdapter.Connection.ConnectionString = strConnectionString;
            try
            {
                sp06101_OM_Work_Unit_Types_PicklistTableAdapter.Fill(dataSet_OM_Contract.sp06101_OM_Work_Unit_Types_Picklist, 1);
            }
            catch (Exception) { }

            sp06108_OM_Unit_Descriptors_PicklistTableAdapter.Connection.ConnectionString = strConnectionString;
            try
            {
                sp06108_OM_Unit_Descriptors_PicklistTableAdapter.Fill(dataSet_OM_Contract.sp06108_OM_Unit_Descriptors_Picklist, 1);
            }
            catch (Exception) { }

            sp06174_OM_Job_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl6.ForceInitialize();

            sp06176_OM_Job_Wizard_Visits_SelectedTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl5.ForceInitialize();

            // Set Starting Page //
            if (string.IsNullOrWhiteSpace(i_str_PassedInClientContractIDs) || string.IsNullOrWhiteSpace(i_str_PassedInSiteContractIDs) || string.IsNullOrWhiteSpace(i_str_PassedInVisitIDs))
            {
                i_str_SelectedClientContractID = "";
                barEditItemDateRange.EditValue = PopupContainerEditDateRange_Get_Selected();
                xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
                btnWelcomeNext.Focus();

                timerWelcomePage.Start();  // allow wizard to move from the Welcome page after 2 seconds. //
            }
            else  // Passed in Client Contract ID and Site Contract ID and Visit ID //
            {
                GridView view = (GridView)gridControl1.MainView;
                i_str_SelectedClientContractID = "";
                barEditItemDateRange.EditValue = PopupContainerEditDateRange_Get_Selected();
                xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
                btnWelcomeNext.Focus();

                int intValueToFind = 0;
                int intFoundRow = 0;
                char[] delimiters = new char[] { ',' };
                string[] strArray = i_str_PassedInClientContractIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                foreach (string strID in strArray)
                {
                    intValueToFind = Convert.ToInt32(strID);
                    intFoundRow = view.LocateByValue(0, view.Columns["ClientContractID"], intValueToFind);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                        barEditItemDateRange.EditValue = "Custom Filter";
                        i_str_SelectedClientContractID += strID + ",";
                    }
                }
                Set_Selected_Client_Count_Label();
                if (!string.IsNullOrWhiteSpace(i_str_SelectedClientContractID))  // Clients Found //
                {
                    MoveToPage2();
                    gridControl2.Focus();
                    selection2.ClearSelection();  // Clear any selected records so we just selected the passed in Site Contract IDs //

                    view = (GridView)gridControl2.MainView;
                    i_str_SelectedSiteContractID = "";

                    selection2.ClearSelection();  // MoveToPage2 call will select all records, so de-selct them //

                    intValueToFind = 0;
                    intFoundRow = 0;
                    strArray = i_str_PassedInSiteContractIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string strID in strArray)
                    {
                        intValueToFind = Convert.ToInt32(strID);
                        intFoundRow = view.LocateByValue(0, view.Columns["SiteContractID"], intValueToFind);
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            view.MakeRowVisible(intFoundRow, false);
                            barEditItemDateRange.EditValue = "Custom Filter";
                            i_str_SelectedSiteContractID += strID + ",";
                        }
                    }
                    Set_Selected_Site_Count_Label();

                    if (!string.IsNullOrWhiteSpace(i_str_SelectedSiteContractID))  // Sites Found //
                    {
                        MoveToPage3();
                        gridControl2.Focus();

                        view = (GridView)gridControl3.MainView;
                        i_str_SelectedVisitID = "";

                        intValueToFind = 0;
                        intFoundRow = 0;
                        strArray = i_str_PassedInVisitIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string strID in strArray)
                        {
                            intValueToFind = Convert.ToInt32(strID);
                            intFoundRow = view.LocateByValue(0, view.Columns["VisitID"], intValueToFind);
                            if (intFoundRow != GridControl.InvalidRowHandle)
                            {
                                view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                                view.MakeRowVisible(intFoundRow, false);
                                barEditItemDateRange.EditValue = "Custom Filter";
                                i_str_SelectedVisitID += strID + ",";
                            }
                        }
                        Set_Selected_Visit_Count_Label();
                        if (!string.IsNullOrWhiteSpace(i_str_SelectedVisitID))  // Visits Found //
                        {
                            MoveToPage4();

                            if (i_intPassedInJobSubTypeID > 0)  // Optionally passed in from Tender //
                            {
                                view = (GridView)gridControl4.MainView;
                                intFoundRow = view.LocateByValue(0, view.Columns["JobSubTypeID"], i_intPassedInJobSubTypeID);
                                if (intFoundRow != GridControl.InvalidRowHandle)
                                {
                                    view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                                    view.MakeRowVisible(intFoundRow, false);
                                }
                            }

                        }
                    }
                }
            }

            intDefaultJobCollectionTemplateID = 0;
            DefaultJobCollectionTemplateButtonEdit.EditValue = "";

            emptyEditor = new RepositoryItem();
            ibool_FormStillLoading = false;
        }
        private void timerWelcomePage_Tick(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;
            timerWelcomePage.Stop();
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            ConfigureFormAccordingToMode();
            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            switch (strFormMode)
            {
                case "add":
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    break;
                default:
                    break;
            }
            ibool_FormStillLoading = false;

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(xtraTabPageStep2.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
        }


        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        iBool_TemplateManager = sfpPermissions.blRead;
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            GridView view = null;
            GridView viewParent = null;
            int[] intRowHandles;
            int[] intParentRowHandles;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    viewParent = (GridView)gridControl5.MainView;
                    intParentRowHandles = viewParent.GetSelectedRows();
                    view = (GridView)gridControl6.MainView;
                    intRowHandles = view.GetSelectedRows();

                    if (intParentRowHandles.Length >= 1)
                    {
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                    if (intRowHandles.Length > 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                    if (intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmFocusedGrid.Labour:
                    viewParent = (GridView)gridControl7.MainView;
                    intParentRowHandles = viewParent.GetSelectedRows();
                    view = (GridView)gridControl8.MainView;
                    intRowHandles = view.GetSelectedRows();

                    if (intParentRowHandles.Length >= 1)
                    {
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                    if (intRowHandles.Length > 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                    if (intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    viewParent = (GridView)gridControl9.MainView;
                    intParentRowHandles = viewParent.GetSelectedRows();
                    view = (GridView)gridControl10.MainView;
                    intRowHandles = view.GetSelectedRows();

                    if (intParentRowHandles.Length >= 1)
                    {
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                    if (intRowHandles.Length > 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                    if (intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    viewParent = (GridView)gridControl11.MainView;
                    intParentRowHandles = viewParent.GetSelectedRows();
                    view = (GridView)gridControl12.MainView;
                    intRowHandles = view.GetSelectedRows();

                    if (intParentRowHandles.Length >= 1)
                    {
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                    if (intRowHandles.Length > 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                    if (intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                default:
                    break;
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            viewParent = (GridView)gridControl5.MainView;
            intParentRowHandles = viewParent.GetSelectedRows();
            view = (GridView)gridControl6.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 1);
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);

            viewParent = (GridView)gridControl7.MainView;
            intParentRowHandles = viewParent.GetSelectedRows();
            view = (GridView)gridControl8.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl8.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            gridControl8.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            gridControl8.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 1);
            gridControl8.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            bbiAddLabourFromContract.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            bbiAddLabourFromContractAll.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);

            viewParent = (GridView)gridControl9.MainView;
            intParentRowHandles = viewParent.GetSelectedRows();
            view = (GridView)gridControl10.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl10.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            gridControl10.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            gridControl10.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 1);
            gridControl10.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            bbiAddEquipmentFromContract.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            
            viewParent = (GridView)gridControl11.MainView;
            intParentRowHandles = viewParent.GetSelectedRows();
            view = (GridView)gridControl12.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl12.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            gridControl12.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            gridControl12.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 1);
            gridControl12.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            bbiAddMaterialFromContract.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
        }

        private void frm_OM_Job_Wizard_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

            }
        }

        private void frm_OM_Job_Wizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                //default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CompanyFilter", i_str_selected_Company_ids);
                //default_screen_settings.SaveDefaultScreenSettings();
            }

            // Clear timer in the off chance it is still running //
            timerWelcomePage.Stop();
            timerWelcomePage = null;
        }


        private void xtraTabControl1_SelectedPageChanging(object sender, DevExpress.XtraTab.TabPageChangingEventArgs e)
        {
            // Remember to set each pages Tag property to the page number in the designer //
            if (Convert.ToInt32(e.Page.Tag) > Convert.ToInt32(e.PrevPage.Tag))
            {
                ((PushTransition)transitionManager1.Transitions[xtraTabControl1].TransitionType).Parameters.EffectOptions = DevExpress.Utils.Animation.PushEffectOptions.FromRight;
            }
            else
            {
                ((PushTransition)transitionManager1.Transitions[xtraTabControl1].TransitionType).Parameters.EffectOptions = DevExpress.Utils.Animation.PushEffectOptions.FromLeft;
            }
            transitionManager1.StartTransition(xtraTabControl1);
        }
        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            // Remember to set each pages Tag property to the page number in the designer //
            transitionManager1.EndTransition();
        }

        public override void PostViewClick(object sender, EventArgs e, int row)
        {
            GridView view = (GridView)sender;
            switch (view.GridControl.MainView.Name)
            {
                case "gridView1":
                    {
                        if (row == GridControl.InvalidRowHandle) return;
                        Set_Selected_Client_Count_Label();
                    }
                    break;
                case "gridView2":
                    {
                        if (row == GridControl.InvalidRowHandle) return;
                        Set_Selected_Site_Count_Label();
                    }
                    break;
                case "gridView3":
                    {
                        if (row == GridControl.InvalidRowHandle) return;
                        Set_Selected_Visit_Count_Label();
                    }
                    break;
                case "gridView4":
                    {
                        if (row == GridControl.InvalidRowHandle) return;
                        Set_Selected_Job_Count_Label();
                    }
                    break;
                default:
                    break;
            }
        }

        private void Clear_Created_Jobs()
        {
            // Clear Any Jobs already created //
            GridView view = (GridView)gridControl6.MainView;
            view.BeginUpdate();
            try
            {
                dataSet_OM_Job.sp06174_OM_Job_Edit.Rows.Clear();  // Clear any existing rows //
            }
            catch (Exception) { }
            view.EndUpdate();
        }

        private void Clear_Created_Labour()
        {
            // Clear Any Jobs already created //
            GridView view = (GridView)gridControl8.MainView;
            view.BeginUpdate();
            try
            {
                dataSet_OM_Job.sp06182_OM_Job_Labour_Edit.Rows.Clear();  // Clear any existing rows //
            }
            catch (Exception) { }
            view.EndUpdate();
        }

        private void Clear_Created_Equipment()
        {
            // Clear Any Jobs already created //
            GridView view = (GridView)gridControl10.MainView;
            view.BeginUpdate();
            try
            {
                dataSet_OM_Job.sp06187_OM_Job_Equipment_Edit.Rows.Clear();  // Clear any existing rows //
            }
            catch (Exception) { }
            view.EndUpdate();
        }

        private void Clear_Created_Materials()
        {
            // Clear Any Jobs already created //
            GridView view = (GridView)gridControl12.MainView;
            view.BeginUpdate();
            try
            {
                dataSet_OM_Job.sp06191_OM_Job_Material_Edit.Rows.Clear();  // Clear any existing rows //
            }
            catch (Exception) { }
            view.EndUpdate();
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Client Contracts Available - click the Refresh button";
                    break;
                case "gridView2":
                    message = "No Site Contracts Available - select one or more Client Contracts from Step 1";
                    break;
                case "gridView3":
                    message = "No Visits Available - tick one or more sites in Step 2 before proceeding";
                    break;
                case "gridView4":
                    message = "No Job Types Available - click refresh button before proceeding";
                    break;
                case "gridView5":
                    message = "No Visits Available - tick one or more visits in Step 3 before proceeding";
                    break;
                case "gridView6":
                    message = "No Jobs Available - Select Jobs in Step 4 of the Wizard";
                    break;
                case "gridView7":
                    message = "No Jobs Available - tick one or more visits in Step 3 before proceeding";
                    break;
                case "gridView8":
                    message = "No Labour Available - Select Jobs from the job grid then click the required Add butto";
                    break;
                case "gridView9":
                    message = "No Jobs Available - tick one or more visits in Step 3 before proceeding";
                    break;
                case "gridView10":
                    message = "No Equipment Available - Select Jobs from the job grid then click the required Add button";
                    break;
                case "gridView11":
                    message = "No Jobs Available - tick one or more visits in Step 3 before proceeding";
                    break;
                case "gridView12":
                    message = "No Materials Available - Select Jobs from the job grid then click the required Add button";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView5":
                    Filter_Jobs_On_Visit();
                    view = (GridView)gridControl6.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridView7":
                    Filter_Labour_On_Jobs();
                    view = (GridView)gridControl8.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridView9":
                    Filter_Equipment_On_Jobs();
                    view = (GridView)gridControl10.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridView11":
                    Filter_Materials_On_Jobs();
                    view = (GridView)gridControl12.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion



        #region Welcome Page

        private void btnWelcomeNext_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;
        }

        #endregion


        #region Step 1 Page

        private void btnStep1Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
        }
        private void btnStep1Next_Click(object sender, EventArgs e)
        {
            MoveToPage2();
        }
        private void MoveToPage2()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //         

            int[] intRowHandles = view.GetSelectedRows();
            int intCount = selection1.SelectedCount;
            int intProcessedCount = 0;
            string strSelectedClientContractIDs = "";
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select at least one Client Contract to create the Jobs for before proceeding.", "Create Job(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                {
                    strSelectedClientContractIDs += view.GetRowCellValue(i, "ClientContractID").ToString() + ",";
                    intProcessedCount++;
                    if (intProcessedCount >= intCount) break;
                }
            }
            if (strSelectedClientContractIDs != i_str_SelectedClientContractID || ibool_FormStillLoading)
            {
                i_str_SelectedClientContractID = strSelectedClientContractIDs;
                if (string.IsNullOrWhiteSpace(i_str_SelectedClientContractID)) return;
                try
                {
                    sp06157_OM_Job_Wizard_Site_Contracts_For_Client_ContractTableAdapter.Fill(dataSet_OM_Job.sp06157_OM_Job_Wizard_Site_Contracts_For_Client_Contract, i_str_SelectedClientContractID, 0);
                }
                catch (Exception) { }
                selection2.SelectAll();
                Set_Selected_Site_Count_Label();
            }
            view = (GridView)gridControl2.MainView;
            view.ExpandAllGroups();
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep2;
        }

        private void bbiRefreshClientContracts_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            i_str_PassedInClientContractIDs = "";  // Clear any passed in Contract IDs //
            Load_Client_Contracts();
        }

        private void Load_Client_Contracts()
        {
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            int intActive = Convert.ToInt32(barEditItemActive.EditValue);
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();

            DateTime dtStartDate = new DateTime(2000, 1, 1);
            DateTime dtEndDate = new DateTime(2500, 1, 1);
            if (string.IsNullOrWhiteSpace(i_str_PassedInClientContractIDs))
            {
                dtStartDate = i_dtStart;
                dtEndDate = i_dtEnd;
            }

            try
            {
                sp06156_OM_Job_Wizard_Client_ContractsTableAdapter.Fill(dataSet_OM_Job.sp06156_OM_Job_Wizard_Client_Contracts, i_str_PassedInClientContractIDs, dtStartDate, dtEndDate, intActive);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Client Contracts.\n\nMessage = [" + ex.Message + "].\n\nPlease close the screen then try again. If the problem persists, contact Technical Support.", "Load Client Contracts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            view.EndUpdate();
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void Set_Selected_Client_Count_Label()
        {
            labelControlSelectedClientCount.Text = "       " + selection1.SelectedCount.ToString() + " Selected Client Contract(s)";
        }


        #region GridView1

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Client_Contracts();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            btnStep1Next.PerformClick();
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Contract;
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Contract;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bsiRecordTicking.Enabled = (intCount > 0);
                bbiTick.Enabled = (intCount > 0);
                bbiUntick.Enabled = (intCount > 0);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region Date Range Filter Panel

        private void btnDateRangeOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            string strValue = "";
            if (dateEditFromDate.DateTime == DateTime.MinValue)
            {
                i_dtStart = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtStart = dateEditFromDate.DateTime;
            }
            if (dateEditToDate.DateTime == DateTime.MinValue)
            {
                i_dtEnd = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtEnd = dateEditToDate.DateTime;
            }
            if (dateEditFromDate.DateTime == DateTime.MinValue && dateEditToDate.DateTime == DateTime.MinValue)
            {
                strValue = "No Date Range Filter";
            }
            else
            {
                strValue = (i_dtStart == new DateTime(1900, 1, 1) ? "No Start" : i_dtStart.ToString("dd/MM/yyyy HH:mm")) + " - " + (i_dtEnd == new DateTime(1900, 1, 1) ? "No End" : i_dtEnd.ToString("dd/MM/yyyy HH:mm"));
            }
            return strValue;
        }

        #endregion

        #endregion


        #region Step 2 Page

        private void btnStep2Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;
        }
        private void btnStep2Next_Click(object sender, EventArgs e)
        {
            MoveToPage3();
        }
        private void MoveToPage3()
        {
            GridView view = (GridView)gridControl2.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  

            int intCount = selection2.SelectedCount;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select at least one Site Contract to create the Jobs for before proceeding.", "Create Job(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            SelectedSiteContractCountTextEdit.EditValue = selection2.SelectedCount;

            int intProcessedCount = 0;
            string strSelectedSiteContractIDs = "";
            int intSiteContractsWithJobsTemplatesCount = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                {
                    strSelectedSiteContractIDs += view.GetRowCellValue(i, "SiteContractID").ToString() + ",";
                    if (Convert.ToInt32(view.GetRowCellValue(i, "DefaultJobCollectionTemplateID")) > 0) intSiteContractsWithJobsTemplatesCount++;
                    intProcessedCount++;
                    if (intProcessedCount >= intCount) break;
                }
            }
            if (strSelectedSiteContractIDs != i_str_SelectedSiteContractID || ibool_FormStillLoading)  // Load Site Contract Visit Start Dates Grid //
            {
                i_str_SelectedSiteContractID = strSelectedSiteContractIDs;
                if (string.IsNullOrWhiteSpace(i_str_SelectedSiteContractID)) return;
                view = (GridView)gridControl3.MainView;
                view.BeginUpdate();
                try
                {
                    sp06158_OM_Job_Wizard_Visits_For_Site_ContractTableAdapter.Fill(dataSet_OM_Job.sp06158_OM_Job_Wizard_Visits_For_Site_Contract, i_str_SelectedSiteContractID);
                }
                catch (Exception) { }
                view.ExpandAllGroups();
                view.EndUpdate();
            }
            checkEditJobsFromSiteTemplate.Caption = intSiteContractsWithJobsTemplatesCount.ToString() + " / " + intCount.ToString();  // Page 4 check edit on toolbar //
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep3;
        }

        private void Set_Selected_Site_Count_Label()
        {
            labelControlSelectedSiteCount.Text = "       " + selection2.SelectedCount.ToString() + " Selected Site Contract(s)";
        }


        #region GridView2

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bsiRecordTicking.Enabled = (intCount > 0);
                bbiTick.Enabled = (intCount > 0);
                bbiUntick.Enabled = (intCount > 0);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        private int intDefaultJobCollectionTemplateID = 0;
        private void DefaultJobCollectionTemplateButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            string strClientIDs = "";
            if (e.Button.Tag.ToString() == "choose")
            {
                int intClientContractID = 0;
                int intLastClientContractID = 0;
                bool boolMultipleContracts = false;
                foreach (int intRowHandle in intRowHandles)
                {
                    intClientContractID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ClientContractID"));
                    if (intLastClientContractID > 0 && intLastClientContractID != intClientContractID)
                    {
                        boolMultipleContracts = true;
                        break;
                    }
                }
                if (!boolMultipleContracts && intClientContractID > 0)
                {
                    // We have parent Client Contract so get the ClientID for passing to the Select screen //
                    try
                    {
                        using (var GetID = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                        {
                            GetID.ChangeConnectionString(strConnectionString);
                            strClientIDs = GetID.sp06534_OM_ClientID_From_ClientContractID(intClientContractID).ToString();
                            if (!string.IsNullOrWhiteSpace(strClientIDs)) strClientIDs += ",";
                        }
                    }
                    catch (Exception)
                    {
                        XtraMessageBox.Show("Unable to get the Parent Client ID from the current Client Contract ID - all Job Collection Templates will be shown on the following screen.", "Get Job Collection Template", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                var fChildForm = new frm_OM_Select_Job_Collection_Template_Header();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm._PassedInClientIDsFilter = strClientIDs;
                fChildForm.AllowApplyRules = false;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    intDefaultJobCollectionTemplateID = fChildForm.intSelectedTemplateHeaderID;
                    DefaultJobCollectionTemplateButtonEdit.EditValue = fChildForm.strSelectedTemplateHeaderDescription;
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                intDefaultJobCollectionTemplateID = 0;
                DefaultJobCollectionTemplateButtonEdit.EditValue = "";
            }
        }

        private void simpleButtonApplyDefaultJobCollectionTemplate_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Site Contracts to set the Default Job Collection Template for before proceeding.", "Set Default Job Collection Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            StringBuilder sb = new StringBuilder();
            
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Updating...");
            
            view.BeginDataUpdate();
            foreach (int intRowHandle in intRowHandles)
            {
                sb.Append(view.GetRowCellValue(intRowHandle, "SiteContractID").ToString() + ",");
                view.SetRowCellValue(intRowHandle, "DefaultJobCollectionTemplateID", intDefaultJobCollectionTemplateID);
                view.SetRowCellValue(intRowHandle, "DefaultJobCollectionTemplate", DefaultJobCollectionTemplateButtonEdit.EditValue);
            }
            view.EndDataUpdate();
            if (StoreInDBCheckEdit.Checked && ! string.IsNullOrWhiteSpace(sb.ToString())) // Update Database //
            {
                try
                {
                    using (var UpdateData = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                    {
                        UpdateData.ChangeConnectionString(strConnectionString);
                        UpdateData.sp06535_OM_SiteContract_Update_DefaultJobCollectionTemplateID(sb.ToString(), intDefaultJobCollectionTemplateID);
                    }
                }
                catch (Exception ex)
                {
                    if (splashScreenManager1.IsSplashFormVisible) splashScreenManager1.CloseWaitForm();
                    XtraMessageBox.Show("Unable to update Site Contracts with Default Job Collection Template - an error occured. Please try again./n/nError: " + ex.Message, "Update Default Job Collection Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            if (splashScreenManager1.IsSplashFormVisible) splashScreenManager1.CloseWaitForm();
        }

        #endregion


        #region Step 3 Page

        private void btnStep3Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep2;
        }
        private void btnStep3Next_Click(object sender, EventArgs e)
        {
            MoveToPage4();
        }
        private void MoveToPage4()
        {
            i_str_SelectedVisitTypeIDs = "";  // Clear - we will re-calculate below //
            GridView view = (GridView)gridControl3.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  

            int intCount = selection3.SelectedCount;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select at least one Visit to create the Jobs for before proceeding.", "Create Job(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            SelectedVisitCountTextEdit.EditValue = selection3.SelectedCount;

            int intProcessedCount = 0;
            string strSelectedVisitIDs = "";
            int intVisitsWithJobsCount = 0;
            DateTime dtBlank = new DateTime(1900, 1, 1);
            List<string> VisitTypeIDs = new List<string>();  // Used to build comma separated string at end of loop - list used to prevent duplicates easily //

            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                {
                    strSelectedVisitIDs += view.GetRowCellValue(i, "VisitID").ToString() + ",";
                    if (Convert.ToDateTime(view.GetRowCellValue(i, "LastVisitStartDate")) > dtBlank) intVisitsWithJobsCount++;

                    string strVisitTypeID = view.GetRowCellValue(i, "VisitTypeID").ToString();
                    if (!VisitTypeIDs.Contains(strVisitTypeID)) VisitTypeIDs.Add(strVisitTypeID);  // If list does not contain visitTypeID, add it //
                    
                    intProcessedCount++;
                    if (intProcessedCount >= intCount) break;
                }
            }
            i_str_SelectedVisitTypeIDs = String.Join(",", VisitTypeIDs) + ",";  // Convert list into comma separated string - add comma tyo end in case only one item as no comma will be presnt as nothing to join //
            selection4.ClearSelection();
            Set_Selected_Job_Count_Label();
            Load_Master_Jobs();

            checkEditJobsFromLastVisit.Caption = intVisitsWithJobsCount.ToString() + " / " + intCount.ToString();  // Page 4 check edit on toolbar //

            // Commented out by MB on 19/01/2017 - replace with line after commented out code //
            //if (intVisitsWithJobsCount == intCount)
            //{
            //    beiJobsFromLastVisit.Enabled = true;
            //}
            //else
            //{
            //    beiJobsFromLastVisit.Enabled = false;
            //    beiJobsFromLastVisit.EditValue = 0;
            //    gridControl4.Enabled = true;
            //}
            if (intVisitsWithJobsCount > 0) beiJobsFromLastVisit.Enabled = true;

            if (strSelectedVisitIDs != i_str_SelectedVisitID || ibool_FormStillLoading)
            {
                i_str_SelectedVisitID = strSelectedVisitIDs;
                if (string.IsNullOrWhiteSpace(i_str_SelectedVisitID)) return;
                view = (GridView)gridControl5.MainView;
                view.BeginUpdate();
                try
                {
                    sp06176_OM_Job_Wizard_Visits_SelectedTableAdapter.Fill(dataSet_OM_Job.sp06176_OM_Job_Wizard_Visits_Selected, strSelectedVisitIDs);
                }
                catch (Exception) { }
                view.ExpandAllGroups();
                view.EndUpdate();

                Clear_Created_Jobs();
            } 

            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep4;
        }

        private void Set_Selected_Visit_Count_Label()
        {
            labelControlSelectedVisitCount.Text = "       " + selection3.SelectedCount.ToString() + " Selected Visit(s)";
        }

        private void bbiSelectVisitsFromCriteria_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {           
            string strVisitIDs = "";
            GridView view = (GridView)gridControl3.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  

            for (int i = 0; i < view.DataRowCount; i++)
            {
                strVisitIDs += view.GetRowCellValue(i, "VisitID").ToString() + ",";
            }
            if (string.IsNullOrWhiteSpace(strVisitIDs))
            {
                XtraMessageBox.Show("No visits available to check.", "Select Visits Based On Criteria", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Select_Visit_Using_Job_Type_Criteria();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "multiple";

            if (fChildForm.ShowDialog() != DialogResult.OK) return;
            int intCriteriaTypeID = fChildForm._CriteriaTypeID;  // 0 = With, 1 = With Out // 
            string strSelectedJobSubTypeIDs = fChildForm.strSelectedIDs;
            int intJobSubTypeCount = fChildForm._SelectedCount;
            if (string.IsNullOrWhiteSpace(strSelectedJobSubTypeIDs)) return;

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Selecting...");

            selection3.ClearSelection();

            string strVisitIDsToSelect = "";
            using (var GetRecords = new DataSet_OM_JobTableAdapters.QueriesTableAdapter())
            {
                GetRecords.ChangeConnectionString(strConnectionString);
                try
                {
                    strVisitIDsToSelect = GetRecords.sp06398_Get_Visits_Matching_Job_Type_Criteria(strVisitIDs, strSelectedJobSubTypeIDs, intCriteriaTypeID, intJobSubTypeCount).ToString();
                }
                catch (Exception) { }
            }
            if (string.IsNullOrWhiteSpace(strVisitIDsToSelect))
            {
                splashScreenManager1.CloseWaitForm();
                XtraMessageBox.Show("No visits found matching criteria.", "Select Visits Based On Criteria", MessageBoxButtons.OK, MessageBoxIcon.Stop); 
                return;
            }

            // If here we are good to select the records //
            int intMatched = 0;
            strVisitIDsToSelect = "," + strVisitIDsToSelect + ",";  // Put preceeding and succeeding commas for search matching purposes //
            view.BeginUpdate();
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (strVisitIDsToSelect.Contains("," + view.GetRowCellValue(i, "VisitID").ToString() + ","))
                {
                    view.SetRowCellValue(i, "CheckMarkSelection", 1);
                    intMatched++;
                }
            }
            view.ExpandAllGroups();
            view.EndUpdate();
            Set_Selected_Visit_Count_Label();
            splashScreenManager1.CloseWaitForm();
            XtraMessageBox.Show(intMatched.ToString() + " Visit(s) Found.", "Select Visits Based On Criteria", MessageBoxButtons.OK, MessageBoxIcon.Information); 
        }

        #region GridView3

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bsiRecordTicking.Enabled = (intCount > 0);
                bbiTick.Enabled = (intCount > 0);
                bbiUntick.Enabled = (intCount > 0);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion

        #endregion


        #region Step 4 Page
        
        private void btnStep4Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep3;
            intSelectedJobTemplateCollectionID = 0;
        }
        private void btnStep4Next_Click(object sender, EventArgs e)
        {
            GridView viewMasterJobSubTypes = (GridView)gridControl4.MainView;
            viewMasterJobSubTypes.PostEditor();
            if (!ReferenceEquals(viewMasterJobSubTypes.ActiveFilter.Criteria, null)) viewMasterJobSubTypes.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(viewMasterJobSubTypes.FindFilterText)) viewMasterJobSubTypes.ApplyFindFilter("");  // Clear any Find in place //  

            int intJobsFromLastVisit = Convert.ToInt32(beiJobsFromLastVisit.EditValue);
            int intJobsFromSiteContractTemplate = Convert.ToInt32(beiJobsFromSiteTemplate.EditValue);
            int intCount = selection4.SelectedCount;
            if (intCount <= 0 && intSelectedJobTemplateCollectionID == 0 && intJobsFromLastVisit <= 0 && intJobsFromSiteContractTemplate <= 0)
            {
                XtraMessageBox.Show("Select at least one Job Sub-Type to create for the selected Visit(s) before proceeding.", "Create Job(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            if (intSelectedJobTemplateCollectionID == 0 && intJobsFromLastVisit == 0 && intJobsFromSiteContractTemplate == 0)
            {
                int intProcessedCount = 0;
                string strSelectedJobSubTypeIDs = "";
                for (int i = 0; i < viewMasterJobSubTypes.DataRowCount; i++)
                {
                    if (Convert.ToInt32(viewMasterJobSubTypes.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        strSelectedJobSubTypeIDs += viewMasterJobSubTypes.GetRowCellValue(i, "JobSubTypeID").ToString() + ",";
                        intProcessedCount++;
                        if (intProcessedCount >= intCount) break;
                    }
                }
                if (strSelectedJobSubTypeIDs != i_str_SelectedJobSubTypeIDs || ibool_FormStillLoading)
                {
                    i_str_SelectedJobSubTypeIDs = strSelectedJobSubTypeIDs;
                    Clear_Created_Jobs();
                    Clear_Created_Labour();
                    Clear_Created_Equipment();
                    Clear_Created_Materials();
                    GridView viewNew = (GridView)gridControl6.MainView;
                    int intVisitID = 0;
                    int intVisitNumber = 0;
                    DateTime dtStartDate = DateTime.MinValue;
                    DateTime dtEndDate = DateTime.MinValue;
                    int intJobTypeID = 0;
                    int intJobSubTypeID = 0;
                    string strJobDescription = "";
                    string strJobSubTypeDescription = "";
                    int intSiteContractID = 0;
                    int intClientContractID = 0;
                    int intSiteID = 0;
                    int intClientID = 0;
                    string strContractDescription = "";
                    string strClientName = "";
                    string strSiteName = "";
                    double dblSiteLocationX = (double)0.00;
                    double dblSiteLocationY = (double)0.00;
                    string strSitePostcode = "";
                    int intClientPOID = 0;
                    string strClientPONumber = "";
                    int intVisitCostCalculationLevelID = 0;
                    string strVisitCostCalculationLevel = "";
                    int intVisitStatusID = 0;

                    // Now Create Jobs based on selected Visits and Selected Job Sub-Types //
                    viewNew.BeginUpdate();
                    viewNew.BeginDataUpdate();
                    GridView viewVisits = (GridView)gridControl3.MainView;
                    viewVisits.ActiveFilterString = "[CheckMarkSelection] = 1";  // Apply filter to selected rows //
                    viewMasterJobSubTypes.ActiveFilterString = "[CheckMarkSelection] = 1";  // Apply filter to selected rows //

                    GridView viewJobs = (GridView)gridControl7.MainView;
                    GridView viewEquipment = (GridView)gridControl9.MainView;
                    GridView viewMaterials = (GridView)gridControl11.MainView;

                    gridControl7.BeginUpdate();
                    gridControl9.BeginUpdate();
                    gridControl11.BeginUpdate();

                    try
                    {
                        for (int i = 0; i < viewVisits.DataRowCount; i++)
                        {
                            intVisitID = Convert.ToInt32(viewVisits.GetRowCellValue(i, "VisitID"));
                            intVisitNumber = Convert.ToInt32(viewVisits.GetRowCellValue(i, "VisitNumber"));

                            dtStartDate = DateTime.MinValue;
                            try { dtStartDate = Convert.ToDateTime(viewVisits.GetRowCellValue(i, "ExpectedStartDate")); }
                            catch (Exception) { }
                            if (dtStartDate == DateTime.MinValue)
                            {
                                dtStartDate = DateTime.Now;
                                dtEndDate = DateTime.Now.AddHours(1);
                            }
                            else
                            {
                                dtEndDate = DateTime.MinValue;
                                try { dtEndDate = Convert.ToDateTime(viewVisits.GetRowCellValue(i, "ExpectedEndDate")); }
                                catch (Exception) { }
                                if (dtEndDate == DateTime.MinValue)
                                {
                                    dtEndDate = dtStartDate.AddHours((double)1);
                                }
                                else
                                {
                                    dtEndDate = Convert.ToDateTime(viewVisits.GetRowCellValue(i, "ExpectedEndDate"));
                                }
                            }

                            intSiteContractID = Convert.ToInt32(viewVisits.GetRowCellValue(i, "SiteContractID"));
                            intClientContractID = Convert.ToInt32(viewVisits.GetRowCellValue(i, "ClientContractID"));
                            intSiteID = Convert.ToInt32(viewVisits.GetRowCellValue(i, "SiteID"));
                            intClientID = Convert.ToInt32(viewVisits.GetRowCellValue(i, "ClientID"));
                            strContractDescription = viewVisits.GetRowCellValue(i, "ContractDescription").ToString();
                            strClientName = viewVisits.GetRowCellValue(i, "ClientName").ToString();
                            strSiteName = viewVisits.GetRowCellValue(i, "SiteName").ToString();
                            intVisitNumber = Convert.ToInt32(viewVisits.GetRowCellValue(i, "VisitNumber"));
                            dblSiteLocationX = Convert.ToDouble(viewVisits.GetRowCellValue(i, "LocationX"));
                            dblSiteLocationY = Convert.ToDouble(viewVisits.GetRowCellValue(i, "LocationY"));
                            strSitePostcode = viewVisits.GetRowCellValue(i, "SitePostcode").ToString();
                            intClientPOID = Convert.ToInt32(viewVisits.GetRowCellValue(i, "ClientPOID"));
                            strClientPONumber = viewVisits.GetRowCellValue(i, "ClientPONumber").ToString();

                            intVisitCostCalculationLevelID = Convert.ToInt32(viewVisits.GetRowCellValue(i, "CostCalculationLevel"));
                            strVisitCostCalculationLevel = viewVisits.GetRowCellValue(i, "CostCalculationLevelDescription").ToString();
                            intVisitStatusID = Convert.ToInt32(viewVisits.GetRowCellValue(i, "VisitStatusID"));

                            Get_Client_PO(intVisitID, 2, ref intClientPOID, ref strClientPONumber);
                            if (intClientPOID <= 0)  // No Visit specific Client PO ID so try for Site Contract specific //
                            {
                                Get_Client_PO(intSiteContractID, 1, ref intClientPOID, ref strClientPONumber);
                                if (intClientPOID <= 0)  // No Site Contract specific Client PO ID so try for Client Contract specific //
                                {
                                    Get_Client_PO(intClientContractID, 0, ref intClientPOID, ref strClientPONumber);
                                }
                            }

                            for (int j = 0; j < viewMasterJobSubTypes.DataRowCount; j++)
                            {
                                intJobTypeID = Convert.ToInt32(viewMasterJobSubTypes.GetRowCellValue(j, "JobTypeID"));
                                intJobSubTypeID = Convert.ToInt32(viewMasterJobSubTypes.GetRowCellValue(j, "JobSubTypeID"));
                                strJobDescription = viewMasterJobSubTypes.GetRowCellValue(j, "JobTypeDescription").ToString();
                                strJobSubTypeDescription = viewMasterJobSubTypes.GetRowCellValue(j, "JobSubTypeDescription").ToString();

                                DataRow drNewRow;
                                drNewRow = dataSet_OM_Job.sp06174_OM_Job_Edit.NewRow();
                                drNewRow["strMode"] = "add";
                                drNewRow["strRecordIDs"] = "";
                                drNewRow["VisitID"] = intVisitID;
                                drNewRow["VisitNumber"] = intVisitNumber;
                                drNewRow["JobSubTypeID"] = intJobSubTypeID;
                                drNewRow["JobTypeID"] = intJobTypeID;
                                drNewRow["CreatedByStaffID"] = GlobalSettings.UserID;
                                drNewRow["CreatedByStaffName"] = GlobalSettings.UserSurname + ": " + GlobalSettings.UserForename;
                                drNewRow["Reactive"] = 0;
                                drNewRow["JobNoLongerRequired"] = 0;
                                drNewRow["RequiresAccessPermit"] = 0;
                                drNewRow["AccessPermitID"] = 0;
                                drNewRow["FinanceSystemPONumber"] = "";
                                drNewRow["ExpectedStartDate"] = dtStartDate;
                                drNewRow["ExpectedEndDate"] = dtEndDate;
                                drNewRow["ExpectedDurationUnits"] = (dtEndDate - dtStartDate).TotalHours; //1.00;
                                drNewRow["ExpectedDurationUnitsDescriptorID"] = 1;  // Hours //
                                drNewRow["ActualDurationUnits"] = 0.00;
                                drNewRow["ActualDurationUnits"] = 0.00;
                                drNewRow["ActualDurationUnitsDescriptionID"] = 0;
                                drNewRow["MinimumDaysFromLastVisit"] = 0;
                                drNewRow["MaximumDaysFromLastVisit"] = 0;
                                drNewRow["DaysLeeway"] = 0;
                                drNewRow["SiteContractID"] = intSiteContractID;
                                drNewRow["ClientContractID"] = intClientContractID;
                                drNewRow["SiteID"] = intSiteID;
                                drNewRow["ClientID"] = intClientID;
                                drNewRow["ContractDescription"] = strContractDescription;
                                drNewRow["ClientNameContractDescription"] = strClientName + ", Contract: " + strContractDescription;
                                drNewRow["ClientName"] = strClientName;
                                drNewRow["SiteName"] = strSiteName;
                                drNewRow["JobSubTypeDescription"] = strJobSubTypeDescription;
                                drNewRow["JobTypeDescription"] = strJobDescription;
                                drNewRow["JobTypeJobSubTypeDescription"] = strJobDescription + " - " + strJobSubTypeDescription;
                                drNewRow["LabourCount"] = 0;
                                drNewRow["EquipmentCount"] = 0;
                                drNewRow["MaterialCount"] = 0;
                                drNewRow["LocationX"] = dblSiteLocationX;
                                drNewRow["LocationY"] = dblSiteLocationY;
                                drNewRow["SitePostcode"] = strSitePostcode;
                                drNewRow["JobStatusID"] = (intVisitStatusID == 55 ? 105 : 20);  // Job Creation Started - Ready To Send //

                                drNewRow["ClientPOID"] = intClientPOID;
                                drNewRow["ClientPONumber"] = strClientPONumber;
                                drNewRow["VisitCostCalculationLevelID"] = intVisitCostCalculationLevelID;
                                drNewRow["VisitCostCalculationLevel"] = strVisitCostCalculationLevel;

                                dataSet_OM_Job.sp06174_OM_Job_Edit.Rows.Add(drNewRow);
                            }
                        }
                    }
                    catch (Exception) { }

                    gridControl7.EndUpdate();
                    gridControl9.EndUpdate();
                    gridControl11.EndUpdate();

                    viewNew.ExpandAllGroups();
                    viewNew.EndDataUpdate();
                    viewNew.EndUpdate();

                    viewJobs.ExpandAllGroups();
                    viewJobs.BeginSelection();
                    viewJobs.SelectAll();
                    viewJobs.EndSelection();

                    // Now Clear Filter on Visits Grid //
                    if (!ReferenceEquals(viewVisits.ActiveFilter.Criteria, null)) viewVisits.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                    if (!ReferenceEquals(viewMasterJobSubTypes.ActiveFilter.Criteria, null)) viewMasterJobSubTypes.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                    viewMasterJobSubTypes.ExpandAllGroups();

                    //if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
                }
                GridView view5 = (GridView)gridControl5.MainView;
                view5.ExpandAllGroups();
                view5.SelectAll();
            }
            else if (intJobsFromLastVisit != 0)  // Get jobs from last visit //
            {
                Clear_Created_Jobs();
                Clear_Created_Labour();
                Clear_Created_Equipment();
                Clear_Created_Materials();
                GridView viewNew = (GridView)gridControl6.MainView;
                int intVisitID = 0;
                int intVisitNumber = 0;
                DateTime dtStartDate = DateTime.MinValue;
                DateTime dtEndDate = DateTime.MinValue;
                int intJobTypeID = 0;
                int intJobSubTypeID = 0;
                string strJobDescription = "";
                string strJobSubTypeDescription = "";
                int intSiteContractID = 0;
                int intClientContractID = 0;
                int intSiteID = 0;
                int intClientID = 0;
                string strContractDescription = "";
                string strClientName = "";
                string strSiteName = "";
                double dblSiteLocationX = (double)0.00;
                double dblSiteLocationY = (double)0.00;
                string strSitePostcode = "";
                int intClientPOID = 0;
                string strClientPONumber = "";
                int intVisitCategoryID = 0;
                int intVisitStatusID = 0;

                DateTime dtLastVisitStartDate = DateTime.MinValue;

                // Now Create Jobs based on last completed visit on site contract //
                viewNew.BeginUpdate();
                viewNew.BeginDataUpdate();
                GridView viewVisits = (GridView)gridControl3.MainView;
                viewVisits.ActiveFilterString = "[CheckMarkSelection] = 1";  // Apply filter to selected rows //

                GridView viewJobs = (GridView)gridControl7.MainView;
                GridView viewEquipment = (GridView)gridControl9.MainView;
                GridView viewMaterials = (GridView)gridControl11.MainView;

                gridControl7.BeginUpdate();
                gridControl9.BeginUpdate();
                gridControl11.BeginUpdate();

                SqlConnection SQlConn = new SqlConnection(strConnectionString);
                SqlCommand cmd = null;

                try
                {
                    for (int i = 0; i < viewVisits.DataRowCount; i++)
                    {
                        intVisitID = Convert.ToInt32(viewVisits.GetRowCellValue(i, "VisitID"));
                        intVisitNumber = Convert.ToInt32(viewVisits.GetRowCellValue(i, "VisitNumber"));

                        dtStartDate = DateTime.MinValue;
                        try { dtStartDate = Convert.ToDateTime(viewVisits.GetRowCellValue(i, "ExpectedStartDate")); }
                        catch (Exception) { }
                        if (dtStartDate == DateTime.MinValue)
                        {
                            dtStartDate = DateTime.Now;
                            dtEndDate = DateTime.Now.AddHours(1);
                        }
                        else
                        {
                            dtEndDate = DateTime.MinValue;
                            try { dtEndDate = Convert.ToDateTime(viewVisits.GetRowCellValue(i, "ExpectedEndDate")); }
                            catch (Exception) { }
                            if (dtEndDate == DateTime.MinValue)
                            {
                                dtEndDate = dtStartDate.AddHours((double)1);
                            }
                            else
                            {
                                dtEndDate = Convert.ToDateTime(viewVisits.GetRowCellValue(i, "ExpectedEndDate"));
                            }
                        }

                        intSiteContractID = Convert.ToInt32(viewVisits.GetRowCellValue(i, "SiteContractID"));
                        intClientContractID = Convert.ToInt32(viewVisits.GetRowCellValue(i, "ClientContractID"));
                        intSiteID = Convert.ToInt32(viewVisits.GetRowCellValue(i, "SiteID"));
                        intClientID = Convert.ToInt32(viewVisits.GetRowCellValue(i, "ClientID"));
                        strContractDescription = viewVisits.GetRowCellValue(i, "ContractDescription").ToString();
                        strClientName = viewVisits.GetRowCellValue(i, "ClientName").ToString();
                        strSiteName = viewVisits.GetRowCellValue(i, "SiteName").ToString();
                        intVisitNumber = Convert.ToInt32(viewVisits.GetRowCellValue(i, "VisitNumber"));
                        dblSiteLocationX = Convert.ToDouble(viewVisits.GetRowCellValue(i, "LocationX"));
                        dblSiteLocationY = Convert.ToDouble(viewVisits.GetRowCellValue(i, "LocationY"));
                        strSitePostcode = viewVisits.GetRowCellValue(i, "SitePostcode").ToString();
                        intClientPOID = Convert.ToInt32(viewVisits.GetRowCellValue(i, "ClientPOID"));
                        strClientPONumber = viewVisits.GetRowCellValue(i, "ClientPONumber").ToString();
                        intVisitCategoryID = Convert.ToInt32(viewVisits.GetRowCellValue(i, "VisitCategoryID"));
                        dtLastVisitStartDate = Convert.ToDateTime(viewVisits.GetRowCellValue(i, "LastVisitStartDate"));
                        intVisitStatusID = Convert.ToInt32(viewVisits.GetRowCellValue(i, "VisitStatusID"));

                        Get_Client_PO(intVisitID, 2, ref intClientPOID, ref strClientPONumber);
                        if (intClientPOID <= 0)  // No Visit specific Client PO ID so try for Site Contract specific //
                        {
                            Get_Client_PO(intSiteContractID, 1, ref intClientPOID, ref strClientPONumber);
                            if (intClientPOID <= 0)  // No Site Contract specific Client PO ID so try for Client Contract specific //
                            {
                                Get_Client_PO(intClientContractID, 0, ref intClientPOID, ref strClientPONumber);
                            }
                        }

                        // Get matching Jobs from Job collection Template and any Template rules //
                        cmd = null;
                        cmd = new SqlCommand("sp06396_OM_Job_Wizard_Get_Last_Jobs_For_Site_Contract", SQlConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@SiteContractID", intSiteContractID));
                        cmd.Parameters.Add(new SqlParameter("@LastVisitDateTime", dtLastVisitStartDate));
                        SqlDataAdapter sdaJobs = new SqlDataAdapter(cmd);
                        DataSet dsJobs = new DataSet("NewDataSet");
                        sdaJobs.Fill(dsJobs, "Table");

                        foreach (DataRow dr in dsJobs.Tables[0].Rows)
                        {
                            intJobTypeID = Convert.ToInt32(dr["JobTypeID"]);
                            intJobSubTypeID = Convert.ToInt32(dr["JobSubTypeID"]);
                            strJobDescription = dr["JobTypeDescription"].ToString();
                            strJobSubTypeDescription = dr["JobSubTypeDescription"].ToString();

                            DataRow drNewRow;
                            drNewRow = dataSet_OM_Job.sp06174_OM_Job_Edit.NewRow();
                            drNewRow["strMode"] = "add";
                            drNewRow["strRecordIDs"] = "";
                            drNewRow["VisitID"] = intVisitID;
                            drNewRow["VisitNumber"] = intVisitNumber;
                            drNewRow["JobSubTypeID"] = intJobSubTypeID;
                            drNewRow["JobTypeID"] = intJobTypeID;
                            drNewRow["CreatedByStaffID"] = GlobalSettings.UserID;
                            drNewRow["CreatedByStaffName"] = GlobalSettings.UserSurname + ": " + GlobalSettings.UserForename;
                            drNewRow["Reactive"] = 0;
                            drNewRow["JobNoLongerRequired"] = 0;
                            drNewRow["RequiresAccessPermit"] = 0;
                            drNewRow["AccessPermitID"] = 0;
                            drNewRow["FinanceSystemPONumber"] = "";
                            drNewRow["ExpectedStartDate"] = dtStartDate;
                            drNewRow["ExpectedEndDate"] = dtEndDate;
                            drNewRow["ExpectedDurationUnits"] = (dtEndDate - dtStartDate).TotalHours; //1.00;
                            drNewRow["ExpectedDurationUnitsDescriptorID"] = 1;  // Hours //
                            drNewRow["ActualDurationUnits"] = 0.00;
                            drNewRow["ActualDurationUnits"] = 0.00;
                            drNewRow["ActualDurationUnitsDescriptionID"] = 0;
                            drNewRow["MinimumDaysFromLastVisit"] = 0;
                            drNewRow["MaximumDaysFromLastVisit"] = 0;
                            drNewRow["DaysLeeway"] = 0;
                            drNewRow["SiteContractID"] = intSiteContractID;
                            drNewRow["ClientContractID"] = intClientContractID;
                            drNewRow["SiteID"] = intSiteID;
                            drNewRow["ClientID"] = intClientID;
                            drNewRow["ContractDescription"] = strContractDescription;
                            drNewRow["ClientNameContractDescription"] = strClientName + ", Contract: " + strContractDescription;
                            drNewRow["ClientName"] = strClientName;
                            drNewRow["SiteName"] = strSiteName;
                            drNewRow["JobSubTypeDescription"] = strJobSubTypeDescription;
                            drNewRow["JobTypeDescription"] = strJobDescription;
                            drNewRow["JobTypeJobSubTypeDescription"] = strJobDescription + " - " + strJobSubTypeDescription;
                            drNewRow["LabourCount"] = 0;
                            drNewRow["EquipmentCount"] = 0;
                            drNewRow["MaterialCount"] = 0;
                            drNewRow["LocationX"] = dblSiteLocationX;
                            drNewRow["LocationY"] = dblSiteLocationY;
                            drNewRow["SitePostcode"] = strSitePostcode;
                            drNewRow["JobStatusID"] = (intVisitStatusID == 55 ? 105 : 20);  // Job Creation Started - Ready To Send //

                            drNewRow["ClientPOID"] = intClientPOID;
                            drNewRow["ClientPONumber"] = strClientPONumber;

                            dataSet_OM_Job.sp06174_OM_Job_Edit.Rows.Add(drNewRow);
                        }
                    }
                }
                catch (Exception) { }

                gridControl7.EndUpdate();
                gridControl9.EndUpdate();
                gridControl11.EndUpdate();

                viewNew.ExpandAllGroups();
                viewNew.EndDataUpdate();
                viewNew.EndUpdate();

                viewJobs.ExpandAllGroups();
                viewJobs.BeginSelection();
                viewJobs.SelectAll();
                viewJobs.EndSelection();

                // Now Clear Filter on Visits Grid //
                if (!ReferenceEquals(viewVisits.ActiveFilter.Criteria, null)) viewVisits.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                if (!ReferenceEquals(viewMasterJobSubTypes.ActiveFilter.Criteria, null)) viewMasterJobSubTypes.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                viewMasterJobSubTypes.ExpandAllGroups();

                //if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
                GridView view5 = (GridView)gridControl5.MainView;
                view5.ExpandAllGroups();
                view5.SelectAll();
            }
            else if (intJobsFromSiteContractTemplate != 0)  // Get jobs from site contract default job collection template //
            {
                Clear_Created_Jobs();
                Clear_Created_Labour();
                Clear_Created_Equipment();
                Clear_Created_Materials();
                GridView viewNew = (GridView)gridControl6.MainView;

                // Now Create Jobs based on selected Visits and their parent Site Contract's Default Job Collection Template //
                viewNew.BeginUpdate();
                viewNew.BeginDataUpdate();
                GridView viewVisits = (GridView)gridControl3.MainView;
                viewVisits.ActiveFilterString = "[CheckMarkSelection] = 1";  // Apply filter to selected rows //

                GridView viewJobs = (GridView)gridControl7.MainView;
                GridView viewEquipment = (GridView)gridControl9.MainView;
                GridView viewMaterials = (GridView)gridControl11.MainView;

                gridControl7.BeginUpdate();
                gridControl9.BeginUpdate();
                gridControl11.BeginUpdate();

                List<int> JobTemplatesList = new List<int>();
                List<int> SiteContractsList = new List<int>();
                StringBuilder sbVisitIDs = new StringBuilder();

                GridView viewSiteContracts = (GridView)gridControl2.MainView;
                int intSiteContractsCount = viewSiteContracts.DataRowCount;
                // Build unique list of DefaultJobCollectionTemplateIDs from selected site contracts //
                try
                {
                    for (int i = 0; i < intSiteContractsCount; i++)
                    {
                        if (Convert.ToInt32(viewSiteContracts.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                        {
                            int intDefaultJobCollectionTemplateID = Convert.ToInt32(viewSiteContracts.GetRowCellValue(i, "DefaultJobCollectionTemplateID"));
                            if (intDefaultJobCollectionTemplateID > 0 && !JobTemplatesList.Contains(intDefaultJobCollectionTemplateID)) JobTemplatesList.Add(intDefaultJobCollectionTemplateID);
                        }
                    }
                    if (JobTemplatesList.Count > 0)  // Only process if we have some Job Collection Templates //
                    {
                        foreach (int DefaultJobCollectionTemplateID in JobTemplatesList)
                        {
                            // Build unique list of selected Site Contracts selected which share this DefaultJobCollectionTemplateID //
                            SiteContractsList.Clear();
                            for (int i = 0; i < intSiteContractsCount; i++)
                            {
                                if (Convert.ToInt32(viewSiteContracts.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                                {
                                    int intDefaultJobCollectionTemplateID = Convert.ToInt32(viewSiteContracts.GetRowCellValue(i, "DefaultJobCollectionTemplateID"));
                                    if (Convert.ToInt32(viewSiteContracts.GetRowCellValue(i, "DefaultJobCollectionTemplateID")) == DefaultJobCollectionTemplateID) SiteContractsList.Add(Convert.ToInt32(viewSiteContracts.GetRowCellValue(i, "SiteContractID")));
                                }
                            }
                            if (SiteContractsList.Count > 0)  // Only process if we have some Site Contracts //
                            {
                                // Build comma separated string of VisitID from selected Visit which are libked to the processed SiteContractID //
                                foreach (int SiteContractsID in SiteContractsList)
                                {
                                    sbVisitIDs.Clear();
                                    for (int i = 0; i < viewVisits.DataRowCount; i++)
                                    {
                                        if (Convert.ToInt32(viewVisits.GetRowCellValue(i, "SiteContractID")) == SiteContractsID) sbVisitIDs.Append(viewVisits.GetRowCellValue(i, "VisitID").ToString() + ",");
                                    }
                                    if (!string.IsNullOrWhiteSpace(sbVisitIDs.ToString()))
                                    {
                                        SqlConnection SQlConn = new SqlConnection(strConnectionString);
                                        SqlCommand cmd = null;
                                        cmd = null;
                                        cmd = new SqlCommand("sp06402_OM_Jobs_From_Job_Collection_Jobs_For_Visit", SQlConn);
                                        cmd.CommandType = CommandType.StoredProcedure;
                                        cmd.Parameters.Add(new SqlParameter("@JobCollectionTemplateHeaderID", DefaultJobCollectionTemplateID));
                                        cmd.Parameters.Add(new SqlParameter("@VisitIDs", sbVisitIDs.ToString()));
                                        cmd.Parameters.Add(new SqlParameter("@StaffID", GlobalSettings.UserID));
                                        SqlDataAdapter sdaJobs = new SqlDataAdapter(cmd);
                                        DataSet dsJobs = new DataSet("NewDataSet");
                                        sdaJobs.Fill(dsJobs, "Table");

                                        if (dsJobs.Tables[0].Rows.Count > 0)
                                        {
                                            foreach (DataRow dr in dsJobs.Tables[0].Rows)
                                            {
                                                //dataSet_OM_Job.sp06174_OM_Job_Edit.ImportRow(dr);
                                                dataSet_OM_Job.sp06174_OM_Job_Edit.ImportRow(dr);
                                                DataRow row = dataSet_OM_Job.sp06174_OM_Job_Edit.Rows[dataSet_OM_Job.sp06174_OM_Job_Edit.Rows.Count - 1];
                                                row.AcceptChanges();
                                                row.SetAdded();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception) { }

                gridControl7.EndUpdate();
                gridControl9.EndUpdate();
                gridControl11.EndUpdate();

                viewNew.ExpandAllGroups();
                viewNew.EndDataUpdate();
                viewNew.EndUpdate();

                viewJobs.ExpandAllGroups();
                viewJobs.BeginSelection();
                viewJobs.SelectAll();
                viewJobs.EndSelection();

                // Now Clear Filter on Visits Grid //
                if (!ReferenceEquals(viewVisits.ActiveFilter.Criteria, null)) viewVisits.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                if (!ReferenceEquals(viewMasterJobSubTypes.ActiveFilter.Criteria, null)) viewMasterJobSubTypes.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                viewMasterJobSubTypes.ExpandAllGroups();

                //if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
                GridView view5 = (GridView)gridControl5.MainView;
                view5.ExpandAllGroups();
                view5.SelectAll();
            }
            else  // Apply the Rules for the selected template //
            {
                Clear_Created_Jobs();
                Clear_Created_Labour();
                Clear_Created_Equipment();
                Clear_Created_Materials();
                GridView viewNew = (GridView)gridControl6.MainView;

                // Now Create Jobs based on selected Visits and Selected Job Sub-Types //
                viewNew.BeginUpdate();
                viewNew.BeginDataUpdate();
                GridView viewVisits = (GridView)gridControl3.MainView;
                viewVisits.ActiveFilterString = "[CheckMarkSelection] = 1";  // Apply filter to selected rows //

                GridView viewJobs = (GridView)gridControl7.MainView;
                GridView viewEquipment = (GridView)gridControl9.MainView;
                GridView viewMaterials = (GridView)gridControl11.MainView;

                gridControl7.BeginUpdate();
                gridControl9.BeginUpdate();
                gridControl11.BeginUpdate();

                StringBuilder sb = new StringBuilder();
                try
                {
                    for (int i = 0; i < viewVisits.DataRowCount; i++)
                    {
                        sb.Append(viewVisits.GetRowCellValue(i, "VisitID").ToString() + ",");
                    }

                    SqlConnection SQlConn = new SqlConnection(strConnectionString);
                    SqlCommand cmd = null;
                    cmd = null;
                    cmd = new SqlCommand("sp06402_OM_Jobs_From_Job_Collection_Jobs_For_Visit", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@JobCollectionTemplateHeaderID", intSelectedJobTemplateCollectionID));
                    cmd.Parameters.Add(new SqlParameter("@VisitIDs", sb.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@StaffID", GlobalSettings.UserID));
                    SqlDataAdapter sdaJobs = new SqlDataAdapter(cmd);
                    DataSet dsJobs = new DataSet("NewDataSet");
                    sdaJobs.Fill(dsJobs, "Table");

                    if (dsJobs.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsJobs.Tables[0].Rows)
                        {
                            //dataSet_OM_Job.sp06174_OM_Job_Edit.ImportRow(dr);
                            dataSet_OM_Job.sp06174_OM_Job_Edit.ImportRow(dr);
                            DataRow row = dataSet_OM_Job.sp06174_OM_Job_Edit.Rows[dataSet_OM_Job.sp06174_OM_Job_Edit.Rows.Count - 1];
                            row.AcceptChanges();
                            row.SetAdded();
                        }
                    }
                }
                catch (Exception ex) { }

                gridControl7.EndUpdate();
                gridControl9.EndUpdate();
                gridControl11.EndUpdate();

                viewNew.ExpandAllGroups();
                viewNew.EndDataUpdate();
                viewNew.EndUpdate();

                viewJobs.ExpandAllGroups();
                viewJobs.BeginSelection();
                viewJobs.SelectAll();
                viewJobs.EndSelection();

                // Now Clear Filter on Visits Grid //
                if (!ReferenceEquals(viewVisits.ActiveFilter.Criteria, null)) viewVisits.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                if (!ReferenceEquals(viewMasterJobSubTypes.ActiveFilter.Criteria, null)) viewMasterJobSubTypes.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                viewMasterJobSubTypes.ExpandAllGroups();

                //if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
                GridView view5 = (GridView)gridControl5.MainView;
                view5.ExpandAllGroups();
                view5.SelectAll();
            }
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep5;
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void Load_Master_Jobs()
        {
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            GridView view = (GridView)gridControl4.MainView;
            view.BeginUpdate();
            try
            {
                sp06162_OM_Job_Wizard_Master_Job_Sub_Types_AvailableTableAdapter.Fill(dataSet_OM_Job.sp06162_OM_Job_Wizard_Master_Job_Sub_Types_Available, i_str_SelectedVisitTypeIDs);  // i_str_SelectedVisitTypeIDs is set on MoveToPage4() event //
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Client Contracts.\n\nMessage = [" + ex.Message + "].\n\nPlease close the screen then try again. If the problem persists, contact Technical Support.", "Load Client Contracts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            view.ExpandAllGroups();
            view.EndUpdate();
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void Set_Selected_Job_Count_Label()
        {
            labelControlSelectedJobCount.Text = "       " + selection4.SelectedCount.ToString() + " Selected Job(s)";
        }

        private void Get_Client_PO(int intRecordID, int intRecordTypeID, ref int intClientPOID, ref string strClientPONumber)
        {
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp06328_OM_Default_Client_PO_Number_For_Record", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@RecordID", intRecordID));
            cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordTypeID));
            SqlDataAdapter sdaPO = new SqlDataAdapter(cmd);
            DataSet dsPO = new DataSet("NewDataSet");
            sdaPO.Fill(dsPO, "Table");
            foreach (DataRow dr in dsPO.Tables[0].Rows)
            {
                intClientPOID = Convert.ToInt32(dr["ClientPurchaseOrderID"]);
                strClientPONumber = dr["PONumber"].ToString();
                break;
            }
        }

        #region GridView4

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Master_Jobs();
                        selection4.ClearSelection();
                        Set_Selected_Job_Count_Label();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.JobMaster;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.JobMaster;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bsiRecordTicking.Enabled = (intCount > 0);
                bbiTick.Enabled = (intCount > 0);
                bbiUntick.Enabled = (intCount > 0);
                
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion

        private void bbiSelectJobCollection_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Get list of selected visits and extract unique client IDs to pass to Select Job Templated screen //
            GridView viewVisits = (GridView)gridControl3.MainView;
            if (!ReferenceEquals(viewVisits.ActiveFilter.Criteria, null)) viewVisits.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(viewVisits.FindFilterText)) viewVisits.ApplyFindFilter("");  // Clear any Find in place //  

            int intCount = selection3.SelectedCount;
            int intProcessedCount = 0;
            string strClientFilter = ",";
            if (intCount > 0)
            {
                for (int i = 0; i < viewVisits.DataRowCount; i++)
                {
                    if (Convert.ToInt32(viewVisits.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        if (!strClientFilter.Contains(',' + viewVisits.GetRowCellValue(i, "ClientID").ToString() + ','))
                        {
                            strClientFilter += viewVisits.GetRowCellValue(i, "ClientID").ToString() + ',';
                        }
                        intProcessedCount++;
                        if (intProcessedCount >= intCount) break;
                    }
                }
            }
            strClientFilter = strClientFilter.Remove(0, 1);  // Remove dummy ',' at start //
            
            selection4.ClearSelection();
            var fChildForm = new frm_OM_Select_Job_Collection_Template_Header();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._PassedInClientIDsFilter = strClientFilter;

            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                if (!fChildForm._boolApplyRules)
                {
                    intSelectedJobTemplateCollectionID = 0;
                    string strSelectedItemIDs = fChildForm.strSelectedItemIDs;
                    if (string.IsNullOrWhiteSpace(strSelectedItemIDs)) return;
                    strSelectedItemIDs = "," + strSelectedItemIDs;  // Add preceeding comma for matching purposes //

                    GridView view = (GridView)gridControl4.MainView;
                    view.BeginUpdate();
                    intCount = view.DataRowCount;
                    for (int i = 0; i < intCount; i++)
                    {
                        if (strSelectedItemIDs.Contains("," + view.GetRowCellValue(i, "JobSubTypeID").ToString() + ","))
                        {
                            view.SetRowCellValue(i, "CheckMarkSelection", 1);
                        }
                    }
                    view.EndUpdate();
                }
                else  // Select and apply rules //
                {
                    intSelectedJobTemplateCollectionID = fChildForm.intSelectedTemplateHeaderID;
                    btnStep4Next.PerformClick();
                }
            }
        }

        private void bbiJobCollectionManager_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var fChildForm = new frm_OM_Job_Collection_Template_Manager();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();
            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void bbiMasterJobManager_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var fChildForm = new frm_Core_Master_Job_Manager();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();
            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }
        
        private void checkEditJobsFromLastVisit_EditValueChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                gridControl4.Enabled = false;
                selection4.ClearSelection();
                Set_Selected_Job_Count_Label();
                beiJobsFromSiteTemplate.EditValue = 0;
            }
            else
            {
                gridControl4.Enabled = true;
            }
        }

        private void checkEditJobsFromSiteTemplate_EditValueChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                gridControl4.Enabled = false;
                selection4.ClearSelection();
                Set_Selected_Job_Count_Label();
                beiJobsFromLastVisit.EditValue = 0;
            }
            else
            {
                gridControl4.Enabled = true;
            }
        }

        #endregion


        #region Step 5 Page - Jobs

        private void btnStep5Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep4;
        }
        private void btnStep5Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl6.MainView;
            view.PostEditor();
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            Clear_Filter_Jobs_On_Visit();
            
            if (view.DataRowCount <= 0)
            {
                XtraMessageBox.Show("No Job to be created. Move back to the previous step to select the work to be added before proceeding.", "Refine Work", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            Clear_Created_Labour();
            Clear_Created_Equipment();
            Clear_Created_Materials();

            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep6;
            view = (GridView)gridControl7.MainView;
            view.ExpandAllGroups();

            if (i_intPassedInLabourID > 0)  // Called by a tender so select all jobs and add the labour //
            {
                view.SelectAll();
                Add_Labour(i_intPassedInLabourID);
            }

            Filter_Labour_On_Jobs();
        }


        #region GridView5

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            SetMenuStatus();
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;
                
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView6

        private void gridControl6_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Job();
                    }
                    else if ("block add".Equals(e.Button.Tag))
                    {
                        Block_Add_Job();
                    }
                    else if ("block edit".Equals(e.Button.Tag))
                    {
                        Block_Edit_Jobs();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView6_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "ExpectedStartDate":
                    {
                        if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "ExpectedStartDate")) < DateTime.Today)
                        {
                            e.Appearance.BackColor = Color.FromArgb(248, 196, 196);
                            e.Appearance.ForeColor = Color.FromArgb(198, 16, 61);
                        }
                    }
                    break;
                case "ExpectedEndDate":
                    {
                        if (i_dtPassedInWorkCompletedBy != null)
                        {
                            if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "ExpectedEndDate")) > i_dtPassedInWorkCompletedBy)
                            {
                                e.Appearance.BackColor = Color.FromArgb(248, 196, 196);
                                e.Appearance.ForeColor = Color.FromArgb(198, 16, 61);
                            }
                        }
                    }
                    break;
            }
        }

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;
            SetMenuStatus();
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemButtonEditJobTypeJobSubType_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            GridView view = (GridView)gridControl6.MainView;
            int intFocusedRow = view.FocusedRowHandle;
            if (intFocusedRow == GridControl.InvalidRowHandle) return;
            
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
                var fChildForm = new frm_OM_Select_Job_Sub_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm._Mode = "single";
                //fChildForm.intOperationManagerJobsOnly = 1;
                fChildForm.strPassedInVisitTypeIDs = i_str_SelectedVisitTypeIDs;

                if (currentRow == null) return;
                if (strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    fChildForm.intOriginalParentID = 0;
                    fChildForm.intOriginalChildID = 0;
                }
                else
                {
                    fChildForm.intOriginalParentID = (string.IsNullOrWhiteSpace(currentRow.JobTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.JobTypeID));
                    fChildForm.intOriginalChildID = (string.IsNullOrWhiteSpace(currentRow.JobSubTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.JobSubTypeID));
                }

                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.JobSubTypeID = fChildForm.intSelectedChildID;
                    currentRow.JobTypeID = fChildForm.intSelectedParentID;
                    currentRow.JobSubTypeDescription = fChildForm.strSelectedChildDescriptions;
                    currentRow.JobTypeDescription = fChildForm.strSelectedParentDescriptions;
                    currentRow.JobTypeJobSubTypeDescription = fChildForm.strSelectedParentDescriptions + " - " + fChildForm.strSelectedChildDescriptions;
                    sp06174OMJobEditBindingSource.EndEdit();
                }
            }
        }

        private void repositoryItemDateEdit1_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            GridView view = (GridView)gridControl6.MainView;
            int intFocusedRow = view.FocusedRowHandle;
            if (intFocusedRow == GridControl.InvalidRowHandle) return;
            if (!(view.FocusedColumn.Name == "colExpectedStartDate" || view.FocusedColumn.Name == "colExpectedEndDate")) return;
                
            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;

            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            if (view.FocusedColumn.Name == "colExpectedStartDate")
            {
                DateEdit de = (DateEdit)sender;
                dtStart = de.DateTime;
                try { dtEnd = currentRow.ExpectedEndDate; }
                catch (Exception) { }
            }
            else  // colExpectedEndDate //
            {
                try { dtStart = currentRow.ExpectedStartDate; }
                catch (Exception) { }
                DateEdit de = (DateEdit)sender;
                dtEnd = de.DateTime;
            }
            decUnits = currentRow.ExpectedDurationUnits;
            intUnitDescriptorID = currentRow.ExpectedDurationUnitsDescriptorID;
            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, view.FocusedColumn.Name);
        }

        private void repositoryItemSpinEdit2DP_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            GridView view = (GridView)gridControl6.MainView;
            int intFocusedRow = view.FocusedRowHandle;
            if (intFocusedRow == GridControl.InvalidRowHandle) return;
            if (view.FocusedColumn.Name != "colExpectedDurationUnits") return;

            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;

            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            try { dtStart = currentRow.ExpectedStartDate; }
            catch (Exception) { }
            
            try { dtEnd = currentRow.ExpectedEndDate; }
            catch (Exception) { }

            SpinEdit se = (SpinEdit)sender;
            decUnits = Convert.ToDecimal(se.Value);
            intUnitDescriptorID = currentRow.ExpectedDurationUnitsDescriptorID;
            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, view.FocusedColumn.Name);
        }

        private void repositoryItemGridLookUpEditDurationUnitDescriptor_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            GridView view = (GridView)gridControl6.MainView;
            int intFocusedRow = view.FocusedRowHandle;
            if (intFocusedRow == GridControl.InvalidRowHandle) return;
            if (view.FocusedColumn.Name != "colExpectedDurationUnitsDescriptorID") return;

            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;

            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            try { dtStart = currentRow.ExpectedStartDate; }
            catch (Exception) { }
            
            try { dtEnd = currentRow.ExpectedEndDate; }
            catch (Exception) { }

            decUnits = currentRow.ExpectedDurationUnits;
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            intUnitDescriptorID = Convert.ToInt32(glue.EditValue);
            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, view.FocusedColumn.Name);
        }

        private void CalculateDates(DateTime? StartDate, decimal Units, int UnitDescriptorID, DateTime? EndDate, string ChangedColumnName)
        {
            GridView view = (GridView)gridControl6.MainView;
            int intFocusedRow = view.FocusedRowHandle;
            if (intFocusedRow == GridControl.InvalidRowHandle) return;

            var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
            var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;

            switch (ChangedColumnName)
            {
                case "colExpectedStartDate":
                    {
                        if (StartDate <= DateTime.MinValue) return;
                        if (Units > (decimal)0.00 && UnitDescriptorID > 0)  // Calculate End Date //
                        {
                            switch (UnitDescriptorID)
                            {
                                case 1:  // Hours //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddHours((double)Units);
                                    }
                                    break;
                                case 2:  // Days //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddDays((double)Units);
                                    }
                                    break;
                                case 3:  // Weeks //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddDays((double)Units * (double)7);
                                    }
                                    break;
                                case 4:  // Months //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddMonths((int)Units);
                                    }
                                    break;
                                case 5:  // Years //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddYears((int)Units);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            currentRow.ExpectedEndDate = (EndDate == null ? DateTime.MinValue : (DateTime)EndDate);
                        }
                        else if (EndDate != null)  // Calculate Duration //
                        {
                            // Calculate the interval between the two dates.
                            DateDiff dateDiff = DateDiff.Difference((DateTime)StartDate, (DateTime)EndDate);

                            if (dateDiff.Years > 0 && (dateDiff.Months == 0 && dateDiff.Weeks == 0 && dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                Units = (decimal)dateDiff.Years;
                                UnitDescriptorID = 5;
                            }
                            else if (dateDiff.Months > 0 && (dateDiff.Weeks == 0 && dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                Units = (decimal)dateDiff.Months + ((decimal)dateDiff.Years * (decimal)12);
                                UnitDescriptorID = 4;
                            }
                            else if (dateDiff.Weeks > 0 && (dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalDays / (decimal)7;
                                UnitDescriptorID = 3;
                            }
                            else if (dateDiff.Days > 0 && (dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalDays;
                                UnitDescriptorID = 2;
                                if (Units % 7 == 0) // Check if remainder is 0 if yes then number is devisable for weeks so convert to weeks //
                                {
                                    Units = (decimal)ts.TotalDays / (decimal)7;
                                    UnitDescriptorID = 3;
                                }
                            }
                            else if (dateDiff.Hours > 0 && (dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalHours;
                                UnitDescriptorID = 1;
                            }
                            else if (dateDiff.Minutes > 0)
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalMinutes / (decimal)60;
                                UnitDescriptorID = 1;
                            }
                            currentRow.ExpectedDurationUnits = (Units <= (decimal)0.00 ? (decimal)0.00 : Units);
                            currentRow.ExpectedDurationUnitsDescriptorID = UnitDescriptorID;
                        }
                    }
                    break;
                case "colExpectedEndDate":
                    {
                        if (EndDate <= DateTime.MinValue) return;
                        if (Units > (decimal)0.00 && UnitDescriptorID > 0)  // Calculate End Date //
                        {
                            switch (UnitDescriptorID)
                            {
                                case 1:  // Hours //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddHours((double)Units * (double)-1);
                                    }
                                    break;
                                case 2:  // Days //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddDays((double)Units * (double)-1);
                                    }
                                    break;
                                case 3:  // Weeks //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddDays(((double)Units * (double)7) * (double)-1);
                                    }
                                    break;
                                case 4:  // Months //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddMonths((int)Units * -1);
                                    }
                                    break;
                                case 5:  // Years //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddYears((int)Units * -1);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            currentRow.ExpectedStartDate = (StartDate == null ? DateTime.MinValue : (DateTime)StartDate);
                        }
                        else if (StartDate != null)  // Calculate Duration //
                        {
                            // Calculate the interval between the two dates.
                            DateDiff dateDiff = DateDiff.Difference((DateTime)StartDate, (DateTime)EndDate);

                            if (dateDiff.Years > 0 && (dateDiff.Months == 0 && dateDiff.Weeks == 0 && dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                Units = (decimal)dateDiff.Years;
                                UnitDescriptorID = 5;
                            }
                            else if (dateDiff.Months > 0 && (dateDiff.Weeks == 0 && dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                Units = (decimal)dateDiff.Months + ((decimal)dateDiff.Years * (decimal)12);
                                UnitDescriptorID = 4;
                            }
                            else if (dateDiff.Weeks > 0 && (dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalDays / (decimal)7;
                                UnitDescriptorID = 3;
                            }
                            else if (dateDiff.Days > 0 && (dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalDays;
                                UnitDescriptorID = 2;
                                if (Units % 7 == 0) // Check if remainder is 0 if yes then number is devisable for weeks so convert to weeks //
                                {
                                    Units = (decimal)ts.TotalDays / (decimal)7;
                                    UnitDescriptorID = 3;
                                }
                            }
                            else if (dateDiff.Hours > 0 && (dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalHours;
                                UnitDescriptorID = 1;
                            }
                            else if (dateDiff.Minutes > 0)
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalMinutes / (decimal)60;
                                UnitDescriptorID = 1;
                            }
                            currentRow.ExpectedDurationUnits = (Units <= (decimal)0.00 ? (decimal)0.00 : Units);
                            currentRow.ExpectedDurationUnitsDescriptorID = UnitDescriptorID;
                        }
                    }
                    break;
                case "colExpectedDurationUnits":
                case "colExpectedDurationUnitsDescriptorID":
                    {
                        if (Units > (decimal)0.00 && UnitDescriptorID > 0 && StartDate != null)  // Calculate End Date //
                        {
                            switch (UnitDescriptorID)
                            {
                                case 1:  // Hours //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddHours((double)Units);
                                    }
                                    break;
                                case 2:  // Days //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddDays((double)Units);
                                    }
                                    break;
                                case 3:  // Weeks //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddDays((double)Units * (double)7);
                                    }
                                    break;
                                case 4:  // Months //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddMonths((int)Units);
                                    }
                                    break;
                                case 5:  // Years //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddYears((int)Units);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            currentRow.ExpectedEndDate = (EndDate == null ? DateTime.MinValue : (DateTime)EndDate);
                        }
                        else if (Units > (decimal)0.00 && UnitDescriptorID > 0 && EndDate != null)  // Calculate Start Date //
                        {
                            switch (UnitDescriptorID)
                            {
                                case 1:  // Hours //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddHours((double)Units * (double)-1);
                                    }
                                    break;
                                case 2:  // Days //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddDays((double)Units * (double)-1);
                                    }
                                    break;
                                case 3:  // Weeks //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddDays(((double)Units * (double)7) * (double)-1);
                                    }
                                    break;
                                case 4:  // Months //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddMonths((int)Units * -1);
                                    }
                                    break;
                                case 5:  // Years //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddYears((int)Units * -1);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            currentRow.ExpectedStartDate = (StartDate == null ? DateTime.MinValue : (DateTime)StartDate);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemButtonEditClientPONumber_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                int intClientContractID = 0;
                try { intClientContractID = (string.IsNullOrEmpty(currentRow.ClientContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientContractID)); }
                catch (Exception) { }

                int intSiteContractID = 0;
                try { intSiteContractID = (string.IsNullOrEmpty(currentRow.SiteContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteContractID)); }
                catch (Exception) { }

                int intClientPOID = 0;
                try { intClientPOID = (string.IsNullOrEmpty(currentRow.ClientPOID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientPOID)); }
                catch (Exception) { }

                int intVisitID = 0;
                try { intVisitID = (string.IsNullOrEmpty(currentRow.VisitID.ToString()) ? 0 : Convert.ToInt32(currentRow.VisitID)); }
                catch (Exception) { }

                var fChildForm = new frm_OM_Select_Client_PO_Multi_Page();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.boolPage1Enabled = true;
                fChildForm.boolPage2Enabled = true;
                fChildForm.boolPage3Enabled = true;
                fChildForm.intActivePage = 3;
                fChildForm._PassedInClientContractIDs = intClientContractID.ToString() + ",";
                fChildForm._PassedInSiteContractIDs = intSiteContractID.ToString() + ",";
                fChildForm._PassedInVisitIDs = intVisitID.ToString() + ",";
                fChildForm.intOriginalPOID = intClientPOID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.ClientPOID = fChildForm.intSelectedPOID;
                    currentRow.ClientPONumber = fChildForm.strSelectedPONumbers;
                    sp06174OMJobEditBindingSource.EndEdit();
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                var currentRowView = (DataRowView)sp06174OMJobEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06174_OM_Job_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                currentRow.ClientPOID = 0;
                currentRow.ClientPONumber = "";
                sp06174OMJobEditBindingSource.EndEdit();
            }
        }

        #endregion


        private void Filter_Jobs_On_Visit()
        {
            GridView view = (GridView)gridControl6.MainView;
            view.PostEditor();

            string strFilterCondition = "";
            GridView parentView = (GridView)gridControl5.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                strFilterCondition = "[VisitID] = 9999999999";  // Don't show anything //
            }
            else
            {
                foreach (int i in intRowHandles)
                {
                    strFilterCondition += (string.IsNullOrWhiteSpace(strFilterCondition) ? "" : " or ");
                    strFilterCondition += "[VisitID] = " + parentView.GetRowCellValue(i, "VisitID").ToString();
                }
            }
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = strFilterCondition;
            view.MakeRowVisible(-1, true);
            view.ExpandAllGroups();
            view.EndUpdate();
            SetMenuStatus();
        }

        private void Clear_Filter_Jobs_On_Visit()
        {
            GridView view = (GridView)gridControl6.MainView;
            view.PostEditor();

            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
            SetMenuStatus();
        }

        private void Add_Job()
        {
            GridView viewVisits = (GridView)gridControl5.MainView;
            int[] intRowHandles = viewVisits.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Visits to add the Jobs to before proceeding.", "Add Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intRowHandles.Length > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + " Visit(s) selected. If you proceed a new Job will be created for each of these records.\n\nProceed?", "Add Jobs", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }
            var fChildForm = new frm_OM_Select_Job_Sub_Type();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "single";
            //fChildForm.intOperationManagerJobsOnly = 1;
            fChildForm.strPassedInVisitTypeIDs = i_str_SelectedVisitTypeIDs;

            GridView view = (GridView)gridControl6.MainView;
            view.PostEditor();
            int intVisitID = 0;
            DateTime dtStartDate = DateTime.MinValue;
            DateTime dtEndDate = DateTime.MinValue;
            int intJobTypeID = 0;
            int intJobSubTypeID = 0;
            string strJobDescription = "";
            string strJobSubTypeDescription = "";
            int intSiteContractID = 0;
            int intClientContractID = 0;
            int intSiteID = 0;
            int intClientID = 0;
            string strContractDescription = "";
            string strClientName = "";
            string strSiteName = "";
            int intVisitNumber = 0;
            double dblSiteLocationX = (double)0.00;
            double dblSiteLocationY = (double)0.00;
            string strSitePostcode = "";

            if (fChildForm.ShowDialog() != DialogResult.OK) return;
            intJobSubTypeID = fChildForm.intSelectedChildID;
            intJobTypeID = fChildForm.intSelectedParentID;
            strJobSubTypeDescription = fChildForm.strSelectedChildDescriptions;
            strJobDescription = fChildForm.strSelectedParentDescriptions;

            view.BeginUpdate();  // Lock view so we don't see filter switching on and off //
            foreach (int intRowHandle in intRowHandles)
            {
                intVisitID = Convert.ToInt32(viewVisits.GetRowCellValue(intRowHandle, "VisitID"));
                intVisitNumber = Convert.ToInt32(viewVisits.GetRowCellValue(intRowHandle, "VisitNumber"));
                dtStartDate = Convert.ToDateTime(viewVisits.GetRowCellValue(intRowHandle, "ExpectedStartDate"));
                dtEndDate = dtStartDate.AddHours((double)1);
                intSiteContractID = Convert.ToInt32(viewVisits.GetRowCellValue(intRowHandle, "SiteContractID"));
                intClientContractID = Convert.ToInt32(viewVisits.GetRowCellValue(intRowHandle, "ClientContractID"));
                intSiteID = Convert.ToInt32(viewVisits.GetRowCellValue(intRowHandle, "SiteID"));
                intClientID = Convert.ToInt32(viewVisits.GetRowCellValue(intRowHandle, "ClientID"));
                strContractDescription = viewVisits.GetRowCellValue(intRowHandle, "ContractDescription").ToString();
                strClientName = viewVisits.GetRowCellValue(intRowHandle, "ClientName").ToString();
                strSiteName = viewVisits.GetRowCellValue(intRowHandle, "SiteName").ToString();
                intVisitNumber = Convert.ToInt32(viewVisits.GetRowCellValue(intRowHandle, "VisitNumber"));
                dblSiteLocationX = Convert.ToDouble(viewVisits.GetRowCellValue(intRowHandle, "LocationX"));
                dblSiteLocationY = Convert.ToDouble(viewVisits.GetRowCellValue(intRowHandle, "LocationY"));
                strSitePostcode = viewVisits.GetRowCellValue(intRowHandle, "SitePostcode").ToString();

                try
                {
                    DataRow drNewRow;
                    drNewRow = dataSet_OM_Job.sp06174_OM_Job_Edit.NewRow();
                    drNewRow["strMode"] = "add";
                    drNewRow["strRecordIDs"] = "";
                    drNewRow["VisitID"] = intVisitID;
                    drNewRow["VisitNumber"] = intVisitNumber;
                    drNewRow["JobSubTypeID"] = intJobSubTypeID;
                    drNewRow["JobTypeID"] = intJobTypeID;
                    drNewRow["CreatedByStaffID"] = GlobalSettings.UserID;
                    drNewRow["CreatedByStaffName"] = GlobalSettings.UserSurname + ": " + GlobalSettings.UserForename;
                    drNewRow["Reactive"] = 0;
                    drNewRow["JobNoLongerRequired"] = 0;
                    drNewRow["RequiresAccessPermit"] = 0;
                    drNewRow["AccessPermitID"] = 0;
                    drNewRow["ClientPONumber"] = "";
                    drNewRow["ClientPOID"] = 0;
                    drNewRow["FinanceSystemPONumber"] = "";
                    drNewRow["ExpectedStartDate"] = dtStartDate;
                    drNewRow["ExpectedEndDate"] = dtEndDate;
                    drNewRow["ExpectedDurationUnits"] = 1.00;
                    drNewRow["ExpectedDurationUnitsDescriptorID"] = 1;  // Hours //
                    drNewRow["ActualDurationUnits"] = 0.00;
                    drNewRow["ActualDurationUnits"] = 0.00;
                    drNewRow["ActualDurationUnitsDescriptionID"] = 0;
                    drNewRow["MinimumDaysFromLastVisit"] = 0;
                    drNewRow["MaximumDaysFromLastVisit"] = 0;
                    drNewRow["DaysLeeway"] = 0;
                    drNewRow["SiteContractID"] = intSiteContractID;
                    drNewRow["ClientContractID"] = intClientContractID;
                    drNewRow["SiteID"] = intSiteID;
                    drNewRow["ClientID"] = intClientID;
                    drNewRow["ContractDescription"] = strContractDescription;
                    drNewRow["ClientNameContractDescription"] = strClientName + ", Contract: " + strContractDescription;
                    drNewRow["ClientName"] = strClientName;
                    drNewRow["SiteName"] = strSiteName;
                    drNewRow["JobSubTypeDescription"] = strJobSubTypeDescription;
                    drNewRow["JobTypeDescription"] = strJobDescription;
                    drNewRow["JobTypeJobSubTypeDescription"] = strJobDescription + " - " + strJobSubTypeDescription;
                    drNewRow["LabourCount"] = 0;
                    drNewRow["EquipmentCount"] = 0;
                    drNewRow["MaterialCount"] = 0;
                    drNewRow["LocationX"] = dblSiteLocationX;
                    drNewRow["LocationY"] = dblSiteLocationY;
                    drNewRow["SitePostcode"] = strSitePostcode;
                    drNewRow["JobStatusID"] = 10;  // Job Creation Stated //
                    dataSet_OM_Job.sp06174_OM_Job_Edit.Rows.Add(drNewRow);
                }
                catch (Exception) { } 
            }
            view.EndUpdate();  // UnLock view //
            view.ExpandAllGroups();
        }

        private void Block_Add_Job()
        {
            GridView viewVisits = (GridView)gridControl5.MainView;
            int[] intRowHandles = viewVisits.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Visits to block add the Jobs to before proceeding.", "Block Add Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intRowHandles.Length > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + " Visit(s) selected. If you proceed one or more new Jobs will be created for each of these records.\n\nProceed?", "Block Add Jobs", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }
            var fChildForm = new frm_OM_Select_Job_Sub_Type();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "multiple";
            //fChildForm.intOperationManagerJobsOnly = 1;
            fChildForm.strPassedInVisitTypeIDs = i_str_SelectedVisitTypeIDs;

            GridView view = (GridView)gridControl6.MainView;
            view.PostEditor();
            int intVisitID = 0;
            DateTime dtStartDate = DateTime.MinValue;
            DateTime dtEndDate = DateTime.MinValue;
            int intJobTypeID = 0;
            int intJobSubTypeID = 0;
            string strJobDescription = "";
            string strJobSubTypeDescription = "";
            int intSiteContractID = 0;
            int intClientContractID = 0;
            int intSiteID = 0;
            int intClientID = 0;
            string strContractDescription = "";
            string strClientName = "";
            string strSiteName = "";
            int intVisitNumber = 0;
            double dblSiteLocationX = (double)0.00;
            double dblSiteLocationY = (double)0.00;
            string strSitePostcode = "";
            int intClientPOID = 0;
            string strClientPONumber = "";

            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {

                view.BeginUpdate();  // Lock view so we don't see filter switching on and off //
                foreach (int intRowHandle in intRowHandles)
                {
                    intVisitID = Convert.ToInt32(viewVisits.GetRowCellValue(intRowHandle, "VisitID"));
                    intVisitNumber = Convert.ToInt32(viewVisits.GetRowCellValue(intRowHandle, "VisitNumber"));
                    dtStartDate = Convert.ToDateTime(viewVisits.GetRowCellValue(intRowHandle, "ExpectedStartDate"));
                    dtEndDate = dtStartDate.AddHours((double)1);
                    intSiteContractID = Convert.ToInt32(viewVisits.GetRowCellValue(intRowHandle, "SiteContractID"));
                    intClientContractID = Convert.ToInt32(viewVisits.GetRowCellValue(intRowHandle, "ClientContractID"));
                    intSiteID = Convert.ToInt32(viewVisits.GetRowCellValue(intRowHandle, "SiteID"));
                    intClientID = Convert.ToInt32(viewVisits.GetRowCellValue(intRowHandle, "ClientID"));
                    strContractDescription = viewVisits.GetRowCellValue(intRowHandle, "ContractDescription").ToString();
                    strClientName = viewVisits.GetRowCellValue(intRowHandle, "ClientName").ToString();
                    strSiteName = viewVisits.GetRowCellValue(intRowHandle, "SiteName").ToString();
                    intVisitNumber = Convert.ToInt32(viewVisits.GetRowCellValue(intRowHandle, "VisitNumber"));
                    dblSiteLocationX = Convert.ToDouble(viewVisits.GetRowCellValue(intRowHandle, "LocationX"));
                    dblSiteLocationY = Convert.ToDouble(viewVisits.GetRowCellValue(intRowHandle, "LocationY"));
                    strSitePostcode = viewVisits.GetRowCellValue(intRowHandle, "SitePostcode").ToString();
                    intClientPOID = Convert.ToInt32(viewVisits.GetRowCellValue(intRowHandle, "ClientPOID"));
                    strClientPONumber = viewVisits.GetRowCellValue(intRowHandle, "ClientPONumber").ToString();

                    if (intClientPOID <= 0)
                    {
                        // Update default Client PO # //
                        int intRecordTypeID = 2;
                        SqlConnection SQlConn = new SqlConnection(strConnectionString);
                        SqlCommand cmd = null;
                        cmd = new SqlCommand("sp06328_OM_Default_Client_PO_Number_For_Record", SQlConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@RecordID", intVisitID));
                        cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordTypeID));
                        SqlDataAdapter sdaPO = new SqlDataAdapter(cmd);
                        DataSet dsPO = new DataSet("NewDataSet");
                        sdaPO.Fill(dsPO, "Table");
                        foreach (DataRow dr in dsPO.Tables[0].Rows)
                        {
                            intClientPOID = Convert.ToInt32(dr["ClientPurchaseOrderID"]);
                            strClientPONumber = dr["PONumber"].ToString();
                            break;
                        }
                        if (intClientPOID <= 0)  // No Visit specific Client PO ID so try for Site Contract specific //
                        {
                            intRecordTypeID = 1;
                            cmd = null;
                            cmd = new SqlCommand("sp06328_OM_Default_Client_PO_Number_For_Record", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@RecordID", intSiteContractID));
                            cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordTypeID));
                            sdaPO = new SqlDataAdapter(cmd);
                            dsPO = new DataSet("NewDataSet");
                            sdaPO.Fill(dsPO, "Table");
                            foreach (DataRow dr in dsPO.Tables[0].Rows)
                            {
                                intClientPOID = Convert.ToInt32(dr["ClientPurchaseOrderID"]);
                                strClientPONumber = dr["PONumber"].ToString();
                                break;
                            }
                        }
                        if (intClientPOID <= 0)  // No Site Contract specific Client PO ID so try for Client Contract specific //
                        {
                            intRecordTypeID = 0;
                            cmd = null;
                            cmd = new SqlCommand("sp06328_OM_Default_Client_PO_Number_For_Record", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@RecordID", intClientContractID));
                            cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordTypeID));
                            sdaPO = new SqlDataAdapter(cmd);
                            dsPO = new DataSet("NewDataSet");
                            sdaPO.Fill(dsPO, "Table");
                            foreach (DataRow dr in dsPO.Tables[0].Rows)
                            {
                                intClientPOID = Convert.ToInt32(dr["ClientPurchaseOrderID"]);
                                strClientPONumber = dr["PONumber"].ToString();
                                break;
                            }
                        }
                    }

                    GridView viewMasterJobSubTypes = (GridView)fChildForm.gridControl2.MainView;
                    for (int i = 0; i < viewMasterJobSubTypes.DataRowCount; i++)
                    {
                        if (Convert.ToBoolean(viewMasterJobSubTypes.GetRowCellValue(i, "CheckMarkSelection")))
                        {
                            intJobSubTypeID = Convert.ToInt32(viewMasterJobSubTypes.GetRowCellValue(i, "JobSubTypeID"));
                            intJobTypeID = Convert.ToInt32(viewMasterJobSubTypes.GetRowCellValue(i, "JobTypeID"));
                            strJobSubTypeDescription = viewMasterJobSubTypes.GetRowCellValue(i, "JobSubTypeDescription").ToString();
                            strJobDescription = viewMasterJobSubTypes.GetRowCellValue(i, "JobTypeDescription").ToString();

                            try
                            {
                                DataRow drNewRow;
                                drNewRow = dataSet_OM_Job.sp06174_OM_Job_Edit.NewRow();
                                drNewRow["strMode"] = "add";
                                drNewRow["strRecordIDs"] = "";
                                drNewRow["VisitID"] = intVisitID;
                                drNewRow["VisitNumber"] = intVisitNumber;
                                drNewRow["JobSubTypeID"] = intJobSubTypeID;
                                drNewRow["JobTypeID"] = intJobTypeID;
                                drNewRow["CreatedByStaffID"] = GlobalSettings.UserID;
                                drNewRow["CreatedByStaffName"] = GlobalSettings.UserSurname + ": " + GlobalSettings.UserForename;
                                drNewRow["Reactive"] = 0;
                                drNewRow["JobNoLongerRequired"] = 0;
                                drNewRow["RequiresAccessPermit"] = 0;
                                drNewRow["AccessPermitID"] = 0;
                                drNewRow["FinanceSystemPONumber"] = "";
                                drNewRow["ExpectedStartDate"] = dtStartDate;
                                drNewRow["ExpectedEndDate"] = dtEndDate;
                                drNewRow["ExpectedDurationUnits"] = 1.00;
                                drNewRow["ExpectedDurationUnitsDescriptorID"] = 1;  // Hours //
                                drNewRow["ActualDurationUnits"] = 0.00;
                                drNewRow["ActualDurationUnits"] = 0.00;
                                drNewRow["ActualDurationUnitsDescriptionID"] = 0;
                                drNewRow["MinimumDaysFromLastVisit"] = 0;
                                drNewRow["MaximumDaysFromLastVisit"] = 0;
                                drNewRow["DaysLeeway"] = 0;
                                drNewRow["SiteContractID"] = intSiteContractID;
                                drNewRow["ClientContractID"] = intClientContractID;
                                drNewRow["SiteID"] = intSiteID;
                                drNewRow["ClientID"] = intClientID;
                                drNewRow["ContractDescription"] = strContractDescription;
                                drNewRow["ClientNameContractDescription"] = strClientName + ", Contract: " + strContractDescription;
                                drNewRow["ClientName"] = strClientName;
                                drNewRow["SiteName"] = strSiteName;
                                drNewRow["JobSubTypeDescription"] = strJobSubTypeDescription;
                                drNewRow["JobTypeDescription"] = strJobDescription;
                                drNewRow["JobTypeJobSubTypeDescription"] = strJobDescription + " - " + strJobSubTypeDescription;
                                drNewRow["LabourCount"] = 0;
                                drNewRow["EquipmentCount"] = 0;
                                drNewRow["MaterialCount"] = 0;
                                drNewRow["LocationX"] = dblSiteLocationX;
                                drNewRow["LocationY"] = dblSiteLocationY;
                                drNewRow["SitePostcode"] = strSitePostcode;
                                drNewRow["JobStatusID"] = 10;  // Job Creation Stated //

                                drNewRow["ClientPOID"] = intClientPOID;
                                drNewRow["ClientPONumber"] = strClientPONumber;

                                dataSet_OM_Job.sp06174_OM_Job_Edit.Rows.Add(drNewRow);
                            }
                            catch (Exception) { }
                        }
                    }
                }
                view.EndUpdate();  // UnLock view //
                view.ExpandAllGroups();
            }
        }

        private void Block_Edit_Jobs()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl6.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Jobs to block edit then try again.", "Block Edit Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Job_Wizard_Block_Edit_Job_Info();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                view.BeginUpdate();
                view.BeginSort();
                try
                {
                    foreach (int intRowHandle in intRowHandles)
                    {
                        if (fChildForm.intJobSubTypeID != null) view.SetRowCellValue(intRowHandle, "JobSubTypeID", fChildForm.intJobSubTypeID);
                        if (fChildForm.intJobTypeID != null) view.SetRowCellValue(intRowHandle, "JobTypeID", fChildForm.intJobTypeID);
                        if (fChildForm.strJobTypeJobSubTypeDescription != null) view.SetRowCellValue(intRowHandle, "JobTypeJobSubTypeDescription", fChildForm.strJobTypeJobSubTypeDescription);
                        if (fChildForm.strJobSubTypeDescription != null) view.SetRowCellValue(intRowHandle, "JobSubTypeDescription", fChildForm.strJobSubTypeDescription);
                        if (fChildForm.strJobTypeDescription != null) view.SetRowCellValue(intRowHandle, "JobTypeDescription", fChildForm.strJobTypeDescription);
                        if (fChildForm.dtExpectedStartDate != null) view.SetRowCellValue(intRowHandle, "ExpectedStartDate", fChildForm.dtExpectedStartDate);
                        if (fChildForm.dtExpectedEndDate != null) view.SetRowCellValue(intRowHandle, "ExpectedEndDate", fChildForm.dtExpectedEndDate);
                        if (fChildForm.decExpectedDurationUnits != null) view.SetRowCellValue(intRowHandle, "ExpectedDurationUnits", fChildForm.decExpectedDurationUnits);
                        if (fChildForm.intExpectedDurationUnitsDescriptorID != null) view.SetRowCellValue(intRowHandle, "ExpectedDurationUnitsDescriptorID", fChildForm.intExpectedDurationUnitsDescriptorID);
                        if (fChildForm.intMinimumDaysFromLastVisit != null) view.SetRowCellValue(intRowHandle, "MinimumDaysFromLastVisit", fChildForm.intMinimumDaysFromLastVisit);
                        if (fChildForm.intMaximumDaysFromLastVisit != null) view.SetRowCellValue(intRowHandle, "MaximumDaysFromLastVisit", fChildForm.intMaximumDaysFromLastVisit);
                        if (fChildForm.intDaysLeeway != null) view.SetRowCellValue(intRowHandle, "DaysLeeway", fChildForm.intDaysLeeway);
                        if (fChildForm.intReactive != null) view.SetRowCellValue(intRowHandle, "Reactive", fChildForm.intReactive);
                        if (fChildForm.intRequiresAccessPermit != null) view.SetRowCellValue(intRowHandle, "RequiresAccessPermit", fChildForm.intRequiresAccessPermit);
                        if (fChildForm.strRemarks != null) view.SetRowCellValue(intRowHandle, "Remarks", fChildForm.strRemarks);
                        if (fChildForm.intClientPOID != null) view.SetRowCellValue(intRowHandle, "ClientPOID", fChildForm.intClientPOID);
                        if (fChildForm.strClientPONumber != null) view.SetRowCellValue(intRowHandle, "ClientPONumber", fChildForm.strClientPONumber);
                    }
                }
                catch (Exception) { }
                view.EndSort();
                view.EndUpdate();
            }
        }


        #endregion


        #region Step 6 Page - Labour

        private void btnStep6Previous_Click(object sender, EventArgs e)
        {
            Filter_Jobs_On_Visit();
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep5;
        }
        private void btnStep6Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl8.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            Clear_Filter_Labour_On_Jobs();
            if (checkEditSkipEquipmentAndMaterials.Checked)
            {
                xtraTabControl1.SelectedTabPage = this.xtraTabPageFinish;
            }
            else
            {
                xtraTabControl1.SelectedTabPage = this.xtraTabPageStep7;
                view = (GridView)gridControl9.MainView;
                view.ExpandAllGroups();

                Filter_Equipment_On_Jobs();
            }
        }


        #region GridView7

        private void gridView7_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "LabourCount")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LabourCount")) <= 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridView7_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.NONE;
            SetMenuStatus();
        }

        private void gridView7_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.NONE;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView8

        private void gridControl8_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Labour(0);
                    }
                    else if ("block add".Equals(e.Button.Tag))
                    {
                        Block_Add_Labour();
                    }
                    else if ("block edit".Equals(e.Button.Tag))
                    {
                        Block_Edit_Labour();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView8_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Labour;
            SetMenuStatus();
        }

        private void gridView8_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Labour;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemButtonEditLinkedToPersonType_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06182OMJobLabourEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06182_OM_Job_Labour_EditRow)currentRowView.Row;
                var fChildForm = new frm_Core_Select_Person_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (currentRow == null) return;
                int intOriginalPersonTypeID = 0;
                try { intOriginalPersonTypeID = (string.IsNullOrEmpty(currentRow.LinkedToPersonTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.LinkedToPersonTypeID)); }
                catch (Exception) { }

                fChildForm.intOriginalPersonTypeID = intOriginalPersonTypeID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.LinkedToPersonTypeID = fChildForm.intSelectedPersonTypeID;
                    currentRow.LinkedToPersonType = fChildForm.strSelectedPersonType;

                    if (intOriginalPersonTypeID != fChildForm.intSelectedPersonTypeID)  // Clear selected person since the person type has changed //
                    {
                        currentRow.ContractorID = 0;
                        currentRow.ContractorName = "";
                        currentRow.LabourType = "";
                        currentRow.PostcodeSiteDistance = (double)0.00;
                        currentRow.LatLongSiteDistance = (double)0.00;
                        //dxErrorProvider1.SetError(LinkedToContractorName, "Select a value.");
                    }

                    sp06182OMJobLabourEditBindingSource.EndEdit();
                    GridView view = (GridView)gridControl8.MainView;
                    view.FocusedColumn = colContractorName;  // Move focus to next column so new value is shown in original column //
                }
            }
        }

        private void repositoryItemButtonEditContractorName_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06182OMJobLabourEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06182_OM_Job_Labour_EditRow)currentRowView.Row;
                if (currentRow == null) return;

                int intPersonTypeID = -1;
                try { intPersonTypeID = (string.IsNullOrEmpty(currentRow.LinkedToPersonTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.LinkedToPersonTypeID)); }
                catch (Exception) { }

                if (intPersonTypeID <= -1)
                {
                    XtraMessageBox.Show("Select the Person Type by clicking the Choose button on the Labour Type field before proceeding.", "Select Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                int intOriginalContractorID = (string.IsNullOrEmpty(currentRow.ContractorID.ToString()) ? 0 : Convert.ToInt32(currentRow.ContractorID));
                string strSelectedJobSubTypesIDs = (string.IsNullOrEmpty(currentRow.JobSubTypeID.ToString()) ? "" : currentRow.JobSubTypeID.ToString() + ",");

                if (intPersonTypeID == 1)  // Contractor //
                {
                    var fChildForm = new frm_OM_Select_Contractor();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.intOriginalContractorID = intOriginalContractorID;
                    fChildForm._PassedInLatitude = currentRow.SiteLocationX;
                    fChildForm._PassedInLongitude = currentRow.SiteLocationY;
                    fChildForm._PassedInPostcode = currentRow.SitePostcode;
                    fChildForm._PassedInJobSubTypes = strSelectedJobSubTypesIDs;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        if (fChildForm.intSelectedContractorID == intOriginalContractorID) return;
                        
                        currentRow.ContractorID = fChildForm.intSelectedContractorID;
                        currentRow.ContractorName = fChildForm.strSelectedContractorName;
                        currentRow.LabourType = fChildForm.strSelectedLabourType;

                        // Perform Distance Calculation //
                        double dbLatitude = fChildForm.dbSelectedLatitude;
                        double dbLongitude = fChildForm.dbSelectedLongitude;
                        string strPostcode = fChildForm.strSelectedPostcode;

                        double dbPostcodeDistance = (double)0.00;
                        double dbLatLongDistance = (double)0.00;
                        if (!string.IsNullOrWhiteSpace(strPostcode) && !string.IsNullOrWhiteSpace(currentRow.SitePostcode))
                        {
                            using (var CalcPostcodeDistance = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                CalcPostcodeDistance.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    dbPostcodeDistance = Convert.ToDouble(CalcPostcodeDistance.sp06102_OM_Calculate_Distance_Between_Postcodes(strPostcode, currentRow.SitePostcode));
                                }
                                catch (Exception) { }
                            }
                        }
                        try
                        {
                            dbLatLongDistance = GeoCodeCalc.CalcDistance(dbLatitude, dbLongitude, currentRow.SiteLocationX, currentRow.SiteLocationY);  // Calculate Distance in Miles //
                        }
                        catch (Exception) { }

                        currentRow.PostcodeSiteDistance = (dbPostcodeDistance > (double)2500 ? (double)0.00 : dbPostcodeDistance);
                        currentRow.LatLongSiteDistance = (dbLatLongDistance > (double)2500 ? (double)0.00 : dbLatLongDistance);

                        sp06182OMJobLabourEditBindingSource.EndEdit();
                        GridView view = (GridView)gridControl8.MainView;
                        view.FocusedColumn = colLabourType;  // Move focus to next column so new value is shown in original column //
                    }
                }
                else  // Staff //
                {
                    var fChildForm = new frm_HR_Select_Staff();
                    fChildForm.GlobalSettings = this.GlobalSettings;

                    fChildForm.intOriginalStaffID = intOriginalContractorID;

                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        // Check the CostCalculationLevel - if 1 (Generic Visit Rate) need to set all Labour values for all jobs on visit to the same value //
                        int intVisitID = Convert.ToInt32(currentRow["VisitID"]);
                        GridView VisitView = (GridView)gridControl7.MainView;
                        int intFoundRow = VisitView.LocateByValue(0, VisitView.Columns["VisitID"], intVisitID);
                        if (intFoundRow == GridControl.InvalidRowHandle) return;  // No CostCalculationLevel so abort //
                        int intVisitCostCalculationLevelID = Convert.ToInt32(VisitView.GetRowCellValue(intFoundRow, "VisitCostCalculationLevelID"));

                        currentRow.ContractorID = fChildForm.intSelectedStaffID;
                        currentRow.ContractorName = fChildForm.strSelectedStaffName;
                        currentRow.LabourType = "N\\A";
                        currentRow.PostcodeSiteDistance = (double)0.00;
                        currentRow.LatLongSiteDistance = (double)0.00;

                        GridView view = (GridView)gridControl8.MainView;
                        if (intVisitCostCalculationLevelID == 1)  // Set all Labour Records to the Same Value //
                        {
                            foreach (DataRow dr in dataSet_OM_Job.sp06182_OM_Job_Labour_Edit.Rows)
                            {
                                if (Convert.ToInt32(dr["VisitID"]) == intVisitID)
                                {
                                    dr["ContractorID"] = fChildForm.intSelectedStaffID;
                                    dr["ContractorName"] = fChildForm.strSelectedStaffName;
                                    dr["LabourType"] = "N\\A";
                                    dr["PostcodeSiteDistance"] = (double)0.00;
                                    dr["LatLongSiteDistance"] = (double)0.00;
                                }
                            }
                        }

                        sp06182OMJobLabourEditBindingSource.EndEdit();
                        view.FocusedColumn = colLabourType;  // Move focus to next column so new value is shown in original column //
                    }
                }
            }
        }

        private void repositoryItemSpinEdit2DP8_EditValueChanged(object sender, EventArgs e)
        {
            gridView8.PostEditor();
            GridView view = (GridView)gridControl8.MainView;
            if (view.FocusedColumn.FieldName == "CostUnitsUsed")
            {
                SpinEdit se = (SpinEdit)sender;
                decimal decValue = Convert.ToDecimal(se.Value);
                Calculate_Cost(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            }
            else if (view.FocusedColumn.FieldName == "SellUnitsUsed")
            {
                SpinEdit se = (SpinEdit)sender;
                decimal decValue = Convert.ToDecimal(se.Value);
                Calculate_Sell(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            }
        }
        private void repositoryItemSpinEditCurrency8_EditValueChanged(object sender, EventArgs e)
        {
            gridView8.PostEditor();
            GridView view = (GridView)gridControl8.MainView;
            if (view.FocusedColumn.FieldName == "CostPerUnitExVat")
            {
                SpinEdit se = (SpinEdit)sender;
                decimal decValue = Convert.ToDecimal(se.Value);
                Calculate_Cost(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            }
            else if (view.FocusedColumn.FieldName == "SellPerUnitExVat")
            {
                SpinEdit se = (SpinEdit)sender;
                decimal decValue = Convert.ToDecimal(se.Value);
                Calculate_Sell(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            }
        }

        private void repositoryItemSpinEditPercentage8_EditValueChanged(object sender, EventArgs e)
        {
            gridView8.PostEditor();
            GridView view = (GridView)gridControl8.MainView;
            if (view.FocusedColumn.FieldName == "CostPerUnitVatRate")
            {
                SpinEdit se = (SpinEdit)sender;
                decimal decValue = Convert.ToDecimal(se.Value);
                Calculate_Cost(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            }
            else if (view.FocusedColumn.FieldName == "SellPerUnitVatRate")
            {
                SpinEdit se = (SpinEdit)sender;
                decimal decValue = Convert.ToDecimal(se.Value);
                Calculate_Sell(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            }
        }

        private void Calculate_Cost(GridView view, int intRow, string strCurrentColumn, decimal decValue)
        {
            decimal decTotalValueExVat = (decimal)0.00;
            decimal decTotalValueVat = (decimal)0.00;
            decimal decTotalValue = (decimal)0.00;
            decimal decCostUnitsUsed = (strCurrentColumn == "CostUnitsUsed" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "CostUnitsUsed")));
            decimal decCostPerUnitExVat = (strCurrentColumn == "CostPerUnitExVat" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "CostPerUnitExVat")));
            decimal decCostPerUnitVatRate = (strCurrentColumn == "CostPerUnitVatRate" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "CostPerUnitVatRate")));

            decTotalValueExVat = decCostUnitsUsed * decCostPerUnitExVat;
            decTotalValueVat = (decTotalValueExVat * decCostPerUnitVatRate) / 100;
            decTotalValue = decTotalValueExVat + decTotalValueVat;
            view.SetRowCellValue(intRow, "CostValueExVat", decTotalValueExVat);
            view.SetRowCellValue(intRow, "CostValueVat", decTotalValueVat);
            view.SetRowCellValue(intRow, "CostValueTotal", decTotalValue);
            this.sp06182OMJobLabourEditBindingSource.EndEdit();
        }

        private void Calculate_Sell(GridView view, int intRow, string strCurrentColumn, decimal decValue)
        {
            decimal decTotalValueExVat = (decimal)0.00;
            decimal decTotalValueVat = (decimal)0.00;
            decimal decTotalValue = (decimal)0.00;
            decimal decSellUnitsUsed = (strCurrentColumn == "SellUnitsUsed" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SellUnitsUsed")));
            decimal decSellPerUnitExVat = (strCurrentColumn == "SellPerUnitExVat" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SellPerUnitExVat")));
            decimal decSellPerUnitVatRate = (strCurrentColumn == "SellPerUnitVatRate" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SellPerUnitVatRate")));

            decTotalValueExVat = decSellUnitsUsed * decSellPerUnitExVat;
            decTotalValueVat = (decTotalValueExVat * decSellPerUnitVatRate) / 100;
            decTotalValue = decTotalValueExVat + decTotalValueVat;
            view.SetRowCellValue(intRow, "SellValueExVat", decTotalValueExVat);
            view.SetRowCellValue(intRow, "SellValueVat", decTotalValueVat);
            view.SetRowCellValue(intRow, "SellValueTotal", decTotalValue);
            this.sp06182OMJobLabourEditBindingSource.EndEdit();
        }

        #endregion


        private void Filter_Labour_On_Jobs()
        {
            GridView view = (GridView)gridControl8.MainView;
            view.PostEditor();

            string strFilterCondition = "";
            GridView parentView = (GridView)gridControl7.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                strFilterCondition = "[JobID] = 9999999999";  // Don't show anything //
            }
            else
            {
                foreach (int i in intRowHandles)
                {
                    strFilterCondition += (string.IsNullOrWhiteSpace(strFilterCondition) ? "" : " or ");
                    strFilterCondition += "[JobID] = " + parentView.GetRowCellValue(i, "JobID").ToString();
                }
            }
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = strFilterCondition;
            view.MakeRowVisible(-1, true);
            view.ExpandAllGroups();
            view.EndUpdate();
            SetMenuStatus();
        }

        private void Clear_Filter_Labour_On_Jobs()
        {
            GridView view = (GridView)gridControl8.MainView;
            view.PostEditor();

            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
            SetMenuStatus();
        }

        private void Add_Labour(int TenderLabourID)
        {
            GridView viewJobs = (GridView)gridControl7.MainView;
            int[] intRowHandles = viewJobs.GetSelectedRows();
            if (intRowHandles.Length <= 0 && TenderLabourID <= 0)
            {
                XtraMessageBox.Show("Select one or more Jobs to add the Labour to before proceeding.", "Add Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intRowHandles.Length > 1 && TenderLabourID <= 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + " Job(s) selected. If you proceed a new Labour record will be created for each of these jobs.\n\nProceed?", "Add Labour", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }

            int intPersonType = -1;
            if (TenderLabourID <=0)
            {
                var fChildForm2 = new frm_Core_Select_Person_Type();
                fChildForm2.GlobalSettings = this.GlobalSettings;
                fChildForm2.intOriginalPersonTypeID = 1;  // Contractor //;
                if (fChildForm2.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    intPersonType = fChildForm2.intSelectedPersonTypeID;
                }
                else { return; }
            }
            else  // Initially called by a Tender on first load //
            {
                intPersonType = i_intPassedInLabourTypeID;
            }

            GridView view = (GridView)gridControl8.MainView;
            view.PostEditor();
            int intContractorID = 0;
            string strContractorName = "";
            string strLabourType = "";
            int intLinkedToPersonTypeID = 0;
            string strLinkedToPersonType = "";
            double dbPostcodeDistance = (double)0.00;
            double dbLatLongDistance = (double)0.00;

            int intJobID = 0;
            int intClientID = 0;
            int intSiteID = 0;
            int intVisitID = 0;
            DateTime dtJobExpectedStartDate = DateTime.MinValue;
            string strClientName = "";
            string strSiteName = "";
            int intVisitNumber = 0;
            string strJobTypeDescription = "";
            string strJobSubTypeDescription = "";
            string strContractDescription = "";
            double dblSiteLocationX = (double)0.00;
            double dblSiteLocationY = (double)0.00;
            string strSitePostcode = "";
            int intJobSubTypeID = 0;
            int intJobTypeID = 0;
            string strNewIDs = "";
            double dbLatitude = (double)0.00;
            double dbLongitude = (double)0.00;
            string strPostcode = "";

            if (intPersonType == 1)
            {
                if (TenderLabourID <= 0)
                {
                    // Check if we have just one Job Sub-Type selected as we can pass this through to the select Contractor screen for use with Job Competencies //
                    string strSelectedJobSubTypesIDs = "";
                    string strTempValue = "";
                    bool boolOneJobSubTypeSelected = true;
                    foreach (int intRowHandle in intRowHandles)
                    {
                        strTempValue = viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeID").ToString();
                        if (string.IsNullOrWhiteSpace(strSelectedJobSubTypesIDs))
                        {
                            strSelectedJobSubTypesIDs = strTempValue;
                        }
                        else if (strSelectedJobSubTypesIDs != strTempValue)
                        {
                            boolOneJobSubTypeSelected = false;
                            break;
                        }
                    }
                    if (!boolOneJobSubTypeSelected)
                    {
                        strSelectedJobSubTypesIDs = "";
                    }
                    else
                    {
                        strSelectedJobSubTypesIDs += ",";
                    }

                    var fChildForm = new frm_OM_Select_Contractor();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm._Mode = "single";
                    fChildForm._PassedInLatitude = (string.IsNullOrEmpty(viewJobs.GetRowCellValue(intRowHandles[0], "LocationX").ToString()) ? (double)0.00 : Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandles[0], "LocationX")));
                    fChildForm._PassedInLongitude = (string.IsNullOrEmpty(viewJobs.GetRowCellValue(intRowHandles[0], "LocationY").ToString()) ? (double)0.00 : Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandles[0], "LocationY")));
                    fChildForm._PassedInPostcode = (string.IsNullOrEmpty(viewJobs.GetRowCellValue(intRowHandles[0], "SitePostcode").ToString()) ? "" : viewJobs.GetRowCellValue(intRowHandles[0], "SitePostcode").ToString());
                    fChildForm._PassedInJobSubTypes = strSelectedJobSubTypesIDs;
                    if (fChildForm.ShowDialog() != DialogResult.OK) return;
                    intContractorID = fChildForm.intSelectedContractorID;
                    strContractorName = fChildForm.strSelectedContractorName;
                    strLabourType = fChildForm.strSelectedLabourType;
                    dbLatitude = fChildForm.dbSelectedLatitude;
                    dbLongitude = fChildForm.dbSelectedLongitude;
                    strPostcode = fChildForm.strSelectedPostcode;
                }
                else  // Initially called by a Tender on first load //
                {
                    intContractorID = TenderLabourID;
                    strContractorName = i_strPassedInLabourName;
                    strLabourType = "Contractor";
                    dbLatitude = i_dbPassedInLabourLatitude;
                    dbLongitude = i_dbPassedInLabourLongitude;
                    strPostcode = i_strPassedInLabourPostcode;
                }
                viewJobs.BeginUpdate();
                view.BeginUpdate();  // Lock view //
                foreach (int intRowHandle in intRowHandles)
                {
                    intJobID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobID"));
                    intClientID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "ClientID"));
                    intSiteID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "SiteID"));
                    intVisitID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitID"));
                    dtJobExpectedStartDate = Convert.ToDateTime(viewJobs.GetRowCellValue(intRowHandle, "ExpectedStartDate"));
                    strClientName = viewJobs.GetRowCellValue(intRowHandle, "ClientName").ToString();
                    strSiteName = viewJobs.GetRowCellValue(intRowHandle, "SiteName").ToString();
                    intVisitNumber = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitNumber"));
                    strJobTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobTypeDescription").ToString();
                    strJobSubTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeDescription").ToString();
                    strContractDescription = viewJobs.GetRowCellValue(intRowHandle, "ContractDescription").ToString();
                    dblSiteLocationX = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationX"));
                    dblSiteLocationY = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationY"));
                    strSitePostcode = viewJobs.GetRowCellValue(intRowHandle, "SitePostcode").ToString();
                    intJobSubTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                    intJobTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobTypeID"));
                    intLinkedToPersonTypeID = 1;
                    strLinkedToPersonType = "Contractor";

                    // Perform Distance Calculation //
                    if (!string.IsNullOrWhiteSpace(strPostcode) && !string.IsNullOrWhiteSpace(strSitePostcode))
                    {
                        using (var CalcPostcodeDistance = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                        {
                            CalcPostcodeDistance.ChangeConnectionString(strConnectionString);
                            try
                            {
                                dbPostcodeDistance = Convert.ToDouble(CalcPostcodeDistance.sp06102_OM_Calculate_Distance_Between_Postcodes(strPostcode, strSitePostcode));
                            }
                            catch (Exception) { }
                        }
                    }
                    try
                    {
                        dbLatLongDistance = GeoCodeCalc.CalcDistance(dbLatitude, dbLongitude, dblSiteLocationX, dblSiteLocationY);  // Calculate Distance in Miles //
                    }
                    catch (Exception) { }
                    dbPostcodeDistance = (dbPostcodeDistance > (double)2500 ? (double)0.00 : dbPostcodeDistance);
                    dbLatLongDistance = (dbLatLongDistance > (double)2500 ? (double)0.00 : dbLatLongDistance);

                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_OM_Job.sp06182_OM_Job_Labour_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = "";
                        drNewRow["ContractorID"] = intContractorID;
                        drNewRow["JobID"] = intJobID;
                        drNewRow["CostUnitsUsed"] = (decimal)0.00;
                        drNewRow["CostUnitDescriptorID"] = 1;  // Hours //
                        drNewRow["SellUnitsUsed"] = (decimal)0.00;
                        drNewRow["SellUnitDescriptorID"] = 1;  // Hours //
                        drNewRow["CostPerUnitExVat"] = (decimal)0.00;
                        drNewRow["CostPerUnitVatRate"] = _decDefaultVatRate;
                        drNewRow["SellPerUnitExVat"] = (decimal)0.00;
                        drNewRow["SellPerUnitVatRate"] = _decDefaultVatRate;
                        drNewRow["CostValueExVat"] = (decimal)0.00;
                        drNewRow["CostValueVat"] = (decimal)0.00;
                        drNewRow["CostValueTotal"] = (decimal)0.00;
                        drNewRow["SellValueExVat"] = (decimal)0.00;
                        drNewRow["SellValueVat"] = (decimal)0.00;
                        drNewRow["SellValueTotal"] = (decimal)0.00;
                        drNewRow["ClientID"] = intClientID;
                        drNewRow["SiteID"] = intSiteID;
                        drNewRow["VisitID"] = intVisitID;
                        drNewRow["JobExpectedStartDate"] = dtJobExpectedStartDate;
                        drNewRow["ClientName"] = strClientName;
                        drNewRow["SiteName"] = strSiteName;
                        drNewRow["VisitNumber"] = intVisitNumber;
                        drNewRow["JobTypeDescription"] = strJobTypeDescription;
                        drNewRow["JobSubTypeDescription"] = strJobSubTypeDescription;
                        drNewRow["ContractorName"] = strContractorName;
                        drNewRow["CostUnitDescriptor"] = "Hours";
                        drNewRow["SellUnitDescriptor"] = "Hours";
                        drNewRow["LabourType"] = strLabourType;
                        drNewRow["CISPercentage"] = (decimal)0.00;
                        drNewRow["CISValue"] = (decimal)0.00;
                        drNewRow["PurchaseOrderID"] = 0;
                        drNewRow["ContractDescription"] = strContractDescription;
                        drNewRow["ClientNameContractDescription"] = strClientName + ", Contract: " + strContractDescription;
                        drNewRow["JobTypeJobSubTypeDescription"] = strJobTypeDescription + " - " + strJobSubTypeDescription;
                        drNewRow["PostcodeSiteDistance"] = dbPostcodeDistance;
                        drNewRow["LatLongSiteDistance"] = dbLatLongDistance;
                        drNewRow["SitePostcode"] = strSitePostcode;
                        drNewRow["SiteLocationX"] = dblSiteLocationX;
                        drNewRow["SiteLocationY"] = dblSiteLocationY;
                        drNewRow["JobSubTypeID"] = intJobSubTypeID;
                        drNewRow["JobTypeID"] = intJobTypeID;
                        drNewRow["LinkedToPersonTypeID"] = intLinkedToPersonTypeID;
                        drNewRow["LinkedToPersonType"] = strLinkedToPersonType;
                        dataSet_OM_Job.sp06182_OM_Job_Labour_Edit.Rows.Add(drNewRow);
                        strNewIDs += drNewRow["LabourUsedID"].ToString() + ",";
                    }
                    catch (Exception) { }

                    // Update Labour Count for each job //
                    int intFoundRow = viewJobs.LocateByValue(0, view.Columns["JobID"], intJobID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        viewJobs.SetRowCellValue(intFoundRow, "LabourCount", Convert.ToInt32(viewJobs.GetRowCellValue(intFoundRow, "LabourCount")) + 1);
                    }
                }
                view.EndUpdate();  // UnLock view //
                view.ExpandAllGroups();
                viewJobs.EndUpdate();
            }
            else  // Staff //
            {
                if (TenderLabourID <= 0)
                {
                    var fChildForm = new frm_HR_Select_Staff();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.intOriginalStaffID = 0;
                    fChildForm._Mode = "single";
                    if (fChildForm.ShowDialog() != DialogResult.OK) return;
                    intContractorID = fChildForm.intSelectedStaffID;
                    strContractorName = fChildForm.strSelectedStaffName;
                }
                else  // Initially called by a Tender on first load //
                {
                    intContractorID = TenderLabourID;
                    strContractorName = i_strPassedInLabourName;
                }
                strLabourType = "N\\A";

                viewJobs.BeginUpdate();
                view.BeginUpdate();  // Lock view //
                foreach (int intRowHandle in intRowHandles)
                {
                    intJobID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobID"));
                    intClientID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "ClientID"));
                    intSiteID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "SiteID"));
                    intVisitID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitID"));
                    dtJobExpectedStartDate = Convert.ToDateTime(viewJobs.GetRowCellValue(intRowHandle, "ExpectedStartDate"));
                    strClientName = viewJobs.GetRowCellValue(intRowHandle, "ClientName").ToString();
                    strSiteName = viewJobs.GetRowCellValue(intRowHandle, "SiteName").ToString();
                    intVisitNumber = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitNumber"));
                    strJobTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobTypeDescription").ToString();
                    strJobSubTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeDescription").ToString();
                    strContractDescription = viewJobs.GetRowCellValue(intRowHandle, "ContractDescription").ToString();
                    dblSiteLocationX = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationX"));
                    dblSiteLocationY = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationY"));
                    strSitePostcode = viewJobs.GetRowCellValue(intRowHandle, "SitePostcode").ToString();
                    intJobSubTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                    intJobTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobTypeID"));
                    intLinkedToPersonTypeID = 0;
                    strLinkedToPersonType = "Staff";

                    dbPostcodeDistance = (double)0.00;
                    dbLatLongDistance = (double)0.00;

                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_OM_Job.sp06182_OM_Job_Labour_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = "";
                        drNewRow["ContractorID"] = intContractorID;
                        drNewRow["JobID"] = intJobID;
                        drNewRow["CostUnitsUsed"] = (decimal)0.00;
                        drNewRow["CostUnitDescriptorID"] = 1;  // Hours //
                        drNewRow["SellUnitsUsed"] = (decimal)0.00;
                        drNewRow["SellUnitDescriptorID"] = 1;  // Hours //
                        drNewRow["CostPerUnitExVat"] = (decimal)0.00;
                        drNewRow["CostPerUnitVatRate"] = _decDefaultVatRate;
                        drNewRow["SellPerUnitExVat"] = (decimal)0.00;
                        drNewRow["SellPerUnitVatRate"] = _decDefaultVatRate;
                        drNewRow["CostValueExVat"] = (decimal)0.00;
                        drNewRow["CostValueVat"] = (decimal)0.00;
                        drNewRow["CostValueTotal"] = (decimal)0.00;
                        drNewRow["SellValueExVat"] = (decimal)0.00;
                        drNewRow["SellValueVat"] = (decimal)0.00;
                        drNewRow["SellValueTotal"] = (decimal)0.00;
                        drNewRow["ClientID"] = intClientID;
                        drNewRow["SiteID"] = intSiteID;
                        drNewRow["VisitID"] = intVisitID;
                        drNewRow["JobExpectedStartDate"] = dtJobExpectedStartDate;
                        drNewRow["ClientName"] = strClientName;
                        drNewRow["SiteName"] = strSiteName;
                        drNewRow["VisitNumber"] = intVisitNumber;
                        drNewRow["JobTypeDescription"] = strJobTypeDescription;
                        drNewRow["JobSubTypeDescription"] = strJobSubTypeDescription;
                        drNewRow["ContractorName"] = strContractorName;
                        drNewRow["CostUnitDescriptor"] = "Hours";
                        drNewRow["SellUnitDescriptor"] = "Hours";
                        drNewRow["LabourType"] = strLabourType;
                        drNewRow["CISPercentage"] = (decimal)0.00;
                        drNewRow["CISValue"] = (decimal)0.00;
                        drNewRow["PurchaseOrderID"] = 0;
                        drNewRow["ContractDescription"] = strContractDescription;
                        drNewRow["ClientNameContractDescription"] = strClientName + ", Contract: " + strContractDescription;
                        drNewRow["JobTypeJobSubTypeDescription"] = strJobTypeDescription + " - " + strJobSubTypeDescription;
                        drNewRow["PostcodeSiteDistance"] = dbPostcodeDistance;
                        drNewRow["LatLongSiteDistance"] = dbLatLongDistance;
                        drNewRow["SitePostcode"] = strSitePostcode;
                        drNewRow["SiteLocationX"] = dblSiteLocationX;
                        drNewRow["SiteLocationY"] = dblSiteLocationY;
                        drNewRow["JobSubTypeID"] = intJobSubTypeID;
                        drNewRow["JobTypeID"] = intJobTypeID;
                        drNewRow["LinkedToPersonTypeID"] = intLinkedToPersonTypeID;
                        drNewRow["LinkedToPersonType"] = strLinkedToPersonType;
                        dataSet_OM_Job.sp06182_OM_Job_Labour_Edit.Rows.Add(drNewRow);
                        strNewIDs += drNewRow["LabourUsedID"].ToString() + ",";
                    }
                    catch (Exception) { }

                    // Update Labour Count for each job //
                    int intFoundRow = viewJobs.LocateByValue(0, view.Columns["JobID"], intJobID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        viewJobs.SetRowCellValue(intFoundRow, "LabourCount", Convert.ToInt32(viewJobs.GetRowCellValue(intFoundRow, "LabourCount")) + 1);
                    }
                }
                view.EndUpdate();  // UnLock view //
                view.ExpandAllGroups();
                viewJobs.EndUpdate();
            }

            // Highlight any recently added new rows //
            if (!string.IsNullOrWhiteSpace(strNewIDs))
            {
                char[] delimiters = new char[] { ',' };
                string[] strArray = strNewIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LabourUsedID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
            }
        }

        private void Block_Add_Labour()
        {
            GridView viewJobs = (GridView)gridControl7.MainView;
            int[] intRowHandles = viewJobs.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Jobs to block add the Labour to before proceeding.", "Block Add Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intRowHandles.Length > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + " Job(s) selected. If you proceed one or more new Labour records will be created for each of these jobs.\n\nProceed?", "Block Add Labour", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }

            int intPersonType = -1;
            var fChildForm2 = new frm_Core_Select_Person_Type();
            fChildForm2.GlobalSettings = this.GlobalSettings;
            fChildForm2.intOriginalPersonTypeID = 1;  // Contractor //;
            if (fChildForm2.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                intPersonType = fChildForm2.intSelectedPersonTypeID;
            }
            else { return; }

            GridView view = (GridView)gridControl8.MainView;
            view.PostEditor();
            int intContractorID = 0;
            string strContractorName = "";
            string strLabourType = "";
            int intLinkedToPersonTypeID = 0;
            string strLinkedToPersonType = "";
            double dbPostcodeDistance = (double)0.00;
            double dbLatLongDistance = (double)0.00;

            int intJobID = 0;
            int intClientID = 0;
            int intSiteID = 0;
            int intVisitID = 0;
            DateTime dtJobExpectedStartDate = DateTime.MinValue;
            string strClientName = "";
            string strSiteName = "";
            int intVisitNumber = 0;
            string strJobTypeDescription = "";
            string strJobSubTypeDescription = "";
            string strContractDescription = "";
            double dblSiteLocationX = (double)0.00;
            double dblSiteLocationY = (double)0.00;
            string strSitePostcode = "";
            int intJobSubTypeID = 0;
            int intJobTypeID = 0;

            double dbLatitude = (double)0.00;
            double dbLongitude = (double)0.00;
            string strPostcode = "";

            if (intPersonType == 1)
            {
                // Check if we have just one Job Sub-Type selected as we can pass this through to the select Contractor screen for use with Job Competencies //
                string strSelectedJobSubTypesIDs = "";
                string strTempValue = "";
                bool boolOneJobSubTypeSelected = true;
                foreach (int intRowHandle in intRowHandles)
                {
                    strTempValue = viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeID").ToString();
                    if (string.IsNullOrWhiteSpace(strSelectedJobSubTypesIDs))
                    {
                        strSelectedJobSubTypesIDs = strTempValue;
                    }
                    else if (strSelectedJobSubTypesIDs != strTempValue)
                    {
                        boolOneJobSubTypeSelected = false;
                        break;
                    }
                }
                if (!boolOneJobSubTypeSelected)
                {
                    strSelectedJobSubTypesIDs = "";
                }
                else
                {
                    strSelectedJobSubTypesIDs += ",";
                }

                var fChildForm = new frm_OM_Select_Contractor();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm._Mode = "multiple";
                fChildForm._PassedInLatitude = (string.IsNullOrEmpty(viewJobs.GetRowCellValue(intRowHandles[0], "LocationX").ToString()) ? (double)0.00 : Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandles[0], "LocationX")));
                fChildForm._PassedInLongitude = (string.IsNullOrEmpty(viewJobs.GetRowCellValue(intRowHandles[0], "LocationY").ToString()) ? (double)0.00 : Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandles[0], "LocationY")));
                fChildForm._PassedInPostcode = (string.IsNullOrEmpty(viewJobs.GetRowCellValue(intRowHandles[0], "SitePostcode").ToString()) ? "" : viewJobs.GetRowCellValue(intRowHandles[0], "SitePostcode").ToString());
                fChildForm._PassedInJobSubTypes = strSelectedJobSubTypesIDs;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    viewJobs.BeginUpdate();
                    view.BeginUpdate();  // Lock view so we don't see filter switching on and off //
                    string strNewIDs = "";
                    foreach (int intRowHandle in intRowHandles)
                    {
                        intJobID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobID"));
                        intClientID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "ClientID"));
                        intSiteID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "SiteID"));
                        intVisitID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitID"));
                        dtJobExpectedStartDate = Convert.ToDateTime(viewJobs.GetRowCellValue(intRowHandle, "ExpectedStartDate"));
                        strClientName = viewJobs.GetRowCellValue(intRowHandle, "ClientName").ToString();
                        strSiteName = viewJobs.GetRowCellValue(intRowHandle, "SiteName").ToString();
                        intVisitNumber = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitNumber"));
                        strJobTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobTypeDescription").ToString();
                        strJobSubTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeDescription").ToString();
                        strContractDescription = viewJobs.GetRowCellValue(intRowHandle, "ContractDescription").ToString();
                        dblSiteLocationX = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationX"));
                        dblSiteLocationY = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationY"));
                        strSitePostcode = viewJobs.GetRowCellValue(intRowHandle, "SitePostcode").ToString();
                        intJobSubTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                        intJobTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobTypeID"));
                        intLinkedToPersonTypeID = 1;
                        strLinkedToPersonType = "Contractor";

                        GridView viewContractors = (GridView)fChildForm.gridControl1.MainView;
                        for (int i = 0; i < viewContractors.DataRowCount; i++)
                        {
                            if (Convert.ToBoolean(viewContractors.GetRowCellValue(i, "CheckMarkSelection")))
                            {
                                intContractorID = Convert.ToInt32(viewContractors.GetRowCellValue(i, "ContractorID"));
                                strContractorName = viewContractors.GetRowCellValue(i, "ContractorName").ToString();
                                strLabourType = viewContractors.GetRowCellValue(i, "LabourType").ToString();

                                // Perform Distance Calculation //
                                dbLatitude = Convert.ToDouble(viewContractors.GetRowCellValue(i, "Latitude"));
                                dbLongitude = Convert.ToDouble(viewContractors.GetRowCellValue(i, "Longitude"));
                                strPostcode = viewContractors.GetRowCellValue(i, "Postcode").ToString();

                                if (!string.IsNullOrWhiteSpace(strPostcode) && !string.IsNullOrWhiteSpace(strSitePostcode))
                                {
                                    using (var CalcPostcodeDistance = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                                    {
                                        CalcPostcodeDistance.ChangeConnectionString(strConnectionString);
                                        try
                                        {
                                            dbPostcodeDistance = Convert.ToDouble(CalcPostcodeDistance.sp06102_OM_Calculate_Distance_Between_Postcodes(strPostcode, strSitePostcode));
                                        }
                                        catch (Exception) { }
                                    }
                                }
                                try
                                {
                                    dbLatLongDistance = GeoCodeCalc.CalcDistance(dbLatitude, dbLongitude, dblSiteLocationX, dblSiteLocationY);  // Calculate Distance in Miles //
                                }
                                catch (Exception) { }
                                dbPostcodeDistance = (dbPostcodeDistance > (double)2500 ? (double)0.00 : dbPostcodeDistance);
                                dbLatLongDistance = (dbLatLongDistance > (double)2500 ? (double)0.00 : dbLatLongDistance);

                                try
                                {
                                    DataRow drNewRow;
                                    drNewRow = dataSet_OM_Job.sp06182_OM_Job_Labour_Edit.NewRow();
                                    drNewRow["strMode"] = "add";
                                    drNewRow["strRecordIDs"] = "";
                                    drNewRow["ContractorID"] = intContractorID;
                                    drNewRow["JobID"] = intJobID;
                                    drNewRow["CostUnitsUsed"] = (decimal)0.00;
                                    drNewRow["CostUnitDescriptorID"] = 1;  // Hours //
                                    drNewRow["SellUnitsUsed"] = (decimal)0.00;
                                    drNewRow["SellUnitDescriptorID"] = 1;  // Hours //
                                    drNewRow["CostPerUnitExVat"] = (decimal)0.00;
                                    drNewRow["CostPerUnitVatRate"] = _decDefaultVatRate;
                                    drNewRow["SellPerUnitExVat"] = (decimal)0.00;
                                    drNewRow["SellPerUnitVatRate"] = _decDefaultVatRate;
                                    drNewRow["CostValueExVat"] = (decimal)0.00;
                                    drNewRow["CostValueVat"] = (decimal)0.00;
                                    drNewRow["CostValueTotal"] = (decimal)0.00;
                                    drNewRow["SellValueExVat"] = (decimal)0.00;
                                    drNewRow["SellValueVat"] = (decimal)0.00;
                                    drNewRow["SellValueTotal"] = (decimal)0.00;
                                    drNewRow["ClientID"] = intClientID;
                                    drNewRow["SiteID"] = intSiteID;
                                    drNewRow["VisitID"] = intVisitID;
                                    drNewRow["JobExpectedStartDate"] = dtJobExpectedStartDate;
                                    drNewRow["ClientName"] = strClientName;
                                    drNewRow["SiteName"] = strSiteName;
                                    drNewRow["VisitNumber"] = intVisitNumber;
                                    drNewRow["JobTypeDescription"] = strJobTypeDescription;
                                    drNewRow["JobSubTypeDescription"] = strJobSubTypeDescription;
                                    drNewRow["ContractorName"] = strContractorName;
                                    drNewRow["CostUnitDescriptor"] = "Hours";
                                    drNewRow["SellUnitDescriptor"] = "Hours";
                                    drNewRow["LabourType"] = strLabourType;
                                    drNewRow["CISPercentage"] = (decimal)0.00;
                                    drNewRow["CISValue"] = (decimal)0.00;
                                    drNewRow["PurchaseOrderID"] = 0;
                                    drNewRow["ContractDescription"] = strContractDescription;
                                    drNewRow["ClientNameContractDescription"] = strClientName + ", Contract: " + strContractDescription;
                                    drNewRow["JobTypeJobSubTypeDescription"] = strJobTypeDescription + " - " + strJobSubTypeDescription;
                                    drNewRow["PostcodeSiteDistance"] = dbPostcodeDistance;
                                    drNewRow["LatLongSiteDistance"] = dbLatLongDistance;
                                    drNewRow["SitePostcode"] = strSitePostcode;
                                    drNewRow["SiteLocationX"] = dblSiteLocationX;
                                    drNewRow["SiteLocationY"] = dblSiteLocationY;
                                    drNewRow["JobSubTypeID"] = intJobSubTypeID;
                                    drNewRow["JobTypeID"] = intJobTypeID;
                                    drNewRow["LinkedToPersonTypeID"] = intLinkedToPersonTypeID;
                                    drNewRow["LinkedToPersonType"] = strLinkedToPersonType;
                                    dataSet_OM_Job.sp06182_OM_Job_Labour_Edit.Rows.Add(drNewRow);
                                    strNewIDs += drNewRow["LabourUsedID"].ToString() + ",";

                                    // Update Labour Count for each job //
                                    int intFoundRow = viewJobs.LocateByValue(0, view.Columns["JobID"], intJobID);
                                    if (intFoundRow != GridControl.InvalidRowHandle)
                                    {
                                        viewJobs.SetRowCellValue(intFoundRow, "LabourCount", Convert.ToInt32(viewJobs.GetRowCellValue(intFoundRow, "LabourCount")) + 1);
                                    }
                                }
                                catch (Exception) { }
                            }
                        }
                    }
                    view.EndUpdate();  // UnLock view //
                    view.ExpandAllGroups();
                    viewJobs.EndUpdate();

                    // Highlight any recently added new rows //
                    if (!string.IsNullOrWhiteSpace(strNewIDs))
                    {
                        char[] delimiters = new char[] { ',' };
                        string[] strArray = strNewIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        int intID = 0;
                        int intRowHandle = 0;
                        view.ClearSelection(); // Clear any current selection so just the new record is selected //
                        foreach (string strElement in strArray)
                        {
                            intID = Convert.ToInt32(strElement);
                            intRowHandle = view.LocateByValue(0, view.Columns["LabourUsedID"], intID);
                            if (intRowHandle != GridControl.InvalidRowHandle)
                            {
                                view.SelectRow(intRowHandle);
                                view.MakeRowVisible(intRowHandle, false);
                            }
                        }
                    }
                }
            }
            else  // Staff //
            {
                var fChildForm = new frm_HR_Select_Staff();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalStaffID = 0;
                fChildForm._Mode = "multiple";
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    viewJobs.BeginUpdate();
                    view.BeginUpdate();  // Lock view so we don't see filter switching on and off //
                    string strNewIDs = "";
                    foreach (int intRowHandle in intRowHandles)
                    {
                        intJobID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobID"));
                        intClientID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "ClientID"));
                        intSiteID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "SiteID"));
                        intVisitID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitID"));
                        dtJobExpectedStartDate = Convert.ToDateTime(viewJobs.GetRowCellValue(intRowHandle, "ExpectedStartDate"));
                        strClientName = viewJobs.GetRowCellValue(intRowHandle, "ClientName").ToString();
                        strSiteName = viewJobs.GetRowCellValue(intRowHandle, "SiteName").ToString();
                        intVisitNumber = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitNumber"));
                        strJobTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobTypeDescription").ToString();
                        strJobSubTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeDescription").ToString();
                        strContractDescription = viewJobs.GetRowCellValue(intRowHandle, "ContractDescription").ToString();
                        dblSiteLocationX = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationX"));
                        dblSiteLocationY = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationY"));
                        strSitePostcode = viewJobs.GetRowCellValue(intRowHandle, "SitePostcode").ToString();
                        intJobSubTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                        intJobTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobTypeID"));
                        intLinkedToPersonTypeID = 0;
                        strLinkedToPersonType = "Staff";

                        GridView viewContractors = (GridView)fChildForm.gridControl1.MainView;
                        for (int i = 0; i < viewContractors.DataRowCount; i++)
                        {
                            if (Convert.ToBoolean(viewContractors.GetRowCellValue(i, "CheckMarkSelection")))
                            {
                                intContractorID = Convert.ToInt32(viewContractors.GetRowCellValue(i, "StaffID"));
                                strContractorName = viewContractors.GetRowCellValue(i, "StaffName").ToString();
                                strLabourType = "N\\A";
                                dbPostcodeDistance = (double)0.00;
                                dbLatLongDistance = (double)0.00;

                                try
                                {
                                    DataRow drNewRow;
                                    drNewRow = dataSet_OM_Job.sp06182_OM_Job_Labour_Edit.NewRow();
                                    drNewRow["strMode"] = "add";
                                    drNewRow["strRecordIDs"] = "";
                                    drNewRow["ContractorID"] = intContractorID;
                                    drNewRow["JobID"] = intJobID;
                                    drNewRow["CostUnitsUsed"] = (decimal)0.00;
                                    drNewRow["CostUnitDescriptorID"] = 1;  // Hours //
                                    drNewRow["SellUnitsUsed"] = (decimal)0.00;
                                    drNewRow["SellUnitDescriptorID"] = 1;  // Hours //
                                    drNewRow["CostPerUnitExVat"] = (decimal)0.00;
                                    drNewRow["CostPerUnitVatRate"] = _decDefaultVatRate;
                                    drNewRow["SellPerUnitExVat"] = (decimal)0.00;
                                    drNewRow["SellPerUnitVatRate"] = _decDefaultVatRate;
                                    drNewRow["CostValueExVat"] = (decimal)0.00;
                                    drNewRow["CostValueVat"] = (decimal)0.00;
                                    drNewRow["CostValueTotal"] = (decimal)0.00;
                                    drNewRow["SellValueExVat"] = (decimal)0.00;
                                    drNewRow["SellValueVat"] = (decimal)0.00;
                                    drNewRow["SellValueTotal"] = (decimal)0.00;
                                    drNewRow["ClientID"] = intClientID;
                                    drNewRow["SiteID"] = intSiteID;
                                    drNewRow["VisitID"] = intVisitID;
                                    drNewRow["JobExpectedStartDate"] = dtJobExpectedStartDate;
                                    drNewRow["ClientName"] = strClientName;
                                    drNewRow["SiteName"] = strSiteName;
                                    drNewRow["VisitNumber"] = intVisitNumber;
                                    drNewRow["JobTypeDescription"] = strJobTypeDescription;
                                    drNewRow["JobSubTypeDescription"] = strJobSubTypeDescription;
                                    drNewRow["ContractorName"] = strContractorName;
                                    drNewRow["CostUnitDescriptor"] = "Hours";
                                    drNewRow["SellUnitDescriptor"] = "Hours";
                                    drNewRow["LabourType"] = strLabourType;
                                    drNewRow["CISPercentage"] = (decimal)0.00;
                                    drNewRow["CISValue"] = (decimal)0.00;
                                    drNewRow["PurchaseOrderID"] = 0;
                                    drNewRow["ContractDescription"] = strContractDescription;
                                    drNewRow["ClientNameContractDescription"] = strClientName + ", Contract: " + strContractDescription;
                                    drNewRow["JobTypeJobSubTypeDescription"] = strJobTypeDescription + " - " + strJobSubTypeDescription;
                                    drNewRow["PostcodeSiteDistance"] = dbPostcodeDistance;
                                    drNewRow["LatLongSiteDistance"] = dbLatLongDistance;
                                    drNewRow["SitePostcode"] = strSitePostcode;
                                    drNewRow["SiteLocationX"] = dblSiteLocationX;
                                    drNewRow["SiteLocationY"] = dblSiteLocationY;
                                    drNewRow["JobSubTypeID"] = intJobSubTypeID;
                                    drNewRow["JobTypeID"] = intJobTypeID;
                                    drNewRow["LinkedToPersonTypeID"] = intLinkedToPersonTypeID;
                                    drNewRow["LinkedToPersonType"] = strLinkedToPersonType;
                                    dataSet_OM_Job.sp06182_OM_Job_Labour_Edit.Rows.Add(drNewRow);
                                    strNewIDs += drNewRow["LabourUsedID"].ToString() + ",";

                                    // Update Labour Count for each job //
                                    int intFoundRow = viewJobs.LocateByValue(0, view.Columns["JobID"], intJobID);
                                    if (intFoundRow != GridControl.InvalidRowHandle)
                                    {
                                        viewJobs.SetRowCellValue(intFoundRow, "LabourCount", Convert.ToInt32(viewJobs.GetRowCellValue(intFoundRow, "LabourCount")) + 1);
                                    }
                                }
                                catch (Exception) { }
                            }
                        }
                    }
                    view.EndUpdate();  // UnLock view //
                    view.ExpandAllGroups();
                    viewJobs.EndUpdate();

                    // Highlight any recently added new rows //
                    if (!string.IsNullOrWhiteSpace(strNewIDs))
                    {
                        char[] delimiters = new char[] { ',' };
                        string[] strArray = strNewIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        int intID = 0;
                        int intRowHandle = 0;
                        view.ClearSelection(); // Clear any current selection so just the new record is selected //
                        foreach (string strElement in strArray)
                        {
                            intID = Convert.ToInt32(strElement);
                            intRowHandle = view.LocateByValue(0, view.Columns["LabourUsedID"], intID);
                            if (intRowHandle != GridControl.InvalidRowHandle)
                            {
                                view.SelectRow(intRowHandle);
                                view.MakeRowVisible(intRowHandle, false);
                            }
                        }
                    }
                }
            }
        }

        private void Block_Edit_Labour()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl8.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Job Labour records to block edit then try again.", "Block Edit Job Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Job_Wizard_Block_Edit_Labour_Info();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                double dbLatitude = (double)0.00;
                double dbLongitude = (double)0.00;
                string strPostcode = "";
                double dbPostcodeDistance = (double)0.00;
                double dbLatLongDistance = (double)0.00;
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                view.BeginUpdate();
                view.BeginSort();
                using (var CalcPostcodeDistance = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                {
                    CalcPostcodeDistance.ChangeConnectionString(strConnectionString);
                    foreach (int intRowHandle in intRowHandles)
                    {
                        if (fChildForm.intContractorID != null)
                        {
                            view.SetRowCellValue(intRowHandle, "ContractorID", fChildForm.intContractorID);

                            // Perform Distance Calculation //
                            dbPostcodeDistance = (double)0.00;
                            dbLatLongDistance = (double)0.00;
                            if (fChildForm.dbTeamLocationX != null) dbLatitude = (double)fChildForm.dbTeamLocationX;
                            if (fChildForm.dbTeamLocationY != null) dbLongitude = (double)fChildForm.dbTeamLocationY;
                            if (fChildForm.strTeamPostcode != null) strPostcode = fChildForm.strTeamPostcode;

                            if (!string.IsNullOrWhiteSpace(strPostcode) && !string.IsNullOrWhiteSpace(view.GetRowCellValue(intRowHandle, "SitePostcode").ToString()))
                            {
                                try
                                {
                                    dbPostcodeDistance = Convert.ToDouble(CalcPostcodeDistance.sp06102_OM_Calculate_Distance_Between_Postcodes(strPostcode, view.GetRowCellValue(intRowHandle, "SitePostcode").ToString()));
                                }
                                catch (Exception) { }
                            }
                            try
                            {
                                dbLatLongDistance = GeoCodeCalc.CalcDistance(dbLatitude, dbLongitude, Convert.ToDouble(view.GetRowCellValue(intRowHandle, "SiteLocationX")), Convert.ToDouble(view.GetRowCellValue(intRowHandle, "SiteLocationY")));  // Calculate Distance in Miles //
                            }
                            catch (Exception) { }

                            view.SetRowCellValue(intRowHandle, "PostcodeSiteDistance", (dbPostcodeDistance > (double)2500 ? (double)0.00 : dbPostcodeDistance));
                            view.SetRowCellValue(intRowHandle, "LatLongSiteDistance", (dbLatLongDistance > (double)2500 ? (double)0.00 : dbLatLongDistance));
                        }
                        if (fChildForm.strLinkedToPersonType != null) view.SetRowCellValue(intRowHandle, "LinkedToPersonType", fChildForm.strLinkedToPersonType);
                        if (fChildForm.intLinkedToPersonTypeID != null) view.SetRowCellValue(intRowHandle, "LinkedToPersonTypeID", fChildForm.intLinkedToPersonTypeID);
                        if (fChildForm.strContractorName != null) view.SetRowCellValue(intRowHandle, "ContractorName", fChildForm.strContractorName);
                        if (fChildForm.decCostUnitsUsed != null) view.SetRowCellValue(intRowHandle, "CostUnitsUsed", fChildForm.decCostUnitsUsed);
                        if (fChildForm.intCostUnitDescriptorID != null) view.SetRowCellValue(intRowHandle, "CostUnitDescriptorID", fChildForm.intCostUnitDescriptorID);
                        if (fChildForm.decCostPerUnitExVat != null) view.SetRowCellValue(intRowHandle, "CostPerUnitExVat", fChildForm.decCostPerUnitExVat);
                        if (fChildForm.decCostPerUnitVatRate != null) view.SetRowCellValue(intRowHandle, "CostPerUnitVatRate", fChildForm.decCostPerUnitVatRate);

                        if (fChildForm.decSellUnitsUsed != null) view.SetRowCellValue(intRowHandle, "SellUnitsUsed", fChildForm.decSellUnitsUsed);
                        if (fChildForm.intSellUnitDescriptorID != null) view.SetRowCellValue(intRowHandle, "SellUnitDescriptorID", fChildForm.intSellUnitDescriptorID);
                        if (fChildForm.decSellPerUnitExVat != null) view.SetRowCellValue(intRowHandle, "SellPerUnitExVat", fChildForm.decSellPerUnitExVat);
                        if (fChildForm.decSellPerUnitVatRate != null) view.SetRowCellValue(intRowHandle, "SellPerUnitVatRate", fChildForm.decSellPerUnitVatRate);
                        
                        if (fChildForm.decCISPercentage != null) view.SetRowCellValue(intRowHandle, "CISPercentage", fChildForm.decCISPercentage);
                        if (fChildForm.decCISValue != null) view.SetRowCellValue(intRowHandle, "CISValue", fChildForm.decCISValue);
                        if (fChildForm.decPostcodeSiteDistance != null) view.SetRowCellValue(intRowHandle, "PostcodeSiteDistance", fChildForm.decPostcodeSiteDistance);
                        if (fChildForm.decLatLongSiteDistance != null) view.SetRowCellValue(intRowHandle, "LatLongSiteDistance", fChildForm.decLatLongSiteDistance);
                        if (fChildForm.strRemarks != null) view.SetRowCellValue(intRowHandle, "Remarks", fChildForm.strRemarks);

                        this.sp06182OMJobLabourEditBindingSource.EndEdit();
                        Calculate_Cost(view, intRowHandle, "", (decimal)0.00);
                        Calculate_Sell(view, intRowHandle, "", (decimal)0.00);
                    }
                }
                view.EndSort();
                view.EndUpdate();
            }
        }

        private void bbiAddLabourFromContract_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int intRecordCount = 0;
            GridView viewJobs = (GridView)gridControl7.MainView;
            viewJobs.PostEditor();
            int[] intRowHandles;
            intRowHandles = viewJobs.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Jobs to add Labour to from Site Contracts then try again.", "Add Labour from Site Contracts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Adding Labour...");
            }

            string strSelectedSiteContractIDs = ",";
            foreach (int intRowHandle in intRowHandles)
            {
                if (!strSelectedSiteContractIDs.Contains("," + viewJobs.GetRowCellValue(intRowHandle, "SiteContractID").ToString() + ",")) strSelectedSiteContractIDs += viewJobs.GetRowCellValue(intRowHandle, "SiteContractID").ToString() + ",";
            }
            strSelectedSiteContractIDs.Remove(0, 1); // Remove preceeding comma //
            var fChildForm = new frm_OM_Select_Site_Contract_Labour();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "multiple";
            fChildForm.strPassedInSiteContractIDs = strSelectedSiteContractIDs;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;

            GridView view = (GridView)gridControl8.MainView;
            view.PostEditor();
            int intContractorID = 0;
            string strContractorName = "";
            string strLabourType = "";
            int intLinkedToPersonTypeID = 0;
            string strLinkedToPersonType = "";

            double dbPostcodeDistance = (double)0.00;
            double dbLatLongDistance = (double)0.00;

            int intJobID = 0;
            int intClientID = 0;
            int intSiteID = 0;
            int intVisitID = 0;
            DateTime dtJobExpectedStartDate = DateTime.MinValue;
            string strClientName = "";
            string strSiteName = "";
            int intVisitNumber = 0;
            string strJobTypeDescription = "";
            string strJobSubTypeDescription = "";
            string strContractDescription = "";
            double dblSiteLocationX = (double)0.00;
            double dblSiteLocationY = (double)0.00;
            string strSitePostcode = "";
            int intJobSubTypeID = 0;
            int intJobTypeID = 0;

            double dbLatitude = (double)0.00;
            double dbLongitude = (double)0.00;
            string strPostcode = "";
            int intSiteContractID = 0;

            viewJobs.BeginUpdate();
            view.BeginUpdate();
            string strNewIDs = "";

            foreach (int intRowHandle in intRowHandles)
            {
                intJobID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobID"));
                intClientID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "ClientID"));
                intSiteID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "SiteID"));
                intVisitID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitID"));
                dtJobExpectedStartDate = Convert.ToDateTime(viewJobs.GetRowCellValue(intRowHandle, "ExpectedStartDate"));
                strClientName = viewJobs.GetRowCellValue(intRowHandle, "ClientName").ToString();
                strSiteName = viewJobs.GetRowCellValue(intRowHandle, "SiteName").ToString();
                intVisitNumber = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitNumber"));
                strJobTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobTypeDescription").ToString();
                strJobSubTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeDescription").ToString();
                strContractDescription = viewJobs.GetRowCellValue(intRowHandle, "ContractDescription").ToString();
                dblSiteLocationX = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationX"));
                dblSiteLocationY = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationY"));
                strSitePostcode = viewJobs.GetRowCellValue(intRowHandle, "SitePostcode").ToString();
                intSiteContractID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "SiteContractID"));
                intJobSubTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                intJobTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobTypeID"));

                GridView viewContractors = (GridView)fChildForm.gridControl1.MainView;
                for (int i = 0; i < viewContractors.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(viewContractors.GetRowCellValue(i, "CheckMarkSelection")) && Convert.ToInt32(viewContractors.GetRowCellValue(i, "SiteContractID")) == intSiteContractID)
                    {
                        intContractorID = Convert.ToInt32(viewContractors.GetRowCellValue(i, "ContractorID"));
                        strContractorName = viewContractors.GetRowCellValue(i, "ContractorName").ToString();
                        strLabourType = viewContractors.GetRowCellValue(i, "InternalExternal").ToString();
                        intLinkedToPersonTypeID = Convert.ToInt32(viewContractors.GetRowCellValue(i, "LinkedToPersonTypeID"));
                        strLinkedToPersonType = viewContractors.GetRowCellValue(i, "LinkedToPersonType").ToString();

                        // Perform Distance Calculation //
                        dbLatitude = Convert.ToDouble(viewContractors.GetRowCellValue(i, "ContractorLatitude"));
                        dbLongitude = Convert.ToDouble(viewContractors.GetRowCellValue(i, "ContractorLongitude"));
                        strPostcode = viewContractors.GetRowCellValue(i, "ContractorPostcode").ToString();

                        if (!string.IsNullOrWhiteSpace(strPostcode) && !string.IsNullOrWhiteSpace(strSitePostcode))
                        {
                            using (var CalcPostcodeDistance = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                CalcPostcodeDistance.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    dbPostcodeDistance = Convert.ToDouble(CalcPostcodeDistance.sp06102_OM_Calculate_Distance_Between_Postcodes(strPostcode, strSitePostcode));
                                }
                                catch (Exception) { }
                            }
                        }
                        try
                        {
                            dbLatLongDistance = GeoCodeCalc.CalcDistance(dbLatitude, dbLongitude, dblSiteLocationX, dblSiteLocationY);  // Calculate Distance in Miles //
                        }
                        catch (Exception) { }
                        dbPostcodeDistance = (dbPostcodeDistance > (double)2500 ? (double)0.00 : dbPostcodeDistance);
                        dbLatLongDistance = (dbLatLongDistance > (double)2500 ? (double)0.00 : dbLatLongDistance);


                        try
                        {
                            DataRow drNewRow;
                            drNewRow = dataSet_OM_Job.sp06182_OM_Job_Labour_Edit.NewRow();
                            drNewRow["strMode"] = "add";
                            drNewRow["strRecordIDs"] = "";
                            drNewRow["ContractorID"] = intContractorID;
                            drNewRow["JobID"] = intJobID;
                            drNewRow["CostUnitsUsed"] = (decimal)0.00;
                            drNewRow["CostUnitDescriptorID"] = Convert.ToInt32(viewContractors.GetRowCellValue(i, "CostUnitDescriptorID"));
                            drNewRow["SellUnitsUsed"] = (decimal)0.00;
                            drNewRow["SellUnitDescriptorID"] = Convert.ToInt32(viewContractors.GetRowCellValue(i, "SellUnitDescriptorID"));
                            drNewRow["CostPerUnitExVat"] = Convert.ToDecimal(viewContractors.GetRowCellValue(i, "CostPerUnitExVat"));
                            drNewRow["CostPerUnitVatRate"] = Convert.ToDecimal(viewContractors.GetRowCellValue(i, "CostPerUnitVatRate"));
                            drNewRow["SellPerUnitExVat"] = Convert.ToDecimal(viewContractors.GetRowCellValue(i, "SellPerUnitExVat"));
                            drNewRow["SellPerUnitVatRate"] = Convert.ToDecimal(viewContractors.GetRowCellValue(i, "SellPerUnitVatRate"));
                            drNewRow["CostValueExVat"] = (decimal)0.00;
                            drNewRow["CostValueVat"] = (decimal)0.00;
                            drNewRow["CostValueTotal"] = (decimal)0.00;
                            drNewRow["SellValueExVat"] = (decimal)0.00;
                            drNewRow["SellValueVat"] = (decimal)0.00;
                            drNewRow["SellValueTotal"] = (decimal)0.00;
                            drNewRow["ClientID"] = intClientID;
                            drNewRow["SiteID"] = intSiteID;
                            drNewRow["VisitID"] = intVisitID;
                            drNewRow["JobExpectedStartDate"] = dtJobExpectedStartDate;
                            drNewRow["ClientName"] = strClientName;
                            drNewRow["SiteName"] = strSiteName;
                            drNewRow["VisitNumber"] = intVisitNumber;
                            drNewRow["JobTypeDescription"] = strJobTypeDescription;
                            drNewRow["JobSubTypeDescription"] = strJobSubTypeDescription;
                            drNewRow["ContractorName"] = strContractorName;
                            drNewRow["CostUnitDescriptor"] = "Hours";
                            drNewRow["SellUnitDescriptor"] = "Hours";
                            drNewRow["LabourType"] = strLabourType;
                            drNewRow["CISPercentage"] = Convert.ToDecimal(viewContractors.GetRowCellValue(i, "CISPercentage"));
                            drNewRow["CISValue"] = Convert.ToDecimal(viewContractors.GetRowCellValue(i, "CISValue"));
                            drNewRow["PurchaseOrderID"] = 0;
                            drNewRow["ContractDescription"] = strContractDescription;
                            drNewRow["ClientNameContractDescription"] = strClientName + ", Contract: " + strContractDescription;
                            drNewRow["JobTypeJobSubTypeDescription"] = strJobTypeDescription + " - " + strJobSubTypeDescription;
                            drNewRow["PostcodeSiteDistance"] = dbPostcodeDistance;
                            drNewRow["LatLongSiteDistance"] = dbLatLongDistance;
                            drNewRow["SitePostcode"] = strSitePostcode;
                            drNewRow["SiteLocationX"] = dblSiteLocationX;
                            drNewRow["SiteLocationY"] = dblSiteLocationY;
                            drNewRow["JobSubTypeID"] = intJobSubTypeID;
                            drNewRow["JobTypeID"] = intJobTypeID;
                            drNewRow["LinkedToPersonTypeID"] = intLinkedToPersonTypeID;
                            drNewRow["LinkedToPersonType"] = strLinkedToPersonType;

                            dataSet_OM_Job.sp06182_OM_Job_Labour_Edit.Rows.Add(drNewRow);
                            strNewIDs += drNewRow["LabourUsedID"].ToString() + ",";

                            // Update Labour Count for each job //
                            int intFoundRow = viewJobs.LocateByValue(0, view.Columns["JobID"], intJobID);
                            if (intFoundRow != GridControl.InvalidRowHandle)
                            {
                                viewJobs.SetRowCellValue(intFoundRow, "LabourCount", Convert.ToInt32(viewJobs.GetRowCellValue(intFoundRow, "LabourCount")) + 1);
                            }
                        }
                        catch (Exception) { }
                    }
                }
            }
            view.EndUpdate();  // UnLock view //
            view.ExpandAllGroups();
            viewJobs.EndUpdate();

            // Highlight any recently added new rows //
            if (!string.IsNullOrWhiteSpace(strNewIDs))
            {
                char[] delimiters = new char[] { ',' };
                string[] strArray = strNewIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LabourUsedID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
            }
            if (splashScreenManager.IsSplashFormVisible)
            {
                splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager.CloseWaitForm();
            }
        }

        private void bbiAddLabourFromContractAll_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int intRecordCount = 0;
            GridView viewJobs = (GridView)gridControl7.MainView;
            viewJobs.PostEditor();
            int[] intRowHandles;
            intRowHandles = viewJobs.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Jobs to add All Labour to from Site Contracts then try again.", "Add All Labour from Site Contracts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
           
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Adding Labour...");
            }

            GridView view = (GridView)gridControl8.MainView;
            view.PostEditor();
            int intContractorID = 0;
            string strContractorName = "";
            string strLabourType = "";
            int intLinkedToPersonTypeID = 0;
            string strLinkedToPersonType = "";

            double dbPostcodeDistance = (double)0.00;
            double dbLatLongDistance = (double)0.00;

            int intJobID = 0;
            int intClientID = 0;
            int intSiteID = 0;
            int intVisitID = 0;
            DateTime dtJobExpectedStartDate = DateTime.MinValue;
            string strClientName = "";
            string strSiteName = "";
            int intVisitNumber = 0;
            string strJobTypeDescription = "";
            string strJobSubTypeDescription = "";
            string strContractDescription = "";
            double dblSiteLocationX = (double)0.00;
            double dblSiteLocationY = (double)0.00;
            string strSitePostcode = "";
            int intJobSubTypeID = 0;
            int intJobTypeID = 0;

            double dbLatitude = (double)0.00;
            double dbLongitude = (double)0.00;
            string strPostcode = "";
            int intSiteContractID = 0;

            viewJobs.BeginUpdate();
            view.BeginUpdate();
            string strNewIDs = "";

            foreach (int intRowHandle in intRowHandles)
            {
                intJobID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobID"));
                intClientID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "ClientID"));
                intSiteID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "SiteID"));
                intVisitID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitID"));
                dtJobExpectedStartDate = Convert.ToDateTime(viewJobs.GetRowCellValue(intRowHandle, "ExpectedStartDate"));
                strClientName = viewJobs.GetRowCellValue(intRowHandle, "ClientName").ToString();
                strSiteName = viewJobs.GetRowCellValue(intRowHandle, "SiteName").ToString();
                intVisitNumber = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitNumber"));
                strJobTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobTypeDescription").ToString();
                strJobSubTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeDescription").ToString();
                strContractDescription = viewJobs.GetRowCellValue(intRowHandle, "ContractDescription").ToString();
                dblSiteLocationX = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationX"));
                dblSiteLocationY = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationY"));
                strSitePostcode = viewJobs.GetRowCellValue(intRowHandle, "SitePostcode").ToString();
                intSiteContractID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "SiteContractID"));
                intJobSubTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                intJobTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobTypeID"));

                // Load All Labour for SiteContractID //
                SqlDataAdapter sdaLabour = new SqlDataAdapter();
                DataSet dsLabour = new DataSet("NewDataSet");
                SqlConnection SQlConn = new SqlConnection(strConnectionString);
                using (var conn = new SqlConnection(strConnectionString))
                {
                    conn.Open();
                    string strParameter = intSiteContractID.ToString() + ",";
                    using (var cmd1 = new SqlCommand())
                    {
                        cmd1.CommandText = "sp06184_OM_Site_Contract_Labour_Cost_List";
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.Add(new SqlParameter("@strRecordIDs", strParameter));
                        cmd1.Connection = conn;
                        sdaLabour = new SqlDataAdapter(cmd1);
                        try
                        {
                            sdaLabour.Fill(dsLabour, "Table");
                        }
                        catch (Exception) { }
                    }
                }
                foreach (DataRow drSiteContractLabourRow in dsLabour.Tables[0].Rows)
                {
                    intContractorID = Convert.ToInt32(drSiteContractLabourRow["ContractorID"]);
                    strContractorName = drSiteContractLabourRow["ContractorName"].ToString();
                    strLabourType = drSiteContractLabourRow["InternalExternal"].ToString();
                    intLinkedToPersonTypeID = Convert.ToInt32(drSiteContractLabourRow["LinkedToPersonTypeID"]);
                    strLinkedToPersonType = drSiteContractLabourRow["LinkedToPersonType"].ToString();

                    // Perform Distance Calculation //
                    dbLatitude = Convert.ToDouble(drSiteContractLabourRow["ContractorLatitude"]);
                    dbLongitude = Convert.ToDouble(drSiteContractLabourRow["ContractorLongitude"]);
                    strPostcode = drSiteContractLabourRow["ContractorPostcode"].ToString();
                    if (intLinkedToPersonTypeID == 1)  // Onlu for Contractor labour - don't do it for Staff //
                    {
                        if (!string.IsNullOrWhiteSpace(strPostcode) && !string.IsNullOrWhiteSpace(strSitePostcode))
                        {
                            using (var CalcPostcodeDistance = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                CalcPostcodeDistance.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    dbPostcodeDistance = Convert.ToDouble(CalcPostcodeDistance.sp06102_OM_Calculate_Distance_Between_Postcodes(strPostcode, strSitePostcode));
                                }
                                catch (Exception) { }
                            }
                        }
                        try
                        {
                            dbLatLongDistance = GeoCodeCalc.CalcDistance(dbLatitude, dbLongitude, dblSiteLocationX, dblSiteLocationY);  // Calculate Distance in Miles //
                        }
                        catch (Exception) { }
                        dbPostcodeDistance = (dbPostcodeDistance > (double)2500 ? (double)0.00 : dbPostcodeDistance);
                        dbLatLongDistance = (dbLatLongDistance > (double)2500 ? (double)0.00 : dbLatLongDistance);
                    }

                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_OM_Job.sp06182_OM_Job_Labour_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = "";
                        drNewRow["ContractorID"] = intContractorID;
                        drNewRow["JobID"] = intJobID;
                        drNewRow["CostUnitsUsed"] = (decimal)0.00;
                        drNewRow["CostUnitDescriptorID"] = Convert.ToInt32(drSiteContractLabourRow["CostUnitDescriptorID"]);
                        drNewRow["SellUnitsUsed"] = (decimal)0.00;
                        drNewRow["SellUnitDescriptorID"] = Convert.ToInt32(drSiteContractLabourRow["SellUnitDescriptorID"]);
                        drNewRow["CostPerUnitExVat"] = Convert.ToDecimal(drSiteContractLabourRow["CostPerUnitExVat"]);
                        drNewRow["CostPerUnitVatRate"] = Convert.ToDecimal(drSiteContractLabourRow["CostPerUnitVatRate"]);
                        drNewRow["SellPerUnitExVat"] = Convert.ToDecimal(drSiteContractLabourRow["SellPerUnitExVat"]);
                        drNewRow["SellPerUnitVatRate"] = Convert.ToDecimal(drSiteContractLabourRow["SellPerUnitVatRate"]);
                        drNewRow["CostValueExVat"] = (decimal)0.00;
                        drNewRow["CostValueVat"] = (decimal)0.00;
                        drNewRow["CostValueTotal"] = (decimal)0.00;
                        drNewRow["SellValueExVat"] = (decimal)0.00;
                        drNewRow["SellValueVat"] = (decimal)0.00;
                        drNewRow["SellValueTotal"] = (decimal)0.00;
                        drNewRow["ClientID"] = intClientID;
                        drNewRow["SiteID"] = intSiteID;
                        drNewRow["VisitID"] = intVisitID;
                        drNewRow["JobExpectedStartDate"] = dtJobExpectedStartDate;
                        drNewRow["ClientName"] = strClientName;
                        drNewRow["SiteName"] = strSiteName;
                        drNewRow["VisitNumber"] = intVisitNumber;
                        drNewRow["JobTypeDescription"] = strJobTypeDescription;
                        drNewRow["JobSubTypeDescription"] = strJobSubTypeDescription;
                        drNewRow["ContractorName"] = strContractorName;
                        drNewRow["CostUnitDescriptor"] = "Hours";
                        drNewRow["SellUnitDescriptor"] = "Hours";
                        drNewRow["LabourType"] = strLabourType;
                        drNewRow["CISPercentage"] = Convert.ToDecimal(drSiteContractLabourRow["CISPercentage"]);
                        drNewRow["CISValue"] = Convert.ToDecimal(drSiteContractLabourRow["CISValue"]);
                        drNewRow["PurchaseOrderID"] = 0;
                        drNewRow["ContractDescription"] = strContractDescription;
                        drNewRow["ClientNameContractDescription"] = strClientName + ", Contract: " + strContractDescription;
                        drNewRow["JobTypeJobSubTypeDescription"] = strJobTypeDescription + " - " + strJobSubTypeDescription;
                        drNewRow["PostcodeSiteDistance"] = dbPostcodeDistance;
                        drNewRow["LatLongSiteDistance"] = dbLatLongDistance;
                        drNewRow["SitePostcode"] = strSitePostcode;
                        drNewRow["SiteLocationX"] = dblSiteLocationX;
                        drNewRow["SiteLocationY"] = dblSiteLocationY;
                        drNewRow["JobSubTypeID"] = intJobSubTypeID;
                        drNewRow["JobTypeID"] = intJobTypeID;
                        drNewRow["LinkedToPersonTypeID"] = intLinkedToPersonTypeID;
                        drNewRow["LinkedToPersonType"] = strLinkedToPersonType;

                        dataSet_OM_Job.sp06182_OM_Job_Labour_Edit.Rows.Add(drNewRow);
                        strNewIDs += drNewRow["LabourUsedID"].ToString() + ",";

                        // Update Labour Count for each job //
                        int intFoundRow = viewJobs.LocateByValue(0, view.Columns["JobID"], intJobID);
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewJobs.SetRowCellValue(intFoundRow, "LabourCount", Convert.ToInt32(viewJobs.GetRowCellValue(intFoundRow, "LabourCount")) + 1);
                        }
                    }
                    catch (Exception) { }
                }

            }
            view.EndUpdate();  // UnLock view //
            view.ExpandAllGroups();
            viewJobs.EndUpdate();

            // Highlight any recently added new rows //
            if (!string.IsNullOrWhiteSpace(strNewIDs))
            {
                char[] delimiters = new char[] { ',' };
                string[] strArray = strNewIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LabourUsedID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
            }
            if (splashScreenManager.IsSplashFormVisible)
            {
                splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager.CloseWaitForm();
            }
        }

        #endregion


        #region Step 7 Page - Equipment

        private void btnStep7Previous_Click(object sender, EventArgs e)
        {
            Filter_Labour_On_Jobs();
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep6;
        }
        private void btnStep7Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl10.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            Clear_Filter_Equipment_On_Jobs();
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep8;

            view = (GridView)gridControl11.MainView;
            view.ExpandAllGroups();

            Filter_Materials_On_Jobs();
        }


        #region GridView9

        private void gridView9_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "EquipmentCount")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "EquipmentCount")) <= 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridView9_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.NONE;
            SetMenuStatus();
        }

        private void gridView9_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.NONE;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView10

        private void gridControl10_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Equipment();
                    }
                    else if ("block add".Equals(e.Button.Tag))
                    {
                        Block_Add_Equipment();
                    }
                    else if ("block edit".Equals(e.Button.Tag))
                    {
                        Block_Edit_Equipment();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView10_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Equipment;
            SetMenuStatus();
        }

        private void gridView10_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Equipment;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemButtonEditEquipmentName_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                GridView viewLabour = (GridView)gridControl8.MainView;  // Labour - get unique list of preferred and pass to equipment screen //
                string strSelectedContractorIDs = ",";
                for (int i = 0; i < viewLabour.DataRowCount; i++)
                {
                    if (!strSelectedContractorIDs.Contains("," + viewLabour.GetRowCellValue(i, "ContractorID").ToString() + ",")) strSelectedContractorIDs += viewLabour.GetRowCellValue(i, "ContractorID").ToString() + ",";
                }
                strSelectedContractorIDs = strSelectedContractorIDs.Remove(0, 1);  // Remove preceeding comma //
                
                var currentRowView = (DataRowView)sp06187OMJobEquipmentEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06187_OM_Job_Equipment_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                int intOriginalEquipmentID = (string.IsNullOrEmpty(currentRow.EquipmentID.ToString()) ? 0 : Convert.ToInt32(currentRow.EquipmentID));

                var fChildForm = new frm_OM_Select_Equipment();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalID = intOriginalEquipmentID;
                fChildForm._FilteredContractorIDs = strSelectedContractorIDs;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (fChildForm.intSelectedID == intOriginalEquipmentID) return;
                    currentRow.EquipmentID = fChildForm.intSelectedID;
                    currentRow.EquipmentName = fChildForm.strSelectedDescription1;
                    currentRow.OwnerName = fChildForm.strSelectedDescription2;
                    currentRow.OwnerType = fChildForm.strSelectedDescription3;

                    sp06187OMJobEquipmentEditBindingSource.EndEdit();
                    GridView view = (GridView)gridControl10.MainView;
                    view.FocusedColumn = colOwnerType;  // Move focus to next column so new value is shown in original column //
                }
            }
        }

        private void repositoryItemSpinEdit2DP10_EditValueChanged(object sender, EventArgs e)
        {
            gridView10.PostEditor();
            GridView view = (GridView)gridControl10.MainView;
            if (view.FocusedColumn.FieldName == "CostUnitsUsed")
            {
                SpinEdit se = (SpinEdit)sender;
                decimal decValue = Convert.ToDecimal(se.Value);
                Calculate_Cost2(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            }
            else if (view.FocusedColumn.FieldName == "SellUnitsUsed")
            {
                SpinEdit se = (SpinEdit)sender;
                decimal decValue = Convert.ToDecimal(se.Value);
                Calculate_Sell2(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            }
        }
        private void repositoryItemSpinEditCurrency10_EditValueChanged(object sender, EventArgs e)
        {
            gridView10.PostEditor();
            GridView view = (GridView)gridControl10.MainView;
            if (view.FocusedColumn.FieldName == "CostPerUnitExVat")
            {
                SpinEdit se = (SpinEdit)sender;
                decimal decValue = Convert.ToDecimal(se.Value);
                Calculate_Cost2(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            }
            else if (view.FocusedColumn.FieldName == "SellPerUnitExVat")
            {
                SpinEdit se = (SpinEdit)sender;
                decimal decValue = Convert.ToDecimal(se.Value);
                Calculate_Sell2(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            }
        }
        private void repositoryItemSpinEditPercentage10_EditValueChanged(object sender, EventArgs e)
        {
            gridView10.PostEditor();
            GridView view = (GridView)gridControl10.MainView;
            if (view.FocusedColumn.FieldName == "CostPerUnitVatRate")
            {
                SpinEdit se = (SpinEdit)sender;
                decimal decValue = Convert.ToDecimal(se.Value);
                Calculate_Cost2(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            }
            else if (view.FocusedColumn.FieldName == "SellPerUnitVatRate")
            {
                SpinEdit se = (SpinEdit)sender;
                decimal decValue = Convert.ToDecimal(se.Value);
                Calculate_Sell2(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            }
        }

        private void Calculate_Cost2(GridView view, int intRow, string strCurrentColumn, decimal decValue)
        {
            decimal decTotalValueExVat = (decimal)0.00;
            decimal decTotalValueVat = (decimal)0.00;
            decimal decTotalValue = (decimal)0.00;
            decimal decCostUnitsUsed = (strCurrentColumn == "CostUnitsUsed" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "CostUnitsUsed")));
            decimal decCostPerUnitExVat = (strCurrentColumn == "CostPerUnitExVat" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "CostPerUnitExVat")));
            decimal decCostPerUnitVatRate = (strCurrentColumn == "CostPerUnitVatRate" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "CostPerUnitVatRate")));

            decTotalValueExVat = decCostUnitsUsed * decCostPerUnitExVat;
            decTotalValueVat = (decTotalValueExVat * decCostPerUnitVatRate) / 100;
            decTotalValue = decTotalValueExVat + decTotalValueVat;
            view.SetRowCellValue(intRow, "CostValueExVat", decTotalValueExVat);
            view.SetRowCellValue(intRow, "CostValueVat", decTotalValueVat);
            view.SetRowCellValue(intRow, "CostValueTotal", decTotalValue);
            this.sp06187OMJobEquipmentEditBindingSource.EndEdit();
        }

        private void Calculate_Sell2(GridView view, int intRow, string strCurrentColumn, decimal decValue)
        {
            decimal decTotalValueExVat = (decimal)0.00;
            decimal decTotalValueVat = (decimal)0.00;
            decimal decTotalValue = (decimal)0.00;
            decimal decSellUnitsUsed = (strCurrentColumn == "SellUnitsUsed" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SellUnitsUsed")));
            decimal decSellPerUnitExVat = (strCurrentColumn == "SellPerUnitExVat" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SellPerUnitExVat")));
            decimal decSellPerUnitVatRate = (strCurrentColumn == "SellPerUnitVatRate" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SellPerUnitVatRate")));

            decTotalValueExVat = decSellUnitsUsed * decSellPerUnitExVat;
            decTotalValueVat = (decTotalValueExVat * decSellPerUnitVatRate) / 100;
            decTotalValue = decTotalValueExVat + decTotalValueVat;
            view.SetRowCellValue(intRow, "SellValueExVat", decTotalValueExVat);
            view.SetRowCellValue(intRow, "SellValueVat", decTotalValueVat);
            view.SetRowCellValue(intRow, "SellValueTotal", decTotalValue);
            this.sp06187OMJobEquipmentEditBindingSource.EndEdit();
        }

        #endregion


        private void Filter_Equipment_On_Jobs()
        {
            GridView view = (GridView)gridControl10.MainView;
            view.PostEditor();

            string strFilterCondition = "";
            GridView parentView = (GridView)gridControl9.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                strFilterCondition = "[JobID] = 9999999999";  // Don't show anything //
            }
            else
            {
                foreach (int i in intRowHandles)
                {
                    strFilterCondition += (string.IsNullOrWhiteSpace(strFilterCondition) ? "" : " or ");
                    strFilterCondition += "[JobID] = " + parentView.GetRowCellValue(i, "JobID").ToString();
                }
            }
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = strFilterCondition;
            view.MakeRowVisible(-1, true);
            view.ExpandAllGroups();
            view.EndUpdate();
            SetMenuStatus();
        }

        private void Clear_Filter_Equipment_On_Jobs()
        {
            GridView view = (GridView)gridControl10.MainView;
            view.PostEditor();

            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
            SetMenuStatus();
        }

        private void Add_Equipment()
        {
            GridView viewJobs = (GridView)gridControl9.MainView;
            int[] intRowHandles = viewJobs.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Jobs to add the Equipment to before proceeding.", "Add Equipment", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intRowHandles.Length > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + " Job(s) selected. If you proceed a new Equipment record will be created for each of these jobs.\n\nProceed?", "Add Equipment", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }

            GridView view = (GridView)gridControl10.MainView;
            view.PostEditor();
            int intEquipmentID = 0;
            string strEquipmentName = "";
            string strOwnerName = "";
            string strOwnerType = "";
            double dbPostcodeDistance = (double)0.00;
            double dbLatLongDistance = (double)0.00;

            int intJobID = 0;
            int intClientID = 0;
            int intSiteID = 0;
            int intVisitID = 0;
            DateTime dtJobExpectedStartDate = DateTime.MinValue;
            string strClientName = "";
            string strSiteName = "";
            int intVisitNumber = 0;
            string strJobTypeDescription = "";
            string strJobSubTypeDescription = "";
            string strContractDescription = "";
            double dblSiteLocationX = (double)0.00;
            double dblSiteLocationY = (double)0.00;
            string strSitePostcode = "";
            int intJobSubTypeID = 0;
            int intJobTypeID = 0;

            GridView viewLabour = (GridView)gridControl8.MainView;  // Labour - get unique list of preferred and pass to equipment screen //
            string strSelectedContractorIDs = ",";
            for (int i = 0; i < viewLabour.DataRowCount; i++)
            {
                if (!strSelectedContractorIDs.Contains("," + viewLabour.GetRowCellValue(i, "ContractorID").ToString() + ",")) strSelectedContractorIDs += viewLabour.GetRowCellValue(i, "ContractorID").ToString() + ",";
            }
            strSelectedContractorIDs = strSelectedContractorIDs.Remove(0, 1);  // Remove preceeding comma //

            var fChildForm = new frm_OM_Select_Equipment();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "single";
            fChildForm._FilteredContractorIDs = strSelectedContractorIDs;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;
            intEquipmentID = fChildForm.intSelectedID;
            strEquipmentName = fChildForm.strSelectedDescription1;
            strOwnerName = fChildForm.strSelectedDescription2;
            strOwnerType = fChildForm.strSelectedDescription3;

            viewJobs.BeginUpdate();
            view.BeginUpdate();  // Lock view //
            string strNewIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                intJobID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobID"));
                intClientID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "ClientID"));
                intSiteID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "SiteID"));
                intVisitID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitID"));
                dtJobExpectedStartDate = Convert.ToDateTime(viewJobs.GetRowCellValue(intRowHandle, "ExpectedStartDate"));
                strClientName = viewJobs.GetRowCellValue(intRowHandle, "ClientName").ToString();
                strSiteName = viewJobs.GetRowCellValue(intRowHandle, "SiteName").ToString();
                intVisitNumber = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitNumber"));
                strJobTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobTypeDescription").ToString();
                strJobSubTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeDescription").ToString();
                strContractDescription = viewJobs.GetRowCellValue(intRowHandle, "ContractDescription").ToString();
                dblSiteLocationX = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationX"));
                dblSiteLocationY = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationY"));
                strSitePostcode = viewJobs.GetRowCellValue(intRowHandle, "SitePostcode").ToString();
                intJobSubTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                intJobTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobTypeID"));

                try
                {
                    DataRow drNewRow;
                    drNewRow = dataSet_OM_Job.sp06187_OM_Job_Equipment_Edit.NewRow();
                    drNewRow["strMode"] = "add";
                    drNewRow["strRecordIDs"] = "";
                    drNewRow["EquipmentID"] = intEquipmentID;
                    drNewRow["JobID"] = intJobID;
                    drNewRow["CostUnitsUsed"] = (decimal)0.00;
                    drNewRow["CostUnitDescriptorID"] = 1;  // Hours //
                    drNewRow["SellUnitsUsed"] = (decimal)0.00;
                    drNewRow["SellUnitDescriptorID"] = 1;  // Hours //
                    drNewRow["CostPerUnitExVat"] = (decimal)0.00;
                    drNewRow["CostPerUnitVatRate"] = _decDefaultVatRate;
                    drNewRow["SellPerUnitExVat"] = (decimal)0.00;
                    drNewRow["SellPerUnitVatRate"] = _decDefaultVatRate;
                    drNewRow["CostValueExVat"] = (decimal)0.00;
                    drNewRow["CostValueVat"] = (decimal)0.00;
                    drNewRow["CostValueTotal"] = (decimal)0.00;
                    drNewRow["SellValueExVat"] = (decimal)0.00;
                    drNewRow["SellValueVat"] = (decimal)0.00;
                    drNewRow["SellValueTotal"] = (decimal)0.00;
                    drNewRow["ClientID"] = intClientID;
                    drNewRow["SiteID"] = intSiteID;
                    drNewRow["VisitID"] = intVisitID;
                    drNewRow["JobExpectedStartDate"] = dtJobExpectedStartDate;
                    drNewRow["ClientName"] = strClientName;
                    drNewRow["SiteName"] = strSiteName;
                    drNewRow["VisitNumber"] = intVisitNumber;
                    drNewRow["JobTypeDescription"] = strJobTypeDescription;
                    drNewRow["JobSubTypeDescription"] = strJobSubTypeDescription;
                    drNewRow["EquipmentName"] = strEquipmentName;
                    drNewRow["CostUnitDescriptor"] = "Hours";
                    drNewRow["SellUnitDescriptor"] = "Hours";
                    drNewRow["OwnerName"] = strOwnerName;
                    drNewRow["OwnerType"] = strOwnerType;
                    drNewRow["PurchaseOrderID"] = 0;
                    drNewRow["ContractDescription"] = strContractDescription;
                    drNewRow["ClientNameContractDescription"] = strClientName + ", Contract: " + strContractDescription;
                    drNewRow["JobTypeJobSubTypeDescription"] = strJobTypeDescription + " - " + strJobSubTypeDescription;
                    drNewRow["PostcodeSiteDistance"] = dbPostcodeDistance;
                    drNewRow["LatLongSiteDistance"] = dbLatLongDistance;
                    drNewRow["SitePostcode"] = strSitePostcode;
                    drNewRow["SiteLocationX"] = dblSiteLocationX;
                    drNewRow["SiteLocationY"] = dblSiteLocationY;
                    drNewRow["JobSubTypeID"] = intJobSubTypeID;
                    drNewRow["JobTypeID"] = intJobTypeID;
                    dataSet_OM_Job.sp06187_OM_Job_Equipment_Edit.Rows.Add(drNewRow);
                    strNewIDs += drNewRow["EquipmentUsedID"].ToString() + ",";
                }
                catch (Exception) { }

                // Update Labour Count for each job //
                int intFoundRow = viewJobs.LocateByValue(0, view.Columns["JobID"], intJobID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    viewJobs.SetRowCellValue(intFoundRow, "EquipmentCount", Convert.ToInt32(viewJobs.GetRowCellValue(intFoundRow, "EquipmentCount")) + 1);
                }
            }
            view.EndUpdate();  // UnLock view //
            view.ExpandAllGroups();
            viewJobs.EndUpdate();

            // Highlight any recently added new rows //
            if (!string.IsNullOrWhiteSpace(strNewIDs))
            {
                char[] delimiters = new char[] { ',' };
                string[] strArray = strNewIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["EquipmentUsedID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
            }
        }

        private void Block_Add_Equipment()
        {
            GridView viewJobs = (GridView)gridControl9.MainView;
            int[] intRowHandles = viewJobs.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Jobs to block add the Equipment to before proceeding.", "Block Add Equipment", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intRowHandles.Length > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + " Job(s) selected. If you proceed one or more new Equipment records will be created for each of these jobs.\n\nProceed?", "Block Add Equipment", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }

            GridView view = (GridView)gridControl10.MainView;
            view.PostEditor();
            int intEquipmentID = 0;
            string strEquipmentName = "";
            string strOwnerName = "";
            string strOwnerType = "";
            double dbPostcodeDistance = (double)0.00;
            double dbLatLongDistance = (double)0.00;

            int intJobID = 0;
            int intClientID = 0;
            int intSiteID = 0;
            int intVisitID = 0;
            DateTime dtJobExpectedStartDate = DateTime.MinValue;
            string strClientName = "";
            string strSiteName = "";
            int intVisitNumber = 0;
            string strJobTypeDescription = "";
            string strJobSubTypeDescription = "";
            string strContractDescription = "";
            double dblSiteLocationX = (double)0.00;
            double dblSiteLocationY = (double)0.00;
            string strSitePostcode = "";
            int intJobSubTypeID = 0;
            int intJobTypeID = 0;

            GridView viewLabour = (GridView)gridControl8.MainView;  // Labour - get unique list of preferred and pass to equipment screen //
            string strSelectedContractorIDs = ",";
            for (int i = 0; i < viewLabour.DataRowCount; i++)
            {
                if (!strSelectedContractorIDs.Contains("," + viewLabour.GetRowCellValue(i, "ContractorID").ToString() + ",")) strSelectedContractorIDs += viewLabour.GetRowCellValue(i, "ContractorID").ToString() + ",";
            }
            strSelectedContractorIDs = strSelectedContractorIDs.Remove(0, 1);  // Remove preceeding comma //

            var fChildForm = new frm_OM_Select_Equipment();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "multiple";
            fChildForm._FilteredContractorIDs = strSelectedContractorIDs;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                viewJobs.BeginUpdate();
                view.BeginUpdate();  // Lock view so we don't see filter switching on and off //
                string strNewIDs = "";
                foreach (int intRowHandle in intRowHandles)
                {
                    intJobID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobID"));
                    intClientID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "ClientID"));
                    intSiteID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "SiteID"));
                    intVisitID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitID"));
                    dtJobExpectedStartDate = Convert.ToDateTime(viewJobs.GetRowCellValue(intRowHandle, "ExpectedStartDate"));
                    strClientName = viewJobs.GetRowCellValue(intRowHandle, "ClientName").ToString();
                    strSiteName = viewJobs.GetRowCellValue(intRowHandle, "SiteName").ToString();
                    intVisitNumber = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitNumber"));
                    strJobTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobTypeDescription").ToString();
                    strJobSubTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeDescription").ToString();
                    strContractDescription = viewJobs.GetRowCellValue(intRowHandle, "ContractDescription").ToString();
                    dblSiteLocationX = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationX"));
                    dblSiteLocationY = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationY"));
                    strSitePostcode = viewJobs.GetRowCellValue(intRowHandle, "SitePostcode").ToString();
                    intJobSubTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                    intJobTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobTypeID"));

                    GridView viewEquipment = (GridView)fChildForm.gridControl1.MainView;
                    for (int i = 0; i < viewEquipment.DataRowCount; i++)
                    {
                        if (Convert.ToBoolean(viewEquipment.GetRowCellValue(i, "CheckMarkSelection")))
                        {
                            intEquipmentID = Convert.ToInt32(viewEquipment.GetRowCellValue(i, "EquipmentID"));
                            strEquipmentName = viewEquipment.GetRowCellValue(i, "EquipmentType").ToString();
                            strOwnerName = viewEquipment.GetRowCellValue(i, "OwnerName").ToString();
                            strOwnerType = viewEquipment.GetRowCellValue(i, "OwnerType").ToString();
                            
                            try
                            {
                                DataRow drNewRow;
                                drNewRow = dataSet_OM_Job.sp06187_OM_Job_Equipment_Edit.NewRow();
                                drNewRow["strMode"] = "add";
                                drNewRow["strRecordIDs"] = "";
                                drNewRow["EquipmentID"] = intEquipmentID;
                                drNewRow["JobID"] = intJobID;
                                drNewRow["CostUnitsUsed"] = (decimal)0.00;
                                drNewRow["CostUnitDescriptorID"] = 1;  // Hours //
                                drNewRow["SellUnitsUsed"] = (decimal)0.00;
                                drNewRow["SellUnitDescriptorID"] = 1;  // Hours //
                                drNewRow["CostPerUnitExVat"] = (decimal)0.00;
                                drNewRow["CostPerUnitVatRate"] = _decDefaultVatRate;
                                drNewRow["SellPerUnitExVat"] = (decimal)0.00;
                                drNewRow["SellPerUnitVatRate"] = _decDefaultVatRate;
                                drNewRow["CostValueExVat"] = (decimal)0.00;
                                drNewRow["CostValueVat"] = (decimal)0.00;
                                drNewRow["CostValueTotal"] = (decimal)0.00;
                                drNewRow["SellValueExVat"] = (decimal)0.00;
                                drNewRow["SellValueVat"] = (decimal)0.00;
                                drNewRow["SellValueTotal"] = (decimal)0.00;
                                drNewRow["ClientID"] = intClientID;
                                drNewRow["SiteID"] = intSiteID;
                                drNewRow["VisitID"] = intVisitID;
                                drNewRow["JobExpectedStartDate"] = dtJobExpectedStartDate;
                                drNewRow["ClientName"] = strClientName;
                                drNewRow["SiteName"] = strSiteName;
                                drNewRow["VisitNumber"] = intVisitNumber;
                                drNewRow["JobTypeDescription"] = strJobTypeDescription;
                                drNewRow["JobSubTypeDescription"] = strJobSubTypeDescription;
                                drNewRow["EquipmentName"] = strEquipmentName;
                                drNewRow["CostUnitDescriptor"] = "Hours";
                                drNewRow["SellUnitDescriptor"] = "Hours";
                                drNewRow["OwnerName"] = strOwnerName;
                                drNewRow["OwnerType"] = strOwnerType;
                                drNewRow["PurchaseOrderID"] = 0;
                                drNewRow["ContractDescription"] = strContractDescription;
                                drNewRow["ClientNameContractDescription"] = strClientName + ", Contract: " + strContractDescription;
                                drNewRow["JobTypeJobSubTypeDescription"] = strJobTypeDescription + " - " + strJobSubTypeDescription;
                                drNewRow["PostcodeSiteDistance"] = dbPostcodeDistance;
                                drNewRow["LatLongSiteDistance"] = dbLatLongDistance;
                                drNewRow["SitePostcode"] = strSitePostcode;
                                drNewRow["SiteLocationX"] = dblSiteLocationX;
                                drNewRow["SiteLocationY"] = dblSiteLocationY;
                                drNewRow["JobSubTypeID"] = intJobSubTypeID;
                                drNewRow["JobTypeID"] = intJobTypeID;
                                dataSet_OM_Job.sp06187_OM_Job_Equipment_Edit.Rows.Add(drNewRow);
                                strNewIDs += drNewRow["EquipmentUsedID"].ToString() + ",";

                                // Update Labour Count for each job //
                                int intFoundRow = viewJobs.LocateByValue(0, view.Columns["JobID"], intJobID);
                                if (intFoundRow != GridControl.InvalidRowHandle)
                                {
                                    viewJobs.SetRowCellValue(intFoundRow, "EquipmentCount", Convert.ToInt32(viewJobs.GetRowCellValue(intFoundRow, "EquipmentCount")) + 1);
                                }
                            }
                            catch (Exception) { }
                        }
                    }
                }
                view.EndUpdate();  // UnLock view //
                view.ExpandAllGroups();
                viewJobs.EndUpdate();

                // Highlight any recently added new rows //
                if (!string.IsNullOrWhiteSpace(strNewIDs))
                {
                    char[] delimiters = new char[] { ',' };
                    string[] strArray = strNewIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    int intID = 0;
                    int intRowHandle = 0;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["EquipmentUsedID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                }
            }
        }

        private void Block_Edit_Equipment()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl10.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Job Equipment records to block edit then try again.", "Block Edit Job Equipment", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Job_Wizard_Block_Edit_Equipment_Info();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                view.BeginUpdate();
                view.BeginSort();
                using (var CalcPostcodeDistance = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                {
                    CalcPostcodeDistance.ChangeConnectionString(strConnectionString);
                    foreach (int intRowHandle in intRowHandles)
                    {
                        if (fChildForm.intEquipmentID != null)
                        {
                            view.SetRowCellValue(intRowHandle, "EquipmentID", fChildForm.intEquipmentID);
                        }
                        if (fChildForm.strEquipmentType != null) view.SetRowCellValue(intRowHandle, "EquipmentName", fChildForm.strEquipmentType);
                        if (fChildForm.strOwnerName != null) view.SetRowCellValue(intRowHandle, "OwnerName", fChildForm.strOwnerName);
                        if (fChildForm.strOwnerType != null) view.SetRowCellValue(intRowHandle, "OwnerType", fChildForm.strOwnerType);
                        
                        if (fChildForm.decCostUnitsUsed != null) view.SetRowCellValue(intRowHandle, "CostUnitsUsed", fChildForm.decCostUnitsUsed);
                        if (fChildForm.intCostUnitDescriptorID != null) view.SetRowCellValue(intRowHandle, "CostUnitDescriptorID", fChildForm.intCostUnitDescriptorID);
                        if (fChildForm.decCostPerUnitExVat != null) view.SetRowCellValue(intRowHandle, "CostPerUnitExVat", fChildForm.decCostPerUnitExVat);
                        if (fChildForm.decCostPerUnitVatRate != null) view.SetRowCellValue(intRowHandle, "CostPerUnitVatRate", fChildForm.decCostPerUnitVatRate);

                        if (fChildForm.decSellUnitsUsed != null) view.SetRowCellValue(intRowHandle, "SellUnitsUsed", fChildForm.decSellUnitsUsed);
                        if (fChildForm.intSellUnitDescriptorID != null) view.SetRowCellValue(intRowHandle, "SellUnitDescriptorID", fChildForm.intSellUnitDescriptorID);
                        if (fChildForm.decSellPerUnitExVat != null) view.SetRowCellValue(intRowHandle, "SellPerUnitExVat", fChildForm.decSellPerUnitExVat);
                        if (fChildForm.decSellPerUnitVatRate != null) view.SetRowCellValue(intRowHandle, "SellPerUnitVatRate", fChildForm.decSellPerUnitVatRate);

                        if (fChildForm.strRemarks != null) view.SetRowCellValue(intRowHandle, "Remarks", fChildForm.strRemarks);

                        this.sp06187OMJobEquipmentEditBindingSource.EndEdit();
                        Calculate_Cost2(view, intRowHandle, "", (decimal)0.00);
                        Calculate_Sell2(view, intRowHandle, "", (decimal)0.00);
                    }
                }
                view.EndSort();
                view.EndUpdate();
            }
        }


        private void bbiAddEquipmentFromContract_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int intRecordCount = 0;
            GridView viewJobs = (GridView)gridControl9.MainView;
            viewJobs.PostEditor();
            int[] intRowHandles;
            intRowHandles = viewJobs.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Jobs to add Equipment to from Site Contracts then try again.", "Add Equipment from Site Contracts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedSiteContractIDs = ",";
            foreach (int intRowHandle in intRowHandles)
            {
                if (!strSelectedSiteContractIDs.Contains("," + viewJobs.GetRowCellValue(intRowHandle, "SiteContractID").ToString() + ",")) strSelectedSiteContractIDs += viewJobs.GetRowCellValue(intRowHandle, "SiteContractID").ToString() + ",";
            }
            strSelectedSiteContractIDs.Remove(0, 1); // Remove preceeding comma //
            var fChildForm = new frm_OM_Select_Site_Contract_Equipment();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "multiple";
            fChildForm.strPassedInSiteContractIDs = strSelectedSiteContractIDs;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;

            GridView view = (GridView)gridControl10.MainView;
            view.PostEditor();
            int intEquipmentID = 0;
            string strEquipmentName = "";
            string strOwnerName = "";
            string strOwnerType = "";
            double dbPostcodeDistance = (double)0.00;
            double dbLatLongDistance = (double)0.00;

            int intJobID = 0;
            int intClientID = 0;
            int intSiteID = 0;
            int intVisitID = 0;
            DateTime dtJobExpectedStartDate = DateTime.MinValue;
            string strClientName = "";
            string strSiteName = "";
            int intVisitNumber = 0;
            string strJobTypeDescription = "";
            string strJobSubTypeDescription = "";
            string strContractDescription = "";
            double dblSiteLocationX = (double)0.00;
            double dblSiteLocationY = (double)0.00;
            string strSitePostcode = "";
            int intJobSubTypeID = 0;
            int intJobTypeID = 0;

            double dbLatitude = (double)0.00;
            double dbLongitude = (double)0.00;
            string strPostcode = "";
            int intSiteContractID = 0;

            viewJobs.BeginUpdate();
            view.BeginUpdate();
            string strNewIDs = "";

            foreach (int intRowHandle in intRowHandles)
            {
                intJobID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobID"));
                intClientID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "ClientID"));
                intSiteID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "SiteID"));
                intVisitID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitID"));
                dtJobExpectedStartDate = Convert.ToDateTime(viewJobs.GetRowCellValue(intRowHandle, "ExpectedStartDate"));
                strClientName = viewJobs.GetRowCellValue(intRowHandle, "ClientName").ToString();
                strSiteName = viewJobs.GetRowCellValue(intRowHandle, "SiteName").ToString();
                intVisitNumber = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitNumber"));
                strJobTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobTypeDescription").ToString();
                strJobSubTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeDescription").ToString();
                strContractDescription = viewJobs.GetRowCellValue(intRowHandle, "ContractDescription").ToString();
                dblSiteLocationX = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationX"));
                dblSiteLocationY = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationY"));
                strSitePostcode = viewJobs.GetRowCellValue(intRowHandle, "SitePostcode").ToString();
                intSiteContractID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "SiteContractID"));
                intJobSubTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                intJobTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobTypeID"));

                GridView viewContractors = (GridView)fChildForm.gridControl1.MainView;
                for (int i = 0; i < viewContractors.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(viewContractors.GetRowCellValue(i, "CheckMarkSelection")) && Convert.ToInt32(viewContractors.GetRowCellValue(i, "SiteContractID")) == intSiteContractID)
                    {
                        intEquipmentID = Convert.ToInt32(viewContractors.GetRowCellValue(i, "EquipmentID"));
                        strEquipmentName = viewContractors.GetRowCellValue(i, "EquipmentName").ToString();
                        strOwnerName = viewContractors.GetRowCellValue(i, "OwnerName").ToString();
                        strOwnerType = viewContractors.GetRowCellValue(i, "OwnerType").ToString();

                        try
                        {
                            DataRow drNewRow;
                            drNewRow = dataSet_OM_Job.sp06187_OM_Job_Equipment_Edit.NewRow();
                            drNewRow["strMode"] = "add";
                            drNewRow["strRecordIDs"] = "";
                            drNewRow["EquipmentID"] = intEquipmentID;
                            drNewRow["JobID"] = intJobID;
                            drNewRow["CostUnitsUsed"] = (decimal)0.00;
                            drNewRow["CostUnitDescriptorID"] = Convert.ToInt32(viewContractors.GetRowCellValue(i, "CostUnitDescriptorID"));
                            drNewRow["SellUnitsUsed"] = (decimal)0.00;
                            drNewRow["SellUnitDescriptorID"] = Convert.ToInt32(viewContractors.GetRowCellValue(i, "SellUnitDescriptorID"));
                            drNewRow["CostPerUnitExVat"] = Convert.ToDecimal(viewContractors.GetRowCellValue(i, "CostPerUnitExVat"));
                            drNewRow["CostPerUnitVatRate"] = Convert.ToDecimal(viewContractors.GetRowCellValue(i, "CostPerUnitVatRate"));
                            drNewRow["SellPerUnitExVat"] = Convert.ToDecimal(viewContractors.GetRowCellValue(i, "SellPerUnitExVat"));
                            drNewRow["SellPerUnitVatRate"] = Convert.ToDecimal(viewContractors.GetRowCellValue(i, "SellPerUnitVatRate"));
                            drNewRow["CostValueExVat"] = (decimal)0.00;
                            drNewRow["CostValueVat"] = (decimal)0.00;
                            drNewRow["CostValueTotal"] = (decimal)0.00;
                            drNewRow["SellValueExVat"] = (decimal)0.00;
                            drNewRow["SellValueVat"] = (decimal)0.00;
                            drNewRow["SellValueTotal"] = (decimal)0.00;
                            drNewRow["ClientID"] = intClientID;
                            drNewRow["SiteID"] = intSiteID;
                            drNewRow["VisitID"] = intVisitID;
                            drNewRow["JobExpectedStartDate"] = dtJobExpectedStartDate;
                            drNewRow["ClientName"] = strClientName;
                            drNewRow["SiteName"] = strSiteName;
                            drNewRow["VisitNumber"] = intVisitNumber;
                            drNewRow["JobTypeDescription"] = strJobTypeDescription;
                            drNewRow["JobSubTypeDescription"] = strJobSubTypeDescription;
                            drNewRow["EquipmentName"] = strEquipmentName;
                            drNewRow["CostUnitDescriptor"] = "Hours";
                            drNewRow["SellUnitDescriptor"] = "Hours";
                            drNewRow["OwnerName"] = strOwnerName;
                            drNewRow["OwnerType"] = strOwnerType;
                            drNewRow["PurchaseOrderID"] = 0;
                            drNewRow["ContractDescription"] = strContractDescription;
                            drNewRow["ClientNameContractDescription"] = strClientName + ", Contract: " + strContractDescription;
                            drNewRow["JobTypeJobSubTypeDescription"] = strJobTypeDescription + " - " + strJobSubTypeDescription;
                            drNewRow["SitePostcode"] = strSitePostcode;
                            drNewRow["SiteLocationX"] = dblSiteLocationX;
                            drNewRow["SiteLocationY"] = dblSiteLocationY;
                            drNewRow["JobSubTypeID"] = intJobSubTypeID;
                            drNewRow["JobTypeID"] = intJobTypeID;
                            dataSet_OM_Job.sp06187_OM_Job_Equipment_Edit.Rows.Add(drNewRow);
                            strNewIDs += drNewRow["EquipmentUsedID"].ToString() + ",";

                            // Update Labour Count for each job //
                            int intFoundRow = viewJobs.LocateByValue(0, view.Columns["JobID"], intJobID);
                            if (intFoundRow != GridControl.InvalidRowHandle)
                            {
                                viewJobs.SetRowCellValue(intFoundRow, "EquipmentCount", Convert.ToInt32(viewJobs.GetRowCellValue(intFoundRow, "EquipmentCount")) + 1);
                            }
                        }
                        catch (Exception) { }
                    }
                }

            }
            view.EndUpdate();  // UnLock view //
            view.ExpandAllGroups();
            viewJobs.EndUpdate();

            // Highlight any recently added new rows //
            if (!string.IsNullOrWhiteSpace(strNewIDs))
            {
                char[] delimiters = new char[] { ',' };
                string[] strArray = strNewIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["EquipmentUsedID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
            }
        }


        #endregion


        #region Step 8 Page - Materials

        private void btnStep8Previous_Click(object sender, EventArgs e)
        {
            Filter_Equipment_On_Jobs();
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep7;
        }
        private void btnStep8Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl12.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            
            Clear_Filter_Materials_On_Jobs();
            xtraTabControl1.SelectedTabPage = this.xtraTabPageFinish;
        }


        #region GridView11

        private void gridView11_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "MaterialCount")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "MaterialCount")) <= 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridView11_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.NONE;
            SetMenuStatus();
        }

        private void gridView11_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.NONE;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView12

        private void gridControl12_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Materials();
                    }
                    else if ("block add".Equals(e.Button.Tag))
                    {
                        Block_Add_Materials();
                    }
                    else if ("block edit".Equals(e.Button.Tag))
                    {
                        Block_Edit_Materials();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView12_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Materials;
            SetMenuStatus();
        }

        private void gridView12_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Materials;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemButtonEditMaterialName_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06191OMJobMaterialEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06191_OM_Job_Material_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                int intOriginalEquipmentID = (string.IsNullOrEmpty(currentRow.MaterialID.ToString()) ? 0 : Convert.ToInt32(currentRow.MaterialID));

                var fChildForm = new frm_OM_Select_Material();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalID = intOriginalEquipmentID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (fChildForm.intSelectedID == intOriginalEquipmentID) return;
                    currentRow.MaterialID = fChildForm.intSelectedID;
                    currentRow.MaterialName = fChildForm.strSelectedDescription1;
                    currentRow.CostPerUnitExVat = fChildForm.decSelectedCostExVat;
                    currentRow.CostPerUnitVatRate = fChildForm.decSelectedCostVatRate;
                    currentRow.SellPerUnitExVat = fChildForm.decSelectedSellExVat;
                    currentRow.SellPerUnitVatRate = fChildForm.decSelectedSellVatRate;

                    sp06191OMJobMaterialEditBindingSource.EndEdit();
                    GridView view = (GridView)gridControl12.MainView;
                    int intRowHandle = view.FocusedRowHandle;
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        Calculate_Cost3(view, intRowHandle, "", (decimal)0.00);
                        Calculate_Sell3(view, intRowHandle, "", (decimal)0.00);
                    }
                    view.FocusedColumn = colCostUnitsUsed3;  // Move focus to next column so new value is shown in original column //
                }
            }
        }

        private void repositoryItemSpinEdit2DP12_EditValueChanged(object sender, EventArgs e)
        {
            gridView12.PostEditor();
            GridView view = (GridView)gridControl12.MainView;
            if (view.FocusedColumn.FieldName == "CostUnitsUsed")
            {
                SpinEdit se = (SpinEdit)sender;
                decimal decValue = Convert.ToDecimal(se.Value);
                Calculate_Cost3(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            }
            else if (view.FocusedColumn.FieldName == "SellUnitsUsed")
            {
                SpinEdit se = (SpinEdit)sender;
                decimal decValue = Convert.ToDecimal(se.Value);
                Calculate_Sell3(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            }
        }
        private void repositoryItemSpinEditCurrency12_EditValueChanged(object sender, EventArgs e)
        {
            gridView12.PostEditor();
            GridView view = (GridView)gridControl12.MainView;
            if (view.FocusedColumn.FieldName == "CostPerUnitExVat")
            {
                SpinEdit se = (SpinEdit)sender;
                decimal decValue = Convert.ToDecimal(se.Value);
                Calculate_Cost3(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            }
            else if (view.FocusedColumn.FieldName == "SellPerUnitExVat")
            {
                SpinEdit se = (SpinEdit)sender;
                decimal decValue = Convert.ToDecimal(se.Value);
                Calculate_Sell3(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            }
        }
        private void repositoryItemSpinEditPercentage12_EditValueChanged(object sender, EventArgs e)
        {
            gridView12.PostEditor();
            GridView view = (GridView)gridControl12.MainView;
            if (view.FocusedColumn.FieldName == "CostPerUnitVatRate")
            {
                SpinEdit se = (SpinEdit)sender;
                decimal decValue = Convert.ToDecimal(se.Value);
                Calculate_Cost3(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            }
            else if (view.FocusedColumn.FieldName == "SellPerUnitVatRate")
            {
                SpinEdit se = (SpinEdit)sender;
                decimal decValue = Convert.ToDecimal(se.Value);
                Calculate_Sell3(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            }
        }

        private void Calculate_Cost3(GridView view, int intRow, string strCurrentColumn, decimal decValue)
        {
            decimal decTotalValueExVat = (decimal)0.00;
            decimal decTotalValueVat = (decimal)0.00;
            decimal decTotalValue = (decimal)0.00;
            decimal decCostUnitsUsed = (strCurrentColumn == "CostUnitsUsed" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "CostUnitsUsed")));
            decimal decCostPerUnitExVat = (strCurrentColumn == "CostPerUnitExVat" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "CostPerUnitExVat")));
            decimal decCostPerUnitVatRate = (strCurrentColumn == "CostPerUnitVatRate" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "CostPerUnitVatRate")));

            decTotalValueExVat = decCostUnitsUsed * decCostPerUnitExVat;
            decTotalValueVat = (decTotalValueExVat * decCostPerUnitVatRate) / 100;
            decTotalValue = decTotalValueExVat + decTotalValueVat;
            view.SetRowCellValue(intRow, "CostValueExVat", decTotalValueExVat);
            view.SetRowCellValue(intRow, "CostValueVat", decTotalValueVat);
            view.SetRowCellValue(intRow, "CostValueTotal", decTotalValue);
            this.sp06187OMJobEquipmentEditBindingSource.EndEdit();
        }

        private void Calculate_Sell3(GridView view, int intRow, string strCurrentColumn, decimal decValue)
        {
            decimal decTotalValueExVat = (decimal)0.00;
            decimal decTotalValueVat = (decimal)0.00;
            decimal decTotalValue = (decimal)0.00;
            decimal decSellUnitsUsed = (strCurrentColumn == "SellUnitsUsed" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SellUnitsUsed")));
            decimal decSellPerUnitExVat = (strCurrentColumn == "SellPerUnitExVat" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SellPerUnitExVat")));
            decimal decSellPerUnitVatRate = (strCurrentColumn == "SellPerUnitVatRate" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SellPerUnitVatRate")));

            decTotalValueExVat = decSellUnitsUsed * decSellPerUnitExVat;
            decTotalValueVat = (decTotalValueExVat * decSellPerUnitVatRate) / 100;
            decTotalValue = decTotalValueExVat + decTotalValueVat;
            view.SetRowCellValue(intRow, "SellValueExVat", decTotalValueExVat);
            view.SetRowCellValue(intRow, "SellValueVat", decTotalValueVat);
            view.SetRowCellValue(intRow, "SellValueTotal", decTotalValue);
            this.sp06187OMJobEquipmentEditBindingSource.EndEdit();
        }

        #endregion


        private void Filter_Materials_On_Jobs()
        {
            GridView view = (GridView)gridControl12.MainView;
            view.PostEditor();

            string strFilterCondition = "";
            GridView parentView = (GridView)gridControl11.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                strFilterCondition = "[JobID] = 9999999999";  // Don't show anything //
            }
            else
            {
                foreach (int i in intRowHandles)
                {
                    strFilterCondition += (string.IsNullOrWhiteSpace(strFilterCondition) ? "" : " or ");
                    strFilterCondition += "[JobID] = " + parentView.GetRowCellValue(i, "JobID").ToString();
                }
            }
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = strFilterCondition;
            view.MakeRowVisible(-1, true);
            view.ExpandAllGroups();
            view.EndUpdate();
            SetMenuStatus();
        }

        private void Clear_Filter_Materials_On_Jobs()
        {
            GridView view = (GridView)gridControl12.MainView;
            view.PostEditor();

            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
            SetMenuStatus();
        }

        private void Add_Materials()
        {
            GridView viewJobs = (GridView)gridControl11.MainView;
            int[] intRowHandles = viewJobs.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Jobs to add the Materials to before proceeding.", "Add Materials", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intRowHandles.Length > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + " Job(s) selected. If you proceed a new Material record will be created for each of these jobs.\n\nProceed?", "Add Materials", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }

            GridView view = (GridView)gridControl12.MainView;
            view.PostEditor();
            int intMaterialID = 0;
            string strMaterialName = "";
            double dbPostcodeDistance = (double)0.00;
            double dbLatLongDistance = (double)0.00;

            int intJobID = 0;
            int intClientID = 0;
            int intSiteID = 0;
            int intVisitID = 0;
            DateTime dtJobExpectedStartDate = DateTime.MinValue;
            string strClientName = "";
            string strSiteName = "";
            int intVisitNumber = 0;
            string strJobTypeDescription = "";
            string strJobSubTypeDescription = "";
            string strContractDescription = "";
            double dblSiteLocationX = (double)0.00;
            double dblSiteLocationY = (double)0.00;
            string strSitePostcode = "";
            int intJobSubTypeID = 0;
            int intJobTypeID = 0;
            decimal decSelectedCostExVat = (decimal)0.00;
            decimal decSelectedCostVatRate = (decimal)0.00;
            decimal decSelectedSellExVat = (decimal)0.00;
            decimal decSelectedSellVatRate = (decimal)0.00;


            var fChildForm = new frm_OM_Select_Material();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "single";
            if (fChildForm.ShowDialog() != DialogResult.OK) return;
            intMaterialID = fChildForm.intSelectedID;
            strMaterialName = fChildForm.strSelectedDescription1;
            decSelectedCostExVat = fChildForm.decSelectedCostExVat;
            decSelectedCostVatRate = fChildForm.decSelectedCostVatRate;
            decSelectedSellExVat = fChildForm.decSelectedSellExVat;
            decSelectedSellVatRate = fChildForm.decSelectedSellVatRate;

            viewJobs.BeginUpdate();
            view.BeginUpdate();  // Lock view //
            string strNewIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                intJobID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobID"));
                intClientID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "ClientID"));
                intSiteID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "SiteID"));
                intVisitID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitID"));
                dtJobExpectedStartDate = Convert.ToDateTime(viewJobs.GetRowCellValue(intRowHandle, "ExpectedStartDate"));
                strClientName = viewJobs.GetRowCellValue(intRowHandle, "ClientName").ToString();
                strSiteName = viewJobs.GetRowCellValue(intRowHandle, "SiteName").ToString();
                intVisitNumber = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitNumber"));
                strJobTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobTypeDescription").ToString();
                strJobSubTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeDescription").ToString();
                strContractDescription = viewJobs.GetRowCellValue(intRowHandle, "ContractDescription").ToString();
                dblSiteLocationX = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationX"));
                dblSiteLocationY = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationY"));
                strSitePostcode = viewJobs.GetRowCellValue(intRowHandle, "SitePostcode").ToString();
                intJobSubTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                intJobTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobTypeID"));

                try
                {
                    DataRow drNewRow;
                    drNewRow = dataSet_OM_Job.sp06191_OM_Job_Material_Edit.NewRow();
                    drNewRow["strMode"] = "add";
                    drNewRow["strRecordIDs"] = "";
                    drNewRow["MaterialID"] = intMaterialID;
                    drNewRow["JobID"] = intJobID;
                    drNewRow["CostUnitsUsed"] = (decimal)0.00;
                    drNewRow["CostUnitDescriptorID"] = 1;  // Units //
                    drNewRow["SellUnitsUsed"] = (decimal)0.00;
                    drNewRow["SellUnitDescriptorID"] = 1;  // Units //
                    drNewRow["CostPerUnitExVat"] = decSelectedCostExVat;
                    drNewRow["CostPerUnitVatRate"] = (decSelectedCostVatRate <= (decimal)0.00 ? _decDefaultVatRate : decSelectedCostVatRate);
                    drNewRow["SellPerUnitExVat"] = decSelectedSellExVat;
                    drNewRow["SellPerUnitVatRate"] = (decSelectedSellVatRate <= (decimal)0.00 ? _decDefaultVatRate : decSelectedSellVatRate);
                    drNewRow["CostValueExVat"] = (decimal)0.00;
                    drNewRow["CostValueVat"] = (decimal)0.00;
                    drNewRow["CostValueTotal"] = (decimal)0.00;
                    drNewRow["SellValueExVat"] = (decimal)0.00;
                    drNewRow["SellValueVat"] = (decimal)0.00;
                    drNewRow["SellValueTotal"] = (decimal)0.00;
                    drNewRow["ClientID"] = intClientID;
                    drNewRow["SiteID"] = intSiteID;
                    drNewRow["VisitID"] = intVisitID;
                    drNewRow["JobExpectedStartDate"] = dtJobExpectedStartDate;
                    drNewRow["ClientName"] = strClientName;
                    drNewRow["SiteName"] = strSiteName;
                    drNewRow["VisitNumber"] = intVisitNumber;
                    drNewRow["JobTypeDescription"] = strJobTypeDescription;
                    drNewRow["JobSubTypeDescription"] = strJobSubTypeDescription;
                    drNewRow["MaterialName"] = strMaterialName;
                    drNewRow["CostUnitDescriptor"] = "Units";
                    drNewRow["SellUnitDescriptor"] = "Units";
                    drNewRow["PurchaseOrderID"] = 0;
                    drNewRow["ContractDescription"] = strContractDescription;
                    drNewRow["ClientNameContractDescription"] = strClientName + ", Contract: " + strContractDescription;
                    drNewRow["JobTypeJobSubTypeDescription"] = strJobTypeDescription + " - " + strJobSubTypeDescription;
                    drNewRow["PostcodeSiteDistance"] = dbPostcodeDistance;
                    drNewRow["LatLongSiteDistance"] = dbLatLongDistance;
                    drNewRow["SitePostcode"] = strSitePostcode;
                    drNewRow["SiteLocationX"] = dblSiteLocationX;
                    drNewRow["SiteLocationY"] = dblSiteLocationY;
                    drNewRow["JobSubTypeID"] = intJobSubTypeID;
                    drNewRow["JobTypeID"] = intJobTypeID;
                    dataSet_OM_Job.sp06191_OM_Job_Material_Edit.Rows.Add(drNewRow);
                    strNewIDs += drNewRow["MaterialUsedID"].ToString() + ",";
                }
                catch (Exception) { }

                // Update Labour Count for each job //
                int intFoundRow = viewJobs.LocateByValue(0, view.Columns["JobID"], intJobID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    viewJobs.SetRowCellValue(intFoundRow, "MaterialCount", Convert.ToInt32(viewJobs.GetRowCellValue(intFoundRow, "MaterialCount")) + 1);
                }
            }
            view.EndUpdate();  // UnLock view //
            view.ExpandAllGroups();
            viewJobs.EndUpdate();

            // Highlight any recently added new rows //
            if (!string.IsNullOrWhiteSpace(strNewIDs))
            {
                char[] delimiters = new char[] { ',' };
                string[] strArray = strNewIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["MaterialUsedID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
            }
        }

        private void Block_Add_Materials()
        {
            GridView viewJobs = (GridView)gridControl11.MainView;
            int[] intRowHandles = viewJobs.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Jobs to block add the Materials to before proceeding.", "Block Add Material", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intRowHandles.Length > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + " Job(s) selected. If you proceed one or more new Material records will be created for each of these jobs.\n\nProceed?", "Block Add Material", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }

            GridView view = (GridView)gridControl12.MainView;
            view.PostEditor();
            int intMaterialID = 0;
            string strMaterialName = "";
            double dbPostcodeDistance = (double)0.00;
            double dbLatLongDistance = (double)0.00;

            int intJobID = 0;
            int intClientID = 0;
            int intSiteID = 0;
            int intVisitID = 0;
            DateTime dtJobExpectedStartDate = DateTime.MinValue;
            string strClientName = "";
            string strSiteName = "";
            int intVisitNumber = 0;
            string strJobTypeDescription = "";
            string strJobSubTypeDescription = "";
            string strContractDescription = "";
            double dblSiteLocationX = (double)0.00;
            double dblSiteLocationY = (double)0.00;
            string strSitePostcode = "";
            int intJobSubTypeID = 0;
            int intJobTypeID = 0;
            decimal decSelectedCostExVat = (decimal)0.00;
            decimal decSelectedCostVatRate = (decimal)0.00;
            decimal decSelectedSellExVat = (decimal)0.00;
            decimal decSelectedSellVatRate = (decimal)0.00;

            var fChildForm = new frm_OM_Select_Material();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "multiple";
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                viewJobs.BeginUpdate();
                view.BeginUpdate();  // Lock view so we don't see filter switching on and off //
                string strNewIDs = "";
                foreach (int intRowHandle in intRowHandles)
                {
                    intJobID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobID"));
                    intClientID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "ClientID"));
                    intSiteID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "SiteID"));
                    intVisitID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitID"));
                    dtJobExpectedStartDate = Convert.ToDateTime(viewJobs.GetRowCellValue(intRowHandle, "ExpectedStartDate"));
                    strClientName = viewJobs.GetRowCellValue(intRowHandle, "ClientName").ToString();
                    strSiteName = viewJobs.GetRowCellValue(intRowHandle, "SiteName").ToString();
                    intVisitNumber = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitNumber"));
                    strJobTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobTypeDescription").ToString();
                    strJobSubTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeDescription").ToString();
                    strContractDescription = viewJobs.GetRowCellValue(intRowHandle, "ContractDescription").ToString();
                    dblSiteLocationX = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationX"));
                    dblSiteLocationY = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationY"));
                    strSitePostcode = viewJobs.GetRowCellValue(intRowHandle, "SitePostcode").ToString();
                    intJobSubTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                    intJobTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobTypeID"));

                    GridView viewEquipment = (GridView)fChildForm.gridControl1.MainView;
                    for (int i = 0; i < viewEquipment.DataRowCount; i++)
                    {
                        if (Convert.ToBoolean(viewEquipment.GetRowCellValue(i, "CheckMarkSelection")))
                        {
                            intMaterialID = Convert.ToInt32(viewEquipment.GetRowCellValue(i, "MaterialID"));
                            strMaterialName = viewEquipment.GetRowCellValue(i, "MaterialDescription").ToString();
                            decSelectedCostExVat = Convert.ToDecimal(viewEquipment.GetRowCellValue(i, "CostPerUnitExVat"));
                            decSelectedCostVatRate = Convert.ToDecimal(viewEquipment.GetRowCellValue(i, "CostPerUnitVatRate"));
                            decSelectedSellExVat = Convert.ToDecimal(viewEquipment.GetRowCellValue(i, "SellPerUnitExVat"));
                            decSelectedSellVatRate = Convert.ToDecimal(viewEquipment.GetRowCellValue(i, "SellPerUnitVatRate"));

                            try
                            {
                                DataRow drNewRow;
                                drNewRow = dataSet_OM_Job.sp06191_OM_Job_Material_Edit.NewRow();
                                drNewRow["strMode"] = "add";
                                drNewRow["strRecordIDs"] = "";
                                drNewRow["MaterialID"] = intMaterialID;
                                drNewRow["JobID"] = intJobID;
                                drNewRow["CostUnitsUsed"] = (decimal)0.00;
                                drNewRow["CostUnitDescriptorID"] = 1;  // Units //
                                drNewRow["SellUnitsUsed"] = (decimal)0.00;
                                drNewRow["SellUnitDescriptorID"] = 1;  // Units //
                                drNewRow["CostPerUnitExVat"] = decSelectedCostExVat;
                                drNewRow["CostPerUnitVatRate"] = (decSelectedCostVatRate <= (decimal)0.00 ? _decDefaultVatRate : decSelectedCostVatRate);
                                drNewRow["SellPerUnitExVat"] = decSelectedSellExVat;
                                drNewRow["SellPerUnitVatRate"] = (decSelectedSellVatRate <= (decimal)0.00 ? _decDefaultVatRate : decSelectedSellVatRate);
                                drNewRow["CostValueExVat"] = (decimal)0.00;
                                drNewRow["CostValueVat"] = (decimal)0.00;
                                drNewRow["CostValueTotal"] = (decimal)0.00;
                                drNewRow["SellValueExVat"] = (decimal)0.00;
                                drNewRow["SellValueVat"] = (decimal)0.00;
                                drNewRow["SellValueTotal"] = (decimal)0.00;
                                drNewRow["ClientID"] = intClientID;
                                drNewRow["SiteID"] = intSiteID;
                                drNewRow["VisitID"] = intVisitID;
                                drNewRow["JobExpectedStartDate"] = dtJobExpectedStartDate;
                                drNewRow["ClientName"] = strClientName;
                                drNewRow["SiteName"] = strSiteName;
                                drNewRow["VisitNumber"] = intVisitNumber;
                                drNewRow["JobTypeDescription"] = strJobTypeDescription;
                                drNewRow["JobSubTypeDescription"] = strJobSubTypeDescription;
                                drNewRow["MaterialName"] = strMaterialName;
                                drNewRow["CostUnitDescriptor"] = "Units";
                                drNewRow["SellUnitDescriptor"] = "Units";
                                drNewRow["PurchaseOrderID"] = 0;
                                drNewRow["ContractDescription"] = strContractDescription;
                                drNewRow["ClientNameContractDescription"] = strClientName + ", Contract: " + strContractDescription;
                                drNewRow["JobTypeJobSubTypeDescription"] = strJobTypeDescription + " - " + strJobSubTypeDescription;
                                drNewRow["PostcodeSiteDistance"] = dbPostcodeDistance;
                                drNewRow["LatLongSiteDistance"] = dbLatLongDistance;
                                drNewRow["SitePostcode"] = strSitePostcode;
                                drNewRow["SiteLocationX"] = dblSiteLocationX;
                                drNewRow["SiteLocationY"] = dblSiteLocationY;
                                drNewRow["JobSubTypeID"] = intJobSubTypeID;
                                drNewRow["JobTypeID"] = intJobTypeID;
                                dataSet_OM_Job.sp06191_OM_Job_Material_Edit.Rows.Add(drNewRow);
                                strNewIDs += drNewRow["MaterialUsedID"].ToString() + ",";

                                // Update Labour Count for each job //
                                int intFoundRow = viewJobs.LocateByValue(0, view.Columns["JobID"], intJobID);
                                if (intFoundRow != GridControl.InvalidRowHandle)
                                {
                                    viewJobs.SetRowCellValue(intFoundRow, "MaterialCount", Convert.ToInt32(viewJobs.GetRowCellValue(intFoundRow, "MaterialCount")) + 1);
                                }
                            }
                            catch (Exception) { }
                        }
                    }
                }
                view.EndUpdate();  // UnLock view //
                view.ExpandAllGroups();
                viewJobs.EndUpdate();

                // Highlight any recently added new rows //
                if (!string.IsNullOrWhiteSpace(strNewIDs))
                {
                    char[] delimiters = new char[] { ',' };
                    string[] strArray = strNewIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    int intID = 0;
                    int intRowHandle = 0;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["MaterialUsedID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                }
            }
        }

        private void Block_Edit_Materials()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl12.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Job Material records to block edit then try again.", "Block Edit Job Materials", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Job_Wizard_Block_Edit_Material_Info();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                view.BeginUpdate();
                view.BeginSort();
                using (var CalcPostcodeDistance = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                {
                    CalcPostcodeDistance.ChangeConnectionString(strConnectionString);
                    foreach (int intRowHandle in intRowHandles)
                    {
                        if (fChildForm.intMaterialID != null)
                        {
                            view.SetRowCellValue(intRowHandle, "MaterialID", fChildForm.intMaterialID);
                        }
                        if (fChildForm.strMaterialDescription != null) view.SetRowCellValue(intRowHandle, "MaterialName", fChildForm.strMaterialDescription);

                        if (fChildForm.decCostUnitsUsed != null) view.SetRowCellValue(intRowHandle, "CostUnitsUsed", fChildForm.decCostUnitsUsed);
                        if (fChildForm.intCostUnitDescriptorID != null) view.SetRowCellValue(intRowHandle, "CostUnitDescriptorID", fChildForm.intCostUnitDescriptorID);
                        if (fChildForm.decCostPerUnitExVat != null) view.SetRowCellValue(intRowHandle, "CostPerUnitExVat", fChildForm.decCostPerUnitExVat);
                        if (fChildForm.decCostPerUnitVatRate != null) view.SetRowCellValue(intRowHandle, "CostPerUnitVatRate", fChildForm.decCostPerUnitVatRate);

                        if (fChildForm.decSellUnitsUsed != null) view.SetRowCellValue(intRowHandle, "SellUnitsUsed", fChildForm.decSellUnitsUsed);
                        if (fChildForm.intSellUnitDescriptorID != null) view.SetRowCellValue(intRowHandle, "SellUnitDescriptorID", fChildForm.intSellUnitDescriptorID);
                        if (fChildForm.decSellPerUnitExVat != null) view.SetRowCellValue(intRowHandle, "SellPerUnitExVat", fChildForm.decSellPerUnitExVat);
                        if (fChildForm.decSellPerUnitVatRate != null) view.SetRowCellValue(intRowHandle, "SellPerUnitVatRate", fChildForm.decSellPerUnitVatRate);

                        if (fChildForm.strRemarks != null) view.SetRowCellValue(intRowHandle, "Remarks", fChildForm.strRemarks);

                        this.sp06191OMJobMaterialEditBindingSource.EndEdit();
                        Calculate_Cost3(view, intRowHandle, "", (decimal)0.00);
                        Calculate_Sell3(view, intRowHandle, "", (decimal)0.00);
                    }
                }
                view.EndSort();
                view.EndUpdate();
            }
        }


        private void bbiAddMaterialFromContract_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int intRecordCount = 0;
            GridView viewJobs = (GridView)gridControl11.MainView;
            viewJobs.PostEditor();
            int[] intRowHandles;
            intRowHandles = viewJobs.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Jobs to add Materials to from Site Contracts then try again.", "Add Materials from Site Contracts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedSiteContractIDs = ",";
            foreach (int intRowHandle in intRowHandles)
            {
                if (!strSelectedSiteContractIDs.Contains("," + viewJobs.GetRowCellValue(intRowHandle, "SiteContractID").ToString() + ",")) strSelectedSiteContractIDs += viewJobs.GetRowCellValue(intRowHandle, "SiteContractID").ToString() + ",";
            }
            strSelectedSiteContractIDs.Remove(0, 1); // Remove preceeding comma //
            var fChildForm = new frm_OM_Select_Site_Contract_Material();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "multiple";
            fChildForm.strPassedInSiteContractIDs = strSelectedSiteContractIDs;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;

            GridView view = (GridView)gridControl12.MainView;
            view.PostEditor();
            int intMaterialID = 0;
            string strMaterialName = "";

            int intJobID = 0;
            int intClientID = 0;
            int intSiteID = 0;
            int intVisitID = 0;
            DateTime dtJobExpectedStartDate = DateTime.MinValue;
            string strClientName = "";
            string strSiteName = "";
            int intVisitNumber = 0;
            string strJobTypeDescription = "";
            string strJobSubTypeDescription = "";
            string strContractDescription = "";
            double dblSiteLocationX = (double)0.00;
            double dblSiteLocationY = (double)0.00;
            string strSitePostcode = "";
            int intJobSubTypeID = 0;
            int intJobTypeID = 0;

            int intSiteContractID = 0;

            viewJobs.BeginUpdate();
            view.BeginUpdate();
            string strNewIDs = "";

            foreach (int intRowHandle in intRowHandles)
            {
                intJobID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobID"));
                intClientID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "ClientID"));
                intSiteID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "SiteID"));
                intVisitID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitID"));
                dtJobExpectedStartDate = Convert.ToDateTime(viewJobs.GetRowCellValue(intRowHandle, "ExpectedStartDate"));
                strClientName = viewJobs.GetRowCellValue(intRowHandle, "ClientName").ToString();
                strSiteName = viewJobs.GetRowCellValue(intRowHandle, "SiteName").ToString();
                intVisitNumber = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "VisitNumber"));
                strJobTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobTypeDescription").ToString();
                strJobSubTypeDescription = viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeDescription").ToString();
                strContractDescription = viewJobs.GetRowCellValue(intRowHandle, "ContractDescription").ToString();
                dblSiteLocationX = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationX"));
                dblSiteLocationY = Convert.ToDouble(viewJobs.GetRowCellValue(intRowHandle, "LocationY"));
                strSitePostcode = viewJobs.GetRowCellValue(intRowHandle, "SitePostcode").ToString();
                intSiteContractID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "SiteContractID"));
                intJobSubTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                intJobTypeID = Convert.ToInt32(viewJobs.GetRowCellValue(intRowHandle, "JobTypeID"));

                GridView viewContractors = (GridView)fChildForm.gridControl1.MainView;
                for (int i = 0; i < viewContractors.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(viewContractors.GetRowCellValue(i, "CheckMarkSelection")) && Convert.ToInt32(viewContractors.GetRowCellValue(i, "SiteContractID")) == intSiteContractID)
                    {
                        intMaterialID = Convert.ToInt32(viewContractors.GetRowCellValue(i, "MaterialID"));
                        strMaterialName = viewContractors.GetRowCellValue(i, "MaterialName").ToString();

                        try
                        {
                            DataRow drNewRow;
                            drNewRow = dataSet_OM_Job.sp06191_OM_Job_Material_Edit.NewRow();
                            drNewRow["strMode"] = "add";
                            drNewRow["strRecordIDs"] = "";
                            drNewRow["MaterialID"] = intMaterialID;
                            drNewRow["JobID"] = intJobID;
                            drNewRow["CostUnitsUsed"] = (decimal)0.00;
                            drNewRow["CostUnitDescriptorID"] = Convert.ToInt32(viewContractors.GetRowCellValue(i, "CostUnitDescriptorID"));
                            drNewRow["SellUnitsUsed"] = (decimal)0.00;
                            drNewRow["SellUnitDescriptorID"] = Convert.ToInt32(viewContractors.GetRowCellValue(i, "SellUnitDescriptorID"));
                            drNewRow["CostPerUnitExVat"] = Convert.ToDecimal(viewContractors.GetRowCellValue(i, "CostPerUnitExVat"));
                            drNewRow["CostPerUnitVatRate"] = Convert.ToDecimal(viewContractors.GetRowCellValue(i, "CostPerUnitVatRate"));
                            drNewRow["SellPerUnitExVat"] = Convert.ToDecimal(viewContractors.GetRowCellValue(i, "SellPerUnitExVat"));
                            drNewRow["SellPerUnitVatRate"] = Convert.ToDecimal(viewContractors.GetRowCellValue(i, "SellPerUnitVatRate"));
                            drNewRow["CostValueExVat"] = (decimal)0.00;
                            drNewRow["CostValueVat"] = (decimal)0.00;
                            drNewRow["CostValueTotal"] = (decimal)0.00;
                            drNewRow["SellValueExVat"] = (decimal)0.00;
                            drNewRow["SellValueVat"] = (decimal)0.00;
                            drNewRow["SellValueTotal"] = (decimal)0.00;
                            drNewRow["ClientID"] = intClientID;
                            drNewRow["SiteID"] = intSiteID;
                            drNewRow["VisitID"] = intVisitID;
                            drNewRow["JobExpectedStartDate"] = dtJobExpectedStartDate;
                            drNewRow["ClientName"] = strClientName;
                            drNewRow["SiteName"] = strSiteName;
                            drNewRow["VisitNumber"] = intVisitNumber;
                            drNewRow["JobTypeDescription"] = strJobTypeDescription;
                            drNewRow["JobSubTypeDescription"] = strJobSubTypeDescription;
                            drNewRow["MaterialName"] = strMaterialName;
                            drNewRow["CostUnitDescriptor"] = "Hours";
                            drNewRow["SellUnitDescriptor"] = "Hours";
                            drNewRow["PurchaseOrderID"] = 0;
                            drNewRow["ContractDescription"] = strContractDescription;
                            drNewRow["ClientNameContractDescription"] = strClientName + ", Contract: " + strContractDescription;
                            drNewRow["JobTypeJobSubTypeDescription"] = strJobTypeDescription + " - " + strJobSubTypeDescription;
                            drNewRow["SitePostcode"] = strSitePostcode;
                            drNewRow["SiteLocationX"] = dblSiteLocationX;
                            drNewRow["SiteLocationY"] = dblSiteLocationY;
                            drNewRow["JobSubTypeID"] = intJobSubTypeID;
                            drNewRow["JobTypeID"] = intJobTypeID;
                            dataSet_OM_Job.sp06191_OM_Job_Material_Edit.Rows.Add(drNewRow);
                            strNewIDs += drNewRow["MaterialUsedID"].ToString() + ",";

                            // Update Material Count for each job //
                            int intFoundRow = viewJobs.LocateByValue(0, view.Columns["JobID"], intJobID);
                            if (intFoundRow != GridControl.InvalidRowHandle)
                            {
                                viewJobs.SetRowCellValue(intFoundRow, "MaterialCount", Convert.ToInt32(viewJobs.GetRowCellValue(intFoundRow, "MaterialCount")) + 1);
                            }
                        }
                        catch (Exception) { }
                    }
                }

            }
            view.EndUpdate();  // UnLock view //
            view.ExpandAllGroups();
            viewJobs.EndUpdate();

            // Highlight any recently added new rows //
            if (!string.IsNullOrWhiteSpace(strNewIDs))
            {
                char[] delimiters = new char[] { ',' };
                string[] strArray = strNewIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["MaterialUsedID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
            }
        }



        #endregion


        #region Finish Page

        private void btnFinishPrevious_Click(object sender, EventArgs e)
        {
            Filter_Materials_On_Jobs();
            if (checkEditSkipEquipmentAndMaterials.Checked)
            {
                xtraTabControl1.SelectedTabPage = this.xtraTabPageStep6;
            }
            else
            {
                xtraTabControl1.SelectedTabPage = this.xtraTabPageStep8;
            }
        }
        private void btnFinish_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl6.MainView;  // Jobs  //
            view.PostEditor();
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            int intJobCount = view.DataRowCount;

            view = (GridView)gridControl8.MainView;  // Linked Labour //
            view.PostEditor();
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  

            view = (GridView)gridControl10.MainView;  // Linked Equipment //
            view.PostEditor();
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  

            view = (GridView)gridControl12.MainView;  // Linked Materials //
            view.PostEditor();
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  

            
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Saving Changes...");

            // Save Jobs //           
            view = (GridView)gridControl6.MainView; 
            view.PostEditor();
            view.BeginUpdate();
            foreach (DataRow dr in dataSet_OM_Job.sp06174_OM_Job_Edit.Rows)
            {
                dr["DummyJobID"] = dr["JobID"];  // Copy VS generated IDs to dummy column so we can find related children later as the save will overwrite the VS generated auto-incrementing keys //
             }
            sp06174OMJobEditBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            view.EndUpdate();
            try 
            {
                sp06174_OM_Job_EditTableAdapter.Update(dataSet_OM_Job);  // Insert, Update and Delete queries defined in Table Adapter //
                foreach (DataRow dr in dataSet_OM_Job.sp06174_OM_Job_Edit.Rows)
                {
                    dr["strMode"] = "edit";
                }
                sp06145OMVisitEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the Job changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                if (frmCaller != null) frmCaller.SetTenderJobCount(i_intPassedInTenderID, intJobCount);  // Update Edit Tender screen //
            }
            catch (Exception) { }


            // Pick up the new IDs so they can be passed back to the parent manager screen so the new records can be highlighted //
            string strNewIDs = "";
            int intJobID = 0;
            int intDummyJobID = 0;
            string strJobIDs = "";
            foreach (DataRow dr in dataSet_OM_Job.sp06174_OM_Job_Edit.Rows)
            {
                intJobID = Convert.ToInt32(dr["JobID"]);
                strNewIDs += intJobID.ToString() + ";";
                strJobIDs += intJobID.ToString() + ",";
                intDummyJobID = Convert.ToInt32(dr["DummyJobID"]);

                // Process Linked Labour and update their parent Job IDs //
                foreach (DataRow drChild in dataSet_OM_Job.sp06182_OM_Job_Labour_Edit.Rows)
                {
                    if (Convert.ToInt32(drChild["JobID"]) == intDummyJobID) drChild["JobID"] = intJobID;  // Overwrite with new value from saved job //
                }

                // Process Linked Equipment and update their parent Job IDs //
                foreach (DataRow drChild in dataSet_OM_Job.sp06187_OM_Job_Equipment_Edit.Rows)
                {
                    if (Convert.ToInt32(drChild["JobID"]) == intDummyJobID) drChild["JobID"] = intJobID;  // Overwrite with new value from saved job //
                }

                // Process Linked Materials and update their parent Job IDs //
                foreach (DataRow drChild in dataSet_OM_Job.sp06191_OM_Job_Material_Edit.Rows)
                {
                    if (Convert.ToInt32(drChild["JobID"]) == intDummyJobID) drChild["JobID"] = intJobID;  // Overwrite with new value from saved job //
                }
            }
            sp06182OMJobLabourEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
            sp06187OMJobEquipmentEditBindingSource.EndEdit();
            sp06191OMJobMaterialEditBindingSource.EndEdit();

            // Save Linked Labour //
            try
            {
                sp06182_OM_Job_Labour_EditTableAdapter.Update(dataSet_OM_Job);  // Insert, Update and Delete queries defined in Table Adapter //
                foreach (DataRow drChild in dataSet_OM_Job.sp06182_OM_Job_Labour_Edit.Rows)
                {
                    drChild["strMode"] = "edit";
                }
                sp06182OMJobLabourEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the linked Job Labour changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Save Linked Equipment //
            try
            {
                sp06187_OM_Job_Equipment_EditTableAdapter.Update(dataSet_OM_Job);  // Insert, Update and Delete queries defined in Table Adapter //
                foreach (DataRow drChild in dataSet_OM_Job.sp06187_OM_Job_Equipment_Edit.Rows)
                {
                    drChild["strMode"] = "edit";
                }
                sp06187OMJobEquipmentEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the linked Job Equipment changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Save Linked Materials //
            try
            {
                sp06191_OM_Job_Material_EditTableAdapter.Update(dataSet_OM_Job);  // Insert, Update and Delete queries defined in Table Adapter //
                foreach (DataRow drChild in dataSet_OM_Job.sp06191_OM_Job_Material_Edit.Rows)
                {
                    drChild["strMode"] = "edit";
                }
                sp06191OMJobMaterialEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the linked Job Material changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_OM_Job_Manager")
                    {
                        var fParentForm = (frm_OM_Job_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Jobs, strNewIDs);
                    }
                }
            }
            this.dataSet_OM_Job.AcceptChanges();

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }

            string strVisitIDs = "";
            string strSiteContractIDs = ",";
            string strTempSiteContractID = "";
            string strClientContractIDs = ",";
            string strTempClientContractID = "";

            GridView viewVisits = (GridView)gridControl3.MainView;
            int intCount = selection3.SelectedCount;

            int intProcessedCount = 0;
            for (int i = 0; i < viewVisits.DataRowCount; i++)
            {
                if (Convert.ToInt32(viewVisits.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                {
                    strVisitIDs += viewVisits.GetRowCellValue(i, "VisitID").ToString() + ",";

                    strTempSiteContractID = viewVisits.GetRowCellValue(i, "SiteContractID").ToString() + ",";
                    if (!strSiteContractIDs.Contains("," + strTempSiteContractID)) strSiteContractIDs += strTempSiteContractID;

                    strTempClientContractID = viewVisits.GetRowCellValue(i, "ClientContractID").ToString() + ",";
                    if (!strClientContractIDs.Contains("," + strTempClientContractID)) strClientContractIDs += strTempClientContractID;

                    intProcessedCount++;
                    if (intProcessedCount >= intCount) break;
                }
            }
            strSiteContractIDs = strSiteContractIDs.Remove(0, 1);  // Remove preceeding comma //
            strClientContractIDs = strClientContractIDs.Remove(0, 1);  // Remove preceeding comma //

            // Tell Visit Manager to partially reload the parent visits to pick up updated linked job count //
            if (this.ParentForm != null)
            {
                if (Application.OpenForms[strCaller] != null)
                {
                    if (strCaller == "frm_OM_Visit_Manager")
                    {
                        (Application.OpenForms[strCaller] as frm_OM_Visit_Manager).UpdateFormRefreshStatus(2, Utils.enmFocusedGrid.Jobs, strNewIDs.Replace(',', ';'));
                        (Application.OpenForms[strCaller] as frm_OM_Visit_Manager).UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Visit, strVisitIDs.Replace(',', ';'));
                    }
                    else if (strCaller == "frm_OM_Site_Contract_Manager") (Application.OpenForms[strCaller] as frm_OM_Site_Contract_Manager).UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Contract, "");
                    else if (strCaller == "frm_OM_Tender_Manager") (Application.OpenForms[strCaller] as frm_OM_Tender_Manager).UpdateFormRefreshStatus(2, Utils.enmFocusedGrid.Visit, "");  // No ids passed back as there is no job grid on this screen //
                }
            }

            if (checkEdit2.Checked || checkEdit4.Checked)  // Re-Open Job Wizard //
            {
                var fChildForm = new frm_OM_Job_Wizard();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = "";
                fChildForm.strFormMode = "add";
                fChildForm.strCaller = this.strCaller;
                fChildForm.intRecordCount = 0;
                fChildForm.FormPermissions = this.FormPermissions;
                fChildForm.i_str_PassedInClientContractIDs = (checkEdit4.Checked ? strClientContractIDs : "");
                fChildForm.i_str_PassedInSiteContractIDs = (checkEdit4.Checked ? strSiteContractIDs : "");
                fChildForm.i_str_PassedInVisitIDs = (checkEdit4.Checked ? strVisitIDs : "");
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager2 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager2;
                fChildForm.splashScreenManager.ShowWaitForm();
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }
            else if (checkEdit3.Checked)  // Re-open Visit Wizard to create Visit //
            {
                var fChildForm = new frm_OM_Visit_Wizard();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = "";
                fChildForm.strFormMode = "add";
                fChildForm.strCaller = this.strCaller;
                fChildForm.intRecordCount = 0;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager2 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager2;
                fChildForm.splashScreenManager.ShowWaitForm();
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }

            this.Close();
        }

        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }


        private void Add_Record()
        {
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    {
                        if (strFormMode == "view") return;
                        Add_Job();
                    }
                    break;
                case Utils.enmFocusedGrid.Labour:
                    {
                        if (strFormMode == "view") return;
                        Add_Labour(0);
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        if (strFormMode == "view") return;
                        Add_Equipment();
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        if (strFormMode == "view") return;
                        Add_Materials();
                    }
                    break;
            }
        }
        
        private void Block_Add_Record()
        {
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    {
                        if (strFormMode == "view") return;
                        Block_Add_Job();
                    }
                    break;
                case Utils.enmFocusedGrid.Labour:
                    {
                        if (strFormMode == "view") return;
                        Block_Add_Labour();
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        if (strFormMode == "view") return;
                        Block_Add_Equipment();
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        if (strFormMode == "view") return;
                        Block_Add_Materials();
                    }
                    break;
            }
        }
        
        private void Block_Edit_Record()
        {
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    {
                        Block_Edit_Jobs();
                    }
                    break;
                case Utils.enmFocusedGrid.Labour:
                    {
                        Block_Edit_Labour();
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        Block_Edit_Equipment();
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        Block_Edit_Materials();
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    {
                        if (strFormMode == "view") return;
                        int[] intRowHandles;
                        int intCount = 0;
                        GridView view = null;
                        string strMessage = "";

                        view = (GridView)gridControl6.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more new jobs to delete by clicking on them then try again.", "Delete New Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 new Job" : Convert.ToString(intRowHandles.Length) + " new Jobs") + " selected for deleting!\n\nProceed?";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete New Jobs", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                int intJobID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobID"));
                                view.DeleteRow(intRowHandles[i]);
                            }
                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            SetMenuStatus();
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.Labour:
                    {
                        if (strFormMode == "view") return;
                        int[] intRowHandles;
                        int intCount = 0;
                        string strMessage = "";


                        GridView view = (GridView)gridControl8.MainView;
                        GridView viewJobs = (GridView)gridControl7.MainView;
                        int intFoundRow = 0;
                        int intJobID = 0;

                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more new labour records to delete by clicking on them then try again.", "Delete New Labour Records", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 new Labour record" : Convert.ToString(intRowHandles.Length) + " new Labour records") + " selected for deleting!\n\nProceed?";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete New Labour Records", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                intJobID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobID"));
                                try
                                {
                                    view.DeleteRow(intRowHandles[i]);

                                    // Update Labour Count for each job //
                                    intFoundRow = viewJobs.LocateByValue(0, view.Columns["JobID"], intJobID);
                                    if (intFoundRow != GridControl.InvalidRowHandle)
                                    {
                                        viewJobs.SetRowCellValue(intFoundRow, "LabourCount", Convert.ToInt32(viewJobs.GetRowCellValue(intFoundRow, "LabourCount")) - 1);
                                    }
                                }
                                catch (Exception) { }
                            }
                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            SetMenuStatus();
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        if (strFormMode == "view") return;
                        int[] intRowHandles;
                        int intCount = 0;
                        string strMessage = "";


                        GridView view = (GridView)gridControl10.MainView;
                        GridView viewJobs = (GridView)gridControl9.MainView;
                        int intFoundRow = 0;
                        int intJobID = 0;

                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more new Equipment records to delete by clicking on them then try again.", "Delete New Equipment Records", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 new Equipment record" : Convert.ToString(intRowHandles.Length) + " new Equipment records") + " selected for deleting!\n\nProceed?";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete New Labour Records", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                intJobID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobID"));
                                try
                                {
                                    view.DeleteRow(intRowHandles[i]);

                                    // Update Equipment Count for each job //
                                    intFoundRow = viewJobs.LocateByValue(0, view.Columns["JobID"], intJobID);
                                    if (intFoundRow != GridControl.InvalidRowHandle)
                                    {
                                        viewJobs.SetRowCellValue(intFoundRow, "EquipmentCount", Convert.ToInt32(viewJobs.GetRowCellValue(intFoundRow, "EquipmentCount")) - 1);
                                    }
                                }
                                catch (Exception) { }
                            }
                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            SetMenuStatus();
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        if (strFormMode == "view") return;
                        int[] intRowHandles;
                        int intCount = 0;
                        string strMessage = "";


                        GridView view = (GridView)gridControl12.MainView;
                        GridView viewJobs = (GridView)gridControl11.MainView;
                        int intFoundRow = 0;
                        int intJobID = 0;

                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more new Material records to delete by clicking on them then try again.", "Delete New Material Records", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 new Material record" : Convert.ToString(intRowHandles.Length) + " new Material records") + " selected for deleting!\n\nProceed?";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete New Material Records", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                intJobID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobID"));
                                try
                                {
                                    view.DeleteRow(intRowHandles[i]);

                                    // Update Material Count for each job //
                                    intFoundRow = viewJobs.LocateByValue(0, view.Columns["JobID"], intJobID);
                                    if (intFoundRow != GridControl.InvalidRowHandle)
                                    {
                                        viewJobs.SetRowCellValue(intFoundRow, "MaterialCount", Convert.ToInt32(viewJobs.GetRowCellValue(intFoundRow, "MaterialCount")) - 1);
                                    }
                                }
                                catch (Exception) { }
                            }
                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            SetMenuStatus();
                        }
                    }
                    break;
                default:
                    break;
            }
        }


        private void bbiTick_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        view = (GridView)gridControl1.MainView;
                    }
                    break;
                case Utils.enmFocusedGrid.Sites:
                    {
                        view = (GridView)gridControl2.MainView;
                    }
                    break;
                case Utils.enmFocusedGrid.Visit:
                    {
                        view = (GridView)gridControl3.MainView;
                    }
                    break;
                case Utils.enmFocusedGrid.JobMaster:
                    {
                        view = (GridView)gridControl4.MainView;
                    }
                    break;
                default:
                    return;
            }

            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                view.BeginSelection();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);
                }
                view.EndSelection();
                view.EndUpdate();
            }
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        Set_Selected_Client_Count_Label();
                    }
                    break;
                case Utils.enmFocusedGrid.Sites:
                    {
                        Set_Selected_Site_Count_Label();
                    }
                    break;
                case Utils.enmFocusedGrid.Visit:
                    {
                        Set_Selected_Visit_Count_Label();
                    }
                    break;
                case Utils.enmFocusedGrid.JobMaster:
                    {
                        Set_Selected_Job_Count_Label();
                    }
                    break;
                default:
                    return;
            }
        }

        private void bbiUntick_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        view = (GridView)gridControl1.MainView;
                    }
                    break;
                case Utils.enmFocusedGrid.Sites:
                    {
                        view = (GridView)gridControl2.MainView;
                    }
                    break;
                case Utils.enmFocusedGrid.Visit:
                    {
                        view = (GridView)gridControl3.MainView;
                    }
                    break;
                case Utils.enmFocusedGrid.JobMaster:
                    {
                        view = (GridView)gridControl4.MainView;
                    }
                    break;
                default:
                    return;
            }
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                view.BeginSelection();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 0);
                }
                view.EndSelection();
                view.EndUpdate();
            }
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        Set_Selected_Client_Count_Label();
                    }
                    break;
                case Utils.enmFocusedGrid.Sites:
                    {
                        Set_Selected_Site_Count_Label();
                    }
                    break;
                case Utils.enmFocusedGrid.Visit:
                    {
                        Set_Selected_Visit_Count_Label();
                    }
                    break;
                case Utils.enmFocusedGrid.JobMaster:
                    {
                        Set_Selected_Job_Count_Label();
                    }
                    break;
                default:
                    return;
            }
        }


 





    }
}

﻿namespace WoodPlan5
{
    partial class frm_OM_Client_Contract_Person_Responsibility_Select_Person_Type
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Client_Contract_Person_Responsibility_Select_Person_Type));
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonStaff = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCustomer = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(352, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 129);
            this.barDockControlBottom.Size = new System.Drawing.Size(352, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 103);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(352, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 103);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.CausesValidation = false;
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(5, 33);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.PictureAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Size = new System.Drawing.Size(34, 34);
            this.pictureEdit1.TabIndex = 4;
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Location = new System.Drawing.Point(45, 44);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(298, 13);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "Select the <b>Type</b> of Person to add by clicking a <b>button</b> below.";
            // 
            // simpleButtonStaff
            // 
            this.simpleButtonStaff.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonStaff.Image")));
            this.simpleButtonStaff.Location = new System.Drawing.Point(12, 79);
            this.simpleButtonStaff.Name = "simpleButtonStaff";
            this.simpleButtonStaff.Size = new System.Drawing.Size(97, 38);
            this.simpleButtonStaff.TabIndex = 6;
            this.simpleButtonStaff.Text = "Staff";
            this.simpleButtonStaff.Click += new System.EventHandler(this.simpleButtonStaff_Click);
            // 
            // simpleButtonCustomer
            // 
            this.simpleButtonCustomer.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonCustomer.Image")));
            this.simpleButtonCustomer.Location = new System.Drawing.Point(115, 79);
            this.simpleButtonCustomer.Name = "simpleButtonCustomer";
            this.simpleButtonCustomer.Size = new System.Drawing.Size(97, 38);
            this.simpleButtonCustomer.TabIndex = 7;
            this.simpleButtonCustomer.Text = "Customer";
            this.simpleButtonCustomer.Click += new System.EventHandler(this.simpleButtonCustomer_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonCancel.Image")));
            this.simpleButtonCancel.Location = new System.Drawing.Point(243, 79);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(97, 38);
            this.simpleButtonCancel.TabIndex = 8;
            this.simpleButtonCancel.Text = "Cancel";
            this.simpleButtonCancel.Click += new System.EventHandler(this.simpleButtonCancel_Click);
            // 
            // frm_OM_Client_Contract_Person_Responsibility_Select_Person_Type
            // 
            this.AcceptButton = this.simpleButtonStaff;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(352, 129);
            this.ControlBox = false;
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonCustomer);
            this.Controls.Add(this.simpleButtonStaff);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.pictureEdit1);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OM_Client_Contract_Person_Responsibility_Select_Person_Type";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Person Responsibility";
            this.Load += new System.EventHandler(this.frm_OM_Client_Contract_Person_Responsibility_Select_Person_Type_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.pictureEdit1, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.simpleButtonStaff, 0);
            this.Controls.SetChildIndex(this.simpleButtonCustomer, 0);
            this.Controls.SetChildIndex(this.simpleButtonCancel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonStaff;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCustomer;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
    }
}

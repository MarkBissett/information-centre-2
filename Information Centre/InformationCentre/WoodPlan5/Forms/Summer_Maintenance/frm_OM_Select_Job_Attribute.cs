using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Select_Job_Attribute : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;
        bool internalRowFocusing;

        public int intOriginalClientContractID = 0;
        public int intOriginalSiteContractID = 0;
        public int intOriginalVisitID = 0;
        public int intOriginalJobID = 0;
        public int intOriginalJobAttributeID = 0;

        public int intSelectedClientID = 0;
        public int intSelectedClientContractID = 0;
        public int intSelectedSiteID = 0;
        public int intSelectedSiteContractID = 0;
        public int intSelectedVisitID = 0;
        public int intSelectedVisitNumber = 0;
        public int intSelectedJobID = 0;
        public int intSelectedJobTypeID = 0;
        public int intSelectedJobSubTypeID = 0;
        public int intSelectedJobAttributeID = 0;

        public string strSelectedClientName = "";
        public string strSelectedContractDescription = "";
        public string strSelectedSiteName = "";
        public string strSelectedJobTypeDescription = "";
        public string strSelectedJobSubTypeDescription = "";
        public string strSelectedSitePostcode = "";
        public double dblSelectedSiteLatitude = (double)0.00;
        public double dblSelectedSiteLongitude = (double)0.00;
        public string strSelectedOnScreenLabel = "";

        public string _strExcludeIDs = "";

        public DateTime? i_dt_FromDate = null;
        public DateTime? i_dt_ToDate = null;
        private string i_str_date_range = "";

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //
        RepositoryItemCheckEdit tempEditorCheckEdit;
        RepositoryItemMemoExEdit tempEditorMemoExEdit;
        RepositoryItemTextEdit tempEditorTextEditText;
        RepositoryItemTextEdit tempEditorTextEditInteger;
        RepositoryItemTextEdit tempEditorTextEditDecimal;
        RepositoryItemTextEdit tempEditorTextEditCurrency;
        RepositoryItemTextEdit tempEditorTextEditDateTime;

        #endregion

        public frm_OM_Select_Job_Attribute()
        {
            InitializeComponent();
        }

        private void frm_OM_Select_Job_Attribute_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500195;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp06066_OM_Select_Client_ContractTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06117_OM_Site_Contracts_For_Client_ContractTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06147_OM_Visits_For_Site_ContractTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06209_OM_Jobs_For_VisitTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06238_OM_Job_Attributes_For_JobTableAdapter.Connection.ConnectionString = strConnectionString;
            
            if (i_dt_FromDate != null) dateEditFromDate.DateTime = Convert.ToDateTime(i_dt_FromDate);
            if (i_dt_ToDate != null) dateEditToDate.DateTime = Convert.ToDateTime(i_dt_ToDate);
            beiDateFilter.EditValue = PopupContainerEditDateRange_Get_Selected();

            GridView view = (GridView)gridControl1.MainView;
            LoadData();
            gridControl1.ForceInitialize();
            if (intOriginalClientContractID != 0)
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["ClientContractID"], intOriginalClientContractID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            gridControl2.ForceInitialize();
            if (intOriginalSiteContractID != 0)
            {
                view = (GridView)gridControl2.MainView; 
                int intFoundRow = view.LocateByValue(0, view.Columns["SiteContractID"], intOriginalSiteContractID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            gridControl3.ForceInitialize();
            if (intOriginalVisitID != 0)
            {
                view = (GridView)gridControl3.MainView;
                int intFoundRow = view.LocateByValue(0, view.Columns["VisitID"], intOriginalVisitID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }
            
            gridControl4.ForceInitialize();
            if (intOriginalJobID != 0)
            {
                view = (GridView)gridControl4.MainView;
                int intFoundRow = view.LocateByValue(0, view.Columns["JobID"], intOriginalJobID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            gridControl5.ForceInitialize();
            if (intOriginalJobAttributeID != 0)
            {
                view = (GridView)gridControl5.MainView;
                int intFoundRow = view.LocateByValue(0, view.Columns["JobAttributeID"], intOriginalJobAttributeID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            #region Create Extra Attribute Editors

            tempEditorCheckEdit = new RepositoryItemCheckEdit();
            tempEditorCheckEdit.AutoHeight = false;
            tempEditorCheckEdit.Caption = "Check";
            tempEditorCheckEdit.ValueChecked = "1";
            tempEditorCheckEdit.ValueUnchecked = "0";

            tempEditorMemoExEdit = new RepositoryItemMemoExEdit();
            tempEditorMemoExEdit.AutoHeight = false;
            tempEditorMemoExEdit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            tempEditorMemoExEdit.ShowIcon = false;

            tempEditorTextEditText = new RepositoryItemTextEdit();
            tempEditorTextEditText.AutoHeight = false;

            tempEditorTextEditInteger = new RepositoryItemTextEdit();
            tempEditorTextEditInteger.AutoHeight = false;
            tempEditorTextEditInteger.Mask.EditMask = "f0";
            tempEditorTextEditInteger.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            tempEditorTextEditInteger.Mask.UseMaskAsDisplayFormat = true;

            tempEditorTextEditDecimal = new RepositoryItemTextEdit();
            tempEditorTextEditDecimal.AutoHeight = false;
            tempEditorTextEditDecimal.Mask.EditMask = "f2";
            tempEditorTextEditDecimal.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            tempEditorTextEditDecimal.Mask.UseMaskAsDisplayFormat = true;

            tempEditorTextEditCurrency = new RepositoryItemTextEdit();
            tempEditorTextEditCurrency.AutoHeight = false;
            tempEditorTextEditCurrency.Mask.EditMask = "c";
            tempEditorTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            tempEditorTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;

            tempEditorTextEditDateTime = new RepositoryItemTextEdit();
            tempEditorTextEditDateTime.AutoHeight = false;
            tempEditorTextEditDateTime.Mask.EditMask = "g";
            tempEditorTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            tempEditorTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;

            #endregion


            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;

            gridControl2.MainView.BeginUpdate();
            dataSet_OM_Contract.sp06066_OM_Select_Client_Contract.Clear();
            gridControl2.MainView.EndUpdate();

            gridControl3.MainView.BeginUpdate();
            dataSet_OM_Visit.sp06147_OM_Visits_For_Site_Contract.Clear();
            gridControl3.MainView.EndUpdate();

            gridControl4.MainView.BeginUpdate();
            dataSet_OM_Job.sp06209_OM_Jobs_For_Visit.Clear();
            gridControl4.MainView.EndUpdate();

            gridControl5.MainView.BeginUpdate();
            dataSet_OM_Job.sp06238_OM_Job_Attributes_For_Job.Clear();
            gridControl5.MainView.EndUpdate();

            view.BeginUpdate();
            DateTime dtFromDate = (i_dt_FromDate == null || i_dt_FromDate == Convert.ToDateTime("01/01/0001") ? new DateTime(2001, 01, 01) : Convert.ToDateTime(i_dt_FromDate));
            DateTime dtToDate = (i_dt_ToDate == null || i_dt_ToDate == Convert.ToDateTime("01/01/0001") ? new DateTime(2900, 12, 31) : Convert.ToDateTime(i_dt_ToDate));

            sp06066_OM_Select_Client_ContractTableAdapter.Fill(dataSet_OM_Contract.sp06066_OM_Select_Client_Contract, _strExcludeIDs, dtFromDate, dtToDate);
            view.EndUpdate();
        }

        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;

            gridControl3.MainView.BeginUpdate();
            dataSet_OM_Visit.sp06147_OM_Visits_For_Site_Contract.Clear();
            gridControl3.MainView.EndUpdate();

            gridControl4.MainView.BeginUpdate();
            dataSet_OM_Job.sp06209_OM_Jobs_For_Visit.Clear();
            gridControl4.MainView.EndUpdate();

            gridControl5.MainView.BeginUpdate();
            dataSet_OM_Job.sp06238_OM_Job_Attributes_For_Job.Clear();
            gridControl5.MainView.EndUpdate();

            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ClientContractID"])) + ',';
            }

            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                dataSet_OM_Contract.sp06066_OM_Select_Client_Contract.Clear();
            }
            else
            {
                try
                {
                    sp06117_OM_Site_Contracts_For_Client_ContractTableAdapter.Fill(dataSet_OM_Contract.sp06117_OM_Site_Contracts_For_Client_Contract, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 0);
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related Site Contracts.\n\nTry selecting a Client Contract again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void LoadLinkedData2()
        {
            GridView view = (GridView)gridControl2.MainView;

            gridControl4.MainView.BeginUpdate();
            dataSet_OM_Job.sp06209_OM_Jobs_For_Visit.Clear();
            gridControl4.MainView.EndUpdate();

            gridControl5.MainView.BeginUpdate();
            dataSet_OM_Job.sp06238_OM_Job_Attributes_For_Job.Clear();
            gridControl5.MainView.EndUpdate();

            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["SiteContractID"])) + ',';
            }

            gridControl3.MainView.BeginUpdate();
            if (intCount == 0)
            {
                dataSet_OM_Visit.sp06147_OM_Visits_For_Site_Contract.Clear();
            }
            else
            {
                try
                {
                    sp06147_OM_Visits_For_Site_ContractTableAdapter.Fill(dataSet_OM_Visit.sp06147_OM_Visits_For_Site_Contract, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related Visits.\n\nTry selecting a Site Contract again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl3.MainView.EndUpdate();
        }

        private void LoadLinkedData3()
        {
            GridView view = (GridView)gridControl3.MainView;

            gridControl5.MainView.BeginUpdate();
            dataSet_OM_Job.sp06238_OM_Job_Attributes_For_Job.Clear();
            gridControl5.MainView.EndUpdate();

            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["VisitID"])) + ',';
            }

            gridControl4.MainView.BeginUpdate();
            if (intCount == 0)
            {
                dataSet_OM_Job.sp06209_OM_Jobs_For_Visit.Clear();
            }
            else
            {
                try
                {
                    sp06209_OM_Jobs_For_VisitTableAdapter.Fill(dataSet_OM_Job.sp06209_OM_Jobs_For_Visit, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related Jobs.\n\nTry selecting a Visit again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl4.MainView.EndUpdate();
        }

        private void LoadLinkedData4()
        {
            GridView view = (GridView)gridControl4.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["JobID"])) + ',';
            }

            gridControl5.MainView.BeginUpdate();
            if (intCount == 0)
            {
                dataSet_OM_Job.sp06238_OM_Job_Attributes_For_Job.Clear();
            }
            else
            {
                try
                {
                    sp06238_OM_Job_Attributes_For_JobTableAdapter.Fill(dataSet_OM_Job.sp06238_OM_Job_Attributes_For_Job, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related Attributes.\n\nTry selecting a Job again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl5.MainView.EndUpdate();
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Client Crontacts Available - Try adjusting any Date Filters and click Refresh Data button";
                    break;
                case "gridView2":
                    message = "No Site Contracts Available - Select a Client Contract to view linked Site Contracts";
                    break;
                case "gridView3":
                    message = "No Visits Available - Select a Site Contract to view linked Visits";
                    break;
                case "gridView4":
                    message = "No Jobs Available - Select a Visit to view linked Jobs";
                    break;
                case "gridView5":
                    message = "No Attributes Available - Select a Job to view linked Attribute";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedData1();
                    break;
                case "gridView2":
                    LoadLinkedData2();
                    break;
                case "gridView3":
                    LoadLinkedData3();
                    break;
                case "gridView4":
                    LoadLinkedData4();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region GridView5

        private void gridView5_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.ListSourceRowIndex < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "ValueRecorded":
                case "ValueRecordedText":
                    {
                        string strMask = view.GetRowCellValue(e.ListSourceRowIndex, "EditorMask").ToString();
                        if (!string.IsNullOrWhiteSpace(strMask) && !string.IsNullOrWhiteSpace(e.Value.ToString()))
                        {
                            //e.DisplayText = string.Format("{0:N2} " + strMask, e.Value);
                            e.DisplayText = string.Format(strMask, e.Value);
                        }
                        break;
                    }
            }
        }

        private void gridView5_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "ValueRecorded":
                    case "ValueRecordedText":
                        int intDataTypeID = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DataTypeID"));
                        int intEditorTypeID = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "EditorTypeID"));
                        string strValue = view.GetRowCellValue(e.RowHandle, "ValueRecordedText").ToString();
                        if (string.IsNullOrWhiteSpace(strValue))
                        {
                            e.RepositoryItem = emptyEditor;  // No value so don't bother with an editor //
                        }
                        else
                        {
                            switch (intDataTypeID)
                            {
                                case 1:  // Text Short //
                                    e.RepositoryItem = tempEditorTextEditText;
                                    break;
                                case 2:  // Text Long //
                                    e.RepositoryItem = tempEditorMemoExEdit;
                                    break;
                                case 3:  // Integer //
                                    if (intEditorTypeID == 4)  // Spinner //
                                    {
                                        e.RepositoryItem = tempEditorTextEditInteger;
                                    }
                                    else if (intEditorTypeID == 5)  //  Check Edit //
                                    {
                                        e.RepositoryItem = tempEditorCheckEdit;
                                    }
                                    else if (intEditorTypeID == 6)  // 4 = Picklist //
                                    {
                                        e.RepositoryItem = tempEditorTextEditText;
                                    }
                                    break;
                                case 4:  // Decimal //
                                    e.RepositoryItem = tempEditorTextEditDecimal;
                                    break;
                                case 5:  // Money //
                                    e.RepositoryItem = tempEditorTextEditCurrency;
                                    break;
                                case 6:  // DateTime Edit //
                                    e.RepositoryItem = tempEditorTextEditDateTime;
                                    break;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }
        
        private void gridView5_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "ValueRecorded":
                case "ValueRecordedText":
                    int intDataTypeID = Convert.ToInt32(view.GetFocusedRowCellValue("DataTypeID"));
                    int intEditorTypeID = Convert.ToInt32(view.GetFocusedRowCellValue("EditorTypeID"));
                    if (intEditorTypeID != 3) e.Cancel = true;  // Memo Ex Edit //
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region Date Range Filter Panel

        private void btnDateRangeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            i_dt_FromDate = null;    // Reset any prior values first //
            i_dt_ToDate = null;  // Reset any prior values first //
            i_str_date_range = "";
            GridView view = (GridView)gridControl1.MainView;

            i_dt_FromDate = dateEditFromDate.DateTime;
            i_dt_ToDate = dateEditToDate.DateTime;
            i_str_date_range = (i_dt_FromDate == null || i_dt_FromDate == Convert.ToDateTime("01/01/0001") ? "No Start" : Convert.ToDateTime(i_dt_FromDate).ToString("dd/MM/yyyy"));
            i_str_date_range += " - " + (i_dt_ToDate == null || i_dt_ToDate == Convert.ToDateTime("01/01/0001") ? "No End" : Convert.ToDateTime(i_dt_ToDate).ToString("dd/MM/yyyy"));
            return i_str_date_range;
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (intSelectedJobAttributeID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a job attribute record before proceeding.", "Select Job Attribute", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            GridView view4 = (GridView)gridControl4.MainView;
            GridView view5 = (GridView)gridControl5.MainView;
            if (view4.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intSelectedClientID = Convert.ToInt32(view4.GetRowCellValue(view4.FocusedRowHandle, "ClientID"));
                intSelectedClientContractID = Convert.ToInt32(view4.GetRowCellValue(view4.FocusedRowHandle, "ClientContractID"));
                intSelectedSiteID = Convert.ToInt32(view4.GetRowCellValue(view4.FocusedRowHandle, "SiteID"));
                intSelectedSiteContractID = Convert.ToInt32(view4.GetRowCellValue(view4.FocusedRowHandle, "SiteContractID"));
                intSelectedVisitID = Convert.ToInt32(view4.GetRowCellValue(view4.FocusedRowHandle, "VisitID"));
                intSelectedJobID = Convert.ToInt32(view4.GetRowCellValue(view4.FocusedRowHandle, "JobID"));
                intSelectedJobAttributeID = Convert.ToInt32(view5.GetRowCellValue(view5.FocusedRowHandle, "JobAttributeID"));

                intSelectedVisitNumber = (String.IsNullOrEmpty(Convert.ToString(view4.GetRowCellValue(view4.FocusedRowHandle, "VisitNumber"))) ? 0 : Convert.ToInt32(view4.GetRowCellValue(view4.FocusedRowHandle, "VisitNumber")));
                strSelectedClientName = (String.IsNullOrEmpty(Convert.ToString(view4.GetRowCellValue(view4.FocusedRowHandle, "ClientName"))) ? "Unknown Client" : Convert.ToString(view4.GetRowCellValue(view4.FocusedRowHandle, "ClientName")));
                strSelectedContractDescription = (String.IsNullOrEmpty(Convert.ToString(view4.GetRowCellValue(view4.FocusedRowHandle, "ContractDescription"))) ? "Unknown Contract" : Convert.ToString(view4.GetRowCellValue(view4.FocusedRowHandle, "ContractDescription")));
                strSelectedSiteName = (String.IsNullOrEmpty(Convert.ToString(view4.GetRowCellValue(view4.FocusedRowHandle, "SiteName"))) ? "Unknown Site" : Convert.ToString(view4.GetRowCellValue(view4.FocusedRowHandle, "SiteName")));
                intSelectedJobTypeID = (String.IsNullOrEmpty(Convert.ToString(view4.GetRowCellValue(view4.FocusedRowHandle, "JobTypeID"))) ? 0 : Convert.ToInt32(view4.GetRowCellValue(view4.FocusedRowHandle, "JobTypeID")));
                intSelectedJobSubTypeID = (String.IsNullOrEmpty(Convert.ToString(view4.GetRowCellValue(view4.FocusedRowHandle, "JobSubTypeID"))) ? 0 : Convert.ToInt32(view4.GetRowCellValue(view4.FocusedRowHandle, "JobSubTypeID")));
                strSelectedJobTypeDescription = (String.IsNullOrEmpty(Convert.ToString(view4.GetRowCellValue(view4.FocusedRowHandle, "JobTypeDescription"))) ? "Unknown Job Type" : Convert.ToString(view4.GetRowCellValue(view4.FocusedRowHandle, "JobTypeDescription")));
                strSelectedJobSubTypeDescription = (String.IsNullOrEmpty(Convert.ToString(view4.GetRowCellValue(view4.FocusedRowHandle, "JobSubTypeDescription"))) ? "Unknown Job Sub-Type" : Convert.ToString(view4.GetRowCellValue(view4.FocusedRowHandle, "JobSubTypeDescription")));
                strSelectedSitePostcode = (String.IsNullOrEmpty(Convert.ToString(view4.GetRowCellValue(view4.FocusedRowHandle, "SitePostcode"))) ? "" : Convert.ToString(view4.GetRowCellValue(view4.FocusedRowHandle, "SitePostcode")));
                dblSelectedSiteLatitude = (String.IsNullOrEmpty(Convert.ToString(view4.GetRowCellValue(view4.FocusedRowHandle, "LocationX"))) ? (double)0.00 : Convert.ToDouble(view4.GetRowCellValue(view4.FocusedRowHandle, "LocationX")));
                dblSelectedSiteLongitude = (String.IsNullOrEmpty(Convert.ToString(view4.GetRowCellValue(view4.FocusedRowHandle, "LocationY"))) ? (double)0.00 : Convert.ToDouble(view4.GetRowCellValue(view4.FocusedRowHandle, "LocationY")));
                strSelectedOnScreenLabel = (String.IsNullOrEmpty(Convert.ToString(view5.GetRowCellValue(view5.FocusedRowHandle, "OnScreenLabel"))) ? "" : Convert.ToString(view5.GetRowCellValue(view5.FocusedRowHandle, "OnScreenLabel")));
            }
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }



    }
}


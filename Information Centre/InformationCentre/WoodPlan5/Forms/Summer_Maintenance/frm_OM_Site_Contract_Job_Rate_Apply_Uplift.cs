using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using WoodPlan5.Properties;
using BaseObjects;


namespace WoodPlan5
{
    public partial class frm_OM_Site_Contract_Job_Rate_Apply_Uplift : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        public string strReturnedNewIDs = "";

        #endregion

        public frm_OM_Site_Contract_Job_Rate_Apply_Uplift()
        {
            InitializeComponent();
        }

        private void frm_OM_Site_Contract_Job_Rate_Apply_Uplift_Load(object sender, EventArgs e)
        {
            this.FormID = 500250;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            barStaticItemInformation.Caption = "Job Rates Selected For Uplift: " + intRecordCount.ToString();
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
        }


        #region Editors

        private void checkEditClone_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                groupControlClonedValues.Enabled = true;
                FromDate.Enabled = true;
                this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            }
        }

        private void checkEditApplyRateUplift_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                groupControlClonedValues.Enabled = false;
                FromDate.Enabled = false;
                FromDate.EditValue = null;
            }
        }

        private void checkEditTeamPercentage_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                spinEditTeamAmount.Properties.Mask.EditMask = "P";  // Percentage //
            }
            else
            {
                spinEditTeamAmount.Properties.Mask.EditMask = "c";  // Currency //
            }
        }

        private void checkEditTeamFixed_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                spinEditTeamAmount.Properties.Mask.EditMask = "c";  // Currency //
            }
            else
            {
                spinEditTeamAmount.Properties.Mask.EditMask = "P";  // Percentage //
            }
        }

        private void checkEditClientPercentage_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                spinEditClientAmount.Properties.Mask.EditMask = "P";  // Percentage //
            }
            else
            {
                spinEditClientAmount.Properties.Mask.EditMask = "c";  // Currency //
            }
        }

        private void checkEditClientFixed_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                spinEditClientAmount.Properties.Mask.EditMask = "c";  // Currency //
            }
            else
            {
                spinEditClientAmount.Properties.Mask.EditMask = "P";  // Percentage //
            }
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Updating Rates...");

            DataSet_UT_EditTableAdapters.QueriesTableAdapter CloneContracts = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
            CloneContracts.ChangeConnectionString(strConnectionString);

            string strTask = (checkEditClone.Checked ? "clone" : "uplift");
            DateTime? dtFromDate = null;
            try
            {
                dtFromDate = (checkEditClone.Checked ? (DateTime?)FromDate.DateTime : null);
                if (dtFromDate.ToString() == "01/01/0001 00:00:00") dtFromDate = null;
            }
            catch (Exception) { }

            string strTeamUpliftType = (checkEditTeamPercentage.Checked ? "percentage" : "fixed amount");
            string strClientUpliftType = (checkEditClientPercentage.Checked ? "percentage" : "fixed amount");
            int intApplyToVisitsAndJobs = (checkEditApplyUpliftToVisitAndJobs.Checked ? 1 : 0);

            using (var UpliftRates = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
            {
                UpliftRates.ChangeConnectionString(strConnectionString);
                try
                {
                    strReturnedNewIDs = UpliftRates.sp06348_OM_Site_Contract_Rate_Uplift(strRecordIDs, strTask, dtFromDate, Convert.ToDecimal(spinEditTeamAmount.EditValue), strTeamUpliftType, Convert.ToDecimal(spinEditClientAmount.EditValue), strClientUpliftType, intApplyToVisitsAndJobs).ToString();
                }
                catch (Exception ex) 
                {
                    if (splashScreenManager1.IsSplashFormVisible)
                    {
                        splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                        splashScreenManager1.CloseWaitForm();
                    }
                    XtraMessageBox.Show("An error occurred while uplifting rates [" + ex.Message + "]!\n\nTry again - if the problem persists, contact Technical Support.", "Apply Site Contract Rate Uplift", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }

            this.DialogResult = DialogResult.OK;
            this.Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }






    
    }
}


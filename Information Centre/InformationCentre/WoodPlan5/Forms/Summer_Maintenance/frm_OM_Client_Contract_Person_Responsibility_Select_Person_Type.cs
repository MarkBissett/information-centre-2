﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Client_Contract_Person_Responsibility_Select_Person_Type : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        public int _SelectedTypeID = -1;
        public string _SelectedTypeDescription = "";

        #endregion

        public frm_OM_Client_Contract_Person_Responsibility_Select_Person_Type()
        {
            InitializeComponent();
        }

        private void frm_OM_Client_Contract_Person_Responsibility_Select_Person_Type_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500256;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;


            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
        }

        public override void PostLoadView(object objParameter)
        {
        }


        private void simpleButtonStaff_Click(object sender, EventArgs e)
        {
            _SelectedTypeID = 0;
            _SelectedTypeDescription = "Staff";
            this.Close();
        }

        private void simpleButtonCustomer_Click(object sender, EventArgs e)
        {
            _SelectedTypeID = 1;
            _SelectedTypeDescription = "Client";
            this.Close();
        }

        private void simpleButtonCancel_Click(object sender, EventArgs e)
        {
            _SelectedTypeID = -1;
            _SelectedTypeDescription = "";
            this.Close();
        }



    }
}

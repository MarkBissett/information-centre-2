using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_GC_SUM_Permits_Import : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        #endregion

        public frm_GC_SUM_Permits_Import()
        {
            InitializeComponent();
        }

        private void frm_GC_SUM_Permits_Import_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 400128;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;
            
            GridView view = (GridView)gridControl1.MainView;

            gridControl1.ForceInitialize();

            Set_Grid_Highlighter_Transparent(this.Controls);

            PostOpen();
            Load_Spreadsheet();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            //int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Errors Present");
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void Load_Spreadsheet()
        {
            #region Check Files

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Checking Permit Import File...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //


            // Get Forecasting filenames from System_Settings table //
            string strPermitFile1 = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strPermitFile1 = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "ArchivaWorkPermitFile").ToString();
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the name of Permit Import File (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Permit Import File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
            }
            if (fProgress != null) fProgress.UpdateProgress(40); // Update Progress Bar //
 
            // Check Files exist //
            if (!File.Exists(strPermitFile1))
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("The Permit Import File [" + strPermitFile1 + "] does not exist.\n\nPlease ensure todays permit import file has been saved to the correct location and that it has been named correctly before trying again.", "Check Permit Import File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            System.Windows.Forms.Application.DoEvents();  // Allow Form time to repaint itself //

            #endregion

            // Filename present so attempt to parse it //
            fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Importing Permit File...");
            fProgress.Show();

            gridControl1.BeginUpdate();
            int intErrorCount = 0;
            try
            {
                string[] lines = File.ReadAllLines(strPermitFile1);

                int intUpdateProgressThreshhold = lines.Length / 10;
                int intUpdateProgressTempCount = 0;
                int intLineNumber = 0;
                System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

                DataSet_GC_Summer_DataEntryTableAdapters.QueriesTableAdapter ImportRecord = new DataSet_GC_Summer_DataEntryTableAdapters.QueriesTableAdapter();
                ImportRecord.ChangeConnectionString(strConnectionString);
                
                int ClientSiteID = 0;
                string StatusDesc = "";
                DateTime RequestStartDate = new DateTime();
                DateTime RequestEndDate = new DateTime();
                string CategoryDesc = "";
                string ClientPermitNumber = "";

                // Get Excluded values list //
                List<string> ExcludedStatusList = new List<string>();
                int intType = 0;  // Status //
                SqlDataAdapter sdaItems = new SqlDataAdapter();
                DataSet dsItems = new DataSet("NewDataSet");
                SqlConnection SQlConn = new SqlConnection(strConnectionString);
                SqlCommand cmd = null;
                cmd = new SqlCommand("sp06015_GC_Work_Permit_Import_Excluded_Values", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Type", intType));
                sdaItems = new SqlDataAdapter(cmd);
                sdaItems.Fill(dsItems, "Table");
                foreach (DataRow dr in dsItems.Tables[0].Rows)
                {
                    ExcludedStatusList.Add(dr["Description"].ToString().ToLower().Trim());
                }
 
                List<string> ExcludedCategoryList = new List<string>();
                intType = 1;  // Categories //
                cmd = null;
                cmd = new SqlCommand("sp06015_GC_Work_Permit_Import_Excluded_Values", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Type", intType));
                sdaItems = new SqlDataAdapter(cmd);
                sdaItems.Fill(dsItems, "Table");
                foreach (DataRow dr in dsItems.Tables[0].Rows)
                {
                    ExcludedCategoryList.Add(dr["Description"].ToString().ToLower().Trim());
                }
                cmd = null;
                SQlConn.Close();
                SQlConn.Dispose();


                foreach (string line in lines)
                {
                    intLineNumber++;
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                    if (intLineNumber <= 1) continue;  // Skip first line //

                    string[] columns = r.Split(line);  // Get Columns into Array //
                    if (columns.Length < 35) continue;

                    if (ExcludedStatusList.Contains(columns[5].ToString().ToLower().Trim())) continue;

                    if (ExcludedCategoryList.Contains(columns[6].ToString().ToLower().Trim())) continue;

                    if (string.IsNullOrEmpty(columns[8].ToString()))  // Missing Site ID so Add to Error Grid //
                    {
                        Add_Error(intLineNumber, "Missing Site ID");
                        continue;
                    }
                    else
                    {
                        ClientSiteID = Convert.ToInt32(columns[8].ToString());
                    }
                    if (string.IsNullOrEmpty(columns[5].ToString()))  // Missing Status so Add to Error Grid //
                    {
                        Add_Error(intLineNumber, "Missing Request Status");
                        continue;
                    }
                    else
                    {
                        StatusDesc = columns[5].ToString();
                    }
                    if (string.IsNullOrEmpty(columns[2].ToString()))  // Missing Start Date so Add to Error Grid //
                    {
                        Add_Error(intLineNumber, "Missing Request Start Date");
                        continue;
                    }
                    else
                    {
                        RequestStartDate = Convert.ToDateTime(columns[2].ToString());
                    }
                    if (string.IsNullOrEmpty(columns[3].ToString()))  // Missing End Date so Add to Error Grid //
                    {
                        Add_Error(intLineNumber, "Missing Request End Date");
                        continue;
                    }
                    else
                    {
                        RequestEndDate = Convert.ToDateTime(columns[3].ToString());
                    }
                    if (string.IsNullOrEmpty(columns[6].ToString()))  // Missing Category so Add to Error Grid //
                    {
                        Add_Error(intLineNumber, "Missing Category");
                        continue;
                    }
                    else
                    {
                        CategoryDesc = columns[6].ToString();
                    }
                    if (string.IsNullOrEmpty(columns[1].ToString()))  // Missing Request ID (Client Permit Number) so Add to Error Grid //
                    {
                        Add_Error(intLineNumber, "Missing Request ID");
                        continue;
                    }
                    else
                    {
                        ClientPermitNumber = columns[1].ToString();
                    }
                    int intResult = Convert.ToInt32(ImportRecord.sp06013_GC_Work_Permit_Import_From_SS(ClientSiteID, StatusDesc, RequestStartDate, RequestEndDate, CategoryDesc, ClientPermitNumber));
                    switch (intResult)
                    {
                        case -1:  // Unknown Status ID //
                            {
                                Add_Error(intLineNumber, "Unknown Request Status");
                                intErrorCount++;
                            }
                            break;
                        case -2:  // Unknown Category ID //
                            {
                                Add_Error(intLineNumber, "Unknown Category");
                                intErrorCount++;
                            }
                            break;
                        case -3:  // Unknown Client Site ID //
                            {
                                Add_Error(intLineNumber, "Unknown Site ID");
                                intErrorCount++;
                            }
                            break;
                     }
                }
            }
            catch (Exception ex)
            {
                gridControl1.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Importing Permit File [" + strPermitFile1 + "].\n\nMessage = [" + ex.Message + "].\n\nThere may be a fault with the file. Please close this screen then try again. If the problem persists, contact Technical Support.", "Import Permit File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
            }

            gridControl1.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            if (intErrorCount == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Permit File [" + strPermitFile1 + "] Imported Successfully.", "Import Permit File", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                string strMessage = (intErrorCount == 1 ? "1 Error" : Convert.ToString(intErrorCount) + " Errors") + " occurred while Importing Permit File [" + strPermitFile1 + "].\n\nErrors are shown in the errors grid.";
                DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Import Permit File", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        private void Add_Error(int Row, string Error)
        {
            DataRow drNewRow;
            drNewRow = this.dataSet_GC_Summer_DataEntry.sp06014_GC_Work_Permit_Import_Errors_Dummy.NewRow();
            drNewRow["RowNumber"] = Row;
            drNewRow["ErrorMessage"] = Error;
            this.dataSet_GC_Summer_DataEntry.sp06014_GC_Work_Permit_Import_Errors_Dummy.Rows.Add(drNewRow);

        }


    }
}


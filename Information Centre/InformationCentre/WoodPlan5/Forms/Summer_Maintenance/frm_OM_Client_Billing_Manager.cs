using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using LocusEffects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Client_Billing_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        private int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState8;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs8 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        string i_str_selected_client_ids = "";
        string i_str_selected_client_names = "";
        string i_str_selected_site_ids = "";
        string i_str_selected_site_names = "";

        SuperToolTip superToolTipSiteContractFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsSiteContractFilter = null;

        SuperToolTip superToolTipClientContractFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsClientContractFilter = null;

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        #endregion

        public frm_OM_Client_Billing_Manager()
        {
            InitializeComponent();
        }

        private void frm_OM_Client_Billing_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 7017;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp06372_OM_Client_Billing_Manager_Sites_To_BillTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridView2, "SiteContractProfileID");


            sp06218_OM_Waste_Disposal_Centres_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "WasteDisposalCentreID");
            
            sp06219_OM_Waste_Disposal_Centres_Accepted_Waste_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState8 = new RefreshGridState(gridView8, "WasteTypeAcceptedID");

            dateEditFromDate.DateTime = DateTime.Today;

            // Create a SuperToolTip //
            superToolTipSiteContractFilter = new SuperToolTip();
            superToolTipSetupArgsSiteContractFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsSiteContractFilter.Title.Text = "Site Contract Filter - Information";
            superToolTipSetupArgsSiteContractFilter.Title.Image = imageCollectionTooltips.Images[0];
            superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsSiteContractFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsSiteContractFilter.Footer.Text = "";
            superToolTipSiteContractFilter.Setup(superToolTipSetupArgsSiteContractFilter);
            superToolTipSiteContractFilter.AllowHtmlText = DefaultBoolean.True;
            buttonEditSiteFilter.SuperTip = superToolTipSiteContractFilter;

            // Create a SuperToolTip //
            superToolTipClientContractFilter = new SuperToolTip();
            superToolTipSetupArgsClientContractFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsClientContractFilter.Title.Text = "Client Contract Filter - Information";
            superToolTipSetupArgsClientContractFilter.Title.Image = imageCollectionTooltips.Images[0];
            superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsClientContractFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsClientContractFilter.Footer.Text = "";
            superToolTipClientContractFilter.Setup(superToolTipSetupArgsClientContractFilter);
            superToolTipClientContractFilter.AllowHtmlText = DefaultBoolean.True;
            buttonEditClientFilter.SuperTip = superToolTipClientContractFilter;

            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            Application.DoEvents();  // Allow Form time to repaint itself //
            LoadLastSavedUserScreenSettings();

        }

        private void frm_OM_Client_Billing_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data();
                }
                if (UpdateRefreshStatus == 2 || !string.IsNullOrEmpty(i_str_AddedRecordIDs2))
                {
                    Load_Due_Invoices();
                }
                if (UpdateRefreshStatus == 8 || !string.IsNullOrEmpty(i_str_AddedRecordIDs8))
                {
                    LoadLinkedRecords();
                }
            }
            SetMenuStatus();
        }

        private void frm_OM_Client_Billing_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ClientFilter", i_str_selected_client_ids);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SiteFilter", i_str_selected_site_ids);

                default_screen_settings.SaveDefaultScreenSettings();
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            bool boolFilterInPlace = false;
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Client Filter //
                string strItemFilter = default_screen_settings.RetrieveSetting("ClientFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    i_str_selected_client_ids = strItemFilter;
                    var ResolveIDs = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
                    ResolveIDs.ChangeConnectionString(strConnectionString);
                    try
                    {
                        buttonEditClientFilter.Text = ResolveIDs.sp06023_OM_Get_Clients_From_ClientIDs(strItemFilter).ToString();
                        i_str_selected_client_names = buttonEditClientFilter.Text;

                        // Update Filter control's tooltip //
                        string strTooltipText = i_str_selected_client_names.Replace(", ", "\n");
                        superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;
                    }
                    catch (Exception) { }
                    boolFilterInPlace = true;
                }

                // Site Filter //
                strItemFilter = default_screen_settings.RetrieveSetting("SiteFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    i_str_selected_site_ids = strItemFilter;
                    var ResolveIDs = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
                    ResolveIDs.ChangeConnectionString(strConnectionString);
                    try
                    {
                        buttonEditSiteFilter.Text = ResolveIDs.sp06024_OM_Get_Sites_From_SiteIDs(strItemFilter).ToString();
                        i_str_selected_site_names = buttonEditSiteFilter.Text;

                        // Update Filter control's tooltip //
                        string strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                    }
                    catch (Exception) { }
                    boolFilterInPlace = true;
                }

                if (boolFilterInPlace)
                {
                    // Prepare LocusEffects and add custom effect //
                    locusEffectsProvider1 = new LocusEffectsProvider();
                    locusEffectsProvider1.Initialize();
                    locusEffectsProvider1.FramesPerSecond = 30;
                    m_customArrowLocusEffect1 = new ArrowLocusEffect();
                    m_customArrowLocusEffect1.Name = "CustomeArrow1";
                    m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                    m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                    m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                    m_customArrowLocusEffect1.MovementCycles = 20;
                    m_customArrowLocusEffect1.MovementAmplitude = 200;
                    m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                    m_customArrowLocusEffect1.LeadInTime = 0; //msec
                    m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                    m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                    locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                    Point location = dockPanelFilters.PointToScreen(Point.Empty);
                    System.Drawing.Point screenPoint = new System.Drawing.Point(location.X + 10, location.Y + 5);
                    locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
                }
                Load_Due_Invoices();  // Load records //
                Load_Data();  // Load records //
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs8)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs8 != "") i_str_AddedRecordIDs8 = strNewIDs8;
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            if (i_int_FocusedGrid == 2)
            {
                view = (GridView)gridControl2.MainView;
            }
            else if (i_int_FocusedGrid == 1)
            {
                view = (GridView)gridControl1.MainView;
            }
            else if (i_int_FocusedGrid == 8)
            {
                view = (GridView)gridControl8.MainView;
            }
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();

            if (i_int_FocusedGrid == 1)  // Headers //
            {
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView2 navigator custom buttons //
            //view = (GridView)gridControl2.MainView;
            //intRowHandles = view.GetSelectedRows();
            //gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);

            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);

            view = (GridView)gridControl8.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl8.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0);
        }


        #region Data Filter Panel

        private void buttonEditClientFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_client_ids = "";
                i_str_selected_client_names = "";
                buttonEditClientFilter.Text = "";

                // Update Filter control's tooltip //
                superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\nNo Filter Set.";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 0;
                fChildForm.intMustSelectChildren = 0;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intFilterSummerMaintenanceClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter.Text = i_str_selected_client_names;

                    // Update Filter control's tooltip //
                    string strTooltipText = i_str_selected_client_names.Replace(", ", "\n");
                    superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;

                    if (!string.IsNullOrWhiteSpace(fChildForm.strSelectedChildIDs))
                    {
                        i_str_selected_site_ids = fChildForm.strSelectedChildIDs;
                        i_str_selected_site_names = fChildForm.strSelectedChildDescriptions;
                        buttonEditSiteFilter.Text = i_str_selected_site_names;

                        // Update Filter control's tooltip //
                        strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                    }
                    else
                    {
                        // Update Filter control's tooltip //
                        strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
                    }

                }
            }

        }

        private void buttonEditSiteFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_site_ids = "";
                i_str_selected_site_names = "";
                buttonEditSiteFilter.Text = "";

                // Update Filter control's tooltip //
                superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 1;
                fChildForm.intMustSelectChildren = 1;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids;
                fChildForm.strPassedInChildIDs = i_str_selected_site_ids;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intFilterSummerMaintenanceClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter.Text = i_str_selected_client_names;

                    // Update Filter control's tooltip //
                    string strTooltipText = i_str_selected_client_names.Replace(", ", "\n");
                    superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;

                    i_str_selected_site_ids = fChildForm.strSelectedChildIDs;
                    i_str_selected_site_names = fChildForm.strSelectedChildDescriptions;
                    buttonEditSiteFilter.Text = i_str_selected_site_names;

                    // Update Filter control's tooltip //
                    strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                    superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                }
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            Load_Due_Invoices();
        }

        #endregion


        private void Load_Due_Invoices()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            DateTime dtDate = DateTime.Today;
            try
            {
                if (dateEditFromDate.DateTime != DateTime.MinValue)
                {
                    dtDate = dateEditFromDate.DateTime;
                }
            }
            catch (Exception) { }

            GridView view = (GridView)gridControl2.MainView;
            RefreshGridViewState2.SaveViewInfo();
            view.BeginUpdate();
            sp06372_OM_Client_Billing_Manager_Sites_To_BillTableAdapter.Fill(dataSet_OM_Client_PO.sp06372_OM_Client_Billing_Manager_Sites_To_Bill, dtDate, i_str_selected_client_ids, i_str_selected_site_ids);
            RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.ExpandAllGroups();
            view.EndUpdate();
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            GridView view = (GridView)gridControl1.MainView;
            RefreshGridViewState1.SaveViewInfo();
            view.BeginUpdate();
            sp06218_OM_Waste_Disposal_Centres_ManagerTableAdapter.Fill(dataSet_OM_Job.sp06218_OM_Waste_Disposal_Centres_Manager);
            RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.ExpandAllGroups();
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["WasteDisposalCentreID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void LoadLinkedRecords()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["WasteDisposalCentreID"])) + ',';
            }

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            // Populate Linked Records //
            gridControl8.MainView.BeginUpdate();
            this.RefreshGridViewState8.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Job.sp06219_OM_Waste_Disposal_Centres_Accepted_Waste_Types.Clear();
            }
            else // Load users selection //
            {
                sp06219_OM_Waste_Disposal_Centres_Accepted_Waste_TypesTableAdapter.Fill(dataSet_OM_Job.sp06219_OM_Waste_Disposal_Centres_Accepted_Waste_Types, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState8.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl8.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs8 != "")
            {
                strArray = i_str_AddedRecordIDs8.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl8.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["WasteTypeAcceptedID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs8 = "";
            }
        }


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            if (!iBool_AllowAdd) return;
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            System.Reflection.MethodInfo method = null;

            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        var fChildForm = new frm_OM_Waste_Disposal_Centre_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 2:
                    {
                        var fChildForm = new frm_OM_Master_Waste_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 8:     // Item //
                    {
                        var fChildForm = new frm_OM_Waste_Disposal_Waste_Types_Accepted_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControl1.MainView;
                        view = (GridView)gridControl8.MainView;

                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm._WasteDisposalCentreID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "WasteDisposalCentreID"));
                            fChildForm._WasteDisposalCentreName = ParentView.GetRowCellValue(intRowHandles[0], "WasteDisposalCentreName").ToString();
                            fChildForm._WasteDisposalCentreAddressLine1 = ParentView.GetRowCellValue(intRowHandles[0], "AddressLine1").ToString();
                        }
                        else
                        {
                            fChildForm._WasteDisposalCentreID = 0;
                            fChildForm._WasteDisposalCentreName = "";
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Add()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 8:     // Linked Template Items //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;  // Parent gridview //
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WasteDisposalCentreID")) + ',';
                        }
                        this.RefreshGridViewState8.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Waste_Disposal_Waste_Types_Accepted_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WasteDisposalCentreID")) + ',';
                        }
                        var fChildForm = new frm_OM_Waste_Disposal_Centre_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 2:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WasteTypeID")) + ',';
                        }
                        var fChildForm = new frm_OM_Master_Waste_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 8:     // Item //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl8.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WasteTypeAcceptedID")) + ',';
                        }
                        var fChildForm = new frm_OM_Waste_Disposal_Waste_Types_Accepted_Edit(); ;
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WasteDisposalCentreID")) + ',';
                        }
                        var fChildForm = new frm_OM_Waste_Disposal_Centre_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case  2:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WasteTypeID")) + ',';
                        }
                        var fChildForm = new frm_OM_Master_Waste_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 8:     // Item //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl8.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WasteTypeAcceptedID")) + ',';
                        }
                        var fChildForm = new frm_OM_Waste_Disposal_Waste_Types_Accepted_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Waste Disposal Centres to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Waste Disposal Centre" : Convert.ToString(intRowHandles.Length) + " Waste Disposal Centres") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Waste Disposal Centre" : "these Waste Disposal Centres") + " will no longer be available for selection!</color>";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "WasteDisposalCentreID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("waste_disposal_centre", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Data();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 2:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Master Waste Types to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Master Waste Type" : Convert.ToString(intRowHandles.Length) + " Master Waste Types") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Master Waste Type" : "these Master Waste Types") + " will no longer be available for selection!</color>";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "WasteTypeID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("master_waste_type", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Due_Invoices();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 8:  // Item //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl8.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Waste Types Accepted to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Waste Type Accepted" : Convert.ToString(intRowHandles.Length) + " Waste Types Accepted") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Waste Type Accepted" : "these Waste Types Accepted") + " will no longer be available for selection!</color>";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "WasteTypeAcceptedID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("waste_type_accepted", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WasteDisposalCentreID")) + ',';
                        }
                        var fChildForm = new frm_OM_Waste_Disposal_Centre_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 2:
                    {
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WasteTypeID")) + ',';
                        }
                        var fChildForm = new frm_OM_Master_Waste_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 8:     // Item //
                    {
                        view = (GridView)gridControl8.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WasteTypeAcceptedID")) + ',';
                        }
                        var fChildForm = new frm_OM_Waste_Disposal_Waste_Types_Accepted_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                default:
                    break;
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView2":
                    message = "No Client Invoices Due - Adjust any filters then click Refresh button";
                    break;
                case "gridView1":
                    message = "No Client Invoice Headers - Adjust any filters then click the Refresh button";
                    break;
                case "gridView8":
                    message = "No Linked Visits Available - Select one or more Client Invoice Headers to view Linked Visit";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedRecords();
                    view = (GridView)gridControl8.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView2

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Due_Invoices();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView2_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "VisitCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "VisitCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            /*DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }*/
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 2;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView2_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "VisitCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("VisitCount")) <= 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditVisits_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intSiteContractID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SiteContractID"));
            if (intSiteContractID <= 0) return;
            string strRecordIDs = "";

            switch (view.FocusedColumn.FieldName)
            {
                case "VisitCount":
                    {
                        DateTime dtDate = DateTime.Today;
                        try
                        {
                            if (dateEditFromDate.DateTime != DateTime.MinValue)
                            {
                                dtDate = dateEditFromDate.DateTime;
                            }
                        }
                        catch (Exception) { }
                        try
                        {
                            string strParameter = intSiteContractID.ToString() + ",";
                            var GetValue = new DataSet_OM_Client_POTableAdapters.QueriesTableAdapter();
                            GetValue.ChangeConnectionString(strConnectionString);
                            strRecordIDs = GetValue.sp06373_OM_Get_Visits_From_ClientInvoicesDue(intSiteContractID.ToString() + ",", dtDate).ToString();
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view visits linked to this Client Invoice Due [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_visit");
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView1

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Data();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            /*DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }*/
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        #endregion


        #region GridView8

        private void gridControl8_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinkedRecords();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView8_DoubleClick(object sender, EventArgs e)
        {
            /*DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }*/
        }

        private void gridView8_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 8;
            SetMenuStatus();
        }

        private void gridView8_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 8;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion










    }
}


using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Formula_Builder : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";

        GridHitInfo downHitInfo = null;

        public string strFormula = "";
        public int intCallerID = 0;  // 1 = Master Attribute Edit screen, 2 = Job Attribute Edit screen //
        public string strAttributeName = "";
        public int intParentRecordID = 0;
        public int intRecordID = 0;
        public int intFormulaEvaluateOrder = 0;
        int i_int_FocusedGrid = 1;

        #endregion

        public frm_OM_Formula_Builder()
        {
            InitializeComponent();
        }

        private void frm_OM_Formula_Builder_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500276;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp06482_OM_Formula_Builder_Available_Columns_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06482_OM_Formula_Builder_Available_Columns_ListTableAdapter.Fill(dataSet_OM_Core.sp06482_OM_Formula_Builder_Available_Columns_List, intCallerID, strAttributeName, intParentRecordID, intFormulaEvaluateOrder);

            sp06483_OM_Formula_Builder_Available_OperatorsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06483_OM_Formula_Builder_Available_OperatorsTableAdapter.Fill(dataSet_OM_Core.sp06483_OM_Formula_Builder_Available_Operators);

            memoEditFormula.Text = strFormula;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
        }

        public override void PostLoadView(object objParameter)
        {
        }


        private void TransferFieldToFormula(GridView view)
        {
            string strText = memoEditFormula.Text.Trim() ?? "";
            int intcurrentPosition = memoEditFormula.SelectionStart;
            int intLengthOfAddition = 0;

            if (memoEditFormula.SelectionLength > 0)  // Replace selected text //
            {
                memoEditFormula.SelectedText = view.GetFocusedRowCellValue("strField").ToString();
                intLengthOfAddition = view.GetFocusedRowCellValue("strField").ToString().Length;
            }
            else if (memoEditFormula.SelectionStart < memoEditFormula.Text.Length) // Insert at current Position //
            {
                memoEditFormula.SelectedText = view.GetFocusedRowCellValue("strField").ToString() + " ";
                intLengthOfAddition = view.GetFocusedRowCellValue("strField").ToString().Length + 1;
            }
            else  // Append to end of text //
            {
                if (strText == "")
                {
                    strText += view.GetFocusedRowCellValue("strField").ToString();
                    intLengthOfAddition = view.GetFocusedRowCellValue("strField").ToString().Length;
                }
                else
                {
                    strText += " " + view.GetFocusedRowCellValue("strField").ToString();
                    intLengthOfAddition = view.GetFocusedRowCellValue("strField").ToString().Length + 1;
                }
                memoEditFormula.Text = strText;
                memoEditFormula.SelectionStart = intcurrentPosition + intLengthOfAddition;
            }
        }

        private void bbiTransferField_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            TransferFieldToFormula((i_int_FocusedGrid == 1 ? gridView1 : gridView2));
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Formula Fields Available";
                    break;
                case "gridView2":
                    message = "No Functions Available";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region GridView1

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;

            TransferFieldToFormula(view); // Transfer Field Name to current curser position in Formula Box //
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 1;
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiTransferField.Enabled = (view.FocusedRowHandle == GridControl.InvalidRowHandle ? false : true);
                pmGrid1Menu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView2

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;

            TransferFieldToFormula(view); // Transfer Field Name to current curser position in Formula Box //
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 2;
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            /*if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiTransferField.Enabled = (view.FocusedRowHandle == GridControl.InvalidRowHandle ? false : true);
                pmGrid1Menu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }*/
        }

        #endregion


        private void btnTest_Click(object sender, EventArgs e)
        {
            string strResult = TestFormula();
            switch (strResult)
            {
                case "No Formula":
                    {
                        XtraMessageBox.Show("No formula has been entered.\n\nEnter the formula then try again.", "Test Formula", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    break;
                case "Invalid Formula":
                    {
                        XtraMessageBox.Show("The formula entered is Invalid - adjust the formula then try again.", "Test Formula", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    break;
                case "???":
                    {
                        XtraMessageBox.Show("The formula entered is Invalid - adjust the formula then try again.", "Test Formula", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    break;
                default:
                    {
                        XtraMessageBox.Show("The formula entered is Valid.\n\nResult = " + strResult, "Test Formula", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
            }
        }

        private string TestFormula()
        {
            string strFormula = memoEditFormula.Text.Trim() ?? "";
            if (string.IsNullOrWhiteSpace(strFormula)) return "No Formula";  // No Formula //


            DataTable dt = new DataTable("Items");
            dt.Columns.Add("AttributeName", typeof(string));
            dt.Columns.Add("ValueRecorded", typeof(string));

            foreach (DataRow dr in dataSet_OM_Core.sp06482_OM_Formula_Builder_Available_Columns_List.Rows)
            {
                dt.Rows.Add(dr["strField"], dr["ValueRecorded"]);  // Build individual items from JSON into datatable to pass to Queue - Items table //
            }
            // Now Execute it in SQL to see if it returns an error //
            string strResult = "";
            try
            {
                DataSet_OM_CoreTableAdapters.QueriesTableAdapter Test = new DataSet_OM_CoreTableAdapters.QueriesTableAdapter();
                Test.ChangeConnectionString(strConnectionString);
                strResult = Test.sp06485_OM_Formula_Test(intRecordID, intParentRecordID, intCallerID, intFormulaEvaluateOrder, strFormula, dt).ToString();
            }
            catch (Exception)
            {
                return "Invalid Formula";  // Formula Invalid //
            }
            return strResult;  // Formula Valid //
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string strResult = TestFormula();
            switch (strResult)
            {
                case "No Formula":
                    {
                        if (XtraMessageBox.Show("No formula has been entered!\n\nAre you sure you wish to save the formula?", "Save Formula", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == System.Windows.Forms.DialogResult.No) return;
                    }
                    break;
                case "Invalid Formula":
                    {
                        if (XtraMessageBox.Show("The formula entered is Invalid!\n\nAre you sure you wish to save the formula?", "Save Formula", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == System.Windows.Forms.DialogResult.No) return;
                    }
                    break;
                case "???":
                    {
                        if (XtraMessageBox.Show("The formula entered is Invalid!\n\nAre you sure you wish to save the formula?", "Save Formula", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == System.Windows.Forms.DialogResult.No) return;
                    }
                    break;
                default:
                    // Valid Formula so save it //
                    break;
            }
            strFormula = memoEditFormula.Text.Trim();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

 

    }
}


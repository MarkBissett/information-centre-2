namespace WoodPlan5
{
    partial class frm_OM_Site_Contract_Year_Billing_Profile_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Site_Contract_Year_Billing_Profile_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.SiteContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp06095OMSiteContractYearBillingProfileEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.BilledByPersonButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ActualBillAmountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EstimatedBillAmountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.InvoiceDateActualDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.InvoiceDateDueDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.SiteContractProfileIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.LinkedToParentButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.SiteContractYearIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.BilledByPersonIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForBilledByPersonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteContractYearID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteContractProfileID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLinkedToParent = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layGrpAddress = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForInvoiceDateActual = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInvoiceDateDue = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForEstimatedBillAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualBillAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForBilledByPerson = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp06095_OM_Site_Contract_Year_Billing_Profile_EditTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06095_OM_Site_Contract_Year_Billing_Profile_EditTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06095OMSiteContractYearBillingProfileEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BilledByPersonButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualBillAmountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedBillAmountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceDateActualDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceDateActualDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceDateDueDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceDateDueDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractProfileIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToParentButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractYearIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BilledByPersonIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBilledByPersonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractYearID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractProfileID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToParent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvoiceDateActual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvoiceDateDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedBillAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualBillAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBilledByPerson)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 370);
            this.barDockControlBottom.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 344);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(686, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 344);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(686, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 370);
            this.barDockControl2.Size = new System.Drawing.Size(686, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 344);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(686, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 344);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.SiteContractIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.BilledByPersonButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualBillAmountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EstimatedBillAmountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.InvoiceDateActualDateEdit);
            this.dataLayoutControl1.Controls.Add(this.InvoiceDateDueDateEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteContractProfileIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.LinkedToParentButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteContractYearIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.BilledByPersonIDTextEdit);
            this.dataLayoutControl1.DataSource = this.sp06095OMSiteContractYearBillingProfileEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForBilledByPersonID,
            this.ItemForSiteContractID,
            this.ItemForSiteContractYearID,
            this.ItemForSiteContractProfileID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1435, 208, 301, 422);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(686, 344);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // SiteContractIDTextEdit
            // 
            this.SiteContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06095OMSiteContractYearBillingProfileEditBindingSource, "SiteContractID", true));
            this.SiteContractIDTextEdit.Location = new System.Drawing.Point(138, 35);
            this.SiteContractIDTextEdit.MenuManager = this.barManager1;
            this.SiteContractIDTextEdit.Name = "SiteContractIDTextEdit";
            this.SiteContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteContractIDTextEdit, true);
            this.SiteContractIDTextEdit.Size = new System.Drawing.Size(519, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteContractIDTextEdit, optionsSpelling1);
            this.SiteContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteContractIDTextEdit.TabIndex = 45;
            // 
            // sp06095OMSiteContractYearBillingProfileEditBindingSource
            // 
            this.sp06095OMSiteContractYearBillingProfileEditBindingSource.DataMember = "sp06095_OM_Site_Contract_Year_Billing_Profile_Edit";
            this.sp06095OMSiteContractYearBillingProfileEditBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // BilledByPersonButtonEdit
            // 
            this.BilledByPersonButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06095OMSiteContractYearBillingProfileEditBindingSource, "BilledByPerson", true));
            this.BilledByPersonButtonEdit.EditValue = "";
            this.BilledByPersonButtonEdit.Location = new System.Drawing.Point(145, 245);
            this.BilledByPersonButtonEdit.MenuManager = this.barManager1;
            this.BilledByPersonButtonEdit.Name = "BilledByPersonButtonEdit";
            this.BilledByPersonButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to Open Select Employee screen", "choose", null, true)});
            this.BilledByPersonButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.BilledByPersonButtonEdit.Size = new System.Drawing.Size(505, 20);
            this.BilledByPersonButtonEdit.StyleController = this.dataLayoutControl1;
            this.BilledByPersonButtonEdit.TabIndex = 13;
            this.BilledByPersonButtonEdit.TabStop = false;
            this.BilledByPersonButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BilledByPersonButtonEdit_ButtonClick);
            // 
            // ActualBillAmountSpinEdit
            // 
            this.ActualBillAmountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06095OMSiteContractYearBillingProfileEditBindingSource, "ActualBillAmount", true));
            this.ActualBillAmountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ActualBillAmountSpinEdit.Location = new System.Drawing.Point(145, 221);
            this.ActualBillAmountSpinEdit.MenuManager = this.barManager1;
            this.ActualBillAmountSpinEdit.Name = "ActualBillAmountSpinEdit";
            this.ActualBillAmountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActualBillAmountSpinEdit.Properties.Mask.EditMask = "c";
            this.ActualBillAmountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualBillAmountSpinEdit.Size = new System.Drawing.Size(123, 20);
            this.ActualBillAmountSpinEdit.StyleController = this.dataLayoutControl1;
            this.ActualBillAmountSpinEdit.TabIndex = 14;
            // 
            // EstimatedBillAmountSpinEdit
            // 
            this.EstimatedBillAmountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06095OMSiteContractYearBillingProfileEditBindingSource, "EstimatedBillAmount", true));
            this.EstimatedBillAmountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.EstimatedBillAmountSpinEdit.Location = new System.Drawing.Point(145, 197);
            this.EstimatedBillAmountSpinEdit.MenuManager = this.barManager1;
            this.EstimatedBillAmountSpinEdit.Name = "EstimatedBillAmountSpinEdit";
            this.EstimatedBillAmountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EstimatedBillAmountSpinEdit.Properties.Mask.EditMask = "c";
            this.EstimatedBillAmountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EstimatedBillAmountSpinEdit.Size = new System.Drawing.Size(123, 20);
            this.EstimatedBillAmountSpinEdit.StyleController = this.dataLayoutControl1;
            this.EstimatedBillAmountSpinEdit.TabIndex = 13;
            // 
            // InvoiceDateActualDateEdit
            // 
            this.InvoiceDateActualDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06095OMSiteContractYearBillingProfileEditBindingSource, "InvoiceDateActual", true));
            this.InvoiceDateActualDateEdit.EditValue = null;
            this.InvoiceDateActualDateEdit.Location = new System.Drawing.Point(145, 163);
            this.InvoiceDateActualDateEdit.MenuManager = this.barManager1;
            this.InvoiceDateActualDateEdit.Name = "InvoiceDateActualDateEdit";
            this.InvoiceDateActualDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.InvoiceDateActualDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InvoiceDateActualDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InvoiceDateActualDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.InvoiceDateActualDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.InvoiceDateActualDateEdit.Size = new System.Drawing.Size(123, 20);
            this.InvoiceDateActualDateEdit.StyleController = this.dataLayoutControl1;
            this.InvoiceDateActualDateEdit.TabIndex = 13;
            // 
            // InvoiceDateDueDateEdit
            // 
            this.InvoiceDateDueDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06095OMSiteContractYearBillingProfileEditBindingSource, "InvoiceDateDue", true));
            this.InvoiceDateDueDateEdit.EditValue = null;
            this.InvoiceDateDueDateEdit.Location = new System.Drawing.Point(145, 139);
            this.InvoiceDateDueDateEdit.MenuManager = this.barManager1;
            this.InvoiceDateDueDateEdit.Name = "InvoiceDateDueDateEdit";
            this.InvoiceDateDueDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.InvoiceDateDueDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InvoiceDateDueDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InvoiceDateDueDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.InvoiceDateDueDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.InvoiceDateDueDateEdit.Size = new System.Drawing.Size(123, 20);
            this.InvoiceDateDueDateEdit.StyleController = this.dataLayoutControl1;
            this.InvoiceDateDueDateEdit.TabIndex = 13;
            this.InvoiceDateDueDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.InvoiceDateDueDateEdit_Validating);
            // 
            // SiteContractProfileIDTextEdit
            // 
            this.SiteContractProfileIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06095OMSiteContractYearBillingProfileEditBindingSource, "SiteContractProfileID", true));
            this.SiteContractProfileIDTextEdit.Location = new System.Drawing.Point(129, 58);
            this.SiteContractProfileIDTextEdit.MenuManager = this.barManager1;
            this.SiteContractProfileIDTextEdit.Name = "SiteContractProfileIDTextEdit";
            this.SiteContractProfileIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteContractProfileIDTextEdit, true);
            this.SiteContractProfileIDTextEdit.Size = new System.Drawing.Size(528, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteContractProfileIDTextEdit, optionsSpelling2);
            this.SiteContractProfileIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteContractProfileIDTextEdit.TabIndex = 44;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06095OMSiteContractYearBillingProfileEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 139);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(614, 159);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling3);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 35;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp06095OMSiteContractYearBillingProfileEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(121, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(179, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // LinkedToParentButtonEdit
            // 
            this.LinkedToParentButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06095OMSiteContractYearBillingProfileEditBindingSource, "LinkedToParent", true));
            this.LinkedToParentButtonEdit.EditValue = "";
            this.LinkedToParentButtonEdit.Location = new System.Drawing.Point(121, 35);
            this.LinkedToParentButtonEdit.MenuManager = this.barManager1;
            this.LinkedToParentButtonEdit.Name = "LinkedToParentButtonEdit";
            this.LinkedToParentButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to Open Select Employee screen", "choose", null, true)});
            this.LinkedToParentButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.LinkedToParentButtonEdit.Size = new System.Drawing.Size(553, 20);
            this.LinkedToParentButtonEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToParentButtonEdit.TabIndex = 6;
            this.LinkedToParentButtonEdit.TabStop = false;
            this.LinkedToParentButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LinkedToParentButtonEdit_ButtonClick);
            this.LinkedToParentButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.LinkedToParentButtonEdit_Validating);
            // 
            // SiteContractYearIDTextEdit
            // 
            this.SiteContractYearIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06095OMSiteContractYearBillingProfileEditBindingSource, "SiteContractYearID", true));
            this.SiteContractYearIDTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SiteContractYearIDTextEdit.Location = new System.Drawing.Point(138, 82);
            this.SiteContractYearIDTextEdit.MenuManager = this.barManager1;
            this.SiteContractYearIDTextEdit.Name = "SiteContractYearIDTextEdit";
            this.SiteContractYearIDTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.SiteContractYearIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.SiteContractYearIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteContractYearIDTextEdit, true);
            this.SiteContractYearIDTextEdit.Size = new System.Drawing.Size(519, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteContractYearIDTextEdit, optionsSpelling4);
            this.SiteContractYearIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteContractYearIDTextEdit.TabIndex = 27;
            // 
            // BilledByPersonIDTextEdit
            // 
            this.BilledByPersonIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06095OMSiteContractYearBillingProfileEditBindingSource, "BilledByPersonID", true));
            this.BilledByPersonIDTextEdit.Location = new System.Drawing.Point(138, 15);
            this.BilledByPersonIDTextEdit.MenuManager = this.barManager1;
            this.BilledByPersonIDTextEdit.Name = "BilledByPersonIDTextEdit";
            this.BilledByPersonIDTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.BilledByPersonIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.BilledByPersonIDTextEdit, true);
            this.BilledByPersonIDTextEdit.Size = new System.Drawing.Size(519, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.BilledByPersonIDTextEdit, optionsSpelling5);
            this.BilledByPersonIDTextEdit.StyleController = this.dataLayoutControl1;
            this.BilledByPersonIDTextEdit.TabIndex = 15;
            // 
            // ItemForBilledByPersonID
            // 
            this.ItemForBilledByPersonID.Control = this.BilledByPersonIDTextEdit;
            this.ItemForBilledByPersonID.CustomizationFormText = "Billed By Person ID:";
            this.ItemForBilledByPersonID.Location = new System.Drawing.Point(0, 23);
            this.ItemForBilledByPersonID.Name = "ItemForBilledByPersonID";
            this.ItemForBilledByPersonID.Size = new System.Drawing.Size(649, 24);
            this.ItemForBilledByPersonID.Text = "Billed By Person ID:";
            this.ItemForBilledByPersonID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteContractID
            // 
            this.ItemForSiteContractID.Control = this.SiteContractIDTextEdit;
            this.ItemForSiteContractID.CustomizationFormText = "Site Contract ID:";
            this.ItemForSiteContractID.Location = new System.Drawing.Point(0, 23);
            this.ItemForSiteContractID.Name = "ItemForSiteContractID";
            this.ItemForSiteContractID.Size = new System.Drawing.Size(649, 24);
            this.ItemForSiteContractID.Text = "Site Contract ID:";
            this.ItemForSiteContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteContractYearID
            // 
            this.ItemForSiteContractYearID.Control = this.SiteContractYearIDTextEdit;
            this.ItemForSiteContractYearID.CustomizationFormText = "Site Contract Year ID:";
            this.ItemForSiteContractYearID.Location = new System.Drawing.Point(0, 70);
            this.ItemForSiteContractYearID.Name = "ItemForSiteContractYearID";
            this.ItemForSiteContractYearID.Size = new System.Drawing.Size(649, 24);
            this.ItemForSiteContractYearID.Text = "Site Contract Year ID:";
            this.ItemForSiteContractYearID.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForSiteContractProfileID
            // 
            this.ItemForSiteContractProfileID.Control = this.SiteContractProfileIDTextEdit;
            this.ItemForSiteContractProfileID.CustomizationFormText = "Site Contract Profile ID:";
            this.ItemForSiteContractProfileID.Location = new System.Drawing.Point(0, 46);
            this.ItemForSiteContractProfileID.Name = "ItemForSiteContractProfileID";
            this.ItemForSiteContractProfileID.Size = new System.Drawing.Size(649, 24);
            this.ItemForSiteContractProfileID.Text = "Site Contract Profile ID:";
            this.ItemForSiteContractProfileID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(686, 344);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLinkedToParent,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem4,
            this.layoutControlGroup6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(666, 314);
            // 
            // ItemForLinkedToParent
            // 
            this.ItemForLinkedToParent.AllowHide = false;
            this.ItemForLinkedToParent.Control = this.LinkedToParentButtonEdit;
            this.ItemForLinkedToParent.CustomizationFormText = "Client Contract Year:";
            this.ItemForLinkedToParent.Location = new System.Drawing.Point(0, 23);
            this.ItemForLinkedToParent.Name = "ItemForLinkedToParent";
            this.ItemForLinkedToParent.Size = new System.Drawing.Size(666, 24);
            this.ItemForLinkedToParent.Text = "Client Contract Year:";
            this.ItemForLinkedToParent.TextSize = new System.Drawing.Size(106, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(109, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(109, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(109, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(292, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(374, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(109, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(183, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 47);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(666, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 57);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(666, 257);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layGrpAddress;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(642, 211);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layGrpAddress,
            this.layoutControlGroup4});
            // 
            // layGrpAddress
            // 
            this.layGrpAddress.CustomizationFormText = "Address";
            this.layGrpAddress.ExpandButtonVisible = true;
            this.layGrpAddress.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layGrpAddress.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem5,
            this.ItemForInvoiceDateActual,
            this.ItemForInvoiceDateDue,
            this.emptySpaceItem9,
            this.emptySpaceItem8,
            this.ItemForEstimatedBillAmount,
            this.ItemForActualBillAmount,
            this.emptySpaceItem11,
            this.ItemForBilledByPerson,
            this.emptySpaceItem6,
            this.emptySpaceItem7});
            this.layGrpAddress.Location = new System.Drawing.Point(0, 0);
            this.layGrpAddress.Name = "layGrpAddress";
            this.layGrpAddress.Size = new System.Drawing.Size(618, 163);
            this.layGrpAddress.Text = "Details";
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 130);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(618, 33);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForInvoiceDateActual
            // 
            this.ItemForInvoiceDateActual.Control = this.InvoiceDateActualDateEdit;
            this.ItemForInvoiceDateActual.CustomizationFormText = "Invoice Date Actual:";
            this.ItemForInvoiceDateActual.Location = new System.Drawing.Point(0, 24);
            this.ItemForInvoiceDateActual.MaxSize = new System.Drawing.Size(236, 24);
            this.ItemForInvoiceDateActual.MinSize = new System.Drawing.Size(236, 24);
            this.ItemForInvoiceDateActual.Name = "ItemForInvoiceDateActual";
            this.ItemForInvoiceDateActual.Size = new System.Drawing.Size(236, 24);
            this.ItemForInvoiceDateActual.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForInvoiceDateActual.Text = "Invoice Date Actual:";
            this.ItemForInvoiceDateActual.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForInvoiceDateDue
            // 
            this.ItemForInvoiceDateDue.Control = this.InvoiceDateDueDateEdit;
            this.ItemForInvoiceDateDue.CustomizationFormText = "Invoice Date Due:";
            this.ItemForInvoiceDateDue.Location = new System.Drawing.Point(0, 0);
            this.ItemForInvoiceDateDue.MaxSize = new System.Drawing.Size(236, 24);
            this.ItemForInvoiceDateDue.MinSize = new System.Drawing.Size(236, 24);
            this.ItemForInvoiceDateDue.Name = "ItemForInvoiceDateDue";
            this.ItemForInvoiceDateDue.Size = new System.Drawing.Size(236, 24);
            this.ItemForInvoiceDateDue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForInvoiceDateDue.Text = "Invoice Date Due:";
            this.ItemForInvoiceDateDue.TextSize = new System.Drawing.Size(106, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(236, 0);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(382, 24);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(236, 24);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(382, 24);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForEstimatedBillAmount
            // 
            this.ItemForEstimatedBillAmount.Control = this.EstimatedBillAmountSpinEdit;
            this.ItemForEstimatedBillAmount.CustomizationFormText = "Estimated Bill Amount:";
            this.ItemForEstimatedBillAmount.Location = new System.Drawing.Point(0, 58);
            this.ItemForEstimatedBillAmount.MaxSize = new System.Drawing.Size(236, 24);
            this.ItemForEstimatedBillAmount.MinSize = new System.Drawing.Size(236, 24);
            this.ItemForEstimatedBillAmount.Name = "ItemForEstimatedBillAmount";
            this.ItemForEstimatedBillAmount.Size = new System.Drawing.Size(236, 24);
            this.ItemForEstimatedBillAmount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForEstimatedBillAmount.Text = "Estimated Bill Amount:";
            this.ItemForEstimatedBillAmount.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForActualBillAmount
            // 
            this.ItemForActualBillAmount.Control = this.ActualBillAmountSpinEdit;
            this.ItemForActualBillAmount.CustomizationFormText = "Actual Bill Amount:";
            this.ItemForActualBillAmount.Location = new System.Drawing.Point(0, 82);
            this.ItemForActualBillAmount.MaxSize = new System.Drawing.Size(236, 24);
            this.ItemForActualBillAmount.MinSize = new System.Drawing.Size(236, 24);
            this.ItemForActualBillAmount.Name = "ItemForActualBillAmount";
            this.ItemForActualBillAmount.Size = new System.Drawing.Size(236, 24);
            this.ItemForActualBillAmount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForActualBillAmount.Text = "Actual Bill Amount:";
            this.ItemForActualBillAmount.TextSize = new System.Drawing.Size(106, 13);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(236, 82);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(382, 24);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForBilledByPerson
            // 
            this.ItemForBilledByPerson.Control = this.BilledByPersonButtonEdit;
            this.ItemForBilledByPerson.CustomizationFormText = "Billed By Person:";
            this.ItemForBilledByPerson.Location = new System.Drawing.Point(0, 106);
            this.ItemForBilledByPerson.Name = "ItemForBilledByPerson";
            this.ItemForBilledByPerson.Size = new System.Drawing.Size(618, 24);
            this.ItemForBilledByPerson.Text = "Billed By Person:";
            this.ItemForBilledByPerson.TextSize = new System.Drawing.Size(106, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(236, 58);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(382, 24);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(618, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImage")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(618, 163);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Comments:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(618, 163);
            this.ItemForRemarks.Text = "Comments:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 314);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(666, 10);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(666, 10);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06095_OM_Site_Contract_Year_Billing_Profile_EditTableAdapter
            // 
            this.sp06095_OM_Site_Contract_Year_Billing_Profile_EditTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Site_Contract_Year_Billing_Profile_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(686, 400);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Site_Contract_Year_Billing_Profile_Edit";
            this.Text = "Edit Site Contract Year Billing Profile";
            this.Activated += new System.EventHandler(this.frm_OM_Site_Contract_Year_Billing_Profile_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Site_Contract_Year_Billing_Profile_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Site_Contract_Year_Billing_Profile_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06095OMSiteContractYearBillingProfileEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BilledByPersonButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualBillAmountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedBillAmountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceDateActualDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceDateActualDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceDateDueDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvoiceDateDueDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractProfileIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToParentButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractYearIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BilledByPersonIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBilledByPersonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractYearID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractProfileID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToParent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvoiceDateActual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvoiceDateDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedBillAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualBillAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBilledByPerson)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToParent;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.ButtonEdit LinkedToParentButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteContractYearID;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit SiteContractYearIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpAddress;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DevExpress.XtraEditors.TextEdit SiteContractProfileIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteContractProfileID;
        private DevExpress.XtraEditors.DateEdit InvoiceDateDueDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInvoiceDateDue;
        private DevExpress.XtraEditors.DateEdit InvoiceDateActualDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInvoiceDateActual;
        private DevExpress.XtraEditors.SpinEdit EstimatedBillAmountSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEstimatedBillAmount;
        private DevExpress.XtraEditors.TextEdit BilledByPersonIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBilledByPersonID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraEditors.SpinEdit ActualBillAmountSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualBillAmount;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraEditors.ButtonEdit BilledByPersonButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBilledByPerson;
        private DevExpress.XtraEditors.TextEdit SiteContractIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteContractID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private System.Windows.Forms.BindingSource sp06095OMSiteContractYearBillingProfileEditBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06095_OM_Site_Contract_Year_Billing_Profile_EditTableAdapter sp06095_OM_Site_Contract_Year_Billing_Profile_EditTableAdapter;
    }
}

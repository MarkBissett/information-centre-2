using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;  // Required for Path Statement //
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;
using DevExpress.Spreadsheet;
using DevExpress.XtraTab;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using LocusEffects;
using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;

namespace WoodPlan5
{
    public partial class frm_OM_Team_Self_Billing_Finance_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        private string strConnectionStringREADONLY = "";
 
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        private Utils.enmFocusedGrid _enmFocusedGrid = Utils.enmFocusedGrid.Invoice;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewStateInvoices;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateVisit;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateHistoricalExports;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateExportVisit;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDsHistoricalExports = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs8 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        string i_str_selected_Company_IDs = "";
        string i_str_selected_Company_Names = "";
        int i_intTeams = 1;
        int i_intStaff = 1;
        int i_intSelfEmployed = 1;
        DateTime i_dtHistoricalStartDate = DateTime.MinValue;
        DateTime i_dtHistoricalEndDate = DateTime.MaxValue;

        BaseObjects.GridCheckMarksSelection selection1;

        string i_str_selected_invoice_numbers = "";
        BaseObjects.GridCheckMarksSelection selection3;

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        private string strSignaturePath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        public string i_str_SavedDirectoryName = "";
        public string i_str_SavedSelfBillingInvoiceFolder = "";
        public string i_str_SavedFinanceExportFolder = "";


        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        #endregion

        public frm_OM_Team_Self_Billing_Finance_Manager()
        {
            InitializeComponent();
        }

        private void frm_OM_Team_Self_Billing_Finance_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 7021;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;
            strConnectionStringREADONLY = GlobalSettings.ConnectionStringREADONLY;

            Set_Grid_Highlighter_Transparent(this.Controls);

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionStringREADONLY);
            try
            {
                i_str_SavedDirectoryName = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_ReportLayouts").ToString();
                if (!i_str_SavedDirectoryName.EndsWith("\\")) i_str_SavedDirectoryName += "\\";  // Add Backslash to end //
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Team Self-Billing Layouts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Report Layouts Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            try
            {
                i_str_SavedSelfBillingInvoiceFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_TeamSelfBillingInvoicesSavedFolder").ToString();
                if (!i_str_SavedSelfBillingInvoiceFolder.EndsWith("\\")) i_str_SavedSelfBillingInvoiceFolder += "\\";  // Add Backslash to end //
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Saved Team Self-Billing Invoices (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Saved Team Self-Billing Invoices Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            try
            {
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_PictureFilesFolder").ToString();
                strSignaturePath = strDefaultPath;
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Client Pictures and Job Pictures (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Linked Job Pictures Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            try
            {
                i_str_SavedFinanceExportFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_TeamSelfBillingFinanceExportFolder").ToString();
                if (!i_str_SavedFinanceExportFolder.EndsWith("\\")) i_str_SavedFinanceExportFolder += "\\";  // Add Backslash to end //
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Finance Exports (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Finance Export Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            dateEditFromDate.DateTime = DateTime.Today.AddMonths(-3);
            dateEditToDate.DateTime = DateTime.Today.AddDays(1).AddMilliseconds(-5);
            popupContainerEditDateRange.EditValue = PopupContainerEditDateRange_Get_Selected();

            // Add record selection checkboxes to popup Visit System Status grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControlCompanyFilter.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            sp01044_Core_GC_Company_ListTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp01044_Core_GC_Company_ListTableAdapter.Fill(dataSet_Common_Functionality.sp01044_Core_GC_Company_List, 0);
            gridControlCompanyFilter.ForceInitialize();

            sp06477_OM_Get_Unique_Self_Billing_Invoice_ListTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp06477_OM_Get_Unique_Self_Billing_Invoice_ListTableAdapter.Fill(dataSet_OM_Billing.sp06477_OM_Get_Unique_Self_Billing_Invoice_List, null, null);
            gridControl3.ForceInitialize();
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;

            sp06468_OM_Finance_Self_Records_Clients_To_BillTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewStateInvoices = new RefreshGridState(gridViewInvoices, "UniqueID");

            sp06469_OM_Finance_Self_Billing_Manager_VisitsTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewStateVisit = new RefreshGridState(gridViewVisit, "UniqueID");

            sp06472_OM_Finance_Self_Billing_Historical_ExportsTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewStateHistoricalExports = new RefreshGridState(gridViewHistoricalExports, "SelfBillingFinanceExportID");

            sp06473_OM_Finance_Self_Billing_Manager_Visits_On_ExportTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewStateExportVisit = new RefreshGridState(gridViewExportVisits, "UniqueID");
            
            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            Application.DoEvents();  // Allow Form time to repaint itself //
            LoadLastSavedUserScreenSettings();

        }

        private void frm_OM_Team_Self_Billing_Finance_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDsHistoricalExports))
                {
                    Load_Historical_Exports();
                }
                if (UpdateRefreshStatus == 2 || !string.IsNullOrEmpty(i_str_AddedRecordIDs2))
                {
                    Load_Self_Billing_Invoices();
                }
                if (UpdateRefreshStatus == 8 || !string.IsNullOrEmpty(i_str_AddedRecordIDs8))
                {
                    Load_Historical_Export_Visits();
                }
            }
            SetMenuStatus();
        }

        private void frm_OM_Team_Self_Billing_Finance_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CompanyFilter", i_str_selected_Company_IDs);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "TeamsTicked", i_intTeams.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "StaffTicked", i_intStaff.ToString());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SelfEmployedTicked", i_intSelfEmployed.ToString());
                
                
                default_screen_settings.SaveDefaultScreenSettings();
            }
        }

        
        public void LoadLastSavedUserScreenSettings()
        {
            bool boolFilterInPlace = false;
            // Load last used settings for current user for the screen //
            char[] delimiters = new char[] { ',' };
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Company Filter //
                int intFoundRow = 0;
                string strItemFilter = default_screen_settings.RetrieveSetting("CompanyFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControlCompanyFilter.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                            boolFilterInPlace = true;
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditCompanyFilter.Text = PopupContainerEditCompanyFilter_Get_Selected();
                }

                strItemFilter = default_screen_settings.RetrieveSetting("TeamsTicked");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    checkEditTeams.Checked = (strItemFilter == "0" ? false : true);
                    i_intTeams = (checkEditTeams.Checked ? 1 : 0);
                    if (!boolFilterInPlace && !checkEditTeams.Checked) boolFilterInPlace = true;
                }
                else
                {
                    checkEditTeams.Checked = true;
                    i_intTeams = 1;
                }

                strItemFilter = default_screen_settings.RetrieveSetting("StaffTicked");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    checkEditStaff.Checked = (strItemFilter == "0" ? false : true);
                    i_intStaff = (checkEditStaff.Checked ? 1 : 0);
                    if (!boolFilterInPlace && !checkEditStaff.Checked) boolFilterInPlace = true;
                }
                else
                {
                    checkEditStaff.Checked = false;
                    i_intStaff = 0;
                }

                strItemFilter = default_screen_settings.RetrieveSetting("SelfEmployedTicked");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    checkEditSelfEmployed.Checked = (strItemFilter == "0" ? false : true);
                    i_intSelfEmployed = (checkEditSelfEmployed.Checked ? 1 : 0);
                    if (!boolFilterInPlace && !checkEditStaff.Checked) boolFilterInPlace = true;
                }
                else
                {
                    checkEditSelfEmployed.Checked = true;
                    i_intSelfEmployed = 1;
                }

                if (boolFilterInPlace)
                {
                    // Prepare LocusEffects and add custom effect //
                    locusEffectsProvider1 = new LocusEffectsProvider();
                    locusEffectsProvider1.Initialize();
                    locusEffectsProvider1.FramesPerSecond = 30;
                    m_customArrowLocusEffect1 = new ArrowLocusEffect();
                    m_customArrowLocusEffect1.Name = "CustomeArrow1";
                    m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                    m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                    m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                    m_customArrowLocusEffect1.MovementCycles = 20;
                    m_customArrowLocusEffect1.MovementAmplitude = 200;
                    m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                    m_customArrowLocusEffect1.LeadInTime = 0; //msec
                    m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                    m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                    locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                    Point location = dockPanelFilters.PointToScreen(Point.Empty);
                    System.Drawing.Point screenPoint = new System.Drawing.Point(location.X + 10, location.Y + 5);
                    locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
                }
                // Following commented out - asked for by Laura Cain 28/11/2017 - JIRA item: ICE-469 //
                //Load_Self_Billing_Invoices();  // Load records //
                //Load_Historical_Exports();  // Load records //
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs8)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDsHistoricalExports = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs8 != "") i_str_AddedRecordIDs8 = strNewIDs8;
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            if (_enmFocusedGrid == Utils.enmFocusedGrid.Invoice)
            {
                view = (GridView)gridControlInvoices.MainView;
            }
            else if (_enmFocusedGrid == Utils.enmFocusedGrid.Visit)
            {
                view = (GridView)gridControlVisit.MainView;
            }
            else if (_enmFocusedGrid == Utils.enmFocusedGrid.FinanceExport)
            {
                view = (GridView)gridControlHistoricalExports.MainView;
            }
            else if (_enmFocusedGrid == Utils.enmFocusedGrid.FinanceVisitExport)
            {
                view = (GridView)gridControlExportVisits.MainView;
            }
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();

            if (_enmFocusedGrid == Utils.enmFocusedGrid.FinanceExport)
            {
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            view = (GridView)gridControlHistoricalExports.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlHistoricalExports.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);

            view = (GridView)gridControlExportVisits.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlExportVisits.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0);
        }


        #region Data Filter Panel


        private void btnCompanyFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditCompanyFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditCompanyFilter_Get_Selected();
        }

        private string PopupContainerEditCompanyFilter_Get_Selected()
        {
            i_str_selected_Company_IDs = "";    // Reset any prior values first //
            i_str_selected_Company_Names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControlCompanyFilter.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_Company_IDs = "";
                return "No Company Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_Company_IDs = "";
                return "No Company Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_Company_IDs += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_Company_Names = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_Company_Names += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_Company_Names;
        }

        private void checkEditTeams_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (!ce.Checked)
            {
                checkEditSelfEmployed.Checked = false;
                checkEditSelfEmployed.ReadOnly = true;
            }
            else
            {
                checkEditSelfEmployed.ReadOnly = false;
            }
        }



        private void btnLoad_Click(object sender, EventArgs e)
        {
            Load_Self_Billing_Invoices();
        }

        #endregion


        private void Load_Self_Billing_Invoices()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            i_intTeams = (checkEditTeams.Checked ? 1 : 0);
            i_intStaff = (checkEditStaff.Checked ? 1 : 0);
            i_intSelfEmployed = (checkEditSelfEmployed.Checked ? 1 : 0);

            GridView view = (GridView)gridControlInvoices.MainView;
            RefreshGridViewStateInvoices.SaveViewInfo();
            view.BeginUpdate();
            try
            {
                sp06468_OM_Finance_Self_Records_Clients_To_BillTableAdapter.Fill(dataSet_OM_Billing.sp06468_OM_Finance_Self_Records_Clients_To_Bill, i_str_selected_Company_IDs, i_intStaff, i_intTeams, i_intSelfEmployed, i_str_selected_invoice_numbers.Replace(" ", ""));
            }
            catch (SqlException ex)
            {
                if (ex.Number == -2)  // SQL Server Time-Out //
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("The screen has not been able to return your Self-Billing Invoice data due to the volume of records in your request. Please filter your request by using the data filter or contact Technical Support for further help.", "Load Self-Billing Invoices", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading Self-Billing Invoice data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Self-Billing Invoices", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading Self-Billing Invoice data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Self-Billing Invoices", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            RefreshGridViewStateInvoices.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.ExpandAllGroups();
            view.EndUpdate();
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void Load_Visits()
        {
            if (splitContainerControl2.Collapsed) return;  // Don't bother loading related data as the parent panel is collapsed so the grids are invisible //

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();
            GridView view = (GridView)gridControlInvoices.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                gridControlVisit.MainView.BeginUpdate();
                dataSet_OM_Billing.sp06469_OM_Finance_Self_Billing_Manager_Visits.Clear();
                gridControlVisit.MainView.EndUpdate();
            }
            else
            {
                // Create Table parameter to pass to SP --
                DataTable dt = new DataTable();
                dt.Columns.Add("IntegerColumn1", typeof(Int32));
                dt.Columns.Add("IntegerColumn2", typeof(Int32));
                dt.Columns.Add("IntegerColumn3", typeof(Int32));

                foreach (int intRowHandle in intRowHandles)
                {
                    dt.Rows.Add(Convert.ToInt32(view.GetRowCellDisplayText(intRowHandle, view.Columns["SelfBillingInvoiceHeaderID"])), Convert.ToInt32(view.GetRowCellDisplayText(intRowHandle, view.Columns["ClientID"])), Convert.ToInt32(view.GetRowCellDisplayText(intRowHandle, view.Columns["GCCompanyID"])));
                }
                RefreshGridViewStateVisit.SaveViewInfo();  // Store expanded groups and selected rows //
                gridControlVisit.MainView.BeginUpdate();
                try
                {
                    sp06469_OM_Finance_Self_Billing_Manager_VisitsTableAdapter.Fill(dataSet_OM_Billing.sp06469_OM_Finance_Self_Billing_Manager_Visits, i_str_selected_Company_IDs, i_intStaff, i_intTeams, dt);
                }
                catch (SqlException ex)
                {
                    if (ex.Number == -2)  // SQL Server Time-Out //
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("The screen has not been able to return your Visit data due to the volume of records in your request. Please filter your request by using the data filter or contact Technical Support for further help.", "Load Visits", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading Visit data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading Visit data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }          
                RefreshGridViewStateVisit.LoadViewInfo();  // Reload any expanded groups and selected rows //
                gridControlVisit.MainView.EndUpdate();
            }
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void Load_Historical_Exports()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            GridView view = (GridView)gridControlHistoricalExports.MainView;
            RefreshGridViewStateHistoricalExports.SaveViewInfo();
            view.BeginUpdate();
            try
            {
                sp06472_OM_Finance_Self_Billing_Historical_ExportsTableAdapter.Fill(dataSet_OM_Billing.sp06472_OM_Finance_Self_Billing_Historical_Exports, i_dtHistoricalStartDate, i_dtHistoricalEndDate);
            }
            catch (SqlException ex)
            {
                if (ex.Number == -2)  // SQL Server Time-Out //
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("The screen has not been able to return your Historical Export data due to the volume of records in your request. Please filter your request by using the data filter or contact Technical Support for further help.", "Load Historical Exports", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading Historical Export data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Historical Exports", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading Historical Export data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Historical Exports", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            RefreshGridViewStateHistoricalExports.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.ExpandAllGroups();
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsHistoricalExports != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDsHistoricalExports.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControlHistoricalExports.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SelfBillingFinanceExportID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsHistoricalExports = "";
            }

            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void Load_Historical_Export_Visits()
        {
            if (splitContainerControl1.Collapsed) return;  // Don't bother loading related data as the parent panel is collapsed so the grids are invisible //

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();
            GridView view = (GridView)gridControlHistoricalExports.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                gridControlExportVisits.MainView.BeginUpdate();
                dataSet_OM_Billing.sp06473_OM_Finance_Self_Billing_Manager_Visits_On_Export.Clear();
                gridControlExportVisits.MainView.EndUpdate();
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                foreach (int intRowHandle in intRowHandles)
                {
                    sb.Append(Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["SelfBillingFinanceExportID"])) + ',');
                }
                string strIDs = sb.ToString();
                RefreshGridViewStateExportVisit.SaveViewInfo();  // Store expanded groups and selected rows //
                gridControlExportVisits.MainView.BeginUpdate();
                try
                {
                    sp06473_OM_Finance_Self_Billing_Manager_Visits_On_ExportTableAdapter.Fill(dataSet_OM_Billing.sp06473_OM_Finance_Self_Billing_Manager_Visits_On_Export, strIDs);
                }
                catch (SqlException ex)
                {
                    if (ex.Number == -2)  // SQL Server Time-Out //
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("The screen has not been able to return your Visit data due to the volume of records in your request. Please filter your request by using the data filter or contact Technical Support for further help.", "Load Visits", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading Visit data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading Visit data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                RefreshGridViewStateExportVisit.LoadViewInfo();  // Reload any expanded groups and selected rows //
                gridControlExportVisits.MainView.EndUpdate();
            }
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
        }

        private void Block_Add()
        {
        }

        private void Block_Edit()
        {
        }

        private void Edit_Record()
        {
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            StringBuilder sbRecords = new StringBuilder();
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.FinanceExport:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlHistoricalExports.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Historical Exports to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Historical Export" : Convert.ToString(intRowHandles.Length) + " Historical Exports") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Historical Export" : "these Historical Exports") + " will no longer be available for selection. Any linked visits will be un-linked and made available for re-exporting!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                sbRecords.Append(Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SelfBillingFinanceExportID")) + ",");
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString + "Connect Timeout=3600;");
                                RemoveRecords.ChangeCommandTimeout("dbo.sp06000_OM_Delete", 3600);  // Timeout on SQL Connection to 1 hour - need line above too. //               
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("self_billing_invoice_finance_export", sbRecords.ToString());  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Historical_Exports();
                            Load_Self_Billing_Invoices();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            //switch (i_int_FocusedGrid)
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    {
                        view = (GridView)gridControlVisit.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitID")) + ',';
                        }
                        var fChildForm = new frm_OM_Visit_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.FinanceVisitExport:
                    {
                        view = (GridView)gridControlExportVisits.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitID")) + ',';
                        }
                        var fChildForm = new frm_OM_Visit_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                default:
                    break;
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridControlInvoices":
                    message = "No Clients Available";
                    break;
                case "gridViewVisit":
                    message = "No Visits Available - Select one or more Self-Billing Invoices to see linked Visits";
                    break;
                case "gridViewHistoricalExports":
                    message = "No Historical Exports - Adjust the date filters then click Refresh button";
                    break;
                case "gridViewExportVisits":
                    message = "No Visits Available - Select one or more Exports to see linked Visits";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridViewInvoices":
                    Load_Visits();
                    view = (GridView)gridControlVisit.MainView;
                    view.ExpandAllGroups();
                    Set_Selected_Count0();
                    break;
                case "gridViewVisit":
                    Set_Selected_Count0();
                    break;
                case "gridViewHistoricalExports":
                    Load_Historical_Export_Visits();
                    view = (GridView)gridControlExportVisits.MainView;
                    view.ExpandAllGroups();
                    Set_Selected_Count1();
                    break;
                case "gridViewExportVisits":
                    Set_Selected_Count1();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView Invoices

        private void gridControlInvoices_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Self_Billing_Invoices();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewInvoices_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
        }

        private void gridViewInvoices_DoubleClick(object sender, EventArgs e)
        {
            /*DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }*/
        }

        private void gridViewInvoices_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Invoice;
            SetMenuStatus();
        }

        private void gridViewInvoices_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Invoice;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridViewInvoices_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "VisitCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("VisitCount")) <= 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView Visits

        private void gridControlVisit_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Historical_Export_Visits();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewVisit_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "LinkedJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedJobCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedOutstandingJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedOutstandingJobCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedCompletedJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedCompletedJobCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedDocumentCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LabourCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LabourCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedVisitCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedVisitCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "SuspendedJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SuspendedJobCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "ClientSignaturePath":
                        if (string.IsNullOrWhiteSpace(view.GetRowCellValue(e.RowHandle, "ClientSignaturePath").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedPictureCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedPictureCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "InvoicePDF":
                        if (string.IsNullOrWhiteSpace(view.GetRowCellValue(e.RowHandle, "InvoicePDF").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                    case "ClientInvoiceID":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ClientInvoiceID")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridViewVisit_DoubleClick(object sender, EventArgs e)
        {
            /*DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }*/
        }

        private void gridViewVisit_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            SetMenuStatus();
        }

        private void gridViewVisit_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewVisit_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedJobCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedOutstandingJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedOutstandingJobCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedCompletedJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedCompletedJobCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) <= 0) e.Cancel = true;
                    break;
                case "LabourCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LabourCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedVisitCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedVisitCount")) <= 0) e.Cancel = true;
                    break;
                case "SuspendedJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("SuspendedJobCount")) <= 0) e.Cancel = true;
                    break;
                case "ClientSignaturePath":
                    if (string.IsNullOrWhiteSpace(view.GetFocusedRowCellValue("ClientSignaturePath").ToString())) e.Cancel = true;
                    break;
                case "LinkedPictureCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedPictureCount")) <= 0) e.Cancel = true;
                    break;
                case "InvoicePDF":
                    if (string.IsNullOrWhiteSpace(view.GetFocusedRowCellValue("InvoicePDF").ToString())) e.Cancel = true;
                    break;
                case "ClientInvoiceID":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("ClientInvoiceID")) <= 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditGrid1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            if (view.FocusedColumn.FieldName == "LinkedPictureCount")
            {
                int intVisitID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "VisitID"));
                string strJobIDs = "";
                try
                {
                    var GetValue = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                    GetValue.ChangeConnectionString(strConnectionString);
                    strJobIDs = GetValue.sp06130_OM_Visit_Manager_Get_Linked_Job_IDs(intVisitID.ToString() + ",", 0).ToString();
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view linked pictures [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Pictures", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

                Open_Picture_Viewer(strJobIDs, Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID")), 1);
                return;
            }
            else
            {
                int intVisitID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "VisitID"));
                if (intVisitID <= 0) return;
                string strRecordIDs = "";

                int intRecordType = -1;
                switch (view.FocusedColumn.FieldName)
                {
                    case "LinkedJobCount":
                        intRecordType = 0;
                        break;
                    case "LinkedOutstandingJobCount":
                        intRecordType = 1;
                        break;
                    case "LinkedCompletedJobCount":
                        intRecordType = 2;
                        break;
                    case "SuspendedJobCount":
                        intRecordType = 3;
                        break;
                    default:
                        break;
                }
                if (intRecordType == -1) return;

                try
                {
                    string strParameter = intVisitID.ToString() + ",";
                    var GetValue = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                    GetValue.ChangeConnectionString(strConnectionStringREADONLY);
                    strRecordIDs = GetValue.sp06130_OM_Visit_Manager_Get_Linked_Job_IDs(intVisitID.ToString() + ",", intRecordType).ToString();
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view linked jobs [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_job");
            }
        }

        private void repositoryItemHyperLinkEditClientSignature_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFirstPartOfPathWithBackslash = strSignaturePath + "\\";
            string strClientPath = view.GetRowCellValue(view.FocusedRowHandle, "ImagesFolderOM").ToString() + "\\Signatures";
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "ClientSignaturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Client Signature Linked - unable to proceed.", "View Client Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(Path.Combine(strFirstPartOfPathWithBackslash + strClientPath + strFile));
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Client Signature: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Client Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void repositoryItemHyperLinkEditSelfBillingInvoice_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "InvoicePDF").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Team Self-Billing Invoice Linked - unable to proceed. Click the Re-Create PDF button.", "View Team Self-Billing Invoice", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                {
                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                    fChildForm.strPDFFile = Path.Combine(i_str_SavedSelfBillingInvoiceFolder + strFile);
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.Show();
                }
                else
                {
                    System.Diagnostics.Process.Start(Path.Combine(i_str_SavedSelfBillingInvoiceFolder + strFile));
                }
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the Team Self-Billing Invoice: " + strFile + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Team Self-Billing Invoice", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

        }

        private void repositoryItemHyperLinkEditClientInvoiceID_OpenLink(object sender, OpenLinkEventArgs e)
        {

        }


        #endregion


        private void bbiExport_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlInvoices.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("No Invoices are selected for Exporting. Please select one or more invoices before tyring again.", "Export Invoices To Spreadsheet", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strMessage = "You have " + (intCount == 1 ? "1 Invoice" : Convert.ToString(intRowHandles.Length) + " Invoices") + " selected for Exporting To Spreadsheet.\n\n<color=red>Proceed with the export?</color>";
            if (XtraMessageBox.Show(strMessage, "Export Invoices To Spreadsheet", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Exporting Data...");

            // Create Table parameter to pass to SP --
            DataTable dt = new DataTable();
            dt.Columns.Add("IntegerColumn1", typeof(Int32));
            dt.Columns.Add("IntegerColumn2", typeof(Int32));
            dt.Columns.Add("IntegerColumn3", typeof(Int32));

            string strSelfBillingInvoiceNumber = "";
            foreach (int intRowHandle in intRowHandles)
            {
                dt.Rows.Add(Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["SelfBillingInvoiceHeaderID"])), Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["ClientID"])), Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["GCCompanyID"])));
                if (strSelfBillingInvoiceNumber != "Multiple_Invoice_Numbers")
                {
                    if (string.IsNullOrWhiteSpace(strSelfBillingInvoiceNumber))
                    {
                        strSelfBillingInvoiceNumber = view.GetRowCellValue(intRowHandle, view.Columns["SelfBillingInvoiceNumber"]).ToString();
                    }
                    else
                    {
                        if (strSelfBillingInvoiceNumber != view.GetRowCellValue(intRowHandle, view.Columns["SelfBillingInvoiceNumber"]).ToString())
                        {
                            strSelfBillingInvoiceNumber = "Multiple_Invoice_Numbers";
                        }
                    }
                }
            }

            string strFileName = "";
            try
            {
                DateTime dtCurrentDateTime = DateTime.Now;
                int intCompanyID = 0;

                SqlDataAdapter sdaData = null;
                DataSet dsData = null;
                using (SqlConnection conn = new SqlConnection(strConnectionString + "Connect Timeout=3600;"))  // Timeout on SQL Connection to 1 hour - need line further down too. //
                {
                    conn.Open();
                    sdaData = new SqlDataAdapter();
                    dsData = new DataSet("NewDataSet");

                    SqlCommand cmd = new SqlCommand("sp06470_OM_Finance_Self_Bill_Export", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@CompanyIDs", i_str_selected_Company_IDs));
                    cmd.Parameters.Add(new SqlParameter("@IncludeStaff", i_intStaff));
                    cmd.Parameters.Add(new SqlParameter("@IncludeTeams", i_intTeams));
                    cmd.Parameters.Add(new SqlParameter("@IncludeSelfEmployed", i_intSelfEmployed));
                    cmd.Parameters.Add(new SqlParameter("@SelfBillingInvoiceNumbers",  i_str_selected_invoice_numbers.Replace(" ", ""))); 
                    cmd.Parameters.Add(new SqlParameter("@TableParentIDs", dt));
                    cmd.CommandTimeout = 3600; // Timeout on SQL Connection to 1 hour - need line further up too. //

                    sdaData = new SqlDataAdapter(cmd);
                    sdaData.Fill(dsData, "Table");
                }
                //strFileName = "SB" + BaseObjects.DateFunctions.GetIso8601WeekOfYear(DateTime.Today).ToString().PadLeft(2, '0') + DateTime.Today.Year.ToString().Substring(DateTime.Today.Year.ToString().Length - 2);
                strFileName = strSelfBillingInvoiceNumber;

                if (selection1.SelectedCount != 1)  // More than 1 company selected //
                {
                    strFileName = "Multi - " + strFileName + " - " + DateTime.Now.ToString("yyyy-MM-dd_HH_mm_ss") + ".csv";
                }
                else
                {
                    // Get prefix from selected company //
                    string strPrefix = "";
                    GridView viewFilter = (GridView)gridControlCompanyFilter.MainView;
                    int intFoundRow = 0;
                    char[] delimiters = new char[] { ',' };
                    Array arrayItems = i_str_selected_Company_IDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);  // Single quotes because char expected for delimeter //
                    if (arrayItems.Length == 1)
                    {
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(arrayItems.GetValue(0).ToString()));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            strPrefix = viewFilter.GetRowCellValue(intFoundRow, "OMSelfBillingFinanceExportNamePrefix").ToString();
                            if (!string.IsNullOrWhiteSpace(strPrefix)) strPrefix += " - ";
                            intCompanyID = Convert.ToInt32(viewFilter.GetRowCellValue(intFoundRow, "ID"));
                        }
                    }
                    strFileName = strPrefix + strFileName + " - " + dtCurrentDateTime.ToString("yyyy-MM-dd_HH_mm_ss") + ".csv";
                }

                DevExpress.Spreadsheet.Workbook destWorkBook = new DevExpress.Spreadsheet.Workbook();
                destWorkBook.CreateNewDocument();

                destWorkBook.Worksheets.Add().Name = "Exchequer Export";

                destWorkBook.Worksheets["Exchequer Export"].Import(dsData.Tables[0], true, 0, 0);
                destWorkBook.Worksheets["Exchequer Export"].FreezeRows(0);
                destWorkBook.Worksheets["Exchequer Export"].Range["C2:C" + (dsData.Tables[0].Rows.Count + 1).ToString()].NumberFormat = "dd/mm/yyyy";  // Format the column into British DateTime format //
                destWorkBook.Worksheets.RemoveAt(0);  // Delete Sheet 0 from spreadsheet //
                destWorkBook.SaveDocument(Path.Combine(i_str_SavedFinanceExportFolder, strFileName), DocumentFormat.Csv);
                destWorkBook.Dispose();

                // Create Export Header record and link Visit Calculcated Labour to it so it is not picked up by subsequent exports //
                int intNewID = 0;
                using (var CreateHeader = new DataSet_OM_BillingTableAdapters.QueriesTableAdapter())
                {
                    CreateHeader.ChangeConnectionString(strConnectionString + "Connect Timeout=3600;");
                    CreateHeader.ChangeCommandTimeout("dbo.sp06471_OM_Finance_Self_Billing_Create_Export_Header", 3600);  // Timeout on SQL Connection to 1 hour - need line above too. //               
                    intNewID = Convert.ToInt32(CreateHeader.sp06471_OM_Finance_Self_Billing_Create_Export_Header(i_str_selected_Company_IDs, i_intStaff, i_intTeams, dtCurrentDateTime, GlobalSettings.UserID, strFileName, intCompanyID, dt));
                }
                if (intNewID > 0) i_str_AddedRecordIDsHistoricalExports = intNewID.ToString() + ";";  // Make sure new export is selected on load of data //

                // Check if Date Range filter covers todays date - if not, adjust it so it does //
                bool boolDatesAdjusted = false;
                if (dtCurrentDateTime < i_dtHistoricalStartDate)
                {
                    dateEditFromDate.DateTime = dtCurrentDateTime.Date.AddDays(-1);
                    boolDatesAdjusted = true;
                }
                if (dtCurrentDateTime > i_dtHistoricalEndDate)
                {
                    dateEditToDate.DateTime = dtCurrentDateTime.Date.AddDays(2);
                    boolDatesAdjusted = true;
                }
                if (boolDatesAdjusted) PopupContainerEditDateRange_Get_Selected();
                Load_Self_Billing_Invoices();
                Load_Historical_Exports();
                xtraTabControl1.SelectedTabPage = xtraTabPageHistoricalDataExtracts;
            }
            catch (Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }

                XtraMessageBox.Show("An error occurred while Exporting the data to spreadsheet.\n\nError: " + ex.Message + "\n\nPlease try again. If the problem persists contact Technical Support.", "Export Invoices To Spreadsheet", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
            try
            {
                System.Diagnostics.Process.Start(Path.Combine(i_str_SavedFinanceExportFolder, strFileName));
            }
            catch (Exception) { }
        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            Load_Self_Billing_Invoices();
        }
       
        #region Invoice Number Filter Panel

        private void btnInvoiceNumber_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditInvoiceNumber_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                i_str_selected_invoice_numbers = "";
                selection3.ClearSelection();
                popupContainerEditInvoiceNumber.Text = popupContainerEditInvoiceNumber_Get_Selected();
            }
        }

        private void popupContainerEditInvoiceNumber_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = popupContainerEditInvoiceNumber_Get_Selected();
        }

        private string popupContainerEditInvoiceNumber_Get_Selected()
        {
            i_str_selected_invoice_numbers = "";
            GridView view = (GridView)gridControl3.MainView;
            if (view.DataRowCount <= 0 || selection3.SelectedCount <= 0)
            {
                return "No Invoice Number Filter";
            }
            else
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_invoice_numbers += ", " + Convert.ToString(view.GetRowCellValue(i, "SelfBillingInvoiceNumber"));
                    }
                    if (i_str_selected_invoice_numbers.StartsWith(", ")) i_str_selected_invoice_numbers = i_str_selected_invoice_numbers.Substring(2, i_str_selected_invoice_numbers.Length - 2);
                }
            }
            return i_str_selected_invoice_numbers;
        }

        #endregion


        #region Historical Exports Page

        private void bbiRefresh2_ItemClick(object sender, ItemClickEventArgs e)
        {
            Load_Historical_Exports();
        }

        private void btnLoadHistorical_Click(object sender, EventArgs e)
        {
            Load_Historical_Exports();
        }

        private void bciFilterExportsSelected_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlHistoricalExports.MainView;
            if (bciFilterExportsSelected.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more Export records to filter by before proceeding.", "Filter Selected Export Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControlHistoricalExports.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
                gridControlHistoricalExports.EndUpdate();
            }
            else  // Clear Filter //
            {
                gridControlHistoricalExports.BeginUpdate();
                try
                {
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_OM_Billing.sp06472_OM_Finance_Self_Billing_Historical_Exports.Rows)
                    {
                        dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }

            }
            gridControlHistoricalExports.EndUpdate();
        }

        private void bciFilterVisitsSelected2_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlExportVisits.MainView;
            if (bciFilterVisitsSelected2.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more Visit records to filter by before proceeding.", "Filter Selected Visit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControlExportVisits.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
                gridControlExportVisits.EndUpdate();
            }
            else  // Clear Filter //
            {
                gridControlExportVisits.BeginUpdate();
                try
                {
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_OM_Billing.sp06473_OM_Finance_Self_Billing_Manager_Visits_On_Export.Rows)
                    {
                        dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControlExportVisits.EndUpdate();
        }


        #region Date Range \ From Today Filter Panel

        private void btnDateRangeOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }
        private void popupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }
        private string PopupContainerEditDateRange_Get_Selected()
        {
            string strValue = "";
            if (dateEditFromDate.DateTime == DateTime.MinValue)
            {
                i_dtHistoricalStartDate = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtHistoricalStartDate = dateEditFromDate.DateTime;
            }
            if (dateEditToDate.DateTime == DateTime.MinValue)
            {
                i_dtHistoricalEndDate = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtHistoricalEndDate = dateEditToDate.DateTime;
            }
            if (dateEditFromDate.DateTime == DateTime.MinValue && dateEditToDate.DateTime == DateTime.MinValue)
            {
                strValue = "No Date Range Filter";
            }
            else
            {
                strValue = (i_dtHistoricalStartDate == new DateTime(1900, 1, 1) ? "No Start" : i_dtHistoricalStartDate.ToString("dd/MM/yyyy HH:mm")) + " - " + (i_dtHistoricalEndDate == new DateTime(1900, 1, 1) ? "No End" : i_dtHistoricalEndDate.ToString("dd/MM/yyyy HH:mm"));
            }
            return strValue;
        }

        #endregion


        #region GridView Historical Exports

        private void gridControlHistoricalExports_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Historical_Exports();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewHistoricalExports_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {

        }

        private void gridViewHistoricalExports_DoubleClick(object sender, EventArgs e)
        {
            /*DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }*/
        }

        private void gridViewHistoricalExports_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {

        }

        private void gridViewHistoricalExports_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.FinanceExport;
            SetMenuStatus();
        }

        private void gridViewHistoricalExports_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.FinanceExport;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemHyperLinkEditExportFile_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "ExportFile").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                XtraMessageBox.Show("No Export File Linked - unable to proceed.", "View Export File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(Path.Combine(i_str_SavedFinanceExportFolder + strFile));
            }
            catch
            {
                XtraMessageBox.Show("An error occurred while attempting to view the Export File: " + strFile + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Export File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #region GridView Export Visits

        private void gridControlExportVisits_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Historical_Export_Visits();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewExportVisits_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //
            GridView view = (GridView)sender;
            if (e.Column.ColumnType != typeof(DateTime))
            {
                GridColumnCollection cols = new GridColumnCollection(view);
                GridColumn column = cols.AddField(e.Column.FieldName);
                RepositoryItemComboBox comboBox = new RepositoryItemComboBox();
                object[] values = view.DataController.FilterHelper.GetUniqueColumnValues(view.DataController.Columns[column.ColumnHandle], view.OptionsFilter.ColumnFilterPopupMaxRecordsCount, false, true, null);
                if (values != null)
                {
                    comboBox.Items.AddRange(values);
                    column.ColumnEdit = comboBox;
                }
                DevExpress.XtraGrid.Filter.FilterCustomDialog dlg = new DevExpress.XtraGrid.Filter.FilterCustomDialog(column, false);
                dlg.ShowDialog();
                e.FilterInfo = null;
                e.Handled = true;
                view.GridControl.Refresh();
            }
        }

        private void gridViewExportVisits_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "LinkedJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedJobCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedOutstandingJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedOutstandingJobCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedCompletedJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedCompletedJobCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedDocumentCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LabourCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LabourCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedVisitCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedVisitCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "SuspendedJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SuspendedJobCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "ClientSignaturePath":
                        if (string.IsNullOrWhiteSpace(view.GetRowCellValue(e.RowHandle, "ClientSignaturePath").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedPictureCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedPictureCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "InvoicePDF":
                        if (string.IsNullOrWhiteSpace(view.GetRowCellValue(e.RowHandle, "InvoicePDF").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                    case "ClientInvoiceID":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ClientInvoiceID")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridViewExportVisits_DoubleClick(object sender, EventArgs e)
        {
            /*DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }*/
        }

        private void gridViewExportVisits_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            // The code in this event only fires if the Ctrl Key is held down by the end user when the Filter Editor is activated //
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //        
            if (Control.ModifierKeys != Keys.Control) return;  // CTRL key not held down so abort //
            GridView view = (GridView)sender;
            List<RepositoryItemComboBox> myRICBlist = new List<RepositoryItemComboBox>();
            foreach (GridColumn col in view.Columns)
            {
                if (col.Visible == false) continue;
                RepositoryItemComboBox comboBox = new RepositoryItemComboBox();
                object[] values = view.DataController.FilterHelper.GetUniqueColumnValues(view.DataController.Columns[col.ColumnHandle], view.OptionsFilter.ColumnFilterPopupMaxRecordsCount, false, true, null);
                if (values == null || col.ColumnType.ToString() == "System.DateTime" || col.ColumnType.ToString() == "DateTime") continue;
                comboBox.Items.AddRange(values);
                myRICBlist.Add(comboBox);
                e.FilterControl.FilterColumns[col.FieldName].SetColumnEditor(myRICBlist[myRICBlist.Count - 1]);
            }
        }

        private void gridViewExportVisits_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.FinanceVisitExport;
            SetMenuStatus();
        }

        private void gridViewExportVisits_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.FinanceVisitExport;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewExportVisits_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedJobCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedOutstandingJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedOutstandingJobCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedCompletedJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedCompletedJobCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) <= 0) e.Cancel = true;
                    break;
                case "LabourCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LabourCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedVisitCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedVisitCount")) <= 0) e.Cancel = true;
                    break;
                case "SuspendedJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("SuspendedJobCount")) <= 0) e.Cancel = true;
                    break;
                case "ClientSignaturePath":
                    if (string.IsNullOrWhiteSpace(view.GetFocusedRowCellValue("ClientSignaturePath").ToString())) e.Cancel = true;
                    break;
                case "LinkedPictureCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedPictureCount")) <= 0) e.Cancel = true;
                    break;
                case "InvoicePDF":
                    if (string.IsNullOrWhiteSpace(view.GetFocusedRowCellValue("InvoicePDF").ToString())) e.Cancel = true;
                    break;
                case "ClientInvoiceID":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("ClientInvoiceID")) <= 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            if (view.FocusedColumn.FieldName == "LinkedPictureCount")
            {
                int intVisitID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "VisitID"));
                string strJobIDs = "";
                try
                {
                    var GetValue = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                    GetValue.ChangeConnectionString(strConnectionString);
                    strJobIDs = GetValue.sp06130_OM_Visit_Manager_Get_Linked_Job_IDs(intVisitID.ToString() + ",", 0).ToString();
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view linked pictures [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Pictures", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

                Open_Picture_Viewer(strJobIDs, Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID")), 1);
                return;
            }
            else
            {
                int intVisitID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "VisitID"));
                if (intVisitID <= 0) return;
                string strRecordIDs = "";

                int intRecordType = -1;
                switch (view.FocusedColumn.FieldName)
                {
                    case "LinkedJobCount":
                        intRecordType = 0;
                        break;
                    case "LinkedOutstandingJobCount":
                        intRecordType = 1;
                        break;
                    case "LinkedCompletedJobCount":
                        intRecordType = 2;
                        break;
                    case "SuspendedJobCount":
                        intRecordType = 3;
                        break;
                    default:
                        break;
                }
                if (intRecordType == -1) return;

                try
                {
                    string strParameter = intVisitID.ToString() + ",";
                    var GetValue = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                    GetValue.ChangeConnectionString(strConnectionStringREADONLY);
                    strRecordIDs = GetValue.sp06130_OM_Visit_Manager_Get_Linked_Job_IDs(intVisitID.ToString() + ",", intRecordType).ToString();
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view linked jobs [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_job");
            }
        }

        private void repositoryItemHyperLinkEditClientSignature2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFirstPartOfPathWithBackslash = strSignaturePath + "\\";
            string strClientPath = view.GetRowCellValue(view.FocusedRowHandle, "ImagesFolderOM").ToString() + "\\Signatures";
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "ClientSignaturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Client Signature Linked - unable to proceed.", "View Client Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(Path.Combine(strFirstPartOfPathWithBackslash + strClientPath + strFile));
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Client Signature: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Client Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void repositoryItemHyperLinkEditSelfBillingInvoicePDF_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "InvoicePDF").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Team Self-Billing Invoice Linked - unable to proceed. Click the Re-Create PDF button.", "View Team Self-Billing Invoice", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                {
                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                    fChildForm.strPDFFile = Path.Combine(i_str_SavedSelfBillingInvoiceFolder + strFile);
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.Show();
                }
                else
                {
                    System.Diagnostics.Process.Start(Path.Combine(i_str_SavedSelfBillingInvoiceFolder + strFile));
                }
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the Team Self-Billing Invoice: " + strFile + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Team Self-Billing Invoice", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

        }

        private void repositoryItemHyperLinkEditClientInvoiceID2_OpenLink(object sender, OpenLinkEventArgs e)
        {

        }

        #endregion


        #endregion


        private void Set_Selected_Count0()
        {
            GridView view = (GridView)gridControlInvoices.MainView;
            int intSelectedCount = view.GetSelectedRows().Length;
            view = (GridView)gridControlVisit.MainView;
            int intSelectedCount2 = view.GetSelectedRows().Length;
            if (intSelectedCount == 0 && intSelectedCount2 == 0)
            {
                bsiSelectedCount0.Visibility = BarItemVisibility.Never;
            }
            else
            {
                bsiSelectedCount0.Visibility = BarItemVisibility.Always;
                string strText = "";
                if (intSelectedCount > 0)
                {
                    strText = (intSelectedCount == 1 ? "1 Invoice Selected" : "<color=red>" + intSelectedCount.ToString() + " Invoices</Color> Selected");
                }
                if (intSelectedCount > 0 && intSelectedCount2 > 0) strText += "<br>";
                if (intSelectedCount2 > 0)
                {
                    strText += (intSelectedCount2 == 1 ? "1 Visit Selected" : "<color=red>" + intSelectedCount2.ToString() + " Visits</Color> Selected");
                }
                bsiSelectedCount0.Caption = strText;
            }
            bbiExport.Enabled = (intSelectedCount > 0 ? true : false);
        }
        private void Set_Selected_Count1()
        {
            GridView view = (GridView)gridControlHistoricalExports.MainView;
            int intSelectedCount = view.GetSelectedRows().Length;
            view = (GridView)gridControlExportVisits.MainView;
            int intSelectedCount2 = view.GetSelectedRows().Length;
            if (intSelectedCount == 0 && intSelectedCount2 == 0)
            {
                bsiSelectedCount1.Visibility = BarItemVisibility.Never;
            }
            else
            {
                bsiSelectedCount1.Visibility = BarItemVisibility.Always;
                string strText = "";
                if (intSelectedCount > 0)
                {
                    strText = (intSelectedCount == 1 ? "1 Historical Extract Selected" : "<color=red>" + intSelectedCount.ToString() + " Historical Extracts</Color> Selected");
                }
                if (intSelectedCount > 0 && intSelectedCount2 > 0) strText += "<br>";
                if (intSelectedCount2 > 0)
                {
                    strText += (intSelectedCount2 == 1 ? "1 Visit Selected" : "<color=red>" + intSelectedCount2.ToString() + " Visits</Color> Selected");
                }
                bsiSelectedCount1.Caption = strText;
            }
        }

        private void bciFilterInvoicesSelected_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlInvoices.MainView;
            if (bciFilterInvoicesSelected.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more Invoice records to filter by before proceeding.", "Filter Selected Invoice Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControlInvoices.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
                gridControlInvoices.EndUpdate();
            }
            else  // Clear Filter //
            {
                gridControlInvoices.BeginUpdate();
                try
                {
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_OM_Billing.sp06468_OM_Finance_Self_Records_Clients_To_Bill.Rows)
                    {
                        dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControlInvoices.EndUpdate();
        }

        private void bciFilterVisitsSelected_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlVisit.MainView;
            if (bciFilterVisitsSelected.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more Visit records to filter by before proceeding.", "Filter Selected Visit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControlVisit.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
                gridControlVisit.EndUpdate();
            }
            else  // Clear Filter //
            {
                gridControlVisit.BeginUpdate();
                try
                {
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_OM_Billing.sp06469_OM_Finance_Self_Billing_Manager_Visits.Rows)
                    {
                        dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControlVisit.EndUpdate();
        }

        private void xtraTabControl1_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
            switch (xtraTabControl1.SelectedTabPage.Name)
            {
                case "xtraTabPageTeamSelfBillingInvoices":
                    {
                        tabbedControlGroupFilters.SelectedTabPage = layoutControlGroupDataExport;
                    }
                    break;
                case "xtraTabPageHistoricalDataExtracts":
                    {
                        tabbedControlGroupFilters.SelectedTabPage = layoutControlGroupHistoricalExports;
                    }
                    break;
            }
        }


        private void splitContainerControl2_SplitGroupPanelCollapsed(object sender, SplitGroupPanelCollapsedEventArgs e)
        {
            if (!e.Collapsed)
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                Load_Visits();
            }
        }

        private void splitContainerControl1_SplitGroupPanelCollapsed(object sender, SplitGroupPanelCollapsedEventArgs e)
        {
            if (!e.Collapsed)
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                Load_Historical_Export_Visits();
            }
        }


        private void Open_Picture_Viewer(string RecordIDs, int ClientID, int RecordTypeID)
        {
            var fChildForm = new frm_OM_Picture_Viewer();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._PassedInRecordIDs = RecordIDs;
            fChildForm._PassedInRecordTypeID = RecordTypeID;
            fChildForm._ClientID = ClientID;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();
            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
            return;
        }



 

    }
}


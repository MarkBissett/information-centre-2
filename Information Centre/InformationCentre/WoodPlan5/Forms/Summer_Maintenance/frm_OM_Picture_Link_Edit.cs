using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_OM_Picture_Link_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        public int intPictureTypeID = 0;
        private bool ibool_FormEditingCancelled = false;
        
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        public int intRecordTypeID = 0;
        public string strRecordTypeDescription = "";
        public int intRecordSubTypeID = 0;
        public string strRecordSubTypeDescription = "";
        public string strImagesFolderOM = "";
        
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        
        #endregion

        public frm_OM_Picture_Link_Edit()
        {
            InitializeComponent();
        }

        private void frm_OM_Picture_Link_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 500196;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_PictureFilesFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Picture path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Picture path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            sp06241_OM_Picture_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            try
            {
                sp06241_OM_Picture_Types_With_BlankTableAdapter.Fill(dataSet_OM_Job.sp06241_OM_Picture_Types_With_Blank, 1);
            }
            catch (Exception) { }

            // Populate Main Dataset //
            sp06239_OM_Picture_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_OM_Job.sp06239_OM_Picture_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["PictureID"] = 0;
                        drNewRow["LinkedToRecordID"] = intLinkedToRecordID;
                        drNewRow["LinkedRecordDescription"] = strLinkedToRecordDesc;
                        drNewRow["LinkedToRecordTypeID"] = intRecordTypeID;
                        drNewRow["LinkedToRecordType"] = strRecordTypeDescription;
                        drNewRow["PictureTypeID"] = intPictureTypeID;
                        drNewRow["PictureType"] = "";
                        drNewRow["AddedByStaffID"] = this.GlobalSettings.UserID;
                        drNewRow["AddedByStaffName"] = this.GlobalSettings.UserSurname + ": " + this.GlobalSettings.UserForename;
                        drNewRow["DateTimeTaken"] = DateTime.Now;
                        drNewRow["ImagesFolderOM"] = strImagesFolderOM;
                        dataSet_OM_Job.sp06239_OM_Picture_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception) { }
                    break;
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_OM_Job.sp06239_OM_Picture_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["LinkedToRecordID"] = 0;
                        drNewRow["LinkedRecordDescription"] = "Not Applicable - Block Adding";
                        drNewRow["LinkedToRecordTypeID"] = intRecordTypeID;
                        drNewRow["LinkedToRecordType"] = strRecordTypeDescription;
                        drNewRow["PictureTypeID"] = intPictureTypeID;
                        drNewRow["PictureType"] = "";
                        drNewRow["AddedByStaffID"] = this.GlobalSettings.UserID;
                        drNewRow["AddedByStaffName"] = this.GlobalSettings.UserSurname + ": " + this.GlobalSettings.UserForename;
                        drNewRow["DateTimeTaken"] = DateTime.Now;
                        drNewRow["ImagesFolderOM"] = strImagesFolderOM;
                        dataSet_OM_Job.sp06239_OM_Picture_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception) { }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_OM_Job.sp06239_OM_Picture_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["PictureID"] = 0;
                        drNewRow["LinkedToRecordID"] = 0;
                        drNewRow["LinkedToRecordTypeID"] = 0;
                        dataSet_OM_Job.sp06239_OM_Picture_Edit.Rows.Add(drNewRow);
                        dataSet_OM_Job.sp06239_OM_Picture_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception) { }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp06239_OM_Picture_EditTableAdapter.Fill(dataSet_OM_Job.sp06239_OM_Picture_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception) { }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_OM_Job.sp06239_OM_Picture_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        LinkedRecordDescriptionButtonEdit.Focus();

                        LinkedRecordDescriptionButtonEdit.Properties.ReadOnly = false;
                        LinkedRecordDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        LinkedRecordDescriptionButtonEdit.Focus();

                        LinkedRecordDescriptionButtonEdit.Properties.ReadOnly = true;
                        LinkedRecordDescriptionButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        PicturePathButtonEdit.Focus();

                        LinkedRecordDescriptionButtonEdit.Properties.ReadOnly = false;
                        LinkedRecordDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        PicturePathButtonEdit.Focus();

                        LinkedRecordDescriptionButtonEdit.Properties.ReadOnly = true;
                        LinkedRecordDescriptionButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
            ibool_ignoreValidation = false;
            ibool_FormStillLoading = false;
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_OM_Job.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        private void frm_OM_Picture_Link_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_OM_Picture_Link_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            sp06239OMPictureEditBindingSource.EndEdit();
            try
            {
                sp06239_OM_Picture_EditTableAdapter.Update(dataSet_OM_Job);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                var currentRowView = (DataRowView)sp06239OMPictureEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06239_OM_Picture_EditRow)currentRowView.Row;
                this.strFormMode = "edit";  // Switch mode to Edit so than any subsequent changes update this record //
                if (currentRow != null)
                {
                    currentRow.strMode = "edit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    strNewIDs = currentRow.PictureID + ";";
                }
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                var currentRowView = (DataRowView)sp06239OMPictureEditBindingSource.Current;
                var currentRow = (DataSet_OM_Job.sp06239_OM_Picture_EditRow)currentRowView.Row;
                if (currentRow != null)
                {
                    currentRow.strMode = "blockedit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Values returned from Update SP //
                }
           }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_OM_Job_Manager")
                    {
                        var fParentForm = (frm_OM_Job_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(2, Classes.Operations.Utils.enmFocusedGrid.Pictures, strNewIDs);
                    }
                    else if (frmChild.Name == "frm_OM_Visit_Manager")
                    {
                        var fParentForm = (frm_OM_Visit_Manager)frmChild;
                        DataRowView currentRow = (DataRowView)sp06239OMPictureEditBindingSource.Current;
                        if (currentRow != null)
                        {
                            int RecordTypeID = (string.IsNullOrEmpty(currentRow["LinkedToRecordTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedToRecordTypeID"]));
                            switch (RecordTypeID)
                            {
                                case 1:  // Job Pictures //
                                    fParentForm.UpdateFormRefreshStatus(53, Classes.Operations.Utils.enmFocusedGrid.Pictures, strNewIDs);
                                    break;
                                case 3:  // Visit No Access Pictures //
                                    fParentForm.UpdateFormRefreshStatus(12, Classes.Operations.Utils.enmFocusedGrid.NoAccessPicture, strNewIDs);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    else if (frmChild.Name == "frm_OM_Picture_Viewer")
                    {
                        var fParentForm = (frm_OM_Picture_Viewer)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs);
                    }
                }
            }
            SetMenuStatus();  // Disables Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < dataSet_OM_Job.sp06239_OM_Picture_Edit.Rows.Count; i++)
            {
                switch (dataSet_OM_Job.sp06239_OM_Picture_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (this.strFormMode != "blockedit")
                {
                    strMessage = "You have unsaved changes on the current screen...\n\n";
                    if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                    if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                    if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                }
                else  // Block Editing //
                {
                    strMessage = "You have unsaved block edit changes on the current screen...\n";
                }
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void LinkedDocumentTypeButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp06239OMPictureEditBindingSource.Current;
                if (currentRow == null) return;
                int TypeID = (string.IsNullOrEmpty(currentRow["LinkedToRecordTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedToRecordTypeID"]));

                var fChildForm = new frm_OM_Select_Picture_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalID = TypeID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["LinkedToRecordTypeID"] = fChildForm.intSelectedID;
                    currentRow["LinkedToRecordType"] = fChildForm.strSelectedDescription1;

                    if (TypeID != fChildForm.intSelectedID)
                    {
                        currentRow["LinkedToRecordID"] = 0;
                        currentRow["LinkedRecordDescription"] = "";
                        currentRow["PictureTypeID"] = 0;
                        if (strFormMode == "add" || strFormMode == "edit") dxErrorProvider1.SetError(LinkedRecordDescriptionButtonEdit, "Select a value.");
                    }
                    sp06239OMPictureEditBindingSource.EndEdit();
                }
            }
        }
        private void LinkedDocumentTypeButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(LinkedToRecordTypeButtonEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(LinkedToRecordTypeButtonEdit, "");
            }
        }

        private void LinkedRecordDescriptionButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp06239OMPictureEditBindingSource.Current;
            if (currentRow == null) return;
            int RecordTypeID = (string.IsNullOrEmpty(currentRow["LinkedToRecordTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedToRecordTypeID"]));
            if (RecordTypeID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Linked Picture Type before proceeding.", "Link Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intOriginalID = (string.IsNullOrEmpty(currentRow["LinkedToRecordID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedToRecordID"]));
            switch (RecordTypeID)
            {
                case 1:  // Job // 
                    {
                        var fChildForm = new frm_OM_Select_Job();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalJobID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedJobID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedJobID;
                            currentRow["LinkedRecordDescription"] = "Client: " + fChildForm.strSelectedClientName + ", Site: " + fChildForm.strSelectedSiteName + ", Visit #: " + fChildForm.intSelectedVisitNumber.ToString() + ", Job: " + fChildForm.strSelectedJobTypeDescription + " - " + fChildForm.strSelectedJobSubTypeDescription;
                            currentRow["ImagesFolderOM"] = fChildForm.strImagesFolderOM;
                            sp06239OMPictureEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 3:  // Visit No Access // 
                    {
                        var fChildForm = new frm_OM_Select_Visit_No_Access();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalNoAccessID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedNoAccessID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedNoAccessID;
                            currentRow["LinkedRecordDescription"] = "Client: " + fChildForm.strSelectedClientName + ", Site: " + fChildForm.strSelectedSiteName + ", Visit #: " + fChildForm.intSelectedVisitNumber.ToString() + fChildForm.strSelectedNoAccessDescriptionFull;
                            currentRow["ImagesFolderOM"] = fChildForm.strImagesFolderOM;
                            sp06239OMPictureEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 4:  // Job Material // 
                    {
                        var fChildForm = new frm_OM_Select_Job_Material();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalMaterialUsedID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedMaterialUsedID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedMaterialUsedID;
                            currentRow["LinkedRecordDescription"] = "Client: " + fChildForm.strSelectedClientName + ", Site: " + fChildForm.strSelectedSiteName + ", Visit #: " + fChildForm.intSelectedVisitNumber.ToString() + ", Job: " + fChildForm.strSelectedJobTypeDescription + " - " + fChildForm.strSelectedJobSubTypeDescription + ", Material: " + fChildForm.strSelectedMaterialName;
                            currentRow["ImagesFolderOM"] = fChildForm.strImagesFolderOM;
                            sp06239OMPictureEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 5:  // Job Equipment // 
                    {
                        var fChildForm = new frm_OM_Select_Job_Equipment();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalEquipmentUsedID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedEquipmentUsedID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedEquipmentUsedID;
                            currentRow["LinkedRecordDescription"] = "Client: " + fChildForm.strSelectedClientName + ", Site: " + fChildForm.strSelectedSiteName + ", Visit #: " + fChildForm.intSelectedVisitNumber.ToString() + ", Job: " + fChildForm.strSelectedJobTypeDescription + " - " + fChildForm.strSelectedJobSubTypeDescription + ", Equipment: " + fChildForm.strSelectedEquipmentName;
                            currentRow["ImagesFolderOM"] = fChildForm.strImagesFolderOM;
                            sp06239OMPictureEditBindingSource.EndEdit();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        private void LinkedRecordDescriptionButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit glue = (ButtonEdit)sender;
            string strValue = glue.EditValue.ToString();
            if (this.strFormMode != "blockadd" && this.strFormMode != "blockedit" && String.IsNullOrEmpty(strValue))
            {
                dxErrorProvider1.SetError(LinkedRecordDescriptionButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(LinkedRecordDescriptionButtonEdit, "");
            }
        }
 
        private void PicturePathButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(PicturePathButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(PicturePathButtonEdit, "");
            }

        }
        private void PicturePathButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DataRowView currentRow = null;
            currentRow = (DataRowView)sp06239OMPictureEditBindingSource.Current;
            if (currentRow == null) return;
            string strImagesFolderOM = currentRow["ImagesFolderOM"].ToString();
            string strFirstPartOfPathWithBackslash = strDefaultPath + "\\";
            switch (e.Button.Tag.ToString())
            {
                case "select file":
                    {
                        if (strFormMode == "view") return;
                        using (OpenFileDialog dlg = new OpenFileDialog())
                        {
                            dlg.DefaultExt = "tab";
                            dlg.Filter = "";
                            if (strFirstPartOfPathWithBackslash != "") dlg.InitialDirectory = strFirstPartOfPathWithBackslash + strImagesFolderOM;
                            dlg.Multiselect = false;
                            dlg.ShowDialog();
                            if (dlg.FileNames.Length > 0)
                            {
                                string strTempFileName = "";
                                string strTempResult = "";
                                //string strExtension = "";
                                foreach (string filename in dlg.FileNames)
                                {
                                    if (strFirstPartOfPathWithBackslash != "")
                                    {
                                        if (!filename.ToLower().StartsWith(strFirstPartOfPathWithBackslash.ToLower() + strImagesFolderOM.ToLower()))
                                        {
                                            DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files from under the default folder [as specified in the System Configuration Screen - Default Linked Pictures Folder] or one of it's sub-folders.\n\nFile Selection Aborted!", "Select File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            break;
                                        }
                                    }
                                    strTempFileName = filename.Substring(strFirstPartOfPathWithBackslash.Length + strImagesFolderOM.Length);
                                    if (strTempResult == "")
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += strTempFileName;
                                    }
                                    else
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += "\r\n" + strTempFileName;
                                    }
                                }
                                PicturePathButtonEdit.Text = strTempResult;
                            }
                            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter so linked file can be viewed if required //
                        }
                    }
                    break;
                case "view file":
                    {
                        string strFile = currentRow["PicturePath"].ToString();

                        if (string.IsNullOrEmpty(strFile))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Link the File to be Viewed before proceeding.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        try
                        {
                            System.Diagnostics.Process.Start(Path.Combine(strFirstPartOfPathWithBackslash + strImagesFolderOM + strFile));
                        }
                        catch
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the Linked Picture: " + strFile + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
                case "edit file":
                    {
                        if (strFormMode == "view") return;
                        frm_UT_Picture_Capture fChildForm = new frm_UT_Picture_Capture();
                        fChildForm.strCaller = this.Name;
                        fChildForm.strConnectionString = strConnectionString;

                        string strFile = currentRow["PicturePath"].ToString();
                        int intCurrentID = (currentRow["PictureID"] == null ? 0 : Convert.ToInt32(currentRow["PictureID"]));
                        if (intCurrentID == 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Please save the Linked Picture Record prior to attempting to update the linked image.", "Add\\Edit Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (string.IsNullOrWhiteSpace(strFile))
                        {
                            fChildForm.strFormMode = "add";
                        }
                        else
                        {
                            fChildForm.strFormMode = "edit";
                        }

                        fChildForm.intAddToRecordID = -999;  // Calling screen will handle the saving on the image so return image and close screen //;
                        fChildForm._PassedInEditImageID = -999;
                        fChildForm._PassedInImageName = Path.Combine(strFirstPartOfPathWithBackslash + strImagesFolderOM + strFile);
                        fChildForm.intAddToRecordTypeID = 1;  // 1 = OM Job //
                        fChildForm.intCreatedByStaffID = GlobalSettings.UserID;
                        fChildForm.strDefaultPath = strFirstPartOfPathWithBackslash + strImagesFolderOM;
                        fChildForm._CalledByOperations = true;
                        fChildForm.ShowDialog();

                        Image imageCaptured = fChildForm._returnedImage;
                        if (imageCaptured == null) return;  // No image captured //
                        string strPictureRemarks = fChildForm._returnedRemarks;

                        try
                        {
                            string strSavedFileNameNoPath = "";
                            string strSaveFileName = strFirstPartOfPathWithBackslash + strImagesFolderOM;
                            if (!strSaveFileName.EndsWith("\\")) strSaveFileName += "\\";
                            string strPrefix = "OM_Job";

                            strSavedFileNameNoPath = strPrefix + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + "__" + intCurrentID.ToString() + ".jpg";
                            strSaveFileName += strSavedFileNameNoPath;
                            imageCaptured.Save(strSaveFileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                            imageCaptured = null;
                            currentRow["Remarks"] = strPictureRemarks;
                            currentRow["PicturePath"] = "\\" + strSavedFileNameNoPath;
                            sp06239OMPictureEditBindingSource.EndEdit();
                        }
                        catch
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to save the Linked Picture: " + strFile + ".\n\nPlease try again.", "Add\\Edit Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
           }
        }
        
        #endregion


        private void PictureTypeIDGridLookUpEdit_Enter(object sender, EventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp06239OMPictureEditBindingSource.Current;
            if (currentRow == null) return;
            int TypeID = (string.IsNullOrEmpty(currentRow["LinkedToRecordTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedToRecordTypeID"]));

            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            int intHeaderID = Convert.ToInt32(glue.Tag);
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[LinkedToRecordTypeID] = " + TypeID.ToString();
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }

        private void PictureTypeIDGridLookUpEdit_Leave(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();
        }






   
    }
}


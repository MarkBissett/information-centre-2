﻿namespace WoodPlan5
{
    partial class frm_OM_Site_Contract_Copy_Forward
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Site_Contract_Copy_Forward));
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.MemoEditSiteInstructions = new DevExpress.XtraEditors.MemoEdit();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bsiSiteContractCount = new DevExpress.XtraBars.BarStaticItem();
            this.bsiInformation = new DevExpress.XtraBars.BarStaticItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditContractEndDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditContractStartDate = new DevExpress.XtraEditors.DateEdit();
            this.comboBoxEditAddUnitsDescriptor = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditAddUnits = new DevExpress.XtraEditors.SpinEdit();
            this.checkEditAddValueToDates = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditStartAndEnd = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditNewActive = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.checkEditSourceInactive = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditNewYearlyPercentageIncrease = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditKeepYearlyPercentageIncrease = new DevExpress.XtraEditors.CheckEdit();
            this.spinEditYearPercentageIncreaseAmount = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditUpliftAmount = new DevExpress.XtraEditors.SpinEdit();
            this.checkEditAddUpliftAmount = new DevExpress.XtraEditors.CheckEdit();
            this.spinEditUpliftPercentage = new DevExpress.XtraEditors.SpinEdit();
            this.checkEditAddUpliftPercentage = new DevExpress.XtraEditors.CheckEdit();
            this.spinEditContractValue = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.checkEditUpliftAmount = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditNewContractValue = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditSiteInstructionsAppend = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditSiteInstructionsNew = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditHoldDayOfWeek = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditJobMaterials = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditJobEquipment = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditJobLabour = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditJobHealthAndSafety = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditJobStructure = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditVisitPersonResponsibilities = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditVisitStructure = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditDefaultMaterialCosts = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditDefaultEquipmentCosts = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditPreferredLabour = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditPersonResponsibilities = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditYearAndBillingProfile = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditSiteContractBillingRequirements = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditVisitBillingRequirements = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MemoEditSiteInstructions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditContractEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditContractEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditContractStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditContractStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditAddUnitsDescriptor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditAddUnits.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAddValueToDates.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditStartAndEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditNewActive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSourceInactive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditNewYearlyPercentageIncrease.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditKeepYearlyPercentageIncrease.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditYearPercentageIncreaseAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditUpliftAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAddUpliftAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditUpliftPercentage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAddUpliftPercentage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditContractValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditUpliftAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditNewContractValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSiteInstructionsAppend.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSiteInstructionsNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditHoldDayOfWeek.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditJobMaterials.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditJobEquipment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditJobLabour.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditJobHealthAndSafety.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditJobStructure.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditVisitPersonResponsibilities.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditVisitStructure.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDefaultMaterialCosts.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDefaultEquipmentCosts.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditPreferredLabour.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditPersonResponsibilities.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditYearAndBillingProfile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSiteContractBillingRequirements.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditVisitBillingRequirements.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(746, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 572);
            this.barDockControlBottom.Size = new System.Drawing.Size(746, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 546);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(746, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 546);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Images = this.imageCollection1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bsiInformation,
            this.bsiSiteContractCount});
            this.barManager1.MaxItemId = 32;
            this.barManager1.StatusBar = this.bar1;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // MemoEditSiteInstructions
            // 
            this.MemoEditSiteInstructions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MemoEditSiteInstructions.Location = new System.Drawing.Point(212, 27);
            this.MemoEditSiteInstructions.MenuManager = this.barManager1;
            this.MemoEditSiteInstructions.Name = "MemoEditSiteInstructions";
            this.MemoEditSiteInstructions.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.MemoEditSiteInstructions, true);
            this.MemoEditSiteInstructions.Size = new System.Drawing.Size(510, 107);
            this.scSpellChecker.SetSpellCheckerOptions(this.MemoEditSiteInstructions, optionsSpelling1);
            this.MemoEditSiteInstructions.TabIndex = 11;
            // 
            // btnOK
            // 
            this.btnOK.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.ImageIndex = 1;
            this.btnOK.ImageList = this.imageCollection1;
            this.btnOK.Location = new System.Drawing.Point(575, 543);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(78, 22);
            this.btnOK.TabIndex = 13;
            this.btnOK.Text = "<b>OK</b>";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Info_16x16.png");
            this.imageCollection1.InsertGalleryImage("apply_16x16.png", "images/actions/apply_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "apply_16x16.png");
            this.imageCollection1.InsertGalleryImage("cancel_16x16.png", "images/actions/cancel_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/cancel_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "cancel_16x16.png");
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.ImageIndex = 2;
            this.btnCancel.ImageList = this.imageCollection1;
            this.btnCancel.Location = new System.Drawing.Point(659, 543);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(78, 22);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSiteContractCount),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiInformation)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 3";
            // 
            // bsiSiteContractCount
            // 
            this.bsiSiteContractCount.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSiteContractCount.Caption = "<b>1</b> Site Contract Selected.";
            this.bsiSiteContractCount.Id = 31;
            this.bsiSiteContractCount.ImageIndex = 0;
            this.bsiSiteContractCount.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiSiteContractCount.LargeGlyph")));
            this.bsiSiteContractCount.Name = "bsiSiteContractCount";
            this.bsiSiteContractCount.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiSiteContractCount.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bsiInformation
            // 
            this.bsiInformation.Caption = "Complete the fields displayed then click OK.";
            this.bsiInformation.Id = 30;
            this.bsiInformation.ImageIndex = 0;
            this.bsiInformation.Name = "bsiInformation";
            this.bsiInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // groupControl1
            // 
            this.groupControl1.AllowHtmlText = true;
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.dateEditContractEndDate);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.dateEditContractStartDate);
            this.groupControl1.Controls.Add(this.comboBoxEditAddUnitsDescriptor);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.spinEditAddUnits);
            this.groupControl1.Controls.Add(this.checkEditAddValueToDates);
            this.groupControl1.Controls.Add(this.checkEditStartAndEnd);
            this.groupControl1.Location = new System.Drawing.Point(9, 34);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(406, 77);
            this.groupControl1.TabIndex = 15;
            this.groupControl1.Text = "Site Contract <B>Duration</b>";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(354, 29);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(28, 13);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Dates";
            // 
            // dateEditContractEndDate
            // 
            this.dateEditContractEndDate.EditValue = null;
            this.dateEditContractEndDate.Location = new System.Drawing.Point(260, 26);
            this.dateEditContractEndDate.MenuManager = this.barManager1;
            this.dateEditContractEndDate.Name = "dateEditContractEndDate";
            this.dateEditContractEndDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditContractEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditContractEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditContractEndDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditContractEndDate.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditContractEndDate.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateEditContractEndDate.Size = new System.Drawing.Size(88, 20);
            this.dateEditContractEndDate.TabIndex = 7;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(211, 29);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(43, 13);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "and End:";
            // 
            // dateEditContractStartDate
            // 
            this.dateEditContractStartDate.EditValue = null;
            this.dateEditContractStartDate.Location = new System.Drawing.Point(117, 26);
            this.dateEditContractStartDate.MenuManager = this.barManager1;
            this.dateEditContractStartDate.Name = "dateEditContractStartDate";
            this.dateEditContractStartDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditContractStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditContractStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditContractStartDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditContractStartDate.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditContractStartDate.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateEditContractStartDate.Size = new System.Drawing.Size(88, 20);
            this.dateEditContractStartDate.TabIndex = 5;
            // 
            // comboBoxEditAddUnitsDescriptor
            // 
            this.comboBoxEditAddUnitsDescriptor.EditValue = "Years";
            this.comboBoxEditAddUnitsDescriptor.Location = new System.Drawing.Point(117, 50);
            this.comboBoxEditAddUnitsDescriptor.MenuManager = this.barManager1;
            this.comboBoxEditAddUnitsDescriptor.Name = "comboBoxEditAddUnitsDescriptor";
            this.comboBoxEditAddUnitsDescriptor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditAddUnitsDescriptor.Properties.Items.AddRange(new object[] {
            "Days",
            "Weeks",
            "Months",
            "Years"});
            this.comboBoxEditAddUnitsDescriptor.Size = new System.Drawing.Size(69, 20);
            this.comboBoxEditAddUnitsDescriptor.TabIndex = 4;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(191, 54);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(147, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "To Existing Start and End Date";
            // 
            // spinEditAddUnits
            // 
            this.spinEditAddUnits.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditAddUnits.Location = new System.Drawing.Point(55, 50);
            this.spinEditAddUnits.MenuManager = this.barManager1;
            this.spinEditAddUnits.Name = "spinEditAddUnits";
            this.spinEditAddUnits.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditAddUnits.Properties.IsFloatValue = false;
            this.spinEditAddUnits.Properties.Mask.EditMask = "f0";
            this.spinEditAddUnits.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditAddUnits.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.spinEditAddUnits.Properties.MinValue = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.spinEditAddUnits.Size = new System.Drawing.Size(56, 20);
            this.spinEditAddUnits.TabIndex = 2;
            // 
            // checkEditAddValueToDates
            // 
            this.checkEditAddValueToDates.Location = new System.Drawing.Point(6, 51);
            this.checkEditAddValueToDates.MenuManager = this.barManager1;
            this.checkEditAddValueToDates.Name = "checkEditAddValueToDates";
            this.checkEditAddValueToDates.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditAddValueToDates.Properties.Caption = "<b>Add</b>:";
            this.checkEditAddValueToDates.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditAddValueToDates.Properties.RadioGroupIndex = 1;
            this.checkEditAddValueToDates.Size = new System.Drawing.Size(46, 19);
            this.checkEditAddValueToDates.TabIndex = 1;
            this.checkEditAddValueToDates.TabStop = false;
            this.checkEditAddValueToDates.CheckedChanged += new System.EventHandler(this.checkEditAddValueToDates_CheckedChanged);
            // 
            // checkEditStartAndEnd
            // 
            this.checkEditStartAndEnd.EditValue = true;
            this.checkEditStartAndEnd.Location = new System.Drawing.Point(6, 26);
            this.checkEditStartAndEnd.MenuManager = this.barManager1;
            this.checkEditStartAndEnd.Name = "checkEditStartAndEnd";
            this.checkEditStartAndEnd.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditStartAndEnd.Properties.Caption = "Enter <b>New</b> Start:";
            this.checkEditStartAndEnd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditStartAndEnd.Properties.RadioGroupIndex = 1;
            this.checkEditStartAndEnd.Size = new System.Drawing.Size(101, 19);
            this.checkEditStartAndEnd.TabIndex = 0;
            this.checkEditStartAndEnd.CheckedChanged += new System.EventHandler(this.checkEditStartAndEnd_CheckedChanged);
            // 
            // checkEditNewActive
            // 
            this.checkEditNewActive.Location = new System.Drawing.Point(184, 325);
            this.checkEditNewActive.MenuManager = this.barManager1;
            this.checkEditNewActive.Name = "checkEditNewActive";
            this.checkEditNewActive.Properties.Caption = "[Tick if Yes]";
            this.checkEditNewActive.Size = new System.Drawing.Size(75, 19);
            this.checkEditNewActive.TabIndex = 16;
            // 
            // labelControl4
            // 
            this.labelControl4.AllowHtmlString = true;
            this.labelControl4.Location = new System.Drawing.Point(15, 328);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(137, 13);
            this.labelControl4.TabIndex = 17;
            this.labelControl4.Text = "Make New Contract <b>Active</b>:";
            // 
            // labelControl5
            // 
            this.labelControl5.AllowHtmlString = true;
            this.labelControl5.Location = new System.Drawing.Point(15, 349);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(160, 13);
            this.labelControl5.TabIndex = 18;
            this.labelControl5.Text = "Make Source Contract <b>Inactive</b>:";
            // 
            // checkEditSourceInactive
            // 
            this.checkEditSourceInactive.Location = new System.Drawing.Point(184, 348);
            this.checkEditSourceInactive.MenuManager = this.barManager1;
            this.checkEditSourceInactive.Name = "checkEditSourceInactive";
            this.checkEditSourceInactive.Properties.Caption = "[Tick if Yes]";
            this.checkEditSourceInactive.Size = new System.Drawing.Size(75, 19);
            this.checkEditSourceInactive.TabIndex = 19;
            // 
            // groupControl2
            // 
            this.groupControl2.AllowHtmlText = true;
            this.groupControl2.Controls.Add(this.checkEditNewYearlyPercentageIncrease);
            this.groupControl2.Controls.Add(this.checkEditKeepYearlyPercentageIncrease);
            this.groupControl2.Controls.Add(this.spinEditYearPercentageIncreaseAmount);
            this.groupControl2.Controls.Add(this.labelControl6);
            this.groupControl2.Controls.Add(this.spinEditUpliftAmount);
            this.groupControl2.Controls.Add(this.checkEditAddUpliftAmount);
            this.groupControl2.Controls.Add(this.spinEditUpliftPercentage);
            this.groupControl2.Controls.Add(this.checkEditAddUpliftPercentage);
            this.groupControl2.Controls.Add(this.spinEditContractValue);
            this.groupControl2.Controls.Add(this.labelControl8);
            this.groupControl2.Controls.Add(this.checkEditUpliftAmount);
            this.groupControl2.Controls.Add(this.checkEditNewContractValue);
            this.groupControl2.Location = new System.Drawing.Point(9, 128);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(406, 170);
            this.groupControl2.TabIndex = 20;
            this.groupControl2.Text = "Site Contract <B>Value</b>";
            // 
            // checkEditNewYearlyPercentageIncrease
            // 
            this.checkEditNewYearlyPercentageIncrease.Location = new System.Drawing.Point(6, 141);
            this.checkEditNewYearlyPercentageIncrease.MenuManager = this.barManager1;
            this.checkEditNewYearlyPercentageIncrease.Name = "checkEditNewYearlyPercentageIncrease";
            this.checkEditNewYearlyPercentageIncrease.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditNewYearlyPercentageIncrease.Properties.Caption = "<B>New</b> Yearly Percentage Increase:";
            this.checkEditNewYearlyPercentageIncrease.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditNewYearlyPercentageIncrease.Properties.RadioGroupIndex = 4;
            this.checkEditNewYearlyPercentageIncrease.Size = new System.Drawing.Size(184, 19);
            this.checkEditNewYearlyPercentageIncrease.TabIndex = 17;
            this.checkEditNewYearlyPercentageIncrease.TabStop = false;
            this.checkEditNewYearlyPercentageIncrease.CheckedChanged += new System.EventHandler(this.checkEditNewYearlyPerecentageIncrease_CheckedChanged);
            // 
            // checkEditKeepYearlyPercentageIncrease
            // 
            this.checkEditKeepYearlyPercentageIncrease.EditValue = true;
            this.checkEditKeepYearlyPercentageIncrease.Location = new System.Drawing.Point(6, 116);
            this.checkEditKeepYearlyPercentageIncrease.MenuManager = this.barManager1;
            this.checkEditKeepYearlyPercentageIncrease.Name = "checkEditKeepYearlyPercentageIncrease";
            this.checkEditKeepYearlyPercentageIncrease.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditKeepYearlyPercentageIncrease.Properties.Caption = "Keep <b>Existing</b> Yearly Percentage Increase";
            this.checkEditKeepYearlyPercentageIncrease.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditKeepYearlyPercentageIncrease.Properties.RadioGroupIndex = 4;
            this.checkEditKeepYearlyPercentageIncrease.Size = new System.Drawing.Size(244, 19);
            this.checkEditKeepYearlyPercentageIncrease.TabIndex = 16;
            // 
            // spinEditYearPercentageIncreaseAmount
            // 
            this.spinEditYearPercentageIncreaseAmount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditYearPercentageIncreaseAmount.Location = new System.Drawing.Point(192, 141);
            this.spinEditYearPercentageIncreaseAmount.MenuManager = this.barManager1;
            this.spinEditYearPercentageIncreaseAmount.Name = "spinEditYearPercentageIncreaseAmount";
            this.spinEditYearPercentageIncreaseAmount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditYearPercentageIncreaseAmount.Properties.Mask.EditMask = "P";
            this.spinEditYearPercentageIncreaseAmount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditYearPercentageIncreaseAmount.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            131072});
            this.spinEditYearPercentageIncreaseAmount.Properties.MinValue = new decimal(new int[] {
            999999,
            0,
            0,
            -2147352576});
            this.spinEditYearPercentageIncreaseAmount.Size = new System.Drawing.Size(94, 20);
            this.spinEditYearPercentageIncreaseAmount.TabIndex = 14;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(268, 80);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(126, 13);
            this.labelControl6.TabIndex = 13;
            this.labelControl6.Text = "To Existing Contract Value";
            // 
            // spinEditUpliftAmount
            // 
            this.spinEditUpliftAmount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditUpliftAmount.Location = new System.Drawing.Point(169, 77);
            this.spinEditUpliftAmount.MenuManager = this.barManager1;
            this.spinEditUpliftAmount.Name = "spinEditUpliftAmount";
            this.spinEditUpliftAmount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditUpliftAmount.Properties.Mask.EditMask = "c";
            this.spinEditUpliftAmount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditUpliftAmount.Properties.MaxValue = new decimal(new int[] {
            1215752191,
            23,
            0,
            131072});
            this.spinEditUpliftAmount.Properties.MinValue = new decimal(new int[] {
            1215752191,
            23,
            0,
            -2147352576});
            this.spinEditUpliftAmount.Size = new System.Drawing.Size(94, 20);
            this.spinEditUpliftAmount.TabIndex = 12;
            // 
            // checkEditAddUpliftAmount
            // 
            this.checkEditAddUpliftAmount.Location = new System.Drawing.Point(50, 77);
            this.checkEditAddUpliftAmount.MenuManager = this.barManager1;
            this.checkEditAddUpliftAmount.Name = "checkEditAddUpliftAmount";
            this.checkEditAddUpliftAmount.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditAddUpliftAmount.Properties.Caption = "<b>Amount</b> Uplift:";
            this.checkEditAddUpliftAmount.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditAddUpliftAmount.Properties.RadioGroupIndex = 3;
            this.checkEditAddUpliftAmount.Size = new System.Drawing.Size(116, 19);
            this.checkEditAddUpliftAmount.TabIndex = 11;
            this.checkEditAddUpliftAmount.TabStop = false;
            this.checkEditAddUpliftAmount.CheckedChanged += new System.EventHandler(this.checkEditAddUpliftAmount_CheckedChanged);
            // 
            // spinEditUpliftPercentage
            // 
            this.spinEditUpliftPercentage.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditUpliftPercentage.Location = new System.Drawing.Point(169, 52);
            this.spinEditUpliftPercentage.MenuManager = this.barManager1;
            this.spinEditUpliftPercentage.Name = "spinEditUpliftPercentage";
            this.spinEditUpliftPercentage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditUpliftPercentage.Properties.Mask.EditMask = "P";
            this.spinEditUpliftPercentage.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditUpliftPercentage.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            131072});
            this.spinEditUpliftPercentage.Properties.MinValue = new decimal(new int[] {
            999999,
            0,
            0,
            -2147352576});
            this.spinEditUpliftPercentage.Size = new System.Drawing.Size(94, 20);
            this.spinEditUpliftPercentage.TabIndex = 2;
            // 
            // checkEditAddUpliftPercentage
            // 
            this.checkEditAddUpliftPercentage.Location = new System.Drawing.Point(50, 52);
            this.checkEditAddUpliftPercentage.MenuManager = this.barManager1;
            this.checkEditAddUpliftPercentage.Name = "checkEditAddUpliftPercentage";
            this.checkEditAddUpliftPercentage.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditAddUpliftPercentage.Properties.Caption = "<b>Percentage</b> Uplift:";
            this.checkEditAddUpliftPercentage.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditAddUpliftPercentage.Properties.RadioGroupIndex = 3;
            this.checkEditAddUpliftPercentage.Size = new System.Drawing.Size(116, 19);
            this.checkEditAddUpliftPercentage.TabIndex = 10;
            this.checkEditAddUpliftPercentage.TabStop = false;
            this.checkEditAddUpliftPercentage.CheckedChanged += new System.EventHandler(this.checkEditAddUpliftPercentage_CheckedChanged);
            // 
            // spinEditContractValue
            // 
            this.spinEditContractValue.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditContractValue.Location = new System.Drawing.Point(169, 27);
            this.spinEditContractValue.MenuManager = this.barManager1;
            this.spinEditContractValue.Name = "spinEditContractValue";
            this.spinEditContractValue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditContractValue.Properties.Mask.EditMask = "c";
            this.spinEditContractValue.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditContractValue.Properties.MaxValue = new decimal(new int[] {
            1215752191,
            23,
            0,
            131072});
            this.spinEditContractValue.Properties.MinValue = new decimal(new int[] {
            1215752191,
            23,
            0,
            -2147352576});
            this.spinEditContractValue.Size = new System.Drawing.Size(94, 20);
            this.spinEditContractValue.TabIndex = 9;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(268, 55);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(126, 13);
            this.labelControl8.TabIndex = 3;
            this.labelControl8.Text = "To Existing Contract Value";
            // 
            // checkEditUpliftAmount
            // 
            this.checkEditUpliftAmount.Location = new System.Drawing.Point(6, 52);
            this.checkEditUpliftAmount.MenuManager = this.barManager1;
            this.checkEditUpliftAmount.Name = "checkEditUpliftAmount";
            this.checkEditUpliftAmount.Properties.Caption = "Add";
            this.checkEditUpliftAmount.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditUpliftAmount.Properties.RadioGroupIndex = 2;
            this.checkEditUpliftAmount.Size = new System.Drawing.Size(42, 19);
            this.checkEditUpliftAmount.TabIndex = 1;
            this.checkEditUpliftAmount.TabStop = false;
            this.checkEditUpliftAmount.CheckedChanged += new System.EventHandler(this.checkEditUpliftAmount_CheckedChanged);
            // 
            // checkEditNewContractValue
            // 
            this.checkEditNewContractValue.EditValue = true;
            this.checkEditNewContractValue.Location = new System.Drawing.Point(6, 27);
            this.checkEditNewContractValue.MenuManager = this.barManager1;
            this.checkEditNewContractValue.Name = "checkEditNewContractValue";
            this.checkEditNewContractValue.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditNewContractValue.Properties.Caption = "Enter <b>New</b> Contract Value:";
            this.checkEditNewContractValue.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditNewContractValue.Properties.RadioGroupIndex = 2;
            this.checkEditNewContractValue.Size = new System.Drawing.Size(160, 19);
            this.checkEditNewContractValue.TabIndex = 0;
            this.checkEditNewContractValue.CheckedChanged += new System.EventHandler(this.checkEditNewContractValue_CheckedChanged);
            // 
            // groupControl3
            // 
            this.groupControl3.AllowHtmlText = true;
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.checkEditSiteInstructionsAppend);
            this.groupControl3.Controls.Add(this.checkEditSiteInstructionsNew);
            this.groupControl3.Controls.Add(this.MemoEditSiteInstructions);
            this.groupControl3.Location = new System.Drawing.Point(9, 395);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(728, 139);
            this.groupControl3.TabIndex = 21;
            this.groupControl3.Text = "Site Contract <B>Site Instructions</b>";
            // 
            // checkEditSiteInstructionsAppend
            // 
            this.checkEditSiteInstructionsAppend.EditValue = true;
            this.checkEditSiteInstructionsAppend.Location = new System.Drawing.Point(6, 26);
            this.checkEditSiteInstructionsAppend.MenuManager = this.barManager1;
            this.checkEditSiteInstructionsAppend.Name = "checkEditSiteInstructionsAppend";
            this.checkEditSiteInstructionsAppend.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditSiteInstructionsAppend.Properties.Caption = "<b>Append</b> to Existing Site Instructions:";
            this.checkEditSiteInstructionsAppend.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditSiteInstructionsAppend.Properties.RadioGroupIndex = 5;
            this.checkEditSiteInstructionsAppend.Size = new System.Drawing.Size(203, 19);
            this.checkEditSiteInstructionsAppend.TabIndex = 1;
            // 
            // checkEditSiteInstructionsNew
            // 
            this.checkEditSiteInstructionsNew.Location = new System.Drawing.Point(6, 51);
            this.checkEditSiteInstructionsNew.MenuManager = this.barManager1;
            this.checkEditSiteInstructionsNew.Name = "checkEditSiteInstructionsNew";
            this.checkEditSiteInstructionsNew.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditSiteInstructionsNew.Properties.Caption = "<b>Replace</b> Site Instructions With:";
            this.checkEditSiteInstructionsNew.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditSiteInstructionsNew.Properties.RadioGroupIndex = 5;
            this.checkEditSiteInstructionsNew.Size = new System.Drawing.Size(203, 19);
            this.checkEditSiteInstructionsNew.TabIndex = 0;
            this.checkEditSiteInstructionsNew.TabStop = false;
            // 
            // groupControl4
            // 
            this.groupControl4.AllowHtmlText = true;
            this.groupControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl4.Controls.Add(this.checkEditVisitBillingRequirements);
            this.groupControl4.Controls.Add(this.checkEditSiteContractBillingRequirements);
            this.groupControl4.Controls.Add(this.checkEditHoldDayOfWeek);
            this.groupControl4.Controls.Add(this.checkEditJobMaterials);
            this.groupControl4.Controls.Add(this.checkEditJobEquipment);
            this.groupControl4.Controls.Add(this.checkEditJobLabour);
            this.groupControl4.Controls.Add(this.checkEditJobHealthAndSafety);
            this.groupControl4.Controls.Add(this.checkEditJobStructure);
            this.groupControl4.Controls.Add(this.checkEditVisitPersonResponsibilities);
            this.groupControl4.Controls.Add(this.checkEditVisitStructure);
            this.groupControl4.Controls.Add(this.checkEditDefaultMaterialCosts);
            this.groupControl4.Controls.Add(this.checkEditDefaultEquipmentCosts);
            this.groupControl4.Controls.Add(this.checkEditPreferredLabour);
            this.groupControl4.Controls.Add(this.checkEditPersonResponsibilities);
            this.groupControl4.Controls.Add(this.checkEditYearAndBillingProfile);
            this.groupControl4.Location = new System.Drawing.Point(428, 34);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(308, 350);
            this.groupControl4.TabIndex = 22;
            this.groupControl4.Text = "<b>Linked Data</b> To Copy";
            // 
            // checkEditHoldDayOfWeek
            // 
            this.checkEditHoldDayOfWeek.EditValue = true;
            this.checkEditHoldDayOfWeek.Location = new System.Drawing.Point(75, 166);
            this.checkEditHoldDayOfWeek.MenuManager = this.barManager1;
            this.checkEditHoldDayOfWeek.Name = "checkEditHoldDayOfWeek";
            this.checkEditHoldDayOfWeek.Properties.Caption = "Hold Day of Week";
            this.checkEditHoldDayOfWeek.Size = new System.Drawing.Size(110, 19);
            this.checkEditHoldDayOfWeek.TabIndex = 12;
            // 
            // checkEditJobMaterials
            // 
            this.checkEditJobMaterials.EditValue = true;
            this.checkEditJobMaterials.Location = new System.Drawing.Point(37, 304);
            this.checkEditJobMaterials.MenuManager = this.barManager1;
            this.checkEditJobMaterials.Name = "checkEditJobMaterials";
            this.checkEditJobMaterials.Properties.Caption = "Job Materials";
            this.checkEditJobMaterials.Size = new System.Drawing.Size(148, 19);
            this.checkEditJobMaterials.TabIndex = 11;
            // 
            // checkEditJobEquipment
            // 
            this.checkEditJobEquipment.EditValue = true;
            this.checkEditJobEquipment.Location = new System.Drawing.Point(37, 281);
            this.checkEditJobEquipment.MenuManager = this.barManager1;
            this.checkEditJobEquipment.Name = "checkEditJobEquipment";
            this.checkEditJobEquipment.Properties.Caption = "Job Equipment";
            this.checkEditJobEquipment.Size = new System.Drawing.Size(148, 19);
            this.checkEditJobEquipment.TabIndex = 10;
            // 
            // checkEditJobLabour
            // 
            this.checkEditJobLabour.EditValue = true;
            this.checkEditJobLabour.Location = new System.Drawing.Point(37, 258);
            this.checkEditJobLabour.MenuManager = this.barManager1;
            this.checkEditJobLabour.Name = "checkEditJobLabour";
            this.checkEditJobLabour.Properties.Caption = "Job Labour";
            this.checkEditJobLabour.Size = new System.Drawing.Size(148, 19);
            this.checkEditJobLabour.TabIndex = 9;
            // 
            // checkEditJobHealthAndSafety
            // 
            this.checkEditJobHealthAndSafety.EditValue = true;
            this.checkEditJobHealthAndSafety.Location = new System.Drawing.Point(37, 327);
            this.checkEditJobHealthAndSafety.MenuManager = this.barManager1;
            this.checkEditJobHealthAndSafety.Name = "checkEditJobHealthAndSafety";
            this.checkEditJobHealthAndSafety.Properties.Caption = "Job Health And Safety";
            this.checkEditJobHealthAndSafety.Size = new System.Drawing.Size(148, 19);
            this.checkEditJobHealthAndSafety.TabIndex = 8;
            // 
            // checkEditJobStructure
            // 
            this.checkEditJobStructure.EditValue = true;
            this.checkEditJobStructure.Location = new System.Drawing.Point(21, 235);
            this.checkEditJobStructure.MenuManager = this.barManager1;
            this.checkEditJobStructure.Name = "checkEditJobStructure";
            this.checkEditJobStructure.Properties.Caption = "Job";
            this.checkEditJobStructure.Size = new System.Drawing.Size(164, 19);
            this.checkEditJobStructure.TabIndex = 7;
            this.checkEditJobStructure.CheckedChanged += new System.EventHandler(this.checkEditJobStructure_CheckedChanged);
            // 
            // checkEditVisitPersonResponsibilities
            // 
            this.checkEditVisitPersonResponsibilities.EditValue = true;
            this.checkEditVisitPersonResponsibilities.Location = new System.Drawing.Point(21, 189);
            this.checkEditVisitPersonResponsibilities.MenuManager = this.barManager1;
            this.checkEditVisitPersonResponsibilities.Name = "checkEditVisitPersonResponsibilities";
            this.checkEditVisitPersonResponsibilities.Properties.Caption = "Visit Person Responsibilities";
            this.checkEditVisitPersonResponsibilities.Size = new System.Drawing.Size(164, 19);
            this.checkEditVisitPersonResponsibilities.TabIndex = 6;
            // 
            // checkEditVisitStructure
            // 
            this.checkEditVisitStructure.EditValue = true;
            this.checkEditVisitStructure.Location = new System.Drawing.Point(5, 166);
            this.checkEditVisitStructure.MenuManager = this.barManager1;
            this.checkEditVisitStructure.Name = "checkEditVisitStructure";
            this.checkEditVisitStructure.Properties.Caption = "Visit";
            this.checkEditVisitStructure.Size = new System.Drawing.Size(51, 19);
            this.checkEditVisitStructure.TabIndex = 5;
            this.checkEditVisitStructure.CheckedChanged += new System.EventHandler(this.checkEditVisitStructure_CheckedChanged);
            // 
            // checkEditDefaultMaterialCosts
            // 
            this.checkEditDefaultMaterialCosts.EditValue = true;
            this.checkEditDefaultMaterialCosts.Location = new System.Drawing.Point(5, 120);
            this.checkEditDefaultMaterialCosts.MenuManager = this.barManager1;
            this.checkEditDefaultMaterialCosts.Name = "checkEditDefaultMaterialCosts";
            this.checkEditDefaultMaterialCosts.Properties.Caption = "Default Material Costs";
            this.checkEditDefaultMaterialCosts.Size = new System.Drawing.Size(164, 19);
            this.checkEditDefaultMaterialCosts.TabIndex = 4;
            // 
            // checkEditDefaultEquipmentCosts
            // 
            this.checkEditDefaultEquipmentCosts.EditValue = true;
            this.checkEditDefaultEquipmentCosts.Location = new System.Drawing.Point(5, 97);
            this.checkEditDefaultEquipmentCosts.MenuManager = this.barManager1;
            this.checkEditDefaultEquipmentCosts.Name = "checkEditDefaultEquipmentCosts";
            this.checkEditDefaultEquipmentCosts.Properties.Caption = "Default Equipment Costs";
            this.checkEditDefaultEquipmentCosts.Size = new System.Drawing.Size(164, 19);
            this.checkEditDefaultEquipmentCosts.TabIndex = 3;
            // 
            // checkEditPreferredLabour
            // 
            this.checkEditPreferredLabour.EditValue = true;
            this.checkEditPreferredLabour.Location = new System.Drawing.Point(5, 74);
            this.checkEditPreferredLabour.MenuManager = this.barManager1;
            this.checkEditPreferredLabour.Name = "checkEditPreferredLabour";
            this.checkEditPreferredLabour.Properties.Caption = "Preferred Labour";
            this.checkEditPreferredLabour.Size = new System.Drawing.Size(164, 19);
            this.checkEditPreferredLabour.TabIndex = 2;
            // 
            // checkEditPersonResponsibilities
            // 
            this.checkEditPersonResponsibilities.EditValue = true;
            this.checkEditPersonResponsibilities.Location = new System.Drawing.Point(5, 51);
            this.checkEditPersonResponsibilities.MenuManager = this.barManager1;
            this.checkEditPersonResponsibilities.Name = "checkEditPersonResponsibilities";
            this.checkEditPersonResponsibilities.Properties.Caption = "Person Responsibilities";
            this.checkEditPersonResponsibilities.Size = new System.Drawing.Size(164, 19);
            this.checkEditPersonResponsibilities.TabIndex = 1;
            // 
            // checkEditYearAndBillingProfile
            // 
            this.checkEditYearAndBillingProfile.EditValue = true;
            this.checkEditYearAndBillingProfile.Location = new System.Drawing.Point(5, 28);
            this.checkEditYearAndBillingProfile.MenuManager = this.barManager1;
            this.checkEditYearAndBillingProfile.Name = "checkEditYearAndBillingProfile";
            this.checkEditYearAndBillingProfile.Properties.Caption = "Year And Billing Profile";
            this.checkEditYearAndBillingProfile.Size = new System.Drawing.Size(164, 19);
            this.checkEditYearAndBillingProfile.TabIndex = 0;
            // 
            // checkEditSiteContractBillingRequirements
            // 
            this.checkEditSiteContractBillingRequirements.EditValue = true;
            this.checkEditSiteContractBillingRequirements.Location = new System.Drawing.Point(5, 143);
            this.checkEditSiteContractBillingRequirements.MenuManager = this.barManager1;
            this.checkEditSiteContractBillingRequirements.Name = "checkEditSiteContractBillingRequirements";
            this.checkEditSiteContractBillingRequirements.Properties.Caption = "Contract Billing Requirements";
            this.checkEditSiteContractBillingRequirements.Size = new System.Drawing.Size(164, 19);
            this.checkEditSiteContractBillingRequirements.TabIndex = 13;
            // 
            // checkEditVisitBillingRequirements
            // 
            this.checkEditVisitBillingRequirements.EditValue = true;
            this.checkEditVisitBillingRequirements.Location = new System.Drawing.Point(21, 212);
            this.checkEditVisitBillingRequirements.MenuManager = this.barManager1;
            this.checkEditVisitBillingRequirements.Name = "checkEditVisitBillingRequirements";
            this.checkEditVisitBillingRequirements.Properties.Caption = "Visit Billing Requirements";
            this.checkEditVisitBillingRequirements.Size = new System.Drawing.Size(148, 19);
            this.checkEditVisitBillingRequirements.TabIndex = 14;
            // 
            // frm_OM_Site_Contract_Copy_Forward
            // 
            this.ClientSize = new System.Drawing.Size(746, 602);
            this.ControlBox = false;
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.checkEditSourceInactive);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.checkEditNewActive);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OM_Site_Contract_Copy_Forward";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Copy Forward Site Contract";
            this.Load += new System.EventHandler(this.frm_OM_Site_Contract_Copy_Forward_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.groupControl1, 0);
            this.Controls.SetChildIndex(this.checkEditNewActive, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            this.Controls.SetChildIndex(this.checkEditSourceInactive, 0);
            this.Controls.SetChildIndex(this.groupControl2, 0);
            this.Controls.SetChildIndex(this.groupControl3, 0);
            this.Controls.SetChildIndex(this.groupControl4, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MemoEditSiteInstructions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditContractEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditContractEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditContractStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditContractStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditAddUnitsDescriptor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditAddUnits.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAddValueToDates.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditStartAndEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditNewActive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSourceInactive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditNewYearlyPercentageIncrease.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditKeepYearlyPercentageIncrease.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditYearPercentageIncreaseAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditUpliftAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAddUpliftAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditUpliftPercentage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAddUpliftPercentage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditContractValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditUpliftAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditNewContractValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSiteInstructionsAppend.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSiteInstructionsNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditHoldDayOfWeek.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditJobMaterials.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditJobEquipment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditJobLabour.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditJobHealthAndSafety.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditJobStructure.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditVisitPersonResponsibilities.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditVisitStructure.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDefaultMaterialCosts.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDefaultEquipmentCosts.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditPreferredLabour.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditPersonResponsibilities.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditYearAndBillingProfile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSiteContractBillingRequirements.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditVisitBillingRequirements.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.MemoEdit MemoEditSiteInstructions;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem bsiInformation;
        private DevExpress.XtraBars.BarStaticItem bsiSiteContractCount;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.DateEdit dateEditContractEndDate;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit dateEditContractStartDate;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditAddUnitsDescriptor;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SpinEdit spinEditAddUnits;
        private DevExpress.XtraEditors.CheckEdit checkEditAddValueToDates;
        private DevExpress.XtraEditors.CheckEdit checkEditStartAndEnd;
        private DevExpress.XtraEditors.CheckEdit checkEditNewActive;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.CheckEdit checkEditSourceInactive;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.SpinEdit spinEditUpliftPercentage;
        private DevExpress.XtraEditors.CheckEdit checkEditUpliftAmount;
        private DevExpress.XtraEditors.CheckEdit checkEditNewContractValue;
        private DevExpress.XtraEditors.SpinEdit spinEditContractValue;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.SpinEdit spinEditUpliftAmount;
        private DevExpress.XtraEditors.CheckEdit checkEditAddUpliftAmount;
        private DevExpress.XtraEditors.CheckEdit checkEditAddUpliftPercentage;
        private DevExpress.XtraEditors.CheckEdit checkEditNewYearlyPercentageIncrease;
        private DevExpress.XtraEditors.CheckEdit checkEditKeepYearlyPercentageIncrease;
        private DevExpress.XtraEditors.SpinEdit spinEditYearPercentageIncreaseAmount;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.CheckEdit checkEditSiteInstructionsAppend;
        private DevExpress.XtraEditors.CheckEdit checkEditSiteInstructionsNew;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.CheckEdit checkEditPersonResponsibilities;
        private DevExpress.XtraEditors.CheckEdit checkEditYearAndBillingProfile;
        private DevExpress.XtraEditors.CheckEdit checkEditJobHealthAndSafety;
        private DevExpress.XtraEditors.CheckEdit checkEditJobStructure;
        private DevExpress.XtraEditors.CheckEdit checkEditVisitPersonResponsibilities;
        private DevExpress.XtraEditors.CheckEdit checkEditVisitStructure;
        private DevExpress.XtraEditors.CheckEdit checkEditDefaultMaterialCosts;
        private DevExpress.XtraEditors.CheckEdit checkEditDefaultEquipmentCosts;
        private DevExpress.XtraEditors.CheckEdit checkEditPreferredLabour;
        private DevExpress.XtraEditors.CheckEdit checkEditJobMaterials;
        private DevExpress.XtraEditors.CheckEdit checkEditJobEquipment;
        private DevExpress.XtraEditors.CheckEdit checkEditJobLabour;
        private DevExpress.XtraEditors.CheckEdit checkEditHoldDayOfWeek;
        private DevExpress.XtraEditors.CheckEdit checkEditVisitBillingRequirements;
        private DevExpress.XtraEditors.CheckEdit checkEditSiteContractBillingRequirements;
    }
}

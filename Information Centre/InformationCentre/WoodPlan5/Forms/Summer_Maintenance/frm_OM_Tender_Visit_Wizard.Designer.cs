﻿namespace WoodPlan5
{
    partial class frm_OM_Tender_Visit_Wizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Tender_Visit_Wizard));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.Animation.Transition transition1 = new DevExpress.Utils.Animation.Transition();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn126 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn129 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageWelcome = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.TenderDetailsLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.btnWelcomeNext = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.xtraTabPageStep1 = new DevExpress.XtraTab.XtraTabPage();
            this.groupControlBlockEditVisits = new DevExpress.XtraEditors.GroupControl();
            this.separatorControl2 = new DevExpress.XtraEditors.SeparatorControl();
            this.separatorControl1 = new DevExpress.XtraEditors.SeparatorControl();
            this.btnApplyVisitStatus = new DevExpress.XtraEditors.SimpleButton();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridLookUpEditVisitStatusBlockEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06538OMVisitStatusesNotReadyAndReadyNoBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Tender = new WoodPlan5.DataSet_OM_Tender();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.VisitDaysSeparationSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.VisitDescriptorComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnApplyVisitDuration = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.VisitDurationSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.btnCalculateVisitDaysSeparation = new DevExpress.XtraEditors.SimpleButton();
            this.btnApplyVisitDaysSeparation = new DevExpress.XtraEditors.SimpleButton();
            this.VisitTimeEdit = new DevExpress.XtraEditors.TimeSpanEdit();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.btnSplitCosts = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.textEditCalculatedTotalSell = new DevExpress.XtraEditors.TextEdit();
            this.textEditCalculatedEquipmentSell = new DevExpress.XtraEditors.TextEdit();
            this.textEditCalculatedMaterialSell = new DevExpress.XtraEditors.TextEdit();
            this.textEditCalculatedLabourSell = new DevExpress.XtraEditors.TextEdit();
            this.textEditTenderTotalSell = new DevExpress.XtraEditors.TextEdit();
            this.textEditTenderEquipmentSell = new DevExpress.XtraEditors.TextEdit();
            this.textEditTenderMaterialSell = new DevExpress.XtraEditors.TextEdit();
            this.textEditTenderLabourSell = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.textEditCalculatedTotalCost = new DevExpress.XtraEditors.TextEdit();
            this.textEditCalculatedEquipmentCost = new DevExpress.XtraEditors.TextEdit();
            this.textEditCalculatedMaterialCost = new DevExpress.XtraEditors.TextEdit();
            this.textEditCalculatedLabourCost = new DevExpress.XtraEditors.TextEdit();
            this.textEditTenderTotalCost = new DevExpress.XtraEditors.TextEdit();
            this.textEditTenderEquipmentCost = new DevExpress.XtraEditors.TextEdit();
            this.textEditTenderMaterialCost = new DevExpress.XtraEditors.TextEdit();
            this.textEditTenderLabourCost = new DevExpress.XtraEditors.TextEdit();
            this.TenderDetailsLabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.groupControlAddVisits = new DevExpress.XtraEditors.GroupControl();
            this.GapfirstVisitCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.gridLookUpEditVisitStatusID = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditStartDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.StartDateLabel = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditNumberOfVisits = new DevExpress.XtraEditors.SpinEdit();
            this.gridLookUpEditSellCalculationLevel = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06134OMJobCalculationLevelDescriptorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnAddVisits = new DevExpress.XtraEditors.SimpleButton();
            this.spinEditGapBetweenVisitsUnits = new DevExpress.XtraEditors.SpinEdit();
            this.gridLookUpEditGapBetweenVisitsDescriptor = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06133OMVisitGapUnitDescriptorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn127 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn128 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditCostCalculationLevel = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView17 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn130 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn131 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp06145OMVisitEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colstrMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRecordIDs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditVisitNumber = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colExpectedEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCalculationLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditCostCalculationLevel = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVisitSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colStartLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditFloat = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colStartLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinishLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalendarLabel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLabelColourID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVisitCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditVisitCategory = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06250OMVisitCategoriesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditVisitStatusID = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditVisitTypes = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06308OMVisitTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditClientPONumber = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colSellCalculationLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedFromVisitTemplateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysSeparation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditDaysSeparation = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colSiteContractDaysSeparationPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colVisitMaterialCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitMaterialSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitEquipmentSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyMaxDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnStep5Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep5Previous = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPageFinish = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.TenderDetailsLabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.btnFinish = new DevExpress.XtraEditors.SimpleButton();
            this.btnFinishPrevious = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.sp06141OMVisitTemplateHeadersWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemButtonEditChooseStaff = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.sp06133_OM_Visit_Gap_Unit_DescriptorsTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06133_OM_Visit_Gap_Unit_DescriptorsTableAdapter();
            this.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter();
            this.sp06145_OM_Visit_EditTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06145_OM_Visit_EditTableAdapter();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemDuration1 = new DevExpress.XtraScheduler.UI.RepositoryItemDuration();
            this.sp06250_OM_Visit_Categories_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06250_OM_Visit_Categories_With_BlankTableAdapter();
            this.sp06308_OM_Visit_TypesTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06308_OM_Visit_TypesTableAdapter();
            this.transitionManager1 = new DevExpress.Utils.Animation.TransitionManager(this.components);
            this.sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter();
            this.sp06538_OM_Visit_Statuses_Not_Ready_And_Ready_No_BlankTableAdapter = new WoodPlan5.DataSet_OM_TenderTableAdapters.sp06538_OM_Visit_Statuses_Not_Ready_And_Ready_No_BlankTableAdapter();
            this.timerWelcomePage = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageWelcome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.xtraTabPageStep1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlBlockEditVisits)).BeginInit();
            this.groupControlBlockEditVisits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditVisitStatusBlockEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06538OMVisitStatusesNotReadyAndReadyNoBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Tender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitDaysSeparationSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitDescriptorComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitDurationSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitTimeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCalculatedTotalSell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCalculatedEquipmentSell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCalculatedMaterialSell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCalculatedLabourSell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderTotalSell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderEquipmentSell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderMaterialSell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderLabourSell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCalculatedTotalCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCalculatedEquipmentCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCalculatedMaterialCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCalculatedLabourCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderTotalCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderEquipmentCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderMaterialCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderLabourCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlAddVisits)).BeginInit();
            this.groupControlAddVisits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GapfirstVisitCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditVisitStatusID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditNumberOfVisits.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSellCalculationLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06134OMJobCalculationLevelDescriptorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditGapBetweenVisitsUnits.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditGapBetweenVisitsDescriptor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06133OMVisitGapUnitDescriptorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditCostCalculationLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06145OMVisitEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditVisitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditCostCalculationLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditFloat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditVisitCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06250OMVisitCategoriesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditVisitStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditVisitTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06308OMVisitTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditClientPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditDaysSeparation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCurrency)).BeginInit();
            this.xtraTabPageFinish.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06141OMVisitTemplateHeadersWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseStaff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1482, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 625);
            this.barDockControlBottom.Size = new System.Drawing.Size(1482, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 625);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1482, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 625);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Images = this.imageCollection1;
            this.barManager1.MaxItemId = 94;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit4,
            this.repositoryItemSpinEdit1,
            this.repositoryItemDuration1});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "ID";
            this.gridColumn10.FieldName = "ID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 53;
            // 
            // gridColumn126
            // 
            this.gridColumn126.Caption = "ID";
            this.gridColumn126.FieldName = "ID";
            this.gridColumn126.Name = "gridColumn126";
            this.gridColumn126.OptionsColumn.AllowEdit = false;
            this.gridColumn126.OptionsColumn.AllowFocus = false;
            this.gridColumn126.OptionsColumn.ReadOnly = true;
            this.gridColumn126.Width = 53;
            // 
            // gridColumn129
            // 
            this.gridColumn129.Caption = "ID";
            this.gridColumn129.FieldName = "ID";
            this.gridColumn129.Name = "gridColumn129";
            this.gridColumn129.OptionsColumn.AllowEdit = false;
            this.gridColumn129.OptionsColumn.AllowFocus = false;
            this.gridColumn129.OptionsColumn.ReadOnly = true;
            this.gridColumn129.Width = 53;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.Width = 53;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "ID";
            this.gridColumn13.FieldName = "ID";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 53;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, -1);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
            this.xtraTabControl1.Size = new System.Drawing.Size(1486, 630);
            this.xtraTabControl1.TabIndex = 6;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageWelcome,
            this.xtraTabPageStep1,
            this.xtraTabPageFinish});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            this.xtraTabControl1.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.xtraTabControl1_SelectedPageChanging);
            // 
            // xtraTabPageWelcome
            // 
            this.xtraTabPageWelcome.Controls.Add(this.panelControl1);
            this.xtraTabPageWelcome.Controls.Add(this.btnWelcomeNext);
            this.xtraTabPageWelcome.Controls.Add(this.labelControl2);
            this.xtraTabPageWelcome.Controls.Add(this.pictureEdit1);
            this.xtraTabPageWelcome.Name = "xtraTabPageWelcome";
            this.xtraTabPageWelcome.Size = new System.Drawing.Size(1459, 625);
            this.xtraTabPageWelcome.Tag = "0";
            this.xtraTabPageWelcome.Text = "Welcome";
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.TenderDetailsLabelControl);
            this.panelControl1.Controls.Add(this.pictureEdit4);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Location = new System.Drawing.Point(211, 6);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1242, 75);
            this.panelControl1.TabIndex = 17;
            // 
            // TenderDetailsLabelControl
            // 
            this.TenderDetailsLabelControl.AllowHtmlString = true;
            this.TenderDetailsLabelControl.Location = new System.Drawing.Point(9, 57);
            this.TenderDetailsLabelControl.Name = "TenderDetailsLabelControl";
            this.TenderDetailsLabelControl.Size = new System.Drawing.Size(82, 13);
            this.TenderDetailsLabelControl.TabIndex = 17;
            this.TenderDetailsLabelControl.Text = "Selected Tender:";
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit4.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit4.Location = new System.Drawing.Point(1198, 4);
            this.pictureEdit4.MenuManager = this.barManager1;
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Properties.ReadOnly = true;
            this.pictureEdit4.Properties.ShowMenu = false;
            this.pictureEdit4.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit4.TabIndex = 16;
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(5, 1);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(391, 29);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "Welcome to the <b>Tender</b> Visit Wizard";
            // 
            // labelControl9
            // 
            this.labelControl9.AllowHtmlString = true;
            this.labelControl9.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl9.Appearance.Options.UseBackColor = true;
            this.labelControl9.Location = new System.Drawing.Point(9, 35);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(398, 13);
            this.labelControl9.TabIndex = 15;
            this.labelControl9.Text = "This wizard will allow you to create one or more new Visits for the selected <b>T" +
    "ender\r\n</b>";
            // 
            // btnWelcomeNext
            // 
            this.btnWelcomeNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWelcomeNext.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnWelcomeNext.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnWelcomeNext.Location = new System.Drawing.Point(1365, 589);
            this.btnWelcomeNext.Name = "btnWelcomeNext";
            this.btnWelcomeNext.Size = new System.Drawing.Size(88, 30);
            this.btnWelcomeNext.TabIndex = 7;
            this.btnWelcomeNext.Text = "Next";
            this.btnWelcomeNext.Click += new System.EventHandler(this.btnWelcomeNext_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.AllowHtmlString = true;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(220, 104);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(220, 25);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Click <b>Next</b> To Continue";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureEdit1.EditValue = global::WoodPlan5.Properties.Resources.wizard_image;
            this.pictureEdit1.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(205, 625);
            this.pictureEdit1.TabIndex = 4;
            // 
            // xtraTabPageStep1
            // 
            this.xtraTabPageStep1.Controls.Add(this.groupControlBlockEditVisits);
            this.xtraTabPageStep1.Controls.Add(this.panelControl4);
            this.xtraTabPageStep1.Controls.Add(this.groupControlAddVisits);
            this.xtraTabPageStep1.Controls.Add(this.gridControl6);
            this.xtraTabPageStep1.Controls.Add(this.btnStep5Next);
            this.xtraTabPageStep1.Controls.Add(this.btnStep5Previous);
            this.xtraTabPageStep1.Name = "xtraTabPageStep1";
            this.xtraTabPageStep1.Size = new System.Drawing.Size(1459, 625);
            this.xtraTabPageStep1.Tag = "5";
            this.xtraTabPageStep1.Text = "Step 1";
            // 
            // groupControlBlockEditVisits
            // 
            this.groupControlBlockEditVisits.AllowHtmlText = true;
            this.groupControlBlockEditVisits.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupControlBlockEditVisits.CaptionLocation = DevExpress.Utils.Locations.Bottom;
            this.groupControlBlockEditVisits.Controls.Add(this.separatorControl2);
            this.groupControlBlockEditVisits.Controls.Add(this.separatorControl1);
            this.groupControlBlockEditVisits.Controls.Add(this.btnApplyVisitStatus);
            this.groupControlBlockEditVisits.Controls.Add(this.gridLookUpEditVisitStatusBlockEdit);
            this.groupControlBlockEditVisits.Controls.Add(this.labelControl17);
            this.groupControlBlockEditVisits.Controls.Add(this.labelControl5);
            this.groupControlBlockEditVisits.Controls.Add(this.VisitDaysSeparationSpinEdit);
            this.groupControlBlockEditVisits.Controls.Add(this.labelControl4);
            this.groupControlBlockEditVisits.Controls.Add(this.VisitDescriptorComboBoxEdit);
            this.groupControlBlockEditVisits.Controls.Add(this.btnApplyVisitDuration);
            this.groupControlBlockEditVisits.Controls.Add(this.labelControl3);
            this.groupControlBlockEditVisits.Controls.Add(this.VisitDurationSpinEdit);
            this.groupControlBlockEditVisits.Controls.Add(this.btnCalculateVisitDaysSeparation);
            this.groupControlBlockEditVisits.Controls.Add(this.btnApplyVisitDaysSeparation);
            this.groupControlBlockEditVisits.Controls.Add(this.VisitTimeEdit);
            this.groupControlBlockEditVisits.Location = new System.Drawing.Point(7, 566);
            this.groupControlBlockEditVisits.Name = "groupControlBlockEditVisits";
            this.groupControlBlockEditVisits.Size = new System.Drawing.Size(1103, 52);
            this.groupControlBlockEditVisits.TabIndex = 36;
            this.groupControlBlockEditVisits.Text = "<b>Block Edit</b>  -  Use this panel to block edit selected Visits  -  <b>0 Visit" +
    "s Selected</b>";
            // 
            // separatorControl2
            // 
            this.separatorControl2.LineOrientation = System.Windows.Forms.Orientation.Vertical;
            this.separatorControl2.LineThickness = 2;
            this.separatorControl2.Location = new System.Drawing.Point(810, 4);
            this.separatorControl2.Name = "separatorControl2";
            this.separatorControl2.Size = new System.Drawing.Size(18, 22);
            this.separatorControl2.TabIndex = 56;
            // 
            // separatorControl1
            // 
            this.separatorControl1.LineOrientation = System.Windows.Forms.Orientation.Vertical;
            this.separatorControl1.LineThickness = 2;
            this.separatorControl1.Location = new System.Drawing.Point(380, 5);
            this.separatorControl1.Name = "separatorControl1";
            this.separatorControl1.Size = new System.Drawing.Size(18, 22);
            this.separatorControl1.TabIndex = 55;
            // 
            // btnApplyVisitStatus
            // 
            this.btnApplyVisitStatus.ImageOptions.ImageIndex = 12;
            this.btnApplyVisitStatus.ImageOptions.ImageList = this.imageCollection1;
            this.btnApplyVisitStatus.Location = new System.Drawing.Point(1036, 5);
            this.btnApplyVisitStatus.Name = "btnApplyVisitStatus";
            this.btnApplyVisitStatus.Size = new System.Drawing.Size(62, 22);
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Apply Visit Status- Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to apply the entered Visit Status to the <b>selected</b> Visits.\r\n\r\nOnly" +
    " selected Visits will be effected. All others will be left as is.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.btnApplyVisitStatus.SuperTip = superToolTip1;
            this.btnApplyVisitStatus.TabIndex = 37;
            this.btnApplyVisitStatus.Text = "Apply";
            this.btnApplyVisitStatus.Click += new System.EventHandler(this.btnApplyVisitStatus_Click);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 6);
            this.imageCollection1.Images.SetKeyName(6, "refresh_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.BlockEdit_16x16, "BlockEdit_16x16", typeof(global::WoodPlan5.Properties.Resources), 7);
            this.imageCollection1.Images.SetKeyName(7, "BlockEdit_16x16");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 8);
            this.imageCollection1.Images.SetKeyName(8, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("groupheader_16x16.png", "images/reports/groupheader_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/reports/groupheader_16x16.png"), 9);
            this.imageCollection1.Images.SetKeyName(9, "groupheader_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 10);
            this.imageCollection1.Images.SetKeyName(10, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Calculate_16x16, "Calculate_16x16", typeof(global::WoodPlan5.Properties.Resources), 11);
            this.imageCollection1.Images.SetKeyName(11, "Calculate_16x16");
            this.imageCollection1.InsertGalleryImage("apply_16x16.png", "images/actions/apply_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_16x16.png"), 12);
            this.imageCollection1.Images.SetKeyName(12, "apply_16x16.png");
            this.imageCollection1.InsertGalleryImage("accounting_16x16.png", "images/number%20formats/accounting_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/number%20formats/accounting_16x16.png"), 13);
            this.imageCollection1.Images.SetKeyName(13, "accounting_16x16.png");
            // 
            // gridLookUpEditVisitStatusBlockEdit
            // 
            this.gridLookUpEditVisitStatusBlockEdit.EditValue = 5;
            this.gridLookUpEditVisitStatusBlockEdit.Location = new System.Drawing.Point(881, 6);
            this.gridLookUpEditVisitStatusBlockEdit.MenuManager = this.barManager1;
            this.gridLookUpEditVisitStatusBlockEdit.Name = "gridLookUpEditVisitStatusBlockEdit";
            this.gridLookUpEditVisitStatusBlockEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditVisitStatusBlockEdit.Properties.DataSource = this.sp06538OMVisitStatusesNotReadyAndReadyNoBlankBindingSource;
            this.gridLookUpEditVisitStatusBlockEdit.Properties.DisplayMember = "Description";
            this.gridLookUpEditVisitStatusBlockEdit.Properties.NullText = "";
            this.gridLookUpEditVisitStatusBlockEdit.Properties.PopupView = this.gridView3;
            this.gridLookUpEditVisitStatusBlockEdit.Properties.ValueMember = "ID";
            this.gridLookUpEditVisitStatusBlockEdit.Size = new System.Drawing.Size(152, 20);
            this.gridLookUpEditVisitStatusBlockEdit.TabIndex = 29;
            // 
            // sp06538OMVisitStatusesNotReadyAndReadyNoBlankBindingSource
            // 
            this.sp06538OMVisitStatusesNotReadyAndReadyNoBlankBindingSource.DataMember = "sp06538_OM_Visit_Statuses_Not_Ready_And_Ready_No_Blank";
            this.sp06538OMVisitStatusesNotReadyAndReadyNoBlankBindingSource.DataSource = this.dataSet_OM_Tender;
            // 
            // dataSet_OM_Tender
            // 
            this.dataSet_OM_Tender.DataSetName = "DataSet_OM_Tender";
            this.dataSet_OM_Tender.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 53;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Visit Status";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "RecordOrder";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(842, 10);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(35, 13);
            this.labelControl17.TabIndex = 54;
            this.labelControl17.Text = "Status:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(635, 10);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(26, 13);
            this.labelControl5.TabIndex = 34;
            this.labelControl5.Text = "Time:";
            // 
            // VisitDaysSeparationSpinEdit
            // 
            this.VisitDaysSeparationSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.VisitDaysSeparationSpinEdit.Location = new System.Drawing.Point(116, 6);
            this.VisitDaysSeparationSpinEdit.MenuManager = this.barManager1;
            this.VisitDaysSeparationSpinEdit.Name = "VisitDaysSeparationSpinEdit";
            this.VisitDaysSeparationSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitDaysSeparationSpinEdit.Properties.IsFloatValue = false;
            this.VisitDaysSeparationSpinEdit.Properties.Mask.EditMask = "n0";
            this.VisitDaysSeparationSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.VisitDaysSeparationSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.VisitDaysSeparationSpinEdit.Size = new System.Drawing.Size(106, 20);
            this.VisitDaysSeparationSpinEdit.TabIndex = 30;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(412, 10);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(67, 13);
            this.labelControl4.TabIndex = 33;
            this.labelControl4.Text = "Visit Duration:";
            // 
            // VisitDescriptorComboBoxEdit
            // 
            this.VisitDescriptorComboBoxEdit.EditValue = "Days";
            this.VisitDescriptorComboBoxEdit.Location = new System.Drawing.Point(574, 6);
            this.VisitDescriptorComboBoxEdit.MenuManager = this.barManager1;
            this.VisitDescriptorComboBoxEdit.Name = "VisitDescriptorComboBoxEdit";
            this.VisitDescriptorComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitDescriptorComboBoxEdit.Properties.Items.AddRange(new object[] {
            "Minutes",
            "Hours",
            "Days",
            "Weeks",
            "Months",
            "Years"});
            this.VisitDescriptorComboBoxEdit.Size = new System.Drawing.Size(52, 20);
            this.VisitDescriptorComboBoxEdit.TabIndex = 32;
            this.VisitDescriptorComboBoxEdit.EditValueChanged += new System.EventHandler(this.VisitDescriptorComboBoxEdit_EditValueChanged);
            // 
            // btnApplyVisitDuration
            // 
            this.btnApplyVisitDuration.ImageOptions.ImageIndex = 12;
            this.btnApplyVisitDuration.ImageOptions.ImageList = this.imageCollection1;
            this.btnApplyVisitDuration.Location = new System.Drawing.Point(732, 5);
            this.btnApplyVisitDuration.Name = "btnApplyVisitDuration";
            this.btnApplyVisitDuration.Size = new System.Drawing.Size(62, 22);
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Apply Visit Duration - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to apply the entered Visit Duration to the <b>selected</b> Visits.\r\n\r\nOn" +
    "ly selected Visits will be effected. All others will be left as is.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.btnApplyVisitDuration.SuperTip = superToolTip2;
            this.btnApplyVisitDuration.TabIndex = 29;
            this.btnApplyVisitDuration.Text = "Apply";
            this.btnApplyVisitDuration.Click += new System.EventHandler(this.btnApplyVisitDuration_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(7, 10);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(105, 13);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "Visit Days Separation:";
            // 
            // VisitDurationSpinEdit
            // 
            this.VisitDurationSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.VisitDurationSpinEdit.Location = new System.Drawing.Point(483, 6);
            this.VisitDurationSpinEdit.MenuManager = this.barManager1;
            this.VisitDurationSpinEdit.Name = "VisitDurationSpinEdit";
            this.VisitDurationSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitDurationSpinEdit.Properties.IsFloatValue = false;
            this.VisitDurationSpinEdit.Properties.Mask.EditMask = "n0";
            this.VisitDurationSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.VisitDurationSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.VisitDurationSpinEdit.Size = new System.Drawing.Size(88, 20);
            this.VisitDurationSpinEdit.TabIndex = 30;
            // 
            // btnCalculateVisitDaysSeparation
            // 
            this.btnCalculateVisitDaysSeparation.ImageOptions.ImageIndex = 11;
            this.btnCalculateVisitDaysSeparation.ImageOptions.ImageList = this.imageCollection1;
            this.btnCalculateVisitDaysSeparation.Location = new System.Drawing.Point(291, 5);
            this.btnCalculateVisitDaysSeparation.Name = "btnCalculateVisitDaysSeparation";
            this.btnCalculateVisitDaysSeparation.Size = new System.Drawing.Size(72, 22);
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Calculate Visit Days Separation - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = resources.GetString("toolTipItem3.Text");
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.btnCalculateVisitDaysSeparation.SuperTip = superToolTip3;
            this.btnCalculateVisitDaysSeparation.TabIndex = 32;
            this.btnCalculateVisitDaysSeparation.Text = "Calculate";
            this.btnCalculateVisitDaysSeparation.Click += new System.EventHandler(this.btnCalculateVisitDaysSeparation_Click);
            // 
            // btnApplyVisitDaysSeparation
            // 
            this.btnApplyVisitDaysSeparation.ImageOptions.ImageIndex = 12;
            this.btnApplyVisitDaysSeparation.ImageOptions.ImageList = this.imageCollection1;
            this.btnApplyVisitDaysSeparation.Location = new System.Drawing.Point(225, 5);
            this.btnApplyVisitDaysSeparation.Name = "btnApplyVisitDaysSeparation";
            this.btnApplyVisitDaysSeparation.Size = new System.Drawing.Size(63, 22);
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Apply Visit Days Separation - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to apply the entered Visit Days Separation to the <b>selected</b> Visits" +
    ".\r\n\r\nOnly selected Visits will be effected. All others will be left as is.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.btnApplyVisitDaysSeparation.SuperTip = superToolTip4;
            this.btnApplyVisitDaysSeparation.TabIndex = 29;
            this.btnApplyVisitDaysSeparation.Text = "Apply";
            this.btnApplyVisitDaysSeparation.Click += new System.EventHandler(this.btnApplyVisitDaysSeparation_Click);
            // 
            // VisitTimeEdit
            // 
            this.VisitTimeEdit.EditValue = System.TimeSpan.Parse("00:00:00");
            this.VisitTimeEdit.Location = new System.Drawing.Point(665, 6);
            this.VisitTimeEdit.MenuManager = this.barManager1;
            this.VisitTimeEdit.Name = "VisitTimeEdit";
            this.VisitTimeEdit.Properties.AllowEditDays = false;
            this.VisitTimeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.VisitTimeEdit.Properties.Mask.EditMask = "HH:mm";
            this.VisitTimeEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.VisitTimeEdit.Properties.MaxDays = 0;
            this.VisitTimeEdit.Properties.MaxMilliseconds = 0;
            this.VisitTimeEdit.Properties.MaxSeconds = 0;
            this.VisitTimeEdit.Size = new System.Drawing.Size(64, 20);
            this.VisitTimeEdit.TabIndex = 46;
            // 
            // panelControl4
            // 
            this.panelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl4.Controls.Add(this.btnSplitCosts);
            this.panelControl4.Controls.Add(this.simpleButton6);
            this.panelControl4.Controls.Add(this.simpleButton5);
            this.panelControl4.Controls.Add(this.labelControl14);
            this.panelControl4.Controls.Add(this.simpleButton7);
            this.panelControl4.Controls.Add(this.simpleButton8);
            this.panelControl4.Controls.Add(this.simpleButton9);
            this.panelControl4.Controls.Add(this.simpleButton10);
            this.panelControl4.Controls.Add(this.textEditCalculatedTotalSell);
            this.panelControl4.Controls.Add(this.textEditCalculatedEquipmentSell);
            this.panelControl4.Controls.Add(this.textEditCalculatedMaterialSell);
            this.panelControl4.Controls.Add(this.textEditCalculatedLabourSell);
            this.panelControl4.Controls.Add(this.textEditTenderTotalSell);
            this.panelControl4.Controls.Add(this.textEditTenderEquipmentSell);
            this.panelControl4.Controls.Add(this.textEditTenderMaterialSell);
            this.panelControl4.Controls.Add(this.textEditTenderLabourSell);
            this.panelControl4.Controls.Add(this.simpleButton4);
            this.panelControl4.Controls.Add(this.simpleButton3);
            this.panelControl4.Controls.Add(this.simpleButton2);
            this.panelControl4.Controls.Add(this.simpleButton1);
            this.panelControl4.Controls.Add(this.textEditCalculatedTotalCost);
            this.panelControl4.Controls.Add(this.textEditCalculatedEquipmentCost);
            this.panelControl4.Controls.Add(this.textEditCalculatedMaterialCost);
            this.panelControl4.Controls.Add(this.textEditCalculatedLabourCost);
            this.panelControl4.Controls.Add(this.textEditTenderTotalCost);
            this.panelControl4.Controls.Add(this.textEditTenderEquipmentCost);
            this.panelControl4.Controls.Add(this.textEditTenderMaterialCost);
            this.panelControl4.Controls.Add(this.textEditTenderLabourCost);
            this.panelControl4.Controls.Add(this.TenderDetailsLabelControl2);
            this.panelControl4.Controls.Add(this.pictureEdit3);
            this.panelControl4.Controls.Add(this.labelControl15);
            this.panelControl4.Location = new System.Drawing.Point(7, 6);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1446, 75);
            this.panelControl4.TabIndex = 35;
            // 
            // btnSplitCosts
            // 
            this.btnSplitCosts.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSplitCosts.ImageOptions.ImageIndex = 13;
            this.btnSplitCosts.ImageOptions.ImageList = this.imageCollection1;
            this.btnSplitCosts.Location = new System.Drawing.Point(774, 2);
            this.btnSplitCosts.Name = "btnSplitCosts";
            this.btnSplitCosts.Size = new System.Drawing.Size(108, 29);
            this.btnSplitCosts.TabIndex = 33;
            this.btnSplitCosts.Text = "Split Over Visits";
            this.btnSplitCosts.Click += new System.EventHandler(this.btnSplitCosts_Click);
            // 
            // simpleButton6
            // 
            this.simpleButton6.AllowFocus = false;
            this.simpleButton6.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.simpleButton6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton6.Appearance.Options.UseTextOptions = true;
            this.simpleButton6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.simpleButton6.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleButton6.Location = new System.Drawing.Point(774, 52);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(110, 21);
            this.simpleButton6.TabIndex = 39;
            this.simpleButton6.Text = "<b>Calculated</b> Values:";
            // 
            // simpleButton5
            // 
            this.simpleButton5.AllowFocus = false;
            this.simpleButton5.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.simpleButton5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton5.Appearance.Options.UseTextOptions = true;
            this.simpleButton5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.simpleButton5.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleButton5.Location = new System.Drawing.Point(774, 32);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(110, 21);
            this.simpleButton5.TabIndex = 38;
            this.simpleButton5.Text = "<b>Tender</b> Values:";
            // 
            // labelControl14
            // 
            this.labelControl14.AllowHtmlString = true;
            this.labelControl14.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl14.Appearance.Options.UseBackColor = true;
            this.labelControl14.Location = new System.Drawing.Point(9, 35);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(225, 13);
            this.labelControl14.TabIndex = 52;
            this.labelControl14.Text = "Add one ore more Visits to the selected Tender";
            // 
            // simpleButton7
            // 
            this.simpleButton7.AllowFocus = false;
            this.simpleButton7.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.simpleButton7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton7.Appearance.Options.UseTextOptions = true;
            this.simpleButton7.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleButton7.Location = new System.Drawing.Point(1373, 2);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(71, 31);
            this.simpleButton7.TabIndex = 51;
            this.simpleButton7.Text = "<b>Total<br>Sell</b>";
            // 
            // simpleButton8
            // 
            this.simpleButton8.AllowFocus = false;
            this.simpleButton8.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.simpleButton8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton8.Appearance.Options.UseTextOptions = true;
            this.simpleButton8.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleButton8.Location = new System.Drawing.Point(1303, 2);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(71, 31);
            this.simpleButton8.TabIndex = 50;
            this.simpleButton8.Text = "Equipment<br>Sell";
            // 
            // simpleButton9
            // 
            this.simpleButton9.AllowFocus = false;
            this.simpleButton9.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.simpleButton9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton9.Appearance.Options.UseTextOptions = true;
            this.simpleButton9.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleButton9.Location = new System.Drawing.Point(1233, 2);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(71, 31);
            this.simpleButton9.TabIndex = 49;
            this.simpleButton9.Text = "Material<br>Sell";
            // 
            // simpleButton10
            // 
            this.simpleButton10.AllowFocus = false;
            this.simpleButton10.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.simpleButton10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton10.Appearance.Options.UseTextOptions = true;
            this.simpleButton10.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleButton10.Location = new System.Drawing.Point(1163, 2);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(71, 31);
            this.simpleButton10.TabIndex = 48;
            this.simpleButton10.Text = "Labour<br>Sell";
            // 
            // textEditCalculatedTotalSell
            // 
            this.textEditCalculatedTotalSell.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditCalculatedTotalSell.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.textEditCalculatedTotalSell.Location = new System.Drawing.Point(1373, 52);
            this.textEditCalculatedTotalSell.MenuManager = this.barManager1;
            this.textEditCalculatedTotalSell.Name = "textEditCalculatedTotalSell";
            this.textEditCalculatedTotalSell.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditCalculatedTotalSell.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedTotalSell.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditCalculatedTotalSell.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedTotalSell.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditCalculatedTotalSell.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedTotalSell.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditCalculatedTotalSell.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedTotalSell.Properties.AutoHeight = false;
            this.textEditCalculatedTotalSell.Properties.Mask.EditMask = "c";
            this.textEditCalculatedTotalSell.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditCalculatedTotalSell.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditCalculatedTotalSell.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditCalculatedTotalSell, true);
            this.textEditCalculatedTotalSell.Size = new System.Drawing.Size(71, 21);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditCalculatedTotalSell, optionsSpelling1);
            this.textEditCalculatedTotalSell.TabIndex = 47;
            // 
            // textEditCalculatedEquipmentSell
            // 
            this.textEditCalculatedEquipmentSell.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditCalculatedEquipmentSell.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.textEditCalculatedEquipmentSell.Location = new System.Drawing.Point(1303, 52);
            this.textEditCalculatedEquipmentSell.MenuManager = this.barManager1;
            this.textEditCalculatedEquipmentSell.Name = "textEditCalculatedEquipmentSell";
            this.textEditCalculatedEquipmentSell.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditCalculatedEquipmentSell.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedEquipmentSell.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditCalculatedEquipmentSell.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedEquipmentSell.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditCalculatedEquipmentSell.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedEquipmentSell.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditCalculatedEquipmentSell.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedEquipmentSell.Properties.AutoHeight = false;
            this.textEditCalculatedEquipmentSell.Properties.Mask.EditMask = "c";
            this.textEditCalculatedEquipmentSell.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditCalculatedEquipmentSell.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditCalculatedEquipmentSell.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditCalculatedEquipmentSell, true);
            this.textEditCalculatedEquipmentSell.Size = new System.Drawing.Size(71, 21);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditCalculatedEquipmentSell, optionsSpelling2);
            this.textEditCalculatedEquipmentSell.TabIndex = 46;
            // 
            // textEditCalculatedMaterialSell
            // 
            this.textEditCalculatedMaterialSell.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditCalculatedMaterialSell.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.textEditCalculatedMaterialSell.Location = new System.Drawing.Point(1233, 52);
            this.textEditCalculatedMaterialSell.MenuManager = this.barManager1;
            this.textEditCalculatedMaterialSell.Name = "textEditCalculatedMaterialSell";
            this.textEditCalculatedMaterialSell.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditCalculatedMaterialSell.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedMaterialSell.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditCalculatedMaterialSell.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedMaterialSell.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditCalculatedMaterialSell.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedMaterialSell.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditCalculatedMaterialSell.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedMaterialSell.Properties.AutoHeight = false;
            this.textEditCalculatedMaterialSell.Properties.Mask.EditMask = "c";
            this.textEditCalculatedMaterialSell.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditCalculatedMaterialSell.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditCalculatedMaterialSell.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditCalculatedMaterialSell, true);
            this.textEditCalculatedMaterialSell.Size = new System.Drawing.Size(71, 21);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditCalculatedMaterialSell, optionsSpelling3);
            this.textEditCalculatedMaterialSell.TabIndex = 45;
            // 
            // textEditCalculatedLabourSell
            // 
            this.textEditCalculatedLabourSell.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditCalculatedLabourSell.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.textEditCalculatedLabourSell.Location = new System.Drawing.Point(1163, 52);
            this.textEditCalculatedLabourSell.MenuManager = this.barManager1;
            this.textEditCalculatedLabourSell.Name = "textEditCalculatedLabourSell";
            this.textEditCalculatedLabourSell.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditCalculatedLabourSell.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedLabourSell.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditCalculatedLabourSell.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedLabourSell.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditCalculatedLabourSell.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedLabourSell.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditCalculatedLabourSell.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedLabourSell.Properties.AutoHeight = false;
            this.textEditCalculatedLabourSell.Properties.Mask.EditMask = "c";
            this.textEditCalculatedLabourSell.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditCalculatedLabourSell.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditCalculatedLabourSell.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditCalculatedLabourSell, true);
            this.textEditCalculatedLabourSell.Size = new System.Drawing.Size(71, 21);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditCalculatedLabourSell, optionsSpelling4);
            this.textEditCalculatedLabourSell.TabIndex = 44;
            // 
            // textEditTenderTotalSell
            // 
            this.textEditTenderTotalSell.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditTenderTotalSell.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.textEditTenderTotalSell.Location = new System.Drawing.Point(1373, 32);
            this.textEditTenderTotalSell.MenuManager = this.barManager1;
            this.textEditTenderTotalSell.Name = "textEditTenderTotalSell";
            this.textEditTenderTotalSell.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditTenderTotalSell.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderTotalSell.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditTenderTotalSell.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderTotalSell.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditTenderTotalSell.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderTotalSell.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditTenderTotalSell.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderTotalSell.Properties.AutoHeight = false;
            this.textEditTenderTotalSell.Properties.Mask.EditMask = "c";
            this.textEditTenderTotalSell.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditTenderTotalSell.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditTenderTotalSell.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditTenderTotalSell, true);
            this.textEditTenderTotalSell.Size = new System.Drawing.Size(71, 21);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditTenderTotalSell, optionsSpelling5);
            this.textEditTenderTotalSell.TabIndex = 43;
            // 
            // textEditTenderEquipmentSell
            // 
            this.textEditTenderEquipmentSell.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditTenderEquipmentSell.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.textEditTenderEquipmentSell.Location = new System.Drawing.Point(1303, 32);
            this.textEditTenderEquipmentSell.MenuManager = this.barManager1;
            this.textEditTenderEquipmentSell.Name = "textEditTenderEquipmentSell";
            this.textEditTenderEquipmentSell.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditTenderEquipmentSell.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderEquipmentSell.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditTenderEquipmentSell.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderEquipmentSell.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditTenderEquipmentSell.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderEquipmentSell.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditTenderEquipmentSell.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderEquipmentSell.Properties.AutoHeight = false;
            this.textEditTenderEquipmentSell.Properties.Mask.EditMask = "c";
            this.textEditTenderEquipmentSell.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditTenderEquipmentSell.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditTenderEquipmentSell.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditTenderEquipmentSell, true);
            this.textEditTenderEquipmentSell.Size = new System.Drawing.Size(71, 21);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditTenderEquipmentSell, optionsSpelling6);
            this.textEditTenderEquipmentSell.TabIndex = 42;
            // 
            // textEditTenderMaterialSell
            // 
            this.textEditTenderMaterialSell.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditTenderMaterialSell.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.textEditTenderMaterialSell.Location = new System.Drawing.Point(1233, 32);
            this.textEditTenderMaterialSell.MenuManager = this.barManager1;
            this.textEditTenderMaterialSell.Name = "textEditTenderMaterialSell";
            this.textEditTenderMaterialSell.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditTenderMaterialSell.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderMaterialSell.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditTenderMaterialSell.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderMaterialSell.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditTenderMaterialSell.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderMaterialSell.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditTenderMaterialSell.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderMaterialSell.Properties.AutoHeight = false;
            this.textEditTenderMaterialSell.Properties.Mask.EditMask = "c";
            this.textEditTenderMaterialSell.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditTenderMaterialSell.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditTenderMaterialSell.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditTenderMaterialSell, true);
            this.textEditTenderMaterialSell.Size = new System.Drawing.Size(71, 21);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditTenderMaterialSell, optionsSpelling7);
            this.textEditTenderMaterialSell.TabIndex = 41;
            // 
            // textEditTenderLabourSell
            // 
            this.textEditTenderLabourSell.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditTenderLabourSell.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.textEditTenderLabourSell.Location = new System.Drawing.Point(1163, 32);
            this.textEditTenderLabourSell.MenuManager = this.barManager1;
            this.textEditTenderLabourSell.Name = "textEditTenderLabourSell";
            this.textEditTenderLabourSell.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(241)))), ((int)(((byte)(250)))));
            this.textEditTenderLabourSell.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.textEditTenderLabourSell.Properties.Appearance.Options.UseBackColor = true;
            this.textEditTenderLabourSell.Properties.Appearance.Options.UseForeColor = true;
            this.textEditTenderLabourSell.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditTenderLabourSell.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderLabourSell.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditTenderLabourSell.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderLabourSell.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditTenderLabourSell.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderLabourSell.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditTenderLabourSell.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderLabourSell.Properties.AutoHeight = false;
            this.textEditTenderLabourSell.Properties.Mask.EditMask = "c";
            this.textEditTenderLabourSell.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditTenderLabourSell.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditTenderLabourSell.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditTenderLabourSell, true);
            this.textEditTenderLabourSell.Size = new System.Drawing.Size(71, 21);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditTenderLabourSell, optionsSpelling8);
            this.textEditTenderLabourSell.TabIndex = 40;
            // 
            // simpleButton4
            // 
            this.simpleButton4.AllowFocus = false;
            this.simpleButton4.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.simpleButton4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton4.Appearance.Options.UseTextOptions = true;
            this.simpleButton4.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleButton4.Location = new System.Drawing.Point(1093, 2);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(70, 31);
            this.simpleButton4.TabIndex = 37;
            this.simpleButton4.Text = "<b>Total<br>Cost</b>";
            // 
            // simpleButton3
            // 
            this.simpleButton3.AllowFocus = false;
            this.simpleButton3.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.simpleButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton3.Appearance.Options.UseTextOptions = true;
            this.simpleButton3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleButton3.Location = new System.Drawing.Point(1023, 2);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(71, 31);
            this.simpleButton3.TabIndex = 36;
            this.simpleButton3.Text = "Equipment<br>Cost";
            // 
            // simpleButton2
            // 
            this.simpleButton2.AllowFocus = false;
            this.simpleButton2.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.simpleButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton2.Appearance.Options.UseTextOptions = true;
            this.simpleButton2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleButton2.Location = new System.Drawing.Point(953, 2);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(71, 31);
            this.simpleButton2.TabIndex = 35;
            this.simpleButton2.Text = "Material<br>Cost";
            // 
            // simpleButton1
            // 
            this.simpleButton1.AllowFocus = false;
            this.simpleButton1.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.Appearance.Options.UseTextOptions = true;
            this.simpleButton1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleButton1.Location = new System.Drawing.Point(883, 2);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(71, 31);
            this.simpleButton1.TabIndex = 34;
            this.simpleButton1.Text = "Labour<br>Cost";
            // 
            // textEditCalculatedTotalCost
            // 
            this.textEditCalculatedTotalCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditCalculatedTotalCost.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.textEditCalculatedTotalCost.Location = new System.Drawing.Point(1093, 52);
            this.textEditCalculatedTotalCost.MenuManager = this.barManager1;
            this.textEditCalculatedTotalCost.Name = "textEditCalculatedTotalCost";
            this.textEditCalculatedTotalCost.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditCalculatedTotalCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedTotalCost.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditCalculatedTotalCost.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedTotalCost.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditCalculatedTotalCost.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedTotalCost.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditCalculatedTotalCost.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedTotalCost.Properties.AutoHeight = false;
            this.textEditCalculatedTotalCost.Properties.Mask.EditMask = "c";
            this.textEditCalculatedTotalCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditCalculatedTotalCost.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditCalculatedTotalCost.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditCalculatedTotalCost, true);
            this.textEditCalculatedTotalCost.Size = new System.Drawing.Size(70, 21);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditCalculatedTotalCost, optionsSpelling9);
            this.textEditCalculatedTotalCost.TabIndex = 25;
            // 
            // textEditCalculatedEquipmentCost
            // 
            this.textEditCalculatedEquipmentCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditCalculatedEquipmentCost.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.textEditCalculatedEquipmentCost.Location = new System.Drawing.Point(1023, 52);
            this.textEditCalculatedEquipmentCost.MenuManager = this.barManager1;
            this.textEditCalculatedEquipmentCost.Name = "textEditCalculatedEquipmentCost";
            this.textEditCalculatedEquipmentCost.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditCalculatedEquipmentCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedEquipmentCost.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditCalculatedEquipmentCost.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedEquipmentCost.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditCalculatedEquipmentCost.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedEquipmentCost.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditCalculatedEquipmentCost.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedEquipmentCost.Properties.AutoHeight = false;
            this.textEditCalculatedEquipmentCost.Properties.Mask.EditMask = "c";
            this.textEditCalculatedEquipmentCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditCalculatedEquipmentCost.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditCalculatedEquipmentCost.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditCalculatedEquipmentCost, true);
            this.textEditCalculatedEquipmentCost.Size = new System.Drawing.Size(71, 21);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditCalculatedEquipmentCost, optionsSpelling10);
            this.textEditCalculatedEquipmentCost.TabIndex = 24;
            // 
            // textEditCalculatedMaterialCost
            // 
            this.textEditCalculatedMaterialCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditCalculatedMaterialCost.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.textEditCalculatedMaterialCost.Location = new System.Drawing.Point(953, 52);
            this.textEditCalculatedMaterialCost.MenuManager = this.barManager1;
            this.textEditCalculatedMaterialCost.Name = "textEditCalculatedMaterialCost";
            this.textEditCalculatedMaterialCost.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditCalculatedMaterialCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedMaterialCost.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditCalculatedMaterialCost.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedMaterialCost.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditCalculatedMaterialCost.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedMaterialCost.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditCalculatedMaterialCost.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedMaterialCost.Properties.AutoHeight = false;
            this.textEditCalculatedMaterialCost.Properties.Mask.EditMask = "c";
            this.textEditCalculatedMaterialCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditCalculatedMaterialCost.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditCalculatedMaterialCost.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditCalculatedMaterialCost, true);
            this.textEditCalculatedMaterialCost.Size = new System.Drawing.Size(71, 21);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditCalculatedMaterialCost, optionsSpelling11);
            this.textEditCalculatedMaterialCost.TabIndex = 23;
            // 
            // textEditCalculatedLabourCost
            // 
            this.textEditCalculatedLabourCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditCalculatedLabourCost.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.textEditCalculatedLabourCost.Location = new System.Drawing.Point(883, 52);
            this.textEditCalculatedLabourCost.MenuManager = this.barManager1;
            this.textEditCalculatedLabourCost.Name = "textEditCalculatedLabourCost";
            this.textEditCalculatedLabourCost.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditCalculatedLabourCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedLabourCost.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditCalculatedLabourCost.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedLabourCost.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditCalculatedLabourCost.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedLabourCost.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditCalculatedLabourCost.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditCalculatedLabourCost.Properties.AutoHeight = false;
            this.textEditCalculatedLabourCost.Properties.Mask.EditMask = "c";
            this.textEditCalculatedLabourCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditCalculatedLabourCost.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditCalculatedLabourCost.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditCalculatedLabourCost, true);
            this.textEditCalculatedLabourCost.Size = new System.Drawing.Size(71, 21);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditCalculatedLabourCost, optionsSpelling12);
            this.textEditCalculatedLabourCost.TabIndex = 22;
            // 
            // textEditTenderTotalCost
            // 
            this.textEditTenderTotalCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditTenderTotalCost.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.textEditTenderTotalCost.Location = new System.Drawing.Point(1093, 32);
            this.textEditTenderTotalCost.MenuManager = this.barManager1;
            this.textEditTenderTotalCost.Name = "textEditTenderTotalCost";
            this.textEditTenderTotalCost.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditTenderTotalCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderTotalCost.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditTenderTotalCost.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderTotalCost.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditTenderTotalCost.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderTotalCost.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditTenderTotalCost.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderTotalCost.Properties.AutoHeight = false;
            this.textEditTenderTotalCost.Properties.Mask.EditMask = "c";
            this.textEditTenderTotalCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditTenderTotalCost.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditTenderTotalCost.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditTenderTotalCost, true);
            this.textEditTenderTotalCost.Size = new System.Drawing.Size(70, 21);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditTenderTotalCost, optionsSpelling13);
            this.textEditTenderTotalCost.TabIndex = 21;
            // 
            // textEditTenderEquipmentCost
            // 
            this.textEditTenderEquipmentCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditTenderEquipmentCost.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.textEditTenderEquipmentCost.Location = new System.Drawing.Point(1023, 32);
            this.textEditTenderEquipmentCost.MenuManager = this.barManager1;
            this.textEditTenderEquipmentCost.Name = "textEditTenderEquipmentCost";
            this.textEditTenderEquipmentCost.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditTenderEquipmentCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderEquipmentCost.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditTenderEquipmentCost.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderEquipmentCost.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditTenderEquipmentCost.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderEquipmentCost.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditTenderEquipmentCost.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderEquipmentCost.Properties.AutoHeight = false;
            this.textEditTenderEquipmentCost.Properties.Mask.EditMask = "c";
            this.textEditTenderEquipmentCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditTenderEquipmentCost.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditTenderEquipmentCost.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditTenderEquipmentCost, true);
            this.textEditTenderEquipmentCost.Size = new System.Drawing.Size(71, 21);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditTenderEquipmentCost, optionsSpelling14);
            this.textEditTenderEquipmentCost.TabIndex = 20;
            // 
            // textEditTenderMaterialCost
            // 
            this.textEditTenderMaterialCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditTenderMaterialCost.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.textEditTenderMaterialCost.Location = new System.Drawing.Point(953, 32);
            this.textEditTenderMaterialCost.MenuManager = this.barManager1;
            this.textEditTenderMaterialCost.Name = "textEditTenderMaterialCost";
            this.textEditTenderMaterialCost.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditTenderMaterialCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderMaterialCost.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditTenderMaterialCost.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderMaterialCost.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditTenderMaterialCost.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderMaterialCost.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditTenderMaterialCost.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderMaterialCost.Properties.AutoHeight = false;
            this.textEditTenderMaterialCost.Properties.Mask.EditMask = "c";
            this.textEditTenderMaterialCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditTenderMaterialCost.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditTenderMaterialCost.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditTenderMaterialCost, true);
            this.textEditTenderMaterialCost.Size = new System.Drawing.Size(71, 21);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditTenderMaterialCost, optionsSpelling15);
            this.textEditTenderMaterialCost.TabIndex = 19;
            // 
            // textEditTenderLabourCost
            // 
            this.textEditTenderLabourCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditTenderLabourCost.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.textEditTenderLabourCost.Location = new System.Drawing.Point(883, 32);
            this.textEditTenderLabourCost.MenuManager = this.barManager1;
            this.textEditTenderLabourCost.Name = "textEditTenderLabourCost";
            this.textEditTenderLabourCost.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(241)))), ((int)(((byte)(250)))));
            this.textEditTenderLabourCost.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.textEditTenderLabourCost.Properties.Appearance.Options.UseBackColor = true;
            this.textEditTenderLabourCost.Properties.Appearance.Options.UseForeColor = true;
            this.textEditTenderLabourCost.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditTenderLabourCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderLabourCost.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditTenderLabourCost.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderLabourCost.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditTenderLabourCost.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderLabourCost.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditTenderLabourCost.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderLabourCost.Properties.AutoHeight = false;
            this.textEditTenderLabourCost.Properties.Mask.EditMask = "c";
            this.textEditTenderLabourCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditTenderLabourCost.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditTenderLabourCost.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditTenderLabourCost, true);
            this.textEditTenderLabourCost.Size = new System.Drawing.Size(71, 21);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditTenderLabourCost, optionsSpelling16);
            this.textEditTenderLabourCost.TabIndex = 18;
            // 
            // TenderDetailsLabelControl2
            // 
            this.TenderDetailsLabelControl2.AllowHtmlString = true;
            this.TenderDetailsLabelControl2.Location = new System.Drawing.Point(9, 57);
            this.TenderDetailsLabelControl2.Name = "TenderDetailsLabelControl2";
            this.TenderDetailsLabelControl2.Size = new System.Drawing.Size(82, 13);
            this.TenderDetailsLabelControl2.TabIndex = 17;
            this.TenderDetailsLabelControl2.Text = "Selected Tender:";
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit3.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit3.Location = new System.Drawing.Point(728, 4);
            this.pictureEdit3.MenuManager = this.barManager1;
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.ReadOnly = true;
            this.pictureEdit3.Properties.ShowMenu = false;
            this.pictureEdit3.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit3.TabIndex = 16;
            // 
            // labelControl15
            // 
            this.labelControl15.AllowHtmlString = true;
            this.labelControl15.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.labelControl15.Appearance.Options.UseBackColor = true;
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Location = new System.Drawing.Point(5, 1);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(190, 25);
            this.labelControl15.TabIndex = 5;
            this.labelControl15.Text = "Step 1 - Add Visit(s)";
            // 
            // groupControlAddVisits
            // 
            this.groupControlAddVisits.AllowHtmlText = true;
            this.groupControlAddVisits.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControlAddVisits.Controls.Add(this.GapfirstVisitCheckEdit);
            this.groupControlAddVisits.Controls.Add(this.gridLookUpEditVisitStatusID);
            this.groupControlAddVisits.Controls.Add(this.labelControl16);
            this.groupControlAddVisits.Controls.Add(this.dateEditStartDate);
            this.groupControlAddVisits.Controls.Add(this.labelControl13);
            this.groupControlAddVisits.Controls.Add(this.labelControl8);
            this.groupControlAddVisits.Controls.Add(this.StartDateLabel);
            this.groupControlAddVisits.Controls.Add(this.labelControl7);
            this.groupControlAddVisits.Controls.Add(this.labelControl6);
            this.groupControlAddVisits.Controls.Add(this.spinEditNumberOfVisits);
            this.groupControlAddVisits.Controls.Add(this.gridLookUpEditSellCalculationLevel);
            this.groupControlAddVisits.Controls.Add(this.btnAddVisits);
            this.groupControlAddVisits.Controls.Add(this.spinEditGapBetweenVisitsUnits);
            this.groupControlAddVisits.Controls.Add(this.gridLookUpEditGapBetweenVisitsDescriptor);
            this.groupControlAddVisits.Controls.Add(this.gridLookUpEditCostCalculationLevel);
            this.groupControlAddVisits.Location = new System.Drawing.Point(7, 90);
            this.groupControlAddVisits.Name = "groupControlAddVisits";
            this.groupControlAddVisits.Size = new System.Drawing.Size(1446, 51);
            this.groupControlAddVisits.TabIndex = 34;
            this.groupControlAddVisits.Text = "<b>Add Visits</b>  -  Use this panel to specify defaults then add visits";
            // 
            // GapfirstVisitCheckEdit
            // 
            this.GapfirstVisitCheckEdit.EditValue = 1;
            this.GapfirstVisitCheckEdit.Location = new System.Drawing.Point(573, 27);
            this.GapfirstVisitCheckEdit.MenuManager = this.barManager1;
            this.GapfirstVisitCheckEdit.Name = "GapfirstVisitCheckEdit";
            this.GapfirstVisitCheckEdit.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.GapfirstVisitCheckEdit.Properties.Caption = "Gap <b>First</b> Visit";
            this.GapfirstVisitCheckEdit.Properties.ValueChecked = 1;
            this.GapfirstVisitCheckEdit.Properties.ValueUnchecked = 0;
            this.GapfirstVisitCheckEdit.Size = new System.Drawing.Size(91, 19);
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Gap First Visit - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Tick me to apply the Gap to the first visit from the Start date. If unticked, the" +
    " first Visit will occur on the Start Date and only subsequent visits will have t" +
    "he gap calculated.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.GapfirstVisitCheckEdit.SuperTip = superToolTip5;
            this.GapfirstVisitCheckEdit.TabIndex = 54;
            // 
            // gridLookUpEditVisitStatusID
            // 
            this.gridLookUpEditVisitStatusID.EditValue = 5;
            this.gridLookUpEditVisitStatusID.Location = new System.Drawing.Point(719, 26);
            this.gridLookUpEditVisitStatusID.MenuManager = this.barManager1;
            this.gridLookUpEditVisitStatusID.Name = "gridLookUpEditVisitStatusID";
            this.gridLookUpEditVisitStatusID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditVisitStatusID.Properties.DataSource = this.sp06538OMVisitStatusesNotReadyAndReadyNoBlankBindingSource;
            this.gridLookUpEditVisitStatusID.Properties.DisplayMember = "Description";
            this.gridLookUpEditVisitStatusID.Properties.NullText = "";
            this.gridLookUpEditVisitStatusID.Properties.PopupView = this.gridView1;
            this.gridLookUpEditVisitStatusID.Properties.ValueMember = "ID";
            this.gridLookUpEditVisitStatusID.Size = new System.Drawing.Size(152, 20);
            this.gridLookUpEditVisitStatusID.TabIndex = 28;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 53;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Visit Status";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(680, 30);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(35, 13);
            this.labelControl16.TabIndex = 53;
            this.labelControl16.Text = "Status:";
            // 
            // dateEditStartDate
            // 
            this.dateEditStartDate.EditValue = null;
            this.dateEditStartDate.Location = new System.Drawing.Point(218, 27);
            this.dateEditStartDate.MenuManager = this.barManager1;
            this.dateEditStartDate.Name = "dateEditStartDate";
            this.dateEditStartDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStartDate.Properties.Mask.EditMask = "g";
            this.dateEditStartDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditStartDate.Properties.MinValue = new System.DateTime(2017, 1, 1, 0, 0, 0, 0);
            this.dateEditStartDate.Size = new System.Drawing.Size(125, 20);
            this.dateEditStartDate.TabIndex = 52;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(1129, 30);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(75, 13);
            this.labelControl13.TabIndex = 50;
            this.labelControl13.Text = "Sell Calculation:";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(879, 30);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(81, 13);
            this.labelControl8.TabIndex = 49;
            this.labelControl8.Text = "Cost Calculation:";
            // 
            // StartDateLabel
            // 
            this.StartDateLabel.Location = new System.Drawing.Point(160, 30);
            this.StartDateLabel.Name = "StartDateLabel";
            this.StartDateLabel.Size = new System.Drawing.Size(54, 13);
            this.StartDateLabel.TabIndex = 51;
            this.StartDateLabel.Text = "Start Date:";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(362, 30);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(95, 13);
            this.labelControl7.TabIndex = 48;
            this.labelControl7.Text = "Gap Between Visits:";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(7, 30);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(81, 13);
            this.labelControl6.TabIndex = 47;
            this.labelControl6.Text = "Number of Visits:";
            // 
            // spinEditNumberOfVisits
            // 
            this.spinEditNumberOfVisits.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditNumberOfVisits.Location = new System.Drawing.Point(92, 26);
            this.spinEditNumberOfVisits.MenuManager = this.barManager1;
            this.spinEditNumberOfVisits.Name = "spinEditNumberOfVisits";
            this.spinEditNumberOfVisits.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditNumberOfVisits.Properties.IsFloatValue = false;
            this.spinEditNumberOfVisits.Properties.Mask.EditMask = "n0";
            this.spinEditNumberOfVisits.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditNumberOfVisits.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.spinEditNumberOfVisits.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditNumberOfVisits.Size = new System.Drawing.Size(48, 20);
            this.spinEditNumberOfVisits.TabIndex = 23;
            // 
            // gridLookUpEditSellCalculationLevel
            // 
            this.gridLookUpEditSellCalculationLevel.EditValue = 0;
            this.gridLookUpEditSellCalculationLevel.Location = new System.Drawing.Point(1208, 26);
            this.gridLookUpEditSellCalculationLevel.MenuManager = this.barManager1;
            this.gridLookUpEditSellCalculationLevel.Name = "gridLookUpEditSellCalculationLevel";
            this.gridLookUpEditSellCalculationLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditSellCalculationLevel.Properties.DataSource = this.sp06134OMJobCalculationLevelDescriptorsBindingSource;
            this.gridLookUpEditSellCalculationLevel.Properties.DisplayMember = "Description";
            this.gridLookUpEditSellCalculationLevel.Properties.NullText = "";
            this.gridLookUpEditSellCalculationLevel.Properties.PopupView = this.gridView8;
            this.gridLookUpEditSellCalculationLevel.Properties.ValueMember = "ID";
            this.gridLookUpEditSellCalculationLevel.Size = new System.Drawing.Size(144, 20);
            this.gridLookUpEditSellCalculationLevel.TabIndex = 30;
            // 
            // sp06134OMJobCalculationLevelDescriptorsBindingSource
            // 
            this.sp06134OMJobCalculationLevelDescriptorsBindingSource.DataMember = "sp06134_OM_Job_Calculation_Level_Descriptors";
            this.sp06134OMJobCalculationLevelDescriptorsBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.gridColumn10;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = -1;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView8.FormatRules.Add(gridFormatRule1);
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView8.OptionsView.ShowIndicator = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Cost Calculation Level";
            this.gridColumn11.FieldName = "Description";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            this.gridColumn11.Width = 220;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Order";
            this.gridColumn12.FieldName = "RecordOrder";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            // 
            // btnAddVisits
            // 
            this.btnAddVisits.ImageOptions.ImageIndex = 0;
            this.btnAddVisits.ImageOptions.ImageList = this.imageCollection1;
            this.btnAddVisits.Location = new System.Drawing.Point(1360, 25);
            this.btnAddVisits.Name = "btnAddVisits";
            this.btnAddVisits.Size = new System.Drawing.Size(81, 22);
            this.btnAddVisits.TabIndex = 35;
            this.btnAddVisits.Text = "Add Visit(s)";
            this.btnAddVisits.Click += new System.EventHandler(this.btnAddVisits_Click);
            // 
            // spinEditGapBetweenVisitsUnits
            // 
            this.spinEditGapBetweenVisitsUnits.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditGapBetweenVisitsUnits.Location = new System.Drawing.Point(461, 26);
            this.spinEditGapBetweenVisitsUnits.MenuManager = this.barManager1;
            this.spinEditGapBetweenVisitsUnits.Name = "spinEditGapBetweenVisitsUnits";
            this.spinEditGapBetweenVisitsUnits.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditGapBetweenVisitsUnits.Properties.IsFloatValue = false;
            this.spinEditGapBetweenVisitsUnits.Properties.Mask.EditMask = "n0";
            this.spinEditGapBetweenVisitsUnits.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditGapBetweenVisitsUnits.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.spinEditGapBetweenVisitsUnits.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditGapBetweenVisitsUnits.Size = new System.Drawing.Size(40, 20);
            this.spinEditGapBetweenVisitsUnits.TabIndex = 26;
            // 
            // gridLookUpEditGapBetweenVisitsDescriptor
            // 
            this.gridLookUpEditGapBetweenVisitsDescriptor.EditValue = 2;
            this.gridLookUpEditGapBetweenVisitsDescriptor.Location = new System.Drawing.Point(504, 26);
            this.gridLookUpEditGapBetweenVisitsDescriptor.MenuManager = this.barManager1;
            this.gridLookUpEditGapBetweenVisitsDescriptor.Name = "gridLookUpEditGapBetweenVisitsDescriptor";
            this.gridLookUpEditGapBetweenVisitsDescriptor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditGapBetweenVisitsDescriptor.Properties.DataSource = this.sp06133OMVisitGapUnitDescriptorsBindingSource;
            this.gridLookUpEditGapBetweenVisitsDescriptor.Properties.DisplayMember = "Description";
            this.gridLookUpEditGapBetweenVisitsDescriptor.Properties.NullText = "";
            this.gridLookUpEditGapBetweenVisitsDescriptor.Properties.PopupView = this.gridLookUpEdit1View;
            this.gridLookUpEditGapBetweenVisitsDescriptor.Properties.ValueMember = "ID";
            this.gridLookUpEditGapBetweenVisitsDescriptor.Size = new System.Drawing.Size(66, 20);
            this.gridLookUpEditGapBetweenVisitsDescriptor.TabIndex = 27;
            // 
            // sp06133OMVisitGapUnitDescriptorsBindingSource
            // 
            this.sp06133OMVisitGapUnitDescriptorsBindingSource.DataMember = "sp06133_OM_Visit_Gap_Unit_Descriptors";
            this.sp06133OMVisitGapUnitDescriptorsBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn126,
            this.gridColumn127,
            this.gridColumn128});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn126;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn128, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn127
            // 
            this.gridColumn127.Caption = "Gap Descriptor";
            this.gridColumn127.FieldName = "Description";
            this.gridColumn127.Name = "gridColumn127";
            this.gridColumn127.OptionsColumn.AllowEdit = false;
            this.gridColumn127.OptionsColumn.AllowFocus = false;
            this.gridColumn127.OptionsColumn.ReadOnly = true;
            this.gridColumn127.Visible = true;
            this.gridColumn127.VisibleIndex = 0;
            this.gridColumn127.Width = 220;
            // 
            // gridColumn128
            // 
            this.gridColumn128.Caption = "Order";
            this.gridColumn128.FieldName = "RecordOrder";
            this.gridColumn128.Name = "gridColumn128";
            this.gridColumn128.OptionsColumn.AllowEdit = false;
            this.gridColumn128.OptionsColumn.AllowFocus = false;
            this.gridColumn128.OptionsColumn.ReadOnly = true;
            // 
            // gridLookUpEditCostCalculationLevel
            // 
            this.gridLookUpEditCostCalculationLevel.EditValue = 0;
            this.gridLookUpEditCostCalculationLevel.Location = new System.Drawing.Point(964, 26);
            this.gridLookUpEditCostCalculationLevel.MenuManager = this.barManager1;
            this.gridLookUpEditCostCalculationLevel.Name = "gridLookUpEditCostCalculationLevel";
            this.gridLookUpEditCostCalculationLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditCostCalculationLevel.Properties.DataSource = this.sp06134OMJobCalculationLevelDescriptorsBindingSource;
            this.gridLookUpEditCostCalculationLevel.Properties.DisplayMember = "Description";
            this.gridLookUpEditCostCalculationLevel.Properties.NullText = "";
            this.gridLookUpEditCostCalculationLevel.Properties.PopupView = this.gridView17;
            this.gridLookUpEditCostCalculationLevel.Properties.ValueMember = "ID";
            this.gridLookUpEditCostCalculationLevel.Size = new System.Drawing.Size(144, 20);
            this.gridLookUpEditCostCalculationLevel.TabIndex = 29;
            // 
            // gridView17
            // 
            this.gridView17.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn129,
            this.gridColumn130,
            this.gridColumn131});
            this.gridView17.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.gridColumn129;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = -1;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView17.FormatRules.Add(gridFormatRule2);
            this.gridView17.Name = "gridView17";
            this.gridView17.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView17.OptionsLayout.StoreAppearance = true;
            this.gridView17.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView17.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView17.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView17.OptionsView.ColumnAutoWidth = false;
            this.gridView17.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView17.OptionsView.ShowGroupPanel = false;
            this.gridView17.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView17.OptionsView.ShowIndicator = false;
            this.gridView17.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn131, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn130
            // 
            this.gridColumn130.Caption = "Cost Calculation Level";
            this.gridColumn130.FieldName = "Description";
            this.gridColumn130.Name = "gridColumn130";
            this.gridColumn130.OptionsColumn.AllowEdit = false;
            this.gridColumn130.OptionsColumn.AllowFocus = false;
            this.gridColumn130.OptionsColumn.ReadOnly = true;
            this.gridColumn130.Visible = true;
            this.gridColumn130.VisibleIndex = 0;
            this.gridColumn130.Width = 220;
            // 
            // gridColumn131
            // 
            this.gridColumn131.Caption = "Order";
            this.gridColumn131.FieldName = "RecordOrder";
            this.gridColumn131.Name = "gridColumn131";
            this.gridColumn131.OptionsColumn.AllowEdit = false;
            this.gridColumn131.OptionsColumn.AllowFocus = false;
            this.gridColumn131.OptionsColumn.ReadOnly = true;
            // 
            // gridControl6
            // 
            this.gridControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl6.DataSource = this.sp06145OMVisitEditBindingSource;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            this.gridControl6.Location = new System.Drawing.Point(7, 142);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditFloat,
            this.repositoryItemGridLookUpEditCostCalculationLevel,
            this.repositoryItemTextEdit1,
            this.repositoryItemGridLookUpEditVisitCategory,
            this.repositoryItemGridLookUpEditVisitTypes,
            this.repositoryItemButtonEditClientPONumber,
            this.repositoryItemSpinEditVisitNumber,
            this.repositoryItemSpinEditDaysSeparation,
            this.repositoryItemSpinEditCurrency,
            this.repositoryItemDateEdit1,
            this.repositoryItemGridLookUpEditVisitStatusID});
            this.gridControl6.Size = new System.Drawing.Size(1446, 423);
            this.gridControl6.TabIndex = 21;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp06145OMVisitEditBindingSource
            // 
            this.sp06145OMVisitEditBindingSource.DataMember = "sp06145_OM_Visit_Edit";
            this.sp06145OMVisitEditBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView6
            // 
            this.gridView6.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView6.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView6.ColumnPanelRowHeight = 36;
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colstrMode,
            this.colstrRecordIDs,
            this.colVisitID,
            this.colSiteContractID,
            this.colVisitNumber,
            this.colCreatedByStaffID,
            this.colExpectedStartDate,
            this.colExpectedEndDate,
            this.colStartDate,
            this.colEndDate,
            this.colCostCalculationLevel,
            this.colVisitCost,
            this.colVisitSell,
            this.colRemarks1,
            this.colStartLatitude,
            this.colStartLongitude,
            this.colFinishLatitude,
            this.colFinishLongitude,
            this.colClientContractID,
            this.colClientName1,
            this.colContractDescription1,
            this.colSiteName1,
            this.colCreatedByStaffName,
            this.colCalendarLabel,
            this.colLabelColourID,
            this.colWorkNumber,
            this.colVisitCategoryID,
            this.colVisitStatusID,
            this.colVisitTypeID,
            this.colClientPOID,
            this.colClientPONumber,
            this.colSellCalculationLevel,
            this.colCreatedFromVisitTemplateID,
            this.colDaysSeparation,
            this.colSiteContractDaysSeparationPercent,
            this.colVisitLabourCost,
            this.colVisitMaterialCost,
            this.colVisitEquipmentCost,
            this.colVisitLabourSell,
            this.colVisitMaterialSell,
            this.colVisitEquipmentSell,
            this.colDummyMaxDate});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowFilterEditor = false;
            this.gridView6.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView6.OptionsFilter.AllowMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView6.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView6.OptionsFind.FindDelay = 2000;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsLayout.StoreFormatRules = true;
            this.gridView6.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colVisitNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView6_CustomDrawCell);
            this.gridView6.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView6_CustomRowCellEdit);
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView6_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView6_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView6_CustomDrawEmptyForeground);
            this.gridView6.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView6_ShowingEditor);
            this.gridView6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseDown);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            this.gridView6.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView6_ValidatingEditor);
            // 
            // colstrMode
            // 
            this.colstrMode.FieldName = "strMode";
            this.colstrMode.Name = "colstrMode";
            this.colstrMode.OptionsColumn.AllowEdit = false;
            this.colstrMode.OptionsColumn.AllowFocus = false;
            this.colstrMode.OptionsColumn.ReadOnly = true;
            this.colstrMode.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colstrRecordIDs
            // 
            this.colstrRecordIDs.FieldName = "strRecordIDs";
            this.colstrRecordIDs.Name = "colstrRecordIDs";
            this.colstrRecordIDs.OptionsColumn.AllowEdit = false;
            this.colstrRecordIDs.OptionsColumn.AllowFocus = false;
            this.colstrRecordIDs.OptionsColumn.ReadOnly = true;
            this.colstrRecordIDs.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colVisitID
            // 
            this.colVisitID.Caption = "Visit ID";
            this.colVisitID.FieldName = "VisitID";
            this.colVisitID.Name = "colVisitID";
            this.colVisitID.OptionsColumn.AllowEdit = false;
            this.colVisitID.OptionsColumn.AllowFocus = false;
            this.colVisitID.OptionsColumn.ReadOnly = true;
            this.colVisitID.Width = 54;
            // 
            // colSiteContractID
            // 
            this.colSiteContractID.Caption = "Site Contract ID";
            this.colSiteContractID.FieldName = "SiteContractID";
            this.colSiteContractID.Name = "colSiteContractID";
            this.colSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSiteContractID.Width = 98;
            // 
            // colVisitNumber
            // 
            this.colVisitNumber.Caption = "Visit #";
            this.colVisitNumber.ColumnEdit = this.repositoryItemSpinEditVisitNumber;
            this.colVisitNumber.FieldName = "VisitNumber";
            this.colVisitNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colVisitNumber.Name = "colVisitNumber";
            this.colVisitNumber.Visible = true;
            this.colVisitNumber.VisibleIndex = 0;
            this.colVisitNumber.Width = 62;
            // 
            // repositoryItemSpinEditVisitNumber
            // 
            this.repositoryItemSpinEditVisitNumber.AutoHeight = false;
            this.repositoryItemSpinEditVisitNumber.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditVisitNumber.IsFloatValue = false;
            this.repositoryItemSpinEditVisitNumber.Mask.EditMask = "n0";
            this.repositoryItemSpinEditVisitNumber.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditVisitNumber.Name = "repositoryItemSpinEditVisitNumber";
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "CreatedBy Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 113;
            // 
            // colExpectedStartDate
            // 
            this.colExpectedStartDate.Caption = "Expected Start Date";
            this.colExpectedStartDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colExpectedStartDate.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate.Name = "colExpectedStartDate";
            this.colExpectedStartDate.Visible = true;
            this.colExpectedStartDate.VisibleIndex = 1;
            this.colExpectedStartDate.Width = 119;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Mask.EditMask = "g";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemDateEdit1_EditValueChanged);
            // 
            // colExpectedEndDate
            // 
            this.colExpectedEndDate.Caption = "Expected End Date";
            this.colExpectedEndDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colExpectedEndDate.FieldName = "ExpectedEndDate";
            this.colExpectedEndDate.Name = "colExpectedEndDate";
            this.colExpectedEndDate.Visible = true;
            this.colExpectedEndDate.VisibleIndex = 2;
            this.colExpectedEndDate.Width = 110;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Actual Start Date";
            this.colStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.Width = 104;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "Actual End Date";
            this.colEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.Width = 98;
            // 
            // colCostCalculationLevel
            // 
            this.colCostCalculationLevel.Caption = "Cost Calculation";
            this.colCostCalculationLevel.ColumnEdit = this.repositoryItemGridLookUpEditCostCalculationLevel;
            this.colCostCalculationLevel.FieldName = "CostCalculationLevel";
            this.colCostCalculationLevel.Name = "colCostCalculationLevel";
            this.colCostCalculationLevel.Visible = true;
            this.colCostCalculationLevel.VisibleIndex = 8;
            this.colCostCalculationLevel.Width = 100;
            // 
            // repositoryItemGridLookUpEditCostCalculationLevel
            // 
            this.repositoryItemGridLookUpEditCostCalculationLevel.AutoHeight = false;
            this.repositoryItemGridLookUpEditCostCalculationLevel.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditCostCalculationLevel.DataSource = this.sp06134OMJobCalculationLevelDescriptorsBindingSource;
            this.repositoryItemGridLookUpEditCostCalculationLevel.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditCostCalculationLevel.Name = "repositoryItemGridLookUpEditCostCalculationLevel";
            this.repositoryItemGridLookUpEditCostCalculationLevel.NullText = "";
            this.repositoryItemGridLookUpEditCostCalculationLevel.PopupView = this.repositoryItemGridLookUpEdit2View;
            this.repositoryItemGridLookUpEditCostCalculationLevel.ValueMember = "ID";
            // 
            // repositoryItemGridLookUpEdit2View
            // 
            this.repositoryItemGridLookUpEdit2View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.repositoryItemGridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Column = this.colID1;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue3.Value1 = -1;
            gridFormatRule3.Rule = formatConditionRuleValue3;
            this.repositoryItemGridLookUpEdit2View.FormatRules.Add(gridFormatRule3);
            this.repositoryItemGridLookUpEdit2View.Name = "repositoryItemGridLookUpEdit2View";
            this.repositoryItemGridLookUpEdit2View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit2View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit2View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit2View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Cost Calculation Level";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "RecordOrder";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // colVisitCost
            // 
            this.colVisitCost.AppearanceHeader.Options.UseTextOptions = true;
            this.colVisitCost.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVisitCost.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVisitCost.Caption = "Total<br>Cost";
            this.colVisitCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colVisitCost.FieldName = "VisitCost";
            this.colVisitCost.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colVisitCost.Name = "colVisitCost";
            this.colVisitCost.OptionsColumn.AllowEdit = false;
            this.colVisitCost.OptionsColumn.AllowFocus = false;
            this.colVisitCost.OptionsColumn.ReadOnly = true;
            this.colVisitCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitCost", "{0:c}")});
            this.colVisitCost.Visible = true;
            this.colVisitCost.VisibleIndex = 15;
            this.colVisitCost.Width = 70;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colVisitSell
            // 
            this.colVisitSell.AppearanceHeader.Options.UseTextOptions = true;
            this.colVisitSell.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVisitSell.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVisitSell.Caption = "Total<br>Sell";
            this.colVisitSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colVisitSell.FieldName = "VisitSell";
            this.colVisitSell.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colVisitSell.Name = "colVisitSell";
            this.colVisitSell.OptionsColumn.AllowEdit = false;
            this.colVisitSell.OptionsColumn.AllowFocus = false;
            this.colVisitSell.OptionsColumn.ReadOnly = true;
            this.colVisitSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitSell", "{0:c}")});
            this.colVisitSell.Visible = true;
            this.colVisitSell.VisibleIndex = 19;
            this.colVisitSell.Width = 70;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 11;
            this.colRemarks1.Width = 78;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colStartLatitude
            // 
            this.colStartLatitude.Caption = "Start Latitude";
            this.colStartLatitude.ColumnEdit = this.repositoryItemTextEditFloat;
            this.colStartLatitude.FieldName = "StartLatitude";
            this.colStartLatitude.Name = "colStartLatitude";
            this.colStartLatitude.OptionsColumn.AllowEdit = false;
            this.colStartLatitude.OptionsColumn.AllowFocus = false;
            this.colStartLatitude.OptionsColumn.ReadOnly = true;
            this.colStartLatitude.Width = 87;
            // 
            // repositoryItemTextEditFloat
            // 
            this.repositoryItemTextEditFloat.AutoHeight = false;
            this.repositoryItemTextEditFloat.Mask.EditMask = "n8";
            this.repositoryItemTextEditFloat.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditFloat.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditFloat.Name = "repositoryItemTextEditFloat";
            // 
            // colStartLongitude
            // 
            this.colStartLongitude.Caption = "Start Longitude";
            this.colStartLongitude.ColumnEdit = this.repositoryItemTextEditFloat;
            this.colStartLongitude.FieldName = "StartLongitude";
            this.colStartLongitude.Name = "colStartLongitude";
            this.colStartLongitude.OptionsColumn.AllowEdit = false;
            this.colStartLongitude.OptionsColumn.AllowFocus = false;
            this.colStartLongitude.OptionsColumn.ReadOnly = true;
            this.colStartLongitude.Width = 95;
            // 
            // colFinishLatitude
            // 
            this.colFinishLatitude.Caption = "Finish Latitude";
            this.colFinishLatitude.ColumnEdit = this.repositoryItemTextEditFloat;
            this.colFinishLatitude.FieldName = "FinishLatitude";
            this.colFinishLatitude.Name = "colFinishLatitude";
            this.colFinishLatitude.OptionsColumn.AllowEdit = false;
            this.colFinishLatitude.OptionsColumn.AllowFocus = false;
            this.colFinishLatitude.OptionsColumn.ReadOnly = true;
            this.colFinishLatitude.Width = 90;
            // 
            // colFinishLongitude
            // 
            this.colFinishLongitude.Caption = "Finish Longitude";
            this.colFinishLongitude.ColumnEdit = this.repositoryItemTextEditFloat;
            this.colFinishLongitude.FieldName = "FinishLongitude";
            this.colFinishLongitude.Name = "colFinishLongitude";
            this.colFinishLongitude.OptionsColumn.AllowEdit = false;
            this.colFinishLongitude.OptionsColumn.AllowFocus = false;
            this.colFinishLongitude.OptionsColumn.ReadOnly = true;
            this.colFinishLongitude.Width = 98;
            // 
            // colClientContractID
            // 
            this.colClientContractID.Caption = "Client Contract ID";
            this.colClientContractID.FieldName = "ClientContractID";
            this.colClientContractID.Name = "colClientContractID";
            this.colClientContractID.OptionsColumn.AllowEdit = false;
            this.colClientContractID.OptionsColumn.AllowFocus = false;
            this.colClientContractID.OptionsColumn.ReadOnly = true;
            this.colClientContractID.Width = 107;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Width = 262;
            // 
            // colContractDescription1
            // 
            this.colContractDescription1.Caption = "Contract Description";
            this.colContractDescription1.FieldName = "ContractDescription";
            this.colContractDescription1.Name = "colContractDescription1";
            this.colContractDescription1.OptionsColumn.AllowEdit = false;
            this.colContractDescription1.OptionsColumn.AllowFocus = false;
            this.colContractDescription1.OptionsColumn.ReadOnly = true;
            this.colContractDescription1.Width = 302;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Width = 226;
            // 
            // colCreatedByStaffName
            // 
            this.colCreatedByStaffName.Caption = "Created By Person";
            this.colCreatedByStaffName.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName.Name = "colCreatedByStaffName";
            this.colCreatedByStaffName.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName.Width = 111;
            // 
            // colCalendarLabel
            // 
            this.colCalendarLabel.Caption = "Calendar Label";
            this.colCalendarLabel.FieldName = "CalendarLabel";
            this.colCalendarLabel.Name = "colCalendarLabel";
            this.colCalendarLabel.OptionsColumn.AllowEdit = false;
            this.colCalendarLabel.OptionsColumn.AllowFocus = false;
            this.colCalendarLabel.OptionsColumn.ReadOnly = true;
            this.colCalendarLabel.Width = 92;
            // 
            // colLabelColourID
            // 
            this.colLabelColourID.Caption = "Calendar Label Colour ID";
            this.colLabelColourID.FieldName = "LabelColourID";
            this.colLabelColourID.Name = "colLabelColourID";
            this.colLabelColourID.OptionsColumn.AllowEdit = false;
            this.colLabelColourID.OptionsColumn.AllowFocus = false;
            this.colLabelColourID.OptionsColumn.ReadOnly = true;
            this.colLabelColourID.Width = 146;
            // 
            // colWorkNumber
            // 
            this.colWorkNumber.Caption = "Work Number";
            this.colWorkNumber.ColumnEdit = this.repositoryItemTextEdit1;
            this.colWorkNumber.FieldName = "WorkNumber";
            this.colWorkNumber.Name = "colWorkNumber";
            this.colWorkNumber.Visible = true;
            this.colWorkNumber.VisibleIndex = 6;
            this.colWorkNumber.Width = 91;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 50;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colVisitCategoryID
            // 
            this.colVisitCategoryID.Caption = "Visit Category";
            this.colVisitCategoryID.ColumnEdit = this.repositoryItemGridLookUpEditVisitCategory;
            this.colVisitCategoryID.FieldName = "VisitCategoryID";
            this.colVisitCategoryID.Name = "colVisitCategoryID";
            this.colVisitCategoryID.Visible = true;
            this.colVisitCategoryID.VisibleIndex = 4;
            this.colVisitCategoryID.Width = 125;
            // 
            // repositoryItemGridLookUpEditVisitCategory
            // 
            this.repositoryItemGridLookUpEditVisitCategory.AutoHeight = false;
            this.repositoryItemGridLookUpEditVisitCategory.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditVisitCategory.DataSource = this.sp06250OMVisitCategoriesWithBlankBindingSource;
            this.repositoryItemGridLookUpEditVisitCategory.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditVisitCategory.Name = "repositoryItemGridLookUpEditVisitCategory";
            this.repositoryItemGridLookUpEditVisitCategory.NullText = "";
            this.repositoryItemGridLookUpEditVisitCategory.PopupView = this.gridView7;
            this.repositoryItemGridLookUpEditVisitCategory.ValueMember = "ID";
            // 
            // sp06250OMVisitCategoriesWithBlankBindingSource
            // 
            this.sp06250OMVisitCategoriesWithBlankBindingSource.DataMember = "sp06250_OM_Visit_Categories_With_Blank";
            this.sp06250OMVisitCategoriesWithBlankBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15});
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn13;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView7.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn15, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Visit Category";
            this.gridColumn14.FieldName = "Description";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 0;
            this.gridColumn14.Width = 220;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Order";
            this.gridColumn15.FieldName = "RecordOrder";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            // 
            // colVisitStatusID
            // 
            this.colVisitStatusID.Caption = "Visit Status";
            this.colVisitStatusID.ColumnEdit = this.repositoryItemGridLookUpEditVisitStatusID;
            this.colVisitStatusID.FieldName = "VisitStatusID";
            this.colVisitStatusID.Name = "colVisitStatusID";
            this.colVisitStatusID.Visible = true;
            this.colVisitStatusID.VisibleIndex = 7;
            this.colVisitStatusID.Width = 108;
            // 
            // repositoryItemGridLookUpEditVisitStatusID
            // 
            this.repositoryItemGridLookUpEditVisitStatusID.AutoHeight = false;
            this.repositoryItemGridLookUpEditVisitStatusID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditVisitStatusID.DataSource = this.sp06538OMVisitStatusesNotReadyAndReadyNoBlankBindingSource;
            this.repositoryItemGridLookUpEditVisitStatusID.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditVisitStatusID.Name = "repositoryItemGridLookUpEditVisitStatusID";
            this.repositoryItemGridLookUpEditVisitStatusID.PopupView = this.gridView2;
            this.repositoryItemGridLookUpEditVisitStatusID.ValueMember = "ID";
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription1,
            this.colID2,
            this.colRecordOrder});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Visit Status";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 181;
            // 
            // colID2
            // 
            this.colID2.Caption = "Visit Status ID";
            this.colID2.FieldName = "ID";
            this.colID2.Name = "colID2";
            this.colID2.OptionsColumn.AllowEdit = false;
            this.colID2.OptionsColumn.AllowFocus = false;
            this.colID2.OptionsColumn.ReadOnly = true;
            this.colID2.Width = 87;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // colVisitTypeID
            // 
            this.colVisitTypeID.Caption = "Visit Type";
            this.colVisitTypeID.ColumnEdit = this.repositoryItemGridLookUpEditVisitTypes;
            this.colVisitTypeID.FieldName = "VisitTypeID";
            this.colVisitTypeID.Name = "colVisitTypeID";
            this.colVisitTypeID.Visible = true;
            this.colVisitTypeID.VisibleIndex = 5;
            this.colVisitTypeID.Width = 119;
            // 
            // repositoryItemGridLookUpEditVisitTypes
            // 
            this.repositoryItemGridLookUpEditVisitTypes.AutoHeight = false;
            this.repositoryItemGridLookUpEditVisitTypes.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditVisitTypes.DataSource = this.sp06308OMVisitTypesBindingSource;
            this.repositoryItemGridLookUpEditVisitTypes.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditVisitTypes.Name = "repositoryItemGridLookUpEditVisitTypes";
            this.repositoryItemGridLookUpEditVisitTypes.NullText = "";
            this.repositoryItemGridLookUpEditVisitTypes.PopupView = this.gridView5;
            this.repositoryItemGridLookUpEditVisitTypes.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2});
            this.repositoryItemGridLookUpEditVisitTypes.ValueMember = "ID";
            // 
            // sp06308OMVisitTypesBindingSource
            // 
            this.sp06308OMVisitTypesBindingSource.DataMember = "sp06308_OM_Visit_Types";
            this.sp06308OMVisitTypesBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActive,
            this.colDescription2,
            this.colID,
            this.colOrder1});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsLayout.StoreFormatRules = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 50;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Visit Type";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 202;
            // 
            // colID
            // 
            this.colID.Caption = "Visit Type ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.Width = 80;
            // 
            // colOrder1
            // 
            this.colOrder1.Caption = "Order";
            this.colOrder1.FieldName = "Order";
            this.colOrder1.Name = "colOrder1";
            this.colOrder1.Width = 61;
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "Client PO ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            // 
            // colClientPONumber
            // 
            this.colClientPONumber.Caption = "Client PO #";
            this.colClientPONumber.ColumnEdit = this.repositoryItemButtonEditClientPONumber;
            this.colClientPONumber.FieldName = "ClientPONumber";
            this.colClientPONumber.Name = "colClientPONumber";
            this.colClientPONumber.Visible = true;
            this.colClientPONumber.VisibleIndex = 10;
            this.colClientPONumber.Width = 178;
            // 
            // repositoryItemButtonEditClientPONumber
            // 
            this.repositoryItemButtonEditClientPONumber.AutoHeight = false;
            this.repositoryItemButtonEditClientPONumber.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to open the Select Client Purchase Order screen.", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close, "clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to Clear the selected Client Purchase Order.", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditClientPONumber.Name = "repositoryItemButtonEditClientPONumber";
            this.repositoryItemButtonEditClientPONumber.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditClientPONumber.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditClientPONumber_ButtonClick);
            // 
            // colSellCalculationLevel
            // 
            this.colSellCalculationLevel.Caption = "Sell Calculation";
            this.colSellCalculationLevel.ColumnEdit = this.repositoryItemGridLookUpEditCostCalculationLevel;
            this.colSellCalculationLevel.FieldName = "SellCalculationLevel";
            this.colSellCalculationLevel.Name = "colSellCalculationLevel";
            this.colSellCalculationLevel.Visible = true;
            this.colSellCalculationLevel.VisibleIndex = 9;
            this.colSellCalculationLevel.Width = 100;
            // 
            // colCreatedFromVisitTemplateID
            // 
            this.colCreatedFromVisitTemplateID.Caption = "Created From Visit Template ID";
            this.colCreatedFromVisitTemplateID.FieldName = "CreatedFromVisitTemplateID";
            this.colCreatedFromVisitTemplateID.Name = "colCreatedFromVisitTemplateID";
            this.colCreatedFromVisitTemplateID.OptionsColumn.AllowEdit = false;
            this.colCreatedFromVisitTemplateID.OptionsColumn.AllowFocus = false;
            this.colCreatedFromVisitTemplateID.OptionsColumn.ReadOnly = true;
            this.colCreatedFromVisitTemplateID.Width = 168;
            // 
            // colDaysSeparation
            // 
            this.colDaysSeparation.AppearanceHeader.Options.UseTextOptions = true;
            this.colDaysSeparation.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDaysSeparation.Caption = "Visit Days<br>Separation";
            this.colDaysSeparation.ColumnEdit = this.repositoryItemSpinEditDaysSeparation;
            this.colDaysSeparation.FieldName = "DaysSeparation";
            this.colDaysSeparation.Name = "colDaysSeparation";
            this.colDaysSeparation.Visible = true;
            this.colDaysSeparation.VisibleIndex = 3;
            this.colDaysSeparation.Width = 71;
            // 
            // repositoryItemSpinEditDaysSeparation
            // 
            this.repositoryItemSpinEditDaysSeparation.AutoHeight = false;
            editorButtonImageOptions3.Image = global::WoodPlan5.Properties.Resources.Calculate_16x16;
            this.repositoryItemSpinEditDaysSeparation.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Calculate", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Calculate Visit Days Separation", "calculate", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemSpinEditDaysSeparation.IsFloatValue = false;
            this.repositoryItemSpinEditDaysSeparation.Mask.EditMask = "n0";
            this.repositoryItemSpinEditDaysSeparation.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditDaysSeparation.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.repositoryItemSpinEditDaysSeparation.Name = "repositoryItemSpinEditDaysSeparation";
            this.repositoryItemSpinEditDaysSeparation.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemSpinEditDaysSeparation_ButtonClick);
            // 
            // colSiteContractDaysSeparationPercent
            // 
            this.colSiteContractDaysSeparationPercent.Caption = "Site Contract Days Separation %";
            this.colSiteContractDaysSeparationPercent.FieldName = "SiteContractDaysSeparationPercent";
            this.colSiteContractDaysSeparationPercent.Name = "colSiteContractDaysSeparationPercent";
            this.colSiteContractDaysSeparationPercent.OptionsColumn.AllowEdit = false;
            this.colSiteContractDaysSeparationPercent.OptionsColumn.AllowFocus = false;
            this.colSiteContractDaysSeparationPercent.OptionsColumn.ReadOnly = true;
            this.colSiteContractDaysSeparationPercent.Width = 178;
            // 
            // colVisitLabourCost
            // 
            this.colVisitLabourCost.AppearanceHeader.Options.UseTextOptions = true;
            this.colVisitLabourCost.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVisitLabourCost.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVisitLabourCost.Caption = "Labour<br>Cost";
            this.colVisitLabourCost.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colVisitLabourCost.FieldName = "VisitLabourCost";
            this.colVisitLabourCost.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colVisitLabourCost.Name = "colVisitLabourCost";
            this.colVisitLabourCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitLabourCost", "{0:c}")});
            this.colVisitLabourCost.Visible = true;
            this.colVisitLabourCost.VisibleIndex = 12;
            this.colVisitLabourCost.Width = 70;
            // 
            // repositoryItemSpinEditCurrency
            // 
            this.repositoryItemSpinEditCurrency.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemSpinEditCurrency.AutoHeight = false;
            this.repositoryItemSpinEditCurrency.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditCurrency.Mask.EditMask = "c";
            this.repositoryItemSpinEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditCurrency.Name = "repositoryItemSpinEditCurrency";
            // 
            // colVisitMaterialCost
            // 
            this.colVisitMaterialCost.AppearanceHeader.Options.UseTextOptions = true;
            this.colVisitMaterialCost.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVisitMaterialCost.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVisitMaterialCost.Caption = "Material<br>Cost";
            this.colVisitMaterialCost.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colVisitMaterialCost.FieldName = "VisitMaterialCost";
            this.colVisitMaterialCost.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colVisitMaterialCost.Name = "colVisitMaterialCost";
            this.colVisitMaterialCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitMaterialCost", "{0:c}")});
            this.colVisitMaterialCost.Visible = true;
            this.colVisitMaterialCost.VisibleIndex = 13;
            this.colVisitMaterialCost.Width = 70;
            // 
            // colVisitEquipmentCost
            // 
            this.colVisitEquipmentCost.AppearanceHeader.Options.UseTextOptions = true;
            this.colVisitEquipmentCost.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVisitEquipmentCost.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVisitEquipmentCost.Caption = "Equipment<br>Cost";
            this.colVisitEquipmentCost.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colVisitEquipmentCost.FieldName = "VisitEquipmentCost";
            this.colVisitEquipmentCost.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colVisitEquipmentCost.Name = "colVisitEquipmentCost";
            this.colVisitEquipmentCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitEquipmentCost", "{0:c}")});
            this.colVisitEquipmentCost.Visible = true;
            this.colVisitEquipmentCost.VisibleIndex = 14;
            this.colVisitEquipmentCost.Width = 70;
            // 
            // colVisitLabourSell
            // 
            this.colVisitLabourSell.AppearanceHeader.Options.UseTextOptions = true;
            this.colVisitLabourSell.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVisitLabourSell.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVisitLabourSell.Caption = "Labour<br>Sell";
            this.colVisitLabourSell.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colVisitLabourSell.FieldName = "VisitLabourSell";
            this.colVisitLabourSell.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colVisitLabourSell.Name = "colVisitLabourSell";
            this.colVisitLabourSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitLabourSell", "{0:c}")});
            this.colVisitLabourSell.Visible = true;
            this.colVisitLabourSell.VisibleIndex = 16;
            this.colVisitLabourSell.Width = 70;
            // 
            // colVisitMaterialSell
            // 
            this.colVisitMaterialSell.AppearanceHeader.Options.UseTextOptions = true;
            this.colVisitMaterialSell.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVisitMaterialSell.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVisitMaterialSell.Caption = "Material<br>Sell";
            this.colVisitMaterialSell.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colVisitMaterialSell.FieldName = "VisitMaterialSell";
            this.colVisitMaterialSell.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colVisitMaterialSell.Name = "colVisitMaterialSell";
            this.colVisitMaterialSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitMaterialSell", "{0:c}")});
            this.colVisitMaterialSell.Visible = true;
            this.colVisitMaterialSell.VisibleIndex = 17;
            this.colVisitMaterialSell.Width = 70;
            // 
            // colVisitEquipmentSell
            // 
            this.colVisitEquipmentSell.AppearanceHeader.Options.UseTextOptions = true;
            this.colVisitEquipmentSell.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVisitEquipmentSell.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVisitEquipmentSell.Caption = "Equipment<br>Sell";
            this.colVisitEquipmentSell.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colVisitEquipmentSell.FieldName = "VisitEquipmentSell";
            this.colVisitEquipmentSell.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colVisitEquipmentSell.Name = "colVisitEquipmentSell";
            this.colVisitEquipmentSell.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VisitEquipmentSell", "{0:c}")});
            this.colVisitEquipmentSell.Visible = true;
            this.colVisitEquipmentSell.VisibleIndex = 18;
            this.colVisitEquipmentSell.Width = 70;
            // 
            // colDummyMaxDate
            // 
            this.colDummyMaxDate.Caption = "Complete Visits By Date";
            this.colDummyMaxDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDummyMaxDate.FieldName = "DummyMaxDate";
            this.colDummyMaxDate.Name = "colDummyMaxDate";
            this.colDummyMaxDate.OptionsColumn.AllowEdit = false;
            this.colDummyMaxDate.OptionsColumn.AllowFocus = false;
            this.colDummyMaxDate.OptionsColumn.ReadOnly = true;
            this.colDummyMaxDate.Width = 113;
            // 
            // btnStep5Next
            // 
            this.btnStep5Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep5Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep5Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep5Next.Location = new System.Drawing.Point(1365, 589);
            this.btnStep5Next.Name = "btnStep5Next";
            this.btnStep5Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep5Next.TabIndex = 19;
            this.btnStep5Next.Text = "Next";
            this.btnStep5Next.Click += new System.EventHandler(this.btnStep5Next_Click);
            // 
            // btnStep5Previous
            // 
            this.btnStep5Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep5Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep5Previous.Location = new System.Drawing.Point(1271, 589);
            this.btnStep5Previous.Name = "btnStep5Previous";
            this.btnStep5Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep5Previous.TabIndex = 20;
            this.btnStep5Previous.Text = "Previous";
            this.btnStep5Previous.Click += new System.EventHandler(this.btnStep5Previous_Click);
            // 
            // xtraTabPageFinish
            // 
            this.xtraTabPageFinish.Controls.Add(this.panelControl2);
            this.xtraTabPageFinish.Controls.Add(this.groupControl1);
            this.xtraTabPageFinish.Controls.Add(this.labelControl10);
            this.xtraTabPageFinish.Controls.Add(this.btnFinish);
            this.xtraTabPageFinish.Controls.Add(this.btnFinishPrevious);
            this.xtraTabPageFinish.Controls.Add(this.pictureEdit2);
            this.xtraTabPageFinish.Name = "xtraTabPageFinish";
            this.xtraTabPageFinish.Size = new System.Drawing.Size(1459, 625);
            this.xtraTabPageFinish.Tag = "99";
            this.xtraTabPageFinish.Text = "Finished";
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.TenderDetailsLabelControl3);
            this.panelControl2.Controls.Add(this.pictureEdit6);
            this.panelControl2.Controls.Add(this.labelControl11);
            this.panelControl2.Controls.Add(this.labelControl12);
            this.panelControl2.Location = new System.Drawing.Point(211, 6);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1242, 75);
            this.panelControl2.TabIndex = 18;
            // 
            // TenderDetailsLabelControl3
            // 
            this.TenderDetailsLabelControl3.AllowHtmlString = true;
            this.TenderDetailsLabelControl3.Location = new System.Drawing.Point(9, 57);
            this.TenderDetailsLabelControl3.Name = "TenderDetailsLabelControl3";
            this.TenderDetailsLabelControl3.Size = new System.Drawing.Size(82, 13);
            this.TenderDetailsLabelControl3.TabIndex = 19;
            this.TenderDetailsLabelControl3.Text = "Selected Tender:";
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit6.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit6.Location = new System.Drawing.Point(1198, 4);
            this.pictureEdit6.MenuManager = this.barManager1;
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit6.Properties.ReadOnly = true;
            this.pictureEdit6.Properties.ShowMenu = false;
            this.pictureEdit6.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit6.TabIndex = 19;
            // 
            // labelControl11
            // 
            this.labelControl11.AllowHtmlString = true;
            this.labelControl11.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl11.Appearance.Options.UseBackColor = true;
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(5, 1);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(240, 29);
            this.labelControl11.TabIndex = 5;
            this.labelControl11.Text = "Completing the Wizard";
            // 
            // labelControl12
            // 
            this.labelControl12.AllowHtmlString = true;
            this.labelControl12.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl12.Appearance.Options.UseBackColor = true;
            this.labelControl12.Location = new System.Drawing.Point(9, 35);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(506, 13);
            this.labelControl12.TabIndex = 15;
            this.labelControl12.Text = "You have successfully completed the Wizard. On clicking <b>Finish</b>, one or mor" +
    "e visit records will be created.";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.checkEdit2);
            this.groupControl1.Controls.Add(this.checkEdit1);
            this.groupControl1.Location = new System.Drawing.Point(211, 155);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1242, 81);
            this.groupControl1.TabIndex = 16;
            this.groupControl1.Text = "Available Choices:";
            // 
            // checkEdit2
            // 
            this.checkEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit2.EditValue = true;
            this.checkEdit2.Location = new System.Drawing.Point(6, 51);
            this.checkEdit2.MenuManager = this.barManager1;
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit2.Properties.Caption = "Create the Visit(s) then <b>open</b> the <b>Job Wizard</b> to create Jobs for the" +
    " new visits.";
            this.checkEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit2.Properties.RadioGroupIndex = 1;
            this.checkEdit2.Size = new System.Drawing.Size(1230, 19);
            this.checkEdit2.TabIndex = 4;
            // 
            // checkEdit1
            // 
            this.checkEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit1.Location = new System.Drawing.Point(6, 26);
            this.checkEdit1.MenuManager = this.barManager1;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit1.Properties.Caption = "Create the Visit(s) then <b>return</b> to the <b>previous screen</b>.";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit1.Properties.RadioGroupIndex = 1;
            this.checkEdit1.Size = new System.Drawing.Size(1230, 19);
            this.checkEdit1.TabIndex = 0;
            this.checkEdit1.TabStop = false;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(213, 132);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(154, 13);
            this.labelControl10.TabIndex = 15;
            this.labelControl10.Text = "What would you like to do next?";
            // 
            // btnFinish
            // 
            this.btnFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinish.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnFinish.Appearance.Options.UseFont = true;
            this.btnFinish.ImageOptions.Image = global::WoodPlan5.Properties.Resources.apply_32x32;
            this.btnFinish.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnFinish.Location = new System.Drawing.Point(1365, 589);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(88, 30);
            this.btnFinish.TabIndex = 12;
            this.btnFinish.Text = "Finish";
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnFinishPrevious
            // 
            this.btnFinishPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinishPrevious.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnFinishPrevious.Location = new System.Drawing.Point(1271, 589);
            this.btnFinishPrevious.Name = "btnFinishPrevious";
            this.btnFinishPrevious.Size = new System.Drawing.Size(88, 30);
            this.btnFinishPrevious.TabIndex = 11;
            this.btnFinishPrevious.Text = "Previous";
            this.btnFinishPrevious.Click += new System.EventHandler(this.btnFinishPrevious_Click);
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureEdit2.EditValue = global::WoodPlan5.Properties.Resources.wizard_finish1;
            this.pictureEdit2.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit2.MenuManager = this.barManager1;
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.ShowMenu = false;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit2.Size = new System.Drawing.Size(205, 625);
            this.pictureEdit2.TabIndex = 5;
            // 
            // sp06141OMVisitTemplateHeadersWithBlankBindingSource
            // 
            this.sp06141OMVisitTemplateHeadersWithBlankBindingSource.DataMember = "sp06141_OM_Visit_Template_Headers_With_Blank";
            this.sp06141OMVisitTemplateHeadersWithBlankBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.LookAndFeel.SkinName = "Blue";
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.DisplayMember = "Description";
            this.repositoryItemGridLookUpEdit1.LookAndFeel.SkinName = "Blue";
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.NullText = "";
            this.repositoryItemGridLookUpEdit1.ValueMember = "ID";
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // repositoryItemButtonEditChooseStaff
            // 
            this.repositoryItemButtonEditChooseStaff.AutoHeight = false;
            this.repositoryItemButtonEditChooseStaff.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click me to open the Select Staff screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditChooseStaff.Name = "repositoryItemButtonEditChooseStaff";
            this.repositoryItemButtonEditChooseStaff.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "";
            this.repositoryItemCheckEdit4.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // sp06133_OM_Visit_Gap_Unit_DescriptorsTableAdapter
            // 
            this.sp06133_OM_Visit_Gap_Unit_DescriptorsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter
            // 
            this.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06145_OM_Visit_EditTableAdapter
            // 
            this.sp06145_OM_Visit_EditTableAdapter.ClearBeforeFill = true;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemDuration1
            // 
            this.repositoryItemDuration1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDuration1.AutoHeight = false;
            this.repositoryItemDuration1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDuration1.DisabledStateText = null;
            this.repositoryItemDuration1.Name = "repositoryItemDuration1";
            this.repositoryItemDuration1.NullValuePromptShowForEmptyValue = true;
            this.repositoryItemDuration1.ShowEmptyItem = true;
            this.repositoryItemDuration1.ValidateOnEnterKey = true;
            // 
            // sp06250_OM_Visit_Categories_With_BlankTableAdapter
            // 
            this.sp06250_OM_Visit_Categories_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06308_OM_Visit_TypesTableAdapter
            // 
            this.sp06308_OM_Visit_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // transitionManager1
            // 
            this.transitionManager1.FrameInterval = 5000;
            transition1.Control = this.xtraTabControl1;
            transition1.EasingMode = DevExpress.Data.Utils.EasingMode.EaseInOut;
            transition1.WaitingIndicatorProperties.Caption = "Loading...";
            this.transitionManager1.Transitions.Add(transition1);
            // 
            // sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter
            // 
            this.sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06538_OM_Visit_Statuses_Not_Ready_And_Ready_No_BlankTableAdapter
            // 
            this.sp06538_OM_Visit_Statuses_Not_Ready_And_Ready_No_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // timerWelcomePage
            // 
            this.timerWelcomePage.Interval = 2000;
            this.timerWelcomePage.Tick += new System.EventHandler(this.timerWelcomePage_Tick);
            // 
            // frm_OM_Tender_Visit_Wizard
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1482, 625);
            this.Controls.Add(this.xtraTabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Tender_Visit_Wizard";
            this.Text = "Tender Visit Wizard";
            this.Activated += new System.EventHandler(this.frm_OM_Tender_Visit_Wizard_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Tender_Visit_Wizard_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Tender_Visit_Wizard_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageWelcome.ResumeLayout(false);
            this.xtraTabPageWelcome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.xtraTabPageStep1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlBlockEditVisits)).EndInit();
            this.groupControlBlockEditVisits.ResumeLayout(false);
            this.groupControlBlockEditVisits.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditVisitStatusBlockEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06538OMVisitStatusesNotReadyAndReadyNoBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Tender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitDaysSeparationSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitDescriptorComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitDurationSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitTimeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCalculatedTotalSell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCalculatedEquipmentSell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCalculatedMaterialSell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCalculatedLabourSell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderTotalSell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderEquipmentSell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderMaterialSell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderLabourSell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCalculatedTotalCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCalculatedEquipmentCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCalculatedMaterialCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCalculatedLabourCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderTotalCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderEquipmentCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderMaterialCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderLabourCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlAddVisits)).EndInit();
            this.groupControlAddVisits.ResumeLayout(false);
            this.groupControlAddVisits.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GapfirstVisitCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditVisitStatusID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditNumberOfVisits.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSellCalculationLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06134OMJobCalculationLevelDescriptorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditGapBetweenVisitsUnits.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditGapBetweenVisitsDescriptor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06133OMVisitGapUnitDescriptorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditCostCalculationLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06145OMVisitEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditVisitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditCostCalculationLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditFloat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditVisitCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06250OMVisitCategoriesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditVisitStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditVisitTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06308OMVisitTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditClientPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditDaysSeparation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCurrency)).EndInit();
            this.xtraTabPageFinish.ResumeLayout(false);
            this.xtraTabPageFinish.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06141OMVisitTemplateHeadersWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseStaff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageWelcome;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btnWelcomeNext;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageFinish;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.SimpleButton btnFinish;
        private DevExpress.XtraEditors.SimpleButton btnFinishPrevious;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep1;
        private DevExpress.XtraEditors.SimpleButton btnStep5Next;
        private DevExpress.XtraEditors.SimpleButton btnStep5Previous;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditChooseStaff;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private System.Windows.Forms.BindingSource sp06134OMJobCalculationLevelDescriptorsBindingSource;
        private System.Windows.Forms.BindingSource sp06133OMVisitGapUnitDescriptorsBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06133_OM_Visit_Gap_Unit_DescriptorsTableAdapter sp06133_OM_Visit_Gap_Unit_DescriptorsTableAdapter;
        private DataSet_OM_VisitTableAdapters.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter;
        private System.Windows.Forms.BindingSource sp06145OMVisitEditBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colstrMode;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRecordIDs;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCalculationLevel;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditCostCalculationLevel;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit2View;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCost;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitSell;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLatitude;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditFloat;
        private DevExpress.XtraGrid.Columns.GridColumn colStartLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colFinishLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName;
        private DataSet_OM_VisitTableAdapters.sp06145_OM_Visit_EditTableAdapter sp06145_OM_Visit_EditTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraScheduler.UI.RepositoryItemDuration repositoryItemDuration1;
        private DevExpress.XtraGrid.Columns.GridColumn colCalendarLabel;
        private DevExpress.XtraGrid.Columns.GridColumn colLabelColourID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkNumber;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCategoryID;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditVisitCategory;
        private System.Windows.Forms.BindingSource sp06250OMVisitCategoriesWithBlankBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitStatusID;
        private DataSet_OM_VisitTableAdapters.sp06250_OM_Visit_Categories_With_BlankTableAdapter sp06250_OM_Visit_Categories_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitTypeID;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditVisitTypes;
        private System.Windows.Forms.BindingSource sp06308OMVisitTypesBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder1;
        private DataSet_OM_VisitTableAdapters.sp06308_OM_Visit_TypesTableAdapter sp06308_OM_Visit_TypesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPONumber;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditClientPONumber;
        private DevExpress.Utils.Animation.TransitionManager transitionManager1;
        private DevExpress.XtraGrid.Columns.GridColumn colSellCalculationLevel;
        private System.Windows.Forms.BindingSource sp06141OMVisitTemplateHeadersWithBlankBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromVisitTemplateID;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysSeparation;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditDaysSeparation;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractDaysSeparationPercent;
        private DevExpress.XtraEditors.SpinEdit VisitDaysSeparationSpinEdit;
        private DevExpress.XtraEditors.SimpleButton btnApplyVisitDaysSeparation;
        private DevExpress.XtraEditors.SimpleButton btnCalculateVisitDaysSeparation;
        private DevExpress.XtraEditors.SpinEdit VisitDurationSpinEdit;
        private DevExpress.XtraEditors.SimpleButton btnApplyVisitDuration;
        private DevExpress.XtraEditors.ComboBoxEdit VisitDescriptorComboBoxEdit;
        private DevExpress.XtraEditors.TimeSpanEdit VisitTimeEdit;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditSellCalculationLevel;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditCostCalculationLevel;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn129;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn130;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn131;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditGapBetweenVisitsDescriptor;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn126;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn127;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn128;
        private DevExpress.XtraEditors.SpinEdit spinEditGapBetweenVisitsUnits;
        private DevExpress.XtraEditors.SpinEdit spinEditNumberOfVisits;
        private DevExpress.XtraEditors.SimpleButton btnAddVisits;
        private DevExpress.XtraEditors.LabelControl TenderDetailsLabelControl;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl TenderDetailsLabelControl2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.GroupControl groupControlAddVisits;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.GroupControl groupControlBlockEditVisits;
        private DevExpress.XtraEditors.DateEdit dateEditStartDate;
        private DevExpress.XtraEditors.LabelControl StartDateLabel;
        private DevExpress.XtraEditors.TextEdit textEditCalculatedTotalCost;
        private DevExpress.XtraEditors.TextEdit textEditCalculatedEquipmentCost;
        private DevExpress.XtraEditors.TextEdit textEditCalculatedMaterialCost;
        private DevExpress.XtraEditors.TextEdit textEditCalculatedLabourCost;
        private DevExpress.XtraEditors.TextEdit textEditTenderTotalCost;
        private DevExpress.XtraEditors.TextEdit textEditTenderEquipmentCost;
        private DevExpress.XtraEditors.TextEdit textEditTenderMaterialCost;
        private DevExpress.XtraEditors.TextEdit textEditTenderLabourCost;
        private DevExpress.XtraEditors.SimpleButton btnSplitCosts;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitMaterialCost;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitMaterialSell;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitEquipmentSell;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private DevExpress.XtraEditors.TextEdit textEditCalculatedTotalSell;
        private DevExpress.XtraEditors.TextEdit textEditCalculatedEquipmentSell;
        private DevExpress.XtraEditors.TextEdit textEditCalculatedMaterialSell;
        private DevExpress.XtraEditors.TextEdit textEditCalculatedLabourSell;
        private DevExpress.XtraEditors.TextEdit textEditTenderTotalSell;
        private DevExpress.XtraEditors.TextEdit textEditTenderEquipmentSell;
        private DevExpress.XtraEditors.TextEdit textEditTenderMaterialSell;
        private DevExpress.XtraEditors.TextEdit textEditTenderLabourSell;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate;
        private DevExpress.XtraEditors.LabelControl TenderDetailsLabelControl3;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyMaxDate;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditVisitStatusID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DataSet_OM_Tender dataSet_OM_Tender;
        private System.Windows.Forms.BindingSource sp06538OMVisitStatusesNotReadyAndReadyNoBlankBindingSource;
        private DataSet_OM_TenderTableAdapters.sp06538_OM_Visit_Statuses_Not_Ready_And_Ready_No_BlankTableAdapter sp06538_OM_Visit_Statuses_Not_Ready_And_Ready_No_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditVisitStatusID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colID2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditVisitStatusBlockEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.SimpleButton btnApplyVisitStatus;
        private DevExpress.XtraEditors.SeparatorControl separatorControl1;
        private DevExpress.XtraEditors.SeparatorControl separatorControl2;
        private System.Windows.Forms.Timer timerWelcomePage;
        private DevExpress.XtraEditors.CheckEdit GapfirstVisitCheckEdit;
    }
}

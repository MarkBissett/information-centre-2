using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    public partial class frm_OM_Select_Linked_Document_Type : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public int intPassedInTypeID = 0;
        public int intPassedInSubTypeID = 0;
        public int intSelectedTypeID = 0;
        public int intSelectedSubTypeID = 0;
        public string strSelectedTypeName = "";
        public string strSelectedSubTypeName = "";

        GridHitInfo downHitInfo = null;

        #region Sub Type Permission variables

        public bool iBool_Job = false;
        public bool iBool_JobLabour = false;
        public bool iBool_JobEquipment = false;
        public bool iBool_JobMaterial = false;
        public bool iBool_JobPicture = false;
        public bool iBool_JobHSE = false;
        public bool iBool_JobCRM = false;
        public bool iBool_JobComment = false;
        public bool iBool_VisitWaste = false;
        public bool iBool_VisitSpraying = false;
        public bool iBool_JobExtraInfo = false;
        public bool iBool_JobPO = false;
        public bool iBool_ClientContract = false;
        public bool iBool_ClientContractYear = false;
        public bool iBool_SiteContract = false;
        public bool iBool_Visit = false;
        public bool iBool_Accident = false;
        public bool iBool_AccidentInjury = false;
        public bool iBool_Tender = false;

        #endregion

        #endregion

        public frm_OM_Select_Linked_Document_Type()
        {
            InitializeComponent();
        }

        private void frm_OM_Select_Linked_Document_Type_Load(object sender, EventArgs e)
        {
            this.FormID = 500125;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            
            Set_Grid_Highlighter_Transparent(this.Controls);

            sp06047_OM_Linked_Document_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06048_OM_Linked_Document_Sub_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            
            try
            {
                sp06047_OM_Linked_Document_TypesTableAdapter.Fill(dataSet_GC_Summer_Core.sp06047_OM_Linked_Document_Types, (iBool_Job ? 1 : 0),
                                                                                                                            (iBool_JobLabour ? 1 : 0),
                                                                                                                            (iBool_JobEquipment ? 1 : 0),
                                                                                                                            (iBool_JobMaterial ? 1 : 0),
                                                                                                                            (iBool_JobPicture ? 1 : 0),
                                                                                                                            (iBool_JobHSE ? 1 : 0),
                                                                                                                            (iBool_JobCRM ? 1 : 0),
                                                                                                                            (iBool_JobComment ? 1 : 0),
                                                                                                                            (iBool_VisitWaste ? 1 : 0),
                                                                                                                            (iBool_VisitSpraying ? 1 : 0),
                                                                                                                            (iBool_JobExtraInfo ? 1 : 0),
                                                                                                                            (iBool_JobPO ? 1 : 0),
                                                                                                                            (iBool_ClientContract ? 1 : 0),
                                                                                                                            (iBool_ClientContractYear ? 1 : 0),
                                                                                                                            (iBool_SiteContract ? 1 : 0),
                                                                                                                            (iBool_Visit ? 1 : 0),
                                                                                                                            (iBool_Accident ? 1 : 0),
                                                                                                                            (iBool_AccidentInjury ? 1 : 0),
                                                                                                                            (iBool_Tender ? 1 : 0));
            }
            catch (Exception)
            {
                XtraMessageBox.Show("An error occurred while loading the linked document type list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            gridControl1.ForceInitialize();
            GridView view = (GridView)gridControl1.MainView;
            if (intPassedInTypeID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["ID"], intPassedInTypeID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            gridControl2.ForceInitialize();
            view = (GridView)gridControl2.MainView;
            if (intPassedInSubTypeID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["ID"], intPassedInSubTypeID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

        }

        bool internalRowFocusing;


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Linked Document Types Available";
                    break;
                case "gridView2":
                    message = "No Linked Document Sub-Types Available For Selection - Select a Linked Document Type to see Related Sub-Types";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedData1();
                    GridView viewChild = (GridView)gridControl2.MainView;
                    viewChild.ExpandAllGroups();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            var intSelectedID = 0;

            if (intCount == 1)
            {
                //foreach (int intRowHandle in intRowHandles)
                //{
                intSelectedID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], view.Columns["ID"]));
                //}
            }

            //Populate Linked sub types //
            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_GC_Summer_Core.sp06048_OM_Linked_Document_Sub_Types.Clear();
            }
            else
            {
                try
                {
                    sp06048_OM_Linked_Document_Sub_TypesTableAdapter.Fill(dataSet_GC_Summer_Core.sp06048_OM_Linked_Document_Sub_Types, intSelectedID);
                }
                catch (Exception Ex)
                {
                    XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related linked document sub-types.\n\nTry selecting a type again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (intSelectedTypeID == 0)
            {
                XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            intSelectedTypeID = 0;
            strSelectedTypeName = "";

            intSelectedSubTypeID = 0;
            strSelectedSubTypeName = "";

            var view1 = (GridView)gridControl1.MainView;
            if (view1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intSelectedTypeID = Convert.ToInt32(view1.GetRowCellValue(view1.FocusedRowHandle, "ID"));
                strSelectedTypeName = (String.IsNullOrEmpty(Convert.ToString(view1.GetRowCellValue(view1.FocusedRowHandle, "Description"))) ? "Unknown Linked Document Type" : Convert.ToString(view1.GetRowCellValue(view1.FocusedRowHandle, "Description")));
            }

            var view2 = (GridView)gridControl2.MainView;
            if (view2.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intSelectedSubTypeID = Convert.ToInt32(view2.GetRowCellValue(view2.FocusedRowHandle, "ID"));
                strSelectedSubTypeName = (String.IsNullOrEmpty(Convert.ToString(view2.GetRowCellValue(view2.FocusedRowHandle, "Description"))) ? "Unknown Linked Document Sub-Type" : Convert.ToString(view2.GetRowCellValue(view2.FocusedRowHandle, "Description")));
            }
        }



    }
}


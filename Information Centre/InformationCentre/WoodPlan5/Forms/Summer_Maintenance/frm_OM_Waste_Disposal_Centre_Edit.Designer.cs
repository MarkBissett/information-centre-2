namespace WoodPlan5
{
    partial class frm_OM_Waste_Disposal_Centre_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Waste_Disposal_Centre_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.LongitudeSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.sp06221OMWasteDisposalCentreEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.LatitudeSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.PostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine5TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine4TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine3TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WasteDisposalCentreIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.WasteDisposalCentreNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksTextEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ItemForWasteDisposalCentreID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForComments = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWasteDisposalCentreName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForAddressLine1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddressLine2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddressLine3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddressLine4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddressLine5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLatitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLongitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp06221_OM_Waste_Disposal_Centre_EditTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06221_OM_Waste_Disposal_Centre_EditTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LongitudeSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06221OMWasteDisposalCentreEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LatitudeSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine5TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine4TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine3TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WasteDisposalCentreIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WasteDisposalCentreNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWasteDisposalCentreID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWasteDisposalCentreName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLongitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 478);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 452);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 452);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 478);
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 452);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 452);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.LongitudeSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.LatitudeSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.PostcodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine5TextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine4TextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine3TextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.WasteDisposalCentreIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.WasteDisposalCentreNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksTextEdit);
            this.dataLayoutControl1.DataSource = this.sp06221OMWasteDisposalCentreEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForWasteDisposalCentreID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1140, 117, 365, 551);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 452);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // LongitudeSpinEdit
            // 
            this.LongitudeSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06221OMWasteDisposalCentreEditBindingSource, "Longitude", true));
            this.LongitudeSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LongitudeSpinEdit.Location = new System.Drawing.Point(158, 227);
            this.LongitudeSpinEdit.MenuManager = this.barManager1;
            this.LongitudeSpinEdit.Name = "LongitudeSpinEdit";
            this.LongitudeSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LongitudeSpinEdit.Properties.Mask.EditMask = "n8";
            this.LongitudeSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LongitudeSpinEdit.Size = new System.Drawing.Size(154, 20);
            this.LongitudeSpinEdit.StyleController = this.dataLayoutControl1;
            this.LongitudeSpinEdit.TabIndex = 55;
            // 
            // sp06221OMWasteDisposalCentreEditBindingSource
            // 
            this.sp06221OMWasteDisposalCentreEditBindingSource.DataMember = "sp06221_OM_Waste_Disposal_Centre_Edit";
            this.sp06221OMWasteDisposalCentreEditBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // LatitudeSpinEdit
            // 
            this.LatitudeSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06221OMWasteDisposalCentreEditBindingSource, "Latitude", true));
            this.LatitudeSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LatitudeSpinEdit.Location = new System.Drawing.Point(158, 203);
            this.LatitudeSpinEdit.MenuManager = this.barManager1;
            this.LatitudeSpinEdit.Name = "LatitudeSpinEdit";
            this.LatitudeSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LatitudeSpinEdit.Properties.Mask.EditMask = "n8";
            this.LatitudeSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LatitudeSpinEdit.Size = new System.Drawing.Size(154, 20);
            this.LatitudeSpinEdit.StyleController = this.dataLayoutControl1;
            this.LatitudeSpinEdit.TabIndex = 54;
            // 
            // PostcodeTextEdit
            // 
            this.PostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06221OMWasteDisposalCentreEditBindingSource, "Postcode", true));
            this.PostcodeTextEdit.Location = new System.Drawing.Point(158, 179);
            this.PostcodeTextEdit.MenuManager = this.barManager1;
            this.PostcodeTextEdit.Name = "PostcodeTextEdit";
            this.PostcodeTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PostcodeTextEdit, true);
            this.PostcodeTextEdit.Size = new System.Drawing.Size(458, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PostcodeTextEdit, optionsSpelling1);
            this.PostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.PostcodeTextEdit.TabIndex = 53;
            // 
            // AddressLine5TextEdit
            // 
            this.AddressLine5TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06221OMWasteDisposalCentreEditBindingSource, "AddressLine5", true));
            this.AddressLine5TextEdit.Location = new System.Drawing.Point(158, 155);
            this.AddressLine5TextEdit.MenuManager = this.barManager1;
            this.AddressLine5TextEdit.Name = "AddressLine5TextEdit";
            this.AddressLine5TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine5TextEdit, true);
            this.AddressLine5TextEdit.Size = new System.Drawing.Size(458, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine5TextEdit, optionsSpelling2);
            this.AddressLine5TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine5TextEdit.TabIndex = 52;
            // 
            // AddressLine4TextEdit
            // 
            this.AddressLine4TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06221OMWasteDisposalCentreEditBindingSource, "AddressLine4", true));
            this.AddressLine4TextEdit.Location = new System.Drawing.Point(158, 131);
            this.AddressLine4TextEdit.MenuManager = this.barManager1;
            this.AddressLine4TextEdit.Name = "AddressLine4TextEdit";
            this.AddressLine4TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine4TextEdit, true);
            this.AddressLine4TextEdit.Size = new System.Drawing.Size(458, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine4TextEdit, optionsSpelling3);
            this.AddressLine4TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine4TextEdit.TabIndex = 51;
            // 
            // AddressLine3TextEdit
            // 
            this.AddressLine3TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06221OMWasteDisposalCentreEditBindingSource, "AddressLine3", true));
            this.AddressLine3TextEdit.Location = new System.Drawing.Point(158, 107);
            this.AddressLine3TextEdit.MenuManager = this.barManager1;
            this.AddressLine3TextEdit.Name = "AddressLine3TextEdit";
            this.AddressLine3TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine3TextEdit, true);
            this.AddressLine3TextEdit.Size = new System.Drawing.Size(458, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine3TextEdit, optionsSpelling4);
            this.AddressLine3TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine3TextEdit.TabIndex = 50;
            // 
            // AddressLine2TextEdit
            // 
            this.AddressLine2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06221OMWasteDisposalCentreEditBindingSource, "AddressLine2", true));
            this.AddressLine2TextEdit.Location = new System.Drawing.Point(158, 83);
            this.AddressLine2TextEdit.MenuManager = this.barManager1;
            this.AddressLine2TextEdit.Name = "AddressLine2TextEdit";
            this.AddressLine2TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine2TextEdit, true);
            this.AddressLine2TextEdit.Size = new System.Drawing.Size(458, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine2TextEdit, optionsSpelling5);
            this.AddressLine2TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine2TextEdit.TabIndex = 49;
            // 
            // AddressLine1TextEdit
            // 
            this.AddressLine1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06221OMWasteDisposalCentreEditBindingSource, "AddressLine1", true));
            this.AddressLine1TextEdit.Location = new System.Drawing.Point(158, 59);
            this.AddressLine1TextEdit.MenuManager = this.barManager1;
            this.AddressLine1TextEdit.Name = "AddressLine1TextEdit";
            this.AddressLine1TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine1TextEdit, true);
            this.AddressLine1TextEdit.Size = new System.Drawing.Size(458, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine1TextEdit, optionsSpelling6);
            this.AddressLine1TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine1TextEdit.TabIndex = 48;
            // 
            // WasteDisposalCentreIDTextEdit
            // 
            this.WasteDisposalCentreIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06221OMWasteDisposalCentreEditBindingSource, "WasteDisposalCentreID", true));
            this.WasteDisposalCentreIDTextEdit.Location = new System.Drawing.Point(142, 93);
            this.WasteDisposalCentreIDTextEdit.MenuManager = this.barManager1;
            this.WasteDisposalCentreIDTextEdit.Name = "WasteDisposalCentreIDTextEdit";
            this.WasteDisposalCentreIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WasteDisposalCentreIDTextEdit, true);
            this.WasteDisposalCentreIDTextEdit.Size = new System.Drawing.Size(474, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WasteDisposalCentreIDTextEdit, optionsSpelling7);
            this.WasteDisposalCentreIDTextEdit.StyleController = this.dataLayoutControl1;
            this.WasteDisposalCentreIDTextEdit.TabIndex = 25;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp06221OMWasteDisposalCentreEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(158, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // WasteDisposalCentreNameTextEdit
            // 
            this.WasteDisposalCentreNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06221OMWasteDisposalCentreEditBindingSource, "WasteDisposalCentreName", true));
            this.WasteDisposalCentreNameTextEdit.Location = new System.Drawing.Point(158, 35);
            this.WasteDisposalCentreNameTextEdit.MenuManager = this.barManager1;
            this.WasteDisposalCentreNameTextEdit.Name = "WasteDisposalCentreNameTextEdit";
            this.WasteDisposalCentreNameTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WasteDisposalCentreNameTextEdit, true);
            this.WasteDisposalCentreNameTextEdit.Size = new System.Drawing.Size(458, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WasteDisposalCentreNameTextEdit, optionsSpelling8);
            this.WasteDisposalCentreNameTextEdit.StyleController = this.dataLayoutControl1;
            this.WasteDisposalCentreNameTextEdit.TabIndex = 33;
            this.WasteDisposalCentreNameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.WasteDisposalCentreNameTextEdit_Validating);
            // 
            // RemarksTextEdit
            // 
            this.RemarksTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06221OMWasteDisposalCentreEditBindingSource, "Remarks", true));
            this.RemarksTextEdit.Location = new System.Drawing.Point(36, 331);
            this.RemarksTextEdit.MenuManager = this.barManager1;
            this.RemarksTextEdit.Name = "RemarksTextEdit";
            this.RemarksTextEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksTextEdit, true);
            this.RemarksTextEdit.Size = new System.Drawing.Size(556, 73);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksTextEdit, optionsSpelling9);
            this.RemarksTextEdit.StyleController = this.dataLayoutControl1;
            this.RemarksTextEdit.TabIndex = 47;
            // 
            // ItemForWasteDisposalCentreID
            // 
            this.ItemForWasteDisposalCentreID.Control = this.WasteDisposalCentreIDTextEdit;
            this.ItemForWasteDisposalCentreID.CustomizationFormText = "Waste Disposal Centre ID:";
            this.ItemForWasteDisposalCentreID.Location = new System.Drawing.Point(0, 81);
            this.ItemForWasteDisposalCentreID.Name = "ItemForWasteDisposalCentreID";
            this.ItemForWasteDisposalCentreID.Size = new System.Drawing.Size(608, 24);
            this.ItemForWasteDisposalCentreID.Text = "Waste Disposal Centre ID:";
            this.ItemForWasteDisposalCentreID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 452);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.layoutControlGroup6,
            this.ItemForWasteDisposalCentreName,
            this.emptySpaceItem3,
            this.emptySpaceItem6,
            this.ItemForAddressLine1,
            this.ItemForAddressLine2,
            this.ItemForAddressLine3,
            this.ItemForAddressLine4,
            this.ItemForAddressLine5,
            this.ItemForPostcode,
            this.ItemForLatitude,
            this.ItemForLongitude,
            this.emptySpaceItem4,
            this.emptySpaceItem5});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 432);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(146, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(146, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(146, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(323, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(285, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(146, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 249);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(608, 171);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(584, 125);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImage")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForComments});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(560, 77);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForComments
            // 
            this.ItemForComments.Control = this.RemarksTextEdit;
            this.ItemForComments.CustomizationFormText = "Comments";
            this.ItemForComments.Location = new System.Drawing.Point(0, 0);
            this.ItemForComments.Name = "ItemForComments";
            this.ItemForComments.Size = new System.Drawing.Size(560, 77);
            this.ItemForComments.Text = "Comments";
            this.ItemForComments.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForComments.TextVisible = false;
            // 
            // ItemForWasteDisposalCentreName
            // 
            this.ItemForWasteDisposalCentreName.Control = this.WasteDisposalCentreNameTextEdit;
            this.ItemForWasteDisposalCentreName.CustomizationFormText = "Description:";
            this.ItemForWasteDisposalCentreName.Location = new System.Drawing.Point(0, 23);
            this.ItemForWasteDisposalCentreName.Name = "ItemForWasteDisposalCentreName";
            this.ItemForWasteDisposalCentreName.Size = new System.Drawing.Size(608, 24);
            this.ItemForWasteDisposalCentreName.Text = "Waste Disposal Centre Name:";
            this.ItemForWasteDisposalCentreName.TextSize = new System.Drawing.Size(143, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 420);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(608, 12);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 239);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForAddressLine1
            // 
            this.ItemForAddressLine1.Control = this.AddressLine1TextEdit;
            this.ItemForAddressLine1.CustomizationFormText = "Address Line 1:";
            this.ItemForAddressLine1.Location = new System.Drawing.Point(0, 47);
            this.ItemForAddressLine1.Name = "ItemForAddressLine1";
            this.ItemForAddressLine1.Size = new System.Drawing.Size(608, 24);
            this.ItemForAddressLine1.Text = "Address Line 1:";
            this.ItemForAddressLine1.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForAddressLine2
            // 
            this.ItemForAddressLine2.Control = this.AddressLine2TextEdit;
            this.ItemForAddressLine2.CustomizationFormText = "Address Line 2:";
            this.ItemForAddressLine2.Location = new System.Drawing.Point(0, 71);
            this.ItemForAddressLine2.Name = "ItemForAddressLine2";
            this.ItemForAddressLine2.Size = new System.Drawing.Size(608, 24);
            this.ItemForAddressLine2.Text = "Address Line 2:";
            this.ItemForAddressLine2.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForAddressLine3
            // 
            this.ItemForAddressLine3.Control = this.AddressLine3TextEdit;
            this.ItemForAddressLine3.CustomizationFormText = "Address Line 3:";
            this.ItemForAddressLine3.Location = new System.Drawing.Point(0, 95);
            this.ItemForAddressLine3.Name = "ItemForAddressLine3";
            this.ItemForAddressLine3.Size = new System.Drawing.Size(608, 24);
            this.ItemForAddressLine3.Text = "Address Line 3:";
            this.ItemForAddressLine3.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForAddressLine4
            // 
            this.ItemForAddressLine4.Control = this.AddressLine4TextEdit;
            this.ItemForAddressLine4.CustomizationFormText = "Address Line 4:";
            this.ItemForAddressLine4.Location = new System.Drawing.Point(0, 119);
            this.ItemForAddressLine4.Name = "ItemForAddressLine4";
            this.ItemForAddressLine4.Size = new System.Drawing.Size(608, 24);
            this.ItemForAddressLine4.Text = "Address Line 4:";
            this.ItemForAddressLine4.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForAddressLine5
            // 
            this.ItemForAddressLine5.Control = this.AddressLine5TextEdit;
            this.ItemForAddressLine5.CustomizationFormText = "Address Line 5:";
            this.ItemForAddressLine5.Location = new System.Drawing.Point(0, 143);
            this.ItemForAddressLine5.Name = "ItemForAddressLine5";
            this.ItemForAddressLine5.Size = new System.Drawing.Size(608, 24);
            this.ItemForAddressLine5.Text = "Address Line 5:";
            this.ItemForAddressLine5.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForPostcode
            // 
            this.ItemForPostcode.Control = this.PostcodeTextEdit;
            this.ItemForPostcode.CustomizationFormText = "Postcode:";
            this.ItemForPostcode.Location = new System.Drawing.Point(0, 167);
            this.ItemForPostcode.Name = "ItemForPostcode";
            this.ItemForPostcode.Size = new System.Drawing.Size(608, 24);
            this.ItemForPostcode.Text = "Postcode:";
            this.ItemForPostcode.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForLatitude
            // 
            this.ItemForLatitude.Control = this.LatitudeSpinEdit;
            this.ItemForLatitude.CustomizationFormText = "Latitude:";
            this.ItemForLatitude.Location = new System.Drawing.Point(0, 191);
            this.ItemForLatitude.MaxSize = new System.Drawing.Size(304, 24);
            this.ItemForLatitude.MinSize = new System.Drawing.Size(304, 24);
            this.ItemForLatitude.Name = "ItemForLatitude";
            this.ItemForLatitude.Size = new System.Drawing.Size(304, 24);
            this.ItemForLatitude.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLatitude.Text = "Latitude:";
            this.ItemForLatitude.TextSize = new System.Drawing.Size(143, 13);
            // 
            // ItemForLongitude
            // 
            this.ItemForLongitude.Control = this.LongitudeSpinEdit;
            this.ItemForLongitude.CustomizationFormText = "Longitude:";
            this.ItemForLongitude.Location = new System.Drawing.Point(0, 215);
            this.ItemForLongitude.MaxSize = new System.Drawing.Size(304, 24);
            this.ItemForLongitude.MinSize = new System.Drawing.Size(304, 24);
            this.ItemForLongitude.Name = "ItemForLongitude";
            this.ItemForLongitude.Size = new System.Drawing.Size(304, 24);
            this.ItemForLongitude.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLongitude.Text = "Longitude:";
            this.ItemForLongitude.TextSize = new System.Drawing.Size(143, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(304, 191);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(304, 24);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(304, 215);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(304, 24);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06221_OM_Waste_Disposal_Centre_EditTableAdapter
            // 
            this.sp06221_OM_Waste_Disposal_Centre_EditTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Waste_Disposal_Centre_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 508);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Waste_Disposal_Centre_Edit";
            this.Text = "Edit Waste Disposal Centre";
            this.Activated += new System.EventHandler(this.frm_OM_Waste_Disposal_Centre_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Waste_Disposal_Centre_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Waste_Disposal_Centre_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LongitudeSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06221OMWasteDisposalCentreEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LatitudeSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine5TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine4TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine3TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WasteDisposalCentreIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WasteDisposalCentreNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWasteDisposalCentreID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWasteDisposalCentreName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLongitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.TextEdit WasteDisposalCentreIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWasteDisposalCentreID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.TextEdit WasteDisposalCentreNameTextEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForComments;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWasteDisposalCentreName;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private System.Windows.Forms.BindingSource sp06221OMWasteDisposalCentreEditBindingSource;
        private DataSet_OM_Job dataSet_OM_Job;
        private DataSet_OM_JobTableAdapters.sp06221_OM_Waste_Disposal_Centre_EditTableAdapter sp06221_OM_Waste_Disposal_Centre_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit AddressLine1TextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine1;
        private DevExpress.XtraEditors.TextEdit AddressLine2TextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine2;
        private DevExpress.XtraEditors.TextEdit AddressLine4TextEdit;
        private DevExpress.XtraEditors.TextEdit AddressLine3TextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine4;
        private DevExpress.XtraEditors.TextEdit AddressLine5TextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine5;
        private DevExpress.XtraEditors.TextEdit PostcodeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPostcode;
        private DevExpress.XtraEditors.SpinEdit LongitudeSpinEdit;
        private DevExpress.XtraEditors.SpinEdit LatitudeSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLatitude;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLongitude;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
    }
}

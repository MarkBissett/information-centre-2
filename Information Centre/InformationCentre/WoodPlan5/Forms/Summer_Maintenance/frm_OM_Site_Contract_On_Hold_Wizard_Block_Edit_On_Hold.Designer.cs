﻿namespace WoodPlan5
{
    partial class frm_OM_Site_Contract_On_Hold_Wizard_Block_Edit_On_Hold
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.gridLookUpEditOnHoldReasonID = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateEditOnHoldEndDate = new DevExpress.XtraEditors.DateEdit();
            this.DateEditOnHoldStartDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForOnHoldStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOnHoldEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOnHoldReasonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.memoEditRemarks = new DevExpress.XtraEditors.MemoEdit();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditOnHoldReasonID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOnHoldEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOnHoldEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditOnHoldStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditOnHoldStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOnHoldStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOnHoldEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOnHoldReasonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditRemarks.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(594, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 286);
            this.barDockControlBottom.Size = new System.Drawing.Size(594, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 260);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(594, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 260);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "ID";
            this.gridColumn10.FieldName = "ID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn10.Width = 53;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.memoEditRemarks);
            this.layoutControl1.Controls.Add(this.gridLookUpEditOnHoldReasonID);
            this.layoutControl1.Controls.Add(this.dateEditOnHoldEndDate);
            this.layoutControl1.Controls.Add(this.DateEditOnHoldStartDate);
            this.layoutControl1.Location = new System.Drawing.Point(0, 28);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1213, 293, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(593, 227);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // gridLookUpEditOnHoldReasonID
            // 
            this.gridLookUpEditOnHoldReasonID.EditValue = 2;
            this.gridLookUpEditOnHoldReasonID.Location = new System.Drawing.Point(97, 60);
            this.gridLookUpEditOnHoldReasonID.MenuManager = this.barManager1;
            this.gridLookUpEditOnHoldReasonID.Name = "gridLookUpEditOnHoldReasonID";
            this.gridLookUpEditOnHoldReasonID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditOnHoldReasonID.Properties.DataSource = this.sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource;
            this.gridLookUpEditOnHoldReasonID.Properties.DisplayMember = "Description";
            this.gridLookUpEditOnHoldReasonID.Properties.NullText = "";
            this.gridLookUpEditOnHoldReasonID.Properties.ValueMember = "ID";
            this.gridLookUpEditOnHoldReasonID.Properties.View = this.gridView5;
            this.gridLookUpEditOnHoldReasonID.Size = new System.Drawing.Size(484, 20);
            this.gridLookUpEditOnHoldReasonID.StyleController = this.layoutControl1;
            this.gridLookUpEditOnHoldReasonID.TabIndex = 2;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.gridColumn10;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = -1;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView5.FormatRules.Add(gridFormatRule1);
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "On-Hold Reason";
            this.gridColumn11.FieldName = "Description";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            this.gridColumn11.Width = 220;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Order";
            this.gridColumn12.FieldName = "RecordOrder";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // dateEditOnHoldEndDate
            // 
            this.dateEditOnHoldEndDate.EditValue = null;
            this.dateEditOnHoldEndDate.Location = new System.Drawing.Point(97, 36);
            this.dateEditOnHoldEndDate.MenuManager = this.barManager1;
            this.dateEditOnHoldEndDate.Name = "dateEditOnHoldEndDate";
            this.dateEditOnHoldEndDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditOnHoldEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditOnHoldEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditOnHoldEndDate.Properties.Mask.EditMask = "g";
            this.dateEditOnHoldEndDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditOnHoldEndDate.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditOnHoldEndDate.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateEditOnHoldEndDate.Size = new System.Drawing.Size(186, 20);
            this.dateEditOnHoldEndDate.StyleController = this.layoutControl1;
            this.dateEditOnHoldEndDate.TabIndex = 1;
            // 
            // DateEditOnHoldStartDate
            // 
            this.DateEditOnHoldStartDate.EditValue = null;
            this.DateEditOnHoldStartDate.Location = new System.Drawing.Point(97, 12);
            this.DateEditOnHoldStartDate.MenuManager = this.barManager1;
            this.DateEditOnHoldStartDate.Name = "DateEditOnHoldStartDate";
            this.DateEditOnHoldStartDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DateEditOnHoldStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEditOnHoldStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEditOnHoldStartDate.Properties.Mask.EditMask = "g";
            this.DateEditOnHoldStartDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateEditOnHoldStartDate.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.DateEditOnHoldStartDate.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.DateEditOnHoldStartDate.Size = new System.Drawing.Size(186, 20);
            this.DateEditOnHoldStartDate.StyleController = this.layoutControl1;
            this.DateEditOnHoldStartDate.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForOnHoldStartDate,
            this.ItemForOnHoldEndDate,
            this.ItemForOnHoldReasonID,
            this.emptySpaceItem1,
            this.ItemForRemarks});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(593, 227);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // ItemForOnHoldStartDate
            // 
            this.ItemForOnHoldStartDate.Control = this.DateEditOnHoldStartDate;
            this.ItemForOnHoldStartDate.CustomizationFormText = "On-Hold Start:";
            this.ItemForOnHoldStartDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForOnHoldStartDate.Name = "ItemForOnHoldStartDate";
            this.ItemForOnHoldStartDate.Size = new System.Drawing.Size(275, 24);
            this.ItemForOnHoldStartDate.Text = "On-Hold Start:";
            this.ItemForOnHoldStartDate.TextSize = new System.Drawing.Size(82, 13);
            // 
            // ItemForOnHoldEndDate
            // 
            this.ItemForOnHoldEndDate.Control = this.dateEditOnHoldEndDate;
            this.ItemForOnHoldEndDate.CustomizationFormText = "On-Hold End:";
            this.ItemForOnHoldEndDate.Location = new System.Drawing.Point(0, 24);
            this.ItemForOnHoldEndDate.Name = "ItemForOnHoldEndDate";
            this.ItemForOnHoldEndDate.Size = new System.Drawing.Size(275, 24);
            this.ItemForOnHoldEndDate.Text = "On-Hold End:";
            this.ItemForOnHoldEndDate.TextSize = new System.Drawing.Size(82, 13);
            // 
            // ItemForOnHoldReasonID
            // 
            this.ItemForOnHoldReasonID.Control = this.gridLookUpEditOnHoldReasonID;
            this.ItemForOnHoldReasonID.CustomizationFormText = "On-Hold Reason:";
            this.ItemForOnHoldReasonID.Location = new System.Drawing.Point(0, 48);
            this.ItemForOnHoldReasonID.Name = "ItemForOnHoldReasonID";
            this.ItemForOnHoldReasonID.Size = new System.Drawing.Size(573, 24);
            this.ItemForOnHoldReasonID.Text = "On-Hold Reason:";
            this.ItemForOnHoldReasonID.TextSize = new System.Drawing.Size(82, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(275, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(298, 48);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(374, 256);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(96, 22);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(485, 256);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(96, 22);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // memoEditRemarks
            // 
            this.memoEditRemarks.Location = new System.Drawing.Point(97, 84);
            this.memoEditRemarks.MenuManager = this.barManager1;
            this.memoEditRemarks.Name = "memoEditRemarks";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memoEditRemarks, true);
            this.memoEditRemarks.Size = new System.Drawing.Size(484, 131);
            this.scSpellChecker.SetSpellCheckerOptions(this.memoEditRemarks, optionsSpelling1);
            this.memoEditRemarks.StyleController = this.layoutControl1;
            this.memoEditRemarks.TabIndex = 4;
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.memoEditRemarks;
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 72);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(573, 135);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(82, 13);
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource
            // 
            this.sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource.DataMember = "sp06406_OM_Site_Contract_On_Hold_Reasons_With_Blank";
            this.sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter
            // 
            this.sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Site_Contract_On_Hold_Wizard_Block_Edit_On_Hold
            // 
            this.ClientSize = new System.Drawing.Size(594, 286);
            this.ControlBox = false;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OM_Site_Contract_On_Hold_Wizard_Block_Edit_On_Hold";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Site Contract On-Hold Wizard - Block Edit";
            this.Load += new System.EventHandler(this.frm_OM_Site_Contract_On_Hold_Wizard_Block_Edit_On_Hold_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditOnHoldReasonID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOnHoldEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOnHoldEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditOnHoldStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditOnHoldStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOnHoldStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOnHoldEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOnHoldReasonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditRemarks.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.DateEdit DateEditOnHoldStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOnHoldStartDate;
        private DevExpress.XtraEditors.DateEdit dateEditOnHoldEndDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOnHoldEndDate;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditOnHoldReasonID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOnHoldReasonID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.MemoEdit memoEditRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private System.Windows.Forms.BindingSource sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter;
    }
}

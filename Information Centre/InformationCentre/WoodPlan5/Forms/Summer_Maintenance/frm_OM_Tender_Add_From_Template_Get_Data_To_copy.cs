﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;
using DevExpress.XtraEditors;
using DevExpress.Utils;

namespace WoodPlan5
{
    public partial class frm_OM_Tender_Add_From_Template_Get_Data_To_copy : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public int _PassedInTenderID = 0;

        public bool _ClientData = true;
        public bool _SiteData = true;
        public bool _JobData = true;
        public bool _ProposedLabourData = true;
           
        #endregion

        public frm_OM_Tender_Add_From_Template_Get_Data_To_copy()
        {
            InitializeComponent();
        }

        private void frm_OM_Tender_Add_From_Template_Get_Data_To_copy_Load(object sender, EventArgs e)
        {
            this.FormID = 500286;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //

            strConnectionString = this.GlobalSettings.ConnectionString;

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
            groupControl4.Text = "Data To <b>Copy</b> from Tender: " + _PassedInTenderID.ToString();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

      
        private void btnOK_Click(object sender, EventArgs e)
        {
            _ClientData = checkEditClientData.Checked;
            _SiteData = checkEditSiteData.Checked;
            _JobData = checkEditJobData.Checked;
            _ProposedLabourData = checkEditLabourData.Checked;

            if (!_ClientData && !_SiteData && !_JobData && !_ProposedLabourData)
            {
                XtraMessageBox.Show("Tick at least one item to copy before proceeding.", "Select Data to Copy To Tender", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }


        #region Editors
 
        private void checkEditClientData_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                checkEditSiteData.Properties.ReadOnly = false;
            }
            else
            {
                checkEditSiteData.Properties.ReadOnly = true;
                checkEditSiteData.Checked = false;
            }

        }

        #endregion








    }
}

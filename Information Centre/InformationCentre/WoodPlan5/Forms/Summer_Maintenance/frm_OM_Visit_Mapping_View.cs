﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraMap;
using DevExpress.Utils.Menu;  // Required to disable Column Chooser on Grids //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using LocusEffects;
using WoodPlan5.Classes.Operations;

namespace WoodPlan5
{
    public partial class frm_OM_Visit_Mapping_View : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection3;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        private Utils.enmFocusedGrid _enmFocusedGrid = Utils.enmFocusedGrid.Sites;

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;
        private BeaconLocusEffect m_customBeaconLocusEffect1 = null;

        string i_str_MapType = "Bing Hybrid";

        double latitude;
        double longitude;
        private bool BestFitZoomInProcess = false;
        int intPointsToZoomToCount = 0;
        int intRecordIDToZoomTo = 0;

        VectorItemsLayer mapObjectsLayer_Sites { get { return (VectorItemsLayer)mapControl1.Layers[1]; } }
        VectorItemsLayer mapObjectsLayer_Visits { get { return (VectorItemsLayer)mapControl1.Layers[2]; } }
        VectorItemsLayer mapObjectsLayer_Jobs { get { return (VectorItemsLayer)mapControl1.Layers[3]; } }

        public string i_str_PassedInSiteIDs = "";
        public string i_str_PassedInVisitIDs = "";
        public string i_str_PassedInJobIDs = "";
        public string i_str_PassedInClientIDs = "";
        public string i_str_selected_Site_descriptions = "";
        public bool i_bool_LoadSitesInMapOnStart = false;
        public bool i_bool_LoadVisitsInMapOnStart = false;
        public bool i_bool_LoadJobsInMapOnStart = false;
        public string i_str_CentrePointType = "start";  // Optional - permissable values: start\finish //

        private ArrayList arraylistFloatingPanels = new ArrayList();  // Holds which panels were floating when form was deactivated so the panels can be hidden then re-shown on form activate //

        private bool boolFirstLoadOfMap = true;

        SuperToolTip superToolTipSiteContractFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsSiteContractFilter = null;

        const string bingkey = "As_rq12T3oFpxWh52cGqK5yhqcHov7wkd4orPObzZFgo2N92CqPDc1xO_uokdUdT";
        #endregion

        public frm_OM_Visit_Mapping_View()
        {
            InitializeComponent();
        }

        private void frm_OM_Visit_Mapping_View_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 500158;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp06354_OM_Mapping_Layer_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06354_OM_Mapping_Layer_ManagerTableAdapter.Fill(dataSet_OM_Core.sp06354_OM_Mapping_Layer_Manager);

            sp06152_OM_Mapping_VisitsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06153_OM_Mapping_Visits_Map_ObjectsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06154_OM_Visit_Mapping_SitesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06155_OM_Visit_Mapping_Sites_Map_ObjectsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06276_OM_Mapping_JobsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06277_OM_Mapping_Jobs_Map_ObjectsTableAdapter.Connection.ConnectionString = strConnectionString;

            RefreshGridViewState1 = new RefreshGridState(gridView1, "SiteID");
            RefreshGridViewState2 = new RefreshGridState(gridView2, "VisitID");
            RefreshGridViewState3 = new RefreshGridState(gridView2, "JobID");
            gridControl1.ForceInitialize();
            gridControl2.ForceInitialize();
            gridControl3.ForceInitialize();

            // Add record selection checkboxes to grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 65; // Make wider to allow for caption in header //
            selection1.CheckMarkColumn.Caption = "     Select";  // Spaces at start so room for checkbox in front //
            selection1.CheckMarkColumn.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
            selection1.CheckMarkColumn.OptionsColumn.ShowCaption = true;
            selection1.CheckMarkColumn.ToolTip = "Controls which Data is Loaded into the Visit grid and the map.";
            selection1.editColumnHeader.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            selection1.editColumnHeader.Caption = "";
            selection1.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            selection1.CheckMarkColumn.VisibleIndex = 0;

            // Add record selection checkboxes to grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 65; // Make wider to allow for caption in header //
            selection2.CheckMarkColumn.Caption = "     Select";  // Spaces at start so room for checkbox in front //
            selection2.CheckMarkColumn.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
            selection2.CheckMarkColumn.OptionsColumn.ShowCaption = true;
            selection2.CheckMarkColumn.ToolTip = "Controls which Data is Loaded into the map.";
            selection2.editColumnHeader.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            selection2.editColumnHeader.Caption = "";
            selection2.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            selection2.CheckMarkColumn.VisibleIndex = 0;

            // Add record selection checkboxes to grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 65; // Make wider to allow for caption in header //
            selection3.CheckMarkColumn.Caption = "     Select";  // Spaces at start so room for checkbox in front //
            selection3.CheckMarkColumn.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
            selection3.CheckMarkColumn.OptionsColumn.ShowCaption = true;
            selection3.CheckMarkColumn.ToolTip = "Controls which Data is Loaded into the map.";
            selection3.editColumnHeader.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            selection3.editColumnHeader.Caption = "";
            selection3.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            selection3.CheckMarkColumn.VisibleIndex = 0;

            InformationLayer infoLayer = new InformationLayer();
            mapControl1.Layers.Add(infoLayer);
            BingSearchDataProvider searchProvider = new BingSearchDataProvider();
            infoLayer.DataProvider = searchProvider;
            searchProvider.BingKey = bingkey;
            searchProvider.SearchOptions.DistanceUnit = DistanceMeasureUnit.Mile;
            searchProvider.SearchOptions.SearchRadius = 200;
            searchProvider.SearchOptions.ResultsCount = 5;
            infoLayer.ZIndex = 0;  // Z Index - lower the number the nearer the top of the map the layer is drawn - layer 0 is top most //

            // Prepare LocusEffects and add custom effect //
            locusEffectsProvider1 = new LocusEffectsProvider();
            locusEffectsProvider1.Initialize();
            locusEffectsProvider1.FramesPerSecond = 30;
            m_customBeaconLocusEffect1 = new BeaconLocusEffect();
            m_customBeaconLocusEffect1.Name = "CustomBeacon1";
            m_customBeaconLocusEffect1.InitialSize = new Size(150, 150);
            m_customBeaconLocusEffect1.AnimationTime = 1500;
            m_customBeaconLocusEffect1.LeadInTime = 0;
            m_customBeaconLocusEffect1.LeadOutTime = 0;
            m_customBeaconLocusEffect1.AnimationStartColor = Color.Red;
            m_customBeaconLocusEffect1.AnimationEndColor = Color.Red;
            m_customBeaconLocusEffect1.AnimationOuterColor = Color.Salmon;
            m_customBeaconLocusEffect1.RingWidth = 6; // 6;
            m_customBeaconLocusEffect1.OuterRingWidth = 3;// 3;
            m_customBeaconLocusEffect1.BodyFadeOut = true;
            m_customBeaconLocusEffect1.Style = BeaconEffectStyles.Shrink;//.HeartBeat;
            locusEffectsProvider1.AddLocusEffect(m_customBeaconLocusEffect1);

            mapObjectsLayer_Visits.DataLoaded += VectorItemsLayer_Visits_DataLoaded;  // Add listener event for Vector Layer Finished Loading Map Objects //
            mapObjectsLayer_Sites.DataLoaded += VectorItemsLayer_Sites_DataLoaded;  // Add listener event for Vector Layer Finished Loading Map Objects //
            mapObjectsLayer_Jobs.DataLoaded += VectorItemsLayer_Jobs_DataLoaded;  // Add listener event for Vector Layer Finished Loading Map Objects //
            mapControl1.RightClickDrag = false;  // Prevent dragging with the right mouse button //

            // Create a SuperToolTip //
            superToolTipSiteContractFilter = new SuperToolTip();
            superToolTipSetupArgsSiteContractFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsSiteContractFilter.Title.Text = "Site Filter - Information";
            superToolTipSetupArgsSiteContractFilter.Title.Image = imageCollection32x32.Images[0];

            string strTooltipText = i_str_selected_Site_descriptions.Replace("| ", "\n");
            superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;

            superToolTipSetupArgsSiteContractFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsSiteContractFilter.Footer.Text = "";
            superToolTipSiteContractFilter.Setup(superToolTipSetupArgsSiteContractFilter);
            superToolTipSiteContractFilter.AllowHtmlText = DefaultBoolean.True;
            buttonEditChooseSites.SuperTip = superToolTipSiteContractFilter;
            buttonEditChooseSites.EditValue = i_str_selected_Site_descriptions;

            Load_Sites();
            GridView view = (GridView)gridControl1.MainView;
            view.ExpandAllGroups();
            view.BeginUpdate();
            if (view.DataRowCount > 0)
            {
                string strSiteID = "";
                string strTempPassedInSiteIDs = "," + i_str_PassedInSiteIDs;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    strSiteID = "," + view.GetRowCellValue(i, "SiteID").ToString() + ",";
                    if (strTempPassedInSiteIDs.Contains(strSiteID))
                    {
                        view.SetRowCellValue(i, "CheckMarkSelection", 1);
                    }
                }
                view.EndUpdate();
            }


            #region MiniMap

            // Create a mini map and data for it.         
            MiniMap miniMap = new MiniMap()
            {
                Alignment = MiniMapAlignment.BottomRight
            };
            miniMap.Behavior = new DynamicMiniMapBehavior();

            miniMap.Layers.Add(new MiniMapImageTilesLayer()
            {
                DataProvider = new BingMapDataProvider() { BingKey = bingkey }
            });
            //miniMap.Layers.Add(new MiniMapVectorItemsLayer());
            //miniMap.Layers.Add(new MiniMapVectorItemsLayer() { Data = CreateMiniMapAdapter(data) });
            mapControl1.MiniMap = miniMap;
            
            #endregion MiniMap
        }

        /*private IMapDataAdapter CreateMiniMapAdapter(object source)
        {
            ListSourceDataAdapter adapter = new ListSourceDataAdapter();

            adapter.DataSource = source;

            adapter.Mappings.Latitude = "Latitude";
            adapter.Mappings.Longitude = "Longitude";

            adapter.PropertyMappings.Add(new MapItemFillMapping() { DefaultValue = Color.Red });
            adapter.PropertyMappings.Add(new MapItemStrokeMapping() { DefaultValue = Color.White });
            adapter.PropertyMappings.Add(new MapItemStrokeWidthMapping() { DefaultValue = 2 });
            adapter.PropertyMappings.Add(new MapDotSizeMapping() { DefaultValue = 8 });

            adapter.DefaultMapItemType = MapItemType.Dot;

            return adapter;
        }*/

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            VectorItemsLayer pointLayer = this.mapControl1.Layers[1] as VectorItemsLayer;
            pointLayer.ViewportChanged += new ViewportChangedEventHandler(pointLayer_ViewportChanged);
            
            LoadLastSavedUserScreenSettings();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();

            if (selection1.SelectedCount > 0 && !string.IsNullOrWhiteSpace(i_str_PassedInVisitIDs))
            {
                //if (!string.IsNullOrWhiteSpace(i_str_PassedInVisitIDs))
                //{
                Load_Visits();
                GridView view = (GridView)gridControl2.MainView;
                if (view.DataRowCount > 0)
                {
                    string strVisitID = "";
                    string strTempPassedInVisitIDs = "," + i_str_PassedInVisitIDs;
                    view.BeginUpdate();
                    for (int i = 0; i < view.DataRowCount; i++)
                    {
                        strVisitID = "," + view.GetRowCellValue(i, "VisitID").ToString() + ",";
                        if (strTempPassedInVisitIDs.Contains(strVisitID))
                        {
                            view.SetRowCellValue(i, "CheckMarkSelection", 1);
                        }
                    }
                    view.EndUpdate();
                    if (selection2.SelectedCount > 0 && i_bool_LoadVisitsInMapOnStart)
                    {
                        btnLoadVisitsIntoMap.PerformClick();  // Load Visits into map //
                    }
                    if (selection2.SelectedCount > 0 && !string.IsNullOrWhiteSpace(i_str_PassedInJobIDs))
                    {
                        Load_Jobs();
                        view = (GridView)gridControl3.MainView;
                        if (view.DataRowCount > 0)
                        {
                            string strJobID = "";
                            string strTempPassedInJobIDs = "," + i_str_PassedInJobIDs;
                            view.BeginUpdate();
                            for (int i = 0; i < view.DataRowCount; i++)
                            {
                                strJobID = "," + view.GetRowCellValue(i, "JobID").ToString() + ",";
                                if (strTempPassedInJobIDs.Contains(strJobID))
                                {
                                    view.SetRowCellValue(i, "CheckMarkSelection", 1);
                                }
                            }
                            view.EndUpdate();
                            if (selection3.SelectedCount > 0 && i_bool_LoadJobsInMapOnStart) btnLoadJobsIntoMap.PerformClick();  // Load Jobs into map //
                        }
                    }

                }
            }
            if (i_bool_LoadSitesInMapOnStart && selection1.SelectedCount > 0) btnLoadSitesIntoMap.PerformClick();  // Load Sites into map //
            // Legend switched on in VectorItemsLayer_DataLoaded event as it doesn't render to the correct location here //
            Application.DoEvents();  // Allow Form time to repaint itself [required for next line to work correctly] //

            int intPosition = (splitContainerControl2.Height / 2) - 5;
            splitContainerControl2.SplitterPosition = intPosition;

            bciLegend.Checked = true;  // Show Legend //

        }
        
        private void frm_OM_Visit_Mapping_View_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
            
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in arraylistFloatingPanels)
            {
                dp.Show();
            }
        }

        private void frm_OM_Visit_Mapping_View_Deactivate(object sender, EventArgs e)
        {
            arraylistFloatingPanels = new ArrayList();
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in dockManager1.Panels)
            {
                if (dp.Dock == DevExpress.XtraBars.Docking.DockingStyle.Float && dp.Visibility == DevExpress.XtraBars.Docking.DockVisibility.Visible)
                {
                    arraylistFloatingPanels.Add(dp);
                    dp.Hide();
                }
            }
        }

        private void frm_OM_Visit_Mapping_View_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                //default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "PingTypeFilter", i_str_selected_ping_type_ids);
                //default_screen_settings.SaveDefaultScreenSettings();
            }
            mapObjectsLayer_Visits.DataLoaded -= VectorItemsLayer_Visits_DataLoaded;  // Remove Event //
            mapObjectsLayer_Sites.DataLoaded -= VectorItemsLayer_Sites_DataLoaded;  // Remove Event //
            mapObjectsLayer_Jobs.DataLoaded -= VectorItemsLayer_Jobs_DataLoaded;  // Remove Event //
        }


        void pointLayer_ViewportChanged(object sender, ViewportChangedEventArgs e)
        {
            /*VectorItemsLayer pointLayer = (VectorItemsLayer)sender;
            if (BestFitZoomInProcess)
            {
                int p_count = 0;
                for (int i = 0; i < pointLayer.Items.Count; i++)
                {
                    GeoPoint geoPoint = ((MapCustomElement)pointLayer.Items[i]).Location;
                    if (Convert.ToInt32(pointLayer.Items[i].Attributes["ContractorID"].Value) == intRecordIDToZoomTo)
                    {
                        if (geoPoint.Latitude < e.TopLeft.Latitude && geoPoint.Latitude > e.BottomRight.Latitude && geoPoint.Longitude > e.TopLeft.Longitude && geoPoint.Longitude < e.BottomRight.Longitude) p_count++;
                    }
                }
                if (p_count == intPointsToZoomToCount) BestFitZoomInProcess = false;
            }*/
        }

        public void LoadLastSavedUserScreenSettings()
        {
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of navigator custom buttons //
            GridView view = (GridView)gridControl4.MainView;
            int intFocusedRow = view.FocusedRowHandle;
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow <= 1 || intFocusedRow == view.DataRowCount - 1 ? false : true);
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow >= (view.DataRowCount - 2) || intFocusedRow == 0 ? false : true);
        }

        private void buttonEditChooseSites_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            switch (e.Button.Tag.ToString().ToLower())
            {
                case "choose":
                    {
                        var fChildForm = new frm_Core_Select_Client_Site();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intAllowChildSelection = 1;
                        fChildForm.intMustSelectChildren = 1;
                        fChildForm.strPassedInParentIDs = i_str_PassedInClientIDs;
                        fChildForm.strPassedInChildIDs = i_str_PassedInSiteIDs;
                        fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                        fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                        fChildForm.intFilterSummerMaintenanceClient = 1;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (i_str_PassedInSiteIDs == fChildForm.strSelectedChildIDs) return;
                            i_str_PassedInSiteIDs = fChildForm.strSelectedChildIDs;
                            i_str_selected_Site_descriptions = fChildForm.strSelectedFullSiteDescriptions;
                            i_str_PassedInClientIDs = fChildForm.strSelectedParentIDs;
                            buttonEditChooseSites.EditValue = fChildForm.strSelectedChildDescriptions;

                            // Update Filter control's tooltip //
                            string strTooltipText = i_str_selected_Site_descriptions.Replace("| ", "\n");
                            superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                            
                            Load_Sites();
                        }
                    }
                    break;
                default:
                    break;
            }
        }


        #region Map Type

        private void btnOKMapType_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditMapType_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit2_Get_Selected();
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            string strTempType = "";
            if (checkEdit1.Checked)
            {
                strTempType = "Bing Hybrid";
                if (strTempType != i_str_MapType)
                {
                    BingMapDataProvider bingProvider = new BingMapDataProvider();
                    ImageTilesLayer layer = (ImageTilesLayer)mapControl1.Layers[0];
                    layer.DataProvider = bingProvider;
                    bingProvider.BingKey = bingkey;
                    bingProvider.Kind = BingMapKind.Hybrid;
                    i_str_MapType = strTempType;
                    
                    mapControl1.MiniMap.Layers.Clear();
                    mapControl1.MiniMap.Layers.Add(new MiniMapImageTilesLayer()
                    {
                        DataProvider = new BingMapDataProvider() { BingKey = bingkey, Kind = BingMapKind.Hybrid }
                    });
                }
            }
            else if (checkEdit2.Checked)
            {
                strTempType = "Bing Satellite";
                if (strTempType != i_str_MapType)
                {
                    BingMapDataProvider bingProvider = new BingMapDataProvider();
                    ImageTilesLayer layer = (ImageTilesLayer)mapControl1.Layers[0];
                    layer.DataProvider = bingProvider;
                    bingProvider.BingKey = bingkey;
                    bingProvider.Kind = BingMapKind.Area;
                    i_str_MapType = strTempType;

                    mapControl1.MiniMap.Layers.Clear();
                    mapControl1.MiniMap.Layers.Add(new  MiniMapImageTilesLayer()
                    {
                        DataProvider = new BingMapDataProvider() { BingKey = bingkey, Kind = BingMapKind.Area }                        
                    });
                }
            }
            else if (checkEdit3.Checked)
            {
                strTempType = "Bing Road";
                if (strTempType != i_str_MapType)
                {
                    BingMapDataProvider bingProvider = new BingMapDataProvider();
                    ImageTilesLayer layer = (ImageTilesLayer)mapControl1.Layers[0];
                    layer.DataProvider = bingProvider;
                    bingProvider.BingKey = bingkey;
                    bingProvider.Kind = BingMapKind.Road;
                    i_str_MapType = strTempType;

                    mapControl1.MiniMap.Layers.Clear();
                    mapControl1.MiniMap.Layers.Add(new MiniMapImageTilesLayer()
                    {
                        DataProvider = new BingMapDataProvider() { BingKey = bingkey, Kind = BingMapKind.Road }
                    });
                }
            }
            else
            {
                strTempType = "Open Street Map";
                if (strTempType != i_str_MapType)
                {
                    OpenStreetMapDataProvider provider = new OpenStreetMapDataProvider();
                    ImageTilesLayer layer = (ImageTilesLayer)mapControl1.Layers[0];
                    layer.DataProvider = provider;
                    i_str_MapType = strTempType;

                    mapControl1.MiniMap.Layers.Clear();
                    mapControl1.MiniMap.Layers.Add(new MiniMapImageTilesLayer()
                    {
                        DataProvider = new OpenStreetMapDataProvider() { Kind = OpenStreetMapKind.Basic }
                    });
                }
            }
            return i_str_MapType;
        }

        #endregion


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Sites Available - Adjust any filters or click the Choose button";
                    break;
                case "gridView2":
                    message = "No Visits Available - Select one or more Sites and the Client Contract(s) and click Load Visits";
                    break;
                case "gridView3":
                    message = "No Jobs Available - Select one or more Visits and click Load Jobs";
                    break;
                case "gridView4":
                    message = "No Layers Available";
                    break;                    
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        #region GridControl 1

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bbiViewSiteOnMap.Enabled = (intCount == 1);

                pmSiteGridMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridControl 2

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bbiViewVisitStartOnMap.Enabled = (intCount == 1);
                bbiViewVisitFinishOnMap.Enabled = (intCount == 1);

                pmVisitGridMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridControl 3

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bbiViewJobStartOnMap.Enabled = (intCount == 1);
                bbiViewJobFinishOnMap.Enabled = (intCount == 1);

                pmJobGridMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region Map Control

        private void mapControl1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            MapControl map = sender as MapControl;
            MapHitInfo hitInfo = map.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left)
            {
                return;
            }
            if (e.Button == MouseButtons.Right)
            {
                pmMapControl.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void bbiMapPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            WaitDialogForm loadingForm = new WaitDialogForm("Loading Preview...", "Print Preview");
            loadingForm.Show();

            frmPrintPreview fPrintPreview = new frmPrintPreview();
            fPrintPreview.loadingForm = loadingForm;
            fPrintPreview.objObject = this;
            fPrintPreview.MdiParent = this.MdiParent;
            fPrintPreview.GlobalSettings = this.GlobalSettings;
            fPrintPreview.StaffID = GlobalSettings.UserID;
            fPrintPreview.Show();
            
            // Invoke Post Open event on Base form //
            MethodInfo method = typeof(frmBase).GetMethod("PostOpen");
            if (method != null) method.Invoke(fPrintPreview, new object[] { null });
        }

        void VectorItemsLayer_Sites_DataLoaded(object sender, DataLoadedEventArgs e)
        {
            if (this.splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                this.splashScreenManager.CloseWaitForm();
            }

            // Following code is a hack to get round the legend showing up in the wrong place when the screen first opens - code moved here from PostOpen event //
            if (boolFirstLoadOfMap && !string.IsNullOrWhiteSpace(i_str_PassedInSiteIDs) && string.IsNullOrWhiteSpace(i_str_PassedInVisitIDs) && string.IsNullOrWhiteSpace(i_str_PassedInJobIDs))
            {
                boolFirstLoadOfMap = false;

                // Centre on last loaded visit object if there is one //
                double dLatitude = (double)0.0;
                double dLongitude = (double)0.0;
                GridView view = (GridView)gridControl1.MainView;
                bool boolMapCentreSet = false;
                for (int i = view.DataRowCount - 1; i >= 0; i--)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        dLatitude = Convert.ToDouble(view.GetRowCellValue(i, "Latitude"));
                        dLongitude = Convert.ToDouble(view.GetRowCellValue(i, "Longitude"));
                        if (dLatitude != (double)0.0 || dLongitude != (double)0.0)
                        {
                            int intSiteID = Convert.ToInt32(view.GetRowCellValue(i, "SiteID"));
                            ViewObjectOnMap(intSiteID, mapObjectsLayer_Sites, "SiteID", "Site", false);
                            view.SelectRow(i);
                            view.MakeRowVisible(i, false);

                            mapControl1.Refresh();
                            boolMapCentreSet = true;
                        }
                    }
                    if (boolMapCentreSet) break;
                }
                //Application.DoEvents();  // Allow Form time to repaint itself [required for next line to work correctly //
                //bciLegend.Checked = true;  // Show Legend //
            }
        }

        void VectorItemsLayer_Visits_DataLoaded(object sender, DataLoadedEventArgs e)
        {
            if (this.splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                this.splashScreenManager.CloseWaitForm();
            }

            if (boolFirstLoadOfMap && !string.IsNullOrWhiteSpace(i_str_PassedInVisitIDs) && string.IsNullOrWhiteSpace(i_str_PassedInJobIDs))
            {
                boolFirstLoadOfMap = false;

                // Centre on last loaded visit object if there is one //
                double dLatitude = (double)0.0;
                double dLongitude = (double)0.0;
                GridView view = (GridView)gridControl2.MainView;
                bool boolMapCentreSet = false;
                for (int i = view.DataRowCount - 1; i >= 0; i--)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        if (i_str_CentrePointType.ToLower() == "finish")
                        {
                            dLatitude = Convert.ToDouble(view.GetRowCellValue(i, "FinishLatitude"));
                            dLongitude = Convert.ToDouble(view.GetRowCellValue(i, "FinishLongitude"));
                            if (dLatitude != (double)0.0 || dLongitude != (double)0.0)
                            {
                                int intVisitID = Convert.ToInt32(view.GetRowCellValue(i, "VisitID"));
                                ViewObjectOnMap(intVisitID, mapObjectsLayer_Visits, "VisitID", "Visit Finish", false);

                                view.SelectRow(i);
                                view.MakeRowVisible(i, false);

                                mapControl1.Refresh();
                                boolMapCentreSet = true;
                            }
                        }
                        else
                        {
                            dLatitude = Convert.ToDouble(view.GetRowCellValue(i, "StartLatitude"));
                            dLongitude = Convert.ToDouble(view.GetRowCellValue(i, "StartLongitude"));
                            if (dLatitude != (double)0.0 || dLongitude != (double)0.0)
                            {
                                int intVisitID = Convert.ToInt32(view.GetRowCellValue(i, "VisitID"));
                                ViewObjectOnMap(intVisitID, mapObjectsLayer_Visits, "VisitID", "Visit Start", false);

                                view.SelectRow(i);
                                view.MakeRowVisible(i, false);

                                mapControl1.Refresh();
                                boolMapCentreSet = true;
                            }
                        }
                    }
                    if (boolMapCentreSet) break;
                }
            }
        }

        void VectorItemsLayer_Jobs_DataLoaded(object sender, DataLoadedEventArgs e)
        {
            if (this.splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                this.splashScreenManager.CloseWaitForm();
            }

            if (boolFirstLoadOfMap && !string.IsNullOrWhiteSpace(i_str_PassedInJobIDs))
            {
                boolFirstLoadOfMap = false;

                // Centre on last loaded visit object if there is one //
                double dLatitude = (double)0.0;
                double dLongitude = (double)0.0;
                GridView view = (GridView)gridControl3.MainView;
                bool boolMapCentreSet = false;
                for (int i = view.DataRowCount - 1; i >= 0; i--)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        dLatitude = Convert.ToDouble(view.GetRowCellValue(i, "StartLatitude"));
                        dLongitude = Convert.ToDouble(view.GetRowCellValue(i, "StartLongitude"));
                        if (dLatitude != (double)0.0 || dLongitude != (double)0.0)
                        {
                            int intJobID = Convert.ToInt32(view.GetRowCellValue(i, "JobID"));
                            ViewObjectOnMap(intJobID, mapObjectsLayer_Jobs, "JobID", "Job Start", false);
                            view.SelectRow(i);
                            view.MakeRowVisible(i, false);

                            mapControl1.Refresh();
                            boolMapCentreSet = true;
                        }
                    }
                    if (boolMapCentreSet) break;
                }
            }
        }

        #endregion


        #region Legend Panel

        private void bciLegend_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (bciLegend.Checked)
            {
                if (dockPanelLegend.Visibility == DevExpress.XtraBars.Docking.DockVisibility.Hidden)
                {
                    dockPanelLegend.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
                    if (dockPanelLegend.Dock == DockingStyle.Float)
                    {
                        // Important note: Forms StartPosition needs to be set to Manual in the Form's properties //
                        dockPanelLegend.FloatLocation = new System.Drawing.Point((PointToScreen(mapControl1.Location).X) + mapControl1.Width - (dockPanelLegend.Width + 9), PointToScreen(mapControl1.Location).Y + 57);
                        //dockPanelLegend.FloatLocation = new System.Drawing.Point((PointToScreen(mapControl1.Location).X) + 9, PointToScreen(mapControl1.Location).Y + 9);
                    }
                }
            }
            else
            {
                if (dockPanelLegend.Visibility != DevExpress.XtraBars.Docking.DockVisibility.Hidden) dockPanelLegend.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            }

        }

        private void dockPanelLegend_ClosingPanel(object sender, DockPanelCancelEventArgs e)
        {
            if (bciLegend.Checked) bciLegend.Checked = false;
        }

        private void dockPanelLegend_DockChanged(object sender, EventArgs e)
        {
            // Make semi transparent when undocked //
            DockPanel panel = sender as DockPanel;
            if (panel.FloatForm != null)
            {
                panel.FloatForm.AllowTransparency = true;
                panel.FloatForm.Opacity = 0.8;
            }
        }

        #endregion


        #region Layer Manager Panel

        private void bciLayerManager_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (bciLayerManager.Checked)
            {
                if (dockPanelLayerManager.Visibility == DevExpress.XtraBars.Docking.DockVisibility.Hidden)
                {
                    dockPanelLayerManager.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
                    if (dockPanelLegend.Dock == DockingStyle.Float)
                    {
                        // Important note: Forms StartPosition needs to be set to Manual in the Form's properties //
                        dockPanelLayerManager.FloatLocation = new System.Drawing.Point((PointToScreen(mapControl1.Location).X) + 1, PointToScreen(mapControl1.Location).Y + 1);
                    }
                }
            }
            else
            {
                if (dockPanelLayerManager.Visibility != DevExpress.XtraBars.Docking.DockVisibility.Hidden) dockPanelLayerManager.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            }
        }

        private void dockPanelLayerManager_ClosingPanel(object sender, DockPanelCancelEventArgs e)
        {
            if (bciLayerManager.Checked) bciLegend.Checked = false;
        }

        private void dockPanelLayerManager_DockChanged(object sender, EventArgs e)
        {
            // Make semi transparent when undocked //
            DockPanel panel = sender as DockPanel;
            if (panel.FloatForm != null)
            {
                panel.FloatForm.AllowTransparency = true;
                panel.FloatForm.Opacity = 0.8;
            }
        }

        #region GridView4

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView4;
                    if ("up".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow > view.DataRowCount - 2) return;

                        mapControl1.SuspendRender();
                        string strLayerName1 = view.GetRowCellValue(intFocusedRow, "LayerName").ToString();
                        string strLayerName2 = view.GetRowCellValue(intFocusedRow - 1, "LayerName").ToString();
                        int intLayer1ZOrder = 0;
                        int intLayer2ZOrder = 0;
                        VectorItemsLayer Layer1;
                        VectorItemsLayer Layer2;
                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "LayerOrder")) - 1);
                        view.SetRowCellValue(intFocusedRow - 1, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow - 1, "LayerOrder")) + 1);
                        view.EndSort();
                        view.EndDataUpdate();

                        switch (strLayerName1)
                        {
                            case "Sites":
                                Layer1 = mapObjectsLayer_Sites;
                                break;
                            case "Visits":
                                Layer1 = mapObjectsLayer_Visits;
                                break;
                            case "Jobs":
                                Layer1 = mapObjectsLayer_Jobs;
                                break;
                            default:
                                Layer1 = mapObjectsLayer_Sites;
                                break;
	                    }
                        intLayer1ZOrder = Layer1.ZIndex;

                        switch (strLayerName2)
                        {
                            case "Sites":
                                Layer2 = mapObjectsLayer_Sites;
                                break;
                            case "Visits":
                                Layer2 = mapObjectsLayer_Visits;
                                break;
                            case "Jobs":
                                Layer2 = mapObjectsLayer_Jobs;
                                break;
                            default:
                                Layer2 = mapObjectsLayer_Sites;
                                break;
                        }
                        intLayer2ZOrder = Layer2.ZIndex;

                        Layer1.ZIndex = intLayer2ZOrder;
                        Layer2.ZIndex = intLayer1ZOrder;
                        mapControl1.ResumeRender();
                        SetMenuStatus();
                    }
                    else if ("down".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow <= 0) return;

                        mapControl1.SuspendRender();
                        string strLayerName1 = view.GetRowCellValue(intFocusedRow, "LayerName").ToString();
                        string strLayerName2 = view.GetRowCellValue(intFocusedRow + 1, "LayerName").ToString();
                        int intLayer1ZOrder = 0;
                        int intLayer2ZOrder = 0;
                        VectorItemsLayer Layer1;
                        VectorItemsLayer Layer2;
                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "LayerOrder")) + 1);
                        view.SetRowCellValue(intFocusedRow + 1, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow + 1, "LayerOrder")) - 1);
                        view.EndSort();
                        view.EndDataUpdate();

                        switch (strLayerName1)
                        {
                            case "Sites":
                                Layer1 = mapObjectsLayer_Sites;
                                break;
                            case "Visits":
                                Layer1 = mapObjectsLayer_Visits;
                                break;
                            case "Jobs":
                                Layer1 = mapObjectsLayer_Jobs;
                                break;
                            default:
                                Layer1 = mapObjectsLayer_Sites;
                                break;
                        }
                        intLayer1ZOrder = Layer1.ZIndex;

                        switch (strLayerName2)
                        {
                            case "Sites":
                                Layer2 = mapObjectsLayer_Sites;
                                break;
                            case "Visits":
                                Layer2 = mapObjectsLayer_Visits;
                                break;
                            case "Jobs":
                                Layer2 = mapObjectsLayer_Jobs;
                                break;
                            default:
                                Layer2 = mapObjectsLayer_Sites;
                                break;
                        }
                        intLayer2ZOrder = Layer2.ZIndex;

                        Layer1.ZIndex = intLayer2ZOrder;
                        Layer2.ZIndex = intLayer1ZOrder;
                        mapControl1.ResumeRender();
                        SetMenuStatus();
                    }
                    break;
                default:
                    break;
            }
        }

        bool internalRowFocusing;
        private void gridViewVisit_GotFocus(object sender, EventArgs e)
        {
        }

        #endregion

        #endregion


        #region Data Panel

        private void dockPanelData_DockChanged(object sender, EventArgs e)
        {
            // Make semi transparent when undocked //
            DockPanel panel = sender as DockPanel;
            if (panel.FloatForm != null)
            {
                panel.FloatForm.AllowTransparency = true;
                panel.FloatForm.Opacity = 0.8;
            }
        }

        #endregion


        public override void PostViewClick(object sender, EventArgs e, int row)
        {
            GridView view = (GridView)sender;
            if (view.GridControl.MainView.Name == "gridView1")
            {
                /*if (row == GridControl.InvalidRowHandle) return;
                if (row < 0)  // Group or viewed header checkbox clicked //
                {
                    Check_Site_Status_For_Checked();
                }
                else  // Indivdual row checkbox clicked //
                {
                    if (Convert.ToInt32(view.GetRowCellValue(row, "RecordEnabled")) != 1) view.SetRowCellValue(row, "CheckMarkSelection", 0);
                }*/
                btnLoadSitesIntoMap.Enabled = Get_Valid_Selected_Site_Count_For_Map_Viewing() > 0;
                btnLoadVisits.Enabled = selection1.SelectedCount > 0;
            }
        }
        private void Check_Site_Status_For_Checked()
        {
            // Make sure any Disabled rows are de-selected //
            GridView view = (GridView)gridControl1.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "RecordEnabled")) != 1) view.SetRowCellValue(i, "CheckMarkSelection", 0);
            }
        }
        private int Get_Valid_Selected_Site_Count_For_Map_Viewing()
        {
            // Ignore any sites with no Lat \ Long //
            int intCount = 0;
            GridView view = (GridView)gridControl1.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "RecordEnabled")) == 1 && Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1) intCount++;
            }
            return intCount;
        }

        private void btnLoadSitesIntoMap_Click(object sender, EventArgs e)
        {
            int intProcessedCount = 0;
            GridView view = (GridView)gridControl1.MainView;
            if (selection1.SelectedCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more objects to render on the map before proceeding.", "Refresh Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            this.splashScreenManager.ShowWaitForm();
            this.splashScreenManager.SetWaitFormDescription("Loading Sites into Map...");

            string strRecordIDs = "";
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                {
                    intProcessedCount++;
                    if (Convert.ToInt32(view.GetRowCellValue(i, "RecordEnabled")) == 1) strRecordIDs += Convert.ToString(view.GetRowCellValue(i, "SiteID")) + ",";
                    if (intProcessedCount >= selection1.SelectedCount) break;
                }
            }

            try
            {
                mapControl1.SuspendRender();
                //mapObjectsLayer_Sites.Items.BeginUpdate();
                sp06155_OM_Visit_Mapping_Sites_Map_ObjectsTableAdapter.Fill(dataSet_OM_Visit.sp06155_OM_Visit_Mapping_Sites_Map_Objects, strRecordIDs);
                //mapObjectsLayer_Sites.Items.EndUpdate();
                mapControl1.ResumeRender();
                Set_Map_Object_Count();
            }
            catch (Exception ex)
            {
                mapControl1.ResumeRender();
                if (this.splashScreenManager.IsSplashFormVisible)
                {
                    this.splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                    this.splashScreenManager.CloseWaitForm();
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the map...\n\nError: " + ex.Message, "Load Sites into Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Close of Loading Message done from VectorItemsLayer_DataLoaded event //
        }
        private void btnClearSitesFromMap_Click(object sender, EventArgs e)
        {
            Clear_Sites_From_Map();
        }
        private void btnLoadVisits_Click(object sender, EventArgs e)
        {
            Load_Visits();
        }
        private void Load_Sites()
        {
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();
            this.RefreshGridViewState1.SaveViewInfo();
            gridControl1.BeginUpdate();
            try
            {
                sp06154_OM_Visit_Mapping_SitesTableAdapter.Fill(dataSet_OM_Visit.sp06154_OM_Visit_Mapping_Sites, i_str_PassedInSiteIDs);
                this.RefreshGridViewState1.LoadViewInfo();
            }
            catch (Exception ex)
            {
                gridControl1.EndUpdate();
                if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the data...\n\nError: " + ex.Message, "Load Site Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            gridControl1.EndUpdate();
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
            //bsiMapObjectCount.Caption = "Map Object Count: 0";

            selection1.ClearSelection();
            Clear_Sites_From_Map();

            selection2.ClearSelection();
            Clear_Visits_From_DataSet();
            Clear_Visits_From_Map();

            selection3.ClearSelection();
            Clear_Jobs_From_DataSet();
            Clear_Jobs_From_Map();
        }

        private void btnLoadVisitsIntoMap_Click(object sender, EventArgs e)
        {
            int intProcessedCount = 0;
            GridView view = (GridView)gridControl2.MainView;
            if (selection2.SelectedCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more visits to render on the map before proceeding.", "Refresh Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            this.splashScreenManager.ShowWaitForm();
            this.splashScreenManager.SetWaitFormDescription("Loading Visits into Map...");

            string strRecordIDs = "";
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                {
                    intProcessedCount++;
                    strRecordIDs += Convert.ToString(view.GetRowCellValue(i, "VisitID")) + ",";
                    if (intProcessedCount >= selection2.SelectedCount) break;
                }
            }

            try
            {
                mapControl1.SuspendRender();
                //mapObjectsLayer_Visits.Items.BeginUpdate();
                sp06153_OM_Mapping_Visits_Map_ObjectsTableAdapter.Fill(dataSet_OM_Visit.sp06153_OM_Mapping_Visits_Map_Objects, strRecordIDs);
                //mapObjectsLayer_Visits.Items.EndUpdate();
                mapControl1.ResumeRender();
                Set_Map_Object_Count();
            }
            catch (Exception ex)
            {
                mapControl1.ResumeRender();
                if (this.splashScreenManager.IsSplashFormVisible)
                {
                    this.splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                    this.splashScreenManager.CloseWaitForm();
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the map...\n\nError: " + ex.Message, "Load Visits into Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Close of Loading Message done from VectorItemsLayer_DataLoaded event //
        }
        private void btnClearVisitsFromMap_Click(object sender, EventArgs e)
        {
            Clear_Visits_From_Map();
        }
        private void btnLoadJobs_Click(object sender, EventArgs e)
        {
            Load_Jobs();
        }
        private void Load_Visits()
        {
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();
            this.RefreshGridViewState2.SaveViewInfo();
            string strSelectedIDs = "";
            GridView view = (GridView)gridControl1.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                {
                    strSelectedIDs += view.GetRowCellValue(i, view.Columns["SiteID"]).ToString() + ',';
                }
            }
            gridControl2.BeginUpdate();
            try
            {
                sp06152_OM_Mapping_VisitsTableAdapter.Fill(dataSet_OM_Visit.sp06152_OM_Mapping_Visits, strSelectedIDs);
                this.RefreshGridViewState2.LoadViewInfo();
            }
            catch (Exception ex)
            {
                gridControl2.EndUpdate();
                if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the data...\n\nError: " + ex.Message, "Load Visit Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            gridControl2.EndUpdate();
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
            view = (GridView)gridControl2.MainView;
            view.ExpandAllGroups();
            //selection2.SelectAll();
            Clear_Visits_From_Map();

            selection3.ClearSelection();
            Clear_Jobs_From_DataSet();
            Clear_Jobs_From_Map();
        }

        private void btnLoadJobsIntoMap_Click(object sender, EventArgs e)
        {
            int intProcessedCount = 0;
            GridView view = (GridView)gridControl3.MainView;
            if (selection3.SelectedCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more jobs to render on the map before proceeding.", "Refresh Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            this.splashScreenManager.ShowWaitForm();
            this.splashScreenManager.SetWaitFormDescription("Loading Jobs into Map...");

            string strRecordIDs = "";
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                {
                    intProcessedCount++;
                    strRecordIDs += Convert.ToString(view.GetRowCellValue(i, "JobID")) + ",";
                    if (intProcessedCount >= selection3.SelectedCount) break;
                }
            }

            try
            {
                mapControl1.SuspendRender();
                //mapObjectsLayer_Jobs.Items.BeginUpdate();
                sp06277_OM_Mapping_Jobs_Map_ObjectsTableAdapter.Fill(dataSet_OM_Job.sp06277_OM_Mapping_Jobs_Map_Objects, strRecordIDs);
                //mapObjectsLayer_Jobs.Items.EndUpdate();
                mapControl1.ResumeRender();
                Set_Map_Object_Count();
            }
            catch (Exception ex)
            {
                mapControl1.ResumeRender();
                if (this.splashScreenManager.IsSplashFormVisible)
                {
                    this.splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                    this.splashScreenManager.CloseWaitForm();
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the map...\n\nError: " + ex.Message, "Load Jobs into Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Close of Loading Message done from VectorItemsLayer_DataLoaded event //
        }
        private void btnClearJobsFromMap_Click(object sender, EventArgs e)
        {
            Clear_Jobs_From_Map();
        }
        private void Load_Jobs()
        {
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();
            this.RefreshGridViewState3.SaveViewInfo();
            string strSelectedIDs = "";
            GridView view = (GridView)gridControl2.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                {
                    strSelectedIDs += view.GetRowCellValue(i, view.Columns["VisitID"]).ToString() + ',';
                }
            }
            gridControl3.BeginUpdate();
            try
            {
                sp06276_OM_Mapping_JobsTableAdapter.Fill(dataSet_OM_Job.sp06276_OM_Mapping_Jobs, strSelectedIDs);
                this.RefreshGridViewState3.LoadViewInfo();
            }
            catch (Exception ex)
            {
                gridControl3.EndUpdate();
                if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the data...\n\nError: " + ex.Message, "Load Job Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            gridControl3.EndUpdate();
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
            view = (GridView)gridControl3.MainView;
            view.ExpandAllGroups();
            //selection3.SelectAll();
            Clear_Jobs_From_Map();
        }



        private void Clear_Sites_From_Map()
        {
            try
            {
                mapControl1.SuspendRender();
                //mapObjectsLayer_Sites.Items.BeginUpdate();
                dataSet_OM_Visit.sp06155_OM_Visit_Mapping_Sites_Map_Objects.Clear();
                //mapObjectsLayer_Sites.Items.EndUpdate();
                mapControl1.ResumeRender();
                Set_Map_Object_Count();
            }
            catch (Exception ex)
            {
                mapControl1.ResumeRender();
                if (this.splashScreenManager.IsSplashFormVisible)
                {
                    this.splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                    this.splashScreenManager.CloseWaitForm();
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while clearing the map...\n\nError: " + ex.Message, "Clear Sites from Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        private void Clear_Visits_From_DataSet()
        {
            GridView view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            try
            {
                dataSet_OM_Visit.sp06152_OM_Mapping_Visits.Clear();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while clearing the visits data...\n\nError: " + ex.Message, "Clear Visits from Grid", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            view.EndUpdate();
        }
        private void Clear_Visits_From_Map()
        {
            try
            {
                mapControl1.SuspendRender();
                //mapObjectsLayer_Visits.Items.BeginUpdate();
                dataSet_OM_Visit.sp06153_OM_Mapping_Visits_Map_Objects.Clear();
                //mapObjectsLayer_Visits.Items.EndUpdate();
                mapControl1.ResumeRender();
                Set_Map_Object_Count();
            }
            catch (Exception ex)
            {
                mapControl1.ResumeRender();
                if (this.splashScreenManager.IsSplashFormVisible)
                {
                    this.splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                    this.splashScreenManager.CloseWaitForm();
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while clearing the map...\n\nError: " + ex.Message, "Clear Visits from Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }
        
        private void Clear_Jobs_From_Map()
        {
            try
            {
                mapControl1.SuspendRender();
                //mapObjectsLayer_Jobs.Items.BeginUpdate();
                dataSet_OM_Job.sp06277_OM_Mapping_Jobs_Map_Objects.Clear();
                //mapObjectsLayer_Jobs.Items.EndUpdate();
                mapControl1.ResumeRender();
                Set_Map_Object_Count();
            }
            catch (Exception ex)
            {
                mapControl1.ResumeRender();
                if (this.splashScreenManager.IsSplashFormVisible)
                {
                    this.splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                    this.splashScreenManager.CloseWaitForm();
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while clearing the map...\n\nError: " + ex.Message, "Clear Jobs from Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }
        private void Clear_Jobs_From_DataSet()
        {
            GridView view = (GridView)gridControl3.MainView;
            view.BeginUpdate();
            try
            {
                dataSet_OM_Job.sp06276_OM_Mapping_Jobs.Clear();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while clearing the jobs data...\n\nError: " + ex.Message, "Clear Jobs from Grid", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            view.EndUpdate();
        }

       
        private void Set_Map_Object_Count()
        {
            //bsiMapObjectCount.Caption = "Map Object Count: " + Convert.ToString(dataSet_OM_Visit.sp06153_OM_Mapping_Visits_Map_Objects.Rows.Count + dataSet_OM_Visit.sp06155_OM_Visit_Mapping_Sites_Map_Objects.Rows.Count);
            int intSites = dataSet_OM_Visit.sp06155_OM_Visit_Mapping_Sites_Map_Objects.Rows.Count;
            int intVisits = dataSet_OM_Visit.sp06153_OM_Mapping_Visits_Map_Objects.Rows.Count;
            int intJobs = dataSet_OM_Job.sp06277_OM_Mapping_Jobs_Map_Objects.Rows.Count;

            string strMessage = (intSites > 0 ? " Sites: " + intSites.ToString() : "");

            if (!string.IsNullOrWhiteSpace(strMessage) && intVisits > 0) strMessage += ",";
            if (intVisits > 0) strMessage += " Visits: " + intVisits.ToString();

            if (!string.IsNullOrWhiteSpace(strMessage) && intJobs > 0) strMessage += ",";
            if (intJobs > 0) strMessage += " Jobs: " + intJobs.ToString();

            bsiMapObjectCount.Caption = "Map Object Count: " + (string.IsNullOrWhiteSpace(strMessage) ? "0" : strMessage);
        }

        private void bbiExportSitesToExcel_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportGridToExcel();
        }
        private void bbiExportVisitsToExcel_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportGridToExcel();
        }
        private void bbiExportJobsToExcel_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportGridToExcel();
        }
        private void ExportGridToExcel()
        {
            SaveFileDialog sfd = new SaveFileDialog();
            string strFilename = "Export_" + String.Format("{0:yyyy-MM-dd_mm-ss}", DateTime.Now);
            sfd.FileName = strFilename;
            sfd.Filter = "Xlsx-Files (*.xlsx)|*.xlsx";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    switch (_enmFocusedGrid)
                    {
                        case Utils.enmFocusedGrid.Sites:
                            {
                                gridControl1.ExportToXlsx(sfd.FileName);
                            }
                            break;
                        case Utils.enmFocusedGrid.Visit:
                            {
                                gridControl2.ExportToXlsx(sfd.FileName);
                            }
                            break;
                        case Utils.enmFocusedGrid.Jobs:
                            {
                                gridControl3.ExportToXlsx(sfd.FileName);
                            }
                            break;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("Data Exported Sucessfully", "Export Data", MessageBoxButtons.OK);
                    System.Diagnostics.Process.Start(sfd.FileName);
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("There was a problem exporting Data. Please try again.If the problem persists, please contact Technical Support.", "Export Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    throw;
                }
            }
        }

        private void bbiViewSiteOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select only one Site to Zoom the map to before proceeding.", "View Site On Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            ViewObjectOnMap(Convert.ToInt32(view.GetFocusedRowCellValue("SiteID")), mapObjectsLayer_Sites, "SiteID", "Site", true);
        }
        private void bbiViewVisitStartOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select only one Visit to Zoom the map to before proceeding.", "View Visit On Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            ViewObjectOnMap(Convert.ToInt32(view.GetFocusedRowCellValue("VisitID")), mapObjectsLayer_Visits, "VisitID", "Visit Start", true);
        }
        private void bbiViewVisitFinishOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select only one Visit to Zoom the map to before proceeding.", "View Visit Finish On Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            //ViewObjectOnMap(90000000 + Convert.ToInt32(view.GetFocusedRowCellValue("VisitID")), mapObjectsLayer_Visits, "VisitID", "Visit Finish");
            ViewObjectOnMap(Convert.ToInt32(view.GetFocusedRowCellValue("VisitID")), mapObjectsLayer_Visits, "VisitID", "Visit Finish", true);
        }
        private void bbiViewJobStartOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select only one Job to Zoom the map to before proceeding.", "View Job Start On Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            ViewObjectOnMap(Convert.ToInt32(view.GetFocusedRowCellValue("JobID")), mapObjectsLayer_Jobs, "JobID", "Job Start", true);
        }
        private void bbiViewJobFinishOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select only one Job to Zoom the map to before proceeding.", "View Job Finish On Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            ViewObjectOnMap(90000000 + Convert.ToInt32(view.GetFocusedRowCellValue("JobID")), mapObjectsLayer_Jobs, "JobID", "Job Finish", true);
        }
        private void ViewObjectOnMap(int RecordID, VectorItemsLayer pointLayer, string ColumnName, string ColumnDescriptor, bool ShowLocusEffect)
        {
            double Lat = 0;
            double Long = 0;
            intRecordIDToZoomTo = RecordID;
            intPointsToZoomToCount = 0;

            pointLayer.SelectedItems.Clear();
            if (ColumnDescriptor == "Visit Start")
            {
                for (int i = 0; i < pointLayer.Data.Count; i++)
                {
                    if (Convert.ToInt32(pointLayer.Data.GetItem(i).Attributes[ColumnName].Value) == intRecordIDToZoomTo && pointLayer.Data.GetItem(i).Attributes["RecordType"].Value.ToString().Contains("Start"))
                    {
                        pointLayer.SelectedItem = pointLayer.Data.GetItem(i);
                        Lat = ((GeoPoint)((MapCustomElement)pointLayer.Data.GetItem(i)).Location).Latitude;
                        Long = ((GeoPoint)((MapCustomElement)pointLayer.Data.GetItem(i)).Location).Longitude;
                        intPointsToZoomToCount++;
                        break;
                    }
                }
            }
            else if (ColumnDescriptor == "Visit Finish")
            {
                for (int i = 0; i < pointLayer.Data.Count; i++)
                {
                    if (Convert.ToInt32(pointLayer.Data.GetItem(i).Attributes[ColumnName].Value) == intRecordIDToZoomTo && pointLayer.Data.GetItem(i).Attributes["RecordType"].Value.ToString().Contains("End"))
                    {
                        pointLayer.SelectedItem = pointLayer.Data.GetItem(i);
                        Lat = ((GeoPoint)((MapCustomElement)pointLayer.Data.GetItem(i)).Location).Latitude;
                        Long = ((GeoPoint)((MapCustomElement)pointLayer.Data.GetItem(i)).Location).Longitude;
                        intPointsToZoomToCount++;
                        break;
                    }
                }
            }
            else if (ColumnDescriptor == "Job Start")
            {
                for (int i = 0; i < pointLayer.Data.Count; i++)
                {
                    if (Convert.ToInt32(pointLayer.Data.GetItem(i).Attributes[ColumnName].Value) == intRecordIDToZoomTo && pointLayer.Data.GetItem(i).Attributes["ObjectType"].Value.ToString().Contains("Start"))
                    {
                        pointLayer.SelectedItem = pointLayer.Data.GetItem(i);
                        Lat = ((GeoPoint)((MapCustomElement)pointLayer.Data.GetItem(i)).Location).Latitude;
                        Long = ((GeoPoint)((MapCustomElement)pointLayer.Data.GetItem(i)).Location).Longitude;
                        intPointsToZoomToCount++;
                        break;
                    }
                }
            }
            else if (ColumnDescriptor == "Job Finish")
            {
                for (int i = 0; i < pointLayer.Data.Count; i++)
                {
                    if (Convert.ToInt32(pointLayer.Data.GetItem(i).Attributes[ColumnName].Value) == intRecordIDToZoomTo && pointLayer.Data.GetItem(i).Attributes["ObjectType"].Value.ToString().Contains("End"))
                    {
                        pointLayer.SelectedItem = pointLayer.Data.GetItem(i);
                        Lat = ((GeoPoint)((MapCustomElement)pointLayer.Data.GetItem(i)).Location).Latitude;
                        Long = ((GeoPoint)((MapCustomElement)pointLayer.Data.GetItem(i)).Location).Longitude;
                        intPointsToZoomToCount++;
                        break;
                    }
                }
            }
            else if (pointLayer.Data.Count > 0)
            {
                for (int i = 0; i < pointLayer.Data.Count; i++)
                {
                    if (Convert.ToInt32(pointLayer.Data.GetItem(i).Attributes[ColumnName].Value) == intRecordIDToZoomTo)
                    {
                        //Lat = Lat + ((GeoPoint)((MapCustomElement)pointLayer.Data.GetItem(i)).Location).Latitude;
                        //Long = Long + ((GeoPoint)((MapCustomElement)pointLayer.Data.GetItem(i)).Location).Longitude;
                        pointLayer.SelectedItem = pointLayer.Data.GetItem(i);
                        Lat = ((GeoPoint)((MapCustomElement)pointLayer.Data.GetItem(i)).Location).Latitude;
                        Long = ((GeoPoint)((MapCustomElement)pointLayer.Data.GetItem(i)).Location).Longitude;
                        intPointsToZoomToCount++;
                        break;
                    }
                }
            }

            if (intPointsToZoomToCount <= 0)
            {
                string strMessage = "";
                switch (ColumnDescriptor)
                {
                    case "Site":
                        {
                            strMessage = "Unable to find selected Site. Click the Load Sites into Map button before trying again.";
                        }
                        break;
                    case "Visit Start":
                        {
                            strMessage = "Unable to find selected Visit Start. Click the Load Visits into Map button before trying again.";
                        }
                        break;
                    case "Visit Finish":
                        {
                            strMessage = "Unable to find selected Visit Finish. Click the Load Visits into Map button before trying again.\nNote: The Visit may not have Finish Location stored against it.";
                        }
                        break;
                    case "Job Start":
                        {
                            strMessage = "Unable to find selected Job Start. Click the Load Jobs into Map button before trying again.";
                        }
                        break;
                    case "Job Finish":
                        {
                            strMessage = "Unable to find selected Job Finish. Click the Load Jobs into Map button before trying again.\nNote: The Job may not have Finish Location stored against it.";
                        }
                        break;
                    default:
                        {
                            strMessage = "Unable to find selected Map Object. Click the approriate Load Data into Map button before trying again.";
                        }
                        break;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "View Object On Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                GeoPoint centerPoint = new GeoPoint(Lat, Long);

                // Do both items in one step //
                BeginInvoke(new MethodInvoker(() =>
                {
                    mapControl1.ZoomLevel = 16;
                    mapControl1.CenterPoint = centerPoint;
                }));

                mapControl1.Refresh();
                Point location = mapControl1.PointToScreen(Point.Empty);

                //MapPoint mp = pointLayer.GeoToScreenPoint(mapControl1.CenterPoint);
                MapPoint mp = mapControl1.CoordPointToScreenPoint(mapControl1.CenterPoint);
                if (ShowLocusEffect)
                {
                    System.Drawing.Point screenPoint = new Point(location.X + Convert.ToInt32(mp.X), location.Y + Convert.ToInt32(mp.Y));
                    locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customBeaconLocusEffect1.Name);
                }
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An Error occurred while attempting to zoom to the selected coordinates.\n\nError: " + ex.Message, "View Object On Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            } 
        }

        private void bciMiniMap_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            mapControl1.MiniMap.Visible = bciMiniMap.Checked;
        }

        private void bciNavigation_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            mapControl1.NavigationPanelOptions.Visible = bciNavigation.Checked;
        }



 
 


    }
}

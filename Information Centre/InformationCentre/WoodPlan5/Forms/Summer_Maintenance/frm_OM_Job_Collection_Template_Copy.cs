﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;
using DevExpress.XtraEditors;
using DevExpress.Utils;

namespace WoodPlan5
{
    public partial class frm_OM_Job_Collection_Template_Copy : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string _Description = "";
        public string _ClientName = "";
        public int _ClientID = 0;
        public int _OrderID = 0;
        public string _Remarks = "";

        public bool _TemplateItems = true;
        public bool _VisitCategories = true;
        public bool _ItemRules = true;
        public bool _VisitNumbers = true;
 
        #endregion

        public frm_OM_Job_Collection_Template_Copy()
        {
            InitializeComponent();
        }

        private void frm_OM_Job_Collection_Template_Copy_Load(object sender, EventArgs e)
        {
            this.FormID = 500247;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //

            strConnectionString = this.GlobalSettings.ConnectionString;

            DescriptionTextEdit.EditValue = _Description;
            ClientNameButtonEdit.EditValue = _ClientName;
            ClientIDTextEdit.EditValue = _ClientID;
            RecordOrderSpinEdit.EditValue = _OrderID;
            MemoEditRemarks.EditValue = _Remarks;

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

      
        private void btnOK_Click(object sender, EventArgs e)
        {
            _Description = DescriptionTextEdit.EditValue.ToString();
            _ClientName = ClientNameButtonEdit.EditValue.ToString();
            _ClientID = Convert.ToInt32(ClientIDTextEdit.EditValue);
            _OrderID = Convert.ToInt32(RecordOrderSpinEdit.EditValue);
            _Remarks = MemoEditRemarks.EditValue.ToString();
            
            if (string.IsNullOrWhiteSpace(_Description))
            {
                XtraMessageBox.Show("Enter a Description for the new Job Collection Template.", "Copy Job Collection Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            _TemplateItems = checkEditTemplateItems.Checked;
            _VisitCategories = checkEditVisitCategories.Checked;
            _ItemRules = checkEditItemRules.Checked;
            _VisitNumbers = checkEditVisitNumbers.Checked;

            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }


        #region Editors

       private void ClientNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")
            {
                int intClientID = 0;
                try
                {
                    intClientID = (string.IsNullOrEmpty(ClientIDTextEdit.EditValue.ToString()) ? 0 : Convert.ToInt32(ClientIDTextEdit.EditValue));
                }
                catch (Exception) { }
                var fChildForm = new frm_EP_Select_Client();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalClientID = intClientID;
                fChildForm.intFilterSummerMaintenanceClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    ClientIDTextEdit.EditValue = fChildForm.intSelectedClientID;
                    ClientNameButtonEdit.EditValue = fChildForm.strSelectedClientName;
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                if (XtraMessageBox.Show("Warning... You are about to clear the selected Client.\n\nIf you proceed this record will not be client specific so it can be applied to any client.\n\nProceed?", "Clear Client", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                {
                    ClientIDTextEdit.EditValue = 0;
                    ClientNameButtonEdit.EditValue = "Not Client Specific";
                }
            }
        }

        private void checkEditTemplateItems_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                checkEditVisitCategories.Properties.ReadOnly = false;
                checkEditItemRules.Properties.ReadOnly = false;
                checkEditVisitNumbers.Properties.ReadOnly = !checkEditItemRules.Checked;
            }
            else
            {
                checkEditVisitCategories.Properties.ReadOnly = true;
                checkEditVisitCategories.Checked = false;

                checkEditItemRules.Properties.ReadOnly = true;
                checkEditItemRules.Checked = false;

                checkEditVisitNumbers.Properties.ReadOnly = true;
                checkEditVisitNumbers.Checked = false;
            }
        }

        private void checkEditItemRules_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                checkEditVisitNumbers.Properties.ReadOnly = false;
            }
            else
            {
                checkEditVisitNumbers.Properties.ReadOnly = true;
                checkEditVisitNumbers.Checked = false;
            }
        }

        #endregion

 









    }
}

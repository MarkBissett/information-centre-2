﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Site_Contract_On_Hold_Wizard_Block_Edit_On_Hold : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public DateTime? dtOnHoldStartDate = null;
        public DateTime? dtOnHoldEndDate = null;
        public int? intOnHoldReasonID = null;
        public string strRemarks = null;

        #endregion

        public frm_OM_Site_Contract_On_Hold_Wizard_Block_Edit_On_Hold()
        {
            InitializeComponent();
        }

        private void frm_OM_Site_Contract_On_Hold_Wizard_Block_Edit_On_Hold_Load(object sender, EventArgs e)
        {
            this.FormID = 405;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //
            
            strConnectionString = this.GlobalSettings.ConnectionString;

            try
            {
                sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter.Fill(dataSet_OM_Contract.sp06406_OM_Site_Contract_On_Hold_Reasons_With_Blank);
            }
            catch (Exception) { }

            DateEditOnHoldStartDate.EditValue = null;
            dateEditOnHoldEndDate.EditValue = null;
            gridLookUpEditOnHoldReasonID.EditValue = null;
            memoEditRemarks.EditValue = null;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }
       
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (DateEditOnHoldStartDate.EditValue != null) dtOnHoldStartDate = Convert.ToDateTime(DateEditOnHoldStartDate.EditValue);
            if (dateEditOnHoldEndDate.EditValue != null) dtOnHoldEndDate = Convert.ToDateTime(dateEditOnHoldEndDate.EditValue);
            if (gridLookUpEditOnHoldReasonID.EditValue != null) intOnHoldReasonID = Convert.ToInt32(gridLookUpEditOnHoldReasonID.EditValue);
            if (memoEditRemarks.EditValue != null) strRemarks = memoEditRemarks.EditValue.ToString();
            
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }






    }
}

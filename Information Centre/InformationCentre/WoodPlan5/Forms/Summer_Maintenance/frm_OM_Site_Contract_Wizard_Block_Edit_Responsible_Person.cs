﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Site_Contract_Wizard_Block_Edit_Responsible_Person : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string strResponsibilityTypesToLoad = "";
        public string strStaffName = null;
        public int? intResponsibilityTypeID = null;
        public string strResponsibilityType = null;
        public string strRemarks = null;
        public int? intPersonTypeID = null;
        public string strPersonTypeDescription = null;

        public int? intStaffID = null;
        #endregion

        public frm_OM_Site_Contract_Wizard_Block_Edit_Responsible_Person()
        {
            InitializeComponent();
        }

        private void frm_OM_Site_Contract_Wizard_Block_Edit_Responsible_Person_Load(object sender, EventArgs e)
        {
            this.FormID = 500141;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //
            
            StaffIDTextEdit.EditValue = null;
            StaffNameButtonEdit.EditValue = null;
            ResponsibilityTypeIDTextEdit.EditValue = null;
            ResponsibilityTypeButtonEdit.EditValue = null;
            RemarksMemoEdit.EditValue = null;
            PersonTypeIDTextEdit.EditValue = null;
            PersonTypeDescriptionTextEdit.EditValue = null;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

        private void StaffNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm1 = new frm_OM_Client_Contract_Person_Responsibility_Select_Person_Type();
                fChildForm1.GlobalSettings = this.GlobalSettings;
                fChildForm1.ShowDialog();
                int intPersonTypeID = fChildForm1._SelectedTypeID;
                if (intPersonTypeID == -1) return;
                string strPersonType = fChildForm1._SelectedTypeDescription;

                if (intPersonTypeID == 0)  // Staff //
                {
                    var fChildForm = new frm_HR_Select_Staff();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.intOriginalStaffID = 0;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        StaffIDTextEdit.EditValue = fChildForm.intSelectedStaffID;
                        StaffNameButtonEdit.EditValue = fChildForm.strSelectedStaffName;
                        PersonTypeIDTextEdit.EditValue = 0;  // 0 = Staff, 1 = Client //
                        PersonTypeDescriptionTextEdit.EditValue = "Staff";
                        ResponsibilityTypeButtonEdit.EditValue = "";
                        ResponsibilityTypeIDTextEdit.EditValue = 0;
                    }
                }
                else  // Client //
                {
                    var fChildForm = new frm_Core_Select_Client_Contact_Person();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.intOriginalChildSelectedID = 0;
                    fChildForm.intFilterSummerMaintenanceClient = 1;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        StaffIDTextEdit.EditValue = fChildForm.intSelectedChildID;
                        StaffNameButtonEdit.EditValue = fChildForm.strSelectedChildDescription;
                        PersonTypeIDTextEdit.EditValue = 1;  // 0 = Staff, 1 = Client //
                        PersonTypeDescriptionTextEdit.EditValue = "Client";
                        ResponsibilityTypeButtonEdit.EditValue = "";
                        ResponsibilityTypeIDTextEdit.EditValue = 0;
                    }
                }

            }
        }

        private void ResponsibilityTypeIDButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString().ToLower() == "choose")  // Choose Button //
            {
                int ResponsibilityTypeID = 0;
                int PersonTypeID = -1;
                try
                {
                    if (PersonTypeIDTextEdit.EditValue != null) PersonTypeID = Convert.ToInt32(PersonTypeIDTextEdit.EditValue);
                }
                catch (Exception) { }
                if (PersonTypeID < 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select the Person beofre attempting to set the Responsibility Type", "Select Responsibility Type", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                var fChildForm = new frm_OM_Select_Person_Responsibility_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalID = ResponsibilityTypeID;
                fChildForm.strPassedInResponsibilityTypeIDs = strResponsibilityTypesToLoad;
                fChildForm.intPassedInPersonTypeID = PersonTypeID;
                fChildForm.intIncludeBlankRow = 0;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (ResponsibilityTypeID != fChildForm.intSelectedID)
                    {
                        ResponsibilityTypeIDTextEdit.EditValue = fChildForm.intSelectedID;
                        ResponsibilityTypeButtonEdit.EditValue = fChildForm.strSelectedDescription;
                    }
                }
            }
        }
       
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (StaffIDTextEdit.EditValue != null) intStaffID = Convert.ToInt32(StaffIDTextEdit.EditValue);
            if (StaffNameButtonEdit.EditValue != null) strStaffName = StaffNameButtonEdit.EditValue.ToString();
            if (ResponsibilityTypeIDTextEdit.EditValue != null) intResponsibilityTypeID = Convert.ToInt32(ResponsibilityTypeIDTextEdit.EditValue);
            if (ResponsibilityTypeButtonEdit.EditValue != null) strResponsibilityType = ResponsibilityTypeButtonEdit.EditValue.ToString();
            if (PersonTypeIDTextEdit.EditValue != null) intPersonTypeID = Convert.ToInt32(PersonTypeIDTextEdit.EditValue);
            if (PersonTypeDescriptionTextEdit.EditValue != null) strPersonTypeDescription = PersonTypeDescriptionTextEdit.EditValue.ToString();
            if (RemarksMemoEdit.EditValue != null) strRemarks = RemarksMemoEdit.EditValue.ToString();
              
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }





 





    }
}

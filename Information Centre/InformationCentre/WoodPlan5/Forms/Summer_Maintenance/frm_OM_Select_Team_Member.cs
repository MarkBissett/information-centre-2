using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Select_Team_Member : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string _Mode = "single";  // single or multiple //
        public int intOperationsTeamsOnly = 0;  // Default to No //
        public string _PassedInTeamFilter = "";
        public int _PassedInLabourType = 1;  // 0 = staff, 1 = contractor //

        public string strOriginalParentIDs = "";
        public int intOriginalParentID = 0;
        public string strOriginalChildIDs = "";
        public int intOriginalChildID = 0;

        public string strSelectedParentIDs = "";
        public int intSelectedParentID = 0;
        public string strSelectedChildIDs = "";
        public int intSelectedChildID = 0;
        public string strSelectedParentDescriptions = "";
        public string strSelectedChildDescriptions = "";
        public string strFullDescription = "";
        public int intSelectedLabourTypeID = 0;

        GridHitInfo downHitInfo = null;
        bool internalRowFocusing;

        public int _SelectedCount1 = 0;
        public int _SelectedCount2 = 0;
        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;

        #endregion

        public frm_OM_Select_Team_Member()
        {
            InitializeComponent();
        }

        private void frm_OM_Select_Team_Member_Load(object sender, EventArgs e)
        {
            this.FormID = 500219;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp06282_OM_Select_OM_TeamTableAdapter.Connection.ConnectionString = strConnectionString;
            
            sp06283_OM_Select_OM_Team_Members_For_TeamTableAdapter.Connection.ConnectionString = strConnectionString;

            GridView view = (GridView)gridControl1.MainView;
            gridControl1.ForceInitialize();
            LoadData();

            if (_Mode != "single")
            {
                // Add record selection checkboxes to popup grid control //
                selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
                selection1.CheckMarkColumn.VisibleIndex = 0;
                selection1.CheckMarkColumn.Width = 30;

                Array arrayRecords = strOriginalParentIDs.Split(',');  // Single quotes because char expected for delimeter //
                view.BeginUpdate();
                int intFoundRow = 0;
                foreach (string strElement in arrayRecords)
                {
                    if (strElement == "") break;
                    intFoundRow = view.LocateByValue(0, view.Columns["TeamID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
                view.EndUpdate();
            }
            else  // Single selection mode //
            {
                if (intOriginalParentID != 0)  // Record selected so try to find and highlight //
                {
                    int intFoundRow = view.LocateByValue(0, view.Columns["TeamID"], intOriginalParentID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intFoundRow;
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
            }

            view = (GridView)gridControl2.MainView;
            gridControl2.ForceInitialize();
            LoadLinkedData();

            if (_Mode != "single")
            {
                // Add record selection checkboxes to popup grid control //
                selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
                selection2.CheckMarkColumn.VisibleIndex = 0;
                selection2.CheckMarkColumn.Width = 30;


                Array arrayRecords = strOriginalChildIDs.Split(',');  // Single quotes because char expected for delimeter //
                view.BeginUpdate();
                int intFoundRow = 0;
                foreach (string strElement in arrayRecords)
                {
                    if (strElement == "") break;
                    intFoundRow = view.LocateByValue(0, view.Columns["TeamMemberID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
                view.EndUpdate();
            }
            else  // Single selection mode //
            {
                if (intOriginalChildID != 0)  // Record selected so try to find and highlight //
                {
                    int intFoundRow = view.LocateByValue(0, view.Columns["TeamMemberID"], intOriginalChildID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intFoundRow;
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
            }

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp06282_OM_Select_OM_TeamTableAdapter.Fill(dataSet_OM_Job.sp06282_OM_Select_OM_Team, intOperationsTeamsOnly, _PassedInTeamFilter, _PassedInLabourType);
            view.EndUpdate();
        }

        private void LoadLinkedData()
        {
            GridView view = (GridView)gridControl1.MainView;
            string strSelectedIDs = "";    // Reset any prior values first //
            int intCount = 0;
            if (_Mode != "single")
            {
                if (selection1.SelectedCount <= 0) return;
                intCount = selection1.SelectedCount;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSelectedIDs += Convert.ToString(view.GetRowCellValue(i, "TeamID")) + ",";
                    }
                }
            }
            else
            {
                int[] intRowHandles = view.GetSelectedRows();
                intCount = intRowHandles.Length;
                foreach (int intRowHandle in intRowHandles)
                {
                    strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TeamID"])) + ',';
                }
            }

            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_OM_Job.sp06283_OM_Select_OM_Team_Members_For_Team.Clear();
            }
            else
            {
                try
                {
                    sp06283_OM_Select_OM_Team_Members_For_TeamTableAdapter.Fill(dataSet_OM_Job.sp06283_OM_Select_OM_Team_Members_For_Team, strSelectedIDs, _PassedInLabourType);
                }
                catch (Exception Ex)
                {
                    XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related Job Sub-Types.\n\nTry selecting one or more Job Types again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            view = (GridView)gridControl2.MainView;
            view.ExpandAllGroups();
            gridControl2.MainView.EndUpdate();
        }

        public override void PostViewClick(object sender, EventArgs e, int row)
        {
            GridView view = (GridView)sender;
            switch (view.GridControl.MainView.Name)
            {
                case "gridView1":
                    {
                        if (row == GridControl.InvalidRowHandle || _Mode == "single") return;
                        LoadLinkedData();
                    }
                    break;
                default:
                    break;
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Teams Available";
                    break;
                case "gridView2":
                    message = (_Mode != "single" ? "No Team Members Available - Tick one or more Teams to view linked Team Members" : "No Team Members Available - Select a Team by clicking on it to view linked Team Members");
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    if (_Mode == "single") LoadLinkedData();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (string.IsNullOrEmpty(strSelectedChildDescriptions))
            {
                if (_Mode != "single")
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Team Members by ticking them before proceeding.", "Select Team Members", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select a Team Members by clicking on it before proceeding.", "Select Team Members", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            strSelectedParentIDs = "";    // Reset any prior values first //
            strSelectedParentDescriptions = "";  // Reset any prior values first //

            GridView view = (GridView)gridControl2.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            GridView view1 = (GridView)gridControl1.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            if (_Mode != "single")
            {
                if (selection2.SelectedCount <= 0) return;
                strSelectedParentDescriptions = ",";
                int intCount = 0;
                string strTempParentID = "";
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSelectedChildIDs += Convert.ToString(view.GetRowCellValue(i, "TeamMemberID")) + ",";
                        if (intCount == 0)
                        {
                            strSelectedChildDescriptions = Convert.ToString(view.GetRowCellValue(i, "TeamMemberName"));
                        }
                        else if (intCount >= 1)
                        {
                            strSelectedChildDescriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "TeamMemberName"));
                        }
                        strTempParentID = Convert.ToString(view.GetRowCellValue(i, "TeamID"));
                        if (!strSelectedParentIDs.Contains("," + strTempParentID + ","))
                        {
                            strSelectedParentIDs += strTempParentID + ",";
                            strSelectedParentDescriptions += Convert.ToString(view.GetRowCellValue(i, "TeamName")) + ",";
                        }

                        intCount++;
                    }
                }
                strSelectedParentIDs = strSelectedParentIDs.Remove(0, 1);  // Remove preceeding comma //
            }
            else  // Single Selection //
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                {
                    intSelectedChildID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "TeamMemberID"));
                    strSelectedChildDescriptions = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "TeamMemberName"))) ? "Unknown Team Member" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "TeamMemberName")));
                    intSelectedParentID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "TeamID"));
                    strSelectedParentDescriptions = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "TeamName"))) ? "Unknown Team" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "TeamName")));
                    strFullDescription = strSelectedChildDescriptions + " - " + strSelectedParentDescriptions;
                    intSelectedLabourTypeID = Convert.ToInt32(view1.GetRowCellValue(view1.FocusedRowHandle, "LabourTypeID"));
                }
            }
        }






    }
}


namespace WoodPlan5
{
    partial class frm_OM_Site_Contract_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Site_Contract_Manager));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression1 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colOnHold = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridControlContract = new DevExpress.XtraGrid.GridControl();
            this.sp06088OMSiteContractManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridViewContract = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colYearlyPercentageIncrease = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLastClientPaymentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDirectorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckRPIDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDirector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSectorType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedVisitCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colLinkedDocumentCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContract = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colLinkedToParent3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSiteInstructions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedEquipmentCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedLabourCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedMaterialCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedPersonResponsibilityCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationX1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditLatLong = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSiteLocationY1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientBillingType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientBillingTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultScheduleTemplate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultScheduleTemplateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysSeparationPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceLevelID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultJobCollectionTemplate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultJobCollectionTemplateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractExpiredReasonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractExpiredReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp06052OMClientContractManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlConstructionManagerFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnConstructionManagerFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp06388OMClientContractManagerConstructionManagerFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlCMFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnCMFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp06020OMJobManagerCMsFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Summer_Core = new WoodPlan5.DataSet_GC_Summer_Core();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.btnDateRangeOK = new DevExpress.XtraEditors.SimpleButton();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageJobRates = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlJobRate = new DevExpress.XtraGrid.GridControl();
            this.sp06345OMSiteContractManagerLinkedJobRatesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewJobRate = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteContractJobRateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFromDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colToDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colClientSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientName5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStartDate3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractEndDate3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationX2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditLatLong2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSiteLocationY2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParentFull6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDETeamCostPercentagePaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabPageYears = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControlYear = new DevExpress.XtraGrid.GridControl();
            this.sp06110OMSiteContractManagerLinkedYearsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewYear = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteContractYearID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colClientValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParentFull = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCreatedFromClientContractYearID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearlyPercentageIncrease1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridControlProfile = new DevExpress.XtraGrid.GridControl();
            this.sp06111OMSiteContractManagerLinkedProfilesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewProfile = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteContractProfileID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractYearID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceDateDue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colInvoiceDateActual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedBillAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualBillAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBilledByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSiteContractID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colBilledByPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParentFull1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedFromClientContractProfileID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageResponsibilities = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlResponsibility = new DevExpress.XtraGrid.GridControl();
            this.sp06112OMSiteContractManagerLinkedResponsibilitiesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewResponsibility = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPersonResponsibilityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibleForRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibleForRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibilityTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStartDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContractEndDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colResponsibilityType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParentFull2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageLabour = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlLabour = new DevExpress.XtraGrid.GridControl();
            this.sp06113OMSiteContractManagerLinkedLabourBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewLabour = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteContractLabourCostID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostPerUnitExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCostPerUnitVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercent = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSellPerUnitExVat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellPerUnitVatRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCISPercentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCISValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcodeSiteDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMiles = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLatLongSiteDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedToParentFull3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageEquipment = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlEquipment = new DevExpress.XtraGrid.GridControl();
            this.sp06114OMSiteContractManagerLinkedEquipmentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewEquipment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostUnitDescriptorID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn64 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeEditPercent3 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.gridColumn65 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn66 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn71 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn72 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn73 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn74 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn75 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn76 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedToParentFull4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageMaterial = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlMaterial = new DevExpress.XtraGrid.GridControl();
            this.sp06115OMSiteContractManagerLinkedMaterialsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewMaterial = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn89 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn90 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn91 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostUnitDescriptor3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn96 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn97 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn98 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeEditPercent4 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.gridColumn99 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn100 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn101 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn102 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn103 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn104 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn105 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn106 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedToParentFull5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageClientPOs = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlClientPO = new DevExpress.XtraGrid.GridControl();
            this.sp06326OMClientPOsLinkedToParentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Client_PO = new WoodPlan5.DataSet_OM_Client_PO();
            this.gridViewClientPO = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientPurchaseOrderLinkID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPurchaseOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsDefault = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colRemarks4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEditRemarks = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecord = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHTML8 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContractDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartingValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colUsedValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemainingValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarningValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageBillingRequirements = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlBillingRequirement = new DevExpress.XtraGrid.GridControl();
            this.sp01037CoreBillingRequirementItemsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Common_Functionality = new WoodPlan5.DataSet_Common_Functionality();
            this.gridViewBillingRequirement = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBillingRequirementID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequirementID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientBillRequirement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colTeamSelfBillRequirement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRequirementFulfilled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingRequirement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecord1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCheckedByStaff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiImport = new DevExpress.XtraBars.BarButtonItem();
            this.bsiUtilities = new DevExpress.XtraBars.BarSubItem();
            this.bbiInActive = new DevExpress.XtraBars.BarButtonItem();
            this.bbiOnHoldSiteContract = new DevExpress.XtraBars.BarButtonItem();
            this.bbiOffHoldSiteContract = new DevExpress.XtraBars.BarButtonItem();
            this.bciFilterContractsSelected = new DevExpress.XtraBars.BarCheckItem();
            this.bsiSelectedCount = new DevExpress.XtraBars.BarStaticItem();
            this.bsiWizard = new DevExpress.XtraBars.BarSubItem();
            this.bbiAddWizard = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddVisitWizard = new DevExpress.XtraBars.BarButtonItem();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.sp06052_OM_Client_Contract_ManagerTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06052_OM_Client_Contract_ManagerTableAdapter();
            this.sp06088_OM_Site_Contract_ManagerTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06088_OM_Site_Contract_ManagerTableAdapter();
            this.sp06110_OM_Site_Contract_Manager_Linked_YearsTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06110_OM_Site_Contract_Manager_Linked_YearsTableAdapter();
            this.sp06111_OM_Site_Contract_Manager_Linked_ProfilesTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06111_OM_Site_Contract_Manager_Linked_ProfilesTableAdapter();
            this.sp06112_OM_Site_Contract_Manager_Linked_ResponsibilitiesTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06112_OM_Site_Contract_Manager_Linked_ResponsibilitiesTableAdapter();
            this.sp06113_OM_Site_Contract_Manager_Linked_LabourTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06113_OM_Site_Contract_Manager_Linked_LabourTableAdapter();
            this.sp06114_OM_Site_Contract_Manager_Linked_EquipmentTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06114_OM_Site_Contract_Manager_Linked_EquipmentTableAdapter();
            this.sp06115_OM_Site_Contract_Manager_Linked_MaterialsTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06115_OM_Site_Contract_Manager_Linked_MaterialsTableAdapter();
            this.sp06326_OM_Client_POs_Linked_To_ParentTableAdapter = new WoodPlan5.DataSet_OM_Client_POTableAdapters.sp06326_OM_Client_POs_Linked_To_ParentTableAdapter();
            this.sp06345_OM_Site_Contract_Manager_Linked_Job_RatesTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06345_OM_Site_Contract_Manager_Linked_Job_RatesTableAdapter();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.hideContainerLeft = new DevExpress.XtraBars.Docking.AutoHideContainer();
            this.dockPanelFilters = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.popupContainerEditConstructionManager = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEditCMFilter = new DevExpress.XtraEditors.PopupContainerEdit();
            this.btnLoad = new DevExpress.XtraEditors.SimpleButton();
            this.btnClearAllFilters = new DevExpress.XtraEditors.SimpleButton();
            this.checkEditActiveOnly = new DevExpress.XtraEditors.CheckEdit();
            this.popupContainerEditDateRange = new DevExpress.XtraEditors.PopupContainerEdit();
            this.buttonEditClientFilter = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp06388_OM_Client_Contract_Manager_Construction_Manager_FilterTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06388_OM_Client_Contract_Manager_Construction_Manager_FilterTableAdapter();
            this.sp06020_OM_Job_Manager_CMs_FilterTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06020_OM_Job_Manager_CMs_FilterTableAdapter();
            this.sp01037_Core_Billing_Requirement_ItemsTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01037_Core_Billing_Requirement_ItemsTableAdapter();
            this.bbiSetContractExpiryReason = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlContract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06088OMSiteContractManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewContract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsClientContract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06052OMClientContractManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlConstructionManagerFilter)).BeginInit();
            this.popupContainerControlConstructionManagerFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06388OMClientContractManagerConstructionManagerFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCMFilter)).BeginInit();
            this.popupContainerControlCMFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06020OMJobManagerCMsFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Summer_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageJobRates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlJobRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06345OMSiteContractManagerLinkedJobRatesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewJobRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage3)).BeginInit();
            this.xtraTabPageYears.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06110OMSiteContractManagerLinkedYearsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlProfile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06111OMSiteContractManagerLinkedProfilesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProfile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML3)).BeginInit();
            this.xtraTabPageResponsibilities.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlResponsibility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06112OMSiteContractManagerLinkedResponsibilitiesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewResponsibility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML4)).BeginInit();
            this.xtraTabPageLabour.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLabour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06113OMSiteContractManagerLinkedLabourBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLabour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML5)).BeginInit();
            this.xtraTabPageEquipment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEquipment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06114OMSiteContractManagerLinkedEquipmentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEquipment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEditPercent3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML6)).BeginInit();
            this.xtraTabPageMaterial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMaterial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06115OMSiteContractManagerLinkedMaterialsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMaterial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEditPercent4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML7)).BeginInit();
            this.xtraTabPageClientPOs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlClientPO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06326OMClientPOsLinkedToParentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Client_PO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewClientPO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency9)).BeginInit();
            this.xtraTabPageBillingRequirements.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBillingRequirement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01037CoreBillingRequirementItemsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBillingRequirement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockPanel1)).BeginInit();
            this.hideContainerLeft.SuspendLayout();
            this.dockPanelFilters.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditConstructionManager.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditCMFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditActiveOnly.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditDateRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1027, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 577);
            this.barDockControlBottom.Size = new System.Drawing.Size(1027, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 577);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1027, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 577);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.DockManager = this.dockPanel1;
            this.barManager1.Images = this.imageCollection2;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.bbiImport,
            this.bsiUtilities,
            this.bbiInActive,
            this.bsiSelectedCount,
            this.bsiWizard,
            this.bbiAddVisitWizard,
            this.bbiAddWizard,
            this.bbiOnHoldSiteContract,
            this.bbiOffHoldSiteContract,
            this.bciFilterContractsSelected,
            this.bbiSetContractExpiryReason});
            this.barManager1.MaxItemId = 74;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1});
            this.barManager1.HighlightedLinkChanged += new DevExpress.XtraBars.HighlightedLinkChangedEventHandler(this.barManager1_HighlightedLinkChanged);
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 4;
            this.colActive.Width = 51;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colOnHold
            // 
            this.colOnHold.Caption = "On-Hold";
            this.colOnHold.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colOnHold.FieldName = "OnHold";
            this.colOnHold.Name = "colOnHold";
            this.colOnHold.OptionsColumn.AllowEdit = false;
            this.colOnHold.OptionsColumn.AllowFocus = false;
            this.colOnHold.Visible = true;
            this.colOnHold.VisibleIndex = 5;
            this.colOnHold.Width = 58;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControlContract
            // 
            this.gridControlContract.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlContract.DataSource = this.sp06088OMSiteContractManagerBindingSource;
            this.gridControlContract.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlContract.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlContract.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlContract.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlContract.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlContract.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlContract.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlContract.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlContract.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlContract.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlContract.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlContract.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 14, true, true, "Copy Site Contract Forward", "copy_forward")});
            this.gridControlContract.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlContract_EmbeddedNavigator_ButtonClick);
            this.gridControlContract.Location = new System.Drawing.Point(-1, 43);
            this.gridControlContract.MainView = this.gridViewContract;
            this.gridControlContract.MenuManager = this.barManager1;
            this.gridControlContract.Name = "gridControlContract";
            this.gridControlContract.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContract,
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditInteger,
            this.repositoryItemTextEditLatLong,
            this.repositoryItemTextEditHTML9});
            this.gridControlContract.Size = new System.Drawing.Size(1001, 295);
            this.gridControlContract.TabIndex = 4;
            this.gridControlContract.UseEmbeddedNavigator = true;
            this.gridControlContract.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewContract});
            // 
            // sp06088OMSiteContractManagerBindingSource
            // 
            this.sp06088OMSiteContractManagerBindingSource.DataMember = "sp06088_OM_Site_Contract_Manager";
            this.sp06088OMSiteContractManagerBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "Scheduler_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 7);
            this.imageCollection1.Images.SetKeyName(7, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.linked_documents_16_16, "linked_documents_16_16", typeof(global::WoodPlan5.Properties.Resources), 8);
            this.imageCollection1.Images.SetKeyName(8, "linked_documents_16_16");
            this.imageCollection1.InsertGalleryImage("addgroupheader_16x16.png", "images/reports/addgroupheader_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/reports/addgroupheader_16x16.png"), 9);
            this.imageCollection1.Images.SetKeyName(9, "addgroupheader_16x16.png");
            this.imageCollection1.InsertGalleryImage("groupheader_16x16.png", "images/reports/groupheader_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/reports/groupheader_16x16.png"), 10);
            this.imageCollection1.Images.SetKeyName(10, "groupheader_16x16.png");
            this.imageCollection1.InsertGalleryImage("copy_16x16.png", "images/edit/copy_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/copy_16x16.png"), 11);
            this.imageCollection1.Images.SetKeyName(11, "copy_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.BlockAdd_16x16, "BlockAdd_16x16", typeof(global::WoodPlan5.Properties.Resources), 12);
            this.imageCollection1.Images.SetKeyName(12, "BlockAdd_16x16");
            this.imageCollection1.InsertGalleryImage("assigntome_16x16.png", "images/people/assigntome_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/people/assigntome_16x16.png"), 13);
            this.imageCollection1.Images.SetKeyName(13, "assigntome_16x16.png");
            this.imageCollection1.InsertGalleryImage("converttorange_16x16.png", "images/actions/converttorange_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/converttorange_16x16.png"), 14);
            this.imageCollection1.Images.SetKeyName(14, "converttorange_16x16.png");
            this.imageCollection1.InsertGalleryImage("clearfilter_16x16.png", "images/filter/clearfilter_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/filter/clearfilter_16x16.png"), 15);
            this.imageCollection1.Images.SetKeyName(15, "clearfilter_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 16);
            this.imageCollection1.Images.SetKeyName(16, "refresh_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Refresh2_16x16, "Refresh2_16x16", typeof(global::WoodPlan5.Properties.Resources), 17);
            this.imageCollection1.Images.SetKeyName(17, "Refresh2_16x16");
            // 
            // gridViewContract
            // 
            this.gridViewContract.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteContractID,
            this.colClientContractID,
            this.colSiteID,
            this.colStartDate,
            this.colEndDate,
            this.colActive,
            this.colContractValue,
            this.colYearlyPercentageIncrease,
            this.colLastClientPaymentDate,
            this.colRemarks,
            this.colClientID,
            this.colGCCompanyID,
            this.colContractTypeID,
            this.colContractStatusID,
            this.colContractDirectorID,
            this.colClientContractStartDate,
            this.colClientContractEndDate,
            this.colClientContractActive,
            this.colSectorTypeID,
            this.colClientContractValue,
            this.colCheckRPIDate,
            this.colFinanceClientCode,
            this.colBillingCentreCodeID,
            this.colClientName,
            this.colGCCompanyName,
            this.colContractType,
            this.colContractStatus,
            this.colContractDirector,
            this.colSectorType,
            this.colSiteName,
            this.colLinkedVisitCount,
            this.colLinkedDocumentCount,
            this.colLinkedToParent3,
            this.colSiteInstructions,
            this.colLinkedEquipmentCount,
            this.colLinkedLabourCount,
            this.colLinkedMaterialCount,
            this.colLinkedPersonResponsibilityCount,
            this.colSiteLocationX1,
            this.colSiteLocationY1,
            this.colSitePostcode1,
            this.colClientBillingType,
            this.colClientBillingTypeID,
            this.colSiteCategory,
            this.colDefaultScheduleTemplate,
            this.colDefaultScheduleTemplateID,
            this.colOnHold,
            this.colOnHoldReason,
            this.colOnHoldStartDate,
            this.colOnHoldEndDate,
            this.colSelected,
            this.colDaysSeparationPercent,
            this.colSiteCode,
            this.colContractDescription,
            this.colServiceLevelID,
            this.colServiceLevel,
            this.colDefaultJobCollectionTemplate,
            this.colDefaultJobCollectionTemplateID,
            this.colContractExpiredReasonID,
            this.colContractExpiredReason});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActive;
            gridFormatRule1.Name = "FormatInActive";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.colOnHold;
            gridFormatRule2.ColumnApplyTo = this.colOnHold;
            gridFormatRule2.Name = "FormatOnHold";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 1;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridViewContract.FormatRules.Add(gridFormatRule1);
            this.gridViewContract.FormatRules.Add(gridFormatRule2);
            this.gridViewContract.GridControl = this.gridControlContract;
            this.gridViewContract.GroupCount = 1;
            this.gridViewContract.Name = "gridViewContract";
            this.gridViewContract.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewContract.OptionsFind.AlwaysVisible = true;
            this.gridViewContract.OptionsFind.FindDelay = 2000;
            this.gridViewContract.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewContract.OptionsLayout.StoreAppearance = true;
            this.gridViewContract.OptionsLayout.StoreFormatRules = true;
            this.gridViewContract.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewContract.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewContract.OptionsSelection.MultiSelect = true;
            this.gridViewContract.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewContract.OptionsView.ColumnAutoWidth = false;
            this.gridViewContract.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewContract.OptionsView.ShowGroupPanel = false;
            this.gridViewContract.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewContract.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewContract_CustomDrawCell);
            this.gridViewContract.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewContract_CustomRowCellEdit);
            this.gridViewContract.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewContract.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewContract.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewContract.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewContract_ShowingEditor);
            this.gridViewContract.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewContract.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewContract.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewContract.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewContract_MouseUp);
            this.gridViewContract.DoubleClick += new System.EventHandler(this.gridViewContract_DoubleClick);
            this.gridViewContract.GotFocus += new System.EventHandler(this.gridViewContract_GotFocus);
            // 
            // colSiteContractID
            // 
            this.colSiteContractID.Caption = "Site Contract ID";
            this.colSiteContractID.FieldName = "SiteContractID";
            this.colSiteContractID.Name = "colSiteContractID";
            this.colSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSiteContractID.Width = 98;
            // 
            // colClientContractID
            // 
            this.colClientContractID.Caption = "Client Contract ID";
            this.colClientContractID.FieldName = "ClientContractID";
            this.colClientContractID.Name = "colClientContractID";
            this.colClientContractID.OptionsColumn.AllowEdit = false;
            this.colClientContractID.OptionsColumn.AllowFocus = false;
            this.colClientContractID.OptionsColumn.ReadOnly = true;
            this.colClientContractID.Width = 107;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.Visible = true;
            this.colSiteID.VisibleIndex = 0;
            this.colSiteID.Width = 51;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 2;
            this.colStartDate.Width = 100;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "End Date";
            this.colEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 3;
            this.colEndDate.Width = 100;
            // 
            // colContractValue
            // 
            this.colContractValue.Caption = "Contract Value";
            this.colContractValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colContractValue.FieldName = "ContractValue";
            this.colContractValue.Name = "colContractValue";
            this.colContractValue.OptionsColumn.AllowEdit = false;
            this.colContractValue.OptionsColumn.AllowFocus = false;
            this.colContractValue.OptionsColumn.ReadOnly = true;
            this.colContractValue.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ContractValue", "{0:c2}")});
            this.colContractValue.Visible = true;
            this.colContractValue.VisibleIndex = 9;
            this.colContractValue.Width = 90;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colYearlyPercentageIncrease
            // 
            this.colYearlyPercentageIncrease.Caption = "Yearly % Increase";
            this.colYearlyPercentageIncrease.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colYearlyPercentageIncrease.FieldName = "YearlyPercentageIncrease";
            this.colYearlyPercentageIncrease.Name = "colYearlyPercentageIncrease";
            this.colYearlyPercentageIncrease.OptionsColumn.AllowEdit = false;
            this.colYearlyPercentageIncrease.OptionsColumn.AllowFocus = false;
            this.colYearlyPercentageIncrease.OptionsColumn.ReadOnly = true;
            this.colYearlyPercentageIncrease.Visible = true;
            this.colYearlyPercentageIncrease.VisibleIndex = 10;
            this.colYearlyPercentageIncrease.Width = 110;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // colLastClientPaymentDate
            // 
            this.colLastClientPaymentDate.Caption = "Last Client Payment";
            this.colLastClientPaymentDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colLastClientPaymentDate.FieldName = "LastClientPaymentDate";
            this.colLastClientPaymentDate.Name = "colLastClientPaymentDate";
            this.colLastClientPaymentDate.OptionsColumn.AllowEdit = false;
            this.colLastClientPaymentDate.OptionsColumn.AllowFocus = false;
            this.colLastClientPaymentDate.OptionsColumn.ReadOnly = true;
            this.colLastClientPaymentDate.Visible = true;
            this.colLastClientPaymentDate.VisibleIndex = 11;
            this.colLastClientPaymentDate.Width = 116;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 31;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colGCCompanyID
            // 
            this.colGCCompanyID.Caption = "GC Company ID";
            this.colGCCompanyID.FieldName = "GCCompanyID";
            this.colGCCompanyID.Name = "colGCCompanyID";
            this.colGCCompanyID.OptionsColumn.AllowEdit = false;
            this.colGCCompanyID.OptionsColumn.AllowFocus = false;
            this.colGCCompanyID.OptionsColumn.ReadOnly = true;
            this.colGCCompanyID.Width = 97;
            // 
            // colContractTypeID
            // 
            this.colContractTypeID.Caption = "Contract Type ID";
            this.colContractTypeID.FieldName = "ContractTypeID";
            this.colContractTypeID.Name = "colContractTypeID";
            this.colContractTypeID.OptionsColumn.AllowEdit = false;
            this.colContractTypeID.OptionsColumn.AllowFocus = false;
            this.colContractTypeID.OptionsColumn.ReadOnly = true;
            this.colContractTypeID.Width = 104;
            // 
            // colContractStatusID
            // 
            this.colContractStatusID.Caption = "Contract Status ID";
            this.colContractStatusID.FieldName = "ContractStatusID";
            this.colContractStatusID.Name = "colContractStatusID";
            this.colContractStatusID.OptionsColumn.AllowEdit = false;
            this.colContractStatusID.OptionsColumn.AllowFocus = false;
            this.colContractStatusID.OptionsColumn.ReadOnly = true;
            this.colContractStatusID.Width = 111;
            // 
            // colContractDirectorID
            // 
            this.colContractDirectorID.Caption = "Contract Director ID";
            this.colContractDirectorID.FieldName = "ContractDirectorID";
            this.colContractDirectorID.Name = "colContractDirectorID";
            this.colContractDirectorID.OptionsColumn.AllowEdit = false;
            this.colContractDirectorID.OptionsColumn.AllowFocus = false;
            this.colContractDirectorID.OptionsColumn.ReadOnly = true;
            this.colContractDirectorID.Width = 118;
            // 
            // colClientContractStartDate
            // 
            this.colClientContractStartDate.Caption = "Client Contract Start";
            this.colClientContractStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colClientContractStartDate.FieldName = "ClientContractStartDate";
            this.colClientContractStartDate.Name = "colClientContractStartDate";
            this.colClientContractStartDate.OptionsColumn.AllowEdit = false;
            this.colClientContractStartDate.OptionsColumn.AllowFocus = false;
            this.colClientContractStartDate.OptionsColumn.ReadOnly = true;
            this.colClientContractStartDate.Width = 120;
            // 
            // colClientContractEndDate
            // 
            this.colClientContractEndDate.Caption = "Client Contract End";
            this.colClientContractEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colClientContractEndDate.FieldName = "ClientContractEndDate";
            this.colClientContractEndDate.Name = "colClientContractEndDate";
            this.colClientContractEndDate.OptionsColumn.AllowEdit = false;
            this.colClientContractEndDate.OptionsColumn.AllowFocus = false;
            this.colClientContractEndDate.OptionsColumn.ReadOnly = true;
            this.colClientContractEndDate.Width = 114;
            // 
            // colClientContractActive
            // 
            this.colClientContractActive.Caption = "Client Contract Active";
            this.colClientContractActive.FieldName = "ClientContractActive";
            this.colClientContractActive.Name = "colClientContractActive";
            this.colClientContractActive.OptionsColumn.AllowEdit = false;
            this.colClientContractActive.OptionsColumn.AllowFocus = false;
            this.colClientContractActive.OptionsColumn.ReadOnly = true;
            this.colClientContractActive.Width = 126;
            // 
            // colSectorTypeID
            // 
            this.colSectorTypeID.Caption = "Sector Type ID";
            this.colSectorTypeID.FieldName = "SectorTypeID";
            this.colSectorTypeID.Name = "colSectorTypeID";
            this.colSectorTypeID.OptionsColumn.AllowEdit = false;
            this.colSectorTypeID.OptionsColumn.AllowFocus = false;
            this.colSectorTypeID.OptionsColumn.ReadOnly = true;
            this.colSectorTypeID.Width = 93;
            // 
            // colClientContractValue
            // 
            this.colClientContractValue.Caption = "Client Contract Value";
            this.colClientContractValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colClientContractValue.FieldName = "ClientContractValue";
            this.colClientContractValue.Name = "colClientContractValue";
            this.colClientContractValue.OptionsColumn.AllowEdit = false;
            this.colClientContractValue.OptionsColumn.AllowFocus = false;
            this.colClientContractValue.OptionsColumn.ReadOnly = true;
            this.colClientContractValue.Visible = true;
            this.colClientContractValue.VisibleIndex = 13;
            this.colClientContractValue.Width = 122;
            // 
            // colCheckRPIDate
            // 
            this.colCheckRPIDate.Caption = "Check RPI Date";
            this.colCheckRPIDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colCheckRPIDate.FieldName = "CheckRPIDate";
            this.colCheckRPIDate.Name = "colCheckRPIDate";
            this.colCheckRPIDate.OptionsColumn.AllowEdit = false;
            this.colCheckRPIDate.OptionsColumn.AllowFocus = false;
            this.colCheckRPIDate.OptionsColumn.ReadOnly = true;
            this.colCheckRPIDate.Width = 96;
            // 
            // colFinanceClientCode
            // 
            this.colFinanceClientCode.Caption = "Finance Client Code";
            this.colFinanceClientCode.FieldName = "FinanceClientCode";
            this.colFinanceClientCode.Name = "colFinanceClientCode";
            this.colFinanceClientCode.OptionsColumn.AllowEdit = false;
            this.colFinanceClientCode.OptionsColumn.AllowFocus = false;
            this.colFinanceClientCode.OptionsColumn.ReadOnly = true;
            this.colFinanceClientCode.Width = 116;
            // 
            // colBillingCentreCodeID
            // 
            this.colBillingCentreCodeID.Caption = "Billing Centre Code ID";
            this.colBillingCentreCodeID.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID.Name = "colBillingCentreCodeID";
            this.colBillingCentreCodeID.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID.Width = 125;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Width = 251;
            // 
            // colGCCompanyName
            // 
            this.colGCCompanyName.Caption = "GC Company Name";
            this.colGCCompanyName.FieldName = "GCCompanyName";
            this.colGCCompanyName.Name = "colGCCompanyName";
            this.colGCCompanyName.OptionsColumn.AllowEdit = false;
            this.colGCCompanyName.OptionsColumn.AllowFocus = false;
            this.colGCCompanyName.OptionsColumn.ReadOnly = true;
            this.colGCCompanyName.Visible = true;
            this.colGCCompanyName.VisibleIndex = 14;
            this.colGCCompanyName.Width = 113;
            // 
            // colContractType
            // 
            this.colContractType.Caption = "Contract Type";
            this.colContractType.FieldName = "ContractType";
            this.colContractType.Name = "colContractType";
            this.colContractType.OptionsColumn.AllowEdit = false;
            this.colContractType.OptionsColumn.AllowFocus = false;
            this.colContractType.OptionsColumn.ReadOnly = true;
            this.colContractType.Visible = true;
            this.colContractType.VisibleIndex = 15;
            this.colContractType.Width = 134;
            // 
            // colContractStatus
            // 
            this.colContractStatus.Caption = "Contract Status";
            this.colContractStatus.FieldName = "ContractStatus";
            this.colContractStatus.Name = "colContractStatus";
            this.colContractStatus.OptionsColumn.AllowEdit = false;
            this.colContractStatus.OptionsColumn.AllowFocus = false;
            this.colContractStatus.OptionsColumn.ReadOnly = true;
            this.colContractStatus.Visible = true;
            this.colContractStatus.VisibleIndex = 16;
            this.colContractStatus.Width = 131;
            // 
            // colContractDirector
            // 
            this.colContractDirector.Caption = "Contract Director";
            this.colContractDirector.FieldName = "ContractDirector";
            this.colContractDirector.Name = "colContractDirector";
            this.colContractDirector.OptionsColumn.AllowEdit = false;
            this.colContractDirector.OptionsColumn.AllowFocus = false;
            this.colContractDirector.OptionsColumn.ReadOnly = true;
            this.colContractDirector.Visible = true;
            this.colContractDirector.VisibleIndex = 17;
            this.colContractDirector.Width = 127;
            // 
            // colSectorType
            // 
            this.colSectorType.Caption = "Sector Type";
            this.colSectorType.FieldName = "SectorType";
            this.colSectorType.Name = "colSectorType";
            this.colSectorType.OptionsColumn.AllowEdit = false;
            this.colSectorType.OptionsColumn.AllowFocus = false;
            this.colSectorType.OptionsColumn.ReadOnly = true;
            this.colSectorType.Visible = true;
            this.colSectorType.VisibleIndex = 19;
            this.colSectorType.Width = 117;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 1;
            this.colSiteName.Width = 171;
            // 
            // colLinkedVisitCount
            // 
            this.colLinkedVisitCount.Caption = "Linked Visit Count";
            this.colLinkedVisitCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colLinkedVisitCount.FieldName = "LinkedVisitCount";
            this.colLinkedVisitCount.Name = "colLinkedVisitCount";
            this.colLinkedVisitCount.OptionsColumn.ReadOnly = true;
            this.colLinkedVisitCount.Visible = true;
            this.colLinkedVisitCount.VisibleIndex = 23;
            this.colLinkedVisitCount.Width = 105;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colLinkedDocumentCount
            // 
            this.colLinkedDocumentCount.Caption = "Linked Documents";
            this.colLinkedDocumentCount.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocumentsClientContract;
            this.colLinkedDocumentCount.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount.Name = "colLinkedDocumentCount";
            this.colLinkedDocumentCount.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount.Visible = true;
            this.colLinkedDocumentCount.VisibleIndex = 25;
            this.colLinkedDocumentCount.Width = 107;
            // 
            // repositoryItemHyperLinkEditLinkedDocumentsClientContract
            // 
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContract.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContract.Name = "repositoryItemHyperLinkEditLinkedDocumentsClientContract";
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContract.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContract.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocumentsClientContract_OpenLink);
            // 
            // colLinkedToParent3
            // 
            this.colLinkedToParent3.Caption = "Linked To Client Contract";
            this.colLinkedToParent3.ColumnEdit = this.repositoryItemTextEditHTML9;
            this.colLinkedToParent3.FieldName = "LinkedToParent";
            this.colLinkedToParent3.Name = "colLinkedToParent3";
            this.colLinkedToParent3.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent3.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent3.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent3.Visible = true;
            this.colLinkedToParent3.VisibleIndex = 20;
            this.colLinkedToParent3.Width = 362;
            // 
            // repositoryItemTextEditHTML9
            // 
            this.repositoryItemTextEditHTML9.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML9.AutoHeight = false;
            this.repositoryItemTextEditHTML9.Name = "repositoryItemTextEditHTML9";
            // 
            // colSiteInstructions
            // 
            this.colSiteInstructions.Caption = "Site Instructions";
            this.colSiteInstructions.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colSiteInstructions.FieldName = "SiteInstructions";
            this.colSiteInstructions.Name = "colSiteInstructions";
            this.colSiteInstructions.OptionsColumn.ReadOnly = true;
            this.colSiteInstructions.Visible = true;
            this.colSiteInstructions.VisibleIndex = 21;
            this.colSiteInstructions.Width = 99;
            // 
            // colLinkedEquipmentCount
            // 
            this.colLinkedEquipmentCount.Caption = "Equipment Count";
            this.colLinkedEquipmentCount.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colLinkedEquipmentCount.FieldName = "LinkedEquipmentCount";
            this.colLinkedEquipmentCount.Name = "colLinkedEquipmentCount";
            this.colLinkedEquipmentCount.OptionsColumn.AllowEdit = false;
            this.colLinkedEquipmentCount.OptionsColumn.AllowFocus = false;
            this.colLinkedEquipmentCount.OptionsColumn.ReadOnly = true;
            this.colLinkedEquipmentCount.Visible = true;
            this.colLinkedEquipmentCount.VisibleIndex = 26;
            this.colLinkedEquipmentCount.Width = 103;
            // 
            // repositoryItemTextEditInteger
            // 
            this.repositoryItemTextEditInteger.AutoHeight = false;
            this.repositoryItemTextEditInteger.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger.Name = "repositoryItemTextEditInteger";
            // 
            // colLinkedLabourCount
            // 
            this.colLinkedLabourCount.Caption = "Labour Count";
            this.colLinkedLabourCount.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colLinkedLabourCount.FieldName = "LinkedLabourCount";
            this.colLinkedLabourCount.Name = "colLinkedLabourCount";
            this.colLinkedLabourCount.OptionsColumn.AllowEdit = false;
            this.colLinkedLabourCount.OptionsColumn.AllowFocus = false;
            this.colLinkedLabourCount.OptionsColumn.ReadOnly = true;
            this.colLinkedLabourCount.Visible = true;
            this.colLinkedLabourCount.VisibleIndex = 27;
            this.colLinkedLabourCount.Width = 86;
            // 
            // colLinkedMaterialCount
            // 
            this.colLinkedMaterialCount.Caption = "Material Count";
            this.colLinkedMaterialCount.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colLinkedMaterialCount.FieldName = "LinkedMaterialCount";
            this.colLinkedMaterialCount.Name = "colLinkedMaterialCount";
            this.colLinkedMaterialCount.OptionsColumn.AllowEdit = false;
            this.colLinkedMaterialCount.OptionsColumn.AllowFocus = false;
            this.colLinkedMaterialCount.OptionsColumn.ReadOnly = true;
            this.colLinkedMaterialCount.Visible = true;
            this.colLinkedMaterialCount.VisibleIndex = 28;
            this.colLinkedMaterialCount.Width = 91;
            // 
            // colLinkedPersonResponsibilityCount
            // 
            this.colLinkedPersonResponsibilityCount.Caption = "Responsibility Count";
            this.colLinkedPersonResponsibilityCount.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colLinkedPersonResponsibilityCount.FieldName = "LinkedPersonResponsibilityCount";
            this.colLinkedPersonResponsibilityCount.Name = "colLinkedPersonResponsibilityCount";
            this.colLinkedPersonResponsibilityCount.OptionsColumn.AllowEdit = false;
            this.colLinkedPersonResponsibilityCount.OptionsColumn.AllowFocus = false;
            this.colLinkedPersonResponsibilityCount.OptionsColumn.ReadOnly = true;
            this.colLinkedPersonResponsibilityCount.Visible = true;
            this.colLinkedPersonResponsibilityCount.VisibleIndex = 29;
            this.colLinkedPersonResponsibilityCount.Width = 118;
            // 
            // colSiteLocationX1
            // 
            this.colSiteLocationX1.Caption = "Latitude";
            this.colSiteLocationX1.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colSiteLocationX1.FieldName = "SiteLocationX";
            this.colSiteLocationX1.Name = "colSiteLocationX1";
            this.colSiteLocationX1.OptionsColumn.AllowEdit = false;
            this.colSiteLocationX1.OptionsColumn.AllowFocus = false;
            this.colSiteLocationX1.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemTextEditLatLong
            // 
            this.repositoryItemTextEditLatLong.AutoHeight = false;
            this.repositoryItemTextEditLatLong.Mask.EditMask = "##0.00000000";
            this.repositoryItemTextEditLatLong.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditLatLong.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditLatLong.Name = "repositoryItemTextEditLatLong";
            // 
            // colSiteLocationY1
            // 
            this.colSiteLocationY1.Caption = "Longitude";
            this.colSiteLocationY1.ColumnEdit = this.repositoryItemTextEditLatLong;
            this.colSiteLocationY1.FieldName = "SiteLocationY";
            this.colSiteLocationY1.Name = "colSiteLocationY1";
            this.colSiteLocationY1.OptionsColumn.AllowEdit = false;
            this.colSiteLocationY1.OptionsColumn.AllowFocus = false;
            this.colSiteLocationY1.OptionsColumn.ReadOnly = true;
            // 
            // colSitePostcode1
            // 
            this.colSitePostcode1.Caption = "Postcode";
            this.colSitePostcode1.FieldName = "SitePostcode";
            this.colSitePostcode1.Name = "colSitePostcode1";
            this.colSitePostcode1.OptionsColumn.AllowEdit = false;
            this.colSitePostcode1.OptionsColumn.AllowFocus = false;
            this.colSitePostcode1.OptionsColumn.ReadOnly = true;
            // 
            // colClientBillingType
            // 
            this.colClientBillingType.Caption = "Billing Type";
            this.colClientBillingType.FieldName = "ClientBillingType";
            this.colClientBillingType.Name = "colClientBillingType";
            this.colClientBillingType.OptionsColumn.AllowEdit = false;
            this.colClientBillingType.OptionsColumn.AllowFocus = false;
            this.colClientBillingType.OptionsColumn.ReadOnly = true;
            this.colClientBillingType.Visible = true;
            this.colClientBillingType.VisibleIndex = 12;
            this.colClientBillingType.Width = 147;
            // 
            // colClientBillingTypeID
            // 
            this.colClientBillingTypeID.Caption = "Billing Type ID";
            this.colClientBillingTypeID.FieldName = "ClientBillingTypeID";
            this.colClientBillingTypeID.Name = "colClientBillingTypeID";
            this.colClientBillingTypeID.OptionsColumn.AllowEdit = false;
            this.colClientBillingTypeID.OptionsColumn.AllowFocus = false;
            this.colClientBillingTypeID.OptionsColumn.ReadOnly = true;
            this.colClientBillingTypeID.Width = 116;
            // 
            // colSiteCategory
            // 
            this.colSiteCategory.Caption = "Site Category";
            this.colSiteCategory.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colSiteCategory.FieldName = "SiteCategory";
            this.colSiteCategory.Name = "colSiteCategory";
            this.colSiteCategory.OptionsColumn.ReadOnly = true;
            this.colSiteCategory.Visible = true;
            this.colSiteCategory.VisibleIndex = 20;
            this.colSiteCategory.Width = 111;
            // 
            // colDefaultScheduleTemplate
            // 
            this.colDefaultScheduleTemplate.Caption = "Default Schedule Template";
            this.colDefaultScheduleTemplate.FieldName = "DefaultScheduleTemplate";
            this.colDefaultScheduleTemplate.Name = "colDefaultScheduleTemplate";
            this.colDefaultScheduleTemplate.OptionsColumn.AllowEdit = false;
            this.colDefaultScheduleTemplate.OptionsColumn.AllowFocus = false;
            this.colDefaultScheduleTemplate.OptionsColumn.ReadOnly = true;
            this.colDefaultScheduleTemplate.Visible = true;
            this.colDefaultScheduleTemplate.VisibleIndex = 18;
            this.colDefaultScheduleTemplate.Width = 147;
            // 
            // colDefaultScheduleTemplateID
            // 
            this.colDefaultScheduleTemplateID.Caption = "Default Schedule Template ID";
            this.colDefaultScheduleTemplateID.FieldName = "DefaultScheduleTemplateID";
            this.colDefaultScheduleTemplateID.Name = "colDefaultScheduleTemplateID";
            this.colDefaultScheduleTemplateID.OptionsColumn.AllowEdit = false;
            this.colDefaultScheduleTemplateID.OptionsColumn.AllowFocus = false;
            this.colDefaultScheduleTemplateID.OptionsColumn.ReadOnly = true;
            this.colDefaultScheduleTemplateID.Width = 161;
            // 
            // colOnHoldReason
            // 
            this.colOnHoldReason.Caption = "On-Hold Reason";
            this.colOnHoldReason.FieldName = "OnHoldReason";
            this.colOnHoldReason.Name = "colOnHoldReason";
            this.colOnHoldReason.OptionsColumn.AllowEdit = false;
            this.colOnHoldReason.OptionsColumn.AllowFocus = false;
            this.colOnHoldReason.Visible = true;
            this.colOnHoldReason.VisibleIndex = 8;
            this.colOnHoldReason.Width = 140;
            // 
            // colOnHoldStartDate
            // 
            this.colOnHoldStartDate.Caption = "On-Hold Start";
            this.colOnHoldStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colOnHoldStartDate.FieldName = "OnHoldStartDate";
            this.colOnHoldStartDate.Name = "colOnHoldStartDate";
            this.colOnHoldStartDate.OptionsColumn.AllowEdit = false;
            this.colOnHoldStartDate.OptionsColumn.AllowFocus = false;
            this.colOnHoldStartDate.Visible = true;
            this.colOnHoldStartDate.VisibleIndex = 6;
            this.colOnHoldStartDate.Width = 100;
            // 
            // colOnHoldEndDate
            // 
            this.colOnHoldEndDate.Caption = "On-Hold End";
            this.colOnHoldEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colOnHoldEndDate.FieldName = "OnHoldEndDate";
            this.colOnHoldEndDate.Name = "colOnHoldEndDate";
            this.colOnHoldEndDate.OptionsColumn.AllowEdit = false;
            this.colOnHoldEndDate.OptionsColumn.AllowFocus = false;
            this.colOnHoldEndDate.Visible = true;
            this.colOnHoldEndDate.VisibleIndex = 7;
            this.colOnHoldEndDate.Width = 100;
            // 
            // colSelected
            // 
            this.colSelected.Caption = "Selected";
            this.colSelected.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            this.colSelected.Width = 60;
            // 
            // colDaysSeparationPercent
            // 
            this.colDaysSeparationPercent.Caption = "Days Separation %";
            this.colDaysSeparationPercent.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colDaysSeparationPercent.FieldName = "DaysSeparationPercent";
            this.colDaysSeparationPercent.Name = "colDaysSeparationPercent";
            this.colDaysSeparationPercent.OptionsColumn.AllowEdit = false;
            this.colDaysSeparationPercent.OptionsColumn.AllowFocus = false;
            this.colDaysSeparationPercent.OptionsColumn.ReadOnly = true;
            this.colDaysSeparationPercent.Visible = true;
            this.colDaysSeparationPercent.VisibleIndex = 24;
            this.colDaysSeparationPercent.Width = 112;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Width = 115;
            // 
            // colContractDescription
            // 
            this.colContractDescription.Caption = "Contract Description";
            this.colContractDescription.FieldName = "ContractDescription";
            this.colContractDescription.Name = "colContractDescription";
            this.colContractDescription.OptionsColumn.AllowEdit = false;
            this.colContractDescription.OptionsColumn.AllowFocus = false;
            this.colContractDescription.OptionsColumn.ReadOnly = true;
            this.colContractDescription.Width = 236;
            // 
            // colServiceLevelID
            // 
            this.colServiceLevelID.Caption = "Service Level ID";
            this.colServiceLevelID.FieldName = "ServiceLevelID";
            this.colServiceLevelID.Name = "colServiceLevelID";
            this.colServiceLevelID.OptionsColumn.AllowEdit = false;
            this.colServiceLevelID.OptionsColumn.AllowFocus = false;
            this.colServiceLevelID.OptionsColumn.ReadOnly = true;
            this.colServiceLevelID.Width = 96;
            // 
            // colServiceLevel
            // 
            this.colServiceLevel.Caption = "Service Level";
            this.colServiceLevel.FieldName = "ServiceLevel";
            this.colServiceLevel.Name = "colServiceLevel";
            this.colServiceLevel.OptionsColumn.AllowEdit = false;
            this.colServiceLevel.OptionsColumn.AllowFocus = false;
            this.colServiceLevel.OptionsColumn.ReadOnly = true;
            this.colServiceLevel.Visible = true;
            this.colServiceLevel.VisibleIndex = 22;
            this.colServiceLevel.Width = 103;
            // 
            // colDefaultJobCollectionTemplate
            // 
            this.colDefaultJobCollectionTemplate.Caption = "Default Job Collection Template";
            this.colDefaultJobCollectionTemplate.FieldName = "DefaultJobCollectionTemplate";
            this.colDefaultJobCollectionTemplate.Name = "colDefaultJobCollectionTemplate";
            this.colDefaultJobCollectionTemplate.OptionsColumn.AllowEdit = false;
            this.colDefaultJobCollectionTemplate.OptionsColumn.AllowFocus = false;
            this.colDefaultJobCollectionTemplate.OptionsColumn.ReadOnly = true;
            this.colDefaultJobCollectionTemplate.Visible = true;
            this.colDefaultJobCollectionTemplate.VisibleIndex = 30;
            this.colDefaultJobCollectionTemplate.Width = 170;
            // 
            // colDefaultJobCollectionTemplateID
            // 
            this.colDefaultJobCollectionTemplateID.Caption = "Default Job Collection Template ID";
            this.colDefaultJobCollectionTemplateID.FieldName = "DefaultJobCollectionTemplateID";
            this.colDefaultJobCollectionTemplateID.Name = "colDefaultJobCollectionTemplateID";
            this.colDefaultJobCollectionTemplateID.OptionsColumn.AllowEdit = false;
            this.colDefaultJobCollectionTemplateID.OptionsColumn.AllowFocus = false;
            this.colDefaultJobCollectionTemplateID.OptionsColumn.ReadOnly = true;
            this.colDefaultJobCollectionTemplateID.Width = 184;
            // 
            // colContractExpiredReasonID
            // 
            this.colContractExpiredReasonID.Caption = "Expired Reason ID";
            this.colContractExpiredReasonID.FieldName = "ContractExpiredReasonID";
            this.colContractExpiredReasonID.Name = "colContractExpiredReasonID";
            this.colContractExpiredReasonID.OptionsColumn.AllowEdit = false;
            this.colContractExpiredReasonID.OptionsColumn.AllowFocus = false;
            this.colContractExpiredReasonID.OptionsColumn.ReadOnly = true;
            this.colContractExpiredReasonID.Width = 108;
            // 
            // colContractExpiredReason
            // 
            this.colContractExpiredReason.Caption = "Expired Reason";
            this.colContractExpiredReason.FieldName = "ContractExpiredReason";
            this.colContractExpiredReason.Name = "colContractExpiredReason";
            this.colContractExpiredReason.OptionsColumn.AllowEdit = false;
            this.colContractExpiredReason.OptionsColumn.AllowFocus = false;
            this.colContractExpiredReason.OptionsColumn.ReadOnly = true;
            this.colContractExpiredReason.Width = 94;
            // 
            // sp06052OMClientContractManagerBindingSource
            // 
            this.sp06052OMClientContractManagerBindingSource.DataMember = "sp06052_OM_Client_Contract_Manager";
            this.sp06052OMClientContractManagerBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(23, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlConstructionManagerFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlCMFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlDateRange);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControlContract);
            this.splitContainerControl1.Panel1.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl1.Panel1.Text = "Jobs";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1004, 577);
            this.splitContainerControl1.SplitterPosition = 229;
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.SplitGroupPanelCollapsed += new DevExpress.XtraEditors.SplitGroupPanelCollapsedEventHandler(this.splitContainerControl1_SplitGroupPanelCollapsed);
            // 
            // popupContainerControlConstructionManagerFilter
            // 
            this.popupContainerControlConstructionManagerFilter.Controls.Add(this.btnConstructionManagerFilterOK);
            this.popupContainerControlConstructionManagerFilter.Controls.Add(this.gridControl3);
            this.popupContainerControlConstructionManagerFilter.Location = new System.Drawing.Point(141, 156);
            this.popupContainerControlConstructionManagerFilter.Name = "popupContainerControlConstructionManagerFilter";
            this.popupContainerControlConstructionManagerFilter.Size = new System.Drawing.Size(131, 135);
            this.popupContainerControlConstructionManagerFilter.TabIndex = 22;
            // 
            // btnConstructionManagerFilterOK
            // 
            this.btnConstructionManagerFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnConstructionManagerFilterOK.Location = new System.Drawing.Point(3, 112);
            this.btnConstructionManagerFilterOK.Name = "btnConstructionManagerFilterOK";
            this.btnConstructionManagerFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnConstructionManagerFilterOK.TabIndex = 18;
            this.btnConstructionManagerFilterOK.Text = "OK";
            this.btnConstructionManagerFilterOK.Click += new System.EventHandler(this.btnConstructionManagerFilterOK_Click);
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp06388OMClientContractManagerConstructionManagerFilterBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(3, 3);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(125, 107);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp06388OMClientContractManagerConstructionManagerFilterBindingSource
            // 
            this.sp06388OMClientContractManagerConstructionManagerFilterBindingSource.DataMember = "sp06388_OM_Client_Contract_Manager_Construction_Manager_Filter";
            this.sp06388OMClientContractManagerConstructionManagerFilterBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsFind.AlwaysVisible = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Surname";
            this.gridColumn6.FieldName = "Surname";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 122;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Forename";
            this.gridColumn7.FieldName = "Forename";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 1;
            this.gridColumn7.Width = 82;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "ID";
            this.gridColumn8.FieldName = "ID";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Width = 53;
            // 
            // popupContainerControlCMFilter
            // 
            this.popupContainerControlCMFilter.Controls.Add(this.btnCMFilterOK);
            this.popupContainerControlCMFilter.Controls.Add(this.gridControl5);
            this.popupContainerControlCMFilter.Location = new System.Drawing.Point(4, 156);
            this.popupContainerControlCMFilter.Name = "popupContainerControlCMFilter";
            this.popupContainerControlCMFilter.Size = new System.Drawing.Size(131, 135);
            this.popupContainerControlCMFilter.TabIndex = 21;
            // 
            // btnCMFilterOK
            // 
            this.btnCMFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCMFilterOK.Location = new System.Drawing.Point(3, 112);
            this.btnCMFilterOK.Name = "btnCMFilterOK";
            this.btnCMFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnCMFilterOK.TabIndex = 18;
            this.btnCMFilterOK.Text = "OK";
            this.btnCMFilterOK.Click += new System.EventHandler(this.btnCMFilterOK_Click);
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.sp06020OMJobManagerCMsFilterBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(3, 3);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(125, 107);
            this.gridControl5.TabIndex = 1;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp06020OMJobManagerCMsFilterBindingSource
            // 
            this.sp06020OMJobManagerCMsFilterBindingSource.DataMember = "sp06020_OM_Job_Manager_CMs_Filter";
            this.sp06020OMJobManagerCMsFilterBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // dataSet_GC_Summer_Core
            // 
            this.dataSet_GC_Summer_Core.DataSetName = "DataSet_GC_Summer_Core";
            this.dataSet_GC_Summer_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription3,
            this.colForename,
            this.colID1});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsFind.AlwaysVisible = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription3
            // 
            this.colDescription3.Caption = "Surname";
            this.colDescription3.FieldName = "Surname";
            this.colDescription3.Name = "colDescription3";
            this.colDescription3.OptionsColumn.AllowEdit = false;
            this.colDescription3.OptionsColumn.AllowFocus = false;
            this.colDescription3.OptionsColumn.ReadOnly = true;
            this.colDescription3.Visible = true;
            this.colDescription3.VisibleIndex = 0;
            this.colDescription3.Width = 122;
            // 
            // colForename
            // 
            this.colForename.Caption = "Forename";
            this.colForename.FieldName = "Forename";
            this.colForename.Name = "colForename";
            this.colForename.OptionsColumn.AllowEdit = false;
            this.colForename.OptionsColumn.AllowFocus = false;
            this.colForename.OptionsColumn.ReadOnly = true;
            this.colForename.Visible = true;
            this.colForename.VisibleIndex = 1;
            this.colForename.Width = 82;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.Width = 53;
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeOK);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(326, 140);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(205, 107);
            this.popupContainerControlDateRange.TabIndex = 17;
            // 
            // groupControl1
            // 
            this.groupControl1.AllowHtmlText = true;
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFromDate);
            this.groupControl1.Controls.Add(this.dateEditToDate);
            this.groupControl1.Location = new System.Drawing.Point(3, 5);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(199, 76);
            this.groupControl1.TabIndex = 20;
            this.groupControl1.Text = "Date Range  [Expected <b>Start</b> Date]";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(154, 20);
            this.dateEditFromDate.TabIndex = 10;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(154, 20);
            this.dateEditToDate.TabIndex = 11;
            // 
            // btnDateRangeOK
            // 
            this.btnDateRangeOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeOK.Location = new System.Drawing.Point(3, 84);
            this.btnDateRangeOK.Name = "btnDateRangeOK";
            this.btnDateRangeOK.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDateRangeOK.Size = new System.Drawing.Size(50, 20);
            this.btnDateRangeOK.TabIndex = 19;
            this.btnDateRangeOK.Text = "OK";
            this.btnDateRangeOK.Click += new System.EventHandler(this.btnDateRangeOK_Click);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 1);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(1002, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageJobRates;
            this.xtraTabControl1.Size = new System.Drawing.Size(1004, 229);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageJobRates,
            this.xtraTabPageYears,
            this.xtraTabPageResponsibilities,
            this.xtraTabPageLabour,
            this.xtraTabPageEquipment,
            this.xtraTabPageMaterial,
            this.xtraTabPageClientPOs,
            this.xtraTabPageBillingRequirements});
            // 
            // xtraTabPageJobRates
            // 
            this.xtraTabPageJobRates.Controls.Add(this.gridControlJobRate);
            this.xtraTabPageJobRates.Name = "xtraTabPageJobRates";
            this.xtraTabPageJobRates.Size = new System.Drawing.Size(999, 203);
            this.xtraTabPageJobRates.Text = "Job Rates";
            // 
            // gridControlJobRate
            // 
            this.gridControlJobRate.DataSource = this.sp06345OMSiteContractManagerLinkedJobRatesBindingSource;
            this.gridControlJobRate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlJobRate.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlJobRate.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 14, true, true, "Apply Rate Uplift", "apply_uplift")});
            this.gridControlJobRate.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlJobRate_EmbeddedNavigator_ButtonClick);
            this.gridControlJobRate.Location = new System.Drawing.Point(0, 0);
            this.gridControlJobRate.MainView = this.gridViewJobRate;
            this.gridControlJobRate.MenuManager = this.barManager1;
            this.gridControlJobRate.Name = "gridControlJobRate";
            this.gridControlJobRate.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemTextEditCurency,
            this.repositoryItemTextEditLatLong2,
            this.repositoryItemTextEditDateTime6,
            this.repositoryItemTextEditHTML1,
            this.repositoryItemTextEditPercentage3});
            this.gridControlJobRate.Size = new System.Drawing.Size(999, 203);
            this.gridControlJobRate.TabIndex = 26;
            this.gridControlJobRate.UseEmbeddedNavigator = true;
            this.gridControlJobRate.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewJobRate});
            // 
            // sp06345OMSiteContractManagerLinkedJobRatesBindingSource
            // 
            this.sp06345OMSiteContractManagerLinkedJobRatesBindingSource.DataMember = "sp06345_OM_Site_Contract_Manager_Linked_Job_Rates";
            this.sp06345OMSiteContractManagerLinkedJobRatesBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridViewJobRate
            // 
            this.gridViewJobRate.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteContractJobRateID,
            this.colSiteContractID3,
            this.colJobSubTypeID,
            this.colFromDate,
            this.colToDate,
            this.colTeamCost,
            this.colClientSell,
            this.colRemarks5,
            this.colClientName5,
            this.colContractStartDate3,
            this.colContractEndDate3,
            this.colSiteName5,
            this.colLinkedToParent7,
            this.colSitePostcode2,
            this.colSiteLocationX2,
            this.colSiteLocationY2,
            this.colSiteID7,
            this.colLinkedToParentFull6,
            this.colJobTypeID,
            this.colJobType,
            this.colJobSubType,
            this.colVisitCategoryID,
            this.colVisitCategory,
            this.colSiteContractStartDate,
            this.colSiteContractEndDate,
            this.colDETeamCostPercentagePaid});
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleExpression1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleExpression1.Appearance.Options.UseForeColor = true;
            formatConditionRuleExpression1.Expression = "Not Today() Between([FromDate], [ToDate])";
            gridFormatRule3.Rule = formatConditionRuleExpression1;
            this.gridViewJobRate.FormatRules.Add(gridFormatRule3);
            this.gridViewJobRate.GridControl = this.gridControlJobRate;
            this.gridViewJobRate.GroupCount = 2;
            this.gridViewJobRate.Name = "gridViewJobRate";
            this.gridViewJobRate.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewJobRate.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewJobRate.OptionsLayout.StoreAppearance = true;
            this.gridViewJobRate.OptionsLayout.StoreFormatRules = true;
            this.gridViewJobRate.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewJobRate.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewJobRate.OptionsSelection.MultiSelect = true;
            this.gridViewJobRate.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewJobRate.OptionsView.ColumnAutoWidth = false;
            this.gridViewJobRate.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewJobRate.OptionsView.ShowGroupPanel = false;
            this.gridViewJobRate.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent7, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName5, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobSubType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colVisitCategory, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFromDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewJobRate.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewJobRate.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewJobRate.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewJobRate.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewJobRate.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewJobRate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewJobRate.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewJobRate_MouseUp);
            this.gridViewJobRate.DoubleClick += new System.EventHandler(this.gridViewJobRate_DoubleClick);
            this.gridViewJobRate.GotFocus += new System.EventHandler(this.gridViewJobRate_GotFocus);
            // 
            // colSiteContractJobRateID
            // 
            this.colSiteContractJobRateID.Caption = "Site Contract Job Rate ID";
            this.colSiteContractJobRateID.FieldName = "SiteContractJobRateID";
            this.colSiteContractJobRateID.Name = "colSiteContractJobRateID";
            this.colSiteContractJobRateID.OptionsColumn.AllowEdit = false;
            this.colSiteContractJobRateID.OptionsColumn.AllowFocus = false;
            this.colSiteContractJobRateID.OptionsColumn.ReadOnly = true;
            this.colSiteContractJobRateID.Width = 142;
            // 
            // colSiteContractID3
            // 
            this.colSiteContractID3.Caption = "Site Contract ID";
            this.colSiteContractID3.FieldName = "SiteContractID";
            this.colSiteContractID3.Name = "colSiteContractID3";
            this.colSiteContractID3.OptionsColumn.AllowEdit = false;
            this.colSiteContractID3.OptionsColumn.AllowFocus = false;
            this.colSiteContractID3.OptionsColumn.ReadOnly = true;
            this.colSiteContractID3.Width = 96;
            // 
            // colJobSubTypeID
            // 
            this.colJobSubTypeID.Caption = "Job Sub-Type ID";
            this.colJobSubTypeID.FieldName = "JobSubTypeID";
            this.colJobSubTypeID.Name = "colJobSubTypeID";
            this.colJobSubTypeID.OptionsColumn.AllowEdit = false;
            this.colJobSubTypeID.OptionsColumn.AllowFocus = false;
            this.colJobSubTypeID.OptionsColumn.ReadOnly = true;
            this.colJobSubTypeID.Width = 99;
            // 
            // colFromDate
            // 
            this.colFromDate.Caption = "Valid From";
            this.colFromDate.ColumnEdit = this.repositoryItemTextEditDateTime6;
            this.colFromDate.FieldName = "FromDate";
            this.colFromDate.Name = "colFromDate";
            this.colFromDate.OptionsColumn.AllowEdit = false;
            this.colFromDate.OptionsColumn.AllowFocus = false;
            this.colFromDate.OptionsColumn.ReadOnly = true;
            this.colFromDate.Visible = true;
            this.colFromDate.VisibleIndex = 3;
            this.colFromDate.Width = 81;
            // 
            // repositoryItemTextEditDateTime6
            // 
            this.repositoryItemTextEditDateTime6.AutoHeight = false;
            this.repositoryItemTextEditDateTime6.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime6.Name = "repositoryItemTextEditDateTime6";
            // 
            // colToDate
            // 
            this.colToDate.Caption = "Valid To";
            this.colToDate.ColumnEdit = this.repositoryItemTextEditDateTime6;
            this.colToDate.FieldName = "ToDate";
            this.colToDate.Name = "colToDate";
            this.colToDate.OptionsColumn.AllowEdit = false;
            this.colToDate.OptionsColumn.AllowFocus = false;
            this.colToDate.OptionsColumn.ReadOnly = true;
            this.colToDate.Visible = true;
            this.colToDate.VisibleIndex = 4;
            this.colToDate.Width = 85;
            // 
            // colTeamCost
            // 
            this.colTeamCost.Caption = "Team Cost";
            this.colTeamCost.ColumnEdit = this.repositoryItemTextEditCurency;
            this.colTeamCost.FieldName = "TeamCost";
            this.colTeamCost.Name = "colTeamCost";
            this.colTeamCost.OptionsColumn.AllowEdit = false;
            this.colTeamCost.OptionsColumn.AllowFocus = false;
            this.colTeamCost.OptionsColumn.ReadOnly = true;
            this.colTeamCost.Visible = true;
            this.colTeamCost.VisibleIndex = 6;
            this.colTeamCost.Width = 81;
            // 
            // repositoryItemTextEditCurency
            // 
            this.repositoryItemTextEditCurency.AutoHeight = false;
            this.repositoryItemTextEditCurency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurency.Mask.ShowPlaceHolders = false;
            this.repositoryItemTextEditCurency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurency.Name = "repositoryItemTextEditCurency";
            // 
            // colClientSell
            // 
            this.colClientSell.Caption = "Client Sell";
            this.colClientSell.ColumnEdit = this.repositoryItemTextEditCurency;
            this.colClientSell.FieldName = "ClientSell";
            this.colClientSell.Name = "colClientSell";
            this.colClientSell.OptionsColumn.AllowEdit = false;
            this.colClientSell.OptionsColumn.AllowFocus = false;
            this.colClientSell.OptionsColumn.ReadOnly = true;
            this.colClientSell.Visible = true;
            this.colClientSell.VisibleIndex = 7;
            this.colClientSell.Width = 80;
            // 
            // colRemarks5
            // 
            this.colRemarks5.Caption = "Remarks";
            this.colRemarks5.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRemarks5.FieldName = "Remarks";
            this.colRemarks5.Name = "colRemarks5";
            this.colRemarks5.OptionsColumn.ReadOnly = true;
            this.colRemarks5.Visible = true;
            this.colRemarks5.VisibleIndex = 8;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colClientName5
            // 
            this.colClientName5.Caption = "Client Name";
            this.colClientName5.FieldName = "ClientName";
            this.colClientName5.Name = "colClientName5";
            this.colClientName5.OptionsColumn.AllowEdit = false;
            this.colClientName5.OptionsColumn.AllowFocus = false;
            this.colClientName5.OptionsColumn.ReadOnly = true;
            this.colClientName5.Width = 150;
            // 
            // colContractStartDate3
            // 
            this.colContractStartDate3.Caption = "Contract Start";
            this.colContractStartDate3.ColumnEdit = this.repositoryItemTextEditDateTime6;
            this.colContractStartDate3.FieldName = "ContractStartDate";
            this.colContractStartDate3.Name = "colContractStartDate3";
            this.colContractStartDate3.OptionsColumn.AllowEdit = false;
            this.colContractStartDate3.OptionsColumn.AllowFocus = false;
            this.colContractStartDate3.OptionsColumn.ReadOnly = true;
            this.colContractStartDate3.Width = 88;
            // 
            // colContractEndDate3
            // 
            this.colContractEndDate3.Caption = "Contract End";
            this.colContractEndDate3.ColumnEdit = this.repositoryItemTextEditDateTime6;
            this.colContractEndDate3.FieldName = "ContractEndDate";
            this.colContractEndDate3.Name = "colContractEndDate3";
            this.colContractEndDate3.OptionsColumn.AllowEdit = false;
            this.colContractEndDate3.OptionsColumn.AllowFocus = false;
            this.colContractEndDate3.OptionsColumn.ReadOnly = true;
            this.colContractEndDate3.Width = 82;
            // 
            // colSiteName5
            // 
            this.colSiteName5.Caption = "Site Name";
            this.colSiteName5.FieldName = "SiteName";
            this.colSiteName5.Name = "colSiteName5";
            this.colSiteName5.OptionsColumn.AllowEdit = false;
            this.colSiteName5.OptionsColumn.AllowFocus = false;
            this.colSiteName5.OptionsColumn.ReadOnly = true;
            this.colSiteName5.Width = 171;
            // 
            // colLinkedToParent7
            // 
            this.colLinkedToParent7.Caption = "Linked To Contract";
            this.colLinkedToParent7.FieldName = "LinkedToParent";
            this.colLinkedToParent7.Name = "colLinkedToParent7";
            this.colLinkedToParent7.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent7.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent7.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent7.Width = 335;
            // 
            // colSitePostcode2
            // 
            this.colSitePostcode2.Caption = "Site Postcode";
            this.colSitePostcode2.FieldName = "SitePostcode";
            this.colSitePostcode2.Name = "colSitePostcode2";
            this.colSitePostcode2.OptionsColumn.AllowEdit = false;
            this.colSitePostcode2.OptionsColumn.AllowFocus = false;
            this.colSitePostcode2.OptionsColumn.ReadOnly = true;
            this.colSitePostcode2.Width = 84;
            // 
            // colSiteLocationX2
            // 
            this.colSiteLocationX2.Caption = "Latitude";
            this.colSiteLocationX2.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.colSiteLocationX2.FieldName = "SiteLocationX";
            this.colSiteLocationX2.Name = "colSiteLocationX2";
            this.colSiteLocationX2.OptionsColumn.AllowEdit = false;
            this.colSiteLocationX2.OptionsColumn.AllowFocus = false;
            this.colSiteLocationX2.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemTextEditLatLong2
            // 
            this.repositoryItemTextEditLatLong2.AutoHeight = false;
            this.repositoryItemTextEditLatLong2.Mask.EditMask = "##0.00000000";
            this.repositoryItemTextEditLatLong2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditLatLong2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditLatLong2.Name = "repositoryItemTextEditLatLong2";
            // 
            // colSiteLocationY2
            // 
            this.colSiteLocationY2.Caption = "Longitude";
            this.colSiteLocationY2.ColumnEdit = this.repositoryItemTextEditLatLong2;
            this.colSiteLocationY2.FieldName = "SiteLocationY";
            this.colSiteLocationY2.Name = "colSiteLocationY2";
            this.colSiteLocationY2.OptionsColumn.AllowEdit = false;
            this.colSiteLocationY2.OptionsColumn.AllowFocus = false;
            this.colSiteLocationY2.OptionsColumn.ReadOnly = true;
            // 
            // colSiteID7
            // 
            this.colSiteID7.Caption = "Site ID";
            this.colSiteID7.FieldName = "SiteID";
            this.colSiteID7.Name = "colSiteID7";
            this.colSiteID7.OptionsColumn.AllowEdit = false;
            this.colSiteID7.OptionsColumn.AllowFocus = false;
            this.colSiteID7.OptionsColumn.ReadOnly = true;
            // 
            // colLinkedToParentFull6
            // 
            this.colLinkedToParentFull6.Caption = "Linked To Site Contract";
            this.colLinkedToParentFull6.ColumnEdit = this.repositoryItemTextEditHTML1;
            this.colLinkedToParentFull6.FieldName = "LinkedToParentFull";
            this.colLinkedToParentFull6.Name = "colLinkedToParentFull6";
            this.colLinkedToParentFull6.OptionsColumn.AllowEdit = false;
            this.colLinkedToParentFull6.OptionsColumn.AllowFocus = false;
            this.colLinkedToParentFull6.OptionsColumn.ReadOnly = true;
            this.colLinkedToParentFull6.Width = 289;
            // 
            // repositoryItemTextEditHTML1
            // 
            this.repositoryItemTextEditHTML1.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML1.AutoHeight = false;
            this.repositoryItemTextEditHTML1.Name = "repositoryItemTextEditHTML1";
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            this.colJobTypeID.Width = 77;
            // 
            // colJobType
            // 
            this.colJobType.Caption = "Job Type";
            this.colJobType.FieldName = "JobType";
            this.colJobType.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colJobType.Name = "colJobType";
            this.colJobType.OptionsColumn.AllowEdit = false;
            this.colJobType.OptionsColumn.AllowFocus = false;
            this.colJobType.OptionsColumn.ReadOnly = true;
            this.colJobType.Visible = true;
            this.colJobType.VisibleIndex = 0;
            this.colJobType.Width = 154;
            // 
            // colJobSubType
            // 
            this.colJobSubType.Caption = "Job Sub-Type";
            this.colJobSubType.FieldName = "JobSubType";
            this.colJobSubType.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colJobSubType.Name = "colJobSubType";
            this.colJobSubType.OptionsColumn.AllowEdit = false;
            this.colJobSubType.OptionsColumn.AllowFocus = false;
            this.colJobSubType.OptionsColumn.ReadOnly = true;
            this.colJobSubType.Visible = true;
            this.colJobSubType.VisibleIndex = 1;
            this.colJobSubType.Width = 166;
            // 
            // colVisitCategoryID
            // 
            this.colVisitCategoryID.Caption = "Visit Category ID";
            this.colVisitCategoryID.FieldName = "VisitCategoryID";
            this.colVisitCategoryID.Name = "colVisitCategoryID";
            this.colVisitCategoryID.OptionsColumn.AllowEdit = false;
            this.colVisitCategoryID.OptionsColumn.AllowFocus = false;
            this.colVisitCategoryID.OptionsColumn.ReadOnly = true;
            this.colVisitCategoryID.Width = 100;
            // 
            // colVisitCategory
            // 
            this.colVisitCategory.Caption = "Visit Category";
            this.colVisitCategory.FieldName = "VisitCategory";
            this.colVisitCategory.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colVisitCategory.Name = "colVisitCategory";
            this.colVisitCategory.OptionsColumn.AllowEdit = false;
            this.colVisitCategory.OptionsColumn.AllowFocus = false;
            this.colVisitCategory.OptionsColumn.ReadOnly = true;
            this.colVisitCategory.Visible = true;
            this.colVisitCategory.VisibleIndex = 2;
            this.colVisitCategory.Width = 131;
            // 
            // colSiteContractStartDate
            // 
            this.colSiteContractStartDate.Caption = "Site Contract Start Date";
            this.colSiteContractStartDate.ColumnEdit = this.repositoryItemTextEditDateTime6;
            this.colSiteContractStartDate.FieldName = "SiteContractStartDate";
            this.colSiteContractStartDate.Name = "colSiteContractStartDate";
            this.colSiteContractStartDate.OptionsColumn.AllowEdit = false;
            this.colSiteContractStartDate.OptionsColumn.AllowFocus = false;
            this.colSiteContractStartDate.OptionsColumn.ReadOnly = true;
            this.colSiteContractStartDate.Width = 135;
            // 
            // colSiteContractEndDate
            // 
            this.colSiteContractEndDate.Caption = "Site Contract End Date";
            this.colSiteContractEndDate.ColumnEdit = this.repositoryItemTextEditDateTime6;
            this.colSiteContractEndDate.FieldName = "SiteContractEndDate";
            this.colSiteContractEndDate.Name = "colSiteContractEndDate";
            this.colSiteContractEndDate.OptionsColumn.AllowEdit = false;
            this.colSiteContractEndDate.OptionsColumn.AllowFocus = false;
            this.colSiteContractEndDate.OptionsColumn.ReadOnly = true;
            this.colSiteContractEndDate.Width = 129;
            // 
            // colDETeamCostPercentagePaid
            // 
            this.colDETeamCostPercentagePaid.Caption = "DE Team Cost % Paid";
            this.colDETeamCostPercentagePaid.ColumnEdit = this.repositoryItemTextEditPercentage3;
            this.colDETeamCostPercentagePaid.FieldName = "DETeamCostPercentagePaid";
            this.colDETeamCostPercentagePaid.Name = "colDETeamCostPercentagePaid";
            this.colDETeamCostPercentagePaid.OptionsColumn.AllowEdit = false;
            this.colDETeamCostPercentagePaid.OptionsColumn.AllowFocus = false;
            this.colDETeamCostPercentagePaid.OptionsColumn.ReadOnly = true;
            this.colDETeamCostPercentagePaid.Visible = true;
            this.colDETeamCostPercentagePaid.VisibleIndex = 5;
            this.colDETeamCostPercentagePaid.Width = 123;
            // 
            // repositoryItemTextEditPercentage3
            // 
            this.repositoryItemTextEditPercentage3.AutoHeight = false;
            this.repositoryItemTextEditPercentage3.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage3.Name = "repositoryItemTextEditPercentage3";
            // 
            // xtraTabPageYears
            // 
            this.xtraTabPageYears.Controls.Add(this.splitContainerControl2);
            this.xtraTabPageYears.Name = "xtraTabPageYears";
            this.xtraTabPageYears.Size = new System.Drawing.Size(999, 203);
            this.xtraTabPageYears.Text = "Years \\ Billing Profile";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControlYear);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Contract Years ";
            this.splitContainerControl2.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel2.Controls.Add(this.gridControlProfile);
            this.splitContainerControl2.Panel2.ShowCaption = true;
            this.splitContainerControl2.Panel2.Text = "Contract Year Billing Profiles ";
            this.splitContainerControl2.Size = new System.Drawing.Size(999, 203);
            this.splitContainerControl2.SplitterPosition = 450;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridControlYear
            // 
            this.gridControlYear.DataSource = this.sp06110OMSiteContractManagerLinkedYearsBindingSource;
            this.gridControlYear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlYear.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlYear.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlYear.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlYear.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlYear.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlYear.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlYear.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlYear.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlYear.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlYear.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlYear.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlYear.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 11, true, true, "Copy from Client Contract", "copy from client contract"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view")});
            this.gridControlYear.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlYear_EmbeddedNavigator_ButtonClick);
            this.gridControlYear.Location = new System.Drawing.Point(0, 0);
            this.gridControlYear.MainView = this.gridViewYear;
            this.gridControlYear.MenuManager = this.barManager1;
            this.gridControlYear.Name = "gridControlYear";
            this.gridControlYear.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemTextEditMoney2,
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear,
            this.repositoryItemTextEditPercentage2,
            this.repositoryItemTextEditHTML2});
            this.gridControlYear.Size = new System.Drawing.Size(425, 200);
            this.gridControlYear.TabIndex = 5;
            this.gridControlYear.UseEmbeddedNavigator = true;
            this.gridControlYear.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewYear});
            // 
            // sp06110OMSiteContractManagerLinkedYearsBindingSource
            // 
            this.sp06110OMSiteContractManagerLinkedYearsBindingSource.DataMember = "sp06110_OM_Site_Contract_Manager_Linked_Years";
            this.sp06110OMSiteContractManagerLinkedYearsBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridViewYear
            // 
            this.gridViewYear.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteContractYearID,
            this.colSiteContractID1,
            this.colYearDescription,
            this.colStartDate1,
            this.colEndDate1,
            this.colActive1,
            this.colClientValue,
            this.colRemarks1,
            this.colClientID1,
            this.colClientName1,
            this.colContractStartDate,
            this.colContractEndDate,
            this.colLinkedToParent,
            this.colSiteID1,
            this.colSiteName1,
            this.colLinkedToParentFull,
            this.colCreatedFromClientContractYearID,
            this.colYearlyPercentageIncrease1});
            this.gridViewYear.GridControl = this.gridControlYear;
            this.gridViewYear.GroupCount = 2;
            this.gridViewYear.Name = "gridViewYear";
            this.gridViewYear.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewYear.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewYear.OptionsLayout.StoreAppearance = true;
            this.gridViewYear.OptionsLayout.StoreFormatRules = true;
            this.gridViewYear.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewYear.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewYear.OptionsSelection.MultiSelect = true;
            this.gridViewYear.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewYear.OptionsView.ColumnAutoWidth = false;
            this.gridViewYear.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewYear.OptionsView.ShowGroupPanel = false;
            this.gridViewYear.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewYear.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewYear_CustomRowCellEdit);
            this.gridViewYear.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewYear.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewYear.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewYear.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewYear_ShowingEditor);
            this.gridViewYear.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewYear.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewYear.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewYear.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewYear_MouseUp);
            this.gridViewYear.DoubleClick += new System.EventHandler(this.gridViewYear_DoubleClick);
            this.gridViewYear.GotFocus += new System.EventHandler(this.gridViewYear_GotFocus);
            // 
            // colSiteContractYearID
            // 
            this.colSiteContractYearID.Caption = "Site Contract Year ID";
            this.colSiteContractYearID.FieldName = "SiteContractYearID";
            this.colSiteContractYearID.Name = "colSiteContractYearID";
            this.colSiteContractYearID.OptionsColumn.AllowEdit = false;
            this.colSiteContractYearID.OptionsColumn.AllowFocus = false;
            this.colSiteContractYearID.OptionsColumn.ReadOnly = true;
            this.colSiteContractYearID.Width = 132;
            // 
            // colSiteContractID1
            // 
            this.colSiteContractID1.Caption = "Site Contract ID";
            this.colSiteContractID1.FieldName = "SiteContractID";
            this.colSiteContractID1.Name = "colSiteContractID1";
            this.colSiteContractID1.OptionsColumn.AllowEdit = false;
            this.colSiteContractID1.OptionsColumn.AllowFocus = false;
            this.colSiteContractID1.OptionsColumn.ReadOnly = true;
            this.colSiteContractID1.Width = 107;
            // 
            // colYearDescription
            // 
            this.colYearDescription.Caption = "Year Description";
            this.colYearDescription.FieldName = "YearDescription";
            this.colYearDescription.Name = "colYearDescription";
            this.colYearDescription.OptionsColumn.AllowEdit = false;
            this.colYearDescription.OptionsColumn.AllowFocus = false;
            this.colYearDescription.OptionsColumn.ReadOnly = true;
            this.colYearDescription.Visible = true;
            this.colYearDescription.VisibleIndex = 2;
            this.colYearDescription.Width = 157;
            // 
            // colStartDate1
            // 
            this.colStartDate1.Caption = "Start Date";
            this.colStartDate1.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colStartDate1.FieldName = "StartDate";
            this.colStartDate1.Name = "colStartDate1";
            this.colStartDate1.OptionsColumn.AllowEdit = false;
            this.colStartDate1.OptionsColumn.AllowFocus = false;
            this.colStartDate1.OptionsColumn.ReadOnly = true;
            this.colStartDate1.Visible = true;
            this.colStartDate1.VisibleIndex = 0;
            this.colStartDate1.Width = 154;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colEndDate1
            // 
            this.colEndDate1.Caption = "End Date";
            this.colEndDate1.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colEndDate1.FieldName = "EndDate";
            this.colEndDate1.Name = "colEndDate1";
            this.colEndDate1.OptionsColumn.AllowEdit = false;
            this.colEndDate1.OptionsColumn.AllowFocus = false;
            this.colEndDate1.OptionsColumn.ReadOnly = true;
            this.colEndDate1.Visible = true;
            this.colEndDate1.VisibleIndex = 1;
            this.colEndDate1.Width = 100;
            // 
            // colActive1
            // 
            this.colActive1.Caption = "Active";
            this.colActive1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colActive1.FieldName = "Active";
            this.colActive1.Name = "colActive1";
            this.colActive1.OptionsColumn.AllowEdit = false;
            this.colActive1.OptionsColumn.AllowFocus = false;
            this.colActive1.OptionsColumn.ReadOnly = true;
            this.colActive1.Visible = true;
            this.colActive1.VisibleIndex = 3;
            this.colActive1.Width = 51;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colClientValue
            // 
            this.colClientValue.Caption = "Client Value";
            this.colClientValue.ColumnEdit = this.repositoryItemTextEditMoney2;
            this.colClientValue.FieldName = "ClientValue";
            this.colClientValue.Name = "colClientValue";
            this.colClientValue.OptionsColumn.AllowEdit = false;
            this.colClientValue.OptionsColumn.AllowFocus = false;
            this.colClientValue.OptionsColumn.ReadOnly = true;
            this.colClientValue.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ClientValue", "{0:C}")});
            this.colClientValue.Visible = true;
            this.colClientValue.VisibleIndex = 5;
            this.colClientValue.Width = 77;
            // 
            // repositoryItemTextEditMoney2
            // 
            this.repositoryItemTextEditMoney2.AutoHeight = false;
            this.repositoryItemTextEditMoney2.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney2.Name = "repositoryItemTextEditMoney2";
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 6;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Width = 145;
            // 
            // colContractStartDate
            // 
            this.colContractStartDate.Caption = "Contract Start Date";
            this.colContractStartDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colContractStartDate.FieldName = "ContractStartDate";
            this.colContractStartDate.Name = "colContractStartDate";
            this.colContractStartDate.OptionsColumn.AllowEdit = false;
            this.colContractStartDate.OptionsColumn.AllowFocus = false;
            this.colContractStartDate.OptionsColumn.ReadOnly = true;
            this.colContractStartDate.Width = 116;
            // 
            // colContractEndDate
            // 
            this.colContractEndDate.Caption = "Contract End Date";
            this.colContractEndDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colContractEndDate.FieldName = "ContractEndDate";
            this.colContractEndDate.Name = "colContractEndDate";
            this.colContractEndDate.OptionsColumn.AllowEdit = false;
            this.colContractEndDate.OptionsColumn.AllowFocus = false;
            this.colContractEndDate.OptionsColumn.ReadOnly = true;
            this.colContractEndDate.Width = 110;
            // 
            // colLinkedToParent
            // 
            this.colLinkedToParent.Caption = "Linked To Contract";
            this.colLinkedToParent.FieldName = "LinkedToParent";
            this.colLinkedToParent.Name = "colLinkedToParent";
            this.colLinkedToParent.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent.Visible = true;
            this.colLinkedToParent.VisibleIndex = 7;
            this.colLinkedToParent.Width = 407;
            // 
            // colSiteID1
            // 
            this.colSiteID1.Caption = "Site ID";
            this.colSiteID1.FieldName = "SiteID";
            this.colSiteID1.Name = "colSiteID1";
            this.colSiteID1.OptionsColumn.AllowEdit = false;
            this.colSiteID1.OptionsColumn.AllowFocus = false;
            this.colSiteID1.OptionsColumn.ReadOnly = true;
            this.colSiteID1.Visible = true;
            this.colSiteID1.VisibleIndex = 7;
            this.colSiteID1.Width = 51;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Visible = true;
            this.colSiteName1.VisibleIndex = 9;
            this.colSiteName1.Width = 194;
            // 
            // colLinkedToParentFull
            // 
            this.colLinkedToParentFull.Caption = "Linked To Site";
            this.colLinkedToParentFull.ColumnEdit = this.repositoryItemTextEditHTML2;
            this.colLinkedToParentFull.FieldName = "LinkedToParentFull";
            this.colLinkedToParentFull.Name = "colLinkedToParentFull";
            this.colLinkedToParentFull.OptionsColumn.AllowEdit = false;
            this.colLinkedToParentFull.OptionsColumn.AllowFocus = false;
            this.colLinkedToParentFull.OptionsColumn.ReadOnly = true;
            this.colLinkedToParentFull.Width = 464;
            // 
            // repositoryItemTextEditHTML2
            // 
            this.repositoryItemTextEditHTML2.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML2.AutoHeight = false;
            this.repositoryItemTextEditHTML2.Name = "repositoryItemTextEditHTML2";
            // 
            // colCreatedFromClientContractYearID
            // 
            this.colCreatedFromClientContractYearID.Caption = "Created From Client Contract Year ID";
            this.colCreatedFromClientContractYearID.FieldName = "CreatedFromClientContractYearID";
            this.colCreatedFromClientContractYearID.Name = "colCreatedFromClientContractYearID";
            this.colCreatedFromClientContractYearID.OptionsColumn.AllowEdit = false;
            this.colCreatedFromClientContractYearID.OptionsColumn.AllowFocus = false;
            this.colCreatedFromClientContractYearID.OptionsColumn.ReadOnly = true;
            this.colCreatedFromClientContractYearID.Width = 201;
            // 
            // colYearlyPercentageIncrease1
            // 
            this.colYearlyPercentageIncrease1.Caption = "Yearly % Increase";
            this.colYearlyPercentageIncrease1.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colYearlyPercentageIncrease1.FieldName = "YearlyPercentageIncrease";
            this.colYearlyPercentageIncrease1.Name = "colYearlyPercentageIncrease1";
            this.colYearlyPercentageIncrease1.OptionsColumn.AllowEdit = false;
            this.colYearlyPercentageIncrease1.OptionsColumn.AllowFocus = false;
            this.colYearlyPercentageIncrease1.OptionsColumn.ReadOnly = true;
            this.colYearlyPercentageIncrease1.Visible = true;
            this.colYearlyPercentageIncrease1.VisibleIndex = 4;
            this.colYearlyPercentageIncrease1.Width = 110;
            // 
            // repositoryItemTextEditPercentage2
            // 
            this.repositoryItemTextEditPercentage2.AutoHeight = false;
            this.repositoryItemTextEditPercentage2.Name = "repositoryItemTextEditPercentage2";
            // 
            // repositoryItemHyperLinkEditLinkedDocumentsClientContractYear
            // 
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear.Name = "repositoryItemHyperLinkEditLinkedDocumentsClientContractYear";
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear_OpenLink);
            // 
            // gridControlProfile
            // 
            this.gridControlProfile.DataSource = this.sp06111OMSiteContractManagerLinkedProfilesBindingSource;
            this.gridControlProfile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlProfile.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlProfile.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlProfile.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlProfile.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlProfile.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlProfile.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlProfile.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlProfile.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlProfile.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlProfile.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlProfile.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlProfile.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 11, true, true, "Copy from Client Contract Billing Profile", "copy_from_client"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 9, true, true, "Add from Template", "add_from_template"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Template Manager", "template_manager"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view")});
            this.gridControlProfile.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlProfile_EmbeddedNavigator_ButtonClick);
            this.gridControlProfile.Location = new System.Drawing.Point(0, 0);
            this.gridControlProfile.MainView = this.gridViewProfile;
            this.gridControlProfile.MenuManager = this.barManager1;
            this.gridControlProfile.Name = "gridControlProfile";
            this.gridControlProfile.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemCheckEdit3,
            this.repositoryItemTextEditDateEdit3,
            this.repositoryItemTextEditCurrency3,
            this.repositoryItemTextEditHTML3});
            this.gridControlProfile.Size = new System.Drawing.Size(518, 200);
            this.gridControlProfile.TabIndex = 6;
            this.gridControlProfile.UseEmbeddedNavigator = true;
            this.gridControlProfile.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewProfile});
            // 
            // sp06111OMSiteContractManagerLinkedProfilesBindingSource
            // 
            this.sp06111OMSiteContractManagerLinkedProfilesBindingSource.DataMember = "sp06111_OM_Site_Contract_Manager_Linked_Profiles";
            this.sp06111OMSiteContractManagerLinkedProfilesBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridViewProfile
            // 
            this.gridViewProfile.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteContractProfileID,
            this.colSiteContractYearID1,
            this.colInvoiceDateDue,
            this.colInvoiceDateActual,
            this.colEstimatedBillAmount,
            this.colActualBillAmount,
            this.colBilledByPersonID,
            this.colRemarks2,
            this.colSiteContractID2,
            this.colYearDescription1,
            this.colYearStartDate,
            this.colYearEndDate,
            this.colYearActive,
            this.colClientID2,
            this.colClientName2,
            this.colContractStartDate1,
            this.colContractEndDate1,
            this.colLinkedToParent1,
            this.colBilledByPerson,
            this.colSiteID2,
            this.colSiteName2,
            this.colLinkedToParentFull1,
            this.colCreatedFromClientContractProfileID});
            this.gridViewProfile.GridControl = this.gridControlProfile;
            this.gridViewProfile.GroupCount = 3;
            this.gridViewProfile.Name = "gridViewProfile";
            this.gridViewProfile.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewProfile.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewProfile.OptionsLayout.StoreAppearance = true;
            this.gridViewProfile.OptionsLayout.StoreFormatRules = true;
            this.gridViewProfile.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewProfile.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewProfile.OptionsSelection.MultiSelect = true;
            this.gridViewProfile.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewProfile.OptionsView.ColumnAutoWidth = false;
            this.gridViewProfile.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewProfile.OptionsView.ShowGroupPanel = false;
            this.gridViewProfile.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colYearDescription1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInvoiceDateDue, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewProfile.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewProfile.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewProfile.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewProfile.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewProfile.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewProfile.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewProfile.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewProfile_MouseUp);
            this.gridViewProfile.DoubleClick += new System.EventHandler(this.gridViewYear_DoubleClick);
            this.gridViewProfile.GotFocus += new System.EventHandler(this.gridViewProfile_GotFocus);
            // 
            // colSiteContractProfileID
            // 
            this.colSiteContractProfileID.Caption = "Site Contract Profile ID";
            this.colSiteContractProfileID.FieldName = "SiteContractProfileID";
            this.colSiteContractProfileID.Name = "colSiteContractProfileID";
            this.colSiteContractProfileID.OptionsColumn.AllowEdit = false;
            this.colSiteContractProfileID.OptionsColumn.AllowFocus = false;
            this.colSiteContractProfileID.OptionsColumn.ReadOnly = true;
            this.colSiteContractProfileID.Width = 153;
            // 
            // colSiteContractYearID1
            // 
            this.colSiteContractYearID1.Caption = "Site Contract Year ID";
            this.colSiteContractYearID1.FieldName = "SiteContractYearID";
            this.colSiteContractYearID1.Name = "colSiteContractYearID1";
            this.colSiteContractYearID1.OptionsColumn.AllowEdit = false;
            this.colSiteContractYearID1.OptionsColumn.AllowFocus = false;
            this.colSiteContractYearID1.OptionsColumn.ReadOnly = true;
            this.colSiteContractYearID1.Width = 132;
            // 
            // colInvoiceDateDue
            // 
            this.colInvoiceDateDue.Caption = "Invoice Due";
            this.colInvoiceDateDue.ColumnEdit = this.repositoryItemTextEditDateEdit3;
            this.colInvoiceDateDue.FieldName = "InvoiceDateDue";
            this.colInvoiceDateDue.Name = "colInvoiceDateDue";
            this.colInvoiceDateDue.OptionsColumn.AllowEdit = false;
            this.colInvoiceDateDue.OptionsColumn.AllowFocus = false;
            this.colInvoiceDateDue.OptionsColumn.ReadOnly = true;
            this.colInvoiceDateDue.Visible = true;
            this.colInvoiceDateDue.VisibleIndex = 0;
            this.colInvoiceDateDue.Width = 151;
            // 
            // repositoryItemTextEditDateEdit3
            // 
            this.repositoryItemTextEditDateEdit3.AutoHeight = false;
            this.repositoryItemTextEditDateEdit3.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateEdit3.Name = "repositoryItemTextEditDateEdit3";
            // 
            // colInvoiceDateActual
            // 
            this.colInvoiceDateActual.Caption = "Invoice Actual";
            this.colInvoiceDateActual.ColumnEdit = this.repositoryItemTextEditDateEdit3;
            this.colInvoiceDateActual.FieldName = "InvoiceDateActual";
            this.colInvoiceDateActual.Name = "colInvoiceDateActual";
            this.colInvoiceDateActual.OptionsColumn.AllowEdit = false;
            this.colInvoiceDateActual.OptionsColumn.AllowFocus = false;
            this.colInvoiceDateActual.OptionsColumn.ReadOnly = true;
            this.colInvoiceDateActual.Visible = true;
            this.colInvoiceDateActual.VisibleIndex = 1;
            this.colInvoiceDateActual.Width = 100;
            // 
            // colEstimatedBillAmount
            // 
            this.colEstimatedBillAmount.Caption = "Estimated Bill";
            this.colEstimatedBillAmount.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colEstimatedBillAmount.FieldName = "EstimatedBillAmount";
            this.colEstimatedBillAmount.Name = "colEstimatedBillAmount";
            this.colEstimatedBillAmount.OptionsColumn.AllowEdit = false;
            this.colEstimatedBillAmount.OptionsColumn.AllowFocus = false;
            this.colEstimatedBillAmount.OptionsColumn.ReadOnly = true;
            this.colEstimatedBillAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "EstimatedBillAmount", "{0:C}")});
            this.colEstimatedBillAmount.Visible = true;
            this.colEstimatedBillAmount.VisibleIndex = 2;
            this.colEstimatedBillAmount.Width = 83;
            // 
            // repositoryItemTextEditCurrency3
            // 
            this.repositoryItemTextEditCurrency3.AutoHeight = false;
            this.repositoryItemTextEditCurrency3.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency3.Name = "repositoryItemTextEditCurrency3";
            // 
            // colActualBillAmount
            // 
            this.colActualBillAmount.Caption = "Actual Bill";
            this.colActualBillAmount.ColumnEdit = this.repositoryItemTextEditCurrency3;
            this.colActualBillAmount.FieldName = "ActualBillAmount";
            this.colActualBillAmount.Name = "colActualBillAmount";
            this.colActualBillAmount.OptionsColumn.AllowEdit = false;
            this.colActualBillAmount.OptionsColumn.AllowFocus = false;
            this.colActualBillAmount.OptionsColumn.ReadOnly = true;
            this.colActualBillAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ActualBillAmount", "{0:C}")});
            this.colActualBillAmount.Visible = true;
            this.colActualBillAmount.VisibleIndex = 3;
            // 
            // colBilledByPersonID
            // 
            this.colBilledByPersonID.Caption = "Billed By Person ID";
            this.colBilledByPersonID.FieldName = "BilledByPersonID";
            this.colBilledByPersonID.Name = "colBilledByPersonID";
            this.colBilledByPersonID.OptionsColumn.AllowEdit = false;
            this.colBilledByPersonID.OptionsColumn.AllowFocus = false;
            this.colBilledByPersonID.OptionsColumn.ReadOnly = true;
            this.colBilledByPersonID.Width = 110;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colSiteContractID2
            // 
            this.colSiteContractID2.Caption = "Site Contract ID";
            this.colSiteContractID2.FieldName = "SiteContractID";
            this.colSiteContractID2.Name = "colSiteContractID2";
            this.colSiteContractID2.OptionsColumn.AllowEdit = false;
            this.colSiteContractID2.OptionsColumn.AllowFocus = false;
            this.colSiteContractID2.OptionsColumn.ReadOnly = true;
            this.colSiteContractID2.Width = 107;
            // 
            // colYearDescription1
            // 
            this.colYearDescription1.Caption = "Year Description";
            this.colYearDescription1.FieldName = "YearDescription";
            this.colYearDescription1.Name = "colYearDescription1";
            this.colYearDescription1.OptionsColumn.AllowEdit = false;
            this.colYearDescription1.OptionsColumn.AllowFocus = false;
            this.colYearDescription1.OptionsColumn.ReadOnly = true;
            this.colYearDescription1.Width = 99;
            // 
            // colYearStartDate
            // 
            this.colYearStartDate.Caption = "Year Start";
            this.colYearStartDate.ColumnEdit = this.repositoryItemTextEditDateEdit3;
            this.colYearStartDate.FieldName = "YearStartDate";
            this.colYearStartDate.Name = "colYearStartDate";
            this.colYearStartDate.OptionsColumn.AllowEdit = false;
            this.colYearStartDate.OptionsColumn.AllowFocus = false;
            this.colYearStartDate.OptionsColumn.ReadOnly = true;
            // 
            // colYearEndDate
            // 
            this.colYearEndDate.Caption = "Year End";
            this.colYearEndDate.ColumnEdit = this.repositoryItemTextEditDateEdit3;
            this.colYearEndDate.FieldName = "YearEndDate";
            this.colYearEndDate.Name = "colYearEndDate";
            this.colYearEndDate.OptionsColumn.AllowEdit = false;
            this.colYearEndDate.OptionsColumn.AllowFocus = false;
            this.colYearEndDate.OptionsColumn.ReadOnly = true;
            // 
            // colYearActive
            // 
            this.colYearActive.Caption = "Year Active";
            this.colYearActive.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colYearActive.FieldName = "YearActive";
            this.colYearActive.Name = "colYearActive";
            this.colYearActive.OptionsColumn.AllowEdit = false;
            this.colYearActive.OptionsColumn.AllowFocus = false;
            this.colYearActive.OptionsColumn.ReadOnly = true;
            this.colYearActive.Width = 76;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 173;
            // 
            // colContractStartDate1
            // 
            this.colContractStartDate1.Caption = "Contract Start Date";
            this.colContractStartDate1.ColumnEdit = this.repositoryItemTextEditDateEdit3;
            this.colContractStartDate1.FieldName = "ContractStartDate";
            this.colContractStartDate1.Name = "colContractStartDate1";
            this.colContractStartDate1.OptionsColumn.AllowEdit = false;
            this.colContractStartDate1.OptionsColumn.AllowFocus = false;
            this.colContractStartDate1.OptionsColumn.ReadOnly = true;
            this.colContractStartDate1.Width = 116;
            // 
            // colContractEndDate1
            // 
            this.colContractEndDate1.Caption = "Contract End Date";
            this.colContractEndDate1.ColumnEdit = this.repositoryItemTextEditDateEdit3;
            this.colContractEndDate1.FieldName = "ContractEndDate";
            this.colContractEndDate1.Name = "colContractEndDate1";
            this.colContractEndDate1.OptionsColumn.AllowEdit = false;
            this.colContractEndDate1.OptionsColumn.AllowFocus = false;
            this.colContractEndDate1.OptionsColumn.ReadOnly = true;
            this.colContractEndDate1.Width = 110;
            // 
            // colLinkedToParent1
            // 
            this.colLinkedToParent1.Caption = "Linked To Contract";
            this.colLinkedToParent1.ColumnEdit = this.repositoryItemTextEditHTML3;
            this.colLinkedToParent1.FieldName = "LinkedToParent";
            this.colLinkedToParent1.Name = "colLinkedToParent1";
            this.colLinkedToParent1.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent1.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent1.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent1.Visible = true;
            this.colLinkedToParent1.VisibleIndex = 6;
            this.colLinkedToParent1.Width = 524;
            // 
            // repositoryItemTextEditHTML3
            // 
            this.repositoryItemTextEditHTML3.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML3.AutoHeight = false;
            this.repositoryItemTextEditHTML3.Name = "repositoryItemTextEditHTML3";
            // 
            // colBilledByPerson
            // 
            this.colBilledByPerson.Caption = "Billed By Person";
            this.colBilledByPerson.FieldName = "BilledByPerson";
            this.colBilledByPerson.Name = "colBilledByPerson";
            this.colBilledByPerson.OptionsColumn.AllowEdit = false;
            this.colBilledByPerson.OptionsColumn.AllowFocus = false;
            this.colBilledByPerson.OptionsColumn.ReadOnly = true;
            this.colBilledByPerson.Visible = true;
            this.colBilledByPerson.VisibleIndex = 4;
            this.colBilledByPerson.Width = 161;
            // 
            // colSiteID2
            // 
            this.colSiteID2.Caption = "Site ID";
            this.colSiteID2.FieldName = "SiteID";
            this.colSiteID2.Name = "colSiteID2";
            this.colSiteID2.OptionsColumn.AllowEdit = false;
            this.colSiteID2.OptionsColumn.AllowFocus = false;
            this.colSiteID2.OptionsColumn.ReadOnly = true;
            this.colSiteID2.Visible = true;
            this.colSiteID2.VisibleIndex = 6;
            this.colSiteID2.Width = 51;
            // 
            // colSiteName2
            // 
            this.colSiteName2.Caption = "Site Name";
            this.colSiteName2.FieldName = "SiteName";
            this.colSiteName2.Name = "colSiteName2";
            this.colSiteName2.OptionsColumn.AllowEdit = false;
            this.colSiteName2.OptionsColumn.AllowFocus = false;
            this.colSiteName2.OptionsColumn.ReadOnly = true;
            this.colSiteName2.Width = 291;
            // 
            // colLinkedToParentFull1
            // 
            this.colLinkedToParentFull1.Caption = "Linked To Site";
            this.colLinkedToParentFull1.ColumnEdit = this.repositoryItemTextEditHTML3;
            this.colLinkedToParentFull1.FieldName = "LinkedToParentFull";
            this.colLinkedToParentFull1.Name = "colLinkedToParentFull1";
            this.colLinkedToParentFull1.OptionsColumn.AllowEdit = false;
            this.colLinkedToParentFull1.OptionsColumn.AllowFocus = false;
            this.colLinkedToParentFull1.OptionsColumn.ReadOnly = true;
            this.colLinkedToParentFull1.Width = 314;
            // 
            // colCreatedFromClientContractProfileID
            // 
            this.colCreatedFromClientContractProfileID.Caption = "Created From Client Contract Year Profile ID";
            this.colCreatedFromClientContractProfileID.FieldName = "CreatedFromClientContractProfileID";
            this.colCreatedFromClientContractProfileID.Name = "colCreatedFromClientContractProfileID";
            this.colCreatedFromClientContractProfileID.OptionsColumn.AllowEdit = false;
            this.colCreatedFromClientContractProfileID.OptionsColumn.AllowFocus = false;
            this.colCreatedFromClientContractProfileID.OptionsColumn.ReadOnly = true;
            this.colCreatedFromClientContractProfileID.Width = 234;
            // 
            // xtraTabPageResponsibilities
            // 
            this.xtraTabPageResponsibilities.Controls.Add(this.gridControlResponsibility);
            this.xtraTabPageResponsibilities.Name = "xtraTabPageResponsibilities";
            this.xtraTabPageResponsibilities.Size = new System.Drawing.Size(999, 203);
            this.xtraTabPageResponsibilities.Text = "Person Responsibilities";
            // 
            // gridControlResponsibility
            // 
            this.gridControlResponsibility.DataSource = this.sp06112OMSiteContractManagerLinkedResponsibilitiesBindingSource;
            this.gridControlResponsibility.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlResponsibility.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view")});
            this.gridControlResponsibility.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlResponsibility_EmbeddedNavigator_ButtonClick);
            this.gridControlResponsibility.Location = new System.Drawing.Point(0, 0);
            this.gridControlResponsibility.MainView = this.gridViewResponsibility;
            this.gridControlResponsibility.MenuManager = this.barManager1;
            this.gridControlResponsibility.Name = "gridControlResponsibility";
            this.gridControlResponsibility.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemTextEditDateTime4,
            this.repositoryItemTextEditHTML4});
            this.gridControlResponsibility.Size = new System.Drawing.Size(999, 203);
            this.gridControlResponsibility.TabIndex = 6;
            this.gridControlResponsibility.UseEmbeddedNavigator = true;
            this.gridControlResponsibility.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewResponsibility});
            // 
            // sp06112OMSiteContractManagerLinkedResponsibilitiesBindingSource
            // 
            this.sp06112OMSiteContractManagerLinkedResponsibilitiesBindingSource.DataMember = "sp06112_OM_Site_Contract_Manager_Linked_Responsibilities";
            this.sp06112OMSiteContractManagerLinkedResponsibilitiesBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridViewResponsibility
            // 
            this.gridViewResponsibility.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPersonResponsibilityID,
            this.colStaffID,
            this.colResponsibleForRecordID,
            this.colResponsibleForRecordTypeID,
            this.colResponsibilityTypeID,
            this.colRemarks3,
            this.colClientID3,
            this.colClientName3,
            this.colContractStartDate2,
            this.colContractEndDate2,
            this.colLinkedToParent2,
            this.colResponsibilityType,
            this.colStaffName,
            this.colSiteID3,
            this.colSiteName3,
            this.colLinkedToParentFull2,
            this.colPersonTypeDescription,
            this.colPersonTypeID});
            this.gridViewResponsibility.GridControl = this.gridControlResponsibility;
            this.gridViewResponsibility.GroupCount = 2;
            this.gridViewResponsibility.Name = "gridViewResponsibility";
            this.gridViewResponsibility.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewResponsibility.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewResponsibility.OptionsLayout.StoreAppearance = true;
            this.gridViewResponsibility.OptionsLayout.StoreFormatRules = true;
            this.gridViewResponsibility.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewResponsibility.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewResponsibility.OptionsSelection.MultiSelect = true;
            this.gridViewResponsibility.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewResponsibility.OptionsView.ColumnAutoWidth = false;
            this.gridViewResponsibility.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewResponsibility.OptionsView.ShowGroupPanel = false;
            this.gridViewResponsibility.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colResponsibilityType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStaffName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewResponsibility.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewResponsibility.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewResponsibility.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewResponsibility.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewResponsibility.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewResponsibility.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewResponsibility.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewResponsibility_MouseUp);
            this.gridViewResponsibility.DoubleClick += new System.EventHandler(this.gridViewResponsibility_DoubleClick);
            this.gridViewResponsibility.GotFocus += new System.EventHandler(this.gridViewResponsibility_GotFocus);
            // 
            // colPersonResponsibilityID
            // 
            this.colPersonResponsibilityID.Caption = "Person Responsibility ID";
            this.colPersonResponsibilityID.FieldName = "PersonResponsibilityID";
            this.colPersonResponsibilityID.Name = "colPersonResponsibilityID";
            this.colPersonResponsibilityID.OptionsColumn.AllowEdit = false;
            this.colPersonResponsibilityID.OptionsColumn.AllowFocus = false;
            this.colPersonResponsibilityID.OptionsColumn.ReadOnly = true;
            this.colPersonResponsibilityID.Width = 136;
            // 
            // colStaffID
            // 
            this.colStaffID.Caption = "Staff ID";
            this.colStaffID.FieldName = "StaffID";
            this.colStaffID.Name = "colStaffID";
            this.colStaffID.OptionsColumn.AllowEdit = false;
            this.colStaffID.OptionsColumn.AllowFocus = false;
            this.colStaffID.OptionsColumn.ReadOnly = true;
            // 
            // colResponsibleForRecordID
            // 
            this.colResponsibleForRecordID.Caption = "Responsible For Record ID";
            this.colResponsibleForRecordID.FieldName = "ResponsibleForRecordID";
            this.colResponsibleForRecordID.Name = "colResponsibleForRecordID";
            this.colResponsibleForRecordID.OptionsColumn.AllowEdit = false;
            this.colResponsibleForRecordID.OptionsColumn.AllowFocus = false;
            this.colResponsibleForRecordID.OptionsColumn.ReadOnly = true;
            this.colResponsibleForRecordID.Width = 148;
            // 
            // colResponsibleForRecordTypeID
            // 
            this.colResponsibleForRecordTypeID.Caption = "Responsible For Record Type ID";
            this.colResponsibleForRecordTypeID.FieldName = "ResponsibleForRecordTypeID";
            this.colResponsibleForRecordTypeID.Name = "colResponsibleForRecordTypeID";
            this.colResponsibleForRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colResponsibleForRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colResponsibleForRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colResponsibleForRecordTypeID.Width = 175;
            // 
            // colResponsibilityTypeID
            // 
            this.colResponsibilityTypeID.Caption = "Responsibility Type ID";
            this.colResponsibilityTypeID.FieldName = "ResponsibilityTypeID";
            this.colResponsibilityTypeID.Name = "colResponsibilityTypeID";
            this.colResponsibilityTypeID.OptionsColumn.AllowEdit = false;
            this.colResponsibilityTypeID.OptionsColumn.AllowFocus = false;
            this.colResponsibilityTypeID.OptionsColumn.ReadOnly = true;
            this.colResponsibilityTypeID.Width = 127;
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.OptionsColumn.ReadOnly = true;
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 3;
            this.colRemarks3.Width = 179;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colClientID3
            // 
            this.colClientID3.Caption = "Client ID";
            this.colClientID3.FieldName = "ClientID";
            this.colClientID3.Name = "colClientID3";
            this.colClientID3.OptionsColumn.AllowEdit = false;
            this.colClientID3.OptionsColumn.AllowFocus = false;
            this.colClientID3.OptionsColumn.ReadOnly = true;
            // 
            // colClientName3
            // 
            this.colClientName3.Caption = "Client Name";
            this.colClientName3.FieldName = "ClientName";
            this.colClientName3.Name = "colClientName3";
            this.colClientName3.OptionsColumn.AllowEdit = false;
            this.colClientName3.OptionsColumn.AllowFocus = false;
            this.colClientName3.OptionsColumn.ReadOnly = true;
            this.colClientName3.Width = 169;
            // 
            // colContractStartDate2
            // 
            this.colContractStartDate2.Caption = "Contract Start";
            this.colContractStartDate2.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colContractStartDate2.FieldName = "ContractStartDate";
            this.colContractStartDate2.Name = "colContractStartDate2";
            this.colContractStartDate2.OptionsColumn.AllowEdit = false;
            this.colContractStartDate2.OptionsColumn.AllowFocus = false;
            this.colContractStartDate2.OptionsColumn.ReadOnly = true;
            this.colContractStartDate2.Width = 106;
            // 
            // repositoryItemTextEditDateTime4
            // 
            this.repositoryItemTextEditDateTime4.AutoHeight = false;
            this.repositoryItemTextEditDateTime4.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime4.Name = "repositoryItemTextEditDateTime4";
            // 
            // colContractEndDate2
            // 
            this.colContractEndDate2.Caption = "Contract End";
            this.colContractEndDate2.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colContractEndDate2.FieldName = "ContractEndDate";
            this.colContractEndDate2.Name = "colContractEndDate2";
            this.colContractEndDate2.OptionsColumn.AllowEdit = false;
            this.colContractEndDate2.OptionsColumn.AllowFocus = false;
            this.colContractEndDate2.OptionsColumn.ReadOnly = true;
            this.colContractEndDate2.Width = 102;
            // 
            // colLinkedToParent2
            // 
            this.colLinkedToParent2.Caption = "Linked To Contract";
            this.colLinkedToParent2.ColumnEdit = this.repositoryItemTextEditHTML4;
            this.colLinkedToParent2.FieldName = "LinkedToParent";
            this.colLinkedToParent2.Name = "colLinkedToParent2";
            this.colLinkedToParent2.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent2.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent2.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent2.Visible = true;
            this.colLinkedToParent2.VisibleIndex = 1;
            this.colLinkedToParent2.Width = 319;
            // 
            // repositoryItemTextEditHTML4
            // 
            this.repositoryItemTextEditHTML4.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML4.AutoHeight = false;
            this.repositoryItemTextEditHTML4.Name = "repositoryItemTextEditHTML4";
            // 
            // colResponsibilityType
            // 
            this.colResponsibilityType.Caption = "Responsibility Type";
            this.colResponsibilityType.FieldName = "ResponsibilityType";
            this.colResponsibilityType.Name = "colResponsibilityType";
            this.colResponsibilityType.OptionsColumn.AllowEdit = false;
            this.colResponsibilityType.OptionsColumn.AllowFocus = false;
            this.colResponsibilityType.OptionsColumn.ReadOnly = true;
            this.colResponsibilityType.Visible = true;
            this.colResponsibilityType.VisibleIndex = 0;
            this.colResponsibilityType.Width = 176;
            // 
            // colStaffName
            // 
            this.colStaffName.Caption = "Person Name";
            this.colStaffName.FieldName = "StaffName";
            this.colStaffName.Name = "colStaffName";
            this.colStaffName.OptionsColumn.AllowEdit = false;
            this.colStaffName.OptionsColumn.AllowFocus = false;
            this.colStaffName.OptionsColumn.ReadOnly = true;
            this.colStaffName.Visible = true;
            this.colStaffName.VisibleIndex = 2;
            this.colStaffName.Width = 183;
            // 
            // colSiteID3
            // 
            this.colSiteID3.Caption = "Site ID";
            this.colSiteID3.FieldName = "SiteID";
            this.colSiteID3.Name = "colSiteID3";
            this.colSiteID3.OptionsColumn.AllowEdit = false;
            this.colSiteID3.OptionsColumn.AllowFocus = false;
            this.colSiteID3.OptionsColumn.ReadOnly = true;
            this.colSiteID3.Visible = true;
            this.colSiteID3.VisibleIndex = 4;
            this.colSiteID3.Width = 51;
            // 
            // colSiteName3
            // 
            this.colSiteName3.Caption = "Site Name";
            this.colSiteName3.FieldName = "SiteName";
            this.colSiteName3.Name = "colSiteName3";
            this.colSiteName3.OptionsColumn.AllowEdit = false;
            this.colSiteName3.OptionsColumn.AllowFocus = false;
            this.colSiteName3.OptionsColumn.ReadOnly = true;
            this.colSiteName3.Visible = true;
            this.colSiteName3.VisibleIndex = 6;
            this.colSiteName3.Width = 208;
            // 
            // colLinkedToParentFull2
            // 
            this.colLinkedToParentFull2.Caption = "Linked To Site";
            this.colLinkedToParentFull2.ColumnEdit = this.repositoryItemTextEditHTML4;
            this.colLinkedToParentFull2.FieldName = "LinkedToParentFull";
            this.colLinkedToParentFull2.Name = "colLinkedToParentFull2";
            this.colLinkedToParentFull2.OptionsColumn.AllowEdit = false;
            this.colLinkedToParentFull2.OptionsColumn.AllowFocus = false;
            this.colLinkedToParentFull2.OptionsColumn.ReadOnly = true;
            this.colLinkedToParentFull2.Width = 397;
            // 
            // colPersonTypeDescription
            // 
            this.colPersonTypeDescription.Caption = "Person Type";
            this.colPersonTypeDescription.FieldName = "PersonTypeDescription";
            this.colPersonTypeDescription.Name = "colPersonTypeDescription";
            this.colPersonTypeDescription.OptionsColumn.AllowEdit = false;
            this.colPersonTypeDescription.OptionsColumn.AllowFocus = false;
            this.colPersonTypeDescription.OptionsColumn.ReadOnly = true;
            this.colPersonTypeDescription.Visible = true;
            this.colPersonTypeDescription.VisibleIndex = 1;
            this.colPersonTypeDescription.Width = 114;
            // 
            // colPersonTypeID
            // 
            this.colPersonTypeID.Caption = "Person Type ID";
            this.colPersonTypeID.FieldName = "PersonTypeID";
            this.colPersonTypeID.Name = "colPersonTypeID";
            this.colPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colPersonTypeID.Width = 93;
            // 
            // xtraTabPageLabour
            // 
            this.xtraTabPageLabour.Controls.Add(this.gridControlLabour);
            this.xtraTabPageLabour.Name = "xtraTabPageLabour";
            this.xtraTabPageLabour.Size = new System.Drawing.Size(999, 203);
            this.xtraTabPageLabour.Text = "Preferred Labour";
            // 
            // gridControlLabour
            // 
            this.gridControlLabour.DataSource = this.sp06113OMSiteContractManagerLinkedLabourBindingSource;
            this.gridControlLabour.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlLabour.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlLabour.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 12, true, true, "Block Add Records", "block_add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 13, true, true, "Re-assign Default Labour and Update Outstanding Jobs", "reassign_labour")});
            this.gridControlLabour.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlLabour_EmbeddedNavigator_ButtonClick);
            this.gridControlLabour.Location = new System.Drawing.Point(0, 0);
            this.gridControlLabour.MainView = this.gridViewLabour;
            this.gridControlLabour.MenuManager = this.barManager1;
            this.gridControlLabour.Name = "gridControlLabour";
            this.gridControlLabour.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit9,
            this.repositoryItemTextEditMoney,
            this.repositoryItemTextEditPercent,
            this.repositoryItemTextEditMiles,
            this.repositoryItemTextEditDateTime3,
            this.repositoryItemTextEditHTML5});
            this.gridControlLabour.Size = new System.Drawing.Size(999, 203);
            this.gridControlLabour.TabIndex = 25;
            this.gridControlLabour.UseEmbeddedNavigator = true;
            this.gridControlLabour.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLabour});
            // 
            // sp06113OMSiteContractManagerLinkedLabourBindingSource
            // 
            this.sp06113OMSiteContractManagerLinkedLabourBindingSource.DataMember = "sp06113_OM_Site_Contract_Manager_Linked_Labour";
            this.sp06113OMSiteContractManagerLinkedLabourBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridViewLabour
            // 
            this.gridViewLabour.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteContractLabourCostID,
            this.gridColumn1,
            this.colContractorID,
            this.colCostUnitDescriptorID,
            this.colSellUnitDescriptorID,
            this.colCostPerUnitExVat,
            this.colCostPerUnitVatRate,
            this.colSellPerUnitExVat,
            this.colSellPerUnitVatRate,
            this.colCISPercentage,
            this.colCISValue,
            this.colPostcodeSiteDistance,
            this.colLatLongSiteDistance,
            this.colRemarks6,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.colSiteName4,
            this.colContractorName,
            this.colLinkedToParent4,
            this.colLinkedToParentFull3,
            this.colSiteLocationX,
            this.colSiteLocationY,
            this.colSiteID4,
            this.colSitePostcode,
            this.colLinkedToPersonType,
            this.colLinkedToPersonTypeID});
            this.gridViewLabour.GridControl = this.gridControlLabour;
            this.gridViewLabour.GroupCount = 2;
            this.gridViewLabour.Name = "gridViewLabour";
            this.gridViewLabour.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewLabour.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewLabour.OptionsLayout.StoreAppearance = true;
            this.gridViewLabour.OptionsLayout.StoreFormatRules = true;
            this.gridViewLabour.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewLabour.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewLabour.OptionsSelection.MultiSelect = true;
            this.gridViewLabour.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewLabour.OptionsView.ColumnAutoWidth = false;
            this.gridViewLabour.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewLabour.OptionsView.ShowGroupPanel = false;
            this.gridViewLabour.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent4, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName4, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToPersonType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewLabour.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewLabour.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewLabour.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewLabour.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewLabour.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewLabour.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewLabour.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewLabour_MouseUp);
            this.gridViewLabour.DoubleClick += new System.EventHandler(this.gridViewLabour_DoubleClick);
            this.gridViewLabour.GotFocus += new System.EventHandler(this.gridViewLabour_GotFocus);
            // 
            // colSiteContractLabourCostID
            // 
            this.colSiteContractLabourCostID.Caption = "Site Contract Labour Cost ID";
            this.colSiteContractLabourCostID.FieldName = "SiteContractLabourCostID";
            this.colSiteContractLabourCostID.Name = "colSiteContractLabourCostID";
            this.colSiteContractLabourCostID.OptionsColumn.AllowEdit = false;
            this.colSiteContractLabourCostID.OptionsColumn.AllowFocus = false;
            this.colSiteContractLabourCostID.OptionsColumn.ReadOnly = true;
            this.colSiteContractLabourCostID.Width = 159;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Site Contract ID";
            this.gridColumn1.FieldName = "SiteContractID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 98;
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            // 
            // colCostUnitDescriptorID
            // 
            this.colCostUnitDescriptorID.Caption = "Cost Unit Descriptor";
            this.colCostUnitDescriptorID.FieldName = "CostUnitDescriptor";
            this.colCostUnitDescriptorID.Name = "colCostUnitDescriptorID";
            this.colCostUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colCostUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colCostUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colCostUnitDescriptorID.Visible = true;
            this.colCostUnitDescriptorID.VisibleIndex = 2;
            this.colCostUnitDescriptorID.Width = 117;
            // 
            // colSellUnitDescriptorID
            // 
            this.colSellUnitDescriptorID.Caption = "Sell Unit Descriptor";
            this.colSellUnitDescriptorID.FieldName = "SellUnitDescriptor";
            this.colSellUnitDescriptorID.Name = "colSellUnitDescriptorID";
            this.colSellUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colSellUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colSellUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colSellUnitDescriptorID.Visible = true;
            this.colSellUnitDescriptorID.VisibleIndex = 5;
            this.colSellUnitDescriptorID.Width = 111;
            // 
            // colCostPerUnitExVat
            // 
            this.colCostPerUnitExVat.Caption = "Cost Per Unit Ex VAT";
            this.colCostPerUnitExVat.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colCostPerUnitExVat.FieldName = "CostPerUnitExVat";
            this.colCostPerUnitExVat.Name = "colCostPerUnitExVat";
            this.colCostPerUnitExVat.OptionsColumn.AllowEdit = false;
            this.colCostPerUnitExVat.OptionsColumn.AllowFocus = false;
            this.colCostPerUnitExVat.OptionsColumn.ReadOnly = true;
            this.colCostPerUnitExVat.Visible = true;
            this.colCostPerUnitExVat.VisibleIndex = 3;
            this.colCostPerUnitExVat.Width = 121;
            // 
            // repositoryItemTextEditMoney
            // 
            this.repositoryItemTextEditMoney.AutoHeight = false;
            this.repositoryItemTextEditMoney.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney.Mask.ShowPlaceHolders = false;
            this.repositoryItemTextEditMoney.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney.Name = "repositoryItemTextEditMoney";
            // 
            // colCostPerUnitVatRate
            // 
            this.colCostPerUnitVatRate.Caption = "Cost Per Unit VAT Rate";
            this.colCostPerUnitVatRate.ColumnEdit = this.repositoryItemTextEditPercent;
            this.colCostPerUnitVatRate.FieldName = "CostPerUnitVatRate";
            this.colCostPerUnitVatRate.Name = "colCostPerUnitVatRate";
            this.colCostPerUnitVatRate.OptionsColumn.AllowEdit = false;
            this.colCostPerUnitVatRate.OptionsColumn.AllowFocus = false;
            this.colCostPerUnitVatRate.OptionsColumn.ReadOnly = true;
            this.colCostPerUnitVatRate.Visible = true;
            this.colCostPerUnitVatRate.VisibleIndex = 4;
            this.colCostPerUnitVatRate.Width = 132;
            // 
            // repositoryItemTextEditPercent
            // 
            this.repositoryItemTextEditPercent.AutoHeight = false;
            this.repositoryItemTextEditPercent.Mask.EditMask = "P";
            this.repositoryItemTextEditPercent.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercent.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercent.Name = "repositoryItemTextEditPercent";
            // 
            // colSellPerUnitExVat
            // 
            this.colSellPerUnitExVat.Caption = "Sell Per Unit Ex VAT";
            this.colSellPerUnitExVat.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colSellPerUnitExVat.FieldName = "SellPerUnitExVat";
            this.colSellPerUnitExVat.Name = "colSellPerUnitExVat";
            this.colSellPerUnitExVat.OptionsColumn.AllowEdit = false;
            this.colSellPerUnitExVat.OptionsColumn.AllowFocus = false;
            this.colSellPerUnitExVat.OptionsColumn.ReadOnly = true;
            this.colSellPerUnitExVat.Visible = true;
            this.colSellPerUnitExVat.VisibleIndex = 6;
            this.colSellPerUnitExVat.Width = 115;
            // 
            // colSellPerUnitVatRate
            // 
            this.colSellPerUnitVatRate.Caption = "Sell Per Unit VAT Rate";
            this.colSellPerUnitVatRate.ColumnEdit = this.repositoryItemTextEditPercent;
            this.colSellPerUnitVatRate.FieldName = "SellPerUnitVatRate";
            this.colSellPerUnitVatRate.Name = "colSellPerUnitVatRate";
            this.colSellPerUnitVatRate.OptionsColumn.AllowEdit = false;
            this.colSellPerUnitVatRate.OptionsColumn.AllowFocus = false;
            this.colSellPerUnitVatRate.OptionsColumn.ReadOnly = true;
            this.colSellPerUnitVatRate.Visible = true;
            this.colSellPerUnitVatRate.VisibleIndex = 7;
            this.colSellPerUnitVatRate.Width = 126;
            // 
            // colCISPercentage
            // 
            this.colCISPercentage.Caption = "CIS %";
            this.colCISPercentage.ColumnEdit = this.repositoryItemTextEditPercent;
            this.colCISPercentage.FieldName = "CISPercentage";
            this.colCISPercentage.Name = "colCISPercentage";
            this.colCISPercentage.OptionsColumn.AllowEdit = false;
            this.colCISPercentage.OptionsColumn.AllowFocus = false;
            this.colCISPercentage.OptionsColumn.ReadOnly = true;
            this.colCISPercentage.Visible = true;
            this.colCISPercentage.VisibleIndex = 8;
            this.colCISPercentage.Width = 52;
            // 
            // colCISValue
            // 
            this.colCISValue.Caption = "CIS Value";
            this.colCISValue.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colCISValue.FieldName = "CISValue";
            this.colCISValue.Name = "colCISValue";
            this.colCISValue.OptionsColumn.AllowEdit = false;
            this.colCISValue.OptionsColumn.AllowFocus = false;
            this.colCISValue.OptionsColumn.ReadOnly = true;
            this.colCISValue.Visible = true;
            this.colCISValue.VisibleIndex = 9;
            this.colCISValue.Width = 67;
            // 
            // colPostcodeSiteDistance
            // 
            this.colPostcodeSiteDistance.Caption = "Distance From Site Postcode";
            this.colPostcodeSiteDistance.ColumnEdit = this.repositoryItemTextEditMiles;
            this.colPostcodeSiteDistance.FieldName = "PostcodeSiteDistance";
            this.colPostcodeSiteDistance.Name = "colPostcodeSiteDistance";
            this.colPostcodeSiteDistance.OptionsColumn.AllowEdit = false;
            this.colPostcodeSiteDistance.OptionsColumn.AllowFocus = false;
            this.colPostcodeSiteDistance.OptionsColumn.ReadOnly = true;
            this.colPostcodeSiteDistance.Visible = true;
            this.colPostcodeSiteDistance.VisibleIndex = 10;
            this.colPostcodeSiteDistance.Width = 157;
            // 
            // repositoryItemTextEditMiles
            // 
            this.repositoryItemTextEditMiles.AutoHeight = false;
            this.repositoryItemTextEditMiles.Mask.EditMask = "########0.00 Miles";
            this.repositoryItemTextEditMiles.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMiles.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMiles.Name = "repositoryItemTextEditMiles";
            // 
            // colLatLongSiteDistance
            // 
            this.colLatLongSiteDistance.Caption = "Distance From Site Lat\\Long";
            this.colLatLongSiteDistance.ColumnEdit = this.repositoryItemTextEditMiles;
            this.colLatLongSiteDistance.FieldName = "LatLongSiteDistance";
            this.colLatLongSiteDistance.Name = "colLatLongSiteDistance";
            this.colLatLongSiteDistance.OptionsColumn.AllowEdit = false;
            this.colLatLongSiteDistance.OptionsColumn.AllowFocus = false;
            this.colLatLongSiteDistance.OptionsColumn.ReadOnly = true;
            this.colLatLongSiteDistance.Visible = true;
            this.colLatLongSiteDistance.VisibleIndex = 11;
            this.colLatLongSiteDistance.Width = 155;
            // 
            // colRemarks6
            // 
            this.colRemarks6.Caption = "Remarks";
            this.colRemarks6.ColumnEdit = this.repositoryItemMemoExEdit9;
            this.colRemarks6.FieldName = "Remarks";
            this.colRemarks6.Name = "colRemarks6";
            this.colRemarks6.OptionsColumn.ReadOnly = true;
            this.colRemarks6.Visible = true;
            this.colRemarks6.VisibleIndex = 12;
            // 
            // repositoryItemMemoExEdit9
            // 
            this.repositoryItemMemoExEdit9.AutoHeight = false;
            this.repositoryItemMemoExEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit9.Name = "repositoryItemMemoExEdit9";
            this.repositoryItemMemoExEdit9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit9.ShowIcon = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Client Name";
            this.gridColumn2.FieldName = "ClientName";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 166;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Contract Start";
            this.gridColumn3.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.gridColumn3.FieldName = "ContractStartDate";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 90;
            // 
            // repositoryItemTextEditDateTime3
            // 
            this.repositoryItemTextEditDateTime3.AutoHeight = false;
            this.repositoryItemTextEditDateTime3.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime3.Name = "repositoryItemTextEditDateTime3";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Contract End";
            this.gridColumn4.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.gridColumn4.FieldName = "ContractEndDate";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 84;
            // 
            // colSiteName4
            // 
            this.colSiteName4.Caption = "Site Name";
            this.colSiteName4.FieldName = "SiteName";
            this.colSiteName4.Name = "colSiteName4";
            this.colSiteName4.OptionsColumn.AllowEdit = false;
            this.colSiteName4.OptionsColumn.AllowFocus = false;
            this.colSiteName4.OptionsColumn.ReadOnly = true;
            this.colSiteName4.Visible = true;
            this.colSiteName4.VisibleIndex = 15;
            this.colSiteName4.Width = 191;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Labour Name";
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.OptionsColumn.AllowEdit = false;
            this.colContractorName.OptionsColumn.AllowFocus = false;
            this.colContractorName.OptionsColumn.ReadOnly = true;
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 1;
            this.colContractorName.Width = 176;
            // 
            // colLinkedToParent4
            // 
            this.colLinkedToParent4.Caption = "Linked To Contract";
            this.colLinkedToParent4.ColumnEdit = this.repositoryItemTextEditHTML5;
            this.colLinkedToParent4.FieldName = "LinkedToParent";
            this.colLinkedToParent4.Name = "colLinkedToParent4";
            this.colLinkedToParent4.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent4.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent4.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent4.Visible = true;
            this.colLinkedToParent4.VisibleIndex = 13;
            this.colLinkedToParent4.Width = 329;
            // 
            // repositoryItemTextEditHTML5
            // 
            this.repositoryItemTextEditHTML5.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML5.AutoHeight = false;
            this.repositoryItemTextEditHTML5.Name = "repositoryItemTextEditHTML5";
            // 
            // colLinkedToParentFull3
            // 
            this.colLinkedToParentFull3.Caption = "Linked To Site";
            this.colLinkedToParentFull3.ColumnEdit = this.repositoryItemTextEditHTML5;
            this.colLinkedToParentFull3.FieldName = "LinkedToParentFull";
            this.colLinkedToParentFull3.Name = "colLinkedToParentFull3";
            this.colLinkedToParentFull3.OptionsColumn.AllowEdit = false;
            this.colLinkedToParentFull3.OptionsColumn.AllowFocus = false;
            this.colLinkedToParentFull3.OptionsColumn.ReadOnly = true;
            this.colLinkedToParentFull3.Width = 87;
            // 
            // colSiteLocationX
            // 
            this.colSiteLocationX.Caption = "Site Latitude";
            this.colSiteLocationX.FieldName = "SiteLocationX";
            this.colSiteLocationX.Name = "colSiteLocationX";
            this.colSiteLocationX.OptionsColumn.AllowEdit = false;
            this.colSiteLocationX.OptionsColumn.AllowFocus = false;
            this.colSiteLocationX.OptionsColumn.ReadOnly = true;
            this.colSiteLocationX.Width = 107;
            // 
            // colSiteLocationY
            // 
            this.colSiteLocationY.Caption = "Site Longitude";
            this.colSiteLocationY.FieldName = "SiteLocationY";
            this.colSiteLocationY.Name = "colSiteLocationY";
            this.colSiteLocationY.OptionsColumn.AllowEdit = false;
            this.colSiteLocationY.OptionsColumn.AllowFocus = false;
            this.colSiteLocationY.OptionsColumn.ReadOnly = true;
            this.colSiteLocationY.Width = 118;
            // 
            // colSiteID4
            // 
            this.colSiteID4.Caption = "Site ID";
            this.colSiteID4.FieldName = "SiteID";
            this.colSiteID4.Name = "colSiteID4";
            this.colSiteID4.OptionsColumn.AllowEdit = false;
            this.colSiteID4.OptionsColumn.AllowFocus = false;
            this.colSiteID4.OptionsColumn.ReadOnly = true;
            this.colSiteID4.Visible = true;
            this.colSiteID4.VisibleIndex = 13;
            this.colSiteID4.Width = 51;
            // 
            // colSitePostcode
            // 
            this.colSitePostcode.Caption = "Site Postcode";
            this.colSitePostcode.FieldName = "SitePostcode";
            this.colSitePostcode.Name = "colSitePostcode";
            this.colSitePostcode.OptionsColumn.AllowEdit = false;
            this.colSitePostcode.OptionsColumn.AllowFocus = false;
            this.colSitePostcode.OptionsColumn.ReadOnly = true;
            this.colSitePostcode.Width = 86;
            // 
            // colLinkedToPersonType
            // 
            this.colLinkedToPersonType.Caption = "Labour Type";
            this.colLinkedToPersonType.FieldName = "LinkedToPersonType";
            this.colLinkedToPersonType.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colLinkedToPersonType.Name = "colLinkedToPersonType";
            this.colLinkedToPersonType.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonType.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonType.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonType.Visible = true;
            this.colLinkedToPersonType.VisibleIndex = 0;
            this.colLinkedToPersonType.Width = 127;
            // 
            // colLinkedToPersonTypeID
            // 
            this.colLinkedToPersonTypeID.Caption = "Labour Type ID";
            this.colLinkedToPersonTypeID.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.Name = "colLinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID.Width = 93;
            // 
            // xtraTabPageEquipment
            // 
            this.xtraTabPageEquipment.Controls.Add(this.gridControlEquipment);
            this.xtraTabPageEquipment.Name = "xtraTabPageEquipment";
            this.xtraTabPageEquipment.Size = new System.Drawing.Size(999, 203);
            this.xtraTabPageEquipment.Text = "Default Equipment Costs";
            // 
            // gridControlEquipment
            // 
            this.gridControlEquipment.DataSource = this.sp06114OMSiteContractManagerLinkedEquipmentBindingSource;
            this.gridControlEquipment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlEquipment.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlEquipment.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 12, true, true, "Block Add Records", "block_add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "block edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view")});
            this.gridControlEquipment.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlEquipment_EmbeddedNavigator_ButtonClick);
            this.gridControlEquipment.Location = new System.Drawing.Point(0, 0);
            this.gridControlEquipment.MainView = this.gridViewEquipment;
            this.gridControlEquipment.MenuManager = this.barManager1;
            this.gridControlEquipment.Name = "gridControlEquipment";
            this.gridControlEquipment.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit12,
            this.repositoryItemTextEditMoney3,
            this.repositoryItemTimeEditPercent3,
            this.repositoryItemTextEditHTML6});
            this.gridControlEquipment.Size = new System.Drawing.Size(999, 203);
            this.gridControlEquipment.TabIndex = 24;
            this.gridControlEquipment.UseEmbeddedNavigator = true;
            this.gridControlEquipment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewEquipment});
            // 
            // sp06114OMSiteContractManagerLinkedEquipmentBindingSource
            // 
            this.sp06114OMSiteContractManagerLinkedEquipmentBindingSource.DataMember = "sp06114_OM_Site_Contract_Manager_Linked_Equipment";
            this.sp06114OMSiteContractManagerLinkedEquipmentBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridViewEquipment
            // 
            this.gridViewEquipment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn58,
            this.gridColumn59,
            this.gridColumn60,
            this.colCostUnitDescriptorID2,
            this.gridColumn62,
            this.gridColumn63,
            this.gridColumn64,
            this.gridColumn65,
            this.gridColumn66,
            this.gridColumn71,
            this.gridColumn72,
            this.gridColumn73,
            this.gridColumn74,
            this.gridColumn75,
            this.gridColumn76,
            this.colOwnerName,
            this.colOwnerType,
            this.colLinkedToParent5,
            this.colLinkedToParentFull4,
            this.colSiteID5});
            this.gridViewEquipment.GridControl = this.gridControlEquipment;
            this.gridViewEquipment.GroupCount = 2;
            this.gridViewEquipment.Name = "gridViewEquipment";
            this.gridViewEquipment.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewEquipment.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewEquipment.OptionsLayout.StoreAppearance = true;
            this.gridViewEquipment.OptionsLayout.StoreFormatRules = true;
            this.gridViewEquipment.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewEquipment.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewEquipment.OptionsSelection.MultiSelect = true;
            this.gridViewEquipment.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewEquipment.OptionsView.ColumnAutoWidth = false;
            this.gridViewEquipment.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewEquipment.OptionsView.ShowGroupPanel = false;
            this.gridViewEquipment.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent5, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn75, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn76, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewEquipment.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewEquipment.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewEquipment.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewEquipment.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewEquipment.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewEquipment.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewEquipment.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewEquipment_MouseUp);
            this.gridViewEquipment.DoubleClick += new System.EventHandler(this.gridViewEquipment_DoubleClick);
            this.gridViewEquipment.GotFocus += new System.EventHandler(this.gridViewEquipment_GotFocus);
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Site Contract Equipment Cost ID";
            this.gridColumn58.FieldName = "SiteContractEquipmentCostID";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.AllowEdit = false;
            this.gridColumn58.OptionsColumn.AllowFocus = false;
            this.gridColumn58.OptionsColumn.ReadOnly = true;
            this.gridColumn58.Width = 159;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "Site Contract ID";
            this.gridColumn59.FieldName = "SiteContractID";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.AllowEdit = false;
            this.gridColumn59.OptionsColumn.AllowFocus = false;
            this.gridColumn59.OptionsColumn.ReadOnly = true;
            this.gridColumn59.Width = 98;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "Equipment ID";
            this.gridColumn60.FieldName = "EquipmentID";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.AllowEdit = false;
            this.gridColumn60.OptionsColumn.AllowFocus = false;
            this.gridColumn60.OptionsColumn.ReadOnly = true;
            // 
            // colCostUnitDescriptorID2
            // 
            this.colCostUnitDescriptorID2.Caption = "Cost Unit Descriptor";
            this.colCostUnitDescriptorID2.FieldName = "CostUnitDescriptor";
            this.colCostUnitDescriptorID2.Name = "colCostUnitDescriptorID2";
            this.colCostUnitDescriptorID2.OptionsColumn.AllowEdit = false;
            this.colCostUnitDescriptorID2.OptionsColumn.AllowFocus = false;
            this.colCostUnitDescriptorID2.OptionsColumn.ReadOnly = true;
            this.colCostUnitDescriptorID2.Visible = true;
            this.colCostUnitDescriptorID2.VisibleIndex = 3;
            this.colCostUnitDescriptorID2.Width = 117;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "Sell Unit Descriptor";
            this.gridColumn62.FieldName = "SellUnitDescriptor";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.OptionsColumn.AllowEdit = false;
            this.gridColumn62.OptionsColumn.AllowFocus = false;
            this.gridColumn62.OptionsColumn.ReadOnly = true;
            this.gridColumn62.Visible = true;
            this.gridColumn62.VisibleIndex = 6;
            this.gridColumn62.Width = 111;
            // 
            // gridColumn63
            // 
            this.gridColumn63.Caption = "Cost Per Unit Ex VAT";
            this.gridColumn63.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.gridColumn63.FieldName = "CostPerUnitExVat";
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.OptionsColumn.AllowEdit = false;
            this.gridColumn63.OptionsColumn.AllowFocus = false;
            this.gridColumn63.OptionsColumn.ReadOnly = true;
            this.gridColumn63.Visible = true;
            this.gridColumn63.VisibleIndex = 4;
            this.gridColumn63.Width = 121;
            // 
            // repositoryItemTextEditMoney3
            // 
            this.repositoryItemTextEditMoney3.AutoHeight = false;
            this.repositoryItemTextEditMoney3.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney3.Name = "repositoryItemTextEditMoney3";
            // 
            // gridColumn64
            // 
            this.gridColumn64.Caption = "Cost Per Unit VAT Rate";
            this.gridColumn64.ColumnEdit = this.repositoryItemTimeEditPercent3;
            this.gridColumn64.FieldName = "CostPerUnitVatRate";
            this.gridColumn64.Name = "gridColumn64";
            this.gridColumn64.OptionsColumn.AllowEdit = false;
            this.gridColumn64.OptionsColumn.AllowFocus = false;
            this.gridColumn64.OptionsColumn.ReadOnly = true;
            this.gridColumn64.Visible = true;
            this.gridColumn64.VisibleIndex = 5;
            this.gridColumn64.Width = 132;
            // 
            // repositoryItemTimeEditPercent3
            // 
            this.repositoryItemTimeEditPercent3.AutoHeight = false;
            this.repositoryItemTimeEditPercent3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTimeEditPercent3.Mask.EditMask = "P";
            this.repositoryItemTimeEditPercent3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTimeEditPercent3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTimeEditPercent3.Name = "repositoryItemTimeEditPercent3";
            // 
            // gridColumn65
            // 
            this.gridColumn65.Caption = "Sell Per Unit Ex VAT";
            this.gridColumn65.ColumnEdit = this.repositoryItemTextEditMoney3;
            this.gridColumn65.FieldName = "SellPerUnitExVat";
            this.gridColumn65.Name = "gridColumn65";
            this.gridColumn65.OptionsColumn.AllowEdit = false;
            this.gridColumn65.OptionsColumn.AllowFocus = false;
            this.gridColumn65.OptionsColumn.ReadOnly = true;
            this.gridColumn65.Visible = true;
            this.gridColumn65.VisibleIndex = 7;
            this.gridColumn65.Width = 115;
            // 
            // gridColumn66
            // 
            this.gridColumn66.Caption = "Sell Per Unit VAT Rate";
            this.gridColumn66.ColumnEdit = this.repositoryItemTimeEditPercent3;
            this.gridColumn66.FieldName = "SellPerUnitVatRate";
            this.gridColumn66.Name = "gridColumn66";
            this.gridColumn66.OptionsColumn.AllowEdit = false;
            this.gridColumn66.OptionsColumn.AllowFocus = false;
            this.gridColumn66.OptionsColumn.ReadOnly = true;
            this.gridColumn66.Visible = true;
            this.gridColumn66.VisibleIndex = 8;
            this.gridColumn66.Width = 126;
            // 
            // gridColumn71
            // 
            this.gridColumn71.Caption = "Remarks";
            this.gridColumn71.ColumnEdit = this.repositoryItemMemoExEdit12;
            this.gridColumn71.FieldName = "Remarks";
            this.gridColumn71.Name = "gridColumn71";
            this.gridColumn71.OptionsColumn.ReadOnly = true;
            this.gridColumn71.Visible = true;
            this.gridColumn71.VisibleIndex = 9;
            // 
            // repositoryItemMemoExEdit12
            // 
            this.repositoryItemMemoExEdit12.AutoHeight = false;
            this.repositoryItemMemoExEdit12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit12.Name = "repositoryItemMemoExEdit12";
            this.repositoryItemMemoExEdit12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit12.ShowIcon = false;
            // 
            // gridColumn72
            // 
            this.gridColumn72.Caption = "Client Name";
            this.gridColumn72.FieldName = "ClientName";
            this.gridColumn72.Name = "gridColumn72";
            this.gridColumn72.OptionsColumn.AllowEdit = false;
            this.gridColumn72.OptionsColumn.AllowFocus = false;
            this.gridColumn72.OptionsColumn.ReadOnly = true;
            this.gridColumn72.Width = 166;
            // 
            // gridColumn73
            // 
            this.gridColumn73.Caption = "Contract Start";
            this.gridColumn73.FieldName = "ContractStartDate";
            this.gridColumn73.Name = "gridColumn73";
            this.gridColumn73.OptionsColumn.AllowEdit = false;
            this.gridColumn73.OptionsColumn.AllowFocus = false;
            this.gridColumn73.OptionsColumn.ReadOnly = true;
            this.gridColumn73.Width = 90;
            // 
            // gridColumn74
            // 
            this.gridColumn74.Caption = "Contract End";
            this.gridColumn74.FieldName = "ContractEndDate";
            this.gridColumn74.Name = "gridColumn74";
            this.gridColumn74.OptionsColumn.AllowEdit = false;
            this.gridColumn74.OptionsColumn.AllowFocus = false;
            this.gridColumn74.OptionsColumn.ReadOnly = true;
            this.gridColumn74.Width = 84;
            // 
            // gridColumn75
            // 
            this.gridColumn75.Caption = "Site Name";
            this.gridColumn75.FieldName = "SiteName";
            this.gridColumn75.Name = "gridColumn75";
            this.gridColumn75.OptionsColumn.AllowEdit = false;
            this.gridColumn75.OptionsColumn.AllowFocus = false;
            this.gridColumn75.OptionsColumn.ReadOnly = true;
            this.gridColumn75.Width = 191;
            // 
            // gridColumn76
            // 
            this.gridColumn76.Caption = "Equipment Type";
            this.gridColumn76.FieldName = "EquipmentType";
            this.gridColumn76.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn76.Name = "gridColumn76";
            this.gridColumn76.OptionsColumn.AllowEdit = false;
            this.gridColumn76.OptionsColumn.AllowFocus = false;
            this.gridColumn76.OptionsColumn.ReadOnly = true;
            this.gridColumn76.Visible = true;
            this.gridColumn76.VisibleIndex = 0;
            this.gridColumn76.Width = 176;
            // 
            // colOwnerName
            // 
            this.colOwnerName.Caption = "Owner";
            this.colOwnerName.FieldName = "OwnerName";
            this.colOwnerName.Name = "colOwnerName";
            this.colOwnerName.OptionsColumn.AllowEdit = false;
            this.colOwnerName.OptionsColumn.AllowFocus = false;
            this.colOwnerName.OptionsColumn.ReadOnly = true;
            this.colOwnerName.Visible = true;
            this.colOwnerName.VisibleIndex = 1;
            this.colOwnerName.Width = 117;
            // 
            // colOwnerType
            // 
            this.colOwnerType.Caption = "Owner Type";
            this.colOwnerType.FieldName = "OwnerType";
            this.colOwnerType.Name = "colOwnerType";
            this.colOwnerType.OptionsColumn.AllowEdit = false;
            this.colOwnerType.OptionsColumn.AllowFocus = false;
            this.colOwnerType.OptionsColumn.ReadOnly = true;
            this.colOwnerType.Visible = true;
            this.colOwnerType.VisibleIndex = 2;
            this.colOwnerType.Width = 80;
            // 
            // colLinkedToParent5
            // 
            this.colLinkedToParent5.Caption = "Linked To Contract";
            this.colLinkedToParent5.ColumnEdit = this.repositoryItemTextEditHTML6;
            this.colLinkedToParent5.FieldName = "LinkedToParent";
            this.colLinkedToParent5.Name = "colLinkedToParent5";
            this.colLinkedToParent5.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent5.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent5.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent5.Visible = true;
            this.colLinkedToParent5.VisibleIndex = 10;
            this.colLinkedToParent5.Width = 332;
            // 
            // repositoryItemTextEditHTML6
            // 
            this.repositoryItemTextEditHTML6.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML6.AutoHeight = false;
            this.repositoryItemTextEditHTML6.Name = "repositoryItemTextEditHTML6";
            // 
            // colLinkedToParentFull4
            // 
            this.colLinkedToParentFull4.Caption = "Linked To Site";
            this.colLinkedToParentFull4.ColumnEdit = this.repositoryItemTextEditHTML6;
            this.colLinkedToParentFull4.FieldName = "LinkedToParentFull";
            this.colLinkedToParentFull4.Name = "colLinkedToParentFull4";
            this.colLinkedToParentFull4.OptionsColumn.AllowEdit = false;
            this.colLinkedToParentFull4.OptionsColumn.AllowFocus = false;
            this.colLinkedToParentFull4.OptionsColumn.ReadOnly = true;
            this.colLinkedToParentFull4.Width = 183;
            // 
            // colSiteID5
            // 
            this.colSiteID5.Caption = "Site ID";
            this.colSiteID5.FieldName = "SiteID";
            this.colSiteID5.Name = "colSiteID5";
            this.colSiteID5.OptionsColumn.AllowEdit = false;
            this.colSiteID5.OptionsColumn.AllowFocus = false;
            this.colSiteID5.OptionsColumn.ReadOnly = true;
            this.colSiteID5.Visible = true;
            this.colSiteID5.VisibleIndex = 10;
            this.colSiteID5.Width = 51;
            // 
            // xtraTabPageMaterial
            // 
            this.xtraTabPageMaterial.Controls.Add(this.gridControlMaterial);
            this.xtraTabPageMaterial.Name = "xtraTabPageMaterial";
            this.xtraTabPageMaterial.Size = new System.Drawing.Size(999, 203);
            this.xtraTabPageMaterial.Text = "Default Material Costs";
            // 
            // gridControlMaterial
            // 
            this.gridControlMaterial.DataSource = this.sp06115OMSiteContractManagerLinkedMaterialsBindingSource;
            this.gridControlMaterial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlMaterial.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlMaterial.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 12, true, true, "Block Add Records", "block_add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view")});
            this.gridControlMaterial.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlMaterial_EmbeddedNavigator_ButtonClick);
            this.gridControlMaterial.Location = new System.Drawing.Point(0, 0);
            this.gridControlMaterial.MainView = this.gridViewMaterial;
            this.gridControlMaterial.MenuManager = this.barManager1;
            this.gridControlMaterial.Name = "gridControlMaterial";
            this.gridControlMaterial.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit14,
            this.repositoryItemTextEditMoney4,
            this.repositoryItemTimeEditPercent4,
            this.repositoryItemTextEditHTML7});
            this.gridControlMaterial.Size = new System.Drawing.Size(999, 203);
            this.gridControlMaterial.TabIndex = 25;
            this.gridControlMaterial.UseEmbeddedNavigator = true;
            this.gridControlMaterial.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMaterial});
            // 
            // sp06115OMSiteContractManagerLinkedMaterialsBindingSource
            // 
            this.sp06115OMSiteContractManagerLinkedMaterialsBindingSource.DataMember = "sp06115_OM_Site_Contract_Manager_Linked_Materials";
            this.sp06115OMSiteContractManagerLinkedMaterialsBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridViewMaterial
            // 
            this.gridViewMaterial.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn89,
            this.gridColumn90,
            this.gridColumn91,
            this.colCostUnitDescriptor3,
            this.gridColumn96,
            this.gridColumn97,
            this.gridColumn98,
            this.gridColumn99,
            this.gridColumn100,
            this.gridColumn101,
            this.gridColumn102,
            this.gridColumn103,
            this.gridColumn104,
            this.gridColumn105,
            this.gridColumn106,
            this.colLinkedToParent6,
            this.colLinkedToParentFull5,
            this.colSiteID6});
            this.gridViewMaterial.GridControl = this.gridControlMaterial;
            this.gridViewMaterial.GroupCount = 2;
            this.gridViewMaterial.Name = "gridViewMaterial";
            this.gridViewMaterial.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewMaterial.OptionsLayout.StoreAppearance = true;
            this.gridViewMaterial.OptionsLayout.StoreFormatRules = true;
            this.gridViewMaterial.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewMaterial.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewMaterial.OptionsSelection.MultiSelect = true;
            this.gridViewMaterial.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewMaterial.OptionsView.ColumnAutoWidth = false;
            this.gridViewMaterial.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewMaterial.OptionsView.ShowGroupPanel = false;
            this.gridViewMaterial.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent6, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn105, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn106, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewMaterial.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewMaterial.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewMaterial.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewMaterial.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewMaterial.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewMaterial.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewMaterial.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewMaterial_MouseUp);
            this.gridViewMaterial.DoubleClick += new System.EventHandler(this.gridViewMaterial_DoubleClick);
            this.gridViewMaterial.GotFocus += new System.EventHandler(this.gridViewMaterial_GotFocus);
            // 
            // gridColumn89
            // 
            this.gridColumn89.Caption = "Site Contract Material Cost ID";
            this.gridColumn89.FieldName = "SiteContractMaterialCostID";
            this.gridColumn89.Name = "gridColumn89";
            this.gridColumn89.OptionsColumn.AllowEdit = false;
            this.gridColumn89.OptionsColumn.AllowFocus = false;
            this.gridColumn89.OptionsColumn.ReadOnly = true;
            this.gridColumn89.Width = 159;
            // 
            // gridColumn90
            // 
            this.gridColumn90.Caption = "Site Contract ID";
            this.gridColumn90.FieldName = "SiteContractID";
            this.gridColumn90.Name = "gridColumn90";
            this.gridColumn90.OptionsColumn.AllowEdit = false;
            this.gridColumn90.OptionsColumn.AllowFocus = false;
            this.gridColumn90.OptionsColumn.ReadOnly = true;
            this.gridColumn90.Width = 98;
            // 
            // gridColumn91
            // 
            this.gridColumn91.Caption = "Material ID";
            this.gridColumn91.FieldName = "MaterialID";
            this.gridColumn91.Name = "gridColumn91";
            this.gridColumn91.OptionsColumn.AllowEdit = false;
            this.gridColumn91.OptionsColumn.AllowFocus = false;
            this.gridColumn91.OptionsColumn.ReadOnly = true;
            // 
            // colCostUnitDescriptor3
            // 
            this.colCostUnitDescriptor3.Caption = "Cost Unit Descriptor";
            this.colCostUnitDescriptor3.FieldName = "CostUnitDescriptor";
            this.colCostUnitDescriptor3.Name = "colCostUnitDescriptor3";
            this.colCostUnitDescriptor3.OptionsColumn.AllowEdit = false;
            this.colCostUnitDescriptor3.OptionsColumn.AllowFocus = false;
            this.colCostUnitDescriptor3.OptionsColumn.ReadOnly = true;
            this.colCostUnitDescriptor3.Visible = true;
            this.colCostUnitDescriptor3.VisibleIndex = 1;
            this.colCostUnitDescriptor3.Width = 117;
            // 
            // gridColumn96
            // 
            this.gridColumn96.Caption = "Sell Unit Descriptor";
            this.gridColumn96.FieldName = "SellUnitDescriptor";
            this.gridColumn96.Name = "gridColumn96";
            this.gridColumn96.OptionsColumn.AllowEdit = false;
            this.gridColumn96.OptionsColumn.AllowFocus = false;
            this.gridColumn96.OptionsColumn.ReadOnly = true;
            this.gridColumn96.Visible = true;
            this.gridColumn96.VisibleIndex = 4;
            this.gridColumn96.Width = 111;
            // 
            // gridColumn97
            // 
            this.gridColumn97.Caption = "Cost Per Unit Ex VAT";
            this.gridColumn97.ColumnEdit = this.repositoryItemTextEditMoney4;
            this.gridColumn97.FieldName = "CostPerUnitExVat";
            this.gridColumn97.Name = "gridColumn97";
            this.gridColumn97.OptionsColumn.AllowEdit = false;
            this.gridColumn97.OptionsColumn.AllowFocus = false;
            this.gridColumn97.OptionsColumn.ReadOnly = true;
            this.gridColumn97.Visible = true;
            this.gridColumn97.VisibleIndex = 2;
            this.gridColumn97.Width = 121;
            // 
            // repositoryItemTextEditMoney4
            // 
            this.repositoryItemTextEditMoney4.AutoHeight = false;
            this.repositoryItemTextEditMoney4.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney4.Name = "repositoryItemTextEditMoney4";
            // 
            // gridColumn98
            // 
            this.gridColumn98.Caption = "Cost Per Unit VAT Rate";
            this.gridColumn98.ColumnEdit = this.repositoryItemTimeEditPercent4;
            this.gridColumn98.FieldName = "CostPerUnitVatRate";
            this.gridColumn98.Name = "gridColumn98";
            this.gridColumn98.OptionsColumn.AllowEdit = false;
            this.gridColumn98.OptionsColumn.AllowFocus = false;
            this.gridColumn98.OptionsColumn.ReadOnly = true;
            this.gridColumn98.Visible = true;
            this.gridColumn98.VisibleIndex = 3;
            this.gridColumn98.Width = 132;
            // 
            // repositoryItemTimeEditPercent4
            // 
            this.repositoryItemTimeEditPercent4.AutoHeight = false;
            this.repositoryItemTimeEditPercent4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTimeEditPercent4.Mask.EditMask = "P";
            this.repositoryItemTimeEditPercent4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTimeEditPercent4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTimeEditPercent4.Name = "repositoryItemTimeEditPercent4";
            // 
            // gridColumn99
            // 
            this.gridColumn99.Caption = "Sell Per Unit Ex VAT";
            this.gridColumn99.ColumnEdit = this.repositoryItemTextEditMoney4;
            this.gridColumn99.FieldName = "SellPerUnitExVat";
            this.gridColumn99.Name = "gridColumn99";
            this.gridColumn99.OptionsColumn.AllowEdit = false;
            this.gridColumn99.OptionsColumn.AllowFocus = false;
            this.gridColumn99.OptionsColumn.ReadOnly = true;
            this.gridColumn99.Visible = true;
            this.gridColumn99.VisibleIndex = 5;
            this.gridColumn99.Width = 115;
            // 
            // gridColumn100
            // 
            this.gridColumn100.Caption = "Sell Per Unit VAT Rate";
            this.gridColumn100.ColumnEdit = this.repositoryItemTimeEditPercent4;
            this.gridColumn100.FieldName = "SellPerUnitVatRate";
            this.gridColumn100.Name = "gridColumn100";
            this.gridColumn100.OptionsColumn.AllowEdit = false;
            this.gridColumn100.OptionsColumn.AllowFocus = false;
            this.gridColumn100.OptionsColumn.ReadOnly = true;
            this.gridColumn100.Visible = true;
            this.gridColumn100.VisibleIndex = 6;
            this.gridColumn100.Width = 126;
            // 
            // gridColumn101
            // 
            this.gridColumn101.Caption = "Remarks";
            this.gridColumn101.ColumnEdit = this.repositoryItemMemoExEdit14;
            this.gridColumn101.FieldName = "Remarks";
            this.gridColumn101.Name = "gridColumn101";
            this.gridColumn101.OptionsColumn.ReadOnly = true;
            this.gridColumn101.Visible = true;
            this.gridColumn101.VisibleIndex = 7;
            // 
            // repositoryItemMemoExEdit14
            // 
            this.repositoryItemMemoExEdit14.AutoHeight = false;
            this.repositoryItemMemoExEdit14.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit14.Name = "repositoryItemMemoExEdit14";
            this.repositoryItemMemoExEdit14.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit14.ShowIcon = false;
            // 
            // gridColumn102
            // 
            this.gridColumn102.Caption = "Client Name";
            this.gridColumn102.FieldName = "ClientName";
            this.gridColumn102.Name = "gridColumn102";
            this.gridColumn102.OptionsColumn.AllowEdit = false;
            this.gridColumn102.OptionsColumn.AllowFocus = false;
            this.gridColumn102.OptionsColumn.ReadOnly = true;
            this.gridColumn102.Width = 166;
            // 
            // gridColumn103
            // 
            this.gridColumn103.Caption = "Contract Start";
            this.gridColumn103.FieldName = "ContractStartDate";
            this.gridColumn103.Name = "gridColumn103";
            this.gridColumn103.OptionsColumn.AllowEdit = false;
            this.gridColumn103.OptionsColumn.AllowFocus = false;
            this.gridColumn103.OptionsColumn.ReadOnly = true;
            this.gridColumn103.Width = 90;
            // 
            // gridColumn104
            // 
            this.gridColumn104.Caption = "Contract End";
            this.gridColumn104.FieldName = "ContractEndDate";
            this.gridColumn104.Name = "gridColumn104";
            this.gridColumn104.OptionsColumn.AllowEdit = false;
            this.gridColumn104.OptionsColumn.AllowFocus = false;
            this.gridColumn104.OptionsColumn.ReadOnly = true;
            this.gridColumn104.Width = 84;
            // 
            // gridColumn105
            // 
            this.gridColumn105.Caption = "Site Name";
            this.gridColumn105.FieldName = "SiteName";
            this.gridColumn105.Name = "gridColumn105";
            this.gridColumn105.OptionsColumn.AllowEdit = false;
            this.gridColumn105.OptionsColumn.AllowFocus = false;
            this.gridColumn105.OptionsColumn.ReadOnly = true;
            this.gridColumn105.Width = 191;
            // 
            // gridColumn106
            // 
            this.gridColumn106.Caption = "Material Description";
            this.gridColumn106.FieldName = "MaterialDescription";
            this.gridColumn106.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn106.Name = "gridColumn106";
            this.gridColumn106.OptionsColumn.AllowEdit = false;
            this.gridColumn106.OptionsColumn.AllowFocus = false;
            this.gridColumn106.OptionsColumn.ReadOnly = true;
            this.gridColumn106.Visible = true;
            this.gridColumn106.VisibleIndex = 0;
            this.gridColumn106.Width = 176;
            // 
            // colLinkedToParent6
            // 
            this.colLinkedToParent6.Caption = "Linked To Contract";
            this.colLinkedToParent6.ColumnEdit = this.repositoryItemTextEditHTML7;
            this.colLinkedToParent6.FieldName = "LinkedToParent";
            this.colLinkedToParent6.Name = "colLinkedToParent6";
            this.colLinkedToParent6.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent6.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent6.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent6.Visible = true;
            this.colLinkedToParent6.VisibleIndex = 8;
            this.colLinkedToParent6.Width = 242;
            // 
            // repositoryItemTextEditHTML7
            // 
            this.repositoryItemTextEditHTML7.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML7.AutoHeight = false;
            this.repositoryItemTextEditHTML7.Name = "repositoryItemTextEditHTML7";
            // 
            // colLinkedToParentFull5
            // 
            this.colLinkedToParentFull5.Caption = "Linked To Site";
            this.colLinkedToParentFull5.ColumnEdit = this.repositoryItemTextEditHTML7;
            this.colLinkedToParentFull5.FieldName = "LinkedToParentFull";
            this.colLinkedToParentFull5.Name = "colLinkedToParentFull5";
            this.colLinkedToParentFull5.OptionsColumn.AllowEdit = false;
            this.colLinkedToParentFull5.OptionsColumn.AllowFocus = false;
            this.colLinkedToParentFull5.OptionsColumn.ReadOnly = true;
            this.colLinkedToParentFull5.Width = 275;
            // 
            // colSiteID6
            // 
            this.colSiteID6.Caption = "Site ID";
            this.colSiteID6.FieldName = "SiteID";
            this.colSiteID6.Name = "colSiteID6";
            this.colSiteID6.OptionsColumn.AllowEdit = false;
            this.colSiteID6.OptionsColumn.AllowFocus = false;
            this.colSiteID6.OptionsColumn.ReadOnly = true;
            this.colSiteID6.Visible = true;
            this.colSiteID6.VisibleIndex = 8;
            this.colSiteID6.Width = 51;
            // 
            // xtraTabPageClientPOs
            // 
            this.xtraTabPageClientPOs.Controls.Add(this.gridControlClientPO);
            this.xtraTabPageClientPOs.Name = "xtraTabPageClientPOs";
            this.xtraTabPageClientPOs.Size = new System.Drawing.Size(999, 203);
            this.xtraTabPageClientPOs.Text = "Client Purchase Orders";
            // 
            // gridControlClientPO
            // 
            this.gridControlClientPO.DataSource = this.sp06326OMClientPOsLinkedToParentBindingSource;
            this.gridControlClientPO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlClientPO.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlClientPO.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view")});
            this.gridControlClientPO.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlClientPO_EmbeddedNavigator_ButtonClick);
            this.gridControlClientPO.Location = new System.Drawing.Point(0, 0);
            this.gridControlClientPO.MainView = this.gridViewClientPO;
            this.gridControlClientPO.MenuManager = this.barManager1;
            this.gridControlClientPO.Name = "gridControlClientPO";
            this.gridControlClientPO.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEditRemarks,
            this.repositoryItemTextEditDateTime9,
            this.repositoryItemCheckEdit4,
            this.repositoryItemTextEditCurrency9,
            this.repositoryItemTextEditHTML8});
            this.gridControlClientPO.Size = new System.Drawing.Size(999, 203);
            this.gridControlClientPO.TabIndex = 8;
            this.gridControlClientPO.UseEmbeddedNavigator = true;
            this.gridControlClientPO.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewClientPO});
            // 
            // sp06326OMClientPOsLinkedToParentBindingSource
            // 
            this.sp06326OMClientPOsLinkedToParentBindingSource.DataMember = "sp06326_OM_Client_POs_Linked_To_Parent";
            this.sp06326OMClientPOsLinkedToParentBindingSource.DataSource = this.dataSet_OM_Client_PO;
            // 
            // dataSet_OM_Client_PO
            // 
            this.dataSet_OM_Client_PO.DataSetName = "DataSet_OM_Client_PO";
            this.dataSet_OM_Client_PO.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewClientPO
            // 
            this.gridViewClientPO.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientPurchaseOrderLinkID,
            this.colClientPurchaseOrderID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colIsDefault,
            this.colRemarks4,
            this.colPONumber,
            this.colClientName4,
            this.colLinkedToRecordType,
            this.colLinkedToRecord,
            this.colContractDescription1,
            this.gridColumn5,
            this.colVisitNumber,
            this.colStartDate2,
            this.colEndDate2,
            this.colStartingValue,
            this.colUsedValue,
            this.colRemainingValue,
            this.colWarningValue,
            this.colClientID4});
            this.gridViewClientPO.GridControl = this.gridControlClientPO;
            this.gridViewClientPO.GroupCount = 1;
            this.gridViewClientPO.Name = "gridViewClientPO";
            this.gridViewClientPO.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewClientPO.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewClientPO.OptionsLayout.StoreAppearance = true;
            this.gridViewClientPO.OptionsLayout.StoreFormatRules = true;
            this.gridViewClientPO.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewClientPO.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewClientPO.OptionsSelection.MultiSelect = true;
            this.gridViewClientPO.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewClientPO.OptionsView.ColumnAutoWidth = false;
            this.gridViewClientPO.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewClientPO.OptionsView.ShowGroupPanel = false;
            this.gridViewClientPO.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToRecord, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEndDate2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPONumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewClientPO.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewClientPO_CustomDrawCell);
            this.gridViewClientPO.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewClientPO.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewClientPO.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewClientPO.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewClientPO.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewClientPO.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewClientPO.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewClientPO_MouseUp);
            this.gridViewClientPO.DoubleClick += new System.EventHandler(this.gridViewClientPO_DoubleClick);
            this.gridViewClientPO.GotFocus += new System.EventHandler(this.gridViewClientPO_GotFocus);
            // 
            // colClientPurchaseOrderLinkID
            // 
            this.colClientPurchaseOrderLinkID.Caption = "Client PO Link ID";
            this.colClientPurchaseOrderLinkID.FieldName = "ClientPurchaseOrderLinkID";
            this.colClientPurchaseOrderLinkID.Name = "colClientPurchaseOrderLinkID";
            this.colClientPurchaseOrderLinkID.OptionsColumn.AllowEdit = false;
            this.colClientPurchaseOrderLinkID.OptionsColumn.AllowFocus = false;
            this.colClientPurchaseOrderLinkID.OptionsColumn.ReadOnly = true;
            this.colClientPurchaseOrderLinkID.Width = 98;
            // 
            // colClientPurchaseOrderID
            // 
            this.colClientPurchaseOrderID.Caption = "Client PO ID";
            this.colClientPurchaseOrderID.FieldName = "ClientPurchaseOrderID";
            this.colClientPurchaseOrderID.Name = "colClientPurchaseOrderID";
            this.colClientPurchaseOrderID.OptionsColumn.AllowEdit = false;
            this.colClientPurchaseOrderID.OptionsColumn.AllowFocus = false;
            this.colClientPurchaseOrderID.OptionsColumn.ReadOnly = true;
            this.colClientPurchaseOrderID.Width = 77;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked To Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 115;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked To Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 142;
            // 
            // colIsDefault
            // 
            this.colIsDefault.Caption = "Default";
            this.colIsDefault.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colIsDefault.FieldName = "IsDefault";
            this.colIsDefault.Name = "colIsDefault";
            this.colIsDefault.OptionsColumn.AllowEdit = false;
            this.colIsDefault.OptionsColumn.AllowFocus = false;
            this.colIsDefault.OptionsColumn.ReadOnly = true;
            this.colIsDefault.Visible = true;
            this.colIsDefault.VisibleIndex = 3;
            this.colIsDefault.Width = 54;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // colRemarks4
            // 
            this.colRemarks4.Caption = "Remarks";
            this.colRemarks4.ColumnEdit = this.repositoryItemMemoExEditRemarks;
            this.colRemarks4.FieldName = "Remarks";
            this.colRemarks4.Name = "colRemarks4";
            this.colRemarks4.OptionsColumn.ReadOnly = true;
            this.colRemarks4.Visible = true;
            this.colRemarks4.VisibleIndex = 8;
            // 
            // repositoryItemMemoExEditRemarks
            // 
            this.repositoryItemMemoExEditRemarks.AutoHeight = false;
            this.repositoryItemMemoExEditRemarks.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEditRemarks.Name = "repositoryItemMemoExEditRemarks";
            this.repositoryItemMemoExEditRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEditRemarks.ShowIcon = false;
            // 
            // colPONumber
            // 
            this.colPONumber.Caption = "PO #";
            this.colPONumber.FieldName = "PONumber";
            this.colPONumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPONumber.Name = "colPONumber";
            this.colPONumber.OptionsColumn.AllowEdit = false;
            this.colPONumber.OptionsColumn.AllowFocus = false;
            this.colPONumber.OptionsColumn.ReadOnly = true;
            this.colPONumber.Visible = true;
            this.colPONumber.VisibleIndex = 2;
            this.colPONumber.Width = 123;
            // 
            // colClientName4
            // 
            this.colClientName4.Caption = "Client Name";
            this.colClientName4.FieldName = "ClientName";
            this.colClientName4.Name = "colClientName4";
            this.colClientName4.OptionsColumn.AllowEdit = false;
            this.colClientName4.OptionsColumn.AllowFocus = false;
            this.colClientName4.OptionsColumn.ReadOnly = true;
            this.colClientName4.Width = 245;
            // 
            // colLinkedToRecordType
            // 
            this.colLinkedToRecordType.Caption = "Link Type";
            this.colLinkedToRecordType.FieldName = "LinkedToRecordType";
            this.colLinkedToRecordType.Name = "colLinkedToRecordType";
            this.colLinkedToRecordType.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordType.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordType.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordType.Width = 178;
            // 
            // colLinkedToRecord
            // 
            this.colLinkedToRecord.Caption = "Linked To Contract";
            this.colLinkedToRecord.ColumnEdit = this.repositoryItemTextEditHTML8;
            this.colLinkedToRecord.FieldName = "LinkedToRecord";
            this.colLinkedToRecord.Name = "colLinkedToRecord";
            this.colLinkedToRecord.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecord.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecord.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecord.Visible = true;
            this.colLinkedToRecord.VisibleIndex = 9;
            this.colLinkedToRecord.Width = 415;
            // 
            // repositoryItemTextEditHTML8
            // 
            this.repositoryItemTextEditHTML8.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML8.AutoHeight = false;
            this.repositoryItemTextEditHTML8.Name = "repositoryItemTextEditHTML8";
            // 
            // colContractDescription1
            // 
            this.colContractDescription1.Caption = "Contract";
            this.colContractDescription1.FieldName = "ContractDescription";
            this.colContractDescription1.Name = "colContractDescription1";
            this.colContractDescription1.OptionsColumn.AllowEdit = false;
            this.colContractDescription1.OptionsColumn.AllowFocus = false;
            this.colContractDescription1.OptionsColumn.ReadOnly = true;
            this.colContractDescription1.Width = 326;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Site";
            this.gridColumn5.FieldName = "SiteName";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 204;
            // 
            // colVisitNumber
            // 
            this.colVisitNumber.Caption = "Visit #";
            this.colVisitNumber.FieldName = "VisitNumber";
            this.colVisitNumber.Name = "colVisitNumber";
            this.colVisitNumber.OptionsColumn.AllowEdit = false;
            this.colVisitNumber.OptionsColumn.AllowFocus = false;
            this.colVisitNumber.OptionsColumn.ReadOnly = true;
            this.colVisitNumber.Width = 52;
            // 
            // colStartDate2
            // 
            this.colStartDate2.ColumnEdit = this.repositoryItemTextEditDateTime9;
            this.colStartDate2.FieldName = "StartDate";
            this.colStartDate2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStartDate2.Name = "colStartDate2";
            this.colStartDate2.OptionsColumn.AllowEdit = false;
            this.colStartDate2.OptionsColumn.AllowFocus = false;
            this.colStartDate2.OptionsColumn.ReadOnly = true;
            this.colStartDate2.Visible = true;
            this.colStartDate2.VisibleIndex = 0;
            this.colStartDate2.Width = 144;
            // 
            // repositoryItemTextEditDateTime9
            // 
            this.repositoryItemTextEditDateTime9.AutoHeight = false;
            this.repositoryItemTextEditDateTime9.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime9.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime9.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime9.Name = "repositoryItemTextEditDateTime9";
            // 
            // colEndDate2
            // 
            this.colEndDate2.ColumnEdit = this.repositoryItemTextEditDateTime9;
            this.colEndDate2.FieldName = "EndDate";
            this.colEndDate2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEndDate2.Name = "colEndDate2";
            this.colEndDate2.OptionsColumn.AllowEdit = false;
            this.colEndDate2.OptionsColumn.AllowFocus = false;
            this.colEndDate2.OptionsColumn.ReadOnly = true;
            this.colEndDate2.Visible = true;
            this.colEndDate2.VisibleIndex = 1;
            this.colEndDate2.Width = 85;
            // 
            // colStartingValue
            // 
            this.colStartingValue.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.colStartingValue.FieldName = "StartingValue";
            this.colStartingValue.Name = "colStartingValue";
            this.colStartingValue.OptionsColumn.AllowEdit = false;
            this.colStartingValue.OptionsColumn.AllowFocus = false;
            this.colStartingValue.OptionsColumn.ReadOnly = true;
            this.colStartingValue.Visible = true;
            this.colStartingValue.VisibleIndex = 4;
            this.colStartingValue.Width = 100;
            // 
            // repositoryItemTextEditCurrency9
            // 
            this.repositoryItemTextEditCurrency9.AutoHeight = false;
            this.repositoryItemTextEditCurrency9.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency9.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency9.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency9.Name = "repositoryItemTextEditCurrency9";
            // 
            // colUsedValue
            // 
            this.colUsedValue.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.colUsedValue.FieldName = "UsedValue";
            this.colUsedValue.Name = "colUsedValue";
            this.colUsedValue.OptionsColumn.AllowEdit = false;
            this.colUsedValue.OptionsColumn.AllowFocus = false;
            this.colUsedValue.OptionsColumn.ReadOnly = true;
            this.colUsedValue.Visible = true;
            this.colUsedValue.VisibleIndex = 5;
            this.colUsedValue.Width = 100;
            // 
            // colRemainingValue
            // 
            this.colRemainingValue.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.colRemainingValue.FieldName = "RemainingValue";
            this.colRemainingValue.Name = "colRemainingValue";
            this.colRemainingValue.OptionsColumn.AllowEdit = false;
            this.colRemainingValue.OptionsColumn.AllowFocus = false;
            this.colRemainingValue.OptionsColumn.ReadOnly = true;
            this.colRemainingValue.Visible = true;
            this.colRemainingValue.VisibleIndex = 6;
            this.colRemainingValue.Width = 100;
            // 
            // colWarningValue
            // 
            this.colWarningValue.ColumnEdit = this.repositoryItemTextEditCurrency9;
            this.colWarningValue.FieldName = "WarningValue";
            this.colWarningValue.Name = "colWarningValue";
            this.colWarningValue.OptionsColumn.AllowEdit = false;
            this.colWarningValue.OptionsColumn.AllowFocus = false;
            this.colWarningValue.OptionsColumn.ReadOnly = true;
            this.colWarningValue.Visible = true;
            this.colWarningValue.VisibleIndex = 7;
            this.colWarningValue.Width = 100;
            // 
            // colClientID4
            // 
            this.colClientID4.Caption = "Client ID";
            this.colClientID4.FieldName = "ClientID";
            this.colClientID4.Name = "colClientID4";
            this.colClientID4.OptionsColumn.AllowEdit = false;
            this.colClientID4.OptionsColumn.AllowFocus = false;
            this.colClientID4.OptionsColumn.ReadOnly = true;
            this.colClientID4.Width = 60;
            // 
            // xtraTabPageBillingRequirements
            // 
            this.xtraTabPageBillingRequirements.Controls.Add(this.gridControlBillingRequirement);
            this.xtraTabPageBillingRequirements.Name = "xtraTabPageBillingRequirements";
            this.xtraTabPageBillingRequirements.Size = new System.Drawing.Size(999, 203);
            this.xtraTabPageBillingRequirements.Text = "Billing Requirements";
            // 
            // gridControlBillingRequirement
            // 
            this.gridControlBillingRequirement.DataSource = this.sp01037CoreBillingRequirementItemsBindingSource;
            this.gridControlBillingRequirement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlBillingRequirement.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 9, true, true, "Add from Template", "add_from_template"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Template Manager", "template_manager"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 17, true, true, "Add from parent Client Contract", "add_from_parent")});
            this.gridControlBillingRequirement.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlBillingRequirement_EmbeddedNavigator_ButtonClick);
            this.gridControlBillingRequirement.Location = new System.Drawing.Point(0, 0);
            this.gridControlBillingRequirement.MainView = this.gridViewBillingRequirement;
            this.gridControlBillingRequirement.MenuManager = this.barManager1;
            this.gridControlBillingRequirement.Name = "gridControlBillingRequirement";
            this.gridControlBillingRequirement.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemCheckEdit5});
            this.gridControlBillingRequirement.Size = new System.Drawing.Size(999, 203);
            this.gridControlBillingRequirement.TabIndex = 8;
            this.gridControlBillingRequirement.UseEmbeddedNavigator = true;
            this.gridControlBillingRequirement.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBillingRequirement});
            // 
            // sp01037CoreBillingRequirementItemsBindingSource
            // 
            this.sp01037CoreBillingRequirementItemsBindingSource.DataMember = "sp01037_Core_Billing_Requirement_Items";
            this.sp01037CoreBillingRequirementItemsBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // dataSet_Common_Functionality
            // 
            this.dataSet_Common_Functionality.DataSetName = "DataSet_Common_Functionality";
            this.dataSet_Common_Functionality.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewBillingRequirement
            // 
            this.gridViewBillingRequirement.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBillingRequirementID,
            this.colRequirementID,
            this.colLinkedToRecordID1,
            this.colLinkedToRecordTypeID1,
            this.colClientBillRequirement,
            this.colTeamSelfBillRequirement,
            this.gridColumn9,
            this.colRequirementFulfilled,
            this.colCheckedByStaffID,
            this.colBillingRequirement,
            this.colLinkedToRecord1,
            this.colLinkedToRecordType1,
            this.colCheckedByStaff});
            this.gridViewBillingRequirement.GridControl = this.gridControlBillingRequirement;
            this.gridViewBillingRequirement.GroupCount = 1;
            this.gridViewBillingRequirement.Name = "gridViewBillingRequirement";
            this.gridViewBillingRequirement.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewBillingRequirement.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewBillingRequirement.OptionsLayout.StoreAppearance = true;
            this.gridViewBillingRequirement.OptionsLayout.StoreFormatRules = true;
            this.gridViewBillingRequirement.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewBillingRequirement.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewBillingRequirement.OptionsSelection.MultiSelect = true;
            this.gridViewBillingRequirement.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewBillingRequirement.OptionsView.ColumnAutoWidth = false;
            this.gridViewBillingRequirement.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewBillingRequirement.OptionsView.ShowGroupPanel = false;
            this.gridViewBillingRequirement.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToRecord1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBillingRequirement, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewBillingRequirement.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewBillingRequirement.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewBillingRequirement.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewBillingRequirement.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewBillingRequirement.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewBillingRequirement.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewBillingRequirement.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridControlBillingRequirement_MouseUp);
            this.gridViewBillingRequirement.DoubleClick += new System.EventHandler(this.gridControlBillingRequirement_DoubleClick);
            this.gridViewBillingRequirement.GotFocus += new System.EventHandler(this.gridControlBillingRequirement_GotFocus);
            // 
            // colBillingRequirementID
            // 
            this.colBillingRequirementID.Caption = "Billing Requirement ID";
            this.colBillingRequirementID.FieldName = "BillingRequirementID";
            this.colBillingRequirementID.Name = "colBillingRequirementID";
            this.colBillingRequirementID.OptionsColumn.AllowEdit = false;
            this.colBillingRequirementID.OptionsColumn.AllowFocus = false;
            this.colBillingRequirementID.OptionsColumn.ReadOnly = true;
            this.colBillingRequirementID.Width = 123;
            // 
            // colRequirementID
            // 
            this.colRequirementID.Caption = "Requirement ID";
            this.colRequirementID.FieldName = "RequirementID";
            this.colRequirementID.Name = "colRequirementID";
            this.colRequirementID.OptionsColumn.AllowEdit = false;
            this.colRequirementID.OptionsColumn.AllowFocus = false;
            this.colRequirementID.OptionsColumn.ReadOnly = true;
            this.colRequirementID.Width = 94;
            // 
            // colLinkedToRecordID1
            // 
            this.colLinkedToRecordID1.Caption = "Linked To Record ID";
            this.colLinkedToRecordID1.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID1.Name = "colLinkedToRecordID1";
            this.colLinkedToRecordID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID1.Width = 115;
            // 
            // colLinkedToRecordTypeID1
            // 
            this.colLinkedToRecordTypeID1.Caption = "Linked to Record Type ID";
            this.colLinkedToRecordTypeID1.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID1.Name = "colLinkedToRecordTypeID1";
            this.colLinkedToRecordTypeID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID1.Width = 140;
            // 
            // colClientBillRequirement
            // 
            this.colClientBillRequirement.Caption = "Client Bill Requirement";
            this.colClientBillRequirement.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colClientBillRequirement.FieldName = "ClientBillRequirement";
            this.colClientBillRequirement.Name = "colClientBillRequirement";
            this.colClientBillRequirement.OptionsColumn.AllowEdit = false;
            this.colClientBillRequirement.OptionsColumn.AllowFocus = false;
            this.colClientBillRequirement.OptionsColumn.ReadOnly = true;
            this.colClientBillRequirement.Visible = true;
            this.colClientBillRequirement.VisibleIndex = 1;
            this.colClientBillRequirement.Width = 125;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.ValueChecked = 1;
            this.repositoryItemCheckEdit5.ValueUnchecked = 0;
            // 
            // colTeamSelfBillRequirement
            // 
            this.colTeamSelfBillRequirement.Caption = "Team Self-Bill Requirement";
            this.colTeamSelfBillRequirement.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colTeamSelfBillRequirement.FieldName = "TeamSelfBillRequirement";
            this.colTeamSelfBillRequirement.Name = "colTeamSelfBillRequirement";
            this.colTeamSelfBillRequirement.OptionsColumn.AllowEdit = false;
            this.colTeamSelfBillRequirement.OptionsColumn.AllowFocus = false;
            this.colTeamSelfBillRequirement.OptionsColumn.ReadOnly = true;
            this.colTeamSelfBillRequirement.Visible = true;
            this.colTeamSelfBillRequirement.VisibleIndex = 2;
            this.colTeamSelfBillRequirement.Width = 146;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Remarks";
            this.gridColumn9.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.gridColumn9.FieldName = "Remarks";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 3;
            this.gridColumn9.Width = 159;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // colRequirementFulfilled
            // 
            this.colRequirementFulfilled.Caption = "Requirement Fullfilled";
            this.colRequirementFulfilled.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colRequirementFulfilled.FieldName = "RequirementFulfilled";
            this.colRequirementFulfilled.Name = "colRequirementFulfilled";
            this.colRequirementFulfilled.OptionsColumn.AllowEdit = false;
            this.colRequirementFulfilled.OptionsColumn.AllowFocus = false;
            this.colRequirementFulfilled.OptionsColumn.ReadOnly = true;
            this.colRequirementFulfilled.Width = 121;
            // 
            // colCheckedByStaffID
            // 
            this.colCheckedByStaffID.Caption = "Checked By Staff ID";
            this.colCheckedByStaffID.FieldName = "CheckedByStaffID";
            this.colCheckedByStaffID.Name = "colCheckedByStaffID";
            this.colCheckedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCheckedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCheckedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCheckedByStaffID.Width = 116;
            // 
            // colBillingRequirement
            // 
            this.colBillingRequirement.Caption = "Billing Requirement";
            this.colBillingRequirement.FieldName = "BillingRequirement";
            this.colBillingRequirement.Name = "colBillingRequirement";
            this.colBillingRequirement.OptionsColumn.AllowEdit = false;
            this.colBillingRequirement.OptionsColumn.AllowFocus = false;
            this.colBillingRequirement.OptionsColumn.ReadOnly = true;
            this.colBillingRequirement.Visible = true;
            this.colBillingRequirement.VisibleIndex = 0;
            this.colBillingRequirement.Width = 268;
            // 
            // colLinkedToRecord1
            // 
            this.colLinkedToRecord1.Caption = "Site Contract";
            this.colLinkedToRecord1.FieldName = "LinkedToRecord";
            this.colLinkedToRecord1.Name = "colLinkedToRecord1";
            this.colLinkedToRecord1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecord1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecord1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecord1.Visible = true;
            this.colLinkedToRecord1.VisibleIndex = 4;
            this.colLinkedToRecord1.Width = 469;
            // 
            // colLinkedToRecordType1
            // 
            this.colLinkedToRecordType1.Caption = "Linked To Record Type";
            this.colLinkedToRecordType1.FieldName = "LinkedToRecordType";
            this.colLinkedToRecordType1.Name = "colLinkedToRecordType1";
            this.colLinkedToRecordType1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordType1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordType1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordType1.Width = 142;
            // 
            // colCheckedByStaff
            // 
            this.colCheckedByStaff.Caption = "Checked By Staff";
            this.colCheckedByStaff.FieldName = "CheckedByStaff";
            this.colCheckedByStaff.Name = "colCheckedByStaff";
            this.colCheckedByStaff.OptionsColumn.AllowEdit = false;
            this.colCheckedByStaff.OptionsColumn.AllowFocus = false;
            this.colCheckedByStaff.OptionsColumn.ReadOnly = true;
            this.colCheckedByStaff.Width = 140;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bar1
            // 
            this.bar1.BarName = "GrittingCalloutToolbar";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(519, 261);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiImport, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiUtilities, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterContractsSelected, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSelectedCount, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiWizard, true)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Gritting Callout Toolbar";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 58;
            this.bbiRefresh.ImageOptions.ImageIndex = 0;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bbiImport
            // 
            this.bbiImport.Caption = "Import";
            this.bbiImport.Enabled = false;
            this.bbiImport.Id = 59;
            this.bbiImport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiImport.ImageOptions.Image")));
            this.bbiImport.Name = "bbiImport";
            this.bbiImport.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bsiUtilities
            // 
            this.bsiUtilities.Caption = "Utilities";
            this.bsiUtilities.Id = 64;
            this.bsiUtilities.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiUtilities.ImageOptions.Image")));
            this.bsiUtilities.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiInActive),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiOnHoldSiteContract, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiOffHoldSiteContract),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSetContractExpiryReason, true)});
            this.bsiUtilities.Name = "bsiUtilities";
            this.bsiUtilities.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiInActive
            // 
            this.bbiInActive.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiInActive.Caption = "Set Site Contract <b>In-Active</b>";
            this.bbiInActive.Id = 65;
            this.bbiInActive.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiInActive.ImageOptions.Image")));
            this.bbiInActive.Name = "bbiInActive";
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Set Site Contract In-Active - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to set selected Site Contract(s) as in-active.\r\nDoing this will optional" +
    "ly allow you to set all outstanding visits and jobs on the site contract as Canc" +
    "elled.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiInActive.SuperTip = superToolTip1;
            this.bbiInActive.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiInActive_ItemClick);
            // 
            // bbiOnHoldSiteContract
            // 
            this.bbiOnHoldSiteContract.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiOnHoldSiteContract.Caption = "Set Site Contract <b>On</b>-Hold";
            this.bbiOnHoldSiteContract.Id = 70;
            this.bbiOnHoldSiteContract.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiOnHoldSiteContract.ImageOptions.Image")));
            this.bbiOnHoldSiteContract.Name = "bbiOnHoldSiteContract";
            this.bbiOnHoldSiteContract.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiOnHoldSiteContract_ItemClick);
            // 
            // bbiOffHoldSiteContract
            // 
            this.bbiOffHoldSiteContract.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiOffHoldSiteContract.Caption = "Set Site Contract <b>Off</b>-Hold";
            this.bbiOffHoldSiteContract.Id = 71;
            this.bbiOffHoldSiteContract.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiOffHoldSiteContract.ImageOptions.Image")));
            this.bbiOffHoldSiteContract.Name = "bbiOffHoldSiteContract";
            this.bbiOffHoldSiteContract.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiOffHoldSiteContract_ItemClick);
            // 
            // bciFilterContractsSelected
            // 
            this.bciFilterContractsSelected.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bciFilterContractsSelected.Caption = "Filter Selected <b>Site Contracts</b>";
            this.bciFilterContractsSelected.Id = 72;
            this.bciFilterContractsSelected.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Filter_32x32;
            this.bciFilterContractsSelected.Name = "bciFilterContractsSelected";
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Filter Selected Site Contracts - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = resources.GetString("toolTipItem2.Text");
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bciFilterContractsSelected.SuperTip = superToolTip2;
            this.bciFilterContractsSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterContractsSelected_CheckedChanged);
            // 
            // bsiSelectedCount
            // 
            this.bsiSelectedCount.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSelectedCount.Caption = "0 Site Contracts Selected";
            this.bsiSelectedCount.Id = 66;
            this.bsiSelectedCount.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_16x16;
            this.bsiSelectedCount.Name = "bsiSelectedCount";
            this.bsiSelectedCount.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiSelectedCount.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // bsiWizard
            // 
            this.bsiWizard.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bsiWizard.Caption = "Wizard";
            this.bsiWizard.Id = 67;
            this.bsiWizard.ImageOptions.ImageIndex = 1;
            this.bsiWizard.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddWizard, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddVisitWizard, true)});
            this.bsiWizard.Name = "bsiWizard";
            this.bsiWizard.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiAddWizard
            // 
            this.bbiAddWizard.Caption = "Site Contract Wizard";
            this.bbiAddWizard.Id = 69;
            this.bbiAddWizard.ImageOptions.ImageIndex = 1;
            this.bbiAddWizard.Name = "bbiAddWizard";
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Site Contract Wizard - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to open the <b>Site Contract Wizard</b> screen.\r\n\r\nThe Wizard will guide" +
    " you step-by-step through the Site Contract creation process.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiAddWizard.SuperTip = superToolTip3;
            this.bbiAddWizard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddWizard_ItemClick);
            // 
            // bbiAddVisitWizard
            // 
            this.bbiAddVisitWizard.Caption = "Visit Wizard";
            this.bbiAddVisitWizard.Id = 68;
            this.bbiAddVisitWizard.ImageOptions.ImageIndex = 1;
            this.bbiAddVisitWizard.Name = "bbiAddVisitWizard";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Visit Wizard - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to open the <b>Visit Wizard</b> screen.\r\n\r\nThe Wizard will guide you ste" +
    "p-by-step through the visit creation process.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiAddVisitWizard.SuperTip = superToolTip4;
            this.bbiAddVisitWizard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddVisitWizard_ItemClick);
            // 
            // toolTipController1
            // 
            this.toolTipController1.CloseOnClick = DevExpress.Utils.DefaultBoolean.True;
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            // 
            // sp06052_OM_Client_Contract_ManagerTableAdapter
            // 
            this.sp06052_OM_Client_Contract_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp06088_OM_Site_Contract_ManagerTableAdapter
            // 
            this.sp06088_OM_Site_Contract_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp06110_OM_Site_Contract_Manager_Linked_YearsTableAdapter
            // 
            this.sp06110_OM_Site_Contract_Manager_Linked_YearsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06111_OM_Site_Contract_Manager_Linked_ProfilesTableAdapter
            // 
            this.sp06111_OM_Site_Contract_Manager_Linked_ProfilesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06112_OM_Site_Contract_Manager_Linked_ResponsibilitiesTableAdapter
            // 
            this.sp06112_OM_Site_Contract_Manager_Linked_ResponsibilitiesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06113_OM_Site_Contract_Manager_Linked_LabourTableAdapter
            // 
            this.sp06113_OM_Site_Contract_Manager_Linked_LabourTableAdapter.ClearBeforeFill = true;
            // 
            // sp06114_OM_Site_Contract_Manager_Linked_EquipmentTableAdapter
            // 
            this.sp06114_OM_Site_Contract_Manager_Linked_EquipmentTableAdapter.ClearBeforeFill = true;
            // 
            // sp06115_OM_Site_Contract_Manager_Linked_MaterialsTableAdapter
            // 
            this.sp06115_OM_Site_Contract_Manager_Linked_MaterialsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06326_OM_Client_POs_Linked_To_ParentTableAdapter
            // 
            this.sp06326_OM_Client_POs_Linked_To_ParentTableAdapter.ClearBeforeFill = true;
            // 
            // sp06345_OM_Site_Contract_Manager_Linked_Job_RatesTableAdapter
            // 
            this.sp06345_OM_Site_Contract_Manager_Linked_Job_RatesTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.refresh_32x32, "refresh_32x32", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection2.Images.SetKeyName(0, "refresh_32x32");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.wizard_32_32, "wizard_32_32", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection2.Images.SetKeyName(1, "wizard_32_32");
            // 
            // dockPanel1
            // 
            this.dockPanel1.AutoHideContainers.AddRange(new DevExpress.XtraBars.Docking.AutoHideContainer[] {
            this.hideContainerLeft});
            this.dockPanel1.Form = this;
            this.dockPanel1.MenuManager = this.barManager1;
            this.dockPanel1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // hideContainerLeft
            // 
            this.hideContainerLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.hideContainerLeft.Controls.Add(this.dockPanelFilters);
            this.hideContainerLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.hideContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.hideContainerLeft.Name = "hideContainerLeft";
            this.hideContainerLeft.Size = new System.Drawing.Size(23, 577);
            // 
            // dockPanelFilters
            // 
            this.dockPanelFilters.Controls.Add(this.dockPanel1_Container);
            this.dockPanelFilters.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.ID = new System.Guid("74ea37b4-0038-4577-83f7-870045aaedee");
            this.dockPanelFilters.Location = new System.Drawing.Point(0, 0);
            this.dockPanelFilters.Name = "dockPanelFilters";
            this.dockPanelFilters.Options.ShowCloseButton = false;
            this.dockPanelFilters.OriginalSize = new System.Drawing.Size(320, 200);
            this.dockPanelFilters.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.SavedIndex = 0;
            this.dockPanelFilters.Size = new System.Drawing.Size(320, 577);
            this.dockPanelFilters.Text = "Data Filter";
            this.dockPanelFilters.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(314, 545);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.popupContainerEditConstructionManager);
            this.layoutControl1.Controls.Add(this.popupContainerEditCMFilter);
            this.layoutControl1.Controls.Add(this.btnLoad);
            this.layoutControl1.Controls.Add(this.btnClearAllFilters);
            this.layoutControl1.Controls.Add(this.checkEditActiveOnly);
            this.layoutControl1.Controls.Add(this.popupContainerEditDateRange);
            this.layoutControl1.Controls.Add(this.buttonEditClientFilter);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(727, 374, 353, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(314, 545);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // popupContainerEditConstructionManager
            // 
            this.popupContainerEditConstructionManager.EditValue = "All Construction Managers";
            this.popupContainerEditConstructionManager.Location = new System.Drawing.Point(96, 112);
            this.popupContainerEditConstructionManager.MenuManager = this.barManager1;
            this.popupContainerEditConstructionManager.Name = "popupContainerEditConstructionManager";
            this.popupContainerEditConstructionManager.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditConstructionManager.Properties.PopupControl = this.popupContainerControlConstructionManagerFilter;
            this.popupContainerEditConstructionManager.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditConstructionManager.Size = new System.Drawing.Size(211, 20);
            this.popupContainerEditConstructionManager.StyleController = this.layoutControl1;
            this.popupContainerEditConstructionManager.TabIndex = 10;
            this.popupContainerEditConstructionManager.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditConstructionManager_QueryResultValue);
            // 
            // popupContainerEditCMFilter
            // 
            this.popupContainerEditCMFilter.EditValue = "All CMs";
            this.popupContainerEditCMFilter.Location = new System.Drawing.Point(96, 88);
            this.popupContainerEditCMFilter.MenuManager = this.barManager1;
            this.popupContainerEditCMFilter.Name = "popupContainerEditCMFilter";
            this.popupContainerEditCMFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditCMFilter.Properties.PopupControl = this.popupContainerControlCMFilter;
            this.popupContainerEditCMFilter.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditCMFilter.Size = new System.Drawing.Size(211, 20);
            this.popupContainerEditCMFilter.StyleController = this.layoutControl1;
            this.popupContainerEditCMFilter.TabIndex = 8;
            this.popupContainerEditCMFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditCMFilter_QueryResultValue);
            // 
            // btnLoad
            // 
            this.btnLoad.ImageOptions.ImageIndex = 16;
            this.btnLoad.ImageOptions.ImageList = this.imageCollection1;
            this.btnLoad.Location = new System.Drawing.Point(228, 146);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(79, 22);
            this.btnLoad.StyleController = this.layoutControl1;
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Load Data - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to Load Site Contract Data.\r\n\r\nOnly data matching the specified Filter C" +
    "riteria (From and To Dates and Person Responsibilities etc) will be loaded.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.btnLoad.SuperTip = superToolTip5;
            this.btnLoad.TabIndex = 18;
            this.btnLoad.Text = "Load Data";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click_1);
            // 
            // btnClearAllFilters
            // 
            this.btnClearAllFilters.ImageOptions.ImageIndex = 15;
            this.btnClearAllFilters.ImageOptions.ImageList = this.imageCollection1;
            this.btnClearAllFilters.Location = new System.Drawing.Point(7, 146);
            this.btnClearAllFilters.Name = "btnClearAllFilters";
            this.btnClearAllFilters.Size = new System.Drawing.Size(87, 22);
            this.btnClearAllFilters.StyleController = this.layoutControl1;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Clear Filters - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to clear all filters (except Date Range filter).";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.btnClearAllFilters.SuperTip = superToolTip6;
            this.btnClearAllFilters.TabIndex = 22;
            this.btnClearAllFilters.Text = "Clear Filters";
            this.btnClearAllFilters.Click += new System.EventHandler(this.btnClearAllFilters_Click);
            // 
            // checkEditActiveOnly
            // 
            this.checkEditActiveOnly.Location = new System.Drawing.Point(96, 55);
            this.checkEditActiveOnly.MenuManager = this.barManager1;
            this.checkEditActiveOnly.Name = "checkEditActiveOnly";
            this.checkEditActiveOnly.Properties.Caption = "[Tick if Yes]";
            this.checkEditActiveOnly.Size = new System.Drawing.Size(211, 19);
            this.checkEditActiveOnly.StyleController = this.layoutControl1;
            this.checkEditActiveOnly.TabIndex = 8;
            // 
            // popupContainerEditDateRange
            // 
            this.popupContainerEditDateRange.EditValue = "No Date Range Filter";
            this.popupContainerEditDateRange.Location = new System.Drawing.Point(96, 31);
            this.popupContainerEditDateRange.MenuManager = this.barManager1;
            this.popupContainerEditDateRange.Name = "popupContainerEditDateRange";
            this.popupContainerEditDateRange.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditDateRange.Properties.PopupControl = this.popupContainerControlDateRange;
            this.popupContainerEditDateRange.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditDateRange.Size = new System.Drawing.Size(211, 20);
            this.popupContainerEditDateRange.StyleController = this.layoutControl1;
            this.popupContainerEditDateRange.TabIndex = 7;
            this.popupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateRange_QueryResultValue);
            // 
            // buttonEditClientFilter
            // 
            this.buttonEditClientFilter.EditValue = "No Client Contract Filter";
            this.buttonEditClientFilter.Location = new System.Drawing.Point(96, 7);
            this.buttonEditClientFilter.MenuManager = this.barManager1;
            this.buttonEditClientFilter.Name = "buttonEditClientFilter";
            this.buttonEditClientFilter.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.buttonEditClientFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to open Choose Client Filter Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear Filter", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditClientFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditClientFilter.Size = new System.Drawing.Size(211, 20);
            this.buttonEditClientFilter.StyleController = this.layoutControl1;
            this.buttonEditClientFilter.TabIndex = 7;
            this.buttonEditClientFilter.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditClientFilter_ButtonClick);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem4,
            this.emptySpaceItem3,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.emptySpaceItem4});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(314, 545);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.buttonEditClientFilter;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem1.Text = "Client Contract:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.popupContainerEditDateRange;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem2.Text = "Date Range:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.checkEditActiveOnly;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(304, 23);
            this.layoutControlItem3.Text = "Active Only:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(86, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 165);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(304, 370);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 71);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(304, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnClearAllFilters;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 139);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(91, 26);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(91, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(91, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(91, 139);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(130, 26);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.btnLoad;
            this.layoutControlItem5.Location = new System.Drawing.Point(221, 139);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(83, 26);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(83, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(83, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.popupContainerEditCMFilter;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 81);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem6.Text = "CM:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(86, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.popupContainerEditConstructionManager;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 105);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem7.Text = "Construction Mgr:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(86, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 129);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(304, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp06388_OM_Client_Contract_Manager_Construction_Manager_FilterTableAdapter
            // 
            this.sp06388_OM_Client_Contract_Manager_Construction_Manager_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06020_OM_Job_Manager_CMs_FilterTableAdapter
            // 
            this.sp06020_OM_Job_Manager_CMs_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp01037_Core_Billing_Requirement_ItemsTableAdapter
            // 
            this.sp01037_Core_Billing_Requirement_ItemsTableAdapter.ClearBeforeFill = true;
            // 
            // bbiSetContractExpiryReason
            // 
            this.bbiSetContractExpiryReason.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSetContractExpiryReason.Caption = "Set Contract <b>Expiry</b> Reason";
            this.bbiSetContractExpiryReason.Id = 73;
            this.bbiSetContractExpiryReason.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.Image")));
            this.bbiSetContractExpiryReason.Name = "bbiSetContractExpiryReason";
            this.bbiSetContractExpiryReason.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSetContractExpiryReason_ItemClick);
            // 
            // frm_OM_Site_Contract_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1027, 577);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.hideContainerLeft);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Site_Contract_Manager";
            this.Text = "Site Contract Manager - Operations";
            this.Activated += new System.EventHandler(this.frm_OM_Site_Contract_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Site_Contract_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Site_Contract_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.hideContainerLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlContract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06088OMSiteContractManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewContract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsClientContract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06052OMClientContractManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlConstructionManagerFilter)).EndInit();
            this.popupContainerControlConstructionManagerFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06388OMClientContractManagerConstructionManagerFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCMFilter)).EndInit();
            this.popupContainerControlCMFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06020OMJobManagerCMsFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Summer_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageJobRates.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlJobRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06345OMSiteContractManagerLinkedJobRatesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewJobRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLatLong2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage3)).EndInit();
            this.xtraTabPageYears.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06110OMSiteContractManagerLinkedYearsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsClientContractYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlProfile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06111OMSiteContractManagerLinkedProfilesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProfile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML3)).EndInit();
            this.xtraTabPageResponsibilities.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlResponsibility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06112OMSiteContractManagerLinkedResponsibilitiesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewResponsibility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML4)).EndInit();
            this.xtraTabPageLabour.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLabour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06113OMSiteContractManagerLinkedLabourBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLabour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML5)).EndInit();
            this.xtraTabPageEquipment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEquipment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06114OMSiteContractManagerLinkedEquipmentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEquipment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEditPercent3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML6)).EndInit();
            this.xtraTabPageMaterial.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMaterial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06115OMSiteContractManagerLinkedMaterialsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMaterial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEditPercent4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML7)).EndInit();
            this.xtraTabPageClientPOs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlClientPO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06326OMClientPOsLinkedToParentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Client_PO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewClientPO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency9)).EndInit();
            this.xtraTabPageBillingRequirements.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBillingRequirement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01037CoreBillingRequirementItemsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBillingRequirement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockPanel1)).EndInit();
            this.hideContainerLeft.ResumeLayout(false);
            this.dockPanelFilters.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditConstructionManager.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditCMFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditActiveOnly.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditDateRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditClientFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControlContract;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewContract;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private WoodPlan5.DataSet_AT dataSet_AT;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarButtonItem bbiImport;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControlYear;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewYear;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.GridControl gridControlProfile;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewProfile;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeOK;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private System.Windows.Forms.BindingSource sp06052OMClientContractManagerBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06052_OM_Client_Contract_ManagerTableAdapter sp06052_OM_Client_Contract_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractYearID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colYearDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colActive1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientValue;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colContractEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractProfileID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractYearID1;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceDateDue;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceDateActual;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedBillAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colActualBillAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colBilledByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID2;
        private DevExpress.XtraGrid.Columns.GridColumn colYearDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colYearStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colYearEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colYearActive;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent1;
        private DevExpress.XtraGrid.Columns.GridColumn colBilledByPerson;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageYears;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageResponsibilities;
        private DevExpress.XtraGrid.GridControl gridControlResponsibility;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewResponsibility;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonResponsibilityID;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibleForRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibleForRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibilityTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName3;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStartDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colContractEndDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent2;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibilityType;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffName;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocumentsClientContract;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocumentsClientContractYear;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLabour;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageEquipment;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageMaterial;
        private System.Windows.Forms.BindingSource sp06088OMSiteContractManagerBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colContractValue;
        private DevExpress.XtraGrid.Columns.GridColumn colYearlyPercentageIncrease;
        private DevExpress.XtraGrid.Columns.GridColumn colLastClientPaymentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colGCCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDirectorID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractActive;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractValue;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckRPIDate;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colGCCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colContractType;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDirector;
        private DevExpress.XtraGrid.Columns.GridColumn colSectorType;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedVisitCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount;
        private DataSet_OM_ContractTableAdapters.sp06088_OM_Site_Contract_ManagerTableAdapter sp06088_OM_Site_Contract_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent3;
        private System.Windows.Forms.BindingSource sp06110OMSiteContractManagerLinkedYearsBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06110_OM_Site_Contract_Manager_Linked_YearsTableAdapter sp06110_OM_Site_Contract_Manager_Linked_YearsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParentFull;
        private System.Windows.Forms.BindingSource sp06111OMSiteContractManagerLinkedProfilesBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06111_OM_Site_Contract_Manager_Linked_ProfilesTableAdapter sp06111_OM_Site_Contract_Manager_Linked_ProfilesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParentFull1;
        private System.Windows.Forms.BindingSource sp06112OMSiteContractManagerLinkedResponsibilitiesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName3;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParentFull2;
        private DataSet_OM_ContractTableAdapters.sp06112_OM_Site_Contract_Manager_Linked_ResponsibilitiesTableAdapter sp06112_OM_Site_Contract_Manager_Linked_ResponsibilitiesTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControlLabour;
        private System.Windows.Forms.BindingSource sp06113OMSiteContractManagerLinkedLabourBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLabour;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractLabourCostID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSellUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnitExVat;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnitVatRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercent;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnitExVat;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnitVatRate;
        private DevExpress.XtraGrid.Columns.GridColumn colCISPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colCISValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcodeSiteDistance;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMiles;
        private DevExpress.XtraGrid.Columns.GridColumn colLatLongSiteDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName4;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent4;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParentFull3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID4;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode;
        private DataSet_OM_ContractTableAdapters.sp06113_OM_Site_Contract_Manager_Linked_LabourTableAdapter sp06113_OM_Site_Contract_Manager_Linked_LabourTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControlEquipment;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewEquipment;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitDescriptorID2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn64;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn65;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn66;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn71;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn72;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn73;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn74;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn75;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn76;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerName;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerType;
        private DevExpress.XtraGrid.GridControl gridControlMaterial;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewMaterial;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn89;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn90;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn91;
        private DevExpress.XtraGrid.Columns.GridColumn colCostUnitDescriptor3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn96;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn97;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn98;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn99;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn100;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn101;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn102;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn103;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn104;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn105;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn106;
        private System.Windows.Forms.BindingSource sp06114OMSiteContractManagerLinkedEquipmentBindingSource;
        private System.Windows.Forms.BindingSource sp06115OMSiteContractManagerLinkedMaterialsBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06114_OM_Site_Contract_Manager_Linked_EquipmentTableAdapter sp06114_OM_Site_Contract_Manager_Linked_EquipmentTableAdapter;
        private DataSet_OM_ContractTableAdapters.sp06115_OM_Site_Contract_Manager_Linked_MaterialsTableAdapter sp06115_OM_Site_Contract_Manager_Linked_MaterialsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEditPercent3;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent5;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParentFull4;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEditPercent4;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent6;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParentFull5;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromClientContractYearID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromClientContractProfileID;
        private DevExpress.XtraGrid.Columns.GridColumn colYearlyPercentageIncrease1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteInstructions;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedEquipmentCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedLabourCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedMaterialCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedPersonResponsibilityCount;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationX1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditLatLong;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationY1;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageClientPOs;
        private DevExpress.XtraGrid.GridControl gridControlClientPO;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewClientPO;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPurchaseOrderLinkID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPurchaseOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colIsDefault;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEditRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName4;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecord;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime9;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colStartingValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency9;
        private DevExpress.XtraGrid.Columns.GridColumn colUsedValue;
        private DevExpress.XtraGrid.Columns.GridColumn colRemainingValue;
        private DevExpress.XtraGrid.Columns.GridColumn colWarningValue;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID4;
        private System.Windows.Forms.BindingSource sp06326OMClientPOsLinkedToParentBindingSource;
        private DataSet_OM_Client_PO dataSet_OM_Client_PO;
        private DataSet_OM_Client_POTableAdapters.sp06326_OM_Client_POs_Linked_To_ParentTableAdapter sp06326_OM_Client_POs_Linked_To_ParentTableAdapter;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageJobRates;
        private DevExpress.XtraGrid.GridControl gridControlJobRate;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewJobRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurency;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditLatLong2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime6;
        private System.Windows.Forms.BindingSource sp06345OMSiteContractManagerLinkedJobRatesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractJobRateID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID3;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colFromDate;
        private DevExpress.XtraGrid.Columns.GridColumn colToDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamCost;
        private DevExpress.XtraGrid.Columns.GridColumn colClientSell;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks5;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName5;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStartDate3;
        private DevExpress.XtraGrid.Columns.GridColumn colContractEndDate3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName5;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent7;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationX2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationY2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID7;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParentFull6;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobType;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSubType;
        private DataSet_OM_ContractTableAdapters.sp06345_OM_Site_Contract_Manager_Linked_Job_RatesTableAdapter sp06345_OM_Site_Contract_Manager_Linked_Job_RatesTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientBillingType;
        private DevExpress.XtraGrid.Columns.GridColumn colClientBillingTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonTypeID;
        private DevExpress.XtraBars.Docking.DockManager dockPanel1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelFilters;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.ButtonEdit buttonEditClientFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditDateRange;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.CheckEdit checkEditActiveOnly;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.SimpleButton btnClearAllFilters;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.SimpleButton btnLoad;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditCMFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditConstructionManager;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlCMFilter;
        private DevExpress.XtraEditors.SimpleButton btnCMFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription3;
        private DevExpress.XtraGrid.Columns.GridColumn colForename;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlConstructionManagerFilter;
        private DevExpress.XtraEditors.SimpleButton btnConstructionManagerFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private System.Windows.Forms.BindingSource sp06388OMClientContractManagerConstructionManagerFilterBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06388_OM_Client_Contract_Manager_Construction_Manager_FilterTableAdapter sp06388_OM_Client_Contract_Manager_Construction_Manager_FilterTableAdapter;
        private DataSet_GC_Summer_Core dataSet_GC_Summer_Core;
        private System.Windows.Forms.BindingSource sp06020OMJobManagerCMsFilterBindingSource;
        private DataSet_GC_Summer_CoreTableAdapters.sp06020_OM_Job_Manager_CMs_FilterTableAdapter sp06020_OM_Job_Manager_CMs_FilterTableAdapter;
        private DevExpress.XtraBars.Docking.AutoHideContainer hideContainerLeft;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML7;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML8;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML9;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultScheduleTemplate;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultScheduleTemplateID;
        private DevExpress.XtraBars.BarSubItem bsiUtilities;
        private DevExpress.XtraBars.BarButtonItem bbiInActive;
        private DevExpress.XtraBars.BarStaticItem bsiSelectedCount;
        private DevExpress.XtraBars.BarSubItem bsiWizard;
        public DevExpress.XtraBars.BarButtonItem bbiAddVisitWizard;
        public DevExpress.XtraBars.BarButtonItem bbiAddWizard;
        private DevExpress.XtraBars.BarButtonItem bbiOnHoldSiteContract;
        private DevExpress.XtraBars.BarButtonItem bbiOffHoldSiteContract;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHold;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldReason;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldEndDate;
        private DevExpress.XtraBars.BarCheckItem bciFilterContractsSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysSeparationPercent;
        private DevExpress.XtraGrid.Columns.GridColumn colDETeamCostPercentagePaid;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageBillingRequirements;
        private DevExpress.XtraGrid.GridControl gridControlBillingRequirement;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewBillingRequirement;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private System.Windows.Forms.BindingSource sp01037CoreBillingRequirementItemsBindingSource;
        private DataSet_Common_Functionality dataSet_Common_Functionality;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingRequirementID;
        private DevExpress.XtraGrid.Columns.GridColumn colRequirementID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientBillRequirement;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamSelfBillRequirement;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn colRequirementFulfilled;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingRequirement;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecord1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordType1;
        private DevExpress.XtraGrid.Columns.GridColumn colCheckedByStaff;
        private DataSet_Common_FunctionalityTableAdapters.sp01037_Core_Billing_Requirement_ItemsTableAdapter sp01037_Core_Billing_Requirement_ItemsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceLevelID;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultJobCollectionTemplate;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultJobCollectionTemplateID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractExpiredReasonID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractExpiredReason;
        private DevExpress.XtraBars.BarButtonItem bbiSetContractExpiryReason;
    }
}

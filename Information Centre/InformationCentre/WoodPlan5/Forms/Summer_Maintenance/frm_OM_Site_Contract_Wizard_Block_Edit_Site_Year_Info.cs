﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Site_Contract_Wizard_Block_Edit_Site_Year_Info : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public DateTime? dtStartDate = null;
        public DateTime? dtEndDate = null;
        public string strYearDescription = null;
        public int? intActive = null;
        public decimal? decContractValue = null;
        public string strRemarks = null;
        public decimal? decYearlyPercentageIncrease = null;
        #endregion

        public frm_OM_Site_Contract_Wizard_Block_Edit_Site_Year_Info()
        {
            InitializeComponent();
        }

        private void frm_OM_Site_Contract_Wizard_Block_Edit_Site_Year_Info_Load(object sender, EventArgs e)
        {
            this.FormID = 500133;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //

            StartDateDateEdit.EditValue = null;
            EndDateDateEdit.EditValue = null;
            YearDescriptionTextEdit.EditValue = null;
            ActiveCheckEdit.EditValue = null;
            ClientValueSpinEdit.EditValue = null;
            RemarksMemoEdit.EditValue = null;
            YearlyPercentageIncreaseSpinEdit.EditValue = null;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (StartDateDateEdit.EditValue != null) dtStartDate = Convert.ToDateTime(StartDateDateEdit.EditValue);
            if (EndDateDateEdit.EditValue != null) dtEndDate = Convert.ToDateTime(EndDateDateEdit.EditValue);
            if (YearDescriptionTextEdit.EditValue != null) strYearDescription = YearDescriptionTextEdit.EditValue.ToString();
            if (ActiveCheckEdit.EditValue != null) intActive = Convert.ToInt32(ActiveCheckEdit.EditValue);
            if (ClientValueSpinEdit.EditValue != null) decContractValue = Convert.ToDecimal(ClientValueSpinEdit.EditValue);
            if (RemarksMemoEdit.EditValue != null) strRemarks = RemarksMemoEdit.EditValue.ToString();
            if (YearlyPercentageIncreaseSpinEdit.EditValue != null) decYearlyPercentageIncrease = Convert.ToDecimal(YearlyPercentageIncreaseSpinEdit.EditValue);
              
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }






    }
}

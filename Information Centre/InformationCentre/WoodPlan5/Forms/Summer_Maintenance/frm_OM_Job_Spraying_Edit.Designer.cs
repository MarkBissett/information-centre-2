namespace WoodPlan5
{
    partial class frm_OM_Job_Spraying_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Job_Spraying_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLinkedDocuments = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.MinMaxErrorLabel = new DevExpress.XtraEditors.LabelControl();
            this.AmountUsedDilutedSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.sp06230OMJobSprayingEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.DilutionRatioSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LinkedToRecordTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobExpectedStartDateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobSubTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobSubTypeDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobTypeDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameContractDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AmountUsedDescriptorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06108OMUnitDescriptorsPicklistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AmountUsedSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ChemicalNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.SprayingIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LinkedToRecordIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.LinkedToParentDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ChemicalIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForClientContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobSubTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobSubTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForChemicalID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSprayingID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToRecordID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToRecordTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLinkedToParentDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layGrpAddress = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForAmountUsed = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForChemicalName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAmountUsedDescriptorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDilutionRatio = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAmountUsedDiluted = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForWarningLabel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientNameContractDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForJobExpectedStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp06108_OM_Unit_Descriptors_PicklistTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06108_OM_Unit_Descriptors_PicklistTableAdapter();
            this.sp06230_OM_Job_Spraying_EditTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06230_OM_Job_Spraying_EditTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AmountUsedDilutedSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06230OMJobSprayingEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DilutionRatioSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToRecordTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobExpectedStartDateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameContractDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmountUsedDescriptorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06108OMUnitDescriptorsPicklistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmountUsedSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChemicalNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SprayingIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToRecordIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToParentDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChemicalIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChemicalID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSprayingID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToRecordID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToRecordTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToParentDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmountUsed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChemicalName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmountUsedDescriptorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDilutionRatio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmountUsedDiluted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarningLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientNameContractDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobExpectedStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 614);
            this.barDockControlBottom.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 588);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(686, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 588);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colID1.Width = 53;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiLinkedDocuments});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLinkedDocuments, true)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiLinkedDocuments
            // 
            this.bbiLinkedDocuments.Caption = "Linked Documents";
            this.bbiLinkedDocuments.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLinkedDocuments.Glyph")));
            this.bbiLinkedDocuments.Id = 15;
            this.bbiLinkedDocuments.Name = "bbiLinkedDocuments";
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Linked Documents - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to open the Linked Documents Manager, filtered on the current data entry" +
    " record.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiLinkedDocuments.SuperTip = superToolTip3;
            this.bbiLinkedDocuments.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLinkedDocuments_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Form Mode - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.barStaticItemFormMode.SuperTip = superToolTip4;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(686, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 614);
            this.barDockControl2.Size = new System.Drawing.Size(686, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 588);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(686, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 588);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.MinMaxErrorLabel);
            this.dataLayoutControl1.Controls.Add(this.AmountUsedDilutedSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.DilutionRatioSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToRecordTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobExpectedStartDateTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientContractIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteContractIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobSubTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobSubTypeDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobTypeDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameContractDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AmountUsedDescriptorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.AmountUsedSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ChemicalNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.SprayingIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToRecordIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.LinkedToParentDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ChemicalIDTextEdit);
            this.dataLayoutControl1.DataSource = this.sp06230OMJobSprayingEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientContractID,
            this.ItemForClientID,
            this.ItemForClientName,
            this.ItemForVisitID,
            this.ItemForSiteID,
            this.ItemForSiteContractID,
            this.ItemForJobTypeID,
            this.ItemForJobTypeDescription,
            this.ItemForJobSubTypeID,
            this.ItemForJobSubTypeDescription,
            this.ItemForChemicalID,
            this.ItemForSprayingID,
            this.ItemForLinkedToRecordID,
            this.ItemForLinkedToRecordTypeID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1234, 147, 301, 422);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(686, 588);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // MinMaxErrorLabel
            // 
            this.MinMaxErrorLabel.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("MinMaxErrorLabel.Appearance.Image")));
            this.MinMaxErrorLabel.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.MinMaxErrorLabel.Location = new System.Drawing.Point(349, 284);
            this.MinMaxErrorLabel.Name = "MinMaxErrorLabel";
            this.MinMaxErrorLabel.Size = new System.Drawing.Size(301, 20);
            this.MinMaxErrorLabel.StyleController = this.dataLayoutControl1;
            this.MinMaxErrorLabel.TabIndex = 70;
            this.MinMaxErrorLabel.Text = "       Ratio Outwith Chemical Min\\Max                              ";
            // 
            // AmountUsedDilutedSpinEdit
            // 
            this.AmountUsedDilutedSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "AmountUsedDiluted", true));
            this.AmountUsedDilutedSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AmountUsedDilutedSpinEdit.Location = new System.Drawing.Point(146, 308);
            this.AmountUsedDilutedSpinEdit.MenuManager = this.barManager1;
            this.AmountUsedDilutedSpinEdit.Name = "AmountUsedDilutedSpinEdit";
            this.AmountUsedDilutedSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AmountUsedDilutedSpinEdit.Properties.Mask.EditMask = "n2";
            this.AmountUsedDilutedSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AmountUsedDilutedSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            131072});
            this.AmountUsedDilutedSpinEdit.Size = new System.Drawing.Size(199, 20);
            this.AmountUsedDilutedSpinEdit.StyleController = this.dataLayoutControl1;
            this.AmountUsedDilutedSpinEdit.TabIndex = 5;
            this.AmountUsedDilutedSpinEdit.ValueChanged += new System.EventHandler(this.AmountUsedDilutedSpinEdit_ValueChanged);
            this.AmountUsedDilutedSpinEdit.EditValueChanged += new System.EventHandler(this.AmountUsedDilutedSpinEdit_EditValueChanged);
            // 
            // sp06230OMJobSprayingEditBindingSource
            // 
            this.sp06230OMJobSprayingEditBindingSource.DataMember = "sp06230_OM_Job_Spraying_Edit";
            this.sp06230OMJobSprayingEditBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DilutionRatioSpinEdit
            // 
            this.DilutionRatioSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "DilutionRatio", true));
            this.DilutionRatioSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DilutionRatioSpinEdit.Location = new System.Drawing.Point(146, 284);
            this.DilutionRatioSpinEdit.MenuManager = this.barManager1;
            this.DilutionRatioSpinEdit.Name = "DilutionRatioSpinEdit";
            this.DilutionRatioSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DilutionRatioSpinEdit.Properties.Mask.EditMask = "1 : #####0.00";
            this.DilutionRatioSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DilutionRatioSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            131072});
            this.DilutionRatioSpinEdit.Size = new System.Drawing.Size(199, 20);
            this.DilutionRatioSpinEdit.StyleController = this.dataLayoutControl1;
            this.DilutionRatioSpinEdit.TabIndex = 4;
            this.DilutionRatioSpinEdit.ValueChanged += new System.EventHandler(this.DilutionRatioSpinEdit_ValueChanged);
            this.DilutionRatioSpinEdit.EditValueChanged += new System.EventHandler(this.DilutionRatioSpinEdit_EditValueChanged);
            this.DilutionRatioSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DilutionRatioSpinEdit_Validating);
            // 
            // LinkedToRecordTypeIDTextEdit
            // 
            this.LinkedToRecordTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "LinkedToRecordTypeID", true));
            this.LinkedToRecordTypeIDTextEdit.Location = new System.Drawing.Point(142, 387);
            this.LinkedToRecordTypeIDTextEdit.MenuManager = this.barManager1;
            this.LinkedToRecordTypeIDTextEdit.Name = "LinkedToRecordTypeIDTextEdit";
            this.LinkedToRecordTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToRecordTypeIDTextEdit, true);
            this.LinkedToRecordTypeIDTextEdit.Size = new System.Drawing.Size(50, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToRecordTypeIDTextEdit, optionsSpelling1);
            this.LinkedToRecordTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToRecordTypeIDTextEdit.TabIndex = 69;
            // 
            // JobExpectedStartDateTextEdit
            // 
            this.JobExpectedStartDateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "JobExpectedStartDate", true));
            this.JobExpectedStartDateTextEdit.Location = new System.Drawing.Point(122, 131);
            this.JobExpectedStartDateTextEdit.MenuManager = this.barManager1;
            this.JobExpectedStartDateTextEdit.Name = "JobExpectedStartDateTextEdit";
            this.JobExpectedStartDateTextEdit.Properties.Mask.EditMask = "g";
            this.JobExpectedStartDateTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.JobExpectedStartDateTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.JobExpectedStartDateTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobExpectedStartDateTextEdit, true);
            this.JobExpectedStartDateTextEdit.Size = new System.Drawing.Size(186, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobExpectedStartDateTextEdit, optionsSpelling2);
            this.JobExpectedStartDateTextEdit.StyleController = this.dataLayoutControl1;
            this.JobExpectedStartDateTextEdit.TabIndex = 68;
            // 
            // ClientContractIDTextEdit
            // 
            this.ClientContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "ClientContractID", true));
            this.ClientContractIDTextEdit.Location = new System.Drawing.Point(117, 157);
            this.ClientContractIDTextEdit.MenuManager = this.barManager1;
            this.ClientContractIDTextEdit.Name = "ClientContractIDTextEdit";
            this.ClientContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientContractIDTextEdit, true);
            this.ClientContractIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientContractIDTextEdit, optionsSpelling3);
            this.ClientContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientContractIDTextEdit.TabIndex = 67;
            this.ClientContractIDTextEdit.TabStop = false;
            // 
            // SiteContractIDTextEdit
            // 
            this.SiteContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "SiteContractID", true));
            this.SiteContractIDTextEdit.Location = new System.Drawing.Point(117, 301);
            this.SiteContractIDTextEdit.MenuManager = this.barManager1;
            this.SiteContractIDTextEdit.Name = "SiteContractIDTextEdit";
            this.SiteContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteContractIDTextEdit, true);
            this.SiteContractIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteContractIDTextEdit, optionsSpelling4);
            this.SiteContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteContractIDTextEdit.TabIndex = 66;
            this.SiteContractIDTextEdit.TabStop = false;
            // 
            // JobTypeIDTextEdit
            // 
            this.JobTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "JobTypeID", true));
            this.JobTypeIDTextEdit.Location = new System.Drawing.Point(117, 229);
            this.JobTypeIDTextEdit.MenuManager = this.barManager1;
            this.JobTypeIDTextEdit.Name = "JobTypeIDTextEdit";
            this.JobTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobTypeIDTextEdit, true);
            this.JobTypeIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobTypeIDTextEdit, optionsSpelling5);
            this.JobTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.JobTypeIDTextEdit.TabIndex = 65;
            this.JobTypeIDTextEdit.TabStop = false;
            // 
            // JobSubTypeIDTextEdit
            // 
            this.JobSubTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "JobSubTypeID", true));
            this.JobSubTypeIDTextEdit.Location = new System.Drawing.Point(117, 214);
            this.JobSubTypeIDTextEdit.MenuManager = this.barManager1;
            this.JobSubTypeIDTextEdit.Name = "JobSubTypeIDTextEdit";
            this.JobSubTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobSubTypeIDTextEdit, true);
            this.JobSubTypeIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobSubTypeIDTextEdit, optionsSpelling6);
            this.JobSubTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.JobSubTypeIDTextEdit.TabIndex = 64;
            this.JobSubTypeIDTextEdit.TabStop = false;
            // 
            // JobSubTypeDescriptionTextEdit
            // 
            this.JobSubTypeDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "JobSubTypeDescription", true));
            this.JobSubTypeDescriptionTextEdit.Location = new System.Drawing.Point(117, 214);
            this.JobSubTypeDescriptionTextEdit.MenuManager = this.barManager1;
            this.JobSubTypeDescriptionTextEdit.Name = "JobSubTypeDescriptionTextEdit";
            this.JobSubTypeDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobSubTypeDescriptionTextEdit, true);
            this.JobSubTypeDescriptionTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobSubTypeDescriptionTextEdit, optionsSpelling7);
            this.JobSubTypeDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.JobSubTypeDescriptionTextEdit.TabIndex = 59;
            this.JobSubTypeDescriptionTextEdit.TabStop = false;
            // 
            // JobTypeDescriptionTextEdit
            // 
            this.JobTypeDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "JobTypeDescription", true));
            this.JobTypeDescriptionTextEdit.Location = new System.Drawing.Point(117, 214);
            this.JobTypeDescriptionTextEdit.MenuManager = this.barManager1;
            this.JobTypeDescriptionTextEdit.Name = "JobTypeDescriptionTextEdit";
            this.JobTypeDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobTypeDescriptionTextEdit, true);
            this.JobTypeDescriptionTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobTypeDescriptionTextEdit, optionsSpelling8);
            this.JobTypeDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.JobTypeDescriptionTextEdit.TabIndex = 58;
            this.JobTypeDescriptionTextEdit.TabStop = false;
            // 
            // ClientNameTextEdit
            // 
            this.ClientNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "ClientName", true));
            this.ClientNameTextEdit.Location = new System.Drawing.Point(117, 157);
            this.ClientNameTextEdit.MenuManager = this.barManager1;
            this.ClientNameTextEdit.Name = "ClientNameTextEdit";
            this.ClientNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameTextEdit, true);
            this.ClientNameTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameTextEdit, optionsSpelling9);
            this.ClientNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameTextEdit.TabIndex = 57;
            this.ClientNameTextEdit.TabStop = false;
            // 
            // VisitNumberTextEdit
            // 
            this.VisitNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "VisitNumber", true));
            this.VisitNumberTextEdit.Location = new System.Drawing.Point(122, 83);
            this.VisitNumberTextEdit.MenuManager = this.barManager1;
            this.VisitNumberTextEdit.Name = "VisitNumberTextEdit";
            this.VisitNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitNumberTextEdit, true);
            this.VisitNumberTextEdit.Size = new System.Drawing.Size(552, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitNumberTextEdit, optionsSpelling10);
            this.VisitNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitNumberTextEdit.TabIndex = 56;
            this.VisitNumberTextEdit.TabStop = false;
            // 
            // SiteNameTextEdit
            // 
            this.SiteNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "SiteName", true));
            this.SiteNameTextEdit.Location = new System.Drawing.Point(122, 59);
            this.SiteNameTextEdit.MenuManager = this.barManager1;
            this.SiteNameTextEdit.Name = "SiteNameTextEdit";
            this.SiteNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteNameTextEdit, true);
            this.SiteNameTextEdit.Size = new System.Drawing.Size(552, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteNameTextEdit, optionsSpelling11);
            this.SiteNameTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteNameTextEdit.TabIndex = 1;
            this.SiteNameTextEdit.TabStop = false;
            // 
            // ClientNameContractDescriptionTextEdit
            // 
            this.ClientNameContractDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "ClientNameContractDescription", true));
            this.ClientNameContractDescriptionTextEdit.Location = new System.Drawing.Point(122, 35);
            this.ClientNameContractDescriptionTextEdit.MenuManager = this.barManager1;
            this.ClientNameContractDescriptionTextEdit.Name = "ClientNameContractDescriptionTextEdit";
            this.ClientNameContractDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameContractDescriptionTextEdit, true);
            this.ClientNameContractDescriptionTextEdit.Size = new System.Drawing.Size(552, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameContractDescriptionTextEdit, optionsSpelling12);
            this.ClientNameContractDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameContractDescriptionTextEdit.TabIndex = 0;
            this.ClientNameContractDescriptionTextEdit.TabStop = false;
            // 
            // VisitIDTextEdit
            // 
            this.VisitIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "VisitID", true));
            this.VisitIDTextEdit.Location = new System.Drawing.Point(441, 253);
            this.VisitIDTextEdit.MenuManager = this.barManager1;
            this.VisitIDTextEdit.Name = "VisitIDTextEdit";
            this.VisitIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitIDTextEdit, true);
            this.VisitIDTextEdit.Size = new System.Drawing.Size(216, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitIDTextEdit, optionsSpelling13);
            this.VisitIDTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitIDTextEdit.TabIndex = 53;
            // 
            // SiteIDTextEdit
            // 
            this.SiteIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "SiteID", true));
            this.SiteIDTextEdit.Location = new System.Drawing.Point(117, 325);
            this.SiteIDTextEdit.MenuManager = this.barManager1;
            this.SiteIDTextEdit.Name = "SiteIDTextEdit";
            this.SiteIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteIDTextEdit, true);
            this.SiteIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteIDTextEdit, optionsSpelling14);
            this.SiteIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteIDTextEdit.TabIndex = 52;
            // 
            // ClientIDTextEdit
            // 
            this.ClientIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "ClientID", true));
            this.ClientIDTextEdit.Location = new System.Drawing.Point(117, 157);
            this.ClientIDTextEdit.MenuManager = this.barManager1;
            this.ClientIDTextEdit.Name = "ClientIDTextEdit";
            this.ClientIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientIDTextEdit, true);
            this.ClientIDTextEdit.Size = new System.Drawing.Size(540, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientIDTextEdit, optionsSpelling15);
            this.ClientIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDTextEdit.TabIndex = 51;
            // 
            // AmountUsedDescriptorIDGridLookUpEdit
            // 
            this.AmountUsedDescriptorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "AmountUsedDescriptorID", true));
            this.AmountUsedDescriptorIDGridLookUpEdit.Location = new System.Drawing.Point(459, 260);
            this.AmountUsedDescriptorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.AmountUsedDescriptorIDGridLookUpEdit.Name = "AmountUsedDescriptorIDGridLookUpEdit";
            this.AmountUsedDescriptorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AmountUsedDescriptorIDGridLookUpEdit.Properties.DataSource = this.sp06108OMUnitDescriptorsPicklistBindingSource;
            this.AmountUsedDescriptorIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.AmountUsedDescriptorIDGridLookUpEdit.Properties.NullText = "";
            this.AmountUsedDescriptorIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.AmountUsedDescriptorIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.AmountUsedDescriptorIDGridLookUpEdit.Size = new System.Drawing.Size(191, 20);
            this.AmountUsedDescriptorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.AmountUsedDescriptorIDGridLookUpEdit.TabIndex = 3;
            this.AmountUsedDescriptorIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.AmountUsedDescriptorIDGridLookUpEdit_Validating);
            // 
            // sp06108OMUnitDescriptorsPicklistBindingSource
            // 
            this.sp06108OMUnitDescriptorsPicklistBindingSource.DataMember = "sp06108_OM_Unit_Descriptors_Picklist";
            this.sp06108OMUnitDescriptorsPicklistBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colID1;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Unit Descriptor";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "RecordOrder";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // AmountUsedSpinEdit
            // 
            this.AmountUsedSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "AmountUsed", true));
            this.AmountUsedSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AmountUsedSpinEdit.Location = new System.Drawing.Point(146, 260);
            this.AmountUsedSpinEdit.MenuManager = this.barManager1;
            this.AmountUsedSpinEdit.Name = "AmountUsedSpinEdit";
            this.AmountUsedSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AmountUsedSpinEdit.Properties.Mask.EditMask = "n2";
            this.AmountUsedSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AmountUsedSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            131072});
            this.AmountUsedSpinEdit.Size = new System.Drawing.Size(199, 20);
            this.AmountUsedSpinEdit.StyleController = this.dataLayoutControl1;
            this.AmountUsedSpinEdit.TabIndex = 2;
            this.AmountUsedSpinEdit.EditValueChanged += new System.EventHandler(this.AmountUsedSpinEdit_EditValueChanged);
            this.AmountUsedSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.AmountUsedSpinEdit_Validating);
            this.AmountUsedSpinEdit.Validated += new System.EventHandler(this.AmountUsedSpinEdit_Validated);
            // 
            // ChemicalNameButtonEdit
            // 
            this.ChemicalNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "ChemicalName", true));
            this.ChemicalNameButtonEdit.EditValue = "";
            this.ChemicalNameButtonEdit.Location = new System.Drawing.Point(146, 236);
            this.ChemicalNameButtonEdit.MenuManager = this.barManager1;
            this.ChemicalNameButtonEdit.Name = "ChemicalNameButtonEdit";
            this.ChemicalNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to open the Select Waste Type screen", "choose", null, true)});
            this.ChemicalNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ChemicalNameButtonEdit.Size = new System.Drawing.Size(504, 20);
            this.ChemicalNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.ChemicalNameButtonEdit.TabIndex = 1;
            this.ChemicalNameButtonEdit.TabStop = false;
            this.ChemicalNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ChemicalNameButtonEdit_ButtonClick);
            this.ChemicalNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ChemicalNameButtonEdit_Validating);
            // 
            // SprayingIDTextEdit
            // 
            this.SprayingIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "SprayingID", true));
            this.SprayingIDTextEdit.Location = new System.Drawing.Point(117, 317);
            this.SprayingIDTextEdit.MenuManager = this.barManager1;
            this.SprayingIDTextEdit.Name = "SprayingIDTextEdit";
            this.SprayingIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SprayingIDTextEdit, true);
            this.SprayingIDTextEdit.Size = new System.Drawing.Size(557, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SprayingIDTextEdit, optionsSpelling16);
            this.SprayingIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SprayingIDTextEdit.TabIndex = 42;
            // 
            // LinkedToRecordIDTextEdit
            // 
            this.LinkedToRecordIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "LinkedToRecordID", true));
            this.LinkedToRecordIDTextEdit.Location = new System.Drawing.Point(117, 556);
            this.LinkedToRecordIDTextEdit.MenuManager = this.barManager1;
            this.LinkedToRecordIDTextEdit.Name = "LinkedToRecordIDTextEdit";
            this.LinkedToRecordIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToRecordIDTextEdit, true);
            this.LinkedToRecordIDTextEdit.Size = new System.Drawing.Size(557, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToRecordIDTextEdit, optionsSpelling17);
            this.LinkedToRecordIDTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToRecordIDTextEdit.TabIndex = 44;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 236);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(614, 261);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling18);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 6;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp06230OMJobSprayingEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(117, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(179, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // LinkedToParentDescriptionButtonEdit
            // 
            this.LinkedToParentDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "LinkedToParentDescription", true));
            this.LinkedToParentDescriptionButtonEdit.EditValue = "";
            this.LinkedToParentDescriptionButtonEdit.Location = new System.Drawing.Point(122, 107);
            this.LinkedToParentDescriptionButtonEdit.MenuManager = this.barManager1;
            this.LinkedToParentDescriptionButtonEdit.Name = "LinkedToParentDescriptionButtonEdit";
            this.LinkedToParentDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to open the Select Job screen", "choose", null, true)});
            this.LinkedToParentDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.LinkedToParentDescriptionButtonEdit.Size = new System.Drawing.Size(552, 20);
            this.LinkedToParentDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToParentDescriptionButtonEdit.TabIndex = 0;
            this.LinkedToParentDescriptionButtonEdit.TabStop = false;
            this.LinkedToParentDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LinkedToParentDescriptionButtonEdit_ButtonClick);
            this.LinkedToParentDescriptionButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.LinkedToParentDescriptionButtonEdit_Validating);
            // 
            // ChemicalIDTextEdit
            // 
            this.ChemicalIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06230OMJobSprayingEditBindingSource, "ChemicalID", true));
            this.ChemicalIDTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ChemicalIDTextEdit.Location = new System.Drawing.Point(117, 341);
            this.ChemicalIDTextEdit.MenuManager = this.barManager1;
            this.ChemicalIDTextEdit.Name = "ChemicalIDTextEdit";
            this.ChemicalIDTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.ChemicalIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.ChemicalIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ChemicalIDTextEdit, true);
            this.ChemicalIDTextEdit.Size = new System.Drawing.Size(557, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ChemicalIDTextEdit, optionsSpelling19);
            this.ChemicalIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ChemicalIDTextEdit.TabIndex = 27;
            // 
            // ItemForClientContractID
            // 
            this.ItemForClientContractID.Control = this.ClientContractIDTextEdit;
            this.ItemForClientContractID.CustomizationFormText = "Client Contract ID:";
            this.ItemForClientContractID.Location = new System.Drawing.Point(0, 225);
            this.ItemForClientContractID.Name = "ItemForClientContractID";
            this.ItemForClientContractID.Size = new System.Drawing.Size(649, 24);
            this.ItemForClientContractID.Text = "Client Contract ID:";
            this.ItemForClientContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientIDTextEdit;
            this.ItemForClientID.CustomizationFormText = "Client ID:";
            this.ItemForClientID.Location = new System.Drawing.Point(0, 225);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(649, 24);
            this.ItemForClientID.Text = "Client ID:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.Control = this.ClientNameTextEdit;
            this.ItemForClientName.CustomizationFormText = "Client Name:";
            this.ItemForClientName.Location = new System.Drawing.Point(0, 225);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(649, 24);
            this.ItemForClientName.Text = "Client Name:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForVisitID
            // 
            this.ItemForVisitID.Control = this.VisitIDTextEdit;
            this.ItemForVisitID.CustomizationFormText = "Visit ID:";
            this.ItemForVisitID.Location = new System.Drawing.Point(324, 321);
            this.ItemForVisitID.Name = "ItemForVisitID";
            this.ItemForVisitID.Size = new System.Drawing.Size(325, 24);
            this.ItemForVisitID.Text = "Visit ID:";
            this.ItemForVisitID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteID
            // 
            this.ItemForSiteID.Control = this.SiteIDTextEdit;
            this.ItemForSiteID.CustomizationFormText = "Site ID:";
            this.ItemForSiteID.Location = new System.Drawing.Point(0, 393);
            this.ItemForSiteID.Name = "ItemForSiteID";
            this.ItemForSiteID.Size = new System.Drawing.Size(649, 24);
            this.ItemForSiteID.Text = "Site ID:";
            this.ItemForSiteID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteContractID
            // 
            this.ItemForSiteContractID.Control = this.SiteContractIDTextEdit;
            this.ItemForSiteContractID.CustomizationFormText = "Site Contract ID:";
            this.ItemForSiteContractID.Location = new System.Drawing.Point(0, 369);
            this.ItemForSiteContractID.Name = "ItemForSiteContractID";
            this.ItemForSiteContractID.Size = new System.Drawing.Size(649, 24);
            this.ItemForSiteContractID.Text = "Site Contract ID:";
            this.ItemForSiteContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobTypeID
            // 
            this.ItemForJobTypeID.Control = this.JobTypeIDTextEdit;
            this.ItemForJobTypeID.CustomizationFormText = "Job Type ID:";
            this.ItemForJobTypeID.Location = new System.Drawing.Point(0, 297);
            this.ItemForJobTypeID.Name = "ItemForJobTypeID";
            this.ItemForJobTypeID.Size = new System.Drawing.Size(649, 24);
            this.ItemForJobTypeID.Text = "Job Type ID:";
            this.ItemForJobTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobTypeDescription
            // 
            this.ItemForJobTypeDescription.Control = this.JobTypeDescriptionTextEdit;
            this.ItemForJobTypeDescription.CustomizationFormText = "Job Type:";
            this.ItemForJobTypeDescription.Location = new System.Drawing.Point(0, 273);
            this.ItemForJobTypeDescription.Name = "ItemForJobTypeDescription";
            this.ItemForJobTypeDescription.Size = new System.Drawing.Size(649, 24);
            this.ItemForJobTypeDescription.Text = "Job Type:";
            this.ItemForJobTypeDescription.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobSubTypeID
            // 
            this.ItemForJobSubTypeID.Control = this.JobSubTypeIDTextEdit;
            this.ItemForJobSubTypeID.CustomizationFormText = "Job Sub-Type ID:";
            this.ItemForJobSubTypeID.Location = new System.Drawing.Point(0, 249);
            this.ItemForJobSubTypeID.Name = "ItemForJobSubTypeID";
            this.ItemForJobSubTypeID.Size = new System.Drawing.Size(649, 24);
            this.ItemForJobSubTypeID.Text = "Job Sub-Type ID:";
            this.ItemForJobSubTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobSubTypeDescription
            // 
            this.ItemForJobSubTypeDescription.Control = this.JobSubTypeDescriptionTextEdit;
            this.ItemForJobSubTypeDescription.CustomizationFormText = "Job Sub-Type:";
            this.ItemForJobSubTypeDescription.Location = new System.Drawing.Point(0, 225);
            this.ItemForJobSubTypeDescription.Name = "ItemForJobSubTypeDescription";
            this.ItemForJobSubTypeDescription.Size = new System.Drawing.Size(649, 24);
            this.ItemForJobSubTypeDescription.Text = "Job Sub-Type:";
            this.ItemForJobSubTypeDescription.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForChemicalID
            // 
            this.ItemForChemicalID.Control = this.ChemicalIDTextEdit;
            this.ItemForChemicalID.CustomizationFormText = "Chemical ID:";
            this.ItemForChemicalID.Location = new System.Drawing.Point(0, 24);
            this.ItemForChemicalID.Name = "ItemForChemicalID";
            this.ItemForChemicalID.Size = new System.Drawing.Size(666, 24);
            this.ItemForChemicalID.Text = "Chemical ID:";
            this.ItemForChemicalID.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForSprayingID
            // 
            this.ItemForSprayingID.Control = this.SprayingIDTextEdit;
            this.ItemForSprayingID.CustomizationFormText = "Spraying ID:";
            this.ItemForSprayingID.Location = new System.Drawing.Point(0, 0);
            this.ItemForSprayingID.Name = "ItemForSprayingID";
            this.ItemForSprayingID.Size = new System.Drawing.Size(666, 24);
            this.ItemForSprayingID.Text = "Spraying ID:";
            this.ItemForSprayingID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLinkedToRecordID
            // 
            this.ItemForLinkedToRecordID.Control = this.LinkedToRecordIDTextEdit;
            this.ItemForLinkedToRecordID.CustomizationFormText = "Linked To Record ID:";
            this.ItemForLinkedToRecordID.Location = new System.Drawing.Point(0, 169);
            this.ItemForLinkedToRecordID.Name = "ItemForLinkedToRecordID";
            this.ItemForLinkedToRecordID.Size = new System.Drawing.Size(666, 24);
            this.ItemForLinkedToRecordID.Text = "Linked To Record ID:";
            this.ItemForLinkedToRecordID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLinkedToRecordTypeID
            // 
            this.ItemForLinkedToRecordTypeID.Control = this.LinkedToRecordTypeIDTextEdit;
            this.ItemForLinkedToRecordTypeID.CustomizationFormText = "Linked To Record Type ID:";
            this.ItemForLinkedToRecordTypeID.Location = new System.Drawing.Point(0, 0);
            this.ItemForLinkedToRecordTypeID.Name = "ItemForLinkedToRecordTypeID";
            this.ItemForLinkedToRecordTypeID.Size = new System.Drawing.Size(184, 193);
            this.ItemForLinkedToRecordTypeID.Text = "Linked To Record Type ID:";
            this.ItemForLinkedToRecordTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(686, 588);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLinkedToParentDescription,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.layoutControlGroup6,
            this.ItemForClientNameContractDescription,
            this.ItemForSiteName,
            this.ItemForVisitNumber,
            this.emptySpaceItem6,
            this.emptySpaceItem7,
            this.ItemForJobExpectedStartDate});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(666, 513);
            // 
            // ItemForLinkedToParentDescription
            // 
            this.ItemForLinkedToParentDescription.AllowHide = false;
            this.ItemForLinkedToParentDescription.Control = this.LinkedToParentDescriptionButtonEdit;
            this.ItemForLinkedToParentDescription.CustomizationFormText = "Linked To:";
            this.ItemForLinkedToParentDescription.Location = new System.Drawing.Point(0, 95);
            this.ItemForLinkedToParentDescription.Name = "ItemForLinkedToParentDescription";
            this.ItemForLinkedToParentDescription.Size = new System.Drawing.Size(666, 24);
            this.ItemForLinkedToParentDescription.Text = "Linked To:";
            this.ItemForLinkedToParentDescription.TextSize = new System.Drawing.Size(107, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(105, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(105, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(105, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(288, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(378, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(105, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(183, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 154);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(666, 359);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layGrpAddress;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(642, 313);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layGrpAddress,
            this.layoutControlGroup4});
            // 
            // layGrpAddress
            // 
            this.layGrpAddress.CustomizationFormText = "Address";
            this.layGrpAddress.ExpandButtonVisible = true;
            this.layGrpAddress.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layGrpAddress.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAmountUsed,
            this.ItemForChemicalName,
            this.ItemForAmountUsedDescriptorID,
            this.ItemForDilutionRatio,
            this.ItemForAmountUsedDiluted,
            this.emptySpaceItem8,
            this.ItemForWarningLabel,
            this.emptySpaceItem5,
            this.emptySpaceItem4});
            this.layGrpAddress.Location = new System.Drawing.Point(0, 0);
            this.layGrpAddress.Name = "layGrpAddress";
            this.layGrpAddress.Size = new System.Drawing.Size(618, 265);
            this.layGrpAddress.Text = "Details";
            // 
            // ItemForAmountUsed
            // 
            this.ItemForAmountUsed.Control = this.AmountUsedSpinEdit;
            this.ItemForAmountUsed.CustomizationFormText = "Amount Used:";
            this.ItemForAmountUsed.Location = new System.Drawing.Point(0, 24);
            this.ItemForAmountUsed.Name = "ItemForAmountUsed";
            this.ItemForAmountUsed.Size = new System.Drawing.Size(313, 24);
            this.ItemForAmountUsed.Text = "Amount Used:";
            this.ItemForAmountUsed.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForChemicalName
            // 
            this.ItemForChemicalName.Control = this.ChemicalNameButtonEdit;
            this.ItemForChemicalName.CustomizationFormText = "Chemical Name:";
            this.ItemForChemicalName.Location = new System.Drawing.Point(0, 0);
            this.ItemForChemicalName.Name = "ItemForChemicalName";
            this.ItemForChemicalName.Size = new System.Drawing.Size(618, 24);
            this.ItemForChemicalName.Text = "Chemical Name:";
            this.ItemForChemicalName.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForAmountUsedDescriptorID
            // 
            this.ItemForAmountUsedDescriptorID.Control = this.AmountUsedDescriptorIDGridLookUpEdit;
            this.ItemForAmountUsedDescriptorID.CustomizationFormText = "Amount Used Unit Descriptor:";
            this.ItemForAmountUsedDescriptorID.Location = new System.Drawing.Point(313, 24);
            this.ItemForAmountUsedDescriptorID.Name = "ItemForAmountUsedDescriptorID";
            this.ItemForAmountUsedDescriptorID.Size = new System.Drawing.Size(305, 24);
            this.ItemForAmountUsedDescriptorID.Text = "Unit Descriptor:";
            this.ItemForAmountUsedDescriptorID.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForDilutionRatio
            // 
            this.ItemForDilutionRatio.Control = this.DilutionRatioSpinEdit;
            this.ItemForDilutionRatio.CustomizationFormText = "Dilution Ratio:";
            this.ItemForDilutionRatio.Location = new System.Drawing.Point(0, 48);
            this.ItemForDilutionRatio.Name = "ItemForDilutionRatio";
            this.ItemForDilutionRatio.Size = new System.Drawing.Size(313, 24);
            this.ItemForDilutionRatio.Text = "Dilution Ratio:";
            this.ItemForDilutionRatio.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForAmountUsedDiluted
            // 
            this.ItemForAmountUsedDiluted.Control = this.AmountUsedDilutedSpinEdit;
            this.ItemForAmountUsedDiluted.CustomizationFormText = "Amount Used Diluted :";
            this.ItemForAmountUsedDiluted.Location = new System.Drawing.Point(0, 72);
            this.ItemForAmountUsedDiluted.Name = "ItemForAmountUsedDiluted";
            this.ItemForAmountUsedDiluted.Size = new System.Drawing.Size(313, 24);
            this.ItemForAmountUsedDiluted.Text = "Amount Used Diluted :";
            this.ItemForAmountUsedDiluted.TextSize = new System.Drawing.Size(107, 13);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 176);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(618, 89);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForWarningLabel
            // 
            this.ItemForWarningLabel.Control = this.MinMaxErrorLabel;
            this.ItemForWarningLabel.CustomizationFormText = "Warning:";
            this.ItemForWarningLabel.Location = new System.Drawing.Point(313, 48);
            this.ItemForWarningLabel.MinSize = new System.Drawing.Size(272, 17);
            this.ItemForWarningLabel.Name = "ItemForWarningLabel";
            this.ItemForWarningLabel.Size = new System.Drawing.Size(305, 24);
            this.ItemForWarningLabel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForWarningLabel.Text = "Warning:";
            this.ItemForWarningLabel.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForWarningLabel.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(313, 72);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(305, 24);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 96);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 80);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 80);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(618, 80);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImage")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(618, 265);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Comments:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(618, 265);
            this.ItemForRemarks.Text = "Comments:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // ItemForClientNameContractDescription
            // 
            this.ItemForClientNameContractDescription.Control = this.ClientNameContractDescriptionTextEdit;
            this.ItemForClientNameContractDescription.CustomizationFormText = "Client \\ Contract:";
            this.ItemForClientNameContractDescription.Location = new System.Drawing.Point(0, 23);
            this.ItemForClientNameContractDescription.Name = "ItemForClientNameContractDescription";
            this.ItemForClientNameContractDescription.Size = new System.Drawing.Size(666, 24);
            this.ItemForClientNameContractDescription.Text = "Client \\ Contract:";
            this.ItemForClientNameContractDescription.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForSiteName
            // 
            this.ItemForSiteName.Control = this.SiteNameTextEdit;
            this.ItemForSiteName.CustomizationFormText = "Site Name:";
            this.ItemForSiteName.Location = new System.Drawing.Point(0, 47);
            this.ItemForSiteName.Name = "ItemForSiteName";
            this.ItemForSiteName.Size = new System.Drawing.Size(666, 24);
            this.ItemForSiteName.Text = "Site Name:";
            this.ItemForSiteName.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForVisitNumber
            // 
            this.ItemForVisitNumber.Control = this.VisitNumberTextEdit;
            this.ItemForVisitNumber.CustomizationFormText = "Visit Number:";
            this.ItemForVisitNumber.Location = new System.Drawing.Point(0, 71);
            this.ItemForVisitNumber.Name = "ItemForVisitNumber";
            this.ItemForVisitNumber.Size = new System.Drawing.Size(666, 24);
            this.ItemForVisitNumber.Text = "Visit Number:";
            this.ItemForVisitNumber.TextSize = new System.Drawing.Size(107, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 143);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 11);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 11);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(666, 11);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(300, 119);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(366, 24);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForJobExpectedStartDate
            // 
            this.ItemForJobExpectedStartDate.Control = this.JobExpectedStartDateTextEdit;
            this.ItemForJobExpectedStartDate.CustomizationFormText = "Job Expected Start Date:";
            this.ItemForJobExpectedStartDate.Location = new System.Drawing.Point(0, 119);
            this.ItemForJobExpectedStartDate.MaxSize = new System.Drawing.Size(300, 24);
            this.ItemForJobExpectedStartDate.MinSize = new System.Drawing.Size(300, 24);
            this.ItemForJobExpectedStartDate.Name = "ItemForJobExpectedStartDate";
            this.ItemForJobExpectedStartDate.Size = new System.Drawing.Size(300, 24);
            this.ItemForJobExpectedStartDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForJobExpectedStartDate.Text = "Expected Start Date:";
            this.ItemForJobExpectedStartDate.TextSize = new System.Drawing.Size(107, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 513);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(666, 55);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(666, 55);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06108_OM_Unit_Descriptors_PicklistTableAdapter
            // 
            this.sp06108_OM_Unit_Descriptors_PicklistTableAdapter.ClearBeforeFill = true;
            // 
            // sp06230_OM_Job_Spraying_EditTableAdapter
            // 
            this.sp06230_OM_Job_Spraying_EditTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Job_Spraying_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(686, 644);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Job_Spraying_Edit";
            this.Text = "Edit Job / Visit Spraying";
            this.Activated += new System.EventHandler(this.frm_OM_Job_Spraying_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Job_Spraying_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Job_Spraying_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AmountUsedDilutedSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06230OMJobSprayingEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DilutionRatioSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToRecordTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobExpectedStartDateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameContractDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmountUsedDescriptorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06108OMUnitDescriptorsPicklistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmountUsedSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChemicalNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SprayingIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToRecordIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToParentDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChemicalIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChemicalID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSprayingID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToRecordID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToRecordTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToParentDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmountUsed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChemicalName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmountUsedDescriptorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDilutionRatio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmountUsedDiluted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarningLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientNameContractDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobExpectedStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToParentDescription;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.ButtonEdit LinkedToParentDescriptionButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChemicalID;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit ChemicalIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpAddress;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DevExpress.XtraEditors.TextEdit LinkedToRecordIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToRecordID;
        private DevExpress.XtraEditors.TextEdit SprayingIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSprayingID;
        private DevExpress.XtraEditors.ButtonEdit ChemicalNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChemicalName;
        private DataSet_OM_Job dataSet_OM_Job;
        private DevExpress.XtraEditors.SpinEdit AmountUsedSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAmountUsed;
        private DevExpress.XtraEditors.GridLookUpEdit AmountUsedDescriptorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAmountUsedDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.TextEdit SiteIDTextEdit;
        private DevExpress.XtraEditors.TextEdit ClientIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteID;
        private DevExpress.XtraEditors.TextEdit VisitIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitID;
        private DevExpress.XtraEditors.TextEdit ClientNameContractDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientNameContractDescription;
        private DevExpress.XtraEditors.TextEdit SiteNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteName;
        private DevExpress.XtraEditors.TextEdit VisitNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitNumber;
        private DevExpress.XtraEditors.TextEdit ClientNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraEditors.TextEdit JobTypeDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobTypeDescription;
        private DevExpress.XtraEditors.TextEdit JobSubTypeDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobSubTypeDescription;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.TextEdit JobTypeIDTextEdit;
        private DevExpress.XtraEditors.TextEdit JobSubTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobSubTypeID;
        private DevExpress.XtraEditors.TextEdit ClientContractIDTextEdit;
        private DevExpress.XtraEditors.TextEdit SiteContractIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientContractID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteContractID;
        private DevExpress.XtraBars.BarButtonItem bbiLinkedDocuments;
        private System.Windows.Forms.BindingSource sp06108OMUnitDescriptorsPicklistBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06108_OM_Unit_Descriptors_PicklistTableAdapter sp06108_OM_Unit_Descriptors_PicklistTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.TextEdit JobExpectedStartDateTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobExpectedStartDate;
        private System.Windows.Forms.BindingSource sp06230OMJobSprayingEditBindingSource;
        private DataSet_OM_JobTableAdapters.sp06230_OM_Job_Spraying_EditTableAdapter sp06230_OM_Job_Spraying_EditTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.TextEdit LinkedToRecordTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToRecordTypeID;
        private DevExpress.XtraEditors.SpinEdit DilutionRatioSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDilutionRatio;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.SpinEdit AmountUsedDilutedSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAmountUsedDiluted;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraEditors.LabelControl MinMaxErrorLabel;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWarningLabel;
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;  // Required for Path Statement //
using System.Xml;
using System.Linq;
using System.Xml.Linq;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;
using DevExpress.XtraTab;
using DevExpress.XtraBars.Docking;

using DevExpress.Utils.Menu;  // Required to disable Column Chooser on Grids //
using DevExpress.XtraGrid.Localization;  // Required to disable Column Chooser on Grids //
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using LocusEffects;
using WoodPlan5.Classes.Operations;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_OM_Job_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        private Utils.enmFocusedGrid _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;

        bool iBool_EnableGridColumnChooser = false;
        bool iBool_AddWizardButtonEnabled = false;    
        bool iBool_SendJobsButtonEnabled = false;
        bool iBool_ReassignJobsButtonEnabled = false;
        bool iBool_ManuallyCompleteJobsEnabled = false;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewStateJob;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateLabour;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateEquipment;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateMaterial;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStatePicture;
        public RefreshGridState RefreshGridViewStateHealthAndSafety;
        public RefreshGridState RefreshGridViewStateCRM;
        public RefreshGridState RefreshGridViewStateComment;
        public RefreshGridState RefreshGridViewStateWaste;
        public RefreshGridState RefreshGridViewStateSpraying;
        public RefreshGridState RefreshGridViewStateExtraInfo;
        public RefreshGridState RefreshGridViewStateLabourTiming;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //
        RepositoryItemCheckEdit tempEditorCheckEdit;
        RepositoryItemMemoExEdit tempEditorMemoExEdit;
        RepositoryItemTextEdit tempEditorTextEditText;
        RepositoryItemTextEdit tempEditorTextEditInteger;
        RepositoryItemTextEdit tempEditorTextEditDecimal;
        RepositoryItemTextEdit tempEditorTextEditCurrency;
        RepositoryItemTextEdit tempEditorTextEditDateTime;

        string i_str_AddedRecordIDsJob = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDsLabour = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDsEquipment = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDsMaterial = "";
        string i_str_AddedRecordIDsPicture = "";
        string i_str_AddedRecordIDsHealthAndSafety = "";
        string i_str_AddedRecordIDsCRM = "";
        string i_str_AddedRecordIDsComment = "";
        string i_str_AddedRecordIDsWaste = "";
        string i_str_AddedRecordIDsSpraying = "";
        string i_str_AddedRecordIDsExtraInfo = "";
        string i_str_AddedRecordIDsLabourTiming = "";

        string i_str_selected_JobStatus_ids = "";
        string i_str_selected_JobStatus_names = "";
        BaseObjects.GridCheckMarksSelection selection1;

        BaseObjects.GridCheckMarksSelection selection3;

        string i_str_selected_KAM_ids = "";
        string i_str_selected_KAM_names = "";
        BaseObjects.GridCheckMarksSelection selection5;

        string i_str_selected_CM_ids = "";
        string i_str_selected_CM_names = "";
        BaseObjects.GridCheckMarksSelection selection9;

        string i_str_selected_RecordType_ids = "";
        string i_str_selected_RecordType_descriptions = "";
        BaseObjects.GridCheckMarksSelection selection8;

        string i_str_selected_JobType_ids = "";
        string i_str_selected_JobType_descriptions = "";
        BaseObjects.GridCheckMarksSelection selection10;

        string i_str_selected_JobSubType_ids = "";
        string i_str_selected_JobSubType_descriptions = "";
        BaseObjects.GridCheckMarksSelection selection11;

        string i_str_selected_Labour_Team_ids = "";
        string i_str_selected_Labour_Team_descriptions = "";
        BaseObjects.GridCheckMarksSelection selection12;

        string i_str_selected_EquipmentType_ids = "";
        string i_str_selected_EquipmentType_descriptions = "";
        BaseObjects.GridCheckMarksSelection selection13;

        string i_str_selected_MaterialType_ids = "";
        string i_str_selected_MaterialType_descriptions = "";
        BaseObjects.GridCheckMarksSelection selection14;

        string i_str_selected_ConstructionManager_ids = "";
        string i_str_selected_ConstructionManager_names = "";
        BaseObjects.GridCheckMarksSelection selection15;

        string i_str_selected_VisitType_ids = "";
        string i_str_selected_VisitTypes = "";
        BaseObjects.GridCheckMarksSelection selection2;

        string i_str_selected_client_ids = "";
        string i_str_selected_client_names = "";
        string i_str_selected_site_ids = "";
        string i_str_selected_site_names = "";

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strWasteDocumentPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        public string strPassedInDrillDownIDs = "";
        string istr_FurtherAction = "";  // Set by Gritting Callout Wizard only //

        Bitmap bmpBlank = null;
        Bitmap bmpAlert = null;
        
        public string i_str_SiteIDsToLink = "";  // Populated via Snow Clearance Manager when it needs to create Gritting Callout for linking to the Snow Callouts //

        private DateTime i_dtStart = DateTime.MinValue;
        private DateTime i_dtEnd = DateTime.MaxValue;
        private string i_str_selected_TabPages = "";

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        private DataSet_Selection DS_Selection1;

        public bool _boolSelectAllJobsOnOpen = false;

        SuperToolTip superToolTipSiteContractFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsSiteContractFilter = null;

        SuperToolTip superToolTipClientContractFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsClientContractFilter = null;

        #endregion

        public frm_OM_Job_Manager()
        {
            InitializeComponent();
        }

        private void frm_OM_Job_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 7004;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();

            Set_Grid_Highlighter_Transparent(this.Controls);

            bmpBlank = new Bitmap(16, 16);
            bmpAlert = new Bitmap(imageCollection1.Images[10], 16, 16);

            DS_Selection1 = new Utilities.DataSet_Selection((GridView)gridViewJob, strConnectionString);  // Used by DataSets //

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_PictureFilesFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Job Pictures (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Linked Job Pictures Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strWasteDocumentPath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_WasteDocumentFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Waste Documents (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Waste Document Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
           
            sp06031_OM_Job_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateJob = new RefreshGridState(gridViewJob, "JobID");

            // Add record selection checkboxes to popup Callout Type grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            sp06025_OM_Job_Statuses_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06025_OM_Job_Statuses_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06025_OM_Job_Statuses_Filter);
            gridControl3.ForceInitialize();

            sp06018_OM_Job_Manager_Record_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06018_OM_Job_Manager_Record_TypesTableAdapter.Fill(dataSet_GC_Summer_Core.sp06018_OM_Job_Manager_Record_Types);
            gridControl8.ForceInitialize();
            // Add record selection checkboxes to popup Callout Type grid control //
            selection8 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl8.MainView);
            selection8.CheckMarkColumn.VisibleIndex = 0;
            selection8.CheckMarkColumn.Width = 30;

            sp06026_OM_Job_Type_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06026_OM_Job_Type_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06026_OM_Job_Type_Filter);
            gridControl10.ForceInitialize();
            // Add record selection checkboxes to popup Callout Type grid control //
            selection10 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl10.MainView);
            selection10.CheckMarkColumn.VisibleIndex = 0;
            selection10.CheckMarkColumn.Width = 30;

            sp06027_OM_Job_Sub_Type_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06027_OM_Job_Sub_Type_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06027_OM_Job_Sub_Type_Filter);
            gridControl11.ForceInitialize();
            // Add record selection checkboxes to popup Callout Type grid control //
            selection11 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl11.MainView);
            selection11.CheckMarkColumn.VisibleIndex = 0;
            selection11.CheckMarkColumn.Width = 30;

            sp06028_OM_Labour_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06028_OM_Labour_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06028_OM_Labour_Filter, (checkEditTeam.Checked ? 1 : 0));
            gridControl12.ForceInitialize();
            // Add record selection checkboxes to popup Callout Type grid control //
            selection12 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl12.MainView);
            selection12.CheckMarkColumn.VisibleIndex = 0;
            selection12.CheckMarkColumn.Width = 30;

            sp06029_OM_Equipment_Type_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06029_OM_Equipment_Type_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06029_OM_Equipment_Type_Filter);
            gridControl13.ForceInitialize();
            // Add record selection checkboxes to popup Callout Type grid control //
            selection13 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl13.MainView);
            selection13.CheckMarkColumn.VisibleIndex = 0;
            selection13.CheckMarkColumn.Width = 30;

            sp06030_OM_Material_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06030_OM_Material_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06030_OM_Material_Filter);
            gridControl14.ForceInitialize();
            // Add record selection checkboxes to popup Callout Type grid control //
            selection14 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl14.MainView);
            selection14.CheckMarkColumn.VisibleIndex = 0;
            selection14.CheckMarkColumn.Width = 30;

            //i_dtStart = DateTime.Today.AddMonths(-6);
            //i_dtEnd = DateTime.Today.AddMonths(6);
            i_dtStart = new DateTime(DateTime.Today.Year, 1, 1);
            i_dtEnd = new DateTime(DateTime.Today.AddYears(1).Year, 1, 1);
            dateEditFromDate.DateTime = i_dtStart;
            dateEditToDate.DateTime = i_dtEnd;
            popupContainerEditDateRange.Text = PopupContainerEditDateRange_Get_Selected();
            
            sp06033_OM_Job_Manager_Linked_labourTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateLabour = new RefreshGridState(gridViewLabour, "LabourUsedID");

            sp06034_OM_Job_Manager_Linked_EquipmentTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateEquipment = new RefreshGridState(gridViewEquipment, "EquipmentUsedID");

            sp06035_OM_Job_Manager_Linked_MaterialsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateMaterial = new RefreshGridState(gridViewMaterial, "MaterialUsedID");

            sp06037_OM_Job_Manager_Linked_PicturesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStatePicture = new RefreshGridState(gridViewPicture, "PictureID");

            sp06039_OM_Job_Manager_Linked_Safety_RequirementTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateHealthAndSafety = new RefreshGridState(gridViewHealthAndSafety, "SafetyUsedID");

            sp05089_CRM_Contacts_Linked_To_RecordTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateCRM = new RefreshGridState(gridViewCRM, "CRMID");

            sp06040_OM_Job_Manager_Linked_CommentsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateComment = new RefreshGridState(gridViewComment, "CommentID");

            sp06036_OM_Job_Manager_Linked_WasteTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateWaste = new RefreshGridState(gridViewWaste, "WasteID");

            sp06038_OM_Job_Manager_Linked_SprayingTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateSpraying = new RefreshGridState(gridViewSpraying, "SprayingID");

            sp06041_OM_Job_Manager_Linked_Extra_InfoTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateExtraInfo = new RefreshGridState(gridViewExtraInfo, "JobAttributeID");

            sp06279_OM_Labour_Time_On_Off_SiteTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateLabourTiming = new RefreshGridState(gridViewLabourTiming, "LabourUsedTimingID");

            // Add record selection checkboxes to popup KAM Filter grid control //
            selection5 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection5.CheckMarkColumn.VisibleIndex = 0;
            selection5.CheckMarkColumn.Width = 30;
            sp06019_OM_Job_Manager_KAMs_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06019_OM_Job_Manager_KAMs_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06019_OM_Job_Manager_KAMs_Filter);
            gridControl5.ForceInitialize();

            // Add record selection checkboxes to popup CM Filter grid control //
            selection9 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl9.MainView);
            selection9.CheckMarkColumn.VisibleIndex = 0;
            selection9.CheckMarkColumn.Width = 30;
            sp06020_OM_Job_Manager_CMs_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06020_OM_Job_Manager_CMs_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06020_OM_Job_Manager_CMs_Filter);
            gridControl9.ForceInitialize();

            // Add record selection checkboxes to popup Construction Manager Filter grid control //
            selection15 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection15.CheckMarkColumn.VisibleIndex = 0;
            selection15.CheckMarkColumn.Width = 30;
            sp06294_OM_Job_Manager_Construction_Manager_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06294_OM_Job_Manager_Construction_Manager_FilterTableAdapter.Fill(dataSet_OM_Job.sp06294_OM_Job_Manager_Construction_Manager_Filter);
            gridControl1.ForceInitialize();

            sp06308_OM_Visit_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06308_OM_Visit_TypesTableAdapter.Fill(dataSet_OM_Visit.sp06308_OM_Visit_Types, 0);
            gridControl2.ForceInitialize();
            // Add record selection checkboxes to popup Callout Type grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;

            #region populate_Available_TabPages_List

            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl7.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;

            gridControl7.BeginUpdate();
            this.dataSet_GC_Core.sp04022_Core_Dummy_TabPageList.Rows.Clear();
            foreach (XtraTabPage i in xtraTabControl1.TabPages)
            {
                DataRow drNewRow;
                drNewRow = this.dataSet_GC_Core.sp04022_Core_Dummy_TabPageList.NewRow();
                drNewRow["TabPageName"] = i.Text;
                this.dataSet_GC_Core.sp04022_Core_Dummy_TabPageList.Rows.Add(drNewRow);
            }
            gridControl7.EndUpdate();
            gridControl7.ForceInitialize();

            #endregion

            #region Create Extra Attribute Editors

            tempEditorCheckEdit = new RepositoryItemCheckEdit();
            tempEditorCheckEdit.AutoHeight = false;
            tempEditorCheckEdit.Caption = "Check";
            tempEditorCheckEdit.ValueChecked = "1";
            tempEditorCheckEdit.ValueUnchecked = "0";

            tempEditorMemoExEdit = new RepositoryItemMemoExEdit();
            tempEditorMemoExEdit.AutoHeight = false;
            tempEditorMemoExEdit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            tempEditorMemoExEdit.ShowIcon = false;

            tempEditorTextEditText = new RepositoryItemTextEdit();
            tempEditorTextEditText.AutoHeight = false;

            tempEditorTextEditInteger = new RepositoryItemTextEdit();
            tempEditorTextEditInteger.AutoHeight = false;
            tempEditorTextEditInteger.Mask.EditMask = "f0";
            tempEditorTextEditInteger.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            tempEditorTextEditInteger.Mask.UseMaskAsDisplayFormat = true;

            tempEditorTextEditDecimal = new RepositoryItemTextEdit();
            tempEditorTextEditDecimal.AutoHeight = false;
            tempEditorTextEditDecimal.Mask.EditMask = "f2";
            tempEditorTextEditDecimal.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            tempEditorTextEditDecimal.Mask.UseMaskAsDisplayFormat = true;

            tempEditorTextEditCurrency = new RepositoryItemTextEdit();
            tempEditorTextEditCurrency.AutoHeight = false;
            tempEditorTextEditCurrency.Mask.EditMask = "c";
            tempEditorTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            tempEditorTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;

            tempEditorTextEditDateTime = new RepositoryItemTextEdit();
            tempEditorTextEditDateTime.AutoHeight = false;
            tempEditorTextEditDateTime.Mask.EditMask = "g";
            tempEditorTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            tempEditorTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;

            #endregion

            if (strPassedInDrillDownIDs != "")  // Opened in drill-down mode //
            {
                popupContainerEditKAMFilter.Text = "Custom Filter";
                popupContainerEditCMFilter.Text = "Custom Filter";
                popupContainerControlConstructionManagerFilter.Text = "Custom Filter";
                popupContainerEditJobStatusFilter.Text = "Custom Filter";
                popupContainerEditRecordTypeFilter.Text = "Custom Filter";
                popupContainerEditDateRange.Text = "Custom Filter";
                popupContainerEditJobTypeFilter.Text = "Custom Filter";
                buttonEditClientFilter.Text = "Custom Filter";
                buttonEditSiteFilter.Text = "Custom Filter";
                popupContainerEditJobSubTypeFilter.Text = "Custom Filter";
                popupContainerEditLabourFilter.Text = "Custom Filter";
                popupContainerEditMaterialTypeFilter.Text = "Custom Filter";
                popupContainerEditEquipmentTypeFilter.Text = "Custom Filter";
                Load_Data();  // Load records //
                if (_boolSelectAllJobsOnOpen)
                {
                    GridView view = (GridView)gridControlJob.MainView;
                    view.SelectAll();
                    try
                    {
                        xtraTabControl1.SelectedTabPage = xtraTabPageLabour;
                    }
                    catch (Exception) { }
                }
            }
            popupContainerControlKAMFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlCMFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlConstructionManagerFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlDateRange.Size = new System.Drawing.Size(200, 107);
            popupContainerControlShowTabPages.Size = new System.Drawing.Size(190, 230);
            popupContainerControlRecordTypeFilter.Size = new System.Drawing.Size(175, 125);
            popupContainerControlJobStatusFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlJobTypeFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlJobSubTypeFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlLabourFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlEquipmentTypeFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlMaterialTypeFilter.Size = new System.Drawing.Size(270, 400);

            // Create a SuperToolTip //
            superToolTipSiteContractFilter = new SuperToolTip();
            superToolTipSetupArgsSiteContractFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsSiteContractFilter.Title.Text = "Site Contract Filter - Information";
            superToolTipSetupArgsSiteContractFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsSiteContractFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsSiteContractFilter.Footer.Text = "";
            superToolTipSiteContractFilter.Setup(superToolTipSetupArgsSiteContractFilter);
            superToolTipSiteContractFilter.AllowHtmlText = DefaultBoolean.True;
            buttonEditSiteFilter.SuperTip = superToolTipSiteContractFilter;

            // Create a SuperToolTip //
            superToolTipClientContractFilter = new SuperToolTip();
            superToolTipSetupArgsClientContractFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsClientContractFilter.Title.Text = "Client Contract Filter - Information";
            superToolTipSetupArgsClientContractFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsClientContractFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsClientContractFilter.Footer.Text = "";
            superToolTipClientContractFilter.Setup(superToolTipSetupArgsClientContractFilter);
            superToolTipClientContractFilter.AllowHtmlText = DefaultBoolean.True;
            buttonEditClientFilter.SuperTip = superToolTipClientContractFilter;

            emptyEditor = new RepositoryItem();

            StyleFormatCondition condition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            condition1.Appearance.BackColor = Color.FromArgb(192, 255, 192);
            //condition1.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
            condition1.Appearance.Options.UseBackColor = true;
            condition1.Appearance.ForeColor = Color.Black;
            condition1.ApplyToRow = true;
            condition1.Condition = FormatConditionEnum.Expression;
            condition1.Expression = "[LinkedLabourCount] > 0 and [JobStatusID] >= 20 and [DoNotInvoiceClient] == 0 and [DoNotPayContractor] == 0";
            gridControlJob.MainView.FormatConditions.Add(condition1);

            StyleFormatCondition condition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            condition3.Appearance.Options.UseForeColor = true;
            condition3.Appearance.ForeColor = Color.Red;
            condition3.ApplyToRow = false;
            condition3.Column = colJobStatusDescription;
            condition3.Condition = FormatConditionEnum.Equal;
            condition3.Value1 = "On-Hold";
            gridControlJob.MainView.FormatConditions.Add(condition3);
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
            
            Application.DoEvents();  // Allow Form time to repaint itself //
            splitContainerControl2.SplitterPosition = (splitContainerControl2.Width / 2) - 5;
            
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                LoadLastSavedUserScreenSettings();
            }
            gridControlJob.Focus();  // Need this to make tooltip on column start working as soon as form opens otherwise it doesn't start until user clicks into a grid //
        }

        private void frm_OM_Job_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDsJob))
                {
                    Load_Data();
                }
                if (UpdateRefreshStatus == 2 || !string.IsNullOrEmpty(i_str_AddedRecordIDsJob) || !string.IsNullOrEmpty(i_str_AddedRecordIDsLabour) || !string.IsNullOrEmpty(i_str_AddedRecordIDsEquipment) || !string.IsNullOrEmpty(i_str_AddedRecordIDsMaterial) || !string.IsNullOrEmpty(i_str_AddedRecordIDsPicture) || !string.IsNullOrEmpty(i_str_AddedRecordIDsHealthAndSafety) || !string.IsNullOrEmpty(i_str_AddedRecordIDsCRM) || !string.IsNullOrEmpty(i_str_AddedRecordIDsComment) || !string.IsNullOrEmpty(i_str_AddedRecordIDsWaste) || !string.IsNullOrEmpty(i_str_AddedRecordIDsSpraying) || !string.IsNullOrEmpty(i_str_AddedRecordIDsExtraInfo))
                {
                    LoadLinkedRecords();
                }
                if (UpdateRefreshStatus == 3 || !string.IsNullOrEmpty(i_str_AddedRecordIDsLabourTiming))
                {
                    LoadLinkedRecords_LabourTimings();
                }
            }
            SetMenuStatus();
        }

        private void frm_OM_Job_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ColourCode", (checkEditColourCode.Checked ? "1" : "0"));
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ShownTabPages", i_str_selected_TabPages);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "JobStatusFilter", i_str_selected_JobStatus_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "KAMFilter", i_str_selected_KAM_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CMFilter", i_str_selected_CM_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ConstructionManagerFilter", i_str_selected_ConstructionManager_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ClientFilter", i_str_selected_client_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SiteFilter", i_str_selected_site_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "JobTypeFilter", i_str_selected_JobType_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "JobSubTypeFilter", i_str_selected_JobSubType_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "RecordTypeFilter", i_str_selected_RecordType_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "LabourFilter", i_str_selected_Labour_Team_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "EquipmentTypeFilter", i_str_selected_EquipmentType_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "MaterialTypeFilter", i_str_selected_MaterialType_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "VisitTypeFilter", i_str_selected_VisitType_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "LabourTypeFilter", (checkEditTeam.Checked ? "Team" : "Staff"));
                   
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }


        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Colour Code //
                string strColourCode = default_screen_settings.RetrieveSetting("ColourCode");
           //     if (!string.IsNullOrEmpty(strColourCode)) checkEditColourCode.Checked = (strColourCode == "0" ? false : true);

                // Shown Tab Pages //
                int intFoundRow = 0;
                string strItemFilter = default_screen_settings.RetrieveSetting("ShownTabPages");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl7.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["TabPageName"], strElement.Trim());
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditShowTabPages.Text = PopupContainerEditShowTabPages_Get_Selected();
                }

                // Job Status Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("JobStatusFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl3.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditJobStatusFilter.Text = PopupContainerEditJobStatusFilter_Get_Selected();
                }


                // KAM Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("KAMFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl5.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditKAMFilter.Text = PopupContainerEditKAMFilter_Get_Selected();
                }

                // CM Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("CMFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl9.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditCMFilter.Text = PopupContainerEditCMFilter_Get_Selected();
                }

                // Construction Manager Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("ConstructionManagerFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl1.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditConstructionManagerFilter.Text = PopupContainerEditConstructionManagerFilter_Get_Selected();
                }

                // Record Type Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("RecordTypeFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl8.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditRecordTypeFilter.Text = PopupContainerEditRecordTypeFilter_Get_Selected();
                }

                // Client Filter //
                strItemFilter = default_screen_settings.RetrieveSetting("ClientFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    i_str_selected_client_ids = strItemFilter;
                    var ResolveIDs = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
                    ResolveIDs.ChangeConnectionString(strConnectionString);
                    try
                    {
                        buttonEditClientFilter.Text = ResolveIDs.sp06023_OM_Get_Clients_From_ClientIDs(strItemFilter).ToString();
                        i_str_selected_client_names = buttonEditClientFilter.Text;

                        // Update Filter control's tooltip //
                        string strTooltipText = i_str_selected_client_names.Replace(", ", "\n");
                        superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;
                    }
                    catch (Exception) { }
                }

                // Site Filter //
                strItemFilter = default_screen_settings.RetrieveSetting("SiteFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    i_str_selected_site_ids = strItemFilter;
                    var ResolveIDs = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
                    ResolveIDs.ChangeConnectionString(strConnectionString);
                    try
                    {
                        buttonEditSiteFilter.Text = ResolveIDs.sp06024_OM_Get_Sites_From_SiteIDs(strItemFilter).ToString();
                        i_str_selected_site_names = buttonEditSiteFilter.Text;

                        // Update Filter control's tooltip //
                        string strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                    }
                    catch (Exception) { }
                }

                // Job Type Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("JobTypeFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl10.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditJobTypeFilter.Text = PopupContainerEditJobTypeFilter_Get_Selected();
                }

                // Job Sub-Type Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("JobSubTypeFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl11.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditJobSubTypeFilter.Text = PopupContainerEditJobSubTypeFilter_Get_Selected();
                }

                // Labour Type Check Edit //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("LabourTypeFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    if (strItemFilter == "Team")
                    {
                        checkEditTeam.Checked = true;
                    }
                    else  // Staff //
                    {
                        checkEditStaff.Checked = true;
                    }
                }

                // Labour Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("LabourFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl12.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditLabourFilter.Text = PopupContainerEditLabourFilter_Get_Selected();
                }

                // Equipment Type Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("EquipmentTypeFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl13.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditEquipmentTypeFilter.Text = PopupContainerEditEquipmentTypeFilter_Get_Selected();
                }

                // Material Type Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("MaterialTypeFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl14.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditMaterialTypeFilter.Text = PopupContainerEditMaterialTypeFilter_Get_Selected();
                }

                // Visit Type Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("VisitTypeFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl2.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditVisitTypeFilter.Text = PopupContainerEditVisitTypeFilter_Get_Selected();
                }

                Load_Data();

                // Prepare LocusEffects and add custom effect //
                locusEffectsProvider1 = new LocusEffectsProvider();
                locusEffectsProvider1.Initialize();
                locusEffectsProvider1.FramesPerSecond = 30;
                m_customArrowLocusEffect1 = new ArrowLocusEffect();
                m_customArrowLocusEffect1.Name = "CustomeArrow1";
                m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                m_customArrowLocusEffect1.MovementCycles = 20;
                m_customArrowLocusEffect1.MovementAmplitude = 200;
                m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                m_customArrowLocusEffect1.LeadInTime = 0; //msec
                m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                Point location = dockPanelFilters.PointToScreen(Point.Empty);

                System.Drawing.Point screenPoint = new System.Drawing.Point(location.X + 10, location.Y + 5);
                locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            Load_Data();
        }

        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Jobs...");
            }

            if (i_str_selected_JobStatus_ids == null) i_str_selected_JobStatus_ids = "";
            GridView view = (GridView)gridControlJob.MainView;
            view.BeginUpdate();
            try
            {
                RefreshGridViewStateJob.SaveViewInfo();  // Store expanded groups and selected rows //
                if (popupContainerEditJobStatusFilter.Text == "Custom Filter" && strPassedInDrillDownIDs != "")  // Load passed in Callouts //
                {
                    sp06031_OM_Job_ManagerTableAdapter.Fill(dataSet_OM_Job.sp06031_OM_Job_Manager, strPassedInDrillDownIDs, i_dtStart, i_dtEnd, "", "", "", "", "", "", "", "", "", "", "", "", "", 0);
                    RefreshGridViewStateJob.LoadViewInfo();  // Reload any expanded groups and selected rows //
                    view.ExpandAllGroups();
                }
                else // Load users selection //
                {
                    sp06031_OM_Job_ManagerTableAdapter.Fill(dataSet_OM_Job.sp06031_OM_Job_Manager, "", i_dtStart, i_dtEnd, i_str_selected_KAM_ids, i_str_selected_CM_ids, i_str_selected_client_ids, i_str_selected_site_ids, i_str_selected_RecordType_ids, i_str_selected_JobType_ids, i_str_selected_JobSubType_ids, i_str_selected_JobStatus_ids, i_str_selected_Labour_Team_ids, i_str_selected_EquipmentType_ids, i_str_selected_MaterialType_ids, i_str_selected_ConstructionManager_ids, i_str_selected_VisitType_ids, (checkEditTeam.Checked ? 1 : 0));
                    RefreshGridViewStateJob.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == -2)  // SQL Server Time-Out //
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("The screen has not been able to return your data due to the volume of records in your request. Please filter your request by using the data filter or contact Technical Support for further help.", "Load Jobs", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Job Data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                 DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Job Data.\n\nMessage = [" + ex.Message + "].\n\nIf the error is a result of a query timeout, try adjusting the parameters at the top of the screen to reduce the amount of data returned then try again. If the problem persists, contact Technical Support.", "Load Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsJob != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDsJob.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["JobID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsJob = "";
                view.EndSelection();
            }

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void LoadLinkedRecords()
        {
            if (splitContainerControl1.Collapsed) return;  // Don't bother loading related data as the parent panel is collapsed so the grids are invisible //

            bool boolSplashScreenVisibile = splashScreenManager.IsSplashFormVisible;
            if (!boolSplashScreenVisibile)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Linked Data...");
            }

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControlJob.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;

            StringBuilder sb = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                sb.Append(view.GetRowCellValue(intRowHandle, view.Columns["JobID"]).ToString() + ",");
            }
            string strSelectedIDs = sb.ToString();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            if (xtraTabPageLabour.PageVisible)
            {
                gridControlLabour.MainView.BeginUpdate();
                this.RefreshGridViewStateLabour.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_OM_Job.sp06033_OM_Job_Manager_Linked_labour.Clear();
                }
                else
                {
                    try
                    {
                        sp06033_OM_Job_Manager_Linked_labourTableAdapter.Fill(dataSet_OM_Job.sp06033_OM_Job_Manager_Linked_labour, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    }
                    catch (Exception ex) { }
                    this.RefreshGridViewStateLabour.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlLabour.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDsLabour != "")
                {
                    strArray = i_str_AddedRecordIDsLabour.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlLabour.MainView;
                    view.BeginSelection();
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["LabourUsedID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDsLabour = "";
                    view.EndSelection();
                }
            }


            if (xtraTabPageEquipment.PageVisible)
            {
                gridControlEquipment.MainView.BeginUpdate();
                this.RefreshGridViewStateEquipment.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_OM_Job.sp06034_OM_Job_Manager_Linked_Equipment.Clear();
                }
                else
                {
                    try
                    {
                        sp06034_OM_Job_Manager_Linked_EquipmentTableAdapter.Fill(dataSet_OM_Job.sp06034_OM_Job_Manager_Linked_Equipment, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    }
                    catch (Exception ex) { }
                    this.RefreshGridViewStateEquipment.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlEquipment.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDsEquipment != "")
                {
                    strArray = i_str_AddedRecordIDsEquipment.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlEquipment.MainView;
                    view.BeginSelection();
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["EquipmentUsedID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDsEquipment = "";
                    view.EndSelection();
                }
            }


            if (xtraTabPageMaterials.PageVisible)
            {
                gridControlMaterial.MainView.BeginUpdate();
                this.RefreshGridViewStateMaterial.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_OM_Job.sp06035_OM_Job_Manager_Linked_Materials.Clear();
                }
                else
                {
                    try
                    {
                        sp06035_OM_Job_Manager_Linked_MaterialsTableAdapter.Fill(dataSet_OM_Job.sp06035_OM_Job_Manager_Linked_Materials, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    }
                    catch (Exception ex) { }
                    this.RefreshGridViewStateMaterial.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlMaterial.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDsMaterial != "")
                {
                    strArray = i_str_AddedRecordIDsMaterial.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlMaterial.MainView;
                    view.BeginSelection();
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["MaterialUsedID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDsMaterial = "";
                    view.EndSelection();
                }
            }


            if (xtraTabPagePictures.PageVisible)
            {
                gridControlPicture.MainView.BeginUpdate();
                this.RefreshGridViewStatePicture.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_OM_Job.sp06037_OM_Job_Manager_Linked_Pictures.Clear();
                }
                else
                {
                    try
                    {
                        sp06037_OM_Job_Manager_Linked_PicturesTableAdapter.Fill(dataSet_OM_Job.sp06037_OM_Job_Manager_Linked_Pictures, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), strDefaultPath);
                    }
                    catch (Exception ex) { }
                    this.RefreshGridViewStatePicture.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlPicture.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDsPicture != "")
                {
                    strArray = i_str_AddedRecordIDsPicture.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlPicture.MainView;
                    view.BeginSelection();
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["PictureID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDsPicture = "";
                    view.EndSelection();
                }
            }


            if (xtraTabPageHealthAndSafety.PageVisible)
            {
                gridControlHealthAndSafety.MainView.BeginUpdate();
                this.RefreshGridViewStateHealthAndSafety.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_OM_Job.sp06039_OM_Job_Manager_Linked_Safety_Requirement.Clear();
                }
                else
                {
                    try
                    {
                        sp06039_OM_Job_Manager_Linked_Safety_RequirementTableAdapter.Fill(dataSet_OM_Job.sp06039_OM_Job_Manager_Linked_Safety_Requirement, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    }
                    catch (Exception ex) { }
                    this.RefreshGridViewStateHealthAndSafety.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlHealthAndSafety.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDsHealthAndSafety != "")
                {
                    strArray = i_str_AddedRecordIDsHealthAndSafety.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlHealthAndSafety.MainView;
                    view.BeginSelection();
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["SafetyUsedID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDsHealthAndSafety = "";
                    view.EndSelection();
                }
            }


            if (xtraTabPageCRM.PageVisible)
            {
                gridControlCRM.MainView.BeginUpdate();
                this.RefreshGridViewStateCRM.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.woodPlanDataSet.sp05089_CRM_Contacts_Linked_To_Record.Clear();
                }
                else
                {
                    try
                    {
                        sp05089_CRM_Contacts_Linked_To_RecordTableAdapter.Fill(woodPlanDataSet.sp05089_CRM_Contacts_Linked_To_Record, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 201);
                    }
                    catch (Exception ex) { }
                    this.RefreshGridViewStateCRM.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlCRM.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDsCRM != "")
                {
                    strArray = i_str_AddedRecordIDsCRM.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlCRM.MainView;
                    view.BeginSelection();
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["CRMID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDsCRM = "";
                    view.EndSelection();
                }
            }

            if (xtraTabPageComments.PageVisible)
            {
                gridControlComment.MainView.BeginUpdate();
                this.RefreshGridViewStateComment.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_OM_Job.sp06040_OM_Job_Manager_Linked_Comments.Clear();
                }
                else
                {
                    try
                    {
                        sp06040_OM_Job_Manager_Linked_CommentsTableAdapter.Fill(dataSet_OM_Job.sp06040_OM_Job_Manager_Linked_Comments, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    }
                    catch (Exception ex) { }
                    this.RefreshGridViewStateComment.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlComment.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDsComment != "")
                {
                    strArray = i_str_AddedRecordIDsComment.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlComment.MainView;
                    view.BeginSelection();
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["CommentID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDsComment = "";
                    view.EndSelection();
                }
            }


            if (xtraTabPageWaste.PageVisible)
            {
                gridControlWaste.MainView.BeginUpdate();
                this.RefreshGridViewStateWaste.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_OM_Job.sp06036_OM_Job_Manager_Linked_Waste.Clear();
                }
                else
                {
                    try
                    {
                        sp06036_OM_Job_Manager_Linked_WasteTableAdapter.Fill(dataSet_OM_Job.sp06036_OM_Job_Manager_Linked_Waste, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 1);
                    }
                    catch (Exception ex) { }
                    this.RefreshGridViewStateWaste.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlWaste.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDsWaste != "")
                {
                    strArray = i_str_AddedRecordIDsWaste.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlWaste.MainView;
                    view.BeginSelection();
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["WasteID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDsWaste = "";
                    view.EndSelection();
                }
            }


            if (xtraTabPageSpraying.PageVisible)
            {
                gridControlSpraying.MainView.BeginUpdate();
                this.RefreshGridViewStateSpraying.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_OM_Job.sp06038_OM_Job_Manager_Linked_Spraying.Clear();
                }
                else
                {
                    try
                    {
                        sp06038_OM_Job_Manager_Linked_SprayingTableAdapter.Fill(dataSet_OM_Job.sp06038_OM_Job_Manager_Linked_Spraying, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 1);
                    }
                    catch (Exception ex) { }
                    this.RefreshGridViewStateSpraying.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlSpraying.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDsSpraying != "")
                {
                    strArray = i_str_AddedRecordIDsSpraying.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlSpraying.MainView;
                    view.BeginSelection();
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["SprayingID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDsSpraying = "";
                    view.EndSelection();
                }
            }


            if (xtraTabPageExtraInfo.PageVisible)
            {
                gridControlExtraInfo.MainView.BeginUpdate();
                this.RefreshGridViewStateExtraInfo.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_OM_Job.sp06041_OM_Job_Manager_Linked_Extra_Info.Clear();
                }
                else
                {
                    try
                    {
                        sp06041_OM_Job_Manager_Linked_Extra_InfoTableAdapter.Fill(dataSet_OM_Job.sp06041_OM_Job_Manager_Linked_Extra_Info, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    }
                    catch (Exception ex) { }
                    this.RefreshGridViewStateExtraInfo.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlExtraInfo.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDsExtraInfo != "")
                {
                    strArray = i_str_AddedRecordIDsExtraInfo.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlComment.MainView;
                    view.BeginSelection();
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["JobAttributeID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDsExtraInfo = "";
                    view.EndSelection();
                }
            }

            if (!boolSplashScreenVisibile && splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void LoadLinkedRecords_LabourTimings()
        {
            if (splitContainerControl2.Collapsed) return;  // Don't bother loading related data as the parent panel is collapsed so the grids are invisible //

            bool boolSplashScreenVisibile = splashScreenManager.IsSplashFormVisible;
            if (!boolSplashScreenVisibile)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Linked Data...");
            }

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControlLabour.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;

            StringBuilder sb = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                if (intRowHandle < 0) continue;
                sb.Append(view.GetRowCellValue(intRowHandle, view.Columns["LabourUsedID"]).ToString() + ",");
            }
            string strSelectedIDs = sb.ToString();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            if (xtraTabPageLabour.PageVisible)
            {
                gridControlLabourTiming.MainView.BeginUpdate();
                this.RefreshGridViewStateLabourTiming.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_OM_Job.sp06279_OM_Labour_Time_On_Off_Site.Clear();
                }
                else
                {
                    try
                    {
                        sp06279_OM_Labour_Time_On_Off_SiteTableAdapter.Fill(dataSet_OM_Job.sp06279_OM_Labour_Time_On_Off_Site, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    }
                    catch (Exception ex) { }
                    this.RefreshGridViewStateLabourTiming.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlLabourTiming.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDsLabourTiming != "")
                {
                    strArray = i_str_AddedRecordIDsLabourTiming.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlLabourTiming.MainView;
                    view.BeginSelection();
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["LabourUsedTimingID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDsLabourTiming = "";
                    view.EndSelection();
                }
            }

            if (!boolSplashScreenVisibile && splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }


        public void UpdateFormRefreshStatus(int status, Utils.enmFocusedGrid grid, string newIds)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            switch (grid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    i_str_AddedRecordIDsJob = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsJob : newIds);
                    break;
                case Utils.enmFocusedGrid.Labour:
                    i_str_AddedRecordIDsLabour = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsLabour : newIds);
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    i_str_AddedRecordIDsEquipment = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsEquipment : newIds);
                    break;
                case Utils.enmFocusedGrid.Materials:
                    i_str_AddedRecordIDsMaterial = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsMaterial : newIds);
                    break;
                case Utils.enmFocusedGrid.Pictures:
                    i_str_AddedRecordIDsPicture = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsPicture : newIds);
                    break;
                case Utils.enmFocusedGrid.HealthAndSafety:
                    i_str_AddedRecordIDsHealthAndSafety = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsHealthAndSafety : newIds);
                    break;
                case Utils.enmFocusedGrid.CRM:
                    i_str_AddedRecordIDsCRM = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsCRM : newIds);
                    break;
                case Utils.enmFocusedGrid.Comments:
                    i_str_AddedRecordIDsComment = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsComment : newIds);
                    break;
                case Utils.enmFocusedGrid.Waste:
                    i_str_AddedRecordIDsWaste = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsWaste : newIds);
                    break;
                case Utils.enmFocusedGrid.Spraying:
                    i_str_AddedRecordIDsSpraying = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsSpraying : newIds);
                    break;
                case Utils.enmFocusedGrid.ExtraInfo:
                    i_str_AddedRecordIDsExtraInfo = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsExtraInfo : newIds);
                    break;
                case Utils.enmFocusedGrid.LabourTiming:
                    i_str_AddedRecordIDsLabourTiming = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsLabourTiming : newIds);
                    break;
                default:
                    break;
            }
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                    case 1:  // Show Column Chooser for GridControl 1 //
                        if (sfpPermissions.blRead)
                        {
                            iBool_EnableGridColumnChooser = true;
                        }
                        break;
                    case 2:  // Add Wizard Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_AddWizardButtonEnabled = true;
                        }
                        break;
                    case 3:  // Send Jobs Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_SendJobsButtonEnabled = true;
                        }
                        break;
                    case 4:  // Reassign Jobs Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_ReassignJobsButtonEnabled = true;
                        }
                        break;
                    case 5:  // Manually Complete Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_ManuallyCompleteJobsEnabled = true;
                        }
                        break;
                }
            }
        }
       
        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            bsiAuditTrail.Enabled = false;
            bbiViewAuditTrail.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = (GridView)gridControlJob.MainView;
            GridView ParentView = (GridView)gridControlJob.MainView;

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    view = (GridView)gridControlJob.MainView;
                    break;
                case Utils.enmFocusedGrid.Labour:
                    view = (GridView)gridControlLabour.MainView;
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    view = (GridView)gridControlEquipment.MainView;
                    break;
                case Utils.enmFocusedGrid.Materials:
                    view = (GridView)gridControlMaterial.MainView;
                    break;
                case Utils.enmFocusedGrid.Pictures:
                    view = (GridView)gridControlPicture.MainView;
                    break;
                case Utils.enmFocusedGrid.HealthAndSafety:
                    view = (GridView)gridControlHealthAndSafety.MainView;
                    break;
                case Utils.enmFocusedGrid.CRM:
                    view = (GridView)gridControlCRM.MainView;
                    break;
                case Utils.enmFocusedGrid.Comments:
                    view = (GridView)gridControlComment.MainView;
                    break;
                case Utils.enmFocusedGrid.Waste:
                    view = (GridView)gridControlWaste.MainView;
                    break;
                case Utils.enmFocusedGrid.Spraying:
                    view = (GridView)gridControlSpraying.MainView;
                    break;
                case Utils.enmFocusedGrid.ExtraInfo:
                    view = (GridView)gridControlExtraInfo.MainView;
                    break;
                case Utils.enmFocusedGrid.LabourTiming:
                    view = (GridView)gridControlLabourTiming.MainView;
                    ParentView = (GridView)gridControlLabour.MainView;
                   break;
                default:
                    break;
            }
            int[] intRowHandles = view.GetSelectedRows();
            int[] intParentRowHandles = ParentView.GetSelectedRows();
            GridView ParentLabourTimingView = (GridView)gridControlLabour.MainView;
            int[] intParentLabourRowHandles = ParentLabourTimingView.GetSelectedRows();

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    bbiBlockAdd.Enabled = false;
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmFocusedGrid.Labour:
                case Utils.enmFocusedGrid.Equipment:
                case Utils.enmFocusedGrid.Materials:
                case Utils.enmFocusedGrid.Pictures:
                case Utils.enmFocusedGrid.HealthAndSafety:
                case Utils.enmFocusedGrid.CRM:
                case Utils.enmFocusedGrid.Comments:
                case Utils.enmFocusedGrid.Waste:
                case Utils.enmFocusedGrid.Spraying:
                case Utils.enmFocusedGrid.ExtraInfo:
                case Utils.enmFocusedGrid.LabourTiming:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;

                        if (intParentRowHandles.Length > 1)
                        {
                            alItems.Add("iBlockAdd");
                            bbiBlockAdd.Enabled = true;
                        }
                        else
                        {
                            bbiBlockAdd.Enabled = false;
                        }
                    }
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                default:
                    break;
            }
            bsiAuditTrail.Enabled = (intRowHandles.Length > 0);
            bbiViewAuditTrail.Enabled = (intRowHandles.Length > 0);

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of navigator custom buttons //
            gridControlJob.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlJob.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intParentRowHandles.Length > 0);
            gridControlJob.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intParentRowHandles.Length > 0);
            gridControlJob.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intParentRowHandles.Length > 0);
            gridControlJob.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intParentRowHandles.Length == 1);
            gridControlJob.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = (intParentRowHandles.Length > 0);

            view = (GridView)gridControlLabour.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1);

            view = (GridView)gridControlEquipment.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlEquipment.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlEquipment.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlEquipment.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlEquipment.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlEquipment.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1);
            gridControlEquipment.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = (intRowHandles.Length == 1);

            view = (GridView)gridControlMaterial.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlMaterial.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlMaterial.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlMaterial.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlMaterial.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlMaterial.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1);
            gridControlMaterial.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = (intRowHandles.Length == 1);

            view = (GridView)gridControlPicture.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlPicture.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlPicture.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlPicture.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlPicture.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlPicture.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1);

            view = (GridView)gridControlHealthAndSafety.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlHealthAndSafety.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlHealthAndSafety.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlHealthAndSafety.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlHealthAndSafety.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlHealthAndSafety.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1);

            view = (GridView)gridControlCRM.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1);

            view = (GridView)gridControlComment.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlComment.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlComment.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlComment.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlComment.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlComment.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1);

            view = (GridView)gridControlWaste.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlWaste.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlWaste.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlWaste.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlWaste.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlWaste.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1);

            view = (GridView)gridControlSpraying.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlSpraying.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlSpraying.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlSpraying.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlSpraying.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlSpraying.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1);

            view = (GridView)gridControlExtraInfo.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlExtraInfo.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlExtraInfo.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlExtraInfo.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlExtraInfo.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlExtraInfo.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1);
            gridControlExtraInfo.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = (intParentRowHandles.Length >= 1);

            view = (GridView)gridControlLabourTiming.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlLabourTiming.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlLabourTiming.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowAdd && intParentLabourRowHandles.Length > 0);
            gridControlLabourTiming.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlLabourTiming.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlLabourTiming.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length > 0);

            // Set Enabled Status of Main Toolbar Buttons //
            bbiAddWizard.Enabled = iBool_AddWizardButtonEnabled;
            bsiSendJobs.Enabled = iBool_SendJobsButtonEnabled;
            bbiSelectJobsReadyToSend.Enabled = iBool_SendJobsButtonEnabled;
            bbiSendJobs.Enabled = iBool_SendJobsButtonEnabled && intParentRowHandles.Length > 0;
            bbiReassignJobs.Enabled = iBool_ReassignJobsButtonEnabled && intParentRowHandles.Length > 0;
            bsiManuallyComplete.Enabled = iBool_ManuallyCompleteJobsEnabled;
            bbiSelectJobsToComplete.Enabled = iBool_ManuallyCompleteJobsEnabled;
            bbiManuallyCompleteJobs.Enabled = iBool_ManuallyCompleteJobsEnabled && intParentRowHandles.Length > 0;
            bbiViewSiteOnMap.Enabled = intParentRowHandles.Length > 0;
            bbiViewJobOnMap.Enabled = intParentRowHandles.Length > 0;
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            System.Reflection.MethodInfo method = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Job_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)view.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Job.sp06031_OM_Job_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID1 = row.ClientID;
                            fChildForm.intLinkedToRecordID2 = row.ClientContractID;
                            fChildForm.intLinkedToRecordID3 = row.SiteID;
                            fChildForm.intLinkedToRecordID4 = row.SiteContractID;
                            fChildForm.intLinkedToRecordID5 = row.VisitID;
                            fChildForm.intLinkedToRecordID6 = row.VisitNumber;
                            fChildForm.strLinkedToRecordDesc1 = row.ClientName;
                            fChildForm.strLinkedToRecordDesc2 = row.ContractDescription;
                            fChildForm.strLinkedToRecordDesc3 = row.SiteName;
                            fChildForm.strLinkedToRecordDesc4 = row.SitePostcode;
                            fChildForm._VisitCostCalculationLevelID = row.VisitCostCalculationLevelID;
                            fChildForm._VisitCostCalculationLevel = row.VisitCostCalculationLevelDescription;
                            fChildForm._VisitSellCalculationLevelID = row.VisitSellCalculationLevelID;
                            fChildForm._VisitSellCalculationLevel = row.VisitSellCalculationLevelDescription;
                        }

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;            
                case Utils.enmFocusedGrid.Labour:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Job_Labour_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlJob.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Job.sp06031_OM_Job_ManagerRow)rowView.Row;
                            fChildForm._ClientID = row.ClientID;
                            fChildForm._ClientContractID = row.ClientContractID;
                            fChildForm._SiteID = row.SiteID;
                            fChildForm._SiteContractID = row.SiteContractID;
                            fChildForm._VisitID = row.VisitID;
                            fChildForm._VisitNumber = row.VisitNumber;
                            fChildForm._JobID = row.JobID;
                            fChildForm._JobTypeID = row.JobTypeID;
                            fChildForm._JobSubTypeID = row.JobSubTypeID;
                            fChildForm._ClientName = row.ClientName;
                            fChildForm._ContractDescription = row.ContractDescription;
                            fChildForm._SiteName = row.SiteName;
                            fChildForm._JobTypeDescription = row.JobTypeDescription;
                            fChildForm._JobSubTypeDescription = row.JobSubTypeDescription;
                            fChildForm._SiteLatitude = row.LocationX;
                            fChildForm._SiteLongitude = row.LocationY;
                            fChildForm._SitePostcode = row.SitePostcode;
                        }
                        else if (intRowHandles.Length > 1)  // Check if the same site or same job sub-type and if yes, pass through to edit form to help with the select Contractor screen for use with Job Competencies and Contractor Location //
                        {
                            int intSelectedJobSubTypeID = 0;
                            int intTempJobSubTypeID = 0;
                            bool boolOneJobSubTypeSelected = true;

                            int intSelectedSiteID = 0;
                            int intTempJobSiteID = 0;
                            bool boolOneSiteSelected = true;

                            foreach (int intRowHandle in intRowHandles)
                            {
                                intTempJobSubTypeID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                                if (intSelectedJobSubTypeID == 0)
                                {
                                    intSelectedJobSubTypeID = intTempJobSubTypeID;
                                }
                                else if (intSelectedJobSubTypeID != intTempJobSubTypeID)
                                {
                                    boolOneJobSubTypeSelected = false;
                                }
                                intTempJobSiteID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandle, "SiteID"));
                                if (intSelectedSiteID == 0)
                                {
                                    intSelectedSiteID = intTempJobSiteID;
                                }
                                else if (intSelectedSiteID != intTempJobSiteID)
                                {
                                    boolOneSiteSelected = false;
                                }
                            }
                            if (boolOneJobSubTypeSelected) fChildForm._JobSubTypeID = intSelectedJobSubTypeID;
                            if (boolOneSiteSelected) fChildForm._SiteID = intSelectedSiteID;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;               
                case Utils.enmFocusedGrid.Equipment:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlEquipment.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Job_Equipment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlJob.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Job.sp06031_OM_Job_ManagerRow)rowView.Row;
                            fChildForm._ClientID = row.ClientID;
                            fChildForm._ClientContractID = row.ClientContractID;
                            fChildForm._SiteID = row.SiteID;
                            fChildForm._SiteContractID = row.SiteContractID;
                            fChildForm._VisitID = row.VisitID;
                            fChildForm._VisitNumber = row.VisitNumber;
                            fChildForm._JobID = row.JobID;
                            fChildForm._JobTypeID = row.JobTypeID;
                            fChildForm._JobSubTypeID = row.JobSubTypeID;
                            fChildForm._ClientName = row.ClientName;
                            fChildForm._ContractDescription = row.ContractDescription;
                            fChildForm._SiteName = row.SiteName;
                            fChildForm._JobTypeDescription = row.JobTypeDescription;
                            fChildForm._JobSubTypeDescription = row.JobSubTypeDescription;
                            fChildForm._SiteLatitude = row.LocationX;
                            fChildForm._SiteLongitude = row.LocationY;
                            fChildForm._SitePostcode = row.SitePostcode;
                        }
                        else if (intRowHandles.Length > 1)  // Check if the same site or same job sub-type and if yes, pass through to edit form to help with the select Contractor screen for use with Job Competencies and Contractor Location //
                        {
                            int intSelectedJobSubTypeID = 0;
                            int intTempJobSubTypeID = 0;
                            bool boolOneJobSubTypeSelected = true;

                            int intSelectedSiteID = 0;
                            int intTempJobSiteID = 0;
                            bool boolOneSiteSelected = true;

                            foreach (int intRowHandle in intRowHandles)
                            {
                                intTempJobSubTypeID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                                if (intSelectedJobSubTypeID == 0)
                                {
                                    intSelectedJobSubTypeID = intTempJobSubTypeID;
                                }
                                else if (intSelectedJobSubTypeID != intTempJobSubTypeID)
                                {
                                    boolOneJobSubTypeSelected = false;
                                }
                                intTempJobSiteID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandle, "SiteID"));
                                if (intSelectedSiteID == 0)
                                {
                                    intSelectedSiteID = intTempJobSiteID;
                                }
                                else if (intSelectedSiteID != intTempJobSiteID)
                                {
                                    boolOneSiteSelected = false;
                                }
                            }
                            if (boolOneJobSubTypeSelected) fChildForm._JobSubTypeID = intSelectedJobSubTypeID;
                            if (boolOneSiteSelected) fChildForm._SiteID = intSelectedSiteID;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlMaterial.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Job_Material_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlJob.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Job.sp06031_OM_Job_ManagerRow)rowView.Row;
                            fChildForm._ClientID = row.ClientID;
                            fChildForm._ClientContractID = row.ClientContractID;
                            fChildForm._SiteID = row.SiteID;
                            fChildForm._SiteContractID = row.SiteContractID;
                            fChildForm._VisitID = row.VisitID;
                            fChildForm._VisitNumber = row.VisitNumber;
                            fChildForm._JobID = row.JobID;
                            fChildForm._JobTypeID = row.JobTypeID;
                            fChildForm._JobSubTypeID = row.JobSubTypeID;
                            fChildForm._ClientName = row.ClientName;
                            fChildForm._ContractDescription = row.ContractDescription;
                            fChildForm._SiteName = row.SiteName;
                            fChildForm._JobTypeDescription = row.JobTypeDescription;
                            fChildForm._JobSubTypeDescription = row.JobSubTypeDescription;
                            fChildForm._SiteLatitude = row.LocationX;
                            fChildForm._SiteLongitude = row.LocationY;
                            fChildForm._SitePostcode = row.SitePostcode;
                        }
                        else if (intRowHandles.Length > 1)  // Check if the same site or same job sub-type and if yes, pass through to edit form to help with the select Contractor screen for use with Job Competencies and Contractor Location //
                        {
                            int intSelectedJobSubTypeID = 0;
                            int intTempJobSubTypeID = 0;
                            bool boolOneJobSubTypeSelected = true;

                            int intSelectedSiteID = 0;
                            int intTempJobSiteID = 0;
                            bool boolOneSiteSelected = true;

                            foreach (int intRowHandle in intRowHandles)
                            {
                                intTempJobSubTypeID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                                if (intSelectedJobSubTypeID == 0)
                                {
                                    intSelectedJobSubTypeID = intTempJobSubTypeID;
                                }
                                else if (intSelectedJobSubTypeID != intTempJobSubTypeID)
                                {
                                    boolOneJobSubTypeSelected = false;
                                }
                                intTempJobSiteID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandle, "SiteID"));
                                if (intSelectedSiteID == 0)
                                {
                                    intSelectedSiteID = intTempJobSiteID;
                                }
                                else if (intSelectedSiteID != intTempJobSiteID)
                                {
                                    boolOneSiteSelected = false;
                                }
                            }
                            if (boolOneJobSubTypeSelected) fChildForm._JobSubTypeID = intSelectedJobSubTypeID;
                            if (boolOneSiteSelected) fChildForm._SiteID = intSelectedSiteID;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Pictures:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlPicture.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Picture_Link_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlJob.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Job.sp06031_OM_Job_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.JobID;
                            fChildForm.strLinkedToRecordDesc = HtmlRemoval.StripTagsCharArray(row.FullDescription);
                            fChildForm.intRecordTypeID = 1;  // Job //
                            fChildForm.strRecordTypeDescription = "Job";
                            fChildForm.strImagesFolderOM = row.ImagesFolderOM;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.HealthAndSafety:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlHealthAndSafety.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Job_Safety_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlJob.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Job.sp06031_OM_Job_ManagerRow)rowView.Row;
                            fChildForm._ClientID = row.ClientID;
                            fChildForm._ClientContractID = row.ClientContractID;
                            fChildForm._SiteID = row.SiteID;
                            fChildForm._SiteContractID = row.SiteContractID;
                            fChildForm._VisitID = row.VisitID;
                            fChildForm._VisitNumber = row.VisitNumber;
                            fChildForm._JobID = row.JobID;
                            fChildForm._JobTypeID = row.JobTypeID;
                            fChildForm._JobSubTypeID = row.JobSubTypeID;
                            fChildForm._ClientName = row.ClientName;
                            fChildForm._ContractDescription = row.ContractDescription;
                            fChildForm._SiteName = row.SiteName;
                            fChildForm._JobTypeDescription = row.JobTypeDescription;
                            fChildForm._JobSubTypeDescription = row.JobSubTypeDescription;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.CRM:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlCRM.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_Core_CRM_Contact_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlJob.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Job.sp06031_OM_Job_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.JobID;
                            fChildForm.strLinkedToRecordDesc = String.Format("{0} - {1}", row.SiteName, row.JobSubTypeDescription);
                            fChildForm.intRecordTypeID = 201;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Comments:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlComment.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Comment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlJob.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Job.sp06031_OM_Job_ManagerRow)rowView.Row;
                            fChildForm._ClientID = row.ClientID;
                            fChildForm._ClientContractID = row.ClientContractID;
                            fChildForm._SiteID = row.SiteID;
                            fChildForm._SiteContractID = row.SiteContractID;
                            fChildForm._VisitID = row.VisitID;
                            fChildForm._VisitNumber = row.VisitNumber;
                            fChildForm._JobID = row.JobID;
                            fChildForm._JobTypeID = row.JobTypeID;
                            fChildForm._JobSubTypeID = row.JobSubTypeID;
                            fChildForm._ClientName = row.ClientName;
                            fChildForm._ContractDescription = row.ContractDescription;
                            fChildForm._SiteName = row.SiteName;
                            fChildForm._JobTypeDescription = row.JobTypeDescription;
                            fChildForm._JobSubTypeDescription = row.JobSubTypeDescription;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Waste:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlWaste.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Job_Waste_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlJob.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Job.sp06031_OM_Job_ManagerRow)rowView.Row;
                            fChildForm._ClientID = row.ClientID;
                            fChildForm._ClientContractID = row.ClientContractID;
                            fChildForm._SiteID = row.SiteID;
                            fChildForm._SiteContractID = row.SiteContractID;
                            fChildForm._VisitID = row.VisitID;
                            fChildForm._VisitNumber = row.VisitNumber;
                            fChildForm._LinkedToRecordID = row.JobID;
                            fChildForm._LinkedToRecordTypeID = 1;
                            fChildForm._LinkedToRecordSubTypeID = 1;  // 0 = Visit, 1 = Job //
                            fChildForm._LinkedToParentDescription = row.JobTypeDescription + " - " + row.JobSubTypeDescription;
                            fChildForm._JobTypeID = row.JobTypeID;
                            fChildForm._JobSubTypeID = row.JobSubTypeID;
                            fChildForm._ClientName = row.ClientName;
                            fChildForm._ContractDescription = row.ContractDescription;
                            fChildForm._SiteName = row.SiteName;
                            fChildForm._JobTypeDescription = row.JobTypeDescription;
                            fChildForm._JobSubTypeDescription = row.JobSubTypeDescription;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Spraying:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlSpraying.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Job_Spraying_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlJob.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Job.sp06031_OM_Job_ManagerRow)rowView.Row;
                            fChildForm._ClientID = row.ClientID;
                            fChildForm._ClientContractID = row.ClientContractID;
                            fChildForm._SiteID = row.SiteID;
                            fChildForm._SiteContractID = row.SiteContractID;
                            fChildForm._VisitID = row.VisitID;
                            fChildForm._VisitNumber = row.VisitNumber;
                            fChildForm._LinkedToRecordID = row.JobID;
                            fChildForm._LinkedToRecordTypeID = 1;
                            fChildForm._LinkedToParentDescription = row.JobTypeDescription + " - " + row.JobSubTypeDescription;
                            fChildForm._JobTypeID = row.JobTypeID;
                            fChildForm._JobSubTypeID = row.JobSubTypeID;
                            fChildForm._ClientName = row.ClientName;
                            fChildForm._ContractDescription = row.ContractDescription;
                            fChildForm._SiteName = row.SiteName;
                            fChildForm._JobTypeDescription = row.JobTypeDescription;
                            fChildForm._JobSubTypeDescription = row.JobSubTypeDescription;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ExtraInfo:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlExtraInfo.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Job_Attribute_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlJob.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Job.sp06031_OM_Job_ManagerRow)rowView.Row;
                            fChildForm._ClientID = row.ClientID;
                            fChildForm._ClientContractID = row.ClientContractID;
                            fChildForm._SiteID = row.SiteID;
                            fChildForm._SiteContractID = row.SiteContractID;
                            fChildForm._VisitID = row.VisitID;
                            fChildForm._VisitNumber = row.VisitNumber;
                            fChildForm._JobID = row.JobID;
                            fChildForm._JobTypeID = row.JobTypeID;
                            fChildForm._JobSubTypeID = row.JobSubTypeID;
                            fChildForm._ClientName = row.ClientName;
                            fChildForm._ContractDescription = row.ContractDescription;
                            fChildForm._SiteName = row.SiteName;
                            fChildForm._JobTypeDescription = row.JobTypeDescription;
                            fChildForm._JobSubTypeDescription = row.JobSubTypeDescription;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.LabourTiming:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlLabourTiming.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Job_Labour_On_Off_Site_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlJob.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Job.sp06031_OM_Job_ManagerRow)rowView.Row;
                            fChildForm._ClientID = row.ClientID;
                            fChildForm._ClientContractID = row.ClientContractID;
                            fChildForm._SiteID = row.SiteID;
                            fChildForm._SiteContractID = row.SiteContractID;
                            fChildForm._VisitID = row.VisitID;
                            fChildForm._VisitNumber = row.VisitNumber;
                            fChildForm._JobID = row.JobID;
                            fChildForm._JobTypeID = row.JobTypeID;
                            fChildForm._JobSubTypeID = row.JobSubTypeID;
                            fChildForm._ClientName = row.ClientName;
                            fChildForm._ContractDescription = row.ContractDescription;
                            fChildForm._SiteName = row.SiteName;
                            fChildForm._JobTypeDescription = row.JobTypeDescription;
                            fChildForm._JobSubTypeDescription = row.JobSubTypeDescription;

                            ParentView = (GridView)gridControlLabour.MainView;
                            intRowHandles = ParentView.GetSelectedRows();
                            if (intRowHandles.Length == 1)
                            {
                                var rowView2 = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                                var row2 = (DataSet_OM_Job.sp06033_OM_Job_Manager_Linked_labourRow)rowView2.Row;
                                fChildForm._LabourUsedID = row2.LabourUsedID;
                                fChildForm._TeamName = row2.ContractorName;
                                fChildForm._ContractorID = row2.ContractorID;
                                fChildForm._LinkedToPersonTypeID = row2.LinkedToPersonTypeID;
                            }
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Add()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Labour:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }

                        var fChildForm1 = new frm_OM_Job_Labour_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();

                        // Check if we have just one Job Sub-Type selected as we can pass this through to the select Contractor screen for use with Job Competencies //
                        int intSelectedJobSubTypeID = 0;
                        int intTempValue = 0;
                        bool boolOneJobSubTypeSelected = true;
                        string strSelectedSiteIDs = "";
                        string strTempValue2 = "";
                        bool boolOneSiteSelected = true;
                        foreach (int intRowHandle in intRowHandles)
                        {
                            intTempValue = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                            strTempValue2 = view.GetRowCellValue(intRowHandle, "SiteID").ToString();
                            if (intSelectedJobSubTypeID <= 0)
                            {
                                intSelectedJobSubTypeID = intTempValue;
                            }
                            else if (intSelectedJobSubTypeID != intTempValue)
                            {
                                boolOneJobSubTypeSelected = false;
                            }

                            if (string.IsNullOrWhiteSpace(strSelectedSiteIDs))
                            {
                                strSelectedSiteIDs = strTempValue2;
                            }
                            else if (strSelectedSiteIDs != strTempValue2)
                            {
                                boolOneSiteSelected = false;
                            }
                        }
                        if (boolOneJobSubTypeSelected) fChildForm1._JobSubTypeID = intSelectedJobSubTypeID;

                        if (boolOneSiteSelected)
                        {
                            fChildForm1._SitePostcode = view.GetRowCellValue(intRowHandles[0], "SitePostcode").ToString();
                            fChildForm1._SiteLatitude = Convert.ToDouble(view.GetRowCellValue(intRowHandles[0], "SiteLocationX"));
                            fChildForm1._SiteLongitude = Convert.ToDouble(view.GetRowCellValue(intRowHandles[0], "SiteLocationY"));
                        }

                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }

                        var fChildForm1 = new frm_OM_Job_Equipment_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();

                        // Check if we have just one Job Sub-Type selected as we can pass this through to the select Contractor screen for use with Job Competencies //
                        int intSelectedJobSubTypeID = 0;
                        int intTempValue = 0;
                        bool boolOneJobSubTypeSelected = true;
                        string strSelectedSiteIDs = "";
                        string strTempValue2 = "";
                        bool boolOneSiteSelected = true;
                        foreach (int intRowHandle in intRowHandles)
                        {
                            intTempValue = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                            strTempValue2 = view.GetRowCellValue(intRowHandle, "SiteID").ToString();
                            if (intSelectedJobSubTypeID <= 0)
                            {
                                intSelectedJobSubTypeID = intTempValue;
                            }
                            else if (intSelectedJobSubTypeID != intTempValue)
                            {
                                boolOneJobSubTypeSelected = false;
                            }

                            if (string.IsNullOrWhiteSpace(strSelectedSiteIDs))
                            {
                                strSelectedSiteIDs = strTempValue2;
                            }
                            else if (strSelectedSiteIDs != strTempValue2)
                            {
                                boolOneSiteSelected = false;
                            }
                        }
                        if (boolOneJobSubTypeSelected) fChildForm1._JobSubTypeID = intSelectedJobSubTypeID;

                        if (boolOneSiteSelected)
                        {
                            fChildForm1._SitePostcode = view.GetRowCellValue(intRowHandles[0], "SitePostcode").ToString();
                            fChildForm1._SiteLatitude = Convert.ToDouble(view.GetRowCellValue(intRowHandles[0], "SiteLocationX"));
                            fChildForm1._SiteLongitude = Convert.ToDouble(view.GetRowCellValue(intRowHandles[0], "SiteLocationY"));
                        }

                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }

                        var fChildForm1 = new frm_OM_Job_Material_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();

                        // Check if we have just one Job Sub-Type selected as we can pass this through to the select Contractor screen for use with Job Competencies //
                        int intSelectedJobSubTypeID = 0;
                        int intTempValue = 0;
                        bool boolOneJobSubTypeSelected = true;
                        string strSelectedSiteIDs = "";
                        string strTempValue2 = "";
                        bool boolOneSiteSelected = true;
                        foreach (int intRowHandle in intRowHandles)
                        {
                            intTempValue = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                            strTempValue2 = view.GetRowCellValue(intRowHandle, "SiteID").ToString();
                            if (intSelectedJobSubTypeID <= 0)
                            {
                                intSelectedJobSubTypeID = intTempValue;
                            }
                            else if (intSelectedJobSubTypeID != intTempValue)
                            {
                                boolOneJobSubTypeSelected = false;
                            }

                            if (string.IsNullOrWhiteSpace(strSelectedSiteIDs))
                            {
                                strSelectedSiteIDs = strTempValue2;
                            }
                            else if (strSelectedSiteIDs != strTempValue2)
                            {
                                boolOneSiteSelected = false;
                            }
                        }
                        if (boolOneJobSubTypeSelected) fChildForm1._JobSubTypeID = intSelectedJobSubTypeID;

                        if (boolOneSiteSelected)
                        {
                            fChildForm1._SitePostcode = view.GetRowCellValue(intRowHandles[0], "SitePostcode").ToString();
                            fChildForm1._SiteLatitude = Convert.ToDouble(view.GetRowCellValue(intRowHandles[0], "SiteLocationX"));
                            fChildForm1._SiteLongitude = Convert.ToDouble(view.GetRowCellValue(intRowHandles[0], "SiteLocationY"));
                        }

                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Pictures:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }

                        var fChildForm1 = new frm_OM_Picture_Link_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = "";
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();

                        fChildForm1.intRecordTypeID = 1;  // Job //
                        fChildForm1.strRecordTypeDescription = "Job";

                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.HealthAndSafety:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }

                        var fChildForm1 = new frm_OM_Job_Safety_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = "";
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.CRM:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }

                        var fChildForm1 = new frm_Core_CRM_Contact_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = "";
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.intRecordTypeID = 201;

                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Comments:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }

                        var fChildForm1 = new frm_OM_Comment_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Waste:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }

                        var fChildForm1 = new frm_OM_Job_Waste_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        fChildForm1._LinkedToRecordTypeID = 1;
                        fChildForm1._LinkedToRecordSubTypeID = 1;  // 0 = Visit, 1 = Job //
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Spraying:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }

                        var fChildForm1 = new frm_OM_Job_Spraying_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        fChildForm1._LinkedToRecordTypeID = 1;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ExtraInfo:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }

                        var fChildForm1 = new frm_HR_Employee_Address_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = "";
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.LabourTiming:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LabourUsedID")) + ',';
                        }

                        var fChildForm1 = new frm_OM_Job_Labour_On_Off_Site_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();

                        GridView ParentView = (GridView)gridControlLabour.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length >= 1)
                        {
                            var rowView2 = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row2 = (DataSet_OM_Job.sp06033_OM_Job_Manager_Linked_labourRow)rowView2.Row;
                            fChildForm1._ContractorID = row2.ContractorID;
                            fChildForm1._TeamName = row2.ContractorName;
                            fChildForm1._LinkedToPersonTypeID = row2.LinkedToPersonTypeID;
                        }

                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        bool boolAtLeastOneSelfBillingInvoice = false;
                        bool boolAtLeastOneClientInvoice = false;
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                            if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SelfBillingInvoiceID")) > 0) boolAtLeastOneSelfBillingInvoice = true;
                            if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ClientInvoiceID")) > 0) boolAtLeastOneClientInvoice = true;
                        }

                        var fChildForm = new frm_OM_Job_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm._AtLeastOneSelfBillingInvoice = boolAtLeastOneSelfBillingInvoice;
                        fChildForm._AtLeastOneClientInvoice = boolAtLeastOneClientInvoice;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Labour:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LabourUsedID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Labour_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlEquipment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EquipmentUsedID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Equipment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlMaterial.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "MaterialtUsedID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Material_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Pictures:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlPicture.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PictureID")) + ',';
                        }

                        var fChildForm = new frm_OM_Picture_Link_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.HealthAndSafety:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlHealthAndSafety.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SafetyUsedID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Safety_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.CRM:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlCRM.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }

                        var fChildForm = new frm_Core_CRM_Contact_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Comments:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlComment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CommentID")) + ',';
                        }

                        var fChildForm = new frm_OM_Comment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Waste:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlWaste.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WasteID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Waste_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm._LinkedToRecordTypeID = 1;
                        fChildForm._LinkedToRecordSubTypeID = 1;  // 0 = Visit, 1 = Job //
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Spraying:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlSpraying.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SprayingID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Spraying_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm._LinkedToRecordTypeID = 1;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ExtraInfo:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlExtraInfo.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobAttributeID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Attribute_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.LabourTiming:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlLabourTiming.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LabourUsedTimingID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Labour_On_Off_Site_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Labour:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LabourUsedID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Labour_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlEquipment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EquipmentUsedID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Equipment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlMaterial.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "MaterialUsedID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Material_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Pictures:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlPicture.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PictureID")) + ',';
                        }

                        var fChildForm = new frm_OM_Picture_Link_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.HealthAndSafety:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlHealthAndSafety.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SafetyUsedID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Safety_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.CRM:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlCRM.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }

                        var fChildForm = new frm_Core_CRM_Contact_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Comments:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlComment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CommentID")) + ',';
                        }

                        var fChildForm = new frm_OM_Comment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Waste:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlWaste.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WasteID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Waste_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm._LinkedToRecordTypeID = 1;
                        fChildForm._LinkedToRecordSubTypeID = 1;  // 0 = Visit, 1 = Job //
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Spraying:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlSpraying.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SprayingID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Spraying_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm._LinkedToRecordTypeID = 1;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ExtraInfo:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlExtraInfo.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobAttributeID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Attribute_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.LabourTiming:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlLabourTiming.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LabourUsedTimingID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Labour_On_Off_Site_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            string strRecordIDs = "";
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlJob.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Jobs to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        view.BeginSelection();
                        List<int> disallowedStatuses = new List<int> { 40, 80 };  // 40: Sent,  80 In-Progress //
                        foreach (int intRowHandle in intRowHandles)
                        {
                            if (disallowedStatuses.Contains(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobStatusID"))))
                            {
                                view.UnselectRow(intRowHandle);
                            }
                        }
                        view.EndSelection();
                        // Check there is at least one row still selected //
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Jobs to delete by clicking on them then try again.\n\nOnly Jobs not Sent or In-Progress can be selected for deletion. If these jobs are selected this process will de-select them.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Job" : Convert.ToString(intRowHandles.Length) + " Jobs") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Job" : "these Jobs") + " will no longer be available for selection and any related records will also be deleted!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Data();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.Labour:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlLabour.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Labour to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Labour" : Convert.ToString(intRowHandles.Length) + " Linked Labour") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Labour" : "these Linked Labour") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LabourUsedID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job_labour_used", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlEquipment.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Equipment to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Equipment" : Convert.ToString(intRowHandles.Length) + " Linked Equipment") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Equipment" : "these Linked Equipment") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EquipmentUsedID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job_equipment_used", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlMaterial.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Materials to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Material" : Convert.ToString(intRowHandles.Length) + " Linked Materials") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Material" : "these Linked Materials") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "MaterialUsedID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job_material_used", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.Pictures:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlPicture.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Pictures to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Picture" : Convert.ToString(intRowHandles.Length) + " Linked Pictures") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Picture" : "these Linked Pictures") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PictureID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job_picture", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.HealthAndSafety:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlHealthAndSafety.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Health And Safety Requirments to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Health And Safety Requirement" : Convert.ToString(intRowHandles.Length) + " Linked Health And Safety Requirements") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Health And Safety Requirement" : "these Linked Health And Safety Requirements") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SafetyUsedID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job_safety_used", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.CRM:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlCRM.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked CRM to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked CRM" : Convert.ToString(intRowHandles.Length) + " Linked CRM") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked CRM" : "these Linked CRM") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job_crm", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.Comments:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlComment.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Comments to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Comment" : Convert.ToString(intRowHandles.Length) + " Linked Comments") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Comment" : "these Linked Comments") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CommentID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job_comment", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.Waste:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlWaste.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Waste records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Waste record" : Convert.ToString(intRowHandles.Length) + " Linked Waste records") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Waste record" : "these Linked Waste records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WasteID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job_waste", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.Spraying:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlSpraying.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Spraying to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Spraying" : Convert.ToString(intRowHandles.Length) + " Linked Spraying") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Spraying" : "these Linked Spraying") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SprayingID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job_spraying", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.ExtraInfo:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlExtraInfo.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Extra Info to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Extra Info" : Convert.ToString(intRowHandles.Length) + " Linked Extra Info") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Extra Info" : "these Linked Extra Info") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobAttributeID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job_attribute", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.LabourTiming:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlLabourTiming.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Labour Time On \\ Off Site records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Labour Time On \\ Off Site record" : Convert.ToString(intRowHandles.Length) + " Linked Labour Time On \\ Off Site records") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Labour Time On \\ Off Site record" : "these Linked Labour Time On \\ Off Site records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LabourUsedTimingID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job_labour_used_timing", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords_LabourTimings();
                            
                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            //switch (i_int_FocusedGrid)
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    {
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }
                        var fChildForm = new frm_OM_Job_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Labour:
                    {
                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LabourUsedID")) + ',';
                        }
                        var fChildForm = new frm_OM_Job_Labour_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        view = (GridView)gridControlEquipment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EquipmentUsedID")) + ',';
                        }
                        var fChildForm = new frm_OM_Job_Equipment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        view = (GridView)gridControlMaterial.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "MaterialUsedID")) + ',';
                        }
                        var fChildForm = new frm_OM_Job_Material_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Pictures:
                    {
                        view = (GridView)gridControlPicture.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PictureID")) + ',';
                        }
                        var fChildForm = new frm_OM_Picture_Link_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.HealthAndSafety:
                    {
                        view = (GridView)gridControlHealthAndSafety.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SafetyUsedID")) + ',';
                        }
                        var fChildForm = new frm_OM_Job_Safety_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.CRM:
                    {
                        view = (GridView)gridControlCRM.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }
                        var fChildForm = new frm_Core_CRM_Contact_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Comments:
                    {
                        view = (GridView)gridControlComment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CommentID")) + ',';
                        }
                        var fChildForm = new frm_OM_Comment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Waste:
                    {
                        view = (GridView)gridControlWaste.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WasteID")) + ',';
                        }
                        var fChildForm = new frm_OM_Job_Waste_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm._LinkedToRecordTypeID = 1;
                        fChildForm._LinkedToRecordSubTypeID = 1;  // 0 = Visit, 1 = Job //
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Spraying:
                    {
                        view = (GridView)gridControlSpraying.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SprayingID")) + ',';
                        }
                        var fChildForm = new frm_OM_Job_Spraying_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm._LinkedToRecordTypeID = 1;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ExtraInfo:
                    {
                        view = (GridView)gridControlExtraInfo.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobAttributeID")) + ',';
                        }
                        var fChildForm = new frm_OM_Job_Attribute_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.LabourTiming:
                    {
                        view = (GridView)gridControlLabourTiming.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LabourUsedTimingID")) + ',';
                        }
                        var fChildForm = new frm_OM_Job_Labour_On_Off_Site_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                default:
                    break;
            }
        }


        public override void OnViewAuditTrail(object sender, EventArgs e)
        {
            View_Audit_Trail();
        }
        private void View_Audit_Trail()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Job";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Labour:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LabourUsedID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Job_Labour_Used";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.LabourTiming:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlLabourTiming.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LabourUsedTimingID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Job_Labour_Used_Timing";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlEquipment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EquipmentUsedID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Job_Equipment_Used";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlMaterial.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "MaterialUsedID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Job_Material_Used";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ExtraInfo:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlExtraInfo.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobAttributeID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Job_Attribute";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Spraying:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlSpraying.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SprayingID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Spraying_Used";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Waste:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlWaste.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WasteID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Waste_Used";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Pictures:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlPicture.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PictureID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Picture";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Comments:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlComment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CommentID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Comment";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.HealthAndSafety:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlHealthAndSafety.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SafetyUsedID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Job_Safety_Used";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.CRM:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlCRM.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.GC_CRM";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridViewJob":
                    message = "No Jobs Available - Adjust any filters and click Load Data button";
                    break;
                case "gridViewLabour":
                    message = "No Linked Labour Available - Select one or more Jobs to view Linked Labour";
                    break;
                case "gridViewEquipment":
                    message = "No Linked Equipment Available - Select one or more Jobs to view Linked Equipment";
                    break;
                case "gridViewMaterial":
                    message = "No Linked Materials Available - Select one or more Jobs to view Linked Materials";
                    break;
                case "gridViewPicture":
                    message = "No Linked Pictures Available - Select one or more Jobs to view Linked Pictures";
                    break;
                case "gridViewComment":
                    message = "No Linked Comments Available - Select one or more Jobs to view Linked Comments";
                    break;
                case "gridViewHealthAndSafety":
                    message = "No Linked Health and Safety Available - Select one or more Jobs to view Linked Health and Safety";
                    break;
                case "gridViewCRM":
                    message = "No CRM Contact - Select one or more Jobs to view Linked CRM Contact";
                    break;
                case "gridViewWaste":
                    message = "No Waste - Select one or more Jobs to view Linked Waste";
                    break;
                case "gridViewSpraying":
                    message = "No Spraying - Select one or more Jobs to view Linked Spraying";
                    break;
                case "gridViewExtraInfo":
                    message = "No Extra Info - Select one or more Jobs to view Linked Extra Info";
                    break;
                case "gridViewLabourTiming":
                    message = "No Labour Timings Info - Select one or more Job Labour records to view Linked Labour Timings";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridViewJob":
                    LoadLinkedRecords();
                    view = (GridView)gridControlLabour.MainView;
                    view.ExpandAllGroups();
                    view.SelectAll();  // Select all Labour rows so the lavour grid loads all the child labour timing records //

                    view = (GridView)gridControlEquipment.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControlMaterial.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControlPicture.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControlHealthAndSafety.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControlCRM.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControlComment.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControlWaste.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControlSpraying.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControlExtraInfo.MainView;
                    view.ExpandAllGroups();
                    SetMenuStatus();

                    int intSelectedCount = view.GetSelectedRows().Length;
                    if (intSelectedCount == 0)
                    {
                        bsiSelectedCount.Visibility = BarItemVisibility.Never;
                    }
                    else
                    {
                        bsiSelectedCount.Visibility = BarItemVisibility.Always;
                        bsiSelectedCount.Caption = (intSelectedCount == 1 ? "1 Job Selected" : "<color=red>" + intSelectedCount.ToString() + " Jobs</Color> Selected");
                    }
                    break;
                case "gridViewLabour":
                    LoadLinkedRecords_LabourTimings();
                    view = (GridView)gridControlLabourTiming.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView - Job

        private void gridControlJob_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = gridViewJob;
                        int intRecordType = 21;  // Job //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "JobID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "FullDescription").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    else if ("sync_time".Equals(e.Button.Tag))
                    {
                        Sync_Job_Duration();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewJob_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "StartingValue":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DoNotInvoiceClient")) == 1)
                        {
                            //e.Appearance.BackColor = Color.LightCoral;
                            //e.Appearance.BackColor2 = Color.Red;
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            //e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            //e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "DoNotPayContractor":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DoNotPayContractor")) == 1)
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            //e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            //e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "LinkedLabourCount":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedLabourCount")) <= 0)
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            //e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            //e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
            }
            /*if (e.Column.Name == "LinkedLabourCount")
             {
                 //e.DefaultDraw();
                 //if (Convert.ToInt32(e.CellValue) == 0) e.Graphics.DrawImage(bmpAlert, e.Bounds.Location);
                 if (Convert.ToInt32(e.CellValue) == 0) e.Graphics.DrawImage(bmpMissingTeam, new Point(e.Bounds.Location.X, e.Bounds.Location.Y + 1));
             }*/

        /*    if (e.Column.Name == "Alert")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedLabourCount")) <= 0)
                {
                    e.Graphics.DrawImage(bmpMissingTeam, new Point(e.Bounds.Location.X, e.Bounds.Location.Y + 1));
                }
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DoNotPayContractor")) == 1)
                {
                    e.Graphics.DrawImage(bmpDontBillTeam, new Point(e.Bounds.Location.X + 16, e.Bounds.Location.Y + 1));
                }
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DoNotInvoiceClient")) == 1)
                {
                    e.Graphics.DrawImage(bmpDontBillClient, new Point(e.Bounds.Location.X + 32, e.Bounds.Location.Y + 1));
                }
            }
            */
        }

        private void gridViewJob_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.ListSourceRowIndex < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "ExpectedDurationUnits":
                    {
                        string strMask = "";
                        try { strMask = view.GetRowCellValue(e.ListSourceRowIndex, "ExpectedDurationUnitsDescriptor").ToString() ?? ""; } catch (Exception) { }
                        e.DisplayText = string.Format("{0:N2} " + strMask, e.Value);
                        break;
                    }
                case "ActualDurationUnits":
                    {
                        string strMask = "";
                        try { strMask = view.GetRowCellValue(e.ListSourceRowIndex, "ActualDurationUnitsDescriptor").ToString() ?? ""; } catch (Exception) { }
                        //string strMask = view.GetRowCellValue(e.ListSourceRowIndex, "ActualDurationUnitsDescriptor").ToString() ?? "";
                        e.DisplayText = string.Format("{0:N2} " + strMask, e.Value);
                        break;
                    }
            }
        }

        private void gridViewJob_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "LinkedJobCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedJobCount")) <= 1) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedDocumentCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "DaysUntilDue":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DaysUntilDue")) >= 99999) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedPictureCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedPictureCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
       }

        private void gridViewJob_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "Alert" && e.IsGetData)
            {
                e.Value = bmpBlank;
                GridView view = (GridView)sender;
                DateTime dtEnd = DateTime.Now;
                int rHandle = (sender as GridView).GetRowHandle(e.ListSourceRowIndex);
                if (Convert.ToInt32(view.GetRowCellValue(rHandle, "LinkedLabourCount")) <= 0 || Convert.ToInt32(view.GetRowCellValue(rHandle, "JobStatusID")) < 20 || Convert.ToInt32(view.GetRowCellValue(rHandle, "DoNotPayContractor")) == 1 || Convert.ToInt32(view.GetRowCellValue(rHandle, "DoNotInvoiceClient")) == 1)
                {
                    e.Value = bmpAlert;
                }
            }
        }

        private void gridViewJob_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewJob_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;
            SetMenuStatus();
        }

        private void gridViewJob_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                bbiDatasetSelection.Enabled = (view.RowCount > 0);
                bsiDataset.Enabled = true;
                bbiDatasetSelectionInverted.Enabled = true;
                bbiDatasetManager.Enabled = true;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewJob_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            // Don't bind to generic event since this one has some custom code to hide the column chooser //
            GridView view = (GridView)sender;
            ExtendedGridMenu egmMenu = new ExtendedGridMenu(view);

            // Disable Column Chooser Customize //
            if (e.MenuType == GridMenuType.Column)
            {
                DXMenuItem miCustomize = GetItemByStringId(e.Menu, GridStringId.MenuColumnColumnCustomization);
                if (miCustomize != null) miCustomize.Enabled = iBool_EnableGridColumnChooser;
            }

            egmMenu.PopupMenuShowing(sender, e);
        }
        private DXMenuItem GetItemByStringId(DXPopupMenu menu, GridStringId id)
        {
            foreach (DXMenuItem item in menu.Items)
            {
                if (item.Caption == GridLocalizer.Active.GetLocalizedString(id)) return item;
            }
            return null;
        }

        private void gridViewJob_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedJobCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedJobCount")) <= 1) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) <= 0) e.Cancel = true;
                    break;
                case "DaysUntilDue":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("DaysUntilDue")) >= 99999) e.Cancel = true;
                    break;
                case "LinkedPictureCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedPictureCount")) <= 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedPictureCount":
                    {
                        Open_Picture_Viewer(view.GetRowCellValue(view.FocusedRowHandle, "JobID").ToString() + ",", Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID")), 1);
                    }
                    break;
                case "LinkedJobCount":
                    {                       
                        int intSiteContractID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SiteContractID"));
                        int intJobSubTypeID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "JobSubTypeID"));
                        if (intSiteContractID <= 0 || intJobSubTypeID <= 0) return;
                        string strRecordIDs = "";
                        try
                        {
                            var GetValue = new DataSet_OM_JobTableAdapters.QueriesTableAdapter();
                            GetValue.ChangeConnectionString(strConnectionString);
                            strRecordIDs = GetValue.sp06032_OM_Job_Manager_Get_Linked_Job_IDs(intSiteContractID.ToString() + ",", intJobSubTypeID.ToString() + ",").ToString();
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view linked jobs [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_job");
                    }
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentsJob_OpenLink_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 21;  // Job //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "JobID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "FullDescription").ToString();
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void Sync_Job_Duration()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControlJob.MainView;
            int intRowCount = view.DataRowCount;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Jobs to Sync the duration then try again.", "Sync Job Duration with Visit", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            StringBuilder sb = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobStatusID")) < 120)  // Job not completed //
                {
                    view.UnselectRow(intRowHandle);
                }
                else
                {
                    sb.Append(view.GetRowCellValue(intRowHandle, "JobID").ToString() + ",");
                }
            }
            if (string.IsNullOrWhiteSpace(sb.ToString()))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Jobs to Sync the duration then try again.\n\nNote: This process will de-select any job which has not been completed.", "Sync Job Duration with Visit", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (XtraMessageBox.Show("You are about to Sync Job Start and End Dates with Parent Visit(s).\n\nAre you sure you wish to proceed?\n\nNote: Only the Date part will be Synced, Times will be left as was.", "Sync Job Duration with Visit", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) != DialogResult.Yes) return;

            splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager.ShowWaitForm();
            splashScreenManager.SetWaitFormDescription("Syncing Job Dates...");

            // Update Visit Start And End Dates and any subsequent Visits (higher Visit Number than this one) //
            using (var UpdateRecords = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter())
            {
                UpdateRecords.ChangeConnectionString(strConnectionString);
                try
                {
                    UpdateRecords.sp06401_OM_Sync_Job_Time_To_Visit_Time(sb.ToString());
                }
                catch (Exception) { }
            }

            Load_Data();

            if (splashScreenManager.IsSplashFormVisible)
            {
                splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager.CloseWaitForm();
            }
        }

        #endregion


        #region GridView - Labour

        private void gridControlLabour_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlLabour.MainView;
                        int intRecordType = 22;  // Labour //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "LabourUsedID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "FullDescription").ToString() + ", Contractor: " + view.GetRowCellValue(view.FocusedRowHandle, "ContractorName").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription); 
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewLabour_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.ListSourceRowIndex < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "CostUnitsUsed":
                    {
                        string strMask = view.GetRowCellValue(e.ListSourceRowIndex, "CostUnitDescriptor").ToString();
                        e.DisplayText = string.Format("{0:N2} " + strMask, e.Value);
                        break;
                    }
                case "SellUnitsUsed":
                    {
                        string strMask = view.GetRowCellValue(e.ListSourceRowIndex, "SellUnitDescriptor").ToString();
                        e.DisplayText = string.Format("{0:N2} " + strMask, e.Value);
                        break;
                    }
            }
        }
        
        private void gridViewLabour_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewLabour_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Labour;
            SetMenuStatus();
        }

        private void gridViewLabour_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Labour;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiDataset.Enabled = (view.SelectedRowsCount > 0);
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewLabour_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "Email":
                        if (string.IsNullOrWhiteSpace(view.GetRowCellValue(e.RowHandle, "Email").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                    case "ViewRecord":
                        if (string.IsNullOrWhiteSpace(view.GetRowCellValue(e.RowHandle, "ViewRecord").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridViewLabour_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "Email":
                    if (string.IsNullOrWhiteSpace(view.GetFocusedRowCellValue("Email").ToString())) e.Cancel = true;
                    break;
                case "ViewRecord":
                    if (string.IsNullOrWhiteSpace(view.GetFocusedRowCellValue("ViewRecord").ToString())) e.Cancel = true;
                    break;
            }
        }

        private void repositoryItemHyperLinkEditEmail_OpenLink(object sender, OpenLinkEventArgs e)
        {
            try
            {
                GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
                string strEmailAddress = view.GetRowCellValue(view.FocusedRowHandle, "Email").ToString();
                string strCC = "";
                string strBCC = "";
                string strSubject = "";
                string strBody = "";
                string strAttachments = "";
                if (string.IsNullOrEmpty(strEmailAddress))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No email address stored - unable to proceed.", "Email Team \\ Staff", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                var url = "mailto:" + strEmailAddress
                                + (!string.IsNullOrWhiteSpace(strCC) ? "&cc=" + strCC : "")
                                + (!string.IsNullOrWhiteSpace(strBCC) ? "&bcc=" + strBCC : "")
                                + (!string.IsNullOrWhiteSpace(strSubject) ? "&subject=" + strSubject : "")
                                + (!string.IsNullOrWhiteSpace(strBody) ? "&body=" + strBody : "")
                                + (!string.IsNullOrWhiteSpace(strAttachments) ? "&Attach=" + strAttachments : "");
                System.Diagnostics.Process.Start(url);
            }
            catch (Exception ex) { }
        }

        private void repositoryItemHyperLinkEditViewRecord_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intContractorID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ContractorID"));
            if (intContractorID <= 0) return;
            string strRecordIDs = intContractorID.ToString() + ",";
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "contractors");
        }

        #endregion


        #region GridView - Equipment

        private void gridControlEquipment_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlEquipment.MainView;
                        int intRecordType = 23;  // Equipment //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "EquipmentUsedID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "FullDescription").ToString() + ", Equipment: " + view.GetRowCellValue(view.FocusedRowHandle, "EquipmentType").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    else if ("linked_pictures".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Picture Viewer //
                        GridView view = (GridView)gridControlEquipment.MainView;
                        Open_Picture_Viewer(view.GetRowCellValue(view.FocusedRowHandle, "EquipmentUsedID").ToString() + ",", Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID")), 5);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewEquipment_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.ListSourceRowIndex < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "CostUnitsUsed":
                    {
                        string strMask = view.GetRowCellValue(e.ListSourceRowIndex, "CostUnitDescriptor").ToString();
                        e.DisplayText = string.Format("{0:N2} " + strMask, e.Value);
                        break;
                    }
                case "SellUnitsUsed":
                    {
                        string strMask = view.GetRowCellValue(e.ListSourceRowIndex, "SellUnitDescriptor").ToString();
                        e.DisplayText = string.Format("{0:N2} " + strMask, e.Value);
                        break;
                    }
            }
        }

        private void gridViewEquipment_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "LinkedPictureCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedPictureCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridViewEquipment_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewEquipment_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Equipment;
            SetMenuStatus();
        }

        private void gridViewEquipment_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Equipment;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiDataset.Enabled = (view.SelectedRowsCount > 0);
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewEquipment_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedPictureCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedPictureCount")) <= 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditEquipmentPictures_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedPictureCount":
                    {
                        Open_Picture_Viewer(view.GetRowCellValue(view.FocusedRowHandle, "EquipmentUsedID").ToString() + ",", Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID")), 5);
                    }
                    break;
            }
        }

        #endregion


        #region GridView - Material

        private void gridControlMaterial_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlMaterial.MainView;
                        int intRecordType = 24;  // Material //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "MaterialUsedID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "FullDescription").ToString() + ", Material: " + view.GetRowCellValue(view.FocusedRowHandle, "MaterialName").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    else if ("linked_pictures".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Picture Viewer //
                        GridView view = (GridView)gridControlMaterial.MainView;
                        Open_Picture_Viewer(view.GetRowCellValue(view.FocusedRowHandle, "MaterialUsedID").ToString() + ",", Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID")), 4);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewMaterial_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.ListSourceRowIndex < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "CostUnitsUsed":
                    {
                        string strMask = view.GetRowCellValue(e.ListSourceRowIndex, "CostUnitDescriptor").ToString();
                        e.DisplayText = string.Format("{0:N2} " + strMask, e.Value);
                        break;
                    }
                case "SellUnitsUsed":
                    {
                        string strMask = view.GetRowCellValue(e.ListSourceRowIndex, "SellUnitDescriptor").ToString();
                        e.DisplayText = string.Format("{0:N2} " + strMask, e.Value);
                        break;
                    }
            }
        }

        private void gridViewMaterial_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "LinkedPictureCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedPictureCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }
        
        private void gridViewMaterial_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewMaterial_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Materials;
            SetMenuStatus();
        }

        private void gridViewMaterial_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Materials;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiDataset.Enabled = (view.SelectedRowsCount > 0);
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewMaterial_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedPictureCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedPictureCount")) <= 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditMaterialPictures_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedPictureCount":
                    {
                        Open_Picture_Viewer(view.GetRowCellValue(view.FocusedRowHandle, "MaterialUsedID").ToString() + ",", Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID")), 4);
                    }
                    break;
            }
        }

        #endregion



        #region GridView - Pictures

        private void gridControlPicture_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlPicture.MainView;
                        int intRecordType = 25;  // Pictures //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "PictureID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "LinkedRecordDescription").ToString() + ", Picture: " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "DateTimeTaken")).ToString("dd/MM/yyyy");
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewPicture_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "PicturePath":
                        if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "PicturePath").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridViewPicture_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewPicture_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Pictures;
            SetMenuStatus();
        }

        private void gridViewPicture_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Pictures;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiDataset.Enabled = (view.SelectedRowsCount > 0);
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView4_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "PicturePath":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("PicturePath").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PicturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture Linked - unable to proceed.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view picture: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #region GridView - Comments

        private void gridControlComment_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlComment.MainView;
                        int intRecordType = 28;  // Comments //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "CommentID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "FullDescription").ToString() + ", Comment Date: " + view.GetRowCellValue(view.FocusedRowHandle, "DateRecorded").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewComment_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewComment_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Comments;
            SetMenuStatus();
        }

        private void gridViewComment_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Comments;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiDataset.Enabled = (view.SelectedRowsCount > 0);
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView - Health And Safety

        private void gridControlHealthAndSafety_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlHealthAndSafety.MainView;
                        int intRecordType = 26;  // Health & Safety //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SafetyUsedID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "FullDescription").ToString() + ", Health & Safety Requirement: " + view.GetRowCellValue(view.FocusedRowHandle, "SafetyDescription").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewHealthAndSafety_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewHealthAndSafety_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.HealthAndSafety;
            SetMenuStatus();
        }

        private void gridViewHealthAndSafety_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.HealthAndSafety;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiDataset.Enabled = (view.SelectedRowsCount > 0);
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView - CRM Contacts

        private void gridControlCRM_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        /*// Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlBonuses.MainView;
                        int intRecordType = 11;  // Bonuses //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "BonusID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "BonusDueDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription); */
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewCRM_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewCRM_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.CRM;
            SetMenuStatus();
        }

        private void gridViewCRM_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.CRM;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiDataset.Enabled = (view.SelectedRowsCount > 0);
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView - Waste

        private void gridControlWaste_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlWaste.MainView;
                        int intRecordType = 29;  // Waste //
                        int intRecordSubType = 1;  // 0 = Visit, 1 = Job //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "WasteID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "FullDescription").ToString() + ", Waste: " + view.GetRowCellValue(view.FocusedRowHandle, "WasteType").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewWaste_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            try
            {
                if (e.ListSourceRowIndex < 0) return;
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "WasteAmount":
                        {
                            string strMask = view.GetRowCellValue(e.ListSourceRowIndex, "WasteAmountUnitDescriptor").ToString();
                            e.DisplayText = string.Format("{0:N2} " + strMask, e.Value);
                            break;
                        }
                }
            }
            catch (Exception) { }
        }

        private void gridViewWaste_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewWaste_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Waste;
            SetMenuStatus();
        }

        private void gridViewWaste_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Waste;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiDataset.Enabled = (view.SelectedRowsCount > 0);
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewWaste_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "WasteDocumentPath":
                    {
                        if (string.IsNullOrWhiteSpace(view.GetRowCellValue(e.RowHandle, "WasteDocumentPath").ToString())) e.RepositoryItem = emptyEditor;
                        break;
                    }
                case "WasteDocumentID":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "WasteDocumentID")) == 0) e.RepositoryItem = emptyEditor;
                        break;
                    }
                default:
                    break;
            }
        }

        private void gridViewWaste_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "WasteDocumentPath":
                    {
                        if (string.IsNullOrWhiteSpace(view.GetFocusedRowCellValue("WasteDocumentPath").ToString())) e.Cancel = true;
                        break;
                    }
                case "WasteDocumentID":
                    {
                        if (Convert.ToInt32(view.GetFocusedRowCellValue("WasteDocumentID")) == 0) e.Cancel = true;
                        break;
                    }
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditWasteDocument_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intWasteDocumentID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "WasteDocumentID"));
            if (intWasteDocumentID <= 0) return;

            switch (view.FocusedColumn.FieldName)
            {
                case "WasteDocumentPath":
                    {
                        string strFile = view.GetRowCellValue(view.FocusedRowHandle, "WasteDocumentPath").ToString();
                        string strExtension = view.GetRowCellValue(view.FocusedRowHandle, "WasteDocumentExtension").ToString();
                        if (string.IsNullOrWhiteSpace(strFile))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("No Waste Document Linked - unable to proceed.", "View Waste Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        try
                        {
                            if (strExtension.ToLower() != "pdf")
                            {
                                System.Diagnostics.Process.Start(strWasteDocumentPath + strFile);
                            }
                            else
                            {
                                if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                                {
                                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                                    fChildForm.strPDFFile = strWasteDocumentPath + strFile;
                                    fChildForm.MdiParent = this.MdiParent;
                                    fChildForm.GlobalSettings = this.GlobalSettings;
                                    fChildForm.Show();
                                }
                                else
                                {
                                    System.Diagnostics.Process.Start(strWasteDocumentPath + strFile);
                                }
                            }
                        }
                        catch
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the Linked Waste Document: " + strFile + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Waste Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
                case "WasteDocumentID":
                    {
                        string strRecordIDs = "";
                        try
                        {
                            strRecordIDs = intWasteDocumentID.ToString() + ",";
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view linked Waste Document [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Waste Document Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "waste_document");
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView - Spraying

        private void gridControlSpraying_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlSpraying.MainView;
                        int intRecordType = 30;  // Spraying //
                        int intRecordSubType = 1;  // 0 = Visit, 1 = Job //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SprayingID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "FullDescription").ToString() + ", Chemical: " + view.GetRowCellValue(view.FocusedRowHandle, "ChemicalName").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewSpraying_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.ListSourceRowIndex < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "AmountUsed":
                    {
                        string strMask = view.GetRowCellValue(e.ListSourceRowIndex, "AmountUsedDescriptor").ToString();
                        e.DisplayText = string.Format("{0:N2} " + strMask, e.Value);
                        break;
                    }
                case "AmountUsedDiluted":
                    {
                        string strMask = view.GetRowCellValue(e.ListSourceRowIndex, "AmountUsedDescriptor").ToString();
                        e.DisplayText = string.Format("{0:N2} " + strMask, e.Value);
                        break;
                    }
                default:
                    break;
            }
        }
        
        private void gridViewSpraying_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewSpraying_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Spraying;
            SetMenuStatus();
        }

        private void gridViewSpraying_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Spraying;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiDataset.Enabled = (view.SelectedRowsCount > 0);
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView - Extra Info

        private void gridControlExtraInfo_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlExtraInfo.MainView;
                        int intRecordType = 31;  // Extra Attributes //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "JobAttributeID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "FullDescription").ToString() + ", Attribute: " + view.GetRowCellValue(view.FocusedRowHandle, "OnScreenLabel").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    else if ("add extra attributes".Equals(e.Button.Tag))
                    {
                        GridView view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        int[] intRowHandles;
                        if (!iBool_AllowEdit) return;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length <= 0)
                        {
                            XtraMessageBox.Show("Select at least one job to re-create the Extra Attributes for before proceeding.", "Re-Create Job Extra Attributes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (XtraMessageBox.Show("WARNING... You are about to re-create the extra job attributes for " + intRowHandles.Length.ToString() + " Job(s).\n\nIf you proceed, any existing extra job attributes for the selected job(s) will be deleted first.", "Re-Create Job Extra Attributes", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
                        
                        if (!splashScreenManager.IsSplashFormVisible)
                        {
                            this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager.ShowWaitForm();
                            splashScreenManager.SetWaitFormDescription("Re-Creating Job Extra Info...");
                        }

                        try
                        {
                            using (var ReCreateRecords = new DataSet_OM_JobTableAdapters.QueriesTableAdapter())
                            {
                                ReCreateRecords.ChangeConnectionString(strConnectionString);
                            
                                foreach (int intRowHandle in intRowHandles)
                                {
                                    int intJobID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobID"));
                                    int intJobSubTypeID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                                    i_str_AddedRecordIDsExtraInfo += ReCreateRecords.sp06237_OM_Job_Clear_And_Re_Add_Extra_Attributes(intJobID, intJobSubTypeID).ToString() + ";";
                                }
                            }
                        }
                        catch (Exception) { }
                        LoadLinkedRecords();
                        if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewExtraInfo_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.ListSourceRowIndex < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "ValueRecorded":
                case "ValueRecordedText":
                    {
                        string strMask = view.GetRowCellValue(e.ListSourceRowIndex, "EditorMask").ToString();
                        if (!string.IsNullOrWhiteSpace(strMask) && !string.IsNullOrWhiteSpace(e.Value.ToString()))
                        {
                            //e.DisplayText = string.Format("{0:N2} " + strMask, e.Value);
                            try
                            {
                                e.DisplayText = string.Format(strMask, e.Value);
                            }
                            catch (Exception) { }
                        }
                        break;
                    }
            }
        }

        private void gridViewExtraInfo_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "ValueRecorded":
                    case "ValueRecordedText":
                        int intDataTypeID = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DataTypeID"));
                        int intEditorTypeID = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "EditorTypeID"));
                        string strValue = view.GetRowCellValue(e.RowHandle, "ValueRecordedText").ToString();
                        if (string.IsNullOrWhiteSpace(strValue))
                        {
                            e.RepositoryItem = emptyEditor;  // No value so don't bother with an editor //
                        }
                        else
                        {
                            switch (intDataTypeID)
                            {
                                case 1:  // Text Short //
                                    e.RepositoryItem = tempEditorTextEditText;
                                    break;
                                case 2:  // Text Long //
                                    e.RepositoryItem = tempEditorMemoExEdit;
                                    break;
                                case 3:  // Integer //
                                    if (intEditorTypeID == 4)  // Spinner //
                                    {
                                        e.RepositoryItem = tempEditorTextEditInteger;
                                    }
                                    else if (intEditorTypeID == 5)  //  Check Edit //
                                    {
                                        e.RepositoryItem = tempEditorCheckEdit;
                                    }
                                    else if (intEditorTypeID == 6)  // 4 = Picklist //
                                    {
                                        e.RepositoryItem = tempEditorTextEditText;
                                    }
                                    break;
                                case 4:  // Decimal //
                                    e.RepositoryItem = tempEditorTextEditDecimal;
                                    break;
                                case 5:  // Money //
                                    e.RepositoryItem = tempEditorTextEditCurrency;
                                    break;
                                case 6:  // DateTime Edit //
                                    e.RepositoryItem = tempEditorTextEditDateTime;
                                    break;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }
        
        private void gridViewExtraInfo_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewExtraInfo_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.ExtraInfo;
            SetMenuStatus();
        }

        private void gridViewExtraInfo_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.ExtraInfo;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiDataset.Enabled = (view.SelectedRowsCount > 0);
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewExtraInfo_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "ValueRecorded":
                case "ValueRecordedText":
                    int intDataTypeID = Convert.ToInt32(view.GetFocusedRowCellValue("DataTypeID"));
                    int intEditorTypeID = Convert.ToInt32(view.GetFocusedRowCellValue("EditorTypeID"));
                    if (intEditorTypeID != 3) e.Cancel = true;  // Memo Ex Edit //
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView - Labour Timing

        private void gridControlLabourTiming_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("blockadd".Equals(e.Button.Tag))
                    {
                        Block_Add_Labour_Timing();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewLabourTiming_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewLabourTiming_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.LabourTiming;
            SetMenuStatus();
        }

        private void gridViewLabourTiming_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.LabourTiming;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiDataset.Enabled = false;
                bbiDatasetCreate.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void Block_Add_Labour_Timing()
        {
            GridView view = (GridView)gridControlLabour.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Labour records to block add time on \\ off site to before proceeding.", "Block Add Time On \\ Off site", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strTempID = "";
            string strTeamIDs = ",";
            string strStaffIDs = ",";
            foreach (int intRowHandle in intRowHandles)
            {
                strTempID = view.GetRowCellValue(intRowHandle, view.Columns["ContractorID"]).ToString() + ',';
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["LinkedToPersonTypeID"])) == 1)
                {
                    if (!strTeamIDs.Contains("," + strTempID + ',')) strTeamIDs += strTempID + ",";
                }
                else
                {
                    if (!strStaffIDs.Contains("," + strTempID + ',')) strStaffIDs += strTempID + ",";
                }
            }
            if (strTeamIDs.Length > 0) strTeamIDs = strTeamIDs.Remove(0, 1);  // Remove preceeding comma //
            if (strStaffIDs.Length > 0) strStaffIDs = strStaffIDs.Remove(0, 1);  // Remove preceeding comma //

            var fChildForm = new frm_OM_Job_Labour_On_Off_Site_Block_Add();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strPassedInTeamIDs = strTeamIDs;
            fChildForm.strPassedInStaffIDs = strStaffIDs;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Block Adding...");
                
                GridView viewLabourTiming = (GridView)gridControlLabourTiming.MainView;
                GridView viewChild = (GridView)fChildForm.gridControl1.MainView;
                int intLabourUsedID = 0;
                int intLabourID = 0;
                int intLabourTypeID = 0;
                int intTeamMemberID = 0;
                DateTime dtStartDate = DateTime.MinValue;
                DateTime dtEndDate = DateTime.MinValue;
                decimal decDuration = (decimal)0.00;

                i_str_AddedRecordIDsLabourTiming = "";
                try
                {
                    foreach (int intRowHandle in intRowHandles)
                    {
                        intLabourUsedID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["LabourUsedID"]));
                        intLabourID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["ContractorID"]));
                        intLabourTypeID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["LinkedToPersonTypeID"]));
                        for (int i = 0; i < viewChild.DataRowCount; i++)
                        {
                            if (Convert.ToBoolean(viewChild.GetRowCellValue(i, "CheckMarkSelection")))
                            {
                                intTeamMemberID = Convert.ToInt32(viewChild.GetRowCellValue(i, viewChild.Columns["TeamMemberID"]));
                                dtStartDate = Convert.ToDateTime(viewChild.GetRowCellValue(i, viewChild.Columns["StartDate"]));
                                dtEndDate = Convert.ToDateTime(viewChild.GetRowCellValue(i, viewChild.Columns["EndDate"]));
                                decDuration = (decimal)viewChild.GetRowCellValue(i, viewChild.Columns["Duration"]);

                                using (var AddRecords = new DataSet_OM_JobTableAdapters.QueriesTableAdapter())
                                {
                                    AddRecords.ChangeConnectionString(strConnectionString);
                                    int intNewID = Convert.ToInt32(AddRecords.sp06400_OM_Block_Add_OM_Labour_Time_On_Off_Site(intLabourUsedID, intLabourID, intLabourTypeID, intTeamMemberID, dtStartDate, dtEndDate, decDuration));
                                    if (intNewID != 0) i_str_AddedRecordIDsLabourTiming += intNewID.ToString() + ";";
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while block adding...\n\nError: " + ex.Message, "Block Add Labour Time On \\ Off Site", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                    LoadLinkedRecords_LabourTimings();
                    viewLabourTiming.ExpandAllGroups();

                    if (splashScreenManager1.IsSplashFormVisible)
                    {
                        splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                        splashScreenManager1.CloseWaitForm();
                    }
                    return;
                }
                LoadLinkedRecords_LabourTimings();
                viewLabourTiming.ExpandAllGroups();

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show("Block Add Completed Successfully.", "Block Add Labour Time On \\ Off Site", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        #endregion


        #region DataSet

        public override void OnDatasetCreateEvent(object sender, EventArgs e)
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strCurrentID = "";
            string strSelectedIDs = ",";
            string strColumnName = "";
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:

                    view = (GridView)gridControlJob.MainView;
                    strColumnName = "JobID";
                    break;
                case Utils.enmFocusedGrid.Labour:
                    view = (GridView)gridControlLabour.MainView;
                    strColumnName = "JobID";
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    view = (GridView)gridControlEquipment.MainView;
                    strColumnName = "JobID";
                    break;
                case Utils.enmFocusedGrid.Materials:
                    view = (GridView)gridControlMaterial.MainView;
                    strColumnName = "JobID";
                    break;
                case Utils.enmFocusedGrid.Pictures:
                    view = (GridView)gridControlPicture.MainView;
                    strColumnName = "LinkedToRecordID";
                    break;
                case Utils.enmFocusedGrid.HealthAndSafety:
                    view = (GridView)gridControlHealthAndSafety.MainView;
                    strColumnName = "JobID";
                    break;
                case Utils.enmFocusedGrid.CRM:
                    view = (GridView)gridControlCRM.MainView;
                    strColumnName = "JobID";
                    break;
                case Utils.enmFocusedGrid.Comments:
                    view = (GridView)gridControlComment.MainView;
                    strColumnName = "JobID";
                    break;
                case Utils.enmFocusedGrid.Waste:
                    view = (GridView)gridControlWaste.MainView;
                    strColumnName = "LinkedToRecordID";
                    break;
                case Utils.enmFocusedGrid.Spraying:
                    view = (GridView)gridControlSpraying.MainView;
                    strColumnName = "LinkedToRecordID";
                    break;
                case Utils.enmFocusedGrid.ExtraInfo:
                    view = (GridView)gridControlExtraInfo.MainView;
                    strColumnName = "JobID";
                    break;
                default:
                    return;
            }
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            intCount = 0;
            foreach (int intRowHandle in intRowHandles)
            {
                strCurrentID = view.GetRowCellValue(intRowHandle, view.Columns[strColumnName]).ToString() + ',';
                if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                {
                    strSelectedIDs += strCurrentID;
                    intCount++;
                }
            }
            strSelectedIDs = strSelectedIDs.Remove(0, 1);  // Remove leading ',' //
            CreateDataset("Job", intCount, strSelectedIDs);
        }

        private void CreateDataset(string strType, int intRecordCount, string strSelectedRecordIDs)
        {
            frmDatasetCreate fChildForm = new frmDatasetCreate();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_dataset_type = strType;
            fChildForm.i_int_selected_job_count = intRecordCount;

            if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                int intAction = fChildForm.i_int_returned_action;
                string strName = fChildForm.i_str_dataset_name;
                strType = fChildForm.i_str_dataset_type;
                int intDatasetID = fChildForm.i_int_selected_dataset;
                string strDatasetType = fChildForm.i_str_dataset_type;
                int intReturnValue = 0;
                switch (intAction)
                {
                    case 1:  // Create New dataset and dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AddDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AddDataset.ChangeConnectionString(strConnectionString);

                        intReturnValue = Convert.ToInt32(AddDataset.sp01224_AT_Dataset_create(strDatasetType, strName, this.GlobalSettings.UserID, strSelectedRecordIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to create the dataset!\n\nNo Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 2:  // Append to dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AppendDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AppendDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(AppendDataset.sp01226_AT_Dataset_append_dataset_members(intDatasetID, strSelectedRecordIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to append the selected records to the dataset!\n\nNo Records Append to the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 3: // Replace dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter ReplaceDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        ReplaceDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(ReplaceDataset.sp01227_AT_Dataset_replace_dataset_members(intDatasetID, strSelectedRecordIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to replace the records in the selected dataset!\n\nNo Records Replaced in the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    default:
                        return;
                }
            }
        }

        public override void OnDatasetSelectionEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Job,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    DS_Selection1.SelectRecordsFromDataset(intSelectedDataset, "JobID");
                    break;
                default:
                    break;
            }
        }

        public override void OnDatasetSelectionInvertedEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Job,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    DS_Selection1.SelectRecordsFromDatasetInverted(intSelectedDataset, "JobID");
                    break;
                default:
                    break;
            }
        }

        public override void OnDatasetManagerEvent(object sender, EventArgs e)
        {
            var fChildForm = new frm_DatasetManager();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm._i_PassedInFilterTypes = "Job,";
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        #endregion


        #region Tab Pages Shown Panel

        private void btnShowTabPagesOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditShowTabPages_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditShowTabPages_Get_Selected();
        }

        private string PopupContainerEditShowTabPages_Get_Selected()
        {
            i_str_selected_TabPages = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl7.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_TabPages = "";
                foreach (XtraTabPage i in xtraTabControl1.TabPages)
                {
                    i.PageVisible = true;
                }
                return "All Linked Data";

            }
            else if (selection3.SelectedCount <= 0)
            {
                i_str_selected_TabPages = "";
                foreach (XtraTabPage i in xtraTabControl1.TabPages)
                {
                    i.PageVisible = true;
                }
                return "All Linked Data";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        //i_str_selected_CallOutType_ids += Convert.ToString(view.GetRowCellValue(i, "Value")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_TabPages = Convert.ToString(view.GetRowCellValue(i, "TabPageName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_TabPages += ", " + Convert.ToString(view.GetRowCellValue(i, "TabPageName"));
                        }
                        intCount++;
                    }
                }
            }

            foreach (XtraTabPage i in xtraTabControl1.TabPages)
            {
                string strText = i.Text;
                i.PageVisible = i_str_selected_TabPages.Contains(strText);
            }

            return i_str_selected_TabPages;
        }

        #endregion


        #region KAM Filter Panel

        private void popupContainerEditKAMFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditKAMFilter_Get_Selected();
        }

        private void btnKAMFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditKAMFilter_Get_Selected()
        {
            i_str_selected_KAM_ids = "";    // Reset any prior values first //
            i_str_selected_KAM_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_KAM_ids = "";
                return "All KAMs";

            }
            else if (selection5.SelectedCount <= 0)
            {
                i_str_selected_KAM_ids = "";
                return "All KAMs";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_KAM_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_KAM_names = Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_KAM_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_KAM_names;
        }

        #endregion


        #region CM Filter Panel

        private void popupContainerEditCMFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditCMFilter_Get_Selected();
        }

        private void btnCMFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditCMFilter_Get_Selected()
        {
            i_str_selected_CM_ids = "";    // Reset any prior values first //
            i_str_selected_CM_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl9.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_CM_ids = "";
                return "All CMs";

            }
            else if (selection9.SelectedCount <= 0)
            {
                i_str_selected_CM_ids = "";
                return "All CMs";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_CM_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_CM_names = Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_CM_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_CM_names;
        }

        #endregion


        #region Construction Manager Filter Panel

        private void popupContainerEditConstructionManagerFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditConstructionManagerFilter_Get_Selected();
        }

        private void btnConstructionManagerFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditConstructionManagerFilter_Get_Selected()
        {
            i_str_selected_ConstructionManager_ids = "";    // Reset any prior values first //
            i_str_selected_ConstructionManager_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl1.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_ConstructionManager_ids = "";
                return "All Construction Managers";

            }
            else if (selection15.SelectedCount <= 0)
            {
                i_str_selected_ConstructionManager_ids = "";
                return "All Construction Managers";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_ConstructionManager_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_ConstructionManager_names = Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_ConstructionManager_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_ConstructionManager_names;
        }

        #endregion


        #region Job Status Filter Panel

        private void btnJobStatusFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            //i_int_FocusedGrid = 3;
            //SetMenuStatus();
        }

        private void popupContainerEditJobStatusFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditJobStatusFilter_Get_Selected();
        }

        private string PopupContainerEditJobStatusFilter_Get_Selected()
        {
            i_str_selected_JobStatus_ids = "";    // Reset any prior values first //
            i_str_selected_JobStatus_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl3.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_JobStatus_ids = "";
                return "No Job Status Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_JobStatus_ids = "";
                return "No Job Status Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_JobStatus_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_JobStatus_names = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_JobStatus_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_JobStatus_names;
        }

        #endregion


        #region Job Type Filter Panel

        private void btnJobTypeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditJobTypeFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditJobTypeFilter_Get_Selected();
        }

        private string PopupContainerEditJobTypeFilter_Get_Selected()
        {
            i_str_selected_JobType_ids = "";    // Reset any prior values first //
            i_str_selected_JobType_descriptions = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl10.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_JobType_ids = "";
                return "No Job Type Filter";

            }
            else if (selection10.SelectedCount <= 0)
            {
                i_str_selected_JobType_ids = "";
                return "No Job Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_JobType_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_JobType_descriptions = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_JobType_descriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_JobType_descriptions;
        }

        #endregion


        #region Job Sub-Type Filter Panel

        private void btnJobSubTypeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditJobSubTypeFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditJobSubTypeFilter_Get_Selected();
        }

        private string PopupContainerEditJobSubTypeFilter_Get_Selected()
        {
            i_str_selected_JobSubType_ids = "";    // Reset any prior values first //
            i_str_selected_JobSubType_descriptions = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl11.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_JobSubType_ids = "";
                return "No Job Sub-Type Filter";

            }
            else if (selection11.SelectedCount <= 0)
            {
                i_str_selected_JobSubType_ids = "";
                return "No Job Sub-Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_JobSubType_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_JobSubType_descriptions = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_JobSubType_descriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_JobSubType_descriptions;
        }

        #endregion


        #region Callout Date Range Filter Panel

        private void btnDateRangeOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            string strValue = "";
            if (dateEditFromDate.DateTime == DateTime.MinValue)
            {
                i_dtStart = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtStart = dateEditFromDate.DateTime;
            }
            if (dateEditToDate.DateTime == DateTime.MinValue)
            {
                i_dtEnd = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtEnd = dateEditToDate.DateTime;
            }
            if (dateEditFromDate.DateTime == DateTime.MinValue && dateEditToDate.DateTime == DateTime.MinValue)
            {
                strValue = "No Date Range Filter";
            }
            else
            {
                strValue = (i_dtStart == new DateTime(1900, 1, 1) ? "No Start" : i_dtStart.ToString("dd/MM/yyyy HH:mm")) + " - " + (i_dtEnd == new DateTime(1900, 1, 1) ? "No End" : i_dtEnd.ToString("dd/MM/yyyy HH:mm"));
            }
            return strValue;
        }

        #endregion


        #region Record Type Filter Panel

        private void btnRecordTypeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditRecordTypeFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditRecordTypeFilter_Get_Selected();
        }

        private string PopupContainerEditRecordTypeFilter_Get_Selected()
        {
            i_str_selected_RecordType_ids = "";    // Reset any prior values first //
            i_str_selected_RecordType_descriptions = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl8.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_RecordType_ids = "";
                return "No Record Type Filter";

            }
            else if (selection8.SelectedCount <= 0)
            {
                i_str_selected_RecordType_ids = "";
                return "No Record Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_RecordType_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_RecordType_descriptions = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_RecordType_descriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_RecordType_descriptions;
        }

        #endregion


        #region Labour Filter Panel

        private void btnLabourFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditLabourFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditLabourFilter_Get_Selected();
        }

        private string PopupContainerEditLabourFilter_Get_Selected()
        {
            i_str_selected_Labour_Team_ids = "";    // Reset any prior values first //
            i_str_selected_Labour_Team_descriptions = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl12.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_Labour_Team_ids = "";
                return "No Labour Filter";

            }
            else if (selection12.SelectedCount <= 0)
            {
                i_str_selected_Labour_Team_ids = "";
                return "No Labour Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_Labour_Team_ids += Convert.ToInt32(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_Labour_Team_descriptions = Convert.ToString(view.GetRowCellValue(i, "Name"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_Labour_Team_descriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "Name"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_Labour_Team_descriptions;
        }

        #endregion


        #region Equipment Type Filter Panel

        private void btnEquipmentTypeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditEquipmentTypeFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditEquipmentTypeFilter_Get_Selected();
        }

        private string PopupContainerEditEquipmentTypeFilter_Get_Selected()
        {
            i_str_selected_EquipmentType_ids = "";    // Reset any prior values first //
            i_str_selected_EquipmentType_descriptions = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl13.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_EquipmentType_ids = "";
                return "No Equipment Type Filter";

            }
            else if (selection13.SelectedCount <= 0)
            {
                i_str_selected_EquipmentType_ids = "";
                return "No Equipment Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_EquipmentType_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_EquipmentType_descriptions = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_EquipmentType_descriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_EquipmentType_descriptions;
        }

        #endregion


        #region Material Type Filter Panel

        private void btnMaterialTypeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditMaterialTypeFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditMaterialTypeFilter_Get_Selected();
        }

        private string PopupContainerEditMaterialTypeFilter_Get_Selected()
        {
            i_str_selected_MaterialType_ids = "";    // Reset any prior values first //
            i_str_selected_MaterialType_descriptions = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl14.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_MaterialType_ids = "";
                return "No Material Filter";

            }
            else if (selection14.SelectedCount <= 0)
            {
                i_str_selected_MaterialType_ids = "";
                return "No Material Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_MaterialType_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_MaterialType_descriptions = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_MaterialType_descriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_MaterialType_descriptions;
        }

        #endregion


        #region Visit Type Filter Panel

        private void btnVisitTypeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditVisitTypeFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditVisitTypeFilter_Get_Selected();
        }

        private string PopupContainerEditVisitTypeFilter_Get_Selected()
        {
            i_str_selected_VisitType_ids = "";    // Reset any prior values first //
            i_str_selected_VisitTypes = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_VisitType_ids = "";
                return "No Visit Type Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_VisitType_ids = "";
                return "No Visit Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_VisitType_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_VisitTypes = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_VisitTypes += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_VisitTypes;
        }

        #endregion


        private void buttonEditClientFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_client_ids = "";
                i_str_selected_client_names = "";
                buttonEditClientFilter.Text = "";
                
                // Update Filter control's tooltip //
                superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\nNo Filter Set.";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 0;
                fChildForm.intMustSelectChildren = 0;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intFilterSummerMaintenanceClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter.Text = i_str_selected_client_names;

                    // Update Filter control's tooltip //
                    string strTooltipText = i_str_selected_client_names.Replace(", ", "\n");
                    superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;

                    if (!string.IsNullOrWhiteSpace(fChildForm.strSelectedChildIDs))
                    {
                        i_str_selected_site_ids = fChildForm.strSelectedChildIDs;
                        i_str_selected_site_names = fChildForm.strSelectedChildDescriptions;
                        buttonEditSiteFilter.Text = i_str_selected_site_names;

                        // Update Filter control's tooltip //
                        strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                    }
                    else
                    {
                        // Update Filter control's tooltip //
                        strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                        superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
                    }

                }
            }
        }

        private void buttonEditSiteFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "clear")
            {
                i_str_selected_site_ids = "";
                i_str_selected_site_names = "";
                buttonEditSiteFilter.Text = "";

                // Update Filter control's tooltip //
                superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";
            }
            else if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_Core_Select_Client_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAllowChildSelection = 1;
                fChildForm.intMustSelectChildren = 1;
                fChildForm.strPassedInParentIDs = i_str_selected_client_ids;
                fChildForm.strPassedInChildIDs = i_str_selected_site_ids;
                fChildForm.i_dtStart = DateTime.Today.AddMonths(-12);  // back a year //
                fChildForm.i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
                fChildForm.intFilterSummerMaintenanceClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_selected_client_ids = fChildForm.strSelectedParentIDs;
                    i_str_selected_client_names = fChildForm.strSelectedParentDescriptions;
                    buttonEditClientFilter.Text = i_str_selected_client_names;

                    // Update Filter control's tooltip //
                    string strTooltipText = i_str_selected_client_names.Replace(", ", "\n");
                    superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\n" + strTooltipText;

                    i_str_selected_site_ids = fChildForm.strSelectedChildIDs;
                    i_str_selected_site_names = fChildForm.strSelectedChildDescriptions;
                    buttonEditSiteFilter.Text = i_str_selected_site_names;

                    // Update Filter control's tooltip //
                    strTooltipText = i_str_selected_site_names.Replace(", ", "\n");
                    superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\n" + strTooltipText;
                }
            }
        }


        private void checkEditColourCode_CheckedChanged(object sender, EventArgs e)
        {
            SetGridFormatConditions();
        }

        private void SetGridFormatConditions()
        {
            /*CheckEdit ce = (CheckEdit)checkEditColourCode;
            if (ce.Checked)
            {
                StyleFormatCondition condition1 = new DevExpress.XtraGrid.StyleFormatCondition();
                condition1.Appearance.BackColor = Color.FromArgb(255, 192, 192);
                condition1.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                condition1.Appearance.Options.UseBackColor = true;
                condition1.Appearance.ForeColor = Color.Black;
                condition1.ApplyToRow = true;
                condition1.Condition = FormatConditionEnum.Expression;
                condition1.Expression = "[JobStatusID] == 20";
                gridView1.FormatConditions.Add(condition1);

                StyleFormatCondition condition2 = new DevExpress.XtraGrid.StyleFormatCondition();
                condition2.Appearance.BackColor = Color.FromArgb(255, 192, 192);
                condition2.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                condition2.Appearance.Options.UseBackColor = true;
                condition2.Appearance.ForeColor = Color.Black;
                condition2.ApplyToRow = true;
                condition2.Condition = FormatConditionEnum.Expression;
                condition2.Expression = "[JobStatusID] == 40";
                gridView1.FormatConditions.Add(condition2);

                StyleFormatCondition condition3 = new DevExpress.XtraGrid.StyleFormatCondition();
                condition3.Appearance.BackColor = Color.FromArgb(255, 192, 192);
                condition3.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                condition3.Appearance.Options.UseBackColor = true;
                condition3.Appearance.ForeColor = Color.Black;
                condition3.ApplyToRow = true;
                condition3.Condition = FormatConditionEnum.Expression;
                condition3.Expression = "[JobStatusID] == 60";
                gridView1.FormatConditions.Add(condition3);


                StyleFormatCondition condition4 = new DevExpress.XtraGrid.StyleFormatCondition();
                condition4.Appearance.BackColor = Color.FromArgb(255, 255, 128);
                condition4.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                condition4.Appearance.Options.UseBackColor = true;
                condition4.Appearance.ForeColor = Color.Black;
                condition4.ApplyToRow = true;
                condition4.Condition = FormatConditionEnum.Expression;
                condition4.Expression = "[JobStatusID] == 10";
                gridView1.FormatConditions.Add(condition4);

                StyleFormatCondition condition5 = new DevExpress.XtraGrid.StyleFormatCondition();
                condition5.Appearance.BackColor = Color.FromArgb(255, 255, 128);
                condition5.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                condition5.Appearance.Options.UseBackColor = true;
                condition5.Appearance.ForeColor = Color.Black;
                condition5.ApplyToRow = true;
                condition5.Condition = FormatConditionEnum.Expression;
                condition5.Expression = "[JobStatusID] == 30";
                gridView1.FormatConditions.Add(condition5);


                StyleFormatCondition condition6 = new DevExpress.XtraGrid.StyleFormatCondition();
                condition6.Appearance.BackColor = Color.FromArgb(192, 255, 192);
                condition6.Appearance.BackColor2 = Color.FromArgb(255, 255, 255);
                condition6.Appearance.Options.UseBackColor = true;
                condition6.Appearance.ForeColor = Color.Black;
                condition6.ApplyToRow = true;
                condition6.Condition = FormatConditionEnum.Expression;
                condition6.Expression = "[JobStatusID] == 50";
                gridView1.FormatConditions.Add(condition6);
            }
            else
            {
                gridView1.FormatConditions.Clear();
            }
            */
        }

        private void splitContainerControl1_SplitGroupPanelCollapsed(object sender, SplitGroupPanelCollapsedEventArgs e)
        {
            if (!e.Collapsed)
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLinkedRecords();
            }
        }


        #region Buttons

        private void bbiAddWizard_ItemClick(object sender, ItemClickEventArgs e)
        {
            var fChildForm = new frm_OM_Job_Wizard();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = "";
            fChildForm.strFormMode = "add";
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = 0;
            fChildForm.i_str_PassedInClientContractIDs = "";  // No passed in Client Contract ID //
            fChildForm.i_str_PassedInSiteContractIDs = "";  // No passed in Site Contract ID //
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void bbiViewSchedule_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void bbiSendSchedule_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void bbiViewSiteOnMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlJob.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Jobs to show their parent Site on the map before proceeding.", "View Job Sites on Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedIDs = "";
            string strClientIDs = ",";
            string strSiteIDs = ",";
            string strTempClientID = "";
            string strTempSiteID = "";
            string strSelectedFullSiteDescriptions = "";
            foreach (int intRowHandle in intRowHandles)
            {
                //strSelectedIDs += view.GetRowCellValue(intRowHandle, view.Columns["VisitID"]).ToString() + ',';
                strTempClientID = view.GetRowCellValue(intRowHandle, view.Columns["ClientID"]).ToString();
                strTempSiteID = view.GetRowCellValue(intRowHandle, view.Columns["SiteID"]).ToString();
                if (!strClientIDs.Contains("," + strTempClientID + ',')) strClientIDs += strTempClientID + ",";
                if (!strSiteIDs.Contains("," + strTempSiteID + ','))
                {
                    strSiteIDs += strTempSiteID + ",";
                    strSelectedFullSiteDescriptions += "Client: " + Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientName")) + ", Site: " + Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteName")) + " | ";  // Pipe used as deimiter //
                }
            }
            if (strClientIDs.Length > 0) strClientIDs = strClientIDs.Remove(0, 1);  // Remove preceeding comma //
            if (strSiteIDs.Length > 0) strSiteIDs = strSiteIDs.Remove(0, 1);  // Remove preceeding comma //

            var fChildForm = new frm_OM_Visit_Mapping_View();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_PassedInVisitIDs = strSelectedIDs;
            fChildForm.i_str_PassedInSiteIDs = strSiteIDs;
            fChildForm.i_str_PassedInClientIDs = strClientIDs;
            fChildForm.i_str_selected_Site_descriptions = strSelectedFullSiteDescriptions;
            fChildForm.i_bool_LoadVisitsInMapOnStart = false;
            fChildForm.i_bool_LoadSitesInMapOnStart = true;
            fChildForm.strFormMode = "view";
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void bbiViewJobOnMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlJob.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Jobs to show their start and end locations on the map before proceeding.", "View Jobs on Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedIDs = "";
            string strClientIDs = ",";
            string strSiteIDs = ",";
            string strVisitIDs = ",";
            string strTempClientID = "";
            string strTempSiteID = "";
            string strTempVisitID = "";
            string strSelectedFullSiteDescriptions = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += view.GetRowCellValue(intRowHandle, view.Columns["JobID"]).ToString() + ',';
                strTempClientID = view.GetRowCellValue(intRowHandle, view.Columns["ClientID"]).ToString();
                strTempSiteID = view.GetRowCellValue(intRowHandle, view.Columns["SiteID"]).ToString();
                strTempVisitID = view.GetRowCellValue(intRowHandle, view.Columns["VisitID"]).ToString();
                if (!strClientIDs.Contains("," + strTempClientID + ',')) strClientIDs += strTempClientID + ",";
                if (!strSiteIDs.Contains("," + strTempSiteID + ','))
                {
                    strSiteIDs += strTempSiteID + ",";
                    strSelectedFullSiteDescriptions += "Client: " + Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientName")) + ", Site: " + Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteName")) + " | ";  // Pipe used as deimiter //
                }
                if (!strVisitIDs.Contains("," + strTempVisitID + ',')) strVisitIDs += strTempVisitID + ",";
            }
            if (strClientIDs.Length > 0) strClientIDs = strClientIDs.Remove(0, 1);  // Remove preceeding comma //
            if (strSiteIDs.Length > 0) strSiteIDs = strSiteIDs.Remove(0, 1);  // Remove preceeding comma //
            if (strVisitIDs.Length > 0) strVisitIDs = strVisitIDs.Remove(0, 1);  // Remove preceeding comma //

            var fChildForm = new frm_OM_Visit_Mapping_View();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_PassedInJobIDs = strSelectedIDs;
            fChildForm.i_str_PassedInVisitIDs = strVisitIDs;
            fChildForm.i_str_PassedInSiteIDs = strSiteIDs;
            fChildForm.i_str_PassedInClientIDs = strClientIDs;
            fChildForm.i_str_selected_Site_descriptions = strSelectedFullSiteDescriptions;
            fChildForm.i_bool_LoadVisitsInMapOnStart = false;
            fChildForm.i_bool_LoadSitesInMapOnStart = false;
            fChildForm.i_bool_LoadJobsInMapOnStart = true;
            fChildForm.strFormMode = "view";
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }
      
        #endregion


        private void bbiSelectJobsReadyToSend_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Selecting Jobs...");
            }

            // Select all jobs with a Status of 10 or 20 (Ready to send) //
            GridView view = (GridView)gridControlJob.MainView;
            view.ClearSelection();
             
            view.BeginUpdate();
            view.BeginSelection();

            for (int i = 0; i < view.DataRowCount; i++)
            {
                if ((Convert.ToInt32(view.GetRowCellValue(i, "JobStatusID")) == 10 || Convert.ToInt32(view.GetRowCellValue(i, "JobStatusID")) == 20) && Convert.ToInt32(view.GetRowCellValue(i, "LinkedLabourCount")) > 0 && Convert.ToInt32(view.GetRowCellValue(i, "DaysUntilDue")) <= 7)
                {
                    view.SelectRow(i);
                    continue;
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void bbiSendJobs_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlJob.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Jobs to send before proceeding.", "Send Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intFutureJobs = 0;
            int intGoodJobs = 0;

            view.BeginUpdate();
            view.BeginSelection();
            for (int i = 0; i < intRowHandles.Length; i++)
            {
                if (!(Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID")) == 10 || Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID")) == 20))
                {
                    view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to send //
                    continue;
                }
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "LinkedLabourCount")) <= 0)
                {
                    view.UnselectRow(intRowHandles[i]);  // De-select job as it has no labour //
                    continue;
                }
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "DaysUntilDue")) > 7)
                {
                    //view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to send [Missing SiteGrittingContractID] //
                    //continue;
                    intFutureJobs++;
                }
                else
                {
                    intGoodJobs++;
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Jobs to send before proceeding.\n\nNote: This process de-selects any selected jobs which are not ready to send...\n\nJobs must have:-\n==> Job Status < Sent\n==> Linked Labour Team", "Send Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (intFutureJobs > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have selected...\n\n " + intGoodJobs.ToString() + " job(s) due within 7 days.\n\n" + intFutureJobs.ToString() + " Job(s) which are more than 7 days into the future - You can still send these future jobs.\n\nAre you sure you wish to proceed with sending?", "Send Jobs", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    return;
                }
            }
            else
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have selected " + intGoodJobs.ToString() + " Job(s) - are you sure you wish to proceed with sending?", "Send Jobs", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    return;
                }
            }

            // Good to proceed //
            this.RefreshGridViewStateJob.SaveViewInfo();  // Store Grid View State //
            
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Sending Job(s)...");
            }

            string strJobIDs = "";
            int intSentCount = 0;
            foreach (int intRowHandle in intRowHandles)
            {
                strJobIDs += view.GetRowCellValue(intRowHandle, "JobID").ToString() + ",";
                intSentCount++;
            }
            try
            {
                DataSet_OM_JobTableAdapters.QueriesTableAdapter SendJobs = new DataSet_OM_JobTableAdapters.QueriesTableAdapter();
                SendJobs.ChangeConnectionString(strConnectionString);
                SendJobs.sp06266_OM_Send_Jobs(strJobIDs);
            }
            catch (Exception ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while sending the Jobs.\n\nError: " + ex.Message+"\n\nPlease try again. If the problem persists, contact Technical Support.", "Send Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            Load_Data();
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
            string strMessage = (intSentCount == 1 ? "1 Job" : intSentCount.ToString() + " Jobs") + " Sent to Team(s) Successfully.";
            DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Send Jobs", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void bbiReassignJobs_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!iBool_AllowEdit) return;

            // First... Check at least 1 job is selected //
            GridView view = (GridView)gridControlJob.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Jobs to Reassign Labour to before proceeding.", "Reassign Job Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Second... Check Status of selected jobs are valid for changing the selected team - ie they haven't been accepted or paid etc - de-select any failing criteria //
            splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager.ShowWaitForm();
            splashScreenManager.SetWaitFormDescription("Checking Selection...");

            view.BeginUpdate();
            view.BeginSelection();
            int intJobStatusID = 0;
            for (int i = 0; i < intRowHandles.Length; i++)
            {

                intJobStatusID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID"));
                //if (!(intJobStatusID < 40 || intJobStatusID > 70)) view.UnselectRow(intRowHandles[i]);
                //if (!(intJobStatusID < 40 || intJobStatusID == 40 || intJobStatusID == 50 || intJobStatusID == 60 || intJobStatusID == 70 || intJobStatusID == 90 || intJobStatusID == 100 || intJobStatusID == 110))
                if (intJobStatusID == 80 || intJobStatusID == 120)  // Started or Completed //
                {
                    view.UnselectRow(intRowHandles[i]);
                }
                else if (Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "LinkedLabourCount")) <= 0)
                {
                    view.UnselectRow(intRowHandles[i]);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (splashScreenManager.IsSplashFormVisible)
            {
                splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager.CloseWaitForm();
            }

            // Third... Re-check we still have selected jobs //
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Jobs to Reassign before proceeding.\n\nNote: This process only allows jobs not yet sent or jobs sent but rejected by the team to be re-assigned. Any other jobs are automatically de-selected.", "Reassign Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strRecordIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
            }

            var fChildForm = new frm_OM_Job_Labour_Reassign();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strFormMode = "edit";
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm._PassedInJobIDs = strRecordIDs;

            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
            return;
        }        


        private void bbiAuthorise_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frm_GC_Team_Self_Billing_Invoicing frmInstance = new frm_GC_Team_Self_Billing_Invoicing();

            frmProgress fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            System.Windows.Forms.Application.DoEvents();

            frmInstance.MdiParent = this.MdiParent;
            frmInstance.GlobalSettings = this.GlobalSettings;
            frmInstance.fProgress = fProgress;
            frmInstance.Show();

            MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(frmInstance, new object[] { null });
        
            
            //net.textapp.www.Service mySms = new net.textapp.www.Service();
            //string strReply = mySms.GetSMSInbound(true, "BU048984", "steps", "+447537402046", "");

            /*
            string[] lines = System.IO.File.ReadAllLines(@"C:\Mark_Development\Ground_Control_Documents\SMS_Return2.txt");
            if (lines.Length < 2) return;  // Nothing returned yet //
            char[] delimiters = new char[] { ',' };
  
            char[] delimiters2 = new char[] { ' ' };
            char[] arr = new char[] { '"' };
            DateTime dtDateTime = DateTime.Today;
            int intSiteID = 0;
            decimal decSaltUsed = (decimal)0.00;
            for (int i = 1; i < lines.Length; i++)  // Skip Row 0 as this is the Transaction Code from TextAnywhere //
            {
//eventLog1.WriteEntry("Job Completion - " + strReply, EventLogEntryType.Information);
                string[] strArray = lines[i].Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray.Length < 8) continue;  // Skip row and move to next column - cant flag as error as we don't have enought columns //

                dtDateTime = DateTime.Today;
                if (DateTime.TryParse(strArray[4].TrimStart(arr).TrimEnd(arr).Trim() + " " + strArray[5].TrimStart(arr).TrimEnd(arr).Trim(), out dtDateTime))
                {
                    if (dtDateTime == Convert.ToDateTime("01/01/0001"))  // Error - invalid date so use todays with no time so it's obvious //
                    {
                        dtDateTime = DateTime.Today;
                    }
                }

                string strTelephone = strArray[1].TrimStart(arr).TrimEnd(arr).Trim();
                string strBody = strArray[6].TrimStart(arr).TrimEnd(arr).Trim();
                strBody = strArray[6].TrimStart(arr).TrimEnd(arr);
                string[] strArray2 = strBody.Split(delimiters2, StringSplitOptions.RemoveEmptyEntries);
                string strAction = "";
                decSaltUsed = (decimal)0.00;

                if (strArray2.Length < 2)  // Error - invalid number of columns so flag as an error //
                {
                    //StoreError2(SQLConn, strTelephone, dtDateTime, strBody, 0, (decimal)0.00, 1, "Wrong number of parameters");
                    continue;
                }
                if (!int.TryParse(strArray2[0].TrimStart(arr).Trim(), out intSiteID))  // Error - non numeric value so flag as an error //
                {
                    //StoreError2(SQLConn, strTelephone, dtDateTime, strBody, 0, (decimal)0.00, 1, "Non-numeric Site ID");
                    continue;
                }
                if (strArray2[1].ToUpper().TrimEnd(arr).Trim() == "ABORTED")
                {
                    strAction = "Aborted";
                }
                else if (strArray2[1].ToUpper().TrimEnd(arr).Trim() == "NO" && strArray2.Length == 3)
                {
                    if (strArray2[2].ToUpper().TrimEnd(arr).Trim() == "ACCESS")
                    {
                        //StoreError2(SQLConn, strTelephone, dtDateTime, strBody, intSiteID, (decimal)0.00, 1, "Wrong\\Invalid number of parameters");
                        continue;
                    }
                    strAction = "No Access";
                }
                else
                {
                    strAction = "Completed"; 
                    if (!decimal.TryParse(strArray2[1].TrimEnd(arr).Trim(), out decSaltUsed))  // Error - non numeric value so flag as an error //
                    {
                        //StoreError2(SQLConn, strTelephone, dtDateTime, strBody, intSiteID, (decimal)0.00, 1, "Non-numeric Salt Value");
                        continue;
                    }
                }
                // If we are here attempt to update database - Note the Update SP will create an error record if it can't find an appropriate callout for the site //
               
                continue;
            }*/
            
            /*
            
            net.textapp.www.Service mySms = new net.textapp.www.Service();
            string strReply = mySms.GetSMSReply(true, "BU048984", "steps", "1");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "2");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "3");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "4");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "5");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "6");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "7");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "8");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "9");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "10");
            strReply = mySms.GetSMSReply(true, "BU048984", "steps", "11");


            string[] lines = System.IO.File.ReadAllLines(@"C:\Mark_Development\Ground_Control_Documents\SMS_Return.txt");
            if (lines.Length < 2) return;  // Nothing returned yet //
            string strMessageResponse = lines[1];
            char[] delimiters = new char[] { ',' };
            string[] strArray;
            strArray = strMessageResponse.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length < 4) return;  // Error //
            string strMessageResponseColumns = strArray[4];
            int intLinkedJobCount= 0;

            
                // Parse Reply //
                delimiters = new char[] { ' ' };
                string[] strArray2;
                strArray2 = strMessageResponseColumns.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

                char[] arr = new char[] { '"' };
                if (strArray2.Length <= 1 || strArray == null) // Check for Rejection first //
                {
                    if (strMessageResponseColumns.ToUpper().TrimStart(arr).TrimEnd(arr) != "NO")  // Error - not = OK //
                    {
                        return;
                   }
                    // Value = NO so get Date and Time of acceptance //
                    DateTime dtResponded = DateTime.Today;
                    delimiters = new char[] { ',' };
                    strMessageResponse = lines[lines.Length - 1];
                    string[] strArray3;
                    strArray3 = strMessageResponse.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    if (strArray3.Length > 2)
                    {
                        string strDate = strArray3[strArray3.Length - 2];
                        string strTime = strArray3[strArray3.Length - 1];

                        if (DateTime.TryParse(strDate.TrimStart(arr).TrimEnd(arr) + " " + strTime.TrimStart(arr).TrimEnd(arr) + ":00", out dtResponded))  // Error - invalid date completed so flag as an error //
                        {
                            if (dtResponded == Convert.ToDateTime("01/01/0001"))  // Error - invalid date completed so flag as an error //
                            {
                                dtResponded = DateTime.Today;
                            }
                        }
                    }
                    // Checks Passed so Set Job as rejected //
                }
                else  // Try for acceptance //
                {
                    if (strArray2.Length != 2)  // Error - invalid number of columns so flag as an error //
                    {
                         return;
                    }
                    // Check for Acceptance //
                    int intSites = 0;
                    if (!int.TryParse(strArray2[0].TrimStart(arr), out intSites))  // Error - non numeric value so flag as an error //
                    {
                        return;
                    }
                    if (strArray2[1].ToUpper().TrimEnd(arr) != "OK")  // Error - not = OK //
                    {
                         return;
                    }
                    if (intSites < intLinkedJobCount)  // Invalid number of jobs entered //
                    {
                        return;
                    }
                    // Get Date and Time of acceptance //
                    DateTime dtResponded = DateTime.Today;
                    delimiters = new char[] { ',' };
                    strMessageResponse = lines[lines.Length - 1];
                    string[] strArray3;
                    strArray3 = strMessageResponse.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    if (strArray3.Length > 2)
                    {
                        string strDate = strArray3[strArray3.Length - 2];
                        string strTime = strArray3[strArray3.Length - 1];

                        if (DateTime.TryParse(strDate.TrimStart(arr).TrimEnd(arr) + " " + strTime.TrimStart(arr).TrimEnd(arr) + ":00", out dtResponded))  // Error - invalid date completed so flag as an error //
                        {
                            if (dtResponded == Convert.ToDateTime("01/01/0001"))  // Error - invalid date completed so flag as an error //
                            {
                                dtResponded = DateTime.Today;
                            }
                        }
                    }
                    // Checks Passed so Update text message and callout //
                 }

            */
    
        }

        private void bbiPay_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frm_GC_Finance_Invoice_Jobs frmInstance = new frm_GC_Finance_Invoice_Jobs();

            frmProgress fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            System.Windows.Forms.Application.DoEvents();

            frmInstance.MdiParent = this.MdiParent;
            frmInstance.GlobalSettings = this.GlobalSettings;
            frmInstance.fProgress = fProgress;
            frmInstance.Show();

            MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(frmInstance, new object[] { null });
        }


        private void barManager1_HighlightedLinkChanged(object sender, DevExpress.XtraBars.HighlightedLinkChangedEventArgs e)
        {
            // This event is required by the ToolTipControl object [it handles firing the Tooltips on BarSubItem objects on the toolbar because they don't show Tooltips] //
            toolTipController1.HideHint();
            if (e.Link == null) return;

            BarSubItemLink link = e.PrevLink as BarSubItemLink;
            if (link != null) link.CloseMenu();

            if (e.Link.Item is BarLargeButtonItem) return;

            var Info = new ToolTipControlInfo {Object = e.Link.Item, SuperTip = e.Link.Item.SuperTip};

            toolTipController1.ShowHint(Info);
        }


        private void SetJobStatus(int intStatus)
        {
         /*   // First... Check selected jobs are all appropriate - Status of 10 or 30 - if not, de-select them //
            GridView view = (GridView)gridControlJob.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to set as 'Ready to Send' before proceeding.", "Set Callouts as 'Ready to Send'", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Checking Selected Jobs...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = view.SelectedRowsCount / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();

            foreach (int intRowHandle in intRowHandles)
            {
                if (!(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobStatusID")) == 10 || Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobStatusID")) == 30))
                {
                    view.UnselectRow(intRowHandle);  // De-select job as it is not ready to send [Wrong Status] //
                    continue;
                }
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SiteGrittingContractID")) == 0)
                {
                    view.UnselectRow(intRowHandle);  // De-select job as it is not ready to send [Missing SiteGrittingContractID] //
                    continue;
                }
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SubContractorID")) == 0)
                {
                    view.UnselectRow(intRowHandle);  // De-select job as it is not ready to send [Missing SubContractorID] //
                    continue;
                }

                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "IsFloatingSite")) == 1 && Convert.ToDouble(view.GetRowCellValue(intRowHandle, "SiteLat")) == 0.00 && Convert.ToDouble(view.GetRowCellValue(intRowHandle, "SiteLong")) == 0.00)
                {
                    view.UnselectRow(intRowHandle);  // De-select job as it is not ready to send [Missing Lat \ Long] //
                    continue;
                }

                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to set as 'Ready to Send' before proceeding.\n\nNote: This process de-selects any selected jobs which are not ready to set as 'Ready to Send'...\n\nJobs must have a Site, Team, appropriate Status and a Latitude and Longitude.", "Set Callouts as 'Ready to Send'", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

             // If at this point, we are good to change status so get user confirmation //
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Job" : intRowHandles.Length.ToString() + " Jobs") + " selected for setting as 'Ready to Send'.\n\nProceed?", "Set Callout(s) as 'Ready to Send'", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            intUpdateProgressThreshhold = intRowHandles.Length / 10;
            intUpdateProgressTempCount = 0;

            fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Changing Job Status...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            
            string strJobIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strJobIDs += view.GetRowCellValue(intRowHandle, "GrittingCallOutID").ToString() + ",";
                
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            this.RefreshGridViewStateJob.SaveViewInfo();  // Store Grid View State //
            try
            {
                DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter CreatePDAJobs = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                CreatePDAJobs.ChangeConnectionString(strConnectionString);
                CreatePDAJobs.sp04155_GC_Gritting_Set_Job_Status(strJobIDs, intStatus);
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while changing the status of the selected Gritting Callouts.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Set Callouts as 'Ready To Send'", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            Load_Data();  // Refresh grid to reflect record's updated status //

            DevExpress.XtraEditors.XtraMessageBox.Show("Callout Statuses Changed Successfully.", "Set Callouts as 'Ready to Send'", MessageBoxButtons.OK, MessageBoxIcon.Information);
            */
        }

        private void bbiSelectJobsToComplete_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Select all jobs with a Status of 80, 110 //
            GridView view = (GridView)gridControlJob.MainView;
            view.ClearSelection();

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Selecting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = view.DataRowCount / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();

            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "JobStatusID")) == 80 || Convert.ToInt32(view.GetRowCellValue(i, "JobStatusID")) == 110) view.SelectRow(i);

                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
        }

        private void bbiManuallyCompleteJobs_ItemClick(object sender, ItemClickEventArgs e)
        {
            // First... Check selected jobs are all appropriate - Status of 70 - 110, if not, de-select them //
            GridView view = (GridView)gridControlJob.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Jobs to Manually Complete before proceeding.", "Manually Complete Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Manually Completing Jobs...");

            string strJobIDs = "";
            view.BeginUpdate();
            view.BeginSelection();
            foreach (int intRowHandle in intRowHandles)
            {
                if (!(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobStatusID")) == 80 || Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobStatusID")) == 110))
                {
                    view.UnselectRow(intRowHandle);  // De-select job as it is not ready to complete [Wrong Status] //
                }
                else  // Add ID to string holding record IDs to update //
                {
                    strJobIDs += view.GetRowCellValue(intRowHandle, "JobID").ToString() + ",";
                }
            }
            view.EndSelection();
            view.EndUpdate();

            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                if (splashScreenManager1.IsSplashFormVisible) splashScreenManager1.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Jobs to Manually Complete before proceeding.\n\nNote: This process de-selects any selected jobs which are not ready to complete.", "Manually Complete Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // If at this point, we are good to complete jobs so get user confirmation //
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Job" : intRowHandles.Length.ToString() + " Jobs") + " selected for Manually Completing.\n\nProceed?", "Manually Complete Jobs", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            DataSet_OM_JobTableAdapters.QueriesTableAdapter CompleteJobs = new DataSet_OM_JobTableAdapters.QueriesTableAdapter();
            CompleteJobs.ChangeConnectionString(strConnectionString);
            try
            {
                CompleteJobs.sp06272_OM_Manually_Complete_Job(strJobIDs);
            }
            catch (Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible) splashScreenManager1.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Manually Completing the selected Jobs.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Manually Complete Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (splashScreenManager1.IsSplashFormVisible) splashScreenManager1.CloseWaitForm();
            Load_Data();
            DevExpress.XtraEditors.XtraMessageBox.Show((intRowHandles.Length == 1 ? "1 Job" : Convert.ToString(intRowHandles.Length) + " Jobs") + " Manually Completed Successfully.", "Manually Complete Jobs", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            Load_Data();
        }


        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp06049_OM_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "om_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }
      
        private void bciFilterSelected_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlJob.MainView;
            if (bciFilterSelected.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        gridControlJob.EndUpdate();
                        XtraMessageBox.Show("Select one or more records to filter by before proceeding.", "Filter Selected Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControlJob.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
                gridControlJob.EndUpdate();
            }
            else  // Clear Filter //
            {
                gridControlJob.BeginUpdate();
                try
                {
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_OM_Job.sp06031_OM_Job_Manager.Rows)
                    {
                        if (Convert.ToInt32(dr["Selected"]) == 1) dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControlJob.EndUpdate();
        }

        private void dockPanelFilters_DockChanged(object sender, EventArgs e)
        {
            // Make semi transparent when undocked //
            DockPanel panel = sender as DockPanel;
            if (panel.FloatForm != null)
            {
                panel.FloatForm.AllowTransparency = true;
                panel.FloatForm.Opacity = 0.8;
            }

        }


        #region Route Optimization

        private void bbiOptimization_ItemClick(object sender, ItemClickEventArgs e)
        {
            Send_Data_To_Optimizer();
        }

        private void bbiSendDataToRouteOptimizer_ItemClick(object sender, ItemClickEventArgs e)
        {
            Send_Data_To_Optimizer();
        }

        private void Send_Data_To_Optimizer()
        {
            // First... Check selected jobs are all appropriate - if not, de-select them //
            GridView view = (GridView)gridControlJob.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to send to the Route Optimizer before proceeding.", "Route Optimize Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedCalloutIDs = "";
            int intStatus = 0;
            for (int i = 0; i < intRowHandles.Length; i++)
            {
                intStatus = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "JobStatusID"));
                if (!(intStatus == 50 || intStatus == 70 || intStatus == 72 || intStatus == 73 || intStatus == 75 || intStatus == 76 || intStatus == 80 || intStatus == 100))
                {
                    view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to optimise [Wrong Status] //
                    continue;
                }
                if (Convert.ToDecimal(view.GetRowCellValue(intRowHandles[i], "SiteLat")) == (decimal)0 && Convert.ToDecimal(view.GetRowCellValue(intRowHandles[i], "SiteLong")) == (decimal)0)
                {
                    view.UnselectRow(intRowHandles[i]);  // De-select job as it is not ready to optimise [Wrong Status] //
                    continue;
                }

                strSelectedCalloutIDs += view.GetRowCellValue(intRowHandles[i], "GrittingCallOutID").ToString() + ",";
            }
            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Callouts to send to the Route Optimizer before proceeding.\n\nNote: The system will automatically de-select any callouts with an inappopriate status for route optimisation.", "Route Optimize Callouts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            frm_GC_Gritting_Callouts_Send_To_Optimizer fChildForm = new frm_GC_Gritting_Callouts_Send_To_Optimizer();
            fChildForm.GlobalSettings = this.GlobalSettings;

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();

            fChildForm._CalloutIDs = strSelectedCalloutIDs;
            fChildForm.ShowDialog();
        }

        private void bbiGetDataToRouteOptimize_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Select all jobs with a Status of 50 (Ready to send) //
            GridView view = (GridView)gridControlJob.MainView;
            view.ClearSelection();

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Selecting...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = view.DataRowCount / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();

            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "JobStatusID")) == 50 && !string.IsNullOrEmpty(view.GetRowCellValue(i, "GritMobileTelephoneNumber").ToString()) && (Convert.ToDecimal(view.GetRowCellValue(i, "SiteLat")) != (decimal)0.00 && Convert.ToDecimal(view.GetRowCellValue(i, "SiteLong")) != (decimal)0.00)) view.SelectRow(i);

                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
        }

        private void bbiGetDataFromRouteOptimizer_ItemClick(object sender, ItemClickEventArgs e)
        {
            frm_GC_Gritting_Callouts_Optimizer_Results fChildForm = new frm_GC_Gritting_Callouts_Optimizer_Results();
            fChildForm.GlobalSettings = this.GlobalSettings;

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();

            fChildForm.ShowDialog();
            if (fChildForm.boolRefreshCallingScreen)
            {
                this.RefreshGridViewStateJob.SaveViewInfo();  // Store Grid View State //
                Load_Data();
            }
        }

        #endregion


        private void toolTipController1_GetActiveObjectInfo(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.SelectedControl != gridControlJob) return;
            try
            {
                GridView view = gridControlJob.GetViewAt(e.ControlMousePosition) as GridView;
                if (view == null) return;
                GridHitInfo hi = view.CalcHitInfo(e.ControlMousePosition);
                if (hi.InRowCell && hi.Column.Name == "Alert")
                {
                    if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "LinkedLabourCount")) <= 0 || Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "JobStatusID")) < 20 || Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "DoNotPayContractor")) == 1 || Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "DoNotInvoiceClient")) == 1)
                    {
                        string strMessage = "The Following Warnings are present for this Job:";
                        if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "DoNotPayContractor")) == 1) strMessage += "\n--> Do Not Pay Team.";
                        if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "DoNotInvoiceClient")) == 1) strMessage += "\n--> Do Not Invoice Client.";
                        if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "LinkedLabourCount")) <= 0) strMessage += "\n--> Unable to Send - At least one Labour Team must be linked.";
                        if (Convert.ToInt32(view.GetRowCellValue(hi.RowHandle, "JobStatusID")) < 20) strMessage += "\n--> Unable to Send - Job Status must be >= Job Creation Completed - Ready To Send.";

                        SuperToolTip st = new SuperToolTip();
                        SuperToolTipSetupArgs args = new SuperToolTipSetupArgs(st);
                        args.Title.Text = "Warning Icons - Information";
                        args.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
                        args.Contents.Text = strMessage;
                        args.ShowFooterSeparator = false;
                        args.Footer.Text = "";
                        st.Setup(args);
                        ToolTipControlInfo info = new ToolTipControlInfo(new CellToolTipInfo(hi.RowHandle, hi.Column, "cell"), args.Contents.Text);
                        info.SuperTip = st;
                        info.ToolTipType = ToolTipType.SuperTip;
                        e.Info = info;
                        return;
                    }
                    else return;  // No Tooltip //
                }
                if (hi.InColumnPanel && hi.Column != null && hi.Column.Name == "Alert")
                {
                    SuperToolTip st = new SuperToolTip();
                    SuperToolTipSetupArgs args = new SuperToolTipSetupArgs(st);
                    args.Title.Text = "Warning Icons - Information";
                    args.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
                    args.Contents.Text = "This column displays warning icons for jobs when certain conditions are met. Hover over the warning icon on jobs to see the warning(s).";
                    args.ShowFooterSeparator = false;
                    args.Footer.Text = "";
                    st.Setup(args);
                    ToolTipControlInfo info = new ToolTipControlInfo(new CellToolTipInfo(hi.RowHandle, hi.Column, "cell"), args.Contents.Text);
                    info.SuperTip = st;
                    info.ToolTipType = ToolTipType.SuperTip;
                    e.Info = info;
                    return;
                }
                /*if (hi.HitTest == GridHitTest.GroupPanel)
                {
                    info = new ToolTipControlInfo(hi.HitTest, "Group panel");
                    return;
                }*/

                /*if (hi.HitTest == GridHitTest.RowIndicator)
                {
                    info = new ToolTipControlInfo(GridHitTest.RowIndicator.ToString() + hi.RowHandle.ToString(), "Row Handle: " + hi.RowHandle.ToString());
                    ToolTipTitleItem titleItem1 = new ToolTipTitleItem();
                    byte[] cellIm = view.GetRowCellValue(hi.RowHandle, "Alert") as byte[];
                    Image im = null;
                    if (cellIm != null)
                    {
                        MemoryStream ms = new MemoryStream(cellIm);

                        im = Image.FromStream(ms);
                    }

                    ToolTipItem item1 = new ToolTipItem();
                    item1.Image = im;
                    sTooltip1.Items.Add(item1);
                }*/
                //info = new ToolTipControlInfo(hi.HitTest, "");
                //info.SuperTip = superToolTipSiteContractFilter;
            }
            catch (Exception) { }
        }

        private void checkEditTeam_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                ItemForLabourFilter.CustomizationFormText = "Team:";
                ItemForLabourFilter.Text = "Team:";
                i_str_selected_Labour_Team_ids = "";
                i_str_selected_Labour_Team_descriptions = "";
                selection12.ClearSelection();
                gridControl12.BeginUpdate();
                sp06028_OM_Labour_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06028_OM_Labour_Filter, (checkEditTeam.Checked ? 1 : 0));
                gridControl12.EndUpdate();
                popupContainerEditLabourFilter.Text = PopupContainerEditLabourFilter_Get_Selected();
            }
        }

        private void checkEditStaff_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                ItemForLabourFilter.CustomizationFormText = "Staff:";
                ItemForLabourFilter.Text = "Staff:";
                i_str_selected_Labour_Team_ids = "";
                i_str_selected_Labour_Team_descriptions = "";
                selection12.ClearSelection();
                gridControl12.BeginUpdate();
                sp06028_OM_Labour_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06028_OM_Labour_Filter, (checkEditTeam.Checked ? 1 : 0));
                gridControl12.EndUpdate();
                popupContainerEditLabourFilter.Text = PopupContainerEditLabourFilter_Get_Selected();
            }
        }

        private void btnClearAllFilters_Click(object sender, EventArgs e)
        {
            i_str_selected_client_ids = "";
            i_str_selected_client_names = "";
            buttonEditClientFilter.Text = "";
            superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\nNo Filter Set.";

            i_str_selected_site_ids = "";
            i_str_selected_site_names = "";
            buttonEditSiteFilter.Text = "";
            superToolTipSetupArgsSiteContractFilter.Contents.Text = "I store the currently applied Site Filter.\n\nNo Filter Set.";

            selection5.ClearSelection();
            popupContainerEditKAMFilter.Text = PopupContainerEditKAMFilter_Get_Selected();

            selection9.ClearSelection();
            popupContainerEditCMFilter.Text = PopupContainerEditCMFilter_Get_Selected();

            selection15.ClearSelection();
            popupContainerEditConstructionManagerFilter.Text = PopupContainerEditConstructionManagerFilter_Get_Selected();

            selection8.ClearSelection();
            popupContainerEditRecordTypeFilter.Text = PopupContainerEditRecordTypeFilter_Get_Selected();

            selection10.ClearSelection();
            popupContainerEditJobTypeFilter.Text = PopupContainerEditJobTypeFilter_Get_Selected();

            selection11.ClearSelection();
            popupContainerEditJobSubTypeFilter.Text = PopupContainerEditJobSubTypeFilter_Get_Selected();

            selection1.ClearSelection();
            popupContainerEditJobStatusFilter.Text = PopupContainerEditJobStatusFilter_Get_Selected();

            selection12.ClearSelection();
            popupContainerEditLabourFilter.Text = PopupContainerEditLabourFilter_Get_Selected();

            selection13.ClearSelection();
            popupContainerEditEquipmentTypeFilter.Text = PopupContainerEditEquipmentTypeFilter_Get_Selected();

            selection14.ClearSelection();
            popupContainerEditMaterialTypeFilter.Text = PopupContainerEditMaterialTypeFilter_Get_Selected();

            selection2.ClearSelection();
            popupContainerEditVisitTypeFilter.Text = PopupContainerEditVisitTypeFilter_Get_Selected();
        }


        private void Open_Picture_Viewer(string RecordIDs, int ClientID, int RecordTypeID)
        {
            var fChildForm = new frm_OM_Picture_Viewer();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._PassedInRecordIDs = RecordIDs;
            fChildForm._PassedInRecordTypeID = RecordTypeID;
            fChildForm._ClientID = ClientID;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();
            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
            return;
        }



    }
}



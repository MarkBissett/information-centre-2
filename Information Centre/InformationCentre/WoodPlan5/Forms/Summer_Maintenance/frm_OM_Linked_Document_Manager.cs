using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //
using System.IO;

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Linked_Document_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        public string strPassedInLinkedDocumentIDs = "";  // Used to hold IDs when form opened from another form in drill-down mode //
        public int intPassedInRecordTypeID = 0;
        public int intPassedInRecordSubTypeID = 0;
        public string strPassedInRecordType = "";
        public string strPassedInRecordSubType = "";
        public int intPassedInLinkedToRecordID = 0;
        public string strPassedInLinkedToRecord = "";

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        #region Sub Type Permission variables

        bool iBool_AllowDeleteJob = false;
        bool iBool_AllowAddJob = false;
        bool iBool_AllowEditJob = false;
        bool iBool_Job = false;

        bool iBool_AllowDeleteJobLabour = false;
        bool iBool_AllowAddJobLabour = false;
        bool iBool_AllowEditJobLabour = false;
        bool iBool_JobLabour = false;

        bool iBool_AllowDeleteJobEquipment = false;
        bool iBool_AllowAddJobEquipment = false;
        bool iBool_AllowEditJobEquipment = false;
        bool iBool_JobEquipment = false;

        bool iBool_AllowDeleteJobMaterial = false;
        bool iBool_AllowAddJobMaterial = false;
        bool iBool_AllowEditJobMaterial = false;
        bool iBool_JobMaterial = false;

        bool iBool_AllowDeleteJobPicture = false;
        bool iBool_AllowAddJobPicture = false;
        bool iBool_AllowEditJobPicture = false;
        bool iBool_JobPicture = false;

        bool iBool_AllowDeleteJobHSE = false;
        bool iBool_AllowAddJobHSE = false;
        bool iBool_AllowEditJobHSE = false;
        bool iBool_JobHSE = false;

        bool iBool_AllowDeleteJobCRM = false;
        bool iBool_AllowAddJobCRM = false;
        bool iBool_AllowEditJobCRM = false;
        bool iBool_JobCRM = false;

        bool iBool_AllowDeleteJobComment = false;
        bool iBool_AllowAddJobComment = false;
        bool iBool_AllowEditJobComment = false;
        bool iBool_JobComment = false;

        bool iBool_AllowDeleteVisitWaste = false;
        bool iBool_AllowAddVisitWaste = false;
        bool iBool_AllowEditVisitWaste = false;
        bool iBool_VisitWaste = false;

        bool iBool_AllowDeleteVisitSpraying = false;
        bool iBool_AllowAddVisitSpraying = false;
        bool iBool_AllowEditVisitSpraying = false;
        bool iBool_VisitSpraying = false;

        bool iBool_AllowDeleteJobExtraInfo = false;
        bool iBool_AllowAddJobExtraInfo = false;
        bool iBool_AllowEditJobExtraInfo = false;
        bool iBool_JobExtraInfo = false;

        bool iBool_AllowDeleteJobPO = false;
        bool iBool_AllowAddJobPO = false;
        bool iBool_AllowEditJobPO = false;
        bool iBool_JobPO = false;

        bool iBool_AllowDeleteClientContract = false;
        bool iBool_AllowAddClientContract = false;
        bool iBool_AllowEditClientContract = false;
        bool iBool_ClientContract = false;

        bool iBool_AllowDeleteClientContractYear = false;
        bool iBool_AllowAddClientContractYear = false;
        bool iBool_AllowEditClientContractYear = false;
        bool iBool_ClientContractYear = false;

        bool iBool_AllowDeleteSiteContract = false;
        bool iBool_AllowAddSiteContract = false;
        bool iBool_AllowEditSiteContract = false;
        bool iBool_SiteContract = false;

        bool iBool_AllowDeleteVisit = false;
        bool iBool_AllowAddVisit = false;
        bool iBool_AllowEditVisit = false;
        bool iBool_Visit = false;

        bool iBool_AllowDeleteAccident = false;
        bool iBool_AllowAddAccident = false;
        bool iBool_AllowEditAccident = false;
        bool iBool_Accident = false;

        bool iBool_AllowDeleteAccidentInjury = false;
        bool iBool_AllowAddAccidentInjury = false;
        bool iBool_AllowEditAccidentInjury = false;
        bool iBool_AccidentInjury = false;

        bool iBool_AllowDeleteTender = false;
        bool iBool_AllowAddTender = false;
        bool iBool_AllowEditTender = false;
        bool iBool_Tender = false;

        #endregion

        private int i_int_FocusedGrid = 1;
        private string i_str_selected_type_ids = "";
        private string i_str_selected_types = "";

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        BaseObjects.GridCheckMarksSelection selection1;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strTenderDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        #endregion

        public frm_OM_Linked_Document_Manager()
        {
            InitializeComponent();
        }

        private void frm_OM_Linked_Document_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 7005;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp06042_OM_Linked_Document_Types_No_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06042_OM_Linked_Document_Types_No_BlankTableAdapter.Fill(dataSet_GC_Summer_Core.sp06042_OM_Linked_Document_Types_No_Blank, (iBool_Job ? 1 : 0),
                                                                                                                                        (iBool_JobLabour ? 1 : 0),
                                                                                                                                        (iBool_JobEquipment ? 1 : 0),
                                                                                                                                        (iBool_JobMaterial ? 1 : 0),
                                                                                                                                        (iBool_JobPicture ? 1 : 0),
                                                                                                                                        (iBool_JobHSE ? 1 : 0),
                                                                                                                                        (iBool_JobCRM ? 1 : 0),
                                                                                                                                        (iBool_JobComment ? 1 : 0),
                                                                                                                                        (iBool_VisitWaste ? 1 : 0),
                                                                                                                                        (iBool_VisitSpraying ? 1 : 0),
                                                                                                                                        (iBool_JobExtraInfo ? 1 : 0),
                                                                                                                                        (iBool_JobPO ? 1 : 0),
                                                                                                                                        (iBool_ClientContract ? 1 : 0),
                                                                                                                                        (iBool_ClientContractYear ? 1 : 0),
                                                                                                                                        (iBool_SiteContract ? 1 : 0),
                                                                                                                                        (iBool_Visit ? 1 : 0),
                                                                                                                                        (iBool_Accident ? 1 : 0),
                                                                                                                                        (iBool_AccidentInjury ? 1 : 0),
                                                                                                                                        (iBool_Tender ? 1 : 0));
            gridControl3.ForceInitialize();
            
            sp06043_OM_Linked_Document_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "LinkedDocumentID");

            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_LinkedDocumentPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strTenderDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_TenderLinkedDocumentPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Tender Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Tender Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            if (strPassedInLinkedDocumentIDs != "")  // Opened in drill-down mode //
            {
                beiType.EditValue = "Custom Filter";
                Load_Data();  // Load records //

                // Attempt to pick up the Record Type and SubType descriptions //
                try
                {
                    DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
                    GetSetting.ChangeConnectionString(strConnectionString);
                    string strValue = GetSetting.sp06044_OM_Linked_Document_Get_Type_SubType(intPassedInRecordTypeID, intPassedInRecordSubTypeID).ToString();

                    char[] delimiters = new char[] { ',' };
                    string[] strArray = strValue.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    if (strArray.Length == 2)
                    {
                        strPassedInRecordType = strArray[0];
                        strPassedInRecordSubType = strArray[1];
                    }
                }
                catch (Exception) { }
            }

            popupContainerControlTypes.Size = new System.Drawing.Size(270, 423);

            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            if (strPassedInLinkedDocumentIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
        }

        private void frm_OM_Linked_Document_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data();
                }
            }
            SetMenuStatus();
        }

        private void frm_OM_Linked_Document_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInLinkedDocumentIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "TypeFilter", i_str_selected_type_ids);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Absence Filter //
                int intFoundRow = 0;
                string strFilter = default_screen_settings.RetrieveSetting("TypeFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    Array array = strFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView view = (GridView)gridControl3.MainView;
                    view.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in array)
                    {
                        if (strElement == "") break;
                        intFoundRow = view.LocateByValue(0, view.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            view.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    view.EndUpdate();
                    beiType.EditValue = PopupContainerEdit1_Get_Selected();
                }

                bbiRefresh.PerformClick();
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAdd = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEdit = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDelete = true;
                            }
                        }
                        break;
                    case 1:    // Jobs //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddJob = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditJob = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteJob = true;
                            }
                            iBool_Job = (iBool_AllowAddJob || iBool_AllowEditJob || iBool_AllowDeleteJob);
                        }
                        break;
                    case 2:    // Job Labour //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddJobLabour = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditJobLabour = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteJobLabour = true;
                            }
                            iBool_JobLabour = (iBool_AllowAddJobLabour || iBool_AllowEditJobLabour || iBool_AllowDeleteJobLabour);
                        }
                        break;
                    case 3:    // Job Equipment //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddJobEquipment = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditJobEquipment = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteJobEquipment = true;
                            }
                            iBool_JobEquipment = (iBool_AllowAddJobEquipment || iBool_AllowEditJobEquipment || iBool_AllowDeleteJobEquipment);
                        }
                        break;
                    case 4:    // Job Material //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddJobMaterial = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditJobMaterial = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteJobMaterial = true;
                            }
                            iBool_JobMaterial = (iBool_AllowAddJobMaterial || iBool_AllowEditJobMaterial || iBool_AllowDeleteJobMaterial);
                        }
                        break;
                    case 5:    // Job Picture //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddJobPicture = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditJobPicture = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteJobPicture = true;
                            }
                            iBool_JobPicture = (iBool_AllowAddJobPicture || iBool_AllowEditJobPicture || iBool_AllowDeleteJobPicture);
                        }
                        break;
                    case 6:    // Job Health and Safety //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddJobHSE = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditJobHSE = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteJobHSE = true;
                            }
                            iBool_JobHSE = (iBool_AllowAddJobHSE || iBool_AllowEditJobHSE || iBool_AllowDeleteJobHSE);
                        }
                        break;
                    case 7:    // Job CRM //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddJobCRM = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditJobCRM = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteJobCRM = true;
                            }
                            iBool_JobCRM = (iBool_AllowAddJobCRM || iBool_AllowEditJobCRM || iBool_AllowDeleteJobCRM);
                        }
                        break;
                    case 8:    // Job Comment //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddJobComment = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditJobComment = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteJobComment = true;
                            }
                            iBool_JobComment = (iBool_AllowAddJobComment || iBool_AllowEditJobComment || iBool_AllowDeleteJobComment);
                        }
                        break;
                    case 9:    // Visit Waste //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddVisitWaste = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditVisitWaste = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteVisitWaste = true;
                            }
                            iBool_VisitWaste = (iBool_AllowAddVisitWaste || iBool_AllowEditVisitWaste || iBool_AllowDeleteVisitWaste);
                        }
                        break;
                    case 10:    // Visit Spraying //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddVisitSpraying = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditVisitSpraying = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteVisitSpraying = true;
                            }
                            iBool_VisitSpraying = (iBool_AllowAddVisitSpraying || iBool_AllowEditVisitSpraying || iBool_AllowDeleteVisitSpraying);
                        }
                        break;
                    case 11:    // Job Extra Arttribute //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddJobExtraInfo = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditJobExtraInfo = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteJobExtraInfo = true;
                            }
                            iBool_JobExtraInfo = (iBool_AllowAddJobExtraInfo || iBool_AllowEditJobExtraInfo || iBool_AllowDeleteJobExtraInfo);
                        }
                        break;
                    case 12:    // Job PO //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddJobPO = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditJobPO = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteJobPO = true;
                            }
                            iBool_JobPO = (iBool_AllowAddJobPO || iBool_AllowEditJobPO || iBool_AllowDeleteJobPO);
                        }
                        break;
                    case 13:    // Client Contract //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddClientContract = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditClientContract = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteClientContract = true;
                            }
                            iBool_ClientContract = (iBool_AllowAddClientContract || iBool_AllowEditClientContract || iBool_AllowDeleteClientContract);
                        }
                        break;
                    case 14:    // Client Contract Year //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddClientContractYear = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditClientContractYear = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteClientContractYear = true;
                            }
                            iBool_ClientContractYear = (iBool_AllowAddClientContractYear || iBool_AllowEditClientContractYear || iBool_AllowDeleteClientContractYear);
                        }
                        break;
                    case 15:    // Site Contract //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddSiteContract = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditSiteContract = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteSiteContract = true;
                            }
                            iBool_SiteContract = (iBool_AllowAddSiteContract || iBool_AllowEditSiteContract || iBool_AllowDeleteSiteContract);
                        }
                        break;
                    case 16:    // Visit //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddVisit = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditVisit = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteVisit = true;
                            }
                            iBool_Visit = (iBool_AllowAddVisit || iBool_AllowEditVisit || iBool_AllowDeleteVisit);
                        }
                        break;
                    case 17:    // Accident //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddAccident = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditAccident = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteAccident = true;
                            }
                            iBool_Accident = (iBool_AllowAddAccident || iBool_AllowEditAccident || iBool_AllowDeleteAccident);
                        }
                        break;
                    case 18:    // Accident Injury //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddAccidentInjury = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditAccidentInjury = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteAccidentInjury = true;
                            }
                            iBool_AccidentInjury = (iBool_AllowAddAccidentInjury || iBool_AllowEditAccidentInjury || iBool_AllowDeleteAccidentInjury);
                        }
                        break;
                    case 19:    // Tender //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddTender = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditTender = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteTender = true;
                            }
                            iBool_Tender = (iBool_AllowAddTender || iBool_AllowEditTender || iBool_AllowDeleteTender);
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            if (i_int_FocusedGrid == 1)
            {
                view = (GridView)gridControl1.MainView;
            }
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();

            if (i_int_FocusedGrid == 1)  // Localities //
            {
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
        }


        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            GridView view = (GridView)gridControl1.MainView;
            RefreshGridViewState1.SaveViewInfo();
            view.BeginUpdate();
            if (beiType.EditValue.ToString() == "Custom Filter" && strPassedInLinkedDocumentIDs != "")  // Load passed in Records //
            {
                // Add new Ids into passed in IDs so they are loaded into the grid //
                if (!string.IsNullOrWhiteSpace(i_str_AddedRecordIDs1))
                {
                    strPassedInLinkedDocumentIDs += "," + i_str_AddedRecordIDs1.Replace(";", ",");
                }
                sp06043_OM_Linked_Document_ManagerTableAdapter.Fill(dataSet_GC_Summer_Core.sp06043_OM_Linked_Document_Manager, "", strPassedInLinkedDocumentIDs,
                                                                    (iBool_Job ? 1 : 0),
                                                                    (iBool_JobLabour ? 1 : 0),
                                                                    (iBool_JobEquipment ? 1 : 0),
                                                                    (iBool_JobMaterial ? 1 : 0),
                                                                    (iBool_JobPicture ? 1 : 0),
                                                                    (iBool_JobHSE ? 1 : 0),
                                                                    (iBool_JobCRM ? 1 : 0),
                                                                    (iBool_JobComment ? 1 : 0),
                                                                    (iBool_VisitWaste ? 1 : 0),
                                                                    (iBool_VisitSpraying ? 1 : 0),
                                                                    (iBool_JobExtraInfo ? 1 : 0),
                                                                    (iBool_JobPO ? 1 : 0),
                                                                    (iBool_ClientContract ? 1 : 0),
                                                                    (iBool_ClientContractYear ? 1 : 0),
                                                                    (iBool_SiteContract ? 1 : 0),
                                                                    (iBool_Visit ? 1 : 0),
                                                                    (iBool_Accident ? 1 : 0),
                                                                    (iBool_AccidentInjury ? 1 : 0),
                                                                    (iBool_Tender ? 1 : 0));
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                view.ExpandAllGroups();
            }
            else // Load users selection //
            {
                sp06043_OM_Linked_Document_ManagerTableAdapter.Fill(dataSet_GC_Summer_Core.sp06043_OM_Linked_Document_Manager, i_str_selected_type_ids, "",
                                                                    (iBool_Job ? 1 : 0),
                                                                    (iBool_JobLabour ? 1 : 0),
                                                                    (iBool_JobEquipment ? 1 : 0),
                                                                    (iBool_JobMaterial ? 1 : 0),
                                                                    (iBool_JobPicture ? 1 : 0),
                                                                    (iBool_JobHSE ? 1 : 0),
                                                                    (iBool_JobCRM ? 1 : 0),
                                                                    (iBool_JobComment ? 1 : 0),
                                                                    (iBool_VisitWaste ? 1 : 0),
                                                                    (iBool_VisitSpraying ? 1 : 0),
                                                                    (iBool_JobExtraInfo ? 1 : 0),
                                                                    (iBool_JobPO ? 1 : 0),
                                                                    (iBool_ClientContract ? 1 : 0),
                                                                    (iBool_ClientContractYear ? 1 : 0),
                                                                    (iBool_SiteContract ? 1 : 0),
                                                                    (iBool_Visit ? 1 : 0),
                                                                    (iBool_Accident ? 1 : 0),
                                                                    (iBool_AccidentInjury ? 1 : 0),
                                                                    (iBool_Tender ? 1 : 0));
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LinkedDocumentID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

 
        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            if (!iBool_AllowAdd) return;
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            System.Reflection.MethodInfo method = null;
            switch (i_int_FocusedGrid)
            {
                case 1:  
                    {
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_GC_Summer_Core.sp06043_OM_Linked_Document_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.LinkedToRecordID;
                            fChildForm.strLinkedToRecordDesc = HtmlRemoval.StripTagsCharArray(row.LinkedToRecord);
                            fChildForm.intRecordTypeID = row.LinkedDocumentTypeID;
                            fChildForm.strRecordTypeDescription = row.LinkedDocumentType;
                            fChildForm.intRecordSubTypeID = row.LinkedDocumentSubTypeID;
                            fChildForm.strRecordSubTypeDescription = row.LinkedDocumentSubType;
                        }
                        else
                        {
                            fChildForm.intLinkedToRecordID = intPassedInLinkedToRecordID;
                            fChildForm.strLinkedToRecordDesc = strPassedInLinkedToRecord;
                            fChildForm.intRecordTypeID = intPassedInRecordTypeID;
                            fChildForm.strRecordTypeDescription = strPassedInRecordType;
                            fChildForm.intRecordSubTypeID = intPassedInRecordSubTypeID;
                            fChildForm.strRecordSubTypeDescription = strPassedInRecordSubType;
                        }
                        fChildForm.iBool_Job = iBool_Job;
                        fChildForm.iBool_JobLabour = iBool_JobLabour;
                        fChildForm.iBool_JobEquipment = iBool_JobEquipment;
                        fChildForm.iBool_JobMaterial = iBool_JobMaterial;
                        fChildForm.iBool_JobPicture = iBool_JobPicture;
                        fChildForm.iBool_JobHSE = iBool_JobHSE;
                        fChildForm.iBool_JobCRM = iBool_JobCRM;
                        fChildForm.iBool_JobComment = iBool_JobComment;
                        fChildForm.iBool_VisitWaste = iBool_VisitWaste;
                        fChildForm.iBool_VisitSpraying = iBool_VisitSpraying;
                        fChildForm.iBool_JobExtraInfo = iBool_JobExtraInfo;
                        fChildForm.iBool_JobPO = iBool_JobPO;
                        fChildForm.iBool_ClientContract = iBool_ClientContract;
                        fChildForm.iBool_ClientContractYear = iBool_ClientContractYear;
                        fChildForm.iBool_SiteContract = iBool_SiteContract;
                        fChildForm.iBool_Visit = iBool_Visit;
                        fChildForm.iBool_Accident = iBool_Accident;
                        fChildForm.iBool_AccidentInjury = iBool_AccidentInjury;
                        fChildForm.iBool_Tender = iBool_Tender;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Add()
        {
        }

        private void Block_Edit()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1: 
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.iBool_Job = iBool_Job;
                        fChildForm.iBool_JobLabour = iBool_JobLabour;
                        fChildForm.iBool_JobEquipment = iBool_JobEquipment;
                        fChildForm.iBool_JobMaterial = iBool_JobMaterial;
                        fChildForm.iBool_JobPicture = iBool_JobPicture;
                        fChildForm.iBool_JobHSE = iBool_JobHSE;
                        fChildForm.iBool_JobCRM = iBool_JobCRM;
                        fChildForm.iBool_JobComment = iBool_JobComment;
                        fChildForm.iBool_VisitWaste = iBool_VisitWaste;
                        fChildForm.iBool_VisitSpraying = iBool_VisitSpraying;
                        fChildForm.iBool_JobExtraInfo = iBool_JobExtraInfo;
                        fChildForm.iBool_JobPO = iBool_JobPO;
                        fChildForm.iBool_ClientContract = iBool_ClientContract;
                        fChildForm.iBool_ClientContractYear = iBool_ClientContractYear;
                        fChildForm.iBool_SiteContract = iBool_SiteContract;
                        fChildForm.iBool_Visit = iBool_Visit;
                        fChildForm.iBool_Accident = iBool_Accident;
                        fChildForm.iBool_AccidentInjury = iBool_AccidentInjury;
                        fChildForm.iBool_Tender = iBool_Tender;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:  
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.iBool_Job = iBool_Job;
                        fChildForm.iBool_JobLabour = iBool_JobLabour;
                        fChildForm.iBool_JobEquipment = iBool_JobEquipment;
                        fChildForm.iBool_JobMaterial = iBool_JobMaterial;
                        fChildForm.iBool_JobPicture = iBool_JobPicture;
                        fChildForm.iBool_JobHSE = iBool_JobHSE;
                        fChildForm.iBool_JobCRM = iBool_JobCRM;
                        fChildForm.iBool_JobComment = iBool_JobComment;
                        fChildForm.iBool_VisitWaste = iBool_VisitWaste;
                        fChildForm.iBool_VisitSpraying = iBool_VisitSpraying;
                        fChildForm.iBool_JobExtraInfo = iBool_JobExtraInfo;
                        fChildForm.iBool_JobPO = iBool_JobPO;
                        fChildForm.iBool_ClientContract = iBool_ClientContract;
                        fChildForm.iBool_ClientContractYear = iBool_ClientContractYear;
                        fChildForm.iBool_SiteContract = iBool_SiteContract;
                        fChildForm.iBool_Visit = iBool_Visit;
                        fChildForm.iBool_Accident = iBool_Accident;
                        fChildForm.iBool_AccidentInjury = iBool_AccidentInjury;
                        fChildForm.iBool_Tender = iBool_Tender;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1: 
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Document Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Document Record" : Convert.ToString(intRowHandles.Length) + " Linked Document Records") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Document Record" : "these Linked Document Records") + " will no longer be available for selection!\n\nImportant Note: The physical linked document will not be deleted, only the link to it will be removed.</color>";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "LinkedDocumentID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("linked_document", strRecordIDs);
                                }
                                catch (Exception) { }
                            }
                            Load_Data();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1: 
                    {
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.iBool_Job = iBool_Job;
                        fChildForm.iBool_JobLabour = iBool_JobLabour;
                        fChildForm.iBool_JobEquipment = iBool_JobEquipment;
                        fChildForm.iBool_JobMaterial = iBool_JobMaterial;
                        fChildForm.iBool_JobPicture = iBool_JobPicture;
                        fChildForm.iBool_JobHSE = iBool_JobHSE;
                        fChildForm.iBool_JobCRM = iBool_JobCRM;
                        fChildForm.iBool_JobComment = iBool_JobComment;
                        fChildForm.iBool_VisitWaste = iBool_VisitWaste;
                        fChildForm.iBool_VisitSpraying = iBool_VisitSpraying;
                        fChildForm.iBool_JobExtraInfo = iBool_JobExtraInfo;
                        fChildForm.iBool_JobPO = iBool_JobPO;
                        fChildForm.iBool_ClientContract = iBool_ClientContract;
                        fChildForm.iBool_ClientContractYear = iBool_ClientContractYear;
                        fChildForm.iBool_SiteContract = iBool_SiteContract;
                        fChildForm.iBool_Visit = iBool_Visit;
                        fChildForm.iBool_Accident = iBool_Accident;
                        fChildForm.iBool_AccidentInjury = iBool_AccidentInjury;
                        fChildForm.iBool_Tender = iBool_Tender;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }


        #region Document Type Filter Panel

        private void btnTypeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEditType_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            i_str_selected_type_ids = "";    // Reset any prior values first //
            i_str_selected_types = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl3.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_type_ids = "";
                return "No Document Type Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_type_ids = "";
                return "No Document Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_type_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_types = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_types += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_types;
        }


        #endregion


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Linked Document Records - Adjust any filters then click Refresh button");
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }
        
        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                // Set any of the Dataset Menu items as appropriate if required //
                if (i_int_FocusedGrid == 1)
                {
                    //bsiDataset.Enabled = true;
                    //bbiDatasetManager.Enabled = true;
                    //bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "StatusValue")
            {
                string strStatus = view.GetRowCellValue(e.RowHandle, "StatusValue").ToString();
                if (strStatus.ToLower() == "open")
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink_1(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            string strExtension = view.GetRowCellValue(view.FocusedRowHandle, "DocumentExtension").ToString();

            int intRecordTypeID = 0;
            try
            {
                intRecordTypeID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "LinkedDocumentTypeID"));
            }
            catch (Exception) { }
            string strPath = (intRecordTypeID == 71 ? strTenderDefaultPath : strDefaultPath);
            if (!strPath.EndsWith("/")) strPath += "/";
            string strFullPath = Path.Combine(strPath, strFile.TrimStart(new char[] { '\\', '/' }));  // Make sure filename doesn't start with a proceeding slash otherwise Path.Combine doesn't work) // 

            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Document Linked - unable to proceed.", "View Linked Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!File.Exists(strFullPath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Linked Document File is missing - unable to proceed.", "View Linked Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                if (strExtension.ToLower() != "pdf")
                {
                    System.Diagnostics.Process.Start(strFullPath);
                }
                else
                {
                    if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                    {
                        frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                        fChildForm.strPDFFile = strFullPath;
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.Show();
                    }
                    else
                    {
                        System.Diagnostics.Process.Start(strFullPath);
                    }
                }
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the Linked Document: " + strFullPath + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

  

 


 

    }
}


namespace WoodPlan5
{
    partial class frm_OM_Site_Contract_Job_Rate_Apply_Uplift
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Site_Contract_Job_Rate_Apply_Uplift));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditClientFixed = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditClientPercentage = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.checkEditTeamFixed = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditTeamPercentage = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.spinEditClientAmount = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditTeamAmount = new DevExpress.XtraEditors.SpinEdit();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.groupControlClonedValues = new DevExpress.XtraEditors.GroupControl();
            this.labelControlContractDurationFrom = new DevExpress.XtraEditors.LabelControl();
            this.FromDate = new DevExpress.XtraEditors.DateEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditApplyRateUplift = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditClone = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditApplyUpliftToVisitAndJobs = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditClientFixed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditClientPercentage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTeamFixed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTeamPercentage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditClientAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTeamAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlClonedValues)).BeginInit();
            this.groupControlClonedValues.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditApplyRateUplift.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditClone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditApplyUpliftToVisitAndJobs.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(506, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 208);
            this.barDockControlBottom.Size = new System.Drawing.Size(506, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 208);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(506, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 208);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Images = this.imageCollection1;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.checkEditClientFixed);
            this.groupControl1.Controls.Add(this.checkEditClientPercentage);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.checkEditTeamFixed);
            this.groupControl1.Controls.Add(this.checkEditTeamPercentage);
            this.groupControl1.Location = new System.Drawing.Point(7, 87);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(256, 83);
            this.groupControl1.TabIndex = 5;
            this.groupControl1.Text = "Select Method of Update:";
            // 
            // checkEditClientFixed
            // 
            this.checkEditClientFixed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditClientFixed.Location = new System.Drawing.Point(131, 52);
            this.checkEditClientFixed.MenuManager = this.barManager1;
            this.checkEditClientFixed.Name = "checkEditClientFixed";
            this.checkEditClientFixed.Properties.Caption = "Fixed Amount";
            this.checkEditClientFixed.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditClientFixed.Properties.RadioGroupIndex = 3;
            this.checkEditClientFixed.Size = new System.Drawing.Size(98, 19);
            this.checkEditClientFixed.TabIndex = 22;
            this.checkEditClientFixed.TabStop = false;
            this.checkEditClientFixed.CheckedChanged += new System.EventHandler(this.checkEditClientFixed_CheckedChanged);
            // 
            // checkEditClientPercentage
            // 
            this.checkEditClientPercentage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditClientPercentage.EditValue = true;
            this.checkEditClientPercentage.Location = new System.Drawing.Point(47, 52);
            this.checkEditClientPercentage.MenuManager = this.barManager1;
            this.checkEditClientPercentage.Name = "checkEditClientPercentage";
            this.checkEditClientPercentage.Properties.Caption = "Percentage";
            this.checkEditClientPercentage.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditClientPercentage.Properties.RadioGroupIndex = 3;
            this.checkEditClientPercentage.Size = new System.Drawing.Size(77, 19);
            this.checkEditClientPercentage.TabIndex = 21;
            this.checkEditClientPercentage.CheckedChanged += new System.EventHandler(this.checkEditClientPercentage_CheckedChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Location = new System.Drawing.Point(6, 55);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(35, 13);
            this.labelControl3.TabIndex = 20;
            this.labelControl3.Text = "Client:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Location = new System.Drawing.Point(5, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(35, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Team:";
            // 
            // checkEditTeamFixed
            // 
            this.checkEditTeamFixed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditTeamFixed.Location = new System.Drawing.Point(132, 26);
            this.checkEditTeamFixed.MenuManager = this.barManager1;
            this.checkEditTeamFixed.Name = "checkEditTeamFixed";
            this.checkEditTeamFixed.Properties.Caption = "Fixed Amount";
            this.checkEditTeamFixed.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditTeamFixed.Properties.RadioGroupIndex = 2;
            this.checkEditTeamFixed.Size = new System.Drawing.Size(98, 19);
            this.checkEditTeamFixed.TabIndex = 1;
            this.checkEditTeamFixed.TabStop = false;
            this.checkEditTeamFixed.CheckedChanged += new System.EventHandler(this.checkEditTeamFixed_CheckedChanged);
            // 
            // checkEditTeamPercentage
            // 
            this.checkEditTeamPercentage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditTeamPercentage.EditValue = true;
            this.checkEditTeamPercentage.Location = new System.Drawing.Point(48, 26);
            this.checkEditTeamPercentage.MenuManager = this.barManager1;
            this.checkEditTeamPercentage.Name = "checkEditTeamPercentage";
            this.checkEditTeamPercentage.Properties.Caption = "Percentage";
            this.checkEditTeamPercentage.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditTeamPercentage.Properties.RadioGroupIndex = 2;
            this.checkEditTeamPercentage.Size = new System.Drawing.Size(77, 19);
            this.checkEditTeamPercentage.TabIndex = 0;
            this.checkEditTeamPercentage.CheckedChanged += new System.EventHandler(this.checkEditTeamPercentage_CheckedChanged);
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.spinEditClientAmount);
            this.groupControl2.Controls.Add(this.labelControl4);
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.spinEditTeamAmount);
            this.groupControl2.Location = new System.Drawing.Point(269, 87);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(230, 83);
            this.groupControl2.TabIndex = 6;
            this.groupControl2.Text = "Enter Amount to Increase \\ Decrease By:";
            // 
            // spinEditClientAmount
            // 
            this.spinEditClientAmount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditClientAmount.Location = new System.Drawing.Point(113, 53);
            this.spinEditClientAmount.MenuManager = this.barManager1;
            this.spinEditClientAmount.Name = "spinEditClientAmount";
            this.spinEditClientAmount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditClientAmount.Properties.Mask.EditMask = "P";
            this.spinEditClientAmount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditClientAmount.Size = new System.Drawing.Size(100, 20);
            this.spinEditClientAmount.TabIndex = 3;
            // 
            // labelControl4
            // 
            this.labelControl4.AllowHtmlString = true;
            this.labelControl4.Location = new System.Drawing.Point(8, 56);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(95, 13);
            this.labelControl4.TabIndex = 2;
            this.labelControl4.Text = "<b>Client</b> Sell Amount:";
            // 
            // labelControl2
            // 
            this.labelControl2.AllowHtmlString = true;
            this.labelControl2.Location = new System.Drawing.Point(8, 31);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(101, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "<b>Team</b> Cost Amount:";
            // 
            // spinEditTeamAmount
            // 
            this.spinEditTeamAmount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditTeamAmount.Location = new System.Drawing.Point(113, 28);
            this.spinEditTeamAmount.MenuManager = this.barManager1;
            this.spinEditTeamAmount.Name = "spinEditTeamAmount";
            this.spinEditTeamAmount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditTeamAmount.Properties.Mask.EditMask = "P";
            this.spinEditTeamAmount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditTeamAmount.Size = new System.Drawing.Size(100, 20);
            this.spinEditTeamAmount.TabIndex = 0;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnOK.Appearance.Options.UseFont = true;
            this.btnOK.ImageIndex = 1;
            this.btnOK.ImageList = this.imageCollection1;
            this.btnOK.Location = new System.Drawing.Point(332, 177);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(80, 23);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Info_16x16.png");
            this.imageCollection1.InsertGalleryImage("apply_16x16.png", "images/actions/apply_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "apply_16x16.png");
            this.imageCollection1.InsertGalleryImage("cancel_16x16.png", "images/actions/cancel_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/cancel_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "cancel_16x16.png");
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.ImageIndex = 2;
            this.btnCancel.ImageList = this.imageCollection1;
            this.btnCancel.Location = new System.Drawing.Point(419, 177);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 1;
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Job Rates Selected For Uplift: 0";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 0;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(506, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 208);
            this.barDockControl2.Size = new System.Drawing.Size(506, 26);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 208);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(506, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 208);
            // 
            // groupControlClonedValues
            // 
            this.groupControlClonedValues.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControlClonedValues.Controls.Add(this.labelControlContractDurationFrom);
            this.groupControlClonedValues.Controls.Add(this.FromDate);
            this.groupControlClonedValues.Location = new System.Drawing.Point(269, 7);
            this.groupControlClonedValues.Name = "groupControlClonedValues";
            this.groupControlClonedValues.Size = new System.Drawing.Size(230, 74);
            this.groupControlClonedValues.TabIndex = 14;
            this.groupControlClonedValues.Text = "Enter new rate Start:  [Required if Cloning]";
            // 
            // labelControlContractDurationFrom
            // 
            this.labelControlContractDurationFrom.AllowHtmlString = true;
            this.labelControlContractDurationFrom.Location = new System.Drawing.Point(6, 31);
            this.labelControlContractDurationFrom.Name = "labelControlContractDurationFrom";
            this.labelControlContractDurationFrom.Size = new System.Drawing.Size(79, 13);
            this.labelControlContractDurationFrom.TabIndex = 1;
            this.labelControlContractDurationFrom.Text = "Rate <b>Duration</b>:";
            // 
            // FromDate
            // 
            this.FromDate.EditValue = null;
            this.FromDate.Location = new System.Drawing.Point(90, 28);
            this.FromDate.MenuManager = this.barManager1;
            this.FromDate.Name = "FromDate";
            this.FromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.FromDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.FromDate.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.FromDate.Size = new System.Drawing.Size(100, 20);
            this.FromDate.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.checkEditApplyRateUplift);
            this.groupControl3.Controls.Add(this.checkEditClone);
            this.groupControl3.Location = new System.Drawing.Point(7, 7);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(256, 74);
            this.groupControl3.TabIndex = 19;
            this.groupControl3.Text = "Select Action to be performed:";
            // 
            // checkEditApplyRateUplift
            // 
            this.checkEditApplyRateUplift.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditApplyRateUplift.Location = new System.Drawing.Point(6, 49);
            this.checkEditApplyRateUplift.MenuManager = this.barManager1;
            this.checkEditApplyRateUplift.Name = "checkEditApplyRateUplift";
            this.checkEditApplyRateUplift.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditApplyRateUplift.Properties.Caption = "Apply Rate <b>Uplift</b>";
            this.checkEditApplyRateUplift.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditApplyRateUplift.Properties.RadioGroupIndex = 1;
            this.checkEditApplyRateUplift.Size = new System.Drawing.Size(245, 19);
            this.checkEditApplyRateUplift.TabIndex = 1;
            this.checkEditApplyRateUplift.TabStop = false;
            this.checkEditApplyRateUplift.CheckedChanged += new System.EventHandler(this.checkEditApplyRateUplift_CheckedChanged);
            // 
            // checkEditClone
            // 
            this.checkEditClone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditClone.EditValue = true;
            this.checkEditClone.Location = new System.Drawing.Point(6, 26);
            this.checkEditClone.MenuManager = this.barManager1;
            this.checkEditClone.Name = "checkEditClone";
            this.checkEditClone.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditClone.Properties.Caption = "<b>Clone</b> Rates and Optionally Apply Rate <b>Uplift</b>";
            this.checkEditClone.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditClone.Properties.RadioGroupIndex = 1;
            this.checkEditClone.Size = new System.Drawing.Size(245, 19);
            this.checkEditClone.TabIndex = 0;
            this.checkEditClone.CheckedChanged += new System.EventHandler(this.checkEditClone_CheckedChanged);
            // 
            // checkEditApplyUpliftToVisitAndJobs
            // 
            this.checkEditApplyUpliftToVisitAndJobs.EditValue = 1;
            this.checkEditApplyUpliftToVisitAndJobs.Location = new System.Drawing.Point(7, 179);
            this.checkEditApplyUpliftToVisitAndJobs.MenuManager = this.barManager1;
            this.checkEditApplyUpliftToVisitAndJobs.Name = "checkEditApplyUpliftToVisitAndJobs";
            this.checkEditApplyUpliftToVisitAndJobs.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.checkEditApplyUpliftToVisitAndJobs.Properties.Appearance.Options.UseFont = true;
            this.checkEditApplyUpliftToVisitAndJobs.Properties.Caption = "Apply uplift to existing un-invoiced visits and jobs";
            this.checkEditApplyUpliftToVisitAndJobs.Properties.ValueChecked = 1;
            this.checkEditApplyUpliftToVisitAndJobs.Properties.ValueUnchecked = 0;
            this.checkEditApplyUpliftToVisitAndJobs.Size = new System.Drawing.Size(305, 19);
            this.checkEditApplyUpliftToVisitAndJobs.TabIndex = 24;
            // 
            // frm_OM_Site_Contract_Job_Rate_Apply_Uplift
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(506, 234);
            this.Controls.Add(this.checkEditApplyUpliftToVisitAndJobs);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControlClonedValues);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OM_Site_Contract_Job_Rate_Apply_Uplift";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Site Contract Job Rate - Apply Uplift";
            this.Load += new System.EventHandler(this.frm_OM_Site_Contract_Job_Rate_Apply_Uplift_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.groupControl1, 0);
            this.Controls.SetChildIndex(this.groupControl2, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.groupControlClonedValues, 0);
            this.Controls.SetChildIndex(this.groupControl3, 0);
            this.Controls.SetChildIndex(this.checkEditApplyUpliftToVisitAndJobs, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditClientFixed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditClientPercentage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTeamFixed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTeamPercentage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditClientAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTeamAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlClonedValues)).EndInit();
            this.groupControlClonedValues.ResumeLayout(false);
            this.groupControlClonedValues.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditApplyRateUplift.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditClone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditApplyUpliftToVisitAndJobs.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit checkEditTeamFixed;
        private DevExpress.XtraEditors.CheckEdit checkEditTeamPercentage;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SpinEdit spinEditTeamAmount;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarButtonItem barStaticItemInformation;
        private DevExpress.XtraEditors.GroupControl groupControlClonedValues;
        private DevExpress.XtraEditors.LabelControl labelControlContractDurationFrom;
        private DevExpress.XtraEditors.DateEdit FromDate;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.CheckEdit checkEditApplyRateUplift;
        private DevExpress.XtraEditors.CheckEdit checkEditClone;
        private DevExpress.XtraEditors.CheckEdit checkEditClientFixed;
        private DevExpress.XtraEditors.CheckEdit checkEditClientPercentage;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SpinEdit spinEditClientAmount;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.CheckEdit checkEditApplyUpliftToVisitAndJobs;
    }
}

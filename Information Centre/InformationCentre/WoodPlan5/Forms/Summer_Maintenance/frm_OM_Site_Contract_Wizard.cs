﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;
using DevExpress.Utils.Animation;  // Required by Transition Effects //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;

namespace WoodPlan5
{
    public partial class frm_OM_Site_Contract_Wizard : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        bool internalRowFocusing;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_TemplateManager = false;

        bool iBool_ContractChangesMade = false;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        public int i_intClientID = 0;

        public DateTime i_dtStart;
        public DateTime i_dtEnd;

        public string i_str_PassedInClientContractIDs = "";
        private int i_int_SelectedClientContractID = 0;
        
        BaseObjects.GridCheckMarksSelection selection2;
        private Utils.enmFocusedGrid _enmFocusedGrid = Utils.enmFocusedGrid.Sites;

        private decimal _decDefaultVatRate = (decimal)0.00;

        #endregion

        public frm_OM_Site_Contract_Wizard()
        {
            InitializeComponent();
        }

        private void frm_OM_Site_Contract_Wizard_Load(object sender, EventArgs e)
        {
            this.FormID = 500131;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            // Get Form Permissions (For Edit Templates Button) //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, 7008, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);  // 7008 //
            ProcessPermissionsForForm();

            xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;  // Hide Table Page headers so it doesn't look like a pageframe //
            xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;

            i_dtStart = DateTime.Today.AddMonths(-6);
            i_dtEnd = DateTime.Today.AddMonths(6);
            dateEditFromDate.DateTime = i_dtStart;
            dateEditToDate.DateTime = i_dtEnd;
          
            try
            {
                var GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                _decDefaultVatRate = Convert.ToDecimal(GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingDefaultVatRate"));
            }
            catch (Exception)
            {
                _decDefaultVatRate = (decimal)0.00;
            }
 
            sp06086_OM_Site_Contract_Wizard_Client_ContractsTableAdapter.Connection.ConnectionString = strConnectionString;
            Load_Client_Contracts();
            try
            {
                sp06101_OM_Work_Unit_Types_PicklistTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06101_OM_Work_Unit_Types_PicklistTableAdapter.Fill(dataSet_OM_Contract.sp06101_OM_Work_Unit_Types_Picklist, 0);

                sp06108_OM_Unit_Descriptors_PicklistTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06108_OM_Unit_Descriptors_PicklistTableAdapter.Fill(dataSet_OM_Contract.sp06108_OM_Unit_Descriptors_Picklist, 0);

                sp06355_OM_Client_Billing_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06355_OM_Client_Billing_Types_With_BlankTableAdapter.Fill(dataSet_OM_Contract.sp06355_OM_Client_Billing_Types_With_Blank, 0);

                sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06141_OM_Visit_Template_Headers_With_Blank);

                sp06250_OM_Visit_Categories_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06250_OM_Visit_Categories_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06250_OM_Visit_Categories_With_Blank, 1);
            }
            catch (Exception) { }
             
            sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter.Connection.ConnectionString = strConnectionString;

            sp06090_OM_Site_Contract_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06092_OM_Site_Contract_Year_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06095_OM_Site_Contract_Year_Billing_Profile_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06098_OM_Site_Contract_Labour_Cost_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06103_OM_Site_Contract_Equipment_Cost_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06106_OM_Site_Contract_Material_Cost_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06346_OM_Site_Contract_Job_Rate_EditTableAdapter.Connection.ConnectionString = strConnectionString;

            sp06089_OM_Sites_For_ClientTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl2.ForceInitialize();
            // Add record selection checkboxes to popup Callout Type grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.Width = 30;
            selection2.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            selection2.CheckMarkColumn.VisibleIndex = 0;

            // Set Starting Page //
            if (string.IsNullOrWhiteSpace(i_str_PassedInClientContractIDs))
            {
                i_int_SelectedClientContractID = 0;
                barEditItemDateRange.EditValue = PopupContainerEditDateRange_Get_Selected();
                xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
                btnWelcomeNext.Focus();

                timerWelcomePage.Start();  // allow wizard to move from the Welcome page after 2 seconds. //
            }
            else  // Passed in Client Contract ID //
            {
                gridControl1.ForceInitialize();
                GridView view = (GridView)gridControl1.MainView;

                i_int_SelectedClientContractID = 0;
                barEditItemDateRange.EditValue = PopupContainerEditDateRange_Get_Selected();
                xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
                btnWelcomeNext.Focus();

                char[] delimiters = new char[] { ',' };
                string[] strArray = i_str_PassedInClientContractIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray.Length == 1)
                {
                    int intValueToFind = Convert.ToInt32(strArray[0]);
                    int intFoundRow = view.LocateByValue(0, view.Columns["ClientContractID"], intValueToFind);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intFoundRow);
                        view.MakeRowVisible(intFoundRow, false);
                        barEditItemDateRange.EditValue = "Custom Filter";

                        i_int_SelectedClientContractID = intValueToFind;

                        MoveToPage2();
                        gridControl2.Focus();
                    }
                }
            }
            emptyEditor = new RepositoryItem();
            ibool_FormStillLoading = false;
        }
        private void timerWelcomePage_Tick(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;
            timerWelcomePage.Stop();
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            ConfigureFormAccordingToMode();
            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            switch (strFormMode)
            {
                case "add":
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    break;
                default:
                    break;
            }
            ibool_FormStillLoading = false;

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(xtraTabPageStep2.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
        }


        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        iBool_TemplateManager = sfpPermissions.blRead;
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            GridView view = (GridView)gridControl1.MainView;

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    view = (GridView)gridControl1.MainView;
                    break;
                case Utils.enmFocusedGrid.Sites:
                    view = (GridView)gridControl2.MainView;
                    break;
                case Utils.enmFocusedGrid.SiteEdit:
                    view = (GridView)gridControl3.MainView;
                    break;
                case Utils.enmFocusedGrid.SiteYearEdit:
                    view = (GridView)gridControl5.MainView;
                    break;
                case Utils.enmFocusedGrid.SiteYearBilling:
                    view = (GridView)gridControl7.MainView;
                    break;
                case Utils.enmFocusedGrid.PreferredLabour:
                    view = (GridView)gridControl10.MainView;
                    break;
                case Utils.enmFocusedGrid.PreferredEquipment:
                    view = (GridView)gridControl12.MainView;
                    break;
                case Utils.enmFocusedGrid.PreferredMaterial:
                    view = (GridView)gridControl14.MainView;
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    view = (GridView)gridControl8.MainView;
                    break;
                case Utils.enmFocusedGrid.JobRate:
                    view = (GridView)gridControl18.MainView;
                    break;
                default:
                    break;
            }
            int[] intRowHandles = view.GetSelectedRows();

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                case Utils.enmFocusedGrid.Sites:
                    break;
                case Utils.enmFocusedGrid.SiteEdit:
                    if (strFormMode != "view")
                    {
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = false;
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                        if (intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.SiteYearEdit:
                case Utils.enmFocusedGrid.SiteYearBilling:
                case Utils.enmFocusedGrid.PreferredLabour:
                case Utils.enmFocusedGrid.PreferredEquipment:
                case Utils.enmFocusedGrid.PreferredMaterial:
                case Utils.enmFocusedGrid.PersonResponsibility:
                case Utils.enmFocusedGrid.JobRate:
                    if (strFormMode != "view")
                    {
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = false;
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                        if (intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                default:
                    break;
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            GridView parentView = null;
            int[] intParentRowHandles = null;

            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intRowHandles.Length > 1);
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);

            parentView = (GridView)gridControl6.MainView;
            intParentRowHandles = parentView.GetSelectedRows();
            view = (GridView)gridControl5.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 1);
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);

            parentView = (GridView)gridControl6.MainView;
            intParentRowHandles = parentView.GetSelectedRows();
            view = (GridView)gridControl7.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 1);
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            bbiAddProfile.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            bbiCopyClientContractYearBillingProfile.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            bbiAddProfileFromTemplate.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);

            parentView = (GridView)gridControl9.MainView;
            intParentRowHandles = parentView.GetSelectedRows();
            view = (GridView)gridControl10.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl10.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            gridControl10.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 1);
            gridControl10.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            bbiAddLabourCost.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            bbiBlockAddLabourCost.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);

            parentView = (GridView)gridControl11.MainView;
            intParentRowHandles = parentView.GetSelectedRows();
            view = (GridView)gridControl12.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl12.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            gridControl12.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 1);
            gridControl12.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            bbiAddEquipmentRate.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            bbiBlockAddEquipmentRate.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);

            parentView = (GridView)gridControl13.MainView;
            intParentRowHandles = parentView.GetSelectedRows();
            view = (GridView)gridControl14.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl14.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            gridControl14.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 1);
            gridControl14.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            bbiAddMaterialRate.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            bbiBlockAddMaterialRate.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);

            parentView = (GridView)gridControl15.MainView;
            intParentRowHandles = parentView.GetSelectedRows();
            view = (GridView)gridControl8.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl8.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            gridControl8.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 1);
            gridControl8.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            bbiAddPersonResponsibility.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            bbiBlockAddPersonResponsibility.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);

            parentView = (GridView)gridControl17.MainView;
            intParentRowHandles = parentView.GetSelectedRows();
            view = (GridView)gridControl18.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl18.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            gridControl18.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 1);
            gridControl18.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            bbiAddJobRate.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            bbiBlockAddJobRate.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);

            bbiEditTemplates.Enabled = iBool_TemplateManager;
        }

        private void frm_OM_Site_Contract_Wizard_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

            }
        }

        private void frm_OM_Site_Contract_Wizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                //default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CompanyFilter", i_str_selected_Company_ids);
                //default_screen_settings.SaveDefaultScreenSettings();
            }

            // Clear timer in the off chance it is still running //
            timerWelcomePage.Stop();
            timerWelcomePage = null;
        }


        private void xtraTabControl1_SelectedPageChanging(object sender, DevExpress.XtraTab.TabPageChangingEventArgs e)
        {
            // Remember to set each pages Tag property to the page number in the designer //
            if (Convert.ToInt32(e.Page.Tag) > Convert.ToInt32(e.PrevPage.Tag))
            {
                ((PushTransition)transitionManager1.Transitions[xtraTabControl1].TransitionType).Parameters.EffectOptions = DevExpress.Utils.Animation.PushEffectOptions.FromRight;
            }
            else
            {
                ((PushTransition)transitionManager1.Transitions[xtraTabControl1].TransitionType).Parameters.EffectOptions = DevExpress.Utils.Animation.PushEffectOptions.FromLeft;
            }
            transitionManager1.StartTransition(xtraTabControl1);
        }
        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            // Remember to set each pages Tag property to the page number in the designer //
            transitionManager1.EndTransition();

            switch (xtraTabControl1.SelectedTabPage.Name)
            {
                case "xtraTabPageStep4":
                    {
                        Clear_Filter_Billing_Profile();

                        // Check LinkedToParent Values in case they have changed //
                        GridView parentView = (GridView)gridControl6.MainView;
                        int intParentMax = parentView.DataRowCount;
                        if (intParentMax <= 0) return;

                        GridView childView = (GridView)gridControl7.MainView;
                        int intChildMax = childView.DataRowCount;
                        if (intChildMax <= 0) return;

                        string strDescription = "";
                        int intParentClientContractYearID = 0;
                        for (int i = 0; i < intParentMax; i++)
                        {
                            intParentClientContractYearID = Convert.ToInt32(parentView.GetRowCellValue(i, "ClientContractYearID"));
                            strDescription = (string.IsNullOrWhiteSpace(parentView.GetRowCellValue(i, "StartDate").ToString()) ? "Unknown Start" : Convert.ToDateTime(parentView.GetRowCellValue(i, "StartDate")).ToString("dd/MM/yyyy")) + " - " + (string.IsNullOrWhiteSpace(parentView.GetRowCellValue(i, "EndDate").ToString()) ? "Unknown End" : Convert.ToDateTime(parentView.GetRowCellValue(i, "EndDate")).ToString("dd/MM/yyyy"));
                            for (int j = 0; j < intChildMax; j++)
                            {
                                if (Convert.ToInt32(childView.GetRowCellValue(j, "ClientContractYearID")) == intParentClientContractYearID)
                                {
                                    if (childView.GetRowCellValue(j, "LinkedToParent").ToString() != strDescription) childView.SetRowCellValue(j, "LinkedToParent", strDescription);
                                }
                            }
                        }
                        Filter_Billing_Profile_On_Year();
                    }
                    break;
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Client Contracts Available - click the Refresh button";
                    break;
                case "gridView2":
                    message = "No Client Sites Available - select a different Client Contract from Step 1";
                    break;
                case "gridView3":
                    message = "No Client Sites Available - tick one or more sites in Step 2 before proceeding";
                    break;
                case "gridView4":
                    message = "No Sites Available - tick one or more sites in Step 2 before proceeding";
                    break;
                case "gridView5":
                    message = "No Site Contract Years Available - Select one or more Sites from left list to view linked Years. Click the Add button or enter the number of Contract Years to add and click the Add Years To Grid button";
                    break;
                case "gridView6":
                    message = "No Contract Years Available - Create Contract Years in Step 3 of the Wizard";
                    break;
                case "gridView7":
                    message = "No Billing Profiles Available - select one or more Site Years to see linked Billing Profiles. Click the Add button once a Year is selected to add one or more Profiles";
                    break;
                case "gridView9":
                    message = "No Sites Available - tick one or more sites in Step 2 before proceeding";
                    break;
                case "gridView10":
                    message = "No Site Preferred Labour Available - Select one or more Sites from left list to view linked Preferred Labour. Click the Add button to add Preferred Labour";
                    break;
                case "gridView11":
                    message = "No Sites Available - tick one or more sites in Step 2 before proceeding";
                    break;
                case "gridView12":
                    message = "No Site Default Equipment Rates Available - Select one or more Sites from left list to view linked Default Equipment Rates. Click the Add button to add Default Equipment Rates";
                    break;
                case "gridView13":
                    message = "No Sites Available - tick one or more sites in Step 2 before proceeding";
                    break;
                case "gridView14":
                    message = "No Site Default Material Rates Available - Select one or more Sites from left list to view linked Default Material Rates. Click the Add button to add Default Material Rates";
                    break;
                case "gridView15":
                    message = "No Sites Available - tick one or more sites in Step 2 before proceeding";
                    break;
                case "gridView8":
                    message = "No Responsible People Available - click the Add button to add Responsible People";
                    break;
                case "gridView17":
                    message = "No Sites Available - tick one or more sites in Step 2 before proceeding";
                    break;
                case "gridView18":
                    message = "No Site Job Rates Available - Select one or more Sites from left list to view linked Job Rates. Click the Add button to add Job Rates";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView4":
                    Filter_Contract_Year_On_Site();
                    view = (GridView)gridControl5.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridView6":
                    Filter_Billing_Profile_On_Year();
                    break;
                case "gridView9":
                    Filter_Preferred_labour_On_Site();
                    view = (GridView)gridControl10.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridView11":
                    Filter_Preferred_Equipment_On_Site();
                    view = (GridView)gridControl12.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridView13":
                    Filter_Preferred_Material_On_Site();
                    view = (GridView)gridControl14.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridView15":
                    Filter_Person_Responsibilities_On_Site();
                    view = (GridView)gridControl8.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridView17":
                    Filter_Job_Rate();
                    view = (GridView)gridControl18.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region Welcome Page

        private void btnWelcomeNext_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;
        }

        #endregion


        #region Step 1 Page

        private void btnStep1Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
        }
        private void btnStep1Next_Click(object sender, EventArgs e)
        {
            MoveToPage2();
        }
        private void MoveToPage2()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //         

            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select just one Client Contract to create the Site Contracts for before proceeding.", "Create Site Contract(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ClientContractID")) != i_int_SelectedClientContractID || ibool_FormStillLoading)
            {
                i_int_SelectedClientContractID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ClientContractID"));  // Pick up selected client contract ID //
                int intClientID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ClientID"));  // Pick up selected client contract ID //
                if (intClientID <= 0) return;

                sp06089_OM_Sites_For_ClientTableAdapter.Fill(dataSet_OM_Contract.sp06089_OM_Sites_For_Client, intClientID.ToString() + ",");
                selection2.SelectAll();
                Set_Selected_Site_Count_Label();
            }

            CopyClientContractYearsSimpleButton.Enabled = (Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ClientYearCount")) > 0);  // Set Step 4 button to copy Client Contract Years enabled status //

            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep2;
        }

        private void bbiRefreshClientContracts_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            i_str_PassedInClientContractIDs = "";  // Clear any passed in Contract IDs //
            Load_Client_Contracts();
        }

        private void Load_Client_Contracts()
        {
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            int intActive = Convert.ToInt32(barEditItemActive.EditValue);
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();

            DateTime dtStartDate = new DateTime(2000, 1, 1);
            DateTime dtEndDate = new DateTime(2500, 1, 1);
            if (string.IsNullOrWhiteSpace(i_str_PassedInClientContractIDs))
            {
                dtStartDate = i_dtStart;
                dtEndDate = i_dtEnd;
            }

            try
            {
                sp06086_OM_Site_Contract_Wizard_Client_ContractsTableAdapter.Fill(dataSet_OM_Contract.sp06086_OM_Site_Contract_Wizard_Client_Contracts, i_str_PassedInClientContractIDs, dtStartDate, dtEndDate, intActive);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Client Contracts.\n\nMessage = [" + ex.Message + "].\n\nPlease close the screen then try again. If the problem persists, contact Technical Support.", "Load Client Contracts", MessageBoxButtons.OK, MessageBoxIcon.Stop);

            }
            view.EndUpdate();
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }


        #region GridView1

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Client_Contracts();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            btnStep3Next.PerformClick();
        }
 
        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Contract;
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Contract;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region Date Range Filter Panel

        private void btnDateRangeOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            string strValue = "";
            if (dateEditFromDate.DateTime == DateTime.MinValue)
            {
                i_dtStart = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtStart = dateEditFromDate.DateTime;
            }
            if (dateEditToDate.DateTime == DateTime.MinValue)
            {
                i_dtEnd = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtEnd = dateEditToDate.DateTime;
            }
            if (dateEditFromDate.DateTime == DateTime.MinValue && dateEditToDate.DateTime == DateTime.MinValue)
            {
                strValue = "No Date Range Filter";
            }
            else
            {
                strValue = (i_dtStart == new DateTime(1900, 1, 1) ? "No Start" : i_dtStart.ToString("dd/MM/yyyy HH:mm")) + " - " + (i_dtEnd == new DateTime(1900, 1, 1) ? "No End" : i_dtEnd.ToString("dd/MM/yyyy HH:mm"));
            }
            return strValue;
        }

        #endregion

        #endregion


        #region Step 2 Page

        private void btnStep2Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;
        }
        private void btnStep2Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
       
            int intCount = selection2.SelectedCount;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select at least one Site to create the Site Contracts for before proceeding.", "Create Site Contract(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Clear any Preferred Material records already created //
            view = (GridView)gridControl8.MainView;
            view.BeginUpdate();
            try
            {
                dataSet_OM_Contract.sp06081_OM_Client_Contract_Linked_Responsibilities_Edit.Rows.Clear();  // Clear any existing rows //
            }
            catch (Exception Ex) { }
            view.EndUpdate();
            
            // Clear any Preferred Material records already created //
            view = (GridView)gridControl14.MainView;
            view.BeginUpdate();
            try
            {
                dataSet_OM_Contract.sp06106_OM_Site_Contract_Material_Cost_Edit.Rows.Clear();  // Clear any existing rows //
            }
            catch (Exception Ex) { }
            view.EndUpdate();

            // Clear any Preferred Equipment records already created //
            view = (GridView)gridControl12.MainView;
            view.BeginUpdate();
            try
            {
                dataSet_OM_Contract.sp06103_OM_Site_Contract_Equipment_Cost_Edit.Rows.Clear();  // Clear any existing rows //
            }
            catch (Exception Ex) { }
            view.EndUpdate();

            // Clear any Preferred Labour records already created //
            view = (GridView)gridControl10.MainView;
            view.BeginUpdate();
            try
            {
                dataSet_OM_Contract.sp06098_OM_Site_Contract_Labour_Cost_Edit.Rows.Clear();  // Clear any existing rows //
            }
            catch (Exception Ex) { }
            view.EndUpdate();

            // Clear any Contract Year records already created //
            view = (GridView)gridControl5.MainView;
            view.BeginUpdate();
            try
            {
                dataSet_OM_Contract.sp06092_OM_Site_Contract_Year_Edit.Rows.Clear();  // Clear any existing rows //
            }
            catch (Exception Ex) { }
            view.EndUpdate();

            // Clear any Contract Year Billing Profile records already created //
            view = (GridView)gridControl7.MainView;
            view.BeginUpdate();
            try
            {
                dataSet_OM_Contract.sp06095_OM_Site_Contract_Year_Billing_Profile_Edit.Rows.Clear();  // Clear any existing rows //
            }
            catch (Exception Ex) { }
            view.EndUpdate();

            // Clear any Job Rate records already created //
            view = (GridView)gridControl18.MainView;
            view.BeginUpdate();
            try
            {
                dataSet_OM_Contract.sp06345_OM_Site_Contract_Manager_Linked_Job_Rates.Rows.Clear();  // Clear any existing rows //
            }
            catch (Exception Ex) { }
            view.EndUpdate();

            // Create Site Contract Records for each selected Site //
            GridView viewSites = (GridView)gridControl2.MainView; 
            view = (GridView)gridControl3.MainView;
            GridView viewClientContract = (GridView)gridControl1.MainView;
            int intProcessedCount = 0;
            view.BeginUpdate();
            try
            {
                dataSet_OM_Contract.sp06090_OM_Site_Contract_Edit.Rows.Clear();  // Clear any existing rows //
                for (int i = 0; i < viewSites.DataRowCount; i++)
                {
                    if (Convert.ToInt32(viewSites.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        intProcessedCount++;
                        DataRow drNewRow;
                        drNewRow = dataSet_OM_Contract.sp06090_OM_Site_Contract_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = "";
                        drNewRow["ClientContractID"] = i_int_SelectedClientContractID;
                        drNewRow["SiteID"] = viewSites.GetRowCellValue(i, "SiteID");
                        drNewRow["StartDate"] = DateTime.Today;  //viewClientContract.GetFocusedRowCellValue("StartDate");
                        drNewRow["EndDate"] = viewClientContract.GetFocusedRowCellValue("EndDate");
                        drNewRow["Active"] = 1;  // viewClientContract.GetFocusedRowCellValue("Active");
                        drNewRow["ContractValue"] = (selection2.SelectedCount == 1 ? viewClientContract.GetFocusedRowCellValue("ContractValue") : (decimal)0.00);
                        drNewRow["YearlyPercentageIncrease"] = viewClientContract.GetFocusedRowCellValue("YearlyPercentageIncrease");
                        drNewRow["DummySiteContractID"] = 0;
                        drNewRow["SiteName"] = viewSites.GetRowCellValue(i, "SiteName");
                        drNewRow["SitePostcode"] = viewSites.GetRowCellValue(i, "SitePostcode");
                        drNewRow["LocationX"] = viewSites.GetRowCellValue(i, "LocationX");
                        drNewRow["LocationY"] = viewSites.GetRowCellValue(i, "LocationY");
                        drNewRow["DummyLabourCount"] = 0;
                        drNewRow["DummyEquipmentCount"] = 0;
                        drNewRow["DummyMaterialCount"] = 0;
                        drNewRow["DummyPersonResponsibilityCount"] = 0;
                        drNewRow["DummyJobRateCount"] = 0;
                        drNewRow["ClientBillingTypeID"] = 1;  // Billing By Profile //
                        drNewRow["DefaultScheduleTemplateID"] = 0;
                        dataSet_OM_Contract.sp06090_OM_Site_Contract_Edit.Rows.Add(drNewRow);                    
                        if (intProcessedCount >= selection2.SelectedCount) break;
                    }
                }
            }
            catch (Exception Ex) { }
            view.EndUpdate();
            CurrentContractValueTextEdit.EditValue = Convert.ToDecimal(viewClientContract.GetFocusedRowCellValue("ContractValue"));
            btnSplitValueOverSites.Enabled = (Convert.ToDecimal(viewClientContract.GetFocusedRowCellValue("ContractValue")) != (decimal)0.00);

            if (checkEditCreateGenericJobRates.Checked)  // Create a generic job rate record for each Site Selected //
            {
                gridControl17.ForceInitialize();
                GridView parentView = (GridView)gridControl17.MainView;                
                parentView.SelectAll();
                Add_Job_Rate(false);
            }

            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep3;
        }

        public override void PostViewClick(object sender, EventArgs e, int row)
        {
            GridView view = (GridView)sender;
            if (view.GridControl.MainView.Name != "gridView2") return;  // Only fire for appropriate view //
            if (row == GridControl.InvalidRowHandle) return;
            Set_Selected_Site_Count_Label();
        }

        private void Set_Selected_Site_Count_Label()
        {
            labelControlSelectedSiteCount.Text = "       " + selection2.SelectedCount.ToString() + " Selected Sites";
        }


        #region GridView2

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bsiRecordTicking.Enabled = (intCount > 0);
                bbiTick.Enabled = (intCount > 0);
                bbiUntick.Enabled = (intCount > 0);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion

        #endregion


        #region Step 3 Page

        private void btnStep3Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep2;
        }
        private void btnStep3Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;
            view.PostEditor();
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  

            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep4;
        }


        #region GridView3

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("block_edit".Equals(e.Button.Tag))
                    {
                        Block_Edit_Site_Contracts();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Site_Contracts();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.SiteEdit;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.SiteEdit;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        private void Block_Edit_Site_Contracts()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl3.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Contracts to block edit then try again.", "Block Edit Site Contracts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Site_Contract_Wizard_Block_Edit_Site_Info();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                view.BeginDataUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    if (fChildForm.dtStartDate != null) view.SetRowCellValue(intRowHandle, "StartDate", fChildForm.dtStartDate);
                    if (fChildForm.dtEndDate != null) view.SetRowCellValue(intRowHandle, "EndDate", fChildForm.dtEndDate);
                    if (fChildForm.intActive != null) view.SetRowCellValue(intRowHandle, "Active", fChildForm.intActive);
                    if (fChildForm.decContractValue != null) view.SetRowCellValue(intRowHandle, "ContractValue", fChildForm.decContractValue);
                    if (fChildForm.decYearlyPercentageIncrease != null) view.SetRowCellValue(intRowHandle, "YearlyPercentageIncrease", fChildForm.decYearlyPercentageIncrease);
                    if (fChildForm.strRemarks != null) view.SetRowCellValue(intRowHandle, "Remarks", fChildForm.strRemarks);
                    if (fChildForm.strSiteCategory != null) view.SetRowCellValue(intRowHandle, "SiteCategory", fChildForm.strSiteCategory);
                    if (fChildForm.intClientBillingTypeID != null) view.SetRowCellValue(intRowHandle, "ClientBillingTypeID", fChildForm.intClientBillingTypeID);
                    if (fChildForm.intDefaultScheduleTemplateID != null) view.SetRowCellValue(intRowHandle, "DefaultScheduleTemplateID", fChildForm.intDefaultScheduleTemplateID);
                    if (fChildForm.decDaysSeparationPercent != null) view.SetRowCellValue(intRowHandle, "DaysSeparationPercent", fChildForm.decDaysSeparationPercent);
                }
                view.EndDataUpdate();
            }
        }

        private void Delete_Site_Contracts()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            view = (GridView)gridControl3.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Contracts to remove by clicking on them then try again.", "Remove Site Contract", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? "1 Site Contract" : Convert.ToString(intRowHandles.Length) + " Site Contracts") + " selected for removing.\n\nProceed?\n\nIf you proceed, no " + (intCount == 1 ? "Site Contract" : "Site Contracts") + " will be created for " + (intCount == 1 ? "this Site" : "these Sites") + ".";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Site Contract", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Deleting...");

                int intChildMax = 0;
                GridView childView = (GridView)gridControl5.MainView;
                int intParentSiteContractID = 0;
                for (int i = intRowHandles.Length - 1; i >= 0; i--)
                {
                    intParentSiteContractID = Convert.ToInt32(view.GetRowCellValue(i, "SiteContractID"));

                    // Take out any linked Billing Profiles //


                    // Take out any linked Contract Years //
                    Clear_Filter_Contract_Years();
                    intChildMax = childView.DataRowCount;
                    for (int j = intChildMax - 1; j >= 0; j--)
                    {
                        if (Convert.ToInt32(childView.GetRowCellValue(j, "SiteContractID")) == intParentSiteContractID) childView.DeleteRow(j);  // Delete child row //
                    }

                    view.DeleteRow(intRowHandles[i]);  // Delete parent row //
                }

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for deleting.", "Remove Site Contract", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnSplitValueOverSites_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;
            view.PostEditor();
            int intRecordCount = view.DataRowCount;
            if (intRecordCount <= 0) return;
            
            decimal decValue = (decimal)CurrentContractValueTextEdit.EditValue;
            if (decValue == (decimal)0.00) return;

            decValue = Math.Floor((decValue / (decimal)intRecordCount) * 100) / 100;  // "* 100) / 100"  -  This forces value down to nearest 2 decimal places //

            view.BeginDataUpdate();
            for (int i = 0; i < intRecordCount; i++)
            {
                view.SetRowCellValue(i, "ContractValue", decValue);
            }
            view.EndDataUpdate();
        }

        #endregion


        #region Step 4 Page
        
        private void btnStep4Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep3;

            Clear_Filter_Contract_Year_On_Site();
        }
        private void btnStep4Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl4.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  

            Clear_Filter_Contract_Year_On_Site();

            view = (GridView)gridControl6.MainView;
            view.ExpandAllGroups();

            // Clear Any Contract Year Billing Profile Records already created //
            view = (GridView)gridControl7.MainView;
            view.BeginUpdate();
            try
            {
                dataSet_OM_Contract.sp06095_OM_Site_Contract_Year_Billing_Profile_Edit.Rows.Clear();  // Clear any existing rows //
            }
            catch (Exception Ex) { }
            view.EndUpdate();

            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep5;
        }

        private void Clear_Filter_Contract_Years()
        {
            GridView view = (GridView)gridControl5.MainView;
            view.PostEditor();

            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
            SetMenuStatus();
        }


        #region GridView4

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView5

        private void gridControl5_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Contract_Year();
                    }
                    else if ("block_edit".Equals(e.Button.Tag))
                    {
                        Block_Edit_Site_Contract_Years();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Contract_Year();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.SiteYearEdit;
            SetMenuStatus();
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.SiteYearEdit;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        private void spinEditContractYears_EditValueChanged(object sender, EventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            btnAddYearsBtn.Enabled = (Convert.ToInt32(se.EditValue) > 0);
        }

        private void btnAddYearsBtn_Click(object sender, EventArgs e)
        {
            int intYears = Convert.ToInt32(spinEditContractYears.EditValue);
            if (intYears <= 0)
            {
                XtraMessageBox.Show("Please enter a value greater than 0 for the number of Years to add to the grid before proceeding.", "Add Site Contract Years", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            GridView viewSites = (GridView)gridControl4.MainView;
            int[] intRowHandles = viewSites.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Sites to add the Contract Years to before proceeding.", "Add Site Contract Years", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intRowHandles.Length > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + " Site Contracts selected. If you proceed " + intYears.ToString() + " new contracts Years will be created for each of these records.\n\nProceed?", "Add Site Contract Years", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }

            GridView view = (GridView)gridControl5.MainView;
            view.PostEditor();
            int intSiteContractID = 0;
            int intYearCount = 0;
            string strFilterCondition = "";
            DateTime dtStartDate = DateTime.MinValue;
            DateTime dtMaxStartDate = DateTime.MinValue;
            DateTime dtEndDate = DateTime.MinValue;
            string strSiteName = "";

            decimal decValue = (decimal)0.00;
            if (decValue > (decimal)0.00) decValue = decValue / (decimal)intYears;
            decimal decValueAmountUsed = (decimal)0.00;

            view.BeginUpdate();  // Lock view so we don't see filter switching on and off //
            foreach (int intRowHandle in intRowHandles)
            {
                intSiteContractID = Convert.ToInt32(viewSites.GetRowCellValue(intRowHandle, "SiteContractID"));
                dtStartDate = Convert.ToDateTime(viewSites.GetRowCellValue(intRowHandle, "StartDate"));  // Contract Start //
                decValue = Convert.ToDecimal(viewSites.GetRowCellValue(intRowHandle, "ContractValue"));  // Contract Value //
                strSiteName = viewSites.GetRowCellValue(intRowHandle, "SiteName").ToString();
                decValueAmountUsed = (decimal)0.00;

                // Filter Year grid to the current Site so we can get an accurate starting date //
                strFilterCondition = "[SiteContractID] = " + intSiteContractID.ToString();
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = strFilterCondition;

                intYearCount = view.DataRowCount;
                if (intYearCount > 0)
                {
                    dtMaxStartDate = DateTime.MinValue;
                    for (int i = 0; i < view.DataRowCount; i++)
                    {
                        if (Convert.ToDateTime(view.GetRowCellValue(i, "StartDate")) > dtMaxStartDate) dtMaxStartDate = Convert.ToDateTime(view.GetRowCellValue(i, "StartDate"));
                        decValueAmountUsed += Convert.ToDecimal(view.GetRowCellValue(i, "ClientValue"));
                    }
                    dtStartDate = (dtMaxStartDate == DateTime.MinValue ? DateTime.Today : dtMaxStartDate.AddYears(1));
                }
                dtEndDate = dtStartDate.AddYears(1).AddDays(-1);
                view.ActiveFilter.Clear();  // Remove filter now the dates are calculated //

                if ((decValue - decValueAmountUsed) > (decimal)0.00)
                {
                    decValue = (decValue - decValueAmountUsed) / (decimal)(intYears);
                }
                else
                {
                    decValue = (decimal)0.00;
                }

                for (int i = 0; i < intYears; i++)
                {
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_OM_Contract.sp06092_OM_Site_Contract_Year_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = "";
                        drNewRow["SiteContractID"] = intSiteContractID;
                        drNewRow["YearDescription"] = "Year: " + dtStartDate.Year.ToString();
                        drNewRow["StartDate"] = dtStartDate;
                        drNewRow["EndDate"] = dtEndDate;
                        drNewRow["Active"] = (DateTime.Today.Year == dtStartDate.Year ? 1 : 0);
                        drNewRow["ClientValue"] = decValue;
                        drNewRow["CreatedFromClientContractYearID"] = 0;
                        drNewRow["CreatedFromClientContractYear"] = 0;
                        drNewRow["DummySiteContractYearID"] = 0;
                        drNewRow["DummySiteContractYearID"] = 0;
                        drNewRow["YearlyPercentageIncrease"] = (decimal)0.00;
                        drNewRow["SiteName"] = strSiteName;
                        dataSet_OM_Contract.sp06092_OM_Site_Contract_Year_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception Ex) { }
                    dtStartDate = dtEndDate.AddDays(1);
                    dtEndDate = (i + 1 < intYears ? dtStartDate.AddYears(1).AddDays(-1) : dtEndDate);
                }
            }
            Filter_Contract_Year_On_Site();  // Reapply Filter //
            view.EndUpdate();  // UnLock view //
            view.ExpandAllGroups();
        }
        
        private void Add_Contract_Year()
        {
            GridView viewSites = (GridView)gridControl4.MainView;
            int[] intRowHandles = viewSites.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Sites to add the Contract Years to before proceeding.", "Add Site Contract Years", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intRowHandles.Length > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + " Site Contracts selected. If you proceed a new Contract Year will be created for each of these records.\n\nProceed?", "Add Site Contract Years", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }

            GridView view = (GridView)gridControl5.MainView;
            view.PostEditor();
            int intSiteContractID = 0;
            int intYearCount = 0;
            string strFilterCondition = "";
            DateTime dtStartDate = DateTime.MinValue;
            DateTime dtMaxStartDate = DateTime.MinValue;
            DateTime dtEndDate = DateTime.MinValue;
            string strSiteName = "";
            
            decimal decValue = (decimal)0.00;
            decimal decValueAmountUsed = (decimal)0.00;

            view.BeginUpdate();  // Lock view so we don't see filter switching on and off //
            foreach (int intRowHandle in intRowHandles)
            {
                intSiteContractID = Convert.ToInt32(viewSites.GetRowCellValue(intRowHandle, "SiteContractID"));
                dtStartDate = Convert.ToDateTime(viewSites.GetRowCellValue(intRowHandle, "StartDate"));  // Contract Start //
                decValue = Convert.ToDecimal(viewSites.GetRowCellValue(intRowHandle, "ContractValue"));  // Contract Value //
                strSiteName = viewSites.GetRowCellValue(intRowHandle, "SiteName").ToString();
                decValueAmountUsed = (decimal)0.00;
              
                // Filter Year grid to the current Site so we can get an accurate starting date //
                strFilterCondition = "[SiteContractID] = " + intSiteContractID.ToString();
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = strFilterCondition;

                intYearCount = view.DataRowCount;
                if (intYearCount > 0)
                {
                    dtMaxStartDate = DateTime.MinValue;
                    for (int i = 0; i < view.DataRowCount; i++)
                    {
                        if (Convert.ToDateTime(view.GetRowCellValue(i, "StartDate")) > dtMaxStartDate) dtMaxStartDate = Convert.ToDateTime(view.GetRowCellValue(i, "StartDate"));
                        decValueAmountUsed += Convert.ToDecimal(view.GetRowCellValue(i, "ClientValue"));
                    }
                    dtStartDate = (dtMaxStartDate == DateTime.MinValue ? DateTime.Today : dtMaxStartDate.AddYears(1));
                }
                dtEndDate = dtStartDate.AddYears(1).AddDays(-1);
                view.ActiveFilter.Clear();  // Remove filter now the dates are calculated //

                if ((decValue - decValueAmountUsed) > (decimal)0.00)
                {
                    decValue = (decValue - decValueAmountUsed) / (decimal)1;
                }
                else
                {
                    decValue = (decimal)0.00;
                }

                try
                {
                    DataRow drNewRow;
                    drNewRow = dataSet_OM_Contract.sp06092_OM_Site_Contract_Year_Edit.NewRow();
                    drNewRow["strMode"] = "add";
                    drNewRow["strRecordIDs"] = "";
                    drNewRow["SiteContractID"] = intSiteContractID;
                    drNewRow["YearDescription"] = "Year: " + dtStartDate.Year.ToString();
                    drNewRow["StartDate"] = dtStartDate;
                    drNewRow["EndDate"] = dtEndDate;
                    drNewRow["Active"] = (DateTime.Today.Year == dtStartDate.Year ? 1 : 0);
                    drNewRow["ClientValue"] = decValue;
                    drNewRow["CreatedFromClientContractYearID"] = 0;
                    drNewRow["CreatedFromClientContractYear"] = 0;
                    drNewRow["DummySiteContractYearID"] = 0;
                    drNewRow["YearlyPercentageIncrease"] = (decimal)0.00;
                    drNewRow["SiteName"] = strSiteName;
                    dataSet_OM_Contract.sp06092_OM_Site_Contract_Year_Edit.Rows.Add(drNewRow);
                }
                catch (Exception Ex) { }
            }
            Filter_Contract_Year_On_Site();  // Reapply Filter //
            view.EndUpdate();  // UnLock view //
            view.ExpandAllGroups();
        }

        private void CopyClientContractYearsSimpleButton_Click(object sender, EventArgs e)
        {
            GridView viewSites = (GridView)gridControl4.MainView;
            int[] intRowHandles = viewSites.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Sites to copy the parent Client Contract's Contract Years to before proceeding.", "Copy Client Contract Years to Site Contracts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to copy the parent Client Contract's Contract Years to the selected Site Contracts.\n\nIf you proceed one or more new Contract Years will be created for each of the selected Site Contracts.\n\nProceed?", "Copy Client Contract Years to Site Contracts", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            
            SqlDataAdapter sdaYears = new SqlDataAdapter();
            DataSet dsYears = new DataSet("NewDataSet");
            try
            {
                string strClientContractID = i_int_SelectedClientContractID.ToString() + ",";
                SqlConnection SQlConn = new SqlConnection(strConnectionString);
                SqlCommand cmd = null;
                cmd = new SqlCommand("sp06094_OM_Client_Contract_Years_Linked_To_Client_Contract", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ClientContractIDs", strClientContractID));
                sdaYears = new SqlDataAdapter(cmd);
                sdaYears.Fill(dsYears, "Table");
                SQlConn = null;
                cmd = null;
            }
            catch (Exception Ex) { return; }

            GridView view = (GridView)gridControl5.MainView;
            view.PostEditor();
            int intSiteContractID = 0;
            decimal decValue = (decimal)0.00;
            string strSiteName = "";

            view.BeginUpdate();  // Lock view so we don't see filter switching on and off //
            foreach (int intRowHandle in intRowHandles)
            {
                intSiteContractID = Convert.ToInt32(viewSites.GetRowCellValue(intRowHandle, "SiteContractID"));
                decValue = Convert.ToDecimal(viewSites.GetRowCellValue(intRowHandle, "ContractValue"));  // Contract Value //
                strSiteName = viewSites.GetRowCellValue(intRowHandle, "SiteName").ToString();
                foreach (DataRow dr in dsYears.Tables[0].Rows)
                {
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_OM_Contract.sp06092_OM_Site_Contract_Year_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = "";
                        drNewRow["SiteContractID"] = intSiteContractID;
                        drNewRow["YearDescription"] = dr["YearDescription"];
                        drNewRow["StartDate"] = dr["StartDate"];
                        drNewRow["EndDate"] = dr["EndDate"];
                        drNewRow["Active"] = dr["Active"];
                        drNewRow["ClientValue"] = (Convert.ToDecimal(dr["ContractValuePercentage"]) > (decimal)0.00 ? decValue * Convert.ToDecimal(dr["ContractValuePercentage"]) : (decimal)0.00);
                        drNewRow["CreatedFromClientContractYearID"] = dr["ClientContractYearID"];
                        drNewRow["CreatedFromClientContractYear"] = 1;
                        drNewRow["DummySiteContractYearID"] = 0;
                        drNewRow["YearlyPercentageIncrease"] = dr["YearlyPercentageIncrease"];
                        drNewRow["SiteName"] = strSiteName;
                        dataSet_OM_Contract.sp06092_OM_Site_Contract_Year_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception Ex) { }
                }
            }
            view.EndUpdate();  // UnLock view //
            view.ExpandAllGroups();
        }

        private void Block_Edit_Site_Contract_Years()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl5.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Contract Years to block edit then try again.", "Block Edit Site Contract Years", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Site_Contract_Wizard_Block_Edit_Site_Year_Info();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                view.BeginDataUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    if (fChildForm.dtStartDate != null) view.SetRowCellValue(intRowHandle, "StartDate", fChildForm.dtStartDate);
                    if (fChildForm.dtEndDate != null) view.SetRowCellValue(intRowHandle, "EndDate", fChildForm.dtEndDate);
                    if (fChildForm.strYearDescription != null) view.SetRowCellValue(intRowHandle, "YearDescription", fChildForm.strYearDescription);
                    if (fChildForm.intActive != null) view.SetRowCellValue(intRowHandle, "Active", fChildForm.intActive);
                    if (fChildForm.decContractValue != null) view.SetRowCellValue(intRowHandle, "ClientValue", fChildForm.decContractValue);
                    if (fChildForm.strRemarks != null) view.SetRowCellValue(intRowHandle, "Remarks", fChildForm.strRemarks);
                    if (fChildForm.decYearlyPercentageIncrease != null) view.SetRowCellValue(intRowHandle, "YearlyPercentageIncrease", fChildForm.decYearlyPercentageIncrease);
                }
                view.EndDataUpdate();
            }
        }

        private void Delete_Contract_Year()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            view = (GridView)gridControl5.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Contract Years to remove from the Contract by clicking on them then try again.", "Remove Linked Contract Years from Contract", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? "1 Linked Contract Year" : Convert.ToString(intRowHandles.Length) + " Linked Contract Years") + " selected for removing from the current Contract!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Contract Year" : "these Linked Contract Years") + " will be removed from the Contract. Any Linked Billing Profiles will also be removed.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Contract Year from Client Contract", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Deleting...");

                int intChildMax = 0;
                GridView childView = (GridView)gridControl7.MainView;
                int intParentSiteContractYearID = 0;
                for (int i = intRowHandles.Length - 1; i >= 0; i--)
                {
                    intParentSiteContractYearID = Convert.ToInt32(view.GetRowCellValue(i, "SiteContractYearID"));

                    // Take out any linked Billing Profile first //
                    Clear_Filter_Billing_Profile();
                    intChildMax = childView.DataRowCount;
                    for (int j = intChildMax - 1; j >= 0; j--)
                    {
                        if (Convert.ToInt32(childView.GetRowCellValue(j, "SiteContractYearID")) == intParentSiteContractYearID)
                        {
                            childView.DeleteRow(j);  // Delete child row //
                        }
                    }

                    view.DeleteRow(intRowHandles[i]);  // Delete parent row //
                }

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for unlinking from Contract.", "Remove Linked Contract Years from Contract", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Filter_Contract_Year_On_Site()
        {
            GridView view = (GridView)gridControl5.MainView;
            view.PostEditor();

            string strFilterCondition = "";
            GridView parentView = (GridView)gridControl4.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                strFilterCondition = "[SiteContractID] = 9999999999";  // Don't show anything //
            }
            else
            {
                foreach (int i in intRowHandles)
                {
                    strFilterCondition += (string.IsNullOrWhiteSpace(strFilterCondition) ? "" : " or ");
                    strFilterCondition += "[SiteContractID] = " + parentView.GetRowCellValue(i, "SiteContractID").ToString();
                }
            }
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = strFilterCondition;
            view.MakeRowVisible(-1, true);
            view.ExpandAllGroups();
            view.EndUpdate();
            SetMenuStatus();
        }

        private void Clear_Filter_Contract_Year_On_Site()
        {
            GridView view = (GridView)gridControl5.MainView;
            view.PostEditor();

            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
            SetMenuStatus();
        }

        #endregion


        #region Step 5 Page

        private void btnStep5Previous_Click(object sender, EventArgs e)
        {
            Filter_Contract_Year_On_Site();

            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep4;
        }
        private void btnStep5Next_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep6;
        }


        #region GridView6

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            SetMenuStatus();
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView7

        private void gridControl7_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Billing_Profile();
                    }
                    else if ("block edit".Equals(e.Button.Tag))
                    {
                        Block_Edit_Billing_Profile();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Billing_Profile();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView7_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.SiteYearBilling;
            SetMenuStatus();
        }

        private void gridView7_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.SiteYearBilling;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        private void bbiAddProfile_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Add_Billing_Profile();
        }

        private void bbiCopyClientContractYearBillingProfile_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView viewSiteYears = (GridView)gridControl6.MainView;
            int[] intRowHandles = viewSiteYears.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Sites Years to copy the parent Client Contract Year's Billing Profile to before proceeding.", "Copy Client Contract Year Billing Profile to Site Contract Years", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // De-select any years which have not been created from a Client Contract Year // 
            foreach (int intRowHandle in intRowHandles)
            {
                if (Convert.ToInt32(viewSiteYears.GetRowCellValue(intRowHandle, "CreatedFromClientContractYearID")) == 0) viewSiteYears.UnselectRow(intRowHandle);
            }

            intRowHandles = viewSiteYears.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Sites Years to copy the parent Client Contract Year's Billing Profile to before proceeding.\n\nNote: This process automatically de-selects any rows which were not created from a parent Client Contract Year.", "Copy Client Contract Year Billing Profile to Site Contract Years", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to copy the parent Client Contract Year's Billing Profile to the selected Site Contract Years.\n\nIf you proceed one or more new Contract Year Billing Profiles will be created for each of the selected Site Contract Years.\n\nProceed?", "Copy Client Contract Year Billing Profile to Site Contract Years", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;

            SqlDataAdapter sdaYearProfiles = new SqlDataAdapter();
            DataSet dsYearProfiles = new DataSet("NewDataSet");
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            string strClientContractYearIDs = "";

            GridView view = (GridView)gridControl7.MainView;
            view.PostEditor();
            int intCreatedFromClientContractYearID = 0;
            decimal decValue = (decimal)0.00;

            view.BeginUpdate();  // Lock view so we don't see filter switching on and off //
            foreach (int intRowHandle in intRowHandles)
            {
                intCreatedFromClientContractYearID = Convert.ToInt32(viewSiteYears.GetRowCellValue(intRowHandle, "CreatedFromClientContractYearID"));
                strClientContractYearIDs = intCreatedFromClientContractYearID.ToString() + ",";
                try
                {
                    cmd = null;
                    dsYearProfiles.Clear();  // Clear any prior result set //
                    cmd = new SqlCommand("sp06097_OM_Client_Contract_Billing_Profiles_Linked_To_Client_Contract_Year", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ClientContractYearIDs", strClientContractYearIDs));
                    sdaYearProfiles = new SqlDataAdapter(cmd);
                    
                    sdaYearProfiles.Fill(dsYearProfiles, "Table");
                }
                catch (Exception Ex) 
                {
                    view.EndUpdate();  // UnLock view //
                    return;
                }

                decValue = Convert.ToDecimal(viewSiteYears.GetRowCellValue(intRowHandle, "ClientValue"));
                foreach (DataRow dr in dsYearProfiles.Tables[0].Rows)
                {
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_OM_Contract.sp06095_OM_Site_Contract_Year_Billing_Profile_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = "";
                        drNewRow["SiteContractYearID"] = Convert.ToInt32(viewSiteYears.GetRowCellValue(intRowHandle, "SiteContractYearID"));
                        drNewRow["InvoiceDateDue"] = dr["InvoiceDateDue"];
                        drNewRow["EstimatedBillAmount"] = (Convert.ToDecimal(dr["YearValuePercentage"]) > (decimal)0.00 ? decValue * Convert.ToDecimal(dr["YearValuePercentage"]) : (decimal)0.00);
                        drNewRow["InvoiceDateActual"] = dr["InvoiceDateActual"];
                        drNewRow["ActualBillAmount"] = (decimal)0.00;
                        drNewRow["BilledByPersonID"] = 0;
                        drNewRow["LinkedToParent"] = (string.IsNullOrWhiteSpace(viewSiteYears.GetRowCellValue(intRowHandle, "StartDate").ToString()) ? "Unknown Start" : Convert.ToDateTime(viewSiteYears.GetRowCellValue(intRowHandle, "StartDate")).ToString("dd/MM/yyyy")) + " - " + (string.IsNullOrWhiteSpace(viewSiteYears.GetRowCellValue(intRowHandle, "EndDate").ToString()) ? "Unknown End" : Convert.ToDateTime(viewSiteYears.GetRowCellValue(intRowHandle, "EndDate")).ToString("dd/MM/yyyy"));
                        drNewRow["SiteName"] = viewSiteYears.GetRowCellValue(intRowHandle, "SiteName").ToString();
                        drNewRow["CreatedFromClientContractProfileID"] = dr["ClientContractProfileID"];
                        drNewRow["CreatedFromClientContractProfile"] = 1;
                        dataSet_OM_Contract.sp06095_OM_Site_Contract_Year_Billing_Profile_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception Ex) { }
                }
            }
            SQlConn = null;
            cmd = null;
            view.EndUpdate();  // UnLock view //
            view.ExpandAllGroups();
        }

        private void bbiAddProfileFromTemplate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView parentView = (GridView)gridControl6.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Contract Years to add the Billing Profile Template to by clicking on them then try again.", "Add Billing Profile From Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intCount > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intCount.ToString() + " Contract Years selected. If you proceed a new Billing Profile will be created for each of these records from the chosen Template.\n\nProceed?", "Add Billing Profile From Template", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }
            GridView view = (GridView)gridControl7.MainView;
            view.PostEditor();

            var fChildForm = new frm_OM_Select_Template_Billing_Profile_Header();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                int intTemplateID = fChildForm.intSelectedTemplateHeaderID;
                if (intTemplateID <= 0) return;

                GridControl gridControlTemp = fChildForm.gridControl2;
                GridView viewTemp = (GridView)gridControlTemp.MainView;
                int intRowCount = viewTemp.DataRowCount;
                if (intCount <= 0) return;

                view.PostEditor();
                view.BeginUpdate();
                try
                {
                    decimal decValuePercentage = (decimal)0.00;
                    DateTime dtCalculatedDate;
                    int intUnits = 0;
                    int intUnitDescriptor = 0;
                    foreach (int i in intRowHandles)  // Selected Years //
                    {
                        for (int j = 0; j < intRowCount; j++)  // Template Items //
                        {
                            DataRow drNewRow;
                            drNewRow = dataSet_OM_Contract.sp06095_OM_Site_Contract_Year_Billing_Profile_Edit.NewRow();
                            drNewRow["strMode"] = "add";
                            drNewRow["strRecordIDs"] = "";
                            drNewRow["SiteContractYearID"] = Convert.ToInt32(parentView.GetRowCellValue(i, "SiteContractYearID"));

                            if (!string.IsNullOrWhiteSpace(parentView.GetRowCellValue(i, "StartDate").ToString()))
                            {
                                intUnits = Convert.ToInt32(viewTemp.GetRowCellValue(j, "UnitsFromStart"));
                                intUnitDescriptor = Convert.ToInt32(viewTemp.GetRowCellValue(j, "UnitsFromStartDescriptorID"));
                                if (intUnits > 0 && intUnitDescriptor > 0)
                                {
                                    dtCalculatedDate = Convert.ToDateTime(parentView.GetRowCellValue(i, "StartDate"));
                                    switch (intUnitDescriptor)
                                    {
                                        case 1:  // Days //
                                            {
                                                dtCalculatedDate = dtCalculatedDate.AddDays(intUnits);
                                            }
                                            break;
                                        case 2:  // Weeks //
                                            {
                                                dtCalculatedDate = dtCalculatedDate.AddDays(intUnits * 7);
                                            }
                                            break;
                                        case 3:  // Month //
                                            {
                                                dtCalculatedDate = dtCalculatedDate.AddMonths(intUnits);
                                            }
                                            break;
                                        case 4:  // Years //
                                            {
                                                dtCalculatedDate = dtCalculatedDate.AddYears(intUnits);
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                    drNewRow["InvoiceDateDue"] = dtCalculatedDate;
                                }
                            }
                            decValuePercentage = Convert.ToDecimal(viewTemp.GetRowCellValue(j, "ValuePercentage"));
                            drNewRow["EstimatedBillAmount"] = (decValuePercentage > (decimal)0.00 ? Convert.ToDecimal(parentView.GetRowCellValue(i, "ClientValue")) * (decValuePercentage / 100) : (decimal)0.00);
                            drNewRow["ActualBillAmount"] = (decimal)0.00;
                            drNewRow["BilledByPersonID"] = 0;
                            drNewRow["LinkedToParent"] = (string.IsNullOrWhiteSpace(parentView.GetRowCellValue(i, "StartDate").ToString()) ? "Unknown Start" : Convert.ToDateTime(parentView.GetRowCellValue(i, "StartDate")).ToString("dd/MM/yyyy")) + " - " + (string.IsNullOrWhiteSpace(parentView.GetRowCellValue(i, "EndDate").ToString()) ? "Unknown End" : Convert.ToDateTime(parentView.GetRowCellValue(i, "EndDate")).ToString("dd/MM/yyyy"));
                            drNewRow["SiteName"] = parentView.GetRowCellValue(i, "SiteName").ToString();
                            drNewRow["CreatedFromClientContractProfileID"] = 0;
                            drNewRow["CreatedFromClientContractProfile"] = 0;
                            dataSet_OM_Contract.sp06095_OM_Site_Contract_Year_Billing_Profile_Edit.Rows.Add(drNewRow);
                        }
                    }
                }
                catch (Exception Ex) { }
                view.ExpandAllGroups();
                view.EndUpdate();
            }
        }

        private void bbiEditTemplates_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var fChildForm = new frm_OM_Template_Billing_Profile_Manager();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();
            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void Add_Billing_Profile()
        {
            GridView parentView = (GridView)gridControl6.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Years to add the Billing Profile to by clicking on them then try again.", "Add Billing Profile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intCount > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intCount.ToString() + " Site Years selected. If you proceed a new Billing Profile will be created for each of these records.\n\nProceed?", "Add Billing Profile", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }
            GridView view = (GridView)gridControl7.MainView;
            view.PostEditor();
            view.BeginDataUpdate();
            try
            {
                foreach (int i in intRowHandles)
                {
                    DataRow drNewRow;
                    drNewRow = dataSet_OM_Contract.sp06095_OM_Site_Contract_Year_Billing_Profile_Edit.NewRow();
                    drNewRow["strMode"] = "add";
                    drNewRow["strRecordIDs"] = "";
                    drNewRow["SiteContractYearID"] = Convert.ToInt32(parentView.GetRowCellValue(i, "SiteContractYearID"));
                    drNewRow["EstimatedBillAmount"] = (decimal)0.00;
                    drNewRow["ActualBillAmount"] = (decimal)0.00;
                    drNewRow["BilledByPersonID"] = 0;
                    drNewRow["LinkedToParent"] = (string.IsNullOrWhiteSpace(parentView.GetRowCellValue(i, "StartDate").ToString()) ? "Unknown Start" : Convert.ToDateTime(parentView.GetRowCellValue(i, "StartDate")).ToString("dd/MM/yyyy")) + " - " + (string.IsNullOrWhiteSpace(parentView.GetRowCellValue(i, "EndDate").ToString()) ? "Unknown End" : Convert.ToDateTime(parentView.GetRowCellValue(i, "EndDate")).ToString("dd/MM/yyyy"));
                    drNewRow["SiteName"] = parentView.GetRowCellValue(i, "SiteName").ToString();
                    drNewRow["CreatedFromClientContractProfileID"] = 0;
                    drNewRow["CreatedFromClientContractProfile"] = 0;
                    dataSet_OM_Contract.sp06095_OM_Site_Contract_Year_Billing_Profile_Edit.Rows.Add(drNewRow);
                }
            }
            catch (Exception Ex) { }
            view.ExpandAllGroups();
            view.EndDataUpdate();
        }

        private void Block_Edit_Billing_Profile()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl7.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Contract Year Billing Profiles to block edit then try again.", "Block Edit Site Contract Year Billing Profiles", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            
            var fChildForm = new frm_OM_Site_Contract_Wizard_Block_Edit_Site_Year_Billing_Info();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                view.BeginDataUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    if (fChildForm.dtInvoiceDateDue != null) view.SetRowCellValue(intRowHandle, "InvoiceDateDue", fChildForm.dtInvoiceDateDue);
                    if (fChildForm.decEstimatedBillAmount != null) view.SetRowCellValue(intRowHandle, "EstimatedBillAmount", fChildForm.decEstimatedBillAmount);
                    if (fChildForm.dtInvoiceDateActual != null) view.SetRowCellValue(intRowHandle, "InvoiceDateActual", fChildForm.dtInvoiceDateActual);
                    if (fChildForm.decActualBillAmount != null) view.SetRowCellValue(intRowHandle, "ActualBillAmount", fChildForm.decActualBillAmount);
                    if (fChildForm.strRemarks != null) view.SetRowCellValue(intRowHandle, "Remarks", fChildForm.strRemarks);
                }
                view.EndDataUpdate();
            }
        }

        private void Delete_Billing_Profile()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            view = (GridView)gridControl7.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Billing Profiles to remove from the Site Year by clicking on them then try again.", "Remove Linked Billing Profile from Site Year", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? "1 Linked Billing Profile" : Convert.ToString(intRowHandles.Length) + " Linked Billing Profiles") + " selected for removing from the current Site Year!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Billing Profile" : "these Linked Billing Profiles") + " will be removed from the Site Year.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Billing Profile from Site Year", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Deleting...");

                for (int i = intRowHandles.Length - 1; i >= 0; i--)
                {
                    view.DeleteRow(intRowHandles[i]);
                }

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for unlinking from Site Year.", "Remove Linked Billing Profile from Site Year", MessageBoxButtons.OK, MessageBoxIcon.Information);
                SetMenuStatus();
            }
        }

        private void Filter_Billing_Profile_On_Year()
        {
            GridView view = (GridView)gridControl7.MainView;
            view.PostEditor();

            string strFilterCondition = "";
            GridView parentView = (GridView)gridControl6.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                strFilterCondition = "[SiteContractYearID] = 9999999999";
            }
            else
            {
                foreach (int i in intRowHandles)
                {
                    strFilterCondition += (string.IsNullOrWhiteSpace(strFilterCondition) ? "" : " or ");
                    strFilterCondition += "[SiteContractYearID] = " + parentView.GetRowCellValue(i, "SiteContractYearID").ToString();
                }
            }
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = strFilterCondition;
            view.MakeRowVisible(-1, true);
            view.ExpandAllGroups();
            view.EndUpdate();
            SetMenuStatus();
        }

        private void Clear_Filter_Billing_Profile()
        {
            GridView view = (GridView)gridControl7.MainView;
            view.PostEditor();

            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
            SetMenuStatus();
        }

        #endregion


        #region Step 6 Page

        private void btnStep6Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep5;
        }
        private void btnStep6Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl10.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            Clear_Filter_Preferred_labour();
            if (checkEditSkipEquipment.Checked)
            {
                if (checkEditSkipMaterials.Checked)
                {
                    xtraTabControl1.SelectedTabPage = this.xtraTabPageStep9;  // Skip 2 pages //
                }
                else
                {
                    xtraTabControl1.SelectedTabPage = this.xtraTabPageStep8;  // Skip 1 page //
                }
            }
            else
            {
                xtraTabControl1.SelectedTabPage = this.xtraTabPageStep7;  // Don't skip any pages //
            }
        }


        #region GridView9

        private void gridView9_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "DummyLabourCount")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DummyLabourCount")) <= 0)
                {
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridView9_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            SetMenuStatus();
        }

        private void gridView9_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView10

        private void gridControl10_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Preferred_labour();
                    }
                    else if ("block edit".Equals(e.Button.Tag))
                    {
                        Block_Edit_Preferred_labour();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Preferred_labour();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView10_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.PreferredLabour;
            SetMenuStatus();
        }

        private void gridView10_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.PreferredLabour;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemButtonEditLabourType_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06098OMSiteContractLabourCostEditBindingSource.Current;
                var currentRow = (DataSet_OM_Contract.sp06098_OM_Site_Contract_Labour_Cost_EditRow)currentRowView.Row;
                var fChildForm = new frm_Core_Select_Person_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (currentRow == null) return;
                int intOriginalPersonTypeID = 0;
                try { intOriginalPersonTypeID = (string.IsNullOrEmpty(currentRow.LinkedToPersonTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.LinkedToPersonTypeID)); }
                catch (Exception) { }

                fChildForm.intOriginalPersonTypeID = intOriginalPersonTypeID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.LinkedToPersonTypeID = fChildForm.intSelectedPersonTypeID;
                    currentRow.LinkedToPersonType = fChildForm.strSelectedPersonType;

                    if (intOriginalPersonTypeID != fChildForm.intSelectedPersonTypeID)  // Clear selected person since the person type has changed //
                    {
                        currentRow.ContractorID = 0;
                        currentRow.ContractorName = "";
                        currentRow.PostcodeSiteDistance = (double)0.00;
                        currentRow.LatLongSiteDistance = (double)0.00;
                        //dxErrorProvider1.SetError(LinkedToContractorName, "Select a value.");
                    }
                    sp06098OMSiteContractLabourCostEditBindingSource.EndEdit();
                    GridView view = (GridView)gridControl10.MainView;
                    view.FocusedColumn = colContractorName;  // Move focus to next column so new value is shown in original column //
                }
            }
        }

        private void repositoryItemButtonEditChooseContractor_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06098OMSiteContractLabourCostEditBindingSource.Current;
                var currentRow = (DataSet_OM_Contract.sp06098_OM_Site_Contract_Labour_Cost_EditRow)currentRowView.Row;
                if (currentRow == null) return;

                int intPersonTypeID = -1;
                try { intPersonTypeID = (string.IsNullOrEmpty(currentRow.LinkedToPersonTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.LinkedToPersonTypeID)); }
                catch (Exception) { }

                if (intPersonTypeID <= -1)
                {
                    XtraMessageBox.Show("Select the Person Type by clicking the Choose button on the Labour Type field before proceeding.", "Select Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                if (intPersonTypeID == 1)  // Contractor //
                {
                    int intOriginalContractorID = (string.IsNullOrEmpty(currentRow.ContractorID.ToString()) ? 0 : Convert.ToInt32(currentRow.ContractorID));
                    var fChildForm = new frm_OM_Select_Contractor();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.intOriginalContractorID = intOriginalContractorID;
                    fChildForm._PassedInLatitude = currentRow.SiteLocationX;
                    fChildForm._PassedInLongitude = currentRow.SiteLocationY;
                    fChildForm._PassedInPostcode = currentRow.SitePostcode;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        if (fChildForm.intSelectedContractorID == intOriginalContractorID) return;
                        currentRow.ContractorID = fChildForm.intSelectedContractorID;
                        currentRow.ContractorName = fChildForm.strSelectedContractorName;

                        // Perform Distance Calculation //
                        double dbLatitude = fChildForm.dbSelectedLatitude;
                        double dbLongitude = fChildForm.dbSelectedLongitude;
                        string strPostcode = fChildForm.strSelectedPostcode;

                        double dbPostcodeDistance = (double)0.00;
                        double dbLatLongDistance = (double)0.00;
                        if (!string.IsNullOrWhiteSpace(strPostcode) && !string.IsNullOrWhiteSpace(currentRow.SitePostcode))
                        {
                            using (var CalcPostcodeDistance = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                CalcPostcodeDistance.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    dbPostcodeDistance = Convert.ToDouble(CalcPostcodeDistance.sp06102_OM_Calculate_Distance_Between_Postcodes(strPostcode, currentRow.SitePostcode));
                                }
                                catch (Exception) { }
                            }
                        }
                        try
                        {
                            dbLatLongDistance = GeoCodeCalc.CalcDistance(dbLatitude, dbLongitude, currentRow.SiteLocationX, currentRow.SiteLocationY);  // Calculate Distance in Miles //
                        }
                        catch (Exception) { }

                        currentRow.PostcodeSiteDistance = (dbPostcodeDistance > (double)2500 ? (double)0.00 : dbPostcodeDistance);
                        currentRow.LatLongSiteDistance = (dbLatLongDistance > (double)2500 ? (double)0.00 : dbLatLongDistance);
                        sp06098OMSiteContractLabourCostEditBindingSource.EndEdit();
                        GridView view = (GridView)gridControl10.MainView;
                        view.FocusedColumn = colCostUnitDescriptorID;  // Move focus to next column so new value is shown in original column //
                    }
                }
                else  // Staff //
                {
                    var fChildForm = new frm_HR_Select_Staff();
                    fChildForm.GlobalSettings = this.GlobalSettings;

                    if (strFormMode == "blockadd" || strFormMode == "blockedit")
                    {
                        fChildForm.intOriginalStaffID = 0;
                    }
                    else
                    {
                        fChildForm.intOriginalStaffID = (string.IsNullOrEmpty(currentRow.ContractorID.ToString()) ? 0 : Convert.ToInt32(currentRow.ContractorID));
                    }

                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        currentRow.ContractorID = fChildForm.intSelectedStaffID;
                        currentRow.ContractorName = fChildForm.strSelectedStaffName;
                        currentRow.PostcodeSiteDistance = (double)0.00;
                        currentRow.LatLongSiteDistance = (double)0.00;
                        sp06098OMSiteContractLabourCostEditBindingSource.EndEdit();
                        GridView view = (GridView)gridControl10.MainView;
                        view.FocusedColumn = colCostUnitDescriptorID;  // Move focus to next column so new value is shown in original column //
                    }
                }
            }
        }

        #endregion


        private void bbiAddLabourCost_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Add_Preferred_labour();
        }

        private void bbiBlockAddLabourCost_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Block_Add_Preferred_labour();
        }

        private void Add_Preferred_labour()
        {
            GridView parentView = (GridView)gridControl9.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Sites to add the Preferred Labour to by clicking on them then try again.", "Add Preferred Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intCount > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intCount.ToString() + " Sites selected. If you proceed a new Preferred Labour record will be created for each of these records.\n\nProceed?", "Add Preferred Labour", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }
            GridView view = (GridView)gridControl10.MainView;
            view.PostEditor();
            parentView.BeginDataUpdate();
            view.BeginDataUpdate();
            try
            {
                foreach (int i in intRowHandles)
                {
                    DataRow drNewRow;
                    drNewRow = dataSet_OM_Contract.sp06098_OM_Site_Contract_Labour_Cost_Edit.NewRow();
                    drNewRow["strMode"] = "add";
                    drNewRow["strRecordIDs"] = "";
                    drNewRow["SiteContractID"] = Convert.ToInt32(parentView.GetRowCellValue(i, "SiteContractID"));
                    drNewRow["ContractorID"] = 0;
                    drNewRow["ContractorName"] = "";
                    drNewRow["CostUnitDescriptorID"] = 1;  // Hours //
                    drNewRow["SellUnitDescriptorID"] = 1;  // Hours //
                    drNewRow["CostPerUnitExVat"] = (decimal)0.00;
                    drNewRow["CostPerUnitVatRate"] = _decDefaultVatRate;
                    drNewRow["SellPerUnitExVat"] = (decimal)0.00;
                    drNewRow["SellPerUnitVatRate"] = _decDefaultVatRate;
                    drNewRow["CISPercentage"] = (decimal)0.00;
                    drNewRow["CISValue"] = (decimal)0.00;
                    drNewRow["PostcodeSiteDistance"] = (decimal)0.00;
                    drNewRow["LatLongSiteDistance"] = (decimal)0.00;
                    drNewRow["SiteName"] = parentView.GetRowCellValue(i, "SiteName").ToString();
                    drNewRow["SitePostcode"] = parentView.GetRowCellValue(i, "SitePostcode").ToString();
                    drNewRow["SiteLocationX"] = parentView.GetRowCellValue(i, "LocationX").ToString();
                    drNewRow["SiteLocationY"] = parentView.GetRowCellValue(i, "LocationY").ToString();
                    drNewRow["LinkedToPersonTypeID"] = 1;  // Contractor //
                    drNewRow["LinkedToPersonType"] = "Contractor";
                    dataSet_OM_Contract.sp06098_OM_Site_Contract_Labour_Cost_Edit.Rows.Add(drNewRow);

                    parentView.SetRowCellValue(i, "DummyLabourCount", Convert.ToInt32(parentView.GetRowCellValue(i, "DummyLabourCount")) + 1);  // Update Count in list of Sites //
                }
            }
            catch (Exception Ex) { }
            parentView.EndDataUpdate();
            view.EndDataUpdate();
            view.ExpandAllGroups();
        }

        private void Block_Add_Preferred_labour()
        {
            if (strFormMode == "view") return;
            GridView parentView = (GridView)gridControl9.MainView;
            int[] intSiteListRowHandles = parentView.GetSelectedRows();
            int intCount = intSiteListRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Sites to block add the Preferred Labour to by clicking on them then try again.", "Block Add Preferred Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intPersonType = -1;
            var fChildForm2 = new frm_Core_Select_Person_Type();
            fChildForm2.GlobalSettings = this.GlobalSettings;
            fChildForm2.intOriginalPersonTypeID = 1;  // Contractor //;
            if (fChildForm2.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                intPersonType = fChildForm2.intSelectedPersonTypeID;
            }
            else { return; }

            if (intPersonType == 1)
            {
                var fChildForm = new frm_OM_Select_Contractor();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalContractorID = 0;
                fChildForm._Mode = "multiple";
                fChildForm._PassedInLatitude = (double)0.00;
                fChildForm._PassedInLongitude = (double)0.00;
                fChildForm._PassedInPostcode = "";
                GridView ContractorScreenView = null;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    splashScreenManager1.ShowWaitForm();
                    splashScreenManager1.SetWaitFormDescription("Adding Preferred Labour...");

                    ContractorScreenView = (GridView)fChildForm.gridControl1.MainView;
                    int intContractorRowCount = ContractorScreenView.DataRowCount;

                    GridView view = (GridView)gridControl10.MainView;
                    view.PostEditor();
                    parentView.BeginDataUpdate();
                    view.BeginDataUpdate();
                    for (int i = 0; i < intContractorRowCount; i++)
                    {
                        if (Convert.ToBoolean(ContractorScreenView.GetRowCellValue(i, "CheckMarkSelection")))
                        {
                            foreach (int intSiteListRowHandle in intSiteListRowHandles)
                            {
                                try
                                {
                                    DataRow drNewRow;
                                    drNewRow = dataSet_OM_Contract.sp06098_OM_Site_Contract_Labour_Cost_Edit.NewRow();
                                    drNewRow["strMode"] = "add";
                                    drNewRow["strRecordIDs"] = "";
                                    drNewRow["SiteContractID"] = Convert.ToInt32(parentView.GetRowCellValue(intSiteListRowHandle, "SiteContractID"));
                                    drNewRow["ContractorID"] = Convert.ToInt32(ContractorScreenView.GetRowCellValue(i, "ContractorID"));
                                    drNewRow["ContractorName"] = ContractorScreenView.GetRowCellValue(i, "ContractorName").ToString();
                                    drNewRow["CostUnitDescriptorID"] = 1;  // Hours //
                                    drNewRow["SellUnitDescriptorID"] = 1;  // Hours //
                                    drNewRow["CostPerUnitExVat"] = (decimal)0.00;
                                    drNewRow["CostPerUnitVatRate"] = _decDefaultVatRate;
                                    drNewRow["SellPerUnitExVat"] = (decimal)0.00;
                                    drNewRow["SellPerUnitVatRate"] = _decDefaultVatRate;
                                    drNewRow["CISPercentage"] = (decimal)0.00;
                                    drNewRow["CISValue"] = (decimal)0.00;
                                    drNewRow["SiteName"] = parentView.GetRowCellValue(intSiteListRowHandle, "SiteName").ToString();
                                    drNewRow["SitePostcode"] = parentView.GetRowCellValue(intSiteListRowHandle, "SitePostcode").ToString();
                                    drNewRow["SiteLocationX"] = parentView.GetRowCellValue(intSiteListRowHandle, "LocationX").ToString();
                                    drNewRow["SiteLocationY"] = parentView.GetRowCellValue(intSiteListRowHandle, "LocationY").ToString();
                                    drNewRow["LinkedToPersonTypeID"] = 1;  // Contractor //
                                    drNewRow["LinkedToPersonType"] = "Contractor";

                                    // Perform Distance Calculation //
                                    double dbLatitude = Convert.ToDouble(ContractorScreenView.GetRowCellValue(i, "Latitude"));
                                    double dbLongitude = Convert.ToDouble(ContractorScreenView.GetRowCellValue(i, "Longitude"));
                                    string strPostcode = ContractorScreenView.GetRowCellValue(i, "Postcode").ToString();

                                    double dbPostcodeDistance = (double)0.00;
                                    double dbLatLongDistance = (double)0.00;
                                    if (!string.IsNullOrWhiteSpace(strPostcode) && !string.IsNullOrWhiteSpace(drNewRow["SitePostcode"].ToString()))
                                    {
                                        using (var CalcPostcodeDistance = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                                        {
                                            CalcPostcodeDistance.ChangeConnectionString(strConnectionString);
                                            try
                                            {
                                                dbPostcodeDistance = Convert.ToDouble(CalcPostcodeDistance.sp06102_OM_Calculate_Distance_Between_Postcodes(strPostcode, drNewRow["SitePostcode"].ToString()));
                                            }
                                            catch (Exception) { }
                                        }
                                    }
                                    try
                                    {
                                        dbLatLongDistance = GeoCodeCalc.CalcDistance(dbLatitude, dbLongitude, (double)drNewRow["SiteLocationX"], (double)drNewRow["SiteLocationY"]);  // Calculate Distance in Miles //
                                    }
                                    catch (Exception) { }
                                    drNewRow["PostcodeSiteDistance"] = (dbPostcodeDistance > (double)2500 ? (double)0.00 : dbPostcodeDistance);
                                    drNewRow["LatLongSiteDistance"] = (dbLatLongDistance > (double)2500 ? (double)0.00 : dbLatLongDistance);

                                    dataSet_OM_Contract.sp06098_OM_Site_Contract_Labour_Cost_Edit.Rows.Add(drNewRow);
                                }
                                catch (Exception Ex) { }

                                parentView.SetRowCellValue(intSiteListRowHandle, "DummyLabourCount", Convert.ToInt32(parentView.GetRowCellValue(intSiteListRowHandle, "DummyLabourCount")) + 1);  // Update Count in list of Sites //
                            }
                        }
                    }
                    if (splashScreenManager1.IsSplashFormVisible)
                    {
                        splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                        splashScreenManager1.CloseWaitForm();
                    }
                    view.EndDataUpdate();
                    parentView.EndDataUpdate();
                    view.ExpandAllGroups();
                }
            }
            else  // Staff //
            {
                var fChildForm = new frm_HR_Select_Staff();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalStaffID = 0;
                fChildForm._Mode = "multiple";
                GridView ContractorScreenView = null;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    splashScreenManager1.ShowWaitForm();
                    splashScreenManager1.SetWaitFormDescription("Adding Preferred Labour...");

                    ContractorScreenView = (GridView)fChildForm.gridControl1.MainView;
                    int intContractorRowCount = ContractorScreenView.DataRowCount;

                    GridView view = (GridView)gridControl10.MainView;
                    view.PostEditor();
                    parentView.BeginDataUpdate();
                    view.BeginDataUpdate();
                    for (int i = 0; i < intContractorRowCount; i++)
                    {
                        if (Convert.ToBoolean(ContractorScreenView.GetRowCellValue(i, "CheckMarkSelection")))
                        {
                            foreach (int intSiteListRowHandle in intSiteListRowHandles)
                            {
                                try
                                {
                                    DataRow drNewRow;
                                    drNewRow = dataSet_OM_Contract.sp06098_OM_Site_Contract_Labour_Cost_Edit.NewRow();
                                    drNewRow["strMode"] = "add";
                                    drNewRow["strRecordIDs"] = "";
                                    drNewRow["SiteContractID"] = Convert.ToInt32(parentView.GetRowCellValue(intSiteListRowHandle, "SiteContractID"));
                                    drNewRow["ContractorID"] = Convert.ToInt32(ContractorScreenView.GetRowCellValue(i, "intStaffID"));
                                    drNewRow["ContractorName"] = ContractorScreenView.GetRowCellValue(i, "SurnameForename").ToString();
                                    drNewRow["CostUnitDescriptorID"] = 1;  // Hours //
                                    drNewRow["SellUnitDescriptorID"] = 1;  // Hours //
                                    drNewRow["CostPerUnitExVat"] = (decimal)0.00;
                                    drNewRow["CostPerUnitVatRate"] = _decDefaultVatRate;
                                    drNewRow["SellPerUnitExVat"] = (decimal)0.00;
                                    drNewRow["SellPerUnitVatRate"] = _decDefaultVatRate;
                                    drNewRow["CISPercentage"] = (decimal)0.00;
                                    drNewRow["CISValue"] = (decimal)0.00;
                                    drNewRow["SiteName"] = parentView.GetRowCellValue(intSiteListRowHandle, "SiteName").ToString();
                                    drNewRow["SitePostcode"] = parentView.GetRowCellValue(intSiteListRowHandle, "SitePostcode").ToString();
                                    drNewRow["SiteLocationX"] = parentView.GetRowCellValue(intSiteListRowHandle, "LocationX").ToString();
                                    drNewRow["SiteLocationY"] = parentView.GetRowCellValue(intSiteListRowHandle, "LocationY").ToString();
                                    drNewRow["LinkedToPersonTypeID"] = 0;  // Staff //
                                    drNewRow["LinkedToPersonType"] = "Staff";
                                    drNewRow["PostcodeSiteDistance"] = (double)0.00;
                                    drNewRow["LatLongSiteDistance"] = (double)0.00;

                                    dataSet_OM_Contract.sp06098_OM_Site_Contract_Labour_Cost_Edit.Rows.Add(drNewRow);
                                }
                                catch (Exception Ex) { }

                                parentView.SetRowCellValue(intSiteListRowHandle, "DummyLabourCount", Convert.ToInt32(parentView.GetRowCellValue(intSiteListRowHandle, "DummyLabourCount")) + 1);  // Update Count in list of Sites //
                            }
                        }
                    }
                    if (splashScreenManager1.IsSplashFormVisible)
                    {
                        splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                        splashScreenManager1.CloseWaitForm();
                    }
                    view.EndDataUpdate();
                    parentView.EndDataUpdate();
                    view.ExpandAllGroups();
                }
            }
        }
        
        private void Block_Edit_Preferred_labour()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl10.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Preferred Labour records to block edit then try again.", "Block Edit Site Preferred Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Site_Contract_Wizard_Block_Edit_Preferred_Labour_Info();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                double dbLatitude = (double)0.00;
                double dbLongitude = (double)0.00;
                string strPostcode = "";
                double dbPostcodeDistance = (double)0.00;
                double dbLatLongDistance = (double)0.00;
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                view.BeginDataUpdate();
                using (var CalcPostcodeDistance = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                {
                    CalcPostcodeDistance.ChangeConnectionString(strConnectionString);
                    foreach (int intRowHandle in intRowHandles)
                    {
                        if (fChildForm.intContractorID != null && (fChildForm.intLinkedToPersonTypeID != null && Convert.ToInt32(fChildForm.intLinkedToPersonTypeID) == 1))
                        {
                            view.SetRowCellValue(intRowHandle, "ContractorID", fChildForm.intContractorID);

                            // Perform Distance Calculation //
                            dbPostcodeDistance = (double)0.00;
                            dbLatLongDistance = (double)0.00;
                            if (fChildForm.dbTeamLocationX != null) dbLatitude = (double)fChildForm.dbTeamLocationX;
                            if (fChildForm.dbTeamLocationY != null) dbLongitude = (double)fChildForm.dbTeamLocationY;
                            if (fChildForm.strTeamPostcode != null) strPostcode = fChildForm.strTeamPostcode;

                            if (!string.IsNullOrWhiteSpace(strPostcode) && !string.IsNullOrWhiteSpace(view.GetRowCellValue(intRowHandle, "SitePostcode").ToString()))
                            {
                                try
                                {
                                    dbPostcodeDistance = Convert.ToDouble(CalcPostcodeDistance.sp06102_OM_Calculate_Distance_Between_Postcodes(strPostcode, view.GetRowCellValue(intRowHandle, "SitePostcode").ToString()));
                                }
                                catch (Exception) { }
                            }
                            try
                            {
                                dbLatLongDistance = GeoCodeCalc.CalcDistance(dbLatitude, dbLongitude, Convert.ToDouble(view.GetRowCellValue(intRowHandle, "SiteLocationX")), Convert.ToDouble(view.GetRowCellValue(intRowHandle, "SiteLocationY")));  // Calculate Distance in Miles //
                            }
                            catch (Exception) { }

                            view.SetRowCellValue(intRowHandle, "PostcodeSiteDistance", (dbPostcodeDistance > (double)2500 ? (double)0.00 : dbPostcodeDistance));
                            view.SetRowCellValue(intRowHandle, "LatLongSiteDistance", (dbLatLongDistance > (double)2500 ? (double)0.00 : dbLatLongDistance));
                        }
                        if (fChildForm.strLinkedToPersonType != null) view.SetRowCellValue(intRowHandle, "LinkedToPersonType", fChildForm.strLinkedToPersonType);
                        if (fChildForm.intLinkedToPersonTypeID != null) view.SetRowCellValue(intRowHandle, "LinkedToPersonTypeID", fChildForm.intLinkedToPersonTypeID);
                        if (fChildForm.strContractorName != null) view.SetRowCellValue(intRowHandle, "ContractorName", fChildForm.strContractorName);
                        if (fChildForm.intCostUnitDescriptorID != null) view.SetRowCellValue(intRowHandle, "CostUnitDescriptorID", fChildForm.intCostUnitDescriptorID);
                        if (fChildForm.intSellUnitDescriptorID != null) view.SetRowCellValue(intRowHandle, "SellUnitDescriptorID", fChildForm.intSellUnitDescriptorID);
                        if (fChildForm.decCostPerUnitExVat != null) view.SetRowCellValue(intRowHandle, "CostPerUnitExVat", fChildForm.decCostPerUnitExVat);
                        if (fChildForm.decCostPerUnitVatRate != null) view.SetRowCellValue(intRowHandle, "CostPerUnitVatRate", fChildForm.decCostPerUnitVatRate);
                        if (fChildForm.decSellPerUnitExVat != null) view.SetRowCellValue(intRowHandle, "SellPerUnitExVat", fChildForm.decSellPerUnitExVat);
                        if (fChildForm.decSellPerUnitVatRate != null) view.SetRowCellValue(intRowHandle, "SellPerUnitVatRate", fChildForm.decSellPerUnitVatRate);
                        if (fChildForm.decCISPercentage != null) view.SetRowCellValue(intRowHandle, "CISPercentage", fChildForm.decCISPercentage);
                        if (fChildForm.decCISValue != null) view.SetRowCellValue(intRowHandle, "CISValue", fChildForm.decCISValue);
                        if (fChildForm.decPostcodeSiteDistance != null) view.SetRowCellValue(intRowHandle, "PostcodeSiteDistance", fChildForm.decPostcodeSiteDistance);
                        if (fChildForm.decLatLongSiteDistance != null) view.SetRowCellValue(intRowHandle, "LatLongSiteDistance", fChildForm.decLatLongSiteDistance);
                        if (fChildForm.strRemarks != null) view.SetRowCellValue(intRowHandle, "Remarks", fChildForm.strRemarks);
                    }
                }
                view.EndDataUpdate();
            }
        }

        private void Delete_Preferred_labour()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            view = (GridView)gridControl10.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Preferred Labour to remove from the Site by clicking on them then try again.", "Remove Linked Preferred Labour from Site", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? "1 Linked Preferred Labour" : Convert.ToString(intRowHandles.Length) + " Linked Preferred Labour") + " selected for removing from the current Site!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Preferred Labour" : "these Linked Preferred Labour") + " will be removed from the Site.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Preferred Labour from Site", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                GridView parentView = (GridView)gridControl9.MainView;
                int intFoundRowHandle = 0;
                int intSiteContractID = 0;

                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Deleting...");

                for (int i = intRowHandles.Length - 1; i >= 0; i--)
                {
                    // Reduce number of linked records from parent site list by 1 //
                    intSiteContractID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "SiteContractID"));
                    intFoundRowHandle = parentView.LocateByValue(0, parentView.Columns["SiteContractID"], intSiteContractID);
                    if (intFoundRowHandle != GridControl.InvalidRowHandle) parentView.SetRowCellValue(intFoundRowHandle, "DummyLabourCount", Convert.ToInt32(parentView.GetRowCellValue(intFoundRowHandle, "DummyLabourCount")) - 1);

                    view.DeleteRow(intRowHandles[i]);
                }

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for unlinking from Site.", "Remove Linked Preferred Labour from Site", MessageBoxButtons.OK, MessageBoxIcon.Information);
                SetMenuStatus();
            }
        }

        private void Filter_Preferred_labour_On_Site()
        {
            GridView view = (GridView)gridControl10.MainView;
            view.PostEditor();

            string strFilterCondition = "";
            GridView parentView = (GridView)gridControl9.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                strFilterCondition = "[SiteContractID] = 9999999999";
            }
            else
            {
                foreach (int i in intRowHandles)
                {
                    strFilterCondition += (string.IsNullOrWhiteSpace(strFilterCondition) ? "" : " or ");
                    strFilterCondition += "[SiteContractID] = " + parentView.GetRowCellValue(i, "SiteContractID").ToString();
                }
            }
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = strFilterCondition;
            view.MakeRowVisible(-1, true);
            view.ExpandAllGroups();
            view.EndUpdate();
            SetMenuStatus();
        }

        private void Clear_Filter_Preferred_labour()
        {
            GridView view = (GridView)gridControl10.MainView;
            view.PostEditor();

            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
            SetMenuStatus();
        }

        #endregion


        #region Step 7 Page

        private void btnStep7Previous_Click(object sender, EventArgs e)
        {
            Filter_Preferred_labour_On_Site();
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep6;
        }
        private void btnStep7Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl12.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            Clear_Filter_Preferred_Equipment();
            if (checkEditSkipMaterials.Checked)
            {
                xtraTabControl1.SelectedTabPage = this.xtraTabPageStep9;  // Skip 1 page //
            }
            else
            {
                xtraTabControl1.SelectedTabPage = this.xtraTabPageStep8;  // Go to next page //
            }
         }


        #region GridView11

        private void gridView11_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "DummyEquipmentCount")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DummyEquipmentCount")) <= 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridView11_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            SetMenuStatus();
        }

        private void gridView11_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView12

        private void gridControl12_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Preferred_Equipment();
                    }
                    else if ("block edit".Equals(e.Button.Tag))
                    {
                        Block_Edit_Preferred_Equipment();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Preferred_Equipment();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView12_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.PreferredEquipment;
            SetMenuStatus();
        }

        private void gridView12_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.PreferredEquipment;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemButtonEditChooseEquipment_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                GridView viewLabour = (GridView)gridControl10.MainView;  // Labour - get unique list of preferred and pass to equipment screen //
                string strSelectedContractorIDs = ",";
                for (int i = 0; i < viewLabour.DataRowCount; i++)
                {
                    if (!strSelectedContractorIDs.Contains("," + viewLabour.GetRowCellValue(i, "ContractorID").ToString() + ",")) strSelectedContractorIDs += viewLabour.GetRowCellValue(i, "ContractorID").ToString() + ",";
                }
                strSelectedContractorIDs = strSelectedContractorIDs.Remove(0, 1);  // Remove preceeding comma //

                var currentRowView = (DataRowView)sp06103OMSiteContractEquipmentCostEditBindingSource.Current;
                var currentRow = (DataSet_OM_Contract.sp06103_OM_Site_Contract_Equipment_Cost_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                int intOriginalID = (string.IsNullOrEmpty(currentRow.EquipmentID.ToString()) ? 0 : Convert.ToInt32(currentRow.EquipmentID));
                var fChildForm = new frm_OM_Select_Equipment();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalID = intOriginalID;
                fChildForm._FilteredContractorIDs = strSelectedContractorIDs;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (fChildForm.intSelectedID == intOriginalID) return;
                    currentRow.EquipmentID = fChildForm.intOriginalID;
                    currentRow.EquipmentType = fChildForm.strSelectedDescription1;
                    currentRow.OwnerName = fChildForm.strSelectedDescription2;
                    currentRow.OwnerType = fChildForm.strSelectedDescription3;

                    sp06103OMSiteContractEquipmentCostEditBindingSource.EndEdit();
                    GridView view = (GridView)gridControl12.MainView;
                    view.FocusedColumn = colCostUnitDescriptorID2;  // Move focus to next column so new value is shown in original column //
                }
            }
        }

        #endregion


        private void bbiAddEquipmentCost_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Add_Preferred_Equipment();
        }

        private void bbiBlockAddEquipmentCost_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Block_Add_Preferred_Equipment();
        }

        private void Add_Preferred_Equipment()
        {
            GridView parentView = (GridView)gridControl11.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Sites to add the Default Equipment Costs to by clicking on them then try again.", "Add Default Equipment Cost", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intCount > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intCount.ToString() + " Sites selected. If you proceed a new Default Equipment Cost record will be created for each of these records.\n\nProceed?", "Add Default Equipment Cost", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }
            GridView view = (GridView)gridControl12.MainView;
            view.PostEditor();
            parentView.BeginDataUpdate();
            view.BeginDataUpdate();
            try
            {
                foreach (int i in intRowHandles)
                {
                    DataRow drNewRow;
                    drNewRow = dataSet_OM_Contract.sp06103_OM_Site_Contract_Equipment_Cost_Edit.NewRow();
                    drNewRow["strMode"] = "add";
                    drNewRow["strRecordIDs"] = "";
                    drNewRow["SiteContractID"] = Convert.ToInt32(parentView.GetRowCellValue(i, "SiteContractID"));
                    drNewRow["EquipmentID"] = 0;
                    drNewRow["EquipmentType"] = "";
                    drNewRow["OwnerName"] = "???";
                    drNewRow["OwnerType"] = "???";
                    drNewRow["CostUnitDescriptorID"] = 1;  // Hours //
                    drNewRow["SellUnitDescriptorID"] = 1;  // Hours //
                    drNewRow["CostPerUnitExVat"] = (decimal)0.00;
                    drNewRow["CostPerUnitVatRate"] = _decDefaultVatRate;
                    drNewRow["SellPerUnitExVat"] = (decimal)0.00;
                    drNewRow["SellPerUnitVatRate"] = _decDefaultVatRate;
                    drNewRow["SiteName"] = parentView.GetRowCellValue(i, "SiteName").ToString();
                    dataSet_OM_Contract.sp06103_OM_Site_Contract_Equipment_Cost_Edit.Rows.Add(drNewRow);

                    parentView.SetRowCellValue(i, "DummyEquipmentCount", Convert.ToInt32(parentView.GetRowCellValue(i, "DummyEquipmentCount")) + 1);  // Update Count in list of Sites //
                }
            }
            catch (Exception Ex) { }
            parentView.EndDataUpdate();
            view.EndDataUpdate();
            view.ExpandAllGroups();
        }

        private void Block_Add_Preferred_Equipment()
        {
            if (strFormMode == "view") return;
            GridView parentView = (GridView)gridControl11.MainView;
            int[] intSiteListRowHandles = parentView.GetSelectedRows();
            int intCount = intSiteListRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Sites to block add the Default Equipment Costs to by clicking on them then try again.", "Block Add Default Equipment Costs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            GridView viewLabour = (GridView)gridControl10.MainView;  // Labour - get unique list of preferred and pass to equipment screen //
            string strSelectedContractorIDs = ",";
            for (int i = 0; i < viewLabour.DataRowCount; i++)
            {
                if (!strSelectedContractorIDs.Contains("," + viewLabour.GetRowCellValue(i, "ContractorID").ToString() + ",")) strSelectedContractorIDs += viewLabour.GetRowCellValue(i, "ContractorID").ToString() + ",";
            }
            strSelectedContractorIDs = strSelectedContractorIDs.Remove(0, 1);  // Remove preceeding comma //

            var fChildForm = new frm_OM_Select_Equipment();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.intOriginalID = 0;
            fChildForm._Mode = "multiple";
            fChildForm._FilteredContractorIDs = strSelectedContractorIDs;
            GridView EmployeeScreenView = null;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Adding Default Equipment Costs...");
                
                EmployeeScreenView = (GridView)fChildForm.gridControl1.MainView;
                int intEmployeeRowCount = EmployeeScreenView.DataRowCount;

                GridView view = (GridView)gridControl12.MainView;
                view.PostEditor();
                parentView.BeginDataUpdate();
                view.BeginDataUpdate();
                for (int i = 0; i < intEmployeeRowCount; i++)
                {
                    if (Convert.ToBoolean(EmployeeScreenView.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        foreach (int intSiteListRowHandle in intSiteListRowHandles)
                        {

                            try
                            {
                                DataRow drNewRow;
                                drNewRow = dataSet_OM_Contract.sp06103_OM_Site_Contract_Equipment_Cost_Edit.NewRow();
                                drNewRow["strMode"] = "add";
                                drNewRow["strRecordIDs"] = "";
                                drNewRow["SiteContractID"] = Convert.ToInt32(parentView.GetRowCellValue(intSiteListRowHandle, "SiteContractID"));
                                drNewRow["EquipmentID"] = Convert.ToInt32(EmployeeScreenView.GetRowCellValue(i, "EquipmentID"));
                                drNewRow["EquipmentType"] = EmployeeScreenView.GetRowCellValue(i, "EquipmentType").ToString();
                                drNewRow["OwnerName"] = EmployeeScreenView.GetRowCellValue(i, "OwnerName").ToString();
                                drNewRow["OwnerType"] = EmployeeScreenView.GetRowCellValue(i, "OwnerType").ToString();
                                drNewRow["CostUnitDescriptorID"] = 1;  // Hours //
                                drNewRow["SellUnitDescriptorID"] = 1;  // Hours //
                                drNewRow["CostPerUnitExVat"] = (decimal)0.00;
                                drNewRow["CostPerUnitVatRate"] = _decDefaultVatRate;
                                drNewRow["SellPerUnitExVat"] = (decimal)0.00;
                                drNewRow["SellPerUnitVatRate"] = _decDefaultVatRate;
                                drNewRow["SiteName"] = parentView.GetRowCellValue(intSiteListRowHandle, "SiteName").ToString();
                                dataSet_OM_Contract.sp06103_OM_Site_Contract_Equipment_Cost_Edit.Rows.Add(drNewRow);
                            }
                            catch (Exception Ex) { }

                            parentView.SetRowCellValue(intSiteListRowHandle, "DummyEquipmentCount", Convert.ToInt32(parentView.GetRowCellValue(intSiteListRowHandle, "DummyEquipmentCount")) + 1);  // Update Count in list of Sites //
                        }
                    }
                }

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                parentView.EndDataUpdate();
                view.EndDataUpdate();
                view.ExpandAllGroups();
            }
        }

        private void Block_Edit_Preferred_Equipment()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl12.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Default Equipment Cost records to block edit then try again.", "Block Edit Default Equipment Cost", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Site_Contract_Wizard_Block_Edit_Default_Equipment_Info();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;

                view.BeginDataUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    if (fChildForm.intEquipmentID != null) view.SetRowCellValue(intRowHandle, "EquipmentID", fChildForm.intEquipmentID);
                    if (fChildForm.strEquipmentType != null) view.SetRowCellValue(intRowHandle, "EquipmentType", fChildForm.strEquipmentType);
                    if (fChildForm.strOwnerName != null) view.SetRowCellValue(intRowHandle, "OwnerName", fChildForm.strOwnerName);
                    if (fChildForm.strOwnerType != null) view.SetRowCellValue(intRowHandle, "OwnerType", fChildForm.strOwnerType);
                    if (fChildForm.intCostUnitDescriptorID != null) view.SetRowCellValue(intRowHandle, "CostUnitDescriptorID", fChildForm.intCostUnitDescriptorID);
                    if (fChildForm.intSellUnitDescriptorID != null) view.SetRowCellValue(intRowHandle, "SellUnitDescriptorID", fChildForm.intSellUnitDescriptorID);
                    if (fChildForm.decCostPerUnitExVat != null) view.SetRowCellValue(intRowHandle, "CostPerUnitExVat", fChildForm.decCostPerUnitExVat);
                    if (fChildForm.decCostPerUnitVatRate != null) view.SetRowCellValue(intRowHandle, "CostPerUnitVatRate", fChildForm.decCostPerUnitVatRate);
                    if (fChildForm.decSellPerUnitExVat != null) view.SetRowCellValue(intRowHandle, "SellPerUnitExVat", fChildForm.decSellPerUnitExVat);
                    if (fChildForm.decSellPerUnitVatRate != null) view.SetRowCellValue(intRowHandle, "SellPerUnitVatRate", fChildForm.decSellPerUnitVatRate);
                    if (fChildForm.strRemarks != null) view.SetRowCellValue(intRowHandle, "Remarks", fChildForm.strRemarks);
                }
                view.EndDataUpdate();
            }
        }

        private void Delete_Preferred_Equipment()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            view = (GridView)gridControl12.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Default Equipment Costs to remove from the Site by clicking on them then try again.", "Remove Linked Default Equipment Cost from Site", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? "1 Linked Default Equipment Cost" : Convert.ToString(intRowHandles.Length) + " Linked Default Equipment Cost") + " selected for removing from the current Site!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Default Equipment Cost" : "these Linked Default Equipment Costs") + " will be removed from the Site.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Default Equipment Cost from Site", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                GridView parentView = (GridView)gridControl11.MainView;
                int intFoundRowHandle = 0;
                int intSiteContractID = 0;

                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Deleting...");

                for (int i = intRowHandles.Length - 1; i >= 0; i--)
                {
                    // Reduce number of linked records from parent site list by 1 //
                    intSiteContractID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "SiteContractID"));
                    intFoundRowHandle = parentView.LocateByValue(0, parentView.Columns["SiteContractID"], intSiteContractID);
                    if (intFoundRowHandle != GridControl.InvalidRowHandle) parentView.SetRowCellValue(intFoundRowHandle, "DummyEquipmentCount", Convert.ToInt32(parentView.GetRowCellValue(intFoundRowHandle, "DummyEquipmentCount")) - 1);

                    view.DeleteRow(intRowHandles[i]);
                }

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for unlinking from Site.", "Remove Linked Default Equipment Cost from Site", MessageBoxButtons.OK, MessageBoxIcon.Information);
                SetMenuStatus();
            }
        }

        private void Filter_Preferred_Equipment_On_Site()
        {
            GridView view = (GridView)gridControl12.MainView;
            view.PostEditor();

            string strFilterCondition = "";
            GridView parentView = (GridView)gridControl11.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                strFilterCondition = "[SiteContractID] = 9999999999";
            }
            else
            {
                foreach (int i in intRowHandles)
                {
                    strFilterCondition += (string.IsNullOrWhiteSpace(strFilterCondition) ? "" : " or ");
                    strFilterCondition += "[SiteContractID] = " + parentView.GetRowCellValue(i, "SiteContractID").ToString();
                }
            }
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = strFilterCondition;
            view.MakeRowVisible(-1, true);
            view.ExpandAllGroups();
            view.EndUpdate();
            SetMenuStatus();
        }

        private void Clear_Filter_Preferred_Equipment()
        {
            GridView view = (GridView)gridControl12.MainView;
            view.PostEditor();

            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
            SetMenuStatus();
        }

        #endregion


        #region Step 8 Page

        private void btnStep8Previous_Click(object sender, EventArgs e)
        {
            Filter_Preferred_Equipment_On_Site();
            if (checkEditSkipEquipment.Checked)
            {
                xtraTabControl1.SelectedTabPage = this.xtraTabPageStep6;  // Skip back 1 page //
            }
            else
            {
                xtraTabControl1.SelectedTabPage = this.xtraTabPageStep7;  // Go to previous page //
            }
        }

        private void btnStep8Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl14.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            Clear_Filter_Preferred_Material();
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep9;
        }


        #region GridView13

        private void gridView13_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "DummyMaterialCount")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DummyMaterialCount")) <= 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }
        
        private void gridView13_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            SetMenuStatus();
        }

        private void gridView13_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView14

        private void gridControl14_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Preferred_Material();
                    }
                    else if ("block edit".Equals(e.Button.Tag))
                    {
                        Block_Edit_Preferred_Material();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Preferred_Material();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView14_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.PreferredMaterial;
            SetMenuStatus();
        }

        private void gridView14_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.PreferredMaterial;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemButtonEditChooseMaterial_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06106OMSiteContractMaterialCostEditBindingSource.Current;
                var currentRow = (DataSet_OM_Contract.sp06106_OM_Site_Contract_Material_Cost_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                int intOriginalID = (string.IsNullOrEmpty(currentRow.MaterialID.ToString()) ? 0 : Convert.ToInt32(currentRow.MaterialID));
                var fChildForm = new frm_OM_Select_Material();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalID = intOriginalID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (fChildForm.intSelectedID == intOriginalID) return;
                    currentRow.MaterialID = fChildForm.intOriginalID;
                    currentRow.MaterialDescription = fChildForm.strSelectedDescription1;
                    currentRow.CostUnitDescriptorID = fChildForm.intSelectedDescription2;
                    currentRow.SellUnitDescriptorID = fChildForm.intSelectedDescription2;
                    currentRow.CostPerUnitExVat = fChildForm.decSelectedCostExVat;
                    currentRow.CostPerUnitVatRate = fChildForm.decSelectedCostVatRate;
                    currentRow.SellPerUnitExVat = fChildForm.decSelectedSellExVat;
                    currentRow.SellPerUnitVatRate = fChildForm.decSelectedSellVatRate;

                    sp06106OMSiteContractMaterialCostEditBindingSource.EndEdit();
                    GridView view = (GridView)gridControl14.MainView;
                    view.FocusedColumn = colCostUnitDescriptor3;  // Move focus to next column so new value is shown in original column //
                }
            }
        }

        #endregion


        private void bbiAddMaterialCost_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Add_Preferred_Material();
        }

        private void bbiBlockAddMaterialCost_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Block_Add_Preferred_Material();
        }

        private void Add_Preferred_Material()
        {
            GridView parentView = (GridView)gridControl13.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Sites to add the Default Material Costs to by clicking on them then try again.", "Add Default Material Cost", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intCount > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intCount.ToString() + " Sites selected. If you proceed a new Default Material Cost record will be created for each of these records.\n\nProceed?", "Add Default Material Cost", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }
            GridView view = (GridView)gridControl14.MainView;
            view.PostEditor();
            parentView.BeginDataUpdate();
            view.BeginDataUpdate();
            try
            {
                foreach (int i in intRowHandles)
                {
                    DataRow drNewRow;
                    drNewRow = dataSet_OM_Contract.sp06106_OM_Site_Contract_Material_Cost_Edit.NewRow();
                    drNewRow["strMode"] = "add";
                    drNewRow["strRecordIDs"] = "";
                    drNewRow["SiteContractID"] = Convert.ToInt32(parentView.GetRowCellValue(i, "SiteContractID"));
                    drNewRow["MaterialID"] = 0;
                    drNewRow["MaterialDescription"] = "";
                    drNewRow["CostUnitDescriptorID"] = 1;
                    drNewRow["SellUnitDescriptorID"] = 1;
                    drNewRow["CostPerUnitExVat"] = (decimal)0.00;
                    drNewRow["CostPerUnitVatRate"] = _decDefaultVatRate;
                    drNewRow["SellPerUnitExVat"] = (decimal)0.00;
                    drNewRow["SellPerUnitVatRate"] = _decDefaultVatRate;
                    drNewRow["SiteName"] = parentView.GetRowCellValue(i, "SiteName").ToString();
                    dataSet_OM_Contract.sp06106_OM_Site_Contract_Material_Cost_Edit.Rows.Add(drNewRow);

                    parentView.SetRowCellValue(i, "DummyMaterialCount", Convert.ToInt32(parentView.GetRowCellValue(i, "DummyMaterialCount")) + 1);  // Update Count in list of Sites //
                }
            }
            catch (Exception Ex) { }
            parentView.EndDataUpdate();
            view.EndDataUpdate();
            view.ExpandAllGroups();
        }

        private void Block_Add_Preferred_Material()
        {
            if (strFormMode == "view") return;
            GridView parentView = (GridView)gridControl13.MainView;
            int[] intSiteListRowHandles = parentView.GetSelectedRows();
            int intCount = intSiteListRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Sites to block add the Default Material Costs to by clicking on them then try again.", "Block Add Default Material Costs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Select_Material();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.intOriginalID = 0;
            fChildForm._Mode = "multiple";
            GridView EmployeeScreenView = null;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Adding Default Material Costs...");
                
                EmployeeScreenView = (GridView)fChildForm.gridControl1.MainView;
                int intEmployeeRowCount = EmployeeScreenView.DataRowCount;

                GridView view = (GridView)gridControl14.MainView;
                view.PostEditor();
                parentView.BeginDataUpdate();
                view.BeginDataUpdate();
                for (int i = 0; i < intEmployeeRowCount; i++)
                {
                    if (Convert.ToBoolean(EmployeeScreenView.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        foreach (int intSiteListRowHandle in intSiteListRowHandles)
                        {

                            try
                            {
                                DataRow drNewRow;
                                drNewRow = dataSet_OM_Contract.sp06106_OM_Site_Contract_Material_Cost_Edit.NewRow();
                                drNewRow["strMode"] = "add";
                                drNewRow["strRecordIDs"] = "";
                                drNewRow["SiteContractID"] = Convert.ToInt32(parentView.GetRowCellValue(intSiteListRowHandle, "SiteContractID"));
                                drNewRow["MaterialID"] = Convert.ToInt32(EmployeeScreenView.GetRowCellValue(i, "MaterialID"));
                                drNewRow["MaterialDescription"] = EmployeeScreenView.GetRowCellValue(i, "MaterialDescription").ToString();
                                drNewRow["CostUnitDescriptorID"] = EmployeeScreenView.GetRowCellValue(i, "UnitDescriptorID").ToString();
                                drNewRow["SellUnitDescriptorID"] = EmployeeScreenView.GetRowCellValue(i, "UnitDescriptorID").ToString();
                                drNewRow["CostPerUnitExVat"] = EmployeeScreenView.GetRowCellValue(i, "CostPerUnitExVat").ToString();
                                drNewRow["CostPerUnitVatRate"] = EmployeeScreenView.GetRowCellValue(i, "CostPerUnitVatRate").ToString();
                                drNewRow["SellPerUnitExVat"] = EmployeeScreenView.GetRowCellValue(i, "SellPerUnitExVat").ToString();
                                drNewRow["SellPerUnitVatRate"] = EmployeeScreenView.GetRowCellValue(i, "SellPerUnitVatRate").ToString();
                                drNewRow["SiteName"] = parentView.GetRowCellValue(intSiteListRowHandle, "SiteName").ToString();
                                dataSet_OM_Contract.sp06106_OM_Site_Contract_Material_Cost_Edit.Rows.Add(drNewRow);
                            }
                            catch (Exception Ex) { }

                            parentView.SetRowCellValue(intSiteListRowHandle, "DummyMaterialCount", Convert.ToInt32(parentView.GetRowCellValue(intSiteListRowHandle, "DummyMaterialCount")) + 1);  // Update Count in list of Sites //
                        }
                    }
                }

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                parentView.EndDataUpdate();
                view.EndDataUpdate();
                view.ExpandAllGroups();
            }
        }

        private void Block_Edit_Preferred_Material()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl14.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Default Material Cost records to block edit then try again.", "Block Edit Default Material Cost", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Site_Contract_Wizard_Block_Edit_Default_Material_Info();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;

                view.BeginDataUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    if (fChildForm.intMaterialID != null) view.SetRowCellValue(intRowHandle, "MaterialID", fChildForm.intMaterialID);
                    if (fChildForm.strMaterialDescription != null) view.SetRowCellValue(intRowHandle, "MaterialDescription", fChildForm.strMaterialDescription);
                    if (fChildForm.intCostUnitDescriptorID != null) view.SetRowCellValue(intRowHandle, "CostUnitDescriptorID", fChildForm.intCostUnitDescriptorID);
                    if (fChildForm.intSellUnitDescriptorID != null) view.SetRowCellValue(intRowHandle, "SellUnitDescriptorID", fChildForm.intSellUnitDescriptorID);
                    if (fChildForm.decCostPerUnitExVat != null) view.SetRowCellValue(intRowHandle, "CostPerUnitExVat", fChildForm.decCostPerUnitExVat);
                    if (fChildForm.decCostPerUnitVatRate != null) view.SetRowCellValue(intRowHandle, "CostPerUnitVatRate", fChildForm.decCostPerUnitVatRate);
                    if (fChildForm.decSellPerUnitExVat != null) view.SetRowCellValue(intRowHandle, "SellPerUnitExVat", fChildForm.decSellPerUnitExVat);
                    if (fChildForm.decSellPerUnitVatRate != null) view.SetRowCellValue(intRowHandle, "SellPerUnitVatRate", fChildForm.decSellPerUnitVatRate);
                    if (fChildForm.strRemarks != null) view.SetRowCellValue(intRowHandle, "Remarks", fChildForm.strRemarks);
                }
                view.EndDataUpdate();
            }
        }

        private void Delete_Preferred_Material()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            view = (GridView)gridControl14.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Default Material Costs to remove from the Site by clicking on them then try again.", "Remove Linked Default Material Cost from Site", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? "1 Linked Default Material Cost" : Convert.ToString(intRowHandles.Length) + " Linked Default Material Costs") + " selected for removing from the current Site!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Default Material Costs" : "these Linked Default Material Costs") + " will be removed from the Site.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Default Material Costs from Site", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                GridView parentView = (GridView)gridControl13.MainView;
                int intFoundRowHandle = 0;
                int intSiteContractID = 0;

                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Deleting...");

                for (int i = intRowHandles.Length - 1; i >= 0; i--)
                {
                    // Reduce number of linked records from parent site list by 1 //
                    intSiteContractID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "SiteContractID"));
                    intFoundRowHandle = parentView.LocateByValue(0, parentView.Columns["SiteContractID"], intSiteContractID);
                    if (intFoundRowHandle != GridControl.InvalidRowHandle) parentView.SetRowCellValue(intFoundRowHandle, "DummyMaterialCount", Convert.ToInt32(parentView.GetRowCellValue(intFoundRowHandle, "DummyMaterialCount")) - 1);

                    view.DeleteRow(intRowHandles[i]);
                }

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for unlinking from Site.", "Remove Linked Default Material Costs from Site", MessageBoxButtons.OK, MessageBoxIcon.Information);
                SetMenuStatus();
            }
        }

        private void Filter_Preferred_Material_On_Site()
        {
            GridView view = (GridView)gridControl14.MainView;
            view.PostEditor();

            string strFilterCondition = "";
            GridView parentView = (GridView)gridControl13.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                strFilterCondition = "[SiteContractID] = 9999999999";
            }
            else
            {
                foreach (int i in intRowHandles)
                {
                    strFilterCondition += (string.IsNullOrWhiteSpace(strFilterCondition) ? "" : " or ");
                    strFilterCondition += "[SiteContractID] = " + parentView.GetRowCellValue(i, "SiteContractID").ToString();
                }
            }
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = strFilterCondition;
            view.MakeRowVisible(-1, true);
            view.ExpandAllGroups();
            view.EndUpdate();
            SetMenuStatus();
        }

        private void Clear_Filter_Preferred_Material()
        {
            GridView view = (GridView)gridControl14.MainView;
            view.PostEditor();

            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
            SetMenuStatus();
        }


        #endregion


        #region Step 9 Page

        private void btnStep9Previous_Click(object sender, EventArgs e)
        {
            Filter_Preferred_Material_On_Site();
            if (checkEditSkipMaterials.Checked)
            {
                if (checkEditSkipEquipment.Checked)
                {
                    xtraTabControl1.SelectedTabPage = this.xtraTabPageStep6;  // Skip back 2 pages //
                }
                else
                {
                    xtraTabControl1.SelectedTabPage = this.xtraTabPageStep7;  // Skip back 1 page //
                }
            }
            else
            {
                xtraTabControl1.SelectedTabPage = this.xtraTabPageStep8;  // Go to previous page //
            }
        }
        private void btnStep9Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl8.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place // 
            Clear_Filter_Person_Responsibilities();
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep10;
        }

        private void bbiAddPersonResponsibility_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Add_Responsible_Person();
        }

        private void bbiBlockAddPersonResponsibility_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Block_Add_Responsible_Person();
        }

        private void Filter_Person_Responsibilities_On_Site()
        {
            GridView view = (GridView)gridControl8.MainView;
            view.PostEditor();

            string strFilterCondition = "";
            GridView parentView = (GridView)gridControl15.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                strFilterCondition = "[ResponsibleForRecordID] = 9999999999";
            }
            else
            {
                foreach (int i in intRowHandles)
                {
                    strFilterCondition += (string.IsNullOrWhiteSpace(strFilterCondition) ? "" : " or ");
                    strFilterCondition += "[ResponsibleForRecordID] = " + parentView.GetRowCellValue(i, "SiteContractID").ToString();
                }
            }
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = strFilterCondition;
            view.MakeRowVisible(-1, true);
            view.ExpandAllGroups();
            view.EndUpdate();
            SetMenuStatus();
        }

        private void Clear_Filter_Person_Responsibilities()
        {
            GridView view = (GridView)gridControl8.MainView;
            view.PostEditor();

            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
            SetMenuStatus();
        }
        
        #region GridView15

        private void gridView15_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "DummyPersonResponsibilityCount")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DummyPersonResponsibilityCount")) <= 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridView15_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            SetMenuStatus();
        }

        private void gridView15_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView8

        private void gridControl8_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Responsible_Person();
                    }
                    else if ("block edit".Equals(e.Button.Tag))
                    {
                        Block_Edit_Responsible_Person();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Responsible_Person();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView8_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.PersonResponsibility;
            SetMenuStatus();
        }

        private void gridView8_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.PersonResponsibility;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemButtonEditChoosePerson_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.Current;
                if (currentRow == null) return;
                int StaffID = 0;
                try
                {
                    StaffID = (string.IsNullOrEmpty(currentRow["StaffID"].ToString()) ? 0 : Convert.ToInt32(currentRow["StaffID"]));
                }
                catch (Exception) { }

                var fChildForm1 = new frm_OM_Client_Contract_Person_Responsibility_Select_Person_Type();
                fChildForm1.GlobalSettings = this.GlobalSettings;
                fChildForm1.ShowDialog();
                int intPersonTypeID = fChildForm1._SelectedTypeID;
                if (intPersonTypeID == -1) return;
                string strPersonType = fChildForm1._SelectedTypeDescription;

                if (intPersonTypeID == 0)  // Staff //
                {
                    var fChildForm = new frm_HR_Select_Staff();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.intOriginalStaffID = StaffID;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        if (StaffID != fChildForm.intSelectedStaffID)
                        {
                            currentRow["StaffID"] = fChildForm.intSelectedStaffID;
                            currentRow["StaffName"] = fChildForm.strSelectedStaffName;
                            currentRow["PersonTypeID"] = intPersonTypeID;
                            currentRow["PersonTypeDescription"] = strPersonType;
                            currentRow["ResponsibilityTypeID"] = 0;  // Clear and force user to re-select it since the Person Type may have changed //
                            currentRow["ResponsibilityType"] = "";  // Clear and force user to re-select it since the Person Type may have changed //
                            sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.EndEdit();
                            GridView view = (GridView)gridControl8.MainView;
                            view.FocusedColumn = colResponsibilityType;  // Move focus to next column so new value is shown in original column //
                        }
                    }
                }
                else  // Client Contact //
                {
                    var fChildForm = new frm_Core_Select_Client_Contact_Person();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.intOriginalChildSelectedID = StaffID;
                    fChildForm.intFilterSummerMaintenanceClient = 1;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        if (StaffID != fChildForm.intSelectedChildID)
                        {
                            currentRow["StaffID"] = fChildForm.intSelectedChildID;
                            currentRow["StaffName"] = fChildForm.strSelectedChildDescription;
                            currentRow["PersonTypeID"] = intPersonTypeID;
                            currentRow["PersonTypeDescription"] = strPersonType;
                            currentRow["ResponsibilityTypeID"] = 0;  // Clear and force user to re-select it since the Person Type may have changed //
                            currentRow["ResponsibilityType"] = "";  // Clear and force user to re-select it since the Person Type may have changed //
                            sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.EndEdit();
                            GridView view = (GridView)gridControl8.MainView;
                            view.FocusedColumn = colResponsibilityType;  // Move focus to next column so new value is shown in original column //
                        }
                    }
                }
            }

        }

        private void repositoryItemButtonEditPersonResponsibilityType_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString().ToLower() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.Current;
                if (currentRow == null) return;
                int ResponsibilityTypeID = 0;
                int PersonTypeID = 0;
                try
                {
                    ResponsibilityTypeID = (string.IsNullOrEmpty(currentRow["ResponsibilityTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ResponsibilityTypeID"]));
                    PersonTypeID = (string.IsNullOrEmpty(currentRow["PersonTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["PersonTypeID"]));
                }
                catch (Exception) { }

                var fChildForm = new frm_OM_Select_Person_Responsibility_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalID = ResponsibilityTypeID;
                fChildForm.strPassedInResponsibilityTypeIDs = "21,22,41,42,901,902";
                fChildForm.intPassedInPersonTypeID = PersonTypeID;
                fChildForm.intIncludeBlankRow = 0;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (ResponsibilityTypeID != fChildForm.intSelectedID)
                    {
                        currentRow["ResponsibilityTypeID"] = fChildForm.intSelectedID;
                        currentRow["ResponsibilityType"] = fChildForm.strSelectedDescription;
                        sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.EndEdit();
                        GridView view = (GridView)gridControl8.MainView;
                        view.FocusedColumn = colRemarks3;  // Move focus to next column so new value is shown in original column //
                    }
                }
            }
        }

        #endregion

        private void Add_Responsible_Person()
        {
            GridView parentView = (GridView)gridControl15.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Sites to add the Responsible Person to by clicking on them then try again.", "Add Responsible Person", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intCount > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intCount.ToString() + " Sites selected. If you proceed a new Responsible Person record will be created for each of these records.\n\nProceed?", "Add Responsible Person", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }
            GridView view = (GridView)gridControl8.MainView;
            view.PostEditor();
            parentView.BeginDataUpdate();
            view.BeginDataUpdate();
            try
            {
                foreach (int i in intRowHandles)
                {
                    DataRow drNewRow;
                    drNewRow = dataSet_OM_Contract.sp06081_OM_Client_Contract_Linked_Responsibilities_Edit.NewRow();
                    drNewRow["strMode"] = "add";
                    drNewRow["strRecordIDs"] = "";
                    drNewRow["ResponsibleForRecordID"] = Convert.ToInt32(parentView.GetRowCellValue(i, "SiteContractID"));
                    drNewRow["ResponsibleForRecordTypeID"] = 1;  // 1 = OM Site Contract.  See OM_Master_Person_Responsibility_Record_Type Table //
                    drNewRow["ResponsibilityTypeID"] = 21;  // 21 = Contract Manager //
                    drNewRow["StaffID"] = 0;
                    drNewRow["StaffName"] = "Select a Value";
                    drNewRow["ClientName"] = parentView.GetRowCellValue(i, "SiteName").ToString();
                    drNewRow["PersonTypeID"] = 0;  // 0 = Staff, 1 = Client //
                    drNewRow["PersonTypeDescription"] = "Staff";
                    dataSet_OM_Contract.sp06081_OM_Client_Contract_Linked_Responsibilities_Edit.Rows.Add(drNewRow);

                    parentView.SetRowCellValue(i, "DummyPersonResponsibilityCount", Convert.ToInt32(parentView.GetRowCellValue(i, "DummyPersonResponsibilityCount")) + 1);  // Update Count in list of Sites //
                }
            }
            catch (Exception Ex) { }
            parentView.EndDataUpdate();
            view.EndDataUpdate();
            view.ExpandAllGroups();
        }

        private void Block_Add_Responsible_Person()
        {
            if (strFormMode == "view") return;
            GridView parentView = (GridView)gridControl15.MainView;
            int[] intSiteListRowHandles = parentView.GetSelectedRows();
            int intCount = intSiteListRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Sites to block add the Responsible Persons to by clicking on them then try again.", "Block Add Responsible Persons", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm1 = new frm_OM_Client_Contract_Person_Responsibility_Select_Person_Type();
            fChildForm1.GlobalSettings = this.GlobalSettings;
            fChildForm1.ShowDialog();
            int intPersonTypeID = fChildForm1._SelectedTypeID;
            if (intPersonTypeID == -1) return;
            string strPersonType = fChildForm1._SelectedTypeDescription;

            if (intPersonTypeID == 0)  // Staff //
            {
                var fChildForm = new frm_HR_Select_Staff();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalStaffID = 0;
                fChildForm._Mode = "multiple";
                GridView EmployeeScreenView = null;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    splashScreenManager1.ShowWaitForm();
                    splashScreenManager1.SetWaitFormDescription("Adding Person Responsibilites...");

                    EmployeeScreenView = (GridView)fChildForm.gridControl1.MainView;
                    int intEmployeeRowCount = EmployeeScreenView.DataRowCount;

                    GridView view = (GridView)gridControl8.MainView;
                    view.PostEditor();
                    parentView.BeginDataUpdate();
                    view.BeginDataUpdate();
                    for (int i = 0; i < intEmployeeRowCount; i++)
                    {
                        if (Convert.ToBoolean(EmployeeScreenView.GetRowCellValue(i, "CheckMarkSelection")))
                        {
                            foreach (int intSiteListRowHandle in intSiteListRowHandles)
                            {

                                try
                                {
                                    DataRow drNewRow;
                                    drNewRow = dataSet_OM_Contract.sp06081_OM_Client_Contract_Linked_Responsibilities_Edit.NewRow();
                                    drNewRow["strMode"] = "add";
                                    drNewRow["strRecordIDs"] = "";
                                    drNewRow["ResponsibleForRecordID"] = Convert.ToInt32(parentView.GetRowCellValue(intSiteListRowHandle, "SiteContractID"));
                                    drNewRow["ResponsibleForRecordTypeID"] = 1;  // 1 = OM Site Contract.  See OM_Master_Person_Responsibility_Record_Type Table //
                                    drNewRow["ResponsibilityTypeID"] = 0;
                                    drNewRow["ResponsibilityType"] = "";                                  
                                    drNewRow["StaffID"] = Convert.ToInt32(EmployeeScreenView.GetRowCellValue(i, "intStaffID"));
                                    drNewRow["StaffName"] = EmployeeScreenView.GetRowCellValue(i, "SurnameForename").ToString();
                                    drNewRow["ClientName"] = parentView.GetRowCellValue(intSiteListRowHandle, "SiteName").ToString();
                                    drNewRow["PersonTypeID"] = 0;  // 0 = Staff, 1 = Client //
                                    drNewRow["PersonTypeDescription"] = "Staff";
                                    dataSet_OM_Contract.sp06081_OM_Client_Contract_Linked_Responsibilities_Edit.Rows.Add(drNewRow);
                                }
                                catch (Exception Ex) { }
                                parentView.SetRowCellValue(intSiteListRowHandle, "DummyPersonResponsibilityCount", Convert.ToInt32(parentView.GetRowCellValue(intSiteListRowHandle, "DummyPersonResponsibilityCount")) + 1);  // Update Count in list of Sites //
                            }
                        }
                    }

                    if (splashScreenManager1.IsSplashFormVisible)
                    {
                        splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                        splashScreenManager1.CloseWaitForm();
                    }
                    parentView.EndDataUpdate();
                    view.EndDataUpdate();
                    view.ExpandAllGroups();
                }
            }
            else  // Client //
            {
                var fChildForm = new frm_Core_Select_Client_Contact_Person();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalChildSelectedID = 0;
                fChildForm._Mode = "multiple";
                fChildForm.intFilterSummerMaintenanceClient = 1;
                GridView EmployeeScreenView = null;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    splashScreenManager1.ShowWaitForm();
                    splashScreenManager1.SetWaitFormDescription("Adding Person Responsibilites...");

                    EmployeeScreenView = (GridView)fChildForm.gridControl2.MainView;
                    int intEmployeeRowCount = EmployeeScreenView.DataRowCount;

                    GridView view = (GridView)gridControl8.MainView;
                    view.PostEditor();
                    parentView.BeginDataUpdate();
                    view.BeginDataUpdate();
                    for (int i = 0; i < intEmployeeRowCount; i++)
                    {
                        if (Convert.ToBoolean(EmployeeScreenView.GetRowCellValue(i, "CheckMarkSelection")))
                        {
                            foreach (int intSiteListRowHandle in intSiteListRowHandles)
                            {

                                try
                                {
                                    DataRow drNewRow;
                                    drNewRow = dataSet_OM_Contract.sp06081_OM_Client_Contract_Linked_Responsibilities_Edit.NewRow();
                                    drNewRow["strMode"] = "add";
                                    drNewRow["strRecordIDs"] = "";
                                    drNewRow["ResponsibleForRecordID"] = Convert.ToInt32(parentView.GetRowCellValue(intSiteListRowHandle, "SiteContractID"));
                                    drNewRow["ResponsibleForRecordTypeID"] = 1;  // 1 = OM Site Contract.  See OM_Master_Person_Responsibility_Record_Type Table //
                                    drNewRow["ResponsibilityTypeID"] = 0;
                                    drNewRow["ResponsibilityType"] = "";
                                    drNewRow["StaffID"] = Convert.ToInt32(EmployeeScreenView.GetRowCellValue(i, "ClientContactPersonID"));
                                    drNewRow["StaffName"] = EmployeeScreenView.GetRowCellValue(i, "PersonName").ToString();
                                    drNewRow["ClientName"] = parentView.GetRowCellValue(intSiteListRowHandle, "SiteName").ToString();
                                    drNewRow["PersonTypeID"] = 1;  // 0 = Staff, 1 = Client //
                                    drNewRow["PersonTypeDescription"] = "Client";
                                    dataSet_OM_Contract.sp06081_OM_Client_Contract_Linked_Responsibilities_Edit.Rows.Add(drNewRow);
                                }
                                catch (Exception Ex) { }
                                parentView.SetRowCellValue(intSiteListRowHandle, "DummyPersonResponsibilityCount", Convert.ToInt32(parentView.GetRowCellValue(intSiteListRowHandle, "DummyPersonResponsibilityCount")) + 1);  // Update Count in list of Sites //
                            }
                        }
                    }

                    if (splashScreenManager1.IsSplashFormVisible)
                    {
                        splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                        splashScreenManager1.CloseWaitForm();
                    }
                    parentView.EndDataUpdate();
                    view.EndDataUpdate();
                    view.ExpandAllGroups();
                }
            }
        }

        private void Block_Edit_Responsible_Person()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl8.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Responsible Person records to block edit then try again.", "Block Edit Responsible Persons", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Site_Contract_Wizard_Block_Edit_Responsible_Person();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strResponsibilityTypesToLoad = "21,22,901,902";
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;

                view.BeginDataUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    if (fChildForm.intStaffID != null) view.SetRowCellValue(intRowHandle, "StaffID", fChildForm.intStaffID);
                    if (fChildForm.strStaffName != null) view.SetRowCellValue(intRowHandle, "StaffName", fChildForm.strStaffName);
                    if (fChildForm.intResponsibilityTypeID != null) view.SetRowCellValue(intRowHandle, "ResponsibilityTypeID", fChildForm.intResponsibilityTypeID);
                    if (fChildForm.strResponsibilityType != null) view.SetRowCellValue(intRowHandle, "ResponsibilityType", fChildForm.strResponsibilityType);
                    if (fChildForm.intPersonTypeID != null) view.SetRowCellValue(intRowHandle, "PersonTypeID", fChildForm.intPersonTypeID);
                    if (fChildForm.strPersonTypeDescription != null) view.SetRowCellValue(intRowHandle, "PersonTypeDescription", fChildForm.strPersonTypeDescription);
                    if (fChildForm.strRemarks != null) view.SetRowCellValue(intRowHandle, "Remarks", fChildForm.strRemarks);
                }
                view.EndDataUpdate();
            }
        }

        private void Delete_Responsible_Person()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            view = (GridView)gridControl8.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Person Responsibilities to remove from the Contract by clicking on them then try again.", "Remove Linked Person Responsibilities from Contract", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? "1 Linked Person Responsibility" : Convert.ToString(intRowHandles.Length) + " Linked Person Responsibilities") + " selected for removing from the current Contract!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Person Responsibility" : "these Linked Person Responsibilities") + " will be removed from the Contract.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Contract Year from Client Contract", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                GridView parentView = (GridView)gridControl15.MainView;
                int intFoundRowHandle = 0;
                int intSiteContractID = 0;

                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Deleting...");

                for (int i = intRowHandles.Length - 1; i >= 0; i--)
                {
                    // Reduce number of linked records from parent site list by 1 //
                    intSiteContractID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "ResponsibleForRecordID"));
                    intFoundRowHandle = parentView.LocateByValue(0, parentView.Columns["SiteContractID"], intSiteContractID);
                    if (intFoundRowHandle != GridControl.InvalidRowHandle) parentView.SetRowCellValue(intFoundRowHandle, "DummyPersonResponsibilityCount", Convert.ToInt32(parentView.GetRowCellValue(intFoundRowHandle, "DummyPersonResponsibilityCount")) - 1);

                    view.DeleteRow(intRowHandles[i]);  // Delete parent row //
                }

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for unlinking from Contract.", "Remove Linked Person Responsibilities from Contract", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        #endregion


        #region Step 10 Page

        private void btnStep10Previous_Click(object sender, EventArgs e)
        {
            Filter_Person_Responsibilities_On_Site();
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep9;
        }
        private void btnStep10Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl8.MainView;

            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place // 

            int intInvalidRows = CheckRows((GridView)gridControl18.MainView);
            if (intInvalidRows > 0)
            {
                XtraMessageBox.Show("You have one or more records with errors in them (errors have a cross icon next to them). Please correct before proceeding!", "Add Rates", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            Clear_Filter_Job_Rate();
            xtraTabControl1.SelectedTabPage = this.xtraTabPageFinish;
        }

        #region GridView17

        private void gridView17_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "DummyJobRateCount")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DummyJobRateCount")) <= 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridView17_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            SetMenuStatus();
        }

        private void gridView17_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Sites;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView18

        private void gridControl18_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Job_Rate(true);
                    }
                    else if ("block edit".Equals(e.Button.Tag))
                    {
                        Block_Edit_Job_Rate();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Job_Rate();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView18_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.JobRate;
            SetMenuStatus();
        }

        private void gridView18_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.JobRate;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView18_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.FocusedColumn.FieldName.StartsWith("VisitCategoryID"))
            {
                if (Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "JobSubTypeID")) != 0)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }

        private void gridView18_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            // Code also in btnStep10Next_Click event to validate all the rows initially //
            GridView view = (GridView)sender;
            int intFocusedRow = view.FocusedRowHandle;
            switch (view.FocusedColumn.Name)
            {
                case "colJobSubType":
                    try
                    {
                        DataRow dr = view.GetDataRow(intFocusedRow);
                        if ((string.IsNullOrEmpty(e.Value.ToString().Trim()) || e.Value.ToString() == "0") && Convert.ToInt32(dr["VisitCategoryID"]) == 0)
                        {
                            dr.SetColumnError("VisitCategoryID", view.Columns["VisitCategoryID"].Caption + ": Missing value!");  // Flag row as error until a Visit Category ID has been entered //
                            dr.RowError = "Error(s) Present - correct before proceeding.";
                        }
                        else
                        {
                            dr.SetColumnError("VisitCategoryID", "");  // Flag row as error until a Visit Category ID has been entered //
                            dr.RowError = "";
                        }
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "colVisitCategoryID":
                    try
                    {
                        DataRow dr = view.GetDataRow(intFocusedRow);
                        if ((string.IsNullOrEmpty(e.Value.ToString().Trim()) || e.Value.ToString() == "0") && Convert.ToInt32(dr["JobSubTypeID"]) == 0)
                        {
                            dr.SetColumnError("VisitCategoryID", view.Columns["VisitCategoryID"].Caption + ": Missing value!");  // Flag row as error until a Visit Category ID has been entered //
                            dr.RowError = "Error(s) Present - correct before proceeding.";
                        }
                        else
                        {
                            dr.SetColumnError("VisitCategoryID", "");  // Flag row as error until a Visit Category ID has been entered //
                            dr.RowError = "";
                        }
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
        }

        private void repositoryItemButtonEditJobSubType_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")
            {
                var currentRowView = (DataRowView)sp06346OMSiteContractJobRateEditBindingSource.Current;
                var currentRow = (DataSet_OM_Contract.sp06346_OM_Site_Contract_Job_Rate_EditRow)currentRowView.Row;
                if (currentRow == null) return;

                var fChildForm = new frm_OM_Select_Job_Sub_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm._Mode = "single";
                fChildForm.intOperationManagerJobsOnly = 1;
                fChildForm.intOriginalParentID = (string.IsNullOrWhiteSpace(currentRow.JobTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.JobTypeID));
                fChildForm.intOriginalChildID = (string.IsNullOrWhiteSpace(currentRow.JobSubTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.JobSubTypeID));
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (fChildForm.intSelectedChildID == fChildForm.intOriginalChildID) return;
                    currentRow.JobSubTypeID = fChildForm.intSelectedChildID;
                    currentRow.JobTypeID = fChildForm.intSelectedParentID;
                    currentRow.JobSubType = fChildForm.strSelectedChildDescriptions;
                    currentRow.JobType = fChildForm.strSelectedParentDescriptions;
                    currentRow.VisitCategoryID = 0;  // Clear Visit Category as not appropriate to Job Rates, only generic ones //
                    sp06346OMSiteContractJobRateEditBindingSource.EndEdit();
                    GridView view = (GridView)gridControl18.MainView;
                    view.FocusedColumn = colFromDate;  // Move focus to next column so new value is shown in original column //
                    CheckRows((GridView)gridControl18.MainView);
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                var currentRowView = (DataRowView)sp06346OMSiteContractJobRateEditBindingSource.Current;
                var currentRow = (DataSet_OM_Contract.sp06346_OM_Site_Contract_Job_Rate_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                currentRow.JobSubTypeID = 0;
                currentRow.JobTypeID = 0;
                currentRow.JobSubType = "Generic Rate";
                currentRow.JobType = "";
                sp06346OMSiteContractJobRateEditBindingSource.EndEdit();
                GridView view = (GridView)gridControl18.MainView;
                view.FocusedColumn = colFromDate;  // Move focus to next column so new value is shown in original column //
                CheckRows((GridView)gridControl18.MainView);
            }
        }
        
        #endregion

        private int CheckRows(GridView view)
        {
            int intInvalidRowCount = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                intInvalidRowCount += CheckRow(view, i);
            }
            return intInvalidRowCount;
        }
        private int CheckRow(GridView view, int RowHandle)
        {
            try
            {
                DataRow dr = view.GetDataRow(RowHandle);
                if ((string.IsNullOrEmpty(dr["JobSubTypeID"].ToString().Trim()) || dr["JobSubTypeID"].ToString() == "0") && (string.IsNullOrEmpty(dr["VisitCategoryID"].ToString().Trim()) || dr["VisitCategoryID"].ToString() == "0"))
                {
                    dr.SetColumnError("VisitCategoryID", view.Columns["VisitCategoryID"].Caption + ": Missing value!");  // Flag row as error until a Visit Category ID has been entered //
                    dr.RowError = "Error(s) Present - correct before proceeding.";
                    return 1;
                }
                else
                {
                    dr.SetColumnError("VisitCategoryID", "");  // Flag row as error until a Visit Category ID has been entered //
                    dr.RowError = "";
                }
            }
            catch (Exception)
            {
            }
            return 0;
        }

        private void bbiAddJobRate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Add_Job_Rate(true);
        }

        private void bbiBlockAddJobRate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Block_Add_Job_Rate();
        }

        private void Add_Job_Rate(bool ShowWarnings)
        {
            GridView parentView = (GridView)gridControl17.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (ShowWarnings)
            {
                if (intCount <= 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Sites to add the Job Rates to by clicking on them then try again.", "Add Job Rate", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else if (intCount > 1)
                {
                    if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intCount.ToString() + " Sites selected. If you proceed a new Job Rate record will be created for each of these records.\n\nProceed?", "Add Job Rate", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
                }
            }
            GridView view = (GridView)gridControl18.MainView;
            view.PostEditor();
            parentView.BeginDataUpdate();
            view.BeginDataUpdate();
            try
            {
                foreach (int i in intRowHandles)
                {
                    DataRow drNewRow;
                    drNewRow = dataSet_OM_Contract.sp06346_OM_Site_Contract_Job_Rate_Edit.NewRow();
                    drNewRow["strMode"] = "add";
                    drNewRow["strRecordIDs"] = "";
                    drNewRow["SiteContractID"] = Convert.ToInt32(parentView.GetRowCellValue(i, "SiteContractID"));
                    drNewRow["JobTypeID"] = 0;
                    drNewRow["JobSubTypeID"] = 0;
                    drNewRow["JobType"] = "";
                    drNewRow["JobSubType"] = "Generic Rate";
                    drNewRow["TeamCost"] = (decimal)0.00;
                    drNewRow["ClientSell"] = (decimal)0.00;
                    drNewRow["SiteName"] = parentView.GetRowCellValue(i, "SiteName").ToString();
                    drNewRow["VisitCategoryID"] = 0;
                    dataSet_OM_Contract.sp06346_OM_Site_Contract_Job_Rate_Edit.Rows.Add(drNewRow);

                    parentView.SetRowCellValue(i, "DummyJobRateCount", Convert.ToInt32(parentView.GetRowCellValue(i, "DummyJobRateCount")) + 1);  // Update Count in list of Sites //
                }
            }
            catch (Exception Ex) { }
            parentView.EndDataUpdate();
            view.EndDataUpdate();
            view.ExpandAllGroups();
            CheckRows((GridView)gridControl18.MainView);
        }

        private void Block_Add_Job_Rate()
        {
            if (strFormMode == "view") return;
            GridView parentView = (GridView)gridControl17.MainView;
            int[] intSiteListRowHandles = parentView.GetSelectedRows();
            int intCount = intSiteListRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Sites to block add the Job Rates to by clicking on them then try again.", "Block Add Job Rates", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Select_Job_Sub_Type();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "multiple";
            GridView JobRateScreenView = null;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Adding Job Rates...");

                JobRateScreenView = (GridView)fChildForm.gridControl2.MainView;
                int intEmployeeRowCount = JobRateScreenView.DataRowCount;

                GridView view = (GridView)gridControl18.MainView;
                view.PostEditor();
                parentView.BeginDataUpdate();
                view.BeginDataUpdate();
                for (int i = 0; i < intEmployeeRowCount; i++)
                {
                    if (Convert.ToBoolean(JobRateScreenView.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        foreach (int intSiteListRowHandle in intSiteListRowHandles)
                        {

                            try
                            {
                                DataRow drNewRow;
                                drNewRow = dataSet_OM_Contract.sp06346_OM_Site_Contract_Job_Rate_Edit.NewRow();
                                drNewRow["strMode"] = "add";
                                drNewRow["strRecordIDs"] = "";
                                drNewRow["SiteContractID"] = Convert.ToInt32(parentView.GetRowCellValue(intSiteListRowHandle, "SiteContractID"));
                                drNewRow["JobTypeID"] = Convert.ToInt32(JobRateScreenView.GetRowCellValue(i, "JobTypeID"));
                                drNewRow["JobSubTypeID"] = Convert.ToInt32(JobRateScreenView.GetRowCellValue(i, "JobSubTypeID"));
                                drNewRow["JobType"] = JobRateScreenView.GetRowCellValue(i, "JobTypeDescription").ToString();
                                drNewRow["JobSubType"] = JobRateScreenView.GetRowCellValue(i, "JobSubTypeDescription").ToString();
                                drNewRow["TeamCost"] = (decimal)0.00;
                                drNewRow["ClientSell"] = (decimal)0.00;
                                drNewRow["SiteName"] = parentView.GetRowCellValue(intSiteListRowHandle, "SiteName").ToString();
                                drNewRow["VisitCategoryID"] = 0;
                                dataSet_OM_Contract.sp06346_OM_Site_Contract_Job_Rate_Edit.Rows.Add(drNewRow);
                            }
                            catch (Exception Ex) { }

                            parentView.SetRowCellValue(intSiteListRowHandle, "DummyJobRateCount", Convert.ToInt32(parentView.GetRowCellValue(intSiteListRowHandle, "DummyJobRateCount")) + 1);  // Update Count in list of Sites //
                        }
                    }
                }

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                parentView.EndDataUpdate();
                view.EndDataUpdate();
                view.ExpandAllGroups();
                CheckRows((GridView)gridControl18.MainView);
            }
        }

        private void Block_Edit_Job_Rate()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl18.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Job Rate records to block edit then try again.", "Block Edit Job Rates", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Site_Contract_Wizard_Block_Edit_Job_Rate();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;

                view.BeginDataUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    if (fChildForm.intJobType != null) view.SetRowCellValue(intRowHandle, "JobTypeID", fChildForm.intJobType);
                    if (fChildForm.intJobSubType != null) view.SetRowCellValue(intRowHandle, "JobSubTypeID", fChildForm.intJobSubType);
                    if (fChildForm.strJobType != null) view.SetRowCellValue(intRowHandle, "JobType", fChildForm.strJobType);
                    if (fChildForm.strJobSubType != null) view.SetRowCellValue(intRowHandle, "JobSubType", fChildForm.strJobSubType);
                    if (fChildForm.intVisitCategoryID != null) view.SetRowCellValue(intRowHandle, "VisitCategoryID", fChildForm.intVisitCategoryID);
                    if (fChildForm.decTeamCost != null) view.SetRowCellValue(intRowHandle, "TeamCost", fChildForm.decTeamCost);
                    if (fChildForm.decClientSell != null) view.SetRowCellValue(intRowHandle, "ClientSell", fChildForm.decClientSell);
                    if (fChildForm.dtFromDate != null) view.SetRowCellValue(intRowHandle, "FromDate", fChildForm.dtFromDate);
                    if (fChildForm.dtToDate != null) view.SetRowCellValue(intRowHandle, "ToDate", fChildForm.dtToDate);
                    if (fChildForm.strRemarks != null) view.SetRowCellValue(intRowHandle, "Remarks", fChildForm.strRemarks);
                }
                view.EndDataUpdate();
                CheckRows((GridView)gridControl18.MainView);
            }
        }

        private void Delete_Job_Rate()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            view = (GridView)gridControl18.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Job Rates to remove from the Site by clicking on them then try again.", "Remove Linked Job Rate from Site", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? "1 Linked Job Rate" : Convert.ToString(intRowHandles.Length) + " Linked Job Rates") + " selected for removing from the current Site!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Job Rate" : "these Linked Job Rates") + " will be removed from the Site.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Default Equipment Cost from Site", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                GridView parentView = (GridView)gridControl17.MainView;
                int intFoundRowHandle = 0;
                int intSiteContractID = 0;

                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Deleting...");

                for (int i = intRowHandles.Length - 1; i >= 0; i--)
                {
                    // Reduce number of linked records from parent site list by 1 //
                    intSiteContractID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "SiteContractID"));
                    intFoundRowHandle = parentView.LocateByValue(0, parentView.Columns["SiteContractID"], intSiteContractID);
                    if (intFoundRowHandle != GridControl.InvalidRowHandle) parentView.SetRowCellValue(intFoundRowHandle, "DummyJobRateCount", Convert.ToInt32(parentView.GetRowCellValue(intFoundRowHandle, "DummyJobRateCount")) - 1);

                    view.DeleteRow(intRowHandles[i]);
                }

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for unlinking from Site.", "Remove Linked Job Rate from Site", MessageBoxButtons.OK, MessageBoxIcon.Information);
                SetMenuStatus();
            }
        }

        private void Filter_Job_Rate()
        {
            GridView view = (GridView)gridControl18.MainView;
            view.PostEditor();

            string strFilterCondition = "";
            GridView parentView = (GridView)gridControl17.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                strFilterCondition = "[SiteContractID] = 9999999999";
            }
            else
            {
                foreach (int i in intRowHandles)
                {
                    strFilterCondition += (string.IsNullOrWhiteSpace(strFilterCondition) ? "" : " or ");
                    strFilterCondition += "[SiteContractID] = " + parentView.GetRowCellValue(i, "SiteContractID").ToString();
                }
            }
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = strFilterCondition;
            view.MakeRowVisible(-1, true);
            view.ExpandAllGroups();
            view.EndUpdate();
            SetMenuStatus();
        }

        private void Clear_Filter_Job_Rate()
        {
            GridView view = (GridView)gridControl18.MainView;
            view.PostEditor();

            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
            SetMenuStatus();
        }

        #endregion


        #region Finish Page

        private void btnFinishPrevious_Click(object sender, EventArgs e)
        {
            Filter_Job_Rate();
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep10;
        }
        private void btnFinish_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;  // Linked Site Contracts  //
            view.PostEditor();
            view = (GridView)gridControl5.MainView;  // Linked Site Contract Years //
            view.PostEditor();
            view = (GridView)gridControl7.MainView;  // Linked Site Contract Year Billing Profiles //
            view.PostEditor();
            view = (GridView)gridControl10.MainView;  // Linked Labour //
            view.PostEditor();
            view = (GridView)gridControl12.MainView;  // Linked Equipment //
            view.PostEditor();
            view = (GridView)gridControl14.MainView;  // Linked Materials //
            view.PostEditor();
            view = (GridView)gridControl8.MainView;  // Linked Person Responsibilities //
            view.PostEditor();
            view = (GridView)gridControl18.MainView;  // Linked Job Rates //
            view.PostEditor();

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Saving Changes...");

            // Save Site Contracts //           
            view = (GridView)gridControl3.MainView;
            view.BeginUpdate();
            foreach (DataRow dr in dataSet_OM_Contract.sp06090_OM_Site_Contract_Edit.Rows)
            {
                dr["DummySiteContractID"] = dr["SiteContractID"];  // Copy VS generated IDs to dummy column so we can find related children later as the save will overwrite the VS generated auto-incrementing keys //
            }
            sp06090OMSiteContractEditBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            view.EndUpdate();
            try 
            {
                sp06090_OM_Site_Contract_EditTableAdapter.Update(dataSet_OM_Contract);  // Insert, Update and Delete queries defined in Table Adapter //
                foreach (DataRow dr in dataSet_OM_Contract.sp06090_OM_Site_Contract_Edit.Rows)
                {
                    dr["strMode"] = "edit";
                }
                sp06090OMSiteContractEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the Site Contract changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
              
            // Pick up the new IDs so they can be passed back to the parent manager screen so the new records can be highlighted //
            string strNewIDs = "";
            int intSiteContractID = 0;
            int intDummySiteContractID = 0;
            int intSiteContractYearID = 0;
            int intDummySiteContractYearID = 0;
            string strSiteContractIDs = "";
            string strClientContractIDs = ",";
            string strTempClientContractID = "";
            foreach (DataRow dr in dataSet_OM_Contract.sp06090_OM_Site_Contract_Edit.Rows)
            {
                intSiteContractID = Convert.ToInt32(dr["SiteContractID"]);
                strSiteContractIDs += intSiteContractID.ToString() + ",";
                intDummySiteContractID = Convert.ToInt32(dr["DummySiteContractID"]);

                strNewIDs += intSiteContractID.ToString() + ";";
                strTempClientContractID = dr["ClientContractID"].ToString() + ",";
                if (!strClientContractIDs.Contains("," + strTempClientContractID)) strClientContractIDs += strTempClientContractID;

                // Process Linked Years and update their parent Site IDs //
                foreach (DataRow drChild in dataSet_OM_Contract.sp06092_OM_Site_Contract_Year_Edit.Rows)
                {
                    if (Convert.ToInt32(drChild["SiteContractID"]) == intDummySiteContractID)
                    {
                        intSiteContractYearID = Convert.ToInt32(drChild["SiteContractYearID"]);
                        drChild["DummySiteContractYearID"] = intSiteContractYearID;  // Copy VS generated IDs to dummy column so we can find related children later as the save will overwrite the VS generated auto-incrementing keys //
                        drChild["SiteContractID"] = intSiteContractID;  // Overwrite with new value from saved site contract //
                    }
                }

                // Process Linked Labour and update their parent Site IDs //
                foreach (DataRow drChild in dataSet_OM_Contract.sp06098_OM_Site_Contract_Labour_Cost_Edit.Rows)
                {
                    if (Convert.ToInt32(drChild["SiteContractID"]) == intDummySiteContractID) drChild["SiteContractID"] = intSiteContractID;  // Overwrite with new value from saved site contract //
                }

                // Process Linked Equipment and update their parent Site IDs //
                foreach (DataRow drChild in dataSet_OM_Contract.sp06103_OM_Site_Contract_Equipment_Cost_Edit.Rows)
                {
                    if (Convert.ToInt32(drChild["SiteContractID"]) == intDummySiteContractID) drChild["SiteContractID"] = intSiteContractID;  // Overwrite with new value from saved site contract //
                }

                // Process Linked Materials and update their parent Site IDs //
                foreach (DataRow drChild in dataSet_OM_Contract.sp06106_OM_Site_Contract_Material_Cost_Edit.Rows)
                {
                    if (Convert.ToInt32(drChild["SiteContractID"]) == intDummySiteContractID) drChild["SiteContractID"] = intSiteContractID;  // Overwrite with new value from saved site contract //
                }

                // Process Linked Person Responsibilities and update their parent Site IDs //
                foreach (DataRow drChild in dataSet_OM_Contract.sp06081_OM_Client_Contract_Linked_Responsibilities_Edit.Rows)
                {
                    if (Convert.ToInt32(drChild["ResponsibleForRecordID"]) == intDummySiteContractID) drChild["ResponsibleForRecordID"] = intSiteContractID;  // Overwrite with new value from saved site contract //
                }

                // Process Linked Job Rates and update their parent Site IDs //
                foreach (DataRow drChild in dataSet_OM_Contract.sp06346_OM_Site_Contract_Job_Rate_Edit.Rows)
                {
                    if (Convert.ToInt32(drChild["SiteContractID"]) == intDummySiteContractID) drChild["SiteContractID"] = intSiteContractID;  // Overwrite with new value from saved site contract //
                }
            }
            if (strClientContractIDs.StartsWith(",")) strClientContractIDs = strClientContractIDs.Remove(0, 1);  // Remove preceeding comma //

            sp06092OMSiteContractYearEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
            sp06098OMSiteContractLabourCostEditBindingSource.EndEdit();
            sp06103OMSiteContractEquipmentCostEditBindingSource.EndEdit();
            sp06106OMSiteContractMaterialCostEditBindingSource.EndEdit();
            sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.EndEdit();
            sp06346OMSiteContractJobRateEditBindingSource.EndEdit();

            // Save Years //
            try
            {
                sp06092_OM_Site_Contract_Year_EditTableAdapter.Update(dataSet_OM_Contract);  // Insert, Update and Delete queries defined in Table Adapter //
                foreach (DataRow drChild in dataSet_OM_Contract.sp06092_OM_Site_Contract_Year_Edit.Rows)
                {
                    drChild["strMode"] = "edit";
                }
                sp06092OMSiteContractYearEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the linked Site Contract Year changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }


            // Save Year Billing Profile //
            view = (GridView)gridControl7.MainView;
            view.BeginUpdate();
            foreach (DataRow dr in dataSet_OM_Contract.sp06092_OM_Site_Contract_Year_Edit.Rows)
            {
                intSiteContractYearID = Convert.ToInt32(dr["SiteContractYearID"]);
                intDummySiteContractYearID = Convert.ToInt32(dr["DummySiteContractYearID"]);
                foreach (DataRow drChild in dataSet_OM_Contract.sp06095_OM_Site_Contract_Year_Billing_Profile_Edit.Rows)
                {
                    if (Convert.ToInt32(drChild["SiteContractYearID"]) == intDummySiteContractYearID)
                    {
                        drChild["SiteContractYearID"] = intSiteContractYearID;  // Overwrite with new value from saved site contract year //
                    }
                }
            }
            sp06095OMSiteContractYearBillingProfileEditBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            view.EndUpdate();
            try
            {
                sp06095_OM_Site_Contract_Year_Billing_Profile_EditTableAdapter.Update(dataSet_OM_Contract);  // Insert, Update and Delete queries defined in Table Adapter //
                foreach (DataRow drChild in dataSet_OM_Contract.sp06095_OM_Site_Contract_Year_Billing_Profile_Edit.Rows)
                {
                    drChild["strMode"] = "edit";
                }
                sp06095OMSiteContractYearBillingProfileEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the linked Site Contract Year Billing Profile changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }


            // Save Linked Labour //
            try
            {
                sp06098_OM_Site_Contract_Labour_Cost_EditTableAdapter.Update(dataSet_OM_Contract);  // Insert, Update and Delete queries defined in Table Adapter //
                foreach (DataRow drChild in dataSet_OM_Contract.sp06098_OM_Site_Contract_Labour_Cost_Edit.Rows)
                {
                    drChild["strMode"] = "edit";
                }
                sp06098OMSiteContractLabourCostEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the linked Site Contract Labour changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }


            // Save Linked Equipment //
            try
            {
                sp06103_OM_Site_Contract_Equipment_Cost_EditTableAdapter.Update(dataSet_OM_Contract);  // Insert, Update and Delete queries defined in Table Adapter //
                foreach (DataRow drChild in dataSet_OM_Contract.sp06103_OM_Site_Contract_Equipment_Cost_Edit.Rows)
                {
                    drChild["strMode"] = "edit";
                }
                sp06103OMSiteContractEquipmentCostEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the linked Site Contract Equipment changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }


            // Save Linked Materials //
            try
            {
                sp06106_OM_Site_Contract_Material_Cost_EditTableAdapter.Update(dataSet_OM_Contract);  // Insert, Update and Delete queries defined in Table Adapter //
                foreach (DataRow drChild in dataSet_OM_Contract.sp06106_OM_Site_Contract_Material_Cost_Edit.Rows)
                {
                    drChild["strMode"] = "edit";
                }
                sp06106OMSiteContractMaterialCostEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the linked Site Contract Material changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }


            // Save Linked Person Responsibilities //
            try
            {
                sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter.Update(dataSet_OM_Contract);  // Insert, Update and Delete queries defined in Table Adapter //
                foreach (DataRow drChild in dataSet_OM_Contract.sp06081_OM_Client_Contract_Linked_Responsibilities_Edit.Rows)
                {
                    drChild["strMode"] = "edit";
                }
                sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the linked Site PErson Responsibilities changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }


            // Save Job Rates //
            try
            {
                sp06346_OM_Site_Contract_Job_Rate_EditTableAdapter.Update(dataSet_OM_Contract);  // Insert, Update and Delete queries defined in Table Adapter //
                foreach (DataRow drChild in dataSet_OM_Contract.sp06346_OM_Site_Contract_Job_Rate_Edit.Rows)
                {
                    drChild["strMode"] = "edit";
                }
                sp06346OMSiteContractJobRateEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the linked Site Job Rate changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_OM_Site_Contract_Manager")
                    {
                        var fParentForm = (frm_OM_Site_Contract_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Sites, strNewIDs);
                    }
                }
            }
            this.dataSet_OM_Contract.AcceptChanges();

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }

            if (checkEdit2.Checked)  // Open Visit Wizard //
            {
                if (this.ParentForm != null)
                {
                    var fChildForm = new frm_OM_Visit_Wizard();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = "";
                    fChildForm.strFormMode = "add";
                    fChildForm.strCaller = this.strCaller;
                    fChildForm.intRecordCount = 0;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.i_str_PassedInClientContractIDs = strClientContractIDs;
                    fChildForm.i_str_PassedInSiteContractIDs = strSiteContractIDs;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager2 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager2;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
                else
                {
                    XtraMessageBox.Show("Unable to open another form from this form... The form must be docked inside the main container form first.", "Open Visit Wizard", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else if (checkEdit3.Checked)  // Re-open Site Contract Wizard to create another Contract //
            {
                if (this.ParentForm != null)
                {
                    var fChildForm = new frm_OM_Site_Contract_Wizard();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = "";
                    fChildForm.strFormMode = "add";
                    fChildForm.strCaller = this.strCaller;
                    fChildForm.intRecordCount = 0;
                    fChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager2 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager2;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
                else
                {
                    XtraMessageBox.Show("Unable to open another form from this form... The form must be docked inside the main container form first.", "'Open Site Contract Wizard", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
          
            this.Close();
        }

        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.SiteYearEdit:
                    {
                        Add_Contract_Year();
                    }
                    break;
                case Utils.enmFocusedGrid.SiteYearBilling:
                    {
                        Add_Billing_Profile();
                    }
                    break;
                case Utils.enmFocusedGrid.PreferredLabour:
                    {
                        Add_Preferred_labour();
                    }
                    break;
                case Utils.enmFocusedGrid.PreferredEquipment:
                    {
                        Add_Preferred_Equipment();
                    }
                    break;
                case Utils.enmFocusedGrid.PreferredMaterial:
                    {
                        Add_Preferred_Material();
                    }
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    {
                        Add_Responsible_Person();
                    }
                    break;
                case Utils.enmFocusedGrid.JobRate:
                    {
                        Add_Job_Rate(true);
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit_Record()
        {
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.SiteEdit:
                    {
                        Block_Edit_Site_Contracts();
                    }
                    break;
                case Utils.enmFocusedGrid.SiteYearEdit:
                    {
                        Block_Edit_Site_Contract_Years();
                    }
                    break;
                case Utils.enmFocusedGrid.SiteYearBilling:
                    {
                        Block_Edit_Billing_Profile();
                    }
                    break;
                case Utils.enmFocusedGrid.PreferredLabour:
                    {
                        Block_Edit_Preferred_labour();
                    }
                    break;
                case Utils.enmFocusedGrid.PreferredEquipment:
                    {
                        Block_Edit_Preferred_Equipment();
                    }
                    break;
                case Utils.enmFocusedGrid.PreferredMaterial:
                    {
                        Block_Edit_Preferred_Material();
                    }
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    {
                        Block_Edit_Responsible_Person();
                    }
                    break;
                case Utils.enmFocusedGrid.JobRate:
                    {
                        Block_Edit_Job_Rate();
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.SiteEdit:
                    {
                        Delete_Site_Contracts();
                    }
                    break;
                case Utils.enmFocusedGrid.SiteYearEdit:
                    {
                        Delete_Contract_Year();
                    }
                    break;
                case Utils.enmFocusedGrid.SiteYearBilling:
                    {
                        Delete_Billing_Profile();
                    }
                    break;
                case Utils.enmFocusedGrid.PreferredLabour:
                    {
                        Delete_Preferred_labour();
                    }
                    break;
                case Utils.enmFocusedGrid.PreferredEquipment:
                    {
                        Delete_Preferred_Equipment();
                    }
                    break;
                case Utils.enmFocusedGrid.PreferredMaterial:
                    {
                        Delete_Preferred_Material();
                    }
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    {
                        Delete_Responsible_Person();
                    }
                    break;
                case Utils.enmFocusedGrid.JobRate:
                    {
                        Delete_Job_Rate();
                    }
                    break;
                default:
                    break;
            }
        }


        private void bbiTick_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Sites:
                    {
                        view = (GridView)gridControl2.MainView;
                    }
                    break;
                default:
                    return;
            }
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                view.BeginSelection();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);
                }
                view.EndSelection();
                view.EndUpdate();
            }
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Sites:
                    {
                        Set_Selected_Site_Count_Label();
                    }
                    break;
                default:
                    return;
            }
        }

        private void bbiUntick_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Sites:
                    {
                        view = (GridView)gridControl2.MainView;
                    }
                    break;
                default:
                    return;
            }
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                view.BeginSelection();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 0);
                }
                view.EndSelection();
                view.EndUpdate();
            }
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Sites:
                    {
                        Set_Selected_Site_Count_Label();
                    }
                    break;
                default:
                    return;
            }
        }








    }
}

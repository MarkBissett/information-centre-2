﻿namespace WoodPlan5
{
    partial class frm_OM_Tender_Visit_Wizard_Block_Edit_Visit_Info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEditTenderLabourSell = new DevExpress.XtraEditors.TextEdit();
            this.textEditTenderEquipmentSell = new DevExpress.XtraEditors.TextEdit();
            this.textEditTenderMaterialSell = new DevExpress.XtraEditors.TextEdit();
            this.textEditTenderEquipmentCost = new DevExpress.XtraEditors.TextEdit();
            this.textEditTenderMaterialCost = new DevExpress.XtraEditors.TextEdit();
            this.textEditTenderLabourCost = new DevExpress.XtraEditors.TextEdit();
            this.DaysSeparationSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.gridLookUpEditApplySellCalculationLevel = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06134OMJobCalculationLevelDescriptorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ClientPOIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientPONumberButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.gridLookUpEditApplyVisitTypeID = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06308OMVisitTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkEditCascadeDates = new DevExpress.XtraEditors.CheckEdit();
            this.textEditApplyWorkNumber = new DevExpress.XtraEditors.TextEdit();
            this.gridLookUpEditApplyVisitCategoryID = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06250OMVisitCategoriesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditApplyCostCalculationLevel = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateEditVisitEndDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditVisitStartDate = new DevExpress.XtraEditors.DateEdit();
            this.ItemForClientPOID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForVisitStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostCalculationID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemVisitCategoryID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForCascadeDates = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForVisitTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientPONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellCalculationID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDaysSeparation = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter();
            this.sp06250_OM_Visit_Categories_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06250_OM_Visit_Categories_With_BlankTableAdapter();
            this.sp06308_OM_Visit_TypesTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06308_OM_Visit_TypesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderLabourSell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderEquipmentSell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderMaterialSell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderEquipmentCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderMaterialCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderLabourCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DaysSeparationSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditApplySellCalculationLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06134OMJobCalculationLevelDescriptorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditApplyVisitTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06308OMVisitTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditCascadeDates.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditApplyWorkNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditApplyVisitCategoryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06250OMVisitCategoriesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditApplyCostCalculationLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVisitEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVisitEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVisitStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVisitStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCalculationID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemVisitCategoryID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCascadeDates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellCalculationID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDaysSeparation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(594, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 475);
            this.barDockControlBottom.Size = new System.Drawing.Size(594, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 449);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(594, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 449);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn4.Width = 53;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn1.Width = 53;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "ID";
            this.gridColumn16.FieldName = "ID";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn16.Width = 53;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "ID";
            this.gridColumn10.FieldName = "ID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn10.Width = 53;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.textEditTenderLabourSell);
            this.layoutControl1.Controls.Add(this.textEditTenderEquipmentSell);
            this.layoutControl1.Controls.Add(this.textEditTenderMaterialSell);
            this.layoutControl1.Controls.Add(this.textEditTenderEquipmentCost);
            this.layoutControl1.Controls.Add(this.textEditTenderMaterialCost);
            this.layoutControl1.Controls.Add(this.textEditTenderLabourCost);
            this.layoutControl1.Controls.Add(this.DaysSeparationSpinEdit);
            this.layoutControl1.Controls.Add(this.gridLookUpEditApplySellCalculationLevel);
            this.layoutControl1.Controls.Add(this.ClientPOIDTextEdit);
            this.layoutControl1.Controls.Add(this.ClientPONumberButtonEdit);
            this.layoutControl1.Controls.Add(this.gridLookUpEditApplyVisitTypeID);
            this.layoutControl1.Controls.Add(this.checkEditCascadeDates);
            this.layoutControl1.Controls.Add(this.textEditApplyWorkNumber);
            this.layoutControl1.Controls.Add(this.gridLookUpEditApplyVisitCategoryID);
            this.layoutControl1.Controls.Add(this.gridLookUpEditApplyCostCalculationLevel);
            this.layoutControl1.Controls.Add(this.dateEditVisitEndDate);
            this.layoutControl1.Controls.Add(this.dateEditVisitStartDate);
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientPOID});
            this.layoutControl1.Location = new System.Drawing.Point(0, 28);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1213, 293, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(593, 416);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEditTenderLabourSell
            // 
            this.textEditTenderLabourSell.Location = new System.Drawing.Point(141, 180);
            this.textEditTenderLabourSell.MenuManager = this.barManager1;
            this.textEditTenderLabourSell.Name = "textEditTenderLabourSell";
            this.textEditTenderLabourSell.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditTenderLabourSell.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderLabourSell.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditTenderLabourSell.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderLabourSell.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditTenderLabourSell.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderLabourSell.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditTenderLabourSell.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderLabourSell.Properties.Mask.EditMask = "c";
            this.textEditTenderLabourSell.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditTenderLabourSell.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditTenderLabourSell.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditTenderLabourSell, true);
            this.textEditTenderLabourSell.Size = new System.Drawing.Size(82, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditTenderLabourSell, optionsSpelling1);
            this.textEditTenderLabourSell.StyleController = this.layoutControl1;
            this.textEditTenderLabourSell.TabIndex = 24;
            // 
            // textEditTenderEquipmentSell
            // 
            this.textEditTenderEquipmentSell.Location = new System.Drawing.Point(141, 228);
            this.textEditTenderEquipmentSell.MenuManager = this.barManager1;
            this.textEditTenderEquipmentSell.Name = "textEditTenderEquipmentSell";
            this.textEditTenderEquipmentSell.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditTenderEquipmentSell.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderEquipmentSell.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditTenderEquipmentSell.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderEquipmentSell.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditTenderEquipmentSell.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderEquipmentSell.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditTenderEquipmentSell.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderEquipmentSell.Properties.Mask.EditMask = "c";
            this.textEditTenderEquipmentSell.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditTenderEquipmentSell.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditTenderEquipmentSell.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditTenderEquipmentSell, true);
            this.textEditTenderEquipmentSell.Size = new System.Drawing.Size(82, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditTenderEquipmentSell, optionsSpelling2);
            this.textEditTenderEquipmentSell.StyleController = this.layoutControl1;
            this.textEditTenderEquipmentSell.TabIndex = 43;
            // 
            // textEditTenderMaterialSell
            // 
            this.textEditTenderMaterialSell.Location = new System.Drawing.Point(141, 204);
            this.textEditTenderMaterialSell.MenuManager = this.barManager1;
            this.textEditTenderMaterialSell.Name = "textEditTenderMaterialSell";
            this.textEditTenderMaterialSell.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditTenderMaterialSell.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderMaterialSell.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditTenderMaterialSell.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderMaterialSell.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditTenderMaterialSell.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderMaterialSell.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditTenderMaterialSell.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderMaterialSell.Properties.Mask.EditMask = "c";
            this.textEditTenderMaterialSell.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditTenderMaterialSell.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditTenderMaterialSell.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditTenderMaterialSell, true);
            this.textEditTenderMaterialSell.Size = new System.Drawing.Size(82, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditTenderMaterialSell, optionsSpelling3);
            this.textEditTenderMaterialSell.StyleController = this.layoutControl1;
            this.textEditTenderMaterialSell.TabIndex = 42;
            // 
            // textEditTenderEquipmentCost
            // 
            this.textEditTenderEquipmentCost.Location = new System.Drawing.Point(141, 132);
            this.textEditTenderEquipmentCost.MenuManager = this.barManager1;
            this.textEditTenderEquipmentCost.Name = "textEditTenderEquipmentCost";
            this.textEditTenderEquipmentCost.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditTenderEquipmentCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderEquipmentCost.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditTenderEquipmentCost.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderEquipmentCost.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditTenderEquipmentCost.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderEquipmentCost.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditTenderEquipmentCost.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderEquipmentCost.Properties.Mask.EditMask = "c";
            this.textEditTenderEquipmentCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditTenderEquipmentCost.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditTenderEquipmentCost.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditTenderEquipmentCost, true);
            this.textEditTenderEquipmentCost.Size = new System.Drawing.Size(82, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditTenderEquipmentCost, optionsSpelling4);
            this.textEditTenderEquipmentCost.StyleController = this.layoutControl1;
            this.textEditTenderEquipmentCost.TabIndex = 21;
            // 
            // textEditTenderMaterialCost
            // 
            this.textEditTenderMaterialCost.Location = new System.Drawing.Point(141, 108);
            this.textEditTenderMaterialCost.MenuManager = this.barManager1;
            this.textEditTenderMaterialCost.Name = "textEditTenderMaterialCost";
            this.textEditTenderMaterialCost.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditTenderMaterialCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderMaterialCost.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditTenderMaterialCost.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderMaterialCost.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditTenderMaterialCost.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderMaterialCost.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditTenderMaterialCost.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderMaterialCost.Properties.Mask.EditMask = "c";
            this.textEditTenderMaterialCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditTenderMaterialCost.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditTenderMaterialCost.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditTenderMaterialCost, true);
            this.textEditTenderMaterialCost.Size = new System.Drawing.Size(82, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditTenderMaterialCost, optionsSpelling5);
            this.textEditTenderMaterialCost.StyleController = this.layoutControl1;
            this.textEditTenderMaterialCost.TabIndex = 20;
            // 
            // textEditTenderLabourCost
            // 
            this.textEditTenderLabourCost.Location = new System.Drawing.Point(141, 84);
            this.textEditTenderLabourCost.MenuManager = this.barManager1;
            this.textEditTenderLabourCost.Name = "textEditTenderLabourCost";
            this.textEditTenderLabourCost.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditTenderLabourCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderLabourCost.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditTenderLabourCost.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderLabourCost.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.textEditTenderLabourCost.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderLabourCost.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditTenderLabourCost.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditTenderLabourCost.Properties.Mask.EditMask = "c";
            this.textEditTenderLabourCost.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditTenderLabourCost.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditTenderLabourCost.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditTenderLabourCost, true);
            this.textEditTenderLabourCost.Size = new System.Drawing.Size(82, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditTenderLabourCost, optionsSpelling6);
            this.textEditTenderLabourCost.StyleController = this.layoutControl1;
            this.textEditTenderLabourCost.TabIndex = 23;
            // 
            // DaysSeparationSpinEdit
            // 
            this.DaysSeparationSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DaysSeparationSpinEdit.Location = new System.Drawing.Point(141, 348);
            this.DaysSeparationSpinEdit.MenuManager = this.barManager1;
            this.DaysSeparationSpinEdit.Name = "DaysSeparationSpinEdit";
            this.DaysSeparationSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DaysSeparationSpinEdit.Properties.IsFloatValue = false;
            this.DaysSeparationSpinEdit.Properties.Mask.EditMask = "f0";
            this.DaysSeparationSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DaysSeparationSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.DaysSeparationSpinEdit.Size = new System.Drawing.Size(82, 20);
            this.DaysSeparationSpinEdit.StyleController = this.layoutControl1;
            this.DaysSeparationSpinEdit.TabIndex = 61;
            // 
            // gridLookUpEditApplySellCalculationLevel
            // 
            this.gridLookUpEditApplySellCalculationLevel.Location = new System.Drawing.Point(141, 156);
            this.gridLookUpEditApplySellCalculationLevel.MenuManager = this.barManager1;
            this.gridLookUpEditApplySellCalculationLevel.Name = "gridLookUpEditApplySellCalculationLevel";
            this.gridLookUpEditApplySellCalculationLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditApplySellCalculationLevel.Properties.DataSource = this.sp06134OMJobCalculationLevelDescriptorsBindingSource;
            this.gridLookUpEditApplySellCalculationLevel.Properties.DisplayMember = "Description";
            this.gridLookUpEditApplySellCalculationLevel.Properties.NullText = "";
            this.gridLookUpEditApplySellCalculationLevel.Properties.ValueMember = "ID";
            this.gridLookUpEditApplySellCalculationLevel.Properties.View = this.gridView2;
            this.gridLookUpEditApplySellCalculationLevel.Size = new System.Drawing.Size(423, 20);
            this.gridLookUpEditApplySellCalculationLevel.StyleController = this.layoutControl1;
            this.gridLookUpEditApplySellCalculationLevel.TabIndex = 3;
            this.gridLookUpEditApplySellCalculationLevel.EditValueChanged += new System.EventHandler(this.gridLookUpEditApplySellCalculationLevel_EditValueChanged);
            // 
            // sp06134OMJobCalculationLevelDescriptorsBindingSource
            // 
            this.sp06134OMJobCalculationLevelDescriptorsBindingSource.DataMember = "sp06134_OM_Job_Calculation_Level_Descriptors";
            this.sp06134OMJobCalculationLevelDescriptorsBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.gridColumn4;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = -1;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView2.FormatRules.Add(gridFormatRule1);
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Cost Calculation Level";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "RecordOrder";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // ClientPOIDTextEdit
            // 
            this.ClientPOIDTextEdit.Location = new System.Drawing.Point(141, 180);
            this.ClientPOIDTextEdit.MenuManager = this.barManager1;
            this.ClientPOIDTextEdit.Name = "ClientPOIDTextEdit";
            this.ClientPOIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientPOIDTextEdit, true);
            this.ClientPOIDTextEdit.Size = new System.Drawing.Size(440, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientPOIDTextEdit, optionsSpelling7);
            this.ClientPOIDTextEdit.StyleController = this.layoutControl1;
            this.ClientPOIDTextEdit.TabIndex = 60;
            // 
            // ClientPONumberButtonEdit
            // 
            this.ClientPONumberButtonEdit.EditValue = "";
            this.ClientPONumberButtonEdit.Location = new System.Drawing.Point(141, 324);
            this.ClientPONumberButtonEdit.MenuManager = this.barManager1;
            this.ClientPONumberButtonEdit.Name = "ClientPONumberButtonEdit";
            this.ClientPONumberButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to Open Select Client Purchase Order screen", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to Clear the selected Client PO", "clear", null, true)});
            this.ClientPONumberButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientPONumberButtonEdit.Size = new System.Drawing.Size(423, 20);
            this.ClientPONumberButtonEdit.StyleController = this.layoutControl1;
            this.ClientPONumberButtonEdit.TabIndex = 7;
            this.ClientPONumberButtonEdit.TabStop = false;
            this.ClientPONumberButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientPONumberButtonEdit_ButtonClick);
            // 
            // gridLookUpEditApplyVisitTypeID
            // 
            this.gridLookUpEditApplyVisitTypeID.Location = new System.Drawing.Point(141, 276);
            this.gridLookUpEditApplyVisitTypeID.MenuManager = this.barManager1;
            this.gridLookUpEditApplyVisitTypeID.Name = "gridLookUpEditApplyVisitTypeID";
            this.gridLookUpEditApplyVisitTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditApplyVisitTypeID.Properties.DataSource = this.sp06308OMVisitTypesBindingSource;
            this.gridLookUpEditApplyVisitTypeID.Properties.DisplayMember = "Description";
            this.gridLookUpEditApplyVisitTypeID.Properties.NullText = "";
            this.gridLookUpEditApplyVisitTypeID.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gridLookUpEditApplyVisitTypeID.Properties.ValueMember = "ID";
            this.gridLookUpEditApplyVisitTypeID.Properties.View = this.gridView1;
            this.gridLookUpEditApplyVisitTypeID.Size = new System.Drawing.Size(423, 20);
            this.gridLookUpEditApplyVisitTypeID.StyleController = this.layoutControl1;
            this.gridLookUpEditApplyVisitTypeID.TabIndex = 5;
            // 
            // sp06308OMVisitTypesBindingSource
            // 
            this.sp06308OMVisitTypesBindingSource.DataMember = "sp06308_OM_Visit_Types";
            this.sp06308OMVisitTypesBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.colActive});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn1;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Visit Type";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 195;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "Order";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 49;
            // 
            // checkEditCascadeDates
            // 
            this.checkEditCascadeDates.Location = new System.Drawing.Point(141, 382);
            this.checkEditCascadeDates.MenuManager = this.barManager1;
            this.checkEditCascadeDates.Name = "checkEditCascadeDates";
            this.checkEditCascadeDates.Properties.Caption = "[Tick if Yes - Only used if Expected Start or End filled in]";
            this.checkEditCascadeDates.Properties.ValueChecked = 1;
            this.checkEditCascadeDates.Properties.ValueUnchecked = 0;
            this.checkEditCascadeDates.Size = new System.Drawing.Size(423, 19);
            this.checkEditCascadeDates.StyleController = this.layoutControl1;
            this.checkEditCascadeDates.TabIndex = 8;
            // 
            // textEditApplyWorkNumber
            // 
            this.textEditApplyWorkNumber.Location = new System.Drawing.Point(141, 300);
            this.textEditApplyWorkNumber.MenuManager = this.barManager1;
            this.textEditApplyWorkNumber.Name = "textEditApplyWorkNumber";
            this.textEditApplyWorkNumber.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditApplyWorkNumber, true);
            this.textEditApplyWorkNumber.Size = new System.Drawing.Size(423, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditApplyWorkNumber, optionsSpelling8);
            this.textEditApplyWorkNumber.StyleController = this.layoutControl1;
            this.textEditApplyWorkNumber.TabIndex = 6;
            // 
            // gridLookUpEditApplyVisitCategoryID
            // 
            this.gridLookUpEditApplyVisitCategoryID.Location = new System.Drawing.Point(141, 252);
            this.gridLookUpEditApplyVisitCategoryID.MenuManager = this.barManager1;
            this.gridLookUpEditApplyVisitCategoryID.Name = "gridLookUpEditApplyVisitCategoryID";
            this.gridLookUpEditApplyVisitCategoryID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditApplyVisitCategoryID.Properties.DataSource = this.sp06250OMVisitCategoriesWithBlankBindingSource;
            this.gridLookUpEditApplyVisitCategoryID.Properties.DisplayMember = "Description";
            this.gridLookUpEditApplyVisitCategoryID.Properties.NullText = "";
            this.gridLookUpEditApplyVisitCategoryID.Properties.ValueMember = "ID";
            this.gridLookUpEditApplyVisitCategoryID.Properties.View = this.gridView8;
            this.gridLookUpEditApplyVisitCategoryID.Size = new System.Drawing.Size(423, 20);
            this.gridLookUpEditApplyVisitCategoryID.StyleController = this.layoutControl1;
            this.gridLookUpEditApplyVisitCategoryID.TabIndex = 4;
            // 
            // sp06250OMVisitCategoriesWithBlankBindingSource
            // 
            this.sp06250OMVisitCategoriesWithBlankBindingSource.DataMember = "sp06250_OM_Visit_Categories_With_Blank";
            this.sp06250OMVisitCategoriesWithBlankBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18});
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn16;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView8.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView8.OptionsView.ShowIndicator = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn18, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Visit Categories";
            this.gridColumn17.FieldName = "Description";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 0;
            this.gridColumn17.Width = 220;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Order";
            this.gridColumn18.FieldName = "RecordOrder";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridLookUpEditApplyCostCalculationLevel
            // 
            this.gridLookUpEditApplyCostCalculationLevel.Location = new System.Drawing.Point(141, 60);
            this.gridLookUpEditApplyCostCalculationLevel.MenuManager = this.barManager1;
            this.gridLookUpEditApplyCostCalculationLevel.Name = "gridLookUpEditApplyCostCalculationLevel";
            this.gridLookUpEditApplyCostCalculationLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditApplyCostCalculationLevel.Properties.DataSource = this.sp06134OMJobCalculationLevelDescriptorsBindingSource;
            this.gridLookUpEditApplyCostCalculationLevel.Properties.DisplayMember = "Description";
            this.gridLookUpEditApplyCostCalculationLevel.Properties.NullText = "";
            this.gridLookUpEditApplyCostCalculationLevel.Properties.ValueMember = "ID";
            this.gridLookUpEditApplyCostCalculationLevel.Properties.View = this.gridView5;
            this.gridLookUpEditApplyCostCalculationLevel.Size = new System.Drawing.Size(423, 20);
            this.gridLookUpEditApplyCostCalculationLevel.StyleController = this.layoutControl1;
            this.gridLookUpEditApplyCostCalculationLevel.TabIndex = 2;
            this.gridLookUpEditApplyCostCalculationLevel.EditValueChanged += new System.EventHandler(this.gridLookUpEditApplyCostCalculationLevel_EditValueChanged);
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.gridColumn10;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = -1;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView5.FormatRules.Add(gridFormatRule2);
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Cost Calculation Level";
            this.gridColumn11.FieldName = "Description";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            this.gridColumn11.Width = 220;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Order";
            this.gridColumn12.FieldName = "RecordOrder";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // dateEditVisitEndDate
            // 
            this.dateEditVisitEndDate.EditValue = null;
            this.dateEditVisitEndDate.Location = new System.Drawing.Point(141, 36);
            this.dateEditVisitEndDate.MenuManager = this.barManager1;
            this.dateEditVisitEndDate.Name = "dateEditVisitEndDate";
            this.dateEditVisitEndDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditVisitEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditVisitEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditVisitEndDate.Properties.Mask.EditMask = "g";
            this.dateEditVisitEndDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditVisitEndDate.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditVisitEndDate.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateEditVisitEndDate.Size = new System.Drawing.Size(134, 20);
            this.dateEditVisitEndDate.StyleController = this.layoutControl1;
            this.dateEditVisitEndDate.TabIndex = 1;
            // 
            // dateEditVisitStartDate
            // 
            this.dateEditVisitStartDate.EditValue = null;
            this.dateEditVisitStartDate.Location = new System.Drawing.Point(141, 12);
            this.dateEditVisitStartDate.MenuManager = this.barManager1;
            this.dateEditVisitStartDate.Name = "dateEditVisitStartDate";
            this.dateEditVisitStartDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditVisitStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditVisitStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditVisitStartDate.Properties.Mask.EditMask = "g";
            this.dateEditVisitStartDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditVisitStartDate.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditVisitStartDate.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateEditVisitStartDate.Size = new System.Drawing.Size(134, 20);
            this.dateEditVisitStartDate.StyleController = this.layoutControl1;
            this.dateEditVisitStartDate.TabIndex = 0;
            // 
            // ItemForClientPOID
            // 
            this.ItemForClientPOID.Control = this.ClientPOIDTextEdit;
            this.ItemForClientPOID.Location = new System.Drawing.Point(0, 168);
            this.ItemForClientPOID.Name = "ItemForClientPOID";
            this.ItemForClientPOID.Size = new System.Drawing.Size(573, 24);
            this.ItemForClientPOID.Text = "Client PO ID:";
            this.ItemForClientPOID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForVisitStartDate,
            this.ItemForVisitEndDate,
            this.ItemForCostCalculationID,
            this.ItemVisitCategoryID,
            this.ItemForWorkNumber,
            this.emptySpaceItem1,
            this.ItemForCascadeDates,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.ItemForVisitTypeID,
            this.ItemForClientPONumber,
            this.ItemForSellCalculationID,
            this.ItemForDaysSeparation,
            this.emptySpaceItem4,
            this.layoutControlItem1,
            this.emptySpaceItem5,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.emptySpaceItem6,
            this.layoutControlItem5,
            this.layoutControlItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(576, 423);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // ItemForVisitStartDate
            // 
            this.ItemForVisitStartDate.Control = this.dateEditVisitStartDate;
            this.ItemForVisitStartDate.CustomizationFormText = "Visit Expected Start:";
            this.ItemForVisitStartDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForVisitStartDate.Name = "ItemForVisitStartDate";
            this.ItemForVisitStartDate.Size = new System.Drawing.Size(267, 24);
            this.ItemForVisitStartDate.Text = "Visit Expected Start:";
            this.ItemForVisitStartDate.TextSize = new System.Drawing.Size(126, 13);
            // 
            // ItemForVisitEndDate
            // 
            this.ItemForVisitEndDate.Control = this.dateEditVisitEndDate;
            this.ItemForVisitEndDate.CustomizationFormText = "Visit End Date:";
            this.ItemForVisitEndDate.Location = new System.Drawing.Point(0, 24);
            this.ItemForVisitEndDate.Name = "ItemForVisitEndDate";
            this.ItemForVisitEndDate.Size = new System.Drawing.Size(267, 24);
            this.ItemForVisitEndDate.Text = "Visit Expected End:";
            this.ItemForVisitEndDate.TextSize = new System.Drawing.Size(126, 13);
            // 
            // ItemForCostCalculationID
            // 
            this.ItemForCostCalculationID.Control = this.gridLookUpEditApplyCostCalculationLevel;
            this.ItemForCostCalculationID.CustomizationFormText = "Cost Calculation:";
            this.ItemForCostCalculationID.Location = new System.Drawing.Point(0, 48);
            this.ItemForCostCalculationID.Name = "ItemForCostCalculationID";
            this.ItemForCostCalculationID.Size = new System.Drawing.Size(556, 24);
            this.ItemForCostCalculationID.Text = "Cost Calculation:";
            this.ItemForCostCalculationID.TextSize = new System.Drawing.Size(126, 13);
            // 
            // ItemVisitCategoryID
            // 
            this.ItemVisitCategoryID.Control = this.gridLookUpEditApplyVisitCategoryID;
            this.ItemVisitCategoryID.CustomizationFormText = "Visit Category:";
            this.ItemVisitCategoryID.Location = new System.Drawing.Point(0, 240);
            this.ItemVisitCategoryID.Name = "ItemVisitCategoryID";
            this.ItemVisitCategoryID.Size = new System.Drawing.Size(556, 24);
            this.ItemVisitCategoryID.Text = "Visit Category:";
            this.ItemVisitCategoryID.TextSize = new System.Drawing.Size(126, 13);
            // 
            // ItemForWorkNumber
            // 
            this.ItemForWorkNumber.Control = this.textEditApplyWorkNumber;
            this.ItemForWorkNumber.CustomizationFormText = "Work Number:";
            this.ItemForWorkNumber.Location = new System.Drawing.Point(0, 288);
            this.ItemForWorkNumber.Name = "ItemForWorkNumber";
            this.ItemForWorkNumber.Size = new System.Drawing.Size(556, 24);
            this.ItemForWorkNumber.Text = "Work Number:";
            this.ItemForWorkNumber.TextSize = new System.Drawing.Size(126, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(267, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(289, 48);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForCascadeDates
            // 
            this.ItemForCascadeDates.Control = this.checkEditCascadeDates;
            this.ItemForCascadeDates.CustomizationFormText = "Update Subsequent Visits:";
            this.ItemForCascadeDates.Location = new System.Drawing.Point(0, 370);
            this.ItemForCascadeDates.Name = "ItemForCascadeDates";
            this.ItemForCascadeDates.Size = new System.Drawing.Size(556, 23);
            this.ItemForCascadeDates.Text = "Update Subsequent Visits:";
            this.ItemForCascadeDates.TextSize = new System.Drawing.Size(126, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 360);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(556, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 393);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(556, 10);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForVisitTypeID
            // 
            this.ItemForVisitTypeID.Control = this.gridLookUpEditApplyVisitTypeID;
            this.ItemForVisitTypeID.Location = new System.Drawing.Point(0, 264);
            this.ItemForVisitTypeID.Name = "ItemForVisitTypeID";
            this.ItemForVisitTypeID.Size = new System.Drawing.Size(556, 24);
            this.ItemForVisitTypeID.Text = "Visit Type:";
            this.ItemForVisitTypeID.TextSize = new System.Drawing.Size(126, 13);
            // 
            // ItemForClientPONumber
            // 
            this.ItemForClientPONumber.Control = this.ClientPONumberButtonEdit;
            this.ItemForClientPONumber.Location = new System.Drawing.Point(0, 312);
            this.ItemForClientPONumber.Name = "ItemForClientPONumber";
            this.ItemForClientPONumber.Size = new System.Drawing.Size(556, 24);
            this.ItemForClientPONumber.Text = "Client PO #:";
            this.ItemForClientPONumber.TextSize = new System.Drawing.Size(126, 13);
            // 
            // ItemForSellCalculationID
            // 
            this.ItemForSellCalculationID.Control = this.gridLookUpEditApplySellCalculationLevel;
            this.ItemForSellCalculationID.Location = new System.Drawing.Point(0, 144);
            this.ItemForSellCalculationID.Name = "ItemForSellCalculationID";
            this.ItemForSellCalculationID.Size = new System.Drawing.Size(556, 24);
            this.ItemForSellCalculationID.Text = "Sell Calculation:";
            this.ItemForSellCalculationID.TextSize = new System.Drawing.Size(126, 13);
            // 
            // ItemForDaysSeparation
            // 
            this.ItemForDaysSeparation.Control = this.DaysSeparationSpinEdit;
            this.ItemForDaysSeparation.Location = new System.Drawing.Point(0, 336);
            this.ItemForDaysSeparation.MaxSize = new System.Drawing.Size(215, 24);
            this.ItemForDaysSeparation.MinSize = new System.Drawing.Size(215, 24);
            this.ItemForDaysSeparation.Name = "ItemForDaysSeparation";
            this.ItemForDaysSeparation.Size = new System.Drawing.Size(215, 24);
            this.ItemForDaysSeparation.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDaysSeparation.Text = "Visit Days Separation:";
            this.ItemForDaysSeparation.TextSize = new System.Drawing.Size(126, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(215, 336);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(341, 24);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textEditTenderLabourCost;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(215, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(215, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(215, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "Labour Cost:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(126, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(215, 72);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(341, 72);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEditTenderMaterialCost;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(215, 0);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(215, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(215, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Material Cost:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(126, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textEditTenderEquipmentCost;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(215, 0);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(215, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(215, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Equipment Cost:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(126, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEditTenderLabourSell;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(215, 0);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(215, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(215, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "Labour Sell:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(126, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(215, 168);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(341, 72);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEditTenderMaterialSell;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 192);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(215, 0);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(215, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(215, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "Material Sell:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(126, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEditTenderEquipmentSell;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 216);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(215, 0);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(215, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(215, 24);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "Equipment Sell:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(126, 13);
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(374, 445);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(96, 22);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(485, 445);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(96, 22);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter
            // 
            this.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06250_OM_Visit_Categories_With_BlankTableAdapter
            // 
            this.sp06250_OM_Visit_Categories_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06308_OM_Visit_TypesTableAdapter
            // 
            this.sp06308_OM_Visit_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Tender_Visit_Wizard_Block_Edit_Visit_Info
            // 
            this.ClientSize = new System.Drawing.Size(594, 475);
            this.ControlBox = false;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OM_Tender_Visit_Wizard_Block_Edit_Visit_Info";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tender Visit Wizard - Block Edit Visits";
            this.Load += new System.EventHandler(this.frm_OM_Tender_Visit_Wizard_Block_Edit_Visit_Info_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderLabourSell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderEquipmentSell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderMaterialSell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderEquipmentCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderMaterialCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTenderLabourCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DaysSeparationSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditApplySellCalculationLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06134OMJobCalculationLevelDescriptorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPOIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditApplyVisitTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06308OMVisitTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditCascadeDates.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditApplyWorkNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditApplyVisitCategoryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06250OMVisitCategoriesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditApplyCostCalculationLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVisitEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVisitEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVisitStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditVisitStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPOID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCalculationID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemVisitCategoryID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCascadeDates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellCalculationID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDaysSeparation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DevExpress.XtraEditors.DateEdit dateEditVisitStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitStartDate;
        private DevExpress.XtraEditors.DateEdit dateEditVisitEndDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitEndDate;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditApplyCostCalculationLevel;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostCalculationID;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditApplyVisitCategoryID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraLayout.LayoutControlItem ItemVisitCategoryID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.TextEdit textEditApplyWorkNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkNumber;
        private System.Windows.Forms.BindingSource sp06134OMJobCalculationLevelDescriptorsBindingSource;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private DataSet_OM_VisitTableAdapters.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter;
        private System.Windows.Forms.BindingSource sp06250OMVisitCategoriesWithBlankBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06250_OM_Visit_Categories_With_BlankTableAdapter sp06250_OM_Visit_Categories_With_BlankTableAdapter;
        private DevExpress.XtraEditors.CheckEdit checkEditCascadeDates;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCascadeDates;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditApplyVisitTypeID;
        private System.Windows.Forms.BindingSource sp06308OMVisitTypesBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitTypeID;
        private DataSet_OM_VisitTableAdapters.sp06308_OM_Visit_TypesTableAdapter sp06308_OM_Visit_TypesTableAdapter;
        private DevExpress.XtraEditors.ButtonEdit ClientPONumberButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPONumber;
        private DevExpress.XtraEditors.TextEdit ClientPOIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPOID;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditApplySellCalculationLevel;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellCalculationID;
        private DevExpress.XtraEditors.SpinEdit DaysSeparationSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDaysSeparation;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.TextEdit textEditTenderLabourCost;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit textEditTenderMaterialCost;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit textEditTenderEquipmentCost;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit textEditTenderMaterialSell;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.TextEdit textEditTenderEquipmentSell;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.TextEdit textEditTenderLabourSell;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
    }
}

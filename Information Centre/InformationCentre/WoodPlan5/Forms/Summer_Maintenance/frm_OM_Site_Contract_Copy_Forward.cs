﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;
using DevExpress.XtraEditors;

namespace WoodPlan5
{
    public partial class frm_OM_Site_Contract_Copy_Forward : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public int? _SelectedCount = 1;
        public bool _NewContractStart = true;
        public DateTime? _NewContractStartDate;
        public DateTime? _NewContractEndDate;
        public int? _ContractAddStartUnits = 0;
        public string _ContractAddStartUnitDescriptor = "";

        public bool _ApplyNewContractValue = true;
        public decimal? _NewContractValue = (decimal)0.00;
        public bool _ApplyAddUpliftPercentage = true;
        public decimal? _NewContractUpliftPercentage = (decimal)0.00;
        public bool _ApplyUpliftValue = false;
        public decimal? _NewContractUpliftValue = (decimal)0.00;

        public bool _KeepYearlyPercentageIncrease = true;
        public bool _NewYearlyPerecentageIncrease = false;
        public decimal? _NewContractYearlyPerecentageIncrease = (decimal)0.00;

        public bool _NewContractActive = false;
        public bool _ExistingContractInactive = false;

        public bool _SiteInstructionsAppend = true;
        public bool _SiteInstructionsNew = false;
        public string _SiteInstructions = "";

        public bool _YearAndBillingProfile = true;
        public bool _PersonResponsibilities = true;
        public bool _PreferredLabour = true;
        public bool _DefaultEquipmentCosts = true;
        public bool _DefaultMaterialCosts = true;
        public bool _SiteContractBillingRequirements = true;

        public bool _VisitStructure = true;
        public bool _HoldDayOfWeek = true;

        public bool _VisitPersonResponsibilities = true;
        public bool _VisitBillingRequirements = true;

        public bool _JobStructure = true;
        public bool _JobLabour = true;
        public bool _JobEquipment = true;
        public bool _JobMaterials = true;
        public bool _JobHealthAndSafety = true;

        #endregion

        public frm_OM_Site_Contract_Copy_Forward()
        {
            InitializeComponent();
        }

        private void frm_OM_Site_Contract_Copy_Forward_Load(object sender, EventArgs e)
        {
            this.FormID = 500225;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //

            bsiSiteContractCount.Caption = "<b>" + _SelectedCount.ToString() + "</b> Site Contract" + (_SelectedCount > 1 ? "s" : "") + " Selected.";
            strConnectionString = this.GlobalSettings.ConnectionString;

            checkEditStartAndEnd.Checked = true;
            dateEditContractStartDate.EditValue = null;
            dateEditContractEndDate.EditValue = null;
            spinEditAddUnits.EditValue = 1;
            comboBoxEditAddUnitsDescriptor.EditValue = "Years";

            checkEditNewContractValue.Checked = true;
            spinEditContractValue.EditValue = (decimal)0.00;
            spinEditUpliftPercentage.EditValue = (decimal)0.00;
            spinEditUpliftAmount.EditValue = (decimal)0.00;

            checkEditKeepYearlyPercentageIncrease.Checked = true;
            spinEditYearPercentageIncreaseAmount.EditValue = (decimal)0.00;

            checkEditNewActive.Checked = false;
            checkEditSourceInactive.Checked = false;

            checkEditSiteInstructionsAppend.Checked = true;
            MemoEditSiteInstructions.EditValue = null;

            checkEditStartAndEnd_CheckedChanged(checkEditStartAndEnd, null);
            checkEditAddValueToDates_CheckedChanged(checkEditAddValueToDates, null);

            checkEditNewContractValue_CheckedChanged(checkEditNewContractValue, null);
            checkEditUpliftAmount_CheckedChanged(checkEditUpliftAmount, null);
            checkEditAddUpliftPercentage_CheckedChanged(checkEditAddUpliftPercentage, null);
            checkEditAddUpliftAmount_CheckedChanged(checkEditAddUpliftAmount, null);
            checkEditNewYearlyPerecentageIncrease_CheckedChanged(checkEditNewYearlyPercentageIncrease, null);

            checkEditYearAndBillingProfile.Checked = true;
            checkEditPersonResponsibilities.Checked = true;
            checkEditPreferredLabour.Checked = true;
            checkEditDefaultEquipmentCosts.Checked = true;
            checkEditDefaultMaterialCosts.Checked = true;
            checkEditVisitStructure.Checked = true;
            checkEditHoldDayOfWeek.Checked = true;
            checkEditVisitPersonResponsibilities.Checked = true;
            checkEditJobStructure.Checked = true;
            checkEditJobLabour.Checked = true;
            checkEditJobEquipment.Checked = true;
            checkEditJobMaterials.Checked = true;
            checkEditJobHealthAndSafety.Checked = true;

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            bsiSiteContractCount.Caption = "<b>" + _SelectedCount.ToString() + "</b> Site Contract" + (_SelectedCount > 1 ? "s" : "") + " Selected.";
        }

      
        private void btnOK_Click(object sender, EventArgs e)
        {
            _NewContractStart = checkEditStartAndEnd.Checked;
            if (dateEditContractStartDate.EditValue != null) _NewContractStartDate = Convert.ToDateTime(dateEditContractStartDate.EditValue);
            if (dateEditContractEndDate.EditValue != null) _NewContractEndDate = Convert.ToDateTime(dateEditContractEndDate.EditValue);
            if (spinEditAddUnits.EditValue != null) _ContractAddStartUnits = Convert.ToInt32(spinEditAddUnits.EditValue);
            if (comboBoxEditAddUnitsDescriptor.EditValue != null) _ContractAddStartUnitDescriptor = comboBoxEditAddUnitsDescriptor.EditValue.ToString();

            if (_NewContractStart)
            {
                if (_NewContractStartDate == null || _NewContractEndDate == null || _NewContractEndDate < _NewContractStartDate)
                {
                    XtraMessageBox.Show("Both the Contract Start Date and End date must be entered and the End date must be >= the Start date before proceeding.", "Copy Forward Site Contract(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else  // Check Units and Unit Descriptors are filled in //
            {
                if (_ContractAddStartUnits == 0 || string.IsNullOrWhiteSpace(_ContractAddStartUnitDescriptor))
                {
                    XtraMessageBox.Show("Both the Units and Unit Descriptor to add to the current Contract Start and End Dates must be entered before proceeding.", "Copy Forward Site Contract(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

            _ApplyNewContractValue = checkEditNewContractValue.Checked;
            if (spinEditContractValue.EditValue != null) _NewContractValue = Convert.ToDecimal(spinEditContractValue.EditValue);

            _ApplyAddUpliftPercentage = checkEditAddUpliftPercentage.Checked;
            if (spinEditUpliftPercentage.EditValue != null) _NewContractUpliftPercentage = Convert.ToDecimal(spinEditUpliftPercentage.EditValue);

            _ApplyUpliftValue = checkEditAddUpliftAmount.Checked;
            if (spinEditUpliftAmount.EditValue != null) _NewContractUpliftValue = Convert.ToDecimal(spinEditUpliftAmount.EditValue);

            _KeepYearlyPercentageIncrease = checkEditKeepYearlyPercentageIncrease.Checked;
            _NewYearlyPerecentageIncrease = checkEditNewYearlyPercentageIncrease.Checked;
            if (spinEditYearPercentageIncreaseAmount.EditValue != null) _NewContractYearlyPerecentageIncrease = Convert.ToDecimal(spinEditYearPercentageIncreaseAmount.EditValue);

            _NewContractActive = checkEditNewActive.Checked;
            _ExistingContractInactive = checkEditSourceInactive.Checked;

            _SiteInstructionsAppend = checkEditSiteInstructionsAppend.Checked;
            _SiteInstructionsNew = checkEditSiteInstructionsNew.Checked;
            if (MemoEditSiteInstructions.EditValue != null) _SiteInstructions = MemoEditSiteInstructions.EditValue.ToString();

            _YearAndBillingProfile = checkEditYearAndBillingProfile.Checked;
            _PersonResponsibilities = checkEditPersonResponsibilities.Checked;
            _PreferredLabour = checkEditPreferredLabour.Checked;
            _DefaultEquipmentCosts = checkEditDefaultEquipmentCosts.Checked;
            _DefaultMaterialCosts = checkEditDefaultMaterialCosts.Checked;
            _SiteContractBillingRequirements = checkEditSiteContractBillingRequirements.Checked;
            _VisitStructure = checkEditVisitStructure.Checked;
            _HoldDayOfWeek = checkEditHoldDayOfWeek.Checked;
            _VisitPersonResponsibilities = checkEditVisitPersonResponsibilities.Checked;
            _VisitBillingRequirements = checkEditVisitBillingRequirements.Checked;
            _JobStructure = checkEditJobStructure.Checked;
            _JobLabour = checkEditJobLabour.Checked;
            _JobEquipment = checkEditJobEquipment.Checked;
            _JobMaterials = checkEditJobMaterials.Checked;
            _JobHealthAndSafety = checkEditJobHealthAndSafety.Checked;

            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }


        #region Editors

        private void checkEditStartAndEnd_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                dateEditContractStartDate.Properties.ReadOnly = false;
                dateEditContractEndDate.Properties.ReadOnly = false;
                labelControl2.Enabled = true;
                labelControl3.Enabled = true;
            }
            else
            {
                dateEditContractStartDate.Properties.ReadOnly = true;
                dateEditContractEndDate.Properties.ReadOnly = true;
                dateEditContractStartDate.EditValue = null;
                dateEditContractEndDate.EditValue = null;
                labelControl2.Enabled = false;
                labelControl3.Enabled = false;
            }
        }

        private void checkEditAddValueToDates_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                spinEditAddUnits.Properties.ReadOnly = false;
                comboBoxEditAddUnitsDescriptor.Properties.ReadOnly = false;
                labelControl1.Enabled = true;
            }
            else
            {
                spinEditAddUnits.Properties.ReadOnly = true;
                comboBoxEditAddUnitsDescriptor.Properties.ReadOnly = true;
                spinEditAddUnits.EditValue = 1;
                comboBoxEditAddUnitsDescriptor.EditValue = "Years";
                labelControl1.Enabled = false;
            }
        }

        private void checkEditNewContractValue_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                spinEditContractValue.Properties.ReadOnly = false;
            }
            else
            {
                spinEditContractValue.Properties.ReadOnly = true;
                spinEditContractValue.EditValue = (decimal)0.00;
            }

        }

        private void checkEditUpliftAmount_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
               checkEditAddUpliftPercentage.Properties.ReadOnly = false;
               checkEditAddUpliftPercentage.Checked = true;
               checkEditAddUpliftAmount.Properties.ReadOnly = false;
            }
            else
            {
                checkEditAddUpliftPercentage.Properties.ReadOnly = true;
                checkEditAddUpliftPercentage.Checked = false;
                checkEditAddUpliftAmount.Properties.ReadOnly = false;
                checkEditAddUpliftAmount.Checked = false;
            }
        }

        private void checkEditAddUpliftPercentage_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                spinEditUpliftPercentage.Properties.ReadOnly = false;
                labelControl8.Enabled = true;
            }
            else
            {
                spinEditUpliftPercentage.Properties.ReadOnly = true;
                labelControl8.Enabled = false;
                spinEditUpliftPercentage.EditValue = (decimal)0.00;
            }
        }

        private void checkEditAddUpliftAmount_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked && checkEditUpliftAmount.Checked)
            {
                spinEditUpliftAmount.Properties.ReadOnly = false;
                labelControl6.Enabled = true;
            }
            else
            {
                spinEditUpliftAmount.Properties.ReadOnly = true;
                labelControl6.Enabled = false;
                spinEditUpliftAmount.EditValue = (decimal)0.00;
            }
        }

        private void checkEditNewYearlyPerecentageIncrease_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                spinEditYearPercentageIncreaseAmount.Properties.ReadOnly = false;
            }
            else
            {
                spinEditYearPercentageIncreaseAmount.Properties.ReadOnly = true;
                spinEditYearPercentageIncreaseAmount.EditValue = (decimal)0.00;
            }
        }

        private void checkEditVisitStructure_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                checkEditHoldDayOfWeek.Properties.ReadOnly = false;
                checkEditVisitPersonResponsibilities.Properties.ReadOnly = false;
                checkEditVisitBillingRequirements.Properties.ReadOnly = false;
                checkEditJobStructure.Properties.ReadOnly = false;
                checkEditJobLabour.Properties.ReadOnly = !checkEditJobStructure.Checked;
                checkEditJobEquipment.Properties.ReadOnly = !checkEditJobStructure.Checked;
                checkEditJobMaterials.Properties.ReadOnly = !checkEditJobStructure.Checked;
                checkEditJobHealthAndSafety.Properties.ReadOnly = !checkEditJobStructure.Checked;
            }
            else
            {
                checkEditHoldDayOfWeek.Properties.ReadOnly = true;
                checkEditHoldDayOfWeek.Checked = false;

                checkEditVisitPersonResponsibilities.Properties.ReadOnly = true;
                checkEditVisitPersonResponsibilities.Checked = false;

                checkEditVisitBillingRequirements.Properties.ReadOnly = true;
                checkEditVisitBillingRequirements.Checked = false;

                checkEditJobStructure.Properties.ReadOnly = true;
                checkEditJobStructure.Checked = false;

                checkEditJobLabour.Properties.ReadOnly = true;
                checkEditJobLabour.Checked = false;

                checkEditJobEquipment.Properties.ReadOnly = true;
                checkEditJobEquipment.Checked = false;

                checkEditJobMaterials.Properties.ReadOnly = true;
                checkEditJobMaterials.Checked = false;

                checkEditJobHealthAndSafety.Properties.ReadOnly = true;
                checkEditJobHealthAndSafety.Checked = false;
            }
        }

        private void checkEditJobStructure_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                checkEditJobLabour.Properties.ReadOnly = false;
                checkEditJobEquipment.Properties.ReadOnly = false;
                checkEditJobMaterials.Properties.ReadOnly = false;
                checkEditJobHealthAndSafety.Properties.ReadOnly = false;
            }
            else
            {
                checkEditJobLabour.Properties.ReadOnly = true;
                checkEditJobLabour.Checked = false;

                checkEditJobEquipment.Properties.ReadOnly = true;
                checkEditJobEquipment.Checked = false;

                checkEditJobMaterials.Properties.ReadOnly = true;
                checkEditJobMaterials.Checked = false;

                checkEditJobHealthAndSafety.Properties.ReadOnly = true;
                checkEditJobHealthAndSafety.Checked = false;
            }
        }

        #endregion








    }
}

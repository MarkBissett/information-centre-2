﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;

namespace WoodPlan5
{
    public partial class frm_OM_Site_Contract_Labour_Reassign : BaseObjects.frmBase
    {

        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        bool internalRowFocusing;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_TemplateManager = false;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        public string i_str_PassedInIDs = "";
        
        #endregion

        public frm_OM_Site_Contract_Labour_Reassign()
        {
            InitializeComponent();
        }

        private void frm_OM_Site_Contract_Labour_Reassign_Load(object sender, EventArgs e)
        {
            this.FormID = 50022300;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp06101_OM_Work_Unit_Types_PicklistTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06101_OM_Work_Unit_Types_PicklistTableAdapter.Fill(dataSet_OM_Contract.sp06101_OM_Work_Unit_Types_Picklist, 0);

            sp06292_OM_Site_Contract_Labour_Reassign_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06292_OM_Site_Contract_Labour_Reassign_ListTableAdapter.Fill(dataSet_OM_Contract.sp06292_OM_Site_Contract_Labour_Reassign_List, i_str_PassedInIDs, "edit");

            gridControl10.ForceInitialize();
            GridView view = (GridView)gridControl10.MainView;
            view.ExpandAllGroups();
            emptyEditor = new RepositoryItem();
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            ConfigureFormAccordingToMode();
            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }


        private void ProcessPermissionsForForm()
        {
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            GridView view = (GridView)gridControl10.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            bsiAdd.Enabled = true;
            if (intRowHandles.Length >= 2)
            {
                alItems.Add("iBlockAdd");
                bbiBlockAdd.Enabled = true;
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            gridControl10.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intRowHandles.Length >= 2);
        }

        private void frm_OM_Site_Contract_Labour_Reassign_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

            }
        }

        private void frm_OM_Site_Contract_Labour_Reassign_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }
        
        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            GridView view = (GridView)gridControl10.MainView;  // Linked Labour //
            view.PostEditor();

            for (int i = 0; i < this.dataSet_OM_Contract.sp06292_OM_Site_Contract_Labour_Reassign_List.Rows.Count; i++)
            {
                switch (this.dataSet_OM_Contract.sp06292_OM_Site_Contract_Labour_Reassign_List.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }


        #region GridView10

        private void gridControl10_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("block edit".Equals(e.Button.Tag))
                    {
                        Block_Edit_Preferred_labour();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView10_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "ContractorName")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "OriginalContractorID")) != Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ContractorID")))
                {
                    e.Appearance.BackColor = Color.FromArgb(0xB9, 0xFB, 0xB9);
                    e.Appearance.BackColor2 = Color.PaleGreen;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            if (e.Column.FieldName == "LinkedToPersonType")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "OriginalLinkedToPersonTypeID")) != Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedToPersonTypeID")))
                {
                    e.Appearance.BackColor = Color.FromArgb(0xB9, 0xFB, 0xB9);
                    e.Appearance.BackColor2 = Color.PaleGreen;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridView10_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Site Preferred Labour Available - Select one or more Sites from left list to view linked Preferred Labour. Click the Add button to add Preferred Labour");
        }

        private void gridView10_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView10_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView10_GotFocus(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void gridView10_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView10_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView10_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void repositoryItemButtonEditChoosePersonType_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06292OMSiteContractLabourReassignListBindingSource.Current;
                var currentRow = (DataSet_OM_Contract.sp06292_OM_Site_Contract_Labour_Reassign_ListRow)currentRowView.Row;
                var fChildForm = new frm_Core_Select_Person_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (currentRow == null) return;
                int intOriginalPersonTypeID = 0;
                try { intOriginalPersonTypeID = (string.IsNullOrEmpty(currentRow.LinkedToPersonTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.LinkedToPersonTypeID)); }
                catch (Exception) { }

                fChildForm.intOriginalPersonTypeID = intOriginalPersonTypeID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.LinkedToPersonTypeID = fChildForm.intSelectedPersonTypeID;
                    currentRow.LinkedToPersonType = fChildForm.strSelectedPersonType;

                    if (intOriginalPersonTypeID != fChildForm.intSelectedPersonTypeID)  // Clear selected person since the person type has changed //
                    {
                        currentRow.ContractorID = 0;
                        currentRow.ContractorName = "";
                        currentRow.PostcodeSiteDistance = (double)0.00;
                        currentRow.LatLongSiteDistance = (double)0.00;
                        //dxErrorProvider1.SetError(LinkedToContractorName, "Select a value.");
                    }
                    sp06292OMSiteContractLabourReassignListBindingSource.EndEdit();
                    GridView view = (GridView)gridControl10.MainView;
                    view.FocusedColumn = colContractorName;  // Move focus to next column so new value is shown in original column //
                }
            }
        }

        private void repositoryItemButtonEditChooseContractor_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06292OMSiteContractLabourReassignListBindingSource.Current;
                var currentRow = (DataSet_OM_Contract.sp06292_OM_Site_Contract_Labour_Reassign_ListRow)currentRowView.Row;
                if (currentRow == null) return;

                int intPersonTypeID = -1;
                try { intPersonTypeID = (string.IsNullOrEmpty(currentRow.LinkedToPersonTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.LinkedToPersonTypeID)); }
                catch (Exception) { }

                if (intPersonTypeID <= -1)
                {
                    XtraMessageBox.Show("Select the Person Type by clicking the Choose button on the Labour Type field before proceeding.", "Select Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                if (intPersonTypeID == 1)  // Contractor //
                {
                    int intOriginalContractorID = (string.IsNullOrEmpty(currentRow.ContractorID.ToString()) ? 0 : Convert.ToInt32(currentRow.ContractorID));
                    var fChildForm = new frm_OM_Select_Contractor();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.intOriginalContractorID = intOriginalContractorID;
                    fChildForm._PassedInLatitude = currentRow.SiteLocationX;
                    fChildForm._PassedInLongitude = currentRow.SiteLocationY;
                    fChildForm._PassedInPostcode = currentRow.SitePostcode;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        if (fChildForm.intSelectedContractorID == intOriginalContractorID) return;
                        currentRow.ContractorID = fChildForm.intSelectedContractorID;
                        currentRow.ContractorName = fChildForm.strSelectedContractorName;

                        // Perform Distance Calculation //
                        double dbLatitude = fChildForm.dbSelectedLatitude;
                        double dbLongitude = fChildForm.dbSelectedLongitude;
                        string strPostcode = fChildForm.strSelectedPostcode;

                        double dbPostcodeDistance = (double)0.00;
                        double dbLatLongDistance = (double)0.00;
                        if (!string.IsNullOrWhiteSpace(strPostcode) && !string.IsNullOrWhiteSpace(currentRow.SitePostcode))
                        {
                            using (var CalcPostcodeDistance = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                CalcPostcodeDistance.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    dbPostcodeDistance = Convert.ToDouble(CalcPostcodeDistance.sp06102_OM_Calculate_Distance_Between_Postcodes(strPostcode, currentRow.SitePostcode));
                                }
                                catch (Exception) { }
                            }
                        }
                        try
                        {
                            dbLatLongDistance = GeoCodeCalc.CalcDistance(dbLatitude, dbLongitude, currentRow.SiteLocationX, currentRow.SiteLocationY);  // Calculate Distance in Miles //
                        }
                        catch (Exception) { }

                        currentRow.PostcodeSiteDistance = (dbPostcodeDistance > (double)2500 ? (double)0.00 : dbPostcodeDistance);
                        currentRow.LatLongSiteDistance = (dbLatLongDistance > (double)2500 ? (double)0.00 : dbLatLongDistance);
                        sp06292OMSiteContractLabourReassignListBindingSource.EndEdit();
                        GridView view = (GridView)gridControl10.MainView;
                        view.FocusedColumn = colCostUnitDescriptorID;  // Move focus to next column so new value is shown in original column //
                    }
                }
                else  // Staff //
                {
                    var fChildForm = new frm_HR_Select_Staff();
                    fChildForm.GlobalSettings = this.GlobalSettings;

                    if (strFormMode == "blockadd" || strFormMode == "blockedit")
                    {
                        fChildForm.intOriginalStaffID = 0;
                    }
                    else
                    {
                        fChildForm.intOriginalStaffID = (string.IsNullOrEmpty(currentRow.ContractorID.ToString()) ? 0 : Convert.ToInt32(currentRow.ContractorID));
                    }

                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        currentRow.ContractorID = fChildForm.intSelectedStaffID;
                        currentRow.ContractorName = fChildForm.strSelectedStaffName;
                        currentRow.PostcodeSiteDistance = (double)0.00;
                        currentRow.LatLongSiteDistance = (double)0.00;
                        sp06292OMSiteContractLabourReassignListBindingSource.EndEdit();
                        GridView view = (GridView)gridControl10.MainView;
                        view.FocusedColumn = colCostUnitDescriptorID;  // Move focus to next column so new value is shown in original column //
                    }
                }
            }
        }

        #endregion

       
        private void Block_Edit_Preferred_labour()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl10.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Preferred Labour records to block edit then try again.", "Block Edit Site Preferred Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Site_Contract_Wizard_Block_Edit_Preferred_Labour_Info();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                double dbLatitude = (double)0.00;
                double dbLongitude = (double)0.00;
                string strPostcode = "";
                double dbPostcodeDistance = (double)0.00;
                double dbLatLongDistance = (double)0.00;
                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                view.BeginDataUpdate();
                using (var CalcPostcodeDistance = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                {
                    CalcPostcodeDistance.ChangeConnectionString(strConnectionString);
                    foreach (int intRowHandle in intRowHandles)
                    {
                        if (fChildForm.intContractorID != null)
                        {
                            view.SetRowCellValue(intRowHandle, "ContractorID", fChildForm.intContractorID);

                            // Perform Distance Calculation //
                            dbPostcodeDistance = (double)0.00;
                            dbLatLongDistance = (double)0.00;
                            if (fChildForm.dbTeamLocationX != null) dbLatitude = (double)fChildForm.dbTeamLocationX;
                            if (fChildForm.dbTeamLocationY != null) dbLongitude = (double)fChildForm.dbTeamLocationY;
                            if (fChildForm.strTeamPostcode != null) strPostcode = fChildForm.strTeamPostcode;

                            if (!string.IsNullOrWhiteSpace(strPostcode) && !string.IsNullOrWhiteSpace(view.GetRowCellValue(intRowHandle, "SitePostcode").ToString()))
                            {
                                try
                                {
                                    dbPostcodeDistance = Convert.ToDouble(CalcPostcodeDistance.sp06102_OM_Calculate_Distance_Between_Postcodes(strPostcode, view.GetRowCellValue(intRowHandle, "SitePostcode").ToString()));
                                }
                                catch (Exception) { }
                            }
                            try
                            {
                                dbLatLongDistance = GeoCodeCalc.CalcDistance(dbLatitude, dbLongitude, Convert.ToDouble(view.GetRowCellValue(intRowHandle, "SiteLocationX")), Convert.ToDouble(view.GetRowCellValue(intRowHandle, "SiteLocationY")));  // Calculate Distance in Miles //
                            }
                            catch (Exception) { }

                            view.SetRowCellValue(intRowHandle, "PostcodeSiteDistance", (dbPostcodeDistance > (double)2500 ? (double)0.00 : dbPostcodeDistance));
                            view.SetRowCellValue(intRowHandle, "LatLongSiteDistance", (dbLatLongDistance > (double)2500 ? (double)0.00 : dbLatLongDistance));
                        }
                        if (fChildForm.strLinkedToPersonType != null) view.SetRowCellValue(intRowHandle, "LinkedToPersonType", fChildForm.strLinkedToPersonType);
                        if (fChildForm.intLinkedToPersonTypeID != null) view.SetRowCellValue(intRowHandle, "LinkedToPersonTypeID", fChildForm.intLinkedToPersonTypeID);
                        if (fChildForm.strContractorName != null) view.SetRowCellValue(intRowHandle, "ContractorName", fChildForm.strContractorName);
                        if (fChildForm.intCostUnitDescriptorID != null) view.SetRowCellValue(intRowHandle, "CostUnitDescriptorID", fChildForm.intCostUnitDescriptorID);
                        if (fChildForm.intSellUnitDescriptorID != null) view.SetRowCellValue(intRowHandle, "SellUnitDescriptorID", fChildForm.intSellUnitDescriptorID);
                        if (fChildForm.decCostPerUnitExVat != null) view.SetRowCellValue(intRowHandle, "CostPerUnitExVat", fChildForm.decCostPerUnitExVat);
                        if (fChildForm.decCostPerUnitVatRate != null) view.SetRowCellValue(intRowHandle, "CostPerUnitVatRate", fChildForm.decCostPerUnitVatRate);
                        if (fChildForm.decSellPerUnitExVat != null) view.SetRowCellValue(intRowHandle, "SellPerUnitExVat", fChildForm.decSellPerUnitExVat);
                        if (fChildForm.decSellPerUnitVatRate != null) view.SetRowCellValue(intRowHandle, "SellPerUnitVatRate", fChildForm.decSellPerUnitVatRate);
                        if (fChildForm.decCISPercentage != null) view.SetRowCellValue(intRowHandle, "CISPercentage", fChildForm.decCISPercentage);
                        if (fChildForm.decCISValue != null) view.SetRowCellValue(intRowHandle, "CISValue", fChildForm.decCISValue);
                        if (fChildForm.decPostcodeSiteDistance != null) view.SetRowCellValue(intRowHandle, "PostcodeSiteDistance", fChildForm.decPostcodeSiteDistance);
                        if (fChildForm.decLatLongSiteDistance != null) view.SetRowCellValue(intRowHandle, "LatLongSiteDistance", fChildForm.decLatLongSiteDistance);
                        if (fChildForm.strRemarks != null) view.SetRowCellValue(intRowHandle, "Remarks", fChildForm.strRemarks);
                    }
                }
                view.EndDataUpdate();
            }
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        private void Block_Edit_Record()
        {
            Block_Edit_Preferred_labour();
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //
            GridView view = (GridView)gridControl10.MainView;  // Linked Labour //
            view.PostEditor();

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Saving Changes...");

            this.sp06292OMSiteContractLabourReassignListBindingSource.EndEdit();
            try
            {
                sp06292_OM_Site_Contract_Labour_Reassign_ListTableAdapter.Update(dataSet_OM_Contract);  // Insert and Update queries defined in Table Adapter //
            }
            catch (Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_OM_Site_Contract_Manager")
                    {
                        var fParentForm = (frm_OM_Site_Contract_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Labour, "");
                    }
                    else if (frmChild.Name == "frm_OM_Visit_Manager")
                    {
                        var fParentForm = (frm_OM_Visit_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Labour, "");
                    }
                    else if (frmChild.Name == "frm_OM_Job_Manager")
                    {
                        var fParentForm = (frm_OM_Job_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Labour, "");
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
            return "";  // No problems //
        }

 
    }
}

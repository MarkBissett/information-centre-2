﻿namespace WoodPlan5
{
    partial class frm_OM_Visit_Completion_Wizard_Block_Edit_Job
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.gridColumn74 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.ReworkCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DoNotInvoiceClientCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DoNotPayContractorCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ManuallyCompletedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.NoWorkRequiredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.CancelledReasonIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06268OMCancelledReasonsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn76 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn77 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06177OMJobDurationDescriptorsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ActualDurationUnitsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.JobNoLongerRequiredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.FinishLongitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FinishLatitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StartLongitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StartLatitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.gridLookUpEditJobStatusID = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06418OMVisitCompletionWizardJobStatusesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateEditActualEndDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditActualStartDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForJobNoLongerRequired = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNoWorkRequired = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForManuallyCompleted = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDoNotPayContractor = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDoNotInvoiceClient = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRework = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualDurationUnits = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualDurationUnitsDescriptionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCancelledReasonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForStartLatitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartLongitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForFinishLongitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFinishLatitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sp06418_OM_Visit_Completion_Wizard_Job_StatusesTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06418_OM_Visit_Completion_Wizard_Job_StatusesTableAdapter();
            this.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter();
            this.sp06208OMJObStatusesListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp06208_OM_JOb_Statuses_ListTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06208_OM_JOb_Statuses_ListTableAdapter();
            this.sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReworkCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotInvoiceClientCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotPayContractorCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManuallyCompletedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoWorkRequiredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelledReasonIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06268OMCancelledReasonsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06177OMJobDurationDescriptorsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualDurationUnitsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobNoLongerRequiredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishLongitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishLatitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLongitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLatitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditJobStatusID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06418OMVisitCompletionWizardJobStatusesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditActualEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditActualEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditActualStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditActualStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobNoLongerRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoWorkRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForManuallyCompleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotPayContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotInvoiceClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRework)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualDurationUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualDurationUnitsDescriptionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCancelledReasonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLongitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishLongitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06208OMJObStatusesListBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(684, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 434);
            this.barDockControlBottom.Size = new System.Drawing.Size(684, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 408);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(684, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 408);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridColumn74
            // 
            this.gridColumn74.Caption = "ID";
            this.gridColumn74.FieldName = "ID";
            this.gridColumn74.Name = "gridColumn74";
            this.gridColumn74.OptionsColumn.AllowEdit = false;
            this.gridColumn74.OptionsColumn.AllowFocus = false;
            this.gridColumn74.OptionsColumn.ReadOnly = true;
            this.gridColumn74.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn74.Width = 53;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn1.Width = 53;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "ID";
            this.gridColumn10.FieldName = "ID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn10.Width = 53;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.ReworkCheckEdit);
            this.layoutControl1.Controls.Add(this.DoNotInvoiceClientCheckEdit);
            this.layoutControl1.Controls.Add(this.DoNotPayContractorCheckEdit);
            this.layoutControl1.Controls.Add(this.ManuallyCompletedCheckEdit);
            this.layoutControl1.Controls.Add(this.NoWorkRequiredCheckEdit);
            this.layoutControl1.Controls.Add(this.CancelledReasonIDGridLookUpEdit);
            this.layoutControl1.Controls.Add(this.ActualDurationUnitsDescriptionIDGridLookUpEdit);
            this.layoutControl1.Controls.Add(this.ActualDurationUnitsSpinEdit);
            this.layoutControl1.Controls.Add(this.JobNoLongerRequiredCheckEdit);
            this.layoutControl1.Controls.Add(this.FinishLongitudeTextEdit);
            this.layoutControl1.Controls.Add(this.FinishLatitudeTextEdit);
            this.layoutControl1.Controls.Add(this.StartLongitudeTextEdit);
            this.layoutControl1.Controls.Add(this.StartLatitudeTextEdit);
            this.layoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.layoutControl1.Controls.Add(this.gridLookUpEditJobStatusID);
            this.layoutControl1.Controls.Add(this.dateEditActualEndDate);
            this.layoutControl1.Controls.Add(this.dateEditActualStartDate);
            this.layoutControl1.Location = new System.Drawing.Point(0, 28);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1192, 232, 649, 499);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(683, 375);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // ReworkCheckEdit
            // 
            this.ReworkCheckEdit.Location = new System.Drawing.Point(583, 159);
            this.ReworkCheckEdit.MenuManager = this.barManager1;
            this.ReworkCheckEdit.Name = "ReworkCheckEdit";
            this.ReworkCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.ReworkCheckEdit.Properties.ValueChecked = 1;
            this.ReworkCheckEdit.Properties.ValueUnchecked = 0;
            this.ReworkCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.ReworkCheckEdit.StyleController = this.layoutControl1;
            this.ReworkCheckEdit.TabIndex = 142;
            // 
            // DoNotInvoiceClientCheckEdit
            // 
            this.DoNotInvoiceClientCheckEdit.Location = new System.Drawing.Point(583, 136);
            this.DoNotInvoiceClientCheckEdit.MenuManager = this.barManager1;
            this.DoNotInvoiceClientCheckEdit.Name = "DoNotInvoiceClientCheckEdit";
            this.DoNotInvoiceClientCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.DoNotInvoiceClientCheckEdit.Properties.ValueChecked = 1;
            this.DoNotInvoiceClientCheckEdit.Properties.ValueUnchecked = 0;
            this.DoNotInvoiceClientCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.DoNotInvoiceClientCheckEdit.StyleController = this.layoutControl1;
            this.DoNotInvoiceClientCheckEdit.TabIndex = 9;
            // 
            // DoNotPayContractorCheckEdit
            // 
            this.DoNotPayContractorCheckEdit.Location = new System.Drawing.Point(583, 113);
            this.DoNotPayContractorCheckEdit.MenuManager = this.barManager1;
            this.DoNotPayContractorCheckEdit.Name = "DoNotPayContractorCheckEdit";
            this.DoNotPayContractorCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.DoNotPayContractorCheckEdit.Properties.ValueChecked = 1;
            this.DoNotPayContractorCheckEdit.Properties.ValueUnchecked = 0;
            this.DoNotPayContractorCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.DoNotPayContractorCheckEdit.StyleController = this.layoutControl1;
            this.DoNotPayContractorCheckEdit.TabIndex = 8;
            // 
            // ManuallyCompletedCheckEdit
            // 
            this.ManuallyCompletedCheckEdit.Location = new System.Drawing.Point(583, 90);
            this.ManuallyCompletedCheckEdit.MenuManager = this.barManager1;
            this.ManuallyCompletedCheckEdit.Name = "ManuallyCompletedCheckEdit";
            this.ManuallyCompletedCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.ManuallyCompletedCheckEdit.Properties.ValueChecked = 1;
            this.ManuallyCompletedCheckEdit.Properties.ValueUnchecked = 0;
            this.ManuallyCompletedCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.ManuallyCompletedCheckEdit.StyleController = this.layoutControl1;
            this.ManuallyCompletedCheckEdit.TabIndex = 15;
            // 
            // NoWorkRequiredCheckEdit
            // 
            this.NoWorkRequiredCheckEdit.Location = new System.Drawing.Point(583, 67);
            this.NoWorkRequiredCheckEdit.MenuManager = this.barManager1;
            this.NoWorkRequiredCheckEdit.Name = "NoWorkRequiredCheckEdit";
            this.NoWorkRequiredCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.NoWorkRequiredCheckEdit.Properties.ValueChecked = 1;
            this.NoWorkRequiredCheckEdit.Properties.ValueUnchecked = 0;
            this.NoWorkRequiredCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.NoWorkRequiredCheckEdit.StyleController = this.layoutControl1;
            this.NoWorkRequiredCheckEdit.TabIndex = 15;
            // 
            // CancelledReasonIDGridLookUpEdit
            // 
            this.CancelledReasonIDGridLookUpEdit.Location = new System.Drawing.Point(134, 132);
            this.CancelledReasonIDGridLookUpEdit.MenuManager = this.barManager1;
            this.CancelledReasonIDGridLookUpEdit.Name = "CancelledReasonIDGridLookUpEdit";
            this.CancelledReasonIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CancelledReasonIDGridLookUpEdit.Properties.DataSource = this.sp06268OMCancelledReasonsWithBlankBindingSource;
            this.CancelledReasonIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.CancelledReasonIDGridLookUpEdit.Properties.NullText = "";
            this.CancelledReasonIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.CancelledReasonIDGridLookUpEdit.Properties.View = this.gridView3;
            this.CancelledReasonIDGridLookUpEdit.Size = new System.Drawing.Size(311, 20);
            this.CancelledReasonIDGridLookUpEdit.StyleController = this.layoutControl1;
            this.CancelledReasonIDGridLookUpEdit.TabIndex = 15;
            // 
            // sp06268OMCancelledReasonsWithBlankBindingSource
            // 
            this.sp06268OMCancelledReasonsWithBlankBindingSource.DataMember = "sp06268_OM_Cancelled_Reasons_With_Blank";
            this.sp06268OMCancelledReasonsWithBlankBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn74,
            this.gridColumn76,
            this.gridColumn77});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.gridColumn74;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView3.FormatRules.Add(gridFormatRule1);
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn77, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn76
            // 
            this.gridColumn76.Caption = "Cancelled Reason";
            this.gridColumn76.FieldName = "Description";
            this.gridColumn76.Name = "gridColumn76";
            this.gridColumn76.OptionsColumn.AllowEdit = false;
            this.gridColumn76.OptionsColumn.AllowFocus = false;
            this.gridColumn76.OptionsColumn.ReadOnly = true;
            this.gridColumn76.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn76.Visible = true;
            this.gridColumn76.VisibleIndex = 0;
            this.gridColumn76.Width = 220;
            // 
            // gridColumn77
            // 
            this.gridColumn77.Caption = "Order";
            this.gridColumn77.FieldName = "RecordOrder";
            this.gridColumn77.Name = "gridColumn77";
            this.gridColumn77.OptionsColumn.AllowEdit = false;
            this.gridColumn77.OptionsColumn.AllowFocus = false;
            this.gridColumn77.OptionsColumn.ReadOnly = true;
            this.gridColumn77.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // ActualDurationUnitsDescriptionIDGridLookUpEdit
            // 
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Location = new System.Drawing.Point(134, 60);
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Name = "ActualDurationUnitsDescriptionIDGridLookUpEdit";
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Properties.DataSource = this.sp06177OMJobDurationDescriptorsWithBlankBindingSource;
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Properties.NullText = "";
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Properties.View = this.gridView7;
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Size = new System.Drawing.Size(311, 20);
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.StyleController = this.layoutControl1;
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.TabIndex = 19;
            this.ActualDurationUnitsDescriptionIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.ActualDurationUnitsDescriptionIDGridLookUpEdit_EditValueChanged);
            // 
            // sp06177OMJobDurationDescriptorsWithBlankBindingSource
            // 
            this.sp06177OMJobDurationDescriptorsWithBlankBindingSource.DataMember = "sp06177_OM_Job_Duration_Descriptors_With_Blank";
            this.sp06177OMJobDurationDescriptorsWithBlankBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.gridColumn1;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView7.FormatRules.Add(gridFormatRule2);
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Unit Descriptor";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // ActualDurationUnitsSpinEdit
            // 
            this.ActualDurationUnitsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ActualDurationUnitsSpinEdit.Location = new System.Drawing.Point(134, 36);
            this.ActualDurationUnitsSpinEdit.MenuManager = this.barManager1;
            this.ActualDurationUnitsSpinEdit.Name = "ActualDurationUnitsSpinEdit";
            this.ActualDurationUnitsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActualDurationUnitsSpinEdit.Properties.Mask.EditMask = "n2";
            this.ActualDurationUnitsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualDurationUnitsSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            131072});
            this.ActualDurationUnitsSpinEdit.Size = new System.Drawing.Size(311, 20);
            this.ActualDurationUnitsSpinEdit.StyleController = this.layoutControl1;
            this.ActualDurationUnitsSpinEdit.TabIndex = 18;
            this.ActualDurationUnitsSpinEdit.EditValueChanged += new System.EventHandler(this.ActualDurationUnitsSpinEdit_EditValueChanged);
            // 
            // JobNoLongerRequiredCheckEdit
            // 
            this.JobNoLongerRequiredCheckEdit.Location = new System.Drawing.Point(583, 44);
            this.JobNoLongerRequiredCheckEdit.MenuManager = this.barManager1;
            this.JobNoLongerRequiredCheckEdit.Name = "JobNoLongerRequiredCheckEdit";
            this.JobNoLongerRequiredCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.JobNoLongerRequiredCheckEdit.Properties.ValueChecked = 1;
            this.JobNoLongerRequiredCheckEdit.Properties.ValueUnchecked = 0;
            this.JobNoLongerRequiredCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.JobNoLongerRequiredCheckEdit.StyleController = this.layoutControl1;
            this.JobNoLongerRequiredCheckEdit.TabIndex = 5;
            // 
            // FinishLongitudeTextEdit
            // 
            this.FinishLongitudeTextEdit.Location = new System.Drawing.Point(483, 260);
            this.FinishLongitudeTextEdit.MenuManager = this.barManager1;
            this.FinishLongitudeTextEdit.Name = "FinishLongitudeTextEdit";
            this.FinishLongitudeTextEdit.Properties.Mask.EditMask = "n8";
            this.FinishLongitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.FinishLongitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FinishLongitudeTextEdit, true);
            this.FinishLongitudeTextEdit.Size = new System.Drawing.Size(176, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FinishLongitudeTextEdit, optionsSpelling1);
            this.FinishLongitudeTextEdit.StyleController = this.layoutControl1;
            this.FinishLongitudeTextEdit.TabIndex = 17;
            // 
            // FinishLatitudeTextEdit
            // 
            this.FinishLatitudeTextEdit.Location = new System.Drawing.Point(483, 236);
            this.FinishLatitudeTextEdit.MenuManager = this.barManager1;
            this.FinishLatitudeTextEdit.Name = "FinishLatitudeTextEdit";
            this.FinishLatitudeTextEdit.Properties.Mask.EditMask = "n8";
            this.FinishLatitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.FinishLatitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FinishLatitudeTextEdit, true);
            this.FinishLatitudeTextEdit.Size = new System.Drawing.Size(176, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FinishLatitudeTextEdit, optionsSpelling2);
            this.FinishLatitudeTextEdit.StyleController = this.layoutControl1;
            this.FinishLatitudeTextEdit.TabIndex = 16;
            // 
            // StartLongitudeTextEdit
            // 
            this.StartLongitudeTextEdit.Location = new System.Drawing.Point(146, 260);
            this.StartLongitudeTextEdit.MenuManager = this.barManager1;
            this.StartLongitudeTextEdit.Name = "StartLongitudeTextEdit";
            this.StartLongitudeTextEdit.Properties.Mask.EditMask = "n8";
            this.StartLongitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.StartLongitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StartLongitudeTextEdit, true);
            this.StartLongitudeTextEdit.Size = new System.Drawing.Size(181, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StartLongitudeTextEdit, optionsSpelling3);
            this.StartLongitudeTextEdit.StyleController = this.layoutControl1;
            this.StartLongitudeTextEdit.TabIndex = 15;
            // 
            // StartLatitudeTextEdit
            // 
            this.StartLatitudeTextEdit.Location = new System.Drawing.Point(146, 236);
            this.StartLatitudeTextEdit.MenuManager = this.barManager1;
            this.StartLatitudeTextEdit.Name = "StartLatitudeTextEdit";
            this.StartLatitudeTextEdit.Properties.Mask.EditMask = "n8";
            this.StartLatitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.StartLatitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StartLatitudeTextEdit, true);
            this.StartLatitudeTextEdit.Size = new System.Drawing.Size(181, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StartLatitudeTextEdit, optionsSpelling4);
            this.StartLatitudeTextEdit.StyleController = this.layoutControl1;
            this.StartLatitudeTextEdit.TabIndex = 14;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.Location = new System.Drawing.Point(134, 306);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(537, 57);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling5);
            this.RemarksMemoEdit.StyleController = this.layoutControl1;
            this.RemarksMemoEdit.TabIndex = 9;
            // 
            // gridLookUpEditJobStatusID
            // 
            this.gridLookUpEditJobStatusID.EditValue = 2;
            this.gridLookUpEditJobStatusID.Location = new System.Drawing.Point(134, 108);
            this.gridLookUpEditJobStatusID.MenuManager = this.barManager1;
            this.gridLookUpEditJobStatusID.Name = "gridLookUpEditJobStatusID";
            this.gridLookUpEditJobStatusID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditJobStatusID.Properties.DataSource = this.sp06418OMVisitCompletionWizardJobStatusesBindingSource;
            this.gridLookUpEditJobStatusID.Properties.DisplayMember = "Description";
            this.gridLookUpEditJobStatusID.Properties.NullText = "";
            this.gridLookUpEditJobStatusID.Properties.ValueMember = "ID";
            this.gridLookUpEditJobStatusID.Properties.View = this.gridView5;
            this.gridLookUpEditJobStatusID.Size = new System.Drawing.Size(311, 20);
            this.gridLookUpEditJobStatusID.StyleController = this.layoutControl1;
            this.gridLookUpEditJobStatusID.TabIndex = 2;
            this.gridLookUpEditJobStatusID.EditValueChanged += new System.EventHandler(this.gridLookUpEditJobStatusID_EditValueChanged);
            // 
            // sp06418OMVisitCompletionWizardJobStatusesBindingSource
            // 
            this.sp06418OMVisitCompletionWizardJobStatusesBindingSource.DataMember = "sp06418_OM_Visit_Completion_Wizard_Job_Statuses";
            this.sp06418OMVisitCompletionWizardJobStatusesBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Column = this.gridColumn10;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue3.Value1 = 0;
            gridFormatRule3.Rule = formatConditionRuleValue3;
            this.gridView5.FormatRules.Add(gridFormatRule3);
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Job Status";
            this.gridColumn11.FieldName = "Description";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            this.gridColumn11.Width = 220;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Order";
            this.gridColumn12.FieldName = "RecordOrder";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // dateEditActualEndDate
            // 
            this.dateEditActualEndDate.EditValue = null;
            this.dateEditActualEndDate.Location = new System.Drawing.Point(134, 84);
            this.dateEditActualEndDate.MenuManager = this.barManager1;
            this.dateEditActualEndDate.Name = "dateEditActualEndDate";
            this.dateEditActualEndDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditActualEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditActualEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditActualEndDate.Properties.Mask.EditMask = "g";
            this.dateEditActualEndDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditActualEndDate.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditActualEndDate.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateEditActualEndDate.Size = new System.Drawing.Size(311, 20);
            this.dateEditActualEndDate.StyleController = this.layoutControl1;
            this.dateEditActualEndDate.TabIndex = 1;
            this.dateEditActualEndDate.EditValueChanged += new System.EventHandler(this.dateEditActualEndDate_EditValueChanged);
            // 
            // dateEditActualStartDate
            // 
            this.dateEditActualStartDate.EditValue = null;
            this.dateEditActualStartDate.Location = new System.Drawing.Point(134, 12);
            this.dateEditActualStartDate.MenuManager = this.barManager1;
            this.dateEditActualStartDate.Name = "dateEditActualStartDate";
            this.dateEditActualStartDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditActualStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditActualStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditActualStartDate.Properties.Mask.EditMask = "g";
            this.dateEditActualStartDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditActualStartDate.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditActualStartDate.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateEditActualStartDate.Size = new System.Drawing.Size(311, 20);
            this.dateEditActualStartDate.StyleController = this.layoutControl1;
            this.dateEditActualStartDate.TabIndex = 0;
            this.dateEditActualStartDate.EditValueChanged += new System.EventHandler(this.dateEditActualStartDate_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks,
            this.emptySpaceItem3,
            this.emptySpaceItem14,
            this.layoutControlGroup5,
            this.ItemForActualStartDate,
            this.ItemForActualDurationUnits,
            this.ItemForActualDurationUnitsDescriptionID,
            this.ItemForActualEndDate,
            this.ItemForJobStatusID,
            this.ItemForCancelledReasonID,
            this.emptySpaceItem1,
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.splitterItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(683, 375);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 294);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(663, 61);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(119, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 284);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(663, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(0, 182);
            this.emptySpaceItem14.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem14.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(663, 10);
            this.emptySpaceItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForJobNoLongerRequired,
            this.ItemForNoWorkRequired,
            this.ItemForManuallyCompleted,
            this.ItemForDoNotPayContractor,
            this.ItemForDoNotInvoiceClient,
            this.ItemForRework});
            this.layoutControlGroup5.Location = new System.Drawing.Point(437, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(226, 182);
            this.layoutControlGroup5.Text = " ";
            // 
            // ItemForJobNoLongerRequired
            // 
            this.ItemForJobNoLongerRequired.Control = this.JobNoLongerRequiredCheckEdit;
            this.ItemForJobNoLongerRequired.Location = new System.Drawing.Point(0, 0);
            this.ItemForJobNoLongerRequired.Name = "ItemForJobNoLongerRequired";
            this.ItemForJobNoLongerRequired.Size = new System.Drawing.Size(202, 23);
            this.ItemForJobNoLongerRequired.Text = "Job No Longer Required:";
            this.ItemForJobNoLongerRequired.TextSize = new System.Drawing.Size(119, 13);
            // 
            // ItemForNoWorkRequired
            // 
            this.ItemForNoWorkRequired.Control = this.NoWorkRequiredCheckEdit;
            this.ItemForNoWorkRequired.Location = new System.Drawing.Point(0, 23);
            this.ItemForNoWorkRequired.Name = "ItemForNoWorkRequired";
            this.ItemForNoWorkRequired.Size = new System.Drawing.Size(202, 23);
            this.ItemForNoWorkRequired.Text = "No Work Required:";
            this.ItemForNoWorkRequired.TextSize = new System.Drawing.Size(119, 13);
            // 
            // ItemForManuallyCompleted
            // 
            this.ItemForManuallyCompleted.Control = this.ManuallyCompletedCheckEdit;
            this.ItemForManuallyCompleted.Location = new System.Drawing.Point(0, 46);
            this.ItemForManuallyCompleted.Name = "ItemForManuallyCompleted";
            this.ItemForManuallyCompleted.Size = new System.Drawing.Size(202, 23);
            this.ItemForManuallyCompleted.Text = "Manually Completed:";
            this.ItemForManuallyCompleted.TextSize = new System.Drawing.Size(119, 13);
            // 
            // ItemForDoNotPayContractor
            // 
            this.ItemForDoNotPayContractor.Control = this.DoNotPayContractorCheckEdit;
            this.ItemForDoNotPayContractor.Location = new System.Drawing.Point(0, 69);
            this.ItemForDoNotPayContractor.Name = "ItemForDoNotPayContractor";
            this.ItemForDoNotPayContractor.Size = new System.Drawing.Size(202, 23);
            this.ItemForDoNotPayContractor.Text = "Do Not Pay Team:";
            this.ItemForDoNotPayContractor.TextSize = new System.Drawing.Size(119, 13);
            // 
            // ItemForDoNotInvoiceClient
            // 
            this.ItemForDoNotInvoiceClient.Control = this.DoNotInvoiceClientCheckEdit;
            this.ItemForDoNotInvoiceClient.Location = new System.Drawing.Point(0, 92);
            this.ItemForDoNotInvoiceClient.Name = "ItemForDoNotInvoiceClient";
            this.ItemForDoNotInvoiceClient.Size = new System.Drawing.Size(202, 23);
            this.ItemForDoNotInvoiceClient.Text = "Do Not Invoice Client:";
            this.ItemForDoNotInvoiceClient.TextSize = new System.Drawing.Size(119, 13);
            // 
            // ItemForRework
            // 
            this.ItemForRework.Control = this.ReworkCheckEdit;
            this.ItemForRework.Location = new System.Drawing.Point(0, 115);
            this.ItemForRework.Name = "ItemForRework";
            this.ItemForRework.Size = new System.Drawing.Size(202, 23);
            this.ItemForRework.Text = "Rework:";
            this.ItemForRework.TextSize = new System.Drawing.Size(119, 13);
            // 
            // ItemForActualStartDate
            // 
            this.ItemForActualStartDate.Control = this.dateEditActualStartDate;
            this.ItemForActualStartDate.CustomizationFormText = "Job Start:";
            this.ItemForActualStartDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForActualStartDate.Name = "ItemForActualStartDate";
            this.ItemForActualStartDate.Size = new System.Drawing.Size(437, 24);
            this.ItemForActualStartDate.Text = "Job Start:";
            this.ItemForActualStartDate.TextSize = new System.Drawing.Size(119, 13);
            // 
            // ItemForActualDurationUnits
            // 
            this.ItemForActualDurationUnits.Control = this.ActualDurationUnitsSpinEdit;
            this.ItemForActualDurationUnits.Location = new System.Drawing.Point(0, 24);
            this.ItemForActualDurationUnits.Name = "ItemForActualDurationUnits";
            this.ItemForActualDurationUnits.Size = new System.Drawing.Size(437, 24);
            this.ItemForActualDurationUnits.Text = "Duration Units:";
            this.ItemForActualDurationUnits.TextSize = new System.Drawing.Size(119, 13);
            // 
            // ItemForActualDurationUnitsDescriptionID
            // 
            this.ItemForActualDurationUnitsDescriptionID.Control = this.ActualDurationUnitsDescriptionIDGridLookUpEdit;
            this.ItemForActualDurationUnitsDescriptionID.Location = new System.Drawing.Point(0, 48);
            this.ItemForActualDurationUnitsDescriptionID.Name = "ItemForActualDurationUnitsDescriptionID";
            this.ItemForActualDurationUnitsDescriptionID.Size = new System.Drawing.Size(437, 24);
            this.ItemForActualDurationUnitsDescriptionID.Text = "Units Descriptor:";
            this.ItemForActualDurationUnitsDescriptionID.TextSize = new System.Drawing.Size(119, 13);
            // 
            // ItemForActualEndDate
            // 
            this.ItemForActualEndDate.Control = this.dateEditActualEndDate;
            this.ItemForActualEndDate.CustomizationFormText = "Job End:";
            this.ItemForActualEndDate.Location = new System.Drawing.Point(0, 72);
            this.ItemForActualEndDate.Name = "ItemForActualEndDate";
            this.ItemForActualEndDate.Size = new System.Drawing.Size(437, 24);
            this.ItemForActualEndDate.Text = "Job End:";
            this.ItemForActualEndDate.TextSize = new System.Drawing.Size(119, 13);
            // 
            // ItemForJobStatusID
            // 
            this.ItemForJobStatusID.Control = this.gridLookUpEditJobStatusID;
            this.ItemForJobStatusID.CustomizationFormText = "Job Status:";
            this.ItemForJobStatusID.Location = new System.Drawing.Point(0, 96);
            this.ItemForJobStatusID.Name = "ItemForJobStatusID";
            this.ItemForJobStatusID.Size = new System.Drawing.Size(437, 24);
            this.ItemForJobStatusID.Text = "Job Status:";
            this.ItemForJobStatusID.TextSize = new System.Drawing.Size(119, 13);
            // 
            // ItemForCancelledReasonID
            // 
            this.ItemForCancelledReasonID.Control = this.CancelledReasonIDGridLookUpEdit;
            this.ItemForCancelledReasonID.Location = new System.Drawing.Point(0, 120);
            this.ItemForCancelledReasonID.Name = "ItemForCancelledReasonID";
            this.ItemForCancelledReasonID.Size = new System.Drawing.Size(437, 24);
            this.ItemForCancelledReasonID.Text = "Cancelled Reason:";
            this.ItemForCancelledReasonID.TextSize = new System.Drawing.Size(119, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 144);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(437, 38);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForStartLatitude,
            this.ItemForStartLongitude});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 192);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(331, 92);
            this.layoutControlGroup2.Text = "Location - Start";
            // 
            // ItemForStartLatitude
            // 
            this.ItemForStartLatitude.Control = this.StartLatitudeTextEdit;
            this.ItemForStartLatitude.Location = new System.Drawing.Point(0, 0);
            this.ItemForStartLatitude.Name = "ItemForStartLatitude";
            this.ItemForStartLatitude.Size = new System.Drawing.Size(307, 24);
            this.ItemForStartLatitude.Text = "Start Latitude:";
            this.ItemForStartLatitude.TextSize = new System.Drawing.Size(119, 13);
            // 
            // ItemForStartLongitude
            // 
            this.ItemForStartLongitude.Control = this.StartLongitudeTextEdit;
            this.ItemForStartLongitude.Location = new System.Drawing.Point(0, 24);
            this.ItemForStartLongitude.Name = "ItemForStartLongitude";
            this.ItemForStartLongitude.Size = new System.Drawing.Size(307, 24);
            this.ItemForStartLongitude.Text = "Start Longitude:";
            this.ItemForStartLongitude.TextSize = new System.Drawing.Size(119, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForFinishLongitude,
            this.ItemForFinishLatitude});
            this.layoutControlGroup3.Location = new System.Drawing.Point(337, 192);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(326, 92);
            this.layoutControlGroup3.Text = "Location - Finish";
            // 
            // ItemForFinishLongitude
            // 
            this.ItemForFinishLongitude.Control = this.FinishLongitudeTextEdit;
            this.ItemForFinishLongitude.Location = new System.Drawing.Point(0, 24);
            this.ItemForFinishLongitude.Name = "ItemForFinishLongitude";
            this.ItemForFinishLongitude.Size = new System.Drawing.Size(302, 24);
            this.ItemForFinishLongitude.Text = "Finish Longitude:";
            this.ItemForFinishLongitude.TextSize = new System.Drawing.Size(119, 13);
            // 
            // ItemForFinishLatitude
            // 
            this.ItemForFinishLatitude.Control = this.FinishLatitudeTextEdit;
            this.ItemForFinishLatitude.Location = new System.Drawing.Point(0, 0);
            this.ItemForFinishLatitude.Name = "ItemForFinishLatitude";
            this.ItemForFinishLatitude.Size = new System.Drawing.Size(302, 24);
            this.ItemForFinishLatitude.Text = "Finish Latitude:";
            this.ItemForFinishLatitude.TextSize = new System.Drawing.Size(119, 13);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.Location = new System.Drawing.Point(331, 192);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(6, 92);
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(464, 404);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(96, 22);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(575, 404);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(96, 22);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // sp06418_OM_Visit_Completion_Wizard_Job_StatusesTableAdapter
            // 
            this.sp06418_OM_Visit_Completion_Wizard_Job_StatusesTableAdapter.ClearBeforeFill = true;
            // 
            // sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter
            // 
            this.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06208OMJObStatusesListBindingSource
            // 
            this.sp06208OMJObStatusesListBindingSource.DataMember = "sp06208_OM_JOb_Statuses_List";
            this.sp06208OMJObStatusesListBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // sp06208_OM_JOb_Statuses_ListTableAdapter
            // 
            this.sp06208_OM_JOb_Statuses_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter
            // 
            this.sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Visit_Completion_Wizard_Block_Edit_Job
            // 
            this.ClientSize = new System.Drawing.Size(684, 434);
            this.ControlBox = false;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OM_Visit_Completion_Wizard_Block_Edit_Job";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Visit Completion Wizard - Block Edit Jobs";
            this.Load += new System.EventHandler(this.frm_OM_Visit_Completion_Wizard_Block_Edit_Job_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ReworkCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotInvoiceClientCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoNotPayContractorCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManuallyCompletedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoWorkRequiredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelledReasonIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06268OMCancelledReasonsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualDurationUnitsDescriptionIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06177OMJobDurationDescriptorsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualDurationUnitsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobNoLongerRequiredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishLongitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishLatitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLongitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLatitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditJobStatusID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06418OMVisitCompletionWizardJobStatusesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditActualEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditActualEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditActualStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditActualStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobNoLongerRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoWorkRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForManuallyCompleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotPayContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoNotInvoiceClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRework)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualDurationUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualDurationUnitsDescriptionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCancelledReasonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLongitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishLongitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06208OMJObStatusesListBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.DateEdit dateEditActualStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualStartDate;
        private DevExpress.XtraEditors.DateEdit dateEditActualEndDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualEndDate;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditJobStatusID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobStatusID;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit StartLatitudeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartLatitude;
        private DevExpress.XtraEditors.TextEdit StartLongitudeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartLongitude;
        private DevExpress.XtraEditors.TextEdit FinishLatitudeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinishLatitude;
        private DevExpress.XtraEditors.TextEdit FinishLongitudeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinishLongitude;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.CheckEdit JobNoLongerRequiredCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobNoLongerRequired;
        private System.Windows.Forms.BindingSource sp06418OMVisitCompletionWizardJobStatusesBindingSource;
        private DataSet_OM_Job dataSet_OM_Job;
        private DataSet_OM_JobTableAdapters.sp06418_OM_Visit_Completion_Wizard_Job_StatusesTableAdapter sp06418_OM_Visit_Completion_Wizard_Job_StatusesTableAdapter;
        private DevExpress.XtraEditors.SpinEdit ActualDurationUnitsSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualDurationUnits;
        private DevExpress.XtraEditors.GridLookUpEdit ActualDurationUnitsDescriptionIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualDurationUnitsDescriptionID;
        private System.Windows.Forms.BindingSource sp06177OMJobDurationDescriptorsWithBlankBindingSource;
        private DataSet_OM_JobTableAdapters.sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit CancelledReasonIDGridLookUpEdit;
        private System.Windows.Forms.BindingSource sp06268OMCancelledReasonsWithBlankBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn74;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn76;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn77;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCancelledReasonID;
        private System.Windows.Forms.BindingSource sp06208OMJObStatusesListBindingSource;
        private DataSet_OM_JobTableAdapters.sp06208_OM_JOb_Statuses_ListTableAdapter sp06208_OM_JOb_Statuses_ListTableAdapter;
        private DataSet_OM_JobTableAdapters.sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.CheckEdit NoWorkRequiredCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNoWorkRequired;
        private DevExpress.XtraEditors.CheckEdit ManuallyCompletedCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForManuallyCompleted;
        private DevExpress.XtraEditors.CheckEdit DoNotPayContractorCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDoNotPayContractor;
        private DevExpress.XtraEditors.CheckEdit DoNotInvoiceClientCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDoNotInvoiceClient;
        private DevExpress.XtraEditors.CheckEdit ReworkCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRework;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using DevExpress.Utils;

using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_OM_Visit_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        private string strConnectionStringREADONLY = "";

        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;

        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        public int intPassedInClientContractID = 0;
        public int intMaxVisitID = 0;
        public int intClientID = 0;
        public int intSiteID = 0;
        public decimal? decPassedInSiteContractDaysSeparationPercent = (decimal)0.00;
        public DateTime? dtStartDateTime = null;
        public DateTime? dtEndDateTime = null;
        public string strImagesFolderOM = "";

        public bool _AtLeastOneSelfBillingInvoice = false;
        public bool _AtLeastOneClientInvoice = false;

        private int intDefaultVisitCategoryID = 0;
        private string strSignaturePath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        
        public bool iBool_VisitFullEditAccess = false;

        #endregion

        public frm_OM_Visit_Edit()
        {
            InitializeComponent();
        }
        
        private void frm_OM_Visit_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 500157;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;
            strConnectionStringREADONLY = GlobalSettings.ConnectionStringREADONLY;

            Set_Grid_Highlighter_Transparent(this.Controls);

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strSignaturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_PictureFilesFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Client Signatures (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Client Signature Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            try
            {
                DataSet_OM_VisitTableAdapters.QueriesTableAdapter GetDefaultValue = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter();
                GetDefaultValue.ChangeConnectionString(strConnectionString);
                intDefaultVisitCategoryID = Convert.ToInt32(GetDefaultValue.sp06356_OM_Default_Visit_Category_ID());
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Visit Category ID.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Visit Category ID", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            try
            {
                sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter.Fill(dataSet_OM_Visit.sp06134_OM_Job_Calculation_Level_Descriptors, 1);

                sp06250_OM_Visit_Categories_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06250_OM_Visit_Categories_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06250_OM_Visit_Categories_With_Blank, 0);

                sp06251_OM_Issue_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06251_OM_Issue_Types_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06251_OM_Issue_Types_With_Blank, 1);

                sp06267_OM_Extra_Work_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06267_OM_Extra_Work_Types_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06267_OM_Extra_Work_Types_With_Blank, 1);

                sp06308_OM_Visit_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06308_OM_Visit_TypesTableAdapter.Fill(dataSet_OM_Visit.sp06308_OM_Visit_Types, 1);

                sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06141_OM_Visit_Template_Headers_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06141_OM_Visit_Template_Headers_With_Blank);

                sp06430_OM_Visit_Status_Issues_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06430_OM_Visit_Status_Issues_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06430_OM_Visit_Status_Issues_With_Blank);
            }
            catch (Exception) { }

            // Populate Main Dataset //
            sp06145_OM_Visit_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();

            //LoadCombos();

            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow = dataSet_OM_Visit.sp06145_OM_Visit_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["VisitID"] = 0;
                        drNewRow["SiteContractID"] = intLinkedToRecordID;
                        drNewRow["LinkedToParent"] = (strFormMode == "add" ? strLinkedToRecordDesc : "N\\A when Block Adding");
                        drNewRow["VisitNumber"] = intMaxVisitID + 1;
                        drNewRow["CreatedByStaffID"] = GlobalSettings.UserID;
                        drNewRow["CreatedByStaffName"] = GlobalSettings.UserSurname + ": " + GlobalSettings.UserForename;
                        drNewRow["CostCalculationLevel"] = 1;  // Visit //
                        drNewRow["SellCalculationLevel"] = 1;  // Visit //
                        drNewRow["ExpectedStartDate"] = (dtStartDateTime == null ? DateTime.Today.AddDays(1).AddHours((double)0) : dtStartDateTime);
                        drNewRow["ExpectedEndDate"] = (dtEndDateTime == null ? DateTime.Today.AddDays(2).AddMilliseconds(-2) : dtEndDateTime);
                        drNewRow["VisitCost"] = (decimal)0.00;
                        drNewRow["VisitSell"] = (decimal)0.00;
                        drNewRow["StartLatitude"] = (double)0.00;
                        drNewRow["StartLongitude"] = (double)0.00;
                        drNewRow["FinishLatitude"] = (double)0.00;
                        drNewRow["FinishLongitude"] = (double)0.00;
                        drNewRow["ClientContractID"] = intPassedInClientContractID;
                        drNewRow["VisitStatusID"] = 10;   // Not Started //
                        drNewRow["StatusDescription"] = "Not Started";
                        drNewRow["IssueFound"] = 0;
                        drNewRow["IssueTypeID"] = 0;
                        drNewRow["VisitCategoryID"] = intDefaultVisitCategoryID;
                        drNewRow["ImagesFolderOM"] = strImagesFolderOM;
                        drNewRow["ExtraWorkRequired"] = 0;
                        drNewRow["ExtraWorkTypeID"] = 0;
                        drNewRow["AccidentOnSite"] = 0;
                        drNewRow["VisitTypeID"] = 1;
                        drNewRow["SelfBillingInvoice"] = "";
                        drNewRow["ClientInvoiceID"] = 0;
                        drNewRow["ClientID"] = intClientID;
                        drNewRow["SiteID"] = intSiteID;
                        drNewRow["NoOnetoSign"] = 0;
                        drNewRow["ClientSignaturePath"] = "";
                        drNewRow["CreatedFromVisitTemplateID"] = 0;
                        drNewRow["StatusIssueID"] = 0;
                        drNewRow["SiteContractDaysSeparationPercent"] = decPassedInSiteContractDaysSeparationPercent;
                        drNewRow["DoNotPayContractor"] = 0;
                        drNewRow["DoNotInvoiceClient"] = 0;
                        drNewRow["VisitLabourCost"] = (decimal)0.00;
                        drNewRow["VisitMaterialCost"] = (decimal)0.00;
                        drNewRow["VisitEquipmentCost"] = (decimal)0.00;
                        drNewRow["VisitLabourSell"] = (decimal)0.00;
                        drNewRow["VisitMaterialSell"] = (decimal)0.00;
                        drNewRow["VisitEquipmentSell"] = (decimal)0.00;

                        // Update default Client PO # /
                        int intRecordTypeID = 1;
                        int intDefaultPOID = 0;
                        string strDefaultPONumber = "";
                        SqlConnection SQlConn = new SqlConnection(strConnectionString);
                        SqlCommand cmd = null;
                        cmd = new SqlCommand("sp06328_OM_Default_Client_PO_Number_For_Record", SQlConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@RecordID", intLinkedToRecordID));
                        cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordTypeID));
                        SqlDataAdapter sdaPO = new SqlDataAdapter(cmd);
                        DataSet dsPO = new DataSet("NewDataSet");
                        sdaPO.Fill(dsPO, "Table");
                        foreach (DataRow dr in dsPO.Tables[0].Rows)
                        {
                            intDefaultPOID = Convert.ToInt32(dr["ClientPurchaseOrderID"] );
                            strDefaultPONumber = dr["PONumber"].ToString();
                            break;
                        }
                        if (intDefaultPOID <= 0)  // No Site Contract specific Client PO ID so try for Client contract specific //
                        {
                            intRecordTypeID = 0;
                            cmd = null;
                            cmd = new SqlCommand("sp06328_OM_Default_Client_PO_Number_For_Record", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@RecordID", intPassedInClientContractID));
                            cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordTypeID));
                            sdaPO = new SqlDataAdapter(cmd);
                            dsPO = new DataSet("NewDataSet");
                            sdaPO.Fill(dsPO, "Table");
                            foreach (DataRow dr in dsPO.Tables[0].Rows)
                            {
                                intDefaultPOID = Convert.ToInt32(dr["ClientPurchaseOrderID"]);
                                strDefaultPONumber = dr["PONumber"].ToString();
                                break;
                            }
                        }
                        drNewRow["ClientPOID"] = intDefaultPOID;
                        drNewRow["ClientPONumber"] = strDefaultPONumber;
                        drNewRow["DaysSeparation"] = 0;
                        drNewRow["ExtraCostsFinalised"] = 0;

                        dataSet_OM_Visit.sp06145_OM_Visit_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        //DataRow drNewRow;
                        DataRow drNewRow = dataSet_OM_Visit.sp06145_OM_Visit_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        dataSet_OM_Visit.sp06145_OM_Visit_Edit.Rows.Add(drNewRow);
                        drNewRow.AcceptChanges();
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp06145_OM_Visit_EditTableAdapter.Fill(dataSet_OM_Visit.sp06145_OM_Visit_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);

                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }
       
        private void frm_OM_Visit_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_OM_Visit_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

 
        private void Attach_EditValueChanged_To_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_OM_Visit.sp06145_OM_Visit_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Visit", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        LinkedToParentButtonEdit.Focus();

                        LinkedToParentButtonEdit.Properties.ReadOnly = false;
                        LinkedToParentButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        StartDateDateEdit.Focus();

                        LinkedToParentButtonEdit.Properties.ReadOnly = true;
                        LinkedToParentButtonEdit.Properties.Buttons[0].Enabled = false;

                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        StartDateDateEdit.Focus();

                        LinkedToParentButtonEdit.Properties.ReadOnly = false;
                        LinkedToParentButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        StartDateDateEdit.Focus();

                        LinkedToParentButtonEdit.Properties.ReadOnly = true;
                        LinkedToParentButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            Set_Field_Enabled_Status();
            Set_Financial_Group_Header_Captions();

            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
            ibool_ignoreValidation = true;
            ibool_FormStillLoading = false;
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = dataSet_OM_Visit.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            int intID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp06145OMVisitEditBindingSource.Current;
            if (currentRow != null)
            {
                intID = (currentRow["VisitID"] == null ? 0 : Convert.ToInt32(currentRow["VisitID"]));
            }
            bbiViewOnMap.Enabled = intID > 0 && (strFormMode != "blockedit" || strFormMode != "blockadd");  // Set status of button //
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Saving Changes...");

            this.sp06145OMVisitEditBindingSource.EndEdit();
            try
            {
                sp06145_OM_Visit_EditTableAdapter.Update(dataSet_OM_Visit);  // Insert and Update queries defined in Table Adapter //
            }
            catch (Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
                var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
                this.strFormMode = "edit";  // Switch mode to Edit so than any subsequent changes update this record //
                if (currentRow != null)
                {
                    currentRow.strMode = "edit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    strNewIDs = currentRow.VisitID + ";";
                }
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
                var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
                this.strFormMode = "blockedit";  // Switch mode to BlockEdit so than any subsequent changes update these record //
                if (currentRow != null)
                {
                    currentRow.strMode = "blockedit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Values returned from Update SP //
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                if (this.ParentForm != null)
                {
                    foreach (Form frmChild in this.ParentForm.MdiChildren)
                    {
                        if (frmChild.Name == "frm_OM_Visit_Manager")
                        {
                            var fParentForm = (frm_OM_Visit_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Visit, strNewIDs);
                        }
                        if (frmChild.Name == "frm_OM_Tender_Manager")
                        {
                            var fParentForm = (frm_OM_Tender_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(2, Utils.enmFocusedGrid.Visit, strNewIDs);
                        }
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < dataSet_OM_Visit.sp06145_OM_Visit_Edit.Rows.Count; i++)
            {
                switch (dataSet_OM_Visit.sp06145_OM_Visit_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    Set_Field_Enabled_Status();
                    Set_Financial_Group_Header_Captions();
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void LinkedToParentButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")
            {
                var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
                var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                int intClientContractID = (string.IsNullOrEmpty(currentRow.ClientContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientContractID));
                int intSiteContractID = (string.IsNullOrEmpty(currentRow.SiteContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteContractID));
                var fChildForm = new frm_OM_Select_Site_Contract();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalParentSelectedID = intClientContractID;
                fChildForm.intOriginalChildSelectedID = intSiteContractID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.ClientContractID = fChildForm.intSelectedParentID;
                    currentRow.SiteContractID = fChildForm.intSelectedChildID;
                    currentRow.LinkedToParent = fChildForm.strSelectedChildDescription;
                    currentRow.ImagesFolderOM = fChildForm.strImagesFolderOM;
                    currentRow.ClientID = fChildForm.intSelectedClientID;
                    currentRow.SiteID = fChildForm.intSelectedSiteID;
                    currentRow.SiteContractDaysSeparationPercent = fChildForm.decDaysSeparationPercent;

                    // Update default Client PO # //
                    int intRecordTypeID = 1;
                    int intDefaultPOID = 0;
                    string strDefaultPONumber = "";
                    try
                    {
                        SqlConnection SQlConn = new SqlConnection(strConnectionString);
                        SqlCommand cmd = null;
                        cmd = new SqlCommand("sp06328_OM_Default_Client_PO_Number_For_Record", SQlConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@RecordID", intSiteContractID));
                        cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordTypeID));
                        SqlDataAdapter sdaPO = new SqlDataAdapter(cmd);
                        DataSet dsPO = new DataSet("NewDataSet");
                        sdaPO.Fill(dsPO, "Table");
                        foreach (DataRow dr in dsPO.Tables[0].Rows)
                        {
                            intDefaultPOID = Convert.ToInt32(dr["ClientPurchaseOrderID"]);
                            strDefaultPONumber = dr["PONumber"].ToString();
                            break;
                        }
                        if (intDefaultPOID <= 0)  // No Site Contract specific Client PO ID so try for Client contract specific //
                        {
                            intRecordTypeID = 0;
                            cmd = null;
                            cmd = new SqlCommand("sp06328_OM_Default_Client_PO_Number_For_Record", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@RecordID", intClientContractID));
                            cmd.Parameters.Add(new SqlParameter("@RecordTypeID", intRecordTypeID));
                            sdaPO = new SqlDataAdapter(cmd);
                            dsPO = new DataSet("NewDataSet");
                            sdaPO.Fill(dsPO, "Table");
                            foreach (DataRow dr in dsPO.Tables[0].Rows)
                            {
                                intDefaultPOID = Convert.ToInt32(dr["ClientPurchaseOrderID"]);
                                strDefaultPONumber = dr["PONumber"].ToString();
                                break;
                            }
                        }
                    }
                    catch (Exception) { }
                    currentRow.ClientPOID = intDefaultPOID;
                    currentRow.ClientPONumber = strDefaultPONumber;
                    
                    sp06145OMVisitEditBindingSource.EndEdit();
                    Set_Team_Cost(false);
                    Set_Client_Sell(false);
                    Calculate_Min_Days_Between_Visits(false);
                }
            }
        }
        private void LinkedToParentButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && this.strFormMode != "view" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(LinkedToParentButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(LinkedToParentButtonEdit, "");
            }
        }

        private void VisitTypeIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && this.strFormMode != "view" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(VisitTypeIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(VisitTypeIDGridLookUpEdit, "");
            }
        }

        private void ExpectedStartDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ExpectedStartDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (this.strFormMode == "blockedit" || this.strFormMode == "view") return;

            bool boolStartDateValid = false;
            bool boolEndDateValid = false;
            ValidateDates(de.DateTime, ExpectedEndDateDateEdit.DateTime, ref boolStartDateValid, ref boolEndDateValid);
        }
        private void ExpectedStartDateDateEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            if (this.strFormMode == "blockedit") return;

            Set_Team_Cost(false);
            Set_Client_Sell(false);
            Calculate_Min_Days_Between_Visits(false);
        }
        
        private void ExpectedEndDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (this.strFormMode == "blockedit" || this.strFormMode == "view") return;

            bool boolStartDateValid = false;
            bool boolEndDateValid = false;
            ValidateDates(ExpectedStartDateDateEdit.DateTime, de.DateTime, ref boolStartDateValid, ref boolEndDateValid);
        }

        private void ValidateDates(DateTime? dtStart, DateTime? dtEnd, ref bool boolStartValid, ref bool boolEndValid)
        {
            if (dtStart > dtEnd)
            {
                dxErrorProvider1.SetError(ExpectedStartDateDateEdit, "Expected End Date should be greater than Expected Start Date.");
                dxErrorProvider1.SetError(ExpectedEndDateDateEdit, "Expected End Date should be greater than Expected Start Date.");
                boolStartValid = false;
                boolEndValid = false;
                return;
            }
            if ((dtStart == null || string.IsNullOrEmpty(dtStart.ToString())))
            {
                dxErrorProvider1.SetError(ExpectedStartDateDateEdit, "Select\\enter a value.");
                boolStartValid = false;
            }
            else
            {
                dxErrorProvider1.SetError(ExpectedStartDateDateEdit, "");
                boolStartValid = true;
            }
            if ((dtEnd == null || string.IsNullOrEmpty(dtEnd.ToString())))
            {
                dxErrorProvider1.SetError(ExpectedEndDateDateEdit, "Select\\enter a value.");
                boolEndValid = false;
            }
            else
            {
                dxErrorProvider1.SetError(ExpectedEndDateDateEdit, "");
                boolEndValid = true;
            }
            return;          
        }

        private void VisitNumberSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            if (this.strFormMode != "blockedit" && this.strFormMode != "view" && (string.IsNullOrEmpty(se.EditValue.ToString()) || se.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(VisitNumberSpinEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(VisitNumberSpinEdit, "");
            }
        }
        
        private void StatusDescriptionButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
                var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                int intVisitStatusID = 0;
                try
                {
                    intVisitStatusID = (string.IsNullOrEmpty(currentRow.VisitStatusID.ToString()) ? 0 : Convert.ToInt32(currentRow.VisitStatusID));
                }
                catch (Exception) { }
                var fChildForm = new frm_OM_Select_Visit_Status();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalID = intVisitStatusID;
                fChildForm._Mode = "single";
                fChildForm._IncludeBlank = 0;
                fChildForm._ExcludedStatusIDs = "50,55,60,70";
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.VisitStatusID = fChildForm.intSelectedID;
                    currentRow.StatusDescription = fChildForm.strSelectedDescription1;
                    sp06145OMVisitEditBindingSource.EndEdit();
                }
            }
        }

        private void VisitCategoryIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view" || this.strFormMode == "blockedit") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void VisitCategoryIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && this.strFormMode != "view" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "-1"))
            {
                dxErrorProvider1.SetError(VisitCategoryIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(VisitCategoryIDGridLookUpEdit, "");
            }
        }
        private void VisitCategoryIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            
            Set_Team_Cost(false);
            Set_Client_Sell(false);
        }

        private void CostCalculationLevelGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void CostCalculationLevelGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && this.strFormMode != "view" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "-1"))
            {
                dxErrorProvider1.SetError(CostCalculationLevelGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(CostCalculationLevelGridLookUpEdit, "");
            }
        }
        private void CostCalculationLevelGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            if (this.strFormMode == "blockedit") return;
          
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            int intValue = (string.IsNullOrEmpty(glue.EditValue.ToString()) ? 1 : Convert.ToInt32(glue.EditValue));
            Set_Field_Enabled_Status();
            Set_Team_Cost(false);
        }

        private void CalculateCostButton_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("<color=red>Are you sure you wish to re-calculate the Visit Costs?</color>", "Re-Calculate Visit Costs", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) != DialogResult.Yes) return;
            Set_Team_Cost(true);
        }
        
        private void SellCalculationLevelGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void SellCalculationLevelGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && this.strFormMode != "view" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "-1"))
            {
                dxErrorProvider1.SetError(SellCalculationLevelGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(SellCalculationLevelGridLookUpEdit, "");
            }
        }
        private void SellCalculationLevelGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            if (this.strFormMode == "blockedit") return;
          
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            int intValue = (string.IsNullOrEmpty(glue.EditValue.ToString()) ? 1 : Convert.ToInt32(glue.EditValue));
            Set_Field_Enabled_Status();
            Set_Client_Sell(false);
        }
        private void CalculateSellButton_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("<color=red>Are you sure you wish to re-calculate the Visit Sell Price?</color>", "Re-Calculate Visit Sell Price", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) != DialogResult.Yes) return;
            Set_Client_Sell(true);
        }



        private void VisitLabourCostSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void VisitLabourCostSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = 0;
            try { decValue = (string.IsNullOrEmpty(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value)); }
            catch (Exception) { }

            Calculate_Total_Cost("VisitLabourCost", decValue);
        }

        private void VisitMaterialCostSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void VisitMaterialCostSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = 0;
            try { decValue = (string.IsNullOrEmpty(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value)); }
            catch (Exception) { }

            Calculate_Total_Cost("VisitMaterialCost", decValue);
        }

        private void VisitEquipmentCostSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void VisitEquipmentCostSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = 0;
            try { decValue = (string.IsNullOrEmpty(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value)); }
            catch (Exception) { }

            Calculate_Total_Cost("VisitEquipmentCost", decValue);
        }

        private void VisitLabourSellSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void VisitLabourSellSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = 0;
            try { decValue = (string.IsNullOrEmpty(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value)); }
            catch (Exception) { }

            Calculate_Total_Sell("VisitLabourSell", decValue);
        }

        private void VisitMaterialSellSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void VisitMaterialSellSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = 0;
            try { decValue = (string.IsNullOrEmpty(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value)); }
            catch (Exception) { }

            Calculate_Total_Sell("VisitMaterialSell", decValue);
        }

        private void VisitEquipmentSellSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void VisitEquipmentSellSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            SpinEdit se = (SpinEdit)sender;
            decimal decValue = 0;
            try { decValue = (string.IsNullOrEmpty(se.Value.ToString()) ? 0 : Convert.ToDecimal(se.Value)); }
            catch (Exception) { }

            Calculate_Total_Sell("VisitEquipmentSell", decValue);
        }

        private void Calculate_Total_Cost(string strCurrentColumn, decimal decValue)
        {
            var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
            var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            decimal decVisitLabourCost = (strCurrentColumn == "VisitLabourCost" ? decValue : (string.IsNullOrWhiteSpace(currentRow["VisitLabourCost"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.VisitLabourCost)));
            decimal decVisitMaterialCost = (strCurrentColumn == "VisitMaterialCost" ? decValue : (string.IsNullOrWhiteSpace(currentRow["VisitMaterialCost"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.VisitMaterialCost)));
            decimal decVisitEquipmentCost = (strCurrentColumn == "VisitEquipmentCost" ? decValue : (string.IsNullOrWhiteSpace(currentRow["VisitEquipmentCost"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.VisitEquipmentCost)));

            decimal decCurrentValue = (strCurrentColumn == "VisitCost" ? decValue : (string.IsNullOrWhiteSpace(currentRow["VisitCost"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.VisitCost)));

            if (decCurrentValue != (decVisitLabourCost + decVisitMaterialCost + decVisitEquipmentCost))
            {
                currentRow.VisitCost = (decVisitLabourCost + decVisitMaterialCost + decVisitEquipmentCost);
                sp06145OMVisitEditBindingSource.EndEdit();
            }
        }
        private void Calculate_Total_Sell(string strCurrentColumn, decimal decValue)
        {
            var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
            var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            decimal decVisitLabourSell = (strCurrentColumn == "VisitLabourSell" ? decValue : (string.IsNullOrWhiteSpace(currentRow["VisitLabourSell"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.VisitLabourSell)));
            decimal decVisitMaterialSell = (strCurrentColumn == "VisitMaterialSell" ? decValue : (string.IsNullOrWhiteSpace(currentRow["VisitMaterialSell"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.VisitMaterialSell)));
            decimal decVisitEquipmentSell = (strCurrentColumn == "VisitEquipmentSell" ? decValue : (string.IsNullOrWhiteSpace(currentRow["VisitEquipmentSell"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.VisitEquipmentSell)));

            decimal decCurrentValue = (strCurrentColumn == "VisitSell" ? decValue : (string.IsNullOrWhiteSpace(currentRow["VisitSell"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.VisitSell)));

            if (decCurrentValue != (decVisitLabourSell + decVisitMaterialSell + decVisitEquipmentSell))
            {
                currentRow.VisitSell = (decVisitLabourSell + decVisitMaterialSell + decVisitEquipmentSell);
                sp06145OMVisitEditBindingSource.EndEdit();
            }
        }


        private void DoNotPayContractorCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void DoNotPayContractorCheckEdit_Validated(object sender, EventArgs e)
        {
            Set_Team_Cost(false);
        }

        private void DoNotInvoiceClientCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void DoNotInvoiceClientCheckEdit_Validated(object sender, EventArgs e)
        {
            Set_Client_Sell(false);
        }

        private void StartDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void StartDateDateEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            if (this.strFormMode == "blockedit") return;

            Set_Team_Cost(false);
            Set_Client_Sell(false);
        }

        private void ClientSignaturePathButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
            var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            string strImagesFolderOM = currentRow["ImagesFolderOM"].ToString() + "\\Signatures";
            string strFirstPartOfPathWithBackslash = strSignaturePath + "\\";
            switch (e.Button.Tag.ToString())
            {
                case "select file":
                    {
                        if (strFormMode == "view") return;
                        using (OpenFileDialog dlg = new OpenFileDialog())
                        {
                            dlg.DefaultExt = "tab";
                            dlg.Filter = "";
                            if (strSignaturePath != "") dlg.InitialDirectory = strFirstPartOfPathWithBackslash + strImagesFolderOM;
                            dlg.Multiselect = false;
                            dlg.ShowDialog();
                            if (dlg.FileNames.Length > 0)
                            {
                                string strTempFileName = "";
                                string strTempResult = "";
                                //string strExtension = "";
                                foreach (string filename in dlg.FileNames)
                                {
                                    if (strFirstPartOfPathWithBackslash != "")
                                    {
                                       if (!filename.ToLower().StartsWith(strFirstPartOfPathWithBackslash.ToLower() + strImagesFolderOM.ToLower()))
                                       {
                                            DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files from under the default folder [as specified in the System Configuration Screen - Default Client Signatures Folder] or one of it's sub-folders.\n\nFile Selection Aborted!", "Select File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            break;
                                        }
                                    }
                                    strTempFileName = filename.Substring(strFirstPartOfPathWithBackslash.Length + strImagesFolderOM.Length);
                                    if (strTempResult == "")
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += strTempFileName;
                                    }
                                    else
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += "\r\n" + strTempFileName;
                                    }
                                }
                                ClientSignaturePathButtonEdit.Text = strTempResult;

                                if (!string.IsNullOrWhiteSpace(strTempResult))
                                {
                                    // Clear No One To Sign //
                                    int intNoOnetoSign = 0;
                                    try
                                    {
                                        intNoOnetoSign = (string.IsNullOrEmpty(currentRow.NoOnetoSign.ToString()) ? 0 : Convert.ToInt32(currentRow.NoOnetoSign));
                                    }
                                    catch (Exception) { }
                                    if (intNoOnetoSign > 0)
                                    {
                                        currentRow.NoOnetoSign = 0;
                                    }
                                }
                            }
                            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter so linked file can be viewed if required //
                        }
                    }
                    break;
                case "view file":
                    {
                        string strFile = currentRow["ClientSignaturePath"].ToString();

                        if (string.IsNullOrEmpty(strFile))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Link the Client Signature to be Viewed before proceeding.", "View Linked Client Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        try
                        {
                            System.Diagnostics.Process.Start(Path.Combine(strFirstPartOfPathWithBackslash + strImagesFolderOM + strFile));
                        }
                        catch
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the Linked Client Signature: " + strFile + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Client Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
            }
        }
        
        private void ClientPONumberButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
                var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                if (strFormMode == "blockedit" || strFormMode == "blockadd")
                {
                    var fChildForm = new frm_OM_Select_Client_PO();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strPassedInParentIDs = "";
                    fChildForm.intPassedInChildID = 0;
                    fChildForm.intFilterSummerMaintenanceClient = 1;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        currentRow.ClientPOID = fChildForm.intSelectedChildID;
                        currentRow.ClientPONumber = fChildForm.strSelectedChildDescriptions;
                        sp06145OMVisitEditBindingSource.EndEdit();
                    }
                }
                else
                {
                    int intClientContractID = 0;
                    try { intClientContractID = (string.IsNullOrEmpty(currentRow.ClientContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientContractID)); }
                    catch (Exception) { }

                    int intSiteContractID = 0;
                    try { intSiteContractID = (string.IsNullOrEmpty(currentRow.SiteContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteContractID)); }
                    catch (Exception) { }

                    int intClientPOID = 0;
                    try { intClientPOID = (string.IsNullOrEmpty(currentRow.ClientPOID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientPOID)); }
                    catch (Exception) { }

                    var fChildForm = new frm_OM_Select_Client_PO_Multi_Page();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.boolPage1Enabled = true;
                    fChildForm.boolPage2Enabled = true;
                    fChildForm.boolPage3Enabled = false; // Disable Visit page //
                    fChildForm.intActivePage = 2;
                    fChildForm._PassedInClientContractIDs = intClientContractID.ToString() + ",";
                    fChildForm._PassedInSiteContractIDs = intSiteContractID.ToString() + ",";
                    fChildForm._PassedInVisitIDs = "";
                    fChildForm.intOriginalPOID = intClientPOID;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        currentRow.ClientPOID = fChildForm.intSelectedPOID;
                        currentRow.ClientPONumber = fChildForm.strSelectedPONumbers;
                        sp06145OMVisitEditBindingSource.EndEdit();
                    }
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
                var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                currentRow.ClientPOID = 0;
                currentRow.ClientPONumber = "";
                sp06145OMVisitEditBindingSource.EndEdit();
            }
        }

        private void Set_Financial_Group_Header_Captions()
        {
            var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
            var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            string strSelfBillingInvoice = "";
            try { strSelfBillingInvoice = currentRow.SelfBillingInvoice.ToString(); } catch (Exception) { }
               
            int intClientInvoiceID = 0;
            try { intClientInvoiceID = currentRow.ClientInvoiceID; } catch (Exception) { }

            try
            {
                layoutControlGroupTeamCosts.Text = (string.IsNullOrEmpty(strSelfBillingInvoice) ? "Team <b>Costs</b>" : "Team <b>Costs     <color=red>PAID</color></b>");
                layoutControlGroupClientSell.Text = (intClientInvoiceID == 0 ? "Client <b>Sell</b>" : "Client <b>Sell     <color=red>BILLED</color></b>");
            }
            catch (Exception) { }          
        }

        private void Set_Field_Enabled_Status()
        {
            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit")
            {
                VisitLabourCostSpinEdit.Properties.ReadOnly = true;
                VisitMaterialCostSpinEdit.Properties.ReadOnly = true;
                VisitEquipmentCostSpinEdit.Properties.ReadOnly = true;
                VisitCostSpinEdit.Properties.ReadOnly = true;
                CalculateCostButton.Enabled = false;

                VisitLabourSellSpinEdit.Properties.ReadOnly = true;
                VisitMaterialSellSpinEdit.Properties.ReadOnly = true;
                VisitEquipmentSellSpinEdit.Properties.ReadOnly = true;
                VisitSellSpinEdit.Properties.ReadOnly = true;
                CalculateSellButton.Enabled = false;
                CalculateDaysButton.Enabled = false;

                LinkedToParentButtonEdit.Properties.ReadOnly = true;
                LinkedToParentButtonEdit.Properties.Buttons[0].Enabled = false;

                // ********** Following disabled at request of Paul Reynolds - 24/10/2017 ********** //
                /*
                if (strFormMode == "blockedit" && (_AtLeastOneSelfBillingInvoice || _AtLeastOneClientInvoice))
                {
                    VisitTypeIDGridLookUpEdit.Properties.ReadOnly = true;
                    VisitCategoryIDGridLookUpEdit.Properties.ReadOnly = true;

                    ExpectedStartDateDateEdit.Properties.ReadOnly = true;
                    ExpectedEndDateDateEdit.Properties.ReadOnly = true;
                    StartDateDateEdit.Properties.ReadOnly = true;
                    EndDateDateEdit.Properties.ReadOnly = true;
                }
                 */
                return;
            }
            var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
            var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
            if (currentRow == null) return;
            int intCostCalculationLevel = 0;
            string strSelfBillingInvoice = "";
            try
            {
                intCostCalculationLevel = (string.IsNullOrEmpty(currentRow.CostCalculationLevel.ToString()) ? 0 : Convert.ToInt32(currentRow.CostCalculationLevel));
                strSelfBillingInvoice = (string.IsNullOrEmpty(currentRow.SelfBillingInvoice.ToString()) ? "" : currentRow.SelfBillingInvoice.ToString());
            }
            catch (Exception) { }
            VisitLabourCostSpinEdit.Properties.ReadOnly = (iBool_VisitFullEditAccess && intCostCalculationLevel == 0 && strSelfBillingInvoice == "" ? false : true);
            VisitMaterialCostSpinEdit.Properties.ReadOnly = (iBool_VisitFullEditAccess && intCostCalculationLevel == 0 && strSelfBillingInvoice == "" ? false : true);
            VisitEquipmentCostSpinEdit.Properties.ReadOnly = (iBool_VisitFullEditAccess && intCostCalculationLevel == 0 && strSelfBillingInvoice == "" ? false : true);
            VisitCostSpinEdit.Properties.ReadOnly = true;  // (iBool_VisitFullEditAccess && intCostCalculationLevel == 0 && strSelfBillingInvoice == "" ? false : true);
            CalculateCostButton.Enabled = (iBool_VisitFullEditAccess && intCostCalculationLevel > 0 && strSelfBillingInvoice == "" ? true : false);

            int intSellCalculationLevel = 0;
            int intClientInvoiceID = 0;
            try
            {
                intSellCalculationLevel = (string.IsNullOrEmpty(currentRow.SellCalculationLevel.ToString()) ? 0 : Convert.ToInt32(currentRow.SellCalculationLevel));
                intClientInvoiceID = (string.IsNullOrEmpty(currentRow.ClientInvoiceID.ToString()) ? 0 : Convert.ToInt32(currentRow.ClientInvoiceID));
            }
            catch (Exception) { }
            VisitLabourSellSpinEdit.Properties.ReadOnly = (iBool_VisitFullEditAccess && intSellCalculationLevel == 0 && intClientInvoiceID == 0 ? false : true);
            VisitMaterialSellSpinEdit.Properties.ReadOnly = (iBool_VisitFullEditAccess && intSellCalculationLevel == 0 && intClientInvoiceID == 0 ? false : true);
            VisitEquipmentSellSpinEdit.Properties.ReadOnly = (iBool_VisitFullEditAccess && intSellCalculationLevel == 0 && intClientInvoiceID == 0 ? false : true);
            VisitSellSpinEdit.Properties.ReadOnly = true;  // (iBool_VisitFullEditAccess && intSellCalculationLevel == 0 && intClientInvoiceID == 0 ? false : true);
            CalculateSellButton.Enabled = (iBool_VisitFullEditAccess && intSellCalculationLevel > 0 && intClientInvoiceID == 0 ? true : false);

            LinkedToParentButtonEdit.Properties.ReadOnly = (!iBool_VisitFullEditAccess || strSelfBillingInvoice == "Yes" || intClientInvoiceID > 0 ? true : false);
            LinkedToParentButtonEdit.Properties.Buttons[0].Enabled = (!iBool_VisitFullEditAccess || strSelfBillingInvoice == "Yes" || intClientInvoiceID > 0 ? false : true);

            // ********** Following disabled at request of Paul Reynolds - 24/10/2017 ********** //
            /*
            VisitTypeIDGridLookUpEdit.Properties.ReadOnly = (strSelfBillingInvoice == "Yes" || intClientInvoiceID > 0 ? true : false);
            VisitCategoryIDGridLookUpEdit.Properties.ReadOnly = (strSelfBillingInvoice == "Yes" || intClientInvoiceID > 0 ? true : false);

            ExpectedStartDateDateEdit.Properties.ReadOnly = (strSelfBillingInvoice == "Yes" || intClientInvoiceID > 0 ? true : false);
            ExpectedEndDateDateEdit.Properties.ReadOnly = (strSelfBillingInvoice == "Yes" || intClientInvoiceID > 0 ? true : false);
            StartDateDateEdit.Properties.ReadOnly = (strSelfBillingInvoice == "Yes" || intClientInvoiceID > 0 ? true : false);
            EndDateDateEdit.Properties.ReadOnly = (strSelfBillingInvoice == "Yes" || intClientInvoiceID > 0 ? true : false);
            */

            CostCalculationLevelGridLookUpEdit.ReadOnly = (!iBool_VisitFullEditAccess || strSelfBillingInvoice == "Yes");
            SellCalculationLevelGridLookUpEdit.ReadOnly = (!iBool_VisitFullEditAccess || intClientInvoiceID > 0);

            DoNotPayContractorCheckEdit.ReadOnly = (!iBool_VisitFullEditAccess || strSelfBillingInvoice == "Yes");
            DoNotInvoiceClientCheckEdit.ReadOnly = (!iBool_VisitFullEditAccess || intClientInvoiceID > 0);


            VisitTypeIDGridLookUpEdit.ReadOnly = !iBool_VisitFullEditAccess;
            VisitCategoryIDGridLookUpEdit.ReadOnly = !iBool_VisitFullEditAccess;
            VisitNumberSpinEdit.ReadOnly = !iBool_VisitFullEditAccess;
            StatusDescriptionButtonEdit.ReadOnly = !iBool_VisitFullEditAccess;
            StatusDescriptionButtonEdit.Properties.Buttons[0].Enabled = iBool_VisitFullEditAccess;

            ExpectedStartDateDateEdit.ReadOnly = !iBool_VisitFullEditAccess;
            ExpectedEndDateDateEdit.ReadOnly = !iBool_VisitFullEditAccess;
            StartDateDateEdit.ReadOnly = !iBool_VisitFullEditAccess;
            EndDateDateEdit.ReadOnly = !iBool_VisitFullEditAccess;
            DaysSeparationSpinEdit.ReadOnly = !iBool_VisitFullEditAccess;
            CalculateDaysButton.Enabled = false;  // iBool_VisitFullEditAccess;  // Commented out by MB - 18/12/2017 - Hardcoded to disabled //

            StartLatitudeTextEdit.ReadOnly =  !iBool_VisitFullEditAccess;
            StartLongitudeTextEdit.ReadOnly = !iBool_VisitFullEditAccess;
            FinishLatitudeTextEdit.ReadOnly = !iBool_VisitFullEditAccess;
            FinishLongitudeTextEdit.ReadOnly = !iBool_VisitFullEditAccess;
            ReworkCheckEdit.ReadOnly = !iBool_VisitFullEditAccess;
            StatusIssueIDGridLookUpEdit.ReadOnly = !iBool_VisitFullEditAccess;
            AccidentOnSiteCheckEdit.ReadOnly = !iBool_VisitFullEditAccess;
            AccidentCommentMemoEdit.ReadOnly = !iBool_VisitFullEditAccess;
            NoOnetoSignCheckEdit.ReadOnly = !iBool_VisitFullEditAccess;
            ManagerNameTextEdit.ReadOnly = !iBool_VisitFullEditAccess;
            ManagerNotesMemoEdit.ReadOnly = !iBool_VisitFullEditAccess;
            ClientSignaturePathButtonEdit.ReadOnly = !iBool_VisitFullEditAccess;
            ClientSignaturePathButtonEdit.Properties.Buttons[0].Enabled = iBool_VisitFullEditAccess;

            CreatedFromVisitTemplateIDGridLookUpEdit.ReadOnly = !iBool_VisitFullEditAccess;
            ClientPONumberButtonEdit.ReadOnly = !iBool_VisitFullEditAccess;
            CompletionSheetNumberTextEdit.ReadOnly = !iBool_VisitFullEditAccess;
            ExtraCostsFinalisedCheckEdit.ReadOnly =!iBool_VisitFullEditAccess;

        }

        private void Set_Team_Cost(bool ShowErrorMessages)
        {
            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_FormStillLoading) return;
            var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
            var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
            if (currentRow == null) return;
            int intVisitID = (string.IsNullOrEmpty(currentRow.VisitID.ToString()) ? 0 : Convert.ToInt32(currentRow.VisitID));
            if (intVisitID <= 0 && ShowErrorMessages)
            {
                XtraMessageBox.Show("Save the Visit first before clicking the Re-Calculate Cost Price button.", "Re-Calculate Cost Price", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intDoNotPayContractor = 0;
            try
            {
                intDoNotPayContractor = Convert.ToInt32(currentRow.DoNotPayContractor);
            }
            catch (Exception) { }
            if (intDoNotPayContractor == 1)
            {
                currentRow.VisitLabourCost = (decimal)0.00;
                currentRow.VisitMaterialCost = (decimal)0.00;
                currentRow.VisitEquipmentCost = (decimal)0.00;
                currentRow.VisitCost = (decimal)0.00;
                sp06145OMVisitEditBindingSource.EndEdit();
                return;
            }
            int intCalculationLevel = (string.IsNullOrEmpty(currentRow.CostCalculationLevel.ToString()) ? 0 : Convert.ToInt32(currentRow.CostCalculationLevel));
            if (intCalculationLevel == 0) return;  // Manual method so don't bother to calculate //
            
            int intVisitCategoryID = (string.IsNullOrEmpty(currentRow.VisitCategoryID.ToString()) ? 0 : Convert.ToInt32(currentRow.VisitCategoryID));
            int intSiteContractID = (string.IsNullOrEmpty(currentRow.SiteContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteContractID));
            DateTime dtStartDate = DateTime.MinValue;
            try
            {
                dtStartDate = Convert.ToDateTime(currentRow.StartDate);
            }
            catch (Exception) { }
            if (dtStartDate == DateTime.MinValue)
            {
                try
                {
                    dtStartDate = Convert.ToDateTime(currentRow.ExpectedStartDate);
                }
                catch (Exception) { }
            }
            decimal decCurrentValue = (string.IsNullOrEmpty(currentRow.VisitCost.ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.VisitCost));
            decimal decValue = (decimal)0.00;
            using (var GetValue = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter())
            {
                GetValue.ChangeConnectionString(strConnectionString);
                try
                {
                    decValue = Convert.ToDecimal(GetValue.sp06368_OM_Get_Visit_Value(intVisitID, "teamcost", intCalculationLevel, intVisitCategoryID, intSiteContractID, dtStartDate));
                    if (decValue != decCurrentValue)
                    {
                        currentRow.VisitLabourCost = (decimal)0.00;
                        currentRow.VisitMaterialCost = (decimal)0.00;
                        currentRow.VisitEquipmentCost = (decimal)0.00;
                        currentRow.VisitCost = decValue;
                        sp06145OMVisitEditBindingSource.EndEdit();
                    }
                }
                catch (Exception) { }
            }
        }

        private void Set_Client_Sell(bool ShowErrorMessages)
        {
            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_FormStillLoading) return;
            var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
            var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
            if (currentRow == null) return;
            int intVisitID = (string.IsNullOrEmpty(currentRow.VisitID.ToString()) ? 0 : Convert.ToInt32(currentRow.VisitID));
            if (intVisitID <= 0 && ShowErrorMessages)
            {
                XtraMessageBox.Show("Save the Visit first before clicking the Re-Calculate Sell Price button.", "Re-Calculate Sell Price", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intDoNotInvoiceClient = 0;
            try
            {
                intDoNotInvoiceClient = Convert.ToInt32(currentRow.DoNotInvoiceClient);
            }
            catch (Exception) { }
            if (intDoNotInvoiceClient == 1)
            {
                currentRow.VisitLabourSell = (decimal)0.00;
                currentRow.VisitMaterialSell = (decimal)0.00;
                currentRow.VisitEquipmentSell = (decimal)0.00;
                currentRow.VisitSell = (decimal)0.00;
                sp06145OMVisitEditBindingSource.EndEdit();
                return;
            }

            int intCalculationLevel = (string.IsNullOrEmpty(currentRow.SellCalculationLevel.ToString()) ? 0 : Convert.ToInt32(currentRow.SellCalculationLevel));
            if (intCalculationLevel == 0) return;  // Manual method so don't bother to calculate //
            
            int intVisitCategoryID = (string.IsNullOrEmpty(currentRow.VisitCategoryID.ToString()) ? 0 : Convert.ToInt32(currentRow.VisitCategoryID));
            int intSiteContractID = (string.IsNullOrEmpty(currentRow.SiteContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteContractID));
            DateTime dtStartDate = DateTime.MinValue;
            try
            {
                dtStartDate = Convert.ToDateTime(currentRow.StartDate);
            }
            catch (Exception) { }
            if (dtStartDate == DateTime.MinValue)
            {
                try
                {
                    dtStartDate = Convert.ToDateTime(currentRow.ExpectedStartDate);
                }
                catch (Exception) { }
            }
            decimal decCurrentValue = (string.IsNullOrEmpty(currentRow.VisitSell.ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.VisitSell));
            decimal decValue = (decimal)0.00;
            using (var GetValue = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter())
            {
                GetValue.ChangeConnectionString(strConnectionString);
                try
                {
                    decValue = Convert.ToDecimal(GetValue.sp06368_OM_Get_Visit_Value(intVisitID, "clientsell", intCalculationLevel, intVisitCategoryID, intSiteContractID, dtStartDate));
                    if (decValue != decCurrentValue)
                    {
                        currentRow.VisitLabourSell = (decimal)0.00;
                        currentRow.VisitMaterialSell = (decimal)0.00;
                        currentRow.VisitEquipmentSell = (decimal)0.00;
                        currentRow.VisitSell = decValue;
                        sp06145OMVisitEditBindingSource.EndEdit();
                    }
                }
                catch (Exception) { }
            }
        }

        private void SelfBillingInvoiceButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
            var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
            if (currentRow == null) return;
            string strSelfBillingInvoice = (string.IsNullOrEmpty(currentRow.SelfBillingInvoice.ToString()) ? "" : currentRow.SelfBillingInvoice.ToString());
            if (strSelfBillingInvoice != "Yes")
            {
                XtraMessageBox.Show("This visit has not been invoiced yet - Unable to View Self-Billing Invoice.", "View Self-Billing Invoice.", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intVisitID = (string.IsNullOrEmpty(currentRow.VisitID.ToString()) ? 0 : Convert.ToInt32(currentRow.VisitID));
            if (intVisitID <= 0) return;
            OM_View_Self_Billing_Invoice viewInvoice = new OM_View_Self_Billing_Invoice();
            viewInvoice.View_Self_Billing_Invoice(this, this.GlobalSettings, strConnectionStringREADONLY, intVisitID.ToString() + ",");
        }

        private void ClientInvoiceIDButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {

        }
        
        private void NoOnetoSignCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view" || this.strFormMode == "blockedit") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void NoOnetoSignCheckEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            
            var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
            var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
            if (currentRow == null) return;

            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                // Clear Signature and Manager Name //
                string strSignature = (string.IsNullOrEmpty(currentRow.ClientSignaturePath.ToString()) ? "" : currentRow.ClientSignaturePath.ToString());
                if (!string.IsNullOrWhiteSpace(strSignature))
                {
                    currentRow.ClientSignaturePath = "";
                    currentRow.ManagerName = "";
                    sp06145OMVisitEditBindingSource.EndEdit();
                }
            }
        }
        
        private void CalculateDaysButton_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("<color=red>Are you sure you wish to re-calculate the Minimum Days Between Visits?</color>", "Re-Calculate Minimum Days Between Visits", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) != DialogResult.Yes) return;
            Calculate_Min_Days_Between_Visits(true);
        }
        private void Calculate_Min_Days_Between_Visits(bool ShowErrorMessages)
        {
            /* Following commented out by MB - 18/12/2017 --

            if (strFormMode == "view" || strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_FormStillLoading) return;
            var currentRowView = (DataRowView)sp06145OMVisitEditBindingSource.Current;
            var currentRow = (DataSet_OM_Visit.sp06145_OM_Visit_EditRow)currentRowView.Row;
            if (currentRow == null) return;
            int intSiteContractID = (string.IsNullOrWhiteSpace(currentRow.SiteContractID.ToString()) ? 0 : Convert.ToInt32(currentRow.SiteContractID));
            if (intSiteContractID <= 0 && ShowErrorMessages)
            {
                XtraMessageBox.Show("Select the Site Contract first before clicking the Re-Calculate Minimum Days Between Visits button.", "Re-Calculate Minimum Days Between Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            DateTime dtExpectedStartDate = (string.IsNullOrWhiteSpace(currentRow.ExpectedStartDate.ToString()) ? DateTime.MinValue : Convert.ToDateTime(currentRow.ExpectedStartDate));
            if (dtExpectedStartDate <= DateTime.MinValue && ShowErrorMessages)
            {
                XtraMessageBox.Show("Enter the Visit Expected Start Date first before clicking the Re-Calculate Minimum Days Between Visits button.", "Re-Calculate Minimum Days Between Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            decimal decDaysSeparationPercentage = (string.IsNullOrWhiteSpace(currentRow.SiteContractDaysSeparationPercent.ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow.SiteContractDaysSeparationPercent));
            if (decDaysSeparationPercentage <= (decimal)0.00 && ShowErrorMessages)
            {
                XtraMessageBox.Show("The selected Site Contract has no value set for the Days Separation %. Unable to calculate Minimum Days Between Visits.", "Re-Calculate Minimum Days Between Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intCurrentValue = (string.IsNullOrWhiteSpace(currentRow.DaysSeparation.ToString()) ? 0 : Convert.ToInt32(currentRow.DaysSeparation));

            int intDaysSeparation = 0;
            using (var GetValue = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter())
            {
                GetValue.ChangeConnectionString(strConnectionString);
                try
                {
                    intDaysSeparation = Convert.ToInt32(GetValue.sp06437_OM_Get_Visit_Days_Separation(intSiteContractID, dtExpectedStartDate, decDaysSeparationPercentage));
                    if (intDaysSeparation != intCurrentValue)
                    {
                        currentRow.DaysSeparation = (intDaysSeparation < 0 ? 0 : intDaysSeparation);
                        sp06145OMVisitEditBindingSource.EndEdit();
                    }
                }
                catch (Exception) { }
            }
            */
        }
        
        #endregion


        private void bbiViewOnMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "blockedit" || strFormMode == "blockadd") return;
            DataRowView currentRow = (DataRowView)sp06145OMVisitEditBindingSource.Current;
            if (currentRow == null) return;
            int intRecordID = (string.IsNullOrEmpty(currentRow["VisitID"].ToString()) ? 0 : Convert.ToInt32(currentRow["VisitID"]));
            if (intRecordID <= 0)
            {
                XtraMessageBox.Show("Please Save the Visit before attempting to show it's start and end locations on the map.", "View Visits on Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strClientIDs = currentRow["ClientID"].ToString() + ",";
            string strSiteIDs = currentRow["SiteID"].ToString() + ",";
            string strSelectedFullSiteDescriptions = "Client: " + currentRow["ClientName"].ToString() + ", Site: " + currentRow["SiteName"].ToString() + " | ";  // Pipe used as deimiter //

            var fChildForm = new frm_OM_Visit_Mapping_View();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_PassedInJobIDs = "";
            fChildForm.i_str_PassedInVisitIDs = intRecordID.ToString() + ",";
            fChildForm.i_str_PassedInSiteIDs = strSiteIDs;
            fChildForm.i_str_PassedInClientIDs = strClientIDs;
            fChildForm.i_str_selected_Site_descriptions = strSelectedFullSiteDescriptions;
            fChildForm.i_bool_LoadVisitsInMapOnStart = true;
            fChildForm.i_bool_LoadSitesInMapOnStart = false;
            fChildForm.i_bool_LoadJobsInMapOnStart = false;
            fChildForm.strFormMode = "view";
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }





 
 


 








    }
}


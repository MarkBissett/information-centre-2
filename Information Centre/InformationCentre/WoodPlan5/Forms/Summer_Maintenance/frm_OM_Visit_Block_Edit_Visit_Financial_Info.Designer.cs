﻿namespace WoodPlan5
{
    partial class frm_OM_Visit_Block_Edit_Visit_Financial_Info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule4 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue4 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.checkEditClearSelfBillingInvoice = new DevExpress.XtraEditors.CheckEdit();
            this.spinEditTotalSell = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditTotalCost = new DevExpress.XtraEditors.SpinEdit();
            this.gridLookUpEditApplySellCalculationLevel = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06134OMJobCalculationLevelDescriptorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditApplyCostCalculationLevel = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.spinEditLabourCost = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditMaterialCost = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditEquipmentCost = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditLabourSell = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditMaterialSell = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditEquipmentSell = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCostCalculationID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSellCalculationID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControlVisitCount = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditClearSelfBillingInvoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTotalSell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTotalCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditApplySellCalculationLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06134OMJobCalculationLevelDescriptorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditApplyCostCalculationLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditLabourCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMaterialCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditEquipmentCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditLabourSell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMaterialSell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditEquipmentSell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCalculationID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellCalculationID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(431, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 495);
            this.barDockControlBottom.Size = new System.Drawing.Size(431, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 469);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(431, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 469);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn4.Width = 53;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "ID";
            this.gridColumn10.FieldName = "ID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn10.Width = 53;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.checkEditClearSelfBillingInvoice);
            this.layoutControl1.Controls.Add(this.spinEditTotalSell);
            this.layoutControl1.Controls.Add(this.spinEditTotalCost);
            this.layoutControl1.Controls.Add(this.gridLookUpEditApplySellCalculationLevel);
            this.layoutControl1.Controls.Add(this.gridLookUpEditApplyCostCalculationLevel);
            this.layoutControl1.Controls.Add(this.spinEditLabourCost);
            this.layoutControl1.Controls.Add(this.spinEditMaterialCost);
            this.layoutControl1.Controls.Add(this.spinEditEquipmentCost);
            this.layoutControl1.Controls.Add(this.spinEditLabourSell);
            this.layoutControl1.Controls.Add(this.spinEditMaterialSell);
            this.layoutControl1.Controls.Add(this.spinEditEquipmentSell);
            this.layoutControl1.Location = new System.Drawing.Point(0, 69);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1213, 293, 448, 426);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(430, 395);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // checkEditClearSelfBillingInvoice
            // 
            this.checkEditClearSelfBillingInvoice.Location = new System.Drawing.Point(161, 166);
            this.checkEditClearSelfBillingInvoice.MenuManager = this.barManager1;
            this.checkEditClearSelfBillingInvoice.Name = "checkEditClearSelfBillingInvoice";
            this.checkEditClearSelfBillingInvoice.Properties.Caption = "[tick to Mark as Un-paid]";
            this.checkEditClearSelfBillingInvoice.Properties.ValueChecked = 1;
            this.checkEditClearSelfBillingInvoice.Properties.ValueGrayed = 0;
            this.checkEditClearSelfBillingInvoice.Size = new System.Drawing.Size(245, 19);
            this.checkEditClearSelfBillingInvoice.StyleController = this.layoutControl1;
            this.checkEditClearSelfBillingInvoice.TabIndex = 61;
            // 
            // spinEditTotalSell
            // 
            this.spinEditTotalSell.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditTotalSell.Location = new System.Drawing.Point(108, 341);
            this.spinEditTotalSell.MenuManager = this.barManager1;
            this.spinEditTotalSell.Name = "spinEditTotalSell";
            this.spinEditTotalSell.Properties.Appearance.Options.UseTextOptions = true;
            this.spinEditTotalSell.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditTotalSell.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.spinEditTotalSell.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditTotalSell.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.spinEditTotalSell.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditTotalSell.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.spinEditTotalSell.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditTotalSell.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditTotalSell.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditTotalSell.Properties.Mask.EditMask = "c";
            this.spinEditTotalSell.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditTotalSell.Size = new System.Drawing.Size(144, 20);
            this.spinEditTotalSell.StyleController = this.layoutControl1;
            this.spinEditTotalSell.TabIndex = 23;
            // 
            // spinEditTotalCost
            // 
            this.spinEditTotalCost.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditTotalCost.Location = new System.Drawing.Point(108, 142);
            this.spinEditTotalCost.MenuManager = this.barManager1;
            this.spinEditTotalCost.Name = "spinEditTotalCost";
            this.spinEditTotalCost.Properties.Appearance.Options.UseTextOptions = true;
            this.spinEditTotalCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditTotalCost.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.spinEditTotalCost.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditTotalCost.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.spinEditTotalCost.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditTotalCost.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.spinEditTotalCost.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditTotalCost.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditTotalCost.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditTotalCost.Properties.Mask.EditMask = "c";
            this.spinEditTotalCost.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditTotalCost.Size = new System.Drawing.Size(141, 20);
            this.spinEditTotalCost.StyleController = this.layoutControl1;
            this.spinEditTotalCost.TabIndex = 22;
            // 
            // gridLookUpEditApplySellCalculationLevel
            // 
            this.gridLookUpEditApplySellCalculationLevel.Location = new System.Drawing.Point(108, 245);
            this.gridLookUpEditApplySellCalculationLevel.MenuManager = this.barManager1;
            this.gridLookUpEditApplySellCalculationLevel.Name = "gridLookUpEditApplySellCalculationLevel";
            this.gridLookUpEditApplySellCalculationLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditApplySellCalculationLevel.Properties.DataSource = this.sp06134OMJobCalculationLevelDescriptorsBindingSource;
            this.gridLookUpEditApplySellCalculationLevel.Properties.DisplayMember = "Description";
            this.gridLookUpEditApplySellCalculationLevel.Properties.NullText = "";
            this.gridLookUpEditApplySellCalculationLevel.Properties.ValueMember = "ID";
            this.gridLookUpEditApplySellCalculationLevel.Properties.View = this.gridView2;
            this.gridLookUpEditApplySellCalculationLevel.Size = new System.Drawing.Size(298, 20);
            this.gridLookUpEditApplySellCalculationLevel.StyleController = this.layoutControl1;
            this.gridLookUpEditApplySellCalculationLevel.TabIndex = 3;
            this.gridLookUpEditApplySellCalculationLevel.EditValueChanged += new System.EventHandler(this.gridLookUpEditApplySellCalculationLevel_EditValueChanged);
            // 
            // sp06134OMJobCalculationLevelDescriptorsBindingSource
            // 
            this.sp06134OMJobCalculationLevelDescriptorsBindingSource.DataMember = "sp06134_OM_Job_Calculation_Level_Descriptors";
            this.sp06134OMJobCalculationLevelDescriptorsBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Column = this.gridColumn4;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue3.Value1 = -1;
            gridFormatRule3.Rule = formatConditionRuleValue3;
            this.gridView2.FormatRules.Add(gridFormatRule3);
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Cost Calculation Level";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "RecordOrder";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridLookUpEditApplyCostCalculationLevel
            // 
            this.gridLookUpEditApplyCostCalculationLevel.Location = new System.Drawing.Point(108, 46);
            this.gridLookUpEditApplyCostCalculationLevel.MenuManager = this.barManager1;
            this.gridLookUpEditApplyCostCalculationLevel.Name = "gridLookUpEditApplyCostCalculationLevel";
            this.gridLookUpEditApplyCostCalculationLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditApplyCostCalculationLevel.Properties.DataSource = this.sp06134OMJobCalculationLevelDescriptorsBindingSource;
            this.gridLookUpEditApplyCostCalculationLevel.Properties.DisplayMember = "Description";
            this.gridLookUpEditApplyCostCalculationLevel.Properties.NullText = "";
            this.gridLookUpEditApplyCostCalculationLevel.Properties.ValueMember = "ID";
            this.gridLookUpEditApplyCostCalculationLevel.Properties.View = this.gridView5;
            this.gridLookUpEditApplyCostCalculationLevel.Size = new System.Drawing.Size(298, 20);
            this.gridLookUpEditApplyCostCalculationLevel.StyleController = this.layoutControl1;
            this.gridLookUpEditApplyCostCalculationLevel.TabIndex = 2;
            this.gridLookUpEditApplyCostCalculationLevel.EditValueChanged += new System.EventHandler(this.gridLookUpEditApplyCostCalculationLevel_EditValueChanged);
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule4.ApplyToRow = true;
            gridFormatRule4.Column = this.gridColumn10;
            gridFormatRule4.Name = "Format0";
            formatConditionRuleValue4.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue4.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue4.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue4.Value1 = -1;
            gridFormatRule4.Rule = formatConditionRuleValue4;
            this.gridView5.FormatRules.Add(gridFormatRule4);
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Cost Calculation Level";
            this.gridColumn11.FieldName = "Description";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            this.gridColumn11.Width = 220;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Order";
            this.gridColumn12.FieldName = "RecordOrder";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // spinEditLabourCost
            // 
            this.spinEditLabourCost.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditLabourCost.Location = new System.Drawing.Point(108, 70);
            this.spinEditLabourCost.MenuManager = this.barManager1;
            this.spinEditLabourCost.Name = "spinEditLabourCost";
            this.spinEditLabourCost.Properties.Appearance.Options.UseTextOptions = true;
            this.spinEditLabourCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditLabourCost.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.spinEditLabourCost.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditLabourCost.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.spinEditLabourCost.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditLabourCost.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.spinEditLabourCost.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditLabourCost.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditLabourCost.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditLabourCost.Properties.Mask.EditMask = "c";
            this.spinEditLabourCost.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditLabourCost.Size = new System.Drawing.Size(141, 20);
            this.spinEditLabourCost.StyleController = this.layoutControl1;
            this.spinEditLabourCost.TabIndex = 23;
            // 
            // spinEditMaterialCost
            // 
            this.spinEditMaterialCost.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditMaterialCost.Location = new System.Drawing.Point(108, 94);
            this.spinEditMaterialCost.MenuManager = this.barManager1;
            this.spinEditMaterialCost.Name = "spinEditMaterialCost";
            this.spinEditMaterialCost.Properties.Appearance.Options.UseTextOptions = true;
            this.spinEditMaterialCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditMaterialCost.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.spinEditMaterialCost.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditMaterialCost.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.spinEditMaterialCost.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditMaterialCost.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.spinEditMaterialCost.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditMaterialCost.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditMaterialCost.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditMaterialCost.Properties.Mask.EditMask = "c";
            this.spinEditMaterialCost.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditMaterialCost.Size = new System.Drawing.Size(141, 20);
            this.spinEditMaterialCost.StyleController = this.layoutControl1;
            this.spinEditMaterialCost.TabIndex = 20;
            // 
            // spinEditEquipmentCost
            // 
            this.spinEditEquipmentCost.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditEquipmentCost.Location = new System.Drawing.Point(108, 118);
            this.spinEditEquipmentCost.MenuManager = this.barManager1;
            this.spinEditEquipmentCost.Name = "spinEditEquipmentCost";
            this.spinEditEquipmentCost.Properties.Appearance.Options.UseTextOptions = true;
            this.spinEditEquipmentCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditEquipmentCost.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.spinEditEquipmentCost.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditEquipmentCost.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.spinEditEquipmentCost.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditEquipmentCost.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.spinEditEquipmentCost.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditEquipmentCost.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditEquipmentCost.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditEquipmentCost.Properties.Mask.EditMask = "c";
            this.spinEditEquipmentCost.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditEquipmentCost.Size = new System.Drawing.Size(141, 20);
            this.spinEditEquipmentCost.StyleController = this.layoutControl1;
            this.spinEditEquipmentCost.TabIndex = 21;
            // 
            // spinEditLabourSell
            // 
            this.spinEditLabourSell.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditLabourSell.Location = new System.Drawing.Point(108, 269);
            this.spinEditLabourSell.MenuManager = this.barManager1;
            this.spinEditLabourSell.Name = "spinEditLabourSell";
            this.spinEditLabourSell.Properties.Appearance.Options.UseTextOptions = true;
            this.spinEditLabourSell.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditLabourSell.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.spinEditLabourSell.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditLabourSell.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.spinEditLabourSell.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditLabourSell.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.spinEditLabourSell.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditLabourSell.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditLabourSell.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditLabourSell.Properties.Mask.EditMask = "c";
            this.spinEditLabourSell.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditLabourSell.Size = new System.Drawing.Size(144, 20);
            this.spinEditLabourSell.StyleController = this.layoutControl1;
            this.spinEditLabourSell.TabIndex = 24;
            // 
            // spinEditMaterialSell
            // 
            this.spinEditMaterialSell.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditMaterialSell.Location = new System.Drawing.Point(108, 293);
            this.spinEditMaterialSell.MenuManager = this.barManager1;
            this.spinEditMaterialSell.Name = "spinEditMaterialSell";
            this.spinEditMaterialSell.Properties.Appearance.Options.UseTextOptions = true;
            this.spinEditMaterialSell.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditMaterialSell.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.spinEditMaterialSell.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditMaterialSell.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.spinEditMaterialSell.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditMaterialSell.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.spinEditMaterialSell.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditMaterialSell.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditMaterialSell.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditMaterialSell.Properties.Mask.EditMask = "c";
            this.spinEditMaterialSell.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditMaterialSell.Size = new System.Drawing.Size(144, 20);
            this.spinEditMaterialSell.StyleController = this.layoutControl1;
            this.spinEditMaterialSell.TabIndex = 42;
            // 
            // spinEditEquipmentSell
            // 
            this.spinEditEquipmentSell.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditEquipmentSell.Location = new System.Drawing.Point(108, 317);
            this.spinEditEquipmentSell.MenuManager = this.barManager1;
            this.spinEditEquipmentSell.Name = "spinEditEquipmentSell";
            this.spinEditEquipmentSell.Properties.Appearance.Options.UseTextOptions = true;
            this.spinEditEquipmentSell.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditEquipmentSell.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.spinEditEquipmentSell.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditEquipmentSell.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.spinEditEquipmentSell.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditEquipmentSell.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.spinEditEquipmentSell.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.spinEditEquipmentSell.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditEquipmentSell.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditEquipmentSell.Properties.Mask.EditMask = "c";
            this.spinEditEquipmentSell.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditEquipmentSell.Size = new System.Drawing.Size(144, 20);
            this.spinEditEquipmentSell.StyleController = this.layoutControl1;
            this.spinEditEquipmentSell.TabIndex = 43;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem5,
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.emptySpaceItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(430, 395);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 189);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(410, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowHtmlStringInCaption = true;
            this.layoutControlGroup2.ExpandButtonVisible = true;
            this.layoutControlGroup2.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCostCalculationID,
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem7,
            this.layoutControlItem9});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(410, 189);
            this.layoutControlGroup2.Text = "Team <b>Costs</b>";
            // 
            // ItemForCostCalculationID
            // 
            this.ItemForCostCalculationID.Control = this.gridLookUpEditApplyCostCalculationLevel;
            this.ItemForCostCalculationID.CustomizationFormText = "Cost Calculation:";
            this.ItemForCostCalculationID.Location = new System.Drawing.Point(0, 0);
            this.ItemForCostCalculationID.Name = "ItemForCostCalculationID";
            this.ItemForCostCalculationID.Size = new System.Drawing.Size(386, 24);
            this.ItemForCostCalculationID.Text = "Cost Calculation:";
            this.ItemForCostCalculationID.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.spinEditLabourCost;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(229, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(229, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(229, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "Labour Cost:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(81, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(229, 24);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(157, 96);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.spinEditMaterialCost;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(229, 24);
            this.layoutControlItem2.Text = "Material Cost:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.spinEditEquipmentCost;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(229, 24);
            this.layoutControlItem3.Text = "Equipment Cost:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.spinEditTotalCost;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(229, 24);
            this.layoutControlItem7.Text = "Total Cost:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.checkEditClearSelfBillingInvoice;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(386, 23);
            this.layoutControlItem9.Text = "Remove Self-Billing Invoice:";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(132, 13);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowHtmlStringInCaption = true;
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSellCalculationID,
            this.layoutControlItem4,
            this.emptySpaceItem2,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem8});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 199);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(410, 166);
            this.layoutControlGroup3.Text = "Client <b>Sell</b>";
            // 
            // ItemForSellCalculationID
            // 
            this.ItemForSellCalculationID.Control = this.gridLookUpEditApplySellCalculationLevel;
            this.ItemForSellCalculationID.Location = new System.Drawing.Point(0, 0);
            this.ItemForSellCalculationID.Name = "ItemForSellCalculationID";
            this.ItemForSellCalculationID.Size = new System.Drawing.Size(386, 24);
            this.ItemForSellCalculationID.Text = "Sell Calculation:";
            this.ItemForSellCalculationID.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.spinEditLabourSell;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(232, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(232, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(232, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "Labour Sell:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(81, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(232, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(154, 96);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.spinEditMaterialSell;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(232, 24);
            this.layoutControlItem5.Text = "Material Sell:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.spinEditEquipmentSell;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(232, 24);
            this.layoutControlItem6.Text = "Equipment Sell:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.spinEditTotalSell;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(232, 24);
            this.layoutControlItem8.Text = "Total Sell:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(81, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 365);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(410, 10);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(211, 465);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(96, 22);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(322, 465);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(96, 22);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter
            // 
            this.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter.ClearBeforeFill = true;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = global::WoodPlan5.Properties.Resources.Info_32x32;
            this.pictureEdit1.Location = new System.Drawing.Point(7, 31);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ReadOnly = true;
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Size = new System.Drawing.Size(34, 34);
            this.pictureEdit1.TabIndex = 5;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(48, 32);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(370, 32);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Enter any values to be changed in the boxes present.\r\nNote: Any boxes left untouc" +
    "hed will not be changed on the passed in visits.";
            // 
            // labelControlVisitCount
            // 
            this.labelControlVisitCount.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControlVisitCount.Location = new System.Drawing.Point(311, 35);
            this.labelControlVisitCount.Name = "labelControlVisitCount";
            this.labelControlVisitCount.Size = new System.Drawing.Size(81, 13);
            this.labelControlVisitCount.TabIndex = 7;
            this.labelControlVisitCount.Text = "Visits Selected: 0";
            // 
            // frm_OM_Visit_Block_Edit_Visit_Financial_Info
            // 
            this.ClientSize = new System.Drawing.Size(431, 495);
            this.ControlBox = false;
            this.Controls.Add(this.labelControlVisitCount);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.pictureEdit1);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OM_Visit_Block_Edit_Visit_Financial_Info";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Block Edit Visit Financial Data";
            this.Load += new System.EventHandler(this.frm_OM_Visit_Block_Edit_Visit_Financial_Info_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.pictureEdit1, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.labelControlVisitCount, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditClearSelfBillingInvoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTotalSell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTotalCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditApplySellCalculationLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06134OMJobCalculationLevelDescriptorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditApplyCostCalculationLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditLabourCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMaterialCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditEquipmentCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditLabourSell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMaterialSell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditEquipmentSell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCalculationID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellCalculationID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditApplyCostCalculationLevel;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostCalculationID;
        private System.Windows.Forms.BindingSource sp06134OMJobCalculationLevelDescriptorsBindingSource;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private DataSet_OM_VisitTableAdapters.sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditApplySellCalculationLevel;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellCalculationID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.SpinEdit spinEditLabourCost;
        private DevExpress.XtraEditors.SpinEdit spinEditMaterialCost;
        private DevExpress.XtraEditors.SpinEdit spinEditEquipmentCost;
        private DevExpress.XtraEditors.SpinEdit spinEditLabourSell;
        private DevExpress.XtraEditors.SpinEdit spinEditMaterialSell;
        private DevExpress.XtraEditors.SpinEdit spinEditEquipmentSell;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.SpinEdit spinEditTotalSell;
        private DevExpress.XtraEditors.SpinEdit spinEditTotalCost;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckEdit checkEditClearSelfBillingInvoice;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.LabelControl labelControlVisitCount;
    }
}

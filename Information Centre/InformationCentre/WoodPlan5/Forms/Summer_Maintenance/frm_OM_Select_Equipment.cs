using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Select_Equipment : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;
        bool isRunning = false;

        public string _Mode = "single";  // single or multiple //
        public string _FilteredContractorIDs = "";
        public int intOriginalID = 0;
        public string strOriginalIDs = "";
        public int intSelectedID = 0;
        public string strSelectedIDs = "";
        public string strSelectedDescription1 = "";
        public string strSelectedDescription2 = "";
        public string strSelectedDescription3 = "";
        public int _SelectedCount = 0;
        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection12;

        private DateTime i_dtStart = DateTime.MinValue;
        private DateTime i_dtEnd = DateTime.MaxValue;

        #endregion

        public frm_OM_Select_Equipment()
        {
            InitializeComponent();
        }

        private void frm_OM_Select_Equipment_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 50013700;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp06105_OM_Equipment_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06189_OM_Equipment_Work_LoadTableAdapter.Connection.ConnectionString = strConnectionString;

            i_dtStart = DateTime.Today;
            i_dtEnd = DateTime.Today.AddMonths(12);  // forward a year //
            dateEditFromDate.DateTime = i_dtStart;
            dateEditToDate.DateTime = i_dtEnd;
            barEditItemDateRange.EditValue = PopupContainerEditDateRange_Get_Selected();

            if (!string.IsNullOrWhiteSpace(_FilteredContractorIDs))
            {
                sp06194_OM_Select_Contractor_Filter_ContractorTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06194_OM_Select_Contractor_Filter_ContractorTableAdapter.Fill(dataSet_OM_Contract.sp06194_OM_Select_Contractor_Filter_Contractor, _FilteredContractorIDs);
                gridControl12.ForceInitialize();
                // Add record selection checkboxes to popup grid control //
                selection12 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl12.MainView);
                selection12.CheckMarkColumn.Width = 30;
                selection12.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
                selection12.CheckMarkColumn.VisibleIndex = 0;
                selection12.SelectAll();  // Select all rows //
                beiLabourFilter.EditValue = PopupContainerEditLabourFilter_Get_Selected();
            }
            else
            {
                beiLabourFilter.Enabled = false;
            }

            GridView view = (GridView)gridControl1.MainView;
            LoadData();
            gridControl1.ForceInitialize();

            if (_Mode != "single")
            {
                // Add record selection checkboxes to grid control //
                selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
                selection1.CheckMarkColumn.Width = 30;
                selection1.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
                selection1.CheckMarkColumn.VisibleIndex = 0;

                Array arrayRecords = strOriginalIDs.Split(',');  // Single quotes because char expected for delimeter //
                view.BeginUpdate();
                int intFoundRow = 0;
                foreach (string strElement in arrayRecords)
                {
                    if (strElement == "") break;
                    intFoundRow = view.LocateByValue(0, view.Columns["EquipmentID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
                view.EndUpdate();
            }
            else  // Single selection mode //
            {
                if (intOriginalID != 0)  // Record selected so try to find and highlight //
                {
                    int intFoundRow = view.LocateByValue(0, view.Columns["EquipmentID"], intOriginalID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intFoundRow;
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
            }
            splitContainerControl1.Collapsed = true;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp06105_OM_Equipment_SelectTableAdapter.Fill(dataSet_OM_Contract.sp06105_OM_Equipment_Select, "", _FilteredContractorIDs);            
            view.EndUpdate();
        }

        private void LoadLinkedData()
        {
            GridView view = (GridView)gridControl1.MainView;
            string strSelectedIDs = "";    // Reset any prior values first //
            int intCount = 0;
            if (_Mode != "single")
            {
                if (selection1.SelectedCount > 0)
                    for (int i = 0; i < view.DataRowCount; i++)
                    {
                        if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                        {
                            strSelectedIDs += view.GetRowCellValue(i, "EquipmentID").ToString() + ",";
                            intCount++;
                        }
                    }
            }
            else
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                {
                    strSelectedIDs = view.GetFocusedRowCellValue("EquipmentID").ToString() + ",";
                    intCount = 1;
                }
            }

            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                dataSet_OM_Job.sp06189_OM_Equipment_Work_Load.Clear();
            }
            else
            {
                try
                {
                    sp06189_OM_Equipment_Work_LoadTableAdapter.Fill(dataSet_OM_Job.sp06189_OM_Equipment_Work_Load, strSelectedIDs, i_dtStart, i_dtEnd);
                }
                catch (Exception Ex)
                {
                    XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the Contractor Workload.\n\nTry selecting one or more Contractors again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            view = (GridView)gridControl2.MainView;
            view.ExpandAllGroups();
            gridControl2.MainView.EndUpdate();
        }

        public override void PostViewClick(object sender, EventArgs e, int row)
        {
            GridView view = (GridView)sender;
            switch (view.GridControl.MainView.Name)
            {
                case "gridView1":
                    {
                        if (row == GridControl.InvalidRowHandle || _Mode == "single" || splitContainerControl1.Collapsed) return;
                        LoadLinkedData();
                    }
                    break;
                default:
                    break;
            }
        }

        private void bciShowWorkLoad_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            splitContainerControl1.Collapsed = !bciShowWorkLoad.Checked;
            if (bciShowWorkLoad.Checked) LoadLinkedData();
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Equipment Available";
                    break;
                case "gridView2":
                    message = "No Linked Workloads Available - Select one or more Equipment Records to view Linked Workload";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    if (bciShowWorkLoad.Checked) LoadLinkedData();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region GridView1

        bool internalRowFocusing;

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
        }

        #endregion


        #region GridView2

        private void gridView2_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (_Mode != "single")
            {
                if (string.IsNullOrEmpty(strSelectedIDs))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records by ticking them before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else  // Single selection mode //
            {
                if (intSelectedID == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            if (_Mode != "single")
            {
                strSelectedIDs = "";    // Reset any prior values first //
                strSelectedDescription1 = "";  // Reset any prior values first //
                strSelectedDescription2 = "";  // Reset any prior values first //
                strSelectedDescription3 = "";  // Reset any prior values first //
                string strDescriptor1 = "";
                string strDescriptor2 = "";
                string strDescriptor3 = "";
                if (selection1.SelectedCount <= 0) return;

                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSelectedIDs += Convert.ToString(view.GetRowCellValue(i, "EquipmentID")) + ",";
                        strDescriptor1 = Convert.ToString(view.GetRowCellValue(i, "EquipmentType"));
                        strDescriptor2 = Convert.ToString(view.GetRowCellValue(i, "OwnerName"));
                        strDescriptor3 = Convert.ToString(view.GetRowCellValue(i, "OwnerType"));
                        
                        if (intCount == 0)
                        {
                            strSelectedDescription1 = strDescriptor1;
                            strSelectedDescription2 = strDescriptor2;
                            strSelectedDescription3 = strDescriptor3;
                        }
                        else if (intCount >= 1)
                        {
                            strSelectedDescription1 += ", " + strDescriptor1;
                            strSelectedDescription2 += ", " + strDescriptor2;
                            strSelectedDescription3 += ", " + strDescriptor3;
                        }
                        intCount++;
                    }
                }
                _SelectedCount = intCount;
            }
            else  // Single record selection //
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                {
                    var currentRowView = (DataRowView)sp06105OMEquipmentSelectBindingSource.Current;
                    var currentRow = (DataSet_OM_Contract.sp06105_OM_Equipment_SelectRow)currentRowView.Row;
                    if (currentRow == null) return;
                    intSelectedID = (string.IsNullOrEmpty(currentRow.EquipmentID.ToString()) ? 0 : currentRow.EquipmentID);
                    strSelectedDescription1 = (string.IsNullOrEmpty(currentRow.EquipmentType.ToString()) ? "" : currentRow.EquipmentType);
                    strSelectedDescription2 = (string.IsNullOrEmpty(currentRow.OwnerName.ToString()) ? "" : currentRow.OwnerName);
                    strSelectedDescription3 = (string.IsNullOrEmpty(currentRow.OwnerType.ToString()) ? "" : currentRow.OwnerType);
                }
            }
        }


        #region Date Range Filter Panel

        private void btnDateRangeOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void popupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            string strValue = "";
            if (dateEditFromDate.DateTime == DateTime.MinValue)
            {
                i_dtStart = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtStart = dateEditFromDate.DateTime;
            }
            if (dateEditToDate.DateTime == DateTime.MinValue)
            {
                i_dtEnd = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtEnd = dateEditToDate.DateTime;
            }
            if (dateEditFromDate.DateTime == DateTime.MinValue && dateEditToDate.DateTime == DateTime.MinValue)
            {
                strValue = "No Date Range Filter";
            }
            else
            {
                strValue = (i_dtStart == new DateTime(1900, 1, 1) ? "No Start" : i_dtStart.ToString("dd/MM/yyyy HH:mm")) + " - " + (i_dtEnd == new DateTime(1900, 1, 1) ? "No End" : i_dtEnd.ToString("dd/MM/yyyy HH:mm"));
            }
            return strValue;
        }

        #endregion


        #region Labour Filter Panel

        private void btnLabourFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
            LoadData();
        }

        private void popupContainerEditLabourFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditLabourFilter_Get_Selected();
        }

        private string PopupContainerEditLabourFilter_Get_Selected()
        {
            _FilteredContractorIDs = "";    // Reset any prior values first //
            string str_selected_Labour_Team_descriptions = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl12.MainView;
            if (view.DataRowCount <= 0)
            {
                _FilteredContractorIDs = "";
                return "No Labour Filter";

            }
            else if (selection12.SelectedCount <= 0)
            {
                _FilteredContractorIDs = "";
                return "No Labour Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        _FilteredContractorIDs += Convert.ToInt32(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            str_selected_Labour_Team_descriptions = Convert.ToString(view.GetRowCellValue(i, "Name"));
                        }
                        else if (intCount >= 1)
                        {
                            str_selected_Labour_Team_descriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "Name"));
                        }
                        intCount++;
                    }
                }
            }
            return str_selected_Labour_Team_descriptions;
        }

        #endregion



    }
}


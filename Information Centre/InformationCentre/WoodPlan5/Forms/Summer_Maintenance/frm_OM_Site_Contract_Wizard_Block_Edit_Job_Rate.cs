﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraLayout;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Site_Contract_Wizard_Block_Edit_Job_Rate : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string strJobType = null;
        public string strJobSubType = null;
        public int? intJobType = null;
        public int? intJobSubType = null;
        public int? intVisitCategoryID = null;
        public decimal? decTeamCost = null;
        public decimal? decClientSell = null;
        public DateTime? dtFromDate = null;
        public DateTime? dtToDate = null;
        public string strRemarks = null;

        #endregion

        public frm_OM_Site_Contract_Wizard_Block_Edit_Job_Rate()
        {
            InitializeComponent();
        }

        private void frm_OM_Site_Contract_Wizard_Block_Edit_Job_Rate_Load(object sender, EventArgs e)
        {
            this.FormID = 500249;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //
            
            strConnectionString = this.GlobalSettings.ConnectionString;

            try
            {
                sp06250_OM_Visit_Categories_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06250_OM_Visit_Categories_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06250_OM_Visit_Categories_With_Blank, 0);
            }
            catch (Exception) { }

            JobTypeIDTextEdit.EditValue = null;
            JobTypeTextEdit.EditValue = null;
            JobSubTypeIDTextEdit.EditValue = null;
            JobSubTypeButtonEdit.EditValue = null;
            VisitCategoryIDGridLookUpEdit.EditValue = null;
            TeamCostSpinEdit.EditValue = null;
            ClientSellSpinEdit.EditValue = null;
            FromDateDateEdit.EditValue = null;
            ToDateDateEdit.EditValue = null;
            RemarksMemoEdit.EditValue = null;

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }
        
        private void JobSubTypeButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")
            {
                var fChildForm = new frm_OM_Select_Job_Sub_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm._Mode = "single";
                fChildForm.intOperationManagerJobsOnly = 1;
                fChildForm.intOriginalParentID = (JobTypeIDTextEdit.EditValue == null ? 0 : Convert.ToInt32(JobTypeIDTextEdit.EditValue));
                fChildForm.intOriginalChildID = (JobSubTypeIDTextEdit.EditValue == null ? 0 : Convert.ToInt32(JobSubTypeIDTextEdit.EditValue));
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (fChildForm.intSelectedChildID == fChildForm.intOriginalChildID) return;
                    JobSubTypeIDTextEdit.EditValue = fChildForm.intSelectedChildID;
                    JobTypeIDTextEdit.EditValue = fChildForm.intSelectedParentID;
                    JobTypeTextEdit.EditValue = fChildForm.strSelectedParentDescriptions;
                    JobSubTypeButtonEdit.EditValue = fChildForm.strSelectedChildDescriptions;
                    VisitCategoryIDGridLookUpEdit.EditValue = 0;  // Clear since Visit Category is not relevent on Job Rates (only Visit Generic Rates) //
                    VisitCategoryIDGridLookUpEdit.Properties.ReadOnly = true;
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                JobSubTypeIDTextEdit.EditValue = 0;
                JobTypeIDTextEdit.EditValue = 0;
                JobTypeTextEdit.EditValue = "";
                JobSubTypeButtonEdit.EditValue = "Generic Rate";
                VisitCategoryIDGridLookUpEdit.Properties.ReadOnly = false;
            }
            Validate_VisitCategoryID(VisitCategoryIDGridLookUpEdit);
        }

        private void VisitCategoryIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            Validate_VisitCategoryID(glue);
        }
        private void Validate_VisitCategoryID(GridLookUpEdit glue)
        {
            int? intJobSubType = null;
            if (JobSubTypeIDTextEdit.EditValue != null) intJobSubType = Convert.ToInt32(JobSubTypeIDTextEdit.EditValue);
            
            int? intVisitCategoryID = null;
            if (glue.EditValue != null) intVisitCategoryID = Convert.ToInt32(glue.EditValue);

            if (intJobSubType == null && intVisitCategoryID == null)
            {
                dxErrorProvider1.SetError(VisitCategoryIDGridLookUpEdit, "");
                return;
            }
            else
            {
                if (intJobSubType == null || intJobSubType == 0)
                {

                    if (intVisitCategoryID == null || intVisitCategoryID == 0)
                    {
                        dxErrorProvider1.SetError(VisitCategoryIDGridLookUpEdit, "Select a value.");
                        return;
                    }
                    else
                    {
                        dxErrorProvider1.SetError(VisitCategoryIDGridLookUpEdit, "");
                        return;
                    }
                }
                else
                {
                    dxErrorProvider1.SetError(VisitCategoryIDGridLookUpEdit, "");
                    return;
                }
            }
        }


        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, LayoutControl Layoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                //layoutControl1.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = Layoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        Layoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        Layoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, layoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the screen!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Contract Wizard - Block Edit Job Rates", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (JobTypeTextEdit.EditValue != null) strJobType = JobTypeTextEdit.EditValue.ToString();
            if (JobSubTypeButtonEdit.EditValue != null) strJobSubType = JobSubTypeButtonEdit.EditValue.ToString();
            if (JobTypeIDTextEdit.EditValue != null) intJobType = Convert.ToInt32(JobTypeIDTextEdit.EditValue);
            if (JobSubTypeIDTextEdit.EditValue != null) intJobSubType = Convert.ToInt32(JobSubTypeIDTextEdit.EditValue);
            if (VisitCategoryIDGridLookUpEdit.EditValue != null) intVisitCategoryID = Convert.ToInt32(VisitCategoryIDGridLookUpEdit.EditValue);
            if (TeamCostSpinEdit.EditValue != null) decTeamCost = Convert.ToInt32(TeamCostSpinEdit.EditValue);
            if (ClientSellSpinEdit.EditValue != null) decClientSell = Convert.ToInt32(ClientSellSpinEdit.EditValue);
            if (FromDateDateEdit.EditValue != null) dtFromDate = Convert.ToDateTime(FromDateDateEdit.EditValue);
            if (ToDateDateEdit.EditValue != null) dtToDate = Convert.ToDateTime(ToDateDateEdit.EditValue);
            if (RemarksMemoEdit.EditValue != null) strRemarks = RemarksMemoEdit.EditValue.ToString();
              
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }








    }
}

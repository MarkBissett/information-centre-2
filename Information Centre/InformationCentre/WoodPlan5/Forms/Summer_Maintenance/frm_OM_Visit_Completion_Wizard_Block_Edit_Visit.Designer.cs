﻿namespace WoodPlan5
{
    partial class frm_OM_Visit_Completion_Wizard_Block_Edit_Visit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Visit_Completion_Wizard_Block_Edit_Visit));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.CompletionSheetNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.IssueTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06251OMIssueTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IssueFoundCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ExtraWorkCommentsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ExtraWorkRequiredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ManagerNotesMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ManagerNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NoOnetoSignCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ClientSignaturePathButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.FinishLongitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FinishLatitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StartLongitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StartLatitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.gridLookUpEditExtraWorkTypeID = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06267OMExtraWorkTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkEditRework = new DevExpress.XtraEditors.CheckEdit();
            this.gridLookUpEditVisitStatusID = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06415OMVisitCompletionWizardVisitStatusesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateEditEndDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStartDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartLatitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartLongitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFinishLatitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFinishLongitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNoOnetoSign = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForManagerName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForManagerNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForCompletionSheetNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForIssueFound = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIssueTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRework = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForExtraWorkRequired = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExtraWorkTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExtraWorkComments = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_BlankTableAdapter();
            this.sp06267_OM_Extra_Work_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06267_OM_Extra_Work_Types_With_BlankTableAdapter();
            this.sp06251_OM_Issue_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_VisitTableAdapters.sp06251_OM_Issue_Types_With_BlankTableAdapter();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CompletionSheetNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IssueTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06251OMIssueTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IssueFoundCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExtraWorkCommentsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExtraWorkRequiredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManagerNotesMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManagerNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoOnetoSignCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientSignaturePathButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishLongitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishLatitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLongitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLatitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditExtraWorkTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06267OMExtraWorkTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditRework.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditVisitStatusID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06415OMVisitCompletionWizardVisitStatusesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLongitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishLongitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoOnetoSign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForManagerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForManagerNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompletionSheetNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIssueFound)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIssueTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRework)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExtraWorkRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExtraWorkTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExtraWorkComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(643, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 774);
            this.barDockControlBottom.Size = new System.Drawing.Size(643, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 748);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(643, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 748);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "ID";
            this.gridColumn7.FieldName = "ID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn7.Width = 53;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn4.Width = 53;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "ID";
            this.gridColumn10.FieldName = "ID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn10.Width = 53;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.CompletionSheetNumberTextEdit);
            this.layoutControl1.Controls.Add(this.IssueTypeIDGridLookUpEdit);
            this.layoutControl1.Controls.Add(this.IssueFoundCheckEdit);
            this.layoutControl1.Controls.Add(this.ExtraWorkCommentsMemoEdit);
            this.layoutControl1.Controls.Add(this.ExtraWorkRequiredCheckEdit);
            this.layoutControl1.Controls.Add(this.ManagerNotesMemoEdit);
            this.layoutControl1.Controls.Add(this.ManagerNameTextEdit);
            this.layoutControl1.Controls.Add(this.NoOnetoSignCheckEdit);
            this.layoutControl1.Controls.Add(this.ClientSignaturePathButtonEdit);
            this.layoutControl1.Controls.Add(this.FinishLongitudeTextEdit);
            this.layoutControl1.Controls.Add(this.FinishLatitudeTextEdit);
            this.layoutControl1.Controls.Add(this.StartLongitudeTextEdit);
            this.layoutControl1.Controls.Add(this.StartLatitudeTextEdit);
            this.layoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.layoutControl1.Controls.Add(this.gridLookUpEditExtraWorkTypeID);
            this.layoutControl1.Controls.Add(this.checkEditRework);
            this.layoutControl1.Controls.Add(this.gridLookUpEditVisitStatusID);
            this.layoutControl1.Controls.Add(this.dateEditEndDate);
            this.layoutControl1.Controls.Add(this.dateEditStartDate);
            this.layoutControl1.Location = new System.Drawing.Point(0, 28);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1284, 236, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(642, 715);
            this.layoutControl1.TabIndex = 4;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // CompletionSheetNumberTextEdit
            // 
            this.CompletionSheetNumberTextEdit.Location = new System.Drawing.Point(126, 628);
            this.CompletionSheetNumberTextEdit.MenuManager = this.barManager1;
            this.CompletionSheetNumberTextEdit.Name = "CompletionSheetNumberTextEdit";
            this.CompletionSheetNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CompletionSheetNumberTextEdit, true);
            this.CompletionSheetNumberTextEdit.Size = new System.Drawing.Size(504, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CompletionSheetNumberTextEdit, optionsSpelling1);
            this.CompletionSheetNumberTextEdit.StyleController = this.layoutControl1;
            this.CompletionSheetNumberTextEdit.TabIndex = 71;
            // 
            // IssueTypeIDGridLookUpEdit
            // 
            this.IssueTypeIDGridLookUpEdit.Location = new System.Drawing.Point(138, 404);
            this.IssueTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.IssueTypeIDGridLookUpEdit.Name = "IssueTypeIDGridLookUpEdit";
            this.IssueTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.IssueTypeIDGridLookUpEdit.Properties.DataSource = this.sp06251OMIssueTypesWithBlankBindingSource;
            this.IssueTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.IssueTypeIDGridLookUpEdit.Properties.NullText = "";
            this.IssueTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.IssueTypeIDGridLookUpEdit.Properties.View = this.gridView1;
            this.IssueTypeIDGridLookUpEdit.Size = new System.Drawing.Size(480, 20);
            this.IssueTypeIDGridLookUpEdit.StyleController = this.layoutControl1;
            this.IssueTypeIDGridLookUpEdit.TabIndex = 5;
            // 
            // sp06251OMIssueTypesWithBlankBindingSource
            // 
            this.sp06251OMIssueTypesWithBlankBindingSource.DataMember = "sp06251_OM_Issue_Types_With_Blank";
            this.sp06251OMIssueTypesWithBlankBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn7;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn9, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Issue Types";
            this.gridColumn8.FieldName = "Description";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 220;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Order";
            this.gridColumn9.FieldName = "RecordOrder";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // IssueFoundCheckEdit
            // 
            this.IssueFoundCheckEdit.Location = new System.Drawing.Point(138, 381);
            this.IssueFoundCheckEdit.MenuManager = this.barManager1;
            this.IssueFoundCheckEdit.Name = "IssueFoundCheckEdit";
            this.IssueFoundCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.IssueFoundCheckEdit.Properties.ValueChecked = 1;
            this.IssueFoundCheckEdit.Properties.ValueUnchecked = 0;
            this.IssueFoundCheckEdit.Size = new System.Drawing.Size(100, 19);
            this.IssueFoundCheckEdit.StyleController = this.layoutControl1;
            this.IssueFoundCheckEdit.TabIndex = 5;
            // 
            // ExtraWorkCommentsMemoEdit
            // 
            this.ExtraWorkCommentsMemoEdit.Location = new System.Drawing.Point(138, 552);
            this.ExtraWorkCommentsMemoEdit.MenuManager = this.barManager1;
            this.ExtraWorkCommentsMemoEdit.Name = "ExtraWorkCommentsMemoEdit";
            this.ExtraWorkCommentsMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ExtraWorkCommentsMemoEdit, true);
            this.ExtraWorkCommentsMemoEdit.Size = new System.Drawing.Size(480, 50);
            this.scSpellChecker.SetSpellCheckerOptions(this.ExtraWorkCommentsMemoEdit, optionsSpelling2);
            this.ExtraWorkCommentsMemoEdit.StyleController = this.layoutControl1;
            this.ExtraWorkCommentsMemoEdit.TabIndex = 8;
            // 
            // ExtraWorkRequiredCheckEdit
            // 
            this.ExtraWorkRequiredCheckEdit.Location = new System.Drawing.Point(138, 505);
            this.ExtraWorkRequiredCheckEdit.MenuManager = this.barManager1;
            this.ExtraWorkRequiredCheckEdit.Name = "ExtraWorkRequiredCheckEdit";
            this.ExtraWorkRequiredCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.ExtraWorkRequiredCheckEdit.Properties.ValueChecked = 1;
            this.ExtraWorkRequiredCheckEdit.Properties.ValueUnchecked = 0;
            this.ExtraWorkRequiredCheckEdit.Size = new System.Drawing.Size(100, 19);
            this.ExtraWorkRequiredCheckEdit.StyleController = this.layoutControl1;
            this.ExtraWorkRequiredCheckEdit.TabIndex = 9;
            // 
            // ManagerNotesMemoEdit
            // 
            this.ManagerNotesMemoEdit.Location = new System.Drawing.Point(138, 277);
            this.ManagerNotesMemoEdit.MenuManager = this.barManager1;
            this.ManagerNotesMemoEdit.Name = "ManagerNotesMemoEdit";
            this.ManagerNotesMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ManagerNotesMemoEdit, true);
            this.ManagerNotesMemoEdit.Size = new System.Drawing.Size(480, 46);
            this.scSpellChecker.SetSpellCheckerOptions(this.ManagerNotesMemoEdit, optionsSpelling3);
            this.ManagerNotesMemoEdit.StyleController = this.layoutControl1;
            this.ManagerNotesMemoEdit.TabIndex = 69;
            // 
            // ManagerNameTextEdit
            // 
            this.ManagerNameTextEdit.Location = new System.Drawing.Point(138, 253);
            this.ManagerNameTextEdit.MenuManager = this.barManager1;
            this.ManagerNameTextEdit.Name = "ManagerNameTextEdit";
            this.ManagerNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ManagerNameTextEdit, true);
            this.ManagerNameTextEdit.Size = new System.Drawing.Size(480, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ManagerNameTextEdit, optionsSpelling4);
            this.ManagerNameTextEdit.StyleController = this.layoutControl1;
            this.ManagerNameTextEdit.TabIndex = 68;
            // 
            // NoOnetoSignCheckEdit
            // 
            this.NoOnetoSignCheckEdit.Location = new System.Drawing.Point(138, 230);
            this.NoOnetoSignCheckEdit.MenuManager = this.barManager1;
            this.NoOnetoSignCheckEdit.Name = "NoOnetoSignCheckEdit";
            this.NoOnetoSignCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.NoOnetoSignCheckEdit.Properties.ValueChecked = 1;
            this.NoOnetoSignCheckEdit.Properties.ValueUnchecked = 0;
            this.NoOnetoSignCheckEdit.Size = new System.Drawing.Size(100, 19);
            this.NoOnetoSignCheckEdit.StyleController = this.layoutControl1;
            this.NoOnetoSignCheckEdit.TabIndex = 67;
            this.NoOnetoSignCheckEdit.EditValueChanged += new System.EventHandler(this.NoOnetoSignCheckEdit_EditValueChanged);
            // 
            // ClientSignaturePathButtonEdit
            // 
            this.ClientSignaturePathButtonEdit.Location = new System.Drawing.Point(138, 204);
            this.ClientSignaturePathButtonEdit.MenuManager = this.barManager1;
            this.ClientSignaturePathButtonEdit.Name = "ClientSignaturePathButtonEdit";
            this.ClientSignaturePathButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose File", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleLeft, ((System.Drawing.Image)(resources.GetObject("ClientSignaturePathButtonEdit.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Select File", "select file", null, true)});
            this.ClientSignaturePathButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientSignaturePathButtonEdit.Size = new System.Drawing.Size(480, 22);
            this.ClientSignaturePathButtonEdit.StyleController = this.layoutControl1;
            this.ClientSignaturePathButtonEdit.TabIndex = 18;
            this.ClientSignaturePathButtonEdit.TabStop = false;
            this.ClientSignaturePathButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientSignaturePathButtonEdit_ButtonClick);
            // 
            // FinishLongitudeTextEdit
            // 
            this.FinishLongitudeTextEdit.Location = new System.Drawing.Point(455, 126);
            this.FinishLongitudeTextEdit.MenuManager = this.barManager1;
            this.FinishLongitudeTextEdit.Name = "FinishLongitudeTextEdit";
            this.FinishLongitudeTextEdit.Properties.Mask.EditMask = "n8";
            this.FinishLongitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.FinishLongitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FinishLongitudeTextEdit, true);
            this.FinishLongitudeTextEdit.Size = new System.Drawing.Size(163, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FinishLongitudeTextEdit, optionsSpelling5);
            this.FinishLongitudeTextEdit.StyleController = this.layoutControl1;
            this.FinishLongitudeTextEdit.TabIndex = 17;
            // 
            // FinishLatitudeTextEdit
            // 
            this.FinishLatitudeTextEdit.Location = new System.Drawing.Point(455, 102);
            this.FinishLatitudeTextEdit.MenuManager = this.barManager1;
            this.FinishLatitudeTextEdit.Name = "FinishLatitudeTextEdit";
            this.FinishLatitudeTextEdit.Properties.Mask.EditMask = "n8";
            this.FinishLatitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.FinishLatitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FinishLatitudeTextEdit, true);
            this.FinishLatitudeTextEdit.Size = new System.Drawing.Size(163, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FinishLatitudeTextEdit, optionsSpelling6);
            this.FinishLatitudeTextEdit.StyleController = this.layoutControl1;
            this.FinishLatitudeTextEdit.TabIndex = 16;
            // 
            // StartLongitudeTextEdit
            // 
            this.StartLongitudeTextEdit.Location = new System.Drawing.Point(138, 126);
            this.StartLongitudeTextEdit.MenuManager = this.barManager1;
            this.StartLongitudeTextEdit.Name = "StartLongitudeTextEdit";
            this.StartLongitudeTextEdit.Properties.Mask.EditMask = "n8";
            this.StartLongitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.StartLongitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StartLongitudeTextEdit, true);
            this.StartLongitudeTextEdit.Size = new System.Drawing.Size(169, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StartLongitudeTextEdit, optionsSpelling7);
            this.StartLongitudeTextEdit.StyleController = this.layoutControl1;
            this.StartLongitudeTextEdit.TabIndex = 15;
            // 
            // StartLatitudeTextEdit
            // 
            this.StartLatitudeTextEdit.Location = new System.Drawing.Point(138, 102);
            this.StartLatitudeTextEdit.MenuManager = this.barManager1;
            this.StartLatitudeTextEdit.Name = "StartLatitudeTextEdit";
            this.StartLatitudeTextEdit.Properties.Mask.EditMask = "n8";
            this.StartLatitudeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.StartLatitudeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StartLatitudeTextEdit, true);
            this.StartLatitudeTextEdit.Size = new System.Drawing.Size(169, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StartLatitudeTextEdit, optionsSpelling8);
            this.StartLatitudeTextEdit.StyleController = this.layoutControl1;
            this.StartLatitudeTextEdit.TabIndex = 14;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.Location = new System.Drawing.Point(126, 652);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(504, 51);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling9);
            this.RemarksMemoEdit.StyleController = this.layoutControl1;
            this.RemarksMemoEdit.TabIndex = 9;
            // 
            // gridLookUpEditExtraWorkTypeID
            // 
            this.gridLookUpEditExtraWorkTypeID.EditValue = 2;
            this.gridLookUpEditExtraWorkTypeID.Location = new System.Drawing.Point(138, 528);
            this.gridLookUpEditExtraWorkTypeID.MenuManager = this.barManager1;
            this.gridLookUpEditExtraWorkTypeID.Name = "gridLookUpEditExtraWorkTypeID";
            this.gridLookUpEditExtraWorkTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditExtraWorkTypeID.Properties.DataSource = this.sp06267OMExtraWorkTypesWithBlankBindingSource;
            this.gridLookUpEditExtraWorkTypeID.Properties.DisplayMember = "Description";
            this.gridLookUpEditExtraWorkTypeID.Properties.NullText = "";
            this.gridLookUpEditExtraWorkTypeID.Properties.ValueMember = "ID";
            this.gridLookUpEditExtraWorkTypeID.Properties.View = this.gridView2;
            this.gridLookUpEditExtraWorkTypeID.Size = new System.Drawing.Size(480, 20);
            this.gridLookUpEditExtraWorkTypeID.StyleController = this.layoutControl1;
            this.gridLookUpEditExtraWorkTypeID.TabIndex = 3;
            // 
            // sp06267OMExtraWorkTypesWithBlankBindingSource
            // 
            this.sp06267OMExtraWorkTypesWithBlankBindingSource.DataMember = "sp06267_OM_Extra_Work_Types_With_Blank";
            this.sp06267OMExtraWorkTypesWithBlankBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.gridColumn4;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView2.FormatRules.Add(gridFormatRule1);
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Extra Work Type";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "RecordOrder";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // checkEditRework
            // 
            this.checkEditRework.Location = new System.Drawing.Point(138, 428);
            this.checkEditRework.MenuManager = this.barManager1;
            this.checkEditRework.Name = "checkEditRework";
            this.checkEditRework.Properties.Caption = "[Tick if Yes]";
            this.checkEditRework.Properties.ValueChecked = 1;
            this.checkEditRework.Properties.ValueUnchecked = 0;
            this.checkEditRework.Size = new System.Drawing.Size(100, 19);
            this.checkEditRework.StyleController = this.layoutControl1;
            this.checkEditRework.TabIndex = 8;
            // 
            // gridLookUpEditVisitStatusID
            // 
            this.gridLookUpEditVisitStatusID.EditValue = 2;
            this.gridLookUpEditVisitStatusID.Location = new System.Drawing.Point(126, 36);
            this.gridLookUpEditVisitStatusID.MenuManager = this.barManager1;
            this.gridLookUpEditVisitStatusID.Name = "gridLookUpEditVisitStatusID";
            this.gridLookUpEditVisitStatusID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditVisitStatusID.Properties.DataSource = this.sp06415OMVisitCompletionWizardVisitStatusesWithBlankBindingSource;
            this.gridLookUpEditVisitStatusID.Properties.DisplayMember = "Description";
            this.gridLookUpEditVisitStatusID.Properties.NullText = "";
            this.gridLookUpEditVisitStatusID.Properties.ValueMember = "ID";
            this.gridLookUpEditVisitStatusID.Properties.View = this.gridView5;
            this.gridLookUpEditVisitStatusID.Size = new System.Drawing.Size(504, 20);
            this.gridLookUpEditVisitStatusID.StyleController = this.layoutControl1;
            this.gridLookUpEditVisitStatusID.TabIndex = 2;
            // 
            // sp06415OMVisitCompletionWizardVisitStatusesWithBlankBindingSource
            // 
            this.sp06415OMVisitCompletionWizardVisitStatusesWithBlankBindingSource.DataMember = "sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_Blank";
            this.sp06415OMVisitCompletionWizardVisitStatusesWithBlankBindingSource.DataSource = this.dataSet_OM_Visit;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.gridColumn10;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView5.FormatRules.Add(gridFormatRule2);
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Visit Status";
            this.gridColumn11.FieldName = "Description";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            this.gridColumn11.Width = 220;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Order";
            this.gridColumn12.FieldName = "RecordOrder";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // dateEditEndDate
            // 
            this.dateEditEndDate.EditValue = null;
            this.dateEditEndDate.Location = new System.Drawing.Point(437, 12);
            this.dateEditEndDate.MenuManager = this.barManager1;
            this.dateEditEndDate.Name = "dateEditEndDate";
            this.dateEditEndDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEndDate.Properties.Mask.EditMask = "g";
            this.dateEditEndDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditEndDate.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditEndDate.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateEditEndDate.Size = new System.Drawing.Size(193, 20);
            this.dateEditEndDate.StyleController = this.layoutControl1;
            this.dateEditEndDate.TabIndex = 1;
            // 
            // dateEditStartDate
            // 
            this.dateEditStartDate.EditValue = null;
            this.dateEditStartDate.Location = new System.Drawing.Point(126, 12);
            this.dateEditStartDate.MenuManager = this.barManager1;
            this.dateEditStartDate.Name = "dateEditStartDate";
            this.dateEditStartDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStartDate.Properties.Mask.EditMask = "g";
            this.dateEditStartDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditStartDate.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditStartDate.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateEditStartDate.Size = new System.Drawing.Size(193, 20);
            this.dateEditStartDate.StyleController = this.layoutControl1;
            this.dateEditStartDate.TabIndex = 0;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForStartDate,
            this.ItemForVisitStatusID,
            this.ItemForRemarks,
            this.layoutControlGroup2,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.ItemForCompletionSheetNumber,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.emptySpaceItem7,
            this.emptySpaceItem8,
            this.emptySpaceItem9,
            this.layoutControlGroup5,
            this.layoutControlGroup6,
            this.splitterItem1,
            this.ItemForEndDate});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(642, 715);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // ItemForStartDate
            // 
            this.ItemForStartDate.Control = this.dateEditStartDate;
            this.ItemForStartDate.CustomizationFormText = "Visit Start:";
            this.ItemForStartDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForStartDate.Name = "ItemForStartDate";
            this.ItemForStartDate.Size = new System.Drawing.Size(311, 24);
            this.ItemForStartDate.Text = "Visit Start:";
            this.ItemForStartDate.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForEndDate
            // 
            this.ItemForEndDate.Control = this.dateEditEndDate;
            this.ItemForEndDate.CustomizationFormText = "End Date:";
            this.ItemForEndDate.Location = new System.Drawing.Point(311, 0);
            this.ItemForEndDate.Name = "ItemForEndDate";
            this.ItemForEndDate.Size = new System.Drawing.Size(311, 24);
            this.ItemForEndDate.Text = "Visit End:";
            this.ItemForEndDate.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForVisitStatusID
            // 
            this.ItemForVisitStatusID.Control = this.gridLookUpEditVisitStatusID;
            this.ItemForVisitStatusID.CustomizationFormText = "Visit Status:";
            this.ItemForVisitStatusID.Location = new System.Drawing.Point(0, 24);
            this.ItemForVisitStatusID.Name = "ItemForVisitStatusID";
            this.ItemForVisitStatusID.Size = new System.Drawing.Size(622, 24);
            this.ItemForVisitStatusID.Text = "Visit Status:";
            this.ItemForVisitStatusID.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 640);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(622, 55);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForStartLatitude
            // 
            this.ItemForStartLatitude.Control = this.StartLatitudeTextEdit;
            this.ItemForStartLatitude.Location = new System.Drawing.Point(0, 0);
            this.ItemForStartLatitude.Name = "ItemForStartLatitude";
            this.ItemForStartLatitude.Size = new System.Drawing.Size(287, 24);
            this.ItemForStartLatitude.Text = "Start Latitude:";
            this.ItemForStartLatitude.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForStartLongitude
            // 
            this.ItemForStartLongitude.Control = this.StartLongitudeTextEdit;
            this.ItemForStartLongitude.Location = new System.Drawing.Point(0, 24);
            this.ItemForStartLongitude.Name = "ItemForStartLongitude";
            this.ItemForStartLongitude.Size = new System.Drawing.Size(287, 24);
            this.ItemForStartLongitude.Text = "Start Longitude:";
            this.ItemForStartLongitude.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForFinishLatitude
            // 
            this.ItemForFinishLatitude.Control = this.FinishLatitudeTextEdit;
            this.ItemForFinishLatitude.Location = new System.Drawing.Point(0, 0);
            this.ItemForFinishLatitude.Name = "ItemForFinishLatitude";
            this.ItemForFinishLatitude.Size = new System.Drawing.Size(281, 24);
            this.ItemForFinishLatitude.Text = "Finish Latitude:";
            this.ItemForFinishLatitude.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForFinishLongitude
            // 
            this.ItemForFinishLongitude.Control = this.FinishLongitudeTextEdit;
            this.ItemForFinishLongitude.Location = new System.Drawing.Point(0, 24);
            this.ItemForFinishLongitude.Name = "ItemForFinishLongitude";
            this.ItemForFinishLongitude.Size = new System.Drawing.Size(281, 24);
            this.ItemForFinishLongitude.Text = "Finish Longitude:";
            this.ItemForFinishLongitude.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.ItemForNoOnetoSign,
            this.ItemForManagerName,
            this.ItemForManagerNotes,
            this.emptySpaceItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 160);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(622, 167);
            this.layoutControlGroup2.Text = "Client Signature";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.ClientSignaturePathButtonEdit;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(598, 26);
            this.layoutControlItem1.Text = "Client Signature Path:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForNoOnetoSign
            // 
            this.ItemForNoOnetoSign.Control = this.NoOnetoSignCheckEdit;
            this.ItemForNoOnetoSign.Location = new System.Drawing.Point(0, 26);
            this.ItemForNoOnetoSign.Name = "ItemForNoOnetoSign";
            this.ItemForNoOnetoSign.Size = new System.Drawing.Size(218, 23);
            this.ItemForNoOnetoSign.Text = "No One To Sign:";
            this.ItemForNoOnetoSign.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForManagerName
            // 
            this.ItemForManagerName.Control = this.ManagerNameTextEdit;
            this.ItemForManagerName.Location = new System.Drawing.Point(0, 49);
            this.ItemForManagerName.Name = "ItemForManagerName";
            this.ItemForManagerName.Size = new System.Drawing.Size(598, 24);
            this.ItemForManagerName.Text = "Manager Name:";
            this.ItemForManagerName.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForManagerNotes
            // 
            this.ItemForManagerNotes.Control = this.ManagerNotesMemoEdit;
            this.ItemForManagerNotes.Location = new System.Drawing.Point(0, 73);
            this.ItemForManagerNotes.MaxSize = new System.Drawing.Size(0, 50);
            this.ItemForManagerNotes.MinSize = new System.Drawing.Size(143, 50);
            this.ItemForManagerNotes.Name = "ItemForManagerNotes";
            this.ItemForManagerNotes.Size = new System.Drawing.Size(598, 50);
            this.ItemForManagerNotes.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForManagerNotes.Text = "Manager Notes:";
            this.ItemForManagerNotes.TextSize = new System.Drawing.Size(111, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(218, 26);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(380, 23);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 150);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(622, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(622, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForCompletionSheetNumber
            // 
            this.ItemForCompletionSheetNumber.Control = this.CompletionSheetNumberTextEdit;
            this.ItemForCompletionSheetNumber.Location = new System.Drawing.Point(0, 616);
            this.ItemForCompletionSheetNumber.Name = "ItemForCompletionSheetNumber";
            this.ItemForCompletionSheetNumber.Size = new System.Drawing.Size(622, 24);
            this.ItemForCompletionSheetNumber.Text = "Completion Sheet #:";
            this.ItemForCompletionSheetNumber.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForIssueFound,
            this.ItemForIssueTypeID,
            this.ItemForRework,
            this.emptySpaceItem2,
            this.emptySpaceItem5});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 337);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(622, 114);
            this.layoutControlGroup3.Text = "Issue Found";
            // 
            // ItemForIssueFound
            // 
            this.ItemForIssueFound.Control = this.IssueFoundCheckEdit;
            this.ItemForIssueFound.Location = new System.Drawing.Point(0, 0);
            this.ItemForIssueFound.Name = "ItemForIssueFound";
            this.ItemForIssueFound.Size = new System.Drawing.Size(218, 23);
            this.ItemForIssueFound.Text = "Issue Found:";
            this.ItemForIssueFound.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForIssueTypeID
            // 
            this.ItemForIssueTypeID.Control = this.IssueTypeIDGridLookUpEdit;
            this.ItemForIssueTypeID.Location = new System.Drawing.Point(0, 23);
            this.ItemForIssueTypeID.Name = "ItemForIssueTypeID";
            this.ItemForIssueTypeID.Size = new System.Drawing.Size(598, 24);
            this.ItemForIssueTypeID.Text = "Issue Type:";
            this.ItemForIssueTypeID.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForRework
            // 
            this.ItemForRework.Control = this.checkEditRework;
            this.ItemForRework.CustomizationFormText = "Rework:";
            this.ItemForRework.Location = new System.Drawing.Point(0, 47);
            this.ItemForRework.Name = "ItemForRework";
            this.ItemForRework.Size = new System.Drawing.Size(218, 23);
            this.ItemForRework.Text = "Rework:";
            this.ItemForRework.TextSize = new System.Drawing.Size(111, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(218, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(380, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(218, 47);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(380, 23);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForExtraWorkRequired,
            this.ItemForExtraWorkTypeID,
            this.ItemForExtraWorkComments,
            this.emptySpaceItem6});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 461);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(622, 145);
            this.layoutControlGroup4.Text = "Extra Work";
            // 
            // ItemForExtraWorkRequired
            // 
            this.ItemForExtraWorkRequired.Control = this.ExtraWorkRequiredCheckEdit;
            this.ItemForExtraWorkRequired.Location = new System.Drawing.Point(0, 0);
            this.ItemForExtraWorkRequired.Name = "ItemForExtraWorkRequired";
            this.ItemForExtraWorkRequired.Size = new System.Drawing.Size(218, 23);
            this.ItemForExtraWorkRequired.Text = "Extra Work Required:";
            this.ItemForExtraWorkRequired.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForExtraWorkTypeID
            // 
            this.ItemForExtraWorkTypeID.Control = this.gridLookUpEditExtraWorkTypeID;
            this.ItemForExtraWorkTypeID.Location = new System.Drawing.Point(0, 23);
            this.ItemForExtraWorkTypeID.Name = "ItemForExtraWorkTypeID";
            this.ItemForExtraWorkTypeID.Size = new System.Drawing.Size(598, 24);
            this.ItemForExtraWorkTypeID.Text = "Extra Work Type:";
            this.ItemForExtraWorkTypeID.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForExtraWorkComments
            // 
            this.ItemForExtraWorkComments.Control = this.ExtraWorkCommentsMemoEdit;
            this.ItemForExtraWorkComments.Location = new System.Drawing.Point(0, 47);
            this.ItemForExtraWorkComments.MaxSize = new System.Drawing.Size(0, 54);
            this.ItemForExtraWorkComments.MinSize = new System.Drawing.Size(128, 54);
            this.ItemForExtraWorkComments.Name = "ItemForExtraWorkComments";
            this.ItemForExtraWorkComments.Size = new System.Drawing.Size(598, 54);
            this.ItemForExtraWorkComments.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForExtraWorkComments.Text = "Extra Work Comments:";
            this.ItemForExtraWorkComments.TextSize = new System.Drawing.Size(111, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(218, 0);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(380, 23);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 327);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(622, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 451);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(622, 10);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 606);
            this.emptySpaceItem9.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem9.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(622, 10);
            this.emptySpaceItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(423, 744);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(96, 22);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(534, 744);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(96, 22);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_BlankTableAdapter
            // 
            this.sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06267_OM_Extra_Work_Types_With_BlankTableAdapter
            // 
            this.sp06267_OM_Extra_Work_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06251_OM_Issue_Types_With_BlankTableAdapter
            // 
            this.sp06251_OM_Issue_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForStartLatitude,
            this.ItemForStartLongitude});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 58);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(311, 92);
            this.layoutControlGroup5.Text = "Location - start";
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForFinishLongitude,
            this.ItemForFinishLatitude});
            this.layoutControlGroup6.Location = new System.Drawing.Point(317, 58);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(305, 92);
            this.layoutControlGroup6.Text = "Location - Finish";
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.Location = new System.Drawing.Point(311, 58);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(6, 92);
            // 
            // frm_OM_Visit_Completion_Wizard_Block_Edit_Visit
            // 
            this.ClientSize = new System.Drawing.Size(643, 774);
            this.ControlBox = false;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OM_Visit_Completion_Wizard_Block_Edit_Visit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Visit Completion Wizard - Block Edit Visits";
            this.Load += new System.EventHandler(this.frm_OM_Visit_Completion_Wizard_Block_Edit_Visit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CompletionSheetNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IssueTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06251OMIssueTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IssueFoundCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExtraWorkCommentsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExtraWorkRequiredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManagerNotesMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManagerNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoOnetoSignCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientSignaturePathButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishLongitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinishLatitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLongitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartLatitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditExtraWorkTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06267OMExtraWorkTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditRework.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditVisitStatusID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06415OMVisitCompletionWizardVisitStatusesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartLongitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinishLongitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoOnetoSign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForManagerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForManagerNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompletionSheetNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIssueFound)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIssueTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRework)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExtraWorkRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExtraWorkTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExtraWorkComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.DateEdit dateEditStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartDate;
        private DevExpress.XtraEditors.DateEdit dateEditEndDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndDate;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditVisitStatusID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitStatusID;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private DevExpress.XtraEditors.CheckEdit checkEditRework;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRework;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditExtraWorkTypeID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExtraWorkTypeID;
        private System.Windows.Forms.BindingSource sp06415OMVisitCompletionWizardVisitStatusesWithBlankBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_BlankTableAdapter sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_BlankTableAdapter;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit StartLatitudeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartLatitude;
        private DevExpress.XtraEditors.TextEdit StartLongitudeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartLongitude;
        private DevExpress.XtraEditors.TextEdit FinishLatitudeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinishLatitude;
        private DevExpress.XtraEditors.TextEdit FinishLongitudeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinishLongitude;
        private DevExpress.XtraEditors.ButtonEdit ClientSignaturePathButtonEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.CheckEdit NoOnetoSignCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNoOnetoSign;
        private DevExpress.XtraEditors.TextEdit ManagerNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForManagerName;
        private DevExpress.XtraEditors.MemoEdit ManagerNotesMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForManagerNotes;
        private DevExpress.XtraEditors.CheckEdit ExtraWorkRequiredCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExtraWorkRequired;
        private System.Windows.Forms.BindingSource sp06267OMExtraWorkTypesWithBlankBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06267_OM_Extra_Work_Types_With_BlankTableAdapter sp06267_OM_Extra_Work_Types_With_BlankTableAdapter;
        private DevExpress.XtraEditors.MemoEdit ExtraWorkCommentsMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExtraWorkComments;
        private DevExpress.XtraEditors.CheckEdit IssueFoundCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIssueFound;
        private DevExpress.XtraEditors.GridLookUpEdit IssueTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIssueTypeID;
        private System.Windows.Forms.BindingSource sp06251OMIssueTypesWithBlankBindingSource;
        private DataSet_OM_VisitTableAdapters.sp06251_OM_Issue_Types_With_BlankTableAdapter sp06251_OM_Issue_Types_With_BlankTableAdapter;
        private DevExpress.XtraEditors.TextEdit CompletionSheetNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCompletionSheetNumber;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
    }
}

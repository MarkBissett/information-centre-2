namespace WoodPlan5
{
    partial class frm_OM_Tender_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Tender_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions10 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject37 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject38 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject39 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject40 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions11 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject41 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject42 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject43 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject44 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions12 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject45 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject46 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject47 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject48 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions13 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject49 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject50 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject51 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject52 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions14 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject53 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject54 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject55 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject56 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions15 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject57 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject58 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject59 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject60 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling22 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling23 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling24 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling25 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling26 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling27 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling28 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions16 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject61 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject62 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject63 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject64 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions17 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject65 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject66 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject67 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject68 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions18 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject69 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject70 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject71 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject72 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling29 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions19 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject73 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject74 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject75 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject76 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions20 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject77 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject78 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject79 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject80 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions21 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject81 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject82 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject83 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject84 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling30 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling31 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions22 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject85 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject86 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject87 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject88 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions23 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject89 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject90 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject91 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject92 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling32 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions24 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject93 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject94 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject95 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject96 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions25 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject97 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject98 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject99 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject100 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling33 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling34 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions26 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject101 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject102 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject103 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject104 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions27 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject105 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject106 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject107 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject108 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling35 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling36 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions28 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject109 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject110 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject111 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject112 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions29 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject113 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject114 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject115 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject116 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions30 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject117 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject118 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject119 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject120 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling37 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling38 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling39 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions31 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject121 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject122 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject123 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject124 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling40 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling41 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling42 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling43 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling44 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule4 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue4 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling45 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions32 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject125 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject126 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject127 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject128 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions33 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject129 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject130 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject131 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject132 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling46 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling47 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLinkedDocuments = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.ProposedTotalCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.sp06498OMTenderEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Tender = new WoodPlan5.DataSet_OM_Tender();
            this.ProposedEquipmentCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ProposedMaterialCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ProposedLabourCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ClientContactEmailHyperLinkEdit = new DevExpress.XtraEditors.HyperLinkEdit();
            this.ClientContactMobileTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientContactTelephoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.labelControlWarning = new DevExpress.XtraEditors.LabelControl();
            this.RAMSCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ListedBuildingStatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01053CoreListedBuildingStatusesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Common_Functionality = new WoodPlan5.DataSet_Common_Functionality();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ConservationStatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01052CoreConservationStatusesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TPONumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PlanningProceedDateSetTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PlanningProceedStaffNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PlanningProceedStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TenderTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06525OMTenderTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ddBtnSentToCM = new DevExpress.XtraEditors.DropDownButton();
            this.pmSendQuoteToCM = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiSendQuoteToCM = new DevExpress.XtraBars.BarButtonItem();
            this.bbiNoQuoteRequired = new DevExpress.XtraBars.BarButtonItem();
            this.KAMQuoteRejectionReasonIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ddBtnKAMAuthorised = new DevExpress.XtraEditors.DropDownButton();
            this.pmKAMResponse = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiKAMAccepted = new DevExpress.XtraBars.BarButtonItem();
            this.bbiKAMRejected = new DevExpress.XtraBars.BarButtonItem();
            this.btnCancelTender = new DevExpress.XtraEditors.SimpleButton();
            this.JobCountTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitCountTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.ddBtnClientResponse = new DevExpress.XtraEditors.DropDownButton();
            this.pmClientResponse = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiClientAccepted = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClientRejected = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClientOnHold = new DevExpress.XtraBars.BarButtonItem();
            this.btnTenderClosed = new DevExpress.XtraEditors.SimpleButton();
            this.btnCreateVisits = new DevExpress.XtraEditors.SimpleButton();
            this.btnReturnedFromCM = new DevExpress.XtraEditors.SimpleButton();
            this.ClientQuoteSpreadsheetExtractFileButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ContractorNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.LinkedToPersonTypeButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.StatusIssueIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StatusIsssueButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.btnSendToClient = new DevExpress.XtraEditors.SimpleButton();
            this.SubmittedToCMDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ActualTotalCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ActualEquipmentCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ActualMaterialCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ActualLabourCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.AcceptedTotalSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.AcceptedEquipmentSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.AcceptedMaterialSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.QuotedTotalSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.AcceptedLabourSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.QuotedEquipmentSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.QuotedMaterialSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.NoClientPOAuthorisedByDirectorButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.NoClientPOEmailedToDirectorCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.NoClientPOAuthorisedByDirectorIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NoClientPOCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.RevisionNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TenderProactiveDaysToReturnQuoteTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TenderReactiveDaysToReturnQuoteTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SectorTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.GCCompanyNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LocalAuthorityWebSiteButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.LocalAuthorityTelephoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ContactPersonTitleTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ContactPersonPositionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteCodeButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.PlanningAuthorityOkToProceedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.PlanningAuthoritySubmittedDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.PlanningAuthorityNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TreeProtectedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.TPOCheckedDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.PlanningAuthorityButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.PlanningAuthorityIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TPORequiredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ClientCommentsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.CMCommentsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ClientPONumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RequiredWorkCompletedDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.QuoteAcceptedByClientDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.QuoteSubmittedToClientDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.CMInitialAttendanceDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.SiteLocationYTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteLocationXTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SitePostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteAddressMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.SiteNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.TenderGroupIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TenderGroupDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.JobTypeDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.JobSubTypeDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.WorkSubTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CMNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.CMTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.KAMTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.KAMNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.CreatedByStaffNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientContactIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ContactPersonNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.CreatedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TenderDescriptionMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.StatusIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TenderStatusButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ClientReferenceNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ContractDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OurInternalCommentsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ReactiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.QuotedLabourSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.QuoteNotRequiredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ReturnToClientByDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.RequestReceivedDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.PlanningAuthorityOutcomeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06503OMTenderPlanningOutcomesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenderIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.ClientNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.KAMQuoteRejectionReasonTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.KAMQuoteRejectionRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ItemForClientContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientContactID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCM = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkSubTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTenderGroupID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteLocationX = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteLocationY = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPlanningAuthorityID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForKAM = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSectorType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTenderReactiveDaysToReturnQuote = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTenderProactiveDaysToReturnQuote = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatusIssueID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNoClientPOEmailedToDirector = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNoClientPOAuthorisedByDirectorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForKAMQuoteRejectionReasonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPlanningProceedStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.sp06502OMTenderQuoteRejectionReasonsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp06498_OM_Tender_EditTableAdapter = new WoodPlan5.DataSet_OM_TenderTableAdapters.sp06498_OM_Tender_EditTableAdapter();
            this.sp06502_OM_Tender_Quote_Rejection_Reasons_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_TenderTableAdapters.sp06502_OM_Tender_Quote_Rejection_Reasons_With_BlankTableAdapter();
            this.sp06503_OM_Tender_Planning_Outcomes_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_TenderTableAdapters.sp06503_OM_Tender_Planning_Outcomes_With_BlankTableAdapter();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            this.sp06525_OM_Tender_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_TenderTableAdapters.sp06525_OM_Tender_Types_With_BlankTableAdapter();
            this.sp01052_Core_Conservation_Statuses_With_BlankTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01052_Core_Conservation_Statuses_With_BlankTableAdapter();
            this.sp01053_Core_Listed_Building_Statuses_With_BlankTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01053_Core_Listed_Building_Statuses_With_BlankTableAdapter();
            this.autoGeneratedGroup0 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForRevisionNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTenderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.item2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.item3 = new DevExpress.XtraLayout.SplitterItem();
            this.item6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.item17 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForQuoteNotRequired = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCMReturnedQuoteBtn = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSentToClient = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreateVisitsBtn = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTenderCompletedBtn = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.ItemForSubmittedToCMDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientResponseBtn = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForProgressBar = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitCount = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobCount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForKAMQuoteRejectionReason = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForQuoteAcceptedByClientDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForQuoteSubmittedToClientDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTenderCreatedBtn = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCancelTenderBtn = new DevExpress.XtraLayout.LayoutControlItem();
            this.item0 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForTenderStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatusIsssue = new DevExpress.XtraLayout.LayoutControlItem();
            this.item1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.item5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.item7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.item8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.item9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.item10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.item11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.item12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup15 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForQuotedLabourSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForQuotedMaterialSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForQuotedEquipmentSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForQuotedTotalSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.item4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForAcceptedLabourSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAcceptedMaterialSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAcceptedEquipmentSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAcceptedTotalSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForProposedLabourCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForProposedMaterialCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForProposedEquipmentCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForProposedTotalCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualLabourCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualMaterialCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualEquipmentCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualTotalCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobSubTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCMName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItmeForKAMName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTenderDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTenderGroupDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTenderTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByStaffName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGCCompanyName = new DevExpress.XtraLayout.LayoutControlItem();
            this.item13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.item14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.item15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.item18 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.item19 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSiteName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSitePostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientReferenceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNoClientPO = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNoClientPOAuthorisedByDirector = new DevExpress.XtraLayout.LayoutControlItem();
            this.item16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForClientPONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContactPersonName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContactPersonPosition = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientContactTelephone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientContactMobile = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContactPersonTitle = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientContactEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReactive = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRAMS = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCMInitialAttendanceDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRequiredWorkCompletedDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRequestReceivedDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientQuoteSpreadsheetExtractFile = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTPORequired = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPlanningAuthority = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForPlanningAuthorityNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForPlanningProceedDateSet = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPlanningProceedStaffName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForConservationStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTPONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForListedBuildingStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPlanningAuthorityOutcomeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLocalAuthorityWebSite = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPlanningAuthoritySubmittedDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPlanningAuthorityOkToProceed = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTreeProtected = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTPOCheckedDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLocalAuthorityTelephone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOurInternalComments = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCMComments = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientComments = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForKAMQuoteRejectionRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.autoGeneratedGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProposedTotalCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06498OMTenderEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Tender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProposedEquipmentCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProposedMaterialCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProposedLabourCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContactEmailHyperLinkEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContactMobileTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContactTelephoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RAMSCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ListedBuildingStatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01053CoreListedBuildingStatusesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConservationStatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01052CoreConservationStatusesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TPONumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningProceedDateSetTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningProceedStaffNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningProceedStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06525OMTenderTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmSendQuoteToCM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KAMQuoteRejectionReasonIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmKAMResponse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobCountTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitCountTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmClientResponse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientQuoteSpreadsheetExtractFileButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractorNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIssueIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIsssueButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubmittedToCMDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubmittedToCMDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualTotalCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualEquipmentCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualMaterialCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualLabourCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcceptedTotalSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcceptedEquipmentSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcceptedMaterialSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuotedTotalSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcceptedLabourSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuotedEquipmentSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuotedMaterialSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoClientPOAuthorisedByDirectorButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoClientPOEmailedToDirectorCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoClientPOAuthorisedByDirectorIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoClientPOCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RevisionNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderProactiveDaysToReturnQuoteTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderReactiveDaysToReturnQuoteTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SectorTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GCCompanyNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocalAuthorityWebSiteButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocalAuthorityTelephoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactPersonTitleTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactPersonPositionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteCodeButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningAuthorityOkToProceedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningAuthoritySubmittedDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningAuthoritySubmittedDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningAuthorityNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeProtectedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TPOCheckedDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TPOCheckedDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningAuthorityButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningAuthorityIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TPORequiredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientCommentsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CMCommentsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequiredWorkCompletedDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequiredWorkCompletedDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteAcceptedByClientDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteAcceptedByClientDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteSubmittedToClientDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteSubmittedToClientDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CMInitialAttendanceDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CMInitialAttendanceDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteLocationYTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteLocationXTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SitePostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderGroupIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderGroupDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkSubTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CMNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CMTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KAMTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KAMNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContactIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactPersonNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderDescriptionMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderStatusButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientReferenceNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OurInternalCommentsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuotedLabourSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteNotRequiredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReturnToClientByDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReturnToClientByDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequestReceivedDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequestReceivedDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningAuthorityOutcomeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06503OMTenderPlanningOutcomesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KAMQuoteRejectionReasonTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KAMQuoteRejectionRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContactID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkSubTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderGroupID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteLocationX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteLocationY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningAuthorityID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKAM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSectorType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderReactiveDaysToReturnQuote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderProactiveDaysToReturnQuote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusIssueID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoClientPOEmailedToDirector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoClientPOAuthorisedByDirectorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKAMQuoteRejectionReasonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningProceedStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06502OMTenderQuoteRejectionReasonsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.autoGeneratedGroup0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRevisionNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuoteNotRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCMReturnedQuoteBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSentToClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreateVisitsBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderCompletedBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubmittedToCMDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientResponseBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKAMQuoteRejectionReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuoteAcceptedByClientDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuoteSubmittedToClientDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderCreatedBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCancelTenderBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusIsssue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuotedLabourSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuotedMaterialSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuotedEquipmentSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuotedTotalSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAcceptedLabourSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAcceptedMaterialSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAcceptedEquipmentSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAcceptedTotalSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProposedLabourCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProposedMaterialCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProposedEquipmentCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProposedTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualLabourCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualMaterialCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualEquipmentCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCMName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItmeForKAMName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderGroupDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGCCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSitePostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientReferenceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoClientPO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoClientPOAuthorisedByDirector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactPersonName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactPersonPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContactTelephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContactMobile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactPersonTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContactEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRAMS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCMInitialAttendanceDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRequiredWorkCompletedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRequestReceivedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientQuoteSpreadsheetExtractFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTPORequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningAuthority)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningAuthorityNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningProceedDateSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningProceedStaffName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForConservationStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForListedBuildingStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningAuthorityOutcomeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLocalAuthorityWebSite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningAuthoritySubmittedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningAuthorityOkToProceed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeProtected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTPOCheckedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLocalAuthorityTelephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOurInternalComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCMComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKAMQuoteRejectionRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.autoGeneratedGroup1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(1361, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 680);
            this.barDockControlBottom.Size = new System.Drawing.Size(1361, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 654);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1361, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 654);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Images = this.imageCollection2;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiClientAccepted,
            this.bbiClientRejected,
            this.bbiClientOnHold,
            this.bbiKAMAccepted,
            this.bbiKAMRejected,
            this.bbiSendQuoteToCM,
            this.bbiNoQuoteRequired});
            this.barManager1.MaxItemId = 37;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "ID";
            this.gridColumn7.FieldName = "ID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 53;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 53;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 53;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.Width = 53;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiLinkedDocuments});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLinkedDocuments)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiLinkedDocuments
            // 
            this.bbiLinkedDocuments.Caption = "Linked Documents";
            this.bbiLinkedDocuments.Id = 15;
            this.bbiLinkedDocuments.ImageOptions.Image = global::WoodPlan5.Properties.Resources.linked_documents_16_16;
            this.bbiLinkedDocuments.Name = "bbiLinkedDocuments";
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Linked Documents - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to open the Linked Documents Manager, filtered on the current data entry" +
    " record.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiLinkedDocuments.SuperTip = superToolTip3;
            this.bbiLinkedDocuments.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLinkedDocuments_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Form Mode - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.barStaticItemFormMode.SuperTip = superToolTip4;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(1361, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 680);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(1361, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 654);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1361, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 654);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("apply_16x16.png", "images/actions/apply_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "apply_16x16.png");
            this.imageCollection1.InsertGalleryImage("cancel_16x16.png", "images/actions/cancel_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/cancel_16x16.png"), 6);
            this.imageCollection1.Images.SetKeyName(6, "cancel_16x16.png");
            this.imageCollection1.Images.SetKeyName(7, "Client_Sent.png");
            this.imageCollection1.InsertGalleryImage("deletegroupheader_16x16.png", "images/reports/deletegroupheader_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/reports/deletegroupheader_16x16.png"), 8);
            this.imageCollection1.Images.SetKeyName(8, "deletegroupheader_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.ProposedTotalCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ProposedEquipmentCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ProposedMaterialCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ProposedLabourCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientContactEmailHyperLinkEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientContactMobileTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientContactTelephoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.labelControlWarning);
            this.dataLayoutControl1.Controls.Add(this.RAMSCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ListedBuildingStatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ConservationStatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.TPONumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PlanningProceedDateSetTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PlanningProceedStaffNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PlanningProceedStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TenderTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ddBtnSentToCM);
            this.dataLayoutControl1.Controls.Add(this.KAMQuoteRejectionReasonIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ddBtnKAMAuthorised);
            this.dataLayoutControl1.Controls.Add(this.btnCancelTender);
            this.dataLayoutControl1.Controls.Add(this.JobCountTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitCountTextEdit);
            this.dataLayoutControl1.Controls.Add(this.progressBarControl1);
            this.dataLayoutControl1.Controls.Add(this.ddBtnClientResponse);
            this.dataLayoutControl1.Controls.Add(this.btnTenderClosed);
            this.dataLayoutControl1.Controls.Add(this.btnCreateVisits);
            this.dataLayoutControl1.Controls.Add(this.btnReturnedFromCM);
            this.dataLayoutControl1.Controls.Add(this.ClientQuoteSpreadsheetExtractFileButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractorNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToPersonTypeButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.StatusIssueIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StatusIsssueButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.btnSendToClient);
            this.dataLayoutControl1.Controls.Add(this.SubmittedToCMDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualTotalCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualEquipmentCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualMaterialCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualLabourCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.AcceptedTotalSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.AcceptedEquipmentSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.AcceptedMaterialSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.QuotedTotalSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.AcceptedLabourSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.QuotedEquipmentSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.QuotedMaterialSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.NoClientPOAuthorisedByDirectorButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.NoClientPOEmailedToDirectorCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.NoClientPOAuthorisedByDirectorIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NoClientPOCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.RevisionNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TenderProactiveDaysToReturnQuoteTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TenderReactiveDaysToReturnQuoteTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SectorTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.GCCompanyNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LocalAuthorityWebSiteButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.LocalAuthorityTelephoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactPersonTitleTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactPersonPositionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteCodeButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.PlanningAuthorityOkToProceedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.PlanningAuthoritySubmittedDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.PlanningAuthorityNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TreeProtectedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.TPOCheckedDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.PlanningAuthorityButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.PlanningAuthorityIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TPORequiredCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientCommentsMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.CMCommentsMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientPONumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RequiredWorkCompletedDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.QuoteAcceptedByClientDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.QuoteSubmittedToClientDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.CMInitialAttendanceDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteLocationYTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteLocationXTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SitePostcodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddressMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.TenderGroupIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TenderGroupDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.JobTypeDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.JobSubTypeDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.WorkSubTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CMNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.CMTextEdit);
            this.dataLayoutControl1.Controls.Add(this.KAMTextEdit);
            this.dataLayoutControl1.Controls.Add(this.KAMNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientContactIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ContactPersonNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TenderDescriptionMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.StatusIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TenderStatusButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientReferenceNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteContractIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientContractIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.OurInternalCommentsMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ReactiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.QuotedLabourSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.QuoteNotRequiredCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ReturnToClientByDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.RequestReceivedDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.PlanningAuthorityOutcomeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.TenderIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.ClientNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.KAMQuoteRejectionReasonTextEdit);
            this.dataLayoutControl1.Controls.Add(this.KAMQuoteRejectionRemarksMemoEdit);
            this.dataLayoutControl1.DataSource = this.sp06498OMTenderEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientContractID,
            this.ItemForSiteContractID,
            this.ItemForStatusID,
            this.ItemForCreatedByStaffID,
            this.ItemForClientContactID,
            this.ItemForCM,
            this.ItemForWorkSubTypeID,
            this.ItemForJobTypeID,
            this.ItemForTenderGroupID,
            this.ItemForPlanningAuthorityID,
            this.ItemForKAM,
            this.ItemForSectorType,
            this.ItemForTenderReactiveDaysToReturnQuote,
            this.ItemForTenderProactiveDaysToReturnQuote,
            this.ItemForStatusIssueID,
            this.ItemForNoClientPOEmailedToDirector,
            this.ItemForNoClientPOAuthorisedByDirectorID,
            this.ItemForKAMQuoteRejectionReasonID,
            this.ItemForPlanningProceedStaffID,
            this.autoGeneratedGroup1});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(559, 259, 509, 607);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1361, 654);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ProposedTotalCostSpinEdit
            // 
            this.ProposedTotalCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ProposedTotalCost", true));
            this.ProposedTotalCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ProposedTotalCostSpinEdit.Location = new System.Drawing.Point(1089, 317);
            this.ProposedTotalCostSpinEdit.MenuManager = this.barManager1;
            this.ProposedTotalCostSpinEdit.Name = "ProposedTotalCostSpinEdit";
            this.ProposedTotalCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ProposedTotalCostSpinEdit.Properties.Mask.EditMask = "c";
            this.ProposedTotalCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ProposedTotalCostSpinEdit.Properties.ReadOnly = true;
            this.ProposedTotalCostSpinEdit.Size = new System.Drawing.Size(84, 20);
            this.ProposedTotalCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.ProposedTotalCostSpinEdit.TabIndex = 17;
            // 
            // sp06498OMTenderEditBindingSource
            // 
            this.sp06498OMTenderEditBindingSource.DataMember = "sp06498_OM_Tender_Edit";
            this.sp06498OMTenderEditBindingSource.DataSource = this.dataSet_OM_Tender;
            // 
            // dataSet_OM_Tender
            // 
            this.dataSet_OM_Tender.DataSetName = "DataSet_OM_Tender";
            this.dataSet_OM_Tender.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ProposedEquipmentCostSpinEdit
            // 
            this.ProposedEquipmentCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ProposedEquipmentCost", true));
            this.ProposedEquipmentCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ProposedEquipmentCostSpinEdit.Location = new System.Drawing.Point(1089, 293);
            this.ProposedEquipmentCostSpinEdit.MenuManager = this.barManager1;
            this.ProposedEquipmentCostSpinEdit.Name = "ProposedEquipmentCostSpinEdit";
            this.ProposedEquipmentCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ProposedEquipmentCostSpinEdit.Properties.Mask.EditMask = "c";
            this.ProposedEquipmentCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ProposedEquipmentCostSpinEdit.Size = new System.Drawing.Size(84, 20);
            this.ProposedEquipmentCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.ProposedEquipmentCostSpinEdit.TabIndex = 17;
            this.ProposedEquipmentCostSpinEdit.EditValueChanged += new System.EventHandler(this.ProposedEquipmentCostSpinEdit_EditValueChanged);
            this.ProposedEquipmentCostSpinEdit.Validated += new System.EventHandler(this.ProposedEquipmentCostSpinEdit_Validated);
            // 
            // ProposedMaterialCostSpinEdit
            // 
            this.ProposedMaterialCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ProposedMaterialCost", true));
            this.ProposedMaterialCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ProposedMaterialCostSpinEdit.Location = new System.Drawing.Point(1089, 269);
            this.ProposedMaterialCostSpinEdit.MenuManager = this.barManager1;
            this.ProposedMaterialCostSpinEdit.Name = "ProposedMaterialCostSpinEdit";
            this.ProposedMaterialCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ProposedMaterialCostSpinEdit.Properties.Mask.EditMask = "c";
            this.ProposedMaterialCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ProposedMaterialCostSpinEdit.Size = new System.Drawing.Size(84, 20);
            this.ProposedMaterialCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.ProposedMaterialCostSpinEdit.TabIndex = 17;
            this.ProposedMaterialCostSpinEdit.EditValueChanged += new System.EventHandler(this.ProposedMaterialCostSpinEdit_EditValueChanged);
            this.ProposedMaterialCostSpinEdit.Validated += new System.EventHandler(this.ProposedMaterialCostSpinEdit_Validated);
            // 
            // ProposedLabourCostSpinEdit
            // 
            this.ProposedLabourCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ProposedLabourCost", true));
            this.ProposedLabourCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ProposedLabourCostSpinEdit.Location = new System.Drawing.Point(1089, 245);
            this.ProposedLabourCostSpinEdit.MenuManager = this.barManager1;
            this.ProposedLabourCostSpinEdit.Name = "ProposedLabourCostSpinEdit";
            this.ProposedLabourCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ProposedLabourCostSpinEdit.Properties.Mask.EditMask = "c";
            this.ProposedLabourCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ProposedLabourCostSpinEdit.Size = new System.Drawing.Size(84, 20);
            this.ProposedLabourCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.ProposedLabourCostSpinEdit.TabIndex = 17;
            this.ProposedLabourCostSpinEdit.EditValueChanged += new System.EventHandler(this.ProposedLabourCostSpinEdit_EditValueChanged);
            this.ProposedLabourCostSpinEdit.Validated += new System.EventHandler(this.ProposedLabourCostSpinEdit_Validated);
            // 
            // ClientContactEmailHyperLinkEdit
            // 
            this.ClientContactEmailHyperLinkEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ClientContactEmail", true));
            this.ClientContactEmailHyperLinkEdit.Location = new System.Drawing.Point(440, 268);
            this.ClientContactEmailHyperLinkEdit.MenuManager = this.barManager1;
            this.ClientContactEmailHyperLinkEdit.Name = "ClientContactEmailHyperLinkEdit";
            this.ClientContactEmailHyperLinkEdit.Properties.ReadOnly = true;
            this.ClientContactEmailHyperLinkEdit.Size = new System.Drawing.Size(176, 20);
            this.ClientContactEmailHyperLinkEdit.StyleController = this.dataLayoutControl1;
            this.ClientContactEmailHyperLinkEdit.TabIndex = 116;
            this.ClientContactEmailHyperLinkEdit.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.ClientContactEmailHyperLinkEdit_OpenLink);
            // 
            // ClientContactMobileTextEdit
            // 
            this.ClientContactMobileTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ClientContactMobile", true));
            this.ClientContactMobileTextEdit.Location = new System.Drawing.Point(440, 316);
            this.ClientContactMobileTextEdit.MenuManager = this.barManager1;
            this.ClientContactMobileTextEdit.Name = "ClientContactMobileTextEdit";
            this.ClientContactMobileTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientContactMobileTextEdit, true);
            this.ClientContactMobileTextEdit.Size = new System.Drawing.Size(176, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientContactMobileTextEdit, optionsSpelling1);
            this.ClientContactMobileTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientContactMobileTextEdit.TabIndex = 115;
            // 
            // ClientContactTelephoneTextEdit
            // 
            this.ClientContactTelephoneTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ClientContactTelephone", true));
            this.ClientContactTelephoneTextEdit.Location = new System.Drawing.Point(181, 316);
            this.ClientContactTelephoneTextEdit.MenuManager = this.barManager1;
            this.ClientContactTelephoneTextEdit.Name = "ClientContactTelephoneTextEdit";
            this.ClientContactTelephoneTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientContactTelephoneTextEdit, true);
            this.ClientContactTelephoneTextEdit.Size = new System.Drawing.Size(110, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientContactTelephoneTextEdit, optionsSpelling2);
            this.ClientContactTelephoneTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientContactTelephoneTextEdit.TabIndex = 114;
            // 
            // labelControlWarning
            // 
            this.labelControlWarning.Appearance.Image = global::WoodPlan5.Properties.Resources.attention_16;
            this.labelControlWarning.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlWarning.Appearance.Options.UseImage = true;
            this.labelControlWarning.Appearance.Options.UseImageAlign = true;
            this.labelControlWarning.Location = new System.Drawing.Point(169, 514);
            this.labelControlWarning.Name = "labelControlWarning";
            this.labelControlWarning.Size = new System.Drawing.Size(459, 16);
            this.labelControlWarning.StyleController = this.dataLayoutControl1;
            this.labelControlWarning.TabIndex = 113;
            this.labelControlWarning.Text = "        Please ensure your works do not involve any trees. If they do, ensure TPO" +
    " checks are done.";
            // 
            // RAMSCheckEdit
            // 
            this.RAMSCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "RAMS", true));
            this.RAMSCheckEdit.Location = new System.Drawing.Point(1184, 70);
            this.RAMSCheckEdit.MenuManager = this.barManager1;
            this.RAMSCheckEdit.Name = "RAMSCheckEdit";
            this.RAMSCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.RAMSCheckEdit.Properties.ValueChecked = 1;
            this.RAMSCheckEdit.Properties.ValueUnchecked = 0;
            this.RAMSCheckEdit.Size = new System.Drawing.Size(136, 19);
            this.RAMSCheckEdit.StyleController = this.dataLayoutControl1;
            this.RAMSCheckEdit.TabIndex = 111;
            // 
            // ListedBuildingStatusIDGridLookUpEdit
            // 
            this.ListedBuildingStatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ListedBuildingStatusID", true));
            this.ListedBuildingStatusIDGridLookUpEdit.Location = new System.Drawing.Point(1165, 528);
            this.ListedBuildingStatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ListedBuildingStatusIDGridLookUpEdit.Name = "ListedBuildingStatusIDGridLookUpEdit";
            this.ListedBuildingStatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ListedBuildingStatusIDGridLookUpEdit.Properties.DataSource = this.sp01053CoreListedBuildingStatusesWithBlankBindingSource;
            this.ListedBuildingStatusIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ListedBuildingStatusIDGridLookUpEdit.Properties.NullText = "";
            this.ListedBuildingStatusIDGridLookUpEdit.Properties.PopupView = this.gridView3;
            this.ListedBuildingStatusIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ListedBuildingStatusIDGridLookUpEdit.Size = new System.Drawing.Size(155, 20);
            this.ListedBuildingStatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ListedBuildingStatusIDGridLookUpEdit.TabIndex = 14;
            // 
            // sp01053CoreListedBuildingStatusesWithBlankBindingSource
            // 
            this.sp01053CoreListedBuildingStatusesWithBlankBindingSource.DataMember = "sp01053_Core_Listed_Building_Statuses_With_Blank";
            this.sp01053CoreListedBuildingStatusesWithBlankBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // dataSet_Common_Functionality
            // 
            this.dataSet_Common_Functionality.DataSetName = "DataSet_Common_Functionality";
            this.dataSet_Common_Functionality.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.gridColumn7;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView3.FormatRules.Add(gridFormatRule1);
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn9, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Listed Building Status";
            this.gridColumn8.FieldName = "Description";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 220;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Order";
            this.gridColumn9.FieldName = "RecordOrder";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            // 
            // ConservationStatusIDGridLookUpEdit
            // 
            this.ConservationStatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ConservationStatusID", true));
            this.ConservationStatusIDGridLookUpEdit.Location = new System.Drawing.Point(807, 528);
            this.ConservationStatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ConservationStatusIDGridLookUpEdit.Name = "ConservationStatusIDGridLookUpEdit";
            this.ConservationStatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ConservationStatusIDGridLookUpEdit.Properties.DataSource = this.sp01052CoreConservationStatusesWithBlankBindingSource;
            this.ConservationStatusIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ConservationStatusIDGridLookUpEdit.Properties.NullText = "";
            this.ConservationStatusIDGridLookUpEdit.Properties.PopupView = this.gridView2;
            this.ConservationStatusIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ConservationStatusIDGridLookUpEdit.Size = new System.Drawing.Size(209, 20);
            this.ConservationStatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ConservationStatusIDGridLookUpEdit.TabIndex = 14;
            // 
            // sp01052CoreConservationStatusesWithBlankBindingSource
            // 
            this.sp01052CoreConservationStatusesWithBlankBindingSource.DataMember = "sp01052_Core_Conservation_Statuses_With_Blank";
            this.sp01052CoreConservationStatusesWithBlankBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.gridColumn4;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView2.FormatRules.Add(gridFormatRule2);
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Conservation Status";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "RecordOrder";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            // 
            // TPONumberTextEdit
            // 
            this.TPONumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "TPONumber", true));
            this.TPONumberTextEdit.Location = new System.Drawing.Point(807, 504);
            this.TPONumberTextEdit.MenuManager = this.barManager1;
            this.TPONumberTextEdit.Name = "TPONumberTextEdit";
            this.TPONumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TPONumberTextEdit, true);
            this.TPONumberTextEdit.Size = new System.Drawing.Size(513, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TPONumberTextEdit, optionsSpelling3);
            this.TPONumberTextEdit.StyleController = this.dataLayoutControl1;
            this.TPONumberTextEdit.TabIndex = 74;
            // 
            // PlanningProceedDateSetTextEdit
            // 
            this.PlanningProceedDateSetTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "PlanningProceedDateSet", true));
            this.PlanningProceedDateSetTextEdit.Location = new System.Drawing.Point(807, 623);
            this.PlanningProceedDateSetTextEdit.MenuManager = this.barManager1;
            this.PlanningProceedDateSetTextEdit.Name = "PlanningProceedDateSetTextEdit";
            this.PlanningProceedDateSetTextEdit.Properties.Mask.EditMask = "g";
            this.PlanningProceedDateSetTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.PlanningProceedDateSetTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.PlanningProceedDateSetTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PlanningProceedDateSetTextEdit, true);
            this.PlanningProceedDateSetTextEdit.Size = new System.Drawing.Size(209, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PlanningProceedDateSetTextEdit, optionsSpelling4);
            this.PlanningProceedDateSetTextEdit.StyleController = this.dataLayoutControl1;
            this.PlanningProceedDateSetTextEdit.TabIndex = 110;
            // 
            // PlanningProceedStaffNameTextEdit
            // 
            this.PlanningProceedStaffNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "PlanningProceedStaffName", true));
            this.PlanningProceedStaffNameTextEdit.Location = new System.Drawing.Point(1165, 623);
            this.PlanningProceedStaffNameTextEdit.MenuManager = this.barManager1;
            this.PlanningProceedStaffNameTextEdit.Name = "PlanningProceedStaffNameTextEdit";
            this.PlanningProceedStaffNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PlanningProceedStaffNameTextEdit, true);
            this.PlanningProceedStaffNameTextEdit.Size = new System.Drawing.Size(155, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PlanningProceedStaffNameTextEdit, optionsSpelling5);
            this.PlanningProceedStaffNameTextEdit.StyleController = this.dataLayoutControl1;
            this.PlanningProceedStaffNameTextEdit.TabIndex = 109;
            // 
            // PlanningProceedStaffIDTextEdit
            // 
            this.PlanningProceedStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "PlanningProceedStaffID", true));
            this.PlanningProceedStaffIDTextEdit.Location = new System.Drawing.Point(806, 12);
            this.PlanningProceedStaffIDTextEdit.MenuManager = this.barManager1;
            this.PlanningProceedStaffIDTextEdit.Name = "PlanningProceedStaffIDTextEdit";
            this.PlanningProceedStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PlanningProceedStaffIDTextEdit, true);
            this.PlanningProceedStaffIDTextEdit.Size = new System.Drawing.Size(50, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PlanningProceedStaffIDTextEdit, optionsSpelling6);
            this.PlanningProceedStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PlanningProceedStaffIDTextEdit.TabIndex = 108;
            // 
            // TenderTypeIDGridLookUpEdit
            // 
            this.TenderTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "TenderTypeID", true));
            this.TenderTypeIDGridLookUpEdit.Location = new System.Drawing.Point(169, 70);
            this.TenderTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.TenderTypeIDGridLookUpEdit.Name = "TenderTypeIDGridLookUpEdit";
            this.TenderTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TenderTypeIDGridLookUpEdit.Properties.DataSource = this.sp06525OMTenderTypesWithBlankBindingSource;
            this.TenderTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.TenderTypeIDGridLookUpEdit.Properties.NullText = "";
            this.TenderTypeIDGridLookUpEdit.Properties.PopupView = this.gridView1;
            this.TenderTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.TenderTypeIDGridLookUpEdit.Size = new System.Drawing.Size(123, 20);
            this.TenderTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.TenderTypeIDGridLookUpEdit.TabIndex = 13;
            this.TenderTypeIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.TenderTypeIDGridLookUpEdit_Validating);
            // 
            // sp06525OMTenderTypesWithBlankBindingSource
            // 
            this.sp06525OMTenderTypesWithBlankBindingSource.DataMember = "sp06525_OM_Tender_Types_With_Blank";
            this.sp06525OMTenderTypesWithBlankBindingSource.DataSource = this.dataSet_OM_Tender;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Column = this.gridColumn1;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue3.Value1 = 0;
            gridFormatRule3.Rule = formatConditionRuleValue3;
            this.gridView1.FormatRules.Add(gridFormatRule3);
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Tender Type";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // ddBtnSentToCM
            // 
            this.ddBtnSentToCM.DropDownControl = this.pmSendQuoteToCM;
            this.ddBtnSentToCM.Location = new System.Drawing.Point(204, 652);
            this.ddBtnSentToCM.MenuManager = this.barManager1;
            this.ddBtnSentToCM.Name = "ddBtnSentToCM";
            this.ddBtnSentToCM.Size = new System.Drawing.Size(111, 22);
            this.ddBtnSentToCM.StyleController = this.dataLayoutControl1;
            this.ddBtnSentToCM.TabIndex = 107;
            this.ddBtnSentToCM.Text = "Send Quote to CM";
            this.ddBtnSentToCM.Click += new System.EventHandler(this.ddBtnSentToCM_Click);
            // 
            // pmSendQuoteToCM
            // 
            this.pmSendQuoteToCM.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSendQuoteToCM),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiNoQuoteRequired)});
            this.pmSendQuoteToCM.Manager = this.barManager1;
            this.pmSendQuoteToCM.MenuCaption = "Send Quote To CM";
            this.pmSendQuoteToCM.Name = "pmSendQuoteToCM";
            this.pmSendQuoteToCM.ShowCaption = true;
            // 
            // bbiSendQuoteToCM
            // 
            this.bbiSendQuoteToCM.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSendQuoteToCM.Caption = "<b>Send</b> Quote to <b>Contract Manager</b>";
            this.bbiSendQuoteToCM.Id = 35;
            this.bbiSendQuoteToCM.ImageOptions.ImageIndex = 3;
            this.bbiSendQuoteToCM.Name = "bbiSendQuoteToCM";
            this.bbiSendQuoteToCM.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSendQuoteToCM_ItemClick);
            // 
            // bbiNoQuoteRequired
            // 
            this.bbiNoQuoteRequired.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiNoQuoteRequired.Caption = "<b>No</b> Quote Required";
            this.bbiNoQuoteRequired.Id = 36;
            this.bbiNoQuoteRequired.ImageOptions.ImageIndex = 4;
            this.bbiNoQuoteRequired.Name = "bbiNoQuoteRequired";
            this.bbiNoQuoteRequired.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNoQuoteRequired_ItemClick);
            // 
            // KAMQuoteRejectionReasonIDTextEdit
            // 
            this.KAMQuoteRejectionReasonIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "KAMQuoteRejectionReasonID", true));
            this.KAMQuoteRejectionReasonIDTextEdit.Location = new System.Drawing.Point(174, 152);
            this.KAMQuoteRejectionReasonIDTextEdit.MenuManager = this.barManager1;
            this.KAMQuoteRejectionReasonIDTextEdit.Name = "KAMQuoteRejectionReasonIDTextEdit";
            this.KAMQuoteRejectionReasonIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.KAMQuoteRejectionReasonIDTextEdit, true);
            this.KAMQuoteRejectionReasonIDTextEdit.Size = new System.Drawing.Size(483, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.KAMQuoteRejectionReasonIDTextEdit, optionsSpelling7);
            this.KAMQuoteRejectionReasonIDTextEdit.StyleController = this.dataLayoutControl1;
            this.KAMQuoteRejectionReasonIDTextEdit.TabIndex = 106;
            // 
            // ddBtnKAMAuthorised
            // 
            this.ddBtnKAMAuthorised.DropDownControl = this.pmKAMResponse;
            this.ddBtnKAMAuthorised.Location = new System.Drawing.Point(204, 704);
            this.ddBtnKAMAuthorised.MenuManager = this.barManager1;
            this.ddBtnKAMAuthorised.Name = "ddBtnKAMAuthorised";
            this.ddBtnKAMAuthorised.Size = new System.Drawing.Size(111, 22);
            this.ddBtnKAMAuthorised.StyleController = this.dataLayoutControl1;
            this.ddBtnKAMAuthorised.TabIndex = 105;
            this.ddBtnKAMAuthorised.Text = "KAM Authorised";
            this.ddBtnKAMAuthorised.Click += new System.EventHandler(this.ddBtnKAMAuthorised_Click);
            // 
            // pmKAMResponse
            // 
            this.pmKAMResponse.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiKAMAccepted),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiKAMRejected)});
            this.pmKAMResponse.Manager = this.barManager1;
            this.pmKAMResponse.MenuCaption = "KAm Authorisation";
            this.pmKAMResponse.Name = "pmKAMResponse";
            this.pmKAMResponse.ShowCaption = true;
            // 
            // bbiKAMAccepted
            // 
            this.bbiKAMAccepted.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiKAMAccepted.Caption = "KAM <b>Accepted</b>";
            this.bbiKAMAccepted.Id = 33;
            this.bbiKAMAccepted.ImageOptions.ImageIndex = 0;
            this.bbiKAMAccepted.Name = "bbiKAMAccepted";
            this.bbiKAMAccepted.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiKAMAccepted_ItemClick);
            // 
            // bbiKAMRejected
            // 
            this.bbiKAMRejected.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiKAMRejected.Caption = "KAM <b>Rejected</b>";
            this.bbiKAMRejected.Id = 34;
            this.bbiKAMRejected.ImageOptions.ImageIndex = 1;
            this.bbiKAMRejected.Name = "bbiKAMRejected";
            this.bbiKAMRejected.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiKAMRejected_ItemClick);
            // 
            // btnCancelTender
            // 
            this.btnCancelTender.ImageOptions.ImageIndex = 8;
            this.btnCancelTender.ImageOptions.ImageList = this.imageCollection1;
            this.btnCancelTender.Location = new System.Drawing.Point(516, 808);
            this.btnCancelTender.Name = "btnCancelTender";
            this.btnCancelTender.Size = new System.Drawing.Size(112, 22);
            this.btnCancelTender.StyleController = this.dataLayoutControl1;
            this.btnCancelTender.TabIndex = 104;
            this.btnCancelTender.Text = "Cancel Tender";
            this.btnCancelTender.Click += new System.EventHandler(this.btnCancelTender_Click);
            // 
            // JobCountTextEdit
            // 
            this.JobCountTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "JobCount", true));
            this.JobCountTextEdit.Location = new System.Drawing.Point(516, 782);
            this.JobCountTextEdit.MenuManager = this.barManager1;
            this.JobCountTextEdit.Name = "JobCountTextEdit";
            this.JobCountTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobCountTextEdit, true);
            this.JobCountTextEdit.Size = new System.Drawing.Size(112, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobCountTextEdit, optionsSpelling8);
            this.JobCountTextEdit.StyleController = this.dataLayoutControl1;
            this.JobCountTextEdit.TabIndex = 103;
            // 
            // VisitCountTextEdit
            // 
            this.VisitCountTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "VisitCount", true));
            this.VisitCountTextEdit.Location = new System.Drawing.Point(464, 782);
            this.VisitCountTextEdit.MenuManager = this.barManager1;
            this.VisitCountTextEdit.Name = "VisitCountTextEdit";
            this.VisitCountTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitCountTextEdit, true);
            this.VisitCountTextEdit.Size = new System.Drawing.Size(48, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitCountTextEdit, optionsSpelling9);
            this.VisitCountTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitCountTextEdit.TabIndex = 102;
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ProgressBarValue", true));
            this.progressBarControl1.EditValue = 10;
            this.progressBarControl1.Location = new System.Drawing.Point(24, 652);
            this.progressBarControl1.MenuManager = this.barManager1;
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.progressBarControl1.Properties.EndColor = System.Drawing.Color.PaleGreen;
            this.progressBarControl1.Properties.Maximum = 70;
            this.progressBarControl1.Properties.ProgressKind = DevExpress.XtraEditors.Controls.ProgressKind.Vertical;
            this.progressBarControl1.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.progressBarControl1.Properties.ReadOnly = true;
            this.progressBarControl1.Properties.ShowTitle = true;
            this.progressBarControl1.Properties.StartColor = System.Drawing.Color.Green;
            this.progressBarControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.progressBarControl1.Size = new System.Drawing.Size(18, 178);
            this.progressBarControl1.StyleController = this.dataLayoutControl1;
            this.progressBarControl1.TabIndex = 101;
            // 
            // ddBtnClientResponse
            // 
            this.ddBtnClientResponse.DropDownControl = this.pmClientResponse;
            this.ddBtnClientResponse.Location = new System.Drawing.Point(204, 756);
            this.ddBtnClientResponse.MenuManager = this.barManager1;
            this.ddBtnClientResponse.Name = "ddBtnClientResponse";
            this.ddBtnClientResponse.Size = new System.Drawing.Size(111, 22);
            this.ddBtnClientResponse.StyleController = this.dataLayoutControl1;
            this.ddBtnClientResponse.TabIndex = 100;
            this.ddBtnClientResponse.Text = "Client Accepted";
            this.ddBtnClientResponse.Click += new System.EventHandler(this.ddBtnClientResponse_Click);
            // 
            // pmClientResponse
            // 
            this.pmClientResponse.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClientAccepted),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClientRejected),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClientOnHold)});
            this.pmClientResponse.Manager = this.barManager1;
            this.pmClientResponse.MenuCaption = "Client Response";
            this.pmClientResponse.Name = "pmClientResponse";
            this.pmClientResponse.ShowCaption = true;
            // 
            // bbiClientAccepted
            // 
            this.bbiClientAccepted.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiClientAccepted.Caption = "Client <b>Accepted</b>";
            this.bbiClientAccepted.Id = 30;
            this.bbiClientAccepted.ImageOptions.ImageIndex = 5;
            this.bbiClientAccepted.Name = "bbiClientAccepted";
            this.bbiClientAccepted.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiClientAccepted.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClientAccepted_ItemClick);
            // 
            // bbiClientRejected
            // 
            this.bbiClientRejected.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiClientRejected.Caption = "Client <b>Declined</b>";
            this.bbiClientRejected.Id = 31;
            this.bbiClientRejected.ImageOptions.ImageIndex = 7;
            this.bbiClientRejected.Name = "bbiClientRejected";
            this.bbiClientRejected.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiClientRejected.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClientRejected_ItemClick);
            // 
            // bbiClientOnHold
            // 
            this.bbiClientOnHold.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiClientOnHold.Caption = "Client <b>On-Hold</b>";
            this.bbiClientOnHold.Id = 32;
            this.bbiClientOnHold.ImageOptions.ImageIndex = 6;
            this.bbiClientOnHold.Name = "bbiClientOnHold";
            this.bbiClientOnHold.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiClientOnHold.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClientOnHold_ItemClick);
            // 
            // btnTenderClosed
            // 
            this.btnTenderClosed.Location = new System.Drawing.Point(204, 808);
            this.btnTenderClosed.Name = "btnTenderClosed";
            this.btnTenderClosed.Size = new System.Drawing.Size(111, 22);
            this.btnTenderClosed.StyleController = this.dataLayoutControl1;
            this.btnTenderClosed.TabIndex = 99;
            this.btnTenderClosed.Text = "Tender Closed";
            this.btnTenderClosed.Click += new System.EventHandler(this.btnTenderClosed_Click);
            // 
            // btnCreateVisits
            // 
            this.btnCreateVisits.Location = new System.Drawing.Point(204, 782);
            this.btnCreateVisits.Name = "btnCreateVisits";
            this.btnCreateVisits.Size = new System.Drawing.Size(111, 22);
            this.btnCreateVisits.StyleController = this.dataLayoutControl1;
            this.btnCreateVisits.TabIndex = 98;
            this.btnCreateVisits.Text = "Create Visit(s)";
            this.btnCreateVisits.Click += new System.EventHandler(this.btnCreateVisits_Click);
            // 
            // btnReturnedFromCM
            // 
            this.btnReturnedFromCM.Location = new System.Drawing.Point(204, 678);
            this.btnReturnedFromCM.Name = "btnReturnedFromCM";
            this.btnReturnedFromCM.Size = new System.Drawing.Size(111, 22);
            this.btnReturnedFromCM.StyleController = this.dataLayoutControl1;
            this.btnReturnedFromCM.TabIndex = 92;
            this.btnReturnedFromCM.Text = "CM Returned Quote";
            this.btnReturnedFromCM.Click += new System.EventHandler(this.btnReturnedFromCM_Click);
            // 
            // ClientQuoteSpreadsheetExtractFileButtonEdit
            // 
            this.ClientQuoteSpreadsheetExtractFileButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ClientQuoteSpreadsheetExtractFile", true));
            this.ClientQuoteSpreadsheetExtractFileButtonEdit.Location = new System.Drawing.Point(807, 141);
            this.ClientQuoteSpreadsheetExtractFileButtonEdit.MenuManager = this.barManager1;
            this.ClientQuoteSpreadsheetExtractFileButtonEdit.Name = "ClientQuoteSpreadsheetExtractFileButtonEdit";
            editorButtonImageOptions1.Location = DevExpress.XtraEditors.ImageLocation.Default;
            this.ClientQuoteSpreadsheetExtractFileButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Create File", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to Create Spreadsheet Extract File", "create", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View File", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to open the Spreadsheet Extract file", "view", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click me to clear value", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ClientQuoteSpreadsheetExtractFileButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientQuoteSpreadsheetExtractFileButtonEdit.Size = new System.Drawing.Size(513, 20);
            this.ClientQuoteSpreadsheetExtractFileButtonEdit.StyleController = this.dataLayoutControl1;
            this.ClientQuoteSpreadsheetExtractFileButtonEdit.TabIndex = 13;
            this.ClientQuoteSpreadsheetExtractFileButtonEdit.TabStop = false;
            this.ClientQuoteSpreadsheetExtractFileButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientQuoteSpreadsheetExtractFileButtonEdit_ButtonClick);
            // 
            // ContractorNameButtonEdit
            // 
            this.ContractorNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ProposedLabour", true));
            this.ContractorNameButtonEdit.EditValue = "";
            this.ContractorNameButtonEdit.Location = new System.Drawing.Point(169, 558);
            this.ContractorNameButtonEdit.MenuManager = this.barManager1;
            this.ContractorNameButtonEdit.Name = "ContractorNameButtonEdit";
            this.ContractorNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click me to open the Select Contractor screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Click me to clear value", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ContractorNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ContractorNameButtonEdit.Size = new System.Drawing.Size(123, 20);
            this.ContractorNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.ContractorNameButtonEdit.TabIndex = 14;
            this.ContractorNameButtonEdit.TabStop = false;
            this.ContractorNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ContractorNameButtonEdit_ButtonClick);
            // 
            // LinkedToPersonTypeButtonEdit
            // 
            this.LinkedToPersonTypeButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ProposedLabourType", true));
            this.LinkedToPersonTypeButtonEdit.EditValue = "";
            this.LinkedToPersonTypeButtonEdit.Location = new System.Drawing.Point(441, 558);
            this.LinkedToPersonTypeButtonEdit.MenuManager = this.barManager1;
            this.LinkedToPersonTypeButtonEdit.Name = "LinkedToPersonTypeButtonEdit";
            this.LinkedToPersonTypeButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Click me to Open Select Person Type screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Click me to clear value", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LinkedToPersonTypeButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.LinkedToPersonTypeButtonEdit.Size = new System.Drawing.Size(187, 20);
            this.LinkedToPersonTypeButtonEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToPersonTypeButtonEdit.TabIndex = 15;
            this.LinkedToPersonTypeButtonEdit.TabStop = false;
            this.LinkedToPersonTypeButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LinkedToPersonTypeButtonEdit_ButtonClick);
            // 
            // StatusIssueIDTextEdit
            // 
            this.StatusIssueIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "StatusIssueID", true));
            this.StatusIssueIDTextEdit.Location = new System.Drawing.Point(825, 128);
            this.StatusIssueIDTextEdit.MenuManager = this.barManager1;
            this.StatusIssueIDTextEdit.Name = "StatusIssueIDTextEdit";
            this.StatusIssueIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StatusIssueIDTextEdit, true);
            this.StatusIssueIDTextEdit.Size = new System.Drawing.Size(459, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StatusIssueIDTextEdit, optionsSpelling10);
            this.StatusIssueIDTextEdit.StyleController = this.dataLayoutControl1;
            this.StatusIssueIDTextEdit.TabIndex = 91;
            // 
            // StatusIsssueButtonEdit
            // 
            this.StatusIsssueButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "StatusIsssue", true));
            this.StatusIsssueButtonEdit.EditValue = "";
            this.StatusIsssueButtonEdit.Location = new System.Drawing.Point(464, 628);
            this.StatusIsssueButtonEdit.MenuManager = this.barManager1;
            this.StatusIsssueButtonEdit.Name = "StatusIsssueButtonEdit";
            this.StatusIsssueButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Click me to open the Select Tender Status screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "Click me to clear value", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.StatusIsssueButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.StatusIsssueButtonEdit.Size = new System.Drawing.Size(164, 20);
            this.StatusIsssueButtonEdit.StyleController = this.dataLayoutControl1;
            this.StatusIsssueButtonEdit.TabIndex = 14;
            this.StatusIsssueButtonEdit.TabStop = false;
            this.StatusIsssueButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.StatusIsssueButtonEdit_ButtonClick);
            // 
            // btnSendToClient
            // 
            this.btnSendToClient.Location = new System.Drawing.Point(204, 730);
            this.btnSendToClient.Name = "btnSendToClient";
            this.btnSendToClient.Size = new System.Drawing.Size(111, 22);
            this.btnSendToClient.StyleController = this.dataLayoutControl1;
            this.btnSendToClient.TabIndex = 88;
            this.btnSendToClient.Text = "Sent To Client";
            this.btnSendToClient.Click += new System.EventHandler(this.btnSendToClient_Click);
            // 
            // SubmittedToCMDateDateEdit
            // 
            this.SubmittedToCMDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "SubmittedToCMDate", true));
            this.SubmittedToCMDateDateEdit.EditValue = null;
            this.SubmittedToCMDateDateEdit.Location = new System.Drawing.Point(464, 652);
            this.SubmittedToCMDateDateEdit.MenuManager = this.barManager1;
            this.SubmittedToCMDateDateEdit.Name = "SubmittedToCMDateDateEdit";
            this.SubmittedToCMDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.SubmittedToCMDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SubmittedToCMDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SubmittedToCMDateDateEdit.Properties.Mask.EditMask = "g";
            this.SubmittedToCMDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SubmittedToCMDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.SubmittedToCMDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.SubmittedToCMDateDateEdit.Properties.ReadOnly = true;
            this.SubmittedToCMDateDateEdit.Size = new System.Drawing.Size(164, 20);
            this.SubmittedToCMDateDateEdit.StyleController = this.dataLayoutControl1;
            this.SubmittedToCMDateDateEdit.TabIndex = 13;
            // 
            // ActualTotalCostSpinEdit
            // 
            this.ActualTotalCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ActualTotalCost", true));
            this.ActualTotalCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ActualTotalCostSpinEdit.Location = new System.Drawing.Point(1258, 324);
            this.ActualTotalCostSpinEdit.MenuManager = this.barManager1;
            this.ActualTotalCostSpinEdit.Name = "ActualTotalCostSpinEdit";
            this.ActualTotalCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActualTotalCostSpinEdit.Properties.Mask.EditMask = "c";
            this.ActualTotalCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualTotalCostSpinEdit.Properties.ReadOnly = true;
            this.ActualTotalCostSpinEdit.Size = new System.Drawing.Size(50, 20);
            this.ActualTotalCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.ActualTotalCostSpinEdit.TabIndex = 18;
            // 
            // ActualEquipmentCostSpinEdit
            // 
            this.ActualEquipmentCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ActualEquipmentCost", true));
            this.ActualEquipmentCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ActualEquipmentCostSpinEdit.Location = new System.Drawing.Point(1258, 298);
            this.ActualEquipmentCostSpinEdit.MenuManager = this.barManager1;
            this.ActualEquipmentCostSpinEdit.Name = "ActualEquipmentCostSpinEdit";
            this.ActualEquipmentCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActualEquipmentCostSpinEdit.Properties.Mask.EditMask = "c";
            this.ActualEquipmentCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualEquipmentCostSpinEdit.Size = new System.Drawing.Size(50, 20);
            this.ActualEquipmentCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.ActualEquipmentCostSpinEdit.TabIndex = 18;
            this.ActualEquipmentCostSpinEdit.EditValueChanged += new System.EventHandler(this.ActualEquipmentCostSpinEdit_EditValueChanged);
            this.ActualEquipmentCostSpinEdit.Validated += new System.EventHandler(this.ActualEquipmentCostSpinEdit_Validated);
            // 
            // ActualMaterialCostSpinEdit
            // 
            this.ActualMaterialCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ActualMaterialCost", true));
            this.ActualMaterialCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ActualMaterialCostSpinEdit.Location = new System.Drawing.Point(1258, 271);
            this.ActualMaterialCostSpinEdit.MenuManager = this.barManager1;
            this.ActualMaterialCostSpinEdit.Name = "ActualMaterialCostSpinEdit";
            this.ActualMaterialCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActualMaterialCostSpinEdit.Properties.Mask.EditMask = "c";
            this.ActualMaterialCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualMaterialCostSpinEdit.Size = new System.Drawing.Size(50, 20);
            this.ActualMaterialCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.ActualMaterialCostSpinEdit.TabIndex = 17;
            this.ActualMaterialCostSpinEdit.EditValueChanged += new System.EventHandler(this.ActualMaterialCostSpinEdit_EditValueChanged);
            this.ActualMaterialCostSpinEdit.Validated += new System.EventHandler(this.ActualMaterialCostSpinEdit_Validated);
            // 
            // ActualLabourCostSpinEdit
            // 
            this.ActualLabourCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ActualLabourCost", true));
            this.ActualLabourCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ActualLabourCostSpinEdit.Location = new System.Drawing.Point(1258, 245);
            this.ActualLabourCostSpinEdit.MenuManager = this.barManager1;
            this.ActualLabourCostSpinEdit.Name = "ActualLabourCostSpinEdit";
            this.ActualLabourCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActualLabourCostSpinEdit.Properties.Mask.EditMask = "c";
            this.ActualLabourCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualLabourCostSpinEdit.Size = new System.Drawing.Size(50, 20);
            this.ActualLabourCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.ActualLabourCostSpinEdit.TabIndex = 16;
            this.ActualLabourCostSpinEdit.EditValueChanged += new System.EventHandler(this.ActualLabourCostSpinEdit_EditValueChanged);
            this.ActualLabourCostSpinEdit.Validated += new System.EventHandler(this.ActualLabourCostSpinEdit_Validated);
            // 
            // AcceptedTotalSellSpinEdit
            // 
            this.AcceptedTotalSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "AcceptedTotalSell", true));
            this.AcceptedTotalSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AcceptedTotalSellSpinEdit.Location = new System.Drawing.Point(909, 317);
            this.AcceptedTotalSellSpinEdit.MenuManager = this.barManager1;
            this.AcceptedTotalSellSpinEdit.Name = "AcceptedTotalSellSpinEdit";
            this.AcceptedTotalSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AcceptedTotalSellSpinEdit.Properties.Mask.EditMask = "c";
            this.AcceptedTotalSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AcceptedTotalSellSpinEdit.Properties.ReadOnly = true;
            this.AcceptedTotalSellSpinEdit.Size = new System.Drawing.Size(95, 20);
            this.AcceptedTotalSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.AcceptedTotalSellSpinEdit.TabIndex = 17;
            // 
            // AcceptedEquipmentSellSpinEdit
            // 
            this.AcceptedEquipmentSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "AcceptedEquipmentSell", true));
            this.AcceptedEquipmentSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AcceptedEquipmentSellSpinEdit.Location = new System.Drawing.Point(909, 293);
            this.AcceptedEquipmentSellSpinEdit.MenuManager = this.barManager1;
            this.AcceptedEquipmentSellSpinEdit.Name = "AcceptedEquipmentSellSpinEdit";
            this.AcceptedEquipmentSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AcceptedEquipmentSellSpinEdit.Properties.Mask.EditMask = "c";
            this.AcceptedEquipmentSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AcceptedEquipmentSellSpinEdit.Size = new System.Drawing.Size(95, 20);
            this.AcceptedEquipmentSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.AcceptedEquipmentSellSpinEdit.TabIndex = 17;
            this.AcceptedEquipmentSellSpinEdit.EditValueChanged += new System.EventHandler(this.AcceptedEquipmentSellSpinEdit_EditValueChanged);
            this.AcceptedEquipmentSellSpinEdit.Validated += new System.EventHandler(this.AcceptedEquipmentSellSpinEdit_Validated);
            // 
            // AcceptedMaterialSellSpinEdit
            // 
            this.AcceptedMaterialSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "AcceptedMaterialSell", true));
            this.AcceptedMaterialSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AcceptedMaterialSellSpinEdit.Location = new System.Drawing.Point(909, 269);
            this.AcceptedMaterialSellSpinEdit.MenuManager = this.barManager1;
            this.AcceptedMaterialSellSpinEdit.Name = "AcceptedMaterialSellSpinEdit";
            this.AcceptedMaterialSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AcceptedMaterialSellSpinEdit.Properties.Mask.EditMask = "c";
            this.AcceptedMaterialSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AcceptedMaterialSellSpinEdit.Size = new System.Drawing.Size(95, 20);
            this.AcceptedMaterialSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.AcceptedMaterialSellSpinEdit.TabIndex = 16;
            this.AcceptedMaterialSellSpinEdit.EditValueChanged += new System.EventHandler(this.AcceptedMaterialSellSpinEdit_EditValueChanged);
            this.AcceptedMaterialSellSpinEdit.Validated += new System.EventHandler(this.AcceptedMaterialSellSpinEdit_Validated);
            // 
            // QuotedTotalSellSpinEdit
            // 
            this.QuotedTotalSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "QuotedTotalSell", true));
            this.QuotedTotalSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.QuotedTotalSellSpinEdit.Location = new System.Drawing.Point(731, 317);
            this.QuotedTotalSellSpinEdit.MenuManager = this.barManager1;
            this.QuotedTotalSellSpinEdit.Name = "QuotedTotalSellSpinEdit";
            this.QuotedTotalSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.QuotedTotalSellSpinEdit.Properties.Mask.EditMask = "c";
            this.QuotedTotalSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.QuotedTotalSellSpinEdit.Properties.ReadOnly = true;
            this.QuotedTotalSellSpinEdit.Size = new System.Drawing.Size(93, 20);
            this.QuotedTotalSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.QuotedTotalSellSpinEdit.TabIndex = 16;
            // 
            // AcceptedLabourSellSpinEdit
            // 
            this.AcceptedLabourSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "AcceptedLabourSell", true));
            this.AcceptedLabourSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AcceptedLabourSellSpinEdit.Location = new System.Drawing.Point(909, 245);
            this.AcceptedLabourSellSpinEdit.MenuManager = this.barManager1;
            this.AcceptedLabourSellSpinEdit.Name = "AcceptedLabourSellSpinEdit";
            this.AcceptedLabourSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AcceptedLabourSellSpinEdit.Properties.Mask.EditMask = "c";
            this.AcceptedLabourSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AcceptedLabourSellSpinEdit.Size = new System.Drawing.Size(95, 20);
            this.AcceptedLabourSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.AcceptedLabourSellSpinEdit.TabIndex = 15;
            this.AcceptedLabourSellSpinEdit.EditValueChanged += new System.EventHandler(this.AcceptedLabourSellSpinEdit_EditValueChanged);
            this.AcceptedLabourSellSpinEdit.Validated += new System.EventHandler(this.AcceptedLabourSellSpinEdit_Validated);
            // 
            // QuotedEquipmentSellSpinEdit
            // 
            this.QuotedEquipmentSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "QuotedEquipmentSell", true));
            this.QuotedEquipmentSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.QuotedEquipmentSellSpinEdit.Location = new System.Drawing.Point(731, 293);
            this.QuotedEquipmentSellSpinEdit.MenuManager = this.barManager1;
            this.QuotedEquipmentSellSpinEdit.Name = "QuotedEquipmentSellSpinEdit";
            this.QuotedEquipmentSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.QuotedEquipmentSellSpinEdit.Properties.Mask.EditMask = "c";
            this.QuotedEquipmentSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.QuotedEquipmentSellSpinEdit.Size = new System.Drawing.Size(93, 20);
            this.QuotedEquipmentSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.QuotedEquipmentSellSpinEdit.TabIndex = 14;
            this.QuotedEquipmentSellSpinEdit.EditValueChanged += new System.EventHandler(this.QuotedEquipmentSellSpinEdit_EditValueChanged);
            this.QuotedEquipmentSellSpinEdit.Validated += new System.EventHandler(this.QuotedEquipmentSellSpinEdit_Validated);
            // 
            // QuotedMaterialSellSpinEdit
            // 
            this.QuotedMaterialSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "QuotedMaterialSell", true));
            this.QuotedMaterialSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.QuotedMaterialSellSpinEdit.Location = new System.Drawing.Point(731, 269);
            this.QuotedMaterialSellSpinEdit.MenuManager = this.barManager1;
            this.QuotedMaterialSellSpinEdit.Name = "QuotedMaterialSellSpinEdit";
            this.QuotedMaterialSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.QuotedMaterialSellSpinEdit.Properties.Mask.EditMask = "c";
            this.QuotedMaterialSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.QuotedMaterialSellSpinEdit.Size = new System.Drawing.Size(93, 20);
            this.QuotedMaterialSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.QuotedMaterialSellSpinEdit.TabIndex = 13;
            this.QuotedMaterialSellSpinEdit.EditValueChanged += new System.EventHandler(this.QuotedMaterialSellSpinEdit_EditValueChanged);
            this.QuotedMaterialSellSpinEdit.Validated += new System.EventHandler(this.QuotedMaterialSellSpinEdit_Validated);
            // 
            // NoClientPOAuthorisedByDirectorButtonEdit
            // 
            this.NoClientPOAuthorisedByDirectorButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "NoClientPOAuthorisedByDirector", true));
            this.NoClientPOAuthorisedByDirectorButtonEdit.EditValue = "";
            this.NoClientPOAuthorisedByDirectorButtonEdit.Location = new System.Drawing.Point(569, 234);
            this.NoClientPOAuthorisedByDirectorButtonEdit.MenuManager = this.barManager1;
            this.NoClientPOAuthorisedByDirectorButtonEdit.Name = "NoClientPOAuthorisedByDirectorButtonEdit";
            this.NoClientPOAuthorisedByDirectorButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions10, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject37, serializableAppearanceObject38, serializableAppearanceObject39, serializableAppearanceObject40, "Click me to open the Select Director screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions11, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject41, serializableAppearanceObject42, serializableAppearanceObject43, serializableAppearanceObject44, "Click me to clear value", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.NoClientPOAuthorisedByDirectorButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.NoClientPOAuthorisedByDirectorButtonEdit.Size = new System.Drawing.Size(47, 20);
            this.NoClientPOAuthorisedByDirectorButtonEdit.StyleController = this.dataLayoutControl1;
            this.NoClientPOAuthorisedByDirectorButtonEdit.TabIndex = 14;
            this.NoClientPOAuthorisedByDirectorButtonEdit.TabStop = false;
            this.NoClientPOAuthorisedByDirectorButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.NoClientPOAuthorisedByDirectorButtonEdit_ButtonClick);
            // 
            // NoClientPOEmailedToDirectorCheckEdit
            // 
            this.NoClientPOEmailedToDirectorCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "NoClientPOEmailedToDirector", true));
            this.NoClientPOEmailedToDirectorCheckEdit.Location = new System.Drawing.Point(174, 344);
            this.NoClientPOEmailedToDirectorCheckEdit.MenuManager = this.barManager1;
            this.NoClientPOEmailedToDirectorCheckEdit.Name = "NoClientPOEmailedToDirectorCheckEdit";
            this.NoClientPOEmailedToDirectorCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.NoClientPOEmailedToDirectorCheckEdit.Properties.ReadOnly = true;
            this.NoClientPOEmailedToDirectorCheckEdit.Properties.ValueChecked = 1;
            this.NoClientPOEmailedToDirectorCheckEdit.Properties.ValueUnchecked = 0;
            this.NoClientPOEmailedToDirectorCheckEdit.Size = new System.Drawing.Size(487, 19);
            this.NoClientPOEmailedToDirectorCheckEdit.StyleController = this.dataLayoutControl1;
            this.NoClientPOEmailedToDirectorCheckEdit.TabIndex = 86;
            // 
            // NoClientPOAuthorisedByDirectorIDTextEdit
            // 
            this.NoClientPOAuthorisedByDirectorIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "NoClientPOAuthorisedByDirectorID", true));
            this.NoClientPOAuthorisedByDirectorIDTextEdit.Location = new System.Drawing.Point(204, 354);
            this.NoClientPOAuthorisedByDirectorIDTextEdit.MenuManager = this.barManager1;
            this.NoClientPOAuthorisedByDirectorIDTextEdit.Name = "NoClientPOAuthorisedByDirectorIDTextEdit";
            this.NoClientPOAuthorisedByDirectorIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.NoClientPOAuthorisedByDirectorIDTextEdit, true);
            this.NoClientPOAuthorisedByDirectorIDTextEdit.Size = new System.Drawing.Size(457, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.NoClientPOAuthorisedByDirectorIDTextEdit, optionsSpelling11);
            this.NoClientPOAuthorisedByDirectorIDTextEdit.StyleController = this.dataLayoutControl1;
            this.NoClientPOAuthorisedByDirectorIDTextEdit.TabIndex = 86;
            // 
            // NoClientPOCheckEdit
            // 
            this.NoClientPOCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "NoClientPO", true));
            this.NoClientPOCheckEdit.Location = new System.Drawing.Point(413, 234);
            this.NoClientPOCheckEdit.MenuManager = this.barManager1;
            this.NoClientPOCheckEdit.Name = "NoClientPOCheckEdit";
            this.NoClientPOCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.NoClientPOCheckEdit.Properties.ValueChecked = 1;
            this.NoClientPOCheckEdit.Properties.ValueUnchecked = 0;
            this.NoClientPOCheckEdit.Size = new System.Drawing.Size(76, 19);
            this.NoClientPOCheckEdit.StyleController = this.dataLayoutControl1;
            this.NoClientPOCheckEdit.TabIndex = 85;
            this.NoClientPOCheckEdit.EditValueChanged += new System.EventHandler(this.NoClientPOCheckEdit_EditValueChanged);
            this.NoClientPOCheckEdit.Validated += new System.EventHandler(this.NoClientPOCheckEdit_Validated);
            // 
            // RevisionNumberTextEdit
            // 
            this.RevisionNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "RevisionNumber", true));
            this.RevisionNumberTextEdit.Location = new System.Drawing.Point(400, 12);
            this.RevisionNumberTextEdit.MenuManager = this.barManager1;
            this.RevisionNumberTextEdit.Name = "RevisionNumberTextEdit";
            this.RevisionNumberTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RevisionNumberTextEdit, true);
            this.RevisionNumberTextEdit.Size = new System.Drawing.Size(86, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RevisionNumberTextEdit, optionsSpelling12);
            this.RevisionNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.RevisionNumberTextEdit.TabIndex = 84;
            // 
            // TenderProactiveDaysToReturnQuoteTextEdit
            // 
            this.TenderProactiveDaysToReturnQuoteTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "TenderProactiveDaysToReturnQuote", true));
            this.TenderProactiveDaysToReturnQuoteTextEdit.Location = new System.Drawing.Point(187, 284);
            this.TenderProactiveDaysToReturnQuoteTextEdit.MenuManager = this.barManager1;
            this.TenderProactiveDaysToReturnQuoteTextEdit.Name = "TenderProactiveDaysToReturnQuoteTextEdit";
            this.TenderProactiveDaysToReturnQuoteTextEdit.Properties.Mask.EditMask = "n0";
            this.TenderProactiveDaysToReturnQuoteTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TenderProactiveDaysToReturnQuoteTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TenderProactiveDaysToReturnQuoteTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TenderProactiveDaysToReturnQuoteTextEdit, true);
            this.TenderProactiveDaysToReturnQuoteTextEdit.Size = new System.Drawing.Size(312, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TenderProactiveDaysToReturnQuoteTextEdit, optionsSpelling13);
            this.TenderProactiveDaysToReturnQuoteTextEdit.StyleController = this.dataLayoutControl1;
            this.TenderProactiveDaysToReturnQuoteTextEdit.TabIndex = 83;
            // 
            // TenderReactiveDaysToReturnQuoteTextEdit
            // 
            this.TenderReactiveDaysToReturnQuoteTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "TenderReactiveDaysToReturnQuote", true));
            this.TenderReactiveDaysToReturnQuoteTextEdit.Location = new System.Drawing.Point(187, 284);
            this.TenderReactiveDaysToReturnQuoteTextEdit.MenuManager = this.barManager1;
            this.TenderReactiveDaysToReturnQuoteTextEdit.Name = "TenderReactiveDaysToReturnQuoteTextEdit";
            this.TenderReactiveDaysToReturnQuoteTextEdit.Properties.Mask.EditMask = "n0";
            this.TenderReactiveDaysToReturnQuoteTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TenderReactiveDaysToReturnQuoteTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TenderReactiveDaysToReturnQuoteTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TenderReactiveDaysToReturnQuoteTextEdit, true);
            this.TenderReactiveDaysToReturnQuoteTextEdit.Size = new System.Drawing.Size(312, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TenderReactiveDaysToReturnQuoteTextEdit, optionsSpelling14);
            this.TenderReactiveDaysToReturnQuoteTextEdit.StyleController = this.dataLayoutControl1;
            this.TenderReactiveDaysToReturnQuoteTextEdit.TabIndex = 82;
            // 
            // SectorTypeTextEdit
            // 
            this.SectorTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "SectorType", true));
            this.SectorTypeTextEdit.Location = new System.Drawing.Point(151, 154);
            this.SectorTypeTextEdit.MenuManager = this.barManager1;
            this.SectorTypeTextEdit.Name = "SectorTypeTextEdit";
            this.SectorTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SectorTypeTextEdit, true);
            this.SectorTypeTextEdit.Size = new System.Drawing.Size(348, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SectorTypeTextEdit, optionsSpelling15);
            this.SectorTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.SectorTypeTextEdit.TabIndex = 81;
            // 
            // GCCompanyNameTextEdit
            // 
            this.GCCompanyNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "GCCompanyName", true));
            this.GCCompanyNameTextEdit.Location = new System.Drawing.Point(441, 142);
            this.GCCompanyNameTextEdit.MenuManager = this.barManager1;
            this.GCCompanyNameTextEdit.Name = "GCCompanyNameTextEdit";
            this.GCCompanyNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GCCompanyNameTextEdit, true);
            this.GCCompanyNameTextEdit.Size = new System.Drawing.Size(187, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GCCompanyNameTextEdit, optionsSpelling16);
            this.GCCompanyNameTextEdit.StyleController = this.dataLayoutControl1;
            this.GCCompanyNameTextEdit.TabIndex = 80;
            // 
            // LocalAuthorityWebSiteButtonEdit
            // 
            this.LocalAuthorityWebSiteButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "LocalAuthorityWebSite", true));
            this.LocalAuthorityWebSiteButtonEdit.Location = new System.Drawing.Point(1165, 433);
            this.LocalAuthorityWebSiteButtonEdit.MenuManager = this.barManager1;
            this.LocalAuthorityWebSiteButtonEdit.Name = "LocalAuthorityWebSiteButtonEdit";
            editorButtonImageOptions12.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions12.Image")));
            this.LocalAuthorityWebSiteButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Web Site", -1, true, true, false, editorButtonImageOptions12, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject45, serializableAppearanceObject46, serializableAppearanceObject47, serializableAppearanceObject48, "Go to Web Site", "website", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LocalAuthorityWebSiteButtonEdit.Properties.ReadOnly = true;
            this.LocalAuthorityWebSiteButtonEdit.Size = new System.Drawing.Size(155, 22);
            this.LocalAuthorityWebSiteButtonEdit.StyleController = this.dataLayoutControl1;
            this.LocalAuthorityWebSiteButtonEdit.TabIndex = 79;
            this.LocalAuthorityWebSiteButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LocalAuthorityWebSiteButtonEdit_ButtonClick);
            // 
            // LocalAuthorityTelephoneTextEdit
            // 
            this.LocalAuthorityTelephoneTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "LocalAuthorityTelephone", true));
            this.LocalAuthorityTelephoneTextEdit.Location = new System.Drawing.Point(1165, 457);
            this.LocalAuthorityTelephoneTextEdit.MenuManager = this.barManager1;
            this.LocalAuthorityTelephoneTextEdit.Name = "LocalAuthorityTelephoneTextEdit";
            this.LocalAuthorityTelephoneTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LocalAuthorityTelephoneTextEdit, true);
            this.LocalAuthorityTelephoneTextEdit.Size = new System.Drawing.Size(155, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LocalAuthorityTelephoneTextEdit, optionsSpelling17);
            this.LocalAuthorityTelephoneTextEdit.StyleController = this.dataLayoutControl1;
            this.LocalAuthorityTelephoneTextEdit.TabIndex = 78;
            // 
            // ContactPersonTitleTextEdit
            // 
            this.ContactPersonTitleTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ContactPersonTitle", true));
            this.ContactPersonTitleTextEdit.Location = new System.Drawing.Point(440, 292);
            this.ContactPersonTitleTextEdit.MenuManager = this.barManager1;
            this.ContactPersonTitleTextEdit.Name = "ContactPersonTitleTextEdit";
            this.ContactPersonTitleTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContactPersonTitleTextEdit, true);
            this.ContactPersonTitleTextEdit.Size = new System.Drawing.Size(176, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContactPersonTitleTextEdit, optionsSpelling18);
            this.ContactPersonTitleTextEdit.StyleController = this.dataLayoutControl1;
            this.ContactPersonTitleTextEdit.TabIndex = 76;
            // 
            // ContactPersonPositionTextEdit
            // 
            this.ContactPersonPositionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ContactPersonPosition", true));
            this.ContactPersonPositionTextEdit.Location = new System.Drawing.Point(181, 292);
            this.ContactPersonPositionTextEdit.MenuManager = this.barManager1;
            this.ContactPersonPositionTextEdit.Name = "ContactPersonPositionTextEdit";
            this.ContactPersonPositionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContactPersonPositionTextEdit, true);
            this.ContactPersonPositionTextEdit.Size = new System.Drawing.Size(110, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContactPersonPositionTextEdit, optionsSpelling19);
            this.ContactPersonPositionTextEdit.StyleController = this.dataLayoutControl1;
            this.ContactPersonPositionTextEdit.TabIndex = 75;
            // 
            // SiteCodeButtonEdit
            // 
            this.SiteCodeButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "SiteCode", true));
            this.SiteCodeButtonEdit.Location = new System.Drawing.Point(441, 420);
            this.SiteCodeButtonEdit.MenuManager = this.barManager1;
            this.SiteCodeButtonEdit.Name = "SiteCodeButtonEdit";
            editorButtonImageOptions13.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions13.Image")));
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "View On Map - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to view the Site Location on the map.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.SiteCodeButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View on Map", -1, true, true, false, editorButtonImageOptions13, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject49, serializableAppearanceObject50, serializableAppearanceObject51, serializableAppearanceObject52, "", null, superToolTip5, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SiteCodeButtonEdit.Properties.ReadOnly = true;
            this.SiteCodeButtonEdit.Size = new System.Drawing.Size(175, 22);
            this.SiteCodeButtonEdit.StyleController = this.dataLayoutControl1;
            this.SiteCodeButtonEdit.TabIndex = 14;
            this.SiteCodeButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SiteCodeButtonEdit_ButtonClick);
            // 
            // PlanningAuthorityOkToProceedCheckEdit
            // 
            this.PlanningAuthorityOkToProceedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "PlanningAuthorityOkToProceed", true));
            this.PlanningAuthorityOkToProceedCheckEdit.Location = new System.Drawing.Point(807, 600);
            this.PlanningAuthorityOkToProceedCheckEdit.MenuManager = this.barManager1;
            this.PlanningAuthorityOkToProceedCheckEdit.Name = "PlanningAuthorityOkToProceedCheckEdit";
            this.PlanningAuthorityOkToProceedCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.PlanningAuthorityOkToProceedCheckEdit.Properties.ValueChecked = 1;
            this.PlanningAuthorityOkToProceedCheckEdit.Properties.ValueUnchecked = 0;
            this.PlanningAuthorityOkToProceedCheckEdit.Size = new System.Drawing.Size(513, 19);
            this.PlanningAuthorityOkToProceedCheckEdit.StyleController = this.dataLayoutControl1;
            this.PlanningAuthorityOkToProceedCheckEdit.TabIndex = 74;
            this.PlanningAuthorityOkToProceedCheckEdit.EditValueChanged += new System.EventHandler(this.PlanningAuthorityOkToProceedCheckEdit_EditValueChanged);
            this.PlanningAuthorityOkToProceedCheckEdit.Validated += new System.EventHandler(this.PlanningAuthorityOkToProceedCheckEdit_Validated);
            // 
            // PlanningAuthoritySubmittedDateDateEdit
            // 
            this.PlanningAuthoritySubmittedDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "PlanningAuthoritySubmittedDate", true));
            this.PlanningAuthoritySubmittedDateDateEdit.EditValue = null;
            this.PlanningAuthoritySubmittedDateDateEdit.Location = new System.Drawing.Point(1165, 552);
            this.PlanningAuthoritySubmittedDateDateEdit.MenuManager = this.barManager1;
            this.PlanningAuthoritySubmittedDateDateEdit.Name = "PlanningAuthoritySubmittedDateDateEdit";
            this.PlanningAuthoritySubmittedDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.PlanningAuthoritySubmittedDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PlanningAuthoritySubmittedDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PlanningAuthoritySubmittedDateDateEdit.Properties.Mask.EditMask = "g";
            this.PlanningAuthoritySubmittedDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.PlanningAuthoritySubmittedDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.PlanningAuthoritySubmittedDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.PlanningAuthoritySubmittedDateDateEdit.Size = new System.Drawing.Size(155, 20);
            this.PlanningAuthoritySubmittedDateDateEdit.StyleController = this.dataLayoutControl1;
            this.PlanningAuthoritySubmittedDateDateEdit.TabIndex = 18;
            this.PlanningAuthoritySubmittedDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.PlanningAuthoritySubmittedDateDateEdit_Validating);
            // 
            // PlanningAuthorityNumberTextEdit
            // 
            this.PlanningAuthorityNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "PlanningAuthorityNumber", true));
            this.PlanningAuthorityNumberTextEdit.Location = new System.Drawing.Point(807, 552);
            this.PlanningAuthorityNumberTextEdit.MenuManager = this.barManager1;
            this.PlanningAuthorityNumberTextEdit.Name = "PlanningAuthorityNumberTextEdit";
            this.PlanningAuthorityNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PlanningAuthorityNumberTextEdit, true);
            this.PlanningAuthorityNumberTextEdit.Size = new System.Drawing.Size(209, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PlanningAuthorityNumberTextEdit, optionsSpelling20);
            this.PlanningAuthorityNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.PlanningAuthorityNumberTextEdit.TabIndex = 73;
            // 
            // TreeProtectedCheckEdit
            // 
            this.TreeProtectedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "TreeProtected", true));
            this.TreeProtectedCheckEdit.Location = new System.Drawing.Point(807, 481);
            this.TreeProtectedCheckEdit.MenuManager = this.barManager1;
            this.TreeProtectedCheckEdit.Name = "TreeProtectedCheckEdit";
            this.TreeProtectedCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.TreeProtectedCheckEdit.Properties.ValueChecked = 1;
            this.TreeProtectedCheckEdit.Properties.ValueUnchecked = 0;
            this.TreeProtectedCheckEdit.Size = new System.Drawing.Size(513, 19);
            this.TreeProtectedCheckEdit.StyleController = this.dataLayoutControl1;
            this.TreeProtectedCheckEdit.TabIndex = 72;
            this.TreeProtectedCheckEdit.EditValueChanged += new System.EventHandler(this.TreeProtectedCheckEdit_EditValueChanged);
            this.TreeProtectedCheckEdit.Validated += new System.EventHandler(this.TreeProtectedCheckEdit_Validated);
            // 
            // TPOCheckedDateDateEdit
            // 
            this.TPOCheckedDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "TPOCheckedDate", true));
            this.TPOCheckedDateDateEdit.EditValue = null;
            this.TPOCheckedDateDateEdit.Location = new System.Drawing.Point(807, 457);
            this.TPOCheckedDateDateEdit.MenuManager = this.barManager1;
            this.TPOCheckedDateDateEdit.Name = "TPOCheckedDateDateEdit";
            this.TPOCheckedDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TPOCheckedDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TPOCheckedDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TPOCheckedDateDateEdit.Properties.Mask.EditMask = "g";
            this.TPOCheckedDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TPOCheckedDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.TPOCheckedDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.TPOCheckedDateDateEdit.Size = new System.Drawing.Size(209, 20);
            this.TPOCheckedDateDateEdit.StyleController = this.dataLayoutControl1;
            this.TPOCheckedDateDateEdit.TabIndex = 17;
            this.TPOCheckedDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.TPOCheckedDateDateEdit_Validating);
            // 
            // PlanningAuthorityButtonEdit
            // 
            this.PlanningAuthorityButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "PlanningAuthority", true));
            this.PlanningAuthorityButtonEdit.EditValue = "";
            this.PlanningAuthorityButtonEdit.Location = new System.Drawing.Point(807, 433);
            this.PlanningAuthorityButtonEdit.MenuManager = this.barManager1;
            this.PlanningAuthorityButtonEdit.Name = "PlanningAuthorityButtonEdit";
            this.PlanningAuthorityButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions14, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject53, serializableAppearanceObject54, serializableAppearanceObject55, serializableAppearanceObject56, "Click me to open the Select Local Authority screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions15, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject57, serializableAppearanceObject58, serializableAppearanceObject59, serializableAppearanceObject60, "Click me to clear value", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.PlanningAuthorityButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.PlanningAuthorityButtonEdit.Size = new System.Drawing.Size(209, 20);
            this.PlanningAuthorityButtonEdit.StyleController = this.dataLayoutControl1;
            this.PlanningAuthorityButtonEdit.TabIndex = 14;
            this.PlanningAuthorityButtonEdit.TabStop = false;
            this.PlanningAuthorityButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.PlanningAuthorityButtonEdit_ButtonClick);
            // 
            // PlanningAuthorityIDTextEdit
            // 
            this.PlanningAuthorityIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "PlanningAuthorityID", true));
            this.PlanningAuthorityIDTextEdit.Location = new System.Drawing.Point(172, 345);
            this.PlanningAuthorityIDTextEdit.MenuManager = this.barManager1;
            this.PlanningAuthorityIDTextEdit.Name = "PlanningAuthorityIDTextEdit";
            this.PlanningAuthorityIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PlanningAuthorityIDTextEdit, true);
            this.PlanningAuthorityIDTextEdit.Size = new System.Drawing.Size(478, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PlanningAuthorityIDTextEdit, optionsSpelling21);
            this.PlanningAuthorityIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PlanningAuthorityIDTextEdit.TabIndex = 72;
            // 
            // TPORequiredCheckEdit
            // 
            this.TPORequiredCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "TPORequired", true));
            this.TPORequiredCheckEdit.Location = new System.Drawing.Point(807, 409);
            this.TPORequiredCheckEdit.MenuManager = this.barManager1;
            this.TPORequiredCheckEdit.Name = "TPORequiredCheckEdit";
            this.TPORequiredCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.TPORequiredCheckEdit.Properties.ValueChecked = 1;
            this.TPORequiredCheckEdit.Properties.ValueUnchecked = 0;
            this.TPORequiredCheckEdit.Size = new System.Drawing.Size(347, 19);
            this.TPORequiredCheckEdit.StyleController = this.dataLayoutControl1;
            this.TPORequiredCheckEdit.TabIndex = 71;
            // 
            // ClientCommentsMemoEdit
            // 
            this.ClientCommentsMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ClientComments", true));
            this.ClientCommentsMemoEdit.Location = new System.Drawing.Point(819, 752);
            this.ClientCommentsMemoEdit.MenuManager = this.barManager1;
            this.ClientCommentsMemoEdit.Name = "ClientCommentsMemoEdit";
            this.ClientCommentsMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientCommentsMemoEdit, true);
            this.ClientCommentsMemoEdit.Size = new System.Drawing.Size(489, 29);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientCommentsMemoEdit, optionsSpelling22);
            this.ClientCommentsMemoEdit.StyleController = this.dataLayoutControl1;
            this.ClientCommentsMemoEdit.TabIndex = 50;
            // 
            // CMCommentsMemoEdit
            // 
            this.CMCommentsMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "CMComments", true));
            this.CMCommentsMemoEdit.Location = new System.Drawing.Point(819, 720);
            this.CMCommentsMemoEdit.MenuManager = this.barManager1;
            this.CMCommentsMemoEdit.Name = "CMCommentsMemoEdit";
            this.CMCommentsMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CMCommentsMemoEdit, true);
            this.CMCommentsMemoEdit.Size = new System.Drawing.Size(489, 28);
            this.scSpellChecker.SetSpellCheckerOptions(this.CMCommentsMemoEdit, optionsSpelling23);
            this.CMCommentsMemoEdit.StyleController = this.dataLayoutControl1;
            this.CMCommentsMemoEdit.TabIndex = 49;
            // 
            // ClientPONumberTextEdit
            // 
            this.ClientPONumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ClientPONumber", true));
            this.ClientPONumberTextEdit.Location = new System.Drawing.Point(181, 234);
            this.ClientPONumberTextEdit.MenuManager = this.barManager1;
            this.ClientPONumberTextEdit.Name = "ClientPONumberTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientPONumberTextEdit, true);
            this.ClientPONumberTextEdit.Size = new System.Drawing.Size(83, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientPONumberTextEdit, optionsSpelling24);
            this.ClientPONumberTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientPONumberTextEdit.TabIndex = 70;
            this.ClientPONumberTextEdit.EditValueChanged += new System.EventHandler(this.ClientPONumberTextEdit_EditValueChanged);
            this.ClientPONumberTextEdit.Validated += new System.EventHandler(this.ClientPONumberTextEdit_Validated);
            // 
            // RequiredWorkCompletedDateDateEdit
            // 
            this.RequiredWorkCompletedDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "RequiredWorkCompletedDate", true));
            this.RequiredWorkCompletedDateDateEdit.EditValue = null;
            this.RequiredWorkCompletedDateDateEdit.Location = new System.Drawing.Point(1138, 117);
            this.RequiredWorkCompletedDateDateEdit.MenuManager = this.barManager1;
            this.RequiredWorkCompletedDateDateEdit.Name = "RequiredWorkCompletedDateDateEdit";
            this.RequiredWorkCompletedDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.RequiredWorkCompletedDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RequiredWorkCompletedDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RequiredWorkCompletedDateDateEdit.Properties.Mask.EditMask = "g";
            this.RequiredWorkCompletedDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.RequiredWorkCompletedDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.RequiredWorkCompletedDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.RequiredWorkCompletedDateDateEdit.Size = new System.Drawing.Size(182, 20);
            this.RequiredWorkCompletedDateDateEdit.StyleController = this.dataLayoutControl1;
            this.RequiredWorkCompletedDateDateEdit.TabIndex = 16;
            // 
            // QuoteAcceptedByClientDateDateEdit
            // 
            this.QuoteAcceptedByClientDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "QuoteAcceptedByClientDate", true));
            this.QuoteAcceptedByClientDateDateEdit.EditValue = null;
            this.QuoteAcceptedByClientDateDateEdit.Location = new System.Drawing.Point(464, 756);
            this.QuoteAcceptedByClientDateDateEdit.MenuManager = this.barManager1;
            this.QuoteAcceptedByClientDateDateEdit.Name = "QuoteAcceptedByClientDateDateEdit";
            this.QuoteAcceptedByClientDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.QuoteAcceptedByClientDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.QuoteAcceptedByClientDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.QuoteAcceptedByClientDateDateEdit.Properties.Mask.EditMask = "g";
            this.QuoteAcceptedByClientDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.QuoteAcceptedByClientDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.QuoteAcceptedByClientDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.QuoteAcceptedByClientDateDateEdit.Properties.ReadOnly = true;
            this.QuoteAcceptedByClientDateDateEdit.Size = new System.Drawing.Size(164, 20);
            this.QuoteAcceptedByClientDateDateEdit.StyleController = this.dataLayoutControl1;
            this.QuoteAcceptedByClientDateDateEdit.TabIndex = 15;
            this.QuoteAcceptedByClientDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.QuoteAcceptedByClientDateDateEdit_Validating);
            // 
            // QuoteSubmittedToClientDateDateEdit
            // 
            this.QuoteSubmittedToClientDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "QuoteSubmittedToClientDate", true));
            this.QuoteSubmittedToClientDateDateEdit.EditValue = null;
            this.QuoteSubmittedToClientDateDateEdit.Location = new System.Drawing.Point(464, 729);
            this.QuoteSubmittedToClientDateDateEdit.MenuManager = this.barManager1;
            this.QuoteSubmittedToClientDateDateEdit.Name = "QuoteSubmittedToClientDateDateEdit";
            this.QuoteSubmittedToClientDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.QuoteSubmittedToClientDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.QuoteSubmittedToClientDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.QuoteSubmittedToClientDateDateEdit.Properties.Mask.EditMask = "g";
            this.QuoteSubmittedToClientDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.QuoteSubmittedToClientDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.QuoteSubmittedToClientDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.QuoteSubmittedToClientDateDateEdit.Size = new System.Drawing.Size(164, 20);
            this.QuoteSubmittedToClientDateDateEdit.StyleController = this.dataLayoutControl1;
            this.QuoteSubmittedToClientDateDateEdit.TabIndex = 14;
            this.QuoteSubmittedToClientDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.QuoteSubmittedToClientDateDateEdit_Validating);
            // 
            // CMInitialAttendanceDateDateEdit
            // 
            this.CMInitialAttendanceDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "CMInitialAttendanceDate", true));
            this.CMInitialAttendanceDateDateEdit.EditValue = null;
            this.CMInitialAttendanceDateDateEdit.Location = new System.Drawing.Point(1138, 93);
            this.CMInitialAttendanceDateDateEdit.MenuManager = this.barManager1;
            this.CMInitialAttendanceDateDateEdit.Name = "CMInitialAttendanceDateDateEdit";
            this.CMInitialAttendanceDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.CMInitialAttendanceDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CMInitialAttendanceDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CMInitialAttendanceDateDateEdit.Properties.Mask.EditMask = "g";
            this.CMInitialAttendanceDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CMInitialAttendanceDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.CMInitialAttendanceDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.CMInitialAttendanceDateDateEdit.Size = new System.Drawing.Size(182, 20);
            this.CMInitialAttendanceDateDateEdit.StyleController = this.dataLayoutControl1;
            this.CMInitialAttendanceDateDateEdit.TabIndex = 13;
            // 
            // SiteLocationYTextEdit
            // 
            this.SiteLocationYTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "SiteLocationY", true));
            this.SiteLocationYTextEdit.Location = new System.Drawing.Point(441, 444);
            this.SiteLocationYTextEdit.MenuManager = this.barManager1;
            this.SiteLocationYTextEdit.Name = "SiteLocationYTextEdit";
            this.SiteLocationYTextEdit.Properties.Mask.EditMask = "n8";
            this.SiteLocationYTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.SiteLocationYTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SiteLocationYTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteLocationYTextEdit, true);
            this.SiteLocationYTextEdit.Size = new System.Drawing.Size(175, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteLocationYTextEdit, optionsSpelling25);
            this.SiteLocationYTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteLocationYTextEdit.TabIndex = 69;
            // 
            // SiteLocationXTextEdit
            // 
            this.SiteLocationXTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "SiteLocationX", true));
            this.SiteLocationXTextEdit.Location = new System.Drawing.Point(181, 444);
            this.SiteLocationXTextEdit.MenuManager = this.barManager1;
            this.SiteLocationXTextEdit.Name = "SiteLocationXTextEdit";
            this.SiteLocationXTextEdit.Properties.Mask.EditMask = "n8";
            this.SiteLocationXTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.SiteLocationXTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SiteLocationXTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteLocationXTextEdit, true);
            this.SiteLocationXTextEdit.Size = new System.Drawing.Size(111, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteLocationXTextEdit, optionsSpelling26);
            this.SiteLocationXTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteLocationXTextEdit.TabIndex = 68;
            // 
            // SitePostcodeTextEdit
            // 
            this.SitePostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "SitePostcode", true));
            this.SitePostcodeTextEdit.Location = new System.Drawing.Point(181, 420);
            this.SitePostcodeTextEdit.MenuManager = this.barManager1;
            this.SitePostcodeTextEdit.Name = "SitePostcodeTextEdit";
            this.SitePostcodeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SitePostcodeTextEdit, true);
            this.SitePostcodeTextEdit.Size = new System.Drawing.Size(111, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SitePostcodeTextEdit, optionsSpelling27);
            this.SitePostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.SitePostcodeTextEdit.TabIndex = 67;
            // 
            // SiteAddressMemoEdit
            // 
            this.SiteAddressMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "SiteAddress", true));
            this.SiteAddressMemoEdit.Location = new System.Drawing.Point(441, 396);
            this.SiteAddressMemoEdit.MenuManager = this.barManager1;
            this.SiteAddressMemoEdit.Name = "SiteAddressMemoEdit";
            this.SiteAddressMemoEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteAddressMemoEdit, true);
            this.SiteAddressMemoEdit.Size = new System.Drawing.Size(175, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteAddressMemoEdit, optionsSpelling28);
            this.SiteAddressMemoEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddressMemoEdit.TabIndex = 66;
            // 
            // SiteNameButtonEdit
            // 
            this.SiteNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "SiteName", true));
            this.SiteNameButtonEdit.EditValue = "";
            this.SiteNameButtonEdit.Location = new System.Drawing.Point(181, 396);
            this.SiteNameButtonEdit.MenuManager = this.barManager1;
            this.SiteNameButtonEdit.Name = "SiteNameButtonEdit";
            editorButtonImageOptions18.Image = global::WoodPlan5.Properties.Resources.Add_16x16;
            this.SiteNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject61, serializableAppearanceObject62, serializableAppearanceObject63, serializableAppearanceObject64, "Click me to open the Select Client screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions17, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject65, serializableAppearanceObject66, serializableAppearanceObject67, serializableAppearanceObject68, "Click me to clear value", "clear", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Add", -1, true, true, false, editorButtonImageOptions18, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject69, serializableAppearanceObject70, serializableAppearanceObject71, serializableAppearanceObject72, "Click me to Add new Site", "add", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SiteNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.SiteNameButtonEdit.Size = new System.Drawing.Size(111, 22);
            this.SiteNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.SiteNameButtonEdit.TabIndex = 13;
            this.SiteNameButtonEdit.TabStop = false;
            this.SiteNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SiteNameButtonEdit_ButtonClick);
            // 
            // TenderGroupIDTextEdit
            // 
            this.TenderGroupIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "TenderGroupID", true));
            this.TenderGroupIDTextEdit.Location = new System.Drawing.Point(176, 415);
            this.TenderGroupIDTextEdit.MenuManager = this.barManager1;
            this.TenderGroupIDTextEdit.Name = "TenderGroupIDTextEdit";
            this.TenderGroupIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TenderGroupIDTextEdit, true);
            this.TenderGroupIDTextEdit.Size = new System.Drawing.Size(1108, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TenderGroupIDTextEdit, optionsSpelling29);
            this.TenderGroupIDTextEdit.StyleController = this.dataLayoutControl1;
            this.TenderGroupIDTextEdit.TabIndex = 64;
            // 
            // TenderGroupDescriptionButtonEdit
            // 
            this.TenderGroupDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "TenderGroupDescription", true));
            this.TenderGroupDescriptionButtonEdit.EditValue = "";
            this.TenderGroupDescriptionButtonEdit.Location = new System.Drawing.Point(169, 142);
            this.TenderGroupDescriptionButtonEdit.MenuManager = this.barManager1;
            this.TenderGroupDescriptionButtonEdit.Name = "TenderGroupDescriptionButtonEdit";
            editorButtonImageOptions21.Image = global::WoodPlan5.Properties.Resources.Add_16x16;
            this.TenderGroupDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions19, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject73, serializableAppearanceObject74, serializableAppearanceObject75, serializableAppearanceObject76, "Click me to open the Select Tender Group screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions20, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject77, serializableAppearanceObject78, serializableAppearanceObject79, serializableAppearanceObject80, "Click me to clear value", "clear", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Add", -1, true, true, false, editorButtonImageOptions21, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject81, serializableAppearanceObject82, serializableAppearanceObject83, serializableAppearanceObject84, "Click me to add a new Tender Group", "add", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.TenderGroupDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.TenderGroupDescriptionButtonEdit.Size = new System.Drawing.Size(123, 22);
            this.TenderGroupDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.TenderGroupDescriptionButtonEdit.TabIndex = 15;
            this.TenderGroupDescriptionButtonEdit.TabStop = false;
            this.TenderGroupDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.TenderGroupDescriptionButtonEdit_ButtonClick);
            // 
            // JobTypeDescriptionTextEdit
            // 
            this.JobTypeDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "JobTypeDescription", true));
            this.JobTypeDescriptionTextEdit.Location = new System.Drawing.Point(169, 534);
            this.JobTypeDescriptionTextEdit.MenuManager = this.barManager1;
            this.JobTypeDescriptionTextEdit.Name = "JobTypeDescriptionTextEdit";
            this.JobTypeDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobTypeDescriptionTextEdit, true);
            this.JobTypeDescriptionTextEdit.Size = new System.Drawing.Size(123, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobTypeDescriptionTextEdit, optionsSpelling30);
            this.JobTypeDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.JobTypeDescriptionTextEdit.TabIndex = 63;
            // 
            // JobTypeIDTextEdit
            // 
            this.JobTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "JobTypeID", true));
            this.JobTypeIDTextEdit.Location = new System.Drawing.Point(176, 365);
            this.JobTypeIDTextEdit.MenuManager = this.barManager1;
            this.JobTypeIDTextEdit.Name = "JobTypeIDTextEdit";
            this.JobTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobTypeIDTextEdit, true);
            this.JobTypeIDTextEdit.Size = new System.Drawing.Size(1108, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobTypeIDTextEdit, optionsSpelling31);
            this.JobTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.JobTypeIDTextEdit.TabIndex = 62;
            // 
            // JobSubTypeDescriptionButtonEdit
            // 
            this.JobSubTypeDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "JobSubTypeDescription", true));
            this.JobSubTypeDescriptionButtonEdit.EditValue = "";
            this.JobSubTypeDescriptionButtonEdit.Location = new System.Drawing.Point(441, 534);
            this.JobSubTypeDescriptionButtonEdit.MenuManager = this.barManager1;
            this.JobSubTypeDescriptionButtonEdit.Name = "JobSubTypeDescriptionButtonEdit";
            this.JobSubTypeDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions22, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject85, serializableAppearanceObject86, serializableAppearanceObject87, serializableAppearanceObject88, "Click me to open the Select Job Sub-Type screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions23, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject89, serializableAppearanceObject90, serializableAppearanceObject91, serializableAppearanceObject92, "Click me to clear value", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.JobSubTypeDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.JobSubTypeDescriptionButtonEdit.Size = new System.Drawing.Size(187, 20);
            this.JobSubTypeDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.JobSubTypeDescriptionButtonEdit.TabIndex = 14;
            this.JobSubTypeDescriptionButtonEdit.TabStop = false;
            this.JobSubTypeDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.JobSubTypeDescriptionButtonEdit_ButtonClick);
            this.JobSubTypeDescriptionButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.JobSubTypeDescriptionButtonEdit_Validating);
            // 
            // WorkSubTypeIDTextEdit
            // 
            this.WorkSubTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "WorkSubTypeID", true));
            this.WorkSubTypeIDTextEdit.Location = new System.Drawing.Point(164, 211);
            this.WorkSubTypeIDTextEdit.MenuManager = this.barManager1;
            this.WorkSubTypeIDTextEdit.Name = "WorkSubTypeIDTextEdit";
            this.WorkSubTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WorkSubTypeIDTextEdit, true);
            this.WorkSubTypeIDTextEdit.Size = new System.Drawing.Size(1132, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WorkSubTypeIDTextEdit, optionsSpelling32);
            this.WorkSubTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkSubTypeIDTextEdit.TabIndex = 61;
            // 
            // CMNameButtonEdit
            // 
            this.CMNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "CMName", true));
            this.CMNameButtonEdit.Location = new System.Drawing.Point(441, 490);
            this.CMNameButtonEdit.MenuManager = this.barManager1;
            this.CMNameButtonEdit.Name = "CMNameButtonEdit";
            this.CMNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions24, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject93, serializableAppearanceObject94, serializableAppearanceObject95, serializableAppearanceObject96, "Click to open Select Staff screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions25, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject97, serializableAppearanceObject98, serializableAppearanceObject99, serializableAppearanceObject100, "Click me to clear value", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.CMNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.CMNameButtonEdit.Size = new System.Drawing.Size(187, 20);
            this.CMNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.CMNameButtonEdit.TabIndex = 14;
            this.CMNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.CMNameButtonEdit_ButtonClick);
            this.CMNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.CMNameButtonEdit_Validating);
            // 
            // CMTextEdit
            // 
            this.CMTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "CM", true));
            this.CMTextEdit.Location = new System.Drawing.Point(164, 235);
            this.CMTextEdit.MenuManager = this.barManager1;
            this.CMTextEdit.Name = "CMTextEdit";
            this.CMTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CMTextEdit, true);
            this.CMTextEdit.Size = new System.Drawing.Size(493, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CMTextEdit, optionsSpelling33);
            this.CMTextEdit.StyleController = this.dataLayoutControl1;
            this.CMTextEdit.TabIndex = 60;
            // 
            // KAMTextEdit
            // 
            this.KAMTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "KAM", true));
            this.KAMTextEdit.Location = new System.Drawing.Point(172, 387);
            this.KAMTextEdit.MenuManager = this.barManager1;
            this.KAMTextEdit.Name = "KAMTextEdit";
            this.KAMTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.KAMTextEdit, true);
            this.KAMTextEdit.Size = new System.Drawing.Size(478, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.KAMTextEdit, optionsSpelling34);
            this.KAMTextEdit.StyleController = this.dataLayoutControl1;
            this.KAMTextEdit.TabIndex = 59;
            // 
            // KAMNameButtonEdit
            // 
            this.KAMNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "KAMName", true));
            this.KAMNameButtonEdit.Location = new System.Drawing.Point(169, 490);
            this.KAMNameButtonEdit.MenuManager = this.barManager1;
            this.KAMNameButtonEdit.Name = "KAMNameButtonEdit";
            this.KAMNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions26, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject101, serializableAppearanceObject102, serializableAppearanceObject103, serializableAppearanceObject104, "Click to open Select Staff screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions27, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject105, serializableAppearanceObject106, serializableAppearanceObject107, serializableAppearanceObject108, "Click me to clear value", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.KAMNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.KAMNameButtonEdit.Size = new System.Drawing.Size(123, 20);
            this.KAMNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.KAMNameButtonEdit.TabIndex = 13;
            this.KAMNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.KAMNameButtonEdit_ButtonClick);
            this.KAMNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.KAMNameButtonEdit_Validating);
            // 
            // CreatedByStaffNameTextEdit
            // 
            this.CreatedByStaffNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "CreatedByStaffName", true));
            this.CreatedByStaffNameTextEdit.Location = new System.Drawing.Point(169, 118);
            this.CreatedByStaffNameTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffNameTextEdit.Name = "CreatedByStaffNameTextEdit";
            this.CreatedByStaffNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffNameTextEdit, true);
            this.CreatedByStaffNameTextEdit.Size = new System.Drawing.Size(123, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffNameTextEdit, optionsSpelling35);
            this.CreatedByStaffNameTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffNameTextEdit.TabIndex = 58;
            // 
            // ClientContactIDTextEdit
            // 
            this.ClientContactIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ClientContactID", true));
            this.ClientContactIDTextEdit.Location = new System.Drawing.Point(164, 201);
            this.ClientContactIDTextEdit.MenuManager = this.barManager1;
            this.ClientContactIDTextEdit.Name = "ClientContactIDTextEdit";
            this.ClientContactIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientContactIDTextEdit, true);
            this.ClientContactIDTextEdit.Size = new System.Drawing.Size(493, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientContactIDTextEdit, optionsSpelling36);
            this.ClientContactIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientContactIDTextEdit.TabIndex = 57;
            // 
            // ContactPersonNameButtonEdit
            // 
            this.ContactPersonNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ContactPersonName", true));
            this.ContactPersonNameButtonEdit.EditValue = "";
            this.ContactPersonNameButtonEdit.Location = new System.Drawing.Point(181, 268);
            this.ContactPersonNameButtonEdit.MenuManager = this.barManager1;
            this.ContactPersonNameButtonEdit.Name = "ContactPersonNameButtonEdit";
            editorButtonImageOptions30.Image = global::WoodPlan5.Properties.Resources.Add_16x16;
            this.ContactPersonNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions28, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject109, serializableAppearanceObject110, serializableAppearanceObject111, serializableAppearanceObject112, "Click me to open the Select Client Contact Person screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions29, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject113, serializableAppearanceObject114, serializableAppearanceObject115, serializableAppearanceObject116, "Click me to clear value", "clear", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Add", -1, true, true, false, editorButtonImageOptions30, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject117, serializableAppearanceObject118, serializableAppearanceObject119, serializableAppearanceObject120, "Click me to add a new Client Contact Person", "add", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ContactPersonNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ContactPersonNameButtonEdit.Size = new System.Drawing.Size(110, 22);
            this.ContactPersonNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.ContactPersonNameButtonEdit.TabIndex = 13;
            this.ContactPersonNameButtonEdit.TabStop = false;
            this.ContactPersonNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ContactPersonNameButtonEdit_ButtonClick);
            // 
            // CreatedByStaffIDTextEdit
            // 
            this.CreatedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "CreatedByStaffID", true));
            this.CreatedByStaffIDTextEdit.Location = new System.Drawing.Point(188, 353);
            this.CreatedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffIDTextEdit.Name = "CreatedByStaffIDTextEdit";
            this.CreatedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffIDTextEdit, true);
            this.CreatedByStaffIDTextEdit.Size = new System.Drawing.Size(445, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffIDTextEdit, optionsSpelling37);
            this.CreatedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffIDTextEdit.TabIndex = 56;
            // 
            // TenderDescriptionMemoEdit
            // 
            this.TenderDescriptionMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "TenderDescription", true));
            this.TenderDescriptionMemoEdit.Location = new System.Drawing.Point(441, 70);
            this.TenderDescriptionMemoEdit.MenuManager = this.barManager1;
            this.TenderDescriptionMemoEdit.Name = "TenderDescriptionMemoEdit";
            this.TenderDescriptionMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TenderDescriptionMemoEdit, true);
            this.TenderDescriptionMemoEdit.Size = new System.Drawing.Size(187, 68);
            this.scSpellChecker.SetSpellCheckerOptions(this.TenderDescriptionMemoEdit, optionsSpelling38);
            this.TenderDescriptionMemoEdit.StyleController = this.dataLayoutControl1;
            this.TenderDescriptionMemoEdit.TabIndex = 55;
            this.TenderDescriptionMemoEdit.Validating += new System.ComponentModel.CancelEventHandler(this.TenderDescriptionMemoEdit_Validating);
            // 
            // StatusIDTextEdit
            // 
            this.StatusIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "StatusID", true));
            this.StatusIDTextEdit.Location = new System.Drawing.Point(188, 281);
            this.StatusIDTextEdit.MenuManager = this.barManager1;
            this.StatusIDTextEdit.Name = "StatusIDTextEdit";
            this.StatusIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StatusIDTextEdit, true);
            this.StatusIDTextEdit.Size = new System.Drawing.Size(445, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StatusIDTextEdit, optionsSpelling39);
            this.StatusIDTextEdit.StyleController = this.dataLayoutControl1;
            this.StatusIDTextEdit.TabIndex = 54;
            // 
            // TenderStatusButtonEdit
            // 
            this.TenderStatusButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "TenderStatus", true));
            this.TenderStatusButtonEdit.EditValue = "";
            this.TenderStatusButtonEdit.Location = new System.Drawing.Point(204, 628);
            this.TenderStatusButtonEdit.MenuManager = this.barManager1;
            this.TenderStatusButtonEdit.Name = "TenderStatusButtonEdit";
            this.TenderStatusButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions31, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject121, serializableAppearanceObject122, serializableAppearanceObject123, serializableAppearanceObject124, "Click me to open the Select Tender Status screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.TenderStatusButtonEdit.Properties.ReadOnly = true;
            this.TenderStatusButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.TenderStatusButtonEdit.Size = new System.Drawing.Size(111, 20);
            this.TenderStatusButtonEdit.StyleController = this.dataLayoutControl1;
            this.TenderStatusButtonEdit.TabIndex = 13;
            this.TenderStatusButtonEdit.TabStop = false;
            this.TenderStatusButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.TenderStatusButtonEdit_ButtonClick);
            this.TenderStatusButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.TenderStatusButtonEdit_Validating);
            // 
            // ClientReferenceNumberTextEdit
            // 
            this.ClientReferenceNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ClientReferenceNumber", true));
            this.ClientReferenceNumberTextEdit.Location = new System.Drawing.Point(441, 210);
            this.ClientReferenceNumberTextEdit.MenuManager = this.barManager1;
            this.ClientReferenceNumberTextEdit.Name = "ClientReferenceNumberTextEdit";
            this.ClientReferenceNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientReferenceNumberTextEdit, true);
            this.ClientReferenceNumberTextEdit.Size = new System.Drawing.Size(175, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientReferenceNumberTextEdit, optionsSpelling40);
            this.ClientReferenceNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientReferenceNumberTextEdit.TabIndex = 53;
            // 
            // SiteContractIDTextEdit
            // 
            this.SiteContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "SiteContractID", true));
            this.SiteContractIDTextEdit.Location = new System.Drawing.Point(164, 107);
            this.SiteContractIDTextEdit.MenuManager = this.barManager1;
            this.SiteContractIDTextEdit.Name = "SiteContractIDTextEdit";
            this.SiteContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteContractIDTextEdit, true);
            this.SiteContractIDTextEdit.Size = new System.Drawing.Size(493, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteContractIDTextEdit, optionsSpelling41);
            this.SiteContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteContractIDTextEdit.TabIndex = 52;
            // 
            // ClientContractIDTextEdit
            // 
            this.ClientContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ClientContractID", true));
            this.ClientContractIDTextEdit.Location = new System.Drawing.Point(164, 107);
            this.ClientContractIDTextEdit.MenuManager = this.barManager1;
            this.ClientContractIDTextEdit.Name = "ClientContractIDTextEdit";
            this.ClientContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientContractIDTextEdit, true);
            this.ClientContractIDTextEdit.Size = new System.Drawing.Size(493, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientContractIDTextEdit, optionsSpelling42);
            this.ClientContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientContractIDTextEdit.TabIndex = 51;
            // 
            // ContractDescriptionTextEdit
            // 
            this.ContractDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ContractDescription", true));
            this.ContractDescriptionTextEdit.Location = new System.Drawing.Point(169, 94);
            this.ContractDescriptionTextEdit.MenuManager = this.barManager1;
            this.ContractDescriptionTextEdit.Name = "ContractDescriptionTextEdit";
            this.ContractDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContractDescriptionTextEdit, true);
            this.ContractDescriptionTextEdit.Size = new System.Drawing.Size(123, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContractDescriptionTextEdit, optionsSpelling43);
            this.ContractDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.ContractDescriptionTextEdit.TabIndex = 50;
            // 
            // OurInternalCommentsMemoEdit
            // 
            this.OurInternalCommentsMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "OurInternalComments", true));
            this.OurInternalCommentsMemoEdit.Location = new System.Drawing.Point(819, 681);
            this.OurInternalCommentsMemoEdit.MenuManager = this.barManager1;
            this.OurInternalCommentsMemoEdit.Name = "OurInternalCommentsMemoEdit";
            this.OurInternalCommentsMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.OurInternalCommentsMemoEdit, true);
            this.OurInternalCommentsMemoEdit.Size = new System.Drawing.Size(489, 35);
            this.scSpellChecker.SetSpellCheckerOptions(this.OurInternalCommentsMemoEdit, optionsSpelling44);
            this.OurInternalCommentsMemoEdit.StyleController = this.dataLayoutControl1;
            this.OurInternalCommentsMemoEdit.TabIndex = 48;
            // 
            // ReactiveCheckEdit
            // 
            this.ReactiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "Reactive", true));
            this.ReactiveCheckEdit.Location = new System.Drawing.Point(807, 70);
            this.ReactiveCheckEdit.MenuManager = this.barManager1;
            this.ReactiveCheckEdit.Name = "ReactiveCheckEdit";
            this.ReactiveCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.ReactiveCheckEdit.Properties.ValueChecked = 1;
            this.ReactiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ReactiveCheckEdit.Size = new System.Drawing.Size(228, 19);
            this.ReactiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ReactiveCheckEdit.TabIndex = 7;
            this.ReactiveCheckEdit.EditValueChanged += new System.EventHandler(this.ReactiveCheckEdit_EditValueChanged);
            this.ReactiveCheckEdit.Validated += new System.EventHandler(this.ReactiveCheckEdit_Validated);
            // 
            // QuotedLabourSellSpinEdit
            // 
            this.QuotedLabourSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "QuotedLabourSell", true));
            this.QuotedLabourSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.QuotedLabourSellSpinEdit.Location = new System.Drawing.Point(731, 245);
            this.QuotedLabourSellSpinEdit.MenuManager = this.barManager1;
            this.QuotedLabourSellSpinEdit.Name = "QuotedLabourSellSpinEdit";
            this.QuotedLabourSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.QuotedLabourSellSpinEdit.Properties.Mask.EditMask = "c";
            this.QuotedLabourSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.QuotedLabourSellSpinEdit.Size = new System.Drawing.Size(93, 20);
            this.QuotedLabourSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.QuotedLabourSellSpinEdit.TabIndex = 11;
            this.QuotedLabourSellSpinEdit.EditValueChanged += new System.EventHandler(this.QuotedLabourSellSpinEdit_EditValueChanged);
            this.QuotedLabourSellSpinEdit.Validated += new System.EventHandler(this.QuotedLabourSellSpinEdit_Validated);
            // 
            // QuoteNotRequiredCheckEdit
            // 
            this.QuoteNotRequiredCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "QuoteNotRequired", true));
            this.QuoteNotRequiredCheckEdit.Location = new System.Drawing.Point(464, 678);
            this.QuoteNotRequiredCheckEdit.MenuManager = this.barManager1;
            this.QuoteNotRequiredCheckEdit.Name = "QuoteNotRequiredCheckEdit";
            this.QuoteNotRequiredCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.QuoteNotRequiredCheckEdit.Properties.ReadOnly = true;
            this.QuoteNotRequiredCheckEdit.Properties.ValueChecked = 1;
            this.QuoteNotRequiredCheckEdit.Properties.ValueUnchecked = 0;
            this.QuoteNotRequiredCheckEdit.Size = new System.Drawing.Size(164, 19);
            this.QuoteNotRequiredCheckEdit.StyleController = this.dataLayoutControl1;
            this.QuoteNotRequiredCheckEdit.TabIndex = 10;
            // 
            // ReturnToClientByDateDateEdit
            // 
            this.ReturnToClientByDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ReturnToClientByDate", true));
            this.ReturnToClientByDateDateEdit.EditValue = null;
            this.ReturnToClientByDateDateEdit.Location = new System.Drawing.Point(807, 117);
            this.ReturnToClientByDateDateEdit.MenuManager = this.barManager1;
            this.ReturnToClientByDateDateEdit.Name = "ReturnToClientByDateDateEdit";
            this.ReturnToClientByDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ReturnToClientByDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ReturnToClientByDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ReturnToClientByDateDateEdit.Properties.Mask.EditMask = "g";
            this.ReturnToClientByDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ReturnToClientByDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.ReturnToClientByDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ReturnToClientByDateDateEdit.Size = new System.Drawing.Size(182, 20);
            this.ReturnToClientByDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ReturnToClientByDateDateEdit.TabIndex = 9;
            // 
            // RequestReceivedDateDateEdit
            // 
            this.RequestReceivedDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "RequestReceivedDate", true));
            this.RequestReceivedDateDateEdit.EditValue = null;
            this.RequestReceivedDateDateEdit.Location = new System.Drawing.Point(807, 93);
            this.RequestReceivedDateDateEdit.MenuManager = this.barManager1;
            this.RequestReceivedDateDateEdit.Name = "RequestReceivedDateDateEdit";
            this.RequestReceivedDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.RequestReceivedDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RequestReceivedDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RequestReceivedDateDateEdit.Properties.Mask.EditMask = "g";
            this.RequestReceivedDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.RequestReceivedDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.RequestReceivedDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.RequestReceivedDateDateEdit.Size = new System.Drawing.Size(182, 20);
            this.RequestReceivedDateDateEdit.StyleController = this.dataLayoutControl1;
            this.RequestReceivedDateDateEdit.TabIndex = 8;
            this.RequestReceivedDateDateEdit.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.RequestReceivedDateDateEdit_EditValueChanging);
            this.RequestReceivedDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.RequestReceivedDate_Validating);
            this.RequestReceivedDateDateEdit.Validated += new System.EventHandler(this.RequestReceivedDateDateEdit_Validated);
            // 
            // PlanningAuthorityOutcomeIDGridLookUpEdit
            // 
            this.PlanningAuthorityOutcomeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "PlanningAuthorityOutcomeID", true));
            this.PlanningAuthorityOutcomeIDGridLookUpEdit.Location = new System.Drawing.Point(807, 576);
            this.PlanningAuthorityOutcomeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.PlanningAuthorityOutcomeIDGridLookUpEdit.Name = "PlanningAuthorityOutcomeIDGridLookUpEdit";
            this.PlanningAuthorityOutcomeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PlanningAuthorityOutcomeIDGridLookUpEdit.Properties.DataSource = this.sp06503OMTenderPlanningOutcomesWithBlankBindingSource;
            this.PlanningAuthorityOutcomeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.PlanningAuthorityOutcomeIDGridLookUpEdit.Properties.NullText = "";
            this.PlanningAuthorityOutcomeIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.PlanningAuthorityOutcomeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.PlanningAuthorityOutcomeIDGridLookUpEdit.Size = new System.Drawing.Size(209, 20);
            this.PlanningAuthorityOutcomeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.PlanningAuthorityOutcomeIDGridLookUpEdit.TabIndex = 2;
            // 
            // sp06503OMTenderPlanningOutcomesWithBlankBindingSource
            // 
            this.sp06503OMTenderPlanningOutcomesWithBlankBindingSource.DataMember = "sp06503_OM_Tender_Planning_Outcomes_With_Blank";
            this.sp06503OMTenderPlanningOutcomesWithBlankBindingSource.DataSource = this.dataSet_OM_Tender;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule4.ApplyToRow = true;
            gridFormatRule4.Column = this.colID1;
            gridFormatRule4.Name = "Format0";
            formatConditionRuleValue4.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue4.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue4.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue4.Value1 = 0;
            gridFormatRule4.Rule = formatConditionRuleValue4;
            this.gridLookUpEdit1View.FormatRules.Add(gridFormatRule4);
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Planning Outcome";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "RecordOrder";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // TenderIDTextEdit
            // 
            this.TenderIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "TenderID", true));
            this.TenderIDTextEdit.Location = new System.Drawing.Point(244, 12);
            this.TenderIDTextEdit.MenuManager = this.barManager1;
            this.TenderIDTextEdit.Name = "TenderIDTextEdit";
            this.TenderIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TenderIDTextEdit, true);
            this.TenderIDTextEdit.Size = new System.Drawing.Size(103, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TenderIDTextEdit, optionsSpelling45);
            this.TenderIDTextEdit.StyleController = this.dataLayoutControl1;
            this.TenderIDTextEdit.TabIndex = 44;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp06498OMTenderEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(12, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(164, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // ClientNameButtonEdit
            // 
            this.ClientNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "ClientName", true));
            this.ClientNameButtonEdit.EditValue = "";
            this.ClientNameButtonEdit.Location = new System.Drawing.Point(181, 210);
            this.ClientNameButtonEdit.MenuManager = this.barManager1;
            this.ClientNameButtonEdit.Name = "ClientNameButtonEdit";
            editorButtonImageOptions33.Image = global::WoodPlan5.Properties.Resources.Add_16x16;
            this.ClientNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions32, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject125, serializableAppearanceObject126, serializableAppearanceObject127, serializableAppearanceObject128, "Click me to open the Select Client screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Add", -1, true, true, false, editorButtonImageOptions33, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject129, serializableAppearanceObject130, serializableAppearanceObject131, serializableAppearanceObject132, "Click me to add a new Client", "add", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ClientNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientNameButtonEdit.Size = new System.Drawing.Size(111, 22);
            this.ClientNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameButtonEdit.TabIndex = 0;
            this.ClientNameButtonEdit.TabStop = false;
            this.ClientNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientNameButtonEdit_ButtonClick);
            this.ClientNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ClientNameButtonEdit_Validating);
            // 
            // KAMQuoteRejectionReasonTextEdit
            // 
            this.KAMQuoteRejectionReasonTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "KAMQuoteRejectionReason", true));
            this.KAMQuoteRejectionReasonTextEdit.Location = new System.Drawing.Point(464, 704);
            this.KAMQuoteRejectionReasonTextEdit.MenuManager = this.barManager1;
            this.KAMQuoteRejectionReasonTextEdit.Name = "KAMQuoteRejectionReasonTextEdit";
            this.KAMQuoteRejectionReasonTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.KAMQuoteRejectionReasonTextEdit, true);
            this.KAMQuoteRejectionReasonTextEdit.Size = new System.Drawing.Size(164, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.KAMQuoteRejectionReasonTextEdit, optionsSpelling46);
            this.KAMQuoteRejectionReasonTextEdit.StyleController = this.dataLayoutControl1;
            this.KAMQuoteRejectionReasonTextEdit.TabIndex = 4;
            // 
            // KAMQuoteRejectionRemarksMemoEdit
            // 
            this.KAMQuoteRejectionRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06498OMTenderEditBindingSource, "KAMQuoteRejectionRemarks", true));
            this.KAMQuoteRejectionRemarksMemoEdit.Location = new System.Drawing.Point(819, 785);
            this.KAMQuoteRejectionRemarksMemoEdit.MenuManager = this.barManager1;
            this.KAMQuoteRejectionRemarksMemoEdit.Name = "KAMQuoteRejectionRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.KAMQuoteRejectionRemarksMemoEdit, true);
            this.KAMQuoteRejectionRemarksMemoEdit.Size = new System.Drawing.Size(489, 35);
            this.scSpellChecker.SetSpellCheckerOptions(this.KAMQuoteRejectionRemarksMemoEdit, optionsSpelling47);
            this.KAMQuoteRejectionRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.KAMQuoteRejectionRemarksMemoEdit.TabIndex = 112;
            // 
            // ItemForClientContractID
            // 
            this.ItemForClientContractID.Control = this.ClientContractIDTextEdit;
            this.ItemForClientContractID.CustomizationFormText = "Client Contract ID:";
            this.ItemForClientContractID.Location = new System.Drawing.Point(0, 95);
            this.ItemForClientContractID.Name = "ItemForClientContractID";
            this.ItemForClientContractID.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForClientContractID.Size = new System.Drawing.Size(649, 24);
            this.ItemForClientContractID.Text = "Client Contract ID:";
            this.ItemForClientContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteContractID
            // 
            this.ItemForSiteContractID.Control = this.SiteContractIDTextEdit;
            this.ItemForSiteContractID.CustomizationFormText = "Site Contract ID:";
            this.ItemForSiteContractID.Location = new System.Drawing.Point(0, 95);
            this.ItemForSiteContractID.Name = "ItemForSiteContractID";
            this.ItemForSiteContractID.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForSiteContractID.Size = new System.Drawing.Size(649, 24);
            this.ItemForSiteContractID.Text = "Site Contract ID:";
            this.ItemForSiteContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForStatusID
            // 
            this.ItemForStatusID.Control = this.StatusIDTextEdit;
            this.ItemForStatusID.CustomizationFormText = "Status ID:";
            this.ItemForStatusID.Location = new System.Drawing.Point(0, 24);
            this.ItemForStatusID.Name = "ItemForStatusID";
            this.ItemForStatusID.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForStatusID.Size = new System.Drawing.Size(601, 24);
            this.ItemForStatusID.Text = "Status ID:";
            this.ItemForStatusID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCreatedByStaffID
            // 
            this.ItemForCreatedByStaffID.Control = this.CreatedByStaffIDTextEdit;
            this.ItemForCreatedByStaffID.CustomizationFormText = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.Location = new System.Drawing.Point(0, 96);
            this.ItemForCreatedByStaffID.Name = "ItemForCreatedByStaffID";
            this.ItemForCreatedByStaffID.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForCreatedByStaffID.Size = new System.Drawing.Size(601, 24);
            this.ItemForCreatedByStaffID.Text = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientContactID
            // 
            this.ItemForClientContactID.Control = this.ClientContactIDTextEdit;
            this.ItemForClientContactID.CustomizationFormText = "Client Contact ID:";
            this.ItemForClientContactID.Location = new System.Drawing.Point(0, 189);
            this.ItemForClientContactID.Name = "ItemForClientContactID";
            this.ItemForClientContactID.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForClientContactID.Size = new System.Drawing.Size(649, 24);
            this.ItemForClientContactID.Text = "Client Contact ID:";
            this.ItemForClientContactID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCM
            // 
            this.ItemForCM.Control = this.CMTextEdit;
            this.ItemForCM.CustomizationFormText = "CM ID:";
            this.ItemForCM.Location = new System.Drawing.Point(0, 223);
            this.ItemForCM.Name = "ItemForCM";
            this.ItemForCM.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForCM.Size = new System.Drawing.Size(649, 24);
            this.ItemForCM.Text = "CM ID:";
            this.ItemForCM.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForWorkSubTypeID
            // 
            this.ItemForWorkSubTypeID.Control = this.WorkSubTypeIDTextEdit;
            this.ItemForWorkSubTypeID.CustomizationFormText = "Work Sub-Type ID:";
            this.ItemForWorkSubTypeID.Location = new System.Drawing.Point(0, 199);
            this.ItemForWorkSubTypeID.Name = "ItemForWorkSubTypeID";
            this.ItemForWorkSubTypeID.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForWorkSubTypeID.Size = new System.Drawing.Size(1288, 24);
            this.ItemForWorkSubTypeID.Text = "Work Sub-Type ID:";
            this.ItemForWorkSubTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobTypeID
            // 
            this.ItemForJobTypeID.Control = this.JobTypeIDTextEdit;
            this.ItemForJobTypeID.CustomizationFormText = "Job Type ID:";
            this.ItemForJobTypeID.Location = new System.Drawing.Point(0, 120);
            this.ItemForJobTypeID.Name = "ItemForJobTypeID";
            this.ItemForJobTypeID.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForJobTypeID.Size = new System.Drawing.Size(1264, 24);
            this.ItemForJobTypeID.Text = "Job Type ID:";
            this.ItemForJobTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForTenderGroupID
            // 
            this.ItemForTenderGroupID.Control = this.TenderGroupIDTextEdit;
            this.ItemForTenderGroupID.CustomizationFormText = "Tender Group ID:";
            this.ItemForTenderGroupID.Location = new System.Drawing.Point(0, 170);
            this.ItemForTenderGroupID.Name = "ItemForTenderGroupID";
            this.ItemForTenderGroupID.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForTenderGroupID.Size = new System.Drawing.Size(1264, 24);
            this.ItemForTenderGroupID.Text = "Tender Group ID:";
            this.ItemForTenderGroupID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteLocationX
            // 
            this.ItemForSiteLocationX.Control = this.SiteLocationXTextEdit;
            this.ItemForSiteLocationX.CustomizationFormText = "Site Location X:";
            this.ItemForSiteLocationX.Location = new System.Drawing.Point(0, 48);
            this.ItemForSiteLocationX.Name = "ItemForSiteLocationX";
            this.ItemForSiteLocationX.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForSiteLocationX.Size = new System.Drawing.Size(260, 24);
            this.ItemForSiteLocationX.Text = "Site Location X:";
            this.ItemForSiteLocationX.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForSiteLocationY
            // 
            this.ItemForSiteLocationY.Control = this.SiteLocationYTextEdit;
            this.ItemForSiteLocationY.CustomizationFormText = "Site Location Y:";
            this.ItemForSiteLocationY.Location = new System.Drawing.Point(260, 48);
            this.ItemForSiteLocationY.Name = "ItemForSiteLocationY";
            this.ItemForSiteLocationY.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForSiteLocationY.Size = new System.Drawing.Size(324, 24);
            this.ItemForSiteLocationY.Text = "Site Location Y:";
            this.ItemForSiteLocationY.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForPlanningAuthorityID
            // 
            this.ItemForPlanningAuthorityID.Control = this.PlanningAuthorityIDTextEdit;
            this.ItemForPlanningAuthorityID.CustomizationFormText = "Planning Authority ID:";
            this.ItemForPlanningAuthorityID.Location = new System.Drawing.Point(0, 443);
            this.ItemForPlanningAuthorityID.Name = "ItemForPlanningAuthorityID";
            this.ItemForPlanningAuthorityID.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForPlanningAuthorityID.Size = new System.Drawing.Size(630, 24);
            this.ItemForPlanningAuthorityID.Text = "Planning Authority ID:";
            this.ItemForPlanningAuthorityID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForKAM
            // 
            this.ItemForKAM.Control = this.KAMTextEdit;
            this.ItemForKAM.CustomizationFormText = "KAM ID:";
            this.ItemForKAM.Location = new System.Drawing.Point(0, 71);
            this.ItemForKAM.Name = "ItemForKAM";
            this.ItemForKAM.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForKAM.Size = new System.Drawing.Size(630, 24);
            this.ItemForKAM.Text = "KAM ID:";
            this.ItemForKAM.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSectorType
            // 
            this.ItemForSectorType.Control = this.SectorTypeTextEdit;
            this.ItemForSectorType.CustomizationFormText = "Sector Type:";
            this.ItemForSectorType.Location = new System.Drawing.Point(0, 74);
            this.ItemForSectorType.Name = "ItemForSectorType";
            this.ItemForSectorType.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForSectorType.Size = new System.Drawing.Size(479, 24);
            this.ItemForSectorType.Text = "Sector Type:";
            this.ItemForSectorType.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForTenderReactiveDaysToReturnQuote
            // 
            this.ItemForTenderReactiveDaysToReturnQuote.Control = this.TenderReactiveDaysToReturnQuoteTextEdit;
            this.ItemForTenderReactiveDaysToReturnQuote.CustomizationFormText = "Reactive Days To Return Quote:";
            this.ItemForTenderReactiveDaysToReturnQuote.Location = new System.Drawing.Point(0, 204);
            this.ItemForTenderReactiveDaysToReturnQuote.Name = "ItemForTenderReactiveDaysToReturnQuote";
            this.ItemForTenderReactiveDaysToReturnQuote.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForTenderReactiveDaysToReturnQuote.Size = new System.Drawing.Size(479, 24);
            this.ItemForTenderReactiveDaysToReturnQuote.Text = "Reactive Days To Return Quote:";
            this.ItemForTenderReactiveDaysToReturnQuote.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForTenderProactiveDaysToReturnQuote
            // 
            this.ItemForTenderProactiveDaysToReturnQuote.Control = this.TenderProactiveDaysToReturnQuoteTextEdit;
            this.ItemForTenderProactiveDaysToReturnQuote.CustomizationFormText = "Proactive Days To Return Quote:";
            this.ItemForTenderProactiveDaysToReturnQuote.Location = new System.Drawing.Point(0, 204);
            this.ItemForTenderProactiveDaysToReturnQuote.Name = "ItemForTenderProactiveDaysToReturnQuote";
            this.ItemForTenderProactiveDaysToReturnQuote.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForTenderProactiveDaysToReturnQuote.Size = new System.Drawing.Size(479, 24);
            this.ItemForTenderProactiveDaysToReturnQuote.Text = "Proactive Days To Return Quote:";
            this.ItemForTenderProactiveDaysToReturnQuote.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForStatusIssueID
            // 
            this.ItemForStatusIssueID.Control = this.StatusIssueIDTextEdit;
            this.ItemForStatusIssueID.CustomizationFormText = "Status Issue ID:";
            this.ItemForStatusIssueID.Location = new System.Drawing.Point(0, 48);
            this.ItemForStatusIssueID.Name = "ItemForStatusIssueID";
            this.ItemForStatusIssueID.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForStatusIssueID.Size = new System.Drawing.Size(611, 24);
            this.ItemForStatusIssueID.Text = "Status Issue ID:";
            this.ItemForStatusIssueID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForNoClientPOEmailedToDirector
            // 
            this.ItemForNoClientPOEmailedToDirector.Control = this.NoClientPOEmailedToDirectorCheckEdit;
            this.ItemForNoClientPOEmailedToDirector.CustomizationFormText = "No Client PO Emailed To Director:";
            this.ItemForNoClientPOEmailedToDirector.Location = new System.Drawing.Point(0, 332);
            this.ItemForNoClientPOEmailedToDirector.Name = "ItemForNoClientPOEmailedToDirector";
            this.ItemForNoClientPOEmailedToDirector.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForNoClientPOEmailedToDirector.Size = new System.Drawing.Size(653, 23);
            this.ItemForNoClientPOEmailedToDirector.Text = "No Client PO Emailed To Director:";
            this.ItemForNoClientPOEmailedToDirector.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForNoClientPOAuthorisedByDirectorID
            // 
            this.ItemForNoClientPOAuthorisedByDirectorID.Control = this.NoClientPOAuthorisedByDirectorIDTextEdit;
            this.ItemForNoClientPOAuthorisedByDirectorID.CustomizationFormText = "No Client PO Authorised By Director ID:";
            this.ItemForNoClientPOAuthorisedByDirectorID.Location = new System.Drawing.Point(0, 342);
            this.ItemForNoClientPOAuthorisedByDirectorID.Name = "ItemForNoClientPOAuthorisedByDirectorID";
            this.ItemForNoClientPOAuthorisedByDirectorID.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForNoClientPOAuthorisedByDirectorID.Size = new System.Drawing.Size(653, 24);
            this.ItemForNoClientPOAuthorisedByDirectorID.Text = "No Client PO Authorised By Director ID:";
            this.ItemForNoClientPOAuthorisedByDirectorID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForKAMQuoteRejectionReasonID
            // 
            this.ItemForKAMQuoteRejectionReasonID.Control = this.KAMQuoteRejectionReasonIDTextEdit;
            this.ItemForKAMQuoteRejectionReasonID.CustomizationFormText = "KAM Quote Rejection Reason ID:";
            this.ItemForKAMQuoteRejectionReasonID.Location = new System.Drawing.Point(0, 555);
            this.ItemForKAMQuoteRejectionReasonID.Name = "ItemForKAMQuoteRejectionReasonID";
            this.ItemForKAMQuoteRejectionReasonID.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForKAMQuoteRejectionReasonID.Size = new System.Drawing.Size(649, 24);
            this.ItemForKAMQuoteRejectionReasonID.Text = "KAM Quote Rejection Reason ID:";
            this.ItemForKAMQuoteRejectionReasonID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForPlanningProceedStaffID
            // 
            this.ItemForPlanningProceedStaffID.Control = this.PlanningProceedStaffIDTextEdit;
            this.ItemForPlanningProceedStaffID.CustomizationFormText = "Planning OK Staff ID:";
            this.ItemForPlanningProceedStaffID.Location = new System.Drawing.Point(649, 0);
            this.ItemForPlanningProceedStaffID.Name = "ItemForPlanningProceedStaffID";
            this.ItemForPlanningProceedStaffID.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForPlanningProceedStaffID.Size = new System.Drawing.Size(199, 24);
            this.ItemForPlanningProceedStaffID.Text = "Planning OK Staff ID:";
            this.ItemForPlanningProceedStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.autoGeneratedGroup0});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlGroup1.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1344, 856);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // sp06502OMTenderQuoteRejectionReasonsWithBlankBindingSource
            // 
            this.sp06502OMTenderQuoteRejectionReasonsWithBlankBindingSource.DataMember = "sp06502_OM_Tender_Quote_Rejection_Reasons_With_Blank";
            this.sp06502OMTenderQuoteRejectionReasonsWithBlankBindingSource.DataSource = this.dataSet_OM_Tender;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06498_OM_Tender_EditTableAdapter
            // 
            this.sp06498_OM_Tender_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp06502_OM_Tender_Quote_Rejection_Reasons_With_BlankTableAdapter
            // 
            this.sp06502_OM_Tender_Quote_Rejection_Reasons_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06503_OM_Tender_Planning_Outcomes_With_BlankTableAdapter
            // 
            this.sp06503_OM_Tender_Planning_Outcomes_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            this.imageCollection2.Images.SetKeyName(0, "employee2_accept_32.png");
            this.imageCollection2.Images.SetKeyName(1, "employee2_reject_32.png");
            this.imageCollection2.InsertGalleryImage("borules_32x32.png", "images/business%20objects/borules_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/business%20objects/borules_32x32.png"), 2);
            this.imageCollection2.Images.SetKeyName(2, "borules_32x32.png");
            this.imageCollection2.Images.SetKeyName(3, "Employee_send_quote_32_.png");
            this.imageCollection2.Images.SetKeyName(4, "quote_not_applicable_32.png");
            this.imageCollection2.Images.SetKeyName(5, "customer_send_quote_32.png");
            this.imageCollection2.Images.SetKeyName(6, "customer_on_hold_quote_32.png");
            this.imageCollection2.Images.SetKeyName(7, "customer_reject_quote_32.png");
            // 
            // sp06525_OM_Tender_Types_With_BlankTableAdapter
            // 
            this.sp06525_OM_Tender_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp01052_Core_Conservation_Statuses_With_BlankTableAdapter
            // 
            this.sp01052_Core_Conservation_Statuses_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp01053_Core_Listed_Building_Statuses_With_BlankTableAdapter
            // 
            this.sp01053_Core_Listed_Building_Statuses_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // autoGeneratedGroup0
            // 
            this.autoGeneratedGroup0.AllowDrawBackground = false;
            this.autoGeneratedGroup0.CustomizationFormText = "autoGeneratedGroup0";
            this.autoGeneratedGroup0.GroupBordersVisible = false;
            this.autoGeneratedGroup0.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.ItemForRevisionNumber,
            this.layoutControlGroup14,
            this.ItemForTenderID,
            this.item2,
            this.emptySpaceItem17,
            this.layoutControlItem1,
            this.item3,
            this.item6,
            this.item17,
            this.layoutControlGroup4});
            this.autoGeneratedGroup0.Location = new System.Drawing.Point(0, 0);
            this.autoGeneratedGroup0.Name = "autoGeneratedGroup0";
            this.autoGeneratedGroup0.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.autoGeneratedGroup0.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.autoGeneratedGroup0.Size = new System.Drawing.Size(1324, 836);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(478, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.emptySpaceItem1.Size = new System.Drawing.Size(422, 24);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForRevisionNumber
            // 
            this.ItemForRevisionNumber.Control = this.RevisionNumberTextEdit;
            this.ItemForRevisionNumber.CustomizationFormText = "Revision:";
            this.ItemForRevisionNumber.Location = new System.Drawing.Point(339, 0);
            this.ItemForRevisionNumber.MaxSize = new System.Drawing.Size(139, 24);
            this.ItemForRevisionNumber.MinSize = new System.Drawing.Size(139, 24);
            this.ItemForRevisionNumber.Name = "ItemForRevisionNumber";
            this.ItemForRevisionNumber.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForRevisionNumber.Size = new System.Drawing.Size(139, 24);
            this.ItemForRevisionNumber.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRevisionNumber.Text = "Revision:";
            this.ItemForRevisionNumber.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForRevisionNumber.TextSize = new System.Drawing.Size(44, 13);
            this.ItemForRevisionNumber.TextToControlDistance = 5;
            // 
            // layoutControlGroup14
            // 
            this.layoutControlGroup14.AllowHtmlStringInCaption = true;
            this.layoutControlGroup14.CustomizationFormText = "Tender <b>Status</b>";
            this.layoutControlGroup14.ExpandButtonVisible = true;
            this.layoutControlGroup14.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup14.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForQuoteNotRequired,
            this.ItemForCMReturnedQuoteBtn,
            this.ItemForSentToClient,
            this.ItemForCreateVisitsBtn,
            this.ItemForTenderCompletedBtn,
            this.simpleSeparator1,
            this.ItemForSubmittedToCMDate,
            this.ItemForClientResponseBtn,
            this.ItemForProgressBar,
            this.ItemForVisitCount,
            this.ItemForJobCount,
            this.layoutControlItem2,
            this.ItemForKAMQuoteRejectionReason,
            this.ItemForQuoteAcceptedByClientDate,
            this.ItemForQuoteSubmittedToClientDate,
            this.ItemForTenderCreatedBtn,
            this.ItemForCancelTenderBtn,
            this.item0,
            this.ItemForTenderStatus,
            this.ItemForStatusIsssue,
            this.item1,
            this.item5,
            this.item7,
            this.item8,
            this.item9,
            this.item10,
            this.item11,
            this.item12});
            this.layoutControlGroup14.Location = new System.Drawing.Point(0, 582);
            this.layoutControlGroup14.Name = "layoutControlGroup14";
            this.layoutControlGroup14.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlGroup14.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlGroup14.Size = new System.Drawing.Size(632, 254);
            this.layoutControlGroup14.Text = "Tender <b>Status</b>";
            // 
            // ItemForTenderID
            // 
            this.ItemForTenderID.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.ItemForTenderID.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForTenderID.Control = this.TenderIDTextEdit;
            this.ItemForTenderID.CustomizationFormText = "Tender ID:";
            this.ItemForTenderID.Location = new System.Drawing.Point(168, 0);
            this.ItemForTenderID.MaxSize = new System.Drawing.Size(171, 24);
            this.ItemForTenderID.MinSize = new System.Drawing.Size(171, 24);
            this.ItemForTenderID.Name = "ItemForTenderID";
            this.ItemForTenderID.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForTenderID.Size = new System.Drawing.Size(171, 24);
            this.ItemForTenderID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTenderID.Text = "Tender ID:";
            this.ItemForTenderID.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForTenderID.TextSize = new System.Drawing.Size(59, 13);
            this.ItemForTenderID.TextToControlDistance = 5;
            // 
            // item2
            // 
            this.item2.CustomizationFormText = "Costs";
            this.item2.ExpandButtonVisible = true;
            this.item2.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.item2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup10,
            this.layoutControlGroup12,
            this.layoutControlGroup15,
            this.layoutControlGroup13});
            this.item2.Location = new System.Drawing.Point(638, 165);
            this.item2.Name = "item2";
            this.item2.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item2.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item2.Size = new System.Drawing.Size(686, 198);
            this.item2.Text = "Costs";
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.CustomizationFormText = "emptySpaceItem17";
            this.emptySpaceItem17.Location = new System.Drawing.Point(900, 0);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.emptySpaceItem17.Size = new System.Drawing.Size(424, 24);
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlItem1.Size = new System.Drawing.Size(168, 24);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // item3
            // 
            this.item3.AllowHotTrack = true;
            this.item3.CustomizationFormText = "item3";
            this.item3.Location = new System.Drawing.Point(632, 24);
            this.item3.Name = "item3";
            this.item3.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item3.Size = new System.Drawing.Size(6, 812);
            // 
            // item6
            // 
            this.item6.CustomizationFormText = "Tender Details";
            this.item6.ExpandButtonVisible = true;
            this.item6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.item6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem6,
            this.ItemForJobSubTypeDescription,
            this.ItemForJobTypeDescription,
            this.layoutControlItem3,
            this.ItemForCMName,
            this.ItmeForKAMName,
            this.ItemForTenderDescription,
            this.ItemForTenderGroupDescription,
            this.ItemForTenderTypeID,
            this.ItemForContractDescription,
            this.ItemForCreatedByStaffName,
            this.ItemForGCCompanyName,
            this.item13,
            this.item14,
            this.item15,
            this.item18,
            this.item19});
            this.item6.Location = new System.Drawing.Point(0, 24);
            this.item6.Name = "item6";
            this.item6.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item6.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item6.Size = new System.Drawing.Size(632, 558);
            this.item6.Text = "Tender Details";
            // 
            // item17
            // 
            this.item17.CustomizationFormText = "item17";
            this.item17.ExpandButtonVisible = true;
            this.item17.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.item17.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForReactive,
            this.ItemForEndDate,
            this.ItemForRAMS,
            this.ItemForCMInitialAttendanceDate,
            this.ItemForRequiredWorkCompletedDate,
            this.ItemForRequestReceivedDate,
            this.ItemForClientQuoteSpreadsheetExtractFile});
            this.item17.Location = new System.Drawing.Point(638, 24);
            this.item17.Name = "item17";
            this.item17.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item17.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item17.Size = new System.Drawing.Size(686, 141);
            this.item17.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Planning";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTPORequired,
            this.ItemForPlanningAuthority,
            this.emptySpaceItem13,
            this.ItemForPlanningAuthorityNumber,
            this.emptySpaceItem16,
            this.ItemForPlanningProceedDateSet,
            this.ItemForPlanningProceedStaffName,
            this.ItemForConservationStatusID,
            this.ItemForTPONumber,
            this.ItemForListedBuildingStatusID,
            this.ItemForPlanningAuthorityOutcomeID,
            this.ItemForLocalAuthorityWebSite,
            this.ItemForPlanningAuthoritySubmittedDate,
            this.layoutControlGroup11,
            this.ItemForPlanningAuthorityOkToProceed,
            this.ItemForTreeProtected,
            this.ItemForTPOCheckedDate,
            this.ItemForLocalAuthorityTelephone});
            this.layoutControlGroup4.Location = new System.Drawing.Point(638, 363);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlGroup4.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlGroup4.Size = new System.Drawing.Size(686, 473);
            this.layoutControlGroup4.Text = "Planning";
            // 
            // ItemForQuoteNotRequired
            // 
            this.ItemForQuoteNotRequired.Control = this.QuoteNotRequiredCheckEdit;
            this.ItemForQuoteNotRequired.CustomizationFormText = "Active:";
            this.ItemForQuoteNotRequired.Location = new System.Drawing.Point(295, 50);
            this.ItemForQuoteNotRequired.MinSize = new System.Drawing.Size(225, 23);
            this.ItemForQuoteNotRequired.Name = "ItemForQuoteNotRequired";
            this.ItemForQuoteNotRequired.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForQuoteNotRequired.Size = new System.Drawing.Size(313, 26);
            this.ItemForQuoteNotRequired.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForQuoteNotRequired.Text = "Quote Not Required:";
            this.ItemForQuoteNotRequired.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForCMReturnedQuoteBtn
            // 
            this.ItemForCMReturnedQuoteBtn.Control = this.btnReturnedFromCM;
            this.ItemForCMReturnedQuoteBtn.CustomizationFormText = "Awaiting CM Quote - ";
            this.ItemForCMReturnedQuoteBtn.Location = new System.Drawing.Point(35, 50);
            this.ItemForCMReturnedQuoteBtn.MinSize = new System.Drawing.Size(251, 26);
            this.ItemForCMReturnedQuoteBtn.Name = "ItemForCMReturnedQuoteBtn";
            this.ItemForCMReturnedQuoteBtn.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForCMReturnedQuoteBtn.Size = new System.Drawing.Size(260, 26);
            this.ItemForCMReturnedQuoteBtn.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCMReturnedQuoteBtn.Text = "Awaiting CM Quote - ";
            this.ItemForCMReturnedQuoteBtn.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForSentToClient
            // 
            this.ItemForSentToClient.Control = this.btnSendToClient;
            this.ItemForSentToClient.CustomizationFormText = "Ready To Send To Client - ";
            this.ItemForSentToClient.Location = new System.Drawing.Point(35, 102);
            this.ItemForSentToClient.MinSize = new System.Drawing.Size(223, 26);
            this.ItemForSentToClient.Name = "ItemForSentToClient";
            this.ItemForSentToClient.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForSentToClient.Size = new System.Drawing.Size(260, 26);
            this.ItemForSentToClient.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSentToClient.Text = "Ready To Send To Client - ";
            this.ItemForSentToClient.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForCreateVisitsBtn
            // 
            this.ItemForCreateVisitsBtn.Control = this.btnCreateVisits;
            this.ItemForCreateVisitsBtn.CustomizationFormText = "Visit Creation In Progress - ";
            this.ItemForCreateVisitsBtn.Location = new System.Drawing.Point(35, 154);
            this.ItemForCreateVisitsBtn.MinSize = new System.Drawing.Size(220, 26);
            this.ItemForCreateVisitsBtn.Name = "ItemForCreateVisitsBtn";
            this.ItemForCreateVisitsBtn.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForCreateVisitsBtn.Size = new System.Drawing.Size(260, 26);
            this.ItemForCreateVisitsBtn.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCreateVisitsBtn.Text = "Visit Creation In Progress - ";
            this.ItemForCreateVisitsBtn.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForTenderCompletedBtn
            // 
            this.ItemForTenderCompletedBtn.Control = this.btnTenderClosed;
            this.ItemForTenderCompletedBtn.CustomizationFormText = "Tender Completed - ";
            this.ItemForTenderCompletedBtn.Location = new System.Drawing.Point(35, 180);
            this.ItemForTenderCompletedBtn.MinSize = new System.Drawing.Size(225, 26);
            this.ItemForTenderCompletedBtn.Name = "ItemForTenderCompletedBtn";
            this.ItemForTenderCompletedBtn.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForTenderCompletedBtn.Size = new System.Drawing.Size(260, 26);
            this.ItemForTenderCompletedBtn.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTenderCompletedBtn.Text = "Tender Completed - ";
            this.ItemForTenderCompletedBtn.TextSize = new System.Drawing.Size(142, 13);
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 206);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.simpleSeparator1.Size = new System.Drawing.Size(608, 2);
            // 
            // ItemForSubmittedToCMDate
            // 
            this.ItemForSubmittedToCMDate.Control = this.SubmittedToCMDateDateEdit;
            this.ItemForSubmittedToCMDate.CustomizationFormText = "Submitted To CM:";
            this.ItemForSubmittedToCMDate.Location = new System.Drawing.Point(295, 24);
            this.ItemForSubmittedToCMDate.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForSubmittedToCMDate.Name = "ItemForSubmittedToCMDate";
            this.ItemForSubmittedToCMDate.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForSubmittedToCMDate.Size = new System.Drawing.Size(313, 26);
            this.ItemForSubmittedToCMDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSubmittedToCMDate.Text = "Submitted To CM:";
            this.ItemForSubmittedToCMDate.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForClientResponseBtn
            // 
            this.ItemForClientResponseBtn.Control = this.ddBtnClientResponse;
            this.ItemForClientResponseBtn.CustomizationFormText = "Client Response - ";
            this.ItemForClientResponseBtn.Location = new System.Drawing.Point(35, 128);
            this.ItemForClientResponseBtn.MinSize = new System.Drawing.Size(247, 26);
            this.ItemForClientResponseBtn.Name = "ItemForClientResponseBtn";
            this.ItemForClientResponseBtn.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForClientResponseBtn.Size = new System.Drawing.Size(260, 26);
            this.ItemForClientResponseBtn.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClientResponseBtn.Text = "Client Response - ";
            this.ItemForClientResponseBtn.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForProgressBar
            // 
            this.ItemForProgressBar.Control = this.progressBarControl1;
            this.ItemForProgressBar.CustomizationFormText = "Tender Progress Bar";
            this.ItemForProgressBar.Location = new System.Drawing.Point(0, 24);
            this.ItemForProgressBar.MaxSize = new System.Drawing.Size(22, 0);
            this.ItemForProgressBar.MinSize = new System.Drawing.Size(22, 20);
            this.ItemForProgressBar.Name = "ItemForProgressBar";
            this.ItemForProgressBar.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForProgressBar.Size = new System.Drawing.Size(22, 182);
            this.ItemForProgressBar.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForProgressBar.Text = "Tender Progress Bar";
            this.ItemForProgressBar.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ItemForProgressBar.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForProgressBar.TextToControlDistance = 0;
            this.ItemForProgressBar.TextVisible = false;
            // 
            // ItemForVisitCount
            // 
            this.ItemForVisitCount.Control = this.VisitCountTextEdit;
            this.ItemForVisitCount.CustomizationFormText = "Visit Count:";
            this.ItemForVisitCount.Location = new System.Drawing.Point(295, 154);
            this.ItemForVisitCount.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForVisitCount.Name = "ItemForVisitCount";
            this.ItemForVisitCount.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForVisitCount.Size = new System.Drawing.Size(197, 26);
            this.ItemForVisitCount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForVisitCount.Text = "Visit \\ Job Count:";
            this.ItemForVisitCount.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForJobCount
            // 
            this.ItemForJobCount.Control = this.JobCountTextEdit;
            this.ItemForJobCount.CustomizationFormText = "Job Count:";
            this.ItemForJobCount.Location = new System.Drawing.Point(492, 154);
            this.ItemForJobCount.MinSize = new System.Drawing.Size(54, 24);
            this.ItemForJobCount.Name = "ItemForJobCount";
            this.ItemForJobCount.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForJobCount.Size = new System.Drawing.Size(116, 26);
            this.ItemForJobCount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForJobCount.Text = "Job Count:";
            this.ItemForJobCount.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForJobCount.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.ddBtnKAMAuthorised;
            this.layoutControlItem2.CustomizationFormText = "Awaiting KAM Authorisation - ";
            this.layoutControlItem2.Location = new System.Drawing.Point(35, 76);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(248, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlItem2.Size = new System.Drawing.Size(260, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Awaiting KAM Authorisation - ";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForKAMQuoteRejectionReason
            // 
            this.ItemForKAMQuoteRejectionReason.Control = this.KAMQuoteRejectionReasonTextEdit;
            this.ItemForKAMQuoteRejectionReason.CustomizationFormText = "KAM Quote Rejection Reason:";
            this.ItemForKAMQuoteRejectionReason.Location = new System.Drawing.Point(295, 76);
            this.ItemForKAMQuoteRejectionReason.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForKAMQuoteRejectionReason.Name = "ItemForKAMQuoteRejectionReason";
            this.ItemForKAMQuoteRejectionReason.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForKAMQuoteRejectionReason.Size = new System.Drawing.Size(313, 25);
            this.ItemForKAMQuoteRejectionReason.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForKAMQuoteRejectionReason.Text = "KAM Rejection Reason:";
            this.ItemForKAMQuoteRejectionReason.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForQuoteAcceptedByClientDate
            // 
            this.ItemForQuoteAcceptedByClientDate.Control = this.QuoteAcceptedByClientDateDateEdit;
            this.ItemForQuoteAcceptedByClientDate.CustomizationFormText = "Quote Accepted By Client Date:";
            this.ItemForQuoteAcceptedByClientDate.Location = new System.Drawing.Point(295, 128);
            this.ItemForQuoteAcceptedByClientDate.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForQuoteAcceptedByClientDate.Name = "ItemForQuoteAcceptedByClientDate";
            this.ItemForQuoteAcceptedByClientDate.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForQuoteAcceptedByClientDate.Size = new System.Drawing.Size(313, 26);
            this.ItemForQuoteAcceptedByClientDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForQuoteAcceptedByClientDate.Text = "Accepted Date:";
            this.ItemForQuoteAcceptedByClientDate.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForQuoteSubmittedToClientDate
            // 
            this.ItemForQuoteSubmittedToClientDate.Control = this.QuoteSubmittedToClientDateDateEdit;
            this.ItemForQuoteSubmittedToClientDate.CustomizationFormText = "Quote Submitted To Client:";
            this.ItemForQuoteSubmittedToClientDate.Location = new System.Drawing.Point(295, 101);
            this.ItemForQuoteSubmittedToClientDate.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForQuoteSubmittedToClientDate.Name = "ItemForQuoteSubmittedToClientDate";
            this.ItemForQuoteSubmittedToClientDate.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForQuoteSubmittedToClientDate.Size = new System.Drawing.Size(313, 27);
            this.ItemForQuoteSubmittedToClientDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForQuoteSubmittedToClientDate.Text = "Quote Sent To Client:";
            this.ItemForQuoteSubmittedToClientDate.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForTenderCreatedBtn
            // 
            this.ItemForTenderCreatedBtn.Control = this.ddBtnSentToCM;
            this.ItemForTenderCreatedBtn.CustomizationFormText = "Tender Created - ";
            this.ItemForTenderCreatedBtn.Location = new System.Drawing.Point(35, 24);
            this.ItemForTenderCreatedBtn.MinSize = new System.Drawing.Size(260, 26);
            this.ItemForTenderCreatedBtn.Name = "ItemForTenderCreatedBtn";
            this.ItemForTenderCreatedBtn.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForTenderCreatedBtn.Size = new System.Drawing.Size(260, 26);
            this.ItemForTenderCreatedBtn.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTenderCreatedBtn.Text = "Tender Created - ";
            this.ItemForTenderCreatedBtn.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForCancelTenderBtn
            // 
            this.ItemForCancelTenderBtn.Control = this.btnCancelTender;
            this.ItemForCancelTenderBtn.CustomizationFormText = "Cancel Tender";
            this.ItemForCancelTenderBtn.Location = new System.Drawing.Point(492, 180);
            this.ItemForCancelTenderBtn.MinSize = new System.Drawing.Size(102, 26);
            this.ItemForCancelTenderBtn.Name = "ItemForCancelTenderBtn";
            this.ItemForCancelTenderBtn.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForCancelTenderBtn.Size = new System.Drawing.Size(116, 26);
            this.ItemForCancelTenderBtn.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCancelTenderBtn.Text = "Cancel Tender";
            this.ItemForCancelTenderBtn.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForCancelTenderBtn.TextVisible = false;
            // 
            // item0
            // 
            this.item0.AllowHotTrack = false;
            this.item0.CustomizationFormText = "item0";
            this.item0.Location = new System.Drawing.Point(295, 180);
            this.item0.MinSize = new System.Drawing.Size(104, 24);
            this.item0.Name = "item0";
            this.item0.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item0.Size = new System.Drawing.Size(197, 26);
            this.item0.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.item0.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForTenderStatus
            // 
            this.ItemForTenderStatus.Control = this.TenderStatusButtonEdit;
            this.ItemForTenderStatus.CustomizationFormText = "Status";
            this.ItemForTenderStatus.Location = new System.Drawing.Point(35, 0);
            this.ItemForTenderStatus.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForTenderStatus.Name = "ItemForTenderStatus";
            this.ItemForTenderStatus.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForTenderStatus.Size = new System.Drawing.Size(260, 24);
            this.ItemForTenderStatus.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTenderStatus.Text = "Status";
            this.ItemForTenderStatus.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForStatusIsssue
            // 
            this.ItemForStatusIsssue.Control = this.StatusIsssueButtonEdit;
            this.ItemForStatusIsssue.CustomizationFormText = "Status Issue:";
            this.ItemForStatusIsssue.Location = new System.Drawing.Point(295, 0);
            this.ItemForStatusIsssue.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForStatusIsssue.Name = "ItemForStatusIsssue";
            this.ItemForStatusIsssue.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForStatusIsssue.Size = new System.Drawing.Size(313, 24);
            this.ItemForStatusIsssue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForStatusIsssue.Text = "Status Issue:";
            this.ItemForStatusIsssue.TextSize = new System.Drawing.Size(142, 13);
            // 
            // item1
            // 
            this.item1.AllowHotTrack = false;
            this.item1.CustomizationFormText = "item1";
            this.item1.Location = new System.Drawing.Point(22, 24);
            this.item1.Name = "item1";
            this.item1.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item1.Size = new System.Drawing.Size(13, 26);
            this.item1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // item5
            // 
            this.item5.AllowHotTrack = false;
            this.item5.CustomizationFormText = "item5";
            this.item5.Location = new System.Drawing.Point(22, 50);
            this.item5.Name = "item5";
            this.item5.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item5.Size = new System.Drawing.Size(13, 26);
            this.item5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // item7
            // 
            this.item7.AllowHotTrack = false;
            this.item7.CustomizationFormText = "item7";
            this.item7.Location = new System.Drawing.Point(22, 76);
            this.item7.Name = "item7";
            this.item7.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item7.Size = new System.Drawing.Size(13, 26);
            this.item7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // item8
            // 
            this.item8.AllowHotTrack = false;
            this.item8.CustomizationFormText = "item8";
            this.item8.Location = new System.Drawing.Point(22, 102);
            this.item8.Name = "item8";
            this.item8.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item8.Size = new System.Drawing.Size(13, 26);
            this.item8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // item9
            // 
            this.item9.AllowHotTrack = false;
            this.item9.CustomizationFormText = "item9";
            this.item9.Location = new System.Drawing.Point(22, 128);
            this.item9.Name = "item9";
            this.item9.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item9.Size = new System.Drawing.Size(13, 26);
            this.item9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // item10
            // 
            this.item10.AllowHotTrack = false;
            this.item10.CustomizationFormText = "item10";
            this.item10.Location = new System.Drawing.Point(22, 154);
            this.item10.Name = "item10";
            this.item10.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item10.Size = new System.Drawing.Size(13, 26);
            this.item10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // item11
            // 
            this.item11.AllowHotTrack = false;
            this.item11.CustomizationFormText = "item11";
            this.item11.Location = new System.Drawing.Point(22, 180);
            this.item11.Name = "item11";
            this.item11.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item11.Size = new System.Drawing.Size(13, 26);
            this.item11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // item12
            // 
            this.item12.AllowHotTrack = false;
            this.item12.CustomizationFormText = "item12";
            this.item12.Location = new System.Drawing.Point(0, 0);
            this.item12.Name = "item12";
            this.item12.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item12.Size = new System.Drawing.Size(35, 24);
            this.item12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.AllowHtmlStringInCaption = true;
            this.layoutControlGroup10.CustomizationFormText = "<b>Quoted</b>  Value [Selling]";
            this.layoutControlGroup10.ExpandButtonVisible = true;
            this.layoutControlGroup10.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForQuotedLabourSell,
            this.ItemForQuotedMaterialSell,
            this.ItemForQuotedEquipmentSell,
            this.ItemForQuotedTotalSell,
            this.item4});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutControlGroup10.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlGroup10.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlGroup10.Size = new System.Drawing.Size(178, 152);
            this.layoutControlGroup10.Text = "<b>Quoted</b>  Value [Selling]";
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.AllowHtmlStringInCaption = true;
            this.layoutControlGroup12.CustomizationFormText = "<b>Accepted</b> Value  [Selling]";
            this.layoutControlGroup12.ExpandButtonVisible = true;
            this.layoutControlGroup12.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAcceptedLabourSell,
            this.ItemForAcceptedMaterialSell,
            this.ItemForAcceptedEquipmentSell,
            this.ItemForAcceptedTotalSell});
            this.layoutControlGroup12.Location = new System.Drawing.Point(178, 0);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutControlGroup12.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlGroup12.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlGroup12.Size = new System.Drawing.Size(180, 152);
            this.layoutControlGroup12.Text = "<b>Accepted</b> Value  [Selling]";
            // 
            // layoutControlGroup15
            // 
            this.layoutControlGroup15.AllowHtmlStringInCaption = true;
            this.layoutControlGroup15.CustomizationFormText = "<b>Proposed</b> Cost";
            this.layoutControlGroup15.ExpandButtonVisible = true;
            this.layoutControlGroup15.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup15.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForProposedLabourCost,
            this.ItemForProposedMaterialCost,
            this.ItemForProposedEquipmentCost,
            this.ItemForProposedTotalCost});
            this.layoutControlGroup15.Location = new System.Drawing.Point(358, 0);
            this.layoutControlGroup15.Name = "layoutControlGroup15";
            this.layoutControlGroup15.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutControlGroup15.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlGroup15.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlGroup15.Size = new System.Drawing.Size(169, 152);
            this.layoutControlGroup15.Text = "<b>Proposed</b> Cost";
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.AllowHtmlStringInCaption = true;
            this.layoutControlGroup13.CustomizationFormText = "<b>Actual</b> Cost";
            this.layoutControlGroup13.ExpandButtonVisible = true;
            this.layoutControlGroup13.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForActualLabourCost,
            this.ItemForActualMaterialCost,
            this.ItemForActualEquipmentCost,
            this.ItemForActualTotalCost});
            this.layoutControlGroup13.Location = new System.Drawing.Point(527, 0);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutControlGroup13.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlGroup13.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlGroup13.Size = new System.Drawing.Size(135, 152);
            this.layoutControlGroup13.Text = "<b>Actual</b> Cost";
            // 
            // ItemForQuotedLabourSell
            // 
            this.ItemForQuotedLabourSell.Control = this.QuotedLabourSellSpinEdit;
            this.ItemForQuotedLabourSell.CustomizationFormText = "Quoted Labour Sell:";
            this.ItemForQuotedLabourSell.Location = new System.Drawing.Point(0, 0);
            this.ItemForQuotedLabourSell.MaxSize = new System.Drawing.Size(154, 24);
            this.ItemForQuotedLabourSell.MinSize = new System.Drawing.Size(154, 24);
            this.ItemForQuotedLabourSell.Name = "ItemForQuotedLabourSell";
            this.ItemForQuotedLabourSell.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForQuotedLabourSell.Size = new System.Drawing.Size(154, 24);
            this.ItemForQuotedLabourSell.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForQuotedLabourSell.Text = "Labour:";
            this.ItemForQuotedLabourSell.TextSize = new System.Drawing.Size(54, 13);
            // 
            // ItemForQuotedMaterialSell
            // 
            this.ItemForQuotedMaterialSell.Control = this.QuotedMaterialSellSpinEdit;
            this.ItemForQuotedMaterialSell.CustomizationFormText = "Quoted Total Material:";
            this.ItemForQuotedMaterialSell.Location = new System.Drawing.Point(0, 24);
            this.ItemForQuotedMaterialSell.Name = "ItemForQuotedMaterialSell";
            this.ItemForQuotedMaterialSell.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForQuotedMaterialSell.Size = new System.Drawing.Size(154, 24);
            this.ItemForQuotedMaterialSell.Text = "Material:";
            this.ItemForQuotedMaterialSell.TextSize = new System.Drawing.Size(54, 13);
            // 
            // ItemForQuotedEquipmentSell
            // 
            this.ItemForQuotedEquipmentSell.Control = this.QuotedEquipmentSellSpinEdit;
            this.ItemForQuotedEquipmentSell.CustomizationFormText = "Quoted Total Equipment:";
            this.ItemForQuotedEquipmentSell.Location = new System.Drawing.Point(0, 48);
            this.ItemForQuotedEquipmentSell.Name = "ItemForQuotedEquipmentSell";
            this.ItemForQuotedEquipmentSell.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForQuotedEquipmentSell.Size = new System.Drawing.Size(154, 24);
            this.ItemForQuotedEquipmentSell.Text = "Equipment:";
            this.ItemForQuotedEquipmentSell.TextSize = new System.Drawing.Size(54, 13);
            // 
            // ItemForQuotedTotalSell
            // 
            this.ItemForQuotedTotalSell.Control = this.QuotedTotalSellSpinEdit;
            this.ItemForQuotedTotalSell.CustomizationFormText = "Quoted Total Sell:";
            this.ItemForQuotedTotalSell.Location = new System.Drawing.Point(0, 72);
            this.ItemForQuotedTotalSell.Name = "ItemForQuotedTotalSell";
            this.ItemForQuotedTotalSell.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForQuotedTotalSell.Size = new System.Drawing.Size(154, 24);
            this.ItemForQuotedTotalSell.Text = "Total:";
            this.ItemForQuotedTotalSell.TextSize = new System.Drawing.Size(54, 13);
            // 
            // item4
            // 
            this.item4.AllowHotTrack = false;
            this.item4.CustomizationFormText = "item4";
            this.item4.Location = new System.Drawing.Point(0, 96);
            this.item4.Name = "item4";
            this.item4.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item4.Size = new System.Drawing.Size(154, 10);
            this.item4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForAcceptedLabourSell
            // 
            this.ItemForAcceptedLabourSell.Control = this.AcceptedLabourSellSpinEdit;
            this.ItemForAcceptedLabourSell.CustomizationFormText = "Accepted Sell Labour:";
            this.ItemForAcceptedLabourSell.Location = new System.Drawing.Point(0, 0);
            this.ItemForAcceptedLabourSell.MaxSize = new System.Drawing.Size(156, 24);
            this.ItemForAcceptedLabourSell.MinSize = new System.Drawing.Size(156, 24);
            this.ItemForAcceptedLabourSell.Name = "ItemForAcceptedLabourSell";
            this.ItemForAcceptedLabourSell.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForAcceptedLabourSell.Size = new System.Drawing.Size(156, 24);
            this.ItemForAcceptedLabourSell.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForAcceptedLabourSell.Text = "Labour:";
            this.ItemForAcceptedLabourSell.TextSize = new System.Drawing.Size(54, 13);
            // 
            // ItemForAcceptedMaterialSell
            // 
            this.ItemForAcceptedMaterialSell.Control = this.AcceptedMaterialSellSpinEdit;
            this.ItemForAcceptedMaterialSell.CustomizationFormText = "Accepted Sell Material:";
            this.ItemForAcceptedMaterialSell.Location = new System.Drawing.Point(0, 24);
            this.ItemForAcceptedMaterialSell.Name = "ItemForAcceptedMaterialSell";
            this.ItemForAcceptedMaterialSell.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForAcceptedMaterialSell.Size = new System.Drawing.Size(156, 24);
            this.ItemForAcceptedMaterialSell.Text = "Material:";
            this.ItemForAcceptedMaterialSell.TextSize = new System.Drawing.Size(54, 13);
            // 
            // ItemForAcceptedEquipmentSell
            // 
            this.ItemForAcceptedEquipmentSell.Control = this.AcceptedEquipmentSellSpinEdit;
            this.ItemForAcceptedEquipmentSell.CustomizationFormText = "Accepted Sell Equipment:";
            this.ItemForAcceptedEquipmentSell.Location = new System.Drawing.Point(0, 48);
            this.ItemForAcceptedEquipmentSell.Name = "ItemForAcceptedEquipmentSell";
            this.ItemForAcceptedEquipmentSell.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForAcceptedEquipmentSell.Size = new System.Drawing.Size(156, 24);
            this.ItemForAcceptedEquipmentSell.Text = "Equipment:";
            this.ItemForAcceptedEquipmentSell.TextSize = new System.Drawing.Size(54, 13);
            // 
            // ItemForAcceptedTotalSell
            // 
            this.ItemForAcceptedTotalSell.Control = this.AcceptedTotalSellSpinEdit;
            this.ItemForAcceptedTotalSell.CustomizationFormText = "Accepted Sell Equipment:";
            this.ItemForAcceptedTotalSell.Location = new System.Drawing.Point(0, 72);
            this.ItemForAcceptedTotalSell.Name = "ItemForAcceptedTotalSell";
            this.ItemForAcceptedTotalSell.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForAcceptedTotalSell.Size = new System.Drawing.Size(156, 34);
            this.ItemForAcceptedTotalSell.Text = "Total:";
            this.ItemForAcceptedTotalSell.TextSize = new System.Drawing.Size(54, 13);
            // 
            // ItemForProposedLabourCost
            // 
            this.ItemForProposedLabourCost.Control = this.ProposedLabourCostSpinEdit;
            this.ItemForProposedLabourCost.CustomizationFormText = "Proposed Labour Cost:";
            this.ItemForProposedLabourCost.Location = new System.Drawing.Point(0, 0);
            this.ItemForProposedLabourCost.MaxSize = new System.Drawing.Size(145, 24);
            this.ItemForProposedLabourCost.MinSize = new System.Drawing.Size(145, 24);
            this.ItemForProposedLabourCost.Name = "ItemForProposedLabourCost";
            this.ItemForProposedLabourCost.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForProposedLabourCost.Size = new System.Drawing.Size(145, 24);
            this.ItemForProposedLabourCost.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForProposedLabourCost.Text = "Labour:";
            this.ItemForProposedLabourCost.TextSize = new System.Drawing.Size(54, 13);
            // 
            // ItemForProposedMaterialCost
            // 
            this.ItemForProposedMaterialCost.Control = this.ProposedMaterialCostSpinEdit;
            this.ItemForProposedMaterialCost.CustomizationFormText = "Proposed Material Cost:";
            this.ItemForProposedMaterialCost.Location = new System.Drawing.Point(0, 24);
            this.ItemForProposedMaterialCost.Name = "ItemForProposedMaterialCost";
            this.ItemForProposedMaterialCost.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForProposedMaterialCost.Size = new System.Drawing.Size(145, 24);
            this.ItemForProposedMaterialCost.Text = "Material:";
            this.ItemForProposedMaterialCost.TextSize = new System.Drawing.Size(54, 13);
            // 
            // ItemForProposedEquipmentCost
            // 
            this.ItemForProposedEquipmentCost.Control = this.ProposedEquipmentCostSpinEdit;
            this.ItemForProposedEquipmentCost.CustomizationFormText = "Proposed Equipment Cost:";
            this.ItemForProposedEquipmentCost.Location = new System.Drawing.Point(0, 48);
            this.ItemForProposedEquipmentCost.Name = "ItemForProposedEquipmentCost";
            this.ItemForProposedEquipmentCost.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForProposedEquipmentCost.Size = new System.Drawing.Size(145, 24);
            this.ItemForProposedEquipmentCost.Text = "Equipment:";
            this.ItemForProposedEquipmentCost.TextSize = new System.Drawing.Size(54, 13);
            // 
            // ItemForProposedTotalCost
            // 
            this.ItemForProposedTotalCost.Control = this.ProposedTotalCostSpinEdit;
            this.ItemForProposedTotalCost.CustomizationFormText = "Proposed Total Cost:";
            this.ItemForProposedTotalCost.Location = new System.Drawing.Point(0, 72);
            this.ItemForProposedTotalCost.Name = "ItemForProposedTotalCost";
            this.ItemForProposedTotalCost.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForProposedTotalCost.Size = new System.Drawing.Size(145, 34);
            this.ItemForProposedTotalCost.Text = "Total:";
            this.ItemForProposedTotalCost.TextSize = new System.Drawing.Size(54, 13);
            // 
            // ItemForActualLabourCost
            // 
            this.ItemForActualLabourCost.Control = this.ActualLabourCostSpinEdit;
            this.ItemForActualLabourCost.CustomizationFormText = "Actual Cost Labour:";
            this.ItemForActualLabourCost.Location = new System.Drawing.Point(0, 0);
            this.ItemForActualLabourCost.MinSize = new System.Drawing.Size(111, 24);
            this.ItemForActualLabourCost.Name = "ItemForActualLabourCost";
            this.ItemForActualLabourCost.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForActualLabourCost.Size = new System.Drawing.Size(111, 26);
            this.ItemForActualLabourCost.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForActualLabourCost.Text = "Labour:";
            this.ItemForActualLabourCost.TextSize = new System.Drawing.Size(54, 13);
            // 
            // ItemForActualMaterialCost
            // 
            this.ItemForActualMaterialCost.Control = this.ActualMaterialCostSpinEdit;
            this.ItemForActualMaterialCost.CustomizationFormText = "Actual Cost Material:";
            this.ItemForActualMaterialCost.Location = new System.Drawing.Point(0, 26);
            this.ItemForActualMaterialCost.MinSize = new System.Drawing.Size(111, 24);
            this.ItemForActualMaterialCost.Name = "ItemForActualMaterialCost";
            this.ItemForActualMaterialCost.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForActualMaterialCost.Size = new System.Drawing.Size(111, 27);
            this.ItemForActualMaterialCost.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForActualMaterialCost.Text = "Material:";
            this.ItemForActualMaterialCost.TextSize = new System.Drawing.Size(54, 13);
            // 
            // ItemForActualEquipmentCost
            // 
            this.ItemForActualEquipmentCost.Control = this.ActualEquipmentCostSpinEdit;
            this.ItemForActualEquipmentCost.CustomizationFormText = "Actual Cost Equipment:";
            this.ItemForActualEquipmentCost.Location = new System.Drawing.Point(0, 53);
            this.ItemForActualEquipmentCost.MinSize = new System.Drawing.Size(111, 24);
            this.ItemForActualEquipmentCost.Name = "ItemForActualEquipmentCost";
            this.ItemForActualEquipmentCost.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForActualEquipmentCost.Size = new System.Drawing.Size(111, 26);
            this.ItemForActualEquipmentCost.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForActualEquipmentCost.Text = "Equipment:";
            this.ItemForActualEquipmentCost.TextSize = new System.Drawing.Size(54, 13);
            // 
            // ItemForActualTotalCost
            // 
            this.ItemForActualTotalCost.Control = this.ActualTotalCostSpinEdit;
            this.ItemForActualTotalCost.CustomizationFormText = "Actual Cost Total:";
            this.ItemForActualTotalCost.Location = new System.Drawing.Point(0, 79);
            this.ItemForActualTotalCost.MinSize = new System.Drawing.Size(111, 24);
            this.ItemForActualTotalCost.Name = "ItemForActualTotalCost";
            this.ItemForActualTotalCost.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForActualTotalCost.Size = new System.Drawing.Size(111, 27);
            this.ItemForActualTotalCost.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForActualTotalCost.Text = "Total:";
            this.ItemForActualTotalCost.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.ContractorNameButtonEdit;
            this.layoutControlItem7.CustomizationFormText = "Proposed Labour:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 488);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(197, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlItem7.Size = new System.Drawing.Size(272, 24);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "Proposed Labour:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(142, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.LinkedToPersonTypeButtonEdit;
            this.layoutControlItem6.CustomizationFormText = "Proposed Labour Type:";
            this.layoutControlItem6.Location = new System.Drawing.Point(272, 488);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(197, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlItem6.Size = new System.Drawing.Size(336, 24);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "Proposed Labour Type:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForJobSubTypeDescription
            // 
            this.ItemForJobSubTypeDescription.Control = this.JobSubTypeDescriptionButtonEdit;
            this.ItemForJobSubTypeDescription.CustomizationFormText = "Work Sub-Type:";
            this.ItemForJobSubTypeDescription.Location = new System.Drawing.Point(272, 464);
            this.ItemForJobSubTypeDescription.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForJobSubTypeDescription.Name = "ItemForJobSubTypeDescription";
            this.ItemForJobSubTypeDescription.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForJobSubTypeDescription.Size = new System.Drawing.Size(336, 24);
            this.ItemForJobSubTypeDescription.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForJobSubTypeDescription.Text = "Work Sub-Type:";
            this.ItemForJobSubTypeDescription.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForJobTypeDescription
            // 
            this.ItemForJobTypeDescription.Control = this.JobTypeDescriptionTextEdit;
            this.ItemForJobTypeDescription.CustomizationFormText = "Work Type:";
            this.ItemForJobTypeDescription.Location = new System.Drawing.Point(0, 464);
            this.ItemForJobTypeDescription.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForJobTypeDescription.Name = "ItemForJobTypeDescription";
            this.ItemForJobTypeDescription.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForJobTypeDescription.Size = new System.Drawing.Size(272, 24);
            this.ItemForJobTypeDescription.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForJobTypeDescription.Text = "Work Type:";
            this.ItemForJobTypeDescription.TextSize = new System.Drawing.Size(142, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.labelControlWarning;
            this.layoutControlItem3.CustomizationFormText = " ";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 444);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(607, 20);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlItem3.Size = new System.Drawing.Size(608, 20);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = " ";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForCMName
            // 
            this.ItemForCMName.Control = this.CMNameButtonEdit;
            this.ItemForCMName.CustomizationFormText = "CM:";
            this.ItemForCMName.Location = new System.Drawing.Point(272, 420);
            this.ItemForCMName.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForCMName.Name = "ItemForCMName";
            this.ItemForCMName.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForCMName.Size = new System.Drawing.Size(336, 24);
            this.ItemForCMName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCMName.Text = "CM:";
            this.ItemForCMName.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItmeForKAMName
            // 
            this.ItmeForKAMName.Control = this.KAMNameButtonEdit;
            this.ItmeForKAMName.CustomizationFormText = "KAM:";
            this.ItmeForKAMName.Location = new System.Drawing.Point(0, 420);
            this.ItmeForKAMName.MinSize = new System.Drawing.Size(197, 24);
            this.ItmeForKAMName.Name = "ItmeForKAMName";
            this.ItmeForKAMName.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItmeForKAMName.Size = new System.Drawing.Size(272, 24);
            this.ItmeForKAMName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItmeForKAMName.Text = "KAM:";
            this.ItmeForKAMName.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForTenderDescription
            // 
            this.ItemForTenderDescription.Control = this.TenderDescriptionMemoEdit;
            this.ItemForTenderDescription.CustomizationFormText = "Tender Description:";
            this.ItemForTenderDescription.Location = new System.Drawing.Point(272, 0);
            this.ItemForTenderDescription.MinSize = new System.Drawing.Size(157, 22);
            this.ItemForTenderDescription.Name = "ItemForTenderDescription";
            this.ItemForTenderDescription.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForTenderDescription.Size = new System.Drawing.Size(336, 72);
            this.ItemForTenderDescription.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTenderDescription.Text = "Tender Description:";
            this.ItemForTenderDescription.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForTenderGroupDescription
            // 
            this.ItemForTenderGroupDescription.Control = this.TenderGroupDescriptionButtonEdit;
            this.ItemForTenderGroupDescription.CustomizationFormText = "Tender Group:";
            this.ItemForTenderGroupDescription.Location = new System.Drawing.Point(0, 72);
            this.ItemForTenderGroupDescription.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForTenderGroupDescription.Name = "ItemForTenderGroupDescription";
            this.ItemForTenderGroupDescription.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForTenderGroupDescription.Size = new System.Drawing.Size(272, 24);
            this.ItemForTenderGroupDescription.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTenderGroupDescription.Text = "Tender Group:";
            this.ItemForTenderGroupDescription.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForTenderTypeID
            // 
            this.ItemForTenderTypeID.Control = this.TenderTypeIDGridLookUpEdit;
            this.ItemForTenderTypeID.CustomizationFormText = "Tender Type:";
            this.ItemForTenderTypeID.Location = new System.Drawing.Point(0, 0);
            this.ItemForTenderTypeID.MinSize = new System.Drawing.Size(123, 24);
            this.ItemForTenderTypeID.Name = "ItemForTenderTypeID";
            this.ItemForTenderTypeID.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForTenderTypeID.Size = new System.Drawing.Size(272, 24);
            this.ItemForTenderTypeID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTenderTypeID.Text = "Tender Type:";
            this.ItemForTenderTypeID.TextLocation = DevExpress.Utils.Locations.Left;
            this.ItemForTenderTypeID.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForContractDescription
            // 
            this.ItemForContractDescription.Control = this.ContractDescriptionTextEdit;
            this.ItemForContractDescription.CustomizationFormText = "Client Contract:";
            this.ItemForContractDescription.Location = new System.Drawing.Point(0, 24);
            this.ItemForContractDescription.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForContractDescription.Name = "ItemForContractDescription";
            this.ItemForContractDescription.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForContractDescription.Size = new System.Drawing.Size(272, 24);
            this.ItemForContractDescription.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForContractDescription.Text = "Client Contract:";
            this.ItemForContractDescription.TextLocation = DevExpress.Utils.Locations.Left;
            this.ItemForContractDescription.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForCreatedByStaffName
            // 
            this.ItemForCreatedByStaffName.Control = this.CreatedByStaffNameTextEdit;
            this.ItemForCreatedByStaffName.CustomizationFormText = "Created By:";
            this.ItemForCreatedByStaffName.Location = new System.Drawing.Point(0, 48);
            this.ItemForCreatedByStaffName.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForCreatedByStaffName.Name = "ItemForCreatedByStaffName";
            this.ItemForCreatedByStaffName.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForCreatedByStaffName.Size = new System.Drawing.Size(272, 24);
            this.ItemForCreatedByStaffName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCreatedByStaffName.Text = "Created By:";
            this.ItemForCreatedByStaffName.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForGCCompanyName
            // 
            this.ItemForGCCompanyName.Control = this.GCCompanyNameTextEdit;
            this.ItemForGCCompanyName.CustomizationFormText = "GC Company:";
            this.ItemForGCCompanyName.Location = new System.Drawing.Point(272, 72);
            this.ItemForGCCompanyName.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForGCCompanyName.Name = "ItemForGCCompanyName";
            this.ItemForGCCompanyName.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForGCCompanyName.Size = new System.Drawing.Size(336, 24);
            this.ItemForGCCompanyName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForGCCompanyName.Text = "GC Company:";
            this.ItemForGCCompanyName.TextSize = new System.Drawing.Size(142, 13);
            // 
            // item13
            // 
            this.item13.AllowHotTrack = false;
            this.item13.CustomizationFormText = "item13";
            this.item13.Location = new System.Drawing.Point(0, 410);
            this.item13.Name = "item13";
            this.item13.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item13.Size = new System.Drawing.Size(608, 10);
            this.item13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // item14
            // 
            this.item14.AllowHotTrack = false;
            this.item14.CustomizationFormText = "item14";
            this.item14.Location = new System.Drawing.Point(0, 282);
            this.item14.Name = "item14";
            this.item14.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item14.Size = new System.Drawing.Size(608, 10);
            this.item14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // item15
            // 
            this.item15.AllowHotTrack = false;
            this.item15.CustomizationFormText = "item15";
            this.item15.Location = new System.Drawing.Point(0, 96);
            this.item15.Name = "item15";
            this.item15.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item15.Size = new System.Drawing.Size(608, 10);
            this.item15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // item18
            // 
            this.item18.CustomizationFormText = "Site Details";
            this.item18.ExpandButtonVisible = true;
            this.item18.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.item18.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSiteName,
            this.ItemForSitePostcode,
            this.ItemForSiteLocationX,
            this.ItemForSiteLocationY,
            this.ItemForSiteAddress,
            this.ItemForSiteCode});
            this.item18.Location = new System.Drawing.Point(0, 292);
            this.item18.Name = "item18";
            this.item18.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item18.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item18.Size = new System.Drawing.Size(608, 118);
            this.item18.Text = "Site Details";
            // 
            // item19
            // 
            this.item19.CustomizationFormText = "Client Details";
            this.item19.ExpandButtonVisible = true;
            this.item19.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.item19.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientName,
            this.ItemForClientReferenceNumber,
            this.ItemForNoClientPO,
            this.ItemForNoClientPOAuthorisedByDirector,
            this.item16,
            this.ItemForClientPONumber,
            this.ItemForContactPersonName,
            this.ItemForContactPersonPosition,
            this.ItemForClientContactTelephone,
            this.ItemForClientContactMobile,
            this.ItemForContactPersonTitle,
            this.ItemForClientContactEmail});
            this.item19.Location = new System.Drawing.Point(0, 106);
            this.item19.Name = "item19";
            this.item19.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item19.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item19.Size = new System.Drawing.Size(608, 176);
            this.item19.Text = "Client Details";
            // 
            // ItemForSiteName
            // 
            this.ItemForSiteName.Control = this.SiteNameButtonEdit;
            this.ItemForSiteName.CustomizationFormText = "Site Name:";
            this.ItemForSiteName.Location = new System.Drawing.Point(0, 0);
            this.ItemForSiteName.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForSiteName.Name = "ItemForSiteName";
            this.ItemForSiteName.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForSiteName.Size = new System.Drawing.Size(260, 24);
            this.ItemForSiteName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSiteName.Text = "Site Name:";
            this.ItemForSiteName.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForSitePostcode
            // 
            this.ItemForSitePostcode.Control = this.SitePostcodeTextEdit;
            this.ItemForSitePostcode.CustomizationFormText = "Site Postcode:";
            this.ItemForSitePostcode.Location = new System.Drawing.Point(0, 24);
            this.ItemForSitePostcode.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForSitePostcode.Name = "ItemForSitePostcode";
            this.ItemForSitePostcode.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForSitePostcode.Size = new System.Drawing.Size(260, 24);
            this.ItemForSitePostcode.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSitePostcode.Text = "Site Postcode:";
            this.ItemForSitePostcode.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForSiteAddress
            // 
            this.ItemForSiteAddress.Control = this.SiteAddressMemoEdit;
            this.ItemForSiteAddress.CustomizationFormText = "Site Address:";
            this.ItemForSiteAddress.Location = new System.Drawing.Point(260, 0);
            this.ItemForSiteAddress.MinSize = new System.Drawing.Size(157, 20);
            this.ItemForSiteAddress.Name = "ItemForSiteAddress";
            this.ItemForSiteAddress.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForSiteAddress.Size = new System.Drawing.Size(324, 24);
            this.ItemForSiteAddress.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSiteAddress.Text = "Site Address:";
            this.ItemForSiteAddress.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForSiteCode
            // 
            this.ItemForSiteCode.Control = this.SiteCodeButtonEdit;
            this.ItemForSiteCode.CustomizationFormText = "Site Code:";
            this.ItemForSiteCode.Location = new System.Drawing.Point(260, 24);
            this.ItemForSiteCode.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForSiteCode.Name = "ItemForSiteCode";
            this.ItemForSiteCode.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForSiteCode.Size = new System.Drawing.Size(324, 24);
            this.ItemForSiteCode.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSiteCode.Text = "Site Code:";
            this.ItemForSiteCode.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.AllowHide = false;
            this.ItemForClientName.Control = this.ClientNameButtonEdit;
            this.ItemForClientName.CustomizationFormText = "Client Name:";
            this.ItemForClientName.Location = new System.Drawing.Point(0, 0);
            this.ItemForClientName.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForClientName.Size = new System.Drawing.Size(260, 24);
            this.ItemForClientName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClientName.Text = "Client Name:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForClientReferenceNumber
            // 
            this.ItemForClientReferenceNumber.Control = this.ClientReferenceNumberTextEdit;
            this.ItemForClientReferenceNumber.CustomizationFormText = "Client Reference Number:";
            this.ItemForClientReferenceNumber.Location = new System.Drawing.Point(260, 0);
            this.ItemForClientReferenceNumber.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForClientReferenceNumber.Name = "ItemForClientReferenceNumber";
            this.ItemForClientReferenceNumber.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForClientReferenceNumber.Size = new System.Drawing.Size(324, 24);
            this.ItemForClientReferenceNumber.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClientReferenceNumber.Text = "Client Reference Number:";
            this.ItemForClientReferenceNumber.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForNoClientPO
            // 
            this.ItemForNoClientPO.Control = this.NoClientPOCheckEdit;
            this.ItemForNoClientPO.CustomizationFormText = "No Client PO #:";
            this.ItemForNoClientPO.Location = new System.Drawing.Point(232, 24);
            this.ItemForNoClientPO.MinSize = new System.Drawing.Size(225, 23);
            this.ItemForNoClientPO.Name = "ItemForNoClientPO";
            this.ItemForNoClientPO.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForNoClientPO.Size = new System.Drawing.Size(225, 24);
            this.ItemForNoClientPO.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForNoClientPO.Text = "No Client PO #:";
            this.ItemForNoClientPO.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForNoClientPOAuthorisedByDirector
            // 
            this.ItemForNoClientPOAuthorisedByDirector.Control = this.NoClientPOAuthorisedByDirectorButtonEdit;
            this.ItemForNoClientPOAuthorisedByDirector.CustomizationFormText = "Authorised By:";
            this.ItemForNoClientPOAuthorisedByDirector.Location = new System.Drawing.Point(457, 24);
            this.ItemForNoClientPOAuthorisedByDirector.MinSize = new System.Drawing.Size(127, 24);
            this.ItemForNoClientPOAuthorisedByDirector.Name = "ItemForNoClientPOAuthorisedByDirector";
            this.ItemForNoClientPOAuthorisedByDirector.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForNoClientPOAuthorisedByDirector.Size = new System.Drawing.Size(127, 24);
            this.ItemForNoClientPOAuthorisedByDirector.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForNoClientPOAuthorisedByDirector.Text = "Authorised By:";
            this.ItemForNoClientPOAuthorisedByDirector.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForNoClientPOAuthorisedByDirector.TextSize = new System.Drawing.Size(71, 13);
            this.ItemForNoClientPOAuthorisedByDirector.TextToControlDistance = 5;
            // 
            // item16
            // 
            this.item16.AllowHotTrack = false;
            this.item16.CustomizationFormText = "item16";
            this.item16.Location = new System.Drawing.Point(0, 48);
            this.item16.Name = "item16";
            this.item16.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.item16.Size = new System.Drawing.Size(584, 10);
            this.item16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForClientPONumber
            // 
            this.ItemForClientPONumber.Control = this.ClientPONumberTextEdit;
            this.ItemForClientPONumber.CustomizationFormText = "Client PO #:";
            this.ItemForClientPONumber.Location = new System.Drawing.Point(0, 24);
            this.ItemForClientPONumber.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForClientPONumber.Name = "ItemForClientPONumber";
            this.ItemForClientPONumber.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForClientPONumber.Size = new System.Drawing.Size(232, 24);
            this.ItemForClientPONumber.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClientPONumber.Text = "Client PO #:";
            this.ItemForClientPONumber.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForContactPersonName
            // 
            this.ItemForContactPersonName.Control = this.ContactPersonNameButtonEdit;
            this.ItemForContactPersonName.CustomizationFormText = "Person Name:";
            this.ItemForContactPersonName.Location = new System.Drawing.Point(0, 58);
            this.ItemForContactPersonName.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForContactPersonName.Name = "ItemForContactPersonName";
            this.ItemForContactPersonName.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForContactPersonName.Size = new System.Drawing.Size(259, 24);
            this.ItemForContactPersonName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForContactPersonName.Text = "Person Name:";
            this.ItemForContactPersonName.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForContactPersonPosition
            // 
            this.ItemForContactPersonPosition.Control = this.ContactPersonPositionTextEdit;
            this.ItemForContactPersonPosition.CustomizationFormText = "Person Position:";
            this.ItemForContactPersonPosition.Location = new System.Drawing.Point(0, 82);
            this.ItemForContactPersonPosition.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForContactPersonPosition.Name = "ItemForContactPersonPosition";
            this.ItemForContactPersonPosition.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForContactPersonPosition.Size = new System.Drawing.Size(259, 24);
            this.ItemForContactPersonPosition.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForContactPersonPosition.Text = "Person Position:";
            this.ItemForContactPersonPosition.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForClientContactTelephone
            // 
            this.ItemForClientContactTelephone.Control = this.ClientContactTelephoneTextEdit;
            this.ItemForClientContactTelephone.CustomizationFormText = "Contact Telephone:";
            this.ItemForClientContactTelephone.Location = new System.Drawing.Point(0, 106);
            this.ItemForClientContactTelephone.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForClientContactTelephone.Name = "ItemForClientContactTelephone";
            this.ItemForClientContactTelephone.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForClientContactTelephone.Size = new System.Drawing.Size(259, 24);
            this.ItemForClientContactTelephone.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClientContactTelephone.Text = "Contact Telephone:";
            this.ItemForClientContactTelephone.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForClientContactMobile
            // 
            this.ItemForClientContactMobile.Control = this.ClientContactMobileTextEdit;
            this.ItemForClientContactMobile.CustomizationFormText = "Contact Mobile:";
            this.ItemForClientContactMobile.Location = new System.Drawing.Point(259, 106);
            this.ItemForClientContactMobile.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForClientContactMobile.Name = "ItemForClientContactMobile";
            this.ItemForClientContactMobile.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForClientContactMobile.Size = new System.Drawing.Size(325, 24);
            this.ItemForClientContactMobile.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClientContactMobile.Text = "Contact Mobile:";
            this.ItemForClientContactMobile.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForContactPersonTitle
            // 
            this.ItemForContactPersonTitle.Control = this.ContactPersonTitleTextEdit;
            this.ItemForContactPersonTitle.CustomizationFormText = "Person Title:";
            this.ItemForContactPersonTitle.Location = new System.Drawing.Point(259, 82);
            this.ItemForContactPersonTitle.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForContactPersonTitle.Name = "ItemForContactPersonTitle";
            this.ItemForContactPersonTitle.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForContactPersonTitle.Size = new System.Drawing.Size(325, 24);
            this.ItemForContactPersonTitle.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForContactPersonTitle.Text = "Person Title:";
            this.ItemForContactPersonTitle.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForClientContactEmail
            // 
            this.ItemForClientContactEmail.Control = this.ClientContactEmailHyperLinkEdit;
            this.ItemForClientContactEmail.CustomizationFormText = "Contact Email:";
            this.ItemForClientContactEmail.Location = new System.Drawing.Point(259, 58);
            this.ItemForClientContactEmail.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForClientContactEmail.Name = "ItemForClientContactEmail";
            this.ItemForClientContactEmail.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForClientContactEmail.Size = new System.Drawing.Size(325, 24);
            this.ItemForClientContactEmail.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClientContactEmail.Text = "Contact Email:";
            this.ItemForClientContactEmail.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForReactive
            // 
            this.ItemForReactive.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.ItemForReactive.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForReactive.Control = this.ReactiveCheckEdit;
            this.ItemForReactive.CustomizationFormText = "Reactive:";
            this.ItemForReactive.Location = new System.Drawing.Point(0, 0);
            this.ItemForReactive.MinSize = new System.Drawing.Size(225, 23);
            this.ItemForReactive.Name = "ItemForReactive";
            this.ItemForReactive.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForReactive.Size = new System.Drawing.Size(377, 23);
            this.ItemForReactive.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForReactive.Text = "Reactive:";
            this.ItemForReactive.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForEndDate
            // 
            this.ItemForEndDate.Control = this.ReturnToClientByDateDateEdit;
            this.ItemForEndDate.CustomizationFormText = "Return To Client By Date:";
            this.ItemForEndDate.Location = new System.Drawing.Point(0, 47);
            this.ItemForEndDate.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForEndDate.Name = "ItemForEndDate";
            this.ItemForEndDate.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForEndDate.Size = new System.Drawing.Size(331, 24);
            this.ItemForEndDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForEndDate.Text = "Return To Client By:";
            this.ItemForEndDate.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForRAMS
            // 
            this.ItemForRAMS.Control = this.RAMSCheckEdit;
            this.ItemForRAMS.CustomizationFormText = "RAMS:";
            this.ItemForRAMS.Location = new System.Drawing.Point(377, 0);
            this.ItemForRAMS.MinSize = new System.Drawing.Size(225, 23);
            this.ItemForRAMS.Name = "ItemForRAMS";
            this.ItemForRAMS.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForRAMS.Size = new System.Drawing.Size(285, 23);
            this.ItemForRAMS.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRAMS.Text = "RAMS:";
            this.ItemForRAMS.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForCMInitialAttendanceDate
            // 
            this.ItemForCMInitialAttendanceDate.Control = this.CMInitialAttendanceDateDateEdit;
            this.ItemForCMInitialAttendanceDate.CustomizationFormText = "CM Initial Attendance By Date:";
            this.ItemForCMInitialAttendanceDate.Location = new System.Drawing.Point(331, 23);
            this.ItemForCMInitialAttendanceDate.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForCMInitialAttendanceDate.Name = "ItemForCMInitialAttendanceDate";
            this.ItemForCMInitialAttendanceDate.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForCMInitialAttendanceDate.Size = new System.Drawing.Size(331, 24);
            this.ItemForCMInitialAttendanceDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCMInitialAttendanceDate.Text = "CM Initial Attendance By:";
            this.ItemForCMInitialAttendanceDate.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForRequiredWorkCompletedDate
            // 
            this.ItemForRequiredWorkCompletedDate.Control = this.RequiredWorkCompletedDateDateEdit;
            this.ItemForRequiredWorkCompletedDate.CustomizationFormText = "Required Completion By:";
            this.ItemForRequiredWorkCompletedDate.Location = new System.Drawing.Point(331, 47);
            this.ItemForRequiredWorkCompletedDate.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForRequiredWorkCompletedDate.Name = "ItemForRequiredWorkCompletedDate";
            this.ItemForRequiredWorkCompletedDate.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForRequiredWorkCompletedDate.Size = new System.Drawing.Size(331, 24);
            this.ItemForRequiredWorkCompletedDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRequiredWorkCompletedDate.Text = "Required Completion By:";
            this.ItemForRequiredWorkCompletedDate.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForRequestReceivedDate
            // 
            this.ItemForRequestReceivedDate.Control = this.RequestReceivedDateDateEdit;
            this.ItemForRequestReceivedDate.CustomizationFormText = "Request Received Date:";
            this.ItemForRequestReceivedDate.Location = new System.Drawing.Point(0, 23);
            this.ItemForRequestReceivedDate.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForRequestReceivedDate.Name = "ItemForRequestReceivedDate";
            this.ItemForRequestReceivedDate.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForRequestReceivedDate.Size = new System.Drawing.Size(331, 24);
            this.ItemForRequestReceivedDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRequestReceivedDate.Text = "Request Received:";
            this.ItemForRequestReceivedDate.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForClientQuoteSpreadsheetExtractFile
            // 
            this.ItemForClientQuoteSpreadsheetExtractFile.Control = this.ClientQuoteSpreadsheetExtractFileButtonEdit;
            this.ItemForClientQuoteSpreadsheetExtractFile.CustomizationFormText = "Client Quote SS Extract:";
            this.ItemForClientQuoteSpreadsheetExtractFile.Location = new System.Drawing.Point(0, 71);
            this.ItemForClientQuoteSpreadsheetExtractFile.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForClientQuoteSpreadsheetExtractFile.Name = "ItemForClientQuoteSpreadsheetExtractFile";
            this.ItemForClientQuoteSpreadsheetExtractFile.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForClientQuoteSpreadsheetExtractFile.Size = new System.Drawing.Size(662, 24);
            this.ItemForClientQuoteSpreadsheetExtractFile.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClientQuoteSpreadsheetExtractFile.Text = "Client Quote SS Extract:";
            this.ItemForClientQuoteSpreadsheetExtractFile.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForTPORequired
            // 
            this.ItemForTPORequired.Control = this.TPORequiredCheckEdit;
            this.ItemForTPORequired.CustomizationFormText = "TPO Check Required:";
            this.ItemForTPORequired.Location = new System.Drawing.Point(0, 0);
            this.ItemForTPORequired.MinSize = new System.Drawing.Size(225, 23);
            this.ItemForTPORequired.Name = "ItemForTPORequired";
            this.ItemForTPORequired.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForTPORequired.Size = new System.Drawing.Size(496, 24);
            this.ItemForTPORequired.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTPORequired.Text = "TPO Check Required:";
            this.ItemForTPORequired.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForPlanningAuthority
            // 
            this.ItemForPlanningAuthority.Control = this.PlanningAuthorityButtonEdit;
            this.ItemForPlanningAuthority.CustomizationFormText = "Planning Authority";
            this.ItemForPlanningAuthority.Location = new System.Drawing.Point(0, 24);
            this.ItemForPlanningAuthority.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForPlanningAuthority.Name = "ItemForPlanningAuthority";
            this.ItemForPlanningAuthority.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForPlanningAuthority.Size = new System.Drawing.Size(358, 24);
            this.ItemForPlanningAuthority.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForPlanningAuthority.Text = "Planning Authority";
            this.ItemForPlanningAuthority.TextSize = new System.Drawing.Size(142, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(496, 0);
            this.emptySpaceItem13.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.emptySpaceItem13.Size = new System.Drawing.Size(166, 24);
            this.emptySpaceItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForPlanningAuthorityNumber
            // 
            this.ItemForPlanningAuthorityNumber.Control = this.PlanningAuthorityNumberTextEdit;
            this.ItemForPlanningAuthorityNumber.CustomizationFormText = "Planning Authority Number:";
            this.ItemForPlanningAuthorityNumber.Location = new System.Drawing.Point(0, 143);
            this.ItemForPlanningAuthorityNumber.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForPlanningAuthorityNumber.Name = "ItemForPlanningAuthorityNumber";
            this.ItemForPlanningAuthorityNumber.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForPlanningAuthorityNumber.Size = new System.Drawing.Size(358, 24);
            this.ItemForPlanningAuthorityNumber.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForPlanningAuthorityNumber.Text = "Planning Authority Number:";
            this.ItemForPlanningAuthorityNumber.TextSize = new System.Drawing.Size(142, 13);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.CustomizationFormText = "emptySpaceItem16";
            this.emptySpaceItem16.Location = new System.Drawing.Point(358, 167);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.emptySpaceItem16.Size = new System.Drawing.Size(304, 24);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForPlanningProceedDateSet
            // 
            this.ItemForPlanningProceedDateSet.Control = this.PlanningProceedDateSetTextEdit;
            this.ItemForPlanningProceedDateSet.CustomizationFormText = "Planning OK Date Set:";
            this.ItemForPlanningProceedDateSet.Location = new System.Drawing.Point(0, 214);
            this.ItemForPlanningProceedDateSet.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForPlanningProceedDateSet.Name = "ItemForPlanningProceedDateSet";
            this.ItemForPlanningProceedDateSet.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForPlanningProceedDateSet.Size = new System.Drawing.Size(358, 24);
            this.ItemForPlanningProceedDateSet.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForPlanningProceedDateSet.Text = "Planning OK Date Set:";
            this.ItemForPlanningProceedDateSet.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForPlanningProceedStaffName
            // 
            this.ItemForPlanningProceedStaffName.Control = this.PlanningProceedStaffNameTextEdit;
            this.ItemForPlanningProceedStaffName.CustomizationFormText = "Planning OK Set By:";
            this.ItemForPlanningProceedStaffName.Location = new System.Drawing.Point(358, 214);
            this.ItemForPlanningProceedStaffName.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForPlanningProceedStaffName.Name = "ItemForPlanningProceedStaffName";
            this.ItemForPlanningProceedStaffName.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForPlanningProceedStaffName.Size = new System.Drawing.Size(304, 24);
            this.ItemForPlanningProceedStaffName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForPlanningProceedStaffName.Text = "Planning OK Set By:";
            this.ItemForPlanningProceedStaffName.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForConservationStatusID
            // 
            this.ItemForConservationStatusID.Control = this.ConservationStatusIDGridLookUpEdit;
            this.ItemForConservationStatusID.CustomizationFormText = "Conservation Status:";
            this.ItemForConservationStatusID.Location = new System.Drawing.Point(0, 119);
            this.ItemForConservationStatusID.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForConservationStatusID.Name = "ItemForConservationStatusID";
            this.ItemForConservationStatusID.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForConservationStatusID.Size = new System.Drawing.Size(358, 24);
            this.ItemForConservationStatusID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForConservationStatusID.Text = "Conservation Status:";
            this.ItemForConservationStatusID.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForTPONumber
            // 
            this.ItemForTPONumber.Control = this.TPONumberTextEdit;
            this.ItemForTPONumber.CustomizationFormText = "TPO Number:";
            this.ItemForTPONumber.Location = new System.Drawing.Point(0, 95);
            this.ItemForTPONumber.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForTPONumber.Name = "ItemForTPONumber";
            this.ItemForTPONumber.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForTPONumber.Size = new System.Drawing.Size(662, 24);
            this.ItemForTPONumber.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTPONumber.Text = "TPO Number:";
            this.ItemForTPONumber.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForListedBuildingStatusID
            // 
            this.ItemForListedBuildingStatusID.Control = this.ListedBuildingStatusIDGridLookUpEdit;
            this.ItemForListedBuildingStatusID.CustomizationFormText = "Listed Building Status:";
            this.ItemForListedBuildingStatusID.Location = new System.Drawing.Point(358, 119);
            this.ItemForListedBuildingStatusID.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForListedBuildingStatusID.Name = "ItemForListedBuildingStatusID";
            this.ItemForListedBuildingStatusID.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForListedBuildingStatusID.Size = new System.Drawing.Size(304, 24);
            this.ItemForListedBuildingStatusID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForListedBuildingStatusID.Text = "Listed Building Status:";
            this.ItemForListedBuildingStatusID.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForPlanningAuthorityOutcomeID
            // 
            this.ItemForPlanningAuthorityOutcomeID.Control = this.PlanningAuthorityOutcomeIDGridLookUpEdit;
            this.ItemForPlanningAuthorityOutcomeID.CustomizationFormText = "Planning Outcome:";
            this.ItemForPlanningAuthorityOutcomeID.Location = new System.Drawing.Point(0, 167);
            this.ItemForPlanningAuthorityOutcomeID.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForPlanningAuthorityOutcomeID.Name = "ItemForPlanningAuthorityOutcomeID";
            this.ItemForPlanningAuthorityOutcomeID.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForPlanningAuthorityOutcomeID.Size = new System.Drawing.Size(358, 24);
            this.ItemForPlanningAuthorityOutcomeID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForPlanningAuthorityOutcomeID.Text = "Planning Outcome:";
            this.ItemForPlanningAuthorityOutcomeID.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForLocalAuthorityWebSite
            // 
            this.ItemForLocalAuthorityWebSite.Control = this.LocalAuthorityWebSiteButtonEdit;
            this.ItemForLocalAuthorityWebSite.CustomizationFormText = "Planning Authority Web Site:";
            this.ItemForLocalAuthorityWebSite.Location = new System.Drawing.Point(358, 24);
            this.ItemForLocalAuthorityWebSite.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForLocalAuthorityWebSite.Name = "ItemForLocalAuthorityWebSite";
            this.ItemForLocalAuthorityWebSite.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForLocalAuthorityWebSite.Size = new System.Drawing.Size(304, 24);
            this.ItemForLocalAuthorityWebSite.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLocalAuthorityWebSite.Text = "Web Site:";
            this.ItemForLocalAuthorityWebSite.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForPlanningAuthoritySubmittedDate
            // 
            this.ItemForPlanningAuthoritySubmittedDate.Control = this.PlanningAuthoritySubmittedDateDateEdit;
            this.ItemForPlanningAuthoritySubmittedDate.CustomizationFormText = "Planning Authority Submitted Date:";
            this.ItemForPlanningAuthoritySubmittedDate.Location = new System.Drawing.Point(358, 143);
            this.ItemForPlanningAuthoritySubmittedDate.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForPlanningAuthoritySubmittedDate.Name = "ItemForPlanningAuthoritySubmittedDate";
            this.ItemForPlanningAuthoritySubmittedDate.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForPlanningAuthoritySubmittedDate.Size = new System.Drawing.Size(304, 24);
            this.ItemForPlanningAuthoritySubmittedDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForPlanningAuthoritySubmittedDate.Text = "Planning Submitted Date:";
            this.ItemForPlanningAuthoritySubmittedDate.TextSize = new System.Drawing.Size(142, 13);
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.CustomizationFormText = "Comments";
            this.layoutControlGroup11.ExpandButtonVisible = true;
            this.layoutControlGroup11.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForOurInternalComments,
            this.ItemForCMComments,
            this.ItemForClientComments,
            this.ItemForKAMQuoteRejectionRemarks});
            this.layoutControlGroup11.Location = new System.Drawing.Point(0, 238);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlGroup11.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.layoutControlGroup11.Size = new System.Drawing.Size(662, 189);
            this.layoutControlGroup11.Text = "Comments";
            // 
            // ItemForPlanningAuthorityOkToProceed
            // 
            this.ItemForPlanningAuthorityOkToProceed.Control = this.PlanningAuthorityOkToProceedCheckEdit;
            this.ItemForPlanningAuthorityOkToProceed.CustomizationFormText = "Planning Ok To Proceed:";
            this.ItemForPlanningAuthorityOkToProceed.Location = new System.Drawing.Point(0, 191);
            this.ItemForPlanningAuthorityOkToProceed.MinSize = new System.Drawing.Size(225, 23);
            this.ItemForPlanningAuthorityOkToProceed.Name = "ItemForPlanningAuthorityOkToProceed";
            this.ItemForPlanningAuthorityOkToProceed.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForPlanningAuthorityOkToProceed.Size = new System.Drawing.Size(662, 23);
            this.ItemForPlanningAuthorityOkToProceed.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForPlanningAuthorityOkToProceed.Text = "Ok To Proceed:";
            this.ItemForPlanningAuthorityOkToProceed.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForTreeProtected
            // 
            this.ItemForTreeProtected.Control = this.TreeProtectedCheckEdit;
            this.ItemForTreeProtected.CustomizationFormText = "Tree Protected:";
            this.ItemForTreeProtected.Location = new System.Drawing.Point(0, 72);
            this.ItemForTreeProtected.MinSize = new System.Drawing.Size(225, 23);
            this.ItemForTreeProtected.Name = "ItemForTreeProtected";
            this.ItemForTreeProtected.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForTreeProtected.Size = new System.Drawing.Size(662, 23);
            this.ItemForTreeProtected.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTreeProtected.Text = "Tree Protected:";
            this.ItemForTreeProtected.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForTPOCheckedDate
            // 
            this.ItemForTPOCheckedDate.Control = this.TPOCheckedDateDateEdit;
            this.ItemForTPOCheckedDate.CustomizationFormText = "TPO Checked Date:";
            this.ItemForTPOCheckedDate.Location = new System.Drawing.Point(0, 48);
            this.ItemForTPOCheckedDate.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForTPOCheckedDate.Name = "ItemForTPOCheckedDate";
            this.ItemForTPOCheckedDate.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForTPOCheckedDate.Size = new System.Drawing.Size(358, 24);
            this.ItemForTPOCheckedDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTPOCheckedDate.Text = "TPO Checked Date:";
            this.ItemForTPOCheckedDate.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForLocalAuthorityTelephone
            // 
            this.ItemForLocalAuthorityTelephone.Control = this.LocalAuthorityTelephoneTextEdit;
            this.ItemForLocalAuthorityTelephone.CustomizationFormText = "Planning Authority Telephone:";
            this.ItemForLocalAuthorityTelephone.Location = new System.Drawing.Point(358, 48);
            this.ItemForLocalAuthorityTelephone.MinSize = new System.Drawing.Size(197, 24);
            this.ItemForLocalAuthorityTelephone.Name = "ItemForLocalAuthorityTelephone";
            this.ItemForLocalAuthorityTelephone.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForLocalAuthorityTelephone.Size = new System.Drawing.Size(304, 24);
            this.ItemForLocalAuthorityTelephone.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLocalAuthorityTelephone.Text = "Telephone:";
            this.ItemForLocalAuthorityTelephone.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForOurInternalComments
            // 
            this.ItemForOurInternalComments.Control = this.OurInternalCommentsMemoEdit;
            this.ItemForOurInternalComments.CustomizationFormText = "Our Internal Comments:";
            this.ItemForOurInternalComments.Location = new System.Drawing.Point(0, 0);
            this.ItemForOurInternalComments.MinSize = new System.Drawing.Size(157, 20);
            this.ItemForOurInternalComments.Name = "ItemForOurInternalComments";
            this.ItemForOurInternalComments.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForOurInternalComments.Size = new System.Drawing.Size(638, 39);
            this.ItemForOurInternalComments.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForOurInternalComments.Text = "Our Internal:";
            this.ItemForOurInternalComments.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForCMComments
            // 
            this.ItemForCMComments.Control = this.CMCommentsMemoEdit;
            this.ItemForCMComments.CustomizationFormText = "CM Comments:";
            this.ItemForCMComments.Location = new System.Drawing.Point(0, 39);
            this.ItemForCMComments.MinSize = new System.Drawing.Size(157, 20);
            this.ItemForCMComments.Name = "ItemForCMComments";
            this.ItemForCMComments.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForCMComments.Size = new System.Drawing.Size(638, 32);
            this.ItemForCMComments.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCMComments.Text = "CM:";
            this.ItemForCMComments.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForClientComments
            // 
            this.ItemForClientComments.Control = this.ClientCommentsMemoEdit;
            this.ItemForClientComments.CustomizationFormText = "Client Visible Comments:";
            this.ItemForClientComments.Location = new System.Drawing.Point(0, 71);
            this.ItemForClientComments.MinSize = new System.Drawing.Size(157, 20);
            this.ItemForClientComments.Name = "ItemForClientComments";
            this.ItemForClientComments.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForClientComments.Size = new System.Drawing.Size(638, 33);
            this.ItemForClientComments.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClientComments.Text = "Client Visible:";
            this.ItemForClientComments.TextSize = new System.Drawing.Size(142, 13);
            // 
            // ItemForKAMQuoteRejectionRemarks
            // 
            this.ItemForKAMQuoteRejectionRemarks.Control = this.KAMQuoteRejectionRemarksMemoEdit;
            this.ItemForKAMQuoteRejectionRemarks.CustomizationFormText = "KAM Quote Rejection:";
            this.ItemForKAMQuoteRejectionRemarks.Location = new System.Drawing.Point(0, 104);
            this.ItemForKAMQuoteRejectionRemarks.MinSize = new System.Drawing.Size(157, 20);
            this.ItemForKAMQuoteRejectionRemarks.Name = "ItemForKAMQuoteRejectionRemarks";
            this.ItemForKAMQuoteRejectionRemarks.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.ItemForKAMQuoteRejectionRemarks.Size = new System.Drawing.Size(638, 39);
            this.ItemForKAMQuoteRejectionRemarks.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForKAMQuoteRejectionRemarks.Text = "KAM Quote Rejection:";
            this.ItemForKAMQuoteRejectionRemarks.TextSize = new System.Drawing.Size(142, 13);
            // 
            // autoGeneratedGroup1
            // 
            this.autoGeneratedGroup1.AllowDrawBackground = false;
            this.autoGeneratedGroup1.CustomizationFormText = "autoGeneratedGroup1";
            this.autoGeneratedGroup1.GroupBordersVisible = false;
            this.autoGeneratedGroup1.Location = new System.Drawing.Point(0, 828);
            this.autoGeneratedGroup1.Name = "autoGeneratedGroup1";
            this.autoGeneratedGroup1.OptionsPrint.AppearanceGroupCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.autoGeneratedGroup1.OptionsPrint.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.autoGeneratedGroup1.Size = new System.Drawing.Size(1629, 19);
            // 
            // frm_OM_Tender_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1361, 710);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Tender_Edit";
            this.Text = "Edit Tender";
            this.Activated += new System.EventHandler(this.frm_OM_Tender_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Tender_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Tender_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ProposedTotalCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06498OMTenderEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Tender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProposedEquipmentCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProposedMaterialCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProposedLabourCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContactEmailHyperLinkEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContactMobileTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContactTelephoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RAMSCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ListedBuildingStatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01053CoreListedBuildingStatusesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConservationStatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01052CoreConservationStatusesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TPONumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningProceedDateSetTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningProceedStaffNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningProceedStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06525OMTenderTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmSendQuoteToCM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KAMQuoteRejectionReasonIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmKAMResponse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobCountTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitCountTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmClientResponse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientQuoteSpreadsheetExtractFileButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractorNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIssueIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIsssueButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubmittedToCMDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubmittedToCMDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualTotalCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualEquipmentCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualMaterialCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualLabourCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcceptedTotalSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcceptedEquipmentSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcceptedMaterialSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuotedTotalSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcceptedLabourSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuotedEquipmentSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuotedMaterialSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoClientPOAuthorisedByDirectorButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoClientPOEmailedToDirectorCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoClientPOAuthorisedByDirectorIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoClientPOCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RevisionNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderProactiveDaysToReturnQuoteTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderReactiveDaysToReturnQuoteTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SectorTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GCCompanyNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocalAuthorityWebSiteButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocalAuthorityTelephoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactPersonTitleTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactPersonPositionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteCodeButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningAuthorityOkToProceedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningAuthoritySubmittedDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningAuthoritySubmittedDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningAuthorityNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeProtectedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TPOCheckedDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TPOCheckedDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningAuthorityButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningAuthorityIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TPORequiredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientCommentsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CMCommentsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequiredWorkCompletedDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequiredWorkCompletedDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteAcceptedByClientDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteAcceptedByClientDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteSubmittedToClientDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteSubmittedToClientDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CMInitialAttendanceDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CMInitialAttendanceDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteLocationYTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteLocationXTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SitePostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderGroupIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderGroupDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobSubTypeDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkSubTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CMNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CMTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KAMTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KAMNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContactIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContactPersonNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderDescriptionMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderStatusButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientReferenceNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OurInternalCommentsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuotedLabourSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteNotRequiredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReturnToClientByDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReturnToClientByDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequestReceivedDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequestReceivedDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningAuthorityOutcomeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06503OMTenderPlanningOutcomesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TenderIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KAMQuoteRejectionReasonTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KAMQuoteRejectionRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContactID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkSubTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderGroupID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteLocationX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteLocationY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningAuthorityID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKAM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSectorType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderReactiveDaysToReturnQuote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderProactiveDaysToReturnQuote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusIssueID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoClientPOEmailedToDirector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoClientPOAuthorisedByDirectorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKAMQuoteRejectionReasonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningProceedStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06502OMTenderQuoteRejectionReasonsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.autoGeneratedGroup0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRevisionNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuoteNotRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCMReturnedQuoteBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSentToClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreateVisitsBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderCompletedBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubmittedToCMDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientResponseBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKAMQuoteRejectionReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuoteAcceptedByClientDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuoteSubmittedToClientDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderCreatedBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCancelTenderBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusIsssue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuotedLabourSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuotedMaterialSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuotedEquipmentSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuotedTotalSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAcceptedLabourSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAcceptedMaterialSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAcceptedEquipmentSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAcceptedTotalSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProposedLabourCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProposedMaterialCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProposedEquipmentCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProposedTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualLabourCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualMaterialCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualEquipmentCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobSubTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCMName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItmeForKAMName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderGroupDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTenderTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGCCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSitePostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientReferenceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoClientPO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoClientPOAuthorisedByDirector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactPersonName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactPersonPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContactTelephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContactMobile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContactPersonTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContactEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRAMS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCMInitialAttendanceDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRequiredWorkCompletedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRequestReceivedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientQuoteSpreadsheetExtractFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTPORequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningAuthority)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningAuthorityNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningProceedDateSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningProceedStaffName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForConservationStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForListedBuildingStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningAuthorityOutcomeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLocalAuthorityWebSite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningAuthoritySubmittedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningAuthorityOkToProceed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeProtected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTPOCheckedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLocalAuthorityTelephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOurInternalComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCMComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKAMQuoteRejectionRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.autoGeneratedGroup1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.ButtonEdit ClientNameButtonEdit;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DevExpress.XtraEditors.TextEdit TenderIDTextEdit;
        private DevExpress.XtraEditors.GridLookUpEdit PlanningAuthorityOutcomeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.DateEdit RequestReceivedDateDateEdit;
        private DevExpress.XtraEditors.DateEdit ReturnToClientByDateDateEdit;
        private DevExpress.XtraEditors.CheckEdit QuoteNotRequiredCheckEdit;
        private DevExpress.XtraEditors.SpinEdit QuotedLabourSellSpinEdit;
        private DevExpress.XtraEditors.CheckEdit ReactiveCheckEdit;
        private DevExpress.XtraEditors.MemoEdit OurInternalCommentsMemoEdit;
        private System.Windows.Forms.BindingSource sp06498OMTenderEditBindingSource;
        private DataSet_OM_Tender dataSet_OM_Tender;
        private DataSet_OM_TenderTableAdapters.sp06498_OM_Tender_EditTableAdapter sp06498_OM_Tender_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit ContractDescriptionTextEdit;
        private DevExpress.XtraEditors.TextEdit ClientContractIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientContractID;
        private DevExpress.XtraEditors.TextEdit SiteContractIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteContractID;
        private DevExpress.XtraEditors.TextEdit StatusIDTextEdit;
        private DevExpress.XtraEditors.ButtonEdit TenderStatusButtonEdit;
        private DevExpress.XtraEditors.TextEdit ClientReferenceNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusID;
        private DevExpress.XtraEditors.MemoEdit TenderDescriptionMemoEdit;
        private DevExpress.XtraEditors.ButtonEdit ContactPersonNameButtonEdit;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByStaffID;
        private DevExpress.XtraEditors.TextEdit ClientContactIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientContactID;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffNameTextEdit;
        private DevExpress.XtraEditors.TextEdit CMTextEdit;
        private DevExpress.XtraEditors.TextEdit KAMTextEdit;
        private DevExpress.XtraEditors.ButtonEdit KAMNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCM;
        private DevExpress.XtraLayout.LayoutControlItem ItemForKAM;
        private DevExpress.XtraEditors.ButtonEdit CMNameButtonEdit;
        private DevExpress.XtraEditors.ButtonEdit JobSubTypeDescriptionButtonEdit;
        private DevExpress.XtraEditors.TextEdit WorkSubTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkSubTypeID;
        private DevExpress.XtraEditors.TextEdit JobTypeDescriptionTextEdit;
        private DevExpress.XtraEditors.TextEdit JobTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobTypeID;
        private DevExpress.XtraEditors.ButtonEdit TenderGroupDescriptionButtonEdit;
        private DevExpress.XtraEditors.TextEdit TenderGroupIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTenderGroupID;
        private DevExpress.XtraEditors.TextEdit SiteLocationXTextEdit;
        private DevExpress.XtraEditors.TextEdit SitePostcodeTextEdit;
        private DevExpress.XtraEditors.MemoEdit SiteAddressMemoEdit;
        private DevExpress.XtraEditors.ButtonEdit SiteNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteLocationX;
        private DevExpress.XtraEditors.TextEdit SiteLocationYTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteLocationY;
        private DevExpress.XtraEditors.DateEdit CMInitialAttendanceDateDateEdit;
        private DevExpress.XtraEditors.DateEdit QuoteSubmittedToClientDateDateEdit;
        private DevExpress.XtraEditors.DateEdit RequiredWorkCompletedDateDateEdit;
        private DevExpress.XtraEditors.DateEdit QuoteAcceptedByClientDateDateEdit;
        private DevExpress.XtraEditors.TextEdit ClientPONumberTextEdit;
        private DevExpress.XtraEditors.MemoEdit ClientCommentsMemoEdit;
        private DevExpress.XtraEditors.MemoEdit CMCommentsMemoEdit;
        private DevExpress.XtraEditors.ButtonEdit PlanningAuthorityButtonEdit;
        private DevExpress.XtraEditors.TextEdit PlanningAuthorityIDTextEdit;
        private DevExpress.XtraEditors.CheckEdit TPORequiredCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPlanningAuthorityID;
        private DevExpress.XtraEditors.DateEdit PlanningAuthoritySubmittedDateDateEdit;
        private DevExpress.XtraEditors.TextEdit PlanningAuthorityNumberTextEdit;
        private DevExpress.XtraEditors.CheckEdit TreeProtectedCheckEdit;
        private DevExpress.XtraEditors.DateEdit TPOCheckedDateDateEdit;
        private DevExpress.XtraEditors.CheckEdit PlanningAuthorityOkToProceedCheckEdit;
        private DevExpress.XtraEditors.ButtonEdit SiteCodeButtonEdit;
        private DevExpress.XtraEditors.TextEdit ContactPersonPositionTextEdit;
        private DevExpress.XtraEditors.TextEdit ContactPersonTitleTextEdit;
        private System.Windows.Forms.BindingSource sp06502OMTenderQuoteRejectionReasonsWithBlankBindingSource;
        private DataSet_OM_TenderTableAdapters.sp06502_OM_Tender_Quote_Rejection_Reasons_With_BlankTableAdapter sp06502_OM_Tender_Quote_Rejection_Reasons_With_BlankTableAdapter;
        private System.Windows.Forms.BindingSource sp06503OMTenderPlanningOutcomesWithBlankBindingSource;
        private DataSet_OM_TenderTableAdapters.sp06503_OM_Tender_Planning_Outcomes_With_BlankTableAdapter sp06503_OM_Tender_Planning_Outcomes_With_BlankTableAdapter;
        private DevExpress.XtraEditors.TextEdit LocalAuthorityTelephoneTextEdit;
        private DevExpress.XtraEditors.ButtonEdit LocalAuthorityWebSiteButtonEdit;
        private DevExpress.XtraEditors.TextEdit SectorTypeTextEdit;
        private DevExpress.XtraEditors.TextEdit GCCompanyNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSectorType;
        private DevExpress.XtraEditors.TextEdit TenderProactiveDaysToReturnQuoteTextEdit;
        private DevExpress.XtraEditors.TextEdit TenderReactiveDaysToReturnQuoteTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTenderReactiveDaysToReturnQuote;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTenderProactiveDaysToReturnQuote;
        private DevExpress.XtraEditors.TextEdit RevisionNumberTextEdit;
        private DevExpress.XtraEditors.CheckEdit NoClientPOCheckEdit;
        private DevExpress.XtraEditors.TextEdit NoClientPOAuthorisedByDirectorIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNoClientPOAuthorisedByDirectorID;
        private DevExpress.XtraEditors.ButtonEdit NoClientPOAuthorisedByDirectorButtonEdit;
        private DevExpress.XtraEditors.CheckEdit NoClientPOEmailedToDirectorCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNoClientPOEmailedToDirector;
        private DevExpress.XtraEditors.SpinEdit QuotedEquipmentSellSpinEdit;
        private DevExpress.XtraEditors.SpinEdit QuotedMaterialSellSpinEdit;
        private DevExpress.XtraEditors.SpinEdit AcceptedEquipmentSellSpinEdit;
        private DevExpress.XtraEditors.SpinEdit AcceptedMaterialSellSpinEdit;
        private DevExpress.XtraEditors.SpinEdit QuotedTotalSellSpinEdit;
        private DevExpress.XtraEditors.SpinEdit AcceptedLabourSellSpinEdit;
        private DevExpress.XtraEditors.SpinEdit ActualTotalCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit ActualEquipmentCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit ActualMaterialCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit ActualLabourCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit AcceptedTotalSellSpinEdit;
        private DevExpress.XtraEditors.SimpleButton btnSendToClient;
        private DevExpress.XtraEditors.DateEdit SubmittedToCMDateDateEdit;
        private DevExpress.XtraEditors.TextEdit StatusIssueIDTextEdit;
        private DevExpress.XtraEditors.ButtonEdit StatusIsssueButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusIssueID;
        private DevExpress.XtraEditors.ButtonEdit LinkedToPersonTypeButtonEdit;
        private DevExpress.XtraEditors.ButtonEdit ContractorNameButtonEdit;
        private DevExpress.XtraEditors.ButtonEdit ClientQuoteSpreadsheetExtractFileButtonEdit;
        private DevExpress.XtraEditors.SimpleButton btnReturnedFromCM;
        private DevExpress.XtraEditors.SimpleButton btnTenderClosed;
        private DevExpress.XtraEditors.SimpleButton btnCreateVisits;
        private DevExpress.XtraEditors.DropDownButton ddBtnClientResponse;
        private DevExpress.XtraBars.BarButtonItem bbiClientAccepted;
        private DevExpress.XtraBars.BarButtonItem bbiClientRejected;
        private DevExpress.XtraBars.BarButtonItem bbiClientOnHold;
        private DevExpress.XtraBars.PopupMenu pmClientResponse;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private DevExpress.XtraEditors.TextEdit VisitCountTextEdit;
        private DevExpress.XtraEditors.TextEdit JobCountTextEdit;
        private DevExpress.XtraEditors.SimpleButton btnCancelTender;
        private DevExpress.XtraEditors.DropDownButton ddBtnKAMAuthorised;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.XtraBars.BarButtonItem bbiKAMAccepted;
        private DevExpress.XtraBars.BarButtonItem bbiKAMRejected;
        private DevExpress.XtraBars.PopupMenu pmKAMResponse;
        private DevExpress.XtraEditors.TextEdit KAMQuoteRejectionReasonTextEdit;
        private DevExpress.XtraEditors.TextEdit KAMQuoteRejectionReasonIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForKAMQuoteRejectionReasonID;
        private DevExpress.XtraEditors.DropDownButton ddBtnSentToCM;
        private DevExpress.XtraBars.BarButtonItem bbiSendQuoteToCM;
        private DevExpress.XtraBars.BarButtonItem bbiNoQuoteRequired;
        private DevExpress.XtraBars.PopupMenu pmSendQuoteToCM;
        private DevExpress.XtraEditors.GridLookUpEdit TenderTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.TextEdit PlanningProceedDateSetTextEdit;
        private DevExpress.XtraEditors.TextEdit PlanningProceedStaffNameTextEdit;
        private DevExpress.XtraEditors.TextEdit PlanningProceedStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPlanningProceedStaffID;
        private System.Windows.Forms.BindingSource sp06525OMTenderTypesWithBlankBindingSource;
        private DataSet_OM_TenderTableAdapters.sp06525_OM_Tender_Types_With_BlankTableAdapter sp06525_OM_Tender_Types_With_BlankTableAdapter;
        private DevExpress.XtraEditors.CheckEdit RAMSCheckEdit;
        private DevExpress.XtraEditors.GridLookUpEdit ListedBuildingStatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.GridLookUpEdit ConservationStatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.TextEdit TPONumberTextEdit;
        private System.Windows.Forms.BindingSource sp01053CoreListedBuildingStatusesWithBlankBindingSource;
        private DataSet_Common_Functionality dataSet_Common_Functionality;
        private System.Windows.Forms.BindingSource sp01052CoreConservationStatusesWithBlankBindingSource;
        private DataSet_Common_FunctionalityTableAdapters.sp01052_Core_Conservation_Statuses_With_BlankTableAdapter sp01052_Core_Conservation_Statuses_With_BlankTableAdapter;
        private DataSet_Common_FunctionalityTableAdapters.sp01053_Core_Listed_Building_Statuses_With_BlankTableAdapter sp01053_Core_Listed_Building_Statuses_With_BlankTableAdapter;
        private DevExpress.XtraBars.BarButtonItem bbiLinkedDocuments;
        private DevExpress.XtraEditors.MemoEdit KAMQuoteRejectionRemarksMemoEdit;
        private DevExpress.XtraEditors.LabelControl labelControlWarning;
        private DevExpress.XtraEditors.TextEdit ClientContactTelephoneTextEdit;
        private DevExpress.XtraEditors.TextEdit ClientContactMobileTextEdit;
        private DevExpress.XtraEditors.HyperLinkEdit ClientContactEmailHyperLinkEdit;
        private DevExpress.XtraEditors.SpinEdit ProposedTotalCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit ProposedEquipmentCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit ProposedMaterialCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit ProposedLabourCostSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup autoGeneratedGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup autoGeneratedGroup0;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRevisionNumber;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQuoteNotRequired;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCMReturnedQuoteBtn;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSentToClient;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreateVisitsBtn;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTenderCompletedBtn;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubmittedToCMDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientResponseBtn;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProgressBar;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitCount;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobCount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForKAMQuoteRejectionReason;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQuoteAcceptedByClientDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQuoteSubmittedToClientDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTenderCreatedBtn;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCancelTenderBtn;
        private DevExpress.XtraLayout.EmptySpaceItem item0;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTenderStatus;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusIsssue;
        private DevExpress.XtraLayout.EmptySpaceItem item1;
        private DevExpress.XtraLayout.EmptySpaceItem item5;
        private DevExpress.XtraLayout.EmptySpaceItem item7;
        private DevExpress.XtraLayout.EmptySpaceItem item8;
        private DevExpress.XtraLayout.EmptySpaceItem item9;
        private DevExpress.XtraLayout.EmptySpaceItem item10;
        private DevExpress.XtraLayout.EmptySpaceItem item11;
        private DevExpress.XtraLayout.EmptySpaceItem item12;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTenderID;
        private DevExpress.XtraLayout.LayoutControlGroup item2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQuotedLabourSell;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQuotedMaterialSell;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQuotedEquipmentSell;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQuotedTotalSell;
        private DevExpress.XtraLayout.EmptySpaceItem item4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAcceptedLabourSell;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAcceptedMaterialSell;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAcceptedEquipmentSell;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAcceptedTotalSell;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup15;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProposedLabourCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProposedMaterialCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProposedEquipmentCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProposedTotalCost;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualLabourCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualMaterialCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualEquipmentCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualTotalCost;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.SplitterItem item3;
        private DevExpress.XtraLayout.LayoutControlGroup item6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobSubTypeDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobTypeDescription;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCMName;
        private DevExpress.XtraLayout.LayoutControlItem ItmeForKAMName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTenderDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTenderGroupDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTenderTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByStaffName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGCCompanyName;
        private DevExpress.XtraLayout.EmptySpaceItem item13;
        private DevExpress.XtraLayout.EmptySpaceItem item14;
        private DevExpress.XtraLayout.EmptySpaceItem item15;
        private DevExpress.XtraLayout.LayoutControlGroup item18;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSitePostcode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddress;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteCode;
        private DevExpress.XtraLayout.LayoutControlGroup item19;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientReferenceNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNoClientPO;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNoClientPOAuthorisedByDirector;
        private DevExpress.XtraLayout.EmptySpaceItem item16;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPONumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactPersonName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactPersonPosition;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientContactTelephone;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientContactMobile;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContactPersonTitle;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientContactEmail;
        private DevExpress.XtraLayout.LayoutControlGroup item17;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReactive;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRAMS;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCMInitialAttendanceDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRequiredWorkCompletedDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRequestReceivedDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientQuoteSpreadsheetExtractFile;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTPORequired;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPlanningAuthority;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPlanningAuthorityNumber;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPlanningProceedDateSet;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPlanningProceedStaffName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForConservationStatusID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTPONumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForListedBuildingStatusID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPlanningAuthorityOutcomeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLocalAuthorityWebSite;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPlanningAuthoritySubmittedDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOurInternalComments;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCMComments;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientComments;
        private DevExpress.XtraLayout.LayoutControlItem ItemForKAMQuoteRejectionRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPlanningAuthorityOkToProceed;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTreeProtected;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTPOCheckedDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLocalAuthorityTelephone;
    }
}

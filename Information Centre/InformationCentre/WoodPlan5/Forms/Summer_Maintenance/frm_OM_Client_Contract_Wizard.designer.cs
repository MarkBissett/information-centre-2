﻿namespace WoodPlan5
{
    partial class frm_OM_Client_Contract_Wizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Client_Contract_Wizard));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            this.colActiveClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageWelcome = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.btnWelcomeNext = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageStep1 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.btnStep1Previous = new DevExpress.XtraEditors.SimpleButton();
            this.bntStep1Next = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp06058OMClientContractWizardClientListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageStep2 = new DevExpress.XtraTab.XtraTabPage();
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.ClientInstructionsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.sp06059OMClientContractEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ReactiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ContractDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.BillingCentreCodeButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.CostCentreTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CostCentreCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CostCentreIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.BillingCentreCodeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FinanceClientCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LinkedToPreviousContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LinkedToPreviousContractButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.YearlyPercentageIncreaseSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ContractValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SectorTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06061OMSectorsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ActiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.LastClientPaymentDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.CheckRPIDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.EndDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.StartDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ContractDirectorIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ContractDirectorButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ContractStatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06062OMContractStatusesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ContractTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06063OMContractTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GCCompanyIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp06060OMGCCompaniesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ClientIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientContractIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForClientContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToPreviousContractID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBillingCentreCodeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractDirectorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostCentre = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostCentreCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostCentreID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForGCCompanyID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCheckRPIDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFinanceClientCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToPreviousContract = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractDirector = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActive = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSectorTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForYearlyPercentageIncrease = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLastClientPaymentDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForBillingCentreCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForContractDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReactive = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientInstructions = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.btnStep2Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep2Previous = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPageStep3 = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnAddYearsBtn = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp06067OMClientContractYearEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientContractYearID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit50CharsMax = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colClientValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colYearlyPercentageIncrease = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.spinEditContractYears = new DevExpress.XtraEditors.SpinEdit();
            this.textEditEndDate = new DevExpress.XtraEditors.TextEdit();
            this.textEditStartDate = new DevExpress.XtraEditors.TextEdit();
            this.textEditClientName = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForYearsGrid = new DevExpress.XtraLayout.LayoutControlItem();
            this.ITemForAddYearsToGridButton = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnStep3Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep3Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit7 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageStep4 = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp06069OMClientContractYearBillingProfileEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientContractProfileID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractYearID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceDateDue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colInvoiceDateActual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedBillAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colActualBillAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBilledByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedToParent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnStep4Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep4Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit8 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageStep5 = new DevExpress.XtraTab.XtraTabPage();
            this.standaloneBarDockControl2 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPersonResponsibilityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibleForRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibleForRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibilityTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibilityType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditResponsibilityType = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colPersonTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnStep5Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep5Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit9 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageFinish = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.btnFinish = new DevExpress.XtraEditors.SimpleButton();
            this.btnFinishPrevious = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.sp06058_OM_Client_Contract_Wizard_Client_ListTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06058_OM_Client_Contract_Wizard_Client_ListTableAdapter();
            this.sp06059_OM_Client_Contract_EditTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06059_OM_Client_Contract_EditTableAdapter();
            this.sp06060_OM_GC_Companies_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06060_OM_GC_Companies_With_BlankTableAdapter();
            this.sp06061_OM_Sectors_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06061_OM_Sectors_With_BlankTableAdapter();
            this.sp06062_OM_Contract_Statuses_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06062_OM_Contract_Statuses_With_BlankTableAdapter();
            this.sp06063_OM_Contract_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06063_OM_Contract_Types_With_BlankTableAdapter();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.sp06067_OM_Client_Contract_Year_EditTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06067_OM_Client_Contract_Year_EditTableAdapter();
            this.sp06069_OM_Client_Contract_Year_Billing_Profile_EditTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06069_OM_Client_Contract_Year_Billing_Profile_EditTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiAddProfile = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddProfileFromTemplate = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEditTemplates = new DevExpress.XtraBars.BarButtonItem();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiAddPersonResponsibility = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageWelcome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            this.xtraTabPageStep1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06058OMClientContractWizardClientListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            this.xtraTabPageStep2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ClientInstructionsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06059OMClientContractEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BillingCentreCodeButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BillingCentreCodeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinanceClientCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPreviousContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPreviousContractButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YearlyPercentageIncreaseSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SectorTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06061OMSectorsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastClientPaymentDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastClientPaymentDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckRPIDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckRPIDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractDirectorIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractDirectorButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractStatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06062OMContractStatusesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06063OMContractTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GCCompanyIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06060OMGCCompaniesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContractIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPreviousContractID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBillingCentreCodeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractDirectorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCentre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCentreCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCentreID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGCCompanyID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCheckRPIDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinanceClientCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPreviousContract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractDirector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSectorTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForYearlyPercentageIncrease)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastClientPaymentDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBillingCentreCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientInstructions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            this.xtraTabPageStep3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06067OMClientContractYearEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit50CharsMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditContractYears.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditClientName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForYearsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITemForAddYearsToGridButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).BeginInit();
            this.xtraTabPageStep4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06069OMClientContractYearBillingProfileEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).BeginInit();
            this.xtraTabPageStep5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditResponsibilityType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit9.Properties)).BeginInit();
            this.xtraTabPageFinish.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(869, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Size = new System.Drawing.Size(869, 0);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(869, 0);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl2);
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiAddProfile,
            this.bbiAddProfileFromTemplate,
            this.bbiEditTemplates,
            this.bbiAddPersonResponsibility});
            this.barManager1.MaxItemId = 41;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActiveClient
            // 
            this.colActiveClient.Caption = "Active Client";
            this.colActiveClient.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActiveClient.FieldName = "ActiveClient";
            this.colActiveClient.Name = "colActiveClient";
            this.colActiveClient.OptionsColumn.AllowEdit = false;
            this.colActiveClient.OptionsColumn.AllowFocus = false;
            this.colActiveClient.OptionsColumn.ReadOnly = true;
            this.colActiveClient.Visible = true;
            this.colActiveClient.VisibleIndex = 2;
            this.colActiveClient.Width = 81;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 53;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 53;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "ID";
            this.gridColumn7.FieldName = "ID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 53;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.Width = 53;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = global::WoodPlan5.Properties.Resources.wizard_image;
            this.pictureEdit1.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(205, 536);
            this.pictureEdit1.TabIndex = 4;
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(5, 1);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(446, 29);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "Welcome to the <b>Client Contract Wizard</b>";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
            this.xtraTabControl1.Size = new System.Drawing.Size(873, 541);
            this.xtraTabControl1.TabIndex = 6;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageWelcome,
            this.xtraTabPageStep1,
            this.xtraTabPageStep2,
            this.xtraTabPageStep3,
            this.xtraTabPageStep4,
            this.xtraTabPageStep5,
            this.xtraTabPageFinish});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            // 
            // xtraTabPageWelcome
            // 
            this.xtraTabPageWelcome.Controls.Add(this.panelControl1);
            this.xtraTabPageWelcome.Controls.Add(this.btnWelcomeNext);
            this.xtraTabPageWelcome.Controls.Add(this.labelControl2);
            this.xtraTabPageWelcome.Controls.Add(this.pictureEdit1);
            this.xtraTabPageWelcome.Name = "xtraTabPageWelcome";
            this.xtraTabPageWelcome.Size = new System.Drawing.Size(846, 536);
            this.xtraTabPageWelcome.Text = "Welcome";
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.pictureEdit4);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Location = new System.Drawing.Point(211, 6);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(629, 48);
            this.panelControl1.TabIndex = 17;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit4.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit4.Location = new System.Drawing.Point(585, 4);
            this.pictureEdit4.MenuManager = this.barManager1;
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Properties.ReadOnly = true;
            this.pictureEdit4.Properties.ShowMenu = false;
            this.pictureEdit4.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit4.TabIndex = 16;
            // 
            // labelControl9
            // 
            this.labelControl9.AllowHtmlString = true;
            this.labelControl9.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl9.Appearance.Options.UseBackColor = true;
            this.labelControl9.Location = new System.Drawing.Point(9, 31);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(272, 13);
            this.labelControl9.TabIndex = 15;
            this.labelControl9.Text = "This wizard will allow you to create a new Client Contract\r\n";
            // 
            // btnWelcomeNext
            // 
            this.btnWelcomeNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWelcomeNext.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnWelcomeNext.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnWelcomeNext.Location = new System.Drawing.Point(752, 500);
            this.btnWelcomeNext.Name = "btnWelcomeNext";
            this.btnWelcomeNext.Size = new System.Drawing.Size(88, 30);
            this.btnWelcomeNext.TabIndex = 7;
            this.btnWelcomeNext.Text = "Next";
            this.btnWelcomeNext.Click += new System.EventHandler(this.btnWelcomeNext_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.AllowHtmlString = true;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(220, 104);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(220, 25);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Click <b>Next</b> To Continue";
            // 
            // xtraTabPageStep1
            // 
            this.xtraTabPageStep1.Controls.Add(this.panelControl3);
            this.xtraTabPageStep1.Controls.Add(this.btnStep1Previous);
            this.xtraTabPageStep1.Controls.Add(this.bntStep1Next);
            this.xtraTabPageStep1.Controls.Add(this.gridControl1);
            this.xtraTabPageStep1.Name = "xtraTabPageStep1";
            this.xtraTabPageStep1.Size = new System.Drawing.Size(846, 536);
            this.xtraTabPageStep1.Text = "Step 1";
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.Controls.Add(this.labelControl3);
            this.panelControl3.Controls.Add(this.pictureEdit3);
            this.panelControl3.Controls.Add(this.labelControl5);
            this.panelControl3.Location = new System.Drawing.Point(7, 6);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(833, 48);
            this.panelControl3.TabIndex = 10;
            // 
            // labelControl3
            // 
            this.labelControl3.AllowHtmlString = true;
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseBackColor = true;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(5, 5);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(293, 16);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "<b>Step 1:</b> Select the Client to create the Contract for";
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit3.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit3.Location = new System.Drawing.Point(789, 4);
            this.pictureEdit3.MenuManager = this.barManager1;
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.ReadOnly = true;
            this.pictureEdit3.Properties.ShowMenu = false;
            this.pictureEdit3.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit3.TabIndex = 8;
            // 
            // labelControl5
            // 
            this.labelControl5.AllowHtmlString = true;
            this.labelControl5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl5.Appearance.Options.UseBackColor = true;
            this.labelControl5.Location = new System.Drawing.Point(57, 29);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(371, 13);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Select the client to add the contract to by clicking on it. Once done click Next." +
    "";
            // 
            // btnStep1Previous
            // 
            this.btnStep1Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep1Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep1Previous.Location = new System.Drawing.Point(658, 500);
            this.btnStep1Previous.Name = "btnStep1Previous";
            this.btnStep1Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep1Previous.TabIndex = 9;
            this.btnStep1Previous.Text = "Previous";
            this.btnStep1Previous.Click += new System.EventHandler(this.btnStep1Previous_Click);
            // 
            // bntStep1Next
            // 
            this.bntStep1Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bntStep1Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.bntStep1Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.bntStep1Next.Location = new System.Drawing.Point(752, 500);
            this.bntStep1Next.Name = "bntStep1Next";
            this.bntStep1Next.Size = new System.Drawing.Size(88, 30);
            this.bntStep1Next.TabIndex = 8;
            this.bntStep1Next.Text = "Next";
            this.bntStep1Next.Click += new System.EventHandler(this.bntStep1Next_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp06058OMClientContractWizardClientListBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Reload Data", "reload")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(7, 61);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(833, 433);
            this.gridControl1.TabIndex = 7;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp06058OMClientContractWizardClientListBindingSource
            // 
            this.sp06058OMClientContractWizardClientListBindingSource.DataMember = "sp06058_OM_Client_Contract_Wizard_Client_List";
            this.sp06058OMClientContractWizardClientListBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 6);
            this.imageCollection1.Images.SetKeyName(6, "refresh_16x16");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientID,
            this.colClientName,
            this.colClientCode,
            this.colClientTypeID,
            this.colRemarks,
            this.colClientType,
            this.colActiveClient});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActiveClient;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.FindDelay = 2000;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            this.colClientID.Width = 61;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 262;
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            this.colClientCode.Width = 147;
            // 
            // colClientTypeID
            // 
            this.colClientTypeID.Caption = "Client Type ID";
            this.colClientTypeID.FieldName = "ClientTypeID";
            this.colClientTypeID.Name = "colClientTypeID";
            this.colClientTypeID.OptionsColumn.AllowEdit = false;
            this.colClientTypeID.OptionsColumn.AllowFocus = false;
            this.colClientTypeID.OptionsColumn.ReadOnly = true;
            this.colClientTypeID.Width = 89;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 3;
            this.colRemarks.Width = 112;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colClientType
            // 
            this.colClientType.Caption = "Client Type";
            this.colClientType.FieldName = "ClientType";
            this.colClientType.Name = "colClientType";
            this.colClientType.OptionsColumn.AllowEdit = false;
            this.colClientType.OptionsColumn.AllowFocus = false;
            this.colClientType.OptionsColumn.ReadOnly = true;
            this.colClientType.Visible = true;
            this.colClientType.VisibleIndex = 1;
            this.colClientType.Width = 200;
            // 
            // xtraTabPageStep2
            // 
            this.xtraTabPageStep2.Controls.Add(this.dataLayoutControl1);
            this.xtraTabPageStep2.Controls.Add(this.panelControl4);
            this.xtraTabPageStep2.Controls.Add(this.btnStep2Next);
            this.xtraTabPageStep2.Controls.Add(this.btnStep2Previous);
            this.xtraTabPageStep2.Name = "xtraTabPageStep2";
            this.xtraTabPageStep2.Size = new System.Drawing.Size(846, 536);
            this.xtraTabPageStep2.Text = "Step 2";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataLayoutControl1.Controls.Add(this.ClientInstructionsMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ReactiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.BillingCentreCodeButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.CostCentreTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CostCentreCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CostCentreIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.BillingCentreCodeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FinanceClientCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToPreviousContractIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToPreviousContractButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.YearlyPercentageIncreaseSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SectorTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ActiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.LastClientPaymentDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.CheckRPIDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.EndDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.StartDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractDirectorIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractDirectorButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractStatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.GCCompanyIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientContractIDTextEdit);
            this.dataLayoutControl1.DataSource = this.sp06059OMClientContractEditBindingSource;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientContractID,
            this.ItemForClientID,
            this.ItemForLinkedToPreviousContractID,
            this.ItemForBillingCentreCodeID,
            this.ItemForContractDirectorID,
            this.ItemForCostCentre,
            this.ItemForCostCentreCode,
            this.ItemForCostCentreID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(7, 60);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1519, 216, 250, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(831, 429);
            this.dataLayoutControl1.TabIndex = 16;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ClientInstructionsMemoEdit
            // 
            this.ClientInstructionsMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ClientInstructions", true));
            this.ClientInstructionsMemoEdit.Location = new System.Drawing.Point(155, 508);
            this.ClientInstructionsMemoEdit.MenuManager = this.barManager1;
            this.ClientInstructionsMemoEdit.Name = "ClientInstructionsMemoEdit";
            this.ClientInstructionsMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientInstructionsMemoEdit, true);
            this.ClientInstructionsMemoEdit.Size = new System.Drawing.Size(662, 120);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientInstructionsMemoEdit, optionsSpelling1);
            this.ClientInstructionsMemoEdit.StyleController = this.dataLayoutControl1;
            this.ClientInstructionsMemoEdit.TabIndex = 44;
            // 
            // sp06059OMClientContractEditBindingSource
            // 
            this.sp06059OMClientContractEditBindingSource.DataMember = "sp06059_OM_Client_Contract_Edit";
            this.sp06059OMClientContractEditBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // ReactiveCheckEdit
            // 
            this.ReactiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "Reactive", true));
            this.ReactiveCheckEdit.Location = new System.Drawing.Point(155, 206);
            this.ReactiveCheckEdit.MenuManager = this.barManager1;
            this.ReactiveCheckEdit.Name = "ReactiveCheckEdit";
            this.ReactiveCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.ReactiveCheckEdit.Properties.ValueChecked = 1;
            this.ReactiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ReactiveCheckEdit.Size = new System.Drawing.Size(662, 19);
            this.ReactiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ReactiveCheckEdit.TabIndex = 8;
            // 
            // ContractDescriptionTextEdit
            // 
            this.ContractDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ContractDescription", true));
            this.ContractDescriptionTextEdit.Location = new System.Drawing.Point(155, 62);
            this.ContractDescriptionTextEdit.MenuManager = this.barManager1;
            this.ContractDescriptionTextEdit.Name = "ContractDescriptionTextEdit";
            this.ContractDescriptionTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContractDescriptionTextEdit, true);
            this.ContractDescriptionTextEdit.Size = new System.Drawing.Size(662, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContractDescriptionTextEdit, optionsSpelling2);
            this.ContractDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.ContractDescriptionTextEdit.TabIndex = 7;
            this.ContractDescriptionTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ContractDescriptionTextEdit_Validating);
            // 
            // ClientNameTextEdit
            // 
            this.ClientNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ClientName", true));
            this.ClientNameTextEdit.Location = new System.Drawing.Point(155, 38);
            this.ClientNameTextEdit.MenuManager = this.barManager1;
            this.ClientNameTextEdit.Name = "ClientNameTextEdit";
            this.ClientNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameTextEdit, true);
            this.ClientNameTextEdit.Size = new System.Drawing.Size(662, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameTextEdit, optionsSpelling3);
            this.ClientNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameTextEdit.TabIndex = 0;
            // 
            // BillingCentreCodeButtonEdit
            // 
            this.BillingCentreCodeButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "BillingCentreCode", true));
            this.BillingCentreCodeButtonEdit.Location = new System.Drawing.Point(155, 426);
            this.BillingCentreCodeButtonEdit.MenuManager = this.barManager1;
            this.BillingCentreCodeButtonEdit.Name = "BillingCentreCodeButtonEdit";
            this.BillingCentreCodeButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to open Select Billing Code screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.BillingCentreCodeButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.BillingCentreCodeButtonEdit.Size = new System.Drawing.Size(662, 20);
            this.BillingCentreCodeButtonEdit.StyleController = this.dataLayoutControl1;
            this.BillingCentreCodeButtonEdit.TabIndex = 13;
            this.BillingCentreCodeButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BillingCentreCodeButtonEdit_ButtonClick);
            // 
            // CostCentreTextEdit
            // 
            this.CostCentreTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "CostCentre", true));
            this.CostCentreTextEdit.Location = new System.Drawing.Point(155, 38);
            this.CostCentreTextEdit.MenuManager = this.barManager1;
            this.CostCentreTextEdit.Name = "CostCentreTextEdit";
            this.CostCentreTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CostCentreTextEdit, true);
            this.CostCentreTextEdit.Size = new System.Drawing.Size(645, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CostCentreTextEdit, optionsSpelling4);
            this.CostCentreTextEdit.StyleController = this.dataLayoutControl1;
            this.CostCentreTextEdit.TabIndex = 43;
            // 
            // CostCentreCodeTextEdit
            // 
            this.CostCentreCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "CostCentreCode", true));
            this.CostCentreCodeTextEdit.Location = new System.Drawing.Point(155, 38);
            this.CostCentreCodeTextEdit.MenuManager = this.barManager1;
            this.CostCentreCodeTextEdit.Name = "CostCentreCodeTextEdit";
            this.CostCentreCodeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CostCentreCodeTextEdit, true);
            this.CostCentreCodeTextEdit.Size = new System.Drawing.Size(645, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CostCentreCodeTextEdit, optionsSpelling5);
            this.CostCentreCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.CostCentreCodeTextEdit.TabIndex = 42;
            // 
            // CostCentreIDTextEdit
            // 
            this.CostCentreIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "CostCentreID", true));
            this.CostCentreIDTextEdit.Location = new System.Drawing.Point(155, 38);
            this.CostCentreIDTextEdit.MenuManager = this.barManager1;
            this.CostCentreIDTextEdit.Name = "CostCentreIDTextEdit";
            this.CostCentreIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CostCentreIDTextEdit, true);
            this.CostCentreIDTextEdit.Size = new System.Drawing.Size(645, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CostCentreIDTextEdit, optionsSpelling6);
            this.CostCentreIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CostCentreIDTextEdit.TabIndex = 41;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(14, 38);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.RemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(803, 600);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling7);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 0;
            // 
            // BillingCentreCodeIDTextEdit
            // 
            this.BillingCentreCodeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "BillingCentreCodeID", true));
            this.BillingCentreCodeIDTextEdit.Location = new System.Drawing.Point(153, 347);
            this.BillingCentreCodeIDTextEdit.MenuManager = this.barManager1;
            this.BillingCentreCodeIDTextEdit.Name = "BillingCentreCodeIDTextEdit";
            this.BillingCentreCodeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.BillingCentreCodeIDTextEdit, true);
            this.BillingCentreCodeIDTextEdit.Size = new System.Drawing.Size(666, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.BillingCentreCodeIDTextEdit, optionsSpelling8);
            this.BillingCentreCodeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.BillingCentreCodeIDTextEdit.TabIndex = 39;
            // 
            // FinanceClientCodeTextEdit
            // 
            this.FinanceClientCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "FinanceClientCode", true));
            this.FinanceClientCodeTextEdit.Location = new System.Drawing.Point(155, 402);
            this.FinanceClientCodeTextEdit.MenuManager = this.barManager1;
            this.FinanceClientCodeTextEdit.Name = "FinanceClientCodeTextEdit";
            this.FinanceClientCodeTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FinanceClientCodeTextEdit, true);
            this.FinanceClientCodeTextEdit.Size = new System.Drawing.Size(662, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FinanceClientCodeTextEdit, optionsSpelling9);
            this.FinanceClientCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.FinanceClientCodeTextEdit.TabIndex = 12;
            // 
            // LinkedToPreviousContractIDTextEdit
            // 
            this.LinkedToPreviousContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "LinkedToPreviousContractID", true));
            this.LinkedToPreviousContractIDTextEdit.Location = new System.Drawing.Point(167, 323);
            this.LinkedToPreviousContractIDTextEdit.MenuManager = this.barManager1;
            this.LinkedToPreviousContractIDTextEdit.Name = "LinkedToPreviousContractIDTextEdit";
            this.LinkedToPreviousContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToPreviousContractIDTextEdit, true);
            this.LinkedToPreviousContractIDTextEdit.Size = new System.Drawing.Size(652, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToPreviousContractIDTextEdit, optionsSpelling10);
            this.LinkedToPreviousContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToPreviousContractIDTextEdit.TabIndex = 37;
            // 
            // LinkedToPreviousContractButtonEdit
            // 
            this.LinkedToPreviousContractButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "LinkedToPreviousContract", true));
            this.LinkedToPreviousContractButtonEdit.Location = new System.Drawing.Point(155, 474);
            this.LinkedToPreviousContractButtonEdit.MenuManager = this.barManager1;
            this.LinkedToPreviousContractButtonEdit.Name = "LinkedToPreviousContractButtonEdit";
            this.LinkedToPreviousContractButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click to Open Select Contract screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LinkedToPreviousContractButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.LinkedToPreviousContractButtonEdit.Size = new System.Drawing.Size(662, 20);
            this.LinkedToPreviousContractButtonEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToPreviousContractButtonEdit.TabIndex = 15;
            this.LinkedToPreviousContractButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LinkedToPreviousContractButtonEdit_ButtonClick);
            // 
            // YearlyPercentageIncreaseSpinEdit
            // 
            this.YearlyPercentageIncreaseSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "YearlyPercentageIncrease", true));
            this.YearlyPercentageIncreaseSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.YearlyPercentageIncreaseSpinEdit.Location = new System.Drawing.Point(155, 344);
            this.YearlyPercentageIncreaseSpinEdit.MenuManager = this.barManager1;
            this.YearlyPercentageIncreaseSpinEdit.Name = "YearlyPercentageIncreaseSpinEdit";
            this.YearlyPercentageIncreaseSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.YearlyPercentageIncreaseSpinEdit.Properties.Mask.EditMask = "f2";
            this.YearlyPercentageIncreaseSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.YearlyPercentageIncreaseSpinEdit.Size = new System.Drawing.Size(662, 20);
            this.YearlyPercentageIncreaseSpinEdit.StyleController = this.dataLayoutControl1;
            this.YearlyPercentageIncreaseSpinEdit.TabIndex = 10;
            // 
            // ContractValueSpinEdit
            // 
            this.ContractValueSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ContractValue", true));
            this.ContractValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ContractValueSpinEdit.Location = new System.Drawing.Point(155, 320);
            this.ContractValueSpinEdit.MenuManager = this.barManager1;
            this.ContractValueSpinEdit.Name = "ContractValueSpinEdit";
            this.ContractValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ContractValueSpinEdit.Properties.Mask.EditMask = "c";
            this.ContractValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ContractValueSpinEdit.Size = new System.Drawing.Size(662, 20);
            this.ContractValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.ContractValueSpinEdit.TabIndex = 9;
            // 
            // SectorTypeIDGridLookUpEdit
            // 
            this.SectorTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "SectorTypeID", true));
            this.SectorTypeIDGridLookUpEdit.Location = new System.Drawing.Point(155, 110);
            this.SectorTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SectorTypeIDGridLookUpEdit.Name = "SectorTypeIDGridLookUpEdit";
            this.SectorTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SectorTypeIDGridLookUpEdit.Properties.DataSource = this.sp06061OMSectorsWithBlankBindingSource;
            this.SectorTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.SectorTypeIDGridLookUpEdit.Properties.NullText = "";
            this.SectorTypeIDGridLookUpEdit.Properties.PopupView = this.gridView4;
            this.SectorTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.SectorTypeIDGridLookUpEdit.Size = new System.Drawing.Size(662, 20);
            this.SectorTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SectorTypeIDGridLookUpEdit.TabIndex = 2;
            this.SectorTypeIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SectorTypeIDGridLookUpEdit_Validating);
            // 
            // sp06061OMSectorsWithBlankBindingSource
            // 
            this.sp06061OMSectorsWithBlankBindingSource.DataMember = "sp06061_OM_Sectors_With_Blank";
            this.sp06061OMSectorsWithBlankBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn1;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Sector";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // ActiveCheckEdit
            // 
            this.ActiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "Active", true));
            this.ActiveCheckEdit.Location = new System.Drawing.Point(155, 287);
            this.ActiveCheckEdit.MenuManager = this.barManager1;
            this.ActiveCheckEdit.Name = "ActiveCheckEdit";
            this.ActiveCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.ActiveCheckEdit.Properties.ValueChecked = 1;
            this.ActiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ActiveCheckEdit.Size = new System.Drawing.Size(662, 19);
            this.ActiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ActiveCheckEdit.TabIndex = 8;
            // 
            // LastClientPaymentDateDateEdit
            // 
            this.LastClientPaymentDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "LastClientPaymentDate", true));
            this.LastClientPaymentDateDateEdit.EditValue = null;
            this.LastClientPaymentDateDateEdit.Location = new System.Drawing.Point(155, 450);
            this.LastClientPaymentDateDateEdit.MenuManager = this.barManager1;
            this.LastClientPaymentDateDateEdit.Name = "LastClientPaymentDateDateEdit";
            this.LastClientPaymentDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LastClientPaymentDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LastClientPaymentDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LastClientPaymentDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.LastClientPaymentDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.LastClientPaymentDateDateEdit.Size = new System.Drawing.Size(173, 20);
            this.LastClientPaymentDateDateEdit.StyleController = this.dataLayoutControl1;
            this.LastClientPaymentDateDateEdit.TabIndex = 14;
            // 
            // CheckRPIDateDateEdit
            // 
            this.CheckRPIDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "CheckRPIDate", true));
            this.CheckRPIDateDateEdit.EditValue = null;
            this.CheckRPIDateDateEdit.Location = new System.Drawing.Point(155, 368);
            this.CheckRPIDateDateEdit.MenuManager = this.barManager1;
            this.CheckRPIDateDateEdit.Name = "CheckRPIDateDateEdit";
            this.CheckRPIDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.CheckRPIDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CheckRPIDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CheckRPIDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.CheckRPIDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.CheckRPIDateDateEdit.Size = new System.Drawing.Size(173, 20);
            this.CheckRPIDateDateEdit.StyleController = this.dataLayoutControl1;
            this.CheckRPIDateDateEdit.TabIndex = 11;
            // 
            // EndDateDateEdit
            // 
            this.EndDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "EndDate", true));
            this.EndDateDateEdit.EditValue = null;
            this.EndDateDateEdit.Location = new System.Drawing.Point(155, 263);
            this.EndDateDateEdit.MenuManager = this.barManager1;
            this.EndDateDateEdit.Name = "EndDateDateEdit";
            this.EndDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.EndDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.EndDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.EndDateDateEdit.Size = new System.Drawing.Size(173, 20);
            this.EndDateDateEdit.StyleController = this.dataLayoutControl1;
            this.EndDateDateEdit.TabIndex = 7;
            // 
            // StartDateDateEdit
            // 
            this.StartDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "StartDate", true));
            this.StartDateDateEdit.EditValue = null;
            this.StartDateDateEdit.Location = new System.Drawing.Point(155, 239);
            this.StartDateDateEdit.MenuManager = this.barManager1;
            this.StartDateDateEdit.Name = "StartDateDateEdit";
            this.StartDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.StartDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.StartDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.StartDateDateEdit.Size = new System.Drawing.Size(173, 20);
            this.StartDateDateEdit.StyleController = this.dataLayoutControl1;
            this.StartDateDateEdit.TabIndex = 6;
            this.StartDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.StartDateDateEdit_Validating);
            // 
            // ContractDirectorIDTextEdit
            // 
            this.ContractDirectorIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ContractDirectorID", true));
            this.ContractDirectorIDTextEdit.Location = new System.Drawing.Point(155, 158);
            this.ContractDirectorIDTextEdit.MenuManager = this.barManager1;
            this.ContractDirectorIDTextEdit.Name = "ContractDirectorIDTextEdit";
            this.ContractDirectorIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContractDirectorIDTextEdit, true);
            this.ContractDirectorIDTextEdit.Size = new System.Drawing.Size(645, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContractDirectorIDTextEdit, optionsSpelling11);
            this.ContractDirectorIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ContractDirectorIDTextEdit.TabIndex = 10;
            // 
            // ContractDirectorButtonEdit
            // 
            this.ContractDirectorButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ContractDirector", true));
            this.ContractDirectorButtonEdit.Location = new System.Drawing.Point(155, 158);
            this.ContractDirectorButtonEdit.MenuManager = this.barManager1;
            this.ContractDirectorButtonEdit.Name = "ContractDirectorButtonEdit";
            this.ContractDirectorButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to open Select Staff screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ContractDirectorButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ContractDirectorButtonEdit.Size = new System.Drawing.Size(662, 20);
            this.ContractDirectorButtonEdit.StyleController = this.dataLayoutControl1;
            this.ContractDirectorButtonEdit.TabIndex = 4;
            this.ContractDirectorButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ContractDirectorButtonEdit_ButtonClick);
            this.ContractDirectorButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ContractDirectorButtonEdit_Validating);
            // 
            // ContractStatusIDGridLookUpEdit
            // 
            this.ContractStatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ContractStatusID", true));
            this.ContractStatusIDGridLookUpEdit.Location = new System.Drawing.Point(155, 134);
            this.ContractStatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ContractStatusIDGridLookUpEdit.Name = "ContractStatusIDGridLookUpEdit";
            this.ContractStatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ContractStatusIDGridLookUpEdit.Properties.DataSource = this.sp06062OMContractStatusesWithBlankBindingSource;
            this.ContractStatusIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ContractStatusIDGridLookUpEdit.Properties.NullText = "";
            this.ContractStatusIDGridLookUpEdit.Properties.PopupView = this.gridView3;
            this.ContractStatusIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ContractStatusIDGridLookUpEdit.Size = new System.Drawing.Size(662, 20);
            this.ContractStatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ContractStatusIDGridLookUpEdit.TabIndex = 3;
            this.ContractStatusIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ContractStatusIDGridLookUpEdit_Validating);
            // 
            // sp06062OMContractStatusesWithBlankBindingSource
            // 
            this.sp06062OMContractStatusesWithBlankBindingSource.DataMember = "sp06062_OM_Contract_Statuses_With_Blank";
            this.sp06062OMContractStatusesWithBlankBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn4;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Contract Status";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "RecordOrder";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            // 
            // ContractTypeIDGridLookUpEdit
            // 
            this.ContractTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ContractTypeID", true));
            this.ContractTypeIDGridLookUpEdit.Location = new System.Drawing.Point(155, 182);
            this.ContractTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ContractTypeIDGridLookUpEdit.Name = "ContractTypeIDGridLookUpEdit";
            this.ContractTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ContractTypeIDGridLookUpEdit.Properties.DataSource = this.sp06063OMContractTypesWithBlankBindingSource;
            this.ContractTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ContractTypeIDGridLookUpEdit.Properties.NullText = "";
            this.ContractTypeIDGridLookUpEdit.Properties.PopupView = this.gridView2;
            this.ContractTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ContractTypeIDGridLookUpEdit.Size = new System.Drawing.Size(662, 20);
            this.ContractTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ContractTypeIDGridLookUpEdit.TabIndex = 5;
            this.ContractTypeIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ContractTypeIDGridLookUpEdit_Validating);
            // 
            // sp06063OMContractTypesWithBlankBindingSource
            // 
            this.sp06063OMContractTypesWithBlankBindingSource.DataMember = "sp06063_OM_Contract_Types_With_Blank";
            this.sp06063OMContractTypesWithBlankBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.gridColumn7;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn9, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Contract Type";
            this.gridColumn8.FieldName = "Description";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 220;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Order";
            this.gridColumn9.FieldName = "RecordOrder";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            // 
            // GCCompanyIDGridLookUpEdit
            // 
            this.GCCompanyIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "GCCompanyID", true));
            this.GCCompanyIDGridLookUpEdit.Location = new System.Drawing.Point(155, 86);
            this.GCCompanyIDGridLookUpEdit.MenuManager = this.barManager1;
            this.GCCompanyIDGridLookUpEdit.Name = "GCCompanyIDGridLookUpEdit";
            this.GCCompanyIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GCCompanyIDGridLookUpEdit.Properties.DataSource = this.sp06060OMGCCompaniesWithBlankBindingSource;
            this.GCCompanyIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.GCCompanyIDGridLookUpEdit.Properties.NullText = "";
            this.GCCompanyIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.GCCompanyIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.GCCompanyIDGridLookUpEdit.Size = new System.Drawing.Size(662, 20);
            this.GCCompanyIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.GCCompanyIDGridLookUpEdit.TabIndex = 1;
            this.GCCompanyIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.GCCompanyIDGridLookUpEdit_Validating);
            // 
            // sp06060OMGCCompaniesWithBlankBindingSource
            // 
            this.sp06060OMGCCompaniesWithBlankBindingSource.DataMember = "sp06060_OM_GC_Companies_With_Blank";
            this.sp06060OMGCCompaniesWithBlankBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.colID1;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Company";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // ClientIDTextEdit
            // 
            this.ClientIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ClientID", true));
            this.ClientIDTextEdit.Location = new System.Drawing.Point(95, 12);
            this.ClientIDTextEdit.MenuManager = this.barManager1;
            this.ClientIDTextEdit.Name = "ClientIDTextEdit";
            this.ClientIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientIDTextEdit, true);
            this.ClientIDTextEdit.Size = new System.Drawing.Size(724, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientIDTextEdit, optionsSpelling12);
            this.ClientIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDTextEdit.TabIndex = 5;
            // 
            // ClientContractIDTextEdit
            // 
            this.ClientContractIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ClientContractID", true));
            this.ClientContractIDTextEdit.Location = new System.Drawing.Point(105, 12);
            this.ClientContractIDTextEdit.MenuManager = this.barManager1;
            this.ClientContractIDTextEdit.Name = "ClientContractIDTextEdit";
            this.ClientContractIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientContractIDTextEdit, true);
            this.ClientContractIDTextEdit.Size = new System.Drawing.Size(714, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientContractIDTextEdit, optionsSpelling13);
            this.ClientContractIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientContractIDTextEdit.TabIndex = 4;
            // 
            // ItemForClientContractID
            // 
            this.ItemForClientContractID.Control = this.ClientContractIDTextEdit;
            this.ItemForClientContractID.CustomizationFormText = "Client Contract ID:";
            this.ItemForClientContractID.Location = new System.Drawing.Point(0, 0);
            this.ItemForClientContractID.Name = "ItemForClientContractID";
            this.ItemForClientContractID.Size = new System.Drawing.Size(811, 409);
            this.ItemForClientContractID.Text = "Client Contract ID:";
            this.ItemForClientContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientIDTextEdit;
            this.ItemForClientID.CustomizationFormText = "Client ID:";
            this.ItemForClientID.Location = new System.Drawing.Point(0, 0);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(811, 24);
            this.ItemForClientID.Text = "Client ID:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLinkedToPreviousContractID
            // 
            this.ItemForLinkedToPreviousContractID.Control = this.LinkedToPreviousContractIDTextEdit;
            this.ItemForLinkedToPreviousContractID.CustomizationFormText = "Linked To Previous Contract ID:";
            this.ItemForLinkedToPreviousContractID.Location = new System.Drawing.Point(0, 311);
            this.ItemForLinkedToPreviousContractID.Name = "ItemForLinkedToPreviousContractID";
            this.ItemForLinkedToPreviousContractID.Size = new System.Drawing.Size(811, 98);
            this.ItemForLinkedToPreviousContractID.Text = "Linked To Previous Contract ID:";
            this.ItemForLinkedToPreviousContractID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForBillingCentreCodeID
            // 
            this.ItemForBillingCentreCodeID.Control = this.BillingCentreCodeIDTextEdit;
            this.ItemForBillingCentreCodeID.CustomizationFormText = "Billing Centre Code ID:";
            this.ItemForBillingCentreCodeID.Location = new System.Drawing.Point(0, 335);
            this.ItemForBillingCentreCodeID.Name = "ItemForBillingCentreCodeID";
            this.ItemForBillingCentreCodeID.Size = new System.Drawing.Size(811, 74);
            this.ItemForBillingCentreCodeID.Text = "Billing Centre Code ID:";
            this.ItemForBillingCentreCodeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForContractDirectorID
            // 
            this.ItemForContractDirectorID.Control = this.ContractDirectorIDTextEdit;
            this.ItemForContractDirectorID.CustomizationFormText = "Contract Director ID:";
            this.ItemForContractDirectorID.Location = new System.Drawing.Point(0, 120);
            this.ItemForContractDirectorID.Name = "ItemForContractDirectorID";
            this.ItemForContractDirectorID.Size = new System.Drawing.Size(790, 24);
            this.ItemForContractDirectorID.Text = "Contract Director ID:";
            this.ItemForContractDirectorID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCostCentre
            // 
            this.ItemForCostCentre.Control = this.CostCentreTextEdit;
            this.ItemForCostCentre.CustomizationFormText = "Cost Centre:";
            this.ItemForCostCentre.Location = new System.Drawing.Point(0, 0);
            this.ItemForCostCentre.Name = "ItemForCostCentre";
            this.ItemForCostCentre.Size = new System.Drawing.Size(790, 24);
            this.ItemForCostCentre.Text = "Cost Centre:";
            this.ItemForCostCentre.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCostCentreCode
            // 
            this.ItemForCostCentreCode.Control = this.CostCentreCodeTextEdit;
            this.ItemForCostCentreCode.CustomizationFormText = "Cost Centre Code:";
            this.ItemForCostCentreCode.Location = new System.Drawing.Point(0, 0);
            this.ItemForCostCentreCode.Name = "ItemForCostCentreCode";
            this.ItemForCostCentreCode.Size = new System.Drawing.Size(790, 24);
            this.ItemForCostCentreCode.Text = "Cost Centre Code:";
            this.ItemForCostCentreCode.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCostCentreID
            // 
            this.ItemForCostCentreID.Control = this.CostCentreIDTextEdit;
            this.ItemForCostCentreID.CustomizationFormText = "Cost Centre ID:";
            this.ItemForCostCentreID.Location = new System.Drawing.Point(0, 0);
            this.ItemForCostCentreID.Name = "ItemForCostCentreID";
            this.ItemForCostCentreID.Size = new System.Drawing.Size(790, 24);
            this.ItemForCostCentreID.Text = "Cost Centre ID:";
            this.ItemForCostCentreID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(831, 652);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(831, 652);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Details";
            this.layoutControlGroup2.ExpandButtonVisible = true;
            this.layoutControlGroup2.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForGCCompanyID,
            this.ItemForContractTypeID,
            this.ItemForStartDate,
            this.ItemForContractValue,
            this.ItemForCheckRPIDate,
            this.ItemForFinanceClientCode,
            this.ItemForLinkedToPreviousContract,
            this.ItemForContractDirector,
            this.ItemForContractStatusID,
            this.ItemForEndDate,
            this.ItemForActive,
            this.ItemForSectorTypeID,
            this.ItemForYearlyPercentageIncrease,
            this.ItemForLastClientPaymentDate,
            this.emptySpaceItem2,
            this.emptySpaceItem1,
            this.emptySpaceItem3,
            this.ItemForBillingCentreCode,
            this.emptySpaceItem4,
            this.ItemForClientName,
            this.emptySpaceItem9,
            this.emptySpaceItem10,
            this.emptySpaceItem11,
            this.emptySpaceItem12,
            this.ItemForContractDescription,
            this.ItemForReactive,
            this.ItemForClientInstructions,
            this.emptySpaceItem13});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(807, 604);
            this.layoutControlGroup2.Text = "Details";
            // 
            // ItemForGCCompanyID
            // 
            this.ItemForGCCompanyID.Control = this.GCCompanyIDGridLookUpEdit;
            this.ItemForGCCompanyID.CustomizationFormText = "GC Company:";
            this.ItemForGCCompanyID.Location = new System.Drawing.Point(0, 48);
            this.ItemForGCCompanyID.Name = "ItemForGCCompanyID";
            this.ItemForGCCompanyID.Size = new System.Drawing.Size(807, 24);
            this.ItemForGCCompanyID.Text = "GC Company:";
            this.ItemForGCCompanyID.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForContractTypeID
            // 
            this.ItemForContractTypeID.Control = this.ContractTypeIDGridLookUpEdit;
            this.ItemForContractTypeID.CustomizationFormText = "Contract Type:";
            this.ItemForContractTypeID.Location = new System.Drawing.Point(0, 144);
            this.ItemForContractTypeID.Name = "ItemForContractTypeID";
            this.ItemForContractTypeID.Size = new System.Drawing.Size(807, 24);
            this.ItemForContractTypeID.Text = "Contract Type:";
            this.ItemForContractTypeID.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForStartDate
            // 
            this.ItemForStartDate.Control = this.StartDateDateEdit;
            this.ItemForStartDate.CustomizationFormText = "Start Date:";
            this.ItemForStartDate.Location = new System.Drawing.Point(0, 201);
            this.ItemForStartDate.MaxSize = new System.Drawing.Size(318, 24);
            this.ItemForStartDate.MinSize = new System.Drawing.Size(318, 24);
            this.ItemForStartDate.Name = "ItemForStartDate";
            this.ItemForStartDate.Size = new System.Drawing.Size(318, 24);
            this.ItemForStartDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForStartDate.Text = "Start Date:";
            this.ItemForStartDate.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForContractValue
            // 
            this.ItemForContractValue.Control = this.ContractValueSpinEdit;
            this.ItemForContractValue.CustomizationFormText = "Contract Value:";
            this.ItemForContractValue.Location = new System.Drawing.Point(0, 282);
            this.ItemForContractValue.Name = "ItemForContractValue";
            this.ItemForContractValue.Size = new System.Drawing.Size(807, 24);
            this.ItemForContractValue.Text = "Contract Value:";
            this.ItemForContractValue.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForCheckRPIDate
            // 
            this.ItemForCheckRPIDate.Control = this.CheckRPIDateDateEdit;
            this.ItemForCheckRPIDate.CustomizationFormText = "Check RPI Date:";
            this.ItemForCheckRPIDate.Location = new System.Drawing.Point(0, 330);
            this.ItemForCheckRPIDate.MaxSize = new System.Drawing.Size(318, 24);
            this.ItemForCheckRPIDate.MinSize = new System.Drawing.Size(318, 24);
            this.ItemForCheckRPIDate.Name = "ItemForCheckRPIDate";
            this.ItemForCheckRPIDate.Size = new System.Drawing.Size(318, 24);
            this.ItemForCheckRPIDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCheckRPIDate.Text = "Check RPI Date:";
            this.ItemForCheckRPIDate.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForFinanceClientCode
            // 
            this.ItemForFinanceClientCode.Control = this.FinanceClientCodeTextEdit;
            this.ItemForFinanceClientCode.CustomizationFormText = "Finance Client Code:";
            this.ItemForFinanceClientCode.Location = new System.Drawing.Point(0, 364);
            this.ItemForFinanceClientCode.Name = "ItemForFinanceClientCode";
            this.ItemForFinanceClientCode.Size = new System.Drawing.Size(807, 24);
            this.ItemForFinanceClientCode.Text = "Finance Client Code:";
            this.ItemForFinanceClientCode.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForLinkedToPreviousContract
            // 
            this.ItemForLinkedToPreviousContract.Control = this.LinkedToPreviousContractButtonEdit;
            this.ItemForLinkedToPreviousContract.CustomizationFormText = "Linked To Previous Contract:";
            this.ItemForLinkedToPreviousContract.Location = new System.Drawing.Point(0, 436);
            this.ItemForLinkedToPreviousContract.Name = "ItemForLinkedToPreviousContract";
            this.ItemForLinkedToPreviousContract.Size = new System.Drawing.Size(807, 24);
            this.ItemForLinkedToPreviousContract.Text = "Linked To Previous Contract:";
            this.ItemForLinkedToPreviousContract.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForContractDirector
            // 
            this.ItemForContractDirector.Control = this.ContractDirectorButtonEdit;
            this.ItemForContractDirector.CustomizationFormText = "Contract Director:";
            this.ItemForContractDirector.Location = new System.Drawing.Point(0, 120);
            this.ItemForContractDirector.Name = "ItemForContractDirector";
            this.ItemForContractDirector.Size = new System.Drawing.Size(807, 24);
            this.ItemForContractDirector.Text = "Contract Director:";
            this.ItemForContractDirector.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForContractStatusID
            // 
            this.ItemForContractStatusID.Control = this.ContractStatusIDGridLookUpEdit;
            this.ItemForContractStatusID.CustomizationFormText = "Contract Status:";
            this.ItemForContractStatusID.Location = new System.Drawing.Point(0, 96);
            this.ItemForContractStatusID.Name = "ItemForContractStatusID";
            this.ItemForContractStatusID.Size = new System.Drawing.Size(807, 24);
            this.ItemForContractStatusID.Text = "Contract Status:";
            this.ItemForContractStatusID.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForEndDate
            // 
            this.ItemForEndDate.Control = this.EndDateDateEdit;
            this.ItemForEndDate.CustomizationFormText = "End Date:";
            this.ItemForEndDate.Location = new System.Drawing.Point(0, 225);
            this.ItemForEndDate.MaxSize = new System.Drawing.Size(318, 24);
            this.ItemForEndDate.MinSize = new System.Drawing.Size(318, 24);
            this.ItemForEndDate.Name = "ItemForEndDate";
            this.ItemForEndDate.Size = new System.Drawing.Size(318, 24);
            this.ItemForEndDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForEndDate.Text = "End Date:";
            this.ItemForEndDate.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForActive
            // 
            this.ItemForActive.Control = this.ActiveCheckEdit;
            this.ItemForActive.CustomizationFormText = "Active:";
            this.ItemForActive.Location = new System.Drawing.Point(0, 249);
            this.ItemForActive.Name = "ItemForActive";
            this.ItemForActive.Size = new System.Drawing.Size(807, 23);
            this.ItemForActive.Text = "Active:";
            this.ItemForActive.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForSectorTypeID
            // 
            this.ItemForSectorTypeID.Control = this.SectorTypeIDGridLookUpEdit;
            this.ItemForSectorTypeID.CustomizationFormText = "Sector Type:";
            this.ItemForSectorTypeID.Location = new System.Drawing.Point(0, 72);
            this.ItemForSectorTypeID.Name = "ItemForSectorTypeID";
            this.ItemForSectorTypeID.Size = new System.Drawing.Size(807, 24);
            this.ItemForSectorTypeID.Text = "Sector Type:";
            this.ItemForSectorTypeID.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForYearlyPercentageIncrease
            // 
            this.ItemForYearlyPercentageIncrease.Control = this.YearlyPercentageIncreaseSpinEdit;
            this.ItemForYearlyPercentageIncrease.CustomizationFormText = "Yearly % Increase:";
            this.ItemForYearlyPercentageIncrease.Location = new System.Drawing.Point(0, 306);
            this.ItemForYearlyPercentageIncrease.Name = "ItemForYearlyPercentageIncrease";
            this.ItemForYearlyPercentageIncrease.Size = new System.Drawing.Size(807, 24);
            this.ItemForYearlyPercentageIncrease.Text = "Yearly % Increase:";
            this.ItemForYearlyPercentageIncrease.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForLastClientPaymentDate
            // 
            this.ItemForLastClientPaymentDate.Control = this.LastClientPaymentDateDateEdit;
            this.ItemForLastClientPaymentDate.CustomizationFormText = "Last Client Payment Date:";
            this.ItemForLastClientPaymentDate.Location = new System.Drawing.Point(0, 412);
            this.ItemForLastClientPaymentDate.MaxSize = new System.Drawing.Size(318, 24);
            this.ItemForLastClientPaymentDate.MinSize = new System.Drawing.Size(318, 24);
            this.ItemForLastClientPaymentDate.Name = "ItemForLastClientPaymentDate";
            this.ItemForLastClientPaymentDate.Size = new System.Drawing.Size(318, 24);
            this.ItemForLastClientPaymentDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLastClientPaymentDate.Text = "Last Client Payment Date:";
            this.ItemForLastClientPaymentDate.TextSize = new System.Drawing.Size(138, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 594);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(807, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 191);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(807, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 272);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(807, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForBillingCentreCode
            // 
            this.ItemForBillingCentreCode.Control = this.BillingCentreCodeButtonEdit;
            this.ItemForBillingCentreCode.CustomizationFormText = "Billing Centre Code:";
            this.ItemForBillingCentreCode.Location = new System.Drawing.Point(0, 388);
            this.ItemForBillingCentreCode.Name = "ItemForBillingCentreCode";
            this.ItemForBillingCentreCode.Size = new System.Drawing.Size(807, 24);
            this.ItemForBillingCentreCode.Text = "Billing Centre Code:";
            this.ItemForBillingCentreCode.TextSize = new System.Drawing.Size(138, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 354);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(807, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.Control = this.ClientNameTextEdit;
            this.ItemForClientName.CustomizationFormText = "Client Name:";
            this.ItemForClientName.Location = new System.Drawing.Point(0, 0);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(807, 24);
            this.ItemForClientName.Text = "Client Name:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(138, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(318, 201);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(489, 24);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(318, 225);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(489, 24);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(318, 330);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(489, 24);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(318, 412);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(489, 24);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForContractDescription
            // 
            this.ItemForContractDescription.Control = this.ContractDescriptionTextEdit;
            this.ItemForContractDescription.CustomizationFormText = "Contract Description:";
            this.ItemForContractDescription.Location = new System.Drawing.Point(0, 24);
            this.ItemForContractDescription.Name = "ItemForContractDescription";
            this.ItemForContractDescription.Size = new System.Drawing.Size(807, 24);
            this.ItemForContractDescription.Text = "Contract Description:";
            this.ItemForContractDescription.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForReactive
            // 
            this.ItemForReactive.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.ItemForReactive.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForReactive.Control = this.ReactiveCheckEdit;
            this.ItemForReactive.CustomizationFormText = "Reactive:";
            this.ItemForReactive.Location = new System.Drawing.Point(0, 168);
            this.ItemForReactive.Name = "ItemForReactive";
            this.ItemForReactive.Size = new System.Drawing.Size(807, 23);
            this.ItemForReactive.Text = "Reactive:";
            this.ItemForReactive.TextSize = new System.Drawing.Size(138, 13);
            // 
            // ItemForClientInstructions
            // 
            this.ItemForClientInstructions.Control = this.ClientInstructionsMemoEdit;
            this.ItemForClientInstructions.CustomizationFormText = "Client Instructions:";
            this.ItemForClientInstructions.Location = new System.Drawing.Point(0, 470);
            this.ItemForClientInstructions.MaxSize = new System.Drawing.Size(0, 124);
            this.ItemForClientInstructions.MinSize = new System.Drawing.Size(155, 124);
            this.ItemForClientInstructions.Name = "ItemForClientInstructions";
            this.ItemForClientInstructions.Size = new System.Drawing.Size(807, 124);
            this.ItemForClientInstructions.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForClientInstructions.Text = "Client Instructions:";
            this.ItemForClientInstructions.TextSize = new System.Drawing.Size(138, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(0, 460);
            this.emptySpaceItem13.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem13.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(807, 10);
            this.emptySpaceItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup3.CaptionImageOptions.Image")));
            this.layoutControlGroup3.CustomizationFormText = "Remarks";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(807, 604);
            this.layoutControlGroup3.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(807, 604);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // panelControl4
            // 
            this.panelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl4.Controls.Add(this.pictureEdit5);
            this.panelControl4.Controls.Add(this.labelControl7);
            this.panelControl4.Controls.Add(this.labelControl8);
            this.panelControl4.Location = new System.Drawing.Point(7, 6);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(833, 48);
            this.panelControl4.TabIndex = 15;
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit5.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit5.Location = new System.Drawing.Point(789, 4);
            this.pictureEdit5.MenuManager = this.barManager1;
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Properties.ReadOnly = true;
            this.pictureEdit5.Properties.ShowMenu = false;
            this.pictureEdit5.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit5.TabIndex = 9;
            // 
            // labelControl7
            // 
            this.labelControl7.AllowHtmlString = true;
            this.labelControl7.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseBackColor = true;
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(5, 5);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(199, 16);
            this.labelControl7.TabIndex = 6;
            this.labelControl7.Text = "<b>Step 2:</b> Create the Client Contract\r\n";
            // 
            // labelControl8
            // 
            this.labelControl8.AllowHtmlString = true;
            this.labelControl8.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl8.Appearance.Options.UseBackColor = true;
            this.labelControl8.Location = new System.Drawing.Point(57, 29);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(402, 13);
            this.labelControl8.TabIndex = 7;
            this.labelControl8.Text = "Complete the data entry boxes to create the Client Contract. Once done click Next" +
    ".\r\n";
            // 
            // btnStep2Next
            // 
            this.btnStep2Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep2Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep2Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep2Next.Location = new System.Drawing.Point(752, 500);
            this.btnStep2Next.Name = "btnStep2Next";
            this.btnStep2Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep2Next.TabIndex = 0;
            this.btnStep2Next.Text = "Next";
            this.btnStep2Next.Click += new System.EventHandler(this.btnStep2Next_Click);
            // 
            // btnStep2Previous
            // 
            this.btnStep2Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep2Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep2Previous.Location = new System.Drawing.Point(658, 500);
            this.btnStep2Previous.Name = "btnStep2Previous";
            this.btnStep2Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep2Previous.TabIndex = 10;
            this.btnStep2Previous.Text = "Previous";
            this.btnStep2Previous.Click += new System.EventHandler(this.btnStep2Previous_Click);
            // 
            // xtraTabPageStep3
            // 
            this.xtraTabPageStep3.Controls.Add(this.layoutControl1);
            this.xtraTabPageStep3.Controls.Add(this.btnStep3Next);
            this.xtraTabPageStep3.Controls.Add(this.btnStep3Previous);
            this.xtraTabPageStep3.Controls.Add(this.panelControl5);
            this.xtraTabPageStep3.Name = "xtraTabPageStep3";
            this.xtraTabPageStep3.Size = new System.Drawing.Size(846, 536);
            this.xtraTabPageStep3.Text = "Step 3";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.btnAddYearsBtn);
            this.layoutControl1.Controls.Add(this.gridControl5);
            this.layoutControl1.Controls.Add(this.spinEditContractYears);
            this.layoutControl1.Controls.Add(this.textEditEndDate);
            this.layoutControl1.Controls.Add(this.textEditStartDate);
            this.layoutControl1.Controls.Add(this.textEditClientName);
            this.layoutControl1.Location = new System.Drawing.Point(7, 62);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(534, 136, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup4;
            this.layoutControl1.Size = new System.Drawing.Size(832, 432);
            this.layoutControl1.TabIndex = 19;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnAddYearsBtn
            // 
            this.btnAddYearsBtn.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Add_16x16;
            this.btnAddYearsBtn.Location = new System.Drawing.Point(139, 82);
            this.btnAddYearsBtn.Name = "btnAddYearsBtn";
            this.btnAddYearsBtn.Size = new System.Drawing.Size(116, 22);
            this.btnAddYearsBtn.StyleController = this.layoutControl1;
            this.btnAddYearsBtn.TabIndex = 22;
            this.btnAddYearsBtn.Text = "Add Years To Grid";
            this.btnAddYearsBtn.Click += new System.EventHandler(this.btnAddYearsBtn_Click);
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp06067OMClientContractYearEditBindingSource;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl5.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl5_EmbeddedNavigator_ButtonClick);
            this.gridControl5.Location = new System.Drawing.Point(2, 118);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemSpinEdit1,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEdit50CharsMax,
            this.repositoryItemSpinEditPercentage});
            this.gridControl5.Size = new System.Drawing.Size(828, 312);
            this.gridControl5.TabIndex = 21;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp06067OMClientContractYearEditBindingSource
            // 
            this.sp06067OMClientContractYearEditBindingSource.DataMember = "sp06067_OM_Client_Contract_Year_Edit";
            this.sp06067OMClientContractYearEditBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientContractYearID,
            this.colClientContractID,
            this.colYearDescription,
            this.colStartDate,
            this.colEndDate,
            this.colActive,
            this.colClientValue,
            this.colRemarks1,
            this.colYearlyPercentageIncrease});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView5.OptionsFilter.AllowFilterEditor = false;
            this.gridView5.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView5.OptionsFilter.AllowMRUFilterList = false;
            this.gridView5.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView5.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView5.OptionsFind.FindDelay = 2000;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsLayout.StoreFormatRules = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colClientContractYearID
            // 
            this.colClientContractYearID.Caption = "Client Contract Year ID";
            this.colClientContractYearID.FieldName = "ClientContractYearID";
            this.colClientContractYearID.Name = "colClientContractYearID";
            this.colClientContractYearID.OptionsColumn.AllowEdit = false;
            this.colClientContractYearID.OptionsColumn.AllowFocus = false;
            this.colClientContractYearID.OptionsFilter.AllowAutoFilter = false;
            this.colClientContractYearID.OptionsFilter.AllowFilter = false;
            this.colClientContractYearID.Width = 132;
            // 
            // colClientContractID
            // 
            this.colClientContractID.Caption = "Client Contract ID";
            this.colClientContractID.FieldName = "ClientContractID";
            this.colClientContractID.Name = "colClientContractID";
            this.colClientContractID.OptionsColumn.AllowEdit = false;
            this.colClientContractID.OptionsColumn.AllowFocus = false;
            this.colClientContractID.OptionsFilter.AllowAutoFilter = false;
            this.colClientContractID.OptionsFilter.AllowFilter = false;
            this.colClientContractID.Width = 107;
            // 
            // colYearDescription
            // 
            this.colYearDescription.Caption = "Year Description";
            this.colYearDescription.ColumnEdit = this.repositoryItemTextEdit50CharsMax;
            this.colYearDescription.FieldName = "YearDescription";
            this.colYearDescription.Name = "colYearDescription";
            this.colYearDescription.OptionsFilter.AllowAutoFilter = false;
            this.colYearDescription.OptionsFilter.AllowFilter = false;
            this.colYearDescription.Visible = true;
            this.colYearDescription.VisibleIndex = 2;
            this.colYearDescription.Width = 183;
            // 
            // repositoryItemTextEdit50CharsMax
            // 
            this.repositoryItemTextEdit50CharsMax.AutoHeight = false;
            this.repositoryItemTextEdit50CharsMax.MaxLength = 50;
            this.repositoryItemTextEdit50CharsMax.Name = "repositoryItemTextEdit50CharsMax";
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsFilter.AllowAutoFilter = false;
            this.colStartDate.OptionsFilter.AllowFilter = false;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 0;
            this.colStartDate.Width = 114;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Mask.EditMask = "g";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "End Date";
            this.colEndDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsFilter.AllowAutoFilter = false;
            this.colEndDate.OptionsFilter.AllowFilter = false;
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 1;
            this.colEndDate.Width = 113;
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsFilter.AllowAutoFilter = false;
            this.colActive.OptionsFilter.AllowFilter = false;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 5;
            this.colActive.Width = 52;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colClientValue
            // 
            this.colClientValue.Caption = "Client Value";
            this.colClientValue.ColumnEdit = this.repositoryItemSpinEdit1;
            this.colClientValue.FieldName = "ClientValue";
            this.colClientValue.Name = "colClientValue";
            this.colClientValue.OptionsFilter.AllowAutoFilter = false;
            this.colClientValue.OptionsFilter.AllowFilter = false;
            this.colClientValue.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ClientValue", "{0:c2}")});
            this.colClientValue.Visible = true;
            this.colClientValue.VisibleIndex = 4;
            this.colClientValue.Width = 97;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.Mask.EditMask = "c";
            this.repositoryItemSpinEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsFilter.AllowAutoFilter = false;
            this.colRemarks1.OptionsFilter.AllowFilter = false;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 6;
            this.colRemarks1.Width = 105;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colYearlyPercentageIncrease
            // 
            this.colYearlyPercentageIncrease.Caption = "Yearly % Increase";
            this.colYearlyPercentageIncrease.ColumnEdit = this.repositoryItemSpinEditPercentage;
            this.colYearlyPercentageIncrease.FieldName = "YearlyPercentageIncrease";
            this.colYearlyPercentageIncrease.Name = "colYearlyPercentageIncrease";
            this.colYearlyPercentageIncrease.OptionsFilter.AllowAutoFilter = false;
            this.colYearlyPercentageIncrease.OptionsFilter.AllowFilter = false;
            this.colYearlyPercentageIncrease.Visible = true;
            this.colYearlyPercentageIncrease.VisibleIndex = 3;
            this.colYearlyPercentageIncrease.Width = 100;
            // 
            // repositoryItemSpinEditPercentage
            // 
            this.repositoryItemSpinEditPercentage.AutoHeight = false;
            this.repositoryItemSpinEditPercentage.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditPercentage.Mask.EditMask = "P";
            this.repositoryItemSpinEditPercentage.Mask.ShowPlaceHolders = false;
            this.repositoryItemSpinEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditPercentage.Name = "repositoryItemSpinEditPercentage";
            // 
            // spinEditContractYears
            // 
            this.spinEditContractYears.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditContractYears.Location = new System.Drawing.Point(81, 82);
            this.spinEditContractYears.MenuManager = this.barManager1;
            this.spinEditContractYears.Name = "spinEditContractYears";
            this.spinEditContractYears.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditContractYears.Properties.IsFloatValue = false;
            this.spinEditContractYears.Properties.Mask.EditMask = "N00";
            this.spinEditContractYears.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditContractYears.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.spinEditContractYears.Size = new System.Drawing.Size(54, 20);
            this.spinEditContractYears.StyleController = this.layoutControl1;
            this.spinEditContractYears.TabIndex = 20;
            this.spinEditContractYears.EditValueChanged += new System.EventHandler(this.spinEditContractYears_EditValueChanged);
            // 
            // textEditEndDate
            // 
            this.textEditEndDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "EndDate", true));
            this.textEditEndDate.Location = new System.Drawing.Point(699, 36);
            this.textEditEndDate.MenuManager = this.barManager1;
            this.textEditEndDate.Name = "textEditEndDate";
            this.textEditEndDate.Properties.Mask.EditMask = "g";
            this.textEditEndDate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.textEditEndDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditEndDate.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditEndDate, true);
            this.textEditEndDate.Size = new System.Drawing.Size(119, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditEndDate, optionsSpelling14);
            this.textEditEndDate.StyleController = this.layoutControl1;
            this.textEditEndDate.TabIndex = 6;
            // 
            // textEditStartDate
            // 
            this.textEditStartDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "StartDate", true));
            this.textEditStartDate.Location = new System.Drawing.Point(497, 36);
            this.textEditStartDate.MenuManager = this.barManager1;
            this.textEditStartDate.Name = "textEditStartDate";
            this.textEditStartDate.Properties.Mask.EditMask = "g";
            this.textEditStartDate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.textEditStartDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEditStartDate.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditStartDate, true);
            this.textEditStartDate.Size = new System.Drawing.Size(119, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditStartDate, optionsSpelling15);
            this.textEditStartDate.StyleController = this.layoutControl1;
            this.textEditStartDate.TabIndex = 5;
            // 
            // textEditClientName
            // 
            this.textEditClientName.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ClientName", true));
            this.textEditClientName.Location = new System.Drawing.Point(93, 36);
            this.textEditClientName.MenuManager = this.barManager1;
            this.textEditClientName.Name = "textEditClientName";
            this.textEditClientName.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditClientName, true);
            this.textEditClientName.Size = new System.Drawing.Size(321, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditClientName, optionsSpelling16);
            this.textEditClientName.StyleController = this.layoutControl1;
            this.textEditClientName.TabIndex = 4;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Root";
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlItem4,
            this.emptySpaceItem6,
            this.emptySpaceItem7,
            this.emptySpaceItem8,
            this.ItemForYearsGrid,
            this.ITemForAddYearsToGridButton});
            this.layoutControlGroup4.Name = "Root";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(832, 432);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Client Contract Details";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(832, 70);
            this.layoutControlGroup5.Text = "Client Contract Details";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textEditClientName;
            this.layoutControlItem1.CustomizationFormText = "Client Name:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(404, 24);
            this.layoutControlItem1.Text = "Client Name:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEditStartDate;
            this.layoutControlItem2.CustomizationFormText = "Start Date:";
            this.layoutControlItem2.Location = new System.Drawing.Point(404, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(202, 24);
            this.layoutControlItem2.Text = "Start Date:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textEditEndDate;
            this.layoutControlItem3.CustomizationFormText = "End Date:";
            this.layoutControlItem3.Location = new System.Drawing.Point(606, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(202, 24);
            this.layoutControlItem3.Text = "End Date:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.spinEditContractYears;
            this.layoutControlItem4.CustomizationFormText = "Contract Years:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 80);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(137, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(137, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(137, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "Contract Years:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(76, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 70);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(832, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(257, 80);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(575, 26);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 106);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(832, 10);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForYearsGrid
            // 
            this.ItemForYearsGrid.Control = this.gridControl5;
            this.ItemForYearsGrid.CustomizationFormText = "Years Grid:";
            this.ItemForYearsGrid.Location = new System.Drawing.Point(0, 116);
            this.ItemForYearsGrid.Name = "ItemForYearsGrid";
            this.ItemForYearsGrid.Size = new System.Drawing.Size(832, 316);
            this.ItemForYearsGrid.Text = "Years Grid:";
            this.ItemForYearsGrid.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForYearsGrid.TextVisible = false;
            // 
            // ITemForAddYearsToGridButton
            // 
            this.ITemForAddYearsToGridButton.Control = this.btnAddYearsBtn;
            this.ITemForAddYearsToGridButton.CustomizationFormText = "Add Years To Grid Button";
            this.ITemForAddYearsToGridButton.Location = new System.Drawing.Point(137, 80);
            this.ITemForAddYearsToGridButton.MaxSize = new System.Drawing.Size(120, 26);
            this.ITemForAddYearsToGridButton.MinSize = new System.Drawing.Size(120, 26);
            this.ITemForAddYearsToGridButton.Name = "ITemForAddYearsToGridButton";
            this.ITemForAddYearsToGridButton.Size = new System.Drawing.Size(120, 26);
            this.ITemForAddYearsToGridButton.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ITemForAddYearsToGridButton.Text = "Add Years To Grid Button";
            this.ITemForAddYearsToGridButton.TextSize = new System.Drawing.Size(0, 0);
            this.ITemForAddYearsToGridButton.TextVisible = false;
            // 
            // btnStep3Next
            // 
            this.btnStep3Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep3Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep3Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep3Next.Location = new System.Drawing.Point(752, 500);
            this.btnStep3Next.Name = "btnStep3Next";
            this.btnStep3Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep3Next.TabIndex = 17;
            this.btnStep3Next.Text = "Next";
            this.btnStep3Next.Click += new System.EventHandler(this.btnStep3Next_Click);
            // 
            // btnStep3Previous
            // 
            this.btnStep3Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep3Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep3Previous.Location = new System.Drawing.Point(658, 500);
            this.btnStep3Previous.Name = "btnStep3Previous";
            this.btnStep3Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep3Previous.TabIndex = 18;
            this.btnStep3Previous.Text = "Previous";
            this.btnStep3Previous.Click += new System.EventHandler(this.btnStep3Previous_Click);
            // 
            // panelControl5
            // 
            this.panelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl5.Controls.Add(this.pictureEdit7);
            this.panelControl5.Controls.Add(this.labelControl4);
            this.panelControl5.Controls.Add(this.labelControl6);
            this.panelControl5.Location = new System.Drawing.Point(7, 6);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(833, 48);
            this.panelControl5.TabIndex = 16;
            // 
            // pictureEdit7
            // 
            this.pictureEdit7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit7.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit7.Location = new System.Drawing.Point(789, 4);
            this.pictureEdit7.MenuManager = this.barManager1;
            this.pictureEdit7.Name = "pictureEdit7";
            this.pictureEdit7.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit7.Properties.ReadOnly = true;
            this.pictureEdit7.Properties.ShowMenu = false;
            this.pictureEdit7.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit7.TabIndex = 9;
            // 
            // labelControl4
            // 
            this.labelControl4.AllowHtmlString = true;
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseBackColor = true;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(5, 5);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(256, 16);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "<b>Step 3:</b> Create Contract Year(s) <b>[Optional]</b>\r\n\r\n";
            // 
            // labelControl6
            // 
            this.labelControl6.AllowHtmlString = true;
            this.labelControl6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl6.Appearance.Options.UseBackColor = true;
            this.labelControl6.Location = new System.Drawing.Point(57, 29);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(733, 13);
            this.labelControl6.TabIndex = 7;
            this.labelControl6.Text = "Enter the number of <b>Contract Years</b> the contract spans and click <b>Add Yea" +
    "rs To Grid</b>. Optionally complete the data in the grid. Click Next when done.";
            // 
            // xtraTabPageStep4
            // 
            this.xtraTabPageStep4.Controls.Add(this.layoutControl2);
            this.xtraTabPageStep4.Controls.Add(this.btnStep4Next);
            this.xtraTabPageStep4.Controls.Add(this.btnStep4Previous);
            this.xtraTabPageStep4.Controls.Add(this.panelControl6);
            this.xtraTabPageStep4.Name = "xtraTabPageStep4";
            this.xtraTabPageStep4.Size = new System.Drawing.Size(846, 536);
            this.xtraTabPageStep4.Text = "Step 4";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl2.Controls.Add(this.splitContainerControl1);
            this.layoutControl2.Controls.Add(this.textEdit1);
            this.layoutControl2.Controls.Add(this.textEdit2);
            this.layoutControl2.Controls.Add(this.textEdit3);
            this.layoutControl2.Location = new System.Drawing.Point(7, 62);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(534, 136, 250, 350);
            this.layoutControl2.Root = this.layoutControlGroup6;
            this.layoutControl2.Size = new System.Drawing.Size(832, 432);
            this.layoutControl2.TabIndex = 21;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Location = new System.Drawing.Point(2, 82);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl6);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Contract Year(s)  -  [Read Only]  Set from Step 3";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl7);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Billing Profile";
            this.splitContainerControl1.Size = new System.Drawing.Size(828, 348);
            this.splitContainerControl1.SplitterPosition = 429;
            this.splitContainerControl1.TabIndex = 22;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.sp06067OMClientContractYearEditBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3,
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditDateTime});
            this.gridControl6.Size = new System.Drawing.Size(425, 324);
            this.gridControl6.TabIndex = 21;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowFilterEditor = false;
            this.gridView6.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView6.OptionsFilter.AllowMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView6.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView6.OptionsFind.FindDelay = 2000;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsLayout.StoreFormatRules = true;
            this.gridView6.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn13, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Client Contract Year ID";
            this.gridColumn10.FieldName = "ClientContractYearID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn10.OptionsFilter.AllowFilter = false;
            this.gridColumn10.Width = 132;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Client Contract ID";
            this.gridColumn11.FieldName = "ClientContractID";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn11.OptionsFilter.AllowFilter = false;
            this.gridColumn11.Width = 107;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Year Description";
            this.gridColumn12.FieldName = "YearDescription";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn12.OptionsFilter.AllowFilter = false;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 2;
            this.gridColumn12.Width = 105;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Start Date";
            this.gridColumn13.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.gridColumn13.FieldName = "StartDate";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn13.OptionsFilter.AllowFilter = false;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 0;
            this.gridColumn13.Width = 100;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "End Date";
            this.gridColumn14.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.gridColumn14.FieldName = "EndDate";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn14.OptionsFilter.AllowFilter = false;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 1;
            this.gridColumn14.Width = 100;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Active";
            this.gridColumn15.ColumnEdit = this.repositoryItemCheckEdit3;
            this.gridColumn15.FieldName = "Active";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn15.OptionsFilter.AllowFilter = false;
            this.gridColumn15.Width = 52;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Client Value";
            this.gridColumn16.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.gridColumn16.FieldName = "ClientValue";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn16.OptionsFilter.AllowFilter = false;
            this.gridColumn16.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ClientValue", "{0:c2}")});
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 3;
            this.gridColumn16.Width = 79;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Remarks";
            this.gridColumn17.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.gridColumn17.FieldName = "Remarks";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn17.OptionsFilter.AllowFilter = false;
            this.gridColumn17.Width = 105;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(388, 26);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // gridControl7
            // 
            this.gridControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl7.DataSource = this.sp06069OMClientContractYearBillingProfileEditBindingSource;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl7.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl7.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl7_EmbeddedNavigator_ButtonClick);
            this.gridControl7.Location = new System.Drawing.Point(0, 28);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemDateEdit2,
            this.repositoryItemSpinEditCurrency});
            this.gridControl7.Size = new System.Drawing.Size(389, 297);
            this.gridControl7.TabIndex = 22;
            this.gridControl7.UseEmbeddedNavigator = true;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp06069OMClientContractYearBillingProfileEditBindingSource
            // 
            this.sp06069OMClientContractYearBillingProfileEditBindingSource.DataMember = "sp06069_OM_Client_Contract_Year_Billing_Profile_Edit";
            this.sp06069OMClientContractYearBillingProfileEditBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientContractProfileID,
            this.colClientContractYearID1,
            this.colInvoiceDateDue,
            this.colInvoiceDateActual,
            this.colEstimatedBillAmount,
            this.colActualBillAmount,
            this.colBilledByPersonID,
            this.colRemarks2,
            this.colLinkedToParent});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.GroupCount = 1;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView7.OptionsFilter.AllowFilterEditor = false;
            this.gridView7.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView7.OptionsFilter.AllowMRUFilterList = false;
            this.gridView7.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView7.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView7.OptionsFind.FindDelay = 2000;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsLayout.StoreFormatRules = true;
            this.gridView7.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.MultiSelect = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInvoiceDateDue, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView7.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView7.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView7_MouseUp);
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            // 
            // colClientContractProfileID
            // 
            this.colClientContractProfileID.Caption = "Client Contract Profile ID";
            this.colClientContractProfileID.FieldName = "ClientContractProfileID";
            this.colClientContractProfileID.Name = "colClientContractProfileID";
            this.colClientContractProfileID.OptionsColumn.AllowEdit = false;
            this.colClientContractProfileID.OptionsColumn.AllowFocus = false;
            this.colClientContractProfileID.OptionsColumn.ReadOnly = true;
            this.colClientContractProfileID.Width = 153;
            // 
            // colClientContractYearID1
            // 
            this.colClientContractYearID1.Caption = "Client Contract Year ID";
            this.colClientContractYearID1.FieldName = "ClientContractYearID";
            this.colClientContractYearID1.Name = "colClientContractYearID1";
            this.colClientContractYearID1.OptionsColumn.AllowEdit = false;
            this.colClientContractYearID1.OptionsColumn.AllowFocus = false;
            this.colClientContractYearID1.OptionsColumn.ReadOnly = true;
            this.colClientContractYearID1.Width = 132;
            // 
            // colInvoiceDateDue
            // 
            this.colInvoiceDateDue.Caption = "Invoice Due Date";
            this.colInvoiceDateDue.ColumnEdit = this.repositoryItemDateEdit2;
            this.colInvoiceDateDue.FieldName = "InvoiceDateDue";
            this.colInvoiceDateDue.Name = "colInvoiceDateDue";
            this.colInvoiceDateDue.Visible = true;
            this.colInvoiceDateDue.VisibleIndex = 0;
            this.colInvoiceDateDue.Width = 117;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.Mask.EditMask = "g";
            this.repositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // colInvoiceDateActual
            // 
            this.colInvoiceDateActual.Caption = "Invoice Actual Date";
            this.colInvoiceDateActual.ColumnEdit = this.repositoryItemDateEdit2;
            this.colInvoiceDateActual.FieldName = "InvoiceDateActual";
            this.colInvoiceDateActual.Name = "colInvoiceDateActual";
            this.colInvoiceDateActual.Visible = true;
            this.colInvoiceDateActual.VisibleIndex = 2;
            this.colInvoiceDateActual.Width = 115;
            // 
            // colEstimatedBillAmount
            // 
            this.colEstimatedBillAmount.Caption = "Estimated Bill Amount";
            this.colEstimatedBillAmount.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colEstimatedBillAmount.FieldName = "EstimatedBillAmount";
            this.colEstimatedBillAmount.Name = "colEstimatedBillAmount";
            this.colEstimatedBillAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "EstimatedBillAmount", "{0:c2}")});
            this.colEstimatedBillAmount.Visible = true;
            this.colEstimatedBillAmount.VisibleIndex = 1;
            this.colEstimatedBillAmount.Width = 123;
            // 
            // repositoryItemSpinEditCurrency
            // 
            this.repositoryItemSpinEditCurrency.AutoHeight = false;
            this.repositoryItemSpinEditCurrency.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditCurrency.Mask.EditMask = "c";
            this.repositoryItemSpinEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditCurrency.Name = "repositoryItemSpinEditCurrency";
            // 
            // colActualBillAmount
            // 
            this.colActualBillAmount.Caption = "Actual Bill Amount";
            this.colActualBillAmount.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colActualBillAmount.FieldName = "ActualBillAmount";
            this.colActualBillAmount.Name = "colActualBillAmount";
            this.colActualBillAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ActualBillAmount", "{0:c2}")});
            this.colActualBillAmount.Visible = true;
            this.colActualBillAmount.VisibleIndex = 3;
            this.colActualBillAmount.Width = 106;
            // 
            // colBilledByPersonID
            // 
            this.colBilledByPersonID.Caption = "Billed By Person ID";
            this.colBilledByPersonID.FieldName = "BilledByPersonID";
            this.colBilledByPersonID.Name = "colBilledByPersonID";
            this.colBilledByPersonID.OptionsColumn.AllowEdit = false;
            this.colBilledByPersonID.OptionsColumn.AllowFocus = false;
            this.colBilledByPersonID.OptionsColumn.ReadOnly = true;
            this.colBilledByPersonID.Width = 110;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 4;
            this.colRemarks2.Width = 92;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colLinkedToParent
            // 
            this.colLinkedToParent.Caption = "Linked To Contract Year";
            this.colLinkedToParent.FieldName = "LinkedToParent";
            this.colLinkedToParent.Name = "colLinkedToParent";
            this.colLinkedToParent.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent.Visible = true;
            this.colLinkedToParent.VisibleIndex = 5;
            this.colLinkedToParent.Width = 327;
            // 
            // textEdit1
            // 
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "EndDate", true));
            this.textEdit1.Location = new System.Drawing.Point(684, 36);
            this.textEdit1.MenuManager = this.barManager1;
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Mask.EditMask = "g";
            this.textEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.textEdit1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEdit1.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEdit1, true);
            this.textEdit1.Size = new System.Drawing.Size(134, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEdit1, optionsSpelling17);
            this.textEdit1.StyleController = this.layoutControl2;
            this.textEdit1.TabIndex = 6;
            // 
            // textEdit2
            // 
            this.textEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "StartDate", true));
            this.textEdit2.Location = new System.Drawing.Point(482, 36);
            this.textEdit2.MenuManager = this.barManager1;
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Mask.EditMask = "g";
            this.textEdit2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.textEdit2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.textEdit2.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEdit2, true);
            this.textEdit2.Size = new System.Drawing.Size(134, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEdit2, optionsSpelling18);
            this.textEdit2.StyleController = this.layoutControl2;
            this.textEdit2.TabIndex = 5;
            // 
            // textEdit3
            // 
            this.textEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp06059OMClientContractEditBindingSource, "ClientName", true));
            this.textEdit3.Location = new System.Drawing.Point(78, 36);
            this.textEdit3.MenuManager = this.barManager1;
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEdit3, true);
            this.textEdit3.Size = new System.Drawing.Size(336, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEdit3, optionsSpelling19);
            this.textEdit3.StyleController = this.layoutControl2;
            this.textEdit3.TabIndex = 4;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Root";
            this.layoutControlGroup6.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup6.GroupBordersVisible = false;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.emptySpaceItem5,
            this.layoutControlItem8});
            this.layoutControlGroup6.Name = "Root";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup6.Size = new System.Drawing.Size(832, 432);
            this.layoutControlGroup6.TextVisible = false;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Client Contract Details";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup5";
            this.layoutControlGroup7.Size = new System.Drawing.Size(832, 70);
            this.layoutControlGroup7.Text = "Client Contract Details";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEdit3;
            this.layoutControlItem5.CustomizationFormText = "Client Name:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem1";
            this.layoutControlItem5.Size = new System.Drawing.Size(404, 24);
            this.layoutControlItem5.Text = "Client Name:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEdit2;
            this.layoutControlItem6.CustomizationFormText = "Start Date:";
            this.layoutControlItem6.Location = new System.Drawing.Point(404, 0);
            this.layoutControlItem6.Name = "layoutControlItem2";
            this.layoutControlItem6.Size = new System.Drawing.Size(202, 24);
            this.layoutControlItem6.Text = "Start Date:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEdit1;
            this.layoutControlItem7.CustomizationFormText = "End Date:";
            this.layoutControlItem7.Location = new System.Drawing.Point(606, 0);
            this.layoutControlItem7.Name = "layoutControlItem3";
            this.layoutControlItem7.Size = new System.Drawing.Size(202, 24);
            this.layoutControlItem7.Text = "End Date:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(61, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 70);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem6";
            this.emptySpaceItem5.Size = new System.Drawing.Size(832, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.splitContainerControl1;
            this.layoutControlItem8.CustomizationFormText = "Contract Year and Billing Profile Grids:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 80);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(832, 352);
            this.layoutControlItem8.Text = "Contract Year and Billing Profile Grids:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // btnStep4Next
            // 
            this.btnStep4Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep4Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep4Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep4Next.Location = new System.Drawing.Point(752, 500);
            this.btnStep4Next.Name = "btnStep4Next";
            this.btnStep4Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep4Next.TabIndex = 19;
            this.btnStep4Next.Text = "Next";
            this.btnStep4Next.Click += new System.EventHandler(this.btnStep4Next_Click);
            // 
            // btnStep4Previous
            // 
            this.btnStep4Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep4Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep4Previous.Location = new System.Drawing.Point(658, 500);
            this.btnStep4Previous.Name = "btnStep4Previous";
            this.btnStep4Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep4Previous.TabIndex = 20;
            this.btnStep4Previous.Text = "Previous";
            this.btnStep4Previous.Click += new System.EventHandler(this.btnStep4Previous_Click);
            // 
            // panelControl6
            // 
            this.panelControl6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl6.Controls.Add(this.pictureEdit8);
            this.panelControl6.Controls.Add(this.labelControl13);
            this.panelControl6.Controls.Add(this.labelControl14);
            this.panelControl6.Location = new System.Drawing.Point(7, 6);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(833, 48);
            this.panelControl6.TabIndex = 17;
            // 
            // pictureEdit8
            // 
            this.pictureEdit8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit8.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit8.Location = new System.Drawing.Point(789, 4);
            this.pictureEdit8.MenuManager = this.barManager1;
            this.pictureEdit8.Name = "pictureEdit8";
            this.pictureEdit8.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit8.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit8.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit8.Properties.ReadOnly = true;
            this.pictureEdit8.Properties.ShowMenu = false;
            this.pictureEdit8.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit8.TabIndex = 9;
            // 
            // labelControl13
            // 
            this.labelControl13.AllowHtmlString = true;
            this.labelControl13.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.Options.UseBackColor = true;
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Location = new System.Drawing.Point(5, 5);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(251, 16);
            this.labelControl13.TabIndex = 6;
            this.labelControl13.Text = "<b>Step 4:</b> Create Billing Profile(s) <b>[Optional]</b>\r\n\r\n";
            // 
            // labelControl14
            // 
            this.labelControl14.AllowHtmlString = true;
            this.labelControl14.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl14.Appearance.Options.UseBackColor = true;
            this.labelControl14.Location = new System.Drawing.Point(57, 29);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(484, 13);
            this.labelControl14.TabIndex = 7;
            this.labelControl14.Text = "Select each contract year by clicking on it then set up the Billing Profile in th" +
    "e in the Billing Profile grid.";
            // 
            // xtraTabPageStep5
            // 
            this.xtraTabPageStep5.Controls.Add(this.standaloneBarDockControl2);
            this.xtraTabPageStep5.Controls.Add(this.gridControl8);
            this.xtraTabPageStep5.Controls.Add(this.btnStep5Next);
            this.xtraTabPageStep5.Controls.Add(this.btnStep5Previous);
            this.xtraTabPageStep5.Controls.Add(this.panelControl7);
            this.xtraTabPageStep5.Name = "xtraTabPageStep5";
            this.xtraTabPageStep5.Size = new System.Drawing.Size(846, 536);
            this.xtraTabPageStep5.Text = "Step 5";
            // 
            // standaloneBarDockControl2
            // 
            this.standaloneBarDockControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl2.CausesValidation = false;
            this.standaloneBarDockControl2.Location = new System.Drawing.Point(8, 61);
            this.standaloneBarDockControl2.Manager = this.barManager1;
            this.standaloneBarDockControl2.Name = "standaloneBarDockControl2";
            this.standaloneBarDockControl2.Size = new System.Drawing.Size(832, 25);
            this.standaloneBarDockControl2.Text = "standaloneBarDockControl2";
            // 
            // gridControl8
            // 
            this.gridControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl8.DataSource = this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl8.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl8.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl8_EmbeddedNavigator_ButtonClick);
            this.gridControl8.Location = new System.Drawing.Point(9, 86);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.MenuManager = this.barManager1;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemButtonEdit1,
            this.repositoryItemButtonEditResponsibilityType});
            this.gridControl8.Size = new System.Drawing.Size(831, 408);
            this.gridControl8.TabIndex = 23;
            this.gridControl8.UseEmbeddedNavigator = true;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // sp06081OMClientContractLinkedResponsibilitiesEditBindingSource
            // 
            this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.DataMember = "sp06081_OM_Client_Contract_Linked_Responsibilities_Edit";
            this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPersonResponsibilityID,
            this.colStaffID,
            this.colResponsibleForRecordID,
            this.colResponsibleForRecordTypeID,
            this.colResponsibilityTypeID,
            this.colRemarks3,
            this.colClientID1,
            this.colClientName1,
            this.colContractStartDate,
            this.colContractEndDate,
            this.colLinkedToParent1,
            this.colResponsibilityType,
            this.colStaffName,
            this.colPersonTypeDescription,
            this.colPersonTypeID});
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView8.OptionsFilter.AllowFilterEditor = false;
            this.gridView8.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView8.OptionsFilter.AllowMRUFilterList = false;
            this.gridView8.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView8.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView8.OptionsFind.FindDelay = 2000;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsLayout.StoreFormatRules = true;
            this.gridView8.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.MultiSelect = true;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView8.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView8.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView8_MouseUp);
            this.gridView8.GotFocus += new System.EventHandler(this.gridView8_GotFocus);
            // 
            // colPersonResponsibilityID
            // 
            this.colPersonResponsibilityID.Caption = "Person Responsibility ID";
            this.colPersonResponsibilityID.FieldName = "PersonResponsibilityID";
            this.colPersonResponsibilityID.Name = "colPersonResponsibilityID";
            this.colPersonResponsibilityID.OptionsColumn.AllowEdit = false;
            this.colPersonResponsibilityID.OptionsColumn.AllowFocus = false;
            this.colPersonResponsibilityID.OptionsColumn.ReadOnly = true;
            this.colPersonResponsibilityID.OptionsFilter.AllowFilter = false;
            this.colPersonResponsibilityID.Width = 126;
            // 
            // colStaffID
            // 
            this.colStaffID.Caption = "Staff ID";
            this.colStaffID.FieldName = "StaffID";
            this.colStaffID.Name = "colStaffID";
            this.colStaffID.OptionsColumn.AllowEdit = false;
            this.colStaffID.OptionsColumn.AllowFocus = false;
            this.colStaffID.OptionsColumn.ReadOnly = true;
            this.colStaffID.OptionsFilter.AllowFilter = false;
            this.colStaffID.Width = 49;
            // 
            // colResponsibleForRecordID
            // 
            this.colResponsibleForRecordID.Caption = "Responsible For Record ID";
            this.colResponsibleForRecordID.FieldName = "ResponsibleForRecordID";
            this.colResponsibleForRecordID.Name = "colResponsibleForRecordID";
            this.colResponsibleForRecordID.OptionsColumn.AllowEdit = false;
            this.colResponsibleForRecordID.OptionsColumn.AllowFocus = false;
            this.colResponsibleForRecordID.OptionsColumn.ReadOnly = true;
            this.colResponsibleForRecordID.OptionsFilter.AllowFilter = false;
            this.colResponsibleForRecordID.Width = 138;
            // 
            // colResponsibleForRecordTypeID
            // 
            this.colResponsibleForRecordTypeID.Caption = "Responsible For Record Type ID";
            this.colResponsibleForRecordTypeID.FieldName = "ResponsibleForRecordTypeID";
            this.colResponsibleForRecordTypeID.Name = "colResponsibleForRecordTypeID";
            this.colResponsibleForRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colResponsibleForRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colResponsibleForRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colResponsibleForRecordTypeID.OptionsFilter.AllowFilter = false;
            this.colResponsibleForRecordTypeID.Width = 165;
            // 
            // colResponsibilityTypeID
            // 
            this.colResponsibilityTypeID.Caption = "Responsibility Type ID";
            this.colResponsibilityTypeID.FieldName = "ResponsibilityTypeID";
            this.colResponsibilityTypeID.Name = "colResponsibilityTypeID";
            this.colResponsibilityTypeID.OptionsColumn.AllowEdit = false;
            this.colResponsibilityTypeID.OptionsColumn.AllowFocus = false;
            this.colResponsibilityTypeID.OptionsColumn.ReadOnly = true;
            this.colResponsibilityTypeID.OptionsFilter.AllowFilter = false;
            this.colResponsibilityTypeID.Width = 171;
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.OptionsFilter.AllowFilter = false;
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 3;
            this.colRemarks3.Width = 237;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            this.colClientID1.OptionsFilter.AllowFilter = false;
            this.colClientID1.Width = 54;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.OptionsFilter.AllowFilter = false;
            this.colClientName1.Width = 160;
            // 
            // colContractStartDate
            // 
            this.colContractStartDate.Caption = "Contract Start Date";
            this.colContractStartDate.FieldName = "ContractStartDate";
            this.colContractStartDate.Name = "colContractStartDate";
            this.colContractStartDate.OptionsColumn.AllowEdit = false;
            this.colContractStartDate.OptionsColumn.AllowFocus = false;
            this.colContractStartDate.OptionsColumn.ReadOnly = true;
            this.colContractStartDate.OptionsFilter.AllowFilter = false;
            this.colContractStartDate.Width = 106;
            // 
            // colContractEndDate
            // 
            this.colContractEndDate.Caption = "Contract End Date";
            this.colContractEndDate.FieldName = "ContractEndDate";
            this.colContractEndDate.Name = "colContractEndDate";
            this.colContractEndDate.OptionsColumn.AllowEdit = false;
            this.colContractEndDate.OptionsColumn.AllowFocus = false;
            this.colContractEndDate.OptionsColumn.ReadOnly = true;
            this.colContractEndDate.OptionsFilter.AllowFilter = false;
            this.colContractEndDate.Width = 100;
            // 
            // colLinkedToParent1
            // 
            this.colLinkedToParent1.Caption = "Linked To Parent";
            this.colLinkedToParent1.FieldName = "LinkedToParent";
            this.colLinkedToParent1.Name = "colLinkedToParent1";
            this.colLinkedToParent1.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent1.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent1.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent1.OptionsFilter.AllowFilter = false;
            this.colLinkedToParent1.Width = 91;
            // 
            // colResponsibilityType
            // 
            this.colResponsibilityType.Caption = "Responsibility Type";
            this.colResponsibilityType.ColumnEdit = this.repositoryItemButtonEditResponsibilityType;
            this.colResponsibilityType.FieldName = "ResponsibilityType";
            this.colResponsibilityType.Name = "colResponsibilityType";
            this.colResponsibilityType.OptionsFilter.AllowFilter = false;
            this.colResponsibilityType.Visible = true;
            this.colResponsibilityType.VisibleIndex = 2;
            this.colResponsibilityType.Width = 159;
            // 
            // repositoryItemButtonEditResponsibilityType
            // 
            this.repositoryItemButtonEditResponsibilityType.AutoHeight = false;
            this.repositoryItemButtonEditResponsibilityType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Ci=lick me to open the Select Responsibility Type screen.", "Choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditResponsibilityType.Name = "repositoryItemButtonEditResponsibilityType";
            this.repositoryItemButtonEditResponsibilityType.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditResponsibilityType.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditResponsibilityType_ButtonClick);
            // 
            // colStaffName
            // 
            this.colStaffName.Caption = "Person Name";
            this.colStaffName.ColumnEdit = this.repositoryItemButtonEdit1;
            this.colStaffName.FieldName = "StaffName";
            this.colStaffName.Name = "colStaffName";
            this.colStaffName.OptionsFilter.AllowFilter = false;
            this.colStaffName.Visible = true;
            this.colStaffName.VisibleIndex = 1;
            this.colStaffName.Width = 244;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Click me to open the Select Staff screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // colPersonTypeDescription
            // 
            this.colPersonTypeDescription.Caption = "Person Type";
            this.colPersonTypeDescription.FieldName = "PersonTypeDescription";
            this.colPersonTypeDescription.Name = "colPersonTypeDescription";
            this.colPersonTypeDescription.OptionsColumn.AllowEdit = false;
            this.colPersonTypeDescription.OptionsColumn.AllowFocus = false;
            this.colPersonTypeDescription.OptionsFilter.AllowFilter = false;
            this.colPersonTypeDescription.Visible = true;
            this.colPersonTypeDescription.VisibleIndex = 0;
            this.colPersonTypeDescription.Width = 99;
            // 
            // colPersonTypeID
            // 
            this.colPersonTypeID.Caption = "Person Type ID";
            this.colPersonTypeID.FieldName = "PersonTypeID";
            this.colPersonTypeID.Name = "colPersonTypeID";
            this.colPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colPersonTypeID.OptionsFilter.AllowFilter = false;
            this.colPersonTypeID.Width = 83;
            // 
            // btnStep5Next
            // 
            this.btnStep5Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep5Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep5Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep5Next.Location = new System.Drawing.Point(752, 500);
            this.btnStep5Next.Name = "btnStep5Next";
            this.btnStep5Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep5Next.TabIndex = 21;
            this.btnStep5Next.Text = "Next";
            this.btnStep5Next.Click += new System.EventHandler(this.btnStep5Next_Click);
            // 
            // btnStep5Previous
            // 
            this.btnStep5Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep5Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep5Previous.Location = new System.Drawing.Point(658, 500);
            this.btnStep5Previous.Name = "btnStep5Previous";
            this.btnStep5Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep5Previous.TabIndex = 22;
            this.btnStep5Previous.Text = "Previous";
            this.btnStep5Previous.Click += new System.EventHandler(this.btnStep5Previous_Click);
            // 
            // panelControl7
            // 
            this.panelControl7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl7.Controls.Add(this.pictureEdit9);
            this.panelControl7.Controls.Add(this.labelControl15);
            this.panelControl7.Controls.Add(this.labelControl16);
            this.panelControl7.Location = new System.Drawing.Point(7, 6);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(833, 48);
            this.panelControl7.TabIndex = 18;
            // 
            // pictureEdit9
            // 
            this.pictureEdit9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit9.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit9.Location = new System.Drawing.Point(789, 4);
            this.pictureEdit9.MenuManager = this.barManager1;
            this.pictureEdit9.Name = "pictureEdit9";
            this.pictureEdit9.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit9.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit9.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit9.Properties.ReadOnly = true;
            this.pictureEdit9.Properties.ShowMenu = false;
            this.pictureEdit9.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit9.TabIndex = 9;
            // 
            // labelControl15
            // 
            this.labelControl15.AllowHtmlString = true;
            this.labelControl15.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.Options.UseBackColor = true;
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Location = new System.Drawing.Point(5, 5);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(292, 16);
            this.labelControl15.TabIndex = 6;
            this.labelControl15.Text = "<b>Step 5:</b> Create Person Responsibilities <b>[Optional]</b>";
            // 
            // labelControl16
            // 
            this.labelControl16.AllowHtmlString = true;
            this.labelControl16.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl16.Appearance.Options.UseBackColor = true;
            this.labelControl16.Location = new System.Drawing.Point(57, 29);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(358, 13);
            this.labelControl16.TabIndex = 7;
            this.labelControl16.Text = "Add one or more Person Responsibilities to the grid. Click Next when done.";
            // 
            // xtraTabPageFinish
            // 
            this.xtraTabPageFinish.Controls.Add(this.panelControl2);
            this.xtraTabPageFinish.Controls.Add(this.groupControl1);
            this.xtraTabPageFinish.Controls.Add(this.labelControl10);
            this.xtraTabPageFinish.Controls.Add(this.btnFinish);
            this.xtraTabPageFinish.Controls.Add(this.btnFinishPrevious);
            this.xtraTabPageFinish.Controls.Add(this.pictureEdit2);
            this.xtraTabPageFinish.Name = "xtraTabPageFinish";
            this.xtraTabPageFinish.Size = new System.Drawing.Size(846, 536);
            this.xtraTabPageFinish.Text = "Finished";
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.pictureEdit6);
            this.panelControl2.Controls.Add(this.labelControl11);
            this.panelControl2.Controls.Add(this.labelControl12);
            this.panelControl2.Location = new System.Drawing.Point(211, 6);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(629, 48);
            this.panelControl2.TabIndex = 18;
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit6.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit6.Location = new System.Drawing.Point(585, 4);
            this.pictureEdit6.MenuManager = this.barManager1;
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit6.Properties.ReadOnly = true;
            this.pictureEdit6.Properties.ShowMenu = false;
            this.pictureEdit6.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit6.TabIndex = 19;
            // 
            // labelControl11
            // 
            this.labelControl11.AllowHtmlString = true;
            this.labelControl11.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl11.Appearance.Options.UseBackColor = true;
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(5, 1);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(240, 29);
            this.labelControl11.TabIndex = 5;
            this.labelControl11.Text = "Completing the Wizard";
            // 
            // labelControl12
            // 
            this.labelControl12.AllowHtmlString = true;
            this.labelControl12.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl12.Appearance.Options.UseBackColor = true;
            this.labelControl12.Location = new System.Drawing.Point(9, 31);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(498, 13);
            this.labelControl12.TabIndex = 15;
            this.labelControl12.Text = "You have successfully completed the Wizard. On clicking <b>Finish</b>, a client c" +
    "ontract record will be created.";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.checkEdit2);
            this.groupControl1.Controls.Add(this.checkEdit3);
            this.groupControl1.Controls.Add(this.checkEdit1);
            this.groupControl1.Location = new System.Drawing.Point(211, 155);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(629, 104);
            this.groupControl1.TabIndex = 16;
            this.groupControl1.Text = "Available Choices:";
            // 
            // checkEdit2
            // 
            this.checkEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit2.Location = new System.Drawing.Point(6, 51);
            this.checkEdit2.MenuManager = this.barManager1;
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit2.Properties.Caption = "Create the Client Contract then <b>open</b> the <b>Site Contract Wizard</b> to cr" +
    "eate Site Contracts.";
            this.checkEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit2.Properties.RadioGroupIndex = 1;
            this.checkEdit2.Size = new System.Drawing.Size(617, 19);
            this.checkEdit2.TabIndex = 4;
            this.checkEdit2.TabStop = false;
            // 
            // checkEdit3
            // 
            this.checkEdit3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit3.Location = new System.Drawing.Point(6, 76);
            this.checkEdit3.MenuManager = this.barManager1;
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit3.Properties.Caption = "Create the Client Contract then <b>open</b> the <b>Client Contract Wizard</b> to " +
    "create another Client Contract.";
            this.checkEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit3.Properties.RadioGroupIndex = 1;
            this.checkEdit3.Size = new System.Drawing.Size(617, 19);
            this.checkEdit3.TabIndex = 2;
            this.checkEdit3.TabStop = false;
            // 
            // checkEdit1
            // 
            this.checkEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit1.EditValue = true;
            this.checkEdit1.Location = new System.Drawing.Point(6, 26);
            this.checkEdit1.MenuManager = this.barManager1;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit1.Properties.Caption = "Create the Client Contract then <b>return</b> to the <b>Client Contract Manager</" +
    "b>.";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit1.Properties.RadioGroupIndex = 1;
            this.checkEdit1.Size = new System.Drawing.Size(617, 19);
            this.checkEdit1.TabIndex = 0;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(213, 132);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(154, 13);
            this.labelControl10.TabIndex = 15;
            this.labelControl10.Text = "What would you like to do next?";
            // 
            // btnFinish
            // 
            this.btnFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinish.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnFinish.Appearance.Options.UseFont = true;
            this.btnFinish.ImageOptions.Image = global::WoodPlan5.Properties.Resources.apply_32x32;
            this.btnFinish.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnFinish.Location = new System.Drawing.Point(752, 500);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(88, 30);
            this.btnFinish.TabIndex = 12;
            this.btnFinish.Text = "Finish";
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnFinishPrevious
            // 
            this.btnFinishPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinishPrevious.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnFinishPrevious.Location = new System.Drawing.Point(658, 500);
            this.btnFinishPrevious.Name = "btnFinishPrevious";
            this.btnFinishPrevious.Size = new System.Drawing.Size(88, 30);
            this.btnFinishPrevious.TabIndex = 11;
            this.btnFinishPrevious.Text = "Previous";
            this.btnFinishPrevious.Click += new System.EventHandler(this.btnFinishPrevious_Click);
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureEdit2.EditValue = global::WoodPlan5.Properties.Resources.wizard_finish1;
            this.pictureEdit2.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit2.MenuManager = this.barManager1;
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.ShowMenu = false;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit2.Size = new System.Drawing.Size(205, 536);
            this.pictureEdit2.TabIndex = 5;
            // 
            // sp06058_OM_Client_Contract_Wizard_Client_ListTableAdapter
            // 
            this.sp06058_OM_Client_Contract_Wizard_Client_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp06059_OM_Client_Contract_EditTableAdapter
            // 
            this.sp06059_OM_Client_Contract_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp06060_OM_GC_Companies_With_BlankTableAdapter
            // 
            this.sp06060_OM_GC_Companies_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06061_OM_Sectors_With_BlankTableAdapter
            // 
            this.sp06061_OM_Sectors_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06062_OM_Contract_Statuses_With_BlankTableAdapter
            // 
            this.sp06062_OM_Contract_Statuses_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp06063_OM_Contract_Types_With_BlankTableAdapter
            // 
            this.sp06063_OM_Contract_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // sp06067_OM_Client_Contract_Year_EditTableAdapter
            // 
            this.sp06067_OM_Client_Contract_Year_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp06069_OM_Client_Contract_Year_Billing_Profile_EditTableAdapter
            // 
            this.sp06069_OM_Client_Contract_Year_Billing_Profile_EditTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(890, 333);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddProfile),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddProfileFromTemplate, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEditTemplates, true)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Custom 2";
            // 
            // bbiAddProfile
            // 
            this.bbiAddProfile.Caption = "Add";
            this.bbiAddProfile.Id = 37;
            this.bbiAddProfile.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Add_16x16;
            this.bbiAddProfile.Name = "bbiAddProfile";
            this.bbiAddProfile.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Add Billing Profile - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to add a new Billing Profile record.\r\n\r\n<b>Note:</b> This button is only" +
    " enabled when one or more Contract Years are selected.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiAddProfile.SuperTip = superToolTip1;
            this.bbiAddProfile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddProfile_ItemClick);
            // 
            // bbiAddProfileFromTemplate
            // 
            this.bbiAddProfileFromTemplate.Caption = "Add From Template";
            this.bbiAddProfileFromTemplate.Id = 38;
            this.bbiAddProfileFromTemplate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAddProfileFromTemplate.ImageOptions.Image")));
            this.bbiAddProfileFromTemplate.Name = "bbiAddProfileFromTemplate";
            this.bbiAddProfileFromTemplate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Add From Template - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to add one or more new Billing Profile records from a pre-defined templa" +
    "te.\r\n\r\n<b>Note:</b> This button is only enabled when one or more Contract Years " +
    "are selected.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiAddProfileFromTemplate.SuperTip = superToolTip2;
            this.bbiAddProfileFromTemplate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddProfileFromTemplate_ItemClick);
            // 
            // bbiEditTemplates
            // 
            this.bbiEditTemplates.Caption = "Template Manager";
            this.bbiEditTemplates.Id = 39;
            this.bbiEditTemplates.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiEditTemplates.ImageOptions.Image")));
            this.bbiEditTemplates.Name = "bbiEditTemplates";
            this.bbiEditTemplates.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Template Manager - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to open the Template Manager.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiEditTemplates.SuperTip = superToolTip3;
            this.bbiEditTemplates.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEditTemplates_ItemClick);
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter
            // 
            this.sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter.ClearBeforeFill = true;
            // 
            // bar2
            // 
            this.bar2.BarName = "Person Responsibilities";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar2.FloatLocation = new System.Drawing.Point(448, 269);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddPersonResponsibility)});
            this.bar2.OptionsBar.DisableClose = true;
            this.bar2.OptionsBar.DisableCustomization = true;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.StandaloneBarDockControl = this.standaloneBarDockControl2;
            this.bar2.Text = "Custom 3";
            // 
            // bbiAddPersonResponsibility
            // 
            this.bbiAddPersonResponsibility.Caption = "Add";
            this.bbiAddPersonResponsibility.Id = 40;
            this.bbiAddPersonResponsibility.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Add_16x16;
            this.bbiAddPersonResponsibility.Name = "bbiAddPersonResponsibility";
            this.bbiAddPersonResponsibility.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Add Person Responsibility - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to add a new Person Responsibility record.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiAddPersonResponsibility.SuperTip = superToolTip4;
            this.bbiAddPersonResponsibility.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddPersonResponsibility_ItemClick);
            // 
            // frm_OM_Client_Contract_Wizard
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(869, 537);
            this.Controls.Add(this.xtraTabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Client_Contract_Wizard";
            this.Text = "Client Contract Wizard - Operations";
            this.Activated += new System.EventHandler(this.frm_OM_Client_Contract_Wizard_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Client_Contract_Wizard_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Client_Contract_Wizard_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageWelcome.ResumeLayout(false);
            this.xtraTabPageWelcome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            this.xtraTabPageStep1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06058OMClientContractWizardClientListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            this.xtraTabPageStep2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ClientInstructionsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06059OMClientContractEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BillingCentreCodeButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BillingCentreCodeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinanceClientCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPreviousContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPreviousContractButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YearlyPercentageIncreaseSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SectorTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06061OMSectorsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastClientPaymentDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastClientPaymentDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckRPIDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckRPIDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractDirectorIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractDirectorButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractStatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06062OMContractStatusesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06063OMContractTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GCCompanyIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06060OMGCCompaniesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientContractIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPreviousContractID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBillingCentreCodeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractDirectorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCentre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCentreCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCentreID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGCCompanyID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCheckRPIDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinanceClientCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPreviousContract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractDirector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSectorTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForYearlyPercentageIncrease)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastClientPaymentDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBillingCentreCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientInstructions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            this.xtraTabPageStep3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06067OMClientContractYearEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit50CharsMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditContractYears.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditClientName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForYearsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITemForAddYearsToGridButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).EndInit();
            this.xtraTabPageStep4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06069OMClientContractYearBillingProfileEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).EndInit();
            this.xtraTabPageStep5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06081OMClientContractLinkedResponsibilitiesEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditResponsibilityType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.panelControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit9.Properties)).EndInit();
            this.xtraTabPageFinish.ResumeLayout(false);
            this.xtraTabPageFinish.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageWelcome;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep1;
        private DevExpress.XtraEditors.SimpleButton btnWelcomeNext;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageFinish;
        private DevExpress.XtraEditors.SimpleButton btnStep1Previous;
        private DevExpress.XtraEditors.SimpleButton bntStep1Next;
        private DevExpress.XtraEditors.SimpleButton btnStep2Next;
        private DevExpress.XtraEditors.SimpleButton btnStep2Previous;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.SimpleButton btnFinish;
        private DevExpress.XtraEditors.SimpleButton btnFinishPrevious;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private System.Windows.Forms.BindingSource sp06058OMClientContractWizardClientListBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06058_OM_Client_Contract_Wizard_Client_ListTableAdapter sp06058_OM_Client_Contract_Wizard_Client_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colClientType;
        private DevExpress.XtraGrid.Columns.GridColumn colActiveClient;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.GridLookUpEdit GCCompanyIDGridLookUpEdit;
        private System.Windows.Forms.BindingSource sp06059OMClientContractEditBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.TextEdit ClientIDTextEdit;
        private DevExpress.XtraEditors.TextEdit ClientContractIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientContractID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DataSet_OM_ContractTableAdapters.sp06059_OM_Client_Contract_EditTableAdapter sp06059_OM_Client_Contract_EditTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit ContractTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.GridLookUpEdit ContractStatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.ButtonEdit ContractDirectorButtonEdit;
        private DevExpress.XtraEditors.TextEdit ContractDirectorIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractDirectorID;
        private DevExpress.XtraEditors.DateEdit LastClientPaymentDateDateEdit;
        private DevExpress.XtraEditors.DateEdit CheckRPIDateDateEdit;
        private DevExpress.XtraEditors.DateEdit EndDateDateEdit;
        private DevExpress.XtraEditors.DateEdit StartDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCheckRPIDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLastClientPaymentDate;
        private DevExpress.XtraEditors.CheckEdit ActiveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActive;
        private DevExpress.XtraEditors.GridLookUpEdit SectorTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSectorTypeID;
        private DevExpress.XtraEditors.SpinEdit ContractValueSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractValue;
        private DevExpress.XtraEditors.SpinEdit YearlyPercentageIncreaseSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForYearlyPercentageIncrease;
        private DevExpress.XtraEditors.ButtonEdit LinkedToPreviousContractButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToPreviousContract;
        private DevExpress.XtraEditors.TextEdit LinkedToPreviousContractIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToPreviousContractID;
        private DevExpress.XtraEditors.TextEdit FinanceClientCodeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinanceClientCode;
        private DevExpress.XtraEditors.TextEdit BillingCentreCodeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBillingCentreCodeID;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGCCompanyID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractDirector;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractStatusID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private System.Windows.Forms.BindingSource sp06060OMGCCompaniesWithBlankBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06060_OM_GC_Companies_With_BlankTableAdapter sp06060_OM_GC_Companies_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private System.Windows.Forms.BindingSource sp06061OMSectorsWithBlankBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06061_OM_Sectors_With_BlankTableAdapter sp06061_OM_Sectors_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private System.Windows.Forms.BindingSource sp06062OMContractStatusesWithBlankBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06062_OM_Contract_Statuses_With_BlankTableAdapter sp06062_OM_Contract_Statuses_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private System.Windows.Forms.BindingSource sp06063OMContractTypesWithBlankBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06063_OM_Contract_Types_With_BlankTableAdapter sp06063_OM_Contract_Types_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit CostCentreTextEdit;
        private DevExpress.XtraEditors.TextEdit CostCentreCodeTextEdit;
        private DevExpress.XtraEditors.TextEdit CostCentreIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostCentre;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostCentreCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostCentreID;
        private DevExpress.XtraEditors.ButtonEdit BillingCentreCodeButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBillingCentreCode;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraEditors.TextEdit ClientNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep3;
        private DevExpress.XtraEditors.SimpleButton btnStep3Next;
        private DevExpress.XtraEditors.SimpleButton btnStep3Previous;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit7;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textEditEndDate;
        private DevExpress.XtraEditors.TextEdit textEditStartDate;
        private DevExpress.XtraEditors.TextEdit textEditClientName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.SpinEdit spinEditContractYears;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForYearsGrid;
        private DevExpress.XtraEditors.SimpleButton btnAddYearsBtn;
        private DevExpress.XtraLayout.LayoutControlItem ITemForAddYearsToGridButton;
        private System.Windows.Forms.BindingSource sp06067OMClientContractYearEditBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractYearID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colYearDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit50CharsMax;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DataSet_OM_ContractTableAdapters.sp06067_OM_Client_Contract_Year_EditTableAdapter sp06067_OM_Client_Contract_Year_EditTableAdapter;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep4;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.PictureEdit pictureEdit8;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep5;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.PictureEdit pictureEdit9;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.SimpleButton btnStep4Next;
        private DevExpress.XtraEditors.SimpleButton btnStep4Previous;
        private DevExpress.XtraEditors.SimpleButton btnStep5Next;
        private DevExpress.XtraEditors.SimpleButton btnStep5Previous;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private System.Windows.Forms.BindingSource sp06069OMClientContractYearBillingProfileEditBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractProfileID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractYearID1;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceDateDue;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceDateActual;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedBillAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colActualBillAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colBilledByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent;
        private DataSet_OM_ContractTableAdapters.sp06069_OM_Client_Contract_Year_Billing_Profile_EditTableAdapter sp06069_OM_Client_Contract_Year_Billing_Profile_EditTableAdapter;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiAddProfile;
        private DevExpress.XtraBars.BarButtonItem bbiAddProfileFromTemplate;
        private DevExpress.XtraBars.BarButtonItem bbiEditTemplates;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditCurrency;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private System.Windows.Forms.BindingSource sp06081OMClientContractLinkedResponsibilitiesEditBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonResponsibilityID;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibleForRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibleForRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibilityTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colContractEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent1;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibilityType;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffName;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DataSet_OM_ContractTableAdapters.sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl2;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiAddPersonResponsibility;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraEditors.TextEdit ContractDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractDescription;
        private DevExpress.XtraEditors.CheckEdit ReactiveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReactive;
        private DevExpress.XtraGrid.Columns.GridColumn colYearlyPercentageIncrease;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditPercentage;
        private DevExpress.XtraEditors.MemoEdit ClientInstructionsMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientInstructions;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonTypeID;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditResponsibilityType;
    }
}

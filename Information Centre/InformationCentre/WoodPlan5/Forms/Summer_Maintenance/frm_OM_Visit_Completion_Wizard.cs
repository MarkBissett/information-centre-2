﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;
using DevExpress.XtraScheduler;
using DevExpress.Utils.Animation;  // Required by Transition Effects //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;

namespace WoodPlan5
{
    public partial class frm_OM_Visit_Completion_Wizard : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        bool internalRowFocusing;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_TemplateManager = false;

        bool iBool_ContractChangesMade = false;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        public string i_str_PassedInVisitIDs = "";

        private string strSignaturePath = "";

        private int intDefaultVisitCategoryID = 0;

        private Utils.enmFocusedGrid _enmFocusedGrid = Utils.enmFocusedGrid.Visit;

        #endregion

        public frm_OM_Visit_Completion_Wizard()
        {
            InitializeComponent();
        }

        private void frm_OM_Visit_Completion_Wizard_Load(object sender, EventArgs e)
        {
            this.FormID = 408;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            // Get Form Permissions (For Edit Templates Button) //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, 7008, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);  // 7008 //
            ProcessPermissionsForForm();

            xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;  // Hide Tab Page headers so it doesn't look like a pageframe //
            xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
            //xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;  //  Jump past start page to Step 1 //
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strSignaturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_PictureFilesFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Client Signatures (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Client Signature Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            try
            {
                sp06413_OM_Visit_Completion_Wizard_Get_VisitsTableAdapter.Connection.ConnectionString = strConnectionString;

                sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06415_OM_Visit_Completion_Wizard_Visit_Statuses_With_Blank);

                sp06251_OM_Issue_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06251_OM_Issue_Types_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06251_OM_Issue_Types_With_Blank, 1);

                sp06267_OM_Extra_Work_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06267_OM_Extra_Work_Types_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06267_OM_Extra_Work_Types_With_Blank, 1);

                sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter.Fill(dataSet_OM_Job.sp06177_OM_Job_Duration_Descriptors_With_Blank, 1);

                sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06268_OM_Cancelled_Reasons_With_BlankTableAdapter.Fill(dataSet_OM_Job.sp06268_OM_Cancelled_Reasons_With_Blank, 1);

                sp06418_OM_Visit_Completion_Wizard_Job_StatusesTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06418_OM_Visit_Completion_Wizard_Job_StatusesTableAdapter.Fill(dataSet_OM_Job.sp06418_OM_Visit_Completion_Wizard_Job_Statuses, 1);

                sp06416_OM_Visit_Completion_Wizard_Get_JobsTableAdapter.Connection.ConnectionString = strConnectionString;

                sp06419_OM_Visit_Completion_Wizard_LabourTableAdapter.Connection.ConnectionString = strConnectionString;

                sp06420_OM_Visit_Completion_Wizard_Labour_TimeTableAdapter.Connection.ConnectionString = strConnectionString;
            }
            catch (Exception) { }
            emptyEditor = new RepositoryItem();
            ibool_FormStillLoading = false;

            gridControl2.ForceInitialize();
            Load_Visits();
            CheckRows((GridView)gridControl2.MainView);
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            ConfigureFormAccordingToMode();
            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            switch (strFormMode)
            {
                case "add":
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    break;
                default:
                    break;
            }
            ibool_FormStillLoading = false;

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(xtraTabPageStep1.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
        }


        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        iBool_TemplateManager = sfpPermissions.blRead;
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            GridView view = null;
            GridView viewParent = null;
            int[] intRowHandles;
            int[] intParentRowHandles;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    view = (GridView)gridControl2.MainView;
                    intRowHandles = view.GetSelectedRows();
                    if (intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                    break;
                case Utils.enmFocusedGrid.Jobs:
                    view = (GridView)gridControl4.MainView;
                    intRowHandles = view.GetSelectedRows();
                    if (intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                    break;
                case Utils.enmFocusedGrid.LabourTiming:
                    viewParent = (GridView)gridControl8.MainView;
                    intParentRowHandles = viewParent.GetSelectedRows();
                    view = (GridView)gridControl9.MainView;
                    intRowHandles = view.GetSelectedRows();

                    if (intParentRowHandles.Length >= 1)
                    {
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                    if (intRowHandles.Length > 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                    if (intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                default:
                    break;
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            view = (GridView)gridControl2.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0);
            
            view = (GridView)gridControl4.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0);

            viewParent = (GridView)gridControl8.MainView;
            intParentRowHandles = viewParent.GetSelectedRows();
            view = (GridView)gridControl9.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl9.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            gridControl9.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            gridControl9.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            gridControl9.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            //bbiAddLabourFromContract.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            //bbiAddLabourFromContractAll.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
        }

        private void frm_OM_Visit_Completion_Wizard_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

            }
        }

        private void frm_OM_Visit_Completion_Wizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                //default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CompanyFilter", i_str_selected_Company_ids);
                //default_screen_settings.SaveDefaultScreenSettings();
            }
        }


        private void xtraTabControl1_SelectedPageChanging(object sender, DevExpress.XtraTab.TabPageChangingEventArgs e)
        {
            // Remember to set each pages Tag property to the page number in the designer //
            if (Convert.ToInt32(e.Page.Tag) > Convert.ToInt32(e.PrevPage.Tag))
            {
                ((PushTransition)transitionManager1.Transitions[xtraTabControl1].TransitionType).Parameters.EffectOptions = DevExpress.Utils.Animation.PushEffectOptions.FromRight;
            }
            else
            {
                ((PushTransition)transitionManager1.Transitions[xtraTabControl1].TransitionType).Parameters.EffectOptions = DevExpress.Utils.Animation.PushEffectOptions.FromLeft;
            }
            transitionManager1.StartTransition(xtraTabControl1);
        }
        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            // Remember to set each pages Tag property to the page number in the designer //
            transitionManager1.EndTransition();
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView2":
                    message = "No Visits Available - Adjust any in-place filters";
                    break;
                case "gridView4":
                    message = "No Jobs Available - Adjust any in-place filters";
                    break;
                case "gridView8":
                    message = "No Job Labour - Adjust any filters";
                    break;
                case "gridView9":
                    message = "No Labour Timings Available - Select Labour from the Labour grid then click the required Add button";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView2":
                    {
                        int[] intRowHandles = view.GetSelectedRows();
                        int intCount = intRowHandles.Length;
                        labelControlSelectedVisitCount.Text = intCount.ToString() + " Selected " + (intCount == 1 ? "Visit" : "Visits");
                        break;
                    }
                case "gridView4":
                    {
                        int[] intRowHandles = view.GetSelectedRows();
                        int intCount = intRowHandles.Length;
                        labelControlSelectedJobCount.Text = intCount.ToString() + " Selected " + (intCount == 1 ? "Job" : "Jobs");
                        break;
                    }
                case "gridView8":
                    {
                        int[] intRowHandles = view.GetSelectedRows();
                        int intCount = intRowHandles.Length;
                        labelControlSelectedLabourCount.Text = intCount.ToString() + " Selected " + (intCount == 1 ? "Labour Record" : "Labour Records");

                        Filter_Time_On_Labour();
                        view = (GridView)gridControl9.MainView;
                        view.ExpandAllGroups();
                        break;
                    }
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region Welcome Page

        private void btnWelcomeNext_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;
        }

        #endregion


        #region Step 1 Page

        private void btnStep1Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
        }
        private void btnStep1Next_Click(object sender, EventArgs e)
        {
            MoveToPage2();
        }

        private void MoveToPage2()
        {
            GridView view = (GridView)gridControl2.MainView;
            view.PostEditor();
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //         

            int intErrors = CheckRows(view);
            if (intErrors > 0)
            {
                string strMessage = (intErrors == 1 ? "1 Row" : intErrors.ToString() + " Rows") + " with errors - correct the errors before proceeding.\n\nColumns with invalid values have warning icons in them.";
                XtraMessageBox.Show(strMessage, "Check Data", MessageBoxButtons.OK, MessageBoxIcon.Warning, DefaultBoolean.True);
                return;
            }

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Processing Data...");

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < view.DataRowCount; i++)
            {
                sb.Append(view.GetRowCellValue(i, "VisitID").ToString() + ",");
            }

            GridView viewJobs = (GridView)gridControl4.MainView;
            viewJobs.BeginUpdate();
            try
            {
                if (dataSet_OM_Job.sp06416_OM_Visit_Completion_Wizard_Get_Jobs.Rows.Count == 0)
                {
                    dataSet_OM_Job.sp06416_OM_Visit_Completion_Wizard_Get_Jobs.Rows.Clear();
                    sp06416_OM_Visit_Completion_Wizard_Get_JobsTableAdapter.Fill(dataSet_OM_Job.sp06416_OM_Visit_Completion_Wizard_Get_Jobs, sb.ToString());
                    viewJobs.ExpandAllGroups();
                    CheckRows2((GridView)gridControl4.MainView);
                }
            }
            catch (Exception) { }
            viewJobs.EndUpdate();

            GridView viewLabour = (GridView)gridControl8.MainView;
            GridView viewLabourTiming = (GridView)gridControl9.MainView;
            viewLabour.BeginUpdate();
            viewLabourTiming.BeginUpdate();
            try
            {
                if (dataSet_OM_Job.sp06419_OM_Visit_Completion_Wizard_Labour.Rows.Count == 0)
                {
                    dataSet_OM_Job.sp06419_OM_Visit_Completion_Wizard_Labour.Rows.Clear();
                    sp06419_OM_Visit_Completion_Wizard_LabourTableAdapter.Fill(dataSet_OM_Job.sp06419_OM_Visit_Completion_Wizard_Labour, sb.ToString());
                    viewLabour.ExpandAllGroups();

                    dataSet_OM_Job.sp06420_OM_Visit_Completion_Wizard_Labour_Time.Rows.Clear();
                    sp06420_OM_Visit_Completion_Wizard_Labour_TimeTableAdapter.Fill(dataSet_OM_Job.sp06420_OM_Visit_Completion_Wizard_Labour_Time, sb.ToString());
                    viewLabourTiming.ExpandAllGroups();

                    viewLabour.SelectAll();  // Make sure filter shows all rows //
                    CheckRows3((GridView)gridControl9.MainView);
                }
            }
            catch (Exception) { }
            viewLabour.EndUpdate();
            viewLabourTiming.EndUpdate();


            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }

            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep2;
        }

        private void Load_Visits()
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Visits...");
            }

            GridView view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            try
            {
                sp06413_OM_Visit_Completion_Wizard_Get_VisitsTableAdapter.Fill(dataSet_OM_Visit.sp06413_OM_Visit_Completion_Wizard_Get_Visits, i_str_PassedInVisitIDs);
            }
            catch (Exception) { }
            view.ExpandAllGroups();
            view.EndUpdate();
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }


        #region GridView2

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("block edit".Equals(e.Button.Tag))
                    {
                        Block_Edit_Record();
                    }
                    break;
                default:
                    break;
            }

        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Visit;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                
                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;
                
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView2_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intFocusedRow = view.FocusedRowHandle;
            DataRow dr = view.GetDataRow(intFocusedRow);
            if (dr == null) return;
            switch (view.FocusedColumn.Name)
            {
                case "colStartDate":
                case "colEndDate":
                    {
                        bool boolValidFromDate = true;
                        bool boolValidToDate = true;
                        DateTime? dtFromDate = null;
                        DateTime? dtToDate = null;
                        if (view.FocusedColumn.Name == "colStartDate")
                        {
                            if (e.Value != DBNull.Value) dtFromDate = Convert.ToDateTime(e.Value);
                            if (view.GetFocusedRowCellValue("EndDate") != DBNull.Value) dtToDate = Convert.ToDateTime(view.GetFocusedRowCellValue("EndDate"));
                        }
                        else
                        {
                            if (e.Value != DBNull.Value) dtToDate = Convert.ToDateTime(e.Value);
                            if (view.GetFocusedRowCellValue("StartDate") != DBNull.Value) dtFromDate = Convert.ToDateTime(view.GetFocusedRowCellValue("StartDate"));
                        }
                        if (dtFromDate == null || dtFromDate == DateTime.MinValue)
                        {
                            dr.SetColumnError("StartDate", String.Format("{0} requires a value.", view.Columns["StartDate"].Caption));
                            boolValidFromDate = false;
                        }
                        if (dtToDate == null || dtToDate == DateTime.MinValue)
                        {
                            dr.SetColumnError("EndDate", String.Format("{0} requires a value.", view.Columns["EndDate"].Caption));
                            boolValidToDate = false;
                        }
                        if (dtFromDate != null && dtToDate != null && dtFromDate > dtToDate)
                        {
                            dr.SetColumnError("StartDate", String.Format("{0} should be less than {1}.", view.Columns["StartDate"].Caption, view.Columns["EndDate"].Caption));
                            boolValidFromDate = false;

                            dr.SetColumnError("EndDate", String.Format("{0} should be greater than {1}.", view.Columns["EndDate"].Caption, view.Columns["StartDate"].Caption));
                            boolValidToDate = false;
                        }
                        if (boolValidFromDate) dr.SetColumnError("StartDate", "");  // Clear Error Text //
                        if (boolValidToDate) dr.SetColumnError("EndDate", "");  // Clear Error Text //
                        //if (!boolValidFromDate || !boolValidToDate) dr.RowError = "Error(s) Present - correct before proceeding.";
                    }
                    break;
                case "colVisitStatusID":
                    {
                        bool boolValidVisitStatusID = true;
                        int? intVisitStatusID = null;
                        if (e.Value != DBNull.Value) intVisitStatusID = Convert.ToInt32(e.Value);
                        if (intVisitStatusID == null || intVisitStatusID <= 0)
                        {
                            dr.SetColumnError("VisitStatusID", String.Format("{0} requires a value.", view.Columns["VisitStatusID"].Caption));
                            boolValidVisitStatusID = false;
                            //dr.RowError = "Error(s) Present - correct before proceeding.";
                        }
                        if (boolValidVisitStatusID) dr.SetColumnError("VisitStatusID", "");  // Clear Error Text //
                    }
                    break;
                case "colNoOnetoSign":
                case "colClientSignaturePath":
                    {
                        bool boolValidSigned = true;
                        int? intNoOnetoSign = null;
                        string strClientSignaturePath = null;
                        if (view.FocusedColumn.Name == "colNoOnetoSign")
                        {
                            if (e.Value != DBNull.Value) intNoOnetoSign = Convert.ToInt32(e.Value);
                            if (view.GetFocusedRowCellValue("ClientSignaturePath") != DBNull.Value) strClientSignaturePath = view.GetFocusedRowCellValue("ClientSignaturePath").ToString();
                        }
                        else
                        {
                            if (e.Value != DBNull.Value) strClientSignaturePath = e.Value.ToString();
                            if (view.GetFocusedRowCellValue("NoOnetoSign") != DBNull.Value) intNoOnetoSign = Convert.ToInt32(view.GetFocusedRowCellValue("NoOnetoSign"));
                        }
                        if ((intNoOnetoSign == null || intNoOnetoSign <= 0) && string.IsNullOrWhiteSpace(strClientSignaturePath))
                        {
                            dr.SetColumnError("NoOnetoSign", String.Format("{0} should be ticked or a signature file should be selected for {1}.", view.Columns["NoOnetoSign"].Caption, view.Columns["ClientSignaturePath"].Caption));
                            dr.SetColumnError("ClientSignaturePath", String.Format("{0} should have a signature file selected or {1} should be ticked.", view.Columns["ClientSignaturePath"].Caption, view.Columns["NoOnetoSign"].Caption));
                            boolValidSigned = false;
                            //dr.RowError = "Error(s) Present - correct before proceeding.";
                        }
                        else if ((intNoOnetoSign != null && intNoOnetoSign > 0) && !string.IsNullOrWhiteSpace(strClientSignaturePath))
                        {
                            dr.SetColumnError("NoOnetoSign", String.Format("Either {0} should be ticked OR a signature file should be selected for {1}.", view.Columns["NoOnetoSign"].Caption, view.Columns["ClientSignaturePath"].Caption));
                            dr.SetColumnError("ClientSignaturePath", String.Format("Edither {0} should have a signature file selected OR {1} should be ticked.", view.Columns["ClientSignaturePath"].Caption, view.Columns["NoOnetoSign"].Caption));
                            boolValidSigned = false;
                            //dr.RowError = "Error(s) Present - correct before proceeding.";
                        }
                        else
                        {
                            dr.SetColumnError("NoOnetoSign", "");
                            dr.SetColumnError("ClientSignaturePath", "");
                        }
                    }
                    break;
            }
            //if (!dr.HasErrors) dr.RowError = "";
       }

        private void repositoryItemButtonEditChoosePath_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intFocusedRow = view.FocusedRowHandle;

            string strClientPath = view.GetFocusedRowCellValue("ImagesFolderOM").ToString();
            string strImagesFolderOM = strClientPath + "\\Signatures";
            string strFirstPartOfPathWithBackslash = strSignaturePath + "\\";

            switch (e.Button.Tag.ToString())
            {
                case "choose":
                    {
                        using (OpenFileDialog dlg = new OpenFileDialog())
                        {
                            dlg.DefaultExt = "tab";
                            dlg.Filter = "";
                            if (strSignaturePath != "") dlg.InitialDirectory = strFirstPartOfPathWithBackslash + strImagesFolderOM;
                            dlg.Multiselect = false;
                            dlg.ShowDialog();
                            if (dlg.FileNames.Length > 0)
                            {
                                string strTempFileName = "";
                                string strTempResult = "";
                                //string strExtension = "";
                                foreach (string filename in dlg.FileNames)
                                {
                                    if (strFirstPartOfPathWithBackslash != "")
                                    {
                                        if (!filename.ToLower().StartsWith(strFirstPartOfPathWithBackslash.ToLower() + strImagesFolderOM.ToLower()))
                                        {
                                            DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files from under the default folder [as specified in the System Configuration Screen - Default Client Signatures Folder] or one of it's sub-folders.\n\nFile Selection Aborted!", "Select File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            break;
                                        }
                                    }
                                    strTempFileName = filename.Substring(strFirstPartOfPathWithBackslash.Length + strImagesFolderOM.Length);
                                    if (strTempResult == "")
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += strTempFileName;
                                    }
                                    else
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += "\r\n" + strTempFileName;
                                    }
                                }
                                try
                                {
                                    view.SetFocusedRowCellValue("ClientSignaturePath", strTempResult);
                                }
                                catch (Exception) { }

                                if (!string.IsNullOrWhiteSpace(strTempResult))
                                {
                                    // Clear No One To Sign //
                                    try
                                    {
                                        view.SetFocusedRowCellValue("NoOnetoSign", 0); 
                                    }
                                    catch (Exception) { }
                                }
                                DataRow dr = view.GetDataRow(intFocusedRow);
                                dr.SetColumnError("NoOnetoSign", "");
                                dr.SetColumnError("ClientSignaturePath", "");
                            }
                        }
                    }
                    break;
                case "view":
                    {
                        string strFile = view.GetFocusedRowCellValue("ClientSignaturePath").ToString();

                        if (string.IsNullOrEmpty(strFile))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Link the Client Signature to be Viewed before proceeding.", "View Linked Client Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        try
                        {
                            System.Diagnostics.Process.Start(Path.Combine(strFirstPartOfPathWithBackslash + strImagesFolderOM + strFile));
                        }
                        catch
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the Linked Client Signature: " + strFile + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Client Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
            }
        }

        private void repositoryItemCheckEdit1_EditValueChanged(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intFocusedRow = view.FocusedRowHandle;
            DataRow dr = view.GetDataRow(intFocusedRow);
            if (dr == null) return;
            
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                view.SetFocusedRowCellValue("ClientSignaturePath", "");
                view.SetFocusedRowCellValue("ManagerName", "");
            }
        }

        #endregion


        private int CheckRows(GridView view)
        {
           int intInvalidRowCount = 0;
           for (int i = 0; i < view.DataRowCount; i++)
           {
               intInvalidRowCount += CheckRow(view, i);
           }
           return intInvalidRowCount;
        }
        private int CheckRow(GridView view, int RowHandle)
        {
           try
           {
               DataRow dr = view.GetDataRow(RowHandle);
               DateTime? dtFromDate = null;
               DateTime? dtToDate = null;
               int? intVisitStatusID = null;
               int? intNoOnetoSign = null;
               string strClientSignaturePath = null;

               bool boolValidFromDate = true;
               bool boolValidToDate = true;
               bool boolValidVisitStatusID = true;
               bool boolValidSigned = true;

               if (view.GetRowCellValue(RowHandle, "StartDate") != DBNull.Value) dtFromDate = Convert.ToDateTime(view.GetRowCellValue(RowHandle, "StartDate"));
               if (view.GetRowCellValue(RowHandle, "EndDate") != DBNull.Value) dtToDate = Convert.ToDateTime(view.GetRowCellValue(RowHandle, "EndDate"));
               if (view.GetRowCellValue(RowHandle, "VisitStatusID") != DBNull.Value) intVisitStatusID = Convert.ToInt32(view.GetRowCellValue(RowHandle, "VisitStatusID"));
               if (view.GetRowCellValue(RowHandle, "NoOnetoSign") != DBNull.Value) intNoOnetoSign = Convert.ToInt32(view.GetRowCellValue(RowHandle, "NoOnetoSign"));
               if (view.GetRowCellValue(RowHandle, "ClientSignaturePath") != DBNull.Value) strClientSignaturePath = view.GetRowCellValue(RowHandle, "ClientSignaturePath").ToString();

               if (dtFromDate == null || dtFromDate == DateTime.MinValue)
               {
                   dr.SetColumnError("StartDate", String.Format("{0} requires a value.", view.Columns["StartDate"].Caption));
                   boolValidFromDate = false;
               }
               else
               {
                   dr.SetColumnError("StartDate", "");
               }
               if (dtToDate == null || dtToDate == DateTime.MinValue)
               {
                   dr.SetColumnError("EndDate", String.Format("{0} requires a value.", view.Columns["EndDate"].Caption));
                   boolValidToDate = false;
               }
               else
               {
                   dr.SetColumnError("EndDate", "");
               }
               if (dtFromDate != null && dtToDate != null && dtFromDate > dtToDate)
               {
                   dr.SetColumnError("StartDate", String.Format("{0} should be less than {1}.", view.Columns["StartDate"].Caption, view.Columns["EndDate"].Caption));
                   dr.SetColumnError("EndDate", String.Format("{0} should be greater than {1}.", view.Columns["EndDate"].Caption, view.Columns["StartDate"].Caption));
                   boolValidFromDate = false;
                   boolValidToDate = false;
               }
               else if (boolValidFromDate && boolValidToDate)
               {
                   dr.SetColumnError("StartDate", "");
                   dr.SetColumnError("EndDate", "");
               }

               if (intVisitStatusID == null || intVisitStatusID <= 0)
               {
                   dr.SetColumnError("VisitStatusID", String.Format("{0} requires a value.", view.Columns["VisitStatusID"].Caption));
                   boolValidVisitStatusID = false;
                   //dr.RowError = "Error(s) Present - correct before proceeding.";
               }
               else
               {
                   dr.SetColumnError("VisitStatusID", "");
               }

               if ((intNoOnetoSign == null || intNoOnetoSign <= 0) && string.IsNullOrWhiteSpace(strClientSignaturePath))
               {
                   dr.SetColumnError("NoOnetoSign", String.Format("{0} should be ticked or a signature file should be selected for {1}.", view.Columns["NoOnetoSign"].Caption, view.Columns["ClientSignaturePath"].Caption));
                   dr.SetColumnError("ClientSignaturePath", String.Format("{0} should have a signature file selected or {1} should be ticked.", view.Columns["ClientSignaturePath"].Caption, view.Columns["NoOnetoSign"].Caption));
                   boolValidSigned = false;
                   //dr.RowError = "Error(s) Present - correct before proceeding.";
               }
               else if ((intNoOnetoSign != null && intNoOnetoSign > 0) && !string.IsNullOrWhiteSpace(strClientSignaturePath))
               {
                   dr.SetColumnError("NoOnetoSign", String.Format("Either {0} should be ticked OR a signature file should be selected for {1}.", view.Columns["NoOnetoSign"].Caption, view.Columns["ClientSignaturePath"].Caption));
                   dr.SetColumnError("ClientSignaturePath", String.Format("Edither {0} should have a signature file selected OR {1} should be ticked.", view.Columns["ClientSignaturePath"].Caption, view.Columns["NoOnetoSign"].Caption));
                   boolValidSigned = false;
                   //dr.RowError = "Error(s) Present - correct before proceeding.";
               }
               else
               {
                   dr.SetColumnError("NoOnetoSign", "");
                   dr.SetColumnError("ClientSignaturePath", "");
               }

               //dr.RowError =  (!boolValidFromDate || !boolValidToDate || !boolValidVisitStatusID ? "Error(s) Present - correct before proceeding." : "");
               return (!boolValidFromDate || !boolValidToDate || !boolValidVisitStatusID || !boolValidSigned ? 1 : 0);
           }
           catch (Exception)
           {
           }
           return 0;
        }

        private void Block_Edit_Visits()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl2.MainView;
            int intRowCount = view.DataRowCount;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Visits to block edit then try again.", "Block Edit Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Visit_Completion_Wizard_Block_Edit_Visit();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strSignaturePath = this.strSignaturePath;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Updating...");

                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                view.BeginUpdate();
                view.BeginSort();
                try
                {
                    foreach (int intRowHandle in intRowHandles)
                    {
                        if (fChildForm.dtVisitStartDate != null) view.SetRowCellValue(intRowHandle, "StartDate", fChildForm.dtVisitStartDate);
                        if (fChildForm.dtVisitEndDate != null) view.SetRowCellValue(intRowHandle, "EndDate", fChildForm.dtVisitEndDate);
                        if (fChildForm.intVisitStatusID != null) view.SetRowCellValue(intRowHandle, "VisitStatusID", fChildForm.intVisitStatusID);
                        if (fChildForm.dStartLatitude != null) view.SetRowCellValue(intRowHandle, "StartLatitude", fChildForm.dStartLatitude);
                        if (fChildForm.dStartLongitude != null) view.SetRowCellValue(intRowHandle, "StartLongitude", fChildForm.dStartLongitude);
                        if (fChildForm.dFinishLatitude != null) view.SetRowCellValue(intRowHandle, "FinishLatitude", fChildForm.dFinishLatitude);
                        if (fChildForm.dFinishLongitude != null) view.SetRowCellValue(intRowHandle, "FinishLongitude", fChildForm.dFinishLongitude);
                        if (fChildForm.strClientSignaturePath != null) view.SetRowCellValue(intRowHandle, "ClientSignaturePath", fChildForm.strClientSignaturePath);
                        if (fChildForm.intNoOnetoSign != null) view.SetRowCellValue(intRowHandle, "NoOnetoSign", fChildForm.intNoOnetoSign);
                        if (fChildForm.strManagerName != null) view.SetRowCellValue(intRowHandle, "ManagerName", fChildForm.strManagerName);
                        if (fChildForm.strManagerNotes != null) view.SetRowCellValue(intRowHandle, "ManagerNotes", fChildForm.strManagerNotes);
                        if (fChildForm.intIssueFound != null) view.SetRowCellValue(intRowHandle, "IssueFound", fChildForm.intIssueFound);
                        if (fChildForm.intIssueTypeID != null) view.SetRowCellValue(intRowHandle, "IssueTypeID", fChildForm.intIssueTypeID);
                        if (fChildForm.intRework != null) view.SetRowCellValue(intRowHandle, "Rework", fChildForm.intRework);
                        if (fChildForm.intExtraWorkRequired != null) view.SetRowCellValue(intRowHandle, "ExtraWorkRequired", fChildForm.intExtraWorkRequired);
                        if (fChildForm.intExtraWorkTypeID != null) view.SetRowCellValue(intRowHandle, "ExtraWorkTypeID", fChildForm.intExtraWorkTypeID);
                        if (fChildForm.strExtraWorkComments != null) view.SetRowCellValue(intRowHandle, "ExtraWorkComments", fChildForm.strExtraWorkComments);
                        if (fChildForm.strCompletionSheetNumber != null) view.SetRowCellValue(intRowHandle, "CompletionSheetNumber", fChildForm.strCompletionSheetNumber);
                        if (fChildForm.strRemarks != null) view.SetRowCellValue(intRowHandle, "Remarks", fChildForm.strRemarks);
                        CheckRow(view, intRowHandle);
                    }
                }
                catch (Exception) { }
                view.EndSort();
                view.EndUpdate();
            }
            if (splashScreenManager.IsSplashFormVisible)
            {
                splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager.CloseWaitForm();
            }
        }

        #endregion


        #region Step 2 Page

        private void btnStep2Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;
        }
        private void btnStep2Next_Click(object sender, EventArgs e)
        {
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Processing Data...");

            GridView view = (GridView)gridControl4.MainView;
            view.PostEditor();
            GridView viewSites = (GridView)gridControl2.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //         

            int intErrors = CheckRows2(view);
            if (intErrors > 0)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                string strMessage = (intErrors == 1 ? "1 Row" : intErrors.ToString() + " Rows") + " with errors - correct the errors before proceeding.\n\nColumns with invalid values have warning icons in them.";
                XtraMessageBox.Show(strMessage, "Check Data", MessageBoxButtons.OK, MessageBoxIcon.Warning, DefaultBoolean.True);
                return;
            }
            try
            {
                int intFoundRow = 0;
                int intSiteContractID = 0;
                var strFilterClause = "";
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < viewSites.DataRowCount; i++)
                {
                    sb.Clear();
                    intSiteContractID = Convert.ToInt32(viewSites.GetRowCellValue(i, "SiteContractID"));

                    strFilterClause = "SiteContractID = " + intSiteContractID.ToString();
                    DataRow[] filteredRows_Visits = dataSet_OM_Contract.sp06409_OM_Site_Contract_Wizard_On_Hold_Visits.Select(strFilterClause);
                    foreach (DataRow dr_Visit in filteredRows_Visits)
                    {
                        intFoundRow = view.LocateByValue(0, view.Columns["VisitID"], Convert.ToInt32(dr_Visit["VisitID"]));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            if (Convert.ToInt32(view.GetRowCellValue(intFoundRow, "CheckMarkSelection")) == 1) sb.Append(dr_Visit["VisitID"].ToString() + ",");
                        }
                    }
                    viewSites.SetRowCellValue(i, "VisitIDs", sb.ToString());
                }
                sp06413OMVisitCompletionWizardGetVisitsBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            }
            catch (Exception Ex) { }

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }

            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep3;
        }


        #region GridView4

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("block edit".Equals(e.Button.Tag))
                    {
                        Block_Edit_Record();
                    }
                    break;
                default:
                    break;
            }

        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bsiRecordTicking.Enabled = false; // (intCount > 0);
                bbiTick.Enabled = false; // (intCount > 0);
                bbiUntick.Enabled = false; // (intCount > 0);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView4_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intFocusedRow = view.FocusedRowHandle;
            DataRow dr = view.GetDataRow(intFocusedRow);
            if (dr == null) return;
            switch (view.FocusedColumn.Name)
            {
                case "colActualStartDate":
                case "colActualEndDate":
                    {
                        int intJobStatusID = 0;
                        if (view.GetFocusedRowCellValue("JobStatusID") != DBNull.Value) intJobStatusID = Convert.ToInt32(view.GetFocusedRowCellValue("JobStatusID"));
                        if (intJobStatusID == 90 || intJobStatusID == 100) break;  // Job is aborted or cancelled so duration not relevent //

                        bool boolValidFromDate = true;
                        bool boolValidToDate = true;
                        DateTime? dtFromDate = null;
                        DateTime? dtToDate = null;
                        if (view.FocusedColumn.Name == "colActualStartDate")
                        {
                            if (e.Value != DBNull.Value) dtFromDate = Convert.ToDateTime(e.Value);
                            if (view.GetFocusedRowCellValue("ActualEndDate") != DBNull.Value) dtToDate = Convert.ToDateTime(view.GetFocusedRowCellValue("ActualEndDate"));
                        }
                        else
                        {
                            if (e.Value != DBNull.Value) dtToDate = Convert.ToDateTime(e.Value);
                            if (view.GetFocusedRowCellValue("ActualStartDate") != DBNull.Value) dtFromDate = Convert.ToDateTime(view.GetFocusedRowCellValue("ActualStartDate"));
                        }
                        if (dtFromDate == null || dtFromDate == DateTime.MinValue)
                        {
                            dr.SetColumnError("ActualEndDate", String.Format("{0} requires a value.", view.Columns["ActualEndDate"].Caption));
                            boolValidFromDate = false;
                        }
                        if (dtToDate == null || dtToDate == DateTime.MinValue)
                        {
                            dr.SetColumnError("ActualEndDate", String.Format("{0} requires a value.", view.Columns["ActualEndDate"].Caption));
                            boolValidToDate = false;
                        }
                        if (dtFromDate != null && dtToDate != null && dtFromDate > dtToDate)
                        {
                            dr.SetColumnError("ActualStartDate", String.Format("{0} should be less than {1}.", view.Columns["ActualStartDate"].Caption, view.Columns["ActualEndDate"].Caption));
                            boolValidFromDate = false;

                            dr.SetColumnError("ActualEndDate", String.Format("{0} should be greater than {1}.", view.Columns["ActualEndDate"].Caption, view.Columns["ActualStartDate"].Caption));
                            boolValidToDate = false;
                        }
                        if (boolValidFromDate) dr.SetColumnError("ActualStartDate", "");  // Clear Error Text //
                        if (boolValidToDate) dr.SetColumnError("ActualEndDate", "");  // Clear Error Text //
                        //if (!boolValidFromDate || !boolValidToDate) dr.RowError = "Error(s) Present - correct before proceeding.";
                    }
                    break;
                case "colActualDurationUnits":
                    {
                        int intJobStatusID = 0;
                        if (view.GetFocusedRowCellValue("JobStatusID") != DBNull.Value) intJobStatusID = Convert.ToInt32(view.GetFocusedRowCellValue("JobStatusID"));
                        if (intJobStatusID == 90 || intJobStatusID == 100) break;  // Job is aborted or cancelled so duration not relevent //
                        
                        bool boolValidActualDurationUnits = true;
                        decimal? decActualDurationUnits = null;
                        if (e.Value != DBNull.Value) decActualDurationUnits = Convert.ToDecimal(e.Value);
                        if (decActualDurationUnits == null || decActualDurationUnits <= (decimal)0.00)
                        {
                            dr.SetColumnError("ActualDurationUnits", String.Format("{0} requires a value.", view.Columns["ActualDurationUnits"].Caption));
                            boolValidActualDurationUnits = false;
                            //dr.RowError = "Error(s) Present - correct before proceeding.";
                        }
                        if (boolValidActualDurationUnits) dr.SetColumnError("ActualDurationUnits", "");  // Clear Error Text //
                    }
                    break;
                case "colActualDurationUnitsDescriptionID":
                    {
                        int intJobStatusID = 0;
                        if (view.GetFocusedRowCellValue("JobStatusID") != DBNull.Value) intJobStatusID = Convert.ToInt32(view.GetFocusedRowCellValue("JobStatusID"));
                        if (intJobStatusID == 90 || intJobStatusID == 100) break;  // Job is aborted or cancelled so duration not relevent //
                        
                        bool boolValidActualDurationUnitsDescriptionID = true;
                        int? intActualDurationUnitsDescriptionID = null;
                        if (e.Value != DBNull.Value) intActualDurationUnitsDescriptionID = Convert.ToInt32(e.Value);
                        if (intActualDurationUnitsDescriptionID == null || intActualDurationUnitsDescriptionID <= 0)
                        {
                            dr.SetColumnError("ActualDurationUnitsDescriptionID", String.Format("{0} requires a value.", view.Columns["ActualDurationUnitsDescriptionID"].Caption));
                            boolValidActualDurationUnitsDescriptionID = false;
                            //dr.RowError = "Error(s) Present - correct before proceeding.";
                        }
                        if (boolValidActualDurationUnitsDescriptionID) dr.SetColumnError("ActualDurationUnitsDescriptionID", "");  // Clear Error Text //
                    }
                    break;
                case "colJobStatusID":
                    {
                        bool boolValidjobStatusID = true;
                        int? intJobStatusID = null;
                        if (e.Value != DBNull.Value) intJobStatusID = Convert.ToInt32(e.Value);
                        if (intJobStatusID == null || intJobStatusID <= 0)
                        {
                            dr.SetColumnError("JobStatusID", String.Format("{0} requires a value.", view.Columns["JobStatusID"].Caption));
                            boolValidjobStatusID = false;
                            //dr.RowError = "Error(s) Present - correct before proceeding.";
                        }
                        else if (intJobStatusID == 90 || intJobStatusID == 100)  // Aborted or Cancelled - so clear start and end dates and duration //
                        {
                            dr.SetColumnError("ActualStartDate", "");
                            dr.SetColumnError("ActualEndDate", "");
                            dr.SetColumnError("ActualDurationUnits", "");
                            dr.SetColumnError("ActualDurationUnitsDescriptionID", "");

                        }
                        if (boolValidjobStatusID) dr.SetColumnError("JobStatusID", "");  // Clear Error Text //
                    }
                    break;
            }
            //if (!dr.HasErrors) dr.RowError = "";
        }

        private void repositoryItemDateEditStartDate_EditValueChanged(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl4.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intRow = view.FocusedRowHandle;

            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            DateEdit de = (DateEdit)sender;
            try { dtStart = de.DateTime; }
            catch (Exception) { }

            try { dtEnd = Convert.ToDateTime(view.GetFocusedRowCellValue("ActualEndDate")); }
            catch (Exception) { }

            try { decUnits = Convert.ToDecimal(view.GetFocusedRowCellValue("ActualDurationUnits")); }
            catch (Exception) { }

            try { intUnitDescriptorID = Convert.ToInt32(view.GetFocusedRowCellValue("ActualDurationUnitsDescriptionID")); }
            catch (Exception) { }

            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, "ActualStartDate");
        }

        private void repositoryItemDateEditEndDate_EditValueChanged(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl4.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intRow = view.FocusedRowHandle;

            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            try { dtStart = Convert.ToDateTime(view.GetFocusedRowCellValue("ActualStartDate")); }
            catch (Exception) { }

            DateEdit de = (DateEdit)sender;
            try { dtEnd = de.DateTime; }
            catch (Exception) { }

            try { decUnits = Convert.ToDecimal(view.GetFocusedRowCellValue("ActualDurationUnits")); }
            catch (Exception) { }

            try { intUnitDescriptorID = Convert.ToInt32(view.GetFocusedRowCellValue("ActualDurationUnitsDescriptionID")); }
            catch (Exception) { }

            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, "ActualEndDate");
        }

        private void repositoryItemSpinEdit2_EditValueChanged(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl4.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intRow = view.FocusedRowHandle;

            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            try { dtStart = Convert.ToDateTime(view.GetFocusedRowCellValue("ActualStartDate")); }
            catch (Exception) { }

            try { dtEnd = Convert.ToDateTime(view.GetFocusedRowCellValue("ActualEndDate")); }
            catch (Exception) { }

            SpinEdit se = (SpinEdit)sender;
            try { decUnits = Convert.ToDecimal(se.Value); }
            catch (Exception) { }

            try { intUnitDescriptorID = Convert.ToInt32(view.GetFocusedRowCellValue("ActualDurationUnitsDescriptionID")); }
            catch (Exception) { }

            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, "ActualDurationUnits");
        }

        private void repositoryItemGridLookUpEditUnitDescriptor_EditValueChanged(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl4.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intRow = view.FocusedRowHandle;

            DateTime? dtStart = null;
            DateTime? dtEnd = null;
            decimal decUnits = (decimal)0.00;
            int intUnitDescriptorID = 0;

            try { dtStart = Convert.ToDateTime(view.GetFocusedRowCellValue("ActualStartDate")); }
            catch (Exception) { }

            try { dtEnd = Convert.ToDateTime(view.GetFocusedRowCellValue("ActualEndDate")); }
            catch (Exception) { }

            try { decUnits = Convert.ToDecimal(view.GetFocusedRowCellValue("ActualDurationUnits")); }
            catch (Exception) { }

            GridLookUpEdit glue = (GridLookUpEdit)sender;
            try { intUnitDescriptorID = Convert.ToInt32(glue.EditValue); }
            catch (Exception) { }

            CalculateDates(dtStart, decUnits, intUnitDescriptorID, dtEnd, "ActualDurationUnitsDescriptionID");
        }

        private void CalculateDates(DateTime? StartDate, decimal Units, int UnitDescriptorID, DateTime? EndDate, string ChangedColumnName)
        {
            GridView view = (GridView)gridControl4.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intRow = view.FocusedRowHandle;
            DataRow dr = view.GetDataRow(intRow);

            switch (ChangedColumnName)
            {
                case "ActualStartDate":
                    {
                        if (StartDate <= DateTime.MinValue) return;
                        if (Units > (decimal)0.00 && UnitDescriptorID > 0)  // Calculate End Date //
                        {
                            switch (UnitDescriptorID)
                            {
                                case 1:  // Hours //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddHours((double)Units);
                                    }
                                    break;
                                case 2:  // Days //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddDays((double)Units);
                                    }
                                    break;
                                case 3:  // Weeks //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddDays((double)Units * (double)7);
                                    }
                                    break;
                                case 4:  // Months //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddMonths((int)Units);
                                    }
                                    break;
                                case 5:  // Years //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddYears((int)Units);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            view.SetFocusedRowCellValue("ActualEndDate", (EndDate == null ? DateTime.MinValue : (DateTime)EndDate));
                            dr.SetColumnError("ActualEndDate", "");  // Clear Error Text //
                        }
                        else if (EndDate != null)  // Calculate Duration //
                        {
                            // Calculate the interval between the two dates.
                            DateDiff dateDiff = DateDiff.Difference((DateTime)StartDate, (DateTime)EndDate);

                            if (dateDiff.Years > 0 && (dateDiff.Months == 0 && dateDiff.Weeks == 0 && dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                Units = (decimal)dateDiff.Years;
                                UnitDescriptorID = 5;
                            }
                            else if (dateDiff.Months > 0 && (dateDiff.Weeks == 0 && dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                Units = (decimal)dateDiff.Months + ((decimal)dateDiff.Years * (decimal)12);
                                UnitDescriptorID = 4;
                            }
                            else if (dateDiff.Weeks > 0 && (dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalDays / (decimal)7;
                                UnitDescriptorID = 3;
                            }
                            else if (dateDiff.Days > 0 && (dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalDays;
                                UnitDescriptorID = 2;
                                if (Units % 7 == 0) // Check if remainder is 0 if yes then number is devisable for weeks so convert to weeks //
                                {
                                    Units = (decimal)ts.TotalDays / (decimal)7;
                                    UnitDescriptorID = 3;
                                }
                            }
                            else if (dateDiff.Hours > 0 && (dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalHours;
                                UnitDescriptorID = 1;
                            }
                            else if (dateDiff.Minutes > 0)
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalMinutes / (decimal)60;
                                UnitDescriptorID = 1;
                            }
                            view.SetFocusedRowCellValue("ActualDurationUnits", (Units <= (decimal)0.00 ? (decimal)0.00 : Units));
                            view.SetFocusedRowCellValue("ActualDurationUnitsDescriptionID", UnitDescriptorID);
                            dr.SetColumnError("ActualDurationUnits", "");  // Clear Error Text //
                            dr.SetColumnError("ActualDurationUnitsDescriptionID", "");  // Clear Error Text //
                        }
                    }
                    break;
                case "ActualEndDate":
                    {
                        if (EndDate <= DateTime.MinValue) return;
                        if (Units > (decimal)0.00 && UnitDescriptorID > 0)  // Calculate End Date //
                        {
                            switch (UnitDescriptorID)
                            {
                                case 1:  // Hours //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddHours((double)Units * (double)-1);
                                    }
                                    break;
                                case 2:  // Days //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddDays((double)Units * (double)-1);
                                    }
                                    break;
                                case 3:  // Weeks //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddDays(((double)Units * (double)7) * (double)-1);
                                    }
                                    break;
                                case 4:  // Months //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddMonths((int)Units * -1);
                                    }
                                    break;
                                case 5:  // Years //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddYears((int)Units * -1);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            view.SetFocusedRowCellValue("ActualStartDate", (StartDate == null ? DateTime.MinValue : (DateTime)StartDate));
                            dr.SetColumnError("ActualStartDate", "");  // Clear Error Text //
                        }
                        else if (StartDate != null)  // Calculate Duration //
                        {
                            // Calculate the interval between the two dates.
                            DateDiff dateDiff = DateDiff.Difference((DateTime)StartDate, (DateTime)EndDate);

                            if (dateDiff.Years > 0 && (dateDiff.Months == 0 && dateDiff.Weeks == 0 && dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                Units = (decimal)dateDiff.Years;
                                UnitDescriptorID = 5;
                            }
                            else if (dateDiff.Months > 0 && (dateDiff.Weeks == 0 && dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                Units = (decimal)dateDiff.Months + ((decimal)dateDiff.Years * (decimal)12);
                                UnitDescriptorID = 4;
                            }
                            else if (dateDiff.Weeks > 0 && (dateDiff.Days == 0 && dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalDays / (decimal)7;
                                UnitDescriptorID = 3;
                            }
                            else if (dateDiff.Days > 0 && (dateDiff.Hours == 0 && dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalDays;
                                UnitDescriptorID = 2;
                                if (Units % 7 == 0) // Check if remainder is 0 if yes then number is devisable for weeks so convert to weeks //
                                {
                                    Units = (decimal)ts.TotalDays / (decimal)7;
                                    UnitDescriptorID = 3;
                                }
                            }
                            else if (dateDiff.Hours > 0 && (dateDiff.Minutes == 0))
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalHours;
                                UnitDescriptorID = 1;
                            }
                            else if (dateDiff.Minutes > 0)
                            {
                                TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
                                Units = (decimal)ts.TotalMinutes / (decimal)60;
                                UnitDescriptorID = 1;
                            }
                            view.SetFocusedRowCellValue("ActualDurationUnits", (Units <= (decimal)0.00 ? (decimal)0.00 : Units));
                            view.SetFocusedRowCellValue("ActualDurationUnitsDescriptionID", UnitDescriptorID);
                            dr.SetColumnError("ActualDurationUnits", "");  // Clear Error Text //
                            dr.SetColumnError("ActualDurationUnitsDescriptionID", "");  // Clear Error Text //
                        }
                    }
                    break;
                case "ActualDurationUnits":
                case "ActualDurationUnitsDescriptionID":
                    {
                        if (Units > (decimal)0.00 && UnitDescriptorID > 0 && StartDate != null)  // Calculate End Date //
                        {
                            switch (UnitDescriptorID)
                            {
                                case 1:  // Hours //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddHours((double)Units);
                                    }
                                    break;
                                case 2:  // Days //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddDays((double)Units);
                                    }
                                    break;
                                case 3:  // Weeks //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddDays((double)Units * (double)7);
                                    }
                                    break;
                                case 4:  // Months //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddMonths((int)Units);
                                    }
                                    break;
                                case 5:  // Years //
                                    {
                                        EndDate = Convert.ToDateTime(StartDate).AddYears((int)Units);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            view.SetFocusedRowCellValue("ActualEndDate", (EndDate == null ? DateTime.MinValue : (DateTime)EndDate));
                            dr.SetColumnError("ActualEndDate", "");  // Clear Error Text //
                        }
                        else if (Units > (decimal)0.00 && UnitDescriptorID > 0 && EndDate != null)  // Calculate Start Date //
                        {
                            switch (UnitDescriptorID)
                            {
                                case 1:  // Hours //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddHours((double)Units * (double)-1);
                                    }
                                    break;
                                case 2:  // Days //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddDays((double)Units * (double)-1);
                                    }
                                    break;
                                case 3:  // Weeks //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddDays(((double)Units * (double)7) * (double)-1);
                                    }
                                    break;
                                case 4:  // Months //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddMonths((int)Units * -1);
                                    }
                                    break;
                                case 5:  // Years //
                                    {
                                        StartDate = Convert.ToDateTime(EndDate).AddYears((int)Units * -1);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            view.SetFocusedRowCellValue("ActualStartDate", (StartDate == null ? DateTime.MinValue : (DateTime)StartDate));
                            dr.SetColumnError("ActualStartDate", "");  // Clear Error Text //
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemGridLookUpEditJobStatusID_EditValueChanged(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl4.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intRow = view.FocusedRowHandle;
            DataRow dr = view.GetDataRow(intRow);

            try 
            {
                GridLookUpEdit glue = (GridLookUpEdit)sender;
                int intJobStatusID = Convert.ToInt32(glue.EditValue);
                if (intJobStatusID == 90 || intJobStatusID == 100)
                {
                    view.SetFocusedRowCellValue("ActualStartDate", null);
                    view.SetFocusedRowCellValue("ActualEndDate", null);
                    view.SetFocusedRowCellValue("ActualDurationUnits", (decimal)0.00);
                    view.SetFocusedRowCellValue("ActualDurationUnitsDescriptionID", 0);
                }


            }
            catch (Exception) { }
        }

        #endregion


        private int CheckRows2(GridView view)
        {
            int intInvalidRowCount = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                intInvalidRowCount += CheckRow2(view, i);
            }
            return intInvalidRowCount;
        }
        private int CheckRow2(GridView view, int RowHandle)
        {
            try
            {
                DataRow dr = view.GetDataRow(RowHandle);
                DateTime? dtFromDate = null;
                DateTime? dtToDate = null;
                decimal? decActualDurationUnits = null;
                int? intActualDurationUnitsDescriptionID = null;
                int? intJobStatusID = null;

                bool boolValidFromDate = true;
                bool boolValidToDate = true;
                bool boolValidActualDurationUnits = true;
                bool boolValidActualDurationUnitsDescriptionID = true;
                bool boolValidJobStatusID = true;

                if (view.GetRowCellValue(RowHandle, "ActualStartDate") != DBNull.Value) dtFromDate = Convert.ToDateTime(view.GetRowCellValue(RowHandle, "ActualStartDate"));
                if (view.GetRowCellValue(RowHandle, "ActualEndDate") != DBNull.Value) dtToDate = Convert.ToDateTime(view.GetRowCellValue(RowHandle, "ActualEndDate"));
                if (view.GetRowCellValue(RowHandle, "ActualDurationUnits") != DBNull.Value) decActualDurationUnits = Convert.ToDecimal(view.GetRowCellValue(RowHandle, "ActualDurationUnits"));
                if (view.GetRowCellValue(RowHandle, "ActualDurationUnitsDescriptionID") != DBNull.Value) intActualDurationUnitsDescriptionID = Convert.ToInt32(view.GetRowCellValue(RowHandle, "ActualDurationUnitsDescriptionID"));
                if (view.GetRowCellValue(RowHandle, "JobStatusID") != DBNull.Value) intJobStatusID = Convert.ToInt32(view.GetRowCellValue(RowHandle, "JobStatusID"));

                if (!(intJobStatusID == 90 || intJobStatusID == 100) && (dtFromDate == null || dtFromDate == DateTime.MinValue))
                {
                    dr.SetColumnError("ActualStartDate", String.Format("{0} requires a value.", view.Columns["ActualStartDate"].Caption));
                    boolValidFromDate = false;
                }
                else
                {
                    dr.SetColumnError("ActualStartDate", "");
                }
                if (!(intJobStatusID == 90 || intJobStatusID == 100) && (dtToDate == null || dtToDate == DateTime.MinValue))
                {
                    dr.SetColumnError("ActualEndDate", String.Format("{0} requires a value.", view.Columns["ActualEndDate"].Caption));
                    boolValidToDate = false;
                }
                else
                {
                    dr.SetColumnError("ActualEndDate", "");
                }
                if (dtFromDate != null && dtToDate != null && dtFromDate > dtToDate)
                {
                    dr.SetColumnError("ActualStartDate", String.Format("{0} should be less than {1}.", view.Columns["ActualStartDate"].Caption, view.Columns["ActualEndDate"].Caption));
                    dr.SetColumnError("ActualEndDate", String.Format("{0} should be greater than {1}.", view.Columns["ActualEndDate"].Caption, view.Columns["ActualStartDate"].Caption));
                    boolValidFromDate = false;
                    boolValidToDate = false;
                }
                else if (boolValidFromDate && boolValidToDate)
                {
                    dr.SetColumnError("ActualStartDate", "");
                    dr.SetColumnError("ActualEndDate", "");
                }
                if (!(intJobStatusID == 90 || intJobStatusID == 100) && (decActualDurationUnits == null || decActualDurationUnits <= (decimal)0.00))
                {
                    dr.SetColumnError("ActualDurationUnits", String.Format("{0} requires a value.", view.Columns["ActualDurationUnits"].Caption));
                    boolValidActualDurationUnits = false;
                    //dr.RowError = "Error(s) Present - correct before proceeding.";
                }
                else
                {
                    dr.SetColumnError("ActualDurationUnits", "");
                }
                if (!(intJobStatusID == 90 || intJobStatusID == 100) && (intActualDurationUnitsDescriptionID == null || intActualDurationUnitsDescriptionID <= 0))
                {
                    dr.SetColumnError("ActualDurationUnitsDescriptionID", String.Format("{0} requires a value.", view.Columns["ActualDurationUnitsDescriptionID"].Caption));
                    boolValidActualDurationUnitsDescriptionID = false;
                    //dr.RowError = "Error(s) Present - correct before proceeding.";
                }
                else
                {
                    dr.SetColumnError("ActualDurationUnitsDescriptionID", "");
                }

                if (intJobStatusID == null || intJobStatusID <= 0)
                {
                    dr.SetColumnError("JobStatusID", String.Format("{0} requires a value.", view.Columns["JobStatusID"].Caption));
                    boolValidJobStatusID = false;
                    //dr.RowError = "Error(s) Present - correct before proceeding.";
                }
                else
                {
                    dr.SetColumnError("JobStatusID", "");
                }

                //dr.RowError =  (!boolValidFromDate || !boolValidToDate || !boolValidVisitStatusID ? "Error(s) Present - correct before proceeding." : "");
                return (!boolValidFromDate || !boolValidToDate || !boolValidActualDurationUnits || !boolValidActualDurationUnitsDescriptionID || !boolValidJobStatusID ? 1 : 0);
            }
            catch (Exception)
            {
            }
            return 0;
        }

        private void Block_Edit_Jobs()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl4.MainView;
            int intRowCount = view.DataRowCount;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Jobs to block edit then try again.", "Block Edit Jobs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Visit_Completion_Wizard_Block_Edit_Job();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strSignaturePath = this.strSignaturePath;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Updating...");

                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                view.BeginUpdate();
                view.BeginSort();
                try
                {
                    int intJobStatusID = (fChildForm.intJobStatusID != null ? Convert.ToInt32(fChildForm.intJobStatusID) : 0); 
                    foreach (int intRowHandle in intRowHandles)
                    {
                        if (intJobStatusID == 90 || intJobStatusID == 100)  // Aborted or Cancelled //
                        {
                            view.SetRowCellValue(intRowHandle, "ActualStartDate", null);
                            view.SetRowCellValue(intRowHandle, "ActualEndDate", null);
                            view.SetRowCellValue(intRowHandle, "ActualDurationUnits", 0);
                            view.SetRowCellValue(intRowHandle, "ActualDurationUnitsDescriptionID", 0);
                        }
                        else
                        {
                            if (fChildForm.dtActualStartDate != null) view.SetRowCellValue(intRowHandle, "ActualStartDate", fChildForm.dtActualStartDate);
                            if (fChildForm.dtActualEndDate != null) view.SetRowCellValue(intRowHandle, "ActualEndDate", fChildForm.dtActualEndDate);
                            if (fChildForm.decActualDurationUnits != null) view.SetRowCellValue(intRowHandle, "ActualDurationUnits", fChildForm.decActualDurationUnits);
                            if (fChildForm.intActualDurationUnitsDescriptionID != null) view.SetRowCellValue(intRowHandle, "ActualDurationUnitsDescriptionID", fChildForm.intActualDurationUnitsDescriptionID);
                        }
                        if (fChildForm.intJobStatusID != null) view.SetRowCellValue(intRowHandle, "JobStatusID", fChildForm.intJobStatusID);
                        if (fChildForm.intCancelledReasonID != null) view.SetRowCellValue(intRowHandle, "CancelledReasonID", fChildForm.intCancelledReasonID);
                        if (fChildForm.dStartLatitude != null) view.SetRowCellValue(intRowHandle, "StartLatitude", fChildForm.dStartLatitude);
                        if (fChildForm.dStartLongitude != null) view.SetRowCellValue(intRowHandle, "StartLongitude", fChildForm.dStartLongitude);
                        if (fChildForm.dFinishLatitude != null) view.SetRowCellValue(intRowHandle, "FinishLatitude", fChildForm.dFinishLatitude);
                        if (fChildForm.dFinishLongitude != null) view.SetRowCellValue(intRowHandle, "FinishLongitude", fChildForm.dFinishLongitude);
                        if (fChildForm.strRemarks != null) view.SetRowCellValue(intRowHandle, "Remarks", fChildForm.strRemarks);
                        if (fChildForm.intJobNoLongerRequired != null) view.SetRowCellValue(intRowHandle, "JobNoLongerRequired", fChildForm.intJobNoLongerRequired);
                        if (fChildForm.intNoWorkRequired != null) view.SetRowCellValue(intRowHandle, "NoWorkRequired", fChildForm.intNoWorkRequired);
                        if (fChildForm.intManuallyCompleted != null) view.SetRowCellValue(intRowHandle, "ManuallyCompleted", fChildForm.intManuallyCompleted);
                        if (fChildForm.intDoNotPayContractor != null) view.SetRowCellValue(intRowHandle, "DoNotPayContractor", fChildForm.intDoNotPayContractor);
                        if (fChildForm.intDoNotInvoiceClient != null) view.SetRowCellValue(intRowHandle, "DoNotInvoiceClient", fChildForm.intDoNotInvoiceClient);
                        if (fChildForm.intRework != null) view.SetRowCellValue(intRowHandle, "Rework", fChildForm.intRework);
                        CheckRow2(view, intRowHandle);
                    }
                }
                catch (Exception) { }
                view.EndSort();
                view.EndUpdate();
            }
            if (splashScreenManager.IsSplashFormVisible)
            {
                splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager.CloseWaitForm();
            }
        }

        #endregion


        #region Step 3 Page

        private void btnStep3Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep2;
        }
        private void btnStep3Next_Click(object sender, EventArgs e)
        {
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Processing Data...");

            GridView view = (GridView)gridControl9.MainView;
            view.PostEditor();
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //         

            int intErrors = CheckRows3(view);
            GridView viewLabour = (GridView)gridControl8.MainView;
            viewLabour.SelectAll();
            if (intErrors > 0)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }               
                string strMessage = (intErrors == 1 ? "1 Row" : intErrors.ToString() + " Rows") + " with errors - correct the errors before proceeding.\n\nColumns with invalid values have warning icons in them.";
                XtraMessageBox.Show(strMessage, "Check Data", MessageBoxButtons.OK, MessageBoxIcon.Warning, DefaultBoolean.True);
                return;
            }

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }

            xtraTabControl1.SelectedTabPage = this.xtraTabPageFinish;
        }


        #region GridView8

        private void gridView8_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "LinkedTimingCount")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedTimingCount")) <= 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridView8_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Labour;
            SetMenuStatus();
        }

        private void gridView8_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Labour;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                
                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView9

        private void gridControl9_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Labour_Timing();
                    }
                    else if ("block add".Equals(e.Button.Tag))
                    {
                        Block_Add_Labour_Timing();
                    }
                    else if ("block edit".Equals(e.Button.Tag))
                    {
                        Block_Edit_Labour_Timing();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView9_GotFocus(object sender, EventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.LabourTiming;
            SetMenuStatus();
        }

        private void gridView9_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.LabourTiming;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bsiRecordTicking.Enabled = false;
                bbiTick.Enabled = false;
                bbiUntick.Enabled = false;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView9_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intFocusedRow = view.FocusedRowHandle;
            DataRow dr = view.GetDataRow(intFocusedRow);
            if (dr == null) return;
            switch (view.FocusedColumn.Name)
            {
                case "colStartDateTime":
                case "colEndDateTime":
                    {
                        bool boolValidFromDate = true;
                        bool boolValidToDate = true;
                        DateTime? dtFromDate = null;
                        DateTime? dtToDate = null;
                        if (view.FocusedColumn.Name == "colStartDateTime")
                        {
                            if (e.Value != DBNull.Value) dtFromDate = Convert.ToDateTime(e.Value);
                            if (view.GetFocusedRowCellValue("EndDateTime") != DBNull.Value) dtToDate = Convert.ToDateTime(view.GetFocusedRowCellValue("EndDateTime"));
                        }
                        else
                        {
                            if (e.Value != DBNull.Value) dtToDate = Convert.ToDateTime(e.Value);
                            if (view.GetFocusedRowCellValue("StartDateTime") != DBNull.Value) dtFromDate = Convert.ToDateTime(view.GetFocusedRowCellValue("StartDateTime"));
                        }
                        if (dtFromDate == null || dtFromDate == DateTime.MinValue)
                        {
                            dr.SetColumnError("EndDateTime", String.Format("{0} requires a value.", view.Columns["EndDateTime"].Caption));
                            boolValidFromDate = false;
                        }
                        if (dtToDate == null || dtToDate == DateTime.MinValue)
                        {
                            dr.SetColumnError("EndDateTime", String.Format("{0} requires a value.", view.Columns["EndDateTime"].Caption));
                            boolValidToDate = false;
                        }
                        if (dtFromDate != null && dtToDate != null && dtFromDate > dtToDate)
                        {
                            dr.SetColumnError("StartDateTime", String.Format("{0} should be less than {1}.", view.Columns["StartDateTime"].Caption, view.Columns["EndDateTime"].Caption));
                            boolValidFromDate = false;

                            dr.SetColumnError("EndDateTime", String.Format("{0} should be greater than {1}.", view.Columns["EndDateTime"].Caption, view.Columns["StartDateTime"].Caption));
                            boolValidToDate = false;
                        }
                        if (boolValidFromDate) dr.SetColumnError("StartDateTime", "");  // Clear Error Text //
                        if (boolValidToDate) dr.SetColumnError("EndDateTime", "");  // Clear Error Text //
                        //if (!boolValidFromDate || !boolValidToDate) dr.RowError = "Error(s) Present - correct before proceeding.";
                    }
                    break;
                case "colTeamMemberName":
                    {
                        bool boolValidTeamMemberName = true;
                        string strTeamMemberName = null;
                        if (e.Value != DBNull.Value) strTeamMemberName = e.Value.ToString();
                        if (string.IsNullOrWhiteSpace(strTeamMemberName))
                        {
                            dr.SetColumnError("TeamMemberName", String.Format("{0} requires a value.", view.Columns["TeamMemberName"].Caption));
                            boolValidTeamMemberName = false;
                            //dr.RowError = "Error(s) Present - correct before proceeding.";
                        }
                        if (boolValidTeamMemberName) dr.SetColumnError("TeamMemberName", "");  // Clear Error Text //
                    }
                    break;
            }
        }

        private void repositoryItemButtonEditChooseTeamMember_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            GridView view = (GridView)gridControl9.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intRow = view.FocusedRowHandle;
            DataRow dr = view.GetDataRow(intRow);

            var fChildForm = new frm_OM_Select_Team_Member();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "single";
            fChildForm.intOperationsTeamsOnly = 1;

            try { fChildForm._PassedInTeamFilter = (string.IsNullOrEmpty(dr["ContractorID"].ToString()) ? "" : dr["ContractorID"].ToString() + ","); }
            catch (Exception) { }

            try { fChildForm.intOriginalParentID = (string.IsNullOrEmpty(dr["ContractorID"].ToString()) ? 0 : Convert.ToInt32(dr["ContractorID"])); }
            catch (Exception) { }

            try { fChildForm.intOriginalChildID = (string.IsNullOrEmpty(dr["TeamMemberID"].ToString()) ? 0 : Convert.ToInt32(dr["TeamMemberID"])); }
            catch (Exception) { }

            try { fChildForm._PassedInLabourType = (string.IsNullOrEmpty(dr["LinkedToPersonTypeID"].ToString()) ? 0 : Convert.ToInt32(dr["LinkedToPersonTypeID"])); }
            catch (Exception) { }

            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
            {
                dr["ContractorID"] = fChildForm.intSelectedParentID;
                dr["TeamMemberID"] = fChildForm.intSelectedChildID;
                dr["ContractorName"] = fChildForm.strSelectedParentDescriptions;
                dr["TeamMemberName"] = fChildForm.strSelectedChildDescriptions;
                dr["LinkedToPersonTypeID"] = fChildForm.intSelectedLabourTypeID;
                sp06420OMVisitCompletionWizardLabourTimeBindingSource.EndEdit();
               
                dr.SetColumnError("TeamMemberName", "");
            }
        }

        private void repositoryItemDateEditStartTime_EditValueChanged(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl9.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;

            DateTime? dtStart = null;
            DateTime? dtEnd = null;

            DateEdit de = (DateEdit)sender;
            try { dtStart = de.DateTime; }
            catch (Exception) { }

            try { dtEnd = Convert.ToDateTime(view.GetFocusedRowCellValue("EndDateTime")); }
            catch (Exception) { }

            CalculateDuration(dtStart, dtEnd);

            DataRow dr = view.GetDataRow(view.FocusedRowHandle);
            dr.SetColumnError("StartDateTime", (dtStart == null || dtStart == DateTime.MinValue ? String.Format("{0} requires a value.", view.Columns["StartDateTime"].Caption) : ""));  // Clear Error Text //
        }

        private void repositoryItemDateEditEndTime_EditValueChanged(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl9.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;

            DateTime? dtStart = null;
            DateTime? dtEnd = null;

            DateEdit de = (DateEdit)sender;
            try { dtStart = Convert.ToDateTime(view.GetFocusedRowCellValue("StartDateTime")); }
            catch (Exception) { }

            try { dtEnd = de.DateTime; }
            catch (Exception) { }

            CalculateDuration(dtStart, dtEnd);

            DataRow dr = view.GetDataRow(view.FocusedRowHandle);
            dr.SetColumnError("EndDateTime", (dtStart == null || dtStart == DateTime.MinValue ? String.Format("{0} requires a value.", view.Columns["EndDateTime"].Caption) : ""));  // Clear Error Text //
        }

        private void CalculateDuration(DateTime? StartDate, DateTime? EndDate)
        {
            GridView view = (GridView)gridControl9.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intRow = view.FocusedRowHandle;
            DataRow dr = view.GetDataRow(intRow);

            if (StartDate <= DateTime.MinValue || StartDate == null || EndDate <= DateTime.MinValue || EndDate == null)
            {
                view.SetFocusedRowCellValue("Duration", (decimal)0.00);
                return;
            }
            // Calculate the interval between the two dates.
            TimeSpan ts = (DateTime)EndDate - (DateTime)StartDate;
            decimal Units = (decimal)ts.TotalHours;
            view.SetFocusedRowCellValue("Duration", (Units < (decimal)0.00 ? (decimal)0.00 : (decimal)Units));
            sp06420OMVisitCompletionWizardLabourTimeBindingSource.EndEdit();
        }

        #endregion


        private int CheckRows3(GridView view)
        {
            int intInvalidRowCount = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                intInvalidRowCount += CheckRow3(view, i);
            }
            return intInvalidRowCount;
        }
        private int CheckRow3(GridView view, int RowHandle)
        {
            try
            {
                DataRow dr = view.GetDataRow(RowHandle);
                DateTime? dtFromDate = null;
                DateTime? dtToDate = null;
                string strTeamMemberName = null;

                bool boolValidFromDate = true;
                bool boolValidToDate = true;
                bool boolValidTeamMemberName = true;

                if (view.GetRowCellValue(RowHandle, "StartDateTime") != DBNull.Value) dtFromDate = Convert.ToDateTime(view.GetRowCellValue(RowHandle, "StartDateTime"));
                if (view.GetRowCellValue(RowHandle, "EndDateTime") != DBNull.Value) dtToDate = Convert.ToDateTime(view.GetRowCellValue(RowHandle, "EndDateTime"));
                if (view.GetRowCellValue(RowHandle, "TeamMemberName") != DBNull.Value) strTeamMemberName = view.GetRowCellValue(RowHandle, "TeamMemberName").ToString();

                if (dtFromDate == null || dtFromDate == DateTime.MinValue)
                {
                    dr.SetColumnError("StartDateTime", String.Format("{0} requires a value.", view.Columns["StartDateTime"].Caption));
                    boolValidFromDate = false;
                }
                else
                {
                    dr.SetColumnError("StartDateTime", "");
                }
                if (dtToDate == null || dtToDate == DateTime.MinValue)
                {
                    dr.SetColumnError("EndDateTime", String.Format("{0} requires a value.", view.Columns["EndDateTime"].Caption));
                    boolValidToDate = false;
                }
                else
                {
                    dr.SetColumnError("EndDateTime", "");
                }
                if (dtFromDate != null && dtToDate != null && dtFromDate > dtToDate)
                {
                    dr.SetColumnError("StartDateTime", String.Format("{0} should be less than {1}.", view.Columns["StartDateTime"].Caption, view.Columns["EndDateTime"].Caption));
                    dr.SetColumnError("EndDateTime", String.Format("{0} should be greater than {1}.", view.Columns["EndDateTime"].Caption, view.Columns["StartDateTime"].Caption));
                    boolValidFromDate = false;
                    boolValidToDate = false;
                }
                else if (boolValidFromDate && boolValidToDate)
                {
                    dr.SetColumnError("StartDateTime", "");
                    dr.SetColumnError("EndDateTime", "");
                }

                if (string.IsNullOrWhiteSpace(strTeamMemberName))
                {
                    dr.SetColumnError("TeamMemberName", String.Format("{0} requires a value.", view.Columns["TeamMemberName"].Caption));
                    boolValidTeamMemberName = false;
                    //dr.RowError = "Error(s) Present - correct before proceeding.";
                }
                else
                {
                    dr.SetColumnError("TeamMemberName", "");
                }

                //dr.RowError =  (!boolValidFromDate || !boolValidToDate? "Error(s) Present - correct before proceeding." : "");
                return (!boolValidFromDate || !boolValidToDate || !boolValidTeamMemberName ? 1 : 0);
            }
            catch (Exception)
            {
            }
            return 0;
        }

        private void Filter_Time_On_Labour()
        {
            GridView view = (GridView)gridControl9.MainView;
            view.PostEditor();

            string strFilterCondition = "";
            GridView parentView = (GridView)gridControl8.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                strFilterCondition = "[LabourUsedID] = 9999999999";  // Don't show anything //
            }
            else
            {
                foreach (int i in intRowHandles)
                {
                    strFilterCondition += (string.IsNullOrWhiteSpace(strFilterCondition) ? "" : " or ");
                    strFilterCondition += "[LabourUsedID] = " + parentView.GetRowCellValue(i, "LabourUsedID").ToString();
                }
            }
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = strFilterCondition;
            view.MakeRowVisible(-1, true);
            view.ExpandAllGroups();
            view.EndUpdate();
            SetMenuStatus();
        }

        private void Clear_Filter_Time_On_Labour()
        {
            GridView view = (GridView)gridControl9.MainView;
            view.PostEditor();

            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
            SetMenuStatus();
        }

        private void Clear_Created_Time_On_Labour()
        {
            // Clear Any Jobs already created //
            GridView view = (GridView)gridControl9.MainView;
            view.BeginUpdate();
            try
            {
                dataSet_OM_Job.sp06420_OM_Visit_Completion_Wizard_Labour_Time.Rows.Clear();
            }
            catch (Exception) { }
            view.EndUpdate();
        }


        private void Add_Labour_Timing()
        {
            GridView viewLabour = (GridView)gridControl8.MainView;
            int[] intRowHandles = viewLabour.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Labour records to add the Labour Timings to before proceeding.", "Add Labour Timings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intRowHandles.Length > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + " Labour record(s) selected. If you proceed a new Labour Timing record will be created for each of these Labour records.\n\nProceed?", "Add Labour Timing", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }

            GridView view = (GridView)gridControl9.MainView;
            view.PostEditor();
            StringBuilder sbNewIDs = new StringBuilder();
            
            int intLabourUsedID = 0;
            int intContractorID = 0;
            int intJobID = 0;
            int intClientID = 0;
            int intSiteID = 0;
            string strSiteName = "";
            string strClientName = "";
            string strJobSubTypeDescription = "";
            string strJobTypeDescription = "";
            DateTime dtExpectedStartDate;
            int intVisitNumber = 0;
            string strFriendlyVisitNumber = "";
            string strFullDescription = "";
            string strContractorName = "";
            int intVisitID = 0;
            int intClientContractID = 0;
            int intSiteContractID = 0;
            int intJobTypeID = 0;
            int intJobSubTypeID = 0;
            string strContractDescription = "";
            int intLinkedToPersonTypeID = 0;
            string strLinkedToPersonType = "";
            string strJobTypeJobSubType = "";

            viewLabour.BeginUpdate();
            view.BeginUpdate();  // Lock view //
            foreach (int intRowHandle in intRowHandles)
            {
                intLabourUsedID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "LabourUsedID"));
                intContractorID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "ContractorID"));
                intJobID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "JobID"));
                intClientID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "ClientID"));
                intSiteID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "SiteID"));
                strSiteName = viewLabour.GetRowCellValue(intRowHandle, "SiteName").ToString();
                strClientName = viewLabour.GetRowCellValue(intRowHandle, "ClientName").ToString();
                strJobSubTypeDescription = viewLabour.GetRowCellValue(intRowHandle, "JobSubTypeDescription").ToString();
                strJobTypeDescription = viewLabour.GetRowCellValue(intRowHandle, "JobTypeDescription").ToString();
                dtExpectedStartDate = Convert.ToDateTime(viewLabour.GetRowCellValue(intRowHandle, "ExpectedStartDate"));
                intVisitNumber = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "VisitNumber"));
                strFriendlyVisitNumber = viewLabour.GetRowCellValue(intRowHandle, "FriendlyVisitNumber").ToString();
                strFullDescription = viewLabour.GetRowCellValue(intRowHandle, "FullDescription").ToString();
                strContractorName = viewLabour.GetRowCellValue(intRowHandle, "ContractorName").ToString();
                intVisitID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "VisitID"));
                intClientContractID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "ClientContractID"));
                intSiteContractID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "SiteContractID"));
                intJobTypeID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "JobTypeID"));
                intJobSubTypeID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                strContractDescription = viewLabour.GetRowCellValue(intRowHandle, "ContractDescription").ToString();
                intLinkedToPersonTypeID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "LinkedToPersonTypeID"));
                strLinkedToPersonType = viewLabour.GetRowCellValue(intRowHandle, "LinkedToPersonType").ToString();
                strJobTypeJobSubType = viewLabour.GetRowCellValue(intRowHandle, "JobTypeJobSubType").ToString();

                try
                {
                    DataRow drNewRow;
                    drNewRow = dataSet_OM_Job .sp06420_OM_Visit_Completion_Wizard_Labour_Time.NewRow();
                    drNewRow["strMode"] = "add";
                    drNewRow["strRecordIDs"] = "";
                    drNewRow["LabourUsedID"] = intLabourUsedID;
                    drNewRow["TeamMemberID"] = 0;
                    //drNewRow["StartDateTime"] = null;
                    //drNewRow["EndDateTime"] = null;
                    drNewRow["Duration"] = (decimal)0.00;
                    drNewRow["Remarks"] = null;
                    drNewRow["PdaCreatedID"] = null;
                    drNewRow["TeamMemberName"] = "";
                    drNewRow["ContractorID"] = intContractorID;
                    drNewRow["JobID"] = intJobID;
                    drNewRow["ClientID"] = intClientID;
                    drNewRow["SiteID"] = intSiteID;
                    drNewRow["SiteName"] = strSiteName;
                    drNewRow["ClientName"] = strClientName;
                    drNewRow["JobSubTypeDescription"] = strJobSubTypeDescription;
                    drNewRow["JobTypeDescription"] = strJobTypeDescription;
                    drNewRow["ExpectedStartDate"] = dtExpectedStartDate;
                    drNewRow["VisitNumber"] = intVisitNumber;
                    drNewRow["FriendlyVisitNumber"] = strFriendlyVisitNumber;
                    drNewRow["FullDescription"] = strFullDescription;
                    drNewRow["ContractorName"] = strContractorName;
                    drNewRow["VisitID"] = intVisitID;
                    drNewRow["ClientContractID"] = intClientContractID;
                    drNewRow["SiteContractID"] = intSiteContractID;
                    drNewRow["JobTypeID"] = intJobTypeID;
                    drNewRow["JobSubTypeID"] = intJobSubTypeID;
                    drNewRow["ContractDescription"] = strContractDescription;
                    drNewRow["LinkedToPersonTypeID"] = intLinkedToPersonTypeID;
                    drNewRow["LinkedToPersonType"] = strLinkedToPersonType;
                    drNewRow["JobTypeJobSubType"] = strJobTypeJobSubType;

                    dataSet_OM_Job.sp06420_OM_Visit_Completion_Wizard_Labour_Time.Rows.Add(drNewRow);
                    sbNewIDs.Append(drNewRow["LabourUsedTimingID"].ToString() + ",");
                }
                catch (Exception) { }
                
                // Update Labour Timing Count for each labour record //
                int intFoundRow = viewLabour.LocateByValue(0, view.Columns["LabourUsedID"], intLabourUsedID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    viewLabour.SetRowCellValue(intFoundRow, "LinkedTimingCount", Convert.ToInt32(viewLabour.GetRowCellValue(intFoundRow, "LinkedTimingCount")) + 1);
                }
            }
            view.EndUpdate();  // UnLock view //
            view.ExpandAllGroups();
            viewLabour.EndUpdate();

            // Highlight any recently added new rows //
            if (!string.IsNullOrWhiteSpace(sbNewIDs.ToString()))
            {
                char[] delimiters = new char[] { ',' };
                string[] strArray = sbNewIDs.ToString().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LabourUsedTimingID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                        CheckRow3(view, intRowHandle);
                    }
                }
            }
        }

        private void Block_Add_Labour_Timing()
        {
            GridView viewLabour = (GridView)gridControl8.MainView;
            int[] intRowHandles = viewLabour.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Labour records to block add time on \\ off site to before proceeding.", "Block Add Time On \\ Off Site", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intRowHandles.Length > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + " Labour record(s) selected. If you proceed a new Labour Timing record will be created for each of these Labour records.\n\nProceed?", "Block Add Time On \\ Off Site", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }
            string strTempID = "";
            string strTeamIDs = ",";
            string strStaffIDs = ",";
            foreach (int intRowHandle in intRowHandles)
            {
                strTempID = viewLabour.GetRowCellValue(intRowHandle, viewLabour.Columns["ContractorID"]).ToString() + ',';
                if (Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, viewLabour.Columns["LinkedToPersonTypeID"])) == 1)
                {
                    if (!strTeamIDs.Contains("," + strTempID + ',')) strTeamIDs += strTempID + ",";
                }
                else
                {
                    if (!strStaffIDs.Contains("," + strTempID + ',')) strStaffIDs += strTempID + ",";
                }
            }
            if (strTeamIDs.Length > 0) strTeamIDs = strTeamIDs.Remove(0, 1);  // Remove preceeding comma //
            if (strStaffIDs.Length > 0) strStaffIDs = strStaffIDs.Remove(0, 1);  // Remove preceeding comma //
            
            var fChildForm = new frm_OM_Job_Labour_On_Off_Site_Block_Add();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strPassedInTeamIDs = strTeamIDs;
            fChildForm.strPassedInStaffIDs = strStaffIDs;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Block Adding...");

                GridView viewLabourTiming = (GridView)gridControl9.MainView;
                GridView viewChild = (GridView)fChildForm.gridControl1.MainView;
                int intTeamMemberID = 0;
                int intTeamMemberPersonTypeID = 0;
                string strTeamMemberName = "";
                DateTime dtStartDate = DateTime.MinValue;
                DateTime dtEndDate = DateTime.MinValue;
                decimal decDuration = (decimal)0.00;

                int intLabourUsedID = 0;
                int intContractorID = 0;
                int intJobID = 0;
                int intClientID = 0;
                int intSiteID = 0;
                string strSiteName = "";
                string strClientName = "";
                string strJobSubTypeDescription = "";
                string strJobTypeDescription = "";
                DateTime dtExpectedStartDate;
                int intVisitNumber = 0;
                string strFriendlyVisitNumber = "";
                string strFullDescription = "";
                string strContractorName = "";
                int intVisitID = 0;
                int intClientContractID = 0;
                int intSiteContractID = 0;
                int intJobTypeID = 0;
                int intJobSubTypeID = 0;
                string strContractDescription = "";
                int intLinkedToPersonTypeID = 0;
                string strLinkedToPersonType = "";
                string strJobTypeJobSubType = "";

                GridView view = (GridView)gridControl9.MainView;
                view.PostEditor();
                StringBuilder sbNewIDs = new StringBuilder();

                viewLabour.BeginUpdate();
                view.BeginUpdate();  // Lock view //

                try
                {
                    foreach (int intRowHandle in intRowHandles)
                    {
                        intLabourUsedID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "LabourUsedID"));
                        intContractorID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "ContractorID"));
                        intJobID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "JobID"));
                        intClientID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "ClientID"));
                        intSiteID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "SiteID"));
                        strSiteName = viewLabour.GetRowCellValue(intRowHandle, "SiteName").ToString();
                        strClientName = viewLabour.GetRowCellValue(intRowHandle, "ClientName").ToString();
                        strJobSubTypeDescription = viewLabour.GetRowCellValue(intRowHandle, "JobSubTypeDescription").ToString();
                        strJobTypeDescription = viewLabour.GetRowCellValue(intRowHandle, "JobTypeDescription").ToString();
                        dtExpectedStartDate = Convert.ToDateTime(viewLabour.GetRowCellValue(intRowHandle, "ExpectedStartDate"));
                        intVisitNumber = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "VisitNumber"));
                        strFriendlyVisitNumber = viewLabour.GetRowCellValue(intRowHandle, "FriendlyVisitNumber").ToString();
                        strFullDescription = viewLabour.GetRowCellValue(intRowHandle, "FullDescription").ToString();
                        strContractorName = viewLabour.GetRowCellValue(intRowHandle, "ContractorName").ToString();
                        intVisitID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "VisitID"));
                        intClientContractID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "ClientContractID"));
                        intSiteContractID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "SiteContractID"));
                        intJobTypeID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "JobTypeID"));
                        intJobSubTypeID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                        strContractDescription = viewLabour.GetRowCellValue(intRowHandle, "ContractDescription").ToString();
                        intLinkedToPersonTypeID = Convert.ToInt32(viewLabour.GetRowCellValue(intRowHandle, "LinkedToPersonTypeID"));
                        strLinkedToPersonType = viewLabour.GetRowCellValue(intRowHandle, "LinkedToPersonType").ToString();
                        strJobTypeJobSubType = viewLabour.GetRowCellValue(intRowHandle, "JobTypeJobSubType").ToString();

                        for (int i = 0; i < viewChild.DataRowCount; i++)
                        {
                            if (Convert.ToBoolean(viewChild.GetRowCellValue(i, "CheckMarkSelection")))
                            {
                                intTeamMemberID = Convert.ToInt32(viewChild.GetRowCellValue(i, viewChild.Columns["TeamMemberID"]));
                                intTeamMemberPersonTypeID = Convert.ToInt32(viewChild.GetRowCellValue(i, viewChild.Columns["LabourTypeID"]));

                                strTeamMemberName = viewChild.GetRowCellValue(i, viewChild.Columns["TeamMemberName"]).ToString();
                                dtStartDate = Convert.ToDateTime(viewChild.GetRowCellValue(i, viewChild.Columns["StartDate"]));
                                dtEndDate = Convert.ToDateTime(viewChild.GetRowCellValue(i, viewChild.Columns["EndDate"]));
                                decDuration = (decimal)viewChild.GetRowCellValue(i, viewChild.Columns["Duration"]);

                                using (var CheckValid = new DataSet_OM_JobTableAdapters.QueriesTableAdapter())
                                {
                                    CheckValid.ChangeConnectionString(strConnectionString);
                                    int intMatchCount = Convert.ToInt32(CheckValid.sp06422_OM_Visit_Completion_Wizard_Labour_Time_Block_Add_Check_Valid(intLabourUsedID, intContractorID, intTeamMemberPersonTypeID, intTeamMemberID));
                                    if (intMatchCount > 0)
                                    {
                                        DataRow drNewRow;
                                        drNewRow = dataSet_OM_Job.sp06420_OM_Visit_Completion_Wizard_Labour_Time.NewRow();
                                        drNewRow["strMode"] = "add";
                                        drNewRow["strRecordIDs"] = "";
                                        drNewRow["LabourUsedID"] = intLabourUsedID;
                                        drNewRow["TeamMemberID"] = intTeamMemberID;
                                        drNewRow["StartDateTime"] = dtStartDate;
                                        drNewRow["EndDateTime"] = dtEndDate;
                                        drNewRow["Duration"] = decDuration;
                                        drNewRow["Remarks"] = null;
                                        drNewRow["PdaCreatedID"] = null;
                                        drNewRow["TeamMemberName"] = strTeamMemberName;
                                        drNewRow["ContractorID"] = intContractorID;
                                        drNewRow["JobID"] = intJobID;
                                        drNewRow["ClientID"] = intClientID;
                                        drNewRow["SiteID"] = intSiteID;
                                        drNewRow["SiteName"] = strSiteName;
                                        drNewRow["ClientName"] = strClientName;
                                        drNewRow["JobSubTypeDescription"] = strJobSubTypeDescription;
                                        drNewRow["JobTypeDescription"] = strJobTypeDescription;
                                        drNewRow["ExpectedStartDate"] = dtExpectedStartDate;
                                        drNewRow["VisitNumber"] = intVisitNumber;
                                        drNewRow["FriendlyVisitNumber"] = strFriendlyVisitNumber;
                                        drNewRow["FullDescription"] = strFullDescription;
                                        drNewRow["ContractorName"] = strContractorName;
                                        drNewRow["VisitID"] = intVisitID;
                                        drNewRow["ClientContractID"] = intClientContractID;
                                        drNewRow["SiteContractID"] = intSiteContractID;
                                        drNewRow["JobTypeID"] = intJobTypeID;
                                        drNewRow["JobSubTypeID"] = intJobSubTypeID;
                                        drNewRow["ContractDescription"] = strContractDescription;
                                        drNewRow["LinkedToPersonTypeID"] = intLinkedToPersonTypeID;
                                        drNewRow["LinkedToPersonType"] = strLinkedToPersonType;
                                        drNewRow["JobTypeJobSubType"] = strJobTypeJobSubType;

                                        dataSet_OM_Job.sp06420_OM_Visit_Completion_Wizard_Labour_Time.Rows.Add(drNewRow);
                                        sbNewIDs.Append(drNewRow["LabourUsedTimingID"].ToString() + ",");
                                        
                                        // Update Labour Timing Count for each labour record //
                                        int intFoundRow = viewLabour.LocateByValue(0, view.Columns["LabourUsedID"], intLabourUsedID);
                                        if (intFoundRow != GridControl.InvalidRowHandle)
                                        {
                                            viewLabour.SetRowCellValue(intFoundRow, "LinkedTimingCount", Convert.ToInt32(viewLabour.GetRowCellValue(intFoundRow, "LinkedTimingCount")) + 1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex) {}

                view.EndUpdate();  // UnLock view //
                view.ExpandAllGroups();
                viewLabour.EndUpdate();

                // Highlight any recently added new rows //
                if (!string.IsNullOrWhiteSpace(sbNewIDs.ToString()))
                {
                    char[] delimiters = new char[] { ',' };
                    string[] strArray = sbNewIDs.ToString().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    int intID = 0;
                    int intRowHandle = 0;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["LabourUsedTimingID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                            CheckRow3(view, intRowHandle);
                        }
                    }
                }
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show("Block Add Completed Successfully.", "Block Add Labour Time On \\ Off Site", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Block_Edit_Labour_Timing()
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControl9.MainView;
            int intRowCount = view.DataRowCount;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Labour Timings to block edit then try again.", "Block Edit Labour Timing", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Visit_Completion_Wizard_Block_Edit_Job_Labour_Time();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strSignaturePath = this.strSignaturePath;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Updating...");

                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                view.BeginUpdate();
                view.BeginSort();
                try
                {
                    foreach (int intRowHandle in intRowHandles)
                    {
                        if (fChildForm.dtStartDateTime != null) view.SetRowCellValue(intRowHandle, "StartDateTime", fChildForm.dtStartDateTime);
                        if (fChildForm.dtEndDateTime != null) view.SetRowCellValue(intRowHandle, "EndDateTime", fChildForm.dtEndDateTime);
                        if (fChildForm.decDuration != null) view.SetRowCellValue(intRowHandle, "Duration", fChildForm.decDuration);
                        if (fChildForm.strRemarks != null) view.SetRowCellValue(intRowHandle, "Remarks", fChildForm.strRemarks);

                        CheckRow3(view, intRowHandle);
                    }
                }
                catch (Exception) { }
                view.EndSort();
                view.EndUpdate();
            }
            if (splashScreenManager.IsSplashFormVisible)
            {
                splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager.CloseWaitForm();
            }
        }

        #endregion


        #region Finish Page

        private void btnFinishPrevious_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep3;
        }
        private void btnFinish_Click(object sender, EventArgs e)
        {
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Saving Changes...");

            try
            {
                sp06413_OM_Visit_Completion_Wizard_Get_VisitsTableAdapter.Update(dataSet_OM_Visit);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while updating the Visit(s) [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            try
            {
                sp06416_OM_Visit_Completion_Wizard_Get_JobsTableAdapter.Update(dataSet_OM_Job);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while updating the Job(s) [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            try
            {
                sp06420_OM_Visit_Completion_Wizard_Labour_TimeTableAdapter.Update(dataSet_OM_Job);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while updating the Labout Time(s) [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
              
            // Notify any open instances of Parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                if (Application.OpenForms[strCaller] != null)
                {
                    if (strCaller == "frm_OM_Visit_Manager") (Application.OpenForms[strCaller] as frm_OM_Visit_Manager).UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Visit, "");
                    else if (strCaller == "frm_OM_Job_Manager") (Application.OpenForms[strCaller] as frm_OM_Site_Contract_Manager).UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Visit, "");
                }
            }
            this.dataSet_OM_Contract.AcceptChanges();

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }

            this.Close();
        }

        #endregion


        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }


        private void Block_Edit_Record()
        {
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    {
                        Block_Edit_Visits();
                    }
                    break;
                case Utils.enmFocusedGrid.Jobs:
                    {
                        Block_Edit_Jobs();
                    }
                    break;
                case Utils.enmFocusedGrid.LabourTiming:
                    {
                        Block_Edit_Labour_Timing();
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.LabourTiming:
                    {
                        if (strFormMode == "view") return;
                        int[] intRowHandles;
                        int intCount = 0;
                        string strMessage = "";


                        GridView view = (GridView)gridControl9.MainView;
                        GridView viewLabour = (GridView)gridControl8.MainView;
                        int intFoundRow = 0;
                        int intLabourUsedID = 0;

                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more new labour timing records to delete by clicking on them then try again.", "Delete New Labour Timing Records", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 new Labour Timing record" : Convert.ToString(intRowHandles.Length) + " new Labour Timing records") + " selected for deleting!\n\nProceed?";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete New Labour Timing Records", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                intLabourUsedID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[i], "LabourUsedID"));
                                try
                                {
                                    view.DeleteRow(intRowHandles[i]);
                                    
                                    // Update Labour Timing Count for each labour record //
                                    intFoundRow = viewLabour.LocateByValue(0, view.Columns["LabourUsedID"], intLabourUsedID);
                                    if (intFoundRow != GridControl.InvalidRowHandle)
                                    {
                                        viewLabour.SetRowCellValue(intFoundRow, "LinkedTimingCount", Convert.ToInt32(viewLabour.GetRowCellValue(intFoundRow, "LinkedTimingCount")) - 1);
                                    }
                                }
                                catch (Exception) { }
                            }
                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            SetMenuStatus();
                        }
                    }
                    break;
                default:
                    break;
            }
        }


        public override void PostViewClick(object sender, EventArgs e, int row)
        {
            //GridView view = (GridView)sender;
            //if (view.GridControl.MainView.Name != "gridView4") return;  // Only fire for appropriate view //
            //if (row == GridControl.InvalidRowHandle) return;
            //Set_Selected_Visit_Count_Label();
        }

        private void bbiTick_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            /*GridView view = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    {
                        view = (GridView)gridControl4.MainView;
                    }
                    break;
                default:
                    return;
            }
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                view.BeginSelection();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);
                }
                view.EndSelection();
                view.EndUpdate();
            }
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    {
                        Set_Selected_Visit_Count_Label();
                    }
                    break;
                default:
                    return;
            }*/
        }

        private void bbiUntick_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            /*GridView view = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    {
                        view = (GridView)gridControl4.MainView;
                    }
                    break;
                default:
                    return;
            }
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                view.BeginSelection();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 0);
                }
                view.EndSelection();
                view.EndUpdate();
            }
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Visit:
                    {
                        Set_Selected_Visit_Count_Label();
                    }
                    break;
                default:
                    return;
            }*/
        }






    }
}
